﻿Imports BusinessLogicLayer.Ventas
Imports CapaEntidad

Public Class FrmManteNumerador
    Dim VL_SRV_VENTAS As SrvVentas
    Dim VL_BOTON As String = ""

    Public Const AvisoMSGBOX As String = "SISTEMA TESORERIA"




    Private Sub FrmManteNumerador_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.TabPersonalizado1.TabPages("MANTENIMIENTO").Enabled = False
        VL_DOCUMENTO = "VENTAS"
        LISTAR_DOCUMENTOS()
    End Sub

#Region "LISTAR, AGREGAR, ACTUALIZAR Y ELIMINAR"
    Private Sub LISTAR_DOCUMENTOS()

        VL_SRV_VENTAS = New SrvVentas()
        'dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_DOCUMENTOS(VL_DOCUMENTO)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboDocumentos.DataSource = VL_BeanResultado.dtResultado
            Me.cboDocumentos.DisplayMember = "DESCRIPCION"
            Me.cboDocumentos.ValueMember = "ID_DOCUMENTO"

        End If
    End Sub

    Private Sub LISTAR_DOC_NUMERADOR(VL_ID_DOC As String)

        VL_SRV_VENTAS = New SrvVentas()
        'dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_DOCU_NUMERADOR(VL_ID_DOC)

        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaNumerador.DataSource = VL_BeanResultado.dtResultado

        End If
    End Sub

    Private Sub AGREGAR()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ID_DOC As String = cboDocumentos.SelectedValue
        Dim VL_SERIE As String = txtSerie.Text
        Dim VL_NUMERO As String = txtNumero.Text
        Dim VL_USUARIO As String = gUsuario

        VL_BeanResultado = VL_SRV_VENTAS.SRV_AGREGAR_NUMERADOR_CONT(VL_ID_DOC, VL_SERIE, VL_NUMERO, VL_USUARIO, 1)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ACTUALIZAR()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        Dim VL_ID_DOC_NUM As Int16 = rgListaNumerador.CurrentRow.Cells("ID_DOC_NUMERADOR").Value
        Dim VL_ID_DOC As String = cboDocumentos.SelectedValue
        Dim VL_SERIE As String = txtSerie.Text
        Dim VL_NUMERO As String = txtNumero.Text
        Dim VL_USUARIO As String = gUsuario

        VL_BeanResultado = VL_SRV_VENTAS.SRV_ACTUALIZAR_NUMERADOR_CONT(VL_ID_DOC_NUM, VL_ID_DOC, VL_SERIE, VL_NUMERO, VL_USUARIO, 2)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ELIMINAR()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        Dim VL_ID_DOC_NUM As Int16 = rgListaNumerador.CurrentRow.Cells("ID_DOC_NUMERADOR").Value
        Dim VL_ID_DOC As String = cboDocumentos.SelectedValue
        Dim VL_SERIE As String = txtSerie.Text
        Dim VL_NUMERO As String = txtNumero.Text
        Dim VL_USUARIO As String = gUsuario

        VL_BeanResultado = VL_SRV_VENTAS.SRV_ELIMINAR_NUMERADOR_CONT(VL_ID_DOC_NUM, "", "", "", "", 3)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

#End Region


    Sub LIMPIAR()
        txtNumero.Text = ""
        txtNumero.Text = ""
        lblDoc.Text = ""
        lblIdCuenta.Text = ""
    End Sub


    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        LISTAR_DOC_NUMERADOR(cboDocumentos.SelectedValue)
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Me.TabPersonalizado1.TabPages("MANTENIMIENTO").Enabled = True
        Me.TabPersonalizado1.TabPages("CONSULTA").Enabled = False
        VL_BOTON = "1"
        TabPersonalizado1.SelectedIndex = 1
        txtSerie.Enabled = True
        lblDoc.Text = cboDocumentos.Text
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        If (VL_BOTON = "1") Then  ''AGREGAR VENTAS

            If txtSerie.Text = "" Then
                MessageBox.Show("Ingrese Serie..'", "SISTEMA TESORERIA")
                Return
            ElseIf txtSerie.TextLength < 3 Then
                MessageBox.Show("Ingrese Serie Correctamente son 3 digitos...", "SISTEMA TESORERIA")
                Return
            ElseIf txtNumero.Text = "" Then
                MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
                Return
            ElseIf txtNumero.TextLength < 7 Then
                MessageBox.Show("Ingrese Número Correctamente son 7 digitos...", "SISTEMA TESORERIA")
                Return
            Else
                AGREGAR()
                LISTAR_DOC_NUMERADOR(cboDocumentos.SelectedValue)


                TabPersonalizado1.SelectedIndex = 0
            End If

        End If

        If (VL_BOTON = "2") Then ''ACTUALIZAR VENTA
            If txtSerie.Text = "" Then
                MessageBox.Show("Ingrese Serie..'")
                Return
            ElseIf txtNumero.Text = "" Then
                MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
                Return
            ElseIf txtNumero.TextLength < 7 Then
                MessageBox.Show("Ingrese Número Correctamente son 7 digitos...", "SISTEMA TESORERIA")
                Return
            Else
                ACTUALIZAR()
                LISTAR_DOC_NUMERADOR(cboDocumentos.SelectedValue)

            End If
        End If
        TabPersonalizado1.SelectedIndex = 0
        Me.TabPersonalizado1.TabPages("MANTENIMIENTO").Enabled = False
        Me.TabPersonalizado1.TabPages("CONSULTA").Enabled = True
        LIMPIAR()
    End Sub

    Private Sub rgListaNumerador_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles rgListaNumerador.CellDoubleClick
        VL_BOTON = "2"
       
        Me.TabPersonalizado1.TabPages("MANTENIMIENTO").Enabled = True
        Me.TabPersonalizado1.TabPages("CONSULTA").Enabled = False
        LIMPIAR()

        TabPersonalizado1.SelectedIndex = 1
       
        lblIdCuenta.Text = rgListaNumerador.CurrentRow.Cells("ID_DOC_NUMERADOR").Value + 0
        lblDoc.Text = cboDocumentos.Text
        txtSerie.Text = rgListaNumerador.CurrentRow.Cells("SERIE").Value + ""
        txtNumero.Text = rgListaNumerador.CurrentRow.Cells("NUMERO").Value + ""
       
    End Sub

    Private Sub AGREGAR_IMPUESTO_Click(sender As Object, e As EventArgs) Handles AGREGAR_IMPUESTO.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR()
            LISTAR_DOC_NUMERADOR(cboDocumentos.SelectedValue)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        TabPersonalizado1.SelectedIndex = 0
        Me.TabPersonalizado1.TabPages("CONSULTA").Enabled = True
        Me.TabPersonalizado1.TabPages("MANTENIMIENTO").Enabled = False
        LIMPIAR()
    End Sub
End Class
