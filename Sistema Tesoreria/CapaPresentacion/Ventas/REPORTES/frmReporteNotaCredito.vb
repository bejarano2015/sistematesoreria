﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class frmReporteNotaCredito
#Region "declaraciones"

    Private ID_DOC_VENTA As Int32 = 0
    Private NombreReporte As [String] = ""
    Dim rptNotaCredito As New rptNotaCredito
    Public Property VG_ID_PROGPAGO As String

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Private Shared frmInstance As frmReporteBoletas = Nothing

#End Region

    Public Sub New(VL_NOMREPORTE As String, VL_ID_DOC_VENTA As Int32)
        InitializeComponent()
        NombreReporte = VL_NOMREPORTE
        ID_DOC_VENTA = VL_ID_DOC_VENTA
    End Sub
    Sub VER_IMPRESION()
        rptNotaCredito.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        crParameterFieldDefinitions = rptNotaCredito.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@ID_DOC_VENTA")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = ID_DOC_VENTA
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        'rptBoletas.PrintToPrinter(1, False, 0, 1)
        CrystalReportViewer1.ReportSource = rptNotaCredito
    End Sub
    Sub IMPRIMIR()
        rptNotaCredito.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        crParameterFieldDefinitions = rptNotaCredito.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@ID_DOC_VENTA")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = ID_DOC_VENTA
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        rptNotaCredito.PrintToPrinter(1, False, 0, 1)
        'CrystalReportViewer1.ReportSource = rptBoletas
    End Sub

    Private Sub CrystalReportViewer1_Load(sender As Object, e As EventArgs) Handles CrystalReportViewer1.Load
        If NombreReporte = "4" Then
            VER_IMPRESION()
        ElseIf NombreReporte = "5" Then
            IMPRIMIR()
        End If
    End Sub

    Private Sub frmReporteNotaCredito_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
End Class