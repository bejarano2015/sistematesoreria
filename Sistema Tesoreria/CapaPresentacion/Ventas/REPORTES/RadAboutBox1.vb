﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection
Imports CapaEntidad
Imports BusinessLogicLayer.Ventas

Partial Public Class RadAboutBox1
    Inherits Telerik.WinControls.UI.RadForm

    Public Sub New()
        InitializeComponent()

    End Sub
#Region "DECLARACIONES"
    Dim VL_ID_DOCUMENTO As String
    Dim VL_SERIE As String
    Dim VL_SRV_VENTAS As SrvVentas
    'Public Shared VL_DOCUMENTO As String

#End Region
    Private Sub RadAboutBox1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListarDocumentos(cboDocumentos)
    End Sub
    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        If cboSerie.Text <> "" Then
            ENVIAR_DATOS()
            DialogResult = System.Windows.Forms.DialogResult.OK
        Else
            MessageBox.Show("No selecciono una serie...", "IMPRESION", MessageBoxButtons.OK, MessageBoxIcon.[Error])
            Return
        End If

    End Sub

    Public Sub ENVIAR_DATOS()
        'Aplicar_Validacion OBJ = new Aplicar_Validacion();         

        VL_ID_DOCUMENTO = cboDocumentos.SelectedValue.ToString()
        VL_SERIE = cboSerie.Text
        Hide()
       
    End Sub

    Private Sub cboDocumentos_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDocumentos.SelectedValueChanged
        If cboDocumentos.SelectedValue IsNot Nothing Then
            If cboDocumentos.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                ListarSerie(cboSerie)
            End If
        End If
    End Sub

    Private Sub ListarDocumentos(cboDocumentos As Telerik.WinControls.UI.RadDropDownList)

        Try
            VL_SRV_VENTAS = New SrvVentas()

            Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

            VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_DOCUMENTOS(VL_DOCUMENTO)

            If VL_BeanResultado.blnExiste = True Then
                Me.cboDocumentos.DataSource = VL_BeanResultado.dtResultado
                Me.cboDocumentos.DisplayMember = "DESCRIPCION"
                Me.cboDocumentos.ValueMember = "ID_DOCUMENTO"
                Me.cboDocumentos.SelectedIndex = 0

            End If
        Catch ex As Exception


        End Try


    End Sub

    Private Sub ListarSerie(cboSerie As Telerik.WinControls.UI.RadDropDownList)

        cboSerie.Text = ""
        Try
            VL_SRV_VENTAS = New SrvVentas()
            Dim ID_DOCUMENTO As Int16
            ID_DOCUMENTO = cboDocumentos.SelectedValue
            Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

            VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_SERIE(ID_DOCUMENTO)

            If VL_BeanResultado.blnExiste = True Then
                Me.cboSerie.DataSource = VL_BeanResultado.dtResultado
                Me.cboSerie.DisplayMember = "SERIE"
                Me.cboSerie.ValueMember = "ID_DOC_NUMERADOR"

            End If
        Catch ex As Exception


        End Try


    End Sub

    
End Class

