﻿Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports BusinessLogicLayer.Procesos
Imports BusinessLogicLayer.Ventas.Reportes
Imports System.IO
Imports Telerik.Reporting.Processing
Imports System.Drawing.Printing
Imports CapaEntidad



Public Class WFReportesTelerik1

#Region "declaraciones"
    Private NombreReporte As [String] = ""
    Private vl_Proformacodigo As String = ""

    Private EXPECODIGO As [String] = ""
    Private AÑO As String = ""
    Private PERIODO As [String] = ""
    Private tipo As [String] = ""
    Private BOLETACODIGOPAGO As [String] = ""
    Private FECHAINI As [String] = ""
    Private FECHAFIN As [String] = ""
    Private FILTRO As [String] = ""
    Private ID_DOC_VENTA As [String] = ""

#End Region
    Public Sub New()
        InitializeComponent()
    End Sub
    Public Sub New(VL_NOMREPORTE As String, VL_ID_DOC_VENTA As Int32)
        InitializeComponent()
        NombreReporte = VL_NOMREPORTE
        ID_DOC_VENTA = VL_ID_DOC_VENTA
    End Sub
    Public Sub New(VL_NOMREPORTE As String, vlProformacodigo As String)
        InitializeComponent()
        NombreReporte = VL_NOMREPORTE
        vl_Proformacodigo = vlProformacodigo
    End Sub
    Public Sub New(VL_NOMREPORTE As String, VL_EXPECODIGO As String, VL_AÑO As String, VL_PERIODO As String, vl_tipo As String, VL_BOLETACODIGOPAGO As String, _
        VL_FECHAINI As String, VL_FECHAFIN As String, VL_FILTRO As String)
        InitializeComponent()
        NombreReporte = VL_NOMREPORTE
        EXPECODIGO = VL_EXPECODIGO
        AÑO = VL_AÑO
        PERIODO = VL_PERIODO
        tipo = vl_tipo
        BOLETACODIGOPAGO = VL_BOLETACODIGOPAGO
        FECHAINI = VL_FECHAINI
        FECHAFIN = VL_FECHAFIN
        FILTRO = VL_FILTRO
    End Sub

    Private Sub ReportViewer1_Load(sender As Object, e As EventArgs) Handles ReportViewer1.Load

        'If NombreReporte = "1" Then
        '    CargarReporteProforma()
        'ElseIf NombreReporte = "2" Then
        '    CargarReporteSeguimiento()
        'ElseIf NombreReporte = "3" Then
        '    CargarReporteDecistidos()
        'Else
        If NombreReporte = "4" Then
            CargarBoletaImpresion()
        ElseIf NombreReporte = "5" Then
            CargarBoletamesImpresion()
        End If
    End Sub

    'Private Sub CargarReporteDecistidos()
    '    Dim VL_SrvDevolucion As New SrvDevolucion()
    '    Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()


    '    Try
    '        VL_BeanResultado = VL_SrvDevolucion.Fnc_Reporte_Devolucion("0001", "3", "", "", "")

    '        If VL_BeanResultado.blnExiste = True Then
    '            Dim rpdev As New ReporteDevoluciones()
    '            rpdev.DataSource = (VL_BeanResultado.dtResultado)
    '            ReportViewer1.Report = rpdev
    '            Me.ReportViewer1.RefreshReport()
    '        Else
    '            MessageBox.Show(VL_BeanResultado.strMensaje)

    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString())
    '    End Try
    'End Sub

    'Private Sub CargarReporteProforma()
    '    Dim VL_SrvProforma As New SrvProforma()
    '    Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()

    '    Dim objProforma As New BeanProforma(GlobalVar.VG_OBRA_CODIGO, vl_Proformacodigo, "", "")

    '    Try
    '        VL_BeanResultado = VL_SrvProforma.Fnc_Listar_Proforma(objProforma)

    '        If VL_BeanResultado.blnExiste = True Then
    '            Dim rpProf As New RptProforma()
    '            rpProf.DataSource = (VL_BeanResultado.dtResultado)
    '            reportViewer1.Report = rpProf

    '            Me.reportViewer1.RefreshReport()
    '        Else
    '            MessageBox.Show(VL_BeanResultado.strMensaje)

    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString())
    '    End Try
    'End Sub

    'Private Sub CargarReporteSeguimiento()
    '    Dim VL_SSrvCredito As New SrvCredito()
    '    Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()

    '    Dim objCredito As New BeanCredito(vl_Proformacodigo, "0001")

    '    Try
    '        VL_BeanResultado = VL_SSrvCredito.Fnc_Reporte_SeguimientoCredito(objCredito)

    '        If VL_BeanResultado.blnExiste = True Then
    '            Dim RptSeguimientoCr As New GestionInmobiliario.Reportes.RPTSeguimientoCredito()
    '            RptSeguimientoCr.DataSource = (VL_BeanResultado.dtResultado)
    '            reportViewer1.Report = RptSeguimientoCr

    '            Me.reportViewer1.RefreshReport()
    '        Else
    '            MessageBox.Show(VL_BeanResultado.strMensaje)

    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString())
    '    End Try
    'End Sub


    Private Sub CargarBoletaImpresion()
        Dim VL_SrvBoleta As New SrvBoleta()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvBoleta.Fnc_Gargar_boltaxmes_expediente(ID_DOC_VENTA)

            'If VL_BeanResultado.blnExiste = True Then
            '    Dim rpBoleta As New rptBoletas()
            '    rpBoleta.DataSource = (VL_BeanResultado.dtResultado)
            '    ReportViewer1.Report = rpBoleta
            '    Me.ReportViewer1.RefreshReport()
            '    If tipo = "2" Then
            '        Dim printerSettings As New PrinterSettings()
            '        Dim printController As PrintController = New StandardPrintController()

            '        Dim reportProcessor As New ReportProcessor()
            '        reportProcessor.PrintController = printController
            '        reportProcessor.PrintReport(rpBoleta, printerSettings)
            '        Me.Close()

            ''    End If
            'Else
            'MessageBox.Show(VL_BeanResultado.strMensaje)

            'End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub CargarBoletamesImpresion()
        Dim VL_SrvBoleta As New SrvBoleta()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvBoleta.Fnc_Gargar_boltaxmes_expediente(ID_DOC_VENTA)

            If VL_BeanResultado.blnExiste = True Then
                'Dim rpBoleta As New Boletas()
                'rpBoleta.DataSource = (VL_BeanResultado.dtResultado)
                'ReportViewer1.Report = rpBoleta
                'Me.ReportViewer1.RefreshReport()

                'Dim printerSettings As New PrinterSettings()
                'Dim printController As PrintController = New StandardPrintController()

                'Dim reportProcessor As New ReportProcessor()
                'reportProcessor.PrintController = printController
                'reportProcessor.PrintReport(rpBoleta, printerSettings)
                'Me.Close()
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    'Private Sub imprimir(ByVal esPreview As Boolean)
    '    ' imprimir o mostrar el PrintPreview

    '    If prtSettings Is Nothing Then
    '        prtSettings = New PrinterSettings
    '    End If

    '    If chkSelAntes.Checked Then
    '        If seleccionarImpresora() = False Then Return
    '    End If

    '    If prtDoc Is Nothing Then
    '        prtDoc = New System.Drawing.Printing.PrintDocument
    '        AddHandler prtDoc.PrintPage, AddressOf prt_PrintPage
    '    End If

    '    ' la línea actual
    '    lineaActual = 0

    '    ' la configuración a usar en la impresión
    '    prtDoc.PrinterSettings = prtSettings

    '    If esPreview Then
    '        Dim prtPrev As New PrintPreviewDialog
    '        prtPrev.Document = prtDoc

    '        prtPrev.Text = "Previsualizar documento"
    '        prtPrev.ShowDialog()
    '    Else
    '        prtDoc.Print()
    '    End If
    '    End Sub
End Class
