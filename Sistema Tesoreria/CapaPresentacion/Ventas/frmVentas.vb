﻿Imports CapaEntidad.Ventas
Imports CapaEntidad.Login
Imports BusinessLogicLayer.Ventas
Imports CapaEntidad
Imports BusinessEntityLayer.Login
Imports System.Math
Imports BusinessLogicLayer.Ventas.Reportes
Imports CapaEntidad.Ventas.Reportes
Imports Telerik.WinControls.UI.Export

Public Class frmVentas

    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim VL_SRV_VENTAS As SrvVentas
    Dim VL_SRV_DOC_VENTAS As SrvDocVentasImpuesto
    Dim VL_SRV_VENTAS_LINEA As SrvVentasLinea
    Dim OBJ_VENTAS As New BeanVentas
    Dim OBJ_VENTAS_LINEA As New BeanVentaLinea
    Dim OBJ_DOC_VENTAS As New BeanDocVentasImpuesto
    Dim OBJ_USUARIO As New BeanLogin
    Dim VL_BOTON_VENTAS As String
    Dim VL_BOTON_LINEA As String
    Public Const AvisoMSGBOX As String = "SISTEMA TESORERIA"
    'Public Shared VL_DOCUMENTO As String

#Region "DECLARACIONES REPORTE IMPRESION"

    Dim VL_BOLE_CODIGO As String
    Dim VL_EXPE_CODIGO As String
    Dim VL_BOLETACODIGOPAGO As String
    Dim VL_SERIE_BOLETA As String
    Dim VL_NRO_BOLETA As String
    Dim VL_FECHAINI As [String]
    Dim VL_FECHAFIN As [String]
    Dim ID_NUEVO_DOC As Int32


#End Region
    Private Sub frmVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = False
        VL_DOCUMENTO = "VENTAS"
        LISTAR_VENTAS()

        LISTAR_COSTOS()
        LISTAR_DOCUMENTOS()
        LISTAR_FORMA_PAGO()
        LISTAR_MONEDAS()
        LISTAR_OBRA(cboObra)
        LISTAR_CLIENTES(cboObra.SelectedValue)
        LISTAR_OBRA(cboObraVentaLinea)
        'LISTAR_OBRA(cboObraFiltro)
        LISTAR_VENDEDOR()
        LISTAR_ITEM_VENTAS()
        'LISTAR_VENTAS_LINEA()
        cboEstado.SelectedIndex = 0

    End Sub

#Region "LISTAR COMBOS"
    'Private Sub LISTAR_OBRA(cboListaObra As Telerik.WinControls.UI.RadDropDownList)

    '    VL_SRV_VENTAS = New SrvVentas()
    '    dtTable = New DataTable()
    '    Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

    '    VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_OBRA()

    '    If VL_BeanResultado.blnExiste = True Then
    '        Me.cboObraVentaLinea.DataSource = VL_BeanResultado.dtResultado
    '        Me.cboObraVentaLinea.DisplayMember = "OBRA"
    '        Me.cboObraVentaLinea.ValueMember = "ID_OBRA"

    '    End If
    'End Sub

    Private Sub LISTAR_OBRA(cboListaObra As Telerik.WinControls.UI.RadDropDownList)

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_OBRA()

            If VL_BeanResultado.blnExiste = True Then
                cboListaObra.DataSource = VL_BeanResultado.dtResultado
                cboListaObra.DisplayMember = "OBRA"
                cboListaObra.ValueMember = "ID_OBRA"
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)
                cboListaObra.DataSource = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), AvisoMSGBOX)
        End Try

    End Sub

    Private Sub LISTAR_MONEDAS()

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_MONEDAS()
        If VL_BeanResultado.blnExiste = True Then
            Me.cboMonedas.DataSource = VL_BeanResultado.dtResultado
            Me.cboMonedas.DisplayMember = "MonDescripcion"
            Me.cboMonedas.ValueMember = "MonCodigo"

        End If
    End Sub

    Private Sub LISTAR_COSTOS()

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim EMPRESA_CODIGO As String = gEmpresa
        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_COSTOS(EMPRESA_CODIGO)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboCostos.DataSource = VL_BeanResultado.dtResultado
            Me.cboCostos.DisplayMember = "CCosDescripcion"
            Me.cboCostos.ValueMember = "CCosCodigo"

            Me.cboCostosVenta.DataSource = VL_BeanResultado.dtResultado
            Me.cboCostosVenta.DisplayMember = "CCosDescripcion"
            Me.cboCostosVenta.ValueMember = "CCosCodigo"

        End If
    End Sub
    Private Sub LISTAR_CLIENTES(id_obra As String)

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_CLIENTES(id_obra)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboClientes.DataSource = VL_BeanResultado.dtResultado
            Me.cboClientes.DisplayMember = "NOMBRES"
            Me.cboClientes.ValueMember = "CLIE_CODIGO"
        End If
    End Sub
    Private Sub LISTAR_FORMA_PAGO()

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_FORMA_PAGO()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboFormaPago.DataSource = VL_BeanResultado.dtResultado
            Me.cboFormaPago.DisplayMember = "NOMBRE_FOR_PAGO"
            Me.cboFormaPago.ValueMember = "ID_FOR_PAGO"

        End If
    End Sub
    Private Sub LISTAR_DOCUMENTOS()

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_DOCUMENTOS(VL_DOCUMENTO)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboDocumentos.DataSource = VL_BeanResultado.dtResultado
            Me.cboDocumentos.DisplayMember = "DESCRIPCION"
            Me.cboDocumentos.ValueMember = "ID_DOCUMENTO"

        End If
    End Sub
    Private Sub LISTAR_VENDEDOR()

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_VENDEDOR()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboVendedor.DataSource = VL_BeanResultado.dtResultado
            Me.cboVendedor.DisplayMember = "VENDEDOR"
            Me.cboVendedor.ValueMember = "ID_VENDEDOR"

        End If
    End Sub
    Private Sub LISTAR_ITEM_VENTAS()

        VL_SRV_VENTAS_LINEA = New SrvVentasLinea()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS_LINEA.SRV_LISTAR_ITEM_VENTAS()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboItemVenta.DataSource = VL_BeanResultado.dtResultado
            Me.cboItemVenta.DisplayMember = "Item"
            Me.cboItemVenta.ValueMember = "ID_Item"
        End If
    End Sub
    Private Sub LISTAR_CLIENTES_DETALLES(ID As String, ID_OBRA As String)

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_CLIENTES_DETALLES(ID, ID_OBRA)

        If VL_BeanResultado.blnExiste = True Then

            dtTable = VL_BeanResultado.dtResultado

            txtTipoDocumento.Text = dtTable.Rows(0)(2).ToString()
            txtDoc.Text = dtTable.Rows(0)(3).ToString()
            txtDireccion.Text = dtTable.Rows(0)(4).ToString()

        End If
    End Sub


#End Region


#Region "VENTAS LISTAR, AGREGAR,ACTULIZAR Y ELIMINAR"
    Private Sub LISTAR_VENTAS()

        VL_SRV_VENTAS = New SrvVentas()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_VENTAS.SRV_LISTAR_VENTAS()

        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaVentas.DataSource = VL_BeanResultado.dtResultado
        End If
    End Sub

    Private Sub AGREGAR_VENTAS()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ESTADO As String = cboEstado.Text
        'Dim VL_VIGENTE As String = cboVigente.SelectedItem

        If (VL_ESTADO = "ACTIVO") Then
            VL_ESTADO = "1"
        Else
            VL_ESTADO = "0"
        End If

        Dim DOCUMENTO As Int16 = cboDocumentos.SelectedValue.ToString()
        Dim CLIENTES As String = cboClientes.SelectedValue.ToString()
        Dim MONEDA As String = cboMonedas.SelectedValue.ToString()
        Dim VENDEDOR As Int16 = cboVendedor.SelectedValue.ToString()
        Dim FORMA_PAGO As Int16 = cboFormaPago.SelectedValue.ToString()
        Dim OBRA As Int16 = cboObra.SelectedValue.ToString()
        Dim COSTOS As String = cboCostos.SelectedValue.ToString()

        Dim USUARIO As String = gUsuario

        'OBJ_VENTAS = New BeanVentas(dtFecha.Value.ToString(), VL_ESTADO, DOCUMENTO, txtSerie.Text, txtNumero.Text, CLIENTES, txtDireccion.Text,
        '                            MONEDA, VENDEDOR, Decimal.Parse(txtNeto.Text + 0), Decimal.Parse(txtDescuento.Text + 0), Decimal.Parse(txtSubTotal.Text + 0),
        '                            Decimal.Parse(txtImpuesto.Text + 0), Decimal.Parse(txtTotal.Text + 0), txtObservaciones.Text, FORMA_PAGO, OBRA, COSTOS,
        '                            1, 1, "", "", USUARIO, 1)

        OBJ_VENTAS = New BeanVentas(dtFecha.Value.ToString(), VL_ESTADO, DOCUMENTO, txtSerie.Text, txtNumero.Text, CLIENTES, txtDireccion.Text,
                                    MONEDA, VENDEDOR, 0, 0, 0, 0, 0, txtObservaciones.Text, FORMA_PAGO, OBRA, COSTOS,
                                    1, 1, "", "", USUARIO, 1)


        VL_BeanResultado = VL_SRV_VENTAS.SRV_AGREGAR_VENTAS(OBJ_VENTAS)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ACTUALIZAR_VENTAS()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ESTADO As String = cboEstado.Text

        If (VL_ESTADO = "ACTIVO") Then
            VL_ESTADO = "1"
        Else
            VL_ESTADO = "0"
        End If

        Dim DOCUMENTO As Int16 = cboDocumentos.SelectedValue.ToString()
        Dim CLIENTES As String = cboClientes.SelectedValue.ToString()
        Dim MONEDA As String = cboMonedas.SelectedValue.ToString()
        Dim VENDEDOR As Int16 = cboVendedor.SelectedValue.ToString()
        Dim FORMA_PAGO As Int16 = cboFormaPago.SelectedValue.ToString()
        Dim OBRA As Int16 = cboObra.SelectedValue.ToString()
        Dim COSTOS As String = cboCostos.SelectedValue.ToString()

        Dim USUARIO As String = gUsuario

        'Dim ID As Int16 = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value
        Dim ID As Int16 = txtDocVenta.Text
        'OBJ_VENTAS.ID_DOC_VENTA = ID

        OBJ_VENTAS = New BeanVentas(dtFecha.Value.ToString(), VL_ESTADO, DOCUMENTO, txtSerie.Text, txtNumero.Text, CLIENTES, txtDireccion.Text,
                                    MONEDA, VENDEDOR, Decimal.Parse(txtNeto.Text), Decimal.Parse(txtDescuento.Text), Decimal.Parse(txtSubTotal.Text),
                                    Decimal.Parse(txtImpuesto.Text), Decimal.Parse(txtTotal.Text), txtObservaciones.Text, FORMA_PAGO, OBRA, COSTOS,
                                    1, 1, "", "1", USUARIO, ID, 2)


        VL_BeanResultado = VL_SRV_VENTAS.SRV_ACTUALIZAR_VENTAS(OBJ_VENTAS)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If


    End Sub

    Private Sub ELIMINAR_VENTAS()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim ID As Int16 = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value
        'OBJ_VENTAS.ID_DOC_VENTA = ID
        'OBJ_VENTAS = New BeanVentas("", 0, 1, "", 0, "", "", "", 0, 0, 0, 0, 0, 0, "", 0, 0, 0, 0, 0, 0, "1", "", ID, 3)

        VL_BeanResultado = VL_SRV_VENTAS.SRV_ELIMINAR_VENTAS(ID, 3)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

#End Region

#Region "VENTAS LINEA, AGREGAR, ACTUALIZAR Y ELIMINAR"
    Private Sub LISTAR_VENTAS_LINEA()

        VL_SRV_VENTAS_LINEA = New SrvVentasLinea()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Dim ID As Int16 = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value

        VL_BeanResultado = VL_SRV_VENTAS_LINEA.SRV_LISTAR_VENTAS_LINEA(ID)

        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaDetalles.DataSource = VL_BeanResultado.dtResultado

        End If
    End Sub

    Private Sub AGREGAR_VENTAS_LINEA()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim ID_ITEN As String = cboItemVenta.SelectedValue.ToString()
        Dim OBRA As Int16 = cboObraVentaLinea.SelectedValue.ToString()
        Dim COSTOS As String = cboCostosVenta.SelectedValue.ToString()

        Dim USUARIO As String = OBJ_USUARIO.USUA_NICKNAME

        OBJ_VENTAS_LINEA = New BeanVentaLinea(txtDocVenta.Text, ID_ITEN, txtCantidad.Text,
                                              txtPrecio.Text, txtDCTo.Text, txtImporte.Text, OBRA, COSTOS, txtGlosa.Text, 1)


        VL_BeanResultado = VL_SRV_VENTAS_LINEA.SRV_AGREGAR_VENTAS_LINEA(OBJ_VENTAS_LINEA)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ACTUALIZAR_VENTAS_LINEA()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


        Dim ID_ITEN As String = cboItemVenta.SelectedValue.ToString()
        Dim OBRA As Int16 = cboObraVentaLinea.SelectedValue.ToString()
        Dim COSTOS As String = cboCostosVenta.SelectedValue.ToString()


        Dim ID As Int16 = rgListaDetalles.CurrentRow.Cells("ID_Doc_VentaLinea").Value
        OBJ_VENTAS_LINEA.ID_DOC_VENTA_LINEA = ID


        OBJ_VENTAS_LINEA = New BeanVentaLinea(txtDocVenta.Text, ID_ITEN, txtCantidad.Text,
                                              txtPrecio.Text, txtDCTo.Text, txtImporte.Text, OBRA, COSTOS, txtGlosa.Text, ID, 2)


        VL_BeanResultado = VL_SRV_VENTAS_LINEA.SRV_ACTUALIZAR_VENTAS_LINEA(OBJ_VENTAS_LINEA)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If


    End Sub

    Private Sub ELIMINAR_VENTAS_LINEA()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


        Dim ID_ITEN As String = cboItemVenta.SelectedValue.ToString()
        Dim OBRA As Int16 = cboObraVentaLinea.SelectedValue.ToString()
        Dim COSTOS As String = cboCostosVenta.SelectedValue.ToString()


        Dim ID As Int16 = rgListaDetalles.CurrentRow.Cells("ID_Doc_VentaLinea").Value
        OBJ_VENTAS_LINEA.ID_DOC_VENTA_LINEA = ID


        OBJ_VENTAS_LINEA = New BeanVentaLinea(0, "", 0, 0, 0, 0, "", "", "", ID, 3)


        VL_BeanResultado = VL_SRV_VENTAS_LINEA.SRV_ELIMINAR_VENTAS_LINEA(OBJ_VENTAS_LINEA)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

#End Region

#Region "BOTONES"

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        VL_BOTON_VENTAS = "0"
        TabControl1.SelectedIndex = 1
        GRUPO_VENTA_LINEA.Enabled = False
        GRUPO_MANTENIMIENTO.Enabled = True
        btnAgregarVLinea.Enabled = False
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = True
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = False
        limpiarcontroles()
        LISTAR_DOC_VENTAS_IMPUESTO(0)
        rgListaDetalles.DataSource = ""

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        If (VL_BOTON_VENTAS = "0") Then  ''AGREGAR VENTAS

            'If txtSerie.Text = "" Then

            '    MessageBox.Show("Ingrese Serie..'", "SISTEMA TESORERIA")

            'ElseIf txtNumero.Text = "" Then
            '    MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
            'ElseIf txtImpuesto.Text = "" Then
            '    MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            'ElseIf txtTotal.Text = "" Then
            '    MessageBox.Show("Ingrese Total...", "SISTEMA TESORERIA")
            'ElseIf txtNeto.Text = "" Then
            '    MessageBox.Show("Ingrese Neto...", "SISTEMA TESORERIA")
            'ElseIf txtDescuento.Text = "" Then
            '    MessageBox.Show("Ingrese Descuento...", "SISTEMA TESORERIA")
            'ElseIf txtSubTotal.Text = "" Then
            '    MessageBox.Show("Ingrese SubTotal...", "SISTEMA TESORERIA")
            'Else

            AGREGAR_VENTAS()

            LISTAR_VENTAS()
            LISTAR_VENTAS_LINEA()
            GRUPO_VENTA_LINEA.Enabled = False
            TabControl1.SelectedIndex = 0
            'End If

        End If

        If (VL_BOTON_VENTAS = "1") Then ''ACTUALIZAR VENTA
            'If txtSerie.Text = "" Then

            '    MessageBox.Show("Ingrese Serie..'")

            'ElseIf txtNumero.Text = "" Then
            '    MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
            '    '    'ElseIf txtObservaciones.Text = "" Then
            '    '    MessageBox.Show("Ingrese Observaciones...", "SISTEMA TESORERIA")
            'ElseIf txtImpuesto.Text = "" Then
            '    MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            'ElseIf txtTotal.Text = "" Then
            '    MessageBox.Show("Ingrese Total...", "SISTEMA TESORERIA")
            'ElseIf txtNeto.Text = "" Then
            '    MessageBox.Show("Ingrese Neto...", "SISTEMA TESORERIA")
            'ElseIf txtDescuento.Text = "" Then
            '    MessageBox.Show("Ingrese Descuento...", "SISTEMA TESORERIA")
            'ElseIf txtSubTotal.Text = "" Then
            '    MessageBox.Show("Ingrese SubTotal...", "SISTEMA TESORERIA")
            'Else

            AGREGAR_DOC_IMPUESTO()
            ACTUALIZAR_VENTAS()
            LISTAR_VENTAS()
            LISTAR_VENTAS_LINEA()
            GRUPO_VENTA_LINEA.Enabled = True

            'End If
        End If
        TabControl1.SelectedIndex = 0
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = False
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = True
        limpiarcontroles()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click

        TabControl1.SelectedIndex = 0
        GRUPO_VENTA_LINEA.Enabled = True
        GRUPO_MANTENIMIENTO.Enabled = True
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = True
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = False
        limpiarcontroles()
    End Sub

    Private Sub btnAgregarVLinea_Click(sender As Object, e As EventArgs) Handles btnAgregarVLinea.Click
        GRUPO_VENTA_LINEA.Enabled = True
        GRUPO_MANTENIMIENTO.Enabled = False
        VL_BOTON_LINEA = "3"
    End Sub

    Private Sub btnGuardarVenta_Click(sender As Object, e As EventArgs) Handles btnGuardarVenta.Click

        If (VL_BOTON_LINEA = "3") Then  ''AGREGAR VENTAS

            If txtDocVenta.Text = "" Then
                MessageBox.Show("Ingrese Número documento..'", "SISTEMA TESORERIA")
            ElseIf txtCantidad.Text = "" Then
                MessageBox.Show("Ingrese Cantidad...", "SISTEMA TESORERIA")
            ElseIf txtGlosa.Text = "" Then
                MessageBox.Show("Ingrese Glosa...", "SISTEMA TESORERIA")
            ElseIf txtPrecio.Text = "" Then
                MessageBox.Show("Ingrese Precio...", "SISTEMA TESORERIA")
            ElseIf txtImporte.Text = "" Then
                MessageBox.Show("Ingrese Importe...", "SISTEMA TESORERIA")
            ElseIf txtDCTo.Text = "" Then
                MessageBox.Show("Ingrese descuento..", "SISTEMA TESORERIA")
            Else
                AGREGAR_VENTAS_LINEA()
                LISTAR_VENTAS_LINEA()
                LISTAR_VENTAS()
                GRUPO_VENTA_LINEA.Enabled = False
                GRUPO_MANTENIMIENTO.Enabled = True

            End If

        End If

        If (VL_BOTON_LINEA = "4") Then ''ACTUALIZAR VENTA
            If txtDocVenta.Text = "" Then
                MessageBox.Show("Ingrese Número documento..'")
            ElseIf txtCantidad.Text = "" Then
                MessageBox.Show("Ingrese Cantidad...", "SISTEMA TESORERIA")
            ElseIf txtGlosa.Text = "" Then
                MessageBox.Show("Ingrese Glosa...", "SISTEMA TESORERIA")
            ElseIf txtPrecio.Text = "" Then
                MessageBox.Show("Ingrese Precio...", "SISTEMA TESORERIA")
            ElseIf txtImporte.Text = "" Then
                MessageBox.Show("Ingrese Importe...", "SISTEMA TESORERIA")
            ElseIf txtDCTo.Text = "" Then
                MessageBox.Show("Ingrese descuento..", "SISTEMA TESORERIA")
            Else
                ACTUALIZAR_VENTAS_LINEA()
                LISTAR_VENTAS_LINEA()
                LISTAR_VENTAS()
                GRUPO_VENTA_LINEA.Enabled = False
                GRUPO_MANTENIMIENTO.Enabled = True

            End If
        End If

        NETO()
        DESCUENTO()
        SUBTOTAL()
        IMPUESTO()
        TOTAL()

        LIMPIAR_CONTROLES_VENTAS()
    End Sub

    Private Sub btnCancelarVenta_Click(sender As Object, e As EventArgs) Handles btnCancelarVenta.Click
        GRUPO_VENTA_LINEA.Enabled = False
        GRUPO_MANTENIMIENTO.Enabled = True
        LIMPIAR_CONTROLES_VENTAS()

    End Sub


#End Region

#Region "MECO"
    Private Sub MECO_MODIFICAR_Click(sender As Object, e As EventArgs)
        VL_BOTON_VENTAS = "1"
        GRUPO_VENTA_LINEA.Enabled = False
        btnAgregarVLinea.Enabled = True
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = True
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = False

        limpiarcontroles()
        LIMPIAR_CONTROLES_VENTAS()

        TabControl1.SelectedIndex = 1
        cboClientes.SelectedValue = rgListaVentas.CurrentRow.Cells("CLIE_CODIGO").Value
        cboDocumentos.SelectedValue = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        txtSerie.Text = rgListaVentas.CurrentRow.Cells("Serie").Value + ""
        txtNumero.Text = rgListaVentas.CurrentRow.Cells("Numero").Value + 0
        cboMonedas.SelectedValue = rgListaVentas.CurrentRow.Cells("MonCodigo").Value
        dtFecha.Value = rgListaVentas.CurrentRow.Cells("Fecha").Value + ""
        cboFormaPago.SelectedValue = rgListaVentas.CurrentRow.Cells("Id_Pago").Value
        cboVendedor.SelectedValue = rgListaVentas.CurrentRow.Cells("ID_Vendedor").Value
        cboEstado.SelectedValue = rgListaVentas.CurrentRow.Cells("Estado").Value
        txtObservaciones.Text = rgListaVentas.CurrentRow.Cells("Observaciones").Value + ""
        cboObra.SelectedValue = rgListaVentas.CurrentRow.Cells("Id_Obra").Value
        cboCostos.SelectedValue = rgListaVentas.CurrentRow.Cells("CCosCodigo").Value
        'txtImpuesto.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Impuestos").Value)
        ''txtImpuesto.Text = Round(CDec(Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Impuestos").Value)), 2, MidpointRounding.ToEven)
        'txtDescuento.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Dcto").Value)
        'txtNeto.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Neto").Value)
        'txtSubTotal.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("SubTotal").Value)
        'txtTotal.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Total").Value)

        txtDocVenta.Text = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value ''SE LLENA PARA AGREGAR LA VENTA LINEA

        LISTAR_DOC_VENTAS_IMPUESTO(txtDocVenta.Text)
        LISTAR_VENTAS_LINEA()

        NETO()
        DESCUENTO()
        SUBTOTAL()
        IMPUESTO()
        TOTAL()

    End Sub

    Private Sub MECO_ELIMINAR_Click(sender As Object, e As EventArgs) Handles MECO_ELIMINAR.Click
        If rgListaVentas.CurrentRow.Cells("Serie").Value = "" And rgListaVentas.CurrentRow.Cells("Numero").Value = "" Then
            Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If msg = DialogResult.Yes Then
                ELIMINAR_VENTAS()
                LISTAR_VENTAS()
                LISTAR_VENTAS_LINEA()
            End If
        Else
            MessageBox.Show("No se puede Eliminar...", AvisoMSGBOX)
        End If

    End Sub

    Private Sub MECO_AGREGAR_VENTA_LINEA_Click(sender As Object, e As EventArgs)
        'VL_BOTON = "2"
        'GRUPO_VENTA_LINEA.Enabled = True
        'GRUPO_MANTENIMIENTO.Enabled = False
        'TabControl1.SelectedIndex = 1
        'txtDocVenta.Text = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value
    End Sub

    Private Sub MECO_UPDATE_Click(sender As Object, e As EventArgs) Handles MECO_UPDATE.Click
        VL_BOTON_LINEA = "4"
        GRUPO_VENTA_LINEA.Enabled = True
        GRUPO_MANTENIMIENTO.Enabled = False

        txtDocVenta.Text = rgListaDetalles.CurrentRow.Cells("ID_DocVenta").Value + 0
        cboItemVenta.SelectedValue = rgListaDetalles.CurrentRow.Cells("ID_Item").Value
        cboObraVentaLinea.SelectedValue = rgListaDetalles.CurrentRow.Cells("ID_Obra").Value
        txtDCTo.Text = rgListaDetalles.CurrentRow.Cells("Dcto").Value
        txtGlosa.Text = rgListaDetalles.CurrentRow.Cells("Glosa").Value
        txtCantidad.Text = rgListaDetalles.CurrentRow.Cells("Cantidad").Value
        txtImporte.Text = rgListaDetalles.CurrentRow.Cells("Importe").Value
        txtPrecio.Text = rgListaDetalles.CurrentRow.Cells("Precio").Value
        cboCostosVenta.SelectedValue = rgListaDetalles.CurrentRow.Cells("ID_CCosto").Value

    End Sub

    Private Sub MECO_DELETE_Click(sender As Object, e As EventArgs) Handles MECO_DELETE.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR_VENTAS_LINEA()
            LISTAR_VENTAS_LINEA()
            LISTAR_VENTAS()
            NETO()
            DESCUENTO()
            SUBTOTAL()
            IMPUESTO()
            TOTAL()
        End If
    End Sub

#End Region


#Region "OTROS"
    Private Sub cboClientes_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboClientes.SelectedValueChanged
        If cboClientes.SelectedValue IsNot Nothing Then
            If cboClientes.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                Dim ID As String = cboClientes.SelectedValue
                Dim ID_OBRA As String = cboObra.SelectedValue
                LISTAR_CLIENTES_DETALLES(ID, ID_OBRA)

            End If
        End If
    End Sub


    Sub limpiarcontroles()

        txtCantidad.Text = ""
        txtDCTo.Text = ""
        txtDescuento.Text = ""
        txtDireccion.Text = ""
        txtDoc.Text = ""
        txtDocVenta.Text = ""
        txtGlosa.Text = ""
        txtImporte.Text = ""
        txtNeto.Text = ""
        txtImpuesto.Text = ""
        txtNumero.Text = ""
        txtObservaciones.Text = ""
        txtPrecio.Text = ""
        txtSerie.Text = ""
        txtSubTotal.Text = ""
        txtTipoDocumento.Text = ""
        txtTotal.Text = ""

        cboClientes.SelectedIndex = 0
        cboCostos.SelectedIndex = 0
        cboDocumentos.SelectedIndex = 0
        cboEstado.SelectedIndex = 0
        cboFormaPago.SelectedIndex = 0
        cboItemVenta.SelectedIndex = 0
        cboMonedas.SelectedIndex = 0
        cboObra.SelectedIndex = 0
        cboObraVentaLinea.SelectedIndex = 0
        cboVendedor.SelectedIndex = 0
        cboCostosVenta.SelectedIndex = 0
        dtFecha.Value = Now


    End Sub

    Sub LIMPIAR_CONTROLES_VENTAS()
        txtDCTo.Text = ""
        txtPrecio.Text = ""
        'txtDocVenta.Text = ""
        txtGlosa.Text = ""
        txtCantidad.Text = ""
        txtImporte.Text = ""
        cboCostosVenta.SelectedIndex = 0
        cboObraVentaLinea.SelectedIndex = 0
        cboItemVenta.SelectedIndex = 0
    End Sub

#End Region


#Region "VALIDACION DE SOLO LETRAS Y NUMEROS"
    Public Sub SoloLetras(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Public Sub SoloNumeros(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
            e.Handled = True
        Else
            e.Handled = False
        End If
    End Sub

    Function NumerosDecimales(ByVal e As System.Windows.Forms.KeyPressEventArgs, ByVal Text As Telerik.WinControls.UI.RadTextBox) As Integer

        Dim dig As Integer = Len(Text.Text & e.KeyChar)
        Dim a, esDecimal, NumDecimales As Integer
        Dim esDec As Boolean
        ' se verifica si es un digito o un punto 
        If Char.IsDigit(e.KeyChar) Or e.KeyChar = "." Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
            Return a
        Else
            e.Handled = True
        End If
        ' se verifica que el primer digito ingresado no sea un punto al seleccionar
        If Text.SelectedText <> "" Then
            If e.KeyChar = "." Then
                e.Handled = True
                Return a
            End If
        End If
        If dig = 1 And e.KeyChar = "." Then
            e.Handled = True
            Return a
        End If
        'aqui se hace la verificacion cuando es seleccionado el valor del texto
        'y no sea considerado como la adicion de un digito mas al valor ya contenido en el textbox
        If Text.SelectedText = "" Then
            ' aqui se hace el for para controlar que el numero sea de dos digitos - contadose a partir del punto decimal.
            For a = 0 To dig - 1
                Dim car As String = CStr(Text.Text & e.KeyChar)
                If car.Substring(a, 1) = "." Then
                    esDecimal = esDecimal + 1
                    esDec = True
                End If
                If esDec = True Then
                    NumDecimales = NumDecimales + 1
                End If
                ' aqui se controla los digitos a partir del punto numdecimales = 4 si es de dos decimales 
                If NumDecimales >= 4 Or esDecimal >= 2 Then
                    e.Handled = True
                End If
            Next
        End If
    End Function

#End Region


#Region "LISTAR DOC VENTA IMPUESTO, AGREGAR, ACTUALIZAR"

    Private Sub LISTAR_DOC_VENTAS_IMPUESTO(ID As Int16)

        VL_SRV_DOC_VENTAS = New SrvDocVentasImpuesto()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        VL_BeanResultado = VL_SRV_DOC_VENTAS.SRV_LISTAR_DOC_VENTAS_IMPUESTOS(ID)

        If VL_BeanResultado.blnExiste = True Then

            dtTable = VL_BeanResultado.dtResultado
            rgDocVentaImpuesto.DataSource = VL_BeanResultado.dtResultado

            'txtImpuesto.Text = dtTable.Rows(0)(4).ToString()
            'txtDoc.Text = dtTable.Rows(0)(3).ToString()
            'txtDireccion.Text = dtTable.Rows(0)(4).ToString()

        End If
    End Sub

    Sub AGREGAR_DOC_IMPUESTO()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion
        'VL_SRV_DOC_VENTAS = New SrvDocVentasImpuesto()

        For i As Integer = 0 To rgDocVentaImpuesto.Rows.Count - 1
            If rgDocVentaImpuesto.RowCount > 0 Then

                OBJ_DOC_VENTAS = New BeanDocVentasImpuesto(txtDocVenta.Text,
                    rgDocVentaImpuesto.Rows(i).Cells("ID_Impuesto").Value.ToString(), txtSubTotal.Text,
                    rgDocVentaImpuesto.Rows(i).Cells("Importe").Value.ToString())


                VL_BeanResultado = VL_SRV_DOC_VENTAS.SRV_AGREGAR_DOC_VENTAS(OBJ_DOC_VENTAS)

                If VL_BeanResultado.blnResultado = True Then
                    'MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
                    Return
                End If
            End If
            'txtImpuesto.Text += rgDocVentaImpuesto.Rows(i).Cells("Importe").Value.ToString()

        Next
    End Sub

#End Region

#Region "EVENTOS GRIDVIEW"

    Private Sub rgDocVentaImpuesto_CellValueChanged(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles rgDocVentaImpuesto.CellValueChanged
        'SUMA DE IMPORTE
        Dim soles As Decimal = 0D

        For i As Integer = 0 To rgDocVentaImpuesto.Rows.Count - 1

            If rgDocVentaImpuesto.Rows(i).Cells("Importe").Value <> -1 Then
                soles += Convert.ToDecimal(rgDocVentaImpuesto.Rows(i).Cells("Importe").Value)
            End If
        Next
        'txtImpuesto.Text = soles
        txtImpuesto.Text = Round(CDec(soles), 2, MidpointRounding.ToEven)

        TOTAL()
    End Sub

    Private Sub rgListaVentas_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles rgListaVentas.CellDoubleClick
        VL_BOTON_VENTAS = "1"
        GRUPO_VENTA_LINEA.Enabled = False
        btnAgregarVLinea.Enabled = True
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = True
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = False

        limpiarcontroles()
        LIMPIAR_CONTROLES_VENTAS()

        TabControl1.SelectedIndex = 1
        cboObra.SelectedValue = rgListaVentas.CurrentRow.Cells("Id_Obra").Value
        cboClientes.SelectedValue = rgListaVentas.CurrentRow.Cells("CLIE_CODIGO").Value
        cboDocumentos.SelectedValue = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        txtSerie.Text = rgListaVentas.CurrentRow.Cells("Serie").Value + ""
        txtNumero.Text = rgListaVentas.CurrentRow.Cells("Numero").Value + ""
        cboMonedas.SelectedValue = rgListaVentas.CurrentRow.Cells("MonCodigo").Value
        dtFecha.Value = rgListaVentas.CurrentRow.Cells("Fecha").Value
        cboFormaPago.SelectedValue = rgListaVentas.CurrentRow.Cells("Id_Pago").Value
        cboVendedor.SelectedValue = rgListaVentas.CurrentRow.Cells("ID_Vendedor").Value

        Dim VL_ESTADO As String = rgListaVentas.CurrentRow.Cells("Estado").Value

        If (VL_ESTADO = "0") Then
            cboEstado.SelectedIndex = 1
        Else
            cboEstado.SelectedIndex = 0
        End If
        'cboEstado.SelectedValue = VL_ESTADO
        txtObservaciones.Text = rgListaVentas.CurrentRow.Cells("Observaciones").Value + ""
        'cboObra.SelectedValue = rgListaVentas.CurrentRow.Cells("Id_Obra").Value
        cboCostos.SelectedValue = rgListaVentas.CurrentRow.Cells("CCosCodigo").Value
        'txtImpuesto.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Impuestos").Value)
        ''txtImpuesto.Text = Round(CDec(Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Impuestos").Value)), 2, MidpointRounding.ToEven)
        'txtDescuento.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Dcto").Value)
        'txtNeto.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Neto").Value)
        'txtSubTotal.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("SubTotal").Value)
        'txtTotal.Text = Convert.ToDecimal(rgListaVentas.CurrentRow.Cells("Total").Value)

        txtDocVenta.Text = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value ''SE LLENA PARA AGREGAR LA VENTA LINEA

        LISTAR_DOC_VENTAS_IMPUESTO(txtDocVenta.Text)
        LISTAR_VENTAS_LINEA()

        NETO()
        DESCUENTO()
        SUBTOTAL()
        IMPUESTO()
        TOTAL()
    End Sub
#End Region


#Region "NETO, DESCUENTO, IMPUESTO, SUBTOTAL, TOTAL"
    Private Sub NETO()
        Dim neto As Decimal = 0D

        For i As Integer = 0 To rgListaDetalles.Rows.Count - 1

            If rgListaDetalles.Rows(i).Cells("Importe").Value <> -1 Then
                neto += Convert.ToDecimal(rgListaDetalles.Rows(i).Cells("Importe").Value) + 0
            End If
        Next

        txtNeto.Text = Round(CDec(neto), 2, MidpointRounding.ToEven)

    End Sub

    Sub DESCUENTO()

        Dim descuento As Decimal = 0D

        For i As Integer = 0 To rgListaDetalles.RowCount - 1

            If rgListaDetalles.Rows(i).Cells("Dcto").Value <> -1 Then
                descuento += Convert.ToDecimal(rgListaDetalles.Rows(i).Cells("Dcto").Value) + 0
            End If
        Next

        txtDescuento.Text = Round(CDec(descuento), 2, MidpointRounding.ToEven)

    End Sub


    Sub SUBTOTAL()

        Dim VL_NETO As Decimal = IIf(txtNeto.Text = "", 0, txtNeto.Text)
        Dim VL_DESCUENTO As Decimal = IIf(txtDescuento.Text = "", 0, txtDescuento.Text)
        Dim VL_SUBTOTAL As Decimal = 0D

        VL_SUBTOTAL = VL_NETO - VL_DESCUENTO
        txtSubTotal.Text = Round(CDec(VL_SUBTOTAL), 2, MidpointRounding.ToEven)
    End Sub
    Sub IMPUESTO()
        'Dim ID_DOC As String = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        'If ID_DOC = "303" Then
        'Dim VL_IMPUESTO As Decimal = 0D
        'Dim VL_IMPU_TOTAL As Decimal = 0D
        'Dim VL_SUBTOTAL As Decimal = txtSubTotal.Text


        'For i As Integer = 0 To rgDocVentaImpuesto.Rows.Count - 1 ''Impuesto

        '    If rgDocVentaImpuesto.Rows(i).Cells("Importe").Value <> -1 Then

        '        Dim VL_VALOR_PORCENTAJE As Decimal = 0D
        '        Dim VL_IMPORTE As Decimal = 0D
        '        rgDocVentaImpuesto.Rows(i).Cells("Importe").Value = 0D
        '        'Dim VL_VALOR_PORCENTAJE As Decimal = rgDocVentaImpuesto.Rows(i).Cells("Valor_Porcentaje").Value


        '        VL_IMPORTE += VL_SUBTOTAL * VL_VALOR_PORCENTAJE / 100
        '        'IMPUESTO += Convert.ToDecimal(rgDocVentaImpuesto.Rows(i).Cells("Importe").Value)
        '        rgDocVentaImpuesto.Rows(i).Cells("Importe").Value = VL_IMPORTE

        '    End If

        'Next
        'Else
        '    MessageBox.Show("No es una Boleta de Venta...", AvisoMSGBOX)
        'End If
        'If (txtImpuesto.Text = "") Then
        txtImpuesto.Text = "0.00"
        'End If
    End Sub


    Sub TOTAL()

        Dim VL_IMPUESTO As Decimal = IIf(txtImpuesto.Text = "", 0, txtImpuesto.Text)
        Dim VL_SUBTOTAL As Decimal = IIf(txtSubTotal.Text = "", 0, txtSubTotal.Text)
        Dim VL_TOTAL As Decimal = 0D

        VL_TOTAL = VL_SUBTOTAL + VL_IMPUESTO

        txtTotal.Text = Round(CDec(VL_TOTAL), 2, MidpointRounding.ToEven)
    End Sub

#End Region

#Region "EVENTOS KEY PRESS"
    Private Sub txtPrecio_TextChanged(sender As Object, e As EventArgs) Handles txtPrecio.TextChanged


        Dim cantidad As Int16 = IIf(txtCantidad.Text = "", 0, txtCantidad.Text)
        Dim precio As Decimal = IIf(txtPrecio.Text = "", 0, txtPrecio.Text)
        'Dim importe As Decimal = 0D

        txtImporte.Text = cantidad * precio
        'importe = cantidad * precio
    End Sub
    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        NumerosDecimales(e, txtPrecio)
    End Sub

    Private Sub txtDCTo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtDCTo.KeyPress
        NumerosDecimales(e, txtDCTo)
    End Sub

    Private Sub txtImporte_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtImporte.KeyPress
        NumerosDecimales(e, txtImporte)
    End Sub

#End Region

#Region "MANTENIMIENTO IMPRESION"


    Private Sub UPDATE_DOC_VENTA(ID_DOC_VENTA As Int32, SERIE As String, DOCUMENTO As Int32)
        Dim VL_SrvVentas As New SrvVentas
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Try
            VL_BeanResultado = VL_SrvVentas.SRV_ACTUALIZAR_VENTAS_NUMERADOR(ID_DOC_VENTA, SERIE, DOCUMENTO)

            If VL_BeanResultado.blnResultado = True Then

            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub


#End Region



#Region "MECO IMPRESION"

    Private Sub rgListaVentas_ContextMenuOpening(sender As Object, e As Telerik.WinControls.UI.ContextMenuOpeningEventArgs) Handles rgListaVentas.ContextMenuOpening

        'VL_ID_DOCVENTA = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value

    End Sub

    Private Sub MECO_IMPRIMIR_Click(sender As Object, e As EventArgs) Handles MECO_IMPRIMIR.Click

        'If rgListaVentas.RowCount = 0 Then
        '    MessageBox.Show("No puede Imprimir boleta en grupo, ya que no hay registro", AvisoMSGBOX, MessageBoxButtons.OK, MessageBoxIcon.[Error])
        '    Return
        'End If

        'For i As Integer = 0 To rgListaVentas.RowCount - 1
        '    If rgListaVentas.Rows(i).Cells(7).Value.ToString() = "" Then
        '        MessageBox.Show("No puede Imprimir boleta en grupo, ya que una boleta no fue creada", AvisoMSGBOX, MessageBoxButtons.OK, MessageBoxIcon.[Error])
        '        Return
        '    End If
        'Next
        'Dim ID_DOCVENTA As Int32 = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value
        'Dim wfrReporte As New frmReporteBoletas("5", ID_DOCVENTA)
        'wfrReporte.ShowDialog()
        IMPRIMIR_DOCUMENTOS()
    End Sub

    Private Sub MECO_VER_Click(sender As Object, e As EventArgs) Handles MECO_VER.Click
        Dim ID_DOC = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        If ID_DOC = "303" Then
            Dim ID_DOCVENTA As String = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value.ToString()
            Dim wfrReporte As New frmReporteBoletas("4", ID_DOCVENTA)
            wfrReporte.ShowDialog()
        Else
            MessageBox.Show("No es una Boleta de Venta...", AvisoMSGBOX)
        End If
    End Sub

    Private Sub rgListaVentas_CurrentRowChanged(sender As Object, e As Telerik.WinControls.UI.CurrentRowChangedEventArgs) Handles rgListaVentas.CurrentRowChanged

        'lblresConteo.Text = Convert.ToString(e.CurrentRow.Index + 1) + " de " + rgListaVentas.RowCount.ToString() + ""

    End Sub


    Private Sub MECO_IMPRIMIR_TODOS_Click(sender As Object, e As EventArgs) Handles MECO_IMPRIMIR_TODOS.Click

        'Dim ID_DOCVENTA As Int32 = rgListaVentas.Rows(i).Cells("ID_DocVenta").Value
        IMPRIMIR_DOCUMENTOS()
    End Sub

    Private Sub MECO_ANULAR_Click(sender As Object, e As EventArgs) Handles MECO_ANULAR.Click
        If rgListaVentas.CurrentRow.Cells("Serie").Value <> "" And rgListaVentas.CurrentRow.Cells("Numero").Value <> "" Then
            Dim msg = MessageBox.Show("Esta Seguro que desea Anular Documento de venta...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If msg = DialogResult.Yes Then
                ANULAR()
            End If
        Else
            MessageBox.Show("No se Anular documento...", AvisoMSGBOX)
        End If
        LISTAR_VENTAS()
    End Sub

    Private Sub MECO_REIMPRIMIR_Click(sender As Object, e As EventArgs) Handles MECO_REIMPRIMIR.Click
        If rgListaVentas.CurrentRow.Cells("Serie").Value <> "" And rgListaVentas.CurrentRow.Cells("Numero").Value <> "" Then
            Dim msg = MessageBox.Show("Esta Seguro de Anular y Luego Reimprimir Documento de Venta...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If msg = DialogResult.Yes Then
                'ANULAR()
                ANULAR_REIMPRIMIR()
                'Dim ID_NUEVO_DOC As Int32
                REIMPRIMIR_DOCUMENTOS(ID_NUEVO_DOC)
            End If
        Else
            MessageBox.Show("No se puede Reimprimir Documento...", AvisoMSGBOX)
        End If
        LISTAR_VENTAS()
    End Sub

#End Region

#Region "IMPRIMIR, ANULAR Y REIMPRIMIR"

    Sub REIMPRIMIR_DOCUMENTOS(ID_NUEVODOC As Integer)

        Dim ID_DOC As String = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        If ID_DOC = "303" Then
            Dim FILAS As Integer = rgListaVentas.Rows.Count - 1
            Dim IMPRI As New RadAboutBox1
            IMPRI.ShowDialog()

            If IMPRI.DialogResult = System.Windows.Forms.DialogResult.OK Then

                Dim VL_SERIE As String = IMPRI.cboSerie.Text
                Dim VL_ID_DOCUMENTO As String = IMPRI.cboDocumentos.SelectedValue

                UPDATE_DOC_VENTA(ID_NUEVODOC, VL_SERIE, VL_ID_DOCUMENTO)
                Dim wfrReporte As New frmReporteBoletas("5", ID_NUEVODOC)

                wfrReporte.Show()
                'wfrReporte.Close()
                wfrReporte.Dispose()
            Else
                MessageBox.Show("Se Cancelo la Impresión...", AvisoMSGBOX)
            End If
        Else
            MessageBox.Show("No es una Boleta de Venta...", AvisoMSGBOX)
        End If
    End Sub
    Sub IMPRIMIR_DOCUMENTOS()

        Dim ID_DOC As String = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        If ID_DOC = "303" Then
            Dim serie As String = rgListaVentas.CurrentRow.Cells("Serie").Value + ""
            Dim numero As String = rgListaVentas.CurrentRow.Cells("Numero").Value + ""

            If serie = "" And numero = "" Then

                Dim IMPRI As New RadAboutBox1
                IMPRI.ShowDialog()

                If IMPRI.DialogResult = System.Windows.Forms.DialogResult.OK Then

                    Dim VL_SERIE As String = IMPRI.cboSerie.Text
                    Dim VL_ID_DOCUMENTO As String = IMPRI.cboDocumentos.SelectedValue

                    Dim FILAS As Integer = 0
                    Dim DOC As Integer = 0

                    FILAS = rgListaVentas.Rows.Count - 1

                    For i As Integer = 0 To FILAS
                        If i = 0 Then
                            DOC = rgListaVentas.Rows(i).Cells("ID_DOCUMENTO").Value
                        End If
                        If rgListaVentas.Rows(i).IsSelected = True Then
                            If rgListaVentas.Rows(i).Cells("ID_DOCUMENTO").Value <> DOC Then
                                MessageBox.Show("No puede Imprimir distintos tipos de documentos...", AvisoMSGBOX, MessageBoxButtons.OK, MessageBoxIcon.[Error])
                                Return
                            End If
                        End If
                    Next

                    For i As Integer = 0 To FILAS
                        If rgListaVentas.Rows(i).IsSelected = True Then

                            Dim ID_DOCVENTA As Int32 = rgListaVentas.Rows(i).Cells("ID_DocVenta").Value

                            UPDATE_DOC_VENTA(ID_DOCVENTA, VL_SERIE, VL_ID_DOCUMENTO)
                            Dim wfrReporte As New frmReporteBoletas("5", ID_DOCVENTA)
                            'wfrReporte.Visible = False
                            wfrReporte.Show()
                            'wfrReporte.Close()
                            wfrReporte.Dispose()

                        End If
                    Next

                    LISTAR_VENTAS()
                Else
                    MessageBox.Show("Se Cancelo la Impresión...", AvisoMSGBOX)
                End If
            Else
                MessageBox.Show("No se puede Imprimir, porque ya fueron Impresos...", AvisoMSGBOX)
            End If


        Else
            MessageBox.Show("No es una Boleta de Venta...", AvisoMSGBOX)
        End If
    End Sub

    Sub ANULAR()
        Dim ID_DOC As String = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        If ID_DOC = "303" Then
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
            Dim ID_DOC_VENTA As Int32 = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value

            VL_BeanResultado = VL_SRV_VENTAS.SRV_ANULAR_DOC_VENTA(ID_DOC_VENTA)

            If VL_BeanResultado.blnResultado = False Then
                MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
                Return
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
                Return
            End If
        Else
            MessageBox.Show("No es una Boleta de Venta...", AvisoMSGBOX)
        End If
    End Sub

    Sub ANULAR_REIMPRIMIR()
        Dim ID_DOC As String = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        If ID_DOC = "303" Then
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
            Dim ID_DOC_VENTA As Int32 = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value
            'Dim ID_NUEVO_DOC As Int32
            VL_BeanResultado = VL_SRV_VENTAS.SRV_ANULAR_REIMPRIMIR(ID_DOC_VENTA, ID_NUEVO_DOC)

            If VL_BeanResultado.blnResultado = False Then
                MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
                Return
            Else
                'MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
                Return
            End If
        Else
            MessageBox.Show("No es una Boleta de Venta...", AvisoMSGBOX)
        End If
    End Sub

#End Region

    Private Sub NotaDeCreditoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NotaDeCreditoToolStripMenuItem.Click
        Dim ID_DOC As String = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        If ID_DOC = "307" Then
            Dim ID_DOCVENTA As Int32 = rgListaVentas.CurrentRow.Cells("ID_DocVenta").Value
            Dim wfrReporte As New frmReporteNotaCredito("4", ID_DOCVENTA)
            wfrReporte.ShowDialog()
        Else
            MessageBox.Show("No es un Documento de Nota de Credito...", AvisoMSGBOX)
        End If
    End Sub

    Private Sub ImprimirNotaDeCreditoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImprimirNotaDeCreditoToolStripMenuItem.Click

        Dim ID_DOC As String = rgListaVentas.CurrentRow.Cells("ID_DOCUMENTO").Value
        If ID_DOC = "307" Then
            Dim serie As String = rgListaVentas.CurrentRow.Cells("Serie").Value + ""
            Dim numero As String = rgListaVentas.CurrentRow.Cells("Numero").Value + ""

            If serie = "" And numero = "" Then

                Dim IMPRI As New RadAboutBox1
                IMPRI.ShowDialog()

                If IMPRI.DialogResult = System.Windows.Forms.DialogResult.OK Then

                    Dim VL_SERIE As String = IMPRI.cboSerie.Text
                    Dim VL_ID_DOCUMENTO As String = IMPRI.cboDocumentos.SelectedValue

                    Dim FILAS As Integer = 0
                    Dim FILAS1 As Integer = 0
                    Dim DOC As Integer = 0
                    FILAS = rgListaVentas.Rows.Count - 1
                    'FILAS1 = rgListaVentas.SelectedRows.Count - 1

                    'For i As Integer = 0 To FILAS
                    '    If i = 0 Then
                    '        DOC = rgListaVentas.Rows(i).Cells("ID_DOCUMENTO").Value
                    '    End If
                    '    If rgListaVentas.Rows(i).IsSelected = True Then
                    '        If rgListaVentas.Rows(i).Cells("ID_DOCUMENTO").Value <> DOC Then
                    '            MessageBox.Show("No puede Imprimir distintos tipos de documentos...", AvisoMSGBOX, MessageBoxButtons.OK, MessageBoxIcon.[Error])
                    '            Return
                    '        End If
                    '    End If
                    'Next

                    For i As Integer = 0 To FILAS
                        If rgListaVentas.Rows(i).IsSelected = True Then

                            Dim ID_DOCVENTA As Int32 = rgListaVentas.Rows(i).Cells("ID_DocVenta").Value

                            UPDATE_DOC_VENTA(ID_DOCVENTA, VL_SERIE, VL_ID_DOCUMENTO)
                            Dim wfrNotaCredito As New frmReporteNotaCredito("5", ID_DOCVENTA)
                            'wfrReporte.Visible = False
                            wfrNotaCredito.Show()
                            'wfrNotaCredito.Close()
                            wfrNotaCredito.Dispose()

                        End If
                    Next
                Else
                    MessageBox.Show("Se Cancelo la Impresión...", AvisoMSGBOX)
                End If
            Else
                MessageBox.Show("No se puede Imprimir... ya fueron Impresos...", AvisoMSGBOX)
            End If
            LISTAR_VENTAS()
        Else
            MessageBox.Show("No es un Documento de Nota de Credito...", AvisoMSGBOX)
        End If
    End Sub


    Private Sub Salir_Click(sender As Object, e As EventArgs) Handles Salir.Click
        Dispose()
    End Sub

    Private Sub btnExcel_Click(sender As Object, e As EventArgs) Handles btnExcel.Click
        Dim export As New ExportToExcelML(Me.rgListaVentas)
        export.ExportVisualSettings = True
        Dim save As New SaveFileDialog()
        save.ShowDialog()
        ''export.FileExtension = "xls"
        export.RunExport(save.FileName + ".xls")
        'export.RunExport(save.Filter = "(*.xls)|*.xls")
    End Sub

    Private Sub cboObra_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboObra.SelectedValueChanged
        If cboObra.SelectedValue IsNot Nothing Then
            If cboObra.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                Dim ID As String = cboObra.SelectedValue
                LISTAR_CLIENTES(ID)

            End If
        End If
    End Sub


    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Dim numerador As New FrmManteNumerador()
        numerador = FrmManteNumerador.GetContainerControl()
        'numerador.MdiParent = Me
        numerador.Show()
        numerador.Focus()

    End Sub
End Class