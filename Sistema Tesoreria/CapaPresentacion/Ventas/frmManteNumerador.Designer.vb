﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmManteNumerador
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn31 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn32 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn33 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn34 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn35 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmManteNumerador))
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.btnConsultar = New Telerik.WinControls.UI.RadButton()
        Me.cboDocumentos = New Telerik.WinControls.UI.RadDropDownList()
        Me.TabPersonalizado1 = New Controles_DHMont.TabPersonalizado()
        Me.CONSULTA = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rgListaNumerador = New Telerik.WinControls.UI.RadGridView()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.txtBuscar = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.MANTENIMIENTO = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.txtSerie = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.txtNumero = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.lblDoc = New ctrLibreria.Controles.BeLabel()
        Me.btnGuardar = New Telerik.WinControls.UI.RadButton()
        Me.btnCancelar = New Telerik.WinControls.UI.RadButton()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.lblIdCuenta = New ctrLibreria.Controles.BeLabel()
        Me.MECO_NUMERADOR = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.AGREGAR_IMPUESTO = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnConsultar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPersonalizado1.SuspendLayout()
        Me.CONSULTA.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.rgListaNumerador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaNumerador.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MANTENIMIENTO.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MECO_NUMERADOR.SuspendLayout()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox1.Controls.Add(Me.RadLabel2)
        Me.RadGroupBox1.Controls.Add(Me.btnConsultar)
        Me.RadGroupBox1.Controls.Add(Me.cboDocumentos)
        Me.RadGroupBox1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox1.HeaderText = "Consulta "
        Me.RadGroupBox1.Location = New System.Drawing.Point(12, 15)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(401, 62)
        Me.RadGroupBox1.TabIndex = 64
        Me.RadGroupBox1.Text = "Consulta "
        '
        'RadLabel1
        '
        Me.RadLabel1.Location = New System.Drawing.Point(212, 79)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(2, 2)
        Me.RadLabel1.TabIndex = 48
        '
        'RadLabel2
        '
        Me.RadLabel2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel2.Location = New System.Drawing.Point(5, 28)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(75, 18)
        Me.RadLabel2.TabIndex = 61
        Me.RadLabel2.Text = "Documento :"
        '
        'btnConsultar
        '
        Me.btnConsultar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultar.Location = New System.Drawing.Point(305, 23)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(80, 24)
        Me.btnConsultar.TabIndex = 62
        Me.btnConsultar.Text = "Cargar"
        '
        'cboDocumentos
        '
        Me.cboDocumentos.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboDocumentos.Location = New System.Drawing.Point(82, 26)
        Me.cboDocumentos.Name = "cboDocumentos"
        Me.cboDocumentos.Size = New System.Drawing.Size(199, 20)
        Me.cboDocumentos.TabIndex = 60
        '
        'TabPersonalizado1
        '
        Me.TabPersonalizado1.Controls.Add(Me.CONSULTA)
        Me.TabPersonalizado1.Controls.Add(Me.MANTENIMIENTO)
        Me.TabPersonalizado1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPersonalizado1.Location = New System.Drawing.Point(12, 8)
        Me.TabPersonalizado1.Name = "TabPersonalizado1"
        Me.TabPersonalizado1.SelectedIndex = 0
        Me.TabPersonalizado1.Size = New System.Drawing.Size(463, 351)
        Me.TabPersonalizado1.TabIndex = 65
        '
        'CONSULTA
        '
        Me.CONSULTA.Controls.Add(Me.GroupBox2)
        Me.CONSULTA.Controls.Add(Me.txtBuscar)
        Me.CONSULTA.Controls.Add(Me.BeLabel1)
        Me.CONSULTA.Location = New System.Drawing.Point(4, 22)
        Me.CONSULTA.Name = "CONSULTA"
        Me.CONSULTA.Padding = New System.Windows.Forms.Padding(3)
        Me.CONSULTA.Size = New System.Drawing.Size(455, 325)
        Me.CONSULTA.TabIndex = 0
        Me.CONSULTA.Text = "Consulta"
        Me.CONSULTA.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rgListaNumerador)
        Me.GroupBox2.Controls.Add(Me.RadGroupBox1)
        Me.GroupBox2.Controls.Add(Me.btnNuevo)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(439, 311)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'rgListaNumerador
        '
        Me.rgListaNumerador.ContextMenuStrip = Me.MECO_NUMERADOR
        Me.rgListaNumerador.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgListaNumerador.Location = New System.Drawing.Point(12, 89)
        '
        'rgListaNumerador
        '
        Me.rgListaNumerador.MasterTemplate.AllowAddNewRow = False
        Me.rgListaNumerador.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn31.FieldName = "ID_DOC_NUMERADOR"
        GridViewTextBoxColumn31.HeaderText = "ID_DOC_NUMERADOR"
        GridViewTextBoxColumn31.IsVisible = False
        GridViewTextBoxColumn31.Name = "ID_DOC_NUMERADOR"
        GridViewTextBoxColumn31.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn31.Width = 80
        GridViewTextBoxColumn32.FieldName = "ID_DOCUMENTO"
        GridViewTextBoxColumn32.HeaderText = "ID_DOCUMENTO"
        GridViewTextBoxColumn32.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        GridViewTextBoxColumn32.Name = "ID_DOCUMENTO"
        GridViewTextBoxColumn32.Width = 100
        GridViewTextBoxColumn33.FieldName = "DESCRIPCION"
        GridViewTextBoxColumn33.HeaderText = "DOCUMENTO"
        GridViewTextBoxColumn33.Name = "DESCRIPCION"
        GridViewTextBoxColumn33.Width = 120
        GridViewTextBoxColumn34.FieldName = "SERIE"
        GridViewTextBoxColumn34.HeaderText = "SERIE"
        GridViewTextBoxColumn34.Name = "SERIE"
        GridViewTextBoxColumn34.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn34.Width = 80
        GridViewTextBoxColumn35.FieldName = "NUMERO"
        GridViewTextBoxColumn35.HeaderText = "NUMERO"
        GridViewTextBoxColumn35.Name = "NUMERO"
        GridViewTextBoxColumn35.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn35.Width = 100
        Me.rgListaNumerador.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn31, GridViewTextBoxColumn32, GridViewTextBoxColumn33, GridViewTextBoxColumn34, GridViewTextBoxColumn35})
        Me.rgListaNumerador.Name = "rgListaNumerador"
        Me.rgListaNumerador.ReadOnly = True
        Me.rgListaNumerador.Size = New System.Drawing.Size(420, 187)
        Me.rgListaNumerador.TabIndex = 3
        Me.rgListaNumerador.Text = "RadGridView1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(343, 284)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(77, 23)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'txtBuscar
        '
        Me.txtBuscar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBuscar.BackColor = System.Drawing.Color.Ivory
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtBuscar.KeyEnter = True
        Me.txtBuscar.Location = New System.Drawing.Point(80, 23)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBuscar.ShortcutsEnabled = False
        Me.txtBuscar.Size = New System.Drawing.Size(276, 20)
        Me.txtBuscar.TabIndex = 3
        Me.txtBuscar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(13, 23)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(58, 13)
        Me.BeLabel1.TabIndex = 2
        Me.BeLabel1.Text = "Nombre :"
        '
        'MANTENIMIENTO
        '
        Me.MANTENIMIENTO.BackColor = System.Drawing.Color.AliceBlue
        Me.MANTENIMIENTO.Controls.Add(Me.GroupBox1)
        Me.MANTENIMIENTO.Location = New System.Drawing.Point(4, 22)
        Me.MANTENIMIENTO.Name = "MANTENIMIENTO"
        Me.MANTENIMIENTO.Padding = New System.Windows.Forms.Padding(3)
        Me.MANTENIMIENTO.Size = New System.Drawing.Size(455, 325)
        Me.MANTENIMIENTO.TabIndex = 1
        Me.MANTENIMIENTO.Text = "Mantenimiento"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.BeLabel5)
        Me.GroupBox1.Controls.Add(Me.lblIdCuenta)
        Me.GroupBox1.Controls.Add(Me.btnCancelar)
        Me.GroupBox1.Controls.Add(Me.btnGuardar)
        Me.GroupBox1.Controls.Add(Me.BeLabel4)
        Me.GroupBox1.Controls.Add(Me.txtSerie)
        Me.GroupBox1.Controls.Add(Me.BeLabel2)
        Me.GroupBox1.Controls.Add(Me.txtNumero)
        Me.GroupBox1.Controls.Add(Me.BeLabel3)
        Me.GroupBox1.Controls.Add(Me.lblDoc)
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(8, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(441, 201)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Mantenimiento Numerador"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel4.Location = New System.Drawing.Point(15, 55)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(79, 13)
        Me.BeLabel4.TabIndex = 14
        Me.BeLabel4.Text = "Documento :"
        '
        'txtSerie
        '
        Me.txtSerie.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSerie.BackColor = System.Drawing.Color.White
        Me.txtSerie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Enabled = False
        Me.txtSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.ForeColor = System.Drawing.Color.Black
        Me.txtSerie.KeyEnter = True
        Me.txtSerie.Location = New System.Drawing.Point(100, 81)
        Me.txtSerie.MaxLength = 3
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSerie.ShortcutsEnabled = False
        Me.txtSerie.Size = New System.Drawing.Size(113, 20)
        Me.txtSerie.TabIndex = 8
        Me.txtSerie.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel2.Location = New System.Drawing.Point(50, 82)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(44, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Serie :"
        '
        'txtNumero
        '
        Me.txtNumero.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumero.BackColor = System.Drawing.Color.White
        Me.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumero.ForeColor = System.Drawing.Color.Black
        Me.txtNumero.KeyEnter = True
        Me.txtNumero.Location = New System.Drawing.Point(100, 108)
        Me.txtNumero.MaxLength = 7
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumero.ShortcutsEnabled = False
        Me.txtNumero.Size = New System.Drawing.Size(200, 20)
        Me.txtNumero.TabIndex = 9
        Me.txtNumero.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel3.Location = New System.Drawing.Point(36, 111)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(58, 13)
        Me.BeLabel3.TabIndex = 2
        Me.BeLabel3.Text = "Número :"
        '
        'lblDoc
        '
        Me.lblDoc.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblDoc.AutoSize = True
        Me.lblDoc.BackColor = System.Drawing.Color.Transparent
        Me.lblDoc.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblDoc.Location = New System.Drawing.Point(101, 55)
        Me.lblDoc.Name = "lblDoc"
        Me.lblDoc.Size = New System.Drawing.Size(84, 13)
        Me.lblDoc.TabIndex = 6
        Me.lblDoc.Text = "lblDocumento"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(100, 157)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(80, 24)
        Me.btnGuardar.TabIndex = 63
        Me.btnGuardar.Text = "Guardar"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(220, 157)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(80, 24)
        Me.btnCancelar.TabIndex = 64
        Me.btnCancelar.Text = "Cancelar"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel5.Location = New System.Drawing.Point(15, 30)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel5.TabIndex = 66
        Me.BeLabel5.Text = "Id Doc Num :"
        '
        'lblIdCuenta
        '
        Me.lblIdCuenta.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblIdCuenta.AutoSize = True
        Me.lblIdCuenta.BackColor = System.Drawing.Color.Transparent
        Me.lblIdCuenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblIdCuenta.Location = New System.Drawing.Point(101, 30)
        Me.lblIdCuenta.Name = "lblIdCuenta"
        Me.lblIdCuenta.Size = New System.Drawing.Size(54, 13)
        Me.lblIdCuenta.TabIndex = 65
        Me.lblIdCuenta.Text = "lblIdDoc"
        '
        'MECO_NUMERADOR
        '
        Me.MECO_NUMERADOR.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AGREGAR_IMPUESTO})
        Me.MECO_NUMERADOR.Name = "MECO"
        Me.MECO_NUMERADOR.Size = New System.Drawing.Size(119, 26)
        '
        'AGREGAR_IMPUESTO
        '
        Me.AGREGAR_IMPUESTO.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.AGREGAR_IMPUESTO.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AGREGAR_IMPUESTO.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.AGREGAR_IMPUESTO.Name = "AGREGAR_IMPUESTO"
        Me.AGREGAR_IMPUESTO.Size = New System.Drawing.Size(152, 22)
        Me.AGREGAR_IMPUESTO.Text = "Eliminar"
        '
        'FrmManteNumerador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(478, 371)
        Me.Controls.Add(Me.TabPersonalizado1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "FrmManteNumerador"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.Text = "Mantenimiento Numerador"
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        Me.RadGroupBox1.PerformLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnConsultar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPersonalizado1.ResumeLayout(False)
        Me.CONSULTA.ResumeLayout(False)
        Me.CONSULTA.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.rgListaNumerador.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaNumerador, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MANTENIMIENTO.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MECO_NUMERADOR.ResumeLayout(False)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents btnConsultar As Telerik.WinControls.UI.RadButton
    Friend WithEvents cboDocumentos As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents TabPersonalizado1 As Controles_DHMont.TabPersonalizado
    Friend WithEvents CONSULTA As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rgListaNumerador As Telerik.WinControls.UI.RadGridView
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents txtBuscar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents MANTENIMIENTO As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSerie As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumero As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblDoc As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnCancelar As Telerik.WinControls.UI.RadButton
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblIdCuenta As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnGuardar As Telerik.WinControls.UI.RadButton
    Friend WithEvents MECO_NUMERADOR As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents AGREGAR_IMPUESTO As System.Windows.Forms.ToolStripMenuItem
End Class

