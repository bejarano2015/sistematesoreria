﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMigrarDAta
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn12 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn13 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.RadGroupBox6 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadTextBox1 = New Telerik.WinControls.UI.RadTextBox()
        Me.btnCancelar = New Telerik.WinControls.UI.RadButton()
        Me.btnGuardar = New Telerik.WinControls.UI.RadButton()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.RadDateTimePicker3 = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.RadDateTimePicker2 = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.RadButton1 = New Telerik.WinControls.UI.RadButton()
        Me.rgListaDetalles = New Telerik.WinControls.UI.RadGridView()
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox6.SuspendLayout()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me.RadTextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadDateTimePicker3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadDateTimePicker2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadButton1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaDetalles.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadGroupBox6
        '
        Me.RadGroupBox6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox6.Controls.Add(Me.RadButton1)
        Me.RadGroupBox6.Controls.Add(Me.RadLabel2)
        Me.RadGroupBox6.Controls.Add(Me.RadDateTimePicker2)
        Me.RadGroupBox6.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox6.Controls.Add(Me.RadDateTimePicker3)
        Me.RadGroupBox6.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox6.HeaderText = "Fitro de Consulta"
        Me.RadGroupBox6.Location = New System.Drawing.Point(11, 12)
        Me.RadGroupBox6.Name = "RadGroupBox6"
        Me.RadGroupBox6.Size = New System.Drawing.Size(891, 59)
        Me.RadGroupBox6.TabIndex = 52
        Me.RadGroupBox6.Text = "Fitro de Consulta"
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.Controls.Add(Me.rgListaDetalles)
        Me.RadGroupBox1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox1.HeaderText = "Otros "
        Me.RadGroupBox1.Location = New System.Drawing.Point(7, 76)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(1069, 460)
        Me.RadGroupBox1.TabIndex = 53
        Me.RadGroupBox1.Text = "Otros "
        '
        'RadTextBox1
        '
        Me.RadTextBox1.AutoSize = False
        Me.RadTextBox1.Location = New System.Drawing.Point(99, 70)
        Me.RadTextBox1.Multiline = True
        Me.RadTextBox1.Name = "RadTextBox1"
        Me.RadTextBox1.Size = New System.Drawing.Size(777, 40)
        Me.RadTextBox1.TabIndex = 42
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(958, 576)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(118, 24)
        Me.btnCancelar.TabIndex = 57
        Me.btnCancelar.Text = "Cancelar"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(821, 576)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(115, 24)
        Me.btnGuardar.TabIndex = 56
        Me.btnGuardar.Text = "Migrar  Data"
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.Location = New System.Drawing.Point(25, 21)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(82, 18)
        Me.RadLabel1.TabIndex = 56
        Me.RadLabel1.Text = "Fecha  Desde :"
        '
        'RadDateTimePicker3
        '
        Me.RadDateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.RadDateTimePicker3.Location = New System.Drawing.Point(112, 21)
        Me.RadDateTimePicker3.Name = "RadDateTimePicker3"
        Me.RadDateTimePicker3.Size = New System.Drawing.Size(87, 20)
        Me.RadDateTimePicker3.TabIndex = 57
        Me.RadDateTimePicker3.TabStop = False
        Me.RadDateTimePicker3.Text = "08/01/2015"
        Me.RadDateTimePicker3.Value = New Date(2015, 1, 8, 17, 18, 29, 710)
        '
        'RadLabel2
        '
        Me.RadLabel2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel2.Location = New System.Drawing.Point(245, 21)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(79, 18)
        Me.RadLabel2.TabIndex = 58
        Me.RadLabel2.Text = "Fecha  Hasta :"
        '
        'RadDateTimePicker2
        '
        Me.RadDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.RadDateTimePicker2.Location = New System.Drawing.Point(332, 21)
        Me.RadDateTimePicker2.Name = "RadDateTimePicker2"
        Me.RadDateTimePicker2.Size = New System.Drawing.Size(87, 20)
        Me.RadDateTimePicker2.TabIndex = 59
        Me.RadDateTimePicker2.TabStop = False
        Me.RadDateTimePicker2.Text = "08/01/2015"
        Me.RadDateTimePicker2.Value = New Date(2015, 1, 8, 17, 18, 29, 710)
        '
        'RadButton1
        '
        Me.RadButton1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadButton1.Location = New System.Drawing.Point(681, 21)
        Me.RadButton1.Name = "RadButton1"
        Me.RadButton1.Size = New System.Drawing.Size(149, 24)
        Me.RadButton1.TabIndex = 60
        Me.RadButton1.Text = "Consultar"
        '
        'rgListaDetalles
        '
        Me.rgListaDetalles.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rgListaDetalles.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.rgListaDetalles.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgListaDetalles.EnableHotTracking = False
        Me.rgListaDetalles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rgListaDetalles.ForeColor = System.Drawing.Color.Black
        Me.rgListaDetalles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgListaDetalles.Location = New System.Drawing.Point(9, 21)
        '
        'rgListaDetalles
        '
        Me.rgListaDetalles.MasterTemplate.AllowAddNewRow = False
        Me.rgListaDetalles.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgListaDetalles.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn1.EnableExpressionEditor = False
        GridViewTextBoxColumn1.FieldName = "ID_Doc_VentaLinea"
        GridViewTextBoxColumn1.HeaderText = "Nº Venta Linea"
        GridViewTextBoxColumn1.IsVisible = False
        GridViewTextBoxColumn1.Name = "ID_Doc_VentaLinea"
        GridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn1.Width = 100
        GridViewTextBoxColumn2.EnableExpressionEditor = False
        GridViewTextBoxColumn2.FieldName = "ID_DocVenta"
        GridViewTextBoxColumn2.HeaderText = "Nº Doc. venta"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "ID_DocVenta"
        GridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn2.Width = 100
        GridViewTextBoxColumn3.EnableExpressionEditor = False
        GridViewTextBoxColumn3.FieldName = "ID_Item"
        GridViewTextBoxColumn3.HeaderText = "ID_Item"
        GridViewTextBoxColumn3.IsVisible = False
        GridViewTextBoxColumn3.Name = "ID_Item"
        GridViewTextBoxColumn4.EnableExpressionEditor = False
        GridViewTextBoxColumn4.FieldName = "Item"
        GridViewTextBoxColumn4.HeaderText = "Item"
        GridViewTextBoxColumn4.Name = "Item"
        GridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn4.Width = 120
        GridViewTextBoxColumn5.EnableExpressionEditor = False
        GridViewTextBoxColumn5.FieldName = "Glosa"
        GridViewTextBoxColumn5.HeaderText = "Glosa"
        GridViewTextBoxColumn5.Name = "Glosa"
        GridViewTextBoxColumn5.Width = 400
        GridViewTextBoxColumn6.EnableExpressionEditor = False
        GridViewTextBoxColumn6.FieldName = "Cantidad"
        GridViewTextBoxColumn6.FormatString = "{0:###########,##}"
        GridViewTextBoxColumn6.HeaderText = "Cantidad"
        GridViewTextBoxColumn6.Name = "Cantidad"
        GridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn6.Width = 80
        GridViewTextBoxColumn7.EnableExpressionEditor = False
        GridViewTextBoxColumn7.FieldName = "Precio"
        GridViewTextBoxColumn7.FormatString = "{0:N0}"
        GridViewTextBoxColumn7.HeaderText = "Precio"
        GridViewTextBoxColumn7.Name = "Precio"
        GridViewTextBoxColumn7.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn7.Width = 120
        GridViewTextBoxColumn8.EnableExpressionEditor = False
        GridViewTextBoxColumn8.FieldName = "Dcto"
        GridViewTextBoxColumn8.FormatString = "{0:N0}"
        GridViewTextBoxColumn8.HeaderText = "Descuento"
        GridViewTextBoxColumn8.Name = "Dcto"
        GridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn8.Width = 80
        GridViewTextBoxColumn9.EnableExpressionEditor = False
        GridViewTextBoxColumn9.FieldName = "Importe"
        GridViewTextBoxColumn9.FormatString = "{0:N0}"
        GridViewTextBoxColumn9.HeaderText = "Importe"
        GridViewTextBoxColumn9.Name = "Importe"
        GridViewTextBoxColumn9.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn9.Width = 120
        GridViewTextBoxColumn10.EnableExpressionEditor = False
        GridViewTextBoxColumn10.FieldName = "ID_Obra"
        GridViewTextBoxColumn10.HeaderText = "ID_Obra"
        GridViewTextBoxColumn10.IsVisible = False
        GridViewTextBoxColumn10.Name = "ID_Obra"
        GridViewTextBoxColumn10.Width = 80
        GridViewTextBoxColumn11.EnableExpressionEditor = False
        GridViewTextBoxColumn11.FieldName = "OBRA"
        GridViewTextBoxColumn11.HeaderText = "OBRA"
        GridViewTextBoxColumn11.Name = "OBRA"
        GridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn11.Width = 200
        GridViewTextBoxColumn12.EnableExpressionEditor = False
        GridViewTextBoxColumn12.FieldName = "ID_CCosto"
        GridViewTextBoxColumn12.HeaderText = "ID_CCosto"
        GridViewTextBoxColumn12.IsVisible = False
        GridViewTextBoxColumn12.Name = "ID_CCosto"
        GridViewTextBoxColumn12.Width = 100
        GridViewTextBoxColumn13.EnableExpressionEditor = False
        GridViewTextBoxColumn13.FieldName = "CCosDescripcion"
        GridViewTextBoxColumn13.FormatString = "{0:N0}"
        GridViewTextBoxColumn13.HeaderText = "Costo"
        GridViewTextBoxColumn13.Name = "CCosDescripcion"
        GridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn13.Width = 200
        Me.rgListaDetalles.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8, GridViewTextBoxColumn9, GridViewTextBoxColumn10, GridViewTextBoxColumn11, GridViewTextBoxColumn12, GridViewTextBoxColumn13})
        Me.rgListaDetalles.Name = "rgListaDetalles"
        Me.rgListaDetalles.ReadOnly = True
        Me.rgListaDetalles.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgListaDetalles.ShowGroupPanel = False
        Me.rgListaDetalles.Size = New System.Drawing.Size(1046, 422)
        Me.rgListaDetalles.TabIndex = 3
        Me.rgListaDetalles.Text = "RadGridView1"
        '
        'FrmMigrarDAta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1088, 621)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.RadGroupBox1)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.RadGroupBox6)
        Me.Name = "FrmMigrarDAta"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmMigrarDAta"
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox6.ResumeLayout(False)
        Me.RadGroupBox6.PerformLayout()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        CType(Me.RadTextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadDateTimePicker3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadDateTimePicker2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadButton1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaDetalles.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadGroupBox6 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadTextBox1 As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadButton1 As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadDateTimePicker2 As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadDateTimePicker3 As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents btnCancelar As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnGuardar As Telerik.WinControls.UI.RadButton
    Friend WithEvents rgListaDetalles As Telerik.WinControls.UI.RadGridView
End Class

