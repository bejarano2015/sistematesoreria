﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVistoBuenoUsuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn12 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn13 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn14 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn15 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn16 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewDecimalColumn1 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewDecimalColumn2 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewDecimalColumn3 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewDecimalColumn4 As Telerik.WinControls.UI.GridViewDecimalColumn = New Telerik.WinControls.UI.GridViewDecimalColumn()
        Dim GridViewTextBoxColumn17 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn18 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn19 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn20 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn21 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewCheckBoxColumn1 As Telerik.WinControls.UI.GridViewCheckBoxColumn = New Telerik.WinControls.UI.GridViewCheckBoxColumn()
        Me.tbVistoBueno = New System.Windows.Forms.TabControl()
        Me.UsuarioVB = New System.Windows.Forms.TabPage()
        Me.dgvUsuario = New Telerik.WinControls.UI.RadGridView()
        Me.PermisoUsuario = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblEmpresa = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.lblUsuario = New System.Windows.Forms.Label()
        Me.cboPermiso = New System.Windows.Forms.ComboBox()
        Me.lblArea = New System.Windows.Forms.Label()
        Me.cboArea = New System.Windows.Forms.ComboBox()
        Me.txtEmpresa = New System.Windows.Forms.TextBox()
        Me.lblPermiso = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.dgvPlanillas = New Telerik.WinControls.UI.MasterGridViewTemplate()
        Me.tbVistoBueno.SuspendLayout()
        Me.UsuarioVB.SuspendLayout()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvUsuario.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PermisoUsuario.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPlanillas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbVistoBueno
        '
        Me.tbVistoBueno.Controls.Add(Me.UsuarioVB)
        Me.tbVistoBueno.Controls.Add(Me.PermisoUsuario)
        Me.tbVistoBueno.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbVistoBueno.Location = New System.Drawing.Point(0, 0)
        Me.tbVistoBueno.Name = "tbVistoBueno"
        Me.tbVistoBueno.SelectedIndex = 0
        Me.tbVistoBueno.Size = New System.Drawing.Size(496, 456)
        Me.tbVistoBueno.TabIndex = 2
        '
        'UsuarioVB
        '
        Me.UsuarioVB.Controls.Add(Me.dgvUsuario)
        Me.UsuarioVB.Location = New System.Drawing.Point(4, 22)
        Me.UsuarioVB.Name = "UsuarioVB"
        Me.UsuarioVB.Padding = New System.Windows.Forms.Padding(3)
        Me.UsuarioVB.Size = New System.Drawing.Size(488, 430)
        Me.UsuarioVB.TabIndex = 3
        Me.UsuarioVB.Text = "Usuario VB°"
        Me.UsuarioVB.UseVisualStyleBackColor = True
        '
        'dgvUsuario
        '
        Me.dgvUsuario.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvUsuario.EnableHotTracking = False
        Me.dgvUsuario.Location = New System.Drawing.Point(3, 3)
        '
        'dgvUsuario
        '
        Me.dgvUsuario.MasterTemplate.AllowAddNewRow = False
        Me.dgvUsuario.MasterTemplate.AllowColumnReorder = False
        GridViewTextBoxColumn1.FieldName = "EmprCodigo"
        GridViewTextBoxColumn1.HeaderText = "EmprCodigo"
        GridViewTextBoxColumn1.IsVisible = False
        GridViewTextBoxColumn1.Name = "EmprCodigo"
        GridViewTextBoxColumn2.FieldName = "SisCodigo"
        GridViewTextBoxColumn2.HeaderText = "SisCodigo"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "SisCodigo"
        GridViewTextBoxColumn3.FieldName = "idUsuarioCorre"
        GridViewTextBoxColumn3.HeaderText = "idUsuarioCorre"
        GridViewTextBoxColumn3.IsVisible = False
        GridViewTextBoxColumn3.Name = "idUsuarioCorre"
        GridViewTextBoxColumn4.FieldName = "EmprDescripcion"
        GridViewTextBoxColumn4.HeaderText = "Empresa"
        GridViewTextBoxColumn4.Name = "EmprDescripcion"
        GridViewTextBoxColumn4.ReadOnly = True
        GridViewTextBoxColumn4.Width = 170
        GridViewTextBoxColumn5.FieldName = "UsuNombre"
        GridViewTextBoxColumn5.HeaderText = "Nombre de Usuario"
        GridViewTextBoxColumn5.Name = "UsuNombre"
        GridViewTextBoxColumn5.ReadOnly = True
        GridViewTextBoxColumn5.Width = 170
        GridViewTextBoxColumn6.FieldName = "UsuCodigo"
        GridViewTextBoxColumn6.HeaderText = "Usuario"
        GridViewTextBoxColumn6.Name = "UsuCodigo"
        GridViewTextBoxColumn6.ReadOnly = True
        GridViewTextBoxColumn6.Width = 100
        GridViewTextBoxColumn7.FieldName = "ID_VistoBueno"
        GridViewTextBoxColumn7.HeaderText = "ID_VistoBueno"
        GridViewTextBoxColumn7.IsVisible = False
        GridViewTextBoxColumn7.Name = "ID_VistoBueno"
        Me.dgvUsuario.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7})
        Me.dgvUsuario.MasterTemplate.EnableFiltering = True
        Me.dgvUsuario.MasterTemplate.EnableGrouping = False
        Me.dgvUsuario.MasterTemplate.EnableSorting = False
        Me.dgvUsuario.Name = "dgvUsuario"
        Me.dgvUsuario.Size = New System.Drawing.Size(482, 424)
        Me.dgvUsuario.TabIndex = 0
        Me.dgvUsuario.Text = "RadGridView3"
        '
        'PermisoUsuario
        '
        Me.PermisoUsuario.BackColor = System.Drawing.Color.LightSteelBlue
        Me.PermisoUsuario.Controls.Add(Me.GroupBox1)
        Me.PermisoUsuario.Location = New System.Drawing.Point(4, 22)
        Me.PermisoUsuario.Name = "PermisoUsuario"
        Me.PermisoUsuario.Size = New System.Drawing.Size(488, 430)
        Me.PermisoUsuario.TabIndex = 4
        Me.PermisoUsuario.Text = "Permiso de Usuario"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblEmpresa)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnGrabar)
        Me.GroupBox1.Controls.Add(Me.lblUsuario)
        Me.GroupBox1.Controls.Add(Me.cboPermiso)
        Me.GroupBox1.Controls.Add(Me.lblArea)
        Me.GroupBox1.Controls.Add(Me.cboArea)
        Me.GroupBox1.Controls.Add(Me.txtEmpresa)
        Me.GroupBox1.Controls.Add(Me.lblPermiso)
        Me.GroupBox1.Controls.Add(Me.txtNombre)
        Me.GroupBox1.Controls.Add(Me.txtUsuario)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(472, 411)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        '
        'lblEmpresa
        '
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.Location = New System.Drawing.Point(110, 131)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(54, 13)
        Me.lblEmpresa.TabIndex = 5
        Me.lblEmpresa.Text = "Empresa :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(60, 91)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Nombre de Usuario :"
        '
        'btnGrabar
        '
        Me.btnGrabar.Location = New System.Drawing.Point(196, 266)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(75, 23)
        Me.btnGrabar.TabIndex = 12
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'lblUsuario
        '
        Me.lblUsuario.AutoSize = True
        Me.lblUsuario.Location = New System.Drawing.Point(115, 51)
        Me.lblUsuario.Name = "lblUsuario"
        Me.lblUsuario.Size = New System.Drawing.Size(49, 13)
        Me.lblUsuario.TabIndex = 4
        Me.lblUsuario.Text = "Usuario :"
        '
        'cboPermiso
        '
        Me.cboPermiso.FormattingEnabled = True
        Me.cboPermiso.Items.AddRange(New Object() {"SI", "NO"})
        Me.cboPermiso.Location = New System.Drawing.Point(170, 208)
        Me.cboPermiso.Name = "cboPermiso"
        Me.cboPermiso.Size = New System.Drawing.Size(211, 21)
        Me.cboPermiso.TabIndex = 11
        '
        'lblArea
        '
        Me.lblArea.AutoSize = True
        Me.lblArea.Location = New System.Drawing.Point(129, 171)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.Size = New System.Drawing.Size(35, 13)
        Me.lblArea.TabIndex = 6
        Me.lblArea.Text = "Area :"
        '
        'cboArea
        '
        Me.cboArea.FormattingEnabled = True
        Me.cboArea.Location = New System.Drawing.Point(170, 168)
        Me.cboArea.Name = "cboArea"
        Me.cboArea.Size = New System.Drawing.Size(211, 21)
        Me.cboArea.TabIndex = 10
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Location = New System.Drawing.Point(170, 128)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(211, 20)
        Me.txtEmpresa.TabIndex = 9
        '
        'lblPermiso
        '
        Me.lblPermiso.AutoSize = True
        Me.lblPermiso.Location = New System.Drawing.Point(114, 211)
        Me.lblPermiso.Name = "lblPermiso"
        Me.lblPermiso.Size = New System.Drawing.Size(50, 13)
        Me.lblPermiso.TabIndex = 7
        Me.lblPermiso.Text = "Permiso :"
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(172, 88)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(209, 20)
        Me.txtNombre.TabIndex = 9
        '
        'txtUsuario
        '
        Me.txtUsuario.Location = New System.Drawing.Point(172, 48)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(162, 20)
        Me.txtUsuario.TabIndex = 8
        '
        'dgvPlanillas
        '
        Me.dgvPlanillas.AllowAddNewRow = False
        Me.dgvPlanillas.AllowColumnReorder = False
        Me.dgvPlanillas.AllowDeleteRow = False
        GridViewTextBoxColumn8.FieldName = "REVISION"
        GridViewTextBoxColumn8.HeaderText = "Revisado"
        GridViewTextBoxColumn8.Name = "REVISION"
        GridViewTextBoxColumn8.Width = 70
        GridViewTextBoxColumn9.FieldName = "ID_PAGO_PLANILLA"
        GridViewTextBoxColumn9.HeaderText = "Nro Planilla"
        GridViewTextBoxColumn9.Name = "ID_PAGO_PLANILLA"
        GridViewTextBoxColumn9.ReadOnly = True
        GridViewTextBoxColumn9.Width = 80
        GridViewTextBoxColumn10.FieldName = "Fecha"
        GridViewTextBoxColumn10.HeaderText = "Fecha"
        GridViewTextBoxColumn10.Name = "Fecha"
        GridViewTextBoxColumn10.ReadOnly = True
        GridViewTextBoxColumn10.Width = 80
        GridViewTextBoxColumn11.FieldName = "CONCEPTO"
        GridViewTextBoxColumn11.HeaderText = "Concepto"
        GridViewTextBoxColumn11.Name = "CONCEPTO"
        GridViewTextBoxColumn11.ReadOnly = True
        GridViewTextBoxColumn11.Width = 180
        GridViewTextBoxColumn12.FieldName = "EmprDescripcion"
        GridViewTextBoxColumn12.HeaderText = "Empresa"
        GridViewTextBoxColumn12.Name = "EmprDescripcion"
        GridViewTextBoxColumn12.ReadOnly = True
        GridViewTextBoxColumn12.Width = 200
        GridViewTextBoxColumn13.FieldName = "ObraDescripcion"
        GridViewTextBoxColumn13.HeaderText = "Obra"
        GridViewTextBoxColumn13.Name = "ObraDescripcion"
        GridViewTextBoxColumn13.ReadOnly = True
        GridViewTextBoxColumn13.Width = 200
        GridViewTextBoxColumn14.FieldName = "GLOSA"
        GridViewTextBoxColumn14.HeaderText = "Glosa"
        GridViewTextBoxColumn14.Name = "GLOSA"
        GridViewTextBoxColumn14.ReadOnly = True
        GridViewTextBoxColumn14.Width = 200
        GridViewTextBoxColumn15.FieldName = "ID_MONEDA"
        GridViewTextBoxColumn15.HeaderText = "ID_MONEDA"
        GridViewTextBoxColumn15.IsVisible = False
        GridViewTextBoxColumn15.Name = "ID_MONEDA"
        GridViewTextBoxColumn16.FieldName = "MonDescripcion"
        GridViewTextBoxColumn16.HeaderText = "Moneda"
        GridViewTextBoxColumn16.Name = "MonDescripcion"
        GridViewTextBoxColumn16.ReadOnly = True
        GridViewTextBoxColumn16.Width = 75
        GridViewDecimalColumn1.FieldName = "IMPORTE"
        GridViewDecimalColumn1.FormatString = "{0:N2}"
        GridViewDecimalColumn1.HeaderText = "IMPORTE"
        GridViewDecimalColumn1.IsVisible = False
        GridViewDecimalColumn1.Name = "IMPORTE"
        GridViewDecimalColumn2.FieldName = "Total"
        GridViewDecimalColumn2.FormatString = "{0:N2}"
        GridViewDecimalColumn2.HeaderText = "Total"
        GridViewDecimalColumn2.Name = "Total"
        GridViewDecimalColumn2.ReadOnly = True
        GridViewDecimalColumn2.Width = 70
        GridViewDecimalColumn3.FieldName = "EnProceso"
        GridViewDecimalColumn3.FormatString = "{0:N2}"
        GridViewDecimalColumn3.HeaderText = "En Proceso"
        GridViewDecimalColumn3.Name = "EnProceso"
        GridViewDecimalColumn3.ReadOnly = True
        GridViewDecimalColumn3.Width = 70
        GridViewDecimalColumn4.FieldName = "Saldo"
        GridViewDecimalColumn4.FormatString = "{0:N2}"
        GridViewDecimalColumn4.HeaderText = "Saldo"
        GridViewDecimalColumn4.Name = "Saldo"
        GridViewDecimalColumn4.ReadOnly = True
        GridViewDecimalColumn4.Width = 70
        GridViewTextBoxColumn17.FieldName = "ID_DocOrigen"
        GridViewTextBoxColumn17.HeaderText = "ID_DocOrigen"
        GridViewTextBoxColumn17.IsVisible = False
        GridViewTextBoxColumn17.Name = "ID_DocOrigen"
        GridViewTextBoxColumn18.FieldName = "DocOrigen"
        GridViewTextBoxColumn18.HeaderText = "DocOrigen"
        GridViewTextBoxColumn18.IsVisible = False
        GridViewTextBoxColumn18.Name = "DocOrigen"
        GridViewTextBoxColumn19.FieldName = "Estado"
        GridViewTextBoxColumn19.HeaderText = "Estado"
        GridViewTextBoxColumn19.IsVisible = False
        GridViewTextBoxColumn19.Name = "Estado"
        GridViewTextBoxColumn20.FieldName = "ID_DocPendiente"
        GridViewTextBoxColumn20.HeaderText = "ID_DocPendiente"
        GridViewTextBoxColumn20.IsVisible = False
        GridViewTextBoxColumn20.Name = "ID_DocPendiente"
        GridViewTextBoxColumn21.FieldName = "MonCodigo"
        GridViewTextBoxColumn21.HeaderText = "MonCodigo"
        GridViewTextBoxColumn21.IsVisible = False
        GridViewTextBoxColumn21.Name = "MonCodigo"
        GridViewCheckBoxColumn1.HeaderText = "CODE"
        GridViewCheckBoxColumn1.IsVisible = False
        GridViewCheckBoxColumn1.Name = "CODE"
        Me.dgvPlanillas.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn8, GridViewTextBoxColumn9, GridViewTextBoxColumn10, GridViewTextBoxColumn11, GridViewTextBoxColumn12, GridViewTextBoxColumn13, GridViewTextBoxColumn14, GridViewTextBoxColumn15, GridViewTextBoxColumn16, GridViewDecimalColumn1, GridViewDecimalColumn2, GridViewDecimalColumn3, GridViewDecimalColumn4, GridViewTextBoxColumn17, GridViewTextBoxColumn18, GridViewTextBoxColumn19, GridViewTextBoxColumn20, GridViewTextBoxColumn21, GridViewCheckBoxColumn1})
        Me.dgvPlanillas.EnableFiltering = True
        Me.dgvPlanillas.EnableGrouping = False
        Me.dgvPlanillas.EnableSorting = False
        '
        'frmVistoBuenoUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(496, 456)
        Me.Controls.Add(Me.tbVistoBueno)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmVistoBuenoUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visto Bueno Usuario"
        Me.tbVistoBueno.ResumeLayout(False)
        Me.UsuarioVB.ResumeLayout(False)
        CType(Me.dgvUsuario.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PermisoUsuario.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvPlanillas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tbVistoBueno As System.Windows.Forms.TabControl
    Friend WithEvents UsuarioVB As System.Windows.Forms.TabPage
    Friend WithEvents dgvUsuario As Telerik.WinControls.UI.RadGridView
    Friend WithEvents PermisoUsuario As System.Windows.Forms.TabPage
    Friend WithEvents dgvPlanillas As Telerik.WinControls.UI.MasterGridViewTemplate
    Friend WithEvents cboArea As System.Windows.Forms.ComboBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents lblPermiso As System.Windows.Forms.Label
    Friend WithEvents lblArea As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblUsuario As System.Windows.Forms.Label
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents cboPermiso As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblEmpresa As System.Windows.Forms.Label
    Friend WithEvents txtEmpresa As System.Windows.Forms.TextBox
End Class
