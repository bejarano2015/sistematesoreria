﻿Imports CapaNegocios
Imports CapaDatos
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports IWshRuntimeLibrary
Imports CapaEntidad
Imports BusinessLogicLayer.Mantenimiento
Imports System.Math

Public Class frmNuevaPersona


    Private Sub InsertarNP(ByVal VL_NUM_DOC_IDENTIDAD As String, ByVal VL_NOMBRES As String, ByVal VL_APELLIDO_PATERNO As String, ByVal VL_APELLIDO_MATERNO As String, ByVal VL_ESTADO As Int32)

        Dim VL_SrvProgramacion As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


        VL_BeanResultado = VL_SrvProgramacion.Fnc_Insertar_NP(VL_NUM_DOC_IDENTIDAD, VL_NOMBRES, VL_APELLIDO_PATERNO, VL_APELLIDO_MATERNO, VL_ESTADO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub


    'Fnc_Insertar_NP
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        InsertarNP(txtDni.Text.Trim, txtNombre.Text.Trim, txtPaterno.Text.Trim, txtMaterno.Text.Trim, 1)
        Me.Close()
    End Sub

    Private Sub frmNuevaPersona_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtDni.Focus()
    End Sub
End Class