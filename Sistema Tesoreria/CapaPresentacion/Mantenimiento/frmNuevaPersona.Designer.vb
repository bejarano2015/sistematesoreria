﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNuevaPersona
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblDNI = New System.Windows.Forms.Label()
        Me.lblNombres = New System.Windows.Forms.Label()
        Me.lblPaterno = New System.Windows.Forms.Label()
        Me.lblMaterno = New System.Windows.Forms.Label()
        Me.txtDni = New ctrLibreria.Controles.BeTextBox()
        Me.txtNombre = New ctrLibreria.Controles.BeTextBox()
        Me.txtPaterno = New ctrLibreria.Controles.BeTextBox()
        Me.txtMaterno = New ctrLibreria.Controles.BeTextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGrabar
        '
        Me.btnGrabar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.Location = New System.Drawing.Point(65, 19)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(92, 23)
        Me.btnGrabar.TabIndex = 5
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(236, 19)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(92, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtMaterno)
        Me.GroupBox1.Controls.Add(Me.txtPaterno)
        Me.GroupBox1.Controls.Add(Me.txtNombre)
        Me.GroupBox1.Controls.Add(Me.txtDni)
        Me.GroupBox1.Controls.Add(Me.lblMaterno)
        Me.GroupBox1.Controls.Add(Me.lblPaterno)
        Me.GroupBox1.Controls.Add(Me.lblNombres)
        Me.GroupBox1.Controls.Add(Me.lblDNI)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(382, 181)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Personales"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnCancelar)
        Me.GroupBox2.Controls.Add(Me.btnGrabar)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 197)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(381, 52)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'lblDNI
        '
        Me.lblDNI.AutoSize = True
        Me.lblDNI.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDNI.Location = New System.Drawing.Point(6, 42)
        Me.lblDNI.Name = "lblDNI"
        Me.lblDNI.Size = New System.Drawing.Size(210, 13)
        Me.lblDNI.TabIndex = 0
        Me.lblDNI.Text = "N° Documento Identidad (DNI):"
        '
        'lblNombres
        '
        Me.lblNombres.AutoSize = True
        Me.lblNombres.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombres.Location = New System.Drawing.Point(9, 75)
        Me.lblNombres.Name = "lblNombres"
        Me.lblNombres.Size = New System.Drawing.Size(69, 13)
        Me.lblNombres.TabIndex = 1
        Me.lblNombres.Text = "Nombres:"
        '
        'lblPaterno
        '
        Me.lblPaterno.AutoSize = True
        Me.lblPaterno.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaterno.Location = New System.Drawing.Point(9, 108)
        Me.lblPaterno.Name = "lblPaterno"
        Me.lblPaterno.Size = New System.Drawing.Size(119, 13)
        Me.lblPaterno.TabIndex = 1
        Me.lblPaterno.Text = "Apellido Paterno:"
        '
        'lblMaterno
        '
        Me.lblMaterno.AutoSize = True
        Me.lblMaterno.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaterno.Location = New System.Drawing.Point(9, 140)
        Me.lblMaterno.Name = "lblMaterno"
        Me.lblMaterno.Size = New System.Drawing.Size(121, 13)
        Me.lblMaterno.TabIndex = 1
        Me.lblMaterno.Text = "Apellido Materno:"
        '
        'txtDni
        '
        Me.txtDni.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDni.BackColor = System.Drawing.Color.Ivory
        Me.txtDni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDni.ForeColor = System.Drawing.Color.Black
        Me.txtDni.KeyEnter = True
        Me.txtDni.Location = New System.Drawing.Point(220, 39)
        Me.txtDni.MaxLength = 8
        Me.txtDni.Name = "txtDni"
        Me.txtDni.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDni.ShortcutsEnabled = False
        Me.txtDni.Size = New System.Drawing.Size(145, 21)
        Me.txtDni.TabIndex = 5
        Me.txtDni.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'txtNombre
        '
        Me.txtNombre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNombre.BackColor = System.Drawing.Color.Ivory
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.ForeColor = System.Drawing.Color.Black
        Me.txtNombre.KeyEnter = True
        Me.txtNombre.Location = New System.Drawing.Point(220, 72)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNombre.ShortcutsEnabled = False
        Me.txtNombre.Size = New System.Drawing.Size(145, 21)
        Me.txtNombre.TabIndex = 6
        Me.txtNombre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        '
        'txtPaterno
        '
        Me.txtPaterno.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPaterno.BackColor = System.Drawing.Color.Ivory
        Me.txtPaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPaterno.ForeColor = System.Drawing.Color.Black
        Me.txtPaterno.KeyEnter = True
        Me.txtPaterno.Location = New System.Drawing.Point(221, 105)
        Me.txtPaterno.Name = "txtPaterno"
        Me.txtPaterno.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPaterno.ShortcutsEnabled = False
        Me.txtPaterno.Size = New System.Drawing.Size(144, 21)
        Me.txtPaterno.TabIndex = 7
        Me.txtPaterno.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        '
        'txtMaterno
        '
        Me.txtMaterno.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMaterno.BackColor = System.Drawing.Color.Ivory
        Me.txtMaterno.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMaterno.ForeColor = System.Drawing.Color.Black
        Me.txtMaterno.KeyEnter = True
        Me.txtMaterno.Location = New System.Drawing.Point(220, 137)
        Me.txtMaterno.Name = "txtMaterno"
        Me.txtMaterno.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMaterno.ShortcutsEnabled = False
        Me.txtMaterno.Size = New System.Drawing.Size(145, 21)
        Me.txtMaterno.TabIndex = 8
        Me.txtMaterno.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        '
        'frmNuevaPersona
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(406, 262)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmNuevaPersona"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Nueva Persona"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblMaterno As System.Windows.Forms.Label
    Friend WithEvents lblPaterno As System.Windows.Forms.Label
    Friend WithEvents lblNombres As System.Windows.Forms.Label
    Friend WithEvents lblDNI As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDni As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtMaterno As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPaterno As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNombre As ctrLibreria.Controles.BeTextBox
End Class
