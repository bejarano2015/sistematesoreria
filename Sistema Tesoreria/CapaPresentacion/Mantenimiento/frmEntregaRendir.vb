﻿Imports CapaNegocios
Imports CapaDatos
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports IWshRuntimeLibrary
Imports CapaEntidad
Imports BusinessLogicLayer.Mantenimiento
Imports System.Math



Public Class frmEntregaRendir

    Dim V_FechaInicio As Date = "01/01/2011"
    Dim V_FechaFin As Date = "01/01/2011"
    Dim V_Empresa As String = "0"
    Dim V_Rsol As String = "0"
    Dim V_Solicitante As String = "0"
    Dim V_Monto As Decimal
    Dim v_Area As String

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmEntregaRendir = Nothing

    Public Shared Function Instance() As frmEntregaRendir
        If frmInstance Is Nothing Then
            frmInstance = New frmEntregaRendir
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmDerivarPagosOrdenCompra_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Dim objEntregaRendir As BeanEntregaRendir
    Dim WithEvents cmr As CurrencyManager
    Dim iOpcion As Integer

    Private Sub LimpiarControles()

        txtCodigo.Clear()
        txtGlosa.Clear()
        txtMonto.Clear()
        cboArea.SelectedIndex = 0
        cboMoneda.SelectedIndex = 0
        cboSolicitante.SelectedIndex = 0
        cboAutoriza.SelectedIndex = 0
        cboGirado.SelectedIndex = 0
        cboEmpresa.SelectedIndex = 4
        cboObraArea.SelectedIndex = 0
        dtpFecha.Value = DateTime.Now()
    End Sub

    Private Sub LimpiarControlesBuscar()

        dtpInicio.Value = DateTime.Now()
        dtpFin.Value = DateTime.Now()
        cboBEmpresa.SelectedIndex = 0
        cboBSolicitante.SelectedIndex = 0
    End Sub

#Region "Listar"


    Private Sub ListarTipoDocumentos()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_TipoDocumentos()

            If VL_BeanResultado.blnExiste = True Then
                cboTipoDocumento.DataSource = VL_BeanResultado.dtResultado
                cboTipoDocumento.DisplayMember = "DESCRIPCION"
                cboTipoDocumento.ValueMember = "ID_DOCUMENTO"
                'cboReferencia.SelectedIndex = 4

                'cboReferencia.SelectedIndex = -1
                'cboReferencia.Text = "Seleccione..."

                'cboReferencia.SelectedItem = -1

            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub ListarEmpresas()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Empresas()

            If VL_BeanResultado.blnExiste = True Then
                cboEmpresa.DataSource = VL_BeanResultado.dtResultado
                cboEmpresa.DisplayMember = "EmprDescripcion"
                cboEmpresa.ValueMember = "EmprCodigo"
                cboEmpresa.SelectedIndex = 4

                'cboReferencia.SelectedIndex = -1
                'cboReferencia.Text = "Seleccione..."

                'cboReferencia.SelectedItem = -1

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarBEmpresas()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_BEmpresas()

            If VL_BeanResultado.blnExiste = True Then
                cboBEmpresa.DataSource = VL_BeanResultado.dtResultado
                cboBEmpresa.DisplayMember = "EmprDescripcion"
                cboBEmpresa.ValueMember = "EmprCodigo"




                'cboReferencia.Text = "Seleccione..."
                'cboReferencia.SelectedItem = -1

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub


    Private Sub ListarArea()

        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        objEntregaRendir = New BeanEntregaRendir(cboEmpresa.SelectedValue.ToString())


        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_AreaPorEmpresa(objEntregaRendir)

            If VL_BeanResultado.blnExiste = True Then
                cboArea.DataSource = VL_BeanResultado.dtResultado
                cboArea.DisplayMember = "AreaNombre"
                cboArea.ValueMember = "AreaCodigo"
            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarAutoriza()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Autoriza()

            If VL_BeanResultado.blnExiste = True Then
                cboAutoriza.DataSource = VL_BeanResultado.dtResultado
                cboAutoriza.DisplayMember = "Apellidos_Nombres"
                cboAutoriza.ValueMember = "Id_CorrelativoPadron"

                'cboRAut.DataSource = VL_BeanResultado.dtResultado
                'cboRAut.DisplayMember = "Nombres"
                'cboRAut.ValueMember = "TPlaCodigo"



            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try
    End Sub


    Private Sub ListarSolicitante()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        objEntregaRendir = New BeanEntregaRendir(cboEmpresa.SelectedValue.ToString())


        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Empleados()

            If VL_BeanResultado.blnExiste = True Then
                cboSolicitante.DataSource = VL_BeanResultado.dtResultado
                cboSolicitante.DisplayMember = "Apellidos_Nombres"
                cboSolicitante.ValueMember = "Id_CorrelativoPadron"

                'cboRsoli.DataSource = VL_BeanResultado.dtResultado
                'cboRsoli.DisplayMember = "Nombres"
                'cboRsoli.ValueMember = "TPlaCodigo"



            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarBSolicitante()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()


        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_BEmpleados()

            If VL_BeanResultado.blnExiste = True Then

                cboBSolicitante.DataSource = VL_BeanResultado.dtResultado
                cboBSolicitante.DisplayMember = "Apellidos_Nombres"
                cboBSolicitante.ValueMember = "Id_CorrelativoPadron"

                'cboRBSol.DataSource = VL_BeanResultado.dtResultado
                'cboRBSol.DisplayMember = "Nombres"
                'cboRBSol.ValueMember = "TPlaCodigo"


                'txta.Text = "TPlaCodigo"


            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub


    Private Sub ListarGirado()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        objEntregaRendir = New BeanEntregaRendir(cboEmpresa.SelectedValue.ToString())


        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Girado()

            If VL_BeanResultado.blnExiste = True Then
                cboGirado.DataSource = VL_BeanResultado.dtResultado
                cboGirado.DisplayMember = "Apellidos_Nombres"
                cboGirado.ValueMember = "Id_CorrelativoPadron"

                'cboRgir.DataSource = VL_BeanResultado.dtResultado
                'cboRgir.DisplayMember = "Nombres"
                'cboRgir.ValueMember = "TPlaCodigo"



            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub


    Private Sub ListarObras()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        objEntregaRendir = New BeanEntregaRendir(cboEmpresa.SelectedValue.ToString())


        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Obras()

            If VL_BeanResultado.blnExiste = True Then
                cboObraArea.DataSource = VL_BeanResultado.dtResultado
                cboObraArea.DisplayMember = "ObraDescripcion"
                cboObraArea.ValueMember = "ObraCodigo"

                cboRObra.DataSource = VL_BeanResultado.dtResultado
                cboRObra.DisplayMember = "ObraDescripcion"
                cboRObra.ValueMember = "EmprCodigo"


            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarMonedas()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Monedas()

            If VL_BeanResultado.blnExiste = True Then
                cboMoneda.DataSource = VL_BeanResultado.dtResultado
                cboMoneda.DisplayMember = "MonDescripcion"
                cboMoneda.ValueMember = "MonCodigo"
            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarEstado()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Estado_EntregaRendir()

            If VL_BeanResultado.blnExiste = True Then
                cboEstado.DataSource = VL_BeanResultado.dtResultado
                cboEstado.DisplayMember = "Estado"
                cboEstado.ValueMember = "ID_ESTADO"
            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub FiltrarTodaEntregaRendir(ByVal ER_Inicio As String, ByVal ER_Fin As String)

        Dim VL_SrvEnregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEnregaRendir.Fnc_Listar_EntregaRendir(Convert.ToDateTime(ER_Inicio), Convert.ToDateTime(ER_Fin))

            If VL_BeanResultado.blnExiste = True Then
                'dgvEntregaRendir.DataSource = VL_BeanResultado.dtResultado

                dgvEntregaRendir.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

#End Region

#Region "Procesos"

    Private Sub RegistrarEntregaRendir()

        Dim VL_SrvProgramacion As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        V_Monto = Round(Convert.ToDecimal(txtMonto.Text), 2, MidpointRounding.ToEven)

        objEntregaRendir = New BeanEntregaRendir(cboEmpresa.SelectedValue.ToString(), cboSolicitante.SelectedValue.ToString(), cboObraArea.SelectedValue.ToString(), cboMoneda.SelectedValue.ToString(), V_Monto, txtGlosa.Text, cboEstado.SelectedValue.ToString(), dtpFecha.Value, cboAutoriza.SelectedValue.ToString(), _cboGirado.SelectedValue.ToString(), cboArea.SelectedValue.ToString(), cboRObra.SelectedValue.ToString(), cboTipoDocumento.SelectedValue.ToString(), "0")


        VL_BeanResultado = VL_SrvProgramacion.Fnc_Registrar_Entrega_Rendir(objEntregaRendir)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub


    Private Sub EliminarEntregaRendir(ByVal ID_EntregaRendir As String, ByVal DocOrigen As String)

        Dim VL_SrvProgramacion As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SrvProgramacion.Fnc_Eliminar_Entrega_Rendir(ID_EntregaRendir, DocOrigen)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub
    Private Sub ActualizarEntregaRendir()

        Dim VL_SrvProgramacion As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        V_Monto = txtMonto.Text
        V_Area = cboArea.SelectedValue.ToString()
        objEntregaRendir = New BeanEntregaRendir(txtCodigo.Text, cboEmpresa.SelectedValue.ToString(), cboSolicitante.SelectedValue.ToString(), cboObraArea.SelectedValue.ToString(), cboMoneda.SelectedValue.ToString(), V_Monto, txtGlosa.Text, cboEstado.SelectedValue.ToString(), dtpFecha.Value, cboAutoriza.SelectedValue.ToString(), _cboGirado.SelectedValue.ToString(), v_Area, cboRObra.SelectedValue.ToString())

        VL_BeanResultado = VL_SrvProgramacion.Fnc_Actualizar_Entrega_Rendir(objEntregaRendir)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub


    Private Sub BuscarPorEmpresaSolicitante()

        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        objEntregaRendir = New BeanEntregaRendir(V_FechaInicio, V_FechaFin, cboBEmpresa.SelectedValue.ToString(), cboBSolicitante.SelectedValue.ToString())

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Buscar_Por_Empresa_Solicitante(objEntregaRendir)

            If VL_BeanResultado.blnExiste = True Then
                dgvEntregaRendir.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub BuscarPorFecha()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        objEntregaRendir = New BeanEntregaRendir(Convert.ToDateTime(dtpInicio.Text), Convert.ToDateTime(dtpFin.Text), cboBEmpresa.SelectedValue.ToString(), "")

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Buscar_Por_Fechas(objEntregaRendir)

            If VL_BeanResultado.blnExiste = True Then
                'dgvEntregaRendir.DataSource = VL_BeanResultado.dtResultado

                dgvEntregaRendir.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try

    End Sub


#End Region

    Private Sub frmProgramacionEntregaRendir_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ListarTipoDocumentos()
        ListarEmpresas()
        ListarBEmpresas()
        ListarBSolicitante()

        ListarSolicitante()
        ListarGirado()
        ListarArea()
        ListarObras()


        ListarAutoriza()
        'ListarObras()
        ListarMonedas()
        ListarEstado()
        dtpFecha.Value = DateTime.Now()
        dtpInicio.Value = DateTime.Now()
        dtpFin.Value = DateTime.Now()
        FiltrarTodaEntregaRendir(dtpInicio.Value.ToString(), dtpFin.Value.ToString())
        Eliminar.Enabled = False

    End Sub

    Private Sub ToolStripButton2_Click_1(sender As Object, e As EventArgs) Handles Grabar.Click

        If txtCodigo.Text = "" Then
            If txtMonto.Text = "" And txtGlosa.Text = "" Then

                MessageBox.Show("Debe llenar los Campos 'Monto' y 'Glosa'")

            ElseIf txtMonto.Text = "" Then
                MessageBox.Show("Debe llenar los Campo 'Monto'")

            ElseIf txtGlosa.Text = "" Then
                MessageBox.Show("Debe llenar los Campo 'Glosa'")

            Else
                RegistrarEntregaRendir()
                LimpiarControles()
                txtMonto.Focus()
            End If

        Else
            ActualizarEntregaRendir()
            LimpiarControles()
            ListarTipoDocumentos()
        End If
    End Sub

    Private Sub ToolStripButton5_Click_1(sender As Object, e As EventArgs) Handles Salir.Click
        Me.Close()
    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles Buscar.Click

        'If cboBEmpresa.SelectedIndex = 0 And cboBSolicitante.SelectedIndex = 0 Then

        BuscarPorFecha()
        'Else

        '    BuscarPorEmpresaSolicitante()
        'End If

    End Sub

    Private Sub TabControl2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl2.SelectedIndexChanged

        If (TabControl2.SelectedIndex = 1) Then
            Buscar.Enabled = True
            Eliminar.Enabled = True
            FiltrarTodaEntregaRendir(dtpInicio.Value.ToString(), dtpFin.Value.ToString())
            Grabar.Enabled = False
            LimpiarControles()
        Else
            Buscar.Enabled = False
            Grabar.Enabled = True
            Eliminar.Enabled = False
            LimpiarControlesBuscar()


        End If
    End Sub


    Private Sub cboEmpresa_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedValueChanged
        'ListarSolicitante()
        ' ListarGirado()
        'ListarArea()
        'ListarObras()
    End Sub

    Private Sub Editar_Click(sender As Object, e As EventArgs) Handles Editar.Click
    End Sub






    Private Sub lblOtro_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles lblOtro.LinkClicked
        Dim frmNuevo As New frmNuevaPersona
        frmNuevo.MdiParent = Me.ParentForm
        frmNuevo.Show()
        'Me.Dispose()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        ListarGirado()
    End Sub

    Private Sub Eliminar_Click(sender As Object, e As EventArgs) Handles Eliminar.Click

        Dim identregarendir As String = dgvEntregaRendir.CurrentRow.Cells("Nro").Value
        Dim docorigen As String = dgvEntregaRendir.CurrentRow.Cells("DocOrigen").Value

        EliminarEntregaRendir(identregarendir, docorigen)

        FiltrarTodaEntregaRendir(dtpInicio.Value.ToString(), dtpFin.Value.ToString())
    End Sub

    Private Sub tsmEliminar_Click(sender As Object, e As EventArgs) Handles tsmEliminar.Click
        Dim identregarendir As String = dgvEntregaRendir.CurrentRow.Cells("Nro").Value
        Dim docorigen As String = dgvEntregaRendir.CurrentRow.Cells("DocOrigen").Value

        EliminarEntregaRendir(identregarendir, docorigen)

        FiltrarTodaEntregaRendir(dtpInicio.Value.ToString(), dtpFin.Value.ToString())
    End Sub

    Private Sub Menu_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles Menu.Opening

    End Sub

    Private Sub cboAutoriza_TextChanged(sender As Object, e As EventArgs) Handles cboAutoriza.TextChanged
        'cboAutoriza.DropDownListElement.AutoCompleteSuggest.SuggestMode = SuggestMode.Contains
        'cboAutoriza.FindString()
    End Sub

    Private Sub dgvEntregaRendir_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles dgvEntregaRendir.CellDoubleClick


        txtCodigo.Text = Convert.ToString(dgvEntregaRendir.CurrentRow.Cells("Nro").Value)
        cboEmpresa.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("EmprCodigo").Value
        cboArea.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("AreaCodigo").Value

        dtpFecha.Value = Convert.ToDateTime(dgvEntregaRendir.CurrentRow.Cells("Fecha").Value)
        cboEstado.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("ID_ESTADO").Value
        cboMoneda.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("MonCodigo").Value
        txtMonto.Text = Convert.ToDecimal(dgvEntregaRendir.CurrentRow.Cells("Monto").Value)
        txtGlosa.Text = Convert.ToString(dgvEntregaRendir.CurrentRow.Cells("Concepto").Value)


        cboSolicitante.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("SolCodigo").Value
        'cboRsoli.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("SolTPla").Value

        cboGirado.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("GirCodigo").Value
        '    'cboRgir.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("GirTPla").Value

        cboAutoriza.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("AutCodigo").Value
        '    'cboRAut.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("AutTPla").Value


        cboObraArea.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("ObraAreaCodigo").Value
        '    'cboRObra.SelectedValue = dgvEntregaRendir.CurrentRow.Cells("EmprCodigo").Value

        TabControl2.SelectedTab = tpCuentaRendir


    End Sub
End Class