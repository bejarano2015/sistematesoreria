﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteProgramaciondePagos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbConsulta = New System.Windows.Forms.GroupBox()
        Me.btnGenerarReporte = New System.Windows.Forms.Button()
        Me.cboMoneda = New System.Windows.Forms.ComboBox()
        Me.cboEmpresa = New System.Windows.Forms.ComboBox()
        Me.cboEstado = New System.Windows.Forms.ComboBox()
        Me.dtpFin = New System.Windows.Forms.DateTimePicker()
        Me.dtpInicio = New System.Windows.Forms.DateTimePicker()
        Me.lblMoneda = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblFechaInicio = New System.Windows.Forms.Label()
        Me.gpReporte = New System.Windows.Forms.GroupBox()
        Me.rvReporte = New Telerik.ReportViewer.WinForms.ReportViewer()
        Me.gbConsulta.SuspendLayout()
        Me.gpReporte.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbConsulta
        '
        Me.gbConsulta.Controls.Add(Me.btnGenerarReporte)
        Me.gbConsulta.Controls.Add(Me.cboMoneda)
        Me.gbConsulta.Controls.Add(Me.cboEmpresa)
        Me.gbConsulta.Controls.Add(Me.cboEstado)
        Me.gbConsulta.Controls.Add(Me.dtpFin)
        Me.gbConsulta.Controls.Add(Me.dtpInicio)
        Me.gbConsulta.Controls.Add(Me.lblMoneda)
        Me.gbConsulta.Controls.Add(Me.Label4)
        Me.gbConsulta.Controls.Add(Me.lblEstado)
        Me.gbConsulta.Controls.Add(Me.Label2)
        Me.gbConsulta.Controls.Add(Me.lblFechaInicio)
        Me.gbConsulta.Dock = System.Windows.Forms.DockStyle.Top
        Me.gbConsulta.Location = New System.Drawing.Point(0, 0)
        Me.gbConsulta.Name = "gbConsulta"
        Me.gbConsulta.Size = New System.Drawing.Size(1045, 100)
        Me.gbConsulta.TabIndex = 0
        Me.gbConsulta.TabStop = False
        Me.gbConsulta.Text = "Filtro Generales"
        '
        'btnGenerarReporte
        '
        Me.btnGenerarReporte.Location = New System.Drawing.Point(527, 67)
        Me.btnGenerarReporte.Name = "btnGenerarReporte"
        Me.btnGenerarReporte.Size = New System.Drawing.Size(115, 23)
        Me.btnGenerarReporte.TabIndex = 8
        Me.btnGenerarReporte.Text = "Generar Reporte"
        Me.btnGenerarReporte.UseVisualStyleBackColor = True
        '
        'cboMoneda
        '
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.Location = New System.Drawing.Point(283, 67)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(121, 21)
        Me.cboMoneda.TabIndex = 7
        '
        'cboEmpresa
        '
        Me.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpresa.FormattingEnabled = True
        Me.cboEmpresa.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cboEmpresa.Location = New System.Drawing.Point(508, 34)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(308, 21)
        Me.cboEmpresa.TabIndex = 7
        '
        'cboEstado
        '
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.Location = New System.Drawing.Point(283, 34)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(121, 21)
        Me.cboEstado.TabIndex = 7
        '
        'dtpFin
        '
        Me.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFin.Location = New System.Drawing.Point(105, 70)
        Me.dtpFin.Name = "dtpFin"
        Me.dtpFin.Size = New System.Drawing.Size(95, 20)
        Me.dtpFin.TabIndex = 6
        '
        'dtpInicio
        '
        Me.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInicio.Location = New System.Drawing.Point(105, 35)
        Me.dtpInicio.MinDate = New Date(2015, 1, 1, 0, 0, 0, 0)
        Me.dtpInicio.Name = "dtpInicio"
        Me.dtpInicio.Size = New System.Drawing.Size(96, 20)
        Me.dtpInicio.TabIndex = 5
        '
        'lblMoneda
        '
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.Location = New System.Drawing.Point(234, 71)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(49, 13)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.Text = "Moneda:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(451, 37)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Empresa:"
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Location = New System.Drawing.Point(234, 37)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(43, 13)
        Me.lblEstado.TabIndex = 2
        Me.lblEstado.Text = "Estado:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(27, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Fecha Fin: "
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.AutoSize = True
        Me.lblFechaInicio.Location = New System.Drawing.Point(27, 37)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(71, 13)
        Me.lblFechaInicio.TabIndex = 0
        Me.lblFechaInicio.Text = "Fecha Inicio: "
        '
        'gpReporte
        '
        Me.gpReporte.Controls.Add(Me.rvReporte)
        Me.gpReporte.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gpReporte.Location = New System.Drawing.Point(0, 100)
        Me.gpReporte.Name = "gpReporte"
        Me.gpReporte.Size = New System.Drawing.Size(1045, 383)
        Me.gpReporte.TabIndex = 1
        Me.gpReporte.TabStop = False
        '
        'rvReporte
        '
        Me.rvReporte.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rvReporte.Location = New System.Drawing.Point(3, 16)
        Me.rvReporte.Name = "rvReporte"
        Me.rvReporte.Size = New System.Drawing.Size(1039, 364)
        Me.rvReporte.TabIndex = 0
        '
        'frmReporteProgramaciondePagos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1045, 483)
        Me.Controls.Add(Me.gpReporte)
        Me.Controls.Add(Me.gbConsulta)
        Me.Name = "frmReporteProgramaciondePagos"
        Me.Text = "frmReporteProgramaciondePagos"
        Me.gbConsulta.ResumeLayout(False)
        Me.gbConsulta.PerformLayout()
        Me.gpReporte.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbConsulta As System.Windows.Forms.GroupBox
    Friend WithEvents dtpInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblFechaInicio As System.Windows.Forms.Label
    Friend WithEvents gpReporte As System.Windows.Forms.GroupBox
    Friend WithEvents cboMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents cboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents dtpFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents rvReporte As Telerik.ReportViewer.WinForms.ReportViewer
    Friend WithEvents btnGenerarReporte As System.Windows.Forms.Button
    Friend WithEvents cboEmpresa As System.Windows.Forms.ComboBox
End Class
