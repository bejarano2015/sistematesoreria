﻿Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports BusinessLogicLayer.Procesos

Public Class frmReporteProgramaciondePagos

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmReporteProgramaciondePagos = Nothing
    Public Shared Function Instance() As frmReporteProgramaciondePagos
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteProgramaciondePagos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function


#End Region


#Region "Listar"
    Private Sub ListarEmpresas()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Try
            VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Listar_Combos_Varios_Reportes("1")

            If VL_BeanResultado.blnResultado = True Then

                cboEmpresa.DataSource = VL_BeanResultado.dtResultado
                cboEmpresa.DisplayMember = "EmprDescripcion"
                cboEmpresa.ValueMember = "EmprCodigo"
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub ListarEstados()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Try
            VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Listar_Combos_Varios_Reportes("3")

            If VL_BeanResultado.blnResultado = True Then

                cboMoneda.DataSource = VL_BeanResultado.dtResultado
                cboMoneda.DisplayMember = "MonDescripcion"
                cboMoneda.ValueMember = "MonCodigo"
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub ListarMonedas()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Try
            VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Listar_Combos_Varios_Reportes("2")

            If VL_BeanResultado.blnResultado = True Then

                cboEstado.DataSource = VL_BeanResultado.dtResultado
                cboEstado.DisplayMember = "Estado"
                cboEstado.ValueMember = "ID_ESTADO"
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try
    End Sub


#End Region

#Region "Procesos"


    Private Sub GenerarReporte()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            Dim fechainicio As DateTime = dtpInicio.Value
            Dim fechafin As DateTime = dtpFin.Value
            Dim estado As String = cboEstado.SelectedValue
            Dim moneda As String = cboMoneda.SelectedValue
            Dim empresa As String = cboEmpresa.SelectedValue


            VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Generar_Reportes(fechainicio, fechafin, estado, moneda, empresa)

            If VL_BeanResultado.blnExiste = True Then

                Dim VL_rptFiltro_ProgramacionPago As New rptFiltro_ProgramacionPago()

                VL_rptFiltro_ProgramacionPago.DataSource = (VL_BeanResultado.dtResultado)

                Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
                instanceReportSource.ReportDocument = VL_rptFiltro_ProgramacionPago
                Me.rvReporte.ReportSource = instanceReportSource
                Me.rvReporte.RefreshReport()


                Return
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
                Return
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

#End Region


    Private Sub frmReporteProgramaciondePagos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ListarMonedas()
        ListarEstados()
        ListarEmpresas()

    End Sub

    Private Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click
        GenerarReporte()
    End Sub


End Class