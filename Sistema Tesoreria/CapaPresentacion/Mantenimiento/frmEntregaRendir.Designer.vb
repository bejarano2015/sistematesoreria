﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEntregaRendir
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn12 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn13 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn14 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn15 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn16 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn17 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn18 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn19 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn20 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn21 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn22 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn23 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn24 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn25 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn26 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn27 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn28 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn29 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn30 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEntregaRendir))
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.tpCuentaRendir = New System.Windows.Forms.TabPage()
        Me.gbCuntasRendir = New System.Windows.Forms.GroupBox()
        Me.cboRObra = New System.Windows.Forms.ComboBox()
        Me.txtGlosa = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cboTipoDocumento = New ctrLibreria.Controles.BeComboBox()
        Me.lblTipodocumento = New System.Windows.Forms.Label()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.btnRefrescar = New System.Windows.Forms.Button()
        Me.lblOtro = New System.Windows.Forms.LinkLabel()
        Me.cboGirado = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.cboSolicitante = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.cboObraArea = New ctrLibreria.Controles.BeComboBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.cboAutoriza = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.txtMonto = New ctrLibreria.Controles.BeTextBox()
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.cboArea = New ctrLibreria.Controles.BeComboBox()
        Me.cboEstado = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.cboEmpresa = New ctrLibreria.Controles.BeComboBox()
        Me.dtpFecha = New ctrLibreria.Controles.BeDateTimePicker()
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.tpBusqueda = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvEntregaRendir = New Telerik.WinControls.UI.RadGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboRBSol = New ctrLibreria.Controles.BeComboBox()
        Me.cboBSolicitante = New ctrLibreria.Controles.BeComboBox()
        Me.dtpFin = New ctrLibreria.Controles.BeDateTimePicker()
        Me.dtpInicio = New ctrLibreria.Controles.BeDateTimePicker()
        Me.cboBEmpresa = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.Menu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmEliminar = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsMenu = New System.Windows.Forms.ToolStrip()
        Me.Grabar = New System.Windows.Forms.ToolStripButton()
        Me.Editar = New System.Windows.Forms.ToolStripButton()
        Me.Buscar = New System.Windows.Forms.ToolStripButton()
        Me.Eliminar = New System.Windows.Forms.ToolStripButton()
        Me.Salir = New System.Windows.Forms.ToolStripButton()
        Me.TabControl2.SuspendLayout()
        Me.tpCuentaRendir.SuspendLayout()
        Me.gbCuntasRendir.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.tpBusqueda.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvEntregaRendir, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEntregaRendir.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.Menu.SuspendLayout()
        Me.tsMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.tpCuentaRendir)
        Me.TabControl2.Controls.Add(Me.tpBusqueda)
        Me.TabControl2.Location = New System.Drawing.Point(0, 28)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(1024, 465)
        Me.TabControl2.TabIndex = 4
        '
        'tpCuentaRendir
        '
        Me.tpCuentaRendir.BackColor = System.Drawing.Color.LightSteelBlue
        Me.tpCuentaRendir.Controls.Add(Me.gbCuntasRendir)
        Me.tpCuentaRendir.Location = New System.Drawing.Point(4, 22)
        Me.tpCuentaRendir.Name = "tpCuentaRendir"
        Me.tpCuentaRendir.Padding = New System.Windows.Forms.Padding(3)
        Me.tpCuentaRendir.Size = New System.Drawing.Size(1016, 439)
        Me.tpCuentaRendir.TabIndex = 0
        Me.tpCuentaRendir.Text = "Entrega a Rendir"
        '
        'gbCuntasRendir
        '
        Me.gbCuntasRendir.Controls.Add(Me.cboRObra)
        Me.gbCuntasRendir.Controls.Add(Me.txtGlosa)
        Me.gbCuntasRendir.Controls.Add(Me.GroupBox4)
        Me.gbCuntasRendir.Controls.Add(Me.BeLabel9)
        Me.gbCuntasRendir.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.gbCuntasRendir.Location = New System.Drawing.Point(5, 29)
        Me.gbCuntasRendir.Name = "gbCuntasRendir"
        Me.gbCuntasRendir.Size = New System.Drawing.Size(1004, 393)
        Me.gbCuntasRendir.TabIndex = 0
        Me.gbCuntasRendir.TabStop = False
        Me.gbCuntasRendir.Text = "Entrega a Rendir"
        '
        'cboRObra
        '
        Me.cboRObra.FormattingEnabled = True
        Me.cboRObra.Location = New System.Drawing.Point(512, 292)
        Me.cboRObra.Name = "cboRObra"
        Me.cboRObra.Size = New System.Drawing.Size(121, 21)
        Me.cboRObra.TabIndex = 27
        Me.cboRObra.Visible = False
        '
        'txtGlosa
        '
        Me.txtGlosa.BackColor = System.Drawing.Color.Ivory
        Me.txtGlosa.Font = New System.Drawing.Font("Verdana", 10.0!)
        Me.txtGlosa.Location = New System.Drawing.Point(11, 318)
        Me.txtGlosa.Multiline = True
        Me.txtGlosa.Name = "txtGlosa"
        Me.txtGlosa.Size = New System.Drawing.Size(986, 67)
        Me.txtGlosa.TabIndex = 26
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cboTipoDocumento)
        Me.GroupBox4.Controls.Add(Me.lblTipodocumento)
        Me.GroupBox4.Controls.Add(Me.GroupBox7)
        Me.GroupBox4.Controls.Add(Me.GroupBox6)
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Controls.Add(Me.GroupBox3)
        Me.GroupBox4.Controls.Add(Me.cboArea)
        Me.GroupBox4.Controls.Add(Me.cboEstado)
        Me.GroupBox4.Controls.Add(Me.BeLabel7)
        Me.GroupBox4.Controls.Add(Me.cboEmpresa)
        Me.GroupBox4.Controls.Add(Me.dtpFecha)
        Me.GroupBox4.Controls.Add(Me.txtCodigo)
        Me.GroupBox4.Controls.Add(Me.BeLabel8)
        Me.GroupBox4.Controls.Add(Me.BeLabel15)
        Me.GroupBox4.Controls.Add(Me.BeLabel1)
        Me.GroupBox4.Controls.Add(Me.BeLabel2)
        Me.GroupBox4.Location = New System.Drawing.Point(11, 27)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(986, 258)
        Me.GroupBox4.TabIndex = 25
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Datos"
        '
        'cboTipoDocumento
        '
        Me.cboTipoDocumento.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoDocumento.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDocumento.ForeColor = System.Drawing.Color.Black
        Me.cboTipoDocumento.FormattingEnabled = True
        Me.cboTipoDocumento.KeyEnter = True
        Me.cboTipoDocumento.Location = New System.Drawing.Point(613, 126)
        Me.cboTipoDocumento.Name = "cboTipoDocumento"
        Me.cboTipoDocumento.Size = New System.Drawing.Size(238, 21)
        Me.cboTipoDocumento.TabIndex = 29
        '
        'lblTipodocumento
        '
        Me.lblTipodocumento.AutoSize = True
        Me.lblTipodocumento.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipodocumento.Location = New System.Drawing.Point(507, 129)
        Me.lblTipodocumento.Name = "lblTipodocumento"
        Me.lblTipodocumento.Size = New System.Drawing.Size(100, 13)
        Me.lblTipodocumento.TabIndex = 28
        Me.lblTipodocumento.Text = "T. Documento:"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.btnRefrescar)
        Me.GroupBox7.Controls.Add(Me.lblOtro)
        Me.GroupBox7.Controls.Add(Me.cboGirado)
        Me.GroupBox7.Controls.Add(Me.BeLabel16)
        Me.GroupBox7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox7.Location = New System.Drawing.Point(12, 179)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(486, 73)
        Me.GroupBox7.TabIndex = 26
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "A NOMBRE DE QUIEN SALE EL EFECTIVO O TRANSFERENCIA"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Grid_properties_
        Me.btnRefrescar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnRefrescar.Location = New System.Drawing.Point(393, 31)
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(18, 21)
        Me.btnRefrescar.TabIndex = 23
        Me.btnRefrescar.UseVisualStyleBackColor = True
        '
        'lblOtro
        '
        Me.lblOtro.AutoSize = True
        Me.lblOtro.Location = New System.Drawing.Point(413, 38)
        Me.lblOtro.Name = "lblOtro"
        Me.lblOtro.Size = New System.Drawing.Size(60, 13)
        Me.lblOtro.TabIndex = 22
        Me.lblOtro.TabStop = True
        Me.lblOtro.Text = "Nuevo..."
        '
        'cboGirado
        '
        Me.cboGirado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboGirado.BackColor = System.Drawing.Color.Ivory
        Me.cboGirado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGirado.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGirado.ForeColor = System.Drawing.Color.Black
        Me.cboGirado.FormattingEnabled = True
        Me.cboGirado.KeyEnter = True
        Me.cboGirado.Location = New System.Drawing.Point(120, 31)
        Me.cboGirado.Name = "cboGirado"
        Me.cboGirado.Size = New System.Drawing.Size(267, 21)
        Me.cboGirado.TabIndex = 21
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(16, 34)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(61, 13)
        Me.BeLabel16.TabIndex = 20
        Me.BeLabel16.Text = "Girado a:"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.cboSolicitante)
        Me.GroupBox6.Controls.Add(Me.BeLabel3)
        Me.GroupBox6.Controls.Add(Me.BeLabel4)
        Me.GroupBox6.Controls.Add(Me.cboObraArea)
        Me.GroupBox6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(12, 107)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(486, 69)
        Me.GroupBox6.TabIndex = 27
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "QUIEN SOLICITA EL DINERO"
        '
        'cboSolicitante
        '
        Me.cboSolicitante.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboSolicitante.BackColor = System.Drawing.Color.Ivory
        Me.cboSolicitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSolicitante.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSolicitante.ForeColor = System.Drawing.Color.Black
        Me.cboSolicitante.FormattingEnabled = True
        Me.cboSolicitante.KeyEnter = True
        Me.cboSolicitante.Location = New System.Drawing.Point(120, 19)
        Me.cboSolicitante.Name = "cboSolicitante"
        Me.cboSolicitante.Size = New System.Drawing.Size(360, 21)
        Me.cboSolicitante.TabIndex = 21
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(8, 22)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(70, 13)
        Me.BeLabel3.TabIndex = 20
        Me.BeLabel3.Text = "Solicitante:"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(4, 46)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(110, 13)
        Me.BeLabel4.TabIndex = 20
        Me.BeLabel4.Text = "Centro de Costos:"
        '
        'cboObraArea
        '
        Me.cboObraArea.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObraArea.BackColor = System.Drawing.Color.Ivory
        Me.cboObraArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObraArea.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.cboObraArea.ForeColor = System.Drawing.Color.Black
        Me.cboObraArea.FormattingEnabled = True
        Me.cboObraArea.KeyEnter = True
        Me.cboObraArea.Location = New System.Drawing.Point(120, 43)
        Me.cboObraArea.Name = "cboObraArea"
        Me.cboObraArea.Size = New System.Drawing.Size(360, 21)
        Me.cboObraArea.TabIndex = 18
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cboAutoriza)
        Me.GroupBox5.Controls.Add(Me.BeLabel17)
        Me.GroupBox5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(12, 55)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(486, 45)
        Me.GroupBox5.TabIndex = 26
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "QUIEN AUTORIZA EL REQUERIMIENTO"
        '
        'cboAutoriza
        '
        Me.cboAutoriza.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAutoriza.BackColor = System.Drawing.Color.Ivory
        Me.cboAutoriza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAutoriza.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAutoriza.ForeColor = System.Drawing.Color.Black
        Me.cboAutoriza.FormattingEnabled = True
        Me.cboAutoriza.KeyEnter = True
        Me.cboAutoriza.Location = New System.Drawing.Point(120, 17)
        Me.cboAutoriza.Name = "cboAutoriza"
        Me.cboAutoriza.Size = New System.Drawing.Size(360, 21)
        Me.cboAutoriza.TabIndex = 21
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(16, 20)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(59, 13)
        Me.BeLabel17.TabIndex = 20
        Me.BeLabel17.Text = "Autoriza:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.BeLabel6)
        Me.GroupBox3.Controls.Add(Me.txtMonto)
        Me.GroupBox3.Controls.Add(Me.cboMoneda)
        Me.GroupBox3.Controls.Add(Me.BeLabel5)
        Me.GroupBox3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(611, 150)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(215, 85)
        Me.GroupBox3.TabIndex = 24
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Cantidad"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(9, 60)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(50, 13)
        Me.BeLabel6.TabIndex = 20
        Me.BeLabel6.Text = "Monto:"
        '
        'txtMonto
        '
        Me.txtMonto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMonto.BackColor = System.Drawing.Color.Ivory
        Me.txtMonto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.txtMonto.ForeColor = System.Drawing.Color.Black
        Me.txtMonto.KeyEnter = True
        Me.txtMonto.Location = New System.Drawing.Point(85, 57)
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMonto.ShortcutsEnabled = False
        Me.txtMonto.Size = New System.Drawing.Size(121, 21)
        Me.txtMonto.TabIndex = 31
        Me.txtMonto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(85, 20)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(121, 21)
        Me.cboMoneda.TabIndex = 18
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(9, 23)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(61, 13)
        Me.BeLabel5.TabIndex = 20
        Me.BeLabel5.Text = "Moneda:"
        '
        'cboArea
        '
        Me.cboArea.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboArea.BackColor = System.Drawing.Color.Ivory
        Me.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArea.ForeColor = System.Drawing.Color.Black
        Me.cboArea.FormattingEnabled = True
        Me.cboArea.KeyEnter = True
        Me.cboArea.Location = New System.Drawing.Point(613, 26)
        Me.cboArea.Name = "cboArea"
        Me.cboArea.Size = New System.Drawing.Size(238, 21)
        Me.cboArea.TabIndex = 25
        '
        'cboEstado
        '
        Me.cboEstado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstado.BackColor = System.Drawing.Color.Ivory
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.Enabled = False
        Me.cboEstado.ForeColor = System.Drawing.Color.Black
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.KeyEnter = True
        Me.cboEstado.Location = New System.Drawing.Point(613, 93)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(104, 21)
        Me.cboEstado.TabIndex = 18
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(507, 96)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel7.TabIndex = 20
        Me.BeLabel7.Text = "Estado:"
        '
        'cboEmpresa
        '
        Me.cboEmpresa.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEmpresa.BackColor = System.Drawing.Color.Ivory
        Me.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpresa.ForeColor = System.Drawing.Color.Black
        Me.cboEmpresa.FormattingEnabled = True
        Me.cboEmpresa.KeyEnter = True
        Me.cboEmpresa.Location = New System.Drawing.Point(232, 24)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(239, 21)
        Me.cboEmpresa.TabIndex = 18
        '
        'dtpFecha
        '
        Me.dtpFecha.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFecha.CustomFormat = ""
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.KeyEnter = True
        Me.dtpFecha.Location = New System.Drawing.Point(613, 60)
        Me.dtpFecha.MinDate = New Date(2014, 1, 1, 0, 0, 0, 0)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(104, 21)
        Me.dtpFecha.TabIndex = 22
        Me.dtpFecha.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFecha.Value = New Date(2014, 1, 1, 0, 0, 0, 0)
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.Cornsilk
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(108, 24)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(46, 21)
        Me.txtCodigo.TabIndex = 19
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(507, 66)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel8.TabIndex = 20
        Me.BeLabel8.Text = "F. Emisión:"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(507, 29)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel15.TabIndex = 20
        Me.BeLabel15.Text = "Area:"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(9, 27)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(93, 13)
        Me.BeLabel1.TabIndex = 20
        Me.BeLabel1.Text = "Nro. Entrega:"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(160, 27)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(68, 13)
        Me.BeLabel2.TabIndex = 20
        Me.BeLabel2.Text = "Empresa:"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(8, 302)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel9.TabIndex = 20
        Me.BeLabel9.Text = "Glosa:"
        '
        'tpBusqueda
        '
        Me.tpBusqueda.BackColor = System.Drawing.Color.LightSteelBlue
        Me.tpBusqueda.Controls.Add(Me.GroupBox1)
        Me.tpBusqueda.Location = New System.Drawing.Point(4, 22)
        Me.tpBusqueda.Name = "tpBusqueda"
        Me.tpBusqueda.Padding = New System.Windows.Forms.Padding(3)
        Me.tpBusqueda.Size = New System.Drawing.Size(1016, 439)
        Me.tpBusqueda.TabIndex = 1
        Me.tpBusqueda.Text = "Busqueda"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvEntregaRendir)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1011, 433)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Entrega a Rendir"
        '
        'dgvEntregaRendir
        '
        Me.dgvEntregaRendir.EnableHotTracking = False
        Me.dgvEntregaRendir.Location = New System.Drawing.Point(6, 108)
        '
        'dgvEntregaRendir
        '
        Me.dgvEntregaRendir.MasterTemplate.AllowAddNewRow = False
        Me.dgvEntregaRendir.MasterTemplate.AllowColumnReorder = False
        Me.dgvEntregaRendir.MasterTemplate.AllowDeleteRow = False
        GridViewTextBoxColumn1.FieldName = "Nro"
        GridViewTextBoxColumn1.HeaderText = "Nro"
        GridViewTextBoxColumn1.Name = "Nro"
        GridViewTextBoxColumn1.ReadOnly = True
        GridViewTextBoxColumn1.Width = 35
        GridViewTextBoxColumn2.FieldName = "Fecha"
        GridViewTextBoxColumn2.HeaderText = "Fecha"
        GridViewTextBoxColumn2.Name = "Fecha"
        GridViewTextBoxColumn2.ReadOnly = True
        GridViewTextBoxColumn2.Width = 95
        GridViewTextBoxColumn3.FieldName = "Estado"
        GridViewTextBoxColumn3.HeaderText = "Estado"
        GridViewTextBoxColumn3.Name = "Estado"
        GridViewTextBoxColumn3.ReadOnly = True
        GridViewTextBoxColumn3.Width = 75
        GridViewTextBoxColumn4.FieldName = "EmprDescripcion"
        GridViewTextBoxColumn4.HeaderText = "Empresa"
        GridViewTextBoxColumn4.Name = "EmprDescripcion"
        GridViewTextBoxColumn4.ReadOnly = True
        GridViewTextBoxColumn4.Width = 300
        GridViewTextBoxColumn5.FieldName = "Departamento"
        GridViewTextBoxColumn5.HeaderText = "Area"
        GridViewTextBoxColumn5.Name = "Departamento"
        GridViewTextBoxColumn5.Width = 200
        GridViewTextBoxColumn6.FieldName = "Obra"
        GridViewTextBoxColumn6.HeaderText = "Centro de Costo"
        GridViewTextBoxColumn6.Name = "Obra"
        GridViewTextBoxColumn6.ReadOnly = True
        GridViewTextBoxColumn6.Width = 220
        GridViewTextBoxColumn7.FieldName = "MonDescripcion"
        GridViewTextBoxColumn7.HeaderText = "Moneda"
        GridViewTextBoxColumn7.Name = "MonDescripcion"
        GridViewTextBoxColumn7.ReadOnly = True
        GridViewTextBoxColumn7.Width = 110
        GridViewTextBoxColumn8.FieldName = "Monto"
        GridViewTextBoxColumn8.HeaderText = "Monto"
        GridViewTextBoxColumn8.Name = "Monto"
        GridViewTextBoxColumn8.ReadOnly = True
        GridViewTextBoxColumn8.Width = 85
        GridViewTextBoxColumn9.FieldName = "Concepto"
        GridViewTextBoxColumn9.HeaderText = "Glosa"
        GridViewTextBoxColumn9.Name = "Concepto"
        GridViewTextBoxColumn9.ReadOnly = True
        GridViewTextBoxColumn9.Width = 500
        GridViewTextBoxColumn10.FieldName = "Solicitante"
        GridViewTextBoxColumn10.HeaderText = "Solicitante"
        GridViewTextBoxColumn10.Name = "Solicitante"
        GridViewTextBoxColumn10.ReadOnly = True
        GridViewTextBoxColumn10.Width = 220
        GridViewTextBoxColumn11.FieldName = "Girado"
        GridViewTextBoxColumn11.HeaderText = "Girado"
        GridViewTextBoxColumn11.Name = "Girado"
        GridViewTextBoxColumn11.ReadOnly = True
        GridViewTextBoxColumn11.Width = 220
        GridViewTextBoxColumn12.FieldName = "Autoriza"
        GridViewTextBoxColumn12.HeaderText = "Autoriza"
        GridViewTextBoxColumn12.Name = "Autoriza"
        GridViewTextBoxColumn12.ReadOnly = True
        GridViewTextBoxColumn12.Width = 220
        GridViewTextBoxColumn13.FieldName = "REVISION"
        GridViewTextBoxColumn13.HeaderText = "REVISION"
        GridViewTextBoxColumn13.IsVisible = False
        GridViewTextBoxColumn13.Name = "REVISION"
        GridViewTextBoxColumn13.Width = 100
        GridViewTextBoxColumn14.FieldName = "Saldo"
        GridViewTextBoxColumn14.HeaderText = "Saldo"
        GridViewTextBoxColumn14.IsVisible = False
        GridViewTextBoxColumn14.Name = "Saldo"
        GridViewTextBoxColumn14.Width = 100
        GridViewTextBoxColumn15.FieldName = "EnProceso"
        GridViewTextBoxColumn15.HeaderText = "EnProceso"
        GridViewTextBoxColumn15.IsVisible = False
        GridViewTextBoxColumn15.Name = "EnProceso"
        GridViewTextBoxColumn15.Width = 100
        GridViewTextBoxColumn16.FieldName = "EmprCodigo"
        GridViewTextBoxColumn16.HeaderText = "EmprCodigo"
        GridViewTextBoxColumn16.IsVisible = False
        GridViewTextBoxColumn16.Name = "EmprCodigo"
        GridViewTextBoxColumn17.FieldName = "SolCodigo"
        GridViewTextBoxColumn17.HeaderText = "SolCodigo"
        GridViewTextBoxColumn17.IsVisible = False
        GridViewTextBoxColumn17.Name = "SolCodigo"
        GridViewTextBoxColumn18.FieldName = "AutCodigo"
        GridViewTextBoxColumn18.HeaderText = "AutCodigo"
        GridViewTextBoxColumn18.IsVisible = False
        GridViewTextBoxColumn18.Name = "AutCodigo"
        GridViewTextBoxColumn19.FieldName = "GirCodigo"
        GridViewTextBoxColumn19.HeaderText = "GirCodigo"
        GridViewTextBoxColumn19.IsVisible = False
        GridViewTextBoxColumn19.Name = "GirCodigo"
        GridViewTextBoxColumn20.FieldName = "ObraAreaCodigo"
        GridViewTextBoxColumn20.HeaderText = "ObraAreaCodigo"
        GridViewTextBoxColumn20.IsVisible = False
        GridViewTextBoxColumn20.Name = "ObraAreaCodigo"
        GridViewTextBoxColumn21.FieldName = "MonCodigo"
        GridViewTextBoxColumn21.HeaderText = "MonCodigo"
        GridViewTextBoxColumn21.IsVisible = False
        GridViewTextBoxColumn21.Name = "MonCodigo"
        GridViewTextBoxColumn22.FieldName = "ID_ESTADO"
        GridViewTextBoxColumn22.HeaderText = "ID_ESTADO"
        GridViewTextBoxColumn22.IsVisible = False
        GridViewTextBoxColumn22.Name = "ID_ESTADO"
        GridViewTextBoxColumn23.FieldName = "AreaCodigo"
        GridViewTextBoxColumn23.HeaderText = "AreaCodigo"
        GridViewTextBoxColumn23.IsVisible = False
        GridViewTextBoxColumn23.Name = "AreaCodigo"
        GridViewTextBoxColumn24.FieldName = "SolTPla"
        GridViewTextBoxColumn24.HeaderText = "SolTPla"
        GridViewTextBoxColumn24.IsVisible = False
        GridViewTextBoxColumn24.Name = "SolTPla"
        GridViewTextBoxColumn25.FieldName = "AutTPla"
        GridViewTextBoxColumn25.HeaderText = "AutTPla"
        GridViewTextBoxColumn25.IsVisible = False
        GridViewTextBoxColumn25.Name = "AutTPla"
        GridViewTextBoxColumn26.FieldName = "GirTPla"
        GridViewTextBoxColumn26.HeaderText = "GirTPla"
        GridViewTextBoxColumn26.IsVisible = False
        GridViewTextBoxColumn26.Name = "GirTPla"
        GridViewTextBoxColumn27.FieldName = "ObrEmprCodigo"
        GridViewTextBoxColumn27.HeaderText = "ObrEmprCodigo"
        GridViewTextBoxColumn27.IsVisible = False
        GridViewTextBoxColumn27.Name = "ObrEmprCodigo"
        GridViewTextBoxColumn28.FieldName = "DocOrigen"
        GridViewTextBoxColumn28.HeaderText = "DocOrigen"
        GridViewTextBoxColumn28.IsVisible = False
        GridViewTextBoxColumn28.Name = "DocOrigen"
        GridViewTextBoxColumn29.FieldName = "ID_DocPendiente"
        GridViewTextBoxColumn29.HeaderText = "ID_DocPendiente"
        GridViewTextBoxColumn29.IsVisible = False
        GridViewTextBoxColumn29.Name = "ID_DocPendiente"
        GridViewTextBoxColumn30.FieldName = "ID_DocOrigen"
        GridViewTextBoxColumn30.HeaderText = "ID_DocOrigen"
        GridViewTextBoxColumn30.IsVisible = False
        GridViewTextBoxColumn30.Name = "ID_DocOrigen"
        Me.dgvEntregaRendir.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8, GridViewTextBoxColumn9, GridViewTextBoxColumn10, GridViewTextBoxColumn11, GridViewTextBoxColumn12, GridViewTextBoxColumn13, GridViewTextBoxColumn14, GridViewTextBoxColumn15, GridViewTextBoxColumn16, GridViewTextBoxColumn17, GridViewTextBoxColumn18, GridViewTextBoxColumn19, GridViewTextBoxColumn20, GridViewTextBoxColumn21, GridViewTextBoxColumn22, GridViewTextBoxColumn23, GridViewTextBoxColumn24, GridViewTextBoxColumn25, GridViewTextBoxColumn26, GridViewTextBoxColumn27, GridViewTextBoxColumn28, GridViewTextBoxColumn29, GridViewTextBoxColumn30})
        Me.dgvEntregaRendir.MasterTemplate.EnableFiltering = True
        Me.dgvEntregaRendir.MasterTemplate.EnableGrouping = False
        Me.dgvEntregaRendir.MasterTemplate.EnableSorting = False
        Me.dgvEntregaRendir.Name = "dgvEntregaRendir"
        Me.dgvEntregaRendir.Size = New System.Drawing.Size(996, 311)
        Me.dgvEntregaRendir.StandardTab = True
        Me.dgvEntregaRendir.TabIndex = 8
        Me.dgvEntregaRendir.Text = "RadGridView1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboRBSol)
        Me.GroupBox2.Controls.Add(Me.cboBSolicitante)
        Me.GroupBox2.Controls.Add(Me.dtpFin)
        Me.GroupBox2.Controls.Add(Me.dtpInicio)
        Me.GroupBox2.Controls.Add(Me.cboBEmpresa)
        Me.GroupBox2.Controls.Add(Me.BeLabel14)
        Me.GroupBox2.Controls.Add(Me.BeLabel10)
        Me.GroupBox2.Controls.Add(Me.BeLabel13)
        Me.GroupBox2.Controls.Add(Me.BeLabel11)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(996, 86)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Búsqueda:"
        '
        'cboRBSol
        '
        Me.cboRBSol.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboRBSol.BackColor = System.Drawing.Color.Ivory
        Me.cboRBSol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRBSol.ForeColor = System.Drawing.Color.Black
        Me.cboRBSol.FormattingEnabled = True
        Me.cboRBSol.KeyEnter = True
        Me.cboRBSol.Location = New System.Drawing.Point(414, 59)
        Me.cboRBSol.Name = "cboRBSol"
        Me.cboRBSol.Size = New System.Drawing.Size(121, 21)
        Me.cboRBSol.TabIndex = 9
        Me.cboRBSol.Visible = False
        '
        'cboBSolicitante
        '
        Me.cboBSolicitante.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBSolicitante.BackColor = System.Drawing.Color.Ivory
        Me.cboBSolicitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBSolicitante.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.cboBSolicitante.ForeColor = System.Drawing.Color.Black
        Me.cboBSolicitante.FormattingEnabled = True
        Me.cboBSolicitante.KeyEnter = True
        Me.cboBSolicitante.Location = New System.Drawing.Point(112, 59)
        Me.cboBSolicitante.Name = "cboBSolicitante"
        Me.cboBSolicitante.Size = New System.Drawing.Size(259, 21)
        Me.cboBSolicitante.TabIndex = 8
        Me.cboBSolicitante.Visible = False
        '
        'dtpFin
        '
        Me.dtpFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFin.CalendarFont = New System.Drawing.Font("Verdana", 8.25!)
        Me.dtpFin.CustomFormat = ""
        Me.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFin.KeyEnter = True
        Me.dtpFin.Location = New System.Drawing.Point(325, 20)
        Me.dtpFin.Name = "dtpFin"
        Me.dtpFin.Size = New System.Drawing.Size(107, 21)
        Me.dtpFin.TabIndex = 7
        Me.dtpFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpInicio
        '
        Me.dtpInicio.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpInicio.CalendarFont = New System.Drawing.Font("Verdana", 8.25!)
        Me.dtpInicio.CustomFormat = ""
        Me.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInicio.KeyEnter = True
        Me.dtpInicio.Location = New System.Drawing.Point(112, 20)
        Me.dtpInicio.Name = "dtpInicio"
        Me.dtpInicio.Size = New System.Drawing.Size(107, 21)
        Me.dtpInicio.TabIndex = 7
        Me.dtpInicio.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpInicio.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'cboBEmpresa
        '
        Me.cboBEmpresa.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBEmpresa.BackColor = System.Drawing.Color.Ivory
        Me.cboBEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBEmpresa.Font = New System.Drawing.Font("Verdana", 8.25!)
        Me.cboBEmpresa.ForeColor = System.Drawing.Color.Black
        Me.cboBEmpresa.FormattingEnabled = True
        Me.cboBEmpresa.KeyEnter = True
        Me.cboBEmpresa.Location = New System.Drawing.Point(563, 23)
        Me.cboBEmpresa.Name = "cboBEmpresa"
        Me.cboBEmpresa.Size = New System.Drawing.Size(267, 21)
        Me.cboBEmpresa.TabIndex = 6
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(239, 26)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(74, 13)
        Me.BeLabel14.TabIndex = 2
        Me.BeLabel14.Text = "Fecha Fin:"
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(15, 26)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(91, 13)
        Me.BeLabel10.TabIndex = 2
        Me.BeLabel10.Text = "Fecha Inicio:"
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(26, 62)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(80, 13)
        Me.BeLabel13.TabIndex = 5
        Me.BeLabel13.Text = "Solicitante:"
        Me.BeLabel13.Visible = False
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(467, 26)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(68, 13)
        Me.BeLabel11.TabIndex = 3
        Me.BeLabel11.Text = "Empresa:"
        '
        'Menu
        '
        Me.Menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmEliminar})
        Me.Menu.Name = "Menu"
        Me.Menu.Size = New System.Drawing.Size(118, 26)
        Me.Menu.Text = "Menu:"
        '
        'tsmEliminar
        '
        Me.tsmEliminar.Name = "tsmEliminar"
        Me.tsmEliminar.Size = New System.Drawing.Size(117, 22)
        Me.tsmEliminar.Text = "Eliminar"
        '
        'tsMenu
        '
        Me.tsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Grabar, Me.Editar, Me.Buscar, Me.Eliminar, Me.Salir})
        Me.tsMenu.Location = New System.Drawing.Point(0, 0)
        Me.tsMenu.Name = "tsMenu"
        Me.tsMenu.Size = New System.Drawing.Size(1017, 25)
        Me.tsMenu.TabIndex = 5
        Me.tsMenu.Text = "ToolStrip1"
        '
        'Grabar
        '
        Me.Grabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Grabar.Image = CType(resources.GetObject("Grabar.Image"), System.Drawing.Image)
        Me.Grabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Grabar.Name = "Grabar"
        Me.Grabar.Size = New System.Drawing.Size(23, 22)
        Me.Grabar.Text = "&Grabar"
        Me.Grabar.ToolTipText = "Grabar"
        '
        'Editar
        '
        Me.Editar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Editar.Enabled = False
        Me.Editar.Image = CType(resources.GetObject("Editar.Image"), System.Drawing.Image)
        Me.Editar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Editar.Name = "Editar"
        Me.Editar.Size = New System.Drawing.Size(23, 22)
        Me.Editar.Text = "&Modificar"
        Me.Editar.ToolTipText = "Editar"
        '
        'Buscar
        '
        Me.Buscar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Buscar.Enabled = False
        Me.Buscar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_
        Me.Buscar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Buscar.Name = "Buscar"
        Me.Buscar.Size = New System.Drawing.Size(23, 22)
        Me.Buscar.Text = "&Buscar"
        '
        'Eliminar
        '
        Me.Eliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Eliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.Eliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.Size = New System.Drawing.Size(23, 22)
        Me.Eliminar.Text = "Eliminar"
        Me.Eliminar.ToolTipText = "Eliminar"
        '
        'Salir
        '
        Me.Salir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.Salir.Image = CType(resources.GetObject("Salir.Image"), System.Drawing.Image)
        Me.Salir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Salir.Name = "Salir"
        Me.Salir.Size = New System.Drawing.Size(23, 22)
        Me.Salir.Text = "&Salir"
        Me.Salir.ToolTipText = "Salir"
        '
        'frmEntregaRendir
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 484)
        Me.Controls.Add(Me.tsMenu)
        Me.Controls.Add(Me.TabControl2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmEntregaRendir"
        Me.Text = "Entrega a Rendir"
        Me.TabControl2.ResumeLayout(False)
        Me.tpCuentaRendir.ResumeLayout(False)
        Me.gbCuntasRendir.ResumeLayout(False)
        Me.gbCuntasRendir.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.tpBusqueda.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvEntregaRendir.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEntregaRendir, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Menu.ResumeLayout(False)
        Me.tsMenu.ResumeLayout(False)
        Me.tsMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout

End Sub
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents tpCuentaRendir As System.Windows.Forms.TabPage
    Friend WithEvents gbCuntasRendir As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents cboGirado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboObraArea As ctrLibreria.Controles.BeComboBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboArea As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboEstado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEmpresa As ctrLibreria.Controles.BeComboBox
    Friend WithEvents dtpFecha As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents tpBusqueda As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpInicio As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents cboBEmpresa As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents tsMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents Grabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Editar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Salir As System.Windows.Forms.ToolStripButton
    Friend WithEvents Buscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents cboRBSol As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboBSolicitante As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtGlosa As System.Windows.Forms.TextBox
    Friend WithEvents cboSolicitante As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtMonto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboTipoDocumento As ctrLibreria.Controles.BeComboBox
    Friend WithEvents lblTipodocumento As System.Windows.Forms.Label
    Friend WithEvents lblOtro As System.Windows.Forms.LinkLabel
    Friend WithEvents cboRObra As System.Windows.Forms.ComboBox
    Friend WithEvents btnRefrescar As System.Windows.Forms.Button
    Friend WithEvents Eliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Menu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmEliminar As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents cboAutoriza As ctrLibreria.Controles.BeComboBox
    Friend WithEvents dgvEntregaRendir As Telerik.WinControls.UI.RadGridView
End Class
