﻿Imports CapaNegocios
Imports CapaDatos
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports IWshRuntimeLibrary
Imports CapaEntidad
Imports BusinessLogicLayer.Mantenimiento

Public Class frmVistoBuenoUsuario

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmVistoBuenoUsuario = Nothing

    Public Shared Function Instance() As frmVistoBuenoUsuario
        If frmInstance Is Nothing Then
            frmInstance = New frmVistoBuenoUsuario
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function


    Private Sub frmProgramacionPago_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub



#End Region

    Dim idusariocorre As Decimal
    Dim ID_VistoBueno As Decimal
    Dim permiso As String
    Private Sub ListarUsuarios()
        Dim VL_SrvVistoBueno As New SrvVistoBueno()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvVistoBueno.Fnc_Listar_UsuariosVB(1, 100)

            If VL_BeanResultado.blnExiste = True Then
                dgvUsuario.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub ListarAreaVB()
        Dim VL_SrvVistoBueno As New SrvVistoBueno()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvVistoBueno.Fnc_Listar_UsuariosVB(5, 110)

            If VL_BeanResultado.blnExiste = True Then
                cboArea.DataSource = VL_BeanResultado.dtResultado
                cboArea.DisplayMember = "VistoBueno"
                cboArea.ValueMember = "ID_VistoBueno"
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub ListarAprobacion(ByVal VL_ID_USUARIO As Decimal)
        Dim VL_SrvVistoBueno As New SrvVistoBueno()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvVistoBueno.Fnc_Listar_UsuariosVB(3, VL_ID_USUARIO)

            If VL_BeanResultado.blnExiste = True Then
                permiso = VL_BeanResultado.dtResultado.Rows(0).Item(0)
                If permiso = "SI" Then
                    cboPermiso.SelectedIndex = 0
                ElseIf permiso = "NO" Then
                    cboPermiso.SelectedIndex = 1
                End If
                'cboArea.ValueMember = "ID_VistoBueno"
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub InsertarUsuario(ByVal VL_VISTOBUENO As Decimal, ByVal VL_ID_USUARIO As Decimal)
        Dim VL_SrvVistoBueno As New SrvVistoBueno()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SrvVistoBueno.Fnc_InsertarVarios_VB(1, VL_VISTOBUENO, VL_ID_USUARIO, "1", 1)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
       
    End Sub

    Private Sub frmVistoBuenoUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListarUsuarios()
        ListarAreaVB()
        cboPermiso.SelectedIndex = 0
    End Sub


    Private Sub dgvUsuario_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles dgvUsuario.CellDoubleClick

        tbVistoBueno.TabIndex.Equals(True)
        tbVistoBueno.TabIndex.Equals(False)
        tbVistoBueno.SelectedTab = PermisoUsuario

        txtNombre.Text = dgvUsuario.CurrentRow.Cells("UsuNombre").Value
        txtUsuario.Text = dgvUsuario.CurrentRow.Cells("UsuCodigo").Value
        txtEmpresa.Text = dgvUsuario.CurrentRow.Cells("EmprDescripcion").Value
        idusariocorre = dgvUsuario.CurrentRow.Cells("idUsuarioCorre").Value
        cboArea.SelectedValue = dgvUsuario.CurrentRow.Cells("ID_VistoBueno").Value
        ListarAprobacion(idusariocorre)



    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click

        idusariocorre = dgvUsuario.CurrentRow.Cells("idUsuarioCorre").Value


        'If cboPermiso.SelectedIndex = 0 Then
        '    MessageBox.Show("Usuario ya tiene permisos")
        'ElseIf cboPermiso.SelectedIndex = 1 Then
        InsertarUsuario(cboArea.SelectedValue, idusariocorre)
        'End If

    End Sub
End Class