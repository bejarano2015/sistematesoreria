﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProgramacionPago
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn26 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn27 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn28 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn29 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn30 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn31 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn32 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn33 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn34 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn35 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn36 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn37 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn38 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn39 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn40 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn41 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn42 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn43 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn44 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn45 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn46 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn47 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn48 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn49 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn50 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tsGuardar = New System.Windows.Forms.ToolStripButton()
        Me.tsCancelar = New System.Windows.Forms.ToolStripButton()
        Me.tsbReporte = New System.Windows.Forms.ToolStripButton()
        Me.tsdReportes = New System.Windows.Forms.ToolStripDropDownButton()
        Me.ReporteProgramaciónDePagosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvListaPP1 = New Telerik.WinControls.UI.RadGridView()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpVigencia = New System.Windows.Forms.DateTimePicker()
        Me.dtpCreacion = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboEstado = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboMoneda = New System.Windows.Forms.ComboBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.txtIdPP = New System.Windows.Forms.TextBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.dgvPPD = New System.Windows.Forms.DataGridView()
        Me.ID_PROPAGO_DET = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OcoTipoCompServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PDID_PROGPAGO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FormaPago = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ID_DOC_ORIGEN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCRIPCION = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SERIE_DOC_ORIGEN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NRO_DOC_ORIGEN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PDID_MONEDA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PDMonDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IMPORTE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PDID_ESTADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DES_ESTADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Glosa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SolicitadoA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ID_DocPendiente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Saldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amortizado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcentajePercepcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PorcentajeRetDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TOTALBRUTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Menu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Eliminar = New System.Windows.Forms.ToolStripMenuItem()
        Me.rpgPrueba = New Telerik.WinControls.UI.RadPageView()
        Me.ListarPendiente = New Telerik.WinControls.UI.RadPageViewPage()
        Me.Propiedades = New Telerik.WinControls.UI.RadPageViewPage()
        Me.ListarProgramado = New Telerik.WinControls.UI.RadPageViewPage()
        Me.dgvProgramado = New Telerik.WinControls.UI.RadGridView()
        Me.dgvListaPP = New Telerik.WinControls.UI.MasterGridViewTemplate()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtpFin = New System.Windows.Forms.DateTimePicker()
        Me.dtpInicio = New System.Windows.Forms.DateTimePicker()
        Me.ToolStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvListaPP1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvListaPP1.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        CType(Me.dgvPPD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Menu.SuspendLayout()
        CType(Me.rpgPrueba, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.rpgPrueba.SuspendLayout()
        Me.ListarPendiente.SuspendLayout()
        Me.Propiedades.SuspendLayout()
        Me.ListarProgramado.SuspendLayout()
        CType(Me.dgvProgramado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvProgramado.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvListaPP, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsGuardar, Me.tsCancelar, Me.tsbReporte, Me.tsdReportes})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(609, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tsGuardar
        '
        Me.tsGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsGuardar.Enabled = False
        Me.tsGuardar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.tsGuardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsGuardar.Name = "tsGuardar"
        Me.tsGuardar.Size = New System.Drawing.Size(23, 22)
        Me.tsGuardar.Text = "Guardar"
        '
        'tsCancelar
        '
        Me.tsCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsCancelar.Enabled = False
        Me.tsCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.tsCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsCancelar.Name = "tsCancelar"
        Me.tsCancelar.Size = New System.Drawing.Size(23, 22)
        Me.tsCancelar.Text = "Cancelar"
        '
        'tsbReporte
        '
        Me.tsbReporte.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbReporte.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Print_
        Me.tsbReporte.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbReporte.Name = "tsbReporte"
        Me.tsbReporte.Size = New System.Drawing.Size(23, 22)
        Me.tsbReporte.Text = "ToolStripButton1"
        Me.tsbReporte.Visible = False
        '
        'tsdReportes
        '
        Me.tsdReportes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsdReportes.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReporteProgramaciónDePagosToolStripMenuItem})
        Me.tsdReportes.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Edit_
        Me.tsdReportes.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsdReportes.Name = "tsdReportes"
        Me.tsdReportes.Size = New System.Drawing.Size(29, 22)
        Me.tsdReportes.Tag = "Reportes"
        Me.tsdReportes.Text = "Reportes"
        Me.tsdReportes.Visible = False
        '
        'ReporteProgramaciónDePagosToolStripMenuItem
        '
        Me.ReporteProgramaciónDePagosToolStripMenuItem.Name = "ReporteProgramaciónDePagosToolStripMenuItem"
        Me.ReporteProgramaciónDePagosToolStripMenuItem.Size = New System.Drawing.Size(277, 22)
        Me.ReporteProgramaciónDePagosToolStripMenuItem.Text = "Reporte Total Programación de Pagos "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvListaPP1)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(584, 396)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'dgvListaPP1
        '
        Me.dgvListaPP1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvListaPP1.EnableHotTracking = False
        Me.dgvListaPP1.Location = New System.Drawing.Point(3, 18)
        '
        'dgvListaPP1
        '
        Me.dgvListaPP1.MasterTemplate.AllowAddNewRow = False
        Me.dgvListaPP1.MasterTemplate.AllowColumnReorder = False
        Me.dgvListaPP1.MasterTemplate.AllowDeleteRow = False
        GridViewTextBoxColumn26.FieldName = "ID_PROGPAGO"
        GridViewTextBoxColumn26.HeaderText = "Nro"
        GridViewTextBoxColumn26.Name = "ID_PROGPAGO"
        GridViewTextBoxColumn26.ReadOnly = True
        GridViewTextBoxColumn26.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn26.Width = 65
        GridViewTextBoxColumn27.FieldName = "FECHA"
        GridViewTextBoxColumn27.HeaderText = "Fecha Registro"
        GridViewTextBoxColumn27.IsVisible = False
        GridViewTextBoxColumn27.Name = "FECHA"
        GridViewTextBoxColumn27.ReadOnly = True
        GridViewTextBoxColumn27.Width = 100
        GridViewTextBoxColumn28.FieldName = "VIGENCIA"
        GridViewTextBoxColumn28.HeaderText = "Fecha de Pago"
        GridViewTextBoxColumn28.Name = "VIGENCIA"
        GridViewTextBoxColumn28.ReadOnly = True
        GridViewTextBoxColumn28.Width = 100
        GridViewTextBoxColumn29.FieldName = "ID_MONEDA"
        GridViewTextBoxColumn29.HeaderText = "ID_MONEDA"
        GridViewTextBoxColumn29.IsVisible = False
        GridViewTextBoxColumn29.Name = "ID_MONEDA"
        GridViewTextBoxColumn29.ReadOnly = True
        GridViewTextBoxColumn30.FieldName = "MonDescripcion"
        GridViewTextBoxColumn30.HeaderText = "Moneda"
        GridViewTextBoxColumn30.Name = "MonDescripcion"
        GridViewTextBoxColumn30.ReadOnly = True
        GridViewTextBoxColumn30.Width = 100
        GridViewTextBoxColumn31.FieldName = "TOTAL"
        GridViewTextBoxColumn31.HeaderText = "Total"
        GridViewTextBoxColumn31.Name = "TOTAL"
        GridViewTextBoxColumn31.ReadOnly = True
        GridViewTextBoxColumn31.Width = 100
        GridViewTextBoxColumn32.FieldName = "ID_ESTADO"
        GridViewTextBoxColumn32.HeaderText = "ID_ESTADO"
        GridViewTextBoxColumn32.IsVisible = False
        GridViewTextBoxColumn32.Name = "ID_ESTADO"
        GridViewTextBoxColumn32.ReadOnly = True
        GridViewTextBoxColumn33.FieldName = "DESCRIP_ESTADO"
        GridViewTextBoxColumn33.HeaderText = "Estado"
        GridViewTextBoxColumn33.Name = "DESCRIP_ESTADO"
        GridViewTextBoxColumn33.ReadOnly = True
        GridViewTextBoxColumn33.Width = 100
        GridViewTextBoxColumn34.FieldName = "TOTALBRUTO"
        GridViewTextBoxColumn34.HeaderText = "TOTALBRUTO"
        GridViewTextBoxColumn34.IsVisible = False
        GridViewTextBoxColumn34.Name = "TOTALBRUTO"
        Me.dgvListaPP1.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn26, GridViewTextBoxColumn27, GridViewTextBoxColumn28, GridViewTextBoxColumn29, GridViewTextBoxColumn30, GridViewTextBoxColumn31, GridViewTextBoxColumn32, GridViewTextBoxColumn33, GridViewTextBoxColumn34})
        Me.dgvListaPP1.MasterTemplate.EnableFiltering = True
        Me.dgvListaPP1.MasterTemplate.EnableGrouping = False
        Me.dgvListaPP1.MasterTemplate.EnableSorting = False
        Me.dgvListaPP1.Name = "dgvListaPP1"
        Me.dgvListaPP1.Size = New System.Drawing.Size(578, 375)
        Me.dgvListaPP1.StandardTab = True
        Me.dgvListaPP1.TabIndex = 5
        Me.dgvListaPP1.Text = "RadGridView1"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.GroupBox6)
        Me.GroupBox5.Controls.Add(Me.GroupBox7)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(586, 576)
        Me.GroupBox5.TabIndex = 8
        Me.GroupBox5.TabStop = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.GroupBox3)
        Me.GroupBox6.Controls.Add(Me.Label6)
        Me.GroupBox6.Controls.Add(Me.cboEstado)
        Me.GroupBox6.Controls.Add(Me.Label2)
        Me.GroupBox6.Controls.Add(Me.GroupBox4)
        Me.GroupBox6.Controls.Add(Me.txtIdPP)
        Me.GroupBox6.Location = New System.Drawing.Point(6, 12)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(570, 189)
        Me.GroupBox6.TabIndex = 8
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Programación de Pago"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.dtpVigencia)
        Me.GroupBox3.Controls.Add(Me.dtpCreacion)
        Me.GroupBox3.Location = New System.Drawing.Point(208, 19)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(350, 55)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Vigencia"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(200, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Vigencia:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Fecha creación:"
        '
        'dtpVigencia
        '
        Me.dtpVigencia.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpVigencia.Location = New System.Drawing.Point(261, 20)
        Me.dtpVigencia.Name = "dtpVigencia"
        Me.dtpVigencia.Size = New System.Drawing.Size(79, 20)
        Me.dtpVigencia.TabIndex = 4
        '
        'dtpCreacion
        '
        Me.dtpCreacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpCreacion.Location = New System.Drawing.Point(107, 20)
        Me.dtpCreacion.Name = "dtpCreacion"
        Me.dtpCreacion.Size = New System.Drawing.Size(79, 20)
        Me.dtpCreacion.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(257, 131)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Estado:"
        '
        'cboEstado
        '
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.Location = New System.Drawing.Point(308, 128)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(121, 21)
        Me.cboEstado.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(163, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Nro Programación de Pago:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.cboMoneda)
        Me.GroupBox4.Controls.Add(Me.txtTotal)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 91)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(200, 87)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Cantidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(56, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Moneda:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(16, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(40, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Total:"
        '
        'cboMoneda
        '
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.Location = New System.Drawing.Point(73, 21)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(121, 21)
        Me.cboMoneda.TabIndex = 5
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(73, 54)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(121, 20)
        Me.txtTotal.TabIndex = 3
        Me.txtTotal.Text = "0.00"
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtIdPP
        '
        Me.txtIdPP.Location = New System.Drawing.Point(171, 42)
        Me.txtIdPP.Name = "txtIdPP"
        Me.txtIdPP.ReadOnly = True
        Me.txtIdPP.Size = New System.Drawing.Size(28, 20)
        Me.txtIdPP.TabIndex = 3
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.dgvPPD)
        Me.GroupBox7.Location = New System.Drawing.Point(6, 202)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(570, 195)
        Me.GroupBox7.TabIndex = 9
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Programación Pago Detalle"
        '
        'dgvPPD
        '
        Me.dgvPPD.AllowUserToAddRows = False
        Me.dgvPPD.AllowUserToDeleteRows = False
        Me.dgvPPD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPPD.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID_PROPAGO_DET, Me.OcoTipoCompServ, Me.PDID_PROGPAGO, Me.FormaPago, Me.ID_DOC_ORIGEN, Me.DESCRIPCION, Me.SERIE_DOC_ORIGEN, Me.NRO_DOC_ORIGEN, Me.PDID_MONEDA, Me.PDMonDescripcion, Me.IMPORTE, Me.PDID_ESTADO, Me.DES_ESTADO, Me.Glosa, Me.SolicitadoA, Me.ID_DocPendiente, Me.Saldo, Me.Amortizado, Me.PorcentajePercepcion, Me.PorcentajeRetDet, Me.TOTALBRUTO})
        Me.dgvPPD.ContextMenuStrip = Me.Menu
        Me.dgvPPD.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvPPD.Location = New System.Drawing.Point(3, 18)
        Me.dgvPPD.Name = "dgvPPD"
        Me.dgvPPD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPPD.Size = New System.Drawing.Size(564, 174)
        Me.dgvPPD.TabIndex = 0
        '
        'ID_PROPAGO_DET
        '
        Me.ID_PROPAGO_DET.DataPropertyName = "ID_PROPAGO_DET"
        Me.ID_PROPAGO_DET.HeaderText = "Nro"
        Me.ID_PROPAGO_DET.Name = "ID_PROPAGO_DET"
        Me.ID_PROPAGO_DET.ReadOnly = True
        Me.ID_PROPAGO_DET.Width = 35
        '
        'OcoTipoCompServ
        '
        Me.OcoTipoCompServ.DataPropertyName = "OcoTipoCompServ"
        Me.OcoTipoCompServ.HeaderText = "Tipo"
        Me.OcoTipoCompServ.Name = "OcoTipoCompServ"
        Me.OcoTipoCompServ.ReadOnly = True
        Me.OcoTipoCompServ.Visible = False
        '
        'PDID_PROGPAGO
        '
        Me.PDID_PROGPAGO.DataPropertyName = "ID_PROGPAGO"
        Me.PDID_PROGPAGO.HeaderText = "ID_PROGPAGO"
        Me.PDID_PROGPAGO.Name = "PDID_PROGPAGO"
        Me.PDID_PROGPAGO.ReadOnly = True
        Me.PDID_PROGPAGO.Visible = False
        '
        'FormaPago
        '
        Me.FormaPago.DataPropertyName = "ID_FORMA_PAGO"
        Me.FormaPago.HeaderText = "Forma de Pago"
        Me.FormaPago.Name = "FormaPago"
        '
        'ID_DOC_ORIGEN
        '
        Me.ID_DOC_ORIGEN.DataPropertyName = "ID_DOC_ORIGEN"
        Me.ID_DOC_ORIGEN.HeaderText = "ID_DOC_ORIGEN"
        Me.ID_DOC_ORIGEN.Name = "ID_DOC_ORIGEN"
        Me.ID_DOC_ORIGEN.ReadOnly = True
        Me.ID_DOC_ORIGEN.Visible = False
        '
        'DESCRIPCION
        '
        Me.DESCRIPCION.DataPropertyName = "DESCRIPCION"
        Me.DESCRIPCION.HeaderText = "Descripcion"
        Me.DESCRIPCION.Name = "DESCRIPCION"
        Me.DESCRIPCION.ReadOnly = True
        Me.DESCRIPCION.Width = 130
        '
        'SERIE_DOC_ORIGEN
        '
        Me.SERIE_DOC_ORIGEN.DataPropertyName = "SERIE_DOC_ORIGEN"
        Me.SERIE_DOC_ORIGEN.HeaderText = "SERIE_DOC_ORIGEN"
        Me.SERIE_DOC_ORIGEN.Name = "SERIE_DOC_ORIGEN"
        Me.SERIE_DOC_ORIGEN.ReadOnly = True
        Me.SERIE_DOC_ORIGEN.Visible = False
        '
        'NRO_DOC_ORIGEN
        '
        Me.NRO_DOC_ORIGEN.DataPropertyName = "NRO_DOC_ORIGEN"
        Me.NRO_DOC_ORIGEN.HeaderText = "NRO_DOC_ORIGEN"
        Me.NRO_DOC_ORIGEN.Name = "NRO_DOC_ORIGEN"
        Me.NRO_DOC_ORIGEN.ReadOnly = True
        Me.NRO_DOC_ORIGEN.Visible = False
        '
        'PDID_MONEDA
        '
        Me.PDID_MONEDA.DataPropertyName = "ID_MONEDA"
        Me.PDID_MONEDA.HeaderText = "ID_MONEDA"
        Me.PDID_MONEDA.Name = "PDID_MONEDA"
        Me.PDID_MONEDA.ReadOnly = True
        Me.PDID_MONEDA.Visible = False
        '
        'PDMonDescripcion
        '
        Me.PDMonDescripcion.DataPropertyName = "MonDescripcion"
        Me.PDMonDescripcion.HeaderText = "Moneda"
        Me.PDMonDescripcion.Name = "PDMonDescripcion"
        Me.PDMonDescripcion.ReadOnly = True
        Me.PDMonDescripcion.Width = 125
        '
        'IMPORTE
        '
        Me.IMPORTE.DataPropertyName = "IMPORTE"
        Me.IMPORTE.HeaderText = "Importe"
        Me.IMPORTE.Name = "IMPORTE"
        Me.IMPORTE.ReadOnly = True
        '
        'PDID_ESTADO
        '
        Me.PDID_ESTADO.DataPropertyName = "ID_ESTADO"
        Me.PDID_ESTADO.HeaderText = "ID_ESTADO"
        Me.PDID_ESTADO.Name = "PDID_ESTADO"
        Me.PDID_ESTADO.ReadOnly = True
        Me.PDID_ESTADO.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.PDID_ESTADO.Visible = False
        '
        'DES_ESTADO
        '
        Me.DES_ESTADO.DataPropertyName = "DES_ESTADO"
        Me.DES_ESTADO.HeaderText = "Estado"
        Me.DES_ESTADO.Name = "DES_ESTADO"
        Me.DES_ESTADO.ReadOnly = True
        Me.DES_ESTADO.Width = 130
        '
        'Glosa
        '
        Me.Glosa.DataPropertyName = "Glosa"
        Me.Glosa.HeaderText = "Glosa"
        Me.Glosa.Name = "Glosa"
        Me.Glosa.ReadOnly = True
        '
        'SolicitadoA
        '
        Me.SolicitadoA.DataPropertyName = "SolicitadoA"
        Me.SolicitadoA.HeaderText = "Solicitado/Razon Social"
        Me.SolicitadoA.Name = "SolicitadoA"
        Me.SolicitadoA.ReadOnly = True
        Me.SolicitadoA.Width = 155
        '
        'ID_DocPendiente
        '
        Me.ID_DocPendiente.DataPropertyName = "ID_DocPendiente"
        Me.ID_DocPendiente.HeaderText = "ID_DocPendiente"
        Me.ID_DocPendiente.Name = "ID_DocPendiente"
        Me.ID_DocPendiente.ReadOnly = True
        '
        'Saldo
        '
        Me.Saldo.DataPropertyName = "Saldo"
        Me.Saldo.HeaderText = "Saldo"
        Me.Saldo.Name = "Saldo"
        Me.Saldo.ReadOnly = True
        Me.Saldo.Visible = False
        '
        'Amortizado
        '
        Me.Amortizado.DataPropertyName = "Amortizado"
        Me.Amortizado.HeaderText = "Amortizado"
        Me.Amortizado.Name = "Amortizado"
        Me.Amortizado.Visible = False
        '
        'PorcentajePercepcion
        '
        Me.PorcentajePercepcion.DataPropertyName = "PorcentajePercepcion"
        Me.PorcentajePercepcion.HeaderText = "PorcentajePercepcion"
        Me.PorcentajePercepcion.Name = "PorcentajePercepcion"
        Me.PorcentajePercepcion.Visible = False
        '
        'PorcentajeRetDet
        '
        Me.PorcentajeRetDet.DataPropertyName = "PorcentajeRetDet"
        Me.PorcentajeRetDet.HeaderText = "PorcentajeRetDet"
        Me.PorcentajeRetDet.Name = "PorcentajeRetDet"
        Me.PorcentajeRetDet.Visible = False
        '
        'TOTALBRUTO
        '
        Me.TOTALBRUTO.DataPropertyName = "TOTALBRUTO"
        Me.TOTALBRUTO.HeaderText = "TOTALBRUTO"
        Me.TOTALBRUTO.Name = "TOTALBRUTO"
        Me.TOTALBRUTO.Visible = False
        '
        'Menu
        '
        Me.Menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Eliminar})
        Me.Menu.Name = "Eliminar"
        Me.Menu.Size = New System.Drawing.Size(118, 26)
        '
        'Eliminar
        '
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.Size = New System.Drawing.Size(117, 22)
        Me.Eliminar.Text = "Eliminar"
        '
        'rpgPrueba
        '
        Me.rpgPrueba.Controls.Add(Me.ListarPendiente)
        Me.rpgPrueba.Controls.Add(Me.Propiedades)
        Me.rpgPrueba.Controls.Add(Me.ListarProgramado)
        Me.rpgPrueba.Location = New System.Drawing.Point(0, 78)
        Me.rpgPrueba.Name = "rpgPrueba"
        Me.rpgPrueba.SelectedPage = Me.ListarPendiente
        Me.rpgPrueba.Size = New System.Drawing.Size(609, 451)
        Me.rpgPrueba.TabIndex = 3
        CType(Me.rpgPrueba.GetChildAt(0), Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None
        '
        'ListarPendiente
        '
        Me.ListarPendiente.Controls.Add(Me.GroupBox1)
        Me.ListarPendiente.Location = New System.Drawing.Point(10, 37)
        Me.ListarPendiente.Name = "ListarPendiente"
        Me.ListarPendiente.Size = New System.Drawing.Size(588, 403)
        Me.ListarPendiente.Text = "Listar Pendiente"
        '
        'Propiedades
        '
        Me.Propiedades.Controls.Add(Me.GroupBox5)
        Me.Propiedades.Enabled = False
        Me.Propiedades.Location = New System.Drawing.Point(10, 37)
        Me.Propiedades.Name = "Propiedades"
        Me.Propiedades.Size = New System.Drawing.Size(588, 403)
        Me.Propiedades.Text = "Propiedades"
        '
        'ListarProgramado
        '
        Me.ListarProgramado.Controls.Add(Me.dgvProgramado)
        Me.ListarProgramado.Location = New System.Drawing.Point(10, 37)
        Me.ListarProgramado.Name = "ListarProgramado"
        Me.ListarProgramado.Size = New System.Drawing.Size(588, 403)
        Me.ListarProgramado.Text = "Listar Programado"
        '
        'dgvProgramado
        '
        Me.dgvProgramado.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvProgramado.EnableHotTracking = False
        Me.dgvProgramado.Location = New System.Drawing.Point(0, 0)
        '
        '
        '
        Me.dgvProgramado.MasterTemplate.AllowAddNewRow = False
        Me.dgvProgramado.MasterTemplate.AllowColumnReorder = False
        Me.dgvProgramado.MasterTemplate.AllowDeleteRow = False
        GridViewTextBoxColumn35.FieldName = "ID_PROGPAGO"
        GridViewTextBoxColumn35.HeaderText = "Nro"
        GridViewTextBoxColumn35.Name = "ID_PROGPAGO"
        GridViewTextBoxColumn35.ReadOnly = True
        GridViewTextBoxColumn35.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn35.Width = 65
        GridViewTextBoxColumn36.FieldName = "FECHA"
        GridViewTextBoxColumn36.HeaderText = "Fecha Registro"
        GridViewTextBoxColumn36.IsVisible = False
        GridViewTextBoxColumn36.Name = "FECHA"
        GridViewTextBoxColumn36.ReadOnly = True
        GridViewTextBoxColumn36.Width = 100
        GridViewTextBoxColumn37.FieldName = "VIGENCIA"
        GridViewTextBoxColumn37.HeaderText = "Fecha de Pago"
        GridViewTextBoxColumn37.Name = "VIGENCIA"
        GridViewTextBoxColumn37.ReadOnly = True
        GridViewTextBoxColumn37.Width = 100
        GridViewTextBoxColumn38.FieldName = "ID_MONEDA"
        GridViewTextBoxColumn38.HeaderText = "ID_MONEDA"
        GridViewTextBoxColumn38.IsVisible = False
        GridViewTextBoxColumn38.Name = "ID_MONEDA"
        GridViewTextBoxColumn38.ReadOnly = True
        GridViewTextBoxColumn39.FieldName = "MonDescripcion"
        GridViewTextBoxColumn39.HeaderText = "Moneda"
        GridViewTextBoxColumn39.Name = "MonDescripcion"
        GridViewTextBoxColumn39.ReadOnly = True
        GridViewTextBoxColumn39.Width = 100
        GridViewTextBoxColumn40.FieldName = "TOTAL"
        GridViewTextBoxColumn40.HeaderText = "Total"
        GridViewTextBoxColumn40.Name = "TOTAL"
        GridViewTextBoxColumn40.ReadOnly = True
        GridViewTextBoxColumn40.Width = 100
        GridViewTextBoxColumn41.FieldName = "ID_ESTADO"
        GridViewTextBoxColumn41.HeaderText = "ID_ESTADO"
        GridViewTextBoxColumn41.IsVisible = False
        GridViewTextBoxColumn41.Name = "ID_ESTADO"
        GridViewTextBoxColumn41.ReadOnly = True
        GridViewTextBoxColumn42.FieldName = "DESCRIP_ESTADO"
        GridViewTextBoxColumn42.HeaderText = "Estado"
        GridViewTextBoxColumn42.Name = "DESCRIP_ESTADO"
        GridViewTextBoxColumn42.ReadOnly = True
        GridViewTextBoxColumn42.Width = 100
        Me.dgvProgramado.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn35, GridViewTextBoxColumn36, GridViewTextBoxColumn37, GridViewTextBoxColumn38, GridViewTextBoxColumn39, GridViewTextBoxColumn40, GridViewTextBoxColumn41, GridViewTextBoxColumn42})
        Me.dgvProgramado.MasterTemplate.EnableFiltering = True
        Me.dgvProgramado.MasterTemplate.EnableGrouping = False
        Me.dgvProgramado.MasterTemplate.EnableSorting = False
        Me.dgvProgramado.Name = "dgvProgramado"
        Me.dgvProgramado.Size = New System.Drawing.Size(588, 403)
        Me.dgvProgramado.StandardTab = True
        Me.dgvProgramado.TabIndex = 6
        Me.dgvProgramado.Text = "RadGridView1"
        '
        'dgvListaPP
        '
        Me.dgvListaPP.AllowAddNewRow = False
        Me.dgvListaPP.AllowColumnReorder = False
        Me.dgvListaPP.AllowDeleteRow = False
        GridViewTextBoxColumn43.FieldName = "ID_PROGPAGO"
        GridViewTextBoxColumn43.HeaderText = "Nro"
        GridViewTextBoxColumn43.Name = "ID_PROGPAGO"
        GridViewTextBoxColumn43.ReadOnly = True
        GridViewTextBoxColumn43.Width = 35
        GridViewTextBoxColumn44.FieldName = "FECHA"
        GridViewTextBoxColumn44.HeaderText = "Fecha Registro"
        GridViewTextBoxColumn44.Name = "FECHA"
        GridViewTextBoxColumn44.ReadOnly = True
        GridViewTextBoxColumn44.Width = 100
        GridViewTextBoxColumn45.FieldName = "VIGENCIA"
        GridViewTextBoxColumn45.HeaderText = "Vigencia"
        GridViewTextBoxColumn45.Name = "VIGENCIA"
        GridViewTextBoxColumn45.ReadOnly = True
        GridViewTextBoxColumn45.Width = 100
        GridViewTextBoxColumn46.FieldName = "ID_MONEDA"
        GridViewTextBoxColumn46.HeaderText = "ID_MONEDA"
        GridViewTextBoxColumn46.IsVisible = False
        GridViewTextBoxColumn46.Name = "ID_MONEDA"
        GridViewTextBoxColumn46.ReadOnly = True
        GridViewTextBoxColumn46.Width = 100
        GridViewTextBoxColumn47.FieldName = "MonDescripcion"
        GridViewTextBoxColumn47.HeaderText = "Moneda"
        GridViewTextBoxColumn47.Name = "MonDescripcion"
        GridViewTextBoxColumn47.ReadOnly = True
        GridViewTextBoxColumn47.Width = 100
        GridViewTextBoxColumn48.FieldName = "TOTAL"
        GridViewTextBoxColumn48.HeaderText = "Total"
        GridViewTextBoxColumn48.Name = "TOTAL"
        GridViewTextBoxColumn48.ReadOnly = True
        GridViewTextBoxColumn48.Width = 100
        GridViewTextBoxColumn49.FieldName = "ID_ESTADO"
        GridViewTextBoxColumn49.HeaderText = "ID_ESTADO"
        GridViewTextBoxColumn49.IsVisible = False
        GridViewTextBoxColumn49.Name = "ID_ESTADO"
        GridViewTextBoxColumn49.ReadOnly = True
        GridViewTextBoxColumn49.Width = 100
        GridViewTextBoxColumn50.FieldName = "DESCRIP_ESTADO"
        GridViewTextBoxColumn50.HeaderText = "Estado"
        GridViewTextBoxColumn50.Name = "DESCRIP_ESTADO"
        GridViewTextBoxColumn50.ReadOnly = True
        GridViewTextBoxColumn50.Width = 100
        Me.dgvListaPP.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn43, GridViewTextBoxColumn44, GridViewTextBoxColumn45, GridViewTextBoxColumn46, GridViewTextBoxColumn47, GridViewTextBoxColumn48, GridViewTextBoxColumn49, GridViewTextBoxColumn50})
        Me.dgvListaPP.EnableFiltering = True
        Me.dgvListaPP.EnableGrouping = False
        Me.dgvListaPP.EnableSorting = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.dtpFin)
        Me.GroupBox2.Controls.Add(Me.dtpInicio)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox2.Location = New System.Drawing.Point(0, 25)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(609, 54)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(397, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(194, 25)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(57, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Fecha Fin:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(13, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(68, 13)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Fecha Inicio:"
        '
        'dtpFin
        '
        Me.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFin.Location = New System.Drawing.Point(257, 21)
        Me.dtpFin.Name = "dtpFin"
        Me.dtpFin.Size = New System.Drawing.Size(100, 20)
        Me.dtpFin.TabIndex = 0
        '
        'dtpInicio
        '
        Me.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInicio.Location = New System.Drawing.Point(85, 21)
        Me.dtpInicio.Name = "dtpInicio"
        Me.dtpInicio.Size = New System.Drawing.Size(92, 20)
        Me.dtpInicio.TabIndex = 0
        '
        'frmProgramacionPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.ClientSize = New System.Drawing.Size(609, 527)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.rpgPrueba)
        Me.Controls.Add(Me.ToolStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmProgramacionPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Programación Pago Detalle"
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvListaPP1.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvListaPP1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        CType(Me.dgvPPD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Menu.ResumeLayout(False)
        CType(Me.rpgPrueba, System.ComponentModel.ISupportInitialize).EndInit()
        Me.rpgPrueba.ResumeLayout(False)
        Me.ListarPendiente.ResumeLayout(False)
        Me.Propiedades.ResumeLayout(False)
        Me.ListarProgramado.ResumeLayout(False)
        CType(Me.dgvProgramado.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvProgramado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvListaPP, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents cboMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents dtpCreacion As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtIdPP As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpVigencia As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPPD As System.Windows.Forms.DataGridView
    Friend WithEvents tsGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Menu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents rpgPrueba As Telerik.WinControls.UI.RadPageView
    Friend WithEvents ListarPendiente As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents Propiedades As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents tsCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgvListaPP1 As Telerik.WinControls.UI.RadGridView
    Friend WithEvents dgvListaPP As Telerik.WinControls.UI.MasterGridViewTemplate
    Friend WithEvents tsbReporte As System.Windows.Forms.ToolStripButton
    Friend WithEvents Eliminar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsdReportes As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ReporteProgramaciónDePagosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListarProgramado As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents dgvProgramado As Telerik.WinControls.UI.RadGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtpFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ID_PROPAGO_DET As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OcoTipoCompServ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PDID_PROGPAGO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FormaPago As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ID_DOC_ORIGEN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCRIPCION As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SERIE_DOC_ORIGEN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NRO_DOC_ORIGEN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PDID_MONEDA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PDMonDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IMPORTE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PDID_ESTADO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DES_ESTADO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Glosa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SolicitadoA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ID_DocPendiente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Saldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amortizado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajePercepcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PorcentajeRetDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TOTALBRUTO As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
