﻿Imports CapaNegocios
Imports CapaDatos
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports IWshRuntimeLibrary
Imports CapaEntidad
Imports CapaEntidad.Proceso
Imports BusinessLogicLayer.Procesos
Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad.Mantenimiento


Public Class frmProgramacionPago

    Dim V_ID_PROGPAGO As String
    Dim V_TAB As Boolean
    Dim V_PPD_ID As String
    Dim VL_ID_EntregaRendir As String
    Dim VL_ID_Estado As String
    Dim fechainicio As Date
    Dim fechafin As Date

    Private eLibroBancos As clsLibroBancos
    Dim objEntregaRendir As BeanEntregaRendir
    Dim objProgramacionPagosDetalle As BeanProgramacionPagosDetalle
    Dim objProgramacionPagos As BeanProgramacionPagos
    Dim objOrdenesCompra As BeanOrdenesCompra

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmProgramacionPago = Nothing



    Public Shared Function Instance() As frmProgramacionPago
        If frmInstance Is Nothing Then
            frmInstance = New frmProgramacionPago
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function


    Private Sub frmProgramacionPago_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

#Region "Listar"

    Private Sub ListarProgramacionPagoPendiente(ByVal fechainicio As Date, ByVal fechafin As Date)

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        'objEntregaRendir = New BeanEntregaRendir(cboEmpresa.SelectedValue.ToString())
        Try
            VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Listar_Programacion_Pagos("1", fechainicio, fechafin)

            If VL_BeanResultado.blnExiste = True Then
                dgvListaPP1.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try

    End Sub
    Private Sub ListarProgramacionPagoProgramado(ByVal fechainicio As Date, ByVal fechafin As Date)

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        'objEntregaRendir = New BeanEntregaRendir(cboEmpresa.SelectedValue.ToString())
        Try
            VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Listar_Programacion_Pagos("2", fechainicio, fechafin)

            If VL_BeanResultado.blnExiste = True Then
                dgvProgramado.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try

    End Sub

    Private Sub ListarMonedas()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Monedas()

            If VL_BeanResultado.blnExiste = True Then
                cboMoneda.DataSource = VL_BeanResultado.dtResultado
                cboMoneda.DisplayMember = "MonDescripcion"
                cboMoneda.ValueMember = "MonCodigo"
            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarEstado()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Listar_Estado_PP

            If VL_BeanResultado.blnExiste = True Then


                cboEstado.DataSource = VL_BeanResultado.dtResultado
                cboEstado.DisplayMember = "Estado"
                cboEstado.ValueMember = "ID_ESTADO"
            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarPPD()

        Dim VL_ProgramacionPagoDetalle As SrvProgramacionPagosDetalle = New SrvProgramacionPagosDetalle()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion

        Try
            VL_BeanResultado = VL_ProgramacionPagoDetalle.Fnc_Listar_ProgramacionPagosDetalle(V_ID_PROGPAGO)
            If VL_BeanResultado.blnExiste = True Then

                dgvPPD.DataSource = VL_BeanResultado.dtResultado

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ListarFormaPagos()

        Dim VL_ProgramacionPagoDetalle As SrvProgramacionPagosDetalle = New SrvProgramacionPagosDetalle()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion

        Try
            VL_BeanResultado = VL_ProgramacionPagoDetalle.Fnc_Listar_FormaPago()

            If VL_BeanResultado.blnExiste = True Then

                Dim comboboxColumn As DataGridViewComboBoxColumn = TryCast(dgvPPD.Columns("FormaPago"), DataGridViewComboBoxColumn)
                ' Dim cbocOLUM = dgvPPD.Columns("FormaPago"), da

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

#End Region

#Region "Procesos"

    Private Sub limpiar()

        txtIdPP.Text = ""
        dtpCreacion.Text = DateTime.Now
        dtpVigencia.Text = DateTime.Now
        cboEstado.SelectedIndex = 1
        cboMoneda.SelectedIndex = 1
        txtTotal.Text = ""


    End Sub

    Private Sub sumatotal()

        Dim soles As Decimal = 0D
        Dim dolares As Decimal = 0D

        For i As Integer = 0 To dgvPPD.RowCount - 1

            If dgvPPD.Rows(i).Cells("PDID_MONEDA").Value = "01" Then
                soles += Convert.ToDecimal(dgvPPD.Rows(i).Cells("IMPORTE").Value)
                txtTotal.Text = soles

            End If

            If dgvPPD.Rows(i).Cells("PDID_MONEDA").Value = "02" Then
                dolares += Convert.ToDecimal(dgvPPD.Rows(i).Cells("IMPORTE").Value)
                txtTotal.Text = dolares

            End If

        Next
        '"S/. " + "$/. " + 



    End Sub

  
    Private Sub ActualizarER(ByVal id As Integer)

        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim idmoneda As String = "1"
        VL_ID_Estado = "1"

        objEntregaRendir = New BeanEntregaRendir(id, VL_ID_Estado, idmoneda)

        VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Actualizar_ER(objEntregaRendir)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub ActualizarOC(ByVal id As String)

        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


        VL_ID_Estado = "0"

        objOrdenesCompra = New BeanOrdenesCompra(id, VL_ID_Estado)

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Actualizar_Estado_OC(objOrdenesCompra)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub Actualizard_Estado_DocPendientes(ByVal id As String, ByVal estado As String)

        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Actualizar_Estado_Doc_Pendientes(id, estado)
        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub
    Private Sub Actualizard_DocPendientes(ByVal id As String, ByVal enproceso As String, ByVal saldo As String, ByVal Amortizado As Double)

        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Actualizar_Doc_Pendientes(Convert.ToDecimal(id), Convert.ToDecimal(enproceso), Convert.ToDecimal(saldo), Convert.ToDecimal(Amortizado))
        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub EliminarPPD(ByVal VL_ID_PROPAGO_DET As String)

        Dim VL_SrvProgramacionPagosDetalle As New SrvProgramacionPagosDetalle
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        'Dim VL_ID_PROGPAGO As String = Convert.ToString(dgvPPD.CurrentRow.Cells("ID_PROPAGO_DET").Value)

        'objProgramacionPagosDetalle = New BeanProgramacionPagosDetalle(id)

        VL_BeanResultado = VL_SrvProgramacionPagosDetalle.Fnc_Eliminar_Estado_Programacion_Pagos_Detalle(VL_ID_PROPAGO_DET)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub EliminarPP(ByVal VL_ID_PROGPAGO As String)

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        'Dim VL_ID_PROGPAGO As String = dgvPPD.CurrentRow.Cells("PDID_PROGPAGO").Value
        'Dim VL_Id_Moneda As String = "2"

        'objProgramacionPagos = New BeanProgramacionPagos(VL_ID_PROGPAGO, VL_Id_Moneda)

        VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Eliminar_Programacion_Pagos(VL_ID_PROGPAGO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub ActualizarTotalPP(ByVal VL_ID_PADRE As String, ByVal VL_TOTAL As String)

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()



        'objProgramacionPagos = New BeanProgramacionPagos(VL_ID_PROGPAGO)

        VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Actualizar_Total_Programacion_Pagos(VL_ID_PADRE, VL_TOTAL)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
    End Sub

    Private Sub ActualizarEstadoPP()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


        objProgramacionPagos = New BeanProgramacionPagos(txtIdPP.Text, cboEstado.SelectedValue.ToString, "1")

        VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Actualizar_Estado_Programacion_Pagos(objProgramacionPagos)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
    End Sub

    Private Sub ActualizarFormaPagoPPD(ByVal id_ppd As Integer)

        Dim VL_SrvProgramacionPagosDetalle As New SrvProgramacionPagosDetalle()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_id = Convert.ToString(dgvPPD.Rows(id_ppd).Cells("ID_PROPAGO_DET").Value)
        Dim VL_des = dgvPPD.Rows(id_ppd).Cells("FormaPago").Value

        VL_BeanResultado = VL_SrvProgramacionPagosDetalle.Fnc_ActualizarFormaPago_PPD(VL_id, VL_des)


        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

#End Region

    Private Sub cargarFormaPago()

        Try
            eLibroBancos = New clsLibroBancos
            FormaPago.DataSource = eLibroBancos.fListarTipoMovPagos()
            If eLibroBancos.iNroRegistros > 0 Then
                FormaPago.ValueMember = "IdRendicion"
                FormaPago.DisplayMember = "DescripcionRendicion"
                FormaPago.Selected = 1

            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub frmProgramacionPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' ListarFormaPagos()
        fechainicio = Convert.ToDateTime(dtpInicio.Text)
        fechafin = Convert.ToDateTime(dtpFin.Text)

        cargarFormaPago()
        ListarMonedas()
        ListarEstado()
        dtpCreacion.Enabled = False
        cboMoneda.Enabled = False

        ListarProgramacionPagoPendiente(fechainicio, fechafin)
        ListarProgramacionPagoProgramado(fechainicio, fechafin)
    End Sub

    'Private Sub tcProgramacionPago_SelectedIndexChanged(sender As Object, e As EventArgs)



    '    Dim V_TF_PPD As Integer = dgvPPD.RowCount
    '    Dim prueba1 As Integer = tcProgramacionPago.TabCount - 1

    '    If tcProgramacionPago.SelectedIndex = 1 And V_TAB = False And dgvPPD.RowCount > 0 Then

    '        tcProgramacionPago.SelectedIndex = 0

    '        V_TAB = False
    '        'limpiar()

    '    ElseIf tcProgramacionPago.SelectedIndex = 1 And V_TAB = True Then

    '        tcProgramacionPago.SelectedIndex = 1
    '        V_TAB = False

    '    ElseIf tcProgramacionPago.SelectedIndex = 0 And dgvPPD.RowCount < 1 Then

    '        tcProgramacionPago.SelectedIndex = 1
    '        V_TAB = True
    '        MessageBox.Show("Debe guardar las Modificaciones")

    '        'MessageBox.Show("Debe guardar las Modificaciones")

    '        'ElseIf tcProgramacionPago.SelectedIndex = 0 And V_TAB = True Then

    '        '    tcProgramacionPago.SelectedIndex = 1

    '        'ElseIf tcProgramacionPago.SelectedIndex = 0 And V_TAB = True And dgvPPD.RowCount > 0 Then
    '        '    tcProgramacionPago.SelectedIndex = 0
    '        'Else

    '        '    tcProgramacionPago.SelectedIndex = 1

    '        'Else
    '        'If V_TF_PPD > 0 Then
    '        '    tcProgramacionPago.SelectedIndex = 0
    '        'Else
    '        'MessageBox.Show("Debe guardar las Modificaciones")
    '        'tcProgramacionPago.SelectedIndex = 1
    '        'End If


    '    End If




    'End Sub

    'Private Sub dgvPPD_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvPPD.CellDoubleClick
    '    Dim p As Integer

    '    V_PPD_ID = dgvPPD.CurrentRow.Cells("ID_PROPAGO_DET").Value
    '    Dim V_PP_ID As String = dgvPPD.CurrentRow.Cells("PDID_PROGPAGO").Value
    '    Dim VL_TOTAL As String = txtTotal.Text
    '    Dim V_ID_O As String = dgvPPD.CurrentRow.Cells("ID_DOC_ORIGEN").Value
    '    Dim NRO_DOC_ORIGEN As String = Convert.ToString(dgvPPD.CurrentRow.Cells("NRO_DOC_ORIGEN").Value)

    '    p = MessageBox.Show("DESEA ELIMINAR LA SIGUIENTE OC/ER", "ADVERTENCIA...!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

    '    If p = vbYes Then
    '        dgvPPD.Rows.RemoveAt(dgvPPD.CurrentRow.Index)


    '        If V_ID_O = "2" Then
    '            ActualizarER(NRO_DOC_ORIGEN)
    '        ElseIf V_ID_O = "1" Then
    '            ActualizarOC(NRO_DOC_ORIGEN)
    '        End If

    '        If dgvPPD.RowCount - 1 = -1 Then
    '            txtTotal.Text = "0"
    '        Else
    '            sumatotal()
    '        End If

    '        ActualizarTotalPP(V_PP_ID, txtTotal.Text)
    '        EliminarPPD(V_PPD_ID)
    '        ListarProgramacionPago()
    '    Else

    '    End If

    '    'ACTUALIZA EL ESTADO DE LA ENTREGA A RENDIR ESTADO=1 (PENDIENTE)
    '    'For i As Integer = 0 To dgvPPD.RowCount - 1

    '    '    If dgvPPD.Rows(i).Cells("ID_DOC_ORIGEN").Value = "2" Then

    '    '        ActualizarER(i)

    '    '    End If
    '    'Next
    '    '-------------------------------------------------------------------

    'End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles tsGuardar.Click

        dgvPPD.EndEdit()

        Try
            Dim mo As Integer
            mo = MessageBox.Show("DESEA GUARDAR LOS CAMBIOS REALIZADOS", "ADVERTENCIA...!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If mo = vbYes Then
                Dim id_cabecera As String = txtIdPP.Text

                If dgvPPD.RowCount - 1 = -1 Then

                    EliminarPP(id_cabecera)

                End If


                For i As Integer = 0 To dgvPPD.RowCount - 1

                    Dim ID_DocPendiente As String = dgvPPD.Rows(i).Cells("ID_DocPendiente").Value
                    Dim enproceso As String = "0"
                    Dim saldo As String = dgvPPD.Rows(i).Cells("Saldo").Value
                    Dim amortizado As Double = dgvPPD.Rows(i).Cells("IMPORTE").Value + dgvPPD.Rows(i).Cells("Amortizado").Value
                    Dim totalbruto As String = dgvPPD.Rows(i).Cells("TOTALBRUTO").Value

                    ActualizarFormaPagoPPD(i)
                    ActualizarEstadoPP()
                    Actualizard_DocPendientes(ID_DocPendiente, "0", saldo, amortizado)

                Next
            End If

            fechainicio = DateTime.Now
            fechafin = DateTime.Now

            ListarProgramacionPagoPendiente(fechainicio, fechafin)
            ListarProgramacionPagoProgramado(fechainicio, fechafin)
            MessageBox.Show("La operación se realizo exitosamente...!")

            rpgPrueba.Pages(1).Enabled = False
            rpgPrueba.Pages(0).Enabled = True
            rpgPrueba.SelectedPage = ListarPendiente
            tsGuardar.Enabled = False
            tsCancelar.Enabled = False
            'tcProgramacionPago.SelectedIndex = 0
            If mo = vbNo Then

            End If

        Catch ex As Exception
            MessageBox.Show("Seleccione un tipo de documento!", "ERROR...!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub Eliminar_Click(sender As Object, e As EventArgs) Handles Eliminar.Click

        Dim p As Integer

        V_PPD_ID = dgvPPD.CurrentRow.Cells("ID_PROPAGO_DET").Value
        Dim V_PP_ID As String = dgvPPD.CurrentRow.Cells("PDID_PROGPAGO").Value
        Dim VL_TOTAL As String = txtTotal.Text
        Dim V_ID_O As String = dgvPPD.CurrentRow.Cells("ID_DOC_ORIGEN").Value
        Dim NRO_DOC_ORIGEN As String = Convert.ToString(dgvPPD.CurrentRow.Cells("NRO_DOC_ORIGEN").Value)
        Dim VL_ID_DocPendiente As String = dgvPPD.CurrentRow.Cells("ID_DocPendiente").Value

        p = MessageBox.Show("DESEA ELIMINAR LA SIGUIENTE OC/ER", "ADVERTENCIA...!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

        If p = vbYes Then



            If V_ID_O = "2" Then
                ActualizarER(NRO_DOC_ORIGEN)
            ElseIf V_ID_O = "1" Then
                'ActualizarOC(NRO_DOC_ORIGEN)
            End If



            If dgvPPD.CurrentRow.Cells("ID_DOC_ORIGEN").Value = "1" Then

                Dim Saldo As String = dgvPPD.CurrentRow.Cells("Saldo").Value
                Dim TOTALBRUTO As String = dgvPPD.CurrentRow.Cells("TOTALBRUTO").Value

                Dim resultadoA As Double = Convert.ToDecimal(Saldo) + Convert.ToDecimal(TOTALBRUTO)

                Actualizard_Estado_DocPendientes(VL_ID_DocPendiente, "0")
                Actualizard_DocPendientes(VL_ID_DocPendiente, "0", resultadoA.ToString(), "0")
                ActualizarTotalPP(V_PP_ID, txtTotal.Text)
                EliminarPPD(V_PPD_ID)

            Else
                Dim Saldo As String = dgvPPD.CurrentRow.Cells("Saldo").Value
                Dim importe As String = dgvPPD.CurrentRow.Cells("IMPORTE").Value


                Dim resultadoA As Double = Convert.ToDecimal(Saldo) + Convert.ToDecimal(importe)

                Actualizard_Estado_DocPendientes(VL_ID_DocPendiente, "0")
                Actualizard_DocPendientes(VL_ID_DocPendiente, "0", resultadoA, "0")
                ActualizarTotalPP(V_PP_ID, txtTotal.Text)
                EliminarPPD(V_PPD_ID)

            End If

         

            fechainicio = DateTime.Now
            fechafin = DateTime.Now

            ListarProgramacionPagoPendiente(fechainicio, fechafin)
            ListarProgramacionPagoProgramado(fechainicio, fechafin)
            dgvPPD.Rows.RemoveAt(dgvPPD.CurrentRow.Index)

            If dgvPPD.RowCount - 1 = -1 Then
                txtTotal.Text = "0.00"
            Else
                sumatotal()
            End If
        Else

        End If
        tsCancelar.Enabled = False
        'ACTUALIZA EL ESTADO DE LA ENTREGA A RENDIR ESTADO=1 (PENDIENTE)
        'For i As Integer = 0 To dgvPPD.RowCount - 1

        '    If dgvPPD.Rows(i).Cells("ID_DOC_ORIGEN").Value = "2" Then

        '        ActualizarER(i)

        '    End If
        'Next
        '-------------------------------------------------------------------

    End Sub


    Private Sub ToolStripButton1_Click_1(sender As Object, e As EventArgs) Handles tsCancelar.Click

        rpgPrueba.Pages(1).Enabled = False
        rpgPrueba.Pages(2).Enabled = True
        rpgPrueba.Pages(0).Enabled = True
        rpgPrueba.SelectedPage = ListarPendiente
        tsGuardar.Enabled = False
        tsCancelar.Enabled = False
        tsbReporte.Enabled = True

        dtpInicio.Enabled = True
        dtpFin.Enabled = True
        Button1.Enabled = True


        limpiar()

    End Sub

    'Private Sub RadGridView1_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs)

    '    rpgPrueba.Pages(1).Enabled = True
    '    rpgPrueba.Pages(0).Enabled = False
    '    rpgPrueba.SelectedPage = Propiedades
    '    tsGuardar.Enabled = True
    '    'tcProgramacionPago.SelectedTab = Propiedades

    '    txtIdPP.Text = e.CurrentRow.Cells("ID_PROGPAGO").Value
    '    dtpCreacion.Text = e.CurrentRow.Cells("FECHA").Value
    '    dtpVigencia.Text = e.CurrentRow.Cells("VIGENCIA").Value
    '    cboEstado.SelectedValue = e.CurrentRow.Cells("ID_ESTADO").Value
    '    cboMoneda.SelectedValue = e.CurrentRow.Cells("ID_MONEDA").Value
    '    txtTotal.Text = e.CurrentRow.Cells("TOTAL").Value

    '    If e.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString() <> "" Then

    '        V_ID_PROGPAGO = e.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString()

    '        ListarPPD()
    '    End If
    'End Sub

    Private Sub dgvListaPP12_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles dgvListaPP1.CellDoubleClick
        'tcProgramacionPago.SelectedTab = Propiedades


        txtIdPP.Text = dgvListaPP1.CurrentRow.Cells("ID_PROGPAGO").Value
        dtpCreacion.Text = dgvListaPP1.CurrentRow.Cells("FECHA").Value
        dtpVigencia.Text = dgvListaPP1.CurrentRow.Cells("VIGENCIA").Value
        cboEstado.SelectedValue = dgvListaPP1.CurrentRow.Cells("ID_ESTADO").Value
        cboMoneda.SelectedValue = dgvListaPP1.CurrentRow.Cells("ID_MONEDA").Value
        txtTotal.Text = dgvListaPP1.CurrentRow.Cells("TOTAL").Value

        If dgvListaPP1.CurrentRow.Cells("DESCRIP_ESTADO").Value = "Programado" Then
            'MessageBox.Show("Ya se realizo la programación")

            rpgPrueba.Pages(1).Enabled = True
            rpgPrueba.Pages(2).Enabled = False
            rpgPrueba.Pages(0).Enabled = False

            rpgPrueba.SelectedPage = Propiedades
            tsGuardar.Enabled = False
            tsCancelar.Enabled = True
            tsbReporte.Enabled = False
            cboEstado.Enabled = False
            dtpVigencia.Enabled = False
            dgvPPD.Enabled = False

            dtpInicio.Enabled = False
            dtpFin.Enabled = False
            Button1.Enabled = False



            If dgvListaPP1.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString() <> "" Then

                V_ID_PROGPAGO = dgvListaPP1.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString()

                ListarPPD()
            End If

        Else
            rpgPrueba.Pages(1).Enabled = True
            rpgPrueba.Pages(0).Enabled = False
            rpgPrueba.Pages(2).Enabled = False
            rpgPrueba.SelectedPage = Propiedades
            tsGuardar.Enabled = True
            tsCancelar.Enabled = True
            tsbReporte.Enabled = False

            dtpInicio.Enabled = False
            dtpFin.Enabled = False
            Button1.Enabled = False



            If dgvListaPP1.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString() <> "" Then

                V_ID_PROGPAGO = dgvListaPP1.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString()

                ListarPPD()
            End If
        End If



    End Sub


    Private Sub tsbReporte_Click(sender As Object, e As EventArgs) Handles tsbReporte.Click

        Dim x As frmRPTProgramacionPago = frmRPTProgramacionPago.Instance

        Dim d As New frmRPTProgramacionPago
        d.VG_ID_PROGPAGO = dgvListaPP1.CurrentRow.Cells("ID_PROGPAGO").Value.ToString()
        d.ShowDialog()
        'x.MdiParent = Me

        'x.Show()
    End Sub


    Private Sub dgvListaPP1_CellClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles dgvListaPP1.CellClick

        'If e.ColumnIndex = 0 Then

        '    If dgvListaPP1.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString() <> "" Then

        '        Dim x As New frmRPTProgramacionPago

        '        x.VG_ID_PROGPAGO = dgvListaPP1.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString()
        '        'x.VL_PDOCODIGO = dgvListaPP1.Rows(e.RowIndex).Cells("PDOCODIGO").Value.ToString()

        '        x.ShowDialog()



        '    End If

        'End If

    End Sub



    Private Sub ReporteProgramaciónDePagosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReporteProgramaciónDePagosToolStripMenuItem.Click
        'Dim x As frmReporteProgramaciondePagos = frmReporteProgramaciondePagos.Instance
        'x.MdiParent = frmPrincipal
        'x.Show()
        Dim s As New frmReporteProgramaciondePagos
        s.ShowDialog()


    End Sub

    Private Sub dgvProgramado_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles dgvProgramado.CellDoubleClick

        rpgPrueba.Pages(1).Enabled = True
        rpgPrueba.Pages(0).Enabled = False
        rpgPrueba.Pages(2).Enabled = False
        rpgPrueba.SelectedPage = Propiedades
        tsGuardar.Enabled = False
        tsCancelar.Enabled = True
        tsbReporte.Enabled = False
        cboEstado.Enabled = False
        dtpVigencia.Enabled = False
        dgvPPD.Enabled = False

        dtpInicio.Enabled = False
        dtpFin.Enabled = False
        Button1.Enabled = False



        If dgvProgramado.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString() <> "" Then

            V_ID_PROGPAGO = dgvProgramado.Rows(e.RowIndex).Cells("ID_PROGPAGO").Value.ToString()

            ListarPPD()
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        fechainicio = Convert.ToDateTime(dtpInicio.Text)
        fechafin = Convert.ToDateTime(dtpFin.Text)

        If rpgPrueba.SelectedPage.Equals(ListarPendiente) Then

            ListarProgramacionPagoPendiente(fechainicio, fechafin)

        ElseIf rpgPrueba.SelectedPage.Equals(ListarProgramado) Then

            ListarProgramacionPagoProgramado(fechainicio, fechafin)
        End If



    End Sub

   
End Class