Partial Class rptProgramacionPagos

    'NOTE: The following procedure is required by the telerik Reporting Designer
    'It can be modified using the telerik Reporting Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Dim Group1 As Telerik.Reporting.Group = New Telerik.Reporting.Group()
        Dim Group2 As Telerik.Reporting.Group = New Telerik.Reporting.Group()
        Dim ReportParameter1 As Telerik.Reporting.ReportParameter = New Telerik.Reporting.ReportParameter()
        Dim StyleRule1 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule2 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule3 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule4 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Me.labelsGroupFooterSection = New Telerik.Reporting.GroupFooterSection()
        Me.gLOSACaptionTextBox = New Telerik.Reporting.TextBox()
        Me.TextBox8 = New Telerik.Reporting.TextBox()
        Me.TextBox11 = New Telerik.Reporting.TextBox()
        Me.labelsGroupHeaderSection = New Telerik.Reporting.GroupHeaderSection()
        Me.txtEmpresa = New Telerik.Reporting.TextBox()
        Me.TextBox2 = New Telerik.Reporting.TextBox()
        Me.TextBox3 = New Telerik.Reporting.TextBox()
        Me.TextBox4 = New Telerik.Reporting.TextBox()
        Me.groupFooterSection = New Telerik.Reporting.GroupFooterSection()
        Me.TextBox12 = New Telerik.Reporting.TextBox()
        Me.TextBox13 = New Telerik.Reporting.TextBox()
        Me.Shape1 = New Telerik.Reporting.Shape()
        Me.TextBox14 = New Telerik.Reporting.TextBox()
        Me.Shape3 = New Telerik.Reporting.Shape()
        Me.groupHeaderSection = New Telerik.Reporting.GroupHeaderSection()
        Me.TextBox6 = New Telerik.Reporting.TextBox()
        Me.txtTipoCambio = New Telerik.Reporting.TextBox()
        Me.nETOPAGARDataTextBox = New Telerik.Reporting.TextBox()
        Me.reportFooter = New Telerik.Reporting.ReportFooterSection()
        Me.TextBox1 = New Telerik.Reporting.TextBox()
        Me.txtTotalDolares = New Telerik.Reporting.TextBox()
        Me.Shape2 = New Telerik.Reporting.Shape()
        Me.txtTotalSoles = New Telerik.Reporting.TextBox()
        Me.TextBox15 = New Telerik.Reporting.TextBox()
        Me.TextBox5 = New Telerik.Reporting.TextBox()
        Me.txtTotalGeneralSoles = New Telerik.Reporting.TextBox()
        Me.pageHeader = New Telerik.Reporting.PageHeaderSection()
        Me.txtRuc = New Telerik.Reporting.TextBox()
        Me.txtRazonSocial = New Telerik.Reporting.TextBox()
        Me.TextBox45 = New Telerik.Reporting.TextBox()
        Me.pageFooter = New Telerik.Reporting.PageFooterSection()
        Me.pageInfoTextBox = New Telerik.Reporting.TextBox()
        Me.reportHeader = New Telerik.Reporting.ReportHeaderSection()
        Me.titleTextBox = New Telerik.Reporting.TextBox()
        Me.txtFecha = New Telerik.Reporting.TextBox()
        Me.detail = New Telerik.Reporting.DetailSection()
        Me.gLOSADataTextBox = New Telerik.Reporting.TextBox()
        Me.TextBox7 = New Telerik.Reporting.TextBox()
        Me.Panel1 = New Telerik.Reporting.Panel()
        Me.SqlDataSource1 = New Telerik.Reporting.SqlDataSource()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'labelsGroupFooterSection
        '
        Me.labelsGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Cm(0.629899263381958R)
        Me.labelsGroupFooterSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.gLOSACaptionTextBox, Me.TextBox8, Me.TextBox11})
        Me.labelsGroupFooterSection.Name = "labelsGroupFooterSection"
        Me.labelsGroupFooterSection.Style.Visible = True
        '
        'gLOSACaptionTextBox
        '
        Me.gLOSACaptionTextBox.CanGrow = True
        Me.gLOSACaptionTextBox.Format = "{0:C2}"
        Me.gLOSACaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(0.12989935278892517R))
        Me.gLOSACaptionTextBox.Name = "gLOSACaptionTextBox"
        Me.gLOSACaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8000004291534424R), Telerik.Reporting.Drawing.Unit.Cm(0.43753799796104431R))
        Me.gLOSACaptionTextBox.Style.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.gLOSACaptionTextBox.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.gLOSACaptionTextBox.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.gLOSACaptionTextBox.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.gLOSACaptionTextBox.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.gLOSACaptionTextBox.Style.Font.Bold = True
        Me.gLOSACaptionTextBox.Style.Font.Name = "Tahoma"
        Me.gLOSACaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.gLOSACaptionTextBox.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(1.0R)
        Me.gLOSACaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.gLOSACaptionTextBox.StyleName = ""
        Me.gLOSACaptionTextBox.TextWrap = True
        Me.gLOSACaptionTextBox.Value = "= Sum(CDbl(IIf(Fields.Moneda = 'NUEVOS SOLES', Fields.NETOPAGAR,0)))"
        '
        'TextBox8
        '
        Me.TextBox8.CanGrow = True
        Me.TextBox8.Format = "{0:C2}"
        Me.TextBox8.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.400199890136719R), Telerik.Reporting.Drawing.Unit.Cm(0.12989935278892517R))
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6997995376586914R), Telerik.Reporting.Drawing.Unit.Cm(0.43753799796104431R))
        Me.TextBox8.Style.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.TextBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox8.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox8.Style.Font.Bold = True
        Me.TextBox8.Style.Font.Name = "Tahoma"
        Me.TextBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.TextBox8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(1.0R)
        Me.TextBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox8.StyleName = ""
        Me.TextBox8.Value = "= '$/. ' +  Sum(CDbl(IIf(Fields.Moneda = 'DOLARES', Fields.NETOPAGAR,0)))"
        '
        'TextBox11
        '
        Me.TextBox11.CanGrow = True
        Me.TextBox11.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.092361122369766235R), Telerik.Reporting.Drawing.Unit.Cm(0.12989920377731323R))
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.507437705993652R), Telerik.Reporting.Drawing.Unit.Cm(0.43753799796104431R))
        Me.TextBox11.Style.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.TextBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.Font.Bold = True
        Me.TextBox11.Style.Font.Name = "Tahoma"
        Me.TextBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.TextBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox11.StyleName = ""
        Me.TextBox11.Value = "= 'TOTAL  ' + Fields.EMPRESA"
        '
        'labelsGroupHeaderSection
        '
        Me.labelsGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Cm(0.69999992847442627R)
        Me.labelsGroupHeaderSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.txtEmpresa, Me.TextBox2, Me.TextBox3, Me.TextBox4})
        Me.labelsGroupHeaderSection.Name = "labelsGroupHeaderSection"
        Me.labelsGroupHeaderSection.PrintOnEveryPage = True
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209R), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R))
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.txtEmpresa.Style.Font.Bold = True
        Me.txtEmpresa.Style.Font.Name = "Tahoma"
        Me.txtEmpresa.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.txtEmpresa.Style.Font.Underline = True
        Me.txtEmpresa.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.txtEmpresa.StyleName = ""
        Me.txtEmpresa.Value = "= Fields.EMPRESA"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R))
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6000001430511475R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBox2.Style.Font.Bold = True
        Me.TextBox2.Style.Font.Name = "Tahoma"
        Me.TextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.TextBox2.Style.Font.Underline = True
        Me.TextBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox2.StyleName = ""
        Me.TextBox2.Value = "SOLES"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.292361259460449R), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R))
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBox3.Style.Font.Bold = True
        Me.TextBox3.Style.Font.Name = "Tahoma"
        Me.TextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.TextBox3.Style.Font.Underline = True
        Me.TextBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox3.StyleName = ""
        Me.TextBox3.Value = "DÓLARES"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.299999237060547R), Telerik.Reporting.Drawing.Unit.Cm(0.19999989867210388R))
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1925625801086426R), Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448R))
        Me.TextBox4.Style.Font.Bold = True
        Me.TextBox4.Style.Font.Name = "Tahoma"
        Me.TextBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.TextBox4.Style.Font.Underline = True
        Me.TextBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox4.StyleName = ""
        Me.TextBox4.Value = "ESTADO"
        '
        'groupFooterSection
        '
        Me.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Cm(0.92989975214004517R)
        Me.groupFooterSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox12, Me.TextBox13, Me.Shape1, Me.TextBox14, Me.Shape3})
        Me.groupFooterSection.Name = "groupFooterSection"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209R), Telerik.Reporting.Drawing.Unit.Cm(0.3741704523563385R))
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.399998664855957R), Telerik.Reporting.Drawing.Unit.Cm(0.34280914068222046R))
        Me.TextBox12.Style.Font.Bold = True
        Me.TextBox12.Style.Font.Name = "Tahoma"
        Me.TextBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.TextBox12.Style.Font.Underline = False
        Me.TextBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(2.0R)
        Me.TextBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox12.StyleName = ""
        Me.TextBox12.Value = "= 'TOTAL ' + Fields.DOCUMENTO"
        '
        'TextBox13
        '
        Me.TextBox13.CanGrow = True
        Me.TextBox13.Format = "{0:C2}"
        Me.TextBox13.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.600000381469727R), Telerik.Reporting.Drawing.Unit.Cm(0.3741704523563385R))
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6000003814697266R), Telerik.Reporting.Drawing.Unit.Cm(0.34280875325202942R))
        Me.TextBox13.Style.Font.Bold = True
        Me.TextBox13.Style.Font.Name = "Tahoma"
        Me.TextBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.TextBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox13.StyleName = "Data"
        Me.TextBox13.Value = "= SUM(CDbl(IIf(Fields.Moneda = 'NUEVOS SOLES', Fields.NETOPAGAR,0)))"
        '
        'Shape1
        '
        Me.Shape1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(0.12989947199821472R))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6000020503997803R), Telerik.Reporting.Drawing.Unit.Cm(0.13229165971279144R))
        '
        'TextBox14
        '
        Me.TextBox14.CanGrow = True
        Me.TextBox14.Format = "{0:N2}"
        Me.TextBox14.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.292362213134766R), Telerik.Reporting.Drawing.Unit.Cm(0.3741704523563385R))
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5999991893768311R), Telerik.Reporting.Drawing.Unit.Cm(0.34280875325202942R))
        Me.TextBox14.Style.Font.Bold = True
        Me.TextBox14.Style.Font.Name = "Tahoma"
        Me.TextBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.TextBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox14.StyleName = "Data"
        Me.TextBox14.Value = "= '$/. ' + Sum(CDbl(IIf(Fields.Moneda = 'DOLARES', Fields.NETOPAGAR,0)))"
        '
        'Shape3
        '
        Me.Shape3.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.292363166809082R), Telerik.Reporting.Drawing.Unit.Cm(0.12989960610866547R))
        Me.Shape3.Name = "Shape3"
        Me.Shape3.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape3.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6000020503997803R), Telerik.Reporting.Drawing.Unit.Cm(0.13229165971279144R))
        '
        'groupHeaderSection
        '
        Me.groupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Cm(0.4999997615814209R)
        Me.groupHeaderSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox6})
        Me.groupHeaderSection.Name = "groupHeaderSection"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209R), Telerik.Reporting.Drawing.Unit.Cm(0.044270817190408707R))
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.399998664855957R), Telerik.Reporting.Drawing.Unit.Cm(0.39979976415634155R))
        Me.TextBox6.Style.Font.Bold = True
        Me.TextBox6.Style.Font.Name = "Tahoma"
        Me.TextBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.TextBox6.Style.Font.Underline = True
        Me.TextBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox6.StyleName = ""
        Me.TextBox6.Value = "= Fields.DOCUMENTO"
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.CanGrow = True
        Me.txtTipoCambio.Format = "{0:C2}"
        Me.txtTipoCambio.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.400199890136719R), Telerik.Reporting.Drawing.Unit.Cm(1.1701003313064575R))
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6997979879379272R), Telerik.Reporting.Drawing.Unit.Cm(0.43507501482963562R))
        Me.txtTipoCambio.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTipoCambio.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTipoCambio.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTipoCambio.Style.Font.Bold = True
        Me.txtTipoCambio.Style.Font.Name = "Tahoma"
        Me.txtTipoCambio.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.txtTipoCambio.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.txtTipoCambio.StyleName = "Data"
        Me.txtTipoCambio.Value = "= Parameters.paramTipoCambio.Value"
        '
        'nETOPAGARDataTextBox
        '
        Me.nETOPAGARDataTextBox.CanGrow = True
        Me.nETOPAGARDataTextBox.Format = "{0:C2}"
        Me.nETOPAGARDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.nETOPAGARDataTextBox.Name = "nETOPAGARDataTextBox"
        Me.nETOPAGARDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6000003814697266R), Telerik.Reporting.Drawing.Unit.Cm(0.27000030875205994R))
        Me.nETOPAGARDataTextBox.Style.Font.Name = "Tahoma"
        Me.nETOPAGARDataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.nETOPAGARDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.nETOPAGARDataTextBox.StyleName = "Data"
        Me.nETOPAGARDataTextBox.Value = "= IIf(Fields.Moneda = 'NUEVOS SOLES', Fields.NETOPAGAR,0)"
        '
        'reportFooter
        '
        Me.reportFooter.Height = Telerik.Reporting.Drawing.Unit.Cm(2.9701001644134521R)
        Me.reportFooter.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox1, Me.txtTotalDolares, Me.Shape2, Me.txtTotalSoles, Me.TextBox15, Me.txtTipoCambio, Me.TextBox5, Me.txtTotalGeneralSoles})
        Me.reportFooter.Name = "reportFooter"
        Me.reportFooter.Style.Visible = True
        '
        'TextBox1
        '
        Me.TextBox1.CanGrow = True
        Me.TextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.092360854148864746R), Telerik.Reporting.Drawing.Unit.Cm(0.7143712043762207R))
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.507439613342285R), Telerik.Reporting.Drawing.Unit.Cm(0.89080417156219482R))
        Me.TextBox1.Style.BackgroundColor = System.Drawing.Color.Empty
        Me.TextBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox1.Style.Font.Bold = True
        Me.TextBox1.Style.Font.Name = "Tahoma"
        Me.TextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.TextBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(2.0R)
        Me.TextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox1.StyleName = ""
        Me.TextBox1.Value = "TOTAL GENERAL"
        '
        'txtTotalDolares
        '
        Me.txtTotalDolares.CanGrow = True
        Me.txtTotalDolares.Format = "{0:N2}"
        Me.txtTotalDolares.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.407839775085449R), Telerik.Reporting.Drawing.Unit.Cm(0.71683365106582642R))
        Me.txtTotalDolares.Name = "txtTotalDolares"
        Me.txtTotalDolares.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6921584606170654R), Telerik.Reporting.Drawing.Unit.Cm(0.43507495522499084R))
        Me.txtTotalDolares.Style.BackgroundColor = System.Drawing.Color.Empty
        Me.txtTotalDolares.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalDolares.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalDolares.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalDolares.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalDolares.Style.Font.Bold = True
        Me.txtTotalDolares.Style.Font.Name = "Tahoma"
        Me.txtTotalDolares.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.txtTotalDolares.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(1.0R)
        Me.txtTotalDolares.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.txtTotalDolares.StyleName = ""
        Me.txtTotalDolares.Value = "= '$/. ' + Sum(CDbl(IIf(Fields.Moneda = 'DOLARES', Fields.NETOPAGAR,0)))"
        '
        'Shape2
        '
        Me.Shape2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.092361122369766235R), Telerik.Reporting.Drawing.Unit.Cm(0.51437187194824219R))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.007637023925781R), Telerik.Reporting.Drawing.Unit.Cm(0.13229165971279144R))
        '
        'txtTotalSoles
        '
        Me.txtTotalSoles.CanGrow = True
        Me.txtTotalSoles.Format = "{0:C2}"
        Me.txtTotalSoles.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.59999942779541R), Telerik.Reporting.Drawing.Unit.Cm(0.7143709659576416R))
        Me.txtTotalSoles.Name = "txtTotalSoles"
        Me.txtTotalSoles.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8000001907348633R), Telerik.Reporting.Drawing.Unit.Cm(0.8908049464225769R))
        Me.txtTotalSoles.Style.BackgroundColor = System.Drawing.Color.Empty
        Me.txtTotalSoles.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalSoles.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalSoles.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalSoles.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalSoles.Style.Font.Bold = True
        Me.txtTotalSoles.Style.Font.Name = "Tahoma"
        Me.txtTotalSoles.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.txtTotalSoles.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(1.0R)
        Me.txtTotalSoles.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.txtTotalSoles.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.txtTotalSoles.StyleName = ""
        Me.txtTotalSoles.Value = "= Sum(CDbl(IIf(Fields.Moneda = 'NUEVOS SOLES', Fields.NETOPAGAR,0)))"
        '
        'TextBox15
        '
        Me.TextBox15.CanGrow = True
        Me.TextBox15.Format = "{0:C2}"
        Me.TextBox15.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.40000057220459R), Telerik.Reporting.Drawing.Unit.Cm(1.1701003313064575R))
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99999868869781494R), Telerik.Reporting.Drawing.Unit.Cm(0.43507495522499084R))
        Me.TextBox15.Style.BackgroundColor = System.Drawing.Color.Empty
        Me.TextBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox15.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox15.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox15.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox15.Style.Font.Bold = True
        Me.TextBox15.Style.Font.Name = "Tahoma"
        Me.TextBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.TextBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(1.0R)
        Me.TextBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox15.StyleName = ""
        Me.TextBox15.Value = "T.C."
        '
        'TextBox5
        '
        Me.TextBox5.CanGrow = True
        Me.TextBox5.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999941885471344R), Telerik.Reporting.Drawing.Unit.Cm(1.8701002597808838R))
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.507439613342285R), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478R))
        Me.TextBox5.Style.BackgroundColor = System.Drawing.Color.Empty
        Me.TextBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox5.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox5.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox5.Style.Font.Bold = True
        Me.TextBox5.Style.Font.Name = "Tahoma"
        Me.TextBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.TextBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(2.0R)
        Me.TextBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox5.StyleName = ""
        Me.TextBox5.Value = "TOTAL GENERAL EN SOLES"
        '
        'txtTotalGeneralSoles
        '
        Me.txtTotalGeneralSoles.CanGrow = True
        Me.txtTotalGeneralSoles.Format = "{0:C2}"
        Me.txtTotalGeneralSoles.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.607640266418457R), Telerik.Reporting.Drawing.Unit.Cm(1.8701001405715942R))
        Me.txtTotalGeneralSoles.Name = "txtTotalGeneralSoles"
        Me.txtTotalGeneralSoles.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.49235725402832R), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388R))
        Me.txtTotalGeneralSoles.Style.BackgroundColor = System.Drawing.Color.Empty
        Me.txtTotalGeneralSoles.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalGeneralSoles.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalGeneralSoles.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalGeneralSoles.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.txtTotalGeneralSoles.Style.Font.Bold = True
        Me.txtTotalGeneralSoles.Style.Font.Name = "Tahoma"
        Me.txtTotalGeneralSoles.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.txtTotalGeneralSoles.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(1.0R)
        Me.txtTotalGeneralSoles.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.txtTotalGeneralSoles.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.txtTotalGeneralSoles.StyleName = ""
        Me.txtTotalGeneralSoles.Value = "= Sum(CDbl(IIf(Fields.Moneda = 'NUEVOS SOLES', Fields.NETOPAGAR,0))) + ( Sum(CDbl" & _
    "(IIf(Fields.Moneda = 'DOLARES', Fields.NETOPAGAR,0))) * Parameters.paramTipoCamb" & _
    "io.Value)"
        '
        'pageHeader
        '
        Me.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Cm(0.699999988079071R)
        Me.pageHeader.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.txtRuc, Me.txtRazonSocial, Me.TextBox45})
        Me.pageHeader.Name = "pageHeader"
        '
        'txtRuc
        '
        Me.txtRuc.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.088541708886623383R), Telerik.Reporting.Drawing.Unit.Cm(0.42569437623023987R))
        Me.txtRuc.Name = "txtRuc"
        Me.txtRuc.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.69999885559082R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.txtRuc.Style.Font.Name = "Tahoma"
        Me.txtRuc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.txtRuc.Value = ""
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.088541708886623383R), Telerik.Reporting.Drawing.Unit.Cm(0.092361122369766235R))
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.69999885559082R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.txtRazonSocial.Style.Font.Name = "Tahoma"
        Me.txtRazonSocial.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.txtRazonSocial.Value = ""
        '
        'TextBox45
        '
        Me.TextBox45.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.892561912536621R), Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941R))
        Me.TextBox45.Name = "TextBox45"
        Me.TextBox45.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox45.Style.Font.Name = "Tahoma"
        Me.TextBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox45.Value = "= NOW()"
        '
        'pageFooter
        '
        Me.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Cm(0.69999957084655762R)
        Me.pageFooter.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.pageInfoTextBox})
        Me.pageFooter.Name = "pageFooter"
        '
        'pageInfoTextBox
        '
        Me.pageInfoTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.860832214355469R), Telerik.Reporting.Drawing.Unit.Cm(0.20000016689300537R))
        Me.pageInfoTextBox.Name = "pageInfoTextBox"
        Me.pageInfoTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8277082443237305R), Telerik.Reporting.Drawing.Unit.Cm(0.34708189964294434R))
        Me.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.pageInfoTextBox.StyleName = "PageInfo"
        Me.pageInfoTextBox.Value = "=PageNumber"
        '
        'reportHeader
        '
        Me.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Cm(1.0R)
        Me.reportHeader.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.titleTextBox, Me.txtFecha})
        Me.reportHeader.Name = "reportHeader"
        '
        'titleTextBox
        '
        Me.titleTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.8000001907348633R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.titleTextBox.Name = "titleTextBox"
        Me.titleTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3000001907348633R), Telerik.Reporting.Drawing.Unit.Cm(0.59999990463256836R))
        Me.titleTextBox.Style.Font.Bold = True
        Me.titleTextBox.Style.Font.Name = "Tahoma"
        Me.titleTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14.0R)
        Me.titleTextBox.Style.Font.Underline = False
        Me.titleTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.titleTextBox.StyleName = ""
        Me.titleTextBox.Value = "PROGRAMACIÓN AL"
        '
        'txtFecha
        '
        Me.txtFecha.Format = "{0:d}"
        Me.txtFecha.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.100200653076172R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.txtFecha.Name = "txtFecha"
        Me.txtFecha.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2997984886169434R), Telerik.Reporting.Drawing.Unit.Cm(0.59989970922470093R))
        Me.txtFecha.Style.Font.Bold = True
        Me.txtFecha.Style.Font.Name = "Tahoma"
        Me.txtFecha.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14.0R)
        Me.txtFecha.Style.Font.Underline = False
        Me.txtFecha.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.txtFecha.StyleName = ""
        Me.txtFecha.Value = "= Now()"
        '
        'detail
        '
        Me.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(0.27010050415992737R)
        Me.detail.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.gLOSADataTextBox, Me.nETOPAGARDataTextBox, Me.TextBox7})
        Me.detail.Name = "detail"
        '
        'gLOSADataTextBox
        '
        Me.gLOSADataTextBox.CanGrow = True
        Me.gLOSADataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.092361122369766235R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.gLOSADataTextBox.Name = "gLOSADataTextBox"
        Me.gLOSADataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.207638740539551R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.gLOSADataTextBox.Style.Font.Name = "Tahoma"
        Me.gLOSADataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(5.0R)
        Me.gLOSADataTextBox.StyleName = "Data"
        Me.gLOSADataTextBox.Value = "= Fields.GLOSA"
        '
        'TextBox7
        '
        Me.TextBox7.CanGrow = True
        Me.TextBox7.Format = "{0:N2}"
        Me.TextBox7.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.292362213134766R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5999991893768311R), Telerik.Reporting.Drawing.Unit.Cm(0.27000030875205994R))
        Me.TextBox7.Style.Font.Name = "Tahoma"
        Me.TextBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.TextBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox7.StyleName = "Data"
        Me.TextBox7.Value = "= '$/. ' + Sum(IIf(Fields.Moneda = 'DOLARES', Fields.NETOPAGAR,0)) "
        '
        'Panel1
        '
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0799999237060547R), Telerik.Reporting.Drawing.Unit.Cm(2.5399999618530273R))
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.ConnectionString = "CapaPreTesoreria.My.MySettings.ERPDHMONTConnectionString"
        Me.SqlDataSource1.Name = "SqlDataSource1"
        Me.SqlDataSource1.Parameters.AddRange(New Telerik.Reporting.SqlDataSourceParameter() {New Telerik.Reporting.SqlDataSourceParameter("@Opcion", System.Data.DbType.Int32, Nothing), New Telerik.Reporting.SqlDataSourceParameter("@FECHAINICIO", System.Data.DbType.DateTime, Nothing), New Telerik.Reporting.SqlDataSourceParameter("@FECHAFIN", System.Data.DbType.DateTime, Nothing)})
        Me.SqlDataSource1.SelectCommand = "Tesoreria.uspReporteProgramacionPago"
        Me.SqlDataSource1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        '
        'rptProgramacionPagos
        '
        Me.DataSource = Me.SqlDataSource1
        Group1.GroupFooter = Me.labelsGroupFooterSection
        Group1.GroupHeader = Me.labelsGroupHeaderSection
        Group1.Groupings.Add(New Telerik.Reporting.Grouping("=Fields.CodigoEmpr"))
        Group1.Name = "grpEmpresa"
        Group2.GroupFooter = Me.groupFooterSection
        Group2.GroupHeader = Me.groupHeaderSection
        Group2.Groupings.Add(New Telerik.Reporting.Grouping("=Fields.CodigoEmpr"))
        Group2.Groupings.Add(New Telerik.Reporting.Grouping("=Fields.DOCUMENTO"))
        Group2.Name = "grpDocumento"
        Me.Groups.AddRange(New Telerik.Reporting.Group() {Group1, Group2})
        Me.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.labelsGroupHeaderSection, Me.labelsGroupFooterSection, Me.groupHeaderSection, Me.groupFooterSection, Me.reportFooter, Me.pageHeader, Me.pageFooter, Me.reportHeader, Me.detail})
        Me.Name = "rptFiltro_ProgramacionPago"
        Me.PageSettings.Landscape = False
        Me.PageSettings.Margins = New Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(5.0R), Telerik.Reporting.Drawing.Unit.Mm(5.0R), Telerik.Reporting.Drawing.Unit.Mm(5.0R), Telerik.Reporting.Drawing.Unit.Mm(5.0R))
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        ReportParameter1.Name = "paramTipoCambio"
        ReportParameter1.Type = Telerik.Reporting.ReportParameterType.Float
        Me.ReportParameters.Add(ReportParameter1)
        Me.Style.BackgroundColor = System.Drawing.Color.White
        StyleRule1.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Title")})
        StyleRule1.Style.Color = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(112, Byte), Integer))
        StyleRule1.Style.Font.Name = "Tahoma"
        StyleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18.0R)
        StyleRule2.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Caption")})
        StyleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(112, Byte), Integer))
        StyleRule2.Style.Color = System.Drawing.Color.White
        StyleRule2.Style.Font.Name = "Tahoma"
        StyleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        StyleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        StyleRule3.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Data")})
        StyleRule3.Style.Color = System.Drawing.Color.Black
        StyleRule3.Style.Font.Name = "Tahoma"
        StyleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        StyleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        StyleRule4.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("PageInfo")})
        StyleRule4.Style.Color = System.Drawing.Color.Black
        StyleRule4.Style.Font.Name = "Tahoma"
        StyleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        StyleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.StyleSheet.AddRange(New Telerik.Reporting.Drawing.StyleRule() {StyleRule1, StyleRule2, StyleRule3, StyleRule4})
        Me.Width = Telerik.Reporting.Drawing.Unit.Cm(20.0R)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents labelsGroupHeaderSection As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents gLOSACaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents labelsGroupFooterSection As Telerik.Reporting.GroupFooterSection
    Friend WithEvents reportFooter As Telerik.Reporting.ReportFooterSection
    Friend WithEvents TextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents pageHeader As Telerik.Reporting.PageHeaderSection
    Friend WithEvents pageFooter As Telerik.Reporting.PageFooterSection
    Friend WithEvents pageInfoTextBox As Telerik.Reporting.TextBox
    Friend WithEvents reportHeader As Telerik.Reporting.ReportHeaderSection
    Friend WithEvents titleTextBox As Telerik.Reporting.TextBox
    Friend WithEvents detail As Telerik.Reporting.DetailSection
    Friend WithEvents nETOPAGARDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents txtFecha As Telerik.Reporting.TextBox
    Friend WithEvents txtRuc As Telerik.Reporting.TextBox
    Friend WithEvents txtRazonSocial As Telerik.Reporting.TextBox
    Friend WithEvents TextBox45 As Telerik.Reporting.TextBox
    Friend WithEvents txtEmpresa As Telerik.Reporting.TextBox
    Friend WithEvents TextBox2 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox3 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox4 As Telerik.Reporting.TextBox
    Friend WithEvents gLOSADataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents TextBox7 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox8 As Telerik.Reporting.TextBox
    Friend WithEvents txtTotalSoles As Telerik.Reporting.TextBox
    Friend WithEvents txtTotalDolares As Telerik.Reporting.TextBox
    Friend WithEvents TextBox11 As Telerik.Reporting.TextBox
    Friend WithEvents Shape2 As Telerik.Reporting.Shape
    Friend WithEvents txtTipoCambio As Telerik.Reporting.TextBox
    Friend WithEvents Panel1 As Telerik.Reporting.Panel
    Friend WithEvents TextBox15 As Telerik.Reporting.TextBox
    Friend WithEvents Shape3 As Telerik.Reporting.Shape
    Friend WithEvents TextBox14 As Telerik.Reporting.TextBox
    Friend WithEvents Shape1 As Telerik.Reporting.Shape
    Friend WithEvents TextBox13 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox12 As Telerik.Reporting.TextBox
    Friend WithEvents groupFooterSection As Telerik.Reporting.GroupFooterSection
    Friend WithEvents TextBox6 As Telerik.Reporting.TextBox
    Friend WithEvents groupHeaderSection As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents SqlDataSource1 As Telerik.Reporting.SqlDataSource
    Friend WithEvents TextBox5 As Telerik.Reporting.TextBox
    Friend WithEvents txtTotalGeneralSoles As Telerik.Reporting.TextBox
End Class