Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports Telerik.Reporting
Imports Telerik.Reporting.Drawing

Partial Public Class rptGestionDocumentosPagosTodo
    Inherits Telerik.Reporting.Report
    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub AddItems(ByVal empresa As String, ByVal ruc As String, ByVal nombreorigen As String)
        txtRazonSocial.Value = empresa
        txtRuc.Value = ruc
        txtSubtitulo.Value = nombreorigen
    End Sub

End Class