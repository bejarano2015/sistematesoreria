Partial Class rptFiltro_ProgramacionPago
    
    'NOTE: The following procedure is required by the telerik Reporting Designer
    'It can be modified using the telerik Reporting Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Dim Group1 As Telerik.Reporting.Group = New Telerik.Reporting.Group()
        Dim Group2 As Telerik.Reporting.Group = New Telerik.Reporting.Group()
        Dim StyleRule1 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule2 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule3 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Dim StyleRule4 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Me.labelsGroupFooterSection = New Telerik.Reporting.GroupFooterSection()
        Me.labelsGroupHeaderSection = New Telerik.Reporting.GroupHeaderSection()
        Me.iD_PROGPAGOCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.nETOPAGARCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.solicitaACaptionTextBox = New Telerik.Reporting.TextBox()
        Me.monedaCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.gLOSACaptionTextBox = New Telerik.Reporting.TextBox()
        Me.dOCUMENTOCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.fechaCaptionTextBox = New Telerik.Reporting.TextBox()
        Me.iD_PROGPAGOGroupFooterSection = New Telerik.Reporting.GroupFooterSection()
        Me.iD_PROGPAGOGroupHeaderSection = New Telerik.Reporting.GroupHeaderSection()
        Me.iD_PROGPAGODataTextBox = New Telerik.Reporting.TextBox()
        Me.solicitaADataTextBox = New Telerik.Reporting.TextBox()
        Me.fechaDataTextBox = New Telerik.Reporting.TextBox()
        Me.dOCUMENTODataTextBox = New Telerik.Reporting.TextBox()
        Me.gLOSADataTextBox = New Telerik.Reporting.TextBox()
        Me.monedaDataTextBox = New Telerik.Reporting.TextBox()
        Me.nETOPAGARDataTextBox = New Telerik.Reporting.TextBox()
        Me.ERPDHMONT = New Telerik.Reporting.SqlDataSource()
        Me.reportFooter = New Telerik.Reporting.ReportFooterSection()
        Me.TextBox1 = New Telerik.Reporting.TextBox()
        Me.nETOPAGARSumFunctionTextBox1 = New Telerik.Reporting.TextBox()
        Me.pageHeader = New Telerik.Reporting.PageHeaderSection()
        Me.reportNameTextBox = New Telerik.Reporting.TextBox()
        Me.pageFooter = New Telerik.Reporting.PageFooterSection()
        Me.currentTimeTextBox = New Telerik.Reporting.TextBox()
        Me.pageInfoTextBox = New Telerik.Reporting.TextBox()
        Me.reportHeader = New Telerik.Reporting.ReportHeaderSection()
        Me.titleTextBox = New Telerik.Reporting.TextBox()
        Me.detail = New Telerik.Reporting.DetailSection()
        Me.Shape1 = New Telerik.Reporting.Shape()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'labelsGroupFooterSection
        '
        Me.labelsGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Cm(0.71437495946884155R)
        Me.labelsGroupFooterSection.Name = "labelsGroupFooterSection"
        Me.labelsGroupFooterSection.Style.Visible = False
        '
        'labelsGroupHeaderSection
        '
        Me.labelsGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Cm(1.1058331727981567R)
        Me.labelsGroupHeaderSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.iD_PROGPAGOCaptionTextBox, Me.nETOPAGARCaptionTextBox, Me.solicitaACaptionTextBox, Me.monedaCaptionTextBox, Me.gLOSACaptionTextBox, Me.dOCUMENTOCaptionTextBox, Me.fechaCaptionTextBox})
        Me.labelsGroupHeaderSection.Name = "labelsGroupHeaderSection"
        Me.labelsGroupHeaderSection.PrintOnEveryPage = True
        '
        'iD_PROGPAGOCaptionTextBox
        '
        Me.iD_PROGPAGOCaptionTextBox.CanGrow = True
        Me.iD_PROGPAGOCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.iD_PROGPAGOCaptionTextBox.Name = "iD_PROGPAGOCaptionTextBox"
        Me.iD_PROGPAGOCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.247083306312561R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.iD_PROGPAGOCaptionTextBox.Style.Font.Name = "Arial"
        Me.iD_PROGPAGOCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.iD_PROGPAGOCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.iD_PROGPAGOCaptionTextBox.StyleName = "Caption"
        Me.iD_PROGPAGOCaptionTextBox.Value = "Código"
        '
        'nETOPAGARCaptionTextBox
        '
        Me.nETOPAGARCaptionTextBox.CanGrow = True
        Me.nETOPAGARCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.299999237060547R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.nETOPAGARCaptionTextBox.Name = "nETOPAGARCaptionTextBox"
        Me.nETOPAGARCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6535181999206543R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.nETOPAGARCaptionTextBox.Style.Font.Name = "Arial"
        Me.nETOPAGARCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.nETOPAGARCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.nETOPAGARCaptionTextBox.StyleName = "Caption"
        Me.nETOPAGARCaptionTextBox.Value = "Neto a Pagar"
        '
        'solicitaACaptionTextBox
        '
        Me.solicitaACaptionTextBox.CanGrow = True
        Me.solicitaACaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.1549725532531738R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.solicitaACaptionTextBox.Name = "solicitaACaptionTextBox"
        Me.solicitaACaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3996000289916992R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.solicitaACaptionTextBox.Style.Font.Name = "Arial"
        Me.solicitaACaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.solicitaACaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.solicitaACaptionTextBox.StyleName = "Caption"
        Me.solicitaACaptionTextBox.Value = "Solicita A"
        '
        'monedaCaptionTextBox
        '
        Me.monedaCaptionTextBox.CanGrow = True
        Me.monedaCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.529636383056641R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.monedaCaptionTextBox.Name = "monedaCaptionTextBox"
        Me.monedaCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7019109725952148R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.monedaCaptionTextBox.Style.Font.Name = "Arial"
        Me.monedaCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.monedaCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.monedaCaptionTextBox.StyleName = "Caption"
        Me.monedaCaptionTextBox.Value = "Moneda"
        '
        'gLOSACaptionTextBox
        '
        Me.gLOSACaptionTextBox.CanGrow = True
        Me.gLOSACaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.6230249404907227R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.gLOSACaptionTextBox.Name = "gLOSACaptionTextBox"
        Me.gLOSACaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.838158130645752R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.gLOSACaptionTextBox.Style.Font.Name = "Arial"
        Me.gLOSACaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.gLOSACaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.gLOSACaptionTextBox.StyleName = "Caption"
        Me.gLOSACaptionTextBox.Value = "Observaciones"
        '
        'dOCUMENTOCaptionTextBox
        '
        Me.dOCUMENTOCaptionTextBox.CanGrow = True
        Me.dOCUMENTOCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3748456239700317R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.dOCUMENTOCaptionTextBox.Name = "dOCUMENTOCaptionTextBox"
        Me.dOCUMENTOCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1501853466033936R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.dOCUMENTOCaptionTextBox.Style.Font.Name = "Arial"
        Me.dOCUMENTOCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.dOCUMENTOCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.dOCUMENTOCaptionTextBox.StyleName = "Caption"
        Me.dOCUMENTOCaptionTextBox.Value = "Documento"
        '
        'fechaCaptionTextBox
        '
        Me.fechaCaptionTextBox.CanGrow = True
        Me.fechaCaptionTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.587090015411377R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.fechaCaptionTextBox.Name = "fechaCaptionTextBox"
        Me.fechaCaptionTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4994302988052368R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.fechaCaptionTextBox.Style.Font.Name = "Arial"
        Me.fechaCaptionTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        Me.fechaCaptionTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.fechaCaptionTextBox.StyleName = "Caption"
        Me.fechaCaptionTextBox.Value = "Fecha"
        '
        'iD_PROGPAGOGroupFooterSection
        '
        Me.iD_PROGPAGOGroupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Cm(2.947284460067749R)
        Me.iD_PROGPAGOGroupFooterSection.Name = "iD_PROGPAGOGroupFooterSection"
        Me.iD_PROGPAGOGroupFooterSection.Style.Visible = False
        '
        'iD_PROGPAGOGroupHeaderSection
        '
        Me.iD_PROGPAGOGroupHeaderSection.Height = Telerik.Reporting.Drawing.Unit.Cm(1.1585488319396973R)
        Me.iD_PROGPAGOGroupHeaderSection.Name = "iD_PROGPAGOGroupHeaderSection"
        Me.iD_PROGPAGOGroupHeaderSection.Style.Visible = False
        '
        'iD_PROGPAGODataTextBox
        '
        Me.iD_PROGPAGODataTextBox.CanGrow = True
        Me.iD_PROGPAGODataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0869283676147461R), Telerik.Reporting.Drawing.Unit.Cm(0.000099584787676576525R))
        Me.iD_PROGPAGODataTextBox.Name = "iD_PROGPAGODataTextBox"
        Me.iD_PROGPAGODataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.247083306312561R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.iD_PROGPAGODataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.iD_PROGPAGODataTextBox.StyleName = "Data"
        Me.iD_PROGPAGODataTextBox.Value = "=Fields.ID_PROGPAGO"
        '
        'solicitaADataTextBox
        '
        Me.solicitaADataTextBox.CanGrow = True
        Me.solicitaADataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.1724028587341309R), Telerik.Reporting.Drawing.Unit.Cm(0.000099584787676576525R))
        Me.solicitaADataTextBox.Name = "solicitaADataTextBox"
        Me.solicitaADataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.399599552154541R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.solicitaADataTextBox.Style.Font.Name = "Arial"
        Me.solicitaADataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.solicitaADataTextBox.StyleName = "Data"
        Me.solicitaADataTextBox.Value = "=Fields.SolicitaA"
        '
        'fechaDataTextBox
        '
        Me.fechaDataTextBox.CanGrow = True
        Me.fechaDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.6076886653900146R), Telerik.Reporting.Drawing.Unit.Cm(0.000099584787676576525R))
        Me.fechaDataTextBox.Name = "fechaDataTextBox"
        Me.fechaDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4875655174255371R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.fechaDataTextBox.Style.Font.Name = "Arial"
        Me.fechaDataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.fechaDataTextBox.StyleName = "Data"
        Me.fechaDataTextBox.Value = "=Fields.fecha"
        '
        'dOCUMENTODataTextBox
        '
        Me.dOCUMENTODataTextBox.CanGrow = True
        Me.dOCUMENTODataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.4146325588226318R), Telerik.Reporting.Drawing.Unit.Cm(0.000099584787676576525R))
        Me.dOCUMENTODataTextBox.Name = "dOCUMENTODataTextBox"
        Me.dOCUMENTODataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1193795204162598R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.dOCUMENTODataTextBox.Style.Font.Name = "Arial"
        Me.dOCUMENTODataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.dOCUMENTODataTextBox.StyleName = "Data"
        Me.dOCUMENTODataTextBox.Value = "=Fields.DOCUMENTO"
        '
        'gLOSADataTextBox
        '
        Me.gLOSADataTextBox.CanGrow = True
        Me.gLOSADataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.6491508483886719R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.gLOSADataTextBox.Name = "gLOSADataTextBox"
        Me.gLOSADataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7946372032165527R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.gLOSADataTextBox.Style.Font.Name = "Arial"
        Me.gLOSADataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.gLOSADataTextBox.StyleName = "Data"
        Me.gLOSADataTextBox.Value = "=Fields.GLOSA"
        '
        'monedaDataTextBox
        '
        Me.monedaDataTextBox.CanGrow = True
        Me.monedaDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.520936965942383R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.monedaDataTextBox.Name = "monedaDataTextBox"
        Me.monedaDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7019120454788208R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.monedaDataTextBox.Style.Font.Name = "Arial"
        Me.monedaDataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.monedaDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.monedaDataTextBox.StyleName = "Data"
        Me.monedaDataTextBox.Value = "=Fields.Moneda"
        '
        'nETOPAGARDataTextBox
        '
        Me.nETOPAGARDataTextBox.CanGrow = True
        Me.nETOPAGARDataTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.299997329711914R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.nETOPAGARDataTextBox.Name = "nETOPAGARDataTextBox"
        Me.nETOPAGARDataTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7000026702880859R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.nETOPAGARDataTextBox.Style.Font.Name = "Arial"
        Me.nETOPAGARDataTextBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.0R)
        Me.nETOPAGARDataTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.nETOPAGARDataTextBox.StyleName = "Data"
        Me.nETOPAGARDataTextBox.Value = "=Fields.NETOPAGAR"
        '
        'ERPDHMONT
        '
        Me.ERPDHMONT.ConnectionString = "CapaPreTesoreria.My.MySettings.ERPDHMONT"
        Me.ERPDHMONT.Name = "ERPDHMONT"
        Me.ERPDHMONT.Parameters.AddRange(New Telerik.Reporting.SqlDataSourceParameter() {New Telerik.Reporting.SqlDataSourceParameter("@fechainicio", System.Data.DbType.DateTime, "1-04-2015"), New Telerik.Reporting.SqlDataSourceParameter("@fechafin", System.Data.DbType.DateTime, "22-04-2015"), New Telerik.Reporting.SqlDataSourceParameter("@idestado", System.Data.DbType.AnsiString, "2"), New Telerik.Reporting.SqlDataSourceParameter("@moneda", System.Data.DbType.AnsiString, "01"), New Telerik.Reporting.SqlDataSourceParameter("@CodigoEmpr", System.Data.DbType.AnsiString, "01")})
        Me.ERPDHMONT.SelectCommand = "Tesoreria.USP_Reporte_Programacion_Pago"
        Me.ERPDHMONT.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        '
        'reportFooter
        '
        Me.reportFooter.Height = Telerik.Reporting.Drawing.Unit.Cm(0.65291619300842285R)
        Me.reportFooter.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox1, Me.nETOPAGARSumFunctionTextBox1})
        Me.reportFooter.Name = "reportFooter"
        Me.reportFooter.Style.Visible = True
        '
        'TextBox1
        '
        Me.TextBox1.CanGrow = True
        Me.TextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.399999618530273R), Telerik.Reporting.Drawing.Unit.Cm(0.033526156097650528R))
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.999998927116394R), Telerik.Reporting.Drawing.Unit.Cm(0.619390070438385R))
        Me.TextBox1.Style.Font.Name = "Arial"
        Me.TextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11.0R)
        Me.TextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox1.StyleName = "Caption"
        Me.TextBox1.Value = "Total a Pagar:      "
        '
        'nETOPAGARSumFunctionTextBox1
        '
        Me.nETOPAGARSumFunctionTextBox1.CanGrow = True
        Me.nETOPAGARSumFunctionTextBox1.Format = "{0:N2}"
        Me.nETOPAGARSumFunctionTextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.5R), Telerik.Reporting.Drawing.Unit.Cm(0.033526156097650528R))
        Me.nETOPAGARSumFunctionTextBox1.Name = "nETOPAGARSumFunctionTextBox1"
        Me.nETOPAGARSumFunctionTextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5093396902084351R), Telerik.Reporting.Drawing.Unit.Cm(0.619390070438385R))
        Me.nETOPAGARSumFunctionTextBox1.Style.Font.Name = "Arial"
        Me.nETOPAGARSumFunctionTextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11.0R)
        Me.nETOPAGARSumFunctionTextBox1.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Dotted
        Me.nETOPAGARSumFunctionTextBox1.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        Me.nETOPAGARSumFunctionTextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.nETOPAGARSumFunctionTextBox1.StyleName = "Data"
        Me.nETOPAGARSumFunctionTextBox1.Value = "=Sum(Fields.NETOPAGAR)"
        '
        'pageHeader
        '
        Me.pageHeader.Height = Telerik.Reporting.Drawing.Unit.Cm(1.1058331727981567R)
        Me.pageHeader.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.reportNameTextBox})
        Me.pageHeader.Name = "pageHeader"
        '
        'reportNameTextBox
        '
        Me.reportNameTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.reportNameTextBox.Name = "reportNameTextBox"
        Me.reportNameTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.946983337402344R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.reportNameTextBox.StyleName = "PageInfo"
        Me.reportNameTextBox.Value = "Reporte Programación de Pagos"
        '
        'pageFooter
        '
        Me.pageFooter.Height = Telerik.Reporting.Drawing.Unit.Cm(1.1058331727981567R)
        Me.pageFooter.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.currentTimeTextBox, Me.pageInfoTextBox})
        Me.pageFooter.Name = "pageFooter"
        '
        'currentTimeTextBox
        '
        Me.currentTimeTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.currentTimeTextBox.Name = "currentTimeTextBox"
        Me.currentTimeTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8277082443237305R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.currentTimeTextBox.StyleName = "PageInfo"
        Me.currentTimeTextBox.Value = "=NOW()"
        '
        'pageInfoTextBox
        '
        Me.pageInfoTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.125809669494629R), Telerik.Reporting.Drawing.Unit.Cm(0.052916664630174637R))
        Me.pageInfoTextBox.Name = "pageInfoTextBox"
        Me.pageInfoTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8277082443237305R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.pageInfoTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.pageInfoTextBox.StyleName = "PageInfo"
        Me.pageInfoTextBox.Value = "=PageNumber"
        '
        'reportHeader
        '
        Me.reportHeader.Height = Telerik.Reporting.Drawing.Unit.Cm(2.0529167652130127R)
        Me.reportHeader.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.titleTextBox})
        Me.reportHeader.Name = "reportHeader"
        '
        'titleTextBox
        '
        Me.titleTextBox.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.titleTextBox.Name = "titleTextBox"
        Me.titleTextBox.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.953517913818359R), Telerik.Reporting.Drawing.Unit.Cm(2.0R))
        Me.titleTextBox.Style.Font.Bold = True
        Me.titleTextBox.Style.Font.Underline = True
        Me.titleTextBox.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.titleTextBox.StyleName = "Title"
        Me.titleTextBox.Value = "Programación de Pagos"
        '
        'detail
        '
        Me.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(1.4768688678741455R)
        Me.detail.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.gLOSADataTextBox, Me.dOCUMENTODataTextBox, Me.fechaDataTextBox, Me.solicitaADataTextBox, Me.iD_PROGPAGODataTextBox, Me.monedaDataTextBox, Me.nETOPAGARDataTextBox, Me.Shape1})
        Me.detail.Name = "detail"
        '
        'Shape1
        '
        Me.Shape1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.095253795385360718R), Telerik.Reporting.Drawing.Unit.Cm(0.92113924026489258R))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.85826301574707R), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776R))
        '
        'rptFiltro_ProgramacionPago
        '
        Me.DataSource = Me.ERPDHMONT
        Group1.GroupFooter = Me.labelsGroupFooterSection
        Group1.GroupHeader = Me.labelsGroupHeaderSection
        Group1.Name = "labelsGroup"
        Group2.GroupFooter = Me.iD_PROGPAGOGroupFooterSection
        Group2.GroupHeader = Me.iD_PROGPAGOGroupHeaderSection
        Group2.Groupings.Add(New Telerik.Reporting.Grouping("=Fields.ID_PROGPAGO"))
        Group2.Name = "iD_PROGPAGOGroup"
        Me.Groups.AddRange(New Telerik.Reporting.Group() {Group1, Group2})
        Me.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.labelsGroupHeaderSection, Me.labelsGroupFooterSection, Me.iD_PROGPAGOGroupHeaderSection, Me.iD_PROGPAGOGroupFooterSection, Me.reportFooter, Me.pageHeader, Me.pageFooter, Me.reportHeader, Me.detail})
        Me.Name = "rptFiltro_ProgramacionPago"
        Me.PageSettings.Landscape = False
        Me.PageSettings.Margins = New Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(5.0R), Telerik.Reporting.Drawing.Unit.Mm(5.0R), Telerik.Reporting.Drawing.Unit.Mm(5.0R), Telerik.Reporting.Drawing.Unit.Mm(5.0R))
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Style.BackgroundColor = System.Drawing.Color.White
        StyleRule1.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Title")})
        StyleRule1.Style.Color = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(112, Byte), Integer))
        StyleRule1.Style.Font.Name = "Tahoma"
        StyleRule1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(18.0R)
        StyleRule2.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Caption")})
        StyleRule2.Style.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(28, Byte), Integer), CType(CType(58, Byte), Integer), CType(CType(112, Byte), Integer))
        StyleRule2.Style.Color = System.Drawing.Color.White
        StyleRule2.Style.Font.Name = "Tahoma"
        StyleRule2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.0R)
        StyleRule2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        StyleRule3.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("Data")})
        StyleRule3.Style.Color = System.Drawing.Color.Black
        StyleRule3.Style.Font.Name = "Tahoma"
        StyleRule3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.0R)
        StyleRule3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        StyleRule4.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.StyleSelector("PageInfo")})
        StyleRule4.Style.Color = System.Drawing.Color.Black
        StyleRule4.Style.Font.Name = "Tahoma"
        StyleRule4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        StyleRule4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.StyleSheet.AddRange(New Telerik.Reporting.Drawing.StyleRule() {StyleRule1, StyleRule2, StyleRule3, StyleRule4})
        Me.Width = Telerik.Reporting.Drawing.Unit.Cm(20.092058181762695R)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents ERPDHMONT As Telerik.Reporting.SqlDataSource
    Friend WithEvents labelsGroupHeaderSection As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents iD_PROGPAGOCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents nETOPAGARCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents solicitaACaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents monedaCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents gLOSACaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents fechaCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents dOCUMENTOCaptionTextBox As Telerik.Reporting.TextBox
    Friend WithEvents labelsGroupFooterSection As Telerik.Reporting.GroupFooterSection
    Friend WithEvents iD_PROGPAGOGroupHeaderSection As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents iD_PROGPAGODataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents iD_PROGPAGOGroupFooterSection As Telerik.Reporting.GroupFooterSection
    Friend WithEvents reportFooter As Telerik.Reporting.ReportFooterSection
    Friend WithEvents TextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents nETOPAGARSumFunctionTextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents pageHeader As Telerik.Reporting.PageHeaderSection
    Friend WithEvents reportNameTextBox As Telerik.Reporting.TextBox
    Friend WithEvents pageFooter As Telerik.Reporting.PageFooterSection
    Friend WithEvents currentTimeTextBox As Telerik.Reporting.TextBox
    Friend WithEvents pageInfoTextBox As Telerik.Reporting.TextBox
    Friend WithEvents reportHeader As Telerik.Reporting.ReportHeaderSection
    Friend WithEvents titleTextBox As Telerik.Reporting.TextBox
    Friend WithEvents detail As Telerik.Reporting.DetailSection
    Friend WithEvents nETOPAGARDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents solicitaADataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents monedaDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents gLOSADataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents fechaDataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents dOCUMENTODataTextBox As Telerik.Reporting.TextBox
    Friend WithEvents Shape1 As Telerik.Reporting.Shape
End Class