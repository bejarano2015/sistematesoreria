Partial Class rptGestionDocumentosPagos
    
    'NOTE: The following procedure is required by the telerik Reporting Designer
    'It can be modified using the telerik Reporting Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Dim Group1 As Telerik.Reporting.Group = New Telerik.Reporting.Group()
        Dim Group2 As Telerik.Reporting.Group = New Telerik.Reporting.Group()
        Dim StyleRule1 As Telerik.Reporting.Drawing.StyleRule = New Telerik.Reporting.Drawing.StyleRule()
        Me.groupFooterSection = New Telerik.Reporting.GroupFooterSection()
        Me.TextBox28 = New Telerik.Reporting.TextBox()
        Me.TextBox39 = New Telerik.Reporting.TextBox()
        Me.Shape1 = New Telerik.Reporting.Shape()
        Me.grpCabeceraDocumento = New Telerik.Reporting.GroupHeaderSection()
        Me.TextBox4 = New Telerik.Reporting.TextBox()
        Me.TextBox14 = New Telerik.Reporting.TextBox()
        Me.TextBox3 = New Telerik.Reporting.TextBox()
        Me.TextBox2 = New Telerik.Reporting.TextBox()
        Me.TextBox5 = New Telerik.Reporting.TextBox()
        Me.TextBox6 = New Telerik.Reporting.TextBox()
        Me.TextBox7 = New Telerik.Reporting.TextBox()
        Me.TextBox8 = New Telerik.Reporting.TextBox()
        Me.TextBox12 = New Telerik.Reporting.TextBox()
        Me.TextBox13 = New Telerik.Reporting.TextBox()
        Me.TextBox9 = New Telerik.Reporting.TextBox()
        Me.TextBox10 = New Telerik.Reporting.TextBox()
        Me.TextBox15 = New Telerik.Reporting.TextBox()
        Me.TextBox16 = New Telerik.Reporting.TextBox()
        Me.TextBox17 = New Telerik.Reporting.TextBox()
        Me.TextBox18 = New Telerik.Reporting.TextBox()
        Me.TextBox19 = New Telerik.Reporting.TextBox()
        Me.TextBox20 = New Telerik.Reporting.TextBox()
        Me.TextBox21 = New Telerik.Reporting.TextBox()
        Me.TextBox22 = New Telerik.Reporting.TextBox()
        Me.TextBox35 = New Telerik.Reporting.TextBox()
        Me.TextBox36 = New Telerik.Reporting.TextBox()
        Me.TextBox37 = New Telerik.Reporting.TextBox()
        Me.TextBox38 = New Telerik.Reporting.TextBox()
        Me.groupFooterSection1 = New Telerik.Reporting.GroupFooterSection()
        Me.Shape2 = New Telerik.Reporting.Shape()
        Me.TextBox54 = New Telerik.Reporting.TextBox()
        Me.TextBox55 = New Telerik.Reporting.TextBox()
        Me.grpCabeceraPago = New Telerik.Reporting.GroupHeaderSection()
        Me.TextBox30 = New Telerik.Reporting.TextBox()
        Me.TextBox24 = New Telerik.Reporting.TextBox()
        Me.TextBox26 = New Telerik.Reporting.TextBox()
        Me.TextBox32 = New Telerik.Reporting.TextBox()
        Me.TextBox33 = New Telerik.Reporting.TextBox()
        Me.TextBox34 = New Telerik.Reporting.TextBox()
        Me.TextBox46 = New Telerik.Reporting.TextBox()
        Me.TextBox48 = New Telerik.Reporting.TextBox()
        Me.TextBox11 = New Telerik.Reporting.TextBox()
        Me.TextBox41 = New Telerik.Reporting.TextBox()
        Me.TextBox40 = New Telerik.Reporting.TextBox()
        Me.TextBox27 = New Telerik.Reporting.TextBox()
        Me.TextBox25 = New Telerik.Reporting.TextBox()
        Me.TextBox42 = New Telerik.Reporting.TextBox()
        Me.TextBox29 = New Telerik.Reporting.TextBox()
        Me.TextBox23 = New Telerik.Reporting.TextBox()
        Me.TextBox31 = New Telerik.Reporting.TextBox()
        Me.TextBox43 = New Telerik.Reporting.TextBox()
        Me.TextBox50 = New Telerik.Reporting.TextBox()
        Me.pageHeaderSection1 = New Telerik.Reporting.PageHeaderSection()
        Me.TextBox1 = New Telerik.Reporting.TextBox()
        Me.TextBox45 = New Telerik.Reporting.TextBox()
        Me.txtRazonSocial = New Telerik.Reporting.TextBox()
        Me.txtRuc = New Telerik.Reporting.TextBox()
        Me.txtSubtitulo = New Telerik.Reporting.TextBox()
        Me.detail = New Telerik.Reporting.DetailSection()
        Me.TextBox47 = New Telerik.Reporting.TextBox()
        Me.TextBox49 = New Telerik.Reporting.TextBox()
        Me.TextBox51 = New Telerik.Reporting.TextBox()
        Me.TextBox52 = New Telerik.Reporting.TextBox()
        Me.TextBox53 = New Telerik.Reporting.TextBox()
        Me.pageFooterSection1 = New Telerik.Reporting.PageFooterSection()
        Me.TextBox44 = New Telerik.Reporting.TextBox()
        Me.SqlDataSource1 = New Telerik.Reporting.SqlDataSource()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'groupFooterSection
        '
        Me.groupFooterSection.Height = Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582R)
        Me.groupFooterSection.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox28, Me.TextBox39, Me.Shape1})
        Me.groupFooterSection.Name = "groupFooterSection"
        '
        'TextBox28
        '
        Me.TextBox28.CanGrow = True
        Me.TextBox28.Format = "{0:N2}"
        Me.TextBox28.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.6078367233276367R), Telerik.Reporting.Drawing.Unit.Cm(0.26048529148101807R))
        Me.TextBox28.Name = "TextBox28"
        Me.TextBox28.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3998007774353027R), Telerik.Reporting.Drawing.Unit.Cm(0.29989990592002869R))
        Me.TextBox28.Style.Font.Bold = True
        Me.TextBox28.Style.Font.Name = "Tahoma"
        Me.TextBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox28.StyleName = "Data"
        Me.TextBox28.Value = "= Sum(MONTOPAGO)"
        '
        'TextBox39
        '
        Me.TextBox39.Format = ""
        Me.TextBox39.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5076389312744141R), Telerik.Reporting.Drawing.Unit.Cm(0.26048529148101807R))
        Me.TextBox39.Name = "TextBox39"
        Me.TextBox39.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0999977588653564R), Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134R))
        Me.TextBox39.Style.Font.Bold = True
        Me.TextBox39.Style.Font.Name = "Tahoma"
        Me.TextBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox39.Value = "TOTAL PAGO"
        '
        'Shape1
        '
        Me.Shape1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5076389312744141R), Telerik.Reporting.Drawing.Unit.Cm(0.1279933750629425R))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4999980926513672R), Telerik.Reporting.Drawing.Unit.Cm(0.13229165971279144R))
        '
        'grpCabeceraDocumento
        '
        Me.grpCabeceraDocumento.Height = Telerik.Reporting.Drawing.Unit.Cm(2.2999999523162842R)
        Me.grpCabeceraDocumento.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox4, Me.TextBox14, Me.TextBox3, Me.TextBox2, Me.TextBox5, Me.TextBox6, Me.TextBox7, Me.TextBox8, Me.TextBox12, Me.TextBox13, Me.TextBox9, Me.TextBox10, Me.TextBox15, Me.TextBox16, Me.TextBox17, Me.TextBox18, Me.TextBox19, Me.TextBox20, Me.TextBox21, Me.TextBox22, Me.TextBox35, Me.TextBox36, Me.TextBox37, Me.TextBox38})
        Me.grpCabeceraDocumento.Name = "grpCabeceraDocumento"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.800798773765564R), Telerik.Reporting.Drawing.Unit.Cm(1.0887421369552612R))
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.29979944229126R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox4.Style.Font.Bold = False
        Me.TextBox4.Style.Font.Name = "Tahoma"
        Me.TextBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox4.Value = "CENTRO COSTO/AREA/OBRA:"
        '
        'TextBox14
        '
        Me.TextBox14.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.8004001379013062R), Telerik.Reporting.Drawing.Unit.Cm(0.43618917465209961R))
        Me.TextBox14.Name = "TextBox14"
        Me.TextBox14.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2998003959655762R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox14.Style.Font.Bold = False
        Me.TextBox14.Style.Font.Name = "Tahoma"
        Me.TextBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox14.Value = "FECHA DERIVACION:"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.8003998994827271R), Telerik.Reporting.Drawing.Unit.Cm(0.76854181289672852R))
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3001985549926758R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox3.Style.Font.Bold = False
        Me.TextBox3.Style.Font.Name = "Tahoma"
        Me.TextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox3.Value = "EMPRESA:"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.8002007007598877R), Telerik.Reporting.Drawing.Unit.Cm(0.1159888282418251R))
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox2.Style.Font.Bold = False
        Me.TextBox2.Style.Font.Name = "Tahoma"
        Me.TextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox2.StyleName = ""
        Me.TextBox2.Value = "N�MERO DOCUMENTO:"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.800798773765564R), Telerik.Reporting.Drawing.Unit.Cm(1.4089422225952148R))
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2997989654541016R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox5.Style.Font.Bold = False
        Me.TextBox5.Style.Font.Name = "Tahoma"
        Me.TextBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox5.Value = "PROVEEDOR:"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707638740539551R), Telerik.Reporting.Drawing.Unit.Cm(0.11999999731779099R))
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.299799919128418R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox6.Style.Font.Bold = False
        Me.TextBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox6.Value = "MONEDA:"
        '
        'TextBox7
        '
        Me.TextBox7.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707638740539551R), Telerik.Reporting.Drawing.Unit.Cm(0.43999999761581421R))
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.299799919128418R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox7.Style.Font.Bold = False
        Me.TextBox7.Style.Font.Name = "Tahoma"
        Me.TextBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox7.Value = "TOTAL:"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707638740539551R), Telerik.Reporting.Drawing.Unit.Cm(0.76854211091995239R))
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.299799919128418R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox8.Style.Font.Bold = False
        Me.TextBox8.Style.Font.Name = "Tahoma"
        Me.TextBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox8.Value = "PAGO A CUENTA:"
        '
        'TextBox12
        '
        Me.TextBox12.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707638740539551R), Telerik.Reporting.Drawing.Unit.Cm(1.0900000333786011R))
        Me.TextBox12.Name = "TextBox12"
        Me.TextBox12.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2998003959655762R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox12.Style.Font.Bold = False
        Me.TextBox12.Style.Font.Name = "Tahoma"
        Me.TextBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.3000001907348633R)
        Me.TextBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox12.Value = "TOTAL RETENCION"
        '
        'TextBox13
        '
        Me.TextBox13.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707638740539551R), Telerik.Reporting.Drawing.Unit.Cm(1.4102002382278442R))
        Me.TextBox13.Name = "TextBox13"
        Me.TextBox13.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2998013496398926R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox13.Style.Font.Bold = False
        Me.TextBox13.Style.Font.Name = "Tahoma"
        Me.TextBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox13.Value = "TOTAL DETRACCION:"
        '
        'TextBox9
        '
        Me.TextBox9.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707638740539551R), Telerik.Reporting.Drawing.Unit.Cm(1.7304003238677979R))
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.299799919128418R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox9.Style.Font.Bold = False
        Me.TextBox9.Style.Font.Name = "Tahoma"
        Me.TextBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox9.Value = "SALDO:"
        '
        'TextBox10
        '
        Me.TextBox10.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.800798773765564R), Telerik.Reporting.Drawing.Unit.Cm(1.7291423082351685R))
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2997989654541016R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox10.Style.Font.Bold = False
        Me.TextBox10.Style.Font.Name = "Tahoma"
        Me.TextBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox10.Value = "AUTORIZA:"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3007984161376953R), Telerik.Reporting.Drawing.Unit.Cm(0.1159888282418251R))
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9001994132995605R), Telerik.Reporting.Drawing.Unit.Cm(0.31999990344047546R))
        Me.TextBox15.Style.Font.Bold = True
        Me.TextBox15.Style.Font.Name = "Tahoma"
        Me.TextBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox15.Value = "= Fields.NUMERO"
        '
        'TextBox16
        '
        Me.TextBox16.Format = "{0:d}"
        Me.TextBox16.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3011989593505859R), Telerik.Reporting.Drawing.Unit.Cm(0.43618917465209961R))
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8997983932495117R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox16.Style.Font.Bold = True
        Me.TextBox16.Style.Font.Name = "Tahoma"
        Me.TextBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox16.Value = "= Fields.FECHADERIVACION"
        '
        'TextBox17
        '
        Me.TextBox17.Format = "{0:d}"
        Me.TextBox17.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.207441329956055R), Telerik.Reporting.Drawing.Unit.Cm(0.11999993771314621R))
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9000000953674316R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox17.Style.Font.Bold = True
        Me.TextBox17.Style.Font.Name = "Tahoma"
        Me.TextBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox17.Value = "= Fields.Moneda"
        '
        'TextBox18
        '
        Me.TextBox18.Format = "{0:N2}"
        Me.TextBox18.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.207441329956055R), Telerik.Reporting.Drawing.Unit.Cm(0.45764756202697754R))
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox18.Style.Font.Bold = True
        Me.TextBox18.Style.Font.Name = "Tahoma"
        Me.TextBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox18.Value = "= Fields.Total"
        '
        'TextBox19
        '
        Me.TextBox19.Format = "{0:N2}"
        Me.TextBox19.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.207441329956055R), Telerik.Reporting.Drawing.Unit.Cm(0.78618931770324707R))
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox19.Style.Font.Bold = True
        Me.TextBox19.Style.Font.Name = "Tahoma"
        Me.TextBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox19.Value = "= Fields.Amortizado"
        '
        'TextBox20
        '
        Me.TextBox20.Format = "{0:N2}"
        Me.TextBox20.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.207441329956055R), Telerik.Reporting.Drawing.Unit.Cm(1.1063896417617798R))
        Me.TextBox20.Name = "TextBox20"
        Me.TextBox20.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox20.Style.Font.Bold = True
        Me.TextBox20.Style.Font.Name = "Tahoma"
        Me.TextBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox20.Value = "= Fields.ImporteRetencion"
        '
        'TextBox21
        '
        Me.TextBox21.Format = "{0:N2}"
        Me.TextBox21.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.207441329956055R), Telerik.Reporting.Drawing.Unit.Cm(1.4278477430343628R))
        Me.TextBox21.Name = "TextBox21"
        Me.TextBox21.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox21.Style.Font.Bold = True
        Me.TextBox21.Style.Font.Name = "Tahoma"
        Me.TextBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox21.Value = "= Fields.ImporteDetraccion"
        '
        'TextBox22
        '
        Me.TextBox22.Format = "{0:N2}"
        Me.TextBox22.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.207441329956055R), Telerik.Reporting.Drawing.Unit.Cm(1.7480478286743164R))
        Me.TextBox22.Name = "TextBox22"
        Me.TextBox22.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8999996185302734R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox22.Style.Font.Bold = True
        Me.TextBox22.Style.Font.Name = "Tahoma"
        Me.TextBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox22.Value = "= Fields.Saldo"
        '
        'TextBox35
        '
        Me.TextBox35.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3007984161376953R), Telerik.Reporting.Drawing.Unit.Cm(0.7563895583152771R))
        Me.TextBox35.Name = "TextBox35"
        Me.TextBox35.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9001989364624023R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox35.Style.Font.Bold = True
        Me.TextBox35.Style.Font.Name = "Tahoma"
        Me.TextBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox35.Value = "= Fields.EmprAbreviado"
        '
        'TextBox36
        '
        Me.TextBox36.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3011989593505859R), Telerik.Reporting.Drawing.Unit.Cm(1.0887421369552612R))
        Me.TextBox36.Name = "TextBox36"
        Me.TextBox36.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8997979164123535R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox36.Style.Font.Bold = True
        Me.TextBox36.Style.Font.Name = "Tahoma"
        Me.TextBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox36.Value = "= Fields.CCosDescripcion"
        '
        'TextBox37
        '
        Me.TextBox37.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3011989593505859R), Telerik.Reporting.Drawing.Unit.Cm(1.4089422225952148R))
        Me.TextBox37.Name = "TextBox37"
        Me.TextBox37.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.89979887008667R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox37.Style.Font.Bold = True
        Me.TextBox37.Style.Font.Name = "Tahoma"
        Me.TextBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox37.Value = "= Fields.PrvRazonSocial"
        '
        'TextBox38
        '
        Me.TextBox38.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3011989593505859R), Telerik.Reporting.Drawing.Unit.Cm(1.7291423082351685R))
        Me.TextBox38.Name = "TextBox38"
        Me.TextBox38.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.89979887008667R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox38.Style.Font.Bold = True
        Me.TextBox38.Style.Font.Name = "Tahoma"
        Me.TextBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left
        Me.TextBox38.Value = "= Fields.AUTORIZA"
        '
        'groupFooterSection1
        '
        Me.groupFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.75882750749588013R)
        Me.groupFooterSection1.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.Shape2, Me.TextBox54, Me.TextBox55})
        Me.groupFooterSection1.Name = "groupFooterSection1"
        '
        'Shape2
        '
        Me.Shape2.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.099996566772461R), Telerik.Reporting.Drawing.Unit.Cm(0.1588275134563446R))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.ShapeType = New Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW)
        Me.Shape2.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.4078388214111328R), Telerik.Reporting.Drawing.Unit.Cm(0.13229165971279144R))
        '
        'TextBox54
        '
        Me.TextBox54.Format = ""
        Me.TextBox54.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.099996566772461R), Telerik.Reporting.Drawing.Unit.Cm(0.29131942987442017R))
        Me.TextBox54.Name = "TextBox54"
        Me.TextBox54.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0078418254852295R), Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134R))
        Me.TextBox54.Style.Font.Bold = True
        Me.TextBox54.Style.Font.Name = "Tahoma"
        Me.TextBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle
        Me.TextBox54.Value = "TOTAL DOC. RENDIDO"
        '
        'TextBox55
        '
        Me.TextBox55.CanGrow = True
        Me.TextBox55.Format = "{0:N2}"
        Me.TextBox55.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.1080379486084R), Telerik.Reporting.Drawing.Unit.Cm(0.29131942987442017R))
        Me.TextBox55.Name = "TextBox55"
        Me.TextBox55.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3998007774353027R), Telerik.Reporting.Drawing.Unit.Cm(0.29989990592002869R))
        Me.TextBox55.Style.Font.Bold = True
        Me.TextBox55.Style.Font.Name = "Tahoma"
        Me.TextBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox55.StyleName = "Data"
        Me.TextBox55.Value = "= Sum(TOTAL_DOCUMENTO)"
        '
        'grpCabeceraPago
        '
        Me.grpCabeceraPago.Height = Telerik.Reporting.Drawing.Unit.Cm(1.0395141839981079R)
        Me.grpCabeceraPago.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox30, Me.TextBox24, Me.TextBox26, Me.TextBox32, Me.TextBox33, Me.TextBox34, Me.TextBox46, Me.TextBox48, Me.TextBox11, Me.TextBox41, Me.TextBox40, Me.TextBox27, Me.TextBox25, Me.TextBox42, Me.TextBox29, Me.TextBox23, Me.TextBox31, Me.TextBox43, Me.TextBox50})
        Me.grpCabeceraPago.Name = "grpCabeceraPago"
        '
        'TextBox30
        '
        Me.TextBox30.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5076391696929932R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.TextBox30.Name = "TextBox30"
        Me.TextBox30.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7996004819869995R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox30.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox30.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox30.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox30.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox30.Style.Font.Bold = True
        Me.TextBox30.Style.Font.Name = "Tahoma"
        Me.TextBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox30.Value = "NRO TRANSF."
        '
        'TextBox24
        '
        Me.TextBox24.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.8002007007598877R), Telerik.Reporting.Drawing.Unit.Cm(-0.0000000010513596659933455R))
        Me.TextBox24.Name = "TextBox24"
        Me.TextBox24.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6999999284744263R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox24.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox24.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox24.Style.Font.Bold = True
        Me.TextBox24.Style.Font.Name = "Tahoma"
        Me.TextBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox24.Value = "F. PAGO"
        '
        'TextBox26
        '
        Me.TextBox26.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.30743932723999R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.TextBox26.Name = "TextBox26"
        Me.TextBox26.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2000001668930054R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox26.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox26.Style.Font.Bold = True
        Me.TextBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox26.Value = "MONEDA"
        '
        'TextBox32
        '
        Me.TextBox32.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5076389312744141R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.TextBox32.Name = "TextBox32"
        Me.TextBox32.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox32.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox32.Style.Font.Bold = True
        Me.TextBox32.Style.Font.Name = "Tahoma"
        Me.TextBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox32.Value = "MONTO"
        '
        'TextBox33
        '
        Me.TextBox33.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.00783920288086R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.TextBox33.Name = "TextBox33"
        Me.TextBox33.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.899600625038147R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox33.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox33.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox33.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox33.Style.Font.Bold = True
        Me.TextBox33.Style.Font.Name = "Tahoma"
        Me.TextBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox33.Value = "T. C"
        '
        'TextBox34
        '
        Me.TextBox34.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.90764045715332R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.TextBox34.Name = "TextBox34"
        Me.TextBox34.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7997994422912598R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox34.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox34.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox34.Style.Font.Bold = True
        Me.TextBox34.Style.Font.Name = "Tahoma"
        Me.TextBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox34.Value = "BENEFICIARIO"
        '
        'TextBox46
        '
        Me.TextBox46.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.2997989654541R), Telerik.Reporting.Drawing.Unit.Cm(0.71951413154602051R))
        Me.TextBox46.Name = "TextBox46"
        Me.TextBox46.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8000003099441528R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox46.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox46.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox46.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox46.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox46.Style.Font.Bold = True
        Me.TextBox46.Style.Font.Name = "Tahoma"
        Me.TextBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox46.Value = "F. RENDICION"
        '
        'TextBox48
        '
        Me.TextBox48.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.099998474121094R), Telerik.Reporting.Drawing.Unit.Cm(0.71951407194137573R))
        Me.TextBox48.Name = "TextBox48"
        Me.TextBox48.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8000003099441528R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox48.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox48.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox48.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox48.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox48.Style.Font.Bold = True
        Me.TextBox48.Style.Font.Name = "Tahoma"
        Me.TextBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox48.Value = "DOCUMENTO"
        '
        'TextBox11
        '
        Me.TextBox11.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707640647888184R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.TextBox11.Name = "TextBox11"
        Me.TextBox11.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.600396156311035R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox11.Style.Font.Bold = True
        Me.TextBox11.Style.Font.Name = "Tahoma"
        Me.TextBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox11.Value = "GLOSA"
        '
        'TextBox41
        '
        Me.TextBox41.Format = "{0:N2}"
        Me.TextBox41.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5076389312744141R), Telerik.Reporting.Drawing.Unit.Cm(0.32020008563995361R))
        Me.TextBox41.Name = "TextBox41"
        Me.TextBox41.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4999992847442627R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox41.Style.Font.Name = "Tahoma"
        Me.TextBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox41.Value = "= Fields.MONTOPAGO"
        '
        'TextBox40
        '
        Me.TextBox40.Format = "{0:d}"
        Me.TextBox40.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5076391696929932R), Telerik.Reporting.Drawing.Unit.Cm(0.32020008563995361R))
        Me.TextBox40.Name = "TextBox40"
        Me.TextBox40.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7996002435684204R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox40.Style.Font.Name = "Tahoma"
        Me.TextBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox40.Value = "= Fields.NroFormaPago"
        '
        'TextBox27
        '
        Me.TextBox27.Format = "{0:d}"
        Me.TextBox27.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.30743932723999R), Telerik.Reporting.Drawing.Unit.Cm(0.32020008563995361R))
        Me.TextBox27.Name = "TextBox27"
        Me.TextBox27.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1999994516372681R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox27.Style.Font.Name = "Tahoma"
        Me.TextBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox27.Value = "= Fields.MONEDAPAGO"
        '
        'TextBox25
        '
        Me.TextBox25.Format = "{0:d}"
        Me.TextBox25.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842R), Telerik.Reporting.Drawing.Unit.Cm(0.32020008563995361R))
        Me.TextBox25.Name = "TextBox25"
        Me.TextBox25.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6995996236801147R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox25.Style.Font.Name = "Tahoma"
        Me.TextBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox25.Value = "= Fields.FECHAMOVIMIENTO"
        '
        'TextBox42
        '
        Me.TextBox42.Format = "{0:N2}"
        Me.TextBox42.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.007838249206543R), Telerik.Reporting.Drawing.Unit.Cm(0.32020008563995361R))
        Me.TextBox42.Name = "TextBox42"
        Me.TextBox42.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8996015191078186R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox42.Style.Font.Name = "Tahoma"
        Me.TextBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox42.Value = "= Fields.TipoCambio"
        '
        'TextBox29
        '
        Me.TextBox29.Format = "{0:d}"
        Me.TextBox29.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.9076414108276367R), Telerik.Reporting.Drawing.Unit.Cm(0.32020008563995361R))
        Me.TextBox29.Name = "TextBox29"
        Me.TextBox29.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7997987270355225R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox29.Style.Font.Name = "Tahoma"
        Me.TextBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox29.Value = "= Fields.Beneficiario"
        '
        'TextBox23
        '
        Me.TextBox23.Format = "{0:d}"
        Me.TextBox23.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.707638740539551R), Telerik.Reporting.Drawing.Unit.Cm(0.32020008563995361R))
        Me.TextBox23.Name = "TextBox23"
        Me.TextBox23.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0992031097412109R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox23.Style.Font.Name = "Tahoma"
        Me.TextBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox23.Value = "= Fields.Glosa"
        '
        'TextBox31
        '
        Me.TextBox31.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.907638549804688R), Telerik.Reporting.Drawing.Unit.Cm(0.71951413154602051R))
        Me.TextBox31.Name = "TextBox31"
        Me.TextBox31.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8000003099441528R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox31.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox31.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox31.Style.Font.Bold = True
        Me.TextBox31.Style.Font.Name = "Tahoma"
        Me.TextBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox31.Value = "IMPORTE"
        '
        'TextBox43
        '
        Me.TextBox43.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.707836151123047R), Telerik.Reporting.Drawing.Unit.Cm(0.71941399574279785R))
        Me.TextBox43.Name = "TextBox43"
        Me.TextBox43.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8000003099441528R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox43.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox43.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox43.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox43.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox43.Style.Font.Bold = True
        Me.TextBox43.Style.Font.Name = "Tahoma"
        Me.TextBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox43.Value = "RENDIDO"
        '
        'TextBox50
        '
        Me.TextBox50.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.508035659790039R), Telerik.Reporting.Drawing.Unit.Cm(0.71941399574279785R))
        Me.TextBox50.Name = "TextBox50"
        Me.TextBox50.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8000003099441528R), Telerik.Reporting.Drawing.Unit.Cm(0.31999999284744263R))
        Me.TextBox50.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox50.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox50.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox50.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid
        Me.TextBox50.Style.Font.Bold = True
        Me.TextBox50.Style.Font.Name = "Tahoma"
        Me.TextBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5R)
        Me.TextBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox50.Value = "SALDO"
        '
        'pageHeaderSection1
        '
        Me.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(1.7314581871032715R)
        Me.pageHeaderSection1.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox1, Me.TextBox45, Me.txtRazonSocial, Me.txtRuc, Me.txtSubtitulo})
        Me.pageHeaderSection1.Name = "pageHeaderSection1"
        Me.pageHeaderSection1.Style.Font.Name = "Tahoma"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.6000003814697266R), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478R))
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.0919580459594727R), Telerik.Reporting.Drawing.Unit.Cm(0.40000006556510925R))
        Me.TextBox1.Style.Font.Bold = True
        Me.TextBox1.Style.Font.Name = "Tahoma"
        Me.TextBox1.StyleName = "Title"
        Me.TextBox1.Value = "GESTION DE DOCUMENTOS PENDIENTES - PAGOS"
        '
        'TextBox45
        '
        Me.TextBox45.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(22.399999618530273R), Telerik.Reporting.Drawing.Unit.Cm(0.64118003845214844R))
        Me.TextBox45.Name = "TextBox45"
        Me.TextBox45.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5999999046325684R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox45.Style.Font.Name = "Tahoma"
        Me.TextBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox45.Value = "= NOW()"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.40000006556510925R), Telerik.Reporting.Drawing.Unit.Cm(0.5R))
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.69999885559082R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.txtRazonSocial.Style.Font.Name = "Tahoma"
        Me.txtRazonSocial.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.txtRazonSocial.Value = ""
        '
        'txtRuc
        '
        Me.txtRuc.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.40000006556510925R), Telerik.Reporting.Drawing.Unit.Cm(0.77020019292831421R))
        Me.txtRuc.Name = "txtRuc"
        Me.txtRuc.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.69999885559082R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.txtRuc.Style.Font.Name = "Tahoma"
        Me.txtRuc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.txtRuc.Value = ""
        '
        'txtSubtitulo
        '
        Me.txtSubtitulo.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8000001907348633R), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045R))
        Me.txtSubtitulo.Name = "txtSubtitulo"
        Me.txtSubtitulo.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7000002861022949R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.txtSubtitulo.Style.Font.Bold = True
        Me.txtSubtitulo.Style.Font.Name = "Tahoma"
        Me.txtSubtitulo.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.0R)
        Me.txtSubtitulo.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.txtSubtitulo.Value = ""
        '
        'detail
        '
        Me.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(0.270200252532959R)
        Me.detail.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox47, Me.TextBox49, Me.TextBox51, Me.TextBox52, Me.TextBox53})
        Me.detail.Name = "detail"
        '
        'TextBox47
        '
        Me.TextBox47.Format = "{0:d}"
        Me.TextBox47.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.299999237060547R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.TextBox47.Name = "TextBox47"
        Me.TextBox47.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6995996236801147R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox47.Style.Font.Name = "Tahoma"
        Me.TextBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox47.Value = "= Fields.FECHA_RENDICION"
        '
        'TextBox49
        '
        Me.TextBox49.Format = "{0:d}"
        Me.TextBox49.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.099998474121094R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.TextBox49.Name = "TextBox49"
        Me.TextBox49.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999998331069946R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox49.Style.Font.Name = "Tahoma"
        Me.TextBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center
        Me.TextBox49.Value = "= Fields.SERIEDOCUMENTO"
        '
        'TextBox51
        '
        Me.TextBox51.Format = "{0:N2}"
        Me.TextBox51.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(19.907638549804688R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.TextBox51.Name = "TextBox51"
        Me.TextBox51.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999998331069946R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox51.Style.Font.Name = "Tahoma"
        Me.TextBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox51.Value = "= Fields.TOTAL_DOCUMENTO"
        '
        'TextBox52
        '
        Me.TextBox52.Format = "{0:N2}"
        Me.TextBox52.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(21.707836151123047R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.TextBox52.Name = "TextBox52"
        Me.TextBox52.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999998331069946R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox52.Style.Font.Name = "Tahoma"
        Me.TextBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox52.Value = "= Fields.IMPORTE_RENDIDO"
        '
        'TextBox53
        '
        Me.TextBox53.Format = "{0:N2}"
        Me.TextBox53.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.508035659790039R), Telerik.Reporting.Drawing.Unit.Cm(0.0R))
        Me.TextBox53.Name = "TextBox53"
        Me.TextBox53.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7999998331069946R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox53.Style.Font.Name = "Tahoma"
        Me.TextBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right
        Me.TextBox53.Value = "= Fields.SALDO_A_RENDIR"
        '
        'pageFooterSection1
        '
        Me.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Cm(0.7000001072883606R)
        Me.pageFooterSection1.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.TextBox44})
        Me.pageFooterSection1.Name = "pageFooterSection1"
        '
        'TextBox44
        '
        Me.TextBox44.Location = New Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(23.6080379486084R), Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666R))
        Me.TextBox44.Name = "TextBox44"
        Me.TextBox44.Size = New Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6999995708465576R), Telerik.Reporting.Drawing.Unit.Cm(0.27000001072883606R))
        Me.TextBox44.Style.Font.Name = "Tahoma"
        Me.TextBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.0R)
        Me.TextBox44.Value = "= PageNumber"
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.ConnectionString = "CapaPreTesoreria.My.MySettings.ERPDHMONTConnectionString"
        Me.SqlDataSource1.Name = "SqlDataSource1"
        Me.SqlDataSource1.Parameters.AddRange(New Telerik.Reporting.SqlDataSourceParameter() {New Telerik.Reporting.SqlDataSourceParameter("@Opcion", System.Data.DbType.Int32, "1"), New Telerik.Reporting.SqlDataSourceParameter("@DOCORIGEN", System.Data.DbType.[Decimal], "1"), New Telerik.Reporting.SqlDataSourceParameter("@ID_DOCPENDIENTE", System.Data.DbType.[Decimal], "6")})
        Me.SqlDataSource1.SelectCommand = "Tesoreria.uspReporteGestionDocumentosPagos"
        Me.SqlDataSource1.SelectCommandType = Telerik.Reporting.SqlDataSourceCommandType.StoredProcedure
        '
        'rptGestionDocumentosPagos
        '
        Me.DataSource = Me.SqlDataSource1
        Group1.GroupFooter = Me.groupFooterSection
        Group1.GroupHeader = Me.grpCabeceraDocumento
        Group1.Groupings.Add(New Telerik.Reporting.Grouping("=Fields.DocOrigen"))
        Group1.Groupings.Add(New Telerik.Reporting.Grouping("=Fields.ID_DocPendiente"))
        Group1.Name = "group"
        Group2.GroupFooter = Me.groupFooterSection1
        Group2.GroupHeader = Me.grpCabeceraPago
        Group2.Groupings.Add(New Telerik.Reporting.Grouping("=Fields.ID_DocPendiente"))
        Group2.Name = "group1"
        Me.Groups.AddRange(New Telerik.Reporting.Group() {Group1, Group2})
        Me.Items.AddRange(New Telerik.Reporting.ReportItemBase() {Me.grpCabeceraDocumento, Me.groupFooterSection, Me.grpCabeceraPago, Me.groupFooterSection1, Me.pageHeaderSection1, Me.detail, Me.pageFooterSection1})
        Me.Name = "rptGestionDocumentosPagos"
        Me.PageSettings.Landscape = True
        Me.PageSettings.Margins = New Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0.5R), Telerik.Reporting.Drawing.Unit.Mm(0.5R), Telerik.Reporting.Drawing.Unit.Mm(0.5R), Telerik.Reporting.Drawing.Unit.Mm(0.5R))
        Me.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4
        Me.Style.BackgroundColor = System.Drawing.Color.White
        StyleRule1.Selectors.AddRange(New Telerik.Reporting.Drawing.ISelector() {New Telerik.Reporting.Drawing.TypeSelector(GetType(Telerik.Reporting.TextItemBase)), New Telerik.Reporting.Drawing.TypeSelector(GetType(Telerik.Reporting.HtmlTextBox))})
        StyleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2.0R)
        StyleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2.0R)
        Me.StyleSheet.AddRange(New Telerik.Reporting.Drawing.StyleRule() {StyleRule1})
        Me.Width = Telerik.Reporting.Drawing.Unit.Cm(29.399999618530273R)
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents pageHeaderSection1 As Telerik.Reporting.PageHeaderSection
    Friend WithEvents detail As Telerik.Reporting.DetailSection
    Friend WithEvents pageFooterSection1 As Telerik.Reporting.PageFooterSection
    Friend WithEvents TextBox1 As Telerik.Reporting.TextBox
    Friend WithEvents SqlDataSource1 As Telerik.Reporting.SqlDataSource
    Friend WithEvents TextBox14 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox13 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox12 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox11 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox10 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox9 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox8 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox7 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox6 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox5 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox4 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox3 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox2 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox15 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox16 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox17 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox18 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox19 As Telerik.Reporting.TextBox
    Friend WithEvents grpCabeceraDocumento As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents groupFooterSection As Telerik.Reporting.GroupFooterSection
    Friend WithEvents TextBox20 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox21 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox22 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox23 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox24 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox25 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox27 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox26 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox30 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox32 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox33 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox34 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox29 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox35 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox36 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox37 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox38 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox40 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox41 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox42 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox44 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox45 As Telerik.Reporting.TextBox
    Friend WithEvents txtRazonSocial As Telerik.Reporting.TextBox
    Friend WithEvents txtRuc As Telerik.Reporting.TextBox
    Friend WithEvents txtSubtitulo As Telerik.Reporting.TextBox
    Private WithEvents TextBox28 As Telerik.Reporting.TextBox
    Friend WithEvents Shape1 As Telerik.Reporting.Shape
    Private WithEvents TextBox39 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox46 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox47 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox48 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox49 As Telerik.Reporting.TextBox
    Friend WithEvents grpCabeceraPago As Telerik.Reporting.GroupHeaderSection
    Friend WithEvents groupFooterSection1 As Telerik.Reporting.GroupFooterSection
    Friend WithEvents TextBox31 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox43 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox50 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox51 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox52 As Telerik.Reporting.TextBox
    Friend WithEvents TextBox53 As Telerik.Reporting.TextBox
    Friend WithEvents Shape2 As Telerik.Reporting.Shape
    Private WithEvents TextBox54 As Telerik.Reporting.TextBox
    Private WithEvents TextBox55 As Telerik.Reporting.TextBox
End Class