﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConceptosPlanillas
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn12 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn13 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn14 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn15 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn16 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn17 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn18 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim RadListDataItem1 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem2 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Me.RAD_PAGE = New Telerik.WinControls.UI.RadPageView()
        Me.PAGE_CONSULTA = New Telerik.WinControls.UI.RadPageViewPage()
        Me.btnNuevo = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.rgLista = New Telerik.WinControls.UI.RadGridView()
        Me.MECO = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MECO_DELETE = New System.Windows.Forms.ToolStripMenuItem()
        Me.PAGE_MANTENIMIENTO = New Telerik.WinControls.UI.RadPageViewPage()
        Me.RadGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadGroupBox5 = New Telerik.WinControls.UI.RadGroupBox()
        Me.lblPeriodo = New Telerik.WinControls.UI.RadLabel()
        Me.RadGroupBox6 = New Telerik.WinControls.UI.RadGroupBox()
        Me.chkMensual = New System.Windows.Forms.CheckBox()
        Me.chkSemanal = New System.Windows.Forms.CheckBox()
        Me.chkQuincenal = New System.Windows.Forms.CheckBox()
        Me.cboPeriodo = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel8 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.cboAño = New Telerik.WinControls.UI.RadDropDownList()
        Me.cboMes = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel4 = New Telerik.WinControls.UI.RadLabel()
        Me.btnCancelar = New Telerik.WinControls.UI.RadButton()
        Me.btnGuardar = New Telerik.WinControls.UI.RadButton()
        Me.RadLabel5 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel12 = New Telerik.WinControls.UI.RadLabel()
        Me.cboEstadoOperativo = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadGroupBox4 = New Telerik.WinControls.UI.RadGroupBox()
        Me.CboObra = New Telerik.WinControls.UI.RadDropDownList()
        Me.cboEmpresa = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel7 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel6 = New Telerik.WinControls.UI.RadLabel()
        Me.lblIdDetalle = New Telerik.WinControls.UI.RadLabel()
        Me.RadGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
        Me.txtImporte = New Telerik.WinControls.UI.RadTextBox()
        Me.txtGLosa = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel13 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.txtNumero = New Telerik.WinControls.UI.RadTextBox()
        Me.cboMonedas = New Telerik.WinControls.UI.RadDropDownList()
        Me.dtFecha = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.RadLabel10 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel9 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel11 = New Telerik.WinControls.UI.RadLabel()
        Me.cboConcepto = New Telerik.WinControls.UI.RadDropDownList()
        CType(Me.RAD_PAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RAD_PAGE.SuspendLayout()
        Me.PAGE_CONSULTA.SuspendLayout()
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me.rgLista, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgLista.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MECO.SuspendLayout()
        Me.PAGE_MANTENIMIENTO.SuspendLayout()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox2.SuspendLayout()
        CType(Me.RadGroupBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox5.SuspendLayout()
        CType(Me.lblPeriodo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox6.SuspendLayout()
        CType(Me.cboPeriodo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboAño, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboMes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstadoOperativo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox4.SuspendLayout()
        CType(Me.CboObra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblIdDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox3.SuspendLayout()
        CType(Me.txtImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGLosa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumero, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboMonedas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RAD_PAGE
        '
        Me.RAD_PAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RAD_PAGE.AutoScroll = True
        Me.RAD_PAGE.Controls.Add(Me.PAGE_CONSULTA)
        Me.RAD_PAGE.Controls.Add(Me.PAGE_MANTENIMIENTO)
        Me.RAD_PAGE.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RAD_PAGE.Location = New System.Drawing.Point(2, -1)
        Me.RAD_PAGE.Name = "RAD_PAGE"
        Me.RAD_PAGE.SelectedPage = Me.PAGE_CONSULTA
        Me.RAD_PAGE.Size = New System.Drawing.Size(990, 537)
        Me.RAD_PAGE.TabIndex = 1
        Me.RAD_PAGE.Text = "RadPageView1"
        CType(Me.RAD_PAGE.GetChildAt(0), Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None
        '
        'PAGE_CONSULTA
        '
        Me.PAGE_CONSULTA.Controls.Add(Me.btnNuevo)
        Me.PAGE_CONSULTA.Controls.Add(Me.RadGroupBox1)
        Me.PAGE_CONSULTA.Location = New System.Drawing.Point(10, 37)
        Me.PAGE_CONSULTA.Name = "PAGE_CONSULTA"
        Me.PAGE_CONSULTA.Size = New System.Drawing.Size(969, 489)
        Me.PAGE_CONSULTA.Text = "Consultas"
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Location = New System.Drawing.Point(838, 450)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(110, 24)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "Nuevo Ingreso"
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadGroupBox1.AutoSize = True
        Me.RadGroupBox1.Controls.Add(Me.rgLista)
        Me.RadGroupBox1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox1.HeaderText = "Lista de Conceptos"
        Me.RadGroupBox1.Location = New System.Drawing.Point(4, 4)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(962, 430)
        Me.RadGroupBox1.TabIndex = 1
        Me.RadGroupBox1.Text = "Lista de Conceptos"
        '
        'rgLista
        '
        Me.rgLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rgLista.BackColor = System.Drawing.Color.FromArgb(CType(CType(233, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.rgLista.ContextMenuStrip = Me.MECO
        Me.rgLista.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgLista.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.rgLista.ForeColor = System.Drawing.Color.Black
        Me.rgLista.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgLista.Location = New System.Drawing.Point(5, 21)
        '
        'rgLista
        '
        Me.rgLista.MasterTemplate.AllowAddNewRow = False
        GridViewTextBoxColumn1.FieldName = "ID_PAGO_PLANILLA"
        GridViewTextBoxColumn1.HeaderText = "Nº PAGO"
        GridViewTextBoxColumn1.Name = "ID_PAGO_PLANILLA"
        GridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn1.Width = 70
        GridViewTextBoxColumn2.FieldName = "ID_CONCEPTO"
        GridViewTextBoxColumn2.HeaderText = "ID_CONCEPTO"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "ID_CONCEPTO"
        GridViewTextBoxColumn3.FieldName = "CONCEPTO"
        GridViewTextBoxColumn3.HeaderText = "CONCEPTO"
        GridViewTextBoxColumn3.Name = "CONCEPTO"
        GridViewTextBoxColumn3.Width = 150
        GridViewTextBoxColumn4.FieldName = "ID_EMPRESA"
        GridViewTextBoxColumn4.HeaderText = "ID_EMPRESA"
        GridViewTextBoxColumn4.IsVisible = False
        GridViewTextBoxColumn4.Name = "ID_EMPRESA"
        GridViewTextBoxColumn5.FieldName = "EmprDescripcion"
        GridViewTextBoxColumn5.HeaderText = "EMPRESA"
        GridViewTextBoxColumn5.Name = "EmprDescripcion"
        GridViewTextBoxColumn5.Width = 200
        GridViewTextBoxColumn6.FieldName = "ID_OBRA"
        GridViewTextBoxColumn6.HeaderText = "ID_OBRA"
        GridViewTextBoxColumn6.IsVisible = False
        GridViewTextBoxColumn6.Name = "ID_OBRA"
        GridViewTextBoxColumn7.FieldName = "ObraDescripcion"
        GridViewTextBoxColumn7.HeaderText = "OBRA"
        GridViewTextBoxColumn7.Name = "ObraDescripcion"
        GridViewTextBoxColumn7.Width = 200
        GridViewTextBoxColumn8.FieldName = "FECHA"
        GridViewTextBoxColumn8.FormatString = "{0:dd/MM/yyyy}"
        GridViewTextBoxColumn8.HeaderText = "FECHA"
        GridViewTextBoxColumn8.Name = "FECHA"
        GridViewTextBoxColumn8.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn8.Width = 80
        GridViewTextBoxColumn9.FieldName = "ID_MONEDA"
        GridViewTextBoxColumn9.HeaderText = "ID_MONEDA"
        GridViewTextBoxColumn9.IsVisible = False
        GridViewTextBoxColumn9.Name = "ID_MONEDA"
        GridViewTextBoxColumn10.FieldName = "MonDescripcion"
        GridViewTextBoxColumn10.HeaderText = "MONEDA"
        GridViewTextBoxColumn10.Name = "MonDescripcion"
        GridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn10.Width = 100
        GridViewTextBoxColumn11.FieldName = "IMPORTE"
        GridViewTextBoxColumn11.HeaderText = "IMPORTE"
        GridViewTextBoxColumn11.Name = "IMPORTE"
        GridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn11.Width = 80
        GridViewTextBoxColumn12.FieldName = "GLOSA"
        GridViewTextBoxColumn12.HeaderText = "GLOSA"
        GridViewTextBoxColumn12.Name = "GLOSA"
        GridViewTextBoxColumn12.Width = 200
        GridViewTextBoxColumn13.FieldName = "AÑO"
        GridViewTextBoxColumn13.HeaderText = "AÑO"
        GridViewTextBoxColumn13.Name = "AÑO"
        GridViewTextBoxColumn13.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn13.Width = 70
        GridViewTextBoxColumn14.FieldName = "ID_PERIODO_MES"
        GridViewTextBoxColumn14.HeaderText = "ID_PERIODO_MES"
        GridViewTextBoxColumn14.IsVisible = False
        GridViewTextBoxColumn14.Name = "ID_PERIODO_MES"
        GridViewTextBoxColumn14.Width = 70
        GridViewTextBoxColumn15.FieldName = "MES"
        GridViewTextBoxColumn15.HeaderText = "MES"
        GridViewTextBoxColumn15.Name = "MES"
        GridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn15.Width = 100
        GridViewTextBoxColumn16.FieldName = "ID_PERIODO"
        GridViewTextBoxColumn16.HeaderText = "ID_PERIODO"
        GridViewTextBoxColumn16.IsVisible = False
        GridViewTextBoxColumn16.Name = "ID_PERIODO"
        GridViewTextBoxColumn17.FieldName = "DETALLE"
        GridViewTextBoxColumn17.HeaderText = "PERIODO"
        GridViewTextBoxColumn17.Name = "DETALLE"
        GridViewTextBoxColumn17.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn17.Width = 100
        GridViewTextBoxColumn18.FieldName = "FLAG"
        GridViewTextBoxColumn18.HeaderText = "FLAG"
        GridViewTextBoxColumn18.Name = "FLAG"
        GridViewTextBoxColumn18.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        Me.rgLista.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8, GridViewTextBoxColumn9, GridViewTextBoxColumn10, GridViewTextBoxColumn11, GridViewTextBoxColumn12, GridViewTextBoxColumn13, GridViewTextBoxColumn14, GridViewTextBoxColumn15, GridViewTextBoxColumn16, GridViewTextBoxColumn17, GridViewTextBoxColumn18})
        Me.rgLista.MasterTemplate.EnableFiltering = True
        Me.rgLista.Name = "rgLista"
        Me.rgLista.ReadOnly = True
        Me.rgLista.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgLista.ShowGroupPanel = False
        Me.rgLista.Size = New System.Drawing.Size(953, 389)
        Me.rgLista.TabIndex = 0
        Me.rgLista.Text = "RadGridView1"
        '
        'MECO
        '
        Me.MECO.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MECO_DELETE})
        Me.MECO.Name = "MECO"
        Me.MECO.Size = New System.Drawing.Size(119, 26)
        '
        'MECO_DELETE
        '
        Me.MECO_DELETE.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.MECO_DELETE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_DELETE.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_DELETE.Name = "MECO_DELETE"
        Me.MECO_DELETE.Size = New System.Drawing.Size(118, 22)
        Me.MECO_DELETE.Text = "Eliminar"
        '
        'PAGE_MANTENIMIENTO
        '
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.RadGroupBox2)
        Me.PAGE_MANTENIMIENTO.Location = New System.Drawing.Point(10, 37)
        Me.PAGE_MANTENIMIENTO.Name = "PAGE_MANTENIMIENTO"
        Me.PAGE_MANTENIMIENTO.Size = New System.Drawing.Size(969, 489)
        Me.PAGE_MANTENIMIENTO.Text = "Mantenimiento"
        '
        'RadGroupBox2
        '
        Me.RadGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox2.Controls.Add(Me.RadGroupBox5)
        Me.RadGroupBox2.Controls.Add(Me.btnCancelar)
        Me.RadGroupBox2.Controls.Add(Me.btnGuardar)
        Me.RadGroupBox2.Controls.Add(Me.RadLabel5)
        Me.RadGroupBox2.Controls.Add(Me.RadLabel12)
        Me.RadGroupBox2.Controls.Add(Me.cboEstadoOperativo)
        Me.RadGroupBox2.Controls.Add(Me.RadGroupBox4)
        Me.RadGroupBox2.Controls.Add(Me.lblIdDetalle)
        Me.RadGroupBox2.Controls.Add(Me.RadGroupBox3)
        Me.RadGroupBox2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox2.HeaderText = "Conceptos y Planillas"
        Me.RadGroupBox2.Location = New System.Drawing.Point(8, 7)
        Me.RadGroupBox2.Name = "RadGroupBox2"
        Me.RadGroupBox2.Size = New System.Drawing.Size(938, 388)
        Me.RadGroupBox2.TabIndex = 24
        Me.RadGroupBox2.Text = "Conceptos y Planillas"
        '
        'RadGroupBox5
        '
        Me.RadGroupBox5.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox5.Controls.Add(Me.lblPeriodo)
        Me.RadGroupBox5.Controls.Add(Me.RadGroupBox6)
        Me.RadGroupBox5.Controls.Add(Me.cboPeriodo)
        Me.RadGroupBox5.Controls.Add(Me.RadLabel8)
        Me.RadGroupBox5.Controls.Add(Me.RadLabel3)
        Me.RadGroupBox5.Controls.Add(Me.cboAño)
        Me.RadGroupBox5.Controls.Add(Me.cboMes)
        Me.RadGroupBox5.Controls.Add(Me.RadLabel4)
        Me.RadGroupBox5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox5.HeaderText = "Periodo"
        Me.RadGroupBox5.Location = New System.Drawing.Point(378, 26)
        Me.RadGroupBox5.Name = "RadGroupBox5"
        Me.RadGroupBox5.Size = New System.Drawing.Size(555, 104)
        Me.RadGroupBox5.TabIndex = 42
        Me.RadGroupBox5.Text = "Periodo"
        '
        'lblPeriodo
        '
        Me.lblPeriodo.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo.Location = New System.Drawing.Point(404, 36)
        Me.lblPeriodo.Name = "lblPeriodo"
        Me.lblPeriodo.Size = New System.Drawing.Size(2, 2)
        Me.lblPeriodo.TabIndex = 44
        '
        'RadGroupBox6
        '
        Me.RadGroupBox6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox6.Controls.Add(Me.chkMensual)
        Me.RadGroupBox6.Controls.Add(Me.chkSemanal)
        Me.RadGroupBox6.Controls.Add(Me.chkQuincenal)
        Me.RadGroupBox6.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox6.HeaderText = ""
        Me.RadGroupBox6.Location = New System.Drawing.Point(11, 17)
        Me.RadGroupBox6.Name = "RadGroupBox6"
        Me.RadGroupBox6.Size = New System.Drawing.Size(314, 34)
        Me.RadGroupBox6.TabIndex = 43
        '
        'chkMensual
        '
        Me.chkMensual.AutoSize = True
        Me.chkMensual.Location = New System.Drawing.Point(237, 9)
        Me.chkMensual.Name = "chkMensual"
        Me.chkMensual.Size = New System.Drawing.Size(71, 17)
        Me.chkMensual.TabIndex = 46
        Me.chkMensual.Text = "Mensual"
        Me.chkMensual.UseVisualStyleBackColor = True
        '
        'chkSemanal
        '
        Me.chkSemanal.AutoSize = True
        Me.chkSemanal.Location = New System.Drawing.Point(16, 9)
        Me.chkSemanal.Name = "chkSemanal"
        Me.chkSemanal.Size = New System.Drawing.Size(70, 17)
        Me.chkSemanal.TabIndex = 44
        Me.chkSemanal.Text = "Semanal"
        Me.chkSemanal.UseVisualStyleBackColor = True
        '
        'chkQuincenal
        '
        Me.chkQuincenal.AutoSize = True
        Me.chkQuincenal.Location = New System.Drawing.Point(121, 9)
        Me.chkQuincenal.Name = "chkQuincenal"
        Me.chkQuincenal.Size = New System.Drawing.Size(78, 17)
        Me.chkQuincenal.TabIndex = 45
        Me.chkQuincenal.Text = "Quincenal"
        Me.chkQuincenal.UseVisualStyleBackColor = True
        '
        'cboPeriodo
        '
        Me.cboPeriodo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboPeriodo.Location = New System.Drawing.Point(404, 59)
        Me.cboPeriodo.Name = "cboPeriodo"
        Me.cboPeriodo.Size = New System.Drawing.Size(125, 20)
        Me.cboPeriodo.TabIndex = 43
        '
        'RadLabel8
        '
        Me.RadLabel8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel8.Location = New System.Drawing.Point(348, 60)
        Me.RadLabel8.Name = "RadLabel8"
        Me.RadLabel8.Size = New System.Drawing.Size(54, 18)
        Me.RadLabel8.TabIndex = 42
        Me.RadLabel8.Text = "Periodo :"
        '
        'RadLabel3
        '
        Me.RadLabel3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel3.Location = New System.Drawing.Point(11, 62)
        Me.RadLabel3.Name = "RadLabel3"
        Me.RadLabel3.Size = New System.Drawing.Size(37, 18)
        Me.RadLabel3.TabIndex = 35
        Me.RadLabel3.Text = "Año  :"
        '
        'cboAño
        '
        Me.cboAño.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboAño.Location = New System.Drawing.Point(51, 60)
        Me.cboAño.Name = "cboAño"
        Me.cboAño.Size = New System.Drawing.Size(77, 20)
        Me.cboAño.TabIndex = 41
        '
        'cboMes
        '
        Me.cboMes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboMes.Location = New System.Drawing.Point(201, 60)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(115, 20)
        Me.cboMes.TabIndex = 40
        '
        'RadLabel4
        '
        Me.RadLabel4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel4.Location = New System.Drawing.Point(161, 61)
        Me.RadLabel4.Name = "RadLabel4"
        Me.RadLabel4.Size = New System.Drawing.Size(37, 18)
        Me.RadLabel4.TabIndex = 37
        Me.RadLabel4.Text = "Mes  :"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(752, 161)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(124, 31)
        Me.btnCancelar.TabIndex = 28
        Me.btnCancelar.Text = "Cancelar"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(577, 161)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(124, 31)
        Me.btnGuardar.TabIndex = 27
        Me.btnGuardar.Text = "Guardar"
        '
        'RadLabel5
        '
        Me.RadLabel5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel5.Location = New System.Drawing.Point(737, 409)
        Me.RadLabel5.Name = "RadLabel5"
        Me.RadLabel5.Size = New System.Drawing.Size(57, 18)
        Me.RadLabel5.TabIndex = 39
        Me.RadLabel5.Text = "Periodo  :"
        '
        'RadLabel12
        '
        Me.RadLabel12.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel12.Location = New System.Drawing.Point(673, 343)
        Me.RadLabel12.Name = "RadLabel12"
        Me.RadLabel12.Size = New System.Drawing.Size(104, 18)
        Me.RadLabel12.TabIndex = 26
        Me.RadLabel12.Text = "Estado Operativo :"
        Me.RadLabel12.Visible = False
        '
        'cboEstadoOperativo
        '
        Me.cboEstadoOperativo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        RadListDataItem1.Text = "OPERATIVO"
        RadListDataItem1.TextWrap = True
        RadListDataItem2.Text = "NO OPERATIVO"
        RadListDataItem2.TextWrap = True
        Me.cboEstadoOperativo.Items.Add(RadListDataItem1)
        Me.cboEstadoOperativo.Items.Add(RadListDataItem2)
        Me.cboEstadoOperativo.Location = New System.Drawing.Point(782, 343)
        Me.cboEstadoOperativo.Name = "cboEstadoOperativo"
        Me.cboEstadoOperativo.Size = New System.Drawing.Size(94, 20)
        Me.cboEstadoOperativo.TabIndex = 27
        Me.cboEstadoOperativo.Visible = False
        '
        'RadGroupBox4
        '
        Me.RadGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox4.Controls.Add(Me.CboObra)
        Me.RadGroupBox4.Controls.Add(Me.cboEmpresa)
        Me.RadGroupBox4.Controls.Add(Me.RadLabel7)
        Me.RadGroupBox4.Controls.Add(Me.RadLabel6)
        Me.RadGroupBox4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox4.HeaderText = "Ubicación "
        Me.RadGroupBox4.Location = New System.Drawing.Point(17, 26)
        Me.RadGroupBox4.Name = "RadGroupBox4"
        Me.RadGroupBox4.Size = New System.Drawing.Size(352, 104)
        Me.RadGroupBox4.TabIndex = 1
        Me.RadGroupBox4.Text = "Ubicación "
        '
        'CboObra
        '
        Me.CboObra.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.CboObra.Location = New System.Drawing.Point(80, 59)
        Me.CboObra.Name = "CboObra"
        Me.CboObra.Size = New System.Drawing.Size(256, 20)
        Me.CboObra.TabIndex = 3
        '
        'cboEmpresa
        '
        Me.cboEmpresa.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboEmpresa.Location = New System.Drawing.Point(80, 27)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(254, 20)
        Me.cboEmpresa.TabIndex = 2
        '
        'RadLabel7
        '
        Me.RadLabel7.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel7.Location = New System.Drawing.Point(34, 59)
        Me.RadLabel7.Name = "RadLabel7"
        Me.RadLabel7.Size = New System.Drawing.Size(39, 18)
        Me.RadLabel7.TabIndex = 1
        Me.RadLabel7.Text = "Obra :"
        '
        'RadLabel6
        '
        Me.RadLabel6.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel6.Location = New System.Drawing.Point(15, 27)
        Me.RadLabel6.Name = "RadLabel6"
        Me.RadLabel6.Size = New System.Drawing.Size(58, 18)
        Me.RadLabel6.TabIndex = 0
        Me.RadLabel6.Text = "Empresa :"
        '
        'lblIdDetalle
        '
        Me.lblIdDetalle.Location = New System.Drawing.Point(254, 23)
        Me.lblIdDetalle.Name = "lblIdDetalle"
        Me.lblIdDetalle.Size = New System.Drawing.Size(2, 2)
        Me.lblIdDetalle.TabIndex = 30
        '
        'RadGroupBox3
        '
        Me.RadGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox3.Controls.Add(Me.txtImporte)
        Me.RadGroupBox3.Controls.Add(Me.txtGLosa)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel2)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel13)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox3.Controls.Add(Me.txtNumero)
        Me.RadGroupBox3.Controls.Add(Me.cboMonedas)
        Me.RadGroupBox3.Controls.Add(Me.dtFecha)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel10)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel9)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel11)
        Me.RadGroupBox3.Controls.Add(Me.cboConcepto)
        Me.RadGroupBox3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox3.HeaderText = "Conceptos "
        Me.RadGroupBox3.Location = New System.Drawing.Point(17, 136)
        Me.RadGroupBox3.Name = "RadGroupBox3"
        Me.RadGroupBox3.Size = New System.Drawing.Size(485, 183)
        Me.RadGroupBox3.TabIndex = 25
        Me.RadGroupBox3.Text = "Conceptos "
        '
        'txtImporte
        '
        Me.txtImporte.Location = New System.Drawing.Point(342, 86)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(126, 20)
        Me.txtImporte.TabIndex = 34
        '
        'txtGLosa
        '
        Me.txtGLosa.AutoSize = False
        Me.txtGLosa.Location = New System.Drawing.Point(80, 119)
        Me.txtGLosa.Multiline = True
        Me.txtGLosa.Name = "txtGLosa"
        Me.txtGLosa.Size = New System.Drawing.Size(388, 52)
        Me.txtGLosa.TabIndex = 2
        '
        'RadLabel2
        '
        Me.RadLabel2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel2.Location = New System.Drawing.Point(28, 119)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(45, 18)
        Me.RadLabel2.TabIndex = 0
        Me.RadLabel2.Text = "Glosa  :"
        '
        'RadLabel13
        '
        Me.RadLabel13.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel13.Location = New System.Drawing.Point(281, 86)
        Me.RadLabel13.Name = "RadLabel13"
        Me.RadLabel13.Size = New System.Drawing.Size(55, 18)
        Me.RadLabel13.TabIndex = 33
        Me.RadLabel13.Text = "Importe :"
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.Location = New System.Drawing.Point(17, 23)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(56, 18)
        Me.RadLabel1.TabIndex = 16
        Me.RadLabel1.Text = "Número :"
        '
        'txtNumero
        '
        Me.txtNumero.Enabled = False
        Me.txtNumero.Location = New System.Drawing.Point(80, 25)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(84, 20)
        Me.txtNumero.TabIndex = 17
        '
        'cboMonedas
        '
        Me.cboMonedas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboMonedas.Location = New System.Drawing.Point(80, 86)
        Me.cboMonedas.Name = "cboMonedas"
        Me.cboMonedas.Size = New System.Drawing.Size(182, 20)
        Me.cboMonedas.TabIndex = 23
        '
        'dtFecha
        '
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha.Location = New System.Drawing.Point(342, 57)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(126, 20)
        Me.dtFecha.TabIndex = 25
        Me.dtFecha.TabStop = False
        Me.dtFecha.Text = "09/03/2015"
        Me.dtFecha.Value = New Date(2015, 3, 9, 12, 9, 7, 474)
        '
        'RadLabel10
        '
        Me.RadLabel10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel10.Location = New System.Drawing.Point(17, 87)
        Me.RadLabel10.Name = "RadLabel10"
        Me.RadLabel10.Size = New System.Drawing.Size(56, 18)
        Me.RadLabel10.TabIndex = 22
        Me.RadLabel10.Text = "Moneda :"
        '
        'RadLabel9
        '
        Me.RadLabel9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel9.Location = New System.Drawing.Point(10, 55)
        Me.RadLabel9.Name = "RadLabel9"
        Me.RadLabel9.Size = New System.Drawing.Size(63, 18)
        Me.RadLabel9.TabIndex = 20
        Me.RadLabel9.Text = "Concepto :"
        '
        'RadLabel11
        '
        Me.RadLabel11.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel11.Location = New System.Drawing.Point(290, 57)
        Me.RadLabel11.Name = "RadLabel11"
        Me.RadLabel11.Size = New System.Drawing.Size(46, 18)
        Me.RadLabel11.TabIndex = 24
        Me.RadLabel11.Text = "Fecha  :"
        '
        'cboConcepto
        '
        Me.cboConcepto.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboConcepto.Location = New System.Drawing.Point(80, 55)
        Me.cboConcepto.Name = "cboConcepto"
        Me.cboConcepto.Size = New System.Drawing.Size(182, 20)
        Me.cboConcepto.TabIndex = 21
        '
        'FrmConceptosPlanillas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 545)
        Me.Controls.Add(Me.RAD_PAGE)
        Me.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "FrmConceptosPlanillas"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.Text = "Pago de Planillas"
        CType(Me.RAD_PAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RAD_PAGE.ResumeLayout(False)
        Me.PAGE_CONSULTA.ResumeLayout(False)
        Me.PAGE_CONSULTA.PerformLayout()
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        CType(Me.rgLista.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MECO.ResumeLayout(False)
        Me.PAGE_MANTENIMIENTO.ResumeLayout(False)
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox2.ResumeLayout(False)
        Me.RadGroupBox2.PerformLayout()
        CType(Me.RadGroupBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox5.ResumeLayout(False)
        Me.RadGroupBox5.PerformLayout()
        CType(Me.lblPeriodo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox6.ResumeLayout(False)
        Me.RadGroupBox6.PerformLayout()
        CType(Me.cboPeriodo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboAño, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboMes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstadoOperativo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox4.ResumeLayout(False)
        Me.RadGroupBox4.PerformLayout()
        CType(Me.CboObra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblIdDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox3.ResumeLayout(False)
        Me.RadGroupBox3.PerformLayout()
        CType(Me.txtImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGLosa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumero, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboMonedas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RAD_PAGE As Telerik.WinControls.UI.RadPageView
    Friend WithEvents PAGE_CONSULTA As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents btnNuevo As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents rgLista As Telerik.WinControls.UI.RadGridView
    Friend WithEvents PAGE_MANTENIMIENTO As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents btnCancelar As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnGuardar As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadGroupBox3 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents txtGLosa As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadGroupBox4 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents CboObra As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents cboEmpresa As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel7 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel6 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadGroupBox2 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents lblIdDetalle As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboEstadoOperativo As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel12 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents dtFecha As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents RadLabel11 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboMonedas As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel10 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboConcepto As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel9 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtNumero As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtImporte As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel13 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents MECO As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MECO_DELETE As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RadGroupBox5 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboAño As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents cboMes As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel4 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel5 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents chkMensual As System.Windows.Forms.CheckBox
    Friend WithEvents chkQuincenal As System.Windows.Forms.CheckBox
    Friend WithEvents chkSemanal As System.Windows.Forms.CheckBox
    Friend WithEvents cboPeriodo As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel8 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadGroupBox6 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents lblPeriodo As Telerik.WinControls.UI.RadLabel
End Class

