﻿Imports BusinessLogicLayer.Conceptos_Planillas
Imports CapaEntidad
Imports CapaEntidad.Conceptos_Planillas

Public Class FrmConceptosPlanillas

#Region "DECLARACIONES"

    Dim VL_BOTON As String = ""
    Dim VL_CONCEPTOS As New SrvConceptosPlanillas
    Dim OBJ_CONCEPTO As New BeanConceptoPlanillas

    Public Const AvisoMSGBOX As String = "SISTEMA TESORERIA"
#End Region

    Private Sub FrmConceptosPlanillas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RAD_PAGE.SelectedPage = PAGE_CONSULTA
        PAGE_MANTENIMIENTO.Enabled = False
        PAGE_CONSULTA.Enabled = True

        LISTAR_EMPRESA()
        LISTAR_CONCEPTOS()
        LISTAR_MONEDAS()
        LISTAR_PAGO_PLANILLAS()
        LISTAR_PERIODOS_MES()
        dtFecha.Value = Now
        AÑO()
        cboAño.SelectedIndex = 25
    End Sub

#Region "LISTAR EMPRESA Y OBRA"
    Private Sub LISTAR_EMPRESA()

        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_CONCEPTOS.SRV_LISTAR_EMPRESA()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboEmpresa.DataSource = VL_BeanResultado.dtResultado
            Me.cboEmpresa.DisplayMember = "EmprDescripcion"
            Me.cboEmpresa.ValueMember = "EmprCodigo"
        End If
    End Sub

    Private Sub LISTAR_OBRAS(ID_EMPRESA As String)

        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_CONCEPTOS.SRV_LISTAR_OBRAS(ID_EMPRESA)

        If VL_BeanResultado.blnExiste = True Then
            Me.CboObra.DataSource = VL_BeanResultado.dtResultado
            Me.CboObra.DisplayMember = "ObraDescripcion"
            Me.CboObra.ValueMember = "ObraCodigo"
        End If
    End Sub

    Private Sub LISTAR_PERIODOS(ID_PERIODO As Int16)

        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_CONCEPTOS.SRV_LISTAR_PERIODOS(ID_PERIODO)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboPeriodo.DataSource = VL_BeanResultado.dtResultado
            Me.cboPeriodo.DisplayMember = "DETALLE"
            Me.cboPeriodo.ValueMember = "ID_CORRELATIVO"
        End If
    End Sub

    Private Sub LISTAR_PERIODOS_MES()

        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_CONCEPTOS.SRV_LISTAR_PERIODOS(1)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboMes.DataSource = VL_BeanResultado.dtResultado
            Me.cboMes.DisplayMember = "DETALLE"
            Me.cboMes.ValueMember = "ID_CORRELATIVO"
        End If
    End Sub

    Private Sub LISTAR_MONEDAS()

        VL_CONCEPTOS = New SrvConceptosPlanillas()
        'dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_CONCEPTOS.SRV_LISTAR_MONEDAS()
        If VL_BeanResultado.blnExiste = True Then
            Me.cboMonedas.DataSource = VL_BeanResultado.dtResultado
            Me.cboMonedas.DisplayMember = "MonDescripcion"
            Me.cboMonedas.ValueMember = "MonCodigo"

        End If
    End Sub

    Private Sub LISTAR_CONCEPTOS()

        VL_CONCEPTOS = New SrvConceptosPlanillas()
        'dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_CONCEPTOS.SRV_LISTAR_CONCEPTOS_PLANILLAS()
        If VL_BeanResultado.blnExiste = True Then
            Me.cboConcepto.DataSource = VL_BeanResultado.dtResultado
            Me.cboConcepto.DisplayMember = "CONCEPTO"
            Me.cboConcepto.ValueMember = "ID_CONCEPTO"

        End If
    End Sub
#End Region

#Region "PAGO PLANILLAS LISTAR, AGREGAR, ACTUALIZAR Y ELIMINAR"
    Private Sub LISTAR_PAGO_PLANILLAS()

        VL_CONCEPTOS = New SrvConceptosPlanillas
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_CONCEPTOS.SRV_LISTAR_PAGO_PLANILLAS()

        If VL_BeanResultado.blnExiste = True Then
            Me.rgLista.DataSource = VL_BeanResultado.dtResultado

        End If
    End Sub

    Private Sub AGREGAR()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        Dim VL_FLAG As Int16 = 0

        If chkSemanal.Checked = True Then
            VL_FLAG = 1
        End If
        If chkQuincenal.Checked = True Then
            VL_FLAG = 2
        End If
        If chkMensual.Checked = True Then
            VL_FLAG = 3
        End If

        OBJ_CONCEPTO.ID_CONCEPTO = cboConcepto.SelectedValue
        OBJ_CONCEPTO.ID_EMPRESA = cboEmpresa.SelectedValue
        OBJ_CONCEPTO.ID_OBRA = CboObra.SelectedValue
        OBJ_CONCEPTO.FECHA = dtFecha.Value
        OBJ_CONCEPTO.ID_MONEDA = cboMonedas.SelectedValue
        OBJ_CONCEPTO.IMPORTE = txtImporte.Text
        OBJ_CONCEPTO.GLOSA = txtGLosa.Text
        OBJ_CONCEPTO.AÑO = cboAño.Text
        OBJ_CONCEPTO.ID_MES = cboMes.SelectedValue
        OBJ_CONCEPTO.ID_PERIODO = cboPeriodo.SelectedValue
        OBJ_CONCEPTO.FLAG = VL_FLAG

        VL_BeanResultado = VL_CONCEPTOS.SRV_AGREGAR(OBJ_CONCEPTO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ACTUALIZAR()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_FLAG As Int16 = 0

        If chkSemanal.Checked = True Then
            VL_FLAG = 1
        End If
        If chkQuincenal.Checked = True Then
            VL_FLAG = 2
        End If
        If chkMensual.Checked = True Then
            VL_FLAG = 3
        End If

        OBJ_CONCEPTO.ID_PAGO_PLANILLA = rgLista.CurrentRow.Cells("ID_PAGO_PLANILLA").Value
        OBJ_CONCEPTO.ID_CONCEPTO = cboConcepto.SelectedValue
        OBJ_CONCEPTO.ID_EMPRESA = cboEmpresa.SelectedValue
        OBJ_CONCEPTO.ID_OBRA = CboObra.SelectedValue
        OBJ_CONCEPTO.FECHA = dtFecha.Value
        OBJ_CONCEPTO.ID_MONEDA = cboMonedas.SelectedValue
        OBJ_CONCEPTO.IMPORTE = txtImporte.Text
        OBJ_CONCEPTO.GLOSA = txtGLosa.Text
        OBJ_CONCEPTO.AÑO = cboAño.Text
        OBJ_CONCEPTO.ID_MES = cboMes.SelectedValue
        OBJ_CONCEPTO.ID_PERIODO = cboPeriodo.SelectedValue
        OBJ_CONCEPTO.FLAG = VL_FLAG

        VL_BeanResultado = VL_CONCEPTOS.SRV_ACTUALIZAR(OBJ_CONCEPTO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ELIMINAR()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        OBJ_CONCEPTO.ID_PAGO_PLANILLA = rgLista.CurrentRow.Cells("ID_PAGO_PLANILLA").Value

        Dim ID As Int16 = OBJ_CONCEPTO.ID_PAGO_PLANILLA

        VL_BeanResultado = VL_CONCEPTOS.SRV_ELIMINAR(ID)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

#End Region

#Region "BOTONES"
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        RAD_PAGE.SelectedPage = PAGE_MANTENIMIENTO
        PAGE_MANTENIMIENTO.Enabled = True
        PAGE_CONSULTA.Enabled = False

        VL_BOTON = "0"
        lblPeriodo.Text = ""
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If VL_BOTON = "0" Then
            If cboPeriodo.Text = "" Then
                MessageBox.Show("Ingrese PERIODO...!!", "SISTEMA INVENTARIO")
                Return
            ElseIf txtImporte.Text = "" Then
                MessageBox.Show("Ingrese IMPORTE...!!", "SISTEMA INVENTARIO")
                Return
            Else
                AGREGAR()
                LISTAR_PAGO_PLANILLAS()
            End If
        End If
            If VL_BOTON = "1" Then
                If cboPeriodo.Text = "" Then
                MessageBox.Show("Ingrese PERIODO..'", "SISTEMA INVENTARIO")
                Return
            ElseIf txtImporte.Text = "" Then
                MessageBox.Show("Ingrese IMPORTE...!!", "SISTEMA INVENTARIO")
                Return
            Else
                ACTUALIZAR()
                LISTAR_PAGO_PLANILLAS()
            End If
        End If

            RAD_PAGE.SelectedPage = PAGE_CONSULTA
            PAGE_CONSULTA.Enabled = True
            PAGE_MANTENIMIENTO.Enabled = False
            LIMPIAR()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        RAD_PAGE.SelectedPage = PAGE_CONSULTA
        PAGE_CONSULTA.Enabled = True
        PAGE_MANTENIMIENTO.Enabled = False
        LIMPIAR()
    End Sub
#End Region

    Sub LIMPIAR()
        txtGLosa.Text = ""
        txtImporte.Text = ""
        txtNumero.Text = ""
        cboMonedas.SelectedIndex = 0
        cboEmpresa.SelectedIndex = 0
        cboConcepto.SelectedIndex = 0
        cboAño.SelectedIndex = 25
        cboMes.SelectedIndex = 0
        chkMensual.Checked = False
        chkQuincenal.Checked = False
        chkSemanal.Checked = False
        lblPeriodo.Text = ""
    End Sub
    Private Sub cboEmpresa_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedValueChanged
        If cboEmpresa.SelectedValue IsNot Nothing Then
            If cboEmpresa.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                LISTAR_OBRAS(cboEmpresa.SelectedValue)
            End If
        End If
    End Sub

    Private Sub MECO_DELETE_Click(sender As Object, e As EventArgs) Handles MECO_DELETE.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR()
            LISTAR_PAGO_PLANILLAS()
        End If
    End Sub

    Private Sub rgLista_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles rgLista.CellDoubleClick
        VL_BOTON = "1"
       RAD_PAGE.SelectedPage = PAGE_MANTENIMIENTO
        PAGE_MANTENIMIENTO.Enabled = True
        PAGE_CONSULTA.Enabled = False

        txtNumero.Text = rgLista.CurrentRow.Cells("ID_PAGO_PLANILLA").Value
        cboConcepto.SelectedValue = rgLista.CurrentRow.Cells("ID_CONCEPTO").Value
        cboEmpresa.SelectedValue = rgLista.CurrentRow.Cells("ID_EMPRESA").Value
        CboObra.SelectedValue = rgLista.CurrentRow.Cells("ID_OBRA").Value
        dtFecha.Value = rgLista.CurrentRow.Cells("FECHA").Value
        txtImporte.Text = rgLista.CurrentRow.Cells("IMPORTE").Value
        txtGLosa.Text = rgLista.CurrentRow.Cells("GLOSA").Value
        cboMonedas.SelectedValue = rgLista.CurrentRow.Cells("ID_MONEDA").Value
        cboAño.Text = rgLista.CurrentRow.Cells("AÑO").Value
        cboMes.SelectedValue = rgLista.CurrentRow.Cells("ID_PERIODO_MES").Value

        Dim VL_FLAG As Int16 = rgLista.CurrentRow.Cells("FLAG").Value
        Dim VL_IDPERIODO As Int16
        If VL_FLAG = 1 Then
            chkSemanal.Checked = True
            VL_IDPERIODO = 3
        End If
        If VL_FLAG = 2 Then
            chkQuincenal.Checked = True
            VL_IDPERIODO = 2
        End If
        If VL_FLAG = 3 Then
            chkMensual.Checked = True
            VL_IDPERIODO = 1
        End If
        LISTAR_PERIODOS(VL_IDPERIODO)
        cboPeriodo.SelectedValue = rgLista.CurrentRow.Cells("ID_PERIODO").Value
        lblPeriodo.Text = rgLista.CurrentRow.Cells("DETALLE").Value

    End Sub

#Region "OTROS"
    Private Sub txtImporte_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtImporte.KeyPress
        If InStr(1, "0123456789.-" & Chr(8), e.KeyChar) = 0 Then
            e.KeyChar = ""
        End If
    End Sub

    Sub AÑO()
        For i As Integer = 1990 To 2030
            cboAño.Items.Add(i)
        Next
    End Sub

#End Region

#Region "CHECK EVENTOS"
    Private Sub chkMensual_CheckedChanged(sender As Object, e As EventArgs) Handles chkMensual.CheckedChanged
        chkQuincenal.Checked = False
        chkSemanal.Checked = False

        If chkMensual.Checked = True Then
            LISTAR_PERIODOS(1)
        Else
            cboPeriodo.Text = ""
            LISTAR_PERIODOS(0)
        End If
    End Sub

#End Region

    Private Sub chkQuincenal_CheckedChanged(sender As Object, e As EventArgs) Handles chkQuincenal.CheckedChanged
        chkMensual.Checked = False
        chkSemanal.Checked = False

        If chkQuincenal.Checked = True Then
            LISTAR_PERIODOS(2)
        Else
            cboPeriodo.Text = ""
            LISTAR_PERIODOS(0)
        End If
    End Sub

    Private Sub chkSemanal_CheckedChanged(sender As Object, e As EventArgs) Handles chkSemanal.CheckedChanged
        chkQuincenal.Checked = False
        chkMensual.Checked = False

        If chkSemanal.Checked = True Then
            LISTAR_PERIODOS(3)
        Else
            cboPeriodo.Text = ""
            LISTAR_PERIODOS(0)
        End If
    End Sub

End Class
