﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmItemVentas))
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.TabPersonalizado1 = New Controles_DHMont.TabPersonalizado()
        Me.Page_Consulta = New System.Windows.Forms.TabPage()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.rgListaItem = New Telerik.WinControls.UI.RadGridView()
        Me.MECO = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MECO_MODIFICAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_ELIMINAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.Page_Mantenimiento = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtNumItem = New System.Windows.Forms.TextBox()
        Me.cboEstado = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblTexto = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtItem = New System.Windows.Forms.TextBox()
        Me.cboCuentaCtble = New System.Windows.Forms.ComboBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.TabPersonalizado1.SuspendLayout()
        Me.Page_Consulta.SuspendLayout()
        CType(Me.rgListaItem, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaItem.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MECO.SuspendLayout()
        Me.Page_Mantenimiento.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabPersonalizado1
        '
        Me.TabPersonalizado1.Controls.Add(Me.Page_Consulta)
        Me.TabPersonalizado1.Controls.Add(Me.Page_Mantenimiento)
        Me.TabPersonalizado1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPersonalizado1.Location = New System.Drawing.Point(9, 10)
        Me.TabPersonalizado1.Name = "TabPersonalizado1"
        Me.TabPersonalizado1.SelectedIndex = 0
        Me.TabPersonalizado1.Size = New System.Drawing.Size(636, 373)
        Me.TabPersonalizado1.TabIndex = 2
        '
        'Page_Consulta
        '
        Me.Page_Consulta.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Page_Consulta.Controls.Add(Me.btnNuevo)
        Me.Page_Consulta.Controls.Add(Me.rgListaItem)
        Me.Page_Consulta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Page_Consulta.Location = New System.Drawing.Point(4, 22)
        Me.Page_Consulta.Name = "Page_Consulta"
        Me.Page_Consulta.Padding = New System.Windows.Forms.Padding(3)
        Me.Page_Consulta.Size = New System.Drawing.Size(628, 347)
        Me.Page_Consulta.TabIndex = 0
        Me.Page_Consulta.Text = "Consultas"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.SystemColors.HighlightText
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(519, 305)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(78, 23)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'rgListaItem
        '
        Me.rgListaItem.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.rgListaItem.ContextMenuStrip = Me.MECO
        Me.rgListaItem.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgListaItem.Location = New System.Drawing.Point(0, 0)
        '
        'rgListaItem
        '
        Me.rgListaItem.MasterTemplate.AllowAddNewRow = False
        Me.rgListaItem.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn1.FieldName = "ID_Item"
        GridViewTextBoxColumn1.HeaderText = "Nº Item"
        GridViewTextBoxColumn1.Name = "ID_Item"
        GridViewTextBoxColumn1.Width = 80
        GridViewTextBoxColumn2.FieldName = "Item"
        GridViewTextBoxColumn2.HeaderText = "Item"
        GridViewTextBoxColumn2.Name = "Item"
        GridViewTextBoxColumn2.Width = 200
        GridViewTextBoxColumn3.FieldName = "ID_Cuenta"
        GridViewTextBoxColumn3.HeaderText = "ID_Cuenta"
        GridViewTextBoxColumn3.IsVisible = False
        GridViewTextBoxColumn3.Name = "ID_Cuenta"
        GridViewTextBoxColumn4.FieldName = "CUENTA"
        GridViewTextBoxColumn4.HeaderText = "Cuenta Contable"
        GridViewTextBoxColumn4.Name = "CUENTA"
        GridViewTextBoxColumn4.Width = 200
        GridViewTextBoxColumn5.FieldName = "Estado"
        GridViewTextBoxColumn5.HeaderText = "id_Estado"
        GridViewTextBoxColumn5.IsVisible = False
        GridViewTextBoxColumn5.Name = "Estado"
        GridViewTextBoxColumn5.Width = 80
        GridViewTextBoxColumn6.FieldName = "Est_Texto"
        GridViewTextBoxColumn6.HeaderText = "Estado"
        GridViewTextBoxColumn6.Name = "Est_Texto"
        GridViewTextBoxColumn6.Width = 100
        Me.rgListaItem.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6})
        Me.rgListaItem.Name = "rgListaItem"
        Me.rgListaItem.ReadOnly = True
        Me.rgListaItem.Size = New System.Drawing.Size(626, 291)
        Me.rgListaItem.TabIndex = 0
        Me.rgListaItem.Text = "RadGridView1"
        '
        'MECO
        '
        Me.MECO.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MECO_MODIFICAR, Me.MECO_ELIMINAR})
        Me.MECO.Name = "MECO"
        Me.MECO.Size = New System.Drawing.Size(128, 48)
        '
        'MECO_MODIFICAR
        '
        Me.MECO_MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_MODIFICAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_MODIFICAR.Image = CType(resources.GetObject("MECO_MODIFICAR.Image"), System.Drawing.Image)
        Me.MECO_MODIFICAR.Name = "MECO_MODIFICAR"
        Me.MECO_MODIFICAR.Size = New System.Drawing.Size(127, 22)
        Me.MECO_MODIFICAR.Text = "Modificar"
        '
        'MECO_ELIMINAR
        '
        Me.MECO_ELIMINAR.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.MECO_ELIMINAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_ELIMINAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_ELIMINAR.Name = "MECO_ELIMINAR"
        Me.MECO_ELIMINAR.Size = New System.Drawing.Size(127, 22)
        Me.MECO_ELIMINAR.Text = "Eliminar"
        '
        'Page_Mantenimiento
        '
        Me.Page_Mantenimiento.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Page_Mantenimiento.Controls.Add(Me.GroupBox1)
        Me.Page_Mantenimiento.Controls.Add(Me.btnCancelar)
        Me.Page_Mantenimiento.Controls.Add(Me.btnGuardar)
        Me.Page_Mantenimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Page_Mantenimiento.Location = New System.Drawing.Point(4, 22)
        Me.Page_Mantenimiento.Name = "Page_Mantenimiento"
        Me.Page_Mantenimiento.Padding = New System.Windows.Forms.Padding(3)
        Me.Page_Mantenimiento.Size = New System.Drawing.Size(628, 347)
        Me.Page_Mantenimiento.TabIndex = 1
        Me.Page_Mantenimiento.Text = "Mantenimiento"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtNumItem)
        Me.GroupBox1.Controls.Add(Me.cboEstado)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblTexto)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtItem)
        Me.GroupBox1.Controls.Add(Me.cboCuentaCtble)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(616, 170)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        '
        'txtNumItem
        '
        Me.txtNumItem.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.txtNumItem.Enabled = False
        Me.txtNumItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumItem.Location = New System.Drawing.Point(72, 19)
        Me.txtNumItem.Name = "txtNumItem"
        Me.txtNumItem.Size = New System.Drawing.Size(140, 20)
        Me.txtNumItem.TabIndex = 4
        '
        'cboEstado
        '
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.Items.AddRange(New Object() {"ACTIVADO", "NO ACTIVADO"})
        Me.cboEstado.Location = New System.Drawing.Point(72, 122)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(140, 21)
        Me.cboEstado.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(13, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nº Item:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(8, 126)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Estado :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(24, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Item :"
        '
        'lblTexto
        '
        Me.lblTexto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTexto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTexto.Location = New System.Drawing.Point(293, 83)
        Me.lblTexto.Name = "lblTexto"
        Me.lblTexto.Size = New System.Drawing.Size(303, 70)
        Me.lblTexto.TabIndex = 11
        Me.lblTexto.Text = "Label5"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(8, 89)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Cuenta :"
        '
        'txtItem
        '
        Me.txtItem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItem.Location = New System.Drawing.Point(72, 53)
        Me.txtItem.Name = "txtItem"
        Me.txtItem.Size = New System.Drawing.Size(524, 20)
        Me.txtItem.TabIndex = 5
        '
        'cboCuentaCtble
        '
        Me.cboCuentaCtble.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentaCtble.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCuentaCtble.FormattingEnabled = True
        Me.cboCuentaCtble.Items.AddRange(New Object() {"1", "2"})
        Me.cboCuentaCtble.Location = New System.Drawing.Point(72, 85)
        Me.cboCuentaCtble.Name = "cboCuentaCtble"
        Me.cboCuentaCtble.Size = New System.Drawing.Size(205, 21)
        Me.cboCuentaCtble.TabIndex = 7
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.SystemColors.HighlightText
        Me.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(431, 192)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(99, 23)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.SystemColors.HighlightText
        Me.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnGuardar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.Location = New System.Drawing.Point(162, 192)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(100, 23)
        Me.btnGuardar.TabIndex = 8
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'frmItemVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(650, 395)
        Me.Controls.Add(Me.TabPersonalizado1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmItemVentas"
        Me.Text = "Item Ventas"
        Me.TabPersonalizado1.ResumeLayout(False)
        Me.Page_Consulta.ResumeLayout(False)
        CType(Me.rgListaItem.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaItem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MECO.ResumeLayout(False)
        Me.Page_Mantenimiento.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabPersonalizado1 As Controles_DHMont.TabPersonalizado
    Friend WithEvents Page_Consulta As System.Windows.Forms.TabPage
    Friend WithEvents rgListaItem As Telerik.WinControls.UI.RadGridView
    Friend WithEvents Page_Mantenimiento As System.Windows.Forms.TabPage
    Friend WithEvents lblTexto As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents cboCuentaCtble As System.Windows.Forms.ComboBox
    Friend WithEvents txtItem As System.Windows.Forms.TextBox
    Friend WithEvents txtNumItem As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents cboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents MECO As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MECO_MODIFICAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MECO_ELIMINAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
