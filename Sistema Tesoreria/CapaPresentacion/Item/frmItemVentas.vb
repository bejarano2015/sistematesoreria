﻿Imports CapaEntidad.Item
Imports BusinessLogicLayer.Item
Imports CapaEntidad

Public Class frmItemVentas

    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim VL_SRV_ITEM As SrvItem
    Dim OBJ_ITEM As New BeanItem
    Dim VL_BOTON As String
    Public Const AvisoMSGBOX As String = "AVISO SISTEMA TESORERIA"

    Private Sub frmItemVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TabPersonalizado1.DisablePage(Page_Mantenimiento)
        LISTAR_CUENTAS_CONTABLES()
        LISTAR_ITEM()
        cboEstado.SelectedIndex = "0"

    End Sub

#Region "LISTAR , AGREGAR, MODIFICAR Y ELIMINAR "
    Private Sub LISTAR_CUENTAS_CONTABLES()

        VL_SRV_ITEM = New SrvItem()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_ITEM.SRV_LISTAR_CUENTAS()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboCuentaCtble.DataSource = VL_BeanResultado.dtResultado
            Me.cboCuentaCtble.DisplayMember = "CUENTA"
            Me.cboCuentaCtble.ValueMember = "ID_CTA_CTBLE"
        End If

    End Sub
    Private Sub LISTAR_ITEM()

        VL_SRV_ITEM = New SrvItem()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_ITEM.SRV_LISTAR_ITEM()

        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaItem.DataSource = VL_BeanResultado.dtResultado

        End If

    End Sub

    Private Sub AGREGAR_ITEM()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ESTADO As String = cboEstado.SelectedItem

        If (VL_ESTADO = "ACTIVADO") Then
            VL_ESTADO = "1"
        Else
            VL_ESTADO = "0"
        End If

        OBJ_ITEM = New BeanItem(0, txtItem.Text, cboCuentaCtble.SelectedValue.ToString(), VL_ESTADO, 1)

        VL_BeanResultado = VL_SRV_ITEM.SRV_AGREGAR_ITEM(OBJ_ITEM)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub MODIFICAR_ITEM()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ESTADO As String = cboEstado.SelectedItem

        If (VL_ESTADO = "ACTIVADO") Then
            VL_ESTADO = "1"
        Else
            VL_ESTADO = "0"
        End If

        OBJ_ITEM = New BeanItem(txtNumItem.Text, txtItem.Text, cboCuentaCtble.SelectedValue.ToString(), VL_ESTADO, 2)

        VL_BeanResultado = VL_SRV_ITEM.SRV_ACTUALIZAR_ITEM(OBJ_ITEM)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

    Private Sub ELIMINAR_ITEM()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim ID_ITEM As String
        ID_ITEM = rgListaItem.CurrentRow.Cells("ID_Item").Value

        OBJ_ITEM = New BeanItem(ID_ITEM, "", "", "", 3)

        VL_BeanResultado = VL_SRV_ITEM.SRV_ACTUALIZAR_ITEM(OBJ_ITEM)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

#End Region

    Sub limpiarcontroles()
        txtNumItem.Text = ""
        txtItem.Text = ""
        lblTexto.Text = ""
        cboCuentaCtble.SelectedIndex = "0"
        cboEstado.SelectedIndex = "0"
    End Sub

#Region "BOTONES"
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        TabPersonalizado1.SelectedIndex = 1
        TabPersonalizado1.EnablePage(Page_Mantenimiento)
        VL_BOTON = "0"
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        If (VL_BOTON = "0") Then  ''AGREGAR IMPUESTO
            If txtItem.Text = "" Then
                MessageBox.Show("Ingrese Item...", "SISTEMA TESORERIA")
            Else
                AGREGAR_ITEM()
                LISTAR_ITEM()
                limpiarcontroles()
                TabPersonalizado1.SelectedIndex = 0
            End If

        End If

        If (VL_BOTON = "1") Then ''EDITAR IMPUESTO
            If txtItem.Text = "" Then
                MessageBox.Show("Ingrese Item...", "SISTEMA TESORERIA")
            Else
                MODIFICAR_ITEM()
                LISTAR_ITEM()
                limpiarcontroles()
                TabPersonalizado1.SelectedIndex = 0
            End If
        End If

        VL_BOTON = ""
        TabPersonalizado1.DisablePage(Page_Mantenimiento)

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        TabPersonalizado1.DisablePage(Page_Mantenimiento)
        TabPersonalizado1.SelectedIndex = 0
        limpiarcontroles()

    End Sub

#End Region

#Region "MENU CONTEXTUAL"
    Private Sub MECO_MODIFICAR_Click(sender As Object, e As EventArgs) Handles MECO_MODIFICAR.Click
        VL_BOTON = "1"
        TabPersonalizado1.SelectedIndex = 1
        TabPersonalizado1.EnablePage(Page_Mantenimiento)

        txtNumItem.Text = rgListaItem.CurrentRow.Cells("ID_Item").Value
        txtItem.Text = rgListaItem.CurrentRow.Cells("Item").Value
        cboEstado.SelectedValue = rgListaItem.CurrentRow.Cells("Estado").Value
        cboCuentaCtble.SelectedValue = rgListaItem.CurrentRow.Cells("ID_Cuenta").Value
    End Sub

    Private Sub MECO_ELIMINAR_Click(sender As Object, e As EventArgs) Handles MECO_ELIMINAR.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR_ITEM()
            LISTAR_ITEM()
        End If
    End Sub

#End Region

    Private Sub cboCuentaCtble_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboCuentaCtble.SelectedValueChanged
        lblTexto.Text = cboCuentaCtble.Text
    End Sub

End Class