Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmValorizaciones
    Dim sMaxValor As Integer = 0
    Dim RetencionPorcentaje As Double = 0
    Private eValorizaciones As clsValorizaciones
    Dim iCerrarValo As String = ""
    Private ePagoProveedores As clsPagoProveedores

    Dim VL_TIPORETENCION As String

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmValorizaciones = Nothing
    Public Shared Function Instance() As frmValorizaciones
        If frmInstance Is Nothing Then
            frmInstance = New frmValorizaciones
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmValorizaciones_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmValorizaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eValorizaciones = New clsValorizaciones
        BeLabel12.Text = "Saldo Contra Acta de Recepci�n ( %)"
        'cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today())

        If Trim(glbUsuCategoria) = "A" Then
            cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today(), "")
        ElseIf Trim(glbUsuCategoria) = "U" Then
            cboObra.DataSource = eValorizaciones.fListarObras(73, gEmpresa, gPeriodo, "", gUsuario, Today(), "")
        End If

        If eValorizaciones.iNroRegistros > 0 Then
            cboObra.ValueMember = "ObraCodigo"
            cboObra.DisplayMember = "ObraDescripcion"
            cboObra.SelectedIndex = -1
        End If
        'dtpEnvio.Value = Now.Date().ToShortDateString()
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        dtpFechaFin.Value = Now.Date().ToShortDateString()
        'AxMonthView1.Value = Now.Date().ToShortDateString()
        dtpEnvio.Value = Now.Date()
        'dtpEnvio.Value = Now.Date()
        btnGrabar.Enabled = False
        btnModificar.Enabled = False
        btnGrabar.Enabled = False
        btnImprimir.Enabled = False

        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
            'dgvDetalle.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvDetalle.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
            'dgvDetalle.Columns(x).ReadOnly = True
            'dgvDetalle.Columns(x).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next

    End Sub

    Private Sub cboObra_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboObra.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            If Trim(glbUsuCategoria) = "A" Then
                cboContratista.DataSource = eValorizaciones.fListarObras(1, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")
            ElseIf Trim(glbUsuCategoria) = "U" Then
                cboContratista.DataSource = eValorizaciones.fListarObras(74, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")
            End If
            If eValorizaciones.iNroRegistros > 0 Then
                cboContratista.ValueMember = "IdContratista"
                cboContratista.DisplayMember = "Nombre"
                cboContratista.SelectedIndex = -1

                CCosto.DataSource = Nothing
                CCosto.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
            End If
        End If

        'txtCodigoVal.Clear()
        'txtNumeroOrden.Clear()
        'txtNroContrato.Clear()
        'txtNumSesion.Clear()
        'txtSemana.Clear()
        'txtUbicacion.Clear()
        'chkCerrar.Checked = False
        'chkCerrar.Enabled = True
        'dtpFechaIni.Value = Now.Date().ToShortDateString()
        'dtpFechaFin.Value = Now.Date().ToShortDateString()
        'BeLabel11.Text = "              Valorizaci�n N�                 "
        'cboPartidas.DataSource = Nothing
        'cboPartidas.SelectedIndex = -1
        'Calcular()
        'BeLabel12.Text = "Retenci�n Fondo De Garant�a ( %)"
        'For x As Integer = 0 To dgvDescuentos.RowCount - 1
        '    dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
        'Next
        Limpiar()
    End Sub

    Sub Limpiar()

        'cboObra_SelectionChangeCommitted(sender, e)
        'Calcular()
        txtUbicacion.Clear()
        txtNumeroOrden.Clear()
        txtNroContrato.Clear()

        txtCodigoVal.Clear()
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        dtpFechaFin.Value = Now.Date().ToShortDateString()

        'txtNumeroOrden.Clear()
        'txtDescripcion.Clear()
        'txtNroContrato.Clear()
        txtNumSesion.Clear()
        txtSemana.Clear()
        dtpEnvio.Value = Now.Date().ToShortDateString()
        'txtUbicacion.Clear()
        'txtRetPor.Clear()
        chkCerrar.Checked = False
        chkCerrar.Enabled = True
        
        'cboObra.SelectedIndex = -1
        'cboContratista.SelectedIndex = -1
        'cboContratista.SelectedIndex = -1
        'cboPartidas.SelectedIndex = -1
        'HabilitarControles()
        'dtpEnvio.Value = Now.Date()
        'cboObra.Focus()

        For x As Integer = 0 To dgvDescuentos.RowCount - 1
            dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
        Next
        txtAdelanto.Text = "0.00"
        txtAdelanto1.Text = "0.00"
        txtAdelanto2.Text = "0.00"
        txtAdelanto3.Text = "0.00"
        txtAmorti1.Text = "0.00"
        txtAmorti2.Text = "0.00"
        txtAmorti3.Text = "0.00"
        txtAmorti4.Text = "0.00"

        txtFG1.Text = "0.00"
        txtFG2.Text = "0.00"
        txtFG3.Text = "0.00"
        txtFG4.Text = "0.00"

        txtOtroDes.Text = "0.00"
        'txtDevFG.Text = "0.00"
        txtTotPagar.Text = "0.00"
        
        
       
        txtCuadrilla1.Text = "0.00"
        txtCuadrilla2.Text = "0.00"
        txtCuadrilla3.Text = "0.00"
        txtCuadrilla4.Text = "0.00"
        txtTotDes.Text = "0.00"

        txtCuadrilla1Pre.Text = "0.00"
        txtCuadrilla2Pre.Text = "0.00"
        txtCuadrilla3Pre.Text = "0.00"
        txtCuadrilla1T.Text = "0.00"
        txtCuadrilla2T.Text = "0.00"
        txtCuadrilla3T.Text = "0.00"
        txtCuadrillaTotales.Text = "0.00"

        btnGrabar.Enabled = True
        btnModificar.Enabled = False
        btnImprimir.Enabled = False
        BeLabel12.Text = "Saldo Contra Acta de Recepci�n ( %)"
        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
        BeLabel11.Text = "              Valorizaci�n N�                 "

        txtSubTotal.Text = "0.00"
        txtIGV.Text = "0.00"
        txtTotal.Text = "0.00"
        txtAA1.Text = "0.00"
        txtAA2.Text = "0.00"
        txtAA3.Text = "0.00"
        txtSubTotalVal.Text = "0.00"
        txtSubIGVVal.Text = "0.00"
        txtTotalVal.Text = "0.00"
        txtRetPor.Text = "0.00"
        txtAdelanto.Text = "0.00"
        txtOtroDes.Text = "0.00"
        txtTotPagar.Text = "0.00"
        txtSubTotalSaldo.Text = "0.00"
        txtSubTotalIGV.Text = "0.00"
        txtSubTotalTot.Text = "0.00"
    End Sub
    Private Sub cboPartidas_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectedValueChanged
        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
    End Sub

    Private Sub cboPartidas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectionChangeCommitted
        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
        Limpiar()
        If cboPartidas.SelectedIndex <> -1 Then
            Dim dtTable2 As DataTable
            dtTable2 = New DataTable
            eValorizaciones = New clsValorizaciones
            dtTable2 = eValorizaciones.fListarObras(2, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If dtTable2.Rows.Count > 0 Then
                RetencionPorcentaje = dtTable2.Rows(0).Item("xret")
                BeLabel12.Text = "Saldo Contra Acta de Recepci�n (" & RetencionPorcentaje & "%)"
                BeLabel11.Text = "              Valorizaci�n N�                 "
                txtUbicacion.Text = UCase(dtTable2.Rows(0).Item("ubtDescripcion"))
                txtNumeroOrden.Text = UCase(dtTable2.Rows(0).Item("NumOrden"))
                txtNroContrato.Text = UCase(dtTable2.Rows(0).Item("NContrato"))
                'aqui se muestra el numero actual de la valorizacion o el siguiente a crear
                'txtNumSesion.Text = sMaxValor
                txtSemana.Clear()
                txtSemana.Focus()
            End If
        Else
            txtSemana.Clear()
            txtSemana.Focus()
        End If


        'txtCodigoVal.Clear()
        ''txtNumeroOrden.Clear()
        ''txtNroContrato.Clear()
        'txtNumSesion.Clear()
        ''txtSemana.Clear()
        ''txtUbicacion.Clear()
        'chkCerrar.Checked = False
        'chkCerrar.Enabled = True
        'dtpFechaIni.Value = Now.Date().ToShortDateString()
        'dtpFechaFin.Value = Now.Date().ToShortDateString()
        'BeLabel11.Text = "              Valorizaci�n N�                 "
        'Calcular()
        'txtSemana.Clear()
        'txtSemana.Focus()
        ' Limpiar()
    End Sub

    Sub Calcular()
        'Format(Math.Round(dblSoles, 0), "#,##0.00")
        If dgvDetalle.Rows.Count > 0 Then
            Dim DblTotal As Double = 0
            Dim DblTotalVal As Double = 0
            Dim DblTotalSaldo As Double = 0
            Dim DblTotalAvanceAnt As Double = 0
            Dim Contador As Integer = 0

            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                If Len(dgvDetalle.Rows(x).Cells("M2").Value) > 0 And Len(dgvDetalle.Rows(x).Cells("PU").Value) > 0 Then

                    'MessageBox.Show((dgvDetalle.Rows(x).Cells("M2").Value))
                    'MessageBox.Show((dgvDetalle.Rows(x).Cells("PU").Value))
                    dgvDetalle.Rows(x).Cells("Parcial").Value = Format((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("PU").Value), "#,##0.00")
                End If

                'If Len(dgvDetalle.Rows(x).Cells("V1").Value) > 0 Then
                '    dgvDetalle.Rows(x).Cells("V2").Value = Format(((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("V1").Value)) / (dgvDetalle.Rows(x).Cells("M1").Value), "#,##0.00")
                '    dgvDetalle.Rows(x).Cells("V3").Value = Format((dgvDetalle.Rows(x).Cells("V2").Value) * (dgvDetalle.Rows(x).Cells("PU").Value), "#,##0.00")
                'End If

                'If Len(dgvDetalle.Rows(x).Cells("V2").Value) > 0 Then
                '    dgvDetalle.Rows(x).Cells("V1").Value = Format(((dgvDetalle.Rows(x).Cells("M1").Value) * (dgvDetalle.Rows(x).Cells("V2").Value)) / (dgvDetalle.Rows(x).Cells("M2").Value), "#,##0.00")
                '    dgvDetalle.Rows(x).Cells("V3").Value = Format((dgvDetalle.Rows(x).Cells("V2").Value) * (dgvDetalle.Rows(x).Cells("PU").Value), "#,##0.00")
                'End If

                If Len(dgvDetalle.Rows(x).Cells("Parcial").Value) = 0 Then
                    DblTotal = DblTotal + 0
                Else
                    DblTotal = DblTotal + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("Parcial").Value)))
                End If

                If Len(dgvDetalle.Rows(x).Cells("V3").Value) = 0 Then
                    DblTotalVal = DblTotalVal + 0
                Else
                    DblTotalVal = DblTotalVal + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("V3").Value)))
                End If

                'If Len(dgvDetalle.Rows(x).Cells("V1").Value) Then
                'MessageBox.Show(dgvDetalle.Rows(x).Cells("M1").Value)
                'MessageBox.Show(dgvDetalle.Rows(x).Cells("AA1").Value)
                'MessageBox.Show(dgvDetalle.Rows(x).Cells("V1").Value)

                dgvDetalle.Rows(x).Cells("S1").Value = Format(Convert.ToDouble((dgvDetalle.Rows(x).Cells("M1").Value)) - (Convert.ToDouble((dgvDetalle.Rows(x).Cells("AA1").Value)) + (Convert.ToDouble(dgvDetalle.Rows(x).Cells("V1").Value))), "#,##0.0")
                dgvDetalle.Rows(x).Cells("S2").Value = Format(Convert.ToDouble((dgvDetalle.Rows(x).Cells("M2").Value)) - (Convert.ToDouble((dgvDetalle.Rows(x).Cells("AA2").Value)) + (Convert.ToDouble(dgvDetalle.Rows(x).Cells("V2").Value))), "#,##0.00")
                dgvDetalle.Rows(x).Cells("S3").Value = Format(Convert.ToDouble((dgvDetalle.Rows(x).Cells("Parcial").Value)) - ((Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA3").Value)) + (Convert.ToDouble(dgvDetalle.Rows(x).Cells("V3").Value))), "#,##0.00")

                dgvDetalle.Rows(x).Cells("MONTacumulado").Value = Format(Convert.ToDouble(dgvDetalle.Rows(x).Cells("MONTacumulado").Value), "#,##0.00")
                dgvDetalle.Rows(x).Cells("vametrado").Value = Format(Convert.ToDouble(dgvDetalle.Rows(x).Cells("vametrado").Value), "#,##0.00")
                dgvDetalle.Rows(x).Cells("vapor").Value = Format(Convert.ToDouble(dgvDetalle.Rows(x).Cells("vapor").Value), "#,##0.00")

                'dgvDetalle.Rows(x).Cells("S1").Value = (Convert.ToDouble((dgvDetalle.Rows(x).Cells("AA1").Value)) + (Convert.ToDouble(dgvDetalle.Rows(x).Cells("V1").Value))), "#,##0.00")
                'dgvDetalle.Rows(x).Cells("S2").Value =  (Convert.ToDouble((dgvDetalle.Rows(x).Cells("AA2").Value)) + (Convert.ToDouble(dgvDetalle.Rows(x).Cells("V2").Value))), "#,##0.00")
                'dgvDetalle.Rows(x).Cells("S3").Value =  ((Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA3").Value)) + (Convert.ToDouble(dgvDetalle.Rows(x).Cells("V3").Value))), "#,##0.00")

                If Len(dgvDetalle.Rows(x).Cells("S3").Value) = 0 Then
                    DblTotalSaldo = DblTotalSaldo + 0
                Else
                    DblTotalSaldo = DblTotalSaldo + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("S3").Value)))
                End If

                If Len(dgvDetalle.Rows(x).Cells("AA3").Value) = 0 Then
                    DblTotalAvanceAnt = DblTotalAvanceAnt + 0
                Else
                    DblTotalAvanceAnt = DblTotalAvanceAnt + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA3").Value)))
                End If
                'End If
                Contador = Contador + 1
            Next
            txtSubTotal.Text = Format(DblTotal, "#,##0.00")
            txtIGV.Text = Format(DblTotal * gIgvConvertido, "#,##0.00")
            txtTotal.Text = Format(DblTotal + (DblTotal * gIgvConvertido), "#,##0.00")

            txtAA1.Text = Format(DblTotalAvanceAnt, "#,##0.00")
            txtAA2.Text = Format(DblTotalAvanceAnt * gIgvConvertido, "#,##0.00")
            txtAA3.Text = Format(DblTotalAvanceAnt + (DblTotalAvanceAnt * gIgvConvertido), "#,##0.00")

            txtSubTotalVal.Text = Format(DblTotalVal, "#,##0.00")
            txtSubIGVVal.Text = Format(DblTotalVal * gIgvConvertido, "#,##0.00")
            txtTotalVal.Text = Format(DblTotalVal + (DblTotalVal * gIgvConvertido), "#,##0.00")

            txtSubTotalSaldo.Text = Format(DblTotalSaldo, "#,##0.00")
            ''''campos acumulado
            txtAcumuladoSubtotal.Text = Format(DblTotal - DblTotalSaldo, "#,##0.00")
            txtAcumuladoIGV.Text = Format((DblTotal * gIgvConvertido) - (DblTotalSaldo * gIgvConvertido), "#,##0.00")
            txtAcumuladoTotal.Text = Format((DblTotal + (DblTotal * gIgvConvertido)) - (DblTotalSaldo + (DblTotalSaldo * gIgvConvertido)), "#,##0.00")


            txtSubTotalIGV.Text = Format(DblTotalSaldo * gIgvConvertido, "#,##0.00")
            txtSubTotalTot.Text = Format(DblTotalSaldo + (DblTotalSaldo * gIgvConvertido), "#,##0.00")

            txtBaseSaldoActaR.Text = IIf(VL_TIPORETENCION = "0", txtSubTotalVal.Text, txtTotalVal.Text)

            'txtRetPor.Text = Format(Convert.ToDouble(txtTotalVal.Text) * (RetencionPorcentaje / 100), "#,##0.00")
            txtRetPor.Text = Format(Convert.ToDouble(txtBaseSaldoActaR.Text) * (RetencionPorcentaje / 100), "#,##0.00")

            txtSubTotalFactura.Text = Format(IIf(VL_TIPORETENCION = "0", Convert.ToDouble(txtBaseSaldoActaR.Text) - Convert.ToDouble(txtRetPor.Text), Convert.ToDouble(txtSubTotalVal.Text)), "#,##0.00")
            txtIGVFactura.Text = Format(Convert.ToDecimal(txtSubTotalFactura.Text) * (0.18), "#,##0.00")
            txtTotalFactura.Text = Format(Convert.ToDecimal(txtSubTotalFactura.Text) + (Convert.ToDecimal(txtIGVFactura.Text)), "#,##0.00")


            txtFG2.Text = Format(IIf(VL_TIPORETENCION = "0", Convert.ToDouble(txtSubTotalVal.Text), Convert.ToDouble(txtTotalVal.Text)) * (RetencionPorcentaje / 100), "#,##0.00")
            If Len(Trim(txtAdelanto.Text)) = 0 Then
                txtAdelanto.Text = Format(0, "#,##0.00")
                'txtAmorti2.Text = Format(0, "#,##0.00")
            End If
            
            If Len(Trim(txtOtroDes.Text)) = 0 Then
                txtOtroDes.Text = Format(0, "#,##0.00")
            End If

            'If Len(Trim(txtDevFG.Text)) = 0 Then
            '    txtDevFG.Text = Format(0, "#,##0.00")
            'End If

            'txtAdelanto.Text = Format(0, "#,##0.00")
            'txtOtroDes.Text = Format(0, "#,##0.00")
            'txtDevFG.Text = Format(0, "#,##0.00")

            Dim sumDesc As Double = 0
            sumDesc = Format(Convert.ToDouble(txtTotalVal.Text) * (RetencionPorcentaje / 100), "#,##0.00") + Convert.ToDouble(txtAdelanto.Text) + Convert.ToDouble(txtOtroDes.Text) + 0
            txtTotPagar.Text = Format(Convert.ToDouble(txtTotalVal.Text) - sumDesc, "#,##0.00")

            BeLabel18.Text = "Total de Items : " & Contador
        Else
            txtSubTotal.Text = Format(0, "#,##0.00")
            txtIGV.Text = Format(0 * gIgvConvertido, "#,##0.00")
            txtTotal.Text = Format(0 + (0 * gIgvConvertido), "#,##0.00")

            txtAA1.Text = Format(0, "#,##0.00")
            txtAA2.Text = Format(0 * gIgvConvertido, "#,##0.00")
            txtAA3.Text = Format(0 + (0 * gIgvConvertido), "#,##0.00")

            txtBaseSaldoActaR.Text = Format(0, "#,##0.00")

            txtSubTotalVal.Text = Format(0, "#,##0.00")
            txtSubIGVVal.Text = Format(0 * gIgvConvertido, "#,##0.00")
            txtTotalVal.Text = Format(0 + (0 * gIgvConvertido), "#,##0.00")

            txtSubTotalSaldo.Text = Format(0, "#,##0.00")
            txtSubTotalIGV.Text = Format(0 * gIgvConvertido, "#,##0.00")
            txtSubTotalTot.Text = Format(0 + (0 * gIgvConvertido), "#,##0.00")

            txtRetPor.Text = Format(0, "#,##0.00")
            txtFG2.Text = Format(0, "#,##0.00")
            'txtAdelanto.Text = Format(0, "#,##0.00")
            'txtOtroDes.Text = Format(0, "#,##0.00")
            'txtDevFG.Text = Format(0, "#,##0.00")

            If Len(Trim(txtAdelanto.Text)) = 0 Then
                txtAdelanto.Text = Format(0, "#,##0.00")
                'txtAmorti2.Text = Format(0, "#,##0.00")
            End If
            If Len(Trim(txtOtroDes.Text)) = 0 Then
                txtOtroDes.Text = Format(0, "#,##0.00")
            End If
            'If Len(Trim(txtDevFG.Text)) = 0 Then
            '    txtDevFG.Text = Format(0, "#,##0.00")
            'End If

            txtTotPagar.Text = Format(0, "#,##0.00")
            BeLabel18.Text = "Total de Items : 0"

        End If

        'txtAmorti3.Text = Format(Convert.ToDouble(txtAdelanto1.Text) - Convert.ToDouble(txtAmorti2.Text), "#,##0.00")
        If Trim(txtAdelanto1.Text) = "" Then
            txtAdelanto1.Text = "0.00"
        End If
        If Trim(txtAmorti2.Text) = "" Then
            txtAmorti2.Text = "0.00"
        End If
        txtAmorti4.Text = Format(Convert.ToDouble(txtAdelanto1.Text) - Convert.ToDouble(txtAmorti2.Text), "#,##0.00")
        txtAdelanto.Text = txtAmorti2.Text

        If Trim(txtFG1.Text) = "" Then
            txtFG1.Text = "0.00"
        End If
        If Trim(txtFG2.Text) = "" Then
            txtFG2.Text = "0.00"
        End If

        txtFG3.Text = Format(Convert.ToDouble(txtFG1.Text) + Convert.ToDouble(txtFG2.Text), "#,##0.00")
        txtFG4.Text = Format(Convert.ToDouble(txtFG1.Text) + Convert.ToDouble(txtFG2.Text), "#,##0.00")

        If Trim(txtCuadrilla1.Text) = "" Then
            txtCuadrilla1.Text = "0.00"
        End If
        If Trim(txtCuadrilla2.Text) = "" Then
            txtCuadrilla2.Text = "0.00"
        End If
        If Trim(txtCuadrilla3.Text) = "" Then
            txtCuadrilla3.Text = "0.00"
        End If

        If Trim(txtAdelanto2.Text) = "" Then
            txtAdelanto2.Text = "0.00"
        End If
        If Trim(txtAdelanto3.Text) = "" Then
            txtAdelanto3.Text = "0.00"
        End If
        If Trim(txtAmorti1.Text) = "" Then
            txtAmorti1.Text = "0.00"
        End If
        If Trim(txtAmorti3.Text) = "" Then
            txtAmorti3.Text = "0.00"
        End If


        If Trim(txtCuadrilla1Pre.Text) = "" Then
            txtCuadrilla1Pre.Text = "0.00"
        End If
        If Trim(txtCuadrilla2Pre.Text) = "" Then
            txtCuadrilla2Pre.Text = "0.00"
        End If
        If Trim(txtCuadrilla3Pre.Text) = "" Then
            txtCuadrilla3Pre.Text = "0.00"
        End If
        If Trim(txtAmorti3.Text) = "" Then
            txtAmorti3.Text = "0.00"
        End If


        txtCuadrilla4.Text = Format(Convert.ToDouble(txtCuadrilla1.Text) + Convert.ToDouble(txtCuadrilla2.Text) + Convert.ToDouble(txtCuadrilla3.Text), "#,##0.00")

        txtCuadrilla1T.Text = Format(Convert.ToDouble(txtCuadrilla1.Text) * Convert.ToDouble(txtCuadrilla1Pre.Text), "#,##0.00")
        txtCuadrilla2T.Text = Format(Convert.ToDouble(txtCuadrilla2.Text) * Convert.ToDouble(txtCuadrilla2Pre.Text), "#,##0.00")
        txtCuadrilla3T.Text = Format(Convert.ToDouble(txtCuadrilla3.Text) * Convert.ToDouble(txtCuadrilla3Pre.Text), "#,##0.00")
        txtCuadrillaTotales.Text = Format(Convert.ToDouble(txtCuadrilla1T.Text) + Convert.ToDouble(txtCuadrilla2T.Text) + Convert.ToDouble(txtCuadrilla3T.Text), "#,##0.00")

        Dim DblTotalDes As Double = 0
        If dgvDescuentos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDescuentos.Rows.Count - 1
                If Len(dgvDescuentos.Rows(x).Cells("Des_Acumulado").Value) > 0 Then
                    DblTotalDes = DblTotalDes + dgvDescuentos.Rows(x).Cells("Des_Acumulado").Value
                End If
            Next
        End If
        txtTotDes.Text = Format(DblTotalDes, "#,##0.00")
        txtOtroDes.Text = Format(DblTotalDes, "#,##0.00")

    End Sub

    Private Sub cboPartidas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectedIndexChanged
        For x As Integer = 0 To dgvDescuentos.RowCount - 1
            dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
        Next

        If cboPartidas.SelectedIndex = -1 Then
            'For x As Integer = 0 To dgvDetalle.RowCount - 1
            '    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
            'Next
            BeLabel18.Text = "Total de Items : 0"
            'txtContratista.Clear()
            'txtCodigoContratista.Clear()
            txtNumSesion.Clear()
        End If

        Try
            AxMonthView1.Value = dtpEnvio.Value
            txtSemana.Text = AxMonthView1.Week()
        Catch ex As Exception
        End Try
        txtNumeroOrden.Enabled = False
        txtUbicacion.Enabled = False
        txtNroContrato.Enabled = False
    End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit

        If e.ColumnIndex = 1 Or e.ColumnIndex = 3 Or e.ColumnIndex = 4 Or e.ColumnIndex = 5 Or e.ColumnIndex = 6 Or e.ColumnIndex = 7 Or e.ColumnIndex = 8 Or e.ColumnIndex = 9 Or e.ColumnIndex = 10 Or e.ColumnIndex = 12 Or e.ColumnIndex = 13 Or e.ColumnIndex = 14 Then
            If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If
        ElseIf e.ColumnIndex = 11 Then

            If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")

                If Len(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V2").Value) > 0 Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V1").Value = Format(((dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M1").Value) * (dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V2").Value)) / (dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value), "#,##0.0")
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V3").Value = Format((dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V2").Value) * (dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("PU").Value), "#,##0.00")
                    Dim dSaldo As Double = 0
                    dSaldo = Convert.ToDouble(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("AA2").Value) + Convert.ToDouble(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V2").Value)
                    If dSaldo > dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value Then
                        MessageBox.Show("El metrado de avance es mayor al metrado del contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V1").Value = "0.00"
                        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V2").Value = "0.00"
                        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("V3").Value = "0.00"
                    End If
                End If

                If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If

            'If e.ColumnIndex = 8 Then
            'End If

        End If

        Calcular()
    End Sub

    Private Sub dgvDetalle_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalle.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If columna = 1 Or columna = 3 Or columna = 4 Or columna = 5 Or columna = 6 Or columna = 7 Or columna = 8 Or columna = 9 Or columna = 10 Or columna = 11 Or columna = 12 Or columna = 13 Or columna = 14 Then
            Dim caracter As Char = e.KeyChar
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub


    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        If cboObra.SelectedIndex > -1 And cboContratista.SelectedIndex > -1 And cboPartidas.SelectedIndex > -1 And Len(Trim(txtSemana.Text)) > 0 And dgvDetalle.Rows.Count > 0 And Val(txtTotPagar.Text) > 0 And Len(txtAdelanto1.Text.Trim) > 0 And Len(txtAdelanto2.Text.Trim) > 0 And Len(txtAdelanto3.Text.Trim) > 0 And Len(txtAmorti1.Text.Trim) > 0 And Len(txtAmorti2.Text.Trim) > 0 And Len(txtAmorti3.Text.Trim) > 0 And Len(txtAmorti4.Text.Trim) > 0 And Len(txtFG1.Text.Trim) > 0 And Len(txtFG2.Text.Trim) > 0 And Len(txtFG3.Text.Trim) > 0 And Len(txtFG4.Text.Trim) > 0 And Len(txtCuadrilla1.Text.Trim) > 0 And Len(txtCuadrilla2.Text.Trim) > 0 And Len(txtCuadrilla3.Text.Trim) > 0 And Len(txtCuadrilla4.Text.Trim) > 0 And Len(txtTotDes.Text.Trim) > 0 And Len(txtCuadrilla1Pre.Text.Trim) > 0 And Len(txtCuadrilla2Pre.Text.Trim) > 0 And Len(txtCuadrilla3Pre.Text.Trim) > 0 And Len(txtCuadrilla1T.Text.Trim) > 0 And Len(txtCuadrilla2T.Text.Trim) > 0 And Len(txtCuadrilla3T.Text.Trim) > 0 And Len(txtCuadrillaTotales.Text.Trim) > 0 Then

            If dtpFechaIni.Value > dtpFechaFin.Value Then
                MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dtpFechaIni.Focus()
                Exit Sub
            End If

            If dgvDetalle.Rows.Count > 0 Then
                For y As Integer = 0 To dgvDetalle.Rows.Count - 1
                    If Trim(dgvDetalle.Rows(y).Cells("V2").Value) <> "" Then
                        dgvDetalle.Rows(y).Cells("V2").Value = Format(CDec(dgvDetalle.Rows(y).Cells("V2").Value.ToString), "#,##0.00")

                        If Len(dgvDetalle.Rows(y).Cells("V2").Value) > 0 Then
                            dgvDetalle.Rows(y).Cells("V1").Value = Format(((dgvDetalle.Rows(y).Cells("M1").Value) * (dgvDetalle.Rows(y).Cells("V2").Value)) / (dgvDetalle.Rows(y).Cells("M2").Value), "#,##0.0")
                            dgvDetalle.Rows(y).Cells("V3").Value = Format((dgvDetalle.Rows(y).Cells("V2").Value) * (dgvDetalle.Rows(y).Cells("PU").Value), "#,##0.00")
                            Dim dSaldo As Double = 0
                            dSaldo = Convert.ToDouble(dgvDetalle.Rows(y).Cells("AA2").Value) + Convert.ToDouble(dgvDetalle.Rows(y).Cells("V2").Value)
                            If dSaldo > dgvDetalle.Rows(y).Cells("M2").Value Then
                                gestionaResaltados(dgvDetalle, y, Color.Red)
                                MessageBox.Show("El metrado de avance es mayor al metrado del contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgvDetalle.CurrentCell = dgvDetalle(11, y)
                                Exit Sub
                                'dgvDetalle.Rows(y).Cells("V1").Value = "0.00"
                                'dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
                                'dgvDetalle.Rows(y).Cells("V3").Value = "0.00"
                            End If
                        End If

                        If vb.IsDBNull(dgvDetalle.Rows(y).Cells("V2").Value) = True Then
                            dgvDetalle.Rows(y).Cells("V2").Value = Format(CDec(dgvDetalle.Rows(y).Cells("V2").Value.ToString), "#,##0.00")
                        End If
                    Else
                        dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
                    End If
                Next
            End If



            If dgvDescuentos.Rows.Count > 0 Then
                For y As Integer = 0 To dgvDescuentos.Rows.Count - 1
                    If Len(dgvDescuentos.Rows(y).Cells("Des_Descripcion").Value) = 0 Then
                        MessageBox.Show("Ingrese una Descripci�n", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDescuentos.CurrentCell = dgvDescuentos(0, y)
                        dgvDescuentos.Focus()
                        Exit Sub
                    End If
                    If Len(dgvDescuentos.Rows(y).Cells("Des_Anterior").Value) = 0 Then
                        MessageBox.Show("Ingrese el Monto Anterior", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDescuentos.CurrentCell = dgvDescuentos(1, y)
                        dgvDescuentos.Focus()
                        Exit Sub
                    End If
                    If Len(dgvDescuentos.Rows(y).Cells("Des_Actual").Value) = 0 Then
                        MessageBox.Show("Ingrese el Monto Actual", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDescuentos.CurrentCell = dgvDescuentos(2, y)
                        dgvDescuentos.Focus()
                        Exit Sub
                    End If
                    If Len(dgvDescuentos.Rows(y).Cells("Des_Acumulado").Value) = 0 Then
                        MessageBox.Show("Ingrese el Monto Acumulado", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDescuentos.CurrentCell = dgvDescuentos(3, y)
                        dgvDescuentos.Focus()
                        Exit Sub
                    End If
                Next
            End If


            eValorizaciones = New clsValorizaciones
            'grabar cabecera
            Dim iResultado As Integer
            Dim iResultado2 As Integer
            If Len(Trim(txtCodigoVal.Text)) = 0 Then
                eValorizaciones.fCodigo(6, gEmpresa, "", gPeriodo)
                txtCodigoVal.Text = Trim(eValorizaciones.sCodFuturo)

                iResultado = eValorizaciones.fGrabar(7, gEmpresa, Trim(cboPartidas.SelectedValue), Trim(txtCodigoVal.Text), Convert.ToInt32(Trim(txtNumSesion.Text)), Trim(txtSemana.Text), Trim(cboPartidas.Text), dtpFechaIni.Value, dtpFechaFin.Value, dtpEnvio.Value, Trim(txtNumeroOrden.Text), Trim(txtUbicacion.Text), Convert.ToString(IIf(Me.chkCerrar.Checked = True, 1, 0)), Trim(txtNroContrato.Text), gUsuario, RetencionPorcentaje, 0, Trim(txtTotPagar.Text), Trim(txtSubTotalVal.Text), Trim(txtSubIGVVal.Text), Trim(txtTotalVal.Text), Trim(txtRetPor.Text), Trim(txtAdelanto.Text), Trim(txtOtroDes.Text), 0, Trim(txtTotPagar.Text), "00005", 3, Trim(txtAdelanto1.Text), Trim(txtAdelanto2.Text), Trim(txtAdelanto3.Text), Trim(txtAmorti1.Text), Trim(txtAmorti2.Text), Trim(txtAmorti3.Text), Trim(txtAmorti4.Text), Trim(txtFG1.Text), Trim(txtFG2.Text), Trim(txtFG3.Text), Trim(txtFG4.Text), Trim(txtCuadrilla1.Text), Trim(txtCuadrilla2.Text), Trim(txtCuadrilla3.Text), Trim(txtCuadrilla4.Text), Trim(txtTotDes.Text), Trim(txtCuadrilla1Pre.Text), Trim(txtCuadrilla2Pre.Text), Trim(txtCuadrilla3Pre.Text), Trim(txtCuadrilla1T.Text), Trim(txtCuadrilla2T.Text), Trim(txtCuadrilla3T.Text), Trim(txtCuadrillaTotales.Text), gPeriodo, Trim(CCosto.SelectedValue))
                If dgvDetalle.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                        Dim IdDetValorizacion As String = ""
                        Dim pDescripcion As String = ""
                        Dim pIdPartidaDet As String = ""
                        Dim pCantidad As Double = 0
                        Dim pUnd As String
                        Dim pMetrBase1 As Double = 0
                        Dim pMetrBase2 As Double = 0
                        Dim pPU As Double = 0
                        Dim pParcial As Double = 0
                        Dim pAA1 As Double = 0
                        Dim pAA2 As Double = 0
                        Dim pAA3 As Double = 0
                        Dim pV1 As Double = 0
                        Dim pV2 As Double = 0
                        Dim pV3 As Double = 0
                        Dim pS1 As Double = 0
                        Dim pS2 As Double = 0
                        Dim pS3 As Double = 0
                        pDescripcion = Trim(dgvDetalle.Rows(x).Cells("Descripcion").Value)
                        pCantidad = Convert.ToDouble(dgvDetalle.Rows(x).Cells("Cant").Value)
                        pUnd = Trim(dgvDetalle.Rows(x).Cells("Und").Value)
                        pMetrBase1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("M1").Value)
                        pMetrBase2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("M2").Value)
                        pPU = Convert.ToDouble(dgvDetalle.Rows(x).Cells("PU").Value)
                        pParcial = Convert.ToDouble(dgvDetalle.Rows(x).Cells("Parcial").Value)
                        pAA1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA1").Value)
                        pAA2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA2").Value)
                        pAA3 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA3").Value)
                        pV1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("V1").Value)
                        pV2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("V2").Value)
                        pV3 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("V3").Value)
                        pS1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("S1").Value)
                        pS2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("S2").Value)
                        pS3 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("S3").Value)
                        pIdPartidaDet = Trim(dgvDetalle.Rows(x).Cells("IdPartidaDet").Value())

                        eValorizaciones.fCodigo(9, gEmpresa, Trim(txtCodigoVal.Text), gPeriodo)
                        IdDetValorizacion = Trim(eValorizaciones.sCodFuturo)
                        iResultado2 = eValorizaciones.fGrabarDetalle(8, gEmpresa, Trim(IdDetValorizacion), Trim(txtCodigoVal.Text), gUsuario, pDescripcion, pCantidad, pUnd, pMetrBase1, pMetrBase2, pPU, pParcial, pAA1, pAA2, pAA3, pV1, pV2, pV3, pS1, pS2, pS3, IdDetValorizacion, 1, pIdPartidaDet, gPeriodo)
                    Next
                End If

                If dgvDescuentos.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDescuentos.Rows.Count - 1
                        Dim pIdDetDesValorizacion As String = ""
                        Dim pDescripcionDes As String = ""
                        Dim pAnteriorDes As Double = 0
                        Dim pActualDes As Double = 0
                        Dim pAcumuladoDes As Double = 0
                        pDescripcionDes = Trim(dgvDescuentos.Rows(x).Cells("Des_Descripcion").Value)
                        pAnteriorDes = Convert.ToDouble(dgvDescuentos.Rows(x).Cells("Des_Anterior").Value)
                        pActualDes = Convert.ToDouble(dgvDescuentos.Rows(x).Cells("Des_Actual").Value)
                        pAcumuladoDes = Convert.ToDouble(dgvDescuentos.Rows(x).Cells("Des_Acumulado").Value)
                        'pIdDetDesValorizacion = Trim(dgvDescuentos.Rows(x).Cells("Des_IdDetalle").Value())
                        eValorizaciones.fCodigo(46, gEmpresa, Trim(txtCodigoVal.Text), gPeriodo)
                        pIdDetDesValorizacion = Trim(eValorizaciones.sCodFuturo)
                        iResultado2 = eValorizaciones.fGrabarDetalle(45, gEmpresa, Trim(pIdDetDesValorizacion), Trim(txtCodigoVal.Text), gUsuario, pDescripcionDes, 0, "", 0, 0, 0, 0, pAnteriorDes, pActualDes, pAcumuladoDes, 0, 0, 0, 0, 0, 0, "", 1, "", gPeriodo)
                    Next
                End If

                If iResultado = 1 And iResultado2 = 1 Then
                    MessageBox.Show("Se ha grabado con Exito!", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    TraerValorizacionPorSemana()

                    btnImprimir.Enabled = True
                    btnGrabar.Enabled = True
                    btnModificar.Enabled = False
                End If

            ElseIf Trim(txtCodigoVal.Text) > 0 Then

                iResultado = eValorizaciones.fGrabar(10, gEmpresa, Trim(cboPartidas.SelectedValue), Trim(txtCodigoVal.Text), Convert.ToInt32(Trim(txtNumSesion.Text)), Trim(txtSemana.Text), Trim(cboPartidas.Text), dtpFechaIni.Value, dtpFechaFin.Value, dtpEnvio.Value, Trim(txtNumeroOrden.Text), Trim(txtUbicacion.Text), Convert.ToString(IIf(Me.chkCerrar.Checked = True, 1, 0)), Trim(txtNroContrato.Text), gUsuario, 0, 0, Trim(txtTotPagar.Text), Trim(txtSubTotalVal.Text), Trim(txtSubIGVVal.Text), Trim(txtTotalVal.Text), Trim(txtRetPor.Text), Trim(txtAdelanto.Text), Trim(txtOtroDes.Text), 0, Trim(txtTotPagar.Text), "00005", 3, Trim(txtAdelanto1.Text), Trim(txtAdelanto2.Text), Trim(txtAdelanto3.Text), Trim(txtAmorti1.Text), Trim(txtAmorti2.Text), Trim(txtAmorti3.Text), Trim(txtAmorti4.Text), Trim(txtFG1.Text), Trim(txtFG2.Text), Trim(txtFG3.Text), Trim(txtFG4.Text), Trim(txtCuadrilla1.Text), Trim(txtCuadrilla2.Text), Trim(txtCuadrilla3.Text), Trim(txtCuadrilla4.Text), Trim(txtTotDes.Text), Trim(txtCuadrilla1Pre.Text), Trim(txtCuadrilla2Pre.Text), Trim(txtCuadrilla3Pre.Text), Trim(txtCuadrilla1T.Text), Trim(txtCuadrilla2T.Text), Trim(txtCuadrilla3T.Text), Trim(txtCuadrillaTotales.Text), gPeriodo, Trim(CCosto.SelectedValue))

                If iResultado = 1 And Convert.ToString(IIf(Me.chkCerrar.Checked = True, 1, 0)) = "1" Then
                    Dim dtTable2 As DataTable
                    dtTable2 = New DataTable
                    eValorizaciones = New clsValorizaciones
                    dtTable2 = eValorizaciones.fListarObras(68, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(txtCodigoVal.Text), Today(), "")
                    If dtTable2.Rows.Count > 0 Then
                        For x As Integer = 0 To dtTable2.Rows.Count - 1
                            Dim sIdPartidaAvanceDiario As String = ""
                            Dim sIdPartida As String = ""
                            Dim iActDetalleAvance As Integer = 0

                            sIdPartidaAvanceDiario = Trim(dtTable2.Rows(x).Item("IdPartidaAvanceDiario"))
                            sIdPartida = Trim(dtTable2.Rows(x).Item("IdPartida"))

                            iActDetalleAvance = eValorizaciones.fActDetalleAvance(67, gEmpresa, Trim(sIdPartidaAvanceDiario), Trim(sIdPartida), gUsuario, 1, gPeriodo)
                        Next
                    End If
                End If

                If dgvDetalle.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                        Dim IdDetValorizacion As String = ""
                        Dim pDescripcion As String = ""
                        Dim pCantidad As Double = 0
                        Dim pUnd As String
                        Dim pMetrBase1 As Double = 0
                        Dim pMetrBase2 As Double = 0
                        Dim pPU As Double = 0
                        Dim pParcial As Double = 0
                        Dim pAA1 As Double = 0
                        Dim pAA2 As Double = 0
                        Dim pAA3 As Double = 0
                        Dim pV1 As Double = 0
                        Dim pV2 As Double = 0
                        Dim pV3 As Double = 0
                        Dim pS1 As Double = 0
                        Dim pS2 As Double = 0
                        Dim pS3 As Double = 0
                        pDescripcion = Trim(dgvDetalle.Rows(x).Cells("Descripcion").Value)
                        pCantidad = Convert.ToDouble(dgvDetalle.Rows(x).Cells("Cant").Value)
                        pUnd = Trim(dgvDetalle.Rows(x).Cells("Und").Value)
                        pMetrBase1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("M1").Value)
                        pMetrBase2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("M2").Value)
                        pPU = Convert.ToDouble(dgvDetalle.Rows(x).Cells("PU").Value)
                        pParcial = Convert.ToDouble(dgvDetalle.Rows(x).Cells("Parcial").Value)
                        pAA1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA1").Value)
                        pAA2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA2").Value)
                        pAA3 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("AA3").Value)
                        pV1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("V1").Value)
                        pV2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("V2").Value)
                        pV3 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("V3").Value)
                        pS1 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("S1").Value)
                        pS2 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("S2").Value)
                        pS3 = Convert.ToDouble(dgvDetalle.Rows(x).Cells("S3").Value)
                        IdDetValorizacion = Trim(dgvDetalle.Rows(x).Cells("idDetalle").Value)
                        'eValorizaciones.fCodigo(9, gEmpresa, Trim(txtCodigoVal.Text))
                        'IdDetValorizacion = Trim(eValorizaciones.sCodFuturo)
                        iResultado2 = eValorizaciones.fGrabarDetalle(11, gEmpresa, Trim(IdDetValorizacion), Trim(txtCodigoVal.Text), gUsuario, pDescripcion, pCantidad, pUnd, pMetrBase1, pMetrBase2, pPU, pParcial, pAA1, pAA2, pAA3, pV1, pV2, pV3, pS1, pS2, pS3, IdDetValorizacion, 1, "", gPeriodo)
                    Next
                End If

                If dgvDescuentos.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDescuentos.Rows.Count - 1
                        Dim pIdDetDesValorizacion As String = ""
                        Dim pDescripcionDes As String = ""
                        Dim pAnteriorDes As Double = 0
                        Dim pActualDes As Double = 0
                        Dim pAcumuladoDes As Double = 0
                        pDescripcionDes = Trim(dgvDescuentos.Rows(x).Cells("Des_Descripcion").Value)
                        pAnteriorDes = Convert.ToDouble(dgvDescuentos.Rows(x).Cells("Des_Anterior").Value)
                        pActualDes = Convert.ToDouble(dgvDescuentos.Rows(x).Cells("Des_Actual").Value)
                        pAcumuladoDes = Convert.ToDouble(dgvDescuentos.Rows(x).Cells("Des_Acumulado").Value)
                        pIdDetDesValorizacion = Trim(dgvDescuentos.Rows(x).Cells("Des_IdDetalle").Value())
                        'eValorizaciones.fCodigo(46, gEmpresa, Trim(txtCodigoVal.Text))
                        'pIdDetDesValorizacion = Trim(eValorizaciones.sCodFuturo)
                        iResultado2 = eValorizaciones.fGrabarDetalle(47, gEmpresa, Trim(pIdDetDesValorizacion), Trim(txtCodigoVal.Text), gUsuario, pDescripcionDes, 0, "", 0, 0, 0, 0, pAnteriorDes, pActualDes, pAcumuladoDes, 0, 0, 0, 0, 0, 0, "", 1, "", gPeriodo)
                    Next
                End If

                If iResultado = 1 And iResultado2 = 1 Then
                    MessageBox.Show("Se ha Actualizado con Exito!", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    TraerValorizacionPorSemana()
                    btnImprimir.Enabled = True
                    btnGrabar.Enabled = True
                    btnModificar.Enabled = False
                End If

            End If

        Else
            If cboObra.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Obra", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboObra.Focus()
                Exit Sub
            End If
            If cboContratista.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Contratista", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboContratista.Focus()
                Exit Sub
            End If
            If cboPartidas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Partida", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboPartidas.Focus()
                Exit Sub
            End If
            If Len(Trim(txtSemana.Text)) = 0 Then
                MessageBox.Show("Ingrese el N�mero de Semana", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtSemana.Focus()
                Exit Sub
            End If

            If dgvDetalle.Rows.Count = 0 Then
                MessageBox.Show("No Hay registros en la Valorizaci�n para Grabar", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            '-----------------------------------------------------------------
            If Len(Trim(txtAdelanto1.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto del Adelanto Anterior", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAdelanto1.Focus()
                Exit Sub
            End If
            If Len(Trim(txtAdelanto2.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto del Adelanto Actual", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAdelanto2.Focus()
                Exit Sub
            End If
            If Len(Trim(txtAdelanto3.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto del Adelanto Acumulado", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAdelanto3.Focus()
                Exit Sub
            End If
            If Len(Trim(txtAmorti1.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto de la Amortizaci�n Anterior", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAmorti1.Focus()
                Exit Sub
            End If
            If Len(Trim(txtAmorti2.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto de la Amortizaci�n Actual", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAmorti2.Focus()
                Exit Sub
            End If
            If Len(Trim(txtAmorti3.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto de la Amortizaci�n Acumulada", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAmorti3.Focus()
                Exit Sub
            End If
            If Len(Trim(txtAmorti4.Text)) = 0 Then
                MessageBox.Show("Ingrese el Saldo de la Amortizaci�n ", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAmorti4.Focus()
                Exit Sub
            End If
            If Len(Trim(txtFG1.Text)) = 0 Then
                MessageBox.Show("Ingrese el Fondo de Garant�a Anterior", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtFG1.Focus()
                Exit Sub
            End If
            If Len(Trim(txtFG2.Text)) = 0 Then
                MessageBox.Show("Ingrese el Fondo de Garant�a Actual", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtFG2.Focus()
                Exit Sub
            End If
            If Len(Trim(txtFG3.Text)) = 0 Then
                MessageBox.Show("Ingrese el Fondo de Garant�a Acumulado", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtFG3.Focus()
                Exit Sub
            End If
            If Len(Trim(txtFG4.Text)) = 0 Then
                MessageBox.Show("Ingrese el Fondo de Garant�a a Pagar", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtFG4.Focus()
                Exit Sub
            End If
            If Len(Trim(txtCuadrilla1.Text)) = 0 Then
                MessageBox.Show("Ingrese la Cantidad de Operadores", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCuadrilla1.Focus()
                Exit Sub
            End If

            If Len(Trim(txtCuadrilla1Pre.Text)) = 0 Then
                MessageBox.Show("Ingrese el importe por trabajo de Operador", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCuadrilla1Pre.Focus()
                Exit Sub
            End If

            If Len(Trim(txtCuadrilla2.Text)) = 0 Then
                MessageBox.Show("Ingrese la Cantidad de Oficiales", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCuadrilla2.Focus()
                Exit Sub
            End If

            If Len(Trim(txtCuadrilla2Pre.Text)) = 0 Then
                MessageBox.Show("Ingrese el importe por trabajo de Oficial", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCuadrilla2Pre.Focus()
                Exit Sub
            End If

            If Len(Trim(txtCuadrilla3.Text)) = 0 Then
                MessageBox.Show("Ingrese la Cantidad de Peones (Ayuda)", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCuadrilla3.Focus()
                Exit Sub
            End If

            If Len(Trim(txtCuadrilla3Pre.Text)) = 0 Then
                MessageBox.Show("Ingrese el importe por trabajo de Ayudante", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCuadrilla3Pre.Focus()
                Exit Sub
            End If

            If Len(Trim(txtCuadrilla4.Text)) = 0 Then
                MessageBox.Show("Ingrese el Total de la Cuadrilla", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCuadrilla4.Focus()
                Exit Sub
            End If
            If Len(Trim(txtTotDes.Text)) = 0 Then
                MessageBox.Show("Ingrese el Total de Operadores", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtTotDes.Focus()
                Exit Sub
            End If
            '-----------------------------------------------------------------

            If Val(txtTotPagar.Text) <= 0 Then
                MessageBox.Show("El Total a Pagar es Inv�lido, Verifique.", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtTotPagar.Focus()
                Exit Sub
            End If

        End If
    End Sub

    'Private Sub txtNumSesion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumSesion.KeyPress
    '    Select Case Asc(e.KeyChar)
    '        Case 13
    '            TraerValorizacion()
    '    End Select
    'End Sub

    'Private Sub TraerValorizacion()
    '    If Len(Trim(txtNumSesion.Text)) > 0 Then


    '        eValorizaciones = New clsValorizaciones
    '        Dim dtValorizacion As DataTable
    '        dtValorizacion = New DataTable
    '        dtValorizacion = eValorizaciones.fBuscarValorizacion(12, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), Convert.ToString(txtNumSesion.Text.Trim))
    '        BeLabel11.Text = "              Valorizaci�n N� " & Trim(txtNumSesion.Text) & "              "
    '        If dtValorizacion.Rows.Count > 0 Then

    '            Dim IdValorizacion As String = ""

    '            iCerrarValo = dtValorizacion.Rows(0).Item("Cerrado")
    '            IdValorizacion = dtValorizacion.Rows(0).Item("IdValorizacion")
    '            txtCodigoVal.Text = Trim(IdValorizacion)
    '            dtpFechaIni.Value = dtValorizacion.Rows(0).Item("FechaInicio")
    '            dtpFechaFin.Value = dtValorizacion.Rows(0).Item("FechaFinal")
    '            dtpEnvio.Value = dtValorizacion.Rows(0).Item("FechaEnvio")
    '            txtSemana.Text = Trim(dtValorizacion.Rows(0).Item("Semana"))
    '            txtNumeroOrden.Text = Trim(dtValorizacion.Rows(0).Item("NumOrden"))
    '            'txtDescripcion.Text = Trim(dtValorizacion.Rows(0).Item("Descripcion"))
    '            txtUbicacion.Text = Trim(dtValorizacion.Rows(0).Item("ParUbicacion"))
    '            txtNroContrato.Text = Trim(dtValorizacion.Rows(0).Item("NContrato"))

    '            If iCerrarValo = "0" Then
    '                'continuar valorizacion
    '                HabilitarControles()
    '            ElseIf iCerrarValo = "1" Then
    '                'bloquear todo 
    '                DesabilitarControles()
    '            End If

    '            txtNumeroOrden.Enabled = False
    '            txtUbicacion.Enabled = False
    '            txtNroContrato.Enabled = False

    '            Dim dtDetDescValorizacion As DataTable
    '            dtDetDescValorizacion = New DataTable
    '            dtDetDescValorizacion = eValorizaciones.fBuscarValorizacion(48, gEmpresa, IdValorizacion, Convert.ToString(txtNumSesion.Text.Trim))
    '            If dtDetDescValorizacion.Rows.Count > 0 Then
    '                For x As Integer = 0 To dgvDescuentos.RowCount - 1
    '                    dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
    '                Next
    '                For y As Integer = 0 To dtDetDescValorizacion.Rows.Count - 1
    '                    dgvDescuentos.Rows.Add()
    '                    dgvDescuentos.Rows(y).Cells("Des_Descripcion").Value = dtDetDescValorizacion.Rows(y).Item("Descripcion").ToString
    '                    dgvDescuentos.Rows(y).Cells("Des_Anterior").Value = Format(Convert.ToDouble(dtDetDescValorizacion.Rows(y).Item("Anterior")), "#,##0.0")
    '                    dgvDescuentos.Rows(y).Cells("Des_Actual").Value = Format(Convert.ToDouble(dtDetDescValorizacion.Rows(y).Item("Actual")), "#,##0.0")
    '                    dgvDescuentos.Rows(y).Cells("Des_Acumulado").Value = Format(Convert.ToDouble(dtDetDescValorizacion.Rows(y).Item("Acumulado")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("MetradoPor").ToString
    '                    dgvDescuentos.Rows(y).Cells("Des_IdDetalle").Value = dtDetDescValorizacion.Rows(y).Item("IdDetalleDesc").ToString
    '                    dgvDescuentos.Rows(y).Cells("Des_Estado").Value = "1"
    '                Next
    '            End If

    '            Dim dtDetValorizacion As DataTable
    '            dtDetValorizacion = New DataTable
    '            dtDetValorizacion = eValorizaciones.fBuscarValorizacion(13, gEmpresa, IdValorizacion, Convert.ToString(txtNumSesion.Text.Trim))
    '            If dtDetValorizacion.Rows.Count > 0 Then
    '                For x As Integer = 0 To dgvDetalle.RowCount - 1
    '                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
    '                Next
    '                For y As Integer = 0 To dtDetValorizacion.Rows.Count - 1
    '                    dgvDetalle.Rows.Add()
    '                    dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetValorizacion.Rows(y).Item("Descripcion").ToString
    '                    dgvDetalle.Rows(y).Cells("Cant").Value = dtDetValorizacion.Rows(y).Item("Cantidad").ToString
    '                    dgvDetalle.Rows(y).Cells("Und").Value = dtDetValorizacion.Rows(y).Item("UnidMed").ToString
    '                    dgvDetalle.Rows(y).Cells("M1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("MetradoPor").ToString
    '                    dgvDetalle.Rows(y).Cells("M2").Value = dtDetValorizacion.Rows(y).Item("MetradoImpor").ToString
    '                    dgvDetalle.Rows(y).Cells("PU").Value = dtDetValorizacion.Rows(y).Item("PU").ToString
    '                    dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetValorizacion.Rows(y).Item("Parcial").ToString

    '                    dgvDetalle.Rows(y).Cells("AA1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("AA_MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("AA_MetradoPor").ToString
    '                    dgvDetalle.Rows(y).Cells("AA2").Value = dtDetValorizacion.Rows(y).Item("AA_MetradoImp").ToString
    '                    dgvDetalle.Rows(y).Cells("AA3").Value = dtDetValorizacion.Rows(y).Item("AA_Mon").ToString

    '                    dgvDetalle.Rows(y).Cells("V1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("Val_MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("Val_MetradoPor").ToString
    '                    dgvDetalle.Rows(y).Cells("V2").Value = dtDetValorizacion.Rows(y).Item("Val_MetradoImp").ToString
    '                    dgvDetalle.Rows(y).Cells("V3").Value = dtDetValorizacion.Rows(y).Item("Val_Mon").ToString

    '                    dgvDetalle.Rows(y).Cells("S1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("Sal_MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("Sal_MetradoPor").ToString
    '                    dgvDetalle.Rows(y).Cells("S2").Value = dtDetValorizacion.Rows(y).Item("Sal_MetradoImp").ToString
    '                    dgvDetalle.Rows(y).Cells("S3").Value = dtDetValorizacion.Rows(y).Item("Sal_Mon").ToString

    '                    'dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetPartida.Rows(y).Item("Parcial").ToString
    '                    dgvDetalle.Rows(y).Cells("idDetalle").Value = dtDetValorizacion.Rows(y).Item("IdDetValorizacion").ToString

    '                    dgvDetalle.Rows(y).Cells("Estado").Value = "1"
    '                Next
    '                BeLabel18.Text = "Total de Items : " & dtDetValorizacion.Rows.Count
    '                Calcular()
    '                For y As Integer = 0 To dgvDetalle.Rows.Count - 1
    '                    If dgvDetalle.Rows(y).Cells("S1").Value <= 0 Then
    '                        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
    '                            'dgvDetalle.Columns(x).ReadOnly = True
    '                            dgvDetalle.Rows(y).Cells(x).ReadOnly = True
    '                        Next
    '                    ElseIf dgvDetalle.Rows(y).Cells("S1").Value > 0 Then
    '                        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
    '                            'dgvDetalle.Columns(x).ReadOnly = False
    '                            dgvDetalle.Rows(y).Cells(x).ReadOnly = False
    '                        Next
    '                    End If
    '                Next
    '            End If

    '        ElseIf dtValorizacion.Rows.Count = 0 Then
    '            'Dim iCerrarValo As Integer
    '            Dim dtTable3 As DataTable
    '            dtTable3 = New DataTable
    '            eValorizaciones = New clsValorizaciones
    '            dtTable3 = eValorizaciones.fGenerarNumeroValo(3, gEmpresa, Convert.ToString(cboPartidas.SelectedValue))
    '            If dtTable3.Rows.Count > 0 Then
    '                Dim dtCabValorizacionAnt As DataTable
    '                dtCabValorizacionAnt = New DataTable
    '                dtCabValorizacionAnt = eValorizaciones.fBuscarValorizacion(12, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), Convert.ToInt32(Trim(txtNumSesion.Text)) - 1)
    '                If dtCabValorizacionAnt.Rows.Count > 0 Then
    '                    Dim IdValorizacion As String = ""

    '                    IdValorizacion = Trim(dtCabValorizacionAnt.Rows(0).Item("IdValorizacion"))
    '                    txtNumeroOrden.Text = Trim(dtCabValorizacionAnt.Rows(0).Item("NumOrden"))
    '                    'txtDescripcion.Text = Trim(dtCabValorizacionAnt.Rows(0).Item("Descripcion"))
    '                    txtUbicacion.Text = Trim(dtCabValorizacionAnt.Rows(0).Item("ParUbicacion"))
    '                    txtNroContrato.Text = Trim(dtCabValorizacionAnt.Rows(0).Item("NContrato"))
    '                    'txtSemana.Text = ""
    '                    dtpFechaIni.Value = dtCabValorizacionAnt.Rows(0).Item("FechaFinal")
    '                    dtpFechaIni.Value = dtpFechaIni.Value.AddDays(Val(Trim("1")))

    '                    dtpEnvio.Value = Now.Date().ToShortDateString()
    '                    'dtpFechaFin.Value = dtValorizacion.Rows(0).Item("FechaFinal")
    '                    dtpFechaFin.Value = Now.Date().ToShortDateString()
    '                    Dim dtDetValorizacionAnt As DataTable
    '                    dtDetValorizacionAnt = New DataTable
    '                    dtDetValorizacionAnt = eValorizaciones.fBuscarValorizacion(13, gEmpresa, IdValorizacion, Convert.ToString(txtNumSesion.Text.Trim))
    '                    If dtDetValorizacionAnt.Rows.Count > 0 Then
    '                        For x As Integer = 0 To dgvDetalle.RowCount - 1
    '                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
    '                        Next
    '                        For y As Integer = 0 To dtDetValorizacionAnt.Rows.Count - 1
    '                            dgvDetalle.Rows.Add()
    '                            dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetValorizacionAnt.Rows(y).Item("Descripcion").ToString
    '                            dgvDetalle.Rows(y).Cells("Cant").Value = dtDetValorizacionAnt.Rows(y).Item("Cantidad").ToString
    '                            dgvDetalle.Rows(y).Cells("Und").Value = dtDetValorizacionAnt.Rows(y).Item("UnidMed").ToString
    '                            dgvDetalle.Rows(y).Cells("M1").Value = Format(Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("MetradoPor")), "#,##0.0") 'dtDetValorizacionAnt.Rows(y).Item("MetradoPor").ToString
    '                            dgvDetalle.Rows(y).Cells("M2").Value = dtDetValorizacionAnt.Rows(y).Item("MetradoImpor").ToString
    '                            dgvDetalle.Rows(y).Cells("PU").Value = dtDetValorizacionAnt.Rows(y).Item("PU").ToString
    '                            dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetValorizacionAnt.Rows(y).Item("Parcial").ToString

    '                            'dgvDetalle.Rows(y).Cells("AA1").Value = dtDetValorizacionAnt.Rows(y).Item("Val_MetradoPor").ToString
    '                            'dgvDetalle.Rows(y).Cells("AA2").Value = dtDetValorizacionAnt.Rows(y).Item("Val_MetradoImp").ToString
    '                            'dgvDetalle.Rows(y).Cells("AA3").Value = dtDetValorizacionAnt.Rows(y).Item("Val_Mon").ToString

    '                            dgvDetalle.Rows(y).Cells("AA1").Value = Format(((Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("AA_MetradoPor"))) + (Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Val_MetradoPor")))), "#,##0.0")
    '                            dgvDetalle.Rows(y).Cells("AA2").Value = Format(((Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("AA_MetradoImp"))) + (Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Val_MetradoImp")))), "#,##0.00")
    '                            dgvDetalle.Rows(y).Cells("AA3").Value = Format(((Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("AA_Mon"))) + (Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Val_Mon")))), "#,##0.00")

    '                            dgvDetalle.Rows(y).Cells("V1").Value = "0.0"
    '                            dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
    '                            dgvDetalle.Rows(y).Cells("V3").Value = "0.00"

    '                            dgvDetalle.Rows(y).Cells("S1").Value = "0.0"
    '                            dgvDetalle.Rows(y).Cells("S2").Value = "0.00"
    '                            dgvDetalle.Rows(y).Cells("S3").Value = "0.00"

    '                            dgvDetalle.Rows(y).Cells("Estado").Value = "0"
    '                        Next
    '                        BeLabel18.Text = "Total de Items : " & dtDetValorizacionAnt.Rows.Count
    '                        HabilitarControles()
    '                        txtCodigoVal.Clear()
    '                        txtNumeroOrden.Enabled = False
    '                        txtUbicacion.Enabled = False
    '                        txtNroContrato.Enabled = False
    '                        'dtpFechaIni.Value = Now.Date().ToShortDateString()
    '                        'dtpFechaFin.Value = Now.Date().ToShortDateString()
    '                        Calcular()

    '                        For y As Integer = 0 To dgvDetalle.Rows.Count - 1
    '                            If dgvDetalle.Rows(y).Cells("S1").Value <= 0 Then
    '                                For x As Integer = 0 To dgvDetalle.Columns.Count - 1
    '                                    'dgvDetalle.Columns(x).ReadOnly = True
    '                                    dgvDetalle.Rows(y).Cells(x).ReadOnly = True
    '                                Next
    '                            ElseIf dgvDetalle.Rows(y).Cells("S1").Value > 0 Then
    '                                For x As Integer = 0 To dgvDetalle.Columns.Count - 1
    '                                    'dgvDetalle.Columns(x).ReadOnly = False
    '                                    dgvDetalle.Rows(y).Cells(x).ReadOnly = False
    '                                Next
    '                            End If
    '                        Next

    '                    End If
    '                End If
    '            End If
    '        End If
    '    Else
    '        MessageBox.Show("Ingrese un N�mero de Valorizacion!", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '        txtNumSesion.Focus()
    '    End If
    'End Sub


    Private Sub DesabilitarControles()
        dtpFechaIni.Enabled = False
        dtpFechaFin.Enabled = False
        dtpEnvio.Enabled = False
        txtNumeroOrden.Enabled = False
        'txtDescripcion.Enabled = False
        txtNroContrato.Enabled = False
        'txtNumSesion.Enabled = False
        'txtSemana.Enabled = False
        chkCerrar.Enabled = False
        chkCerrar.Checked = True
        txtUbicacion.Enabled = False
        'dgvDetalle.Enabled = False
        btnGrabar.Enabled = False
        btnImprimir.Enabled = True
        btnModificar.Enabled = False

        'txtRetPor.Enabled = False
        'txtAdelanto.Enabled = False
        'txtOtroDes.Enabled = False
        'txtDevFG.Enabled = False
        'txtTotPagar.Enabled = False


        'txtAdelanto1.Enabled = False
        txtAdelanto2.Enabled = False
        txtAdelanto3.Enabled = False
        txtAmorti1.Enabled = False
        txtAmorti2.Enabled = False
        txtAmorti3.Enabled = False
        'txtAmorti4.Enabled = False
        'txtFG1.Enabled = False
        'txtFG2.Enabled = False
        'txtFG3.Enabled = False
        'txtFG4.Enabled = False
        txtCuadrilla1.Enabled = False
        txtCuadrilla2.Enabled = False
        txtCuadrilla3.Enabled = False
        'txtCuadrilla4.Enabled = False
        'txtTotDes.Enabled = False
        btnAgregar.Enabled = False
        Button1.Enabled = False

    End Sub

    Private Sub HabilitarControles()
        'dtpFechaIni.Enabled = True
        'dtpFechaFin.Enabled = True
        dtpEnvio.Enabled = True
        txtNumeroOrden.Enabled = True
        'txtDescripcion.Enabled = True
        txtNroContrato.Enabled = True
        'txtNumSesion.Enabled = True
        'txtSemana.Enabled = True
        chkCerrar.Enabled = True
        chkCerrar.Checked = False
        txtUbicacion.Enabled = True
        'dgvDetalle.Enabled = True
        btnGrabar.Enabled = True
        btnImprimir.Enabled = True
        'txtRetPor.Enabled = True
        'txtAdelanto.Enabled = True
        'txtOtroDes.Enabled = True
        'txtDevFG.Enabled = True
        'txtTotPagar.Enabled = True

        'txtAdelanto1.Enabled = True
        txtAdelanto2.Enabled = True
        txtAdelanto3.Enabled = True
        txtAmorti1.Enabled = True
        txtAmorti2.Enabled = True
        txtAmorti3.Enabled = True
        'txtAmorti4.Enabled = True
        'txtFG1.Enabled = True
        'txtFG2.Enabled = True
        'txtFG3.Enabled = True
        'txtFG4.Enabled = True
        txtCuadrilla1.Enabled = True
        txtCuadrilla2.Enabled = True
        txtCuadrilla3.Enabled = True
        'txtCuadrilla4.Enabled = True
        'txtTotDes.Enabled = True
        btnAgregar.Enabled = True
        Button1.Enabled = True

    End Sub

    Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObra.SelectedIndexChanged
        
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If Len(Trim(txtCodigoVal.Text)) > 0 Then ''''''
            Dim x As frmValorizacionesReporte = frmValorizacionesReporte.Instance
            'x.MdiParent = frmPrincipal
            x.IdValorizacion = Trim(txtCodigoVal.Text)
            x.IdCC = Trim(CCosto.SelectedValue)
            x.Show()
            'x.ShowDialog()
        ElseIf Len(Trim(txtCodigoVal.Text)) = 0 Then
            MessageBox.Show("Solo se Imprime Valorizaciones Grabadas", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtSemana.Focus()
        End If
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        cboObra_SelectionChangeCommitted(sender, e)
        Calcular()
        txtCodigoVal.Clear()
        txtNumeroOrden.Clear()
        'txtDescripcion.Clear()
        txtNroContrato.Clear()
        txtNumSesion.Clear()
        txtSemana.Clear()
        txtUbicacion.Clear()
        'txtRetPor.Clear()
        chkCerrar.Checked = False
        chkCerrar.Enabled = True
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        dtpFechaFin.Value = Now.Date().ToShortDateString()
        cboObra.SelectedIndex = -1
        cboContratista.DataSource = Nothing
        cboContratista.SelectedIndex = -1
        cboPartidas.SelectedIndex = -1
        HabilitarControles()
        dtpEnvio.Value = Now.Date()
        cboObra.Focus()

        For x As Integer = 0 To dgvDescuentos.RowCount - 1
            dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
        Next
        txtAdelanto.Text = "0.00"
        txtOtroDes.Text = "0.00"
        'txtDevFG.Text = "0.00"
        txtTotPagar.Text = "0.00"
        txtAdelanto1.Text = "0.00"
        txtAdelanto2.Text = "0.00"
        txtAdelanto3.Text = "0.00"
        txtAmorti1.Text = "0.00"
        txtAmorti2.Text = "0.00"
        txtAmorti3.Text = "0.00"
        txtAmorti4.Text = "0.00"
        txtFG1.Text = "0.00"
        txtFG2.Text = "0.00"
        txtFG3.Text = "0.00"
        txtFG4.Text = "0.00"
        txtCuadrilla1.Text = "0.00"
        txtCuadrilla2.Text = "0.00"
        txtCuadrilla3.Text = "0.00"
        txtCuadrilla4.Text = "0.00"
        txtTotDes.Text = "0.00"

        txtCuadrilla1Pre.Text = "0.00"
        txtCuadrilla2Pre.Text = "0.00"
        txtCuadrilla3Pre.Text = "0.00"
        txtCuadrilla1T.Text = "0.00"
        txtCuadrilla2T.Text = "0.00"
        txtCuadrilla3T.Text = "0.00"
        txtCuadrillaTotales.Text = "0.00"

        btnGrabar.Enabled = True
        btnModificar.Enabled = False
        btnImprimir.Enabled = False
        BeLabel12.Text = "Saldo Contra Acta de Recepci�n ( %)"
        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub


    Private Sub chkCerrar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCerrar.CheckedChanged

        Try
            AxMonthView1.Value = dtpEnvio.Value
            txtSemana.Text = AxMonthView1.Week()
        Catch ex As Exception
        End Try

        If Len(Trim(txtCodigoVal.Text)) > 0 Then
            If chkCerrar.Checked = True Then
                If iCerrarValo = "0" Then
                    If (MessageBox.Show("�Esta seguro de Enviar la Valorizaci�n?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        btnGrabar_Click(sender, e)
                    Else
                        chkCerrar.Checked = False
                    End If
                End If
            End If
        ElseIf Len(Trim(txtCodigoVal.Text)) = 0 Then
            If chkCerrar.Checked = True Then
                MessageBox.Show("Para Enviar esta Valorizaci�n, debe primero Grabar", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                chkCerrar.Checked = False
            End If
            'Exit Sub
        End If

    End Sub

    Private Sub txtNumSesion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumSesion.TextChanged

    End Sub

    Private Sub cboContratista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboContratista.SelectedIndexChanged

    End Sub

    Private Sub cboContratista_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboContratista.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            CCosto.DataSource = eValorizaciones.fListarObras(76, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                CCosto.ValueMember = "CCosCodigo"
                CCosto.DisplayMember = "CCosDescripcion"
                CCosto.SelectedIndex = -1

                'CCosto.DataSource = Nothing
                'CCosto.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
            End If
            'End If
        End If

        'txtCodigoVal.Clear()
        'txtNumeroOrden.Clear()
        'txtNroContrato.Clear()
        'txtNumSesion.Clear()
        'txtSemana.Clear()
        'txtUbicacion.Clear()
        'chkCerrar.Checked = False
        'chkCerrar.Enabled = True
        'dtpFechaIni.Value = Now.Date().ToShortDateString()
        'dtpFechaFin.Value = Now.Date().ToShortDateString()
        'BeLabel11.Text = "              Valorizaci�n N�                 "
        'BeLabel12.Text = "Retenci�n Fondo De Garant�a ( %)"
        'Calcular()
        'For x As Integer = 0 To dgvDescuentos.RowCount - 1
        '    dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
        'Next

        Limpiar()


    End Sub

    Private Sub txtRetPor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRetPor.TextChanged
        'Calcular()
    End Sub

    Private Sub txtAdelanto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAdelanto.TextChanged
        'txtAmorti2.Text = Format(txtAdelanto.Text, "#,##0.00")
        Calcular()
    End Sub

    Private Sub txtOtroDes_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOtroDes.TextChanged
        Calcular()
    End Sub

    Private Sub txtDevFG_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Calcular()
    End Sub



    Private Sub dtpEnvio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEnvio.ValueChanged
        'Try
        '    AxMonthView1.Value = dtpEnvio.Value
        '    txtSemana.Text = AxMonthView1.Week()
        'Catch ex As Exception
        'End Try

        Dim dtTable As DataTable
        dtTable = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTable = ePagoProveedores.fListarParametroIGV(dtpEnvio.Value)
        If dtTable.Rows.Count > 0 Then
            gIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
            gIgvConvertido = Format(gIgv / 100, "0.00")
        Else
            gIgv = 0
            gIgvConvertido = 0
        End If
        BeLabel16.Text = "I.G.V. " & gIgv & " %"

    End Sub

    Private Sub txtSemana_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSemana.KeyPress

        Try
            Select Case Asc(e.KeyChar)
                Case 13
                    If cboObra.SelectedIndex > -1 And cboContratista.SelectedIndex > -1 And cboPartidas.SelectedIndex > -1 Then
                        Dim vFinal As Date
                        AxMonthView1.MultiSelect = False
                        AxMonthView1.Week = CInt(txtSemana.Text)
                        AxMonthView1.DayOfWeek = MSComCtl2.DayConstants.mvwMonday
                        AxMonthView1.MultiSelect = True
                        AxMonthView1.MaxSelCount = 7
                        vFinal = DateAdd(DateInterval.Day, 6, AxMonthView1.SelStart)
                        AxMonthView1.SelEnd = vFinal
                        dtpFechaIni.Value = AxMonthView1.SelStart
                        dtpFechaFin.Value = AxMonthView1.SelEnd
                        dtpEnvio.Value = AxMonthView1.SelEnd
                        TraerValorizacionPorSemana()
                    Else
                        btnModificar.Enabled = False
                        If cboObra.SelectedIndex = -1 Then
                            MessageBox.Show("Seleccione una Obra", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboObra.Focus()
                            Exit Sub
                        End If
                        If cboContratista.SelectedIndex = -1 Then
                            MessageBox.Show("Seleccione un Contratista", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboContratista.Focus()
                            Exit Sub
                        End If
                        If cboPartidas.SelectedIndex = -1 Then
                            MessageBox.Show("Seleccione un Contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboPartidas.Focus()
                            Exit Sub
                        End If
                    End If
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
        txtNumeroOrden.Enabled = False
        txtUbicacion.Enabled = False
        txtNroContrato.Enabled = False
        'txtSemana.Focus()
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Timer1.Enabled = True Then
            txtSemana.Focus()
        End If
        Timer1.Enabled = False
    End Sub

    Dim RetPorAnterior As Double = 0

    Private Sub TraerValorizacionPorSemana()
        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next

        dtpFechaIni.Enabled = False
        dtpFechaFin.Enabled = False

        If Len(Trim(txtSemana.Text)) > 0 Then

            Dim dtTable2 As DataTable
            dtTable2 = New DataTable
            eValorizaciones = New clsValorizaciones
            'capturando si la valorizacion es afecta ala base inponible o al total
            VL_TIPORETENCION = eValorizaciones.fBuscarValorizacion(78, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), Convert.ToString(Trim(txtSemana.Text)), dtpFechaIni.Value, dtpFechaFin.Value, gPeriodo, Trim(CCosto.SelectedValue)).Rows(0)(0).ToString
            If VL_TIPORETENCION = "0" Then
                btnImprimirSunat.Enabled = True
            Else
                btnImprimirSunat.Enabled = False
            End If



            dtTable2 = eValorizaciones.fListarObras(2, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            eValorizaciones = New clsValorizaciones
            Dim dtValorizacion As DataTable
            dtValorizacion = New DataTable
            dtValorizacion = eValorizaciones.fBuscarValorizacion(38, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), Convert.ToString(Trim(txtSemana.Text)), dtpFechaIni.Value, dtpFechaFin.Value, gPeriodo, Trim(CCosto.SelectedValue))
            If dtValorizacion.Rows.Count > 0 Then
                btnModificar.Enabled = True
                Dim IdValorizacion As String = ""
                iCerrarValo = dtValorizacion.Rows(0).Item("Cerrado")
                IdValorizacion = dtValorizacion.Rows(0).Item("IdValorizacion")
                txtCodigoVal.Text = Trim(IdValorizacion)
                dtpFechaIni.Value = dtValorizacion.Rows(0).Item("FechaInicio")
                dtpFechaFin.Value = dtValorizacion.Rows(0).Item("FechaFinal")
                dtpEnvio.Value = dtValorizacion.Rows(0).Item("FechaEnvio")
                BeLabel11.Text = "              Valorizaci�n N� " & Trim(dtValorizacion.Rows(0).Item("Numero")) & "              "

                RetencionPorcentaje = dtValorizacion.Rows(0).Item("PorcRetVal")
                BeLabel12.Text = "Saldo Contra Acta de Recepci�n (" & RetencionPorcentaje & "%)"

                txtNumSesion.Text = Trim(dtValorizacion.Rows(0).Item("Numero"))
                txtSemana.Text = Trim(dtValorizacion.Rows(0).Item("Semana"))

                txtAdelanto1.Text = Trim(dtValorizacion.Rows(0).Item("Adelanto1"))
                txtAdelanto2.Text = Trim(dtValorizacion.Rows(0).Item("Adelanto2"))
                txtAdelanto3.Text = Trim(dtValorizacion.Rows(0).Item("Adelanto3"))
                txtAmorti1.Text = Trim(dtValorizacion.Rows(0).Item("Amorti1"))
                txtAmorti2.Text = Trim(dtValorizacion.Rows(0).Item("Amorti2"))
                txtAmorti3.Text = Trim(dtValorizacion.Rows(0).Item("Amorti3"))
                txtAmorti4.Text = Trim(dtValorizacion.Rows(0).Item("Amorti4"))
                txtFG1.Text = Format(dtValorizacion.Rows(0).Item("FG1"), "#,##0.00")
                txtFG2.Text = Format(IIf(VL_TIPORETENCION = "0", ((dtValorizacion.Rows(0).Item("FG2")) / 1.18), (dtValorizacion.Rows(0).Item("FG2"))), "#,##0.00")
                txtFG3.Text = Trim(dtValorizacion.Rows(0).Item("FG3"))
                txtFG4.Text = Trim(dtValorizacion.Rows(0).Item("FG4"))
                txtCuadrilla1.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla1"))
                txtCuadrilla2.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla2"))
                txtCuadrilla3.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla3"))
                txtCuadrilla4.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla4"))

                txtCuadrilla1Pre.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla1Pre"))
                txtCuadrilla2Pre.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla2Pre"))
                txtCuadrilla3Pre.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla3Pre"))
                txtCuadrilla1T.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla1T"))
                txtCuadrilla2T.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla2T"))
                txtCuadrilla3T.Text = Trim(dtValorizacion.Rows(0).Item("Cuadrilla3T"))
                txtCuadrillaTotales.Text = Trim(dtValorizacion.Rows(0).Item("CuadrillaTT"))

                txtTotDes.Text = Trim(dtValorizacion.Rows(0).Item("totDes"))

                txtSubTotalVal.Text = Trim(dtValorizacion.Rows(0).Item("SubTotalVal"))
                txtSubIGVVal.Text = Trim(dtValorizacion.Rows(0).Item("SubIGVVal"))
                txtTotalVal.Text = Trim(dtValorizacion.Rows(0).Item("TotalVal"))



                txtAdelanto.Text = Trim(dtValorizacion.Rows(0).Item("AmortizAdelanto"))
                txtOtroDes.Text = Trim(dtValorizacion.Rows(0).Item("OtrosDesc"))
                'txtDevFG.Text = Trim(dtValorizacion.Rows(0).Item("DevFG"))
                txtTotPagar.Text = Trim(dtValorizacion.Rows(0).Item("TotPagar"))

                txtBaseSaldoActaR.Text = IIf(Trim(dtValorizacion.Rows(0).Item("TipoRetencion")) = "0", Trim(dtValorizacion.Rows(0).Item("SubTotalVal")), Trim(dtValorizacion.Rows(0).Item("TotalVal")))

                'txtRetPor.Text = IIf(Trim(dtValorizacion.Rows(0).Item("TipoRetencion")) = "0", dtValorizacion.Rows(0).Item("RetPor") * (porcretval), Trim(dtValorizacion.Rows(0).Item("RetPor")))
                txtRetPor.Text = Convert.ToDouble(txtBaseSaldoActaR.Text) * ((dtValorizacion.Rows(0).Item("porcretval")) / 100)

                txtSubTotalFactura.Text = Format(IIf(dtValorizacion.Rows(0).Item("TipoRetencion") = "0", Convert.ToDouble(txtBaseSaldoActaR.Text) - Convert.ToDouble(txtRetPor.Text), Convert.ToDouble(dtValorizacion.Rows(0).Item("SubTotalVal"))), "#,##0.00")
                txtIGVFactura.Text = Format(Convert.ToDecimal(txtSubTotalFactura.Text) * (0.18), "#,##0.00")
                txtTotalFactura.Text = Format(Convert.ToDecimal(txtSubTotalFactura.Text) + (Convert.ToDecimal(txtIGVFactura.Text)), "#,##0.00")
                'txtNumeroOrden.Text = Trim(dtValorizacion.Rows(0).Item("NumOrden"))
                'txtUbicacion.Text = Trim(dtValorizacion.Rows(0).Item("ParUbicacion"))
                'txtNroContrato.Text = Trim(dtValorizacion.Rows(0).Item("NContrato"))

                If iCerrarValo = "0" Then
                    'continuar valorizacion
                    HabilitarControles()
                    btnModificar.Enabled = True
                    btnGrabar.Enabled = False
                    btnImprimir.Enabled = False
                    txtNumSesion.Enabled = True
                ElseIf iCerrarValo = "1" Then
                    'bloquear todo 
                    DesabilitarControles()
                    btnModificar.Enabled = False
                    btnGrabar.Enabled = False
                    btnImprimir.Enabled = True
                    txtNumSesion.Enabled = False
                End If

                txtNumeroOrden.Enabled = False
                txtUbicacion.Enabled = False
                txtNroContrato.Enabled = False

                Dim dtDetDescValorizacion As DataTable
                dtDetDescValorizacion = New DataTable
                dtDetDescValorizacion = eValorizaciones.fBuscarValorizacion(48, gEmpresa, IdValorizacion, Convert.ToString(txtNumSesion.Text.Trim), dtpFechaIni.Value, dtpFechaFin.Value, gPeriodo, Trim(CCosto.SelectedValue))
                If dtDetDescValorizacion.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDescuentos.RowCount - 1
                        dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
                    Next
                    For y As Integer = 0 To dtDetDescValorizacion.Rows.Count - 1
                        dgvDescuentos.Rows.Add()
                        dgvDescuentos.Rows(y).Cells("Des_Descripcion").Value = dtDetDescValorizacion.Rows(y).Item("Descripcion").ToString
                        dgvDescuentos.Rows(y).Cells("Des_Anterior").Value = Format(Convert.ToDouble(dtDetDescValorizacion.Rows(y).Item("Anterior")), "#,##0.0")
                        dgvDescuentos.Rows(y).Cells("Des_Actual").Value = Format(Convert.ToDouble(dtDetDescValorizacion.Rows(y).Item("Actual")), "#,##0.0")
                        dgvDescuentos.Rows(y).Cells("Des_Acumulado").Value = Format(Convert.ToDouble(dtDetDescValorizacion.Rows(y).Item("Acumulado")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("MetradoPor").ToString
                        dgvDescuentos.Rows(y).Cells("Des_IdDetalle").Value = dtDetDescValorizacion.Rows(y).Item("IdDetalleDesc").ToString
                        dgvDescuentos.Rows(y).Cells("Des_Estado").Value = "1"
                    Next
                End If

                Dim dtDetValorizacion As DataTable
                dtDetValorizacion = New DataTable
                If iCerrarValo = "0" Then
                    dtDetValorizacion = eValorizaciones.fBuscarValorizacion(66, gEmpresa, IdValorizacion, Convert.ToString(txtNumSesion.Text.Trim), dtpFechaIni.Value, dtpFechaFin.Value, gPeriodo, Trim(CCosto.SelectedValue))
                ElseIf iCerrarValo = "1" Then
                    dtDetValorizacion = eValorizaciones.fBuscarValorizacion(13, gEmpresa, IdValorizacion, Convert.ToString(txtNumSesion.Text.Trim), dtpFechaIni.Value, dtpFechaFin.Value, gPeriodo, Trim(CCosto.SelectedValue))
                End If
                If dtDetValorizacion.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDetalle.RowCount - 1
                        dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    Next
                    For y As Integer = 0 To dtDetValorizacion.Rows.Count - 1
                        dgvDetalle.Rows.Add()
                        dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetValorizacion.Rows(y).Item("Descripcion").ToString
                        dgvDetalle.Rows(y).Cells("Cant").Value = dtDetValorizacion.Rows(y).Item("Cantidad").ToString
                        dgvDetalle.Rows(y).Cells("Und").Value = dtDetValorizacion.Rows(y).Item("UnidMed").ToString
                        dgvDetalle.Rows(y).Cells("M1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("MetradoPor").ToString
                        dgvDetalle.Rows(y).Cells("M2").Value = dtDetValorizacion.Rows(y).Item("MetradoImpor").ToString
                        dgvDetalle.Rows(y).Cells("PU").Value = dtDetValorizacion.Rows(y).Item("PU").ToString
                        dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetValorizacion.Rows(y).Item("Parcial").ToString

                        dgvDetalle.Rows(y).Cells("AA1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("AA_MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("AA_MetradoPor").ToString
                        dgvDetalle.Rows(y).Cells("AA2").Value = dtDetValorizacion.Rows(y).Item("AA_MetradoImp").ToString
                        dgvDetalle.Rows(y).Cells("AA3").Value = dtDetValorizacion.Rows(y).Item("AA_Mon").ToString

                        dgvDetalle.Rows(y).Cells("V1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("Val_MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("Val_MetradoPor").ToString
                        dgvDetalle.Rows(y).Cells("V2").Value = dtDetValorizacion.Rows(y).Item("Val_MetradoImp").ToString
                        dgvDetalle.Rows(y).Cells("V3").Value = dtDetValorizacion.Rows(y).Item("Val_Mon").ToString

                        'A�ADIENDO EL ACUMULADO
                        dgvDetalle.Rows(y).Cells("MONTacumulado").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("Val_Mon")) + Convert.ToDouble(dtDetValorizacion.Rows(y).Item("AA_Mon")).ToString, "#,##0.0")
                        dgvDetalle.Rows(y).Cells("vametrado").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("MetradoImpor")) - Convert.ToDouble(dtDetValorizacion.Rows(y).Item("Sal_MetradoImp")), "#,##0.0")
                        dgvDetalle.Rows(y).Cells("vapor").Value = Format(100 - Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("Sal_MetradoPor")), "#,##0.0"), "#,##0.0")



                        dgvDetalle.Rows(y).Cells("S1").Value = Format(Convert.ToDouble(dtDetValorizacion.Rows(y).Item("Sal_MetradoPor")), "#,##0.0") 'dtDetValorizacion.Rows(y).Item("Sal_MetradoPor").ToString
                        dgvDetalle.Rows(y).Cells("S2").Value = dtDetValorizacion.Rows(y).Item("Sal_MetradoImp").ToString
                        dgvDetalle.Rows(y).Cells("S3").Value = dtDetValorizacion.Rows(y).Item("Sal_Mon").ToString

                        dgvDetalle.Rows(y).Cells("idDetalle").Value = dtDetValorizacion.Rows(y).Item("IdDetValorizacion").ToString
                        dgvDetalle.Rows(y).Cells("IdPartidaDet").Value = dtDetValorizacion.Rows(y).Item("IdPartidaDet").ToString
                        dgvDetalle.Rows(y).Cells("Estado").Value = "1"
                    Next
                    BeLabel18.Text = "Total de Items : " & dtDetValorizacion.Rows.Count
                    Calcular()

                    'For y As Integer = 0 To dgvDetalle.Rows.Count - 1
                    '    If dgvDetalle.Rows(y).Cells("S1").Value <= 0 Then
                    '        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
                    '            dgvDetalle.Rows(y).Cells(x).ReadOnly = True
                    '        Next
                    '    ElseIf dgvDetalle.Rows(y).Cells("S1").Value > 0 Then
                    '        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
                    '            dgvDetalle.Rows(y).Cells(x).ReadOnly = False
                    '        Next
                    '    End If
                    'Next

                End If

            ElseIf dtValorizacion.Rows.Count = 0 Then
                txtNumSesion.Enabled = True
                Dim dtTable3 As DataTable
                dtTable3 = New DataTable
                eValorizaciones = New clsValorizaciones
                dtTable3 = eValorizaciones.fGenerarNumeroValo(3, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), gPeriodo)
                sMaxValor = eValorizaciones.sCadena
                If dtTable3.Rows.Count > 0 Then 'YA HAY VALORIZACIONES
                    txtCodigoVal.Clear()
                    txtNumSesion.Clear()
                    BeLabel11.Text = "                                                       "
                    Dim vFinal As Date
                    AxMonthView1.MultiSelect = False
                    AxMonthView1.Week = CInt(txtSemana.Text)
                    AxMonthView1.DayOfWeek = MSComCtl2.DayConstants.mvwMonday
                    AxMonthView1.MultiSelect = True
                    AxMonthView1.MaxSelCount = 7
                    vFinal = DateAdd(DateInterval.Day, 6, AxMonthView1.SelStart)
                    AxMonthView1.SelEnd = vFinal
                    dtpFechaIni.Value = AxMonthView1.SelStart
                    dtpFechaFin.Value = AxMonthView1.SelEnd
                    dtpEnvio.Value = AxMonthView1.SelEnd
                    txtNumeroOrden.Enabled = False
                    txtUbicacion.Enabled = False
                    txtNroContrato.Enabled = False

                    Dim dtDetPartida As DataTable
                    dtDetPartida = New DataTable
                    eValorizaciones = New clsValorizaciones
                    dtDetPartida = eValorizaciones.fListarAvanceObras(39, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(CCosto.SelectedValue))
                    If dtDetPartida.Rows.Count > 0 Then
                        eValorizaciones = New clsValorizaciones
                        Dim dtValorizacionAnt As DataTable
                        dtValorizacionAnt = New DataTable
                        dtValorizacionAnt = eValorizaciones.fBuscarValorizacion(77, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), Convert.ToInt32(Trim(txtSemana.Text)) - 1, dtpFechaIni.Value, dtpFechaFin.Value, gPeriodo, Trim(CCosto.SelectedValue))
                        If dtValorizacionAnt.Rows.Count > 0 Then

                            Dim IdValorizacion As String = ""
                            Dim CerradoSINO As String = ""


                            RetPorAnterior = IIf(Microsoft.VisualBasic.IsDBNull(dtValorizacionAnt.Rows(0).Item("FG4")) = True, 0, dtValorizacionAnt.Rows(0).Item("FG4"))

                            IdValorizacion = Trim(dtValorizacionAnt.Rows(0).Item("IdValorizacion"))

                            CerradoSINO = Trim(dtValorizacionAnt.Rows(0).Item("Cerrado"))
                            If Trim(CerradoSINO) = "0" Then
                                MessageBox.Show("No se ha enviado la Valorizaci�n de la semana " & Convert.ToInt32(Trim(txtSemana.Text)) - 1 & ". Env�ela y podr� crear la siguiente (" & Trim(txtSemana.Text) & ").", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                txtSemana.Text = Convert.ToInt32(Trim(txtSemana.Text)) - 1

                                txtAdelanto1.Text = "0.00"
                                txtAdelanto2.Text = "0.00"
                                txtAdelanto3.Text = "0.00"
                                txtAmorti1.Text = "0.00"
                                txtAmorti2.Text = "0.00"
                                txtAmorti3.Text = "0.00"
                                txtAmorti4.Text = "0.00"
                                txtFG1.Text = "0.00"
                                txtFG2.Text = "0.00"
                                txtFG3.Text = "0.00"
                                txtFG4.Text = "0.00"
                                txtCuadrilla1.Text = "0.00"
                                txtCuadrilla2.Text = "0.00"
                                txtCuadrilla3.Text = "0.00"
                                txtCuadrilla4.Text = "0.00"
                                txtTotDes.Text = "0.00"

                                txtCuadrilla1Pre.Text = "0.00"
                                txtCuadrilla2Pre.Text = "0.00"
                                txtCuadrilla3Pre.Text = "0.00"
                                txtCuadrilla1T.Text = "0.00"
                                txtCuadrilla2T.Text = "0.00"
                                txtCuadrilla3T.Text = "0.00"
                                txtCuadrillaTotales.Text = "0.00"

                                'txtDevFG.Text = "0.00"
                                For x As Integer = 0 To dgvDescuentos.RowCount - 1
                                    dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
                                Next
                                Calcular()

                                Exit Sub
                            ElseIf Trim(CerradoSINO) = "1" Then

                                'total de adelantos pagado
                                Dim dtPagado As DataTable
                                dtPagado = New DataTable
                                eValorizaciones = New clsValorizaciones
                                dtPagado = eValorizaciones.fListarAvanceObras(43, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(CCosto.SelectedValue))
                                'suma de adelantos pagados x valorizaciones anteriores
                                Dim dtSumaAdelantos As DataTable
                                dtSumaAdelantos = New DataTable
                                eValorizaciones = New clsValorizaciones
                                dtSumaAdelantos = eValorizaciones.fListarAvanceObras(44, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(CCosto.SelectedValue))
                                Dim Pagado As Double = 0
                                Dim SumaAde As Double = 0
                                If dtPagado.Rows.Count > 0 Then
                                    If Microsoft.VisualBasic.IsDBNull(dtPagado.Rows(0).Item("Pagado")) = False Then
                                        Pagado = dtPagado.Rows(0).Item("Pagado")
                                    End If
                                End If
                                If dtSumaAdelantos.Rows.Count > 0 Then
                                    If Microsoft.VisualBasic.IsDBNull(dtSumaAdelantos.Rows(0).Item("SumaAdelantos")) = False Then
                                        SumaAde = dtSumaAdelantos.Rows(0).Item("SumaAdelantos")
                                    End If
                                End If

                                txtAdelanto1.Text = Format(Pagado - SumaAde, "#,##0.00")
                                txtAdelanto2.Text = "0.00"
                                txtAdelanto3.Text = "0.00"
                                txtAmorti1.Text = "0.00"
                                txtAmorti2.Text = "0.00"
                                txtAmorti3.Text = "0.00"
                                txtAmorti4.Text = "0.00"
                                txtFG1.Text = Format(RetPorAnterior, "#,##0.00")
                                txtFG2.Text = "0.00"
                                txtFG3.Text = "0.00"
                                txtFG4.Text = "0.00"
                                txtCuadrilla1.Text = "0.00"
                                txtCuadrilla2.Text = "0.00"
                                txtCuadrilla3.Text = "0.00"
                                txtCuadrilla4.Text = "0.00"
                                txtTotDes.Text = "0.00"

                                txtCuadrilla1Pre.Text = "0.00"
                                txtCuadrilla2Pre.Text = "0.00"
                                txtCuadrilla3Pre.Text = "0.00"
                                txtCuadrilla1T.Text = "0.00"
                                txtCuadrilla2T.Text = "0.00"
                                txtCuadrilla3T.Text = "0.00"
                                txtCuadrillaTotales.Text = "0.00"

                                'txtDevFG.Text = "0.00"
                                For x As Integer = 0 To dgvDescuentos.RowCount - 1
                                    dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
                                Next
                                BeLabel11.Text = "              Valorizaci�n N� " & sMaxValor & "              "
                            End If

                            Dim dtDetValorizacionAnt As DataTable
                            dtDetValorizacionAnt = New DataTable
                            dtDetValorizacionAnt = eValorizaciones.fBuscarValorizacion(42, gEmpresa, IdValorizacion, 0, dtpFechaIni.Value, dtpFechaFin.Value, gPeriodo, Trim(CCosto.SelectedValue))
                            If dtDetValorizacionAnt.Rows.Count > 0 Then
                                For x As Integer = 0 To dgvDetalle.RowCount - 1
                                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                                Next

                                For y As Integer = 0 To dtDetValorizacionAnt.Rows.Count - 1
                                    dgvDetalle.Rows.Add()
                                    dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetValorizacionAnt.Rows(y).Item("Descripcion").ToString
                                    dgvDetalle.Rows(y).Cells("Cant").Value = dtDetValorizacionAnt.Rows(y).Item("Cantidad").ToString
                                    dgvDetalle.Rows(y).Cells("Und").Value = dtDetValorizacionAnt.Rows(y).Item("UnidMed").ToString
                                    dgvDetalle.Rows(y).Cells("M1").Value = Format(Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("MetradoPor")), "#,##0.0") 'dtDetValorizacionAnt.Rows(y).Item("MetradoPor").ToString
                                    dgvDetalle.Rows(y).Cells("M2").Value = dtDetValorizacionAnt.Rows(y).Item("MetradoImpor").ToString
                                    dgvDetalle.Rows(y).Cells("PU").Value = dtDetValorizacionAnt.Rows(y).Item("Con_PrecioSubContra").ToString
                                    dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetValorizacionAnt.Rows(y).Item("Parcial").ToString
                                    dgvDetalle.Rows(y).Cells("AA1").Value = Format(((Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("AA_MetradoPor"))) + (Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Val_MetradoPor")))), "#,##0.0")
                                    dgvDetalle.Rows(y).Cells("AA2").Value = Format(((Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("AA_MetradoImp"))) + (Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Val_MetradoImp")))), "#,##0.00")
                                    dgvDetalle.Rows(y).Cells("AA3").Value = Format(((Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("AA_Mon"))) + (Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Val_Mon")))), "#,##0.00")

                                    dgvDetalle.Rows(y).Cells("MONTacumulado").Value = Format(((Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("AA_Mon"))) + (Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Val_Mon")))), "#,##0.00")
                                    dgvDetalle.Rows(y).Cells("vametrado").Value = Format(Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("MetradoImpor")) - Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Sal_MetradoImp")), "#,##0.0")
                                    dgvDetalle.Rows(y).Cells("vapor").Value = Format(100 - Format(Convert.ToDouble(dtDetValorizacionAnt.Rows(y).Item("Sal_MetradoPor")), "#,##0.0"), "#,##0.0")

                                    dgvDetalle.Rows(y).Cells("V1").Value = "0.0"
                                    dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
                                    dgvDetalle.Rows(y).Cells("V3").Value = "0.00"
                                    dgvDetalle.Rows(y).Cells("S1").Value = "0.0"
                                    dgvDetalle.Rows(y).Cells("S2").Value = "0.00"
                                    dgvDetalle.Rows(y).Cells("S3").Value = "0.00"
                                    dgvDetalle.Rows(y).Cells("Estado").Value = "0"
                                    dgvDetalle.Rows(y).Cells("IdPartidaDet").Value = dtDetValorizacionAnt.Rows(y).Item("IdPartidaDet").ToString
                                Next

                                For y As Integer = 0 To dtDetPartida.Rows.Count - 1
                                    dgvDetalle.Rows(y).Cells("V2").Value = dtDetPartida.Rows(y).Item("SumaSemana").ToString
                                Next

                                If dgvDetalle.Rows.Count > 0 Then
                                    For y As Integer = 0 To dgvDetalle.Rows.Count - 1
                                        If Trim(dgvDetalle.Rows(y).Cells("V2").Value) <> "" Then
                                            dgvDetalle.Rows(y).Cells("V2").Value = Format(CDec(dgvDetalle.Rows(y).Cells("V2").Value.ToString), "#,##0.00")

                                            If Len(dgvDetalle.Rows(y).Cells("V2").Value) > 0 Then
                                                dgvDetalle.Rows(y).Cells("V1").Value = Format(((dgvDetalle.Rows(y).Cells("M1").Value) * (dgvDetalle.Rows(y).Cells("V2").Value)) / (dgvDetalle.Rows(y).Cells("M2").Value), "#,##0.0")
                                                dgvDetalle.Rows(y).Cells("V3").Value = Format((dgvDetalle.Rows(y).Cells("V2").Value) * (dgvDetalle.Rows(y).Cells("PU").Value), "#,##0.00")

                                                dgvDetalle.Rows(y).Cells("MONTacumulado").Value = Format(Convert.ToDouble(dgvDetalle.Rows(y).Cells("MONTacumulado").Value) + Convert.ToDouble(dgvDetalle.Rows(y).Cells("V3").Value), "#,##0.00")
                                                dgvDetalle.Rows(y).Cells("vametrado").Value = Format(Convert.ToDouble(dgvDetalle.Rows(y).Cells("vametrado").Value) + Convert.ToDouble(dgvDetalle.Rows(y).Cells("V2").Value), "#,##0.00")
                                                dgvDetalle.Rows(y).Cells("vapor").Value = Format(Convert.ToDouble(dgvDetalle.Rows(y).Cells("vapor").Value) + Convert.ToDouble(dgvDetalle.Rows(y).Cells("V1").Value), "#,##0.00")

                                                Dim dSaldo As Double = 0
                                                dSaldo = Convert.ToDouble(dgvDetalle.Rows(y).Cells("AA2").Value) + Convert.ToDouble(dgvDetalle.Rows(y).Cells("V2").Value)
                                                If dSaldo > dgvDetalle.Rows(y).Cells("M2").Value Then
                                                    gestionaResaltados(dgvDetalle, y, Color.Red)
                                                    'MessageBox.Show("El metrado de avance es mayor al metrado del contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                                    'dgvDetalle.Rows(y).Cells("V1").Value = "0.00"
                                                    'dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
                                                    'dgvDetalle.Rows(y).Cells("V3").Value = "0.00"
                                                End If
                                            End If

                                            If vb.IsDBNull(dgvDetalle.Rows(y).Cells("V2").Value) = True Then
                                                dgvDetalle.Rows(y).Cells("V2").Value = Format(CDec(dgvDetalle.Rows(y).Cells("V2").Value.ToString), "#,##0.00")
                                            End If
                                        Else
                                            dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
                                        End If
                                    Next
                                End If

                                BeLabel18.Text = "Total de Items : " & dtDetValorizacionAnt.Rows.Count
                                HabilitarControles()
                                txtCodigoVal.Clear()
                                txtNumeroOrden.Enabled = False
                                txtUbicacion.Enabled = False
                                txtNroContrato.Enabled = False
                                Calcular()
                                'For y As Integer = 0 To dgvDetalle.Rows.Count - 1
                                '    If dgvDetalle.Rows(y).Cells("S1").Value <= 0 Then
                                '        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
                                '            dgvDetalle.Rows(y).Cells(x).ReadOnly = True
                                '        Next
                                '    ElseIf dgvDetalle.Rows(y).Cells("S1").Value > 0 Then
                                '        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
                                '            dgvDetalle.Rows(y).Cells(x).ReadOnly = False
                                '        Next
                                '    End If
                                'Next
                            End If

                        End If
                    ElseIf dtDetPartida.Rows.Count = 0 Then
                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                        BeLabel18.Text = "Total de Items : 0"
                        BeLabel11.Text = "              Valorizaci�n N�                 "
                        txtNumSesion.Clear()
                        MessageBox.Show("No se ha encontrado avances en la semana " & Trim(txtSemana.Text), glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        btnModificar.Enabled = False
                        txtAdelanto1.Text = "0.00"
                        txtAdelanto2.Text = "0.00"
                        txtAdelanto3.Text = "0.00"
                        txtAmorti1.Text = "0.00"
                        txtAmorti2.Text = "0.00"
                        txtAmorti3.Text = "0.00"
                        txtAmorti4.Text = "0.00"
                        txtFG1.Text = "0.00"
                        txtFG2.Text = "0.00"
                        txtFG3.Text = "0.00"
                        txtFG4.Text = "0.00"
                        txtCuadrilla1.Text = "0.00"
                        txtCuadrilla2.Text = "0.00"
                        txtCuadrilla3.Text = "0.00"
                        txtCuadrilla4.Text = "0.00"
                        txtTotDes.Text = "0.00"

                        txtCuadrilla1Pre.Text = "0.00"
                        txtCuadrilla2Pre.Text = "0.00"
                        txtCuadrilla3Pre.Text = "0.00"
                        txtCuadrilla1T.Text = "0.00"
                        txtCuadrilla2T.Text = "0.00"
                        txtCuadrilla3T.Text = "0.00"
                        txtCuadrillaTotales.Text = "0.00"

                        'txtDevFG.Text = "0.00"
                        For x As Integer = 0 To dgvDescuentos.RowCount - 1
                            dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
                        Next
                        Calcular()
                        txtSemana.Focus()
                        btnImprimir.Enabled = False
                        Exit Sub
                    End If

                    txtNumSesion.Text = sMaxValor
                    txtNumSesion.Focus()
                ElseIf dtTable3.Rows.Count = 0 Then

                    'esta partida no tiene ni una valorizacion

                    sMaxValor = eValorizaciones.sCadena
                    txtCodigoVal.Clear()

                    BeLabel11.Text = "              Valorizaci�n N� 1              "


                    '-------------------------------
                    Dim vFinal As Date
                    AxMonthView1.MultiSelect = False
                    'AxMonthView1.Year = CInt(TxtAnio.Text)
                    AxMonthView1.Week = CInt(txtSemana.Text)
                    AxMonthView1.DayOfWeek = MSComCtl2.DayConstants.mvwMonday
                    AxMonthView1.MultiSelect = True
                    AxMonthView1.MaxSelCount = 7
                    vFinal = DateAdd(DateInterval.Day, 6, AxMonthView1.SelStart)
                    AxMonthView1.SelEnd = vFinal
                    dtpFechaIni.Value = AxMonthView1.SelStart
                    dtpFechaFin.Value = AxMonthView1.SelEnd
                    dtpEnvio.Value = AxMonthView1.SelEnd
                    ''-------------------------------
                    txtNumeroOrden.Enabled = False
                    txtUbicacion.Enabled = False
                    txtNroContrato.Enabled = False

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim dtDetPartida As DataTable
                    dtDetPartida = New DataTable
                    eValorizaciones = New clsValorizaciones
                    dtDetPartida = eValorizaciones.fListarAvanceObras(39, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(CCosto.SelectedValue))
                    If dtDetPartida.Rows.Count > 0 Then ' hay avance de obra en la semana ingresada?

                        'total de adelantos pagado
                        Dim dtPagado As DataTable
                        dtPagado = New DataTable
                        eValorizaciones = New clsValorizaciones
                        dtPagado = eValorizaciones.fListarAvanceObras(43, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(CCosto.SelectedValue))
                        'suma de adelantos pagados x valorizaciones anteriores
                        Dim dtSumaAdelantos As DataTable
                        dtSumaAdelantos = New DataTable
                        eValorizaciones = New clsValorizaciones
                        dtSumaAdelantos = eValorizaciones.fListarAvanceObras(44, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(CCosto.SelectedValue))
                        Dim Pagado As Double = 0
                        Dim SumaAde As Double = 0
                        If dtPagado.Rows.Count > 0 Then
                            If Microsoft.VisualBasic.IsDBNull(dtPagado.Rows(0).Item("Pagado")) = False Then
                                Pagado = dtPagado.Rows(0).Item("Pagado")
                            End If
                        End If
                        If dtSumaAdelantos.Rows.Count > 0 Then
                            If Microsoft.VisualBasic.IsDBNull(dtSumaAdelantos.Rows(0).Item("SumaAdelantos")) = False Then
                                SumaAde = dtSumaAdelantos.Rows(0).Item("SumaAdelantos")
                            End If
                        End If

                        txtAdelanto1.Text = Format(Pagado - SumaAde, "#,##0.00")
                        txtAdelanto2.Text = "0.00"
                        txtAdelanto3.Text = "0.00"
                        txtAmorti1.Text = "0.00"
                        txtAmorti2.Text = "0.00"
                        txtAmorti3.Text = "0.00"
                        txtAmorti4.Text = "0.00"
                        txtFG1.Text = "0.00"
                        txtFG2.Text = "0.00"
                        txtFG3.Text = "0.00"
                        txtFG4.Text = "0.00"
                        txtCuadrilla1.Text = "0.00"
                        txtCuadrilla2.Text = "0.00"
                        txtCuadrilla3.Text = "0.00"
                        txtCuadrilla4.Text = "0.00"
                        txtTotDes.Text = "0.00"

                        txtCuadrilla1Pre.Text = "0.00"
                        txtCuadrilla2Pre.Text = "0.00"
                        txtCuadrilla3Pre.Text = "0.00"
                        txtCuadrilla1T.Text = "0.00"
                        txtCuadrilla2T.Text = "0.00"
                        txtCuadrilla3T.Text = "0.00"
                        txtCuadrillaTotales.Text = "0.00"

                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next

                        For y As Integer = 0 To dtDetPartida.Rows.Count - 1
                            dgvDetalle.Rows.Add()
                            dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetPartida.Rows(y).Item("Descripcion").ToString
                            dgvDetalle.Rows(y).Cells("Cant").Value = dtDetPartida.Rows(y).Item("Cantidad").ToString
                            dgvDetalle.Rows(y).Cells("Und").Value = dtDetPartida.Rows(y).Item("UnidMed").ToString
                            dgvDetalle.Rows(y).Cells("M1").Value = Format(Convert.ToDouble(dtDetPartida.Rows(y).Item("MetradoPor")), "#,##0.0") 'dtDetPartida.Rows(y).Item("MetradoPor").ToString
                            dgvDetalle.Rows(y).Cells("M2").Value = dtDetPartida.Rows(y).Item("MetradoImpor").ToString
                            dgvDetalle.Rows(y).Cells("PU").Value = dtDetPartida.Rows(y).Item("Con_PrecioSubContra").ToString

                            dgvDetalle.Rows(y).Cells("AA1").Value = "0.0"
                            dgvDetalle.Rows(y).Cells("AA2").Value = "0.00"
                            dgvDetalle.Rows(y).Cells("AA3").Value = "0.00"

                            'NUevo Campo Agregado acumulado
                            dgvDetalle.Rows(y).Cells("MONTacumulado").Value = dtDetPartida.Rows(y).Item("SumaSemana").ToString
                            dgvDetalle.Rows(y).Cells("vametrado").Value = "0.00"
                            dgvDetalle.Rows(y).Cells("vapor").Value = "0.00"



                            dgvDetalle.Rows(y).Cells("V1").Value = "0.0"
                            dgvDetalle.Rows(y).Cells("V2").Value = dtDetPartida.Rows(y).Item("SumaSemana").ToString
                            dgvDetalle.Rows(y).Cells("V3").Value = "0.00"

                            dgvDetalle.Rows(y).Cells("S1").Value = "0.0"
                            dgvDetalle.Rows(y).Cells("S2").Value = "0.0"
                            dgvDetalle.Rows(y).Cells("S3").Value = "0.00"
                            dgvDetalle.Rows(y).Cells("Estado").Value = "0"
                            dgvDetalle.Rows(y).Cells("IdPartidaDet").Value = dtDetPartida.Rows(y).Item("IdPartidaDet").ToString

                        Next
                        BeLabel18.Text = "Total de Items : " & dtDetPartida.Rows.Count
                        If dgvDetalle.Rows.Count > 0 Then
                            For y As Integer = 0 To dgvDetalle.Rows.Count - 1
                                If Trim(dgvDetalle.Rows(y).Cells("V2").Value) <> "" Then
                                    dgvDetalle.Rows(y).Cells("V2").Value = Format(CDec(dgvDetalle.Rows(y).Cells("V2").Value.ToString), "#,##0.00")




                                    If Len(dgvDetalle.Rows(y).Cells("V2").Value) > 0 Then
                                        dgvDetalle.Rows(y).Cells("V1").Value = Format(((dgvDetalle.Rows(y).Cells("M1").Value) * (dgvDetalle.Rows(y).Cells("V2").Value)) / (dgvDetalle.Rows(y).Cells("M2").Value), "#,##0.0")
                                        dgvDetalle.Rows(y).Cells("V3").Value = Format((dgvDetalle.Rows(y).Cells("V2").Value) * (dgvDetalle.Rows(y).Cells("PU").Value), "#,##0.00")

                                        dgvDetalle.Rows(y).Cells("MONTacumulado").Value = dgvDetalle.Rows(y).Cells("V3").Value
                                        dgvDetalle.Rows(y).Cells("vametrado").Value = dtDetPartida.Rows(y).Item("SumaSemana").ToString
                                        dgvDetalle.Rows(y).Cells("vapor").Value = dgvDetalle.Rows(y).Cells("V1").Value

                                        Dim dSaldo As Double = 0
                                        dSaldo = Convert.ToDouble(dgvDetalle.Rows(y).Cells("AA2").Value) + Convert.ToDouble(dgvDetalle.Rows(y).Cells("V2").Value)
                                        If dSaldo > dgvDetalle.Rows(y).Cells("M2").Value Then
                                            'MessageBox.Show("El metrado de avance es mayor al metrado del contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                            'dgvDetalle.Rows(y).Cells("V1").Value = "0.00"
                                            'dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
                                            'dgvDetalle.Rows(y).Cells("V3").Value = "0.00"
                                            gestionaResaltados(dgvDetalle, y, Color.Red)
                                        End If
                                    End If
                                    If vb.IsDBNull(dgvDetalle.Rows(y).Cells("V2").Value) = True Then
                                        dgvDetalle.Rows(y).Cells("V2").Value = Format(CDec(dgvDetalle.Rows(y).Cells("V2").Value.ToString), "#,##0.00")
                                    End If
                                Else
                                    dgvDetalle.Rows(y).Cells("V2").Value = "0.00"
                                End If
                            Next
                        End If
                        Calcular()
                        HabilitarControles()
                    ElseIf dtDetPartida.Rows.Count = 0 Then
                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                        BeLabel18.Text = "Total de Items : 0"
                        BeLabel11.Text = "              Valorizaci�n N�                 "
                        txtNumSesion.Clear()
                        MessageBox.Show("No se ha encontrado avances en la semana " & Trim(txtSemana.Text), glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Calcular()
                        txtSemana.Focus()
                        Exit Sub
                    End If
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    txtNumSesion.Text = sMaxValor
                    txtNumSesion.Focus()
                End If
            End If
        Else
            MessageBox.Show("Ingrese el N�mero de la Semana!", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtSemana.Focus()
        End If
    End Sub

    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(8).Style.BackColor = c
        visor.Rows(fila).Cells(9).Style.BackColor = c
        visor.Rows(fila).Cells(10).Style.BackColor = c
        visor.Rows(fila).Cells(11).Style.BackColor = c
        visor.Rows(fila).Cells(12).Style.BackColor = c
        visor.Rows(fila).Cells(13).Style.BackColor = c
        visor.Rows(fila).Cells(14).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        visor.Rows(fila).Cells(16).Style.BackColor = c
        visor.Rows(fila).Cells(17).Style.BackColor = c
        visor.Rows(fila).Cells(18).Style.BackColor = c
        visor.Rows(fila).Cells(19).Style.BackColor = c
        visor.Rows(fila).Cells(20).Style.BackColor = c
    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Dim A As Integer
        If dgvDescuentos.Rows.Count = 0 Then
            dgvDescuentos.Rows.Add()
            dgvDescuentos.Rows(0).Cells("Des_Anterior").Value = "0.00"
            dgvDescuentos.Rows(0).Cells("Des_Actual").Value = "0.00"
            dgvDescuentos.Rows(0).Cells("Des_Acumulado").Value = "0.00"
            dgvDescuentos.Rows(0).Cells("Des_Estado").Value = "0"
            dgvDescuentos.CurrentCell = dgvDescuentos(0, 0)
        Else
            Dim IntCod As Integer = 0
            dgvDescuentos.Rows.Add()
            A = dgvDescuentos.Rows.Count
            If dgvDescuentos.Rows(A - 2).Cells("Des_Estado").Value = "0" Or dgvDescuentos.Rows(A - 2).Cells("Des_Estado").Value = "1" Then
                A = dgvDescuentos.Rows.Count
                dgvDescuentos.Rows(A - 1).Cells("Des_Anterior").Value = "0.00"
                dgvDescuentos.Rows(A - 1).Cells("Des_Actual").Value = "0.00"
                dgvDescuentos.Rows(A - 1).Cells("Des_Acumulado").Value = "0.00"
                dgvDescuentos.Rows(A - 1).Cells("Des_Estado").Value = "0"
            Else
                dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index + 1).Cells("Des_Estado").Value = "0"
            End If
            dgvDescuentos.CurrentCell = dgvDescuentos(0, A - 1)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim count As Integer = 0
        Dim CodigoIdDetalle As String = ""
        Dim Estado As String = ""
        If dgvDescuentos.Rows.Count > 0 Then
            If MessageBox.Show("� Desea Eliminar el Item de Descuentos ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                CodigoIdDetalle = dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells("Des_IdDetalle").Value
                Estado = dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells("Des_Estado").Value
                If Estado = "1" Then
                    Dim iResultado2 As Integer = 0
                    eValorizaciones = New clsValorizaciones
                    iResultado2 = eValorizaciones.fGrabarDetalle(49, gEmpresa, Trim(CodigoIdDetalle), Trim(txtCodigoVal.Text), gUsuario, "", 0, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 1, "", gPeriodo)
                    dgvDescuentos.Rows.RemoveAt(dgvDescuentos.CurrentRow.Index)
                Else
                    dgvDescuentos.Rows.RemoveAt(dgvDescuentos.CurrentRow.Index)
                End If
                Calcular()
            End If
        End If
    End Sub

    Private Sub dgvDescuentos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDescuentos.CellContentClick

    End Sub

    Private Sub dgvDescuentos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDescuentos.CellEndEdit
        If e.ColumnIndex = 1 Or e.ColumnIndex = 2 Or e.ColumnIndex = 3 Then
            If Trim(dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvDescuentos.Rows(dgvDescuentos.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If
        End If
        Calcular()
    End Sub

    Private Sub dgvDescuentos_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDescuentos.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_KeypressDetPres
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_KeypressDetPres(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvDescuentos.CurrentCell.ColumnIndex
        If columna = 1 Or columna = 2 Or columna = 3 Then
            Dim caracter As Char = e.KeyChar
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCuadrilla1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuadrilla1.TextChanged
        Calcular()
    End Sub

    Private Sub txtAmorti2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAmorti2.TextChanged
        Calcular()
    End Sub

    Private Sub txtCuadrilla2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuadrilla2.TextChanged
        Calcular()
    End Sub

    Private Sub txtCuadrilla3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuadrilla3.TextChanged
        Calcular()
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        btnModificar.Enabled = False
        btnGrabar.Enabled = True
        btnImprimir.Enabled = True
    End Sub

    Private Sub btnVerAvanceSemanal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerAvanceSemanal.Click
        If cboPartidas.SelectedIndex > -1 Then
            Dim frm As New frmValorizacionesAvanceSemanal
            frm.Owner = Me
            frm.sxIdValorizacion = Trim(cboPartidas.SelectedValue)
            frm.sxCC = Trim(CCosto.SelectedValue)
            frm.F1 = dtpFechaIni.Value
            frm.F2 = dtpFechaFin.Value

            frm.EtiquetaLunes = "Lunes " & dtpFechaIni.Value '"Lunes " & AxMonthView1.SelStart
            frm.EtiquetaMartes = "Martes " & dtpFechaIni.Value.AddDays(1) '"Martes " & AxMonthView1.SelStart.AddDays(1)
            frm.EtiquetaMiercoles = "Mi�rcoles " & dtpFechaIni.Value.AddDays(2) '"Mi�rcoles " & AxMonthView1.SelStart.AddDays(2)
            frm.EtiquetaJueves = "Jueves " & dtpFechaIni.Value.AddDays(3) '"Jueves " & AxMonthView1.SelStart.AddDays(3)
            frm.EtiquetaViernes = "Viernes " & dtpFechaIni.Value.AddDays(4) '"Viernes " & AxMonthView1.SelStart.AddDays(4)
            frm.EtiquetaSabado = "S�bado " & dtpFechaIni.Value.AddDays(5) '"S�bado " & AxMonthView1.SelStart.AddDays(5)
            frm.EtiquetaDomingo = "Domingo " & dtpFechaIni.Value.AddDays(6) '"Domingo " & AxMonthView1.SelStart.AddDays(6)

            frm.ShowInTaskbar = False
            frm.ShowDialog()
        Else
            MessageBox.Show("No Existe Detalle de Avance", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub txtCuadrilla1Pre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuadrilla1Pre.TextChanged
        Calcular()
    End Sub

    Private Sub txtCuadrilla2Pre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuadrilla2Pre.TextChanged
        Calcular()
    End Sub

    Private Sub txtCuadrilla3Pre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCuadrilla3Pre.TextChanged
        Calcular()
    End Sub

    Private Sub cboContratista_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboContratista.SizeChanged

    End Sub

    Private Sub CCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CCosto.SelectedIndexChanged

    End Sub

    Private Sub CCosto_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles CCosto.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            cboPartidas.DataSource = eValorizaciones.fListarObras(15, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If eValorizaciones.iNroRegistros > 0 Then
                cboPartidas.ValueMember = "IdPartida"
                cboPartidas.DisplayMember = "ParDescripcion"
                cboPartidas.SelectedIndex = -1
            End If
        End If


        'txtCodigoVal.Clear()
        'txtNumeroOrden.Clear()
        'txtNroContrato.Clear()
        'txtNumSesion.Clear()
        'txtSemana.Clear()
        'txtUbicacion.Clear()
        'chkCerrar.Checked = False
        'chkCerrar.Enabled = True
        'dtpFechaIni.Value = Now.Date().ToShortDateString()
        'dtpFechaFin.Value = Now.Date().ToShortDateString()
        'BeLabel11.Text = "              Valorizaci�n N�                 "
        'BeLabel12.Text = "Retenci�n Fondo De Garant�a ( %)"
        'Calcular()
        'For x As Integer = 0 To dgvDescuentos.RowCount - 1
        '    dgvDescuentos.Rows.Remove(dgvDescuentos.CurrentRow)
        'Next
        Limpiar()
    End Sub

    Private Sub btnImprimirSunat_Click(sender As Object, e As EventArgs) Handles btnImprimirSunat.Click
        If Len(Trim(txtCodigoVal.Text)) > 0 Then
            Dim x As frmValorizacionesReporteConta = frmValorizacionesReporteConta.Instance
            'x.MdiParent = frmPrincipal
            x.IdValorizacion = Trim(txtCodigoVal.Text)
            x.IdCC = Trim(CCosto.SelectedValue)
            x.Show()
            'x.ShowDialog()
        ElseIf Len(Trim(txtCodigoVal.Text)) = 0 Then
            MessageBox.Show("Solo se Imprime Valorizaciones Grabadas", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtSemana.Focus()
        End If
    End Sub

    Private Sub txtSemana_TextChanged(sender As Object, e As EventArgs) Handles txtSemana.TextChanged

    End Sub
End Class