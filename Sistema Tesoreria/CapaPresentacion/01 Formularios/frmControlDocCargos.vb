Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmControlDocCargos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmControlDocCargos = Nothing
    Public Shared Function Instance() As frmControlDocCargos
        If frmInstance Is Nothing Then
            frmInstance = New frmControlDocCargos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmControlDocCargos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eFacturas As clsFacturas

    'Private Sub btnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVer.Click
    '    eFacturas = New clsFacturas

    '    Dim dtBuscaCabeceraCargoxNumero As DataTable
    '    dtBuscaCabeceraCargoxNumero = New DataTable
    '    dtBuscaCabeceraCargoxNumero = eFacturas.fExisteNumCargo(gEmpresa, Trim(Convert.ToString(txtNCargo.Text)), Trim(gPeriodo))

    '    If dtBuscaCabeceraCargoxNumero.Rows.Count > 0 Then

    '    ElseIf dtBuscaCabeceraCargoxNumero.Rows.Count = 0 Then

    '    End If

    'End Sub

    Private Sub txtNro_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNro.TextChanged

        'If chkPendientes.Checked = True And chkArchivados.Checked = True Then
        '    MsgBox("Seleccione un estado!", MsgBoxStyle.Information, glbNameSistema)
        '    cboTodosProv.Focus()
        'End If

        eFacturas = New clsFacturas
        Dim dtBuscaCabeceraCargoxNumero As DataTable
        dtBuscaCabeceraCargoxNumero = New DataTable
        If chkPendientes.Checked = True Then
            btnArchivar.Text = "Archivar"
            If Len(Trim(txtNro.Text)) > 0 Then
                dtBuscaCabeceraCargoxNumero = eFacturas.fFiltrarNumCargo(47, gEmpresa, Trim(Convert.ToString(txtNro.Text)), Trim(gPeriodo), 0)
            ElseIf Len(Trim(txtNro.Text)) = 0 Then
                dtBuscaCabeceraCargoxNumero = eFacturas.fFiltrarNumCargo(48, gEmpresa, Trim(Convert.ToString(txtNro.Text)), Trim(gPeriodo), 0)
            End If
        End If
        If chkArchivados.Checked = True Then
            btnArchivar.Text = "Devolver a Pendientes"
            If Len(Trim(txtNro.Text)) > 0 Then
                dtBuscaCabeceraCargoxNumero = eFacturas.fFiltrarNumCargo(49, gEmpresa, Trim(Convert.ToString(txtNro.Text)), Trim(gPeriodo), 1)
            ElseIf Len(Trim(txtNro.Text)) = 0 Then
                dtBuscaCabeceraCargoxNumero = eFacturas.fFiltrarNumCargo(50, gEmpresa, Trim(Convert.ToString(txtNro.Text)), Trim(gPeriodo), 1)
            End If
        End If
        If dtBuscaCabeceraCargoxNumero.Rows.Count > 0 Then
            dgvCargos.DataSource = dtBuscaCabeceraCargoxNumero
            Label2.Text = "Cantidad de Cargos : " & dtBuscaCabeceraCargoxNumero.Rows.Count
        ElseIf dtBuscaCabeceraCargoxNumero.Rows.Count = 0 Then
            For x As Integer = 0 To dgvCargos.RowCount - 1
                dgvCargos.Rows.Remove(dgvCargos.CurrentRow)
            Next

            For x As Integer = 0 To dgvDetalle.RowCount - 1
                dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
            Next

            For x As Integer = 0 To dgvAnexo.RowCount - 1
                dgvAnexo.Rows.Remove(dgvAnexo.CurrentRow)
            Next

            Label2.Text = "Cantidad de Cargos : 0"
            Label3.Text = "Total de Documentos: 0"
            Label5.Text = "Total de Documentos: 0"
            GroupBox1.Text = ""
        End If

        'If dgvCargos.Rows.Count > 0 Then
        '    For z As Integer = 0 To dgvCargos.Rows.Count - 1
        '        Dim Soles As String = ""
        '        Dim Dolares As String = ""
        '        Soles = dgvCargos.Rows(z).Cells("S").Value
        '        Dolares = dgvCargos.Rows(z).Cells("D").Value
        '        If Soles > 0 And Dolares > 0 Then
        '            dgvCargos.Rows(z).Cells("Moneda").Value = "S/.  -  US$"
        '        ElseIf Soles > 0 And Dolares = 0 Then
        '            dgvCargos.Rows(z).Cells("Moneda").Value = "S/."
        '        ElseIf Soles = 0 And Dolares > 0 Then
        '            dgvCargos.Rows(z).Cells("Moneda").Value = "US$"
        '        ElseIf Soles = 0 And Dolares = 0 Then
        '            dgvCargos.Rows(z).Cells("Moneda").Value = "Ninguno"
        '        End If
        '    Next
        'End If

        'If dtBuscaCabeceraCargoxNumero.Rows.Count > 0 Then

        'ElseIf dtBuscaCabeceraCargoxNumero.Rows.Count = 0 Then

        'End If

    End Sub

   


    Private Sub frmControlDocCargos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtNro.Focus()

        txtNro_TextChanged(sender, e)

    End Sub

    Private Sub chkPendientes_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPendientes.CheckedChanged
        txtNro.Focus()
        txtNro_TextChanged(sender, e)
    End Sub

    Private Sub chkArchivados_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkArchivados.CheckedChanged
        txtNro.Focus()
        txtNro_TextChanged(sender, e)
    End Sub

    Private Sub btnArchivar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnArchivar.Click
        If dgvCargos.Rows.Count > 0 Then

            Dim ContaRecibidos As Integer = 0
            Dim ContaRechazados As Integer = 0
            Dim rEnviaDoc As Integer = 0
            Dim rEnviaDoc2 As Integer = 0
            Dim RecibirODevolver As Integer = 0
            Dim Mensaje As String = ""

            If chkPendientes.Checked = True Then
                RecibirODevolver = 1
                Mensaje = "Cargos Archivados : "
            End If
            If chkArchivados.Checked = True Then
                RecibirODevolver = 0
                Mensaje = "Cargos Devueltos : "
            End If

            For z As Integer = 0 To dgvCargos.Rows.Count - 1
                If Convert.ToBoolean(dgvCargos.Rows(z).Cells("Column8").Value) = True Then
                    If Len(Trim(dgvCargos.Rows(z).Cells("Column7").Value)) Then
                        eFacturas = New clsFacturas
                        Dim IdCargo As String = ""
                        Dim FechaRecibo As DateTime
                        IdCargo = Trim(dgvCargos.Rows(z).Cells("Column7").Value)
                        FechaRecibo = Trim(dgvCargos.Rows(z).Cells("Column10").Value)
                        rEnviaDoc = eFacturas.fActualizarCargosYDocumentos(IdCargo, gEmpresa, RecibirODevolver, FechaRecibo)
                    End If
                    ContaRecibidos = ContaRecibidos + 1
                End If
            Next

            If ContaRecibidos = 0 Then
                MessageBox.Show("Seleccione un Cargo de la Lista para Archivar o Devolver", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If


            MessageBox.Show(Mensaje & ContaRecibidos, "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtNro.Clear()
            txtNro.Focus()
            txtNro_TextChanged(sender, e)

        End If
    End Sub

    Private Sub dgvCargos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCargos.CellContentClick

    End Sub

    Private Sub dgvCargos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCargos.DoubleClick
        If dgvCargos.Rows.Count > 0 Then
            Dim IdCargoReport As String = ""
            Dim NumeroCargo As String = ""
            IdCargoReport = Trim(dgvCargos.Rows(dgvCargos.CurrentRow.Index).Cells("Column7").Value())
            NumeroCargo = Trim(dgvCargos.Rows(dgvCargos.CurrentRow.Index).Cells("Column1").Value())
            GroupBox1.Text = "Detalle del Cargo N�mero " & NumeroCargo
            If Len(Trim(IdCargoReport)) > 0 Then
                Dim dtDetCabeceraCronoReport As DataTable
                dtDetCabeceraCronoReport = New DataTable
                dtDetCabeceraCronoReport = eFacturas.fLisDetCabeceraCronoReport(gEmpresa, Trim(IdCargoReport))
                If dtDetCabeceraCronoReport.Rows.Count > 0 Then
                    If dgvDetalle.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                    End If
                    dgvDetalle.DataSource = dtDetCabeceraCronoReport
                    'Dim y As Integer
                    'If dgvDetalle.Rows.Count > 0 Then
                    '    For y = 0 To dgvDetalle.Rows.Count - 1
                    '        If dgvDetalle.Rows(y).Cells("Column31").Value = "1" Then
                    '            gestionaResaltados(dgvDetalle, y, Color.Orange)
                    '        End If
                    '    Next
                    'End If
                    Label3.Text = "Total de Documentos: " & dtDetCabeceraCronoReport.Rows.Count
                    'iCant1 = dtDetCabeceraCronoReport.Rows.Count
                ElseIf dtDetCabeceraCronoReport.Rows.Count = 0 Then
                    If dgvDetalle.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                    End If
                    Label3.Text = "Total de Documentos: " & dtDetCabeceraCronoReport.Rows.Count
                    'iCant1 = dtDetCabeceraCronoReport.Rows.Count
                End If

                Dim dtDetAnexo As DataTable
                dtDetAnexo = New DataTable
                dtDetAnexo = eFacturas.fLisDetAnexoCronoReport(gEmpresa, Trim(IdCargoReport))
                If dtDetAnexo.Rows.Count > 0 Then
                    If dgvAnexo.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvAnexo.RowCount - 1
                            dgvAnexo.Rows.Remove(dgvAnexo.CurrentRow)
                        Next
                    End If
                    dgvAnexo.DataSource = dtDetAnexo
                    Label5.Text = "Total de Documentos: " & dgvAnexo.Rows.Count
                    'iCant2 = dgvAnexo.Rows.Count
                ElseIf dtDetAnexo.Rows.Count = 0 Then
                    If dgvAnexo.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvAnexo.RowCount - 1
                            dgvAnexo.Rows.Remove(dgvAnexo.CurrentRow)
                        Next
                    End If
                    Label5.Text = "Total de Documentos: " & dgvAnexo.Rows.Count
                    'iCant2 = dgvAnexo.Rows.Count
                End If
            End If
        End If
    End Sub

    Private Sub dgvCargos_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvCargos.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvCargos.CurrentCell.ColumnIndex
        If columna = 7 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
                e.KeyChar = Chr(0)
            End If
        End If

    End Sub

    Private Sub dgvCargos_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvCargos.SelectionChanged
        dgvCargos_DoubleClick(sender, e)
    End Sub

    
End Class