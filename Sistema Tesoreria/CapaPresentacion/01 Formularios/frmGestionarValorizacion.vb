﻿Imports CapaNegocios
Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad
Imports CapaEntidad.Mantenimiento
Imports BusinessLogicLayer.Globales
Imports Telerik.WinControls
Imports Telerik.WinControls.UI
Imports System.Transactions
Imports BusinessLogicLayer.Procesos
Imports CapaEntidad.Proceso

Public Class frmGestionarValorizacion

#Region "Instanciar"
    Private Shared frmInstance As frmGestionarValorizacion = Nothing
    Public Shared Function Instance() As frmGestionarValorizacion
        If frmInstance Is Nothing Then
            frmInstance = New frmGestionarValorizacion
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
#End Region

#Region "Declaraciones"

    Dim VL_BeanContratoSubcontratos = New BeanContratoSubcontratos()

    Dim Co_Contrato As String
    Dim Co_Obra As String
    Dim Co_Proveedor As String
    Dim Nu_Contrato As Integer
    Dim Ss_MontoContrato As String
    Dim Tx_DescrpcionContrato As String
    Dim Po_Scar As Decimal
    Dim Fl_ModoPagoRetencion As Integer
    Dim Ss_ValorizacionAcumulada As Decimal
    Dim Ss_SaldoValorizacion As Decimal
    Dim Ss_ScarAcumulada As Decimal
    Dim Ss_DescuentoAcumulado As Decimal
    Dim Ss_AmortizacionAcumulada As Decimal

    Dim Ss_FacturaSubTotal As Decimal
    Dim Ss_FacturaIgv As Decimal
    Dim Ss_FacturaTotal As Decimal


    Dim Fl_EsNuevo As Boolean

    'registrodocumento

    Dim IdRegistro As String


    Dim Po_ScarContrato As Decimal
    Dim Fl_ModoPagoRetencionContrato As Integer

    Dim Ss_MontoPagar As Decimal
#End Region

#Region "procedimientos"
    Private Sub ListarProveedor()

        Dim vl_clsRegistroDocumento As New clsRegistroDocumento()
        Dim VL_BeanResultado As New DataTable()

        Try
            VL_BeanResultado = vl_clsRegistroDocumento.fListarProveedoresxRuc(gEmpresa, txtContratoRuc.Text, "P")

            If (VL_BeanResultado.Rows.Count > 0) Then
                'cboDocumento.Focus()
                'txtCod.Text = Trim(dtTable.Rows(0).Item("AnaliticoCodigo"))
                txtContratoSubcontratista.Text = Trim(VL_BeanResultado.Rows(0).Item("AnaliticoDescripcion"))
                txtContratoRuc.Text = Trim(VL_BeanResultado.Rows(0).Item("Ruc"))
                lblContratoCodigoProveedor.Text = Trim(VL_BeanResultado.Rows(0).Item("Ruc"))

            Else

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub ListarObra(ByVal cbo_obra As Telerik.WinControls.UI.RadDropDownList)

        Dim VL_SrvGlobal As New SrvGlobal()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim VL_BeanContratoSubcontratos = New BeanContratoSubcontratos()

        Try
            VL_BeanResultado = VL_SrvGlobal.Fnc_Listar_Obras(gEmpresa)

            If VL_BeanResultado.blnExiste = True Then
                cbo_obra.DataSource = VL_BeanResultado.dtResultado
                cbo_obra.DisplayMember = "ObraDescripcion"
                cbo_obra.ValueMember = "ObraCodigo"

                cbo_obra.SelectedIndex = -1

            Else

                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub ListarContratos()

        Dim VL_ServContratoSubcontrato As New ServContratoSubcontrato()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()


        Try
            VL_BeanResultado = VL_ServContratoSubcontrato.Fnc_listar_contrato(gEmpresa)

            If VL_BeanResultado.blnExiste = True Then
                rgvContratos.DataSource = VL_BeanResultado.dtResultado
            Else

                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub ListarFacturasPendientesAsignacion()

        Dim VL_SrvRegistroDocumento As New SrvRegistroDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Dim BeanRegistroDocumento = New BeanRegistroDocumento(gEmpresa, gPeriodo)


        Try
            VL_BeanResultado = VL_SrvRegistroDocumento.Fnc_listar_documentoPendienteAsignacion(BeanRegistroDocumento)

            If VL_BeanResultado.blnExiste = True Then
                rgvValorizacionFacturas.DataSource = VL_BeanResultado.dtResultado
            Else

                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub ListarContratosPorObra()
        Dim VL_SrvGlobal As New SrvGlobal()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim VL_BeanContratoSubcontratos = New BeanContratoSubcontratos()

        Try
            VL_BeanResultado = VL_SrvGlobal.Fnc_listar_ContratoxObra(gEmpresa, cboValorizacionObra.SelectedValue, txtValorizacionRuc.Text)

            If VL_BeanResultado.blnExiste = True Then
                cboValorizacionNroContrato.DataSource = VL_BeanResultado.dtResultado
                cboValorizacionNroContrato.DisplayMember = "Nu_Contrato"
                cboValorizacionNroContrato.ValueMember = "Co_Contrato"

                cboValorizacionNroContrato.SelectedIndex = -1
            Else

                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try

    End Sub
    Private Sub MostrarDatosContrato()
        Dim VL_SrvGlobal As New SrvGlobal()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim VL_BeanContratoSubcontratos = New BeanContratoSubcontratos()

        Try
            VL_BeanResultado = VL_SrvGlobal.Fnc_listar_DatosContrato(cboValorizacionNroContrato.SelectedValue)

            If VL_BeanResultado.blnExiste = True Then
                txtValorizacionPresupuesto.Text = VL_BeanResultado.dtResultado.Rows(0)("Ss_MontoContrato").ToString()
                txtvalorizacionAnterior.Text = VL_BeanResultado.dtResultado.Rows(0)("Ss_ValorizacionAcumulada").ToString()
                txtValorizacionScarAnterior.Text = VL_BeanResultado.dtResultado.Rows(0)("Ss_ScarAcumulada").ToString()
                txtValorizacionDescuentoAnterior.Text = VL_BeanResultado.dtResultado.Rows(0)("Ss_DescuentoAcumulado").ToString()
                Po_ScarContrato = VL_BeanResultado.dtResultado.Rows(0)("Po_Scar").ToString()
                Fl_ModoPagoRetencionContrato = VL_BeanResultado.dtResultado.Rows(0)("Fl_ModoPagoRetencion").ToString()

                Co_Contrato = VL_BeanResultado.dtResultado.Rows(0)("Co_Contrato").ToString()

                txtValorizacionScar.Text = (Po_ScarContrato / 100) * rseValorizacionMonto.Value
            Else

                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try

    End Sub
    Private Sub CargarValorizacionesContrato()
        Dim VL_SrvValorizacionContrato As New SrvValorizacionContrato()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim VL_BeanValorizacionContrato = New BeanValorizacionContrato()

        Try
            VL_BeanResultado = VL_SrvValorizacionContrato.Fnc_listar_Valorizacionescontrato(cboValorizacionNroContrato.SelectedValue)

            If VL_BeanResultado.blnExiste = True Then
                rgvValorizacionContrato.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try

    End Sub


    Private Sub ValidarDatosContrato()
        If txtValorizacionRuc.Text = "" Then
            MessageBox.Show("Cargue Datos de Proveedor", "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If cboValorizacionObra.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione Una Obra", "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If cboValorizacionNroContrato.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione Un Contrato", "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If rseValorizacionNro.Value = 0 Then
            MessageBox.Show("Coloque el Nro Valorizacion", "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If rseValorizacionMonto.Value = 0 Then
            MessageBox.Show("Coloque Monto Valorizacion", "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If rseValorizacionSemana.Value = 0 Then
            MessageBox.Show("Coloque la Semana", "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
    End Sub
#End Region

#Region "Procesos"
    Private Sub AgregarContrato()
        'Dim rowactual As Integer
        Try

            Dim VL_ServContratoSubcontrato As New ServContratoSubcontrato()
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

            Co_Obra = cboContratoObra.SelectedValue
            Co_Proveedor = lblContratoCodigoProveedor.Text
            Nu_Contrato = txtContratoNroContrato.Text
            Ss_MontoContrato = txtContratoMonto.Text
            Tx_DescrpcionContrato = txtContratoDescripcion.Text
            Po_Scar = rseContratoScar.Value
            Fl_ModoPagoRetencion = IIf(rbContratoFacturado.IsChecked, 0, 1)

            Dim VL_BeanContratoSubcontratos = New BeanContratoSubcontratos(gEmpresa, Co_Obra, Co_Proveedor, Nu_Contrato, Ss_MontoContrato, Tx_DescrpcionContrato, Po_Scar, Fl_ModoPagoRetencion, gUsuario)

            VL_BeanResultado = VL_ServContratoSubcontrato.Fnc_Registrar_ContratoSubcontrato(VL_BeanContratoSubcontratos)
            If VL_BeanResultado.blnResultado = True Then
                rgvContratos.DataSource = VL_BeanResultado.dtResultado

            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End If

        Catch ex As Exception

        End Try

    End Sub

    Private Sub AgregarValorizacionContrato()
        'Dim rowactual As Integer
        Try

            Dim VL_SrvValorizacionContrato As New SrvValorizacionContrato()
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()



            Dim VL_BeanValorizacionContrato = New BeanValorizacionContrato(Co_Contrato, IdRegistro, rseValorizacionNro.Value, rseValorizacionMonto.Value, 0, rseValorizacionDescuento.Value, 0, Convert.ToDecimal(txtValorizacionScar.Text), Convert.ToDecimal(txtValorizacionSubTotal.Text), Convert.ToDecimal(txtValorizacionIgv.Text), Convert.ToDecimal(txtValorizacionTotal.Text), Convert.ToDecimal(txtValorizacionNetoaPagar.Text), gUsuario, rseValorizacionSemana.Value)

            VL_BeanResultado = VL_SrvValorizacionContrato.Fnc_Registrar_Valorizacioncontrato(VL_BeanValorizacionContrato)
            If VL_BeanResultado.blnResultado = True Then
                rgvValorizacionContrato.DataSource = VL_BeanResultado.dtResultado
                rgbValorizacion.Enabled = False
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End If

        Catch ex As Exception

        End Try

    End Sub


    Private Function EliminarContrato() As Boolean
        'Dim rowactual As Integer
        Try

            Dim VL_ServContratoSubcontrato As New ServContratoSubcontrato()
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

            Co_Contrato = ""

            Dim VL_BeanContratoSubcontratos = New BeanContratoSubcontratos(Co_Contrato)

            VL_BeanResultado = VL_ServContratoSubcontrato.Fnc_Eliminar_ContratoSubcontrato(VL_BeanContratoSubcontratos)
            If VL_BeanResultado.blnResultado = True Then
                rgvContratos.DataSource = VL_BeanResultado.dtResultado
                Return True
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Function ModificarContrato() As Boolean
        'Dim rowactual As Integer
        Try

            Dim VL_ServContratoSubcontrato As New ServContratoSubcontrato()
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

            Co_Obra = cboContratoObra.SelectedValue
            Co_Proveedor = lblContratoCodigoProveedor.Text
            Nu_Contrato = txtContratoNroContrato.Text
            Ss_MontoContrato = txtContratoMonto.Text
            Tx_DescrpcionContrato = txtContratoDescripcion.Text
            Po_Scar = rseContratoScar.Value
            Fl_ModoPagoRetencion = IIf(rbContratoFacturado.IsChecked, 0, 1)

            Dim VL_BeanContratoSubcontratos = New BeanContratoSubcontratos(Co_Contrato, gEmpresa, Co_Obra, Co_Proveedor, Nu_Contrato, Ss_MontoContrato, Tx_DescrpcionContrato, Po_Scar, Fl_ModoPagoRetencion, gUsuario)

            VL_BeanResultado = VL_ServContratoSubcontrato.Fnc_Modificar_ContratoSubcontrato(VL_BeanContratoSubcontratos)
            If VL_BeanResultado.blnResultado = True Then
                rgvContratos.DataSource = VL_BeanResultado.dtResultado
                Return True
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Sub CargarVariables()
        Co_Contrato = rgvContratos.CurrentRow.Cells("Co_Contrato").Value
        Co_Obra = rgvContratos.CurrentRow.Cells("Co_Obra").Value
        Co_Proveedor = rgvContratos.CurrentRow.Cells("Co_Proveedor").Value
        Nu_Contrato = rgvContratos.CurrentRow.Cells("Nu_Contrato").Value
        Ss_MontoContrato = rgvContratos.CurrentRow.Cells("Ss_MontoContrato").Value
        Tx_DescrpcionContrato = rgvContratos.CurrentRow.Cells("Tx_DescrpcionContrato").Value
        Po_Scar = rgvContratos.CurrentRow.Cells("Po_Scar").Value
        Fl_ModoPagoRetencion = rgvContratos.CurrentRow.Cells("Fl_ModoPagoRetencion").Value

    End Sub
    Private Sub CargarValoresValorizacion()
        Try
            Using scope As TransactionScope = New TransactionScope

                For Each fila As GridViewRowInfo In Me.rgvValorizacionFacturas.Rows
                    fila.Cells("Seleccion").Value = False
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try

        rgvValorizacionFacturas.CurrentRow.Cells("Seleccion").Value = True
        IdRegistro = rgvValorizacionFacturas.CurrentRow.Cells("IdRegistro").Value

        Ss_FacturaSubTotal = rgvValorizacionFacturas.CurrentRow.Cells("ValorCompra").Value
        Ss_FacturaIgv = rgvValorizacionFacturas.CurrentRow.Cells("ImporteIgv").Value
        Ss_FacturaTotal = rgvValorizacionFacturas.CurrentRow.Cells("ImporteTotal").Value



        txtValorizacionRuc.Text = rgvValorizacionFacturas.CurrentRow.Cells("CodProveedor").Value
        txtValorizacionRazonSocial.Text = rgvValorizacionFacturas.CurrentRow.Cells("NombreProveedor").Value
        rgbValorizacion.Text = "Datos de Valorizacion -" + rgvValorizacionFacturas.CurrentRow.Cells("AreaNombre").Value
        cboValorizacionObra.SelectedIndex = -1
        cboValorizacionNroContrato.DataSource = Nothing
        rgvValorizacionContrato.DataSource = Nothing
    End Sub



    Private Sub PintarVariables()
        cboContratoObra.SelectedValue = Co_Obra
        txtContratoRuc.Text = Co_Proveedor
        lblContratoCodigoProveedor.Text = Co_Proveedor
        txtContratoNroContrato.Text = Nu_Contrato
        txtContratoMonto.Text = Ss_MontoContrato
        txtContratoDescripcion.Text = Tx_DescrpcionContrato
        rseContratoScar.Value = Po_Scar
        If (Fl_ModoPagoRetencion = 0) Then
            rbContratoFacturado.IsChecked = True
            rbContratoNoFacturado.IsChecked = False
        Else
            rbContratoFacturado.IsChecked = False
            rbContratoNoFacturado.IsChecked = True
        End If

    End Sub

    Private Sub LimpiarControles()
        Co_Contrato = ""
        Co_Obra = ""
        cboContratoObra.SelectedIndex = -1
        Co_Proveedor = ""
        lblContratoCodigoProveedor.Text = ""
        txtContratoRuc.Text = ""
        Nu_Contrato = 0
        txtContratoNroContrato.Text = ""
        Ss_MontoContrato = ""
        txtContratoMonto.Text = ""
        Tx_DescrpcionContrato = ""
        txtContratoDescripcion.Text = ""
        Po_Scar = 0
        rseContratoScar.Value = 0

    End Sub

    Private Sub CalcularNetoAPAgar()

        txtValorizacionActual.Text = Format(rseValorizacionMonto.Value, "#,##0.00")
        txtValorizacionAcumulada.Text = Format(rseValorizacionMonto.Value + Convert.ToDecimal(txtvalorizacionAnterior.Text), "#,##0.00")
        txtValorizacionSaldo.Text = Format(Convert.ToDecimal(txtValorizacionPresupuesto.Text) - Convert.ToDecimal(txtValorizacionAcumulada.Text), "#,##0.00")


        If Fl_ModoPagoRetencion = 0 Then
            txtValorizacionScar.Text = Format((Po_ScarContrato / 100) * rseValorizacionMonto.Value, "#,##0.00")
            txtValorizacionSubTotal.Text = Format(rseValorizacionMonto.Value, "#,##0.00")
            txtValorizacionIgv.Text = Format(Convert.ToDecimal(txtValorizacionSubTotal.Text) * 0.18, "#,##0.00")
            txtValorizacionTotal.Text = Format(Convert.ToDecimal(txtValorizacionSubTotal.Text) + Convert.ToDecimal(txtValorizacionIgv.Text), "#,##0.00")
        Else
            txtValorizacionScar.Text = Format((Po_ScarContrato / 100) * ((rseValorizacionMonto.Value * 100) / 118), "#,##0.00")
            txtValorizacionSubTotal.Text = Format(((rseValorizacionMonto.Value * 100) / 118) - rseValorizacionDescuento.Value - (((rseValorizacionMonto.Value * 100) / 118) * Po_ScarContrato / 100), "#,##0.00")
            txtValorizacionIgv.Text = Format(Convert.ToDecimal(txtValorizacionSubTotal.Text) * 0.18, "#,##0.00")
            txtValorizacionTotal.Text = Format(Convert.ToDecimal(txtValorizacionSubTotal.Text) + Convert.ToDecimal(txtValorizacionIgv.Text), "#,##0.00")
        End If
        Ss_MontoPagar = Format(rseValorizacionMonto.Value - rseValorizacionDescuento.Value - ((Po_ScarContrato / 100) * rseValorizacionMonto.Value), "#,##0.00")
        txtValorizacionNetoaPagar.Text = Format(Ss_MontoPagar, "#,##0.00")

        txtValorizacionScarActual.Text = Format(Convert.ToDecimal(txtValorizacionScar.Text), "#,##0.00")
        txtValorizacionScarAcumulado.Text = Format(Convert.ToDecimal(txtValorizacionScar.Text) + Convert.ToDecimal(txtValorizacionScarAnterior.Text), "#,##0.00")

        txtValorizacionDescuentoActual.Text = Format(rseValorizacionDescuento.Value, "#,##0.00")
        txtValorizacionDescuentoAcumulado.Text = Format(rseValorizacionDescuento.Value + Convert.ToDecimal(txtValorizacionDescuentoAnterior.Text), "#,##0.00")
    End Sub
#End Region
    Private Sub txtContratoRuc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtContratoRuc.KeyPress
        If Asc(e.KeyChar) = 13 Then
            ListarProveedor()
        End If
    End Sub
    Private Sub frmGestionarValorizacion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListarObra(cboContratoObra)
        ListarObra(cboValorizacionObra)
        ListarFacturasPendientesAsignacion()
        ListarContratos()
        btnContratoGuardar.Enabled = False
        btnContratoCancelar.Enabled = False
    End Sub
    Private Sub rgvContratos_DoubleClick(sender As Object, e As EventArgs) Handles rgvContratos.DoubleClick
        CargarVariables()
        PintarVariables()
        btnContratoGuardar.Enabled = True
        btnContratoCancelar.Enabled = True
        btnContratoNuevo.Enabled = False
        Fl_EsNuevo = False
        rgbContratoSubcontratos.Enabled = True
    End Sub
    Private Sub btnContratoNuevo_Click(sender As Object, e As EventArgs) Handles btnContratoNuevo.Click
        LimpiarControles()
        Fl_EsNuevo = True
        btnContratoGuardar.Enabled = True
        btnContratoCancelar.Enabled = True
        btnContratoNuevo.Enabled = False
        rgbContratoSubcontratos.Enabled = True
    End Sub
    Private Sub btnContratoGuardar_Click(sender As Object, e As EventArgs) Handles btnContratoGuardar.Click
        If Fl_EsNuevo = True Then
            AgregarContrato()
        Else
            ModificarContrato()
        End If
        LimpiarControles()
        btnContratoGuardar.Enabled = False
        btnContratoCancelar.Enabled = False
        btnContratoNuevo.Enabled = True
        rgbContratoSubcontratos.Enabled = False
    End Sub
    Private Sub btnContratoCancelar_Click(sender As Object, e As EventArgs) Handles btnContratoCancelar.Click
        LimpiarControles()
        btnContratoGuardar.Enabled = False
        btnContratoCancelar.Enabled = False
        btnContratoNuevo.Enabled = True
        rgbContratoSubcontratos.Enabled = False
    End Sub
    Private Sub rgvValorizacionFacturas_DoubleClick(sender As Object, e As EventArgs) Handles rgvValorizacionFacturas.DoubleClick
        rgbValorizacion.Enabled = True
        CargarValoresValorizacion()
    End Sub
    Private Sub rgvValorizacionFacturas_RowFormatting(sender As Object, e As Telerik.WinControls.UI.RowFormattingEventArgs) Handles rgvValorizacionFacturas.RowFormatting
        If e.RowElement.RowInfo.Cells("Seleccion").Value = True Then
            e.RowElement.DrawFill = True
            e.RowElement.GradientStyle = GradientStyles.Solid
            e.RowElement.BackColor = Color.Red
        Else
            e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local)

        End If
    End Sub
    Private Sub btnValorizacionAgregarContrato_Click(sender As Object, e As EventArgs) Handles btnValorizacionAgregarContrato.Click
        btnContratoNuevo_Click(Nothing, Nothing)
        lblContratoCodigoProveedor.Text = txtValorizacionRuc.Text
        txtContratoRuc.Text = txtValorizacionRuc.Text
        txtContratoSubcontratista.Text = txtValorizacionRazonSocial.Text
        cboContratoObra.SelectedValue = cboValorizacionObra.SelectedValue
        Me.RadPageView1.SelectedPage = Me.RadPageViewPage1
    End Sub
    Private Sub cboValorizacionObra_SelectedIndexChanged(sender As Object, e As UI.Data.PositionChangedEventArgs) Handles cboValorizacionObra.SelectedIndexChanged
        'If cboValorizacionObra.Text <> "System.Data.DataRowView" Then
        '    If cboValorizacionObra.SelectedValue <> Nothing Then
        '        ListarContratosPorObra()
        '    End If
        'End If
        If cboValorizacionObra.SelectedValue IsNot Nothing Then
            If cboValorizacionObra.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                ListarContratosPorObra()
            End If
        End If
    End Sub
    Private Sub cboValorizacionNroContrato_SelectedIndexChanged(sender As Object, e As UI.Data.PositionChangedEventArgs) Handles cboValorizacionNroContrato.SelectedIndexChanged
        'If cboValorizacionNroContrato.Text <> "System.Data.DataRowView" Then
        '    If cboValorizacionNroContrato.SelectedValue <> Nothing And cboValorizacionNroContrato.Text <> "" Then
        '        MostrarDatosContrato()
        '        CargarValorizacionesContrato()
        '        rseValorizacionMonto.Value = 0
        '    End If
        'End If
        If cboValorizacionNroContrato.SelectedValue IsNot Nothing Then
            If cboValorizacionNroContrato.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                MostrarDatosContrato()
                CargarValorizacionesContrato()
                rseValorizacionMonto.Value = 0
            End If
        End If
    End Sub
    Private Sub rseValorizacionMonto_ValueChanged(sender As Object, e As EventArgs) Handles rseValorizacionMonto.ValueChanged
        CalcularNetoAPAgar()
    End Sub
    Private Sub rseValorizacionDescuento_ValueChanged(sender As Object, e As EventArgs) Handles rseValorizacionDescuento.ValueChanged
        CalcularNetoAPAgar()
    End Sub
    Private Sub btnValorizacionGuardar_Click(sender As Object, e As EventArgs) Handles btnValorizacionGuardar.Click
        ValidarDatosContrato()
        AgregarValorizacionContrato()

    End Sub
End Class