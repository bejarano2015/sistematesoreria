<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuotasVencidas_xVencer
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCuotasVencidas_xVencer))
        Me.cboCia = New ctrLibreria.Controles.BeComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.btnSalir = New System.Windows.Forms.Button
        Me.chkTodos = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDias = New ctrLibreria.Controles.BeTextBox
        Me.chkPorVencer = New System.Windows.Forms.CheckBox
        Me.chkVencidas = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'cboCia
        '
        Me.cboCia.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCia.BackColor = System.Drawing.Color.Ivory
        Me.cboCia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCia.ForeColor = System.Drawing.Color.Black
        Me.cboCia.FormattingEnabled = True
        Me.cboCia.KeyEnter = True
        Me.cboCia.Location = New System.Drawing.Point(88, 12)
        Me.cboCia.Name = "cboCia"
        Me.cboCia.Size = New System.Drawing.Size(139, 21)
        Me.cboCia.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Aseguradora"
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnImprimir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(348, 15)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(73, 25)
        Me.btnImprimir.TabIndex = 5
        Me.btnImprimir.Text = "  &Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalir.Location = New System.Drawing.Point(348, 45)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(73, 25)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.Text = "  &Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'chkTodos
        '
        Me.chkTodos.AutoSize = True
        Me.chkTodos.Location = New System.Drawing.Point(233, 16)
        Me.chkTodos.Name = "chkTodos"
        Me.chkTodos.Size = New System.Drawing.Size(56, 17)
        Me.chkTodos.TabIndex = 1
        Me.chkTodos.Text = "Todos"
        Me.chkTodos.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(192, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Dias"
        '
        'txtDias
        '
        Me.txtDias.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDias.BackColor = System.Drawing.Color.Ivory
        Me.txtDias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDias.ForeColor = System.Drawing.Color.Black
        Me.txtDias.KeyEnter = True
        Me.txtDias.Location = New System.Drawing.Point(222, 48)
        Me.txtDias.Name = "txtDias"
        Me.txtDias.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDias.ShortcutsEnabled = False
        Me.txtDias.Size = New System.Drawing.Size(61, 20)
        Me.txtDias.TabIndex = 4
        Me.txtDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDias.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Númerico
        '
        'chkPorVencer
        '
        Me.chkPorVencer.AutoSize = True
        Me.chkPorVencer.Location = New System.Drawing.Point(95, 50)
        Me.chkPorVencer.Name = "chkPorVencer"
        Me.chkPorVencer.Size = New System.Drawing.Size(79, 17)
        Me.chkPorVencer.TabIndex = 3
        Me.chkPorVencer.Text = "Por Vencer"
        Me.chkPorVencer.UseVisualStyleBackColor = True
        '
        'chkVencidas
        '
        Me.chkVencidas.AutoSize = True
        Me.chkVencidas.Location = New System.Drawing.Point(19, 50)
        Me.chkVencidas.Name = "chkVencidas"
        Me.chkVencidas.Size = New System.Drawing.Size(70, 17)
        Me.chkVencidas.TabIndex = 2
        Me.chkVencidas.Text = "Vencidas"
        Me.chkVencidas.UseVisualStyleBackColor = True
        '
        'frmCuotasVencidas_xVencer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(437, 90)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDias)
        Me.Controls.Add(Me.chkPorVencer)
        Me.Controls.Add(Me.chkVencidas)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.chkTodos)
        Me.Controls.Add(Me.cboCia)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmCuotasVencidas_xVencer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Cuotas Vencidas y Por Vencer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboCia As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents chkTodos As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDias As ctrLibreria.Controles.BeTextBox
    Friend WithEvents chkPorVencer As System.Windows.Forms.CheckBox
    Friend WithEvents chkVencidas As System.Windows.Forms.CheckBox
End Class
