<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmREP_PolizasXPagar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmREP_PolizasXPagar))
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.mtbFechaIni = New ctrLibreria.Controles.BeMaskedTextBox
        Me.mtbFechaFin = New ctrLibreria.Controles.BeMaskedTextBox
        Me.btnSalir = New System.Windows.Forms.Button
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(12, 21)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Fecha Inicio"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(12, 49)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(73, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Fecha Final"
        '
        'mtbFechaIni
        '
        Me.mtbFechaIni.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaIni.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaIni.BeepOnError = True
        Me.mtbFechaIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbFechaIni.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaIni.HidePromptOnLeave = True
        Me.mtbFechaIni.KeyEnter = True
        Me.mtbFechaIni.Location = New System.Drawing.Point(95, 17)
        Me.mtbFechaIni.Mask = "00/00/0000"
        Me.mtbFechaIni.Name = "mtbFechaIni"
        Me.mtbFechaIni.Size = New System.Drawing.Size(67, 20)
        Me.mtbFechaIni.TabIndex = 0
        Me.mtbFechaIni.ValidatingType = GetType(Date)
        '
        'mtbFechaFin
        '
        Me.mtbFechaFin.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaFin.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaFin.BeepOnError = True
        Me.mtbFechaFin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbFechaFin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaFin.HidePromptOnLeave = True
        Me.mtbFechaFin.KeyEnter = True
        Me.mtbFechaFin.Location = New System.Drawing.Point(95, 44)
        Me.mtbFechaFin.Mask = "00/00/0000"
        Me.mtbFechaFin.Name = "mtbFechaFin"
        Me.mtbFechaFin.Size = New System.Drawing.Size(67, 20)
        Me.mtbFechaFin.TabIndex = 1
        Me.mtbFechaFin.ValidatingType = GetType(Date)
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalir.Location = New System.Drawing.Point(255, 42)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(73, 25)
        Me.btnSalir.TabIndex = 4
        Me.btnSalir.Text = "  &Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnImprimir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(255, 12)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(73, 25)
        Me.btnImprimir.TabIndex = 3
        Me.btnImprimir.Text = "  &Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'FrmREP_PolizasXPagar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(340, 86)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.mtbFechaFin)
        Me.Controls.Add(Me.mtbFechaIni)
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.BeLabel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "FrmREP_PolizasXPagar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte de Polizas Por Pagar"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents mtbFechaIni As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents mtbFechaFin As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class
