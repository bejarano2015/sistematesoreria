Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmCabeceraLibros

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCabeceraLibros = Nothing
    Public Shared Function Instance() As frmCabeceraLibros
        If frmInstance Is Nothing Then
            frmInstance = New frmCabeceraLibros
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmCabeceraLibros_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub
    Private Sub frmCabeceraLibros_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eChequera As clsChequera
    Private eLibroBancos As clsLibroBancos
    Private eTempo As clsPlantTempo

    Dim iOpcion As Int16 = 0
    Dim sVisualizarDatos As String = ""

    Private Sub frmCabeceraLibros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eTempo = New clsPlantTempo
        'cboOpcion.DataSource = eTempo.fColOpcion
        'cboOpcion.ValueMember = "Col02"
        'cboOpcion.DisplayMember = "Col01"
        'cboOpcion.SelectedIndex = -1

        eChequera = New clsChequera
        cboBancos.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        If eChequera.iNroRegistros > 0 Then
            cboBancos.ValueMember = "IdBanco"
            cboBancos.DisplayMember = "NombreBanco"
            cboBancos.SelectedIndex = -1
        End If

        btnGrabar.Enabled = True
        btnCancelar.Enabled = False
        btnGrabar.Enabled = False
        btnNuevo.Enabled = False
        btnEliminar.Enabled = False

        Me.ToolTip1.SetToolTip(Me.btnEliminar, "Eliminar")
        Me.ToolTip1.SetToolTip(Me.btnNuevo, "Nuevo")
        Me.ToolTip1.SetToolTip(Me.btnGrabar, "Grabar")
        Me.ToolTip1.SetToolTip(Me.btnCancelar, "Cancelar")

        'cboAno.Text = Year(Now)

    End Sub

    Private Sub cboBancos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBancos.SelectedIndexChanged
        Dim IdBanco As String = ""
        If (cboBancos.SelectedIndex > -1) Then
            Try
                IdBanco = cboBancos.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuentas.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    cboCuentas.ValueMember = "IdCuenta"
                    cboCuentas.DisplayMember = "NumeroCuenta"
                    cboCuentas.SelectedIndex = -1
                    lblMoneda.Text = ""
                    lblTipoCta.Text = ""
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try

            If dgvLibroDet.Rows.Count > 0 Then
                For x As Integer = 0 To dgvLibroDet.RowCount - 1
                    dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
                Next
            End If

            btnNuevo_Click(sender, e)

        End If
    End Sub

    Private Sub cboCuentas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCuentas.SelectionChangeCommitted
        Dim dtTable2 As DataTable
        Dim IdCuenta As String = ""
        If (cboCuentas.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuentas.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtTable2 = New DataTable
                    dtTable2 = eChequera.fListarMoneda(IdCuenta, gEmpresa)
                    lblMoneda.Text = dtTable2.Rows(0).Item("MonDescripcion")
                    lblTipoCta.Text = dtTable2.Rows(0).Item("Descripcion")
                End If

                eLibroBancos = New clsLibroBancos

                Dim dtDatos As DataTable
                dtDatos = New DataTable
                dtDatos = eLibroBancos.TraerLibros(Trim(cboCuentas.SelectedValue), gEmpresa)
                dgvLibroDet.AutoGenerateColumns = False
                dgvLibroDet.DataSource = dtDatos
                btnNuevo.Enabled = True
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try

            cboAno.SelectedIndex = -1
            cboMes.SelectedIndex = -1
            txtCod.Clear()
            txtSaldoInicial.Clear()
            txtSaldo.Clear()
            txtCobrados.Clear()
            txtNOCobrados.Clear()
            cboCuentas.Focus()
            btnNuevo_Click(sender, e)
        End If
    End Sub


    Private Sub dgvLibroDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLibroDet.Click
        If dgvLibroDet.Rows.Count > 0 Then
            iOpcion = 33
            Try
                'dtpFecha.Value = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column1").Value)

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(0).Value) = True Then
                    txtCod.Text = ""
                Else
                    txtCod.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(0).Value)
                End If

                'If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(2).Value) = True Then
                '    cboCuentas.SelectedIndex = -1
                'Else
                '    cboCuentas.SelectedValue = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(2).Value)
                'End If

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(3).Value) = True Then
                    cboMes.SelectedIndex = -1
                Else
                    cboMes.Text = TraerNombreMes(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(3).Value)
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(4).Value) = True Then
                    cboAno.SelectedIndex = -1
                Else
                    cboAno.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(4).Value)
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(5).Value) = True Then
                    txtSaldoInicial.Text = ""
                Else
                    'txtSaldo.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(5).Value)
                    txtSaldoInicial.Text = Format(Convert.ToDouble(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(5).Value), "#,##0.00")
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(6).Value) = True Then
                    txtSaldo.Text = ""
                Else
                    'txtSaldo.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(5).Value)
                    txtSaldo.Text = Format(Convert.ToDouble(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(6).Value), "#,##0.00")
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(7).Value) = True Then
                    txtCobrados.Text = ""
                Else
                    'txtCobrados.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(6).Value)
                    txtCobrados.Text = Format(Convert.ToDouble(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(7).Value), "#,##0.00")
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value) = True Then
                    txtNOCobrados.Text = ""
                Else
                    'txtNOCobrados.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(7).Value)
                    txtNOCobrados.Text = Format(Convert.ToDouble(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value), "#,##0.00")
                End If

                btnGrabar.Enabled = True
                btnNuevo.Enabled = False
                btnCancelar.Enabled = True
                btnEliminar.Enabled = False
                'Calcular()
                'LimpiarControlesDetalle()
                'HabilitarControlesDetalle()
            Catch ex As Exception
                Exit Sub
            End Try

        End If 'If dgvLibroDet.Rows.Count > 0 Then
        txtRestar.Clear()
        txtRestar.Focus()


    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        'LimpiarControlesDetalle()
        'DeshabilitarControlesDetalle()
        iOpcion = 0
        btnNuevo.Enabled = True
        btnGrabar.Enabled = False
        btnCancelar.Enabled = False
        btnEliminar.Enabled = False
        cboAno.SelectedIndex = -1
        cboMes.SelectedIndex = -1
        txtCod.Clear()
        txtSaldoInicial.Clear()
        txtSaldo.Clear()
        txtCobrados.Clear()
        txtNOCobrados.Clear()
        txtRestar.Clear()
        btnNuevo_Click(sender, e)
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        btnGrabar.Enabled = True
        btnCancelar.Enabled = True
        btnNuevo.Enabled = False
        iOpcion = 32
        cboAno.SelectedIndex = -1
        cboMes.SelectedIndex = -1
        txtCod.Clear()
        txtSaldoInicial.Clear()
        txtSaldo.Clear()
        txtCobrados.Clear()
        txtNOCobrados.Clear()
        cboAno.Focus()
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim Fecha As DateTime

        If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboAno.SelectedIndex > -1 And cboMes.SelectedIndex > -1 And Len(txtSaldoInicial.Text.Trim) > 0 And Len(txtSaldo.Text.Trim) > 0 And Len(txtCobrados.Text.Trim) > 0 And Len(txtNOCobrados.Text.Trim) > 0 Then
            Fecha = "01/" & TraerNumeroMes(Trim(cboMes.Text)) & "/" & Trim(cboAno.Text)
            If iOpcion = 32 Then
                If (MessageBox.Show("�Esta seguro de Grabar?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then

                    Dim sCodigoz As String = ""
                    Dim dtDatosx As DataTable
                    dtDatosx = New DataTable
                    dtDatosx = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
                    If dtDatosx.Rows.Count = 1 Then 'si
                        MessageBox.Show("Ya Existe un Libro Bancos con este Mes y A�o!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Exit Sub
                    End If

                    Dim sCodigo As String = ""
                    Dim iResultado As Integer = 0
                    eLibroBancos = New clsLibroBancos
                    eLibroBancos.fCodigo(gEmpresa)
                    sCodigo = eLibroBancos.sCodFuturo 'genera codigo de libro
                    'graba la cabecera de libro
                    iResultado = eLibroBancos.fGrabar2(iOpcion, sCodigo, gEmpresa, Trim(cboCuentas.SelectedValue), TraerNumeroMes(cboMes.Text), Trim(cboAno.Text), Convert.ToDouble(txtSaldoInicial.Text), Convert.ToDouble(txtSaldo.Text), Convert.ToDouble(txtCobrados.Text), Convert.ToDouble(txtNOCobrados.Text), Fecha) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                    If iResultado > 0 Then
                        btnNuevo.Enabled = True
                        btnCancelar.Enabled = False
                        btnGrabar.Enabled = False
                        'cboMes_SelectedIndexChanged(sender, e)
                        'MessageBox.Show("Se Grabo con Exito!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        cboCuentas_SelectionChangeCommitted(sender, e)
                        cboAno.SelectedIndex = -1
                        cboMes.SelectedIndex = -1
                        txtCod.Clear()
                        txtSaldoInicial.Clear()
                        txtSaldo.Clear()
                        txtCobrados.Clear()
                        txtNOCobrados.Clear()
                        cboAno.Focus()
                        txtRestar.Clear()
                        btnNuevo_Click(sender, e)
                    End If
                End If
            ElseIf iOpcion = 33 Then
                If (MessageBox.Show("�Esta seguro de Actualizar?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    Dim sCodigo As String = ""
                    Dim iResultado As Integer = 0
                    eLibroBancos = New clsLibroBancos
                    'eLibroBancos.fCodigo(gEmpresa)
                    'sCodigo = eLibroBancos.sCodFuturo 'genera codigo de libro
                    sCodigo = Trim(txtCod.Text)
                    'graba la cabecera de libro
                    iResultado = eLibroBancos.fGrabar2(iOpcion, sCodigo, gEmpresa, Trim(cboCuentas.SelectedValue), TraerNumeroMes(cboMes.Text), Trim(cboAno.Text), Convert.ToDouble(txtSaldoInicial.Text), Convert.ToDouble(txtSaldo.Text), Convert.ToDouble(txtCobrados.Text), Convert.ToDouble(txtNOCobrados.Text), Fecha) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                    If iResultado > 0 Then
                        btnNuevo.Enabled = True
                        btnCancelar.Enabled = False
                        btnGrabar.Enabled = False
                        'cboMes_SelectedIndexChanged(sender, e)
                        'MessageBox.Show("Se Actualizo con Exito!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        cboCuentas_SelectionChangeCommitted(sender, e)
                        cboAno.SelectedIndex = -1
                        cboMes.SelectedIndex = -1
                        txtCod.Clear()
                        txtSaldoInicial.Clear()
                        txtSaldo.Clear()
                        txtCobrados.Clear()
                        txtNOCobrados.Clear()
                        cboAno.Focus()
                        txtRestar.Clear()
                        btnNuevo_Click(sender, e)
                    End If
                End If
            End If
        Else
            If cboBancos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboBancos.Focus()
                Exit Sub
            End If
            If cboCuentas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCuentas.Focus()
                Exit Sub
            End If
            If cboAno.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboAno.Focus()
                Exit Sub
            End If
            If cboMes.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Mes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboMes.Focus()
                Exit Sub
            End If
            If Len(txtSaldoInicial.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese El Saldo Inicial del Mes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtSaldoInicial.Focus()
                Exit Sub
            End If
            If Len(txtSaldo.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese El Saldo Final del Mes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtSaldo.Focus()
                Exit Sub
            End If
            If Len(txtCobrados.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese El Monto de los Cheques Cobrados", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtCobrados.Focus()
                Exit Sub
            End If
            If Len(txtNOCobrados.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese El Monto de los Cheques NO Cobrados", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtNOCobrados.Focus()
                Exit Sub
            End If
        End If 'sale de preguntar si falta datos para el registro

    End Sub

    Private Sub txtSaldo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSaldo.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtSaldo.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtSaldo.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub txtCobrados_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCobrados.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtCobrados.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtCobrados.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub txtNOCobrados_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNOCobrados.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                btnGrabar_Click(sender, e)
        End Select
    End Sub

    Private Sub txtNOCobrados_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNOCobrados.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtNOCobrados.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtNOCobrados.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub txtSaldoInicial_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSaldoInicial.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtSaldoInicial.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtSaldoInicial.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub cboCuentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuentas.SelectedIndexChanged

    End Sub

    Private Sub dgvLibroDet_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLibroDet.CellContentClick

    End Sub

    Private Sub txtRestar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRestar.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtNOCobrados.Text)) = 0 Then
                    txtNOCobrados.Text = "0.00"
                End If
                If Len(Trim(txtRestar.Text)) = 0 Then
                    txtRestar.Text = "0.00"
                End If
                txtNOCobrados.Text = txtNOCobrados.Text - txtRestar.Text
                txtNOCobrados.Focus()
        End Select
    End Sub

    Private Sub txtRestar_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRestar.LostFocus
        'Dim MontoEscrito As String = ""
        'MontoEscrito = txtRestar.Text.Trim
        'If MontoEscrito.Trim = "" Then
        '    MontoEscrito = 0
        'End If
        'txtRestar.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")

        'If Len(txtNOCobrados.Text.Trim) = 0 Then
        '    txtNOCobrados.Text = "0.00"
        'End If

        'txtNOCobrados.Text = txtNOCobrados.Text - txtRestar.Text
        'txtNOCobrados.Focus()
        'End Select

    End Sub


    Private Sub txtRestar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRestar.TextChanged

    End Sub

    Private Sub txtNOCobrados_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNOCobrados.TextChanged

    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click

    End Sub

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


    '    'Dim aaa As Double
    '    'Dim x As Integer
    '    'Dim c As Integer
    '    'aaa = txtMonto.Text

    '    'If aaa < 1000 Then
    '    '    BeTextBox2.Text = 0
    '    '    Exit Sub
    '    'End If

    '    'For x = 1 To 10
    '    '    aaa = aaa \ 10
    '    '    If aaa < 10 Then Exit For
    '    'Next

    '    'BeTextBox2.Text = aaa

    '    'For c = 1 To x

    '    '    BeTextBox2.Text = BeTextBox2.Text * 10

    '    'Next

    '    ReturnMontoParaCalcItf(txtMonto.Text)


    'End Sub

    'Private Function ReturnMontoParaCalcItf(ByVal Monto As Double) As Double

    '    'Dim aaa As Double
    '    Dim x As Integer
    '    Dim c As Integer
    '    'aaa = txtMonto.Text

    '    Dim MontoParaCalcItf As Double = 0

    '    If Monto < 1000 Then
    '        Return 0
    '        Exit Function
    '    End If

    '    For x = 1 To 10
    '        Monto = Monto \ 10
    '        If Monto < 10 Then Exit For
    '    Next

    '    'BeTextBox2.Text = aaa

    '    For c = 1 To x

    '        Monto = Monto * 10

    '    Next
    '    MontoParaCalcItf = Monto
    '    Return MontoParaCalcItf

    'End Function

End Class