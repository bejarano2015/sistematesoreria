Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmEstadoCuentaProvDoc

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmEstadoCuentaProvDoc = Nothing
    Public Shared Function Instance() As frmEstadoCuentaProvDoc
        If frmInstance Is Nothing Then
            frmInstance = New frmEstadoCuentaProvDoc
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmEstadoCuentaProvDoc_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub
    Private Sub frmEstadoCuentaProvDoc_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eGastosGenerales As clsGastosGenerales

    'Private Sub txtFiltrar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    eGastosGenerales = New clsGastosGenerales
    '    If Len(Trim(txtFiltrar.Text)) > 0 And rdbDescr.Checked = True Then
    '        Dim dtTable As DataTable
    '        dtTable = New DataTable
    '        dtTable = eGastosGenerales.fListarCronogramas7(gEmpresa, Trim(txtFiltrar.Text))
    '        dgvProveedores.DataSource = dtTable
    '    ElseIf Len(Trim(txtFiltrar.Text)) > 0 And rdbRuc.Checked = True Then
    '        Dim dtTable As DataTable
    '        dtTable = New DataTable
    '        dtTable = eGastosGenerales.fListarCronogramas6(gEmpresa, Trim(txtFiltrar.Text))
    '        dgvProveedores.DataSource = dtTable
    '    End If
    '    If Len(Trim(txtFiltrar.Text)) = 0 Then
    '        Dim dtTable As DataTable
    '        dtTable = New DataTable
    '        dtTable = eGastosGenerales.fListarCronogramas8(gEmpresa)
    '        dgvProveedores.DataSource = dtTable
    '    End If
    'End Sub

    'Private Sub rdbDescr_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If dgvProveedores.Rows.Count > 0 Then
    '        'm_EditingRow = -1
    '        For x As Integer = 0 To dgvProveedores.RowCount - 1
    '            dgvProveedores.Rows.Remove(dgvProveedores.CurrentRow)
    '        Next
    '    End If
    '    rdbRuc.Checked = False
    '    rdbDescr.Checked = True
    '    txtFiltrar.Clear()
    '    txtFiltrar.MaxLength = 150
    '    txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
    '    txtFiltrar.Focus()
    'End Sub

    'Private Sub rdbRuc_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If dgvProveedores.Rows.Count > 0 Then
    '        'm_EditingRow = -1
    '        For x As Integer = 0 To dgvProveedores.RowCount - 1
    '            dgvProveedores.Rows.Remove(dgvProveedores.CurrentRow)
    '        Next
    '    End If
    '    rdbRuc.Checked = True
    '    rdbDescr.Checked = False
    '    txtFiltrar.Clear()
    '    txtFiltrar.MaxLength = 11
    '   txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
    '    txtFiltrar.Focus()
    'End Sub

    'Private Sub dgvProveedores_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim CodAnexo As String = ""
    '    Dim TipoAnexo As String = ""
    '    Dim TipoMoneda As String = ""
    '    Dim CodMoneda As String = ""
    '    Try
    '        CodAnexo = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column1").Value)
    '        TipoMoneda = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column5").Value)
    '        TipoAnexo = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column6").Value)

    '        If CodAnexo <> "" And TipoAnexo <> "" And TipoMoneda <> "" Then
    '            If TipoMoneda = "SOLES" Then
    '                CodMoneda = "01"
    '            ElseIf TipoMoneda = "DOLARES" Then
    '                CodMoneda = "02"
    '            End If

    '            eGastosGenerales = New clsGastosGenerales
    '            'If Len(Trim(txtFiltrar.Text)) > 0 And rdbDescr.Checked = True Then
    '            Dim dtTable As DataTable
    '            dtTable = New DataTable
    '            dtTable = eGastosGenerales.fListarSaldoDocs(gEmpresa, CodAnexo, TipoAnexo, CodMoneda)
    '            dgvDocumentos.DataSource = dtTable
    '            TabPersonalizado1.SelectedIndex = 1
    '            'TabControl1.TabPages(1).Show()
    '            'TabControl1.TabPages(1).e
    '            'TabControl1.TabPages(1).Controls.Owner.Focus = True
    '        End If

    '    Catch ex As Exception
    '        Exit Sub
    '    End Try
    'End Sub

    Public Sub Salir()

        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If

    End Sub

    Private Sub frmEstadoCuentaProvDoc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rdbRuc.Checked = True
        txtFiltrar.Text = ""
        rdbRuc.Focus()
        txtFiltrar.Focus()

        txtFiltrar_TextChanged_1(sender, e)

        eGastosGenerales = New clsGastosGenerales
        'eLogin = New clsLogin
        cboTodosProv.DataSource = eGastosGenerales.fListarProveedores
        If eGastosGenerales.iNroRegistros > 0 Then
            cboTodosProv.ValueMember = "RUC"
            cboTodosProv.DisplayMember = "AnaliticoDescripcion"
            cboTodosProv.SelectedIndex = -1
            'txtUsuario.Focus()
        End If

        eGastosGenerales = New clsGastosGenerales
        'eLogin = New clsLogin
        cboTodosSubC.DataSource = eGastosGenerales.fListarSubContratistas
        If eGastosGenerales.iNroRegistros > 0 Then
            cboTodosSubC.ValueMember = "RUC"
            cboTodosSubC.DisplayMember = "AnaliticoDescripcion"
            cboTodosSubC.SelectedIndex = -1
            'txtUsuario.Focus()
        End If


        eGastosGenerales = New clsGastosGenerales
        cboTodosProvxEmpr.DataSource = eGastosGenerales.fListarProveedoresxEmpresa(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboTodosProvxEmpr.ValueMember = "RUC"
            cboTodosProvxEmpr.DisplayMember = "AnaliticoDescripcion"
            cboTodosProvxEmpr.SelectedIndex = -1
            'txtUsuario.Focus()
        End If

        eGastosGenerales = New clsGastosGenerales
        cboTodosSubCxEmpr.DataSource = eGastosGenerales.fListarSubContratistasxEmpresa(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboTodosSubCxEmpr.ValueMember = "RUC"
            cboTodosSubCxEmpr.DisplayMember = "AnaliticoDescripcion"
            cboTodosSubCxEmpr.SelectedIndex = -1
            'txtUsuario.Focus()
        End If

        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Value = "01/" & Mes & "/" & Now.Year()
        dtpFechaInicio.Value = "01/" & Mes & "/" & Now.Year()
        dtpF1.Value = "01/" & Mes & "/" & Now.Year()

        dtpFechaFin.Value = Now.Date()
        dtpFechaFinal.Value = Now.Date()
        dtpF2.Value = Now.Date()

        GroupBox6.Text = "Reporte General de Facturas por Pagar de " & gDesEmpresa
    End Sub

    Private Sub txtFiltrar_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFiltrar.TextChanged
        eGastosGenerales = New clsGastosGenerales

        If rdbRuc.Checked = True Then
            txtFiltrar.MaxLength = 11
            txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        ElseIf rdbDescr.Checked = True Then
            txtFiltrar.MaxLength = 150
            txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        End If

        If Len(Trim(txtFiltrar.Text)) > 0 And rdbDescr.Checked = True Then
            Dim dtTable As DataTable
            dtTable = New DataTable
            dtTable = eGastosGenerales.fListarCronogramas7(gEmpresa, Trim(txtFiltrar.Text))
            dgvProveedores.DataSource = dtTable
        ElseIf Len(Trim(txtFiltrar.Text)) > 0 And rdbRuc.Checked = True Then
            Dim dtTable As DataTable
            dtTable = New DataTable
            dtTable = eGastosGenerales.fListarCronogramas6(gEmpresa, Trim(txtFiltrar.Text))
            dgvProveedores.DataSource = dtTable
        End If
        If Len(Trim(txtFiltrar.Text)) = 0 Then
            Dim dtTable As DataTable
            dtTable = New DataTable
            dtTable = eGastosGenerales.fListarCronogramas8(gEmpresa)
            dgvProveedores.DataSource = dtTable
        End If
    End Sub

    Private Sub dgvProveedores_DoubleClick1(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvProveedores.DoubleClick
        Dim dblSolesDocs As Double = 0
        If dgvProveedores.Rows.Count > 0 Then
            Dim CodAnexo As String = ""
            Dim TipoAnexo As String = ""
            Dim TipoMoneda As String = ""
            Dim CodMoneda As String = ""
            Dim DesProv As String = ""
            Try
                CodAnexo = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column1").Value)
                DesProv = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column3").Value)
                TipoMoneda = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column5").Value)
                TipoAnexo = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column6").Value)
                CodMoneda = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column7").Value)

                If CodAnexo <> "" And TipoAnexo <> "" And TipoMoneda <> "" Then
                    eGastosGenerales = New clsGastosGenerales
                    Dim dtTable As DataTable
                    dtTable = New DataTable
                    dtTable = eGastosGenerales.fListarSaldoDocs(gEmpresa, CodAnexo, TipoAnexo, CodMoneda)
                    dgvDocumentos.DataSource = dtTable

                    If dgvDocumentos.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            Dim StrCad As String = String.Empty
                            StrCad = Trim(dgvDocumentos.Rows(x).Cells("Column10").Value)
                            If Trim(dgvDocumentos.Rows(x).Cells("IdDocumento").Value) <> "05" Then
                                If StrCad = Nothing Then
                                    dblSolesDocs = dblSolesDocs + 0
                                Else
                                    dblSolesDocs = dblSolesDocs + dgvDocumentos.Rows(x).Cells("Column10").Value
                                End If
                            ElseIf Trim(dgvDocumentos.Rows(x).Cells("IdDocumento").Value) = "05" Then
                                If StrCad = Nothing Then
                                    dblSolesDocs = dblSolesDocs - 0
                                Else
                                    dblSolesDocs = dblSolesDocs - dgvDocumentos.Rows(x).Cells("Column10").Value
                                End If
                            End If
                            txtSol.Text = Format(Math.Round(dblSolesDocs, 2), "#,##0.00")
                        Next
                        BeLabel3.Text = "Total de Registros : " & dtTable.Rows.Count
                    Else
                        txtSol.Text = Format(Math.Round(dblSolesDocs, 2), "#,##0.00")
                        BeLabel3.Text = "Total de Registros : 0"
                    End If

                    TabPersonalizado1.SelectedIndex = 1
                    lblEtiqueta.Text = CodAnexo & " - " & DesProv
                    lblMoneda.Text = TipoMoneda
                End If
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub rdbRuc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRuc.Click
        If dgvProveedores.Rows.Count > 0 Then
            'm_EditingRow = -1
            For x As Integer = 0 To dgvProveedores.RowCount - 1
                dgvProveedores.Rows.Remove(dgvProveedores.CurrentRow)
            Next
        End If
        rdbRuc.Checked = True
        rdbDescr.Checked = False

        txtFiltrar.MaxLength = 11
        txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        txtFiltrar.Text = ""
        txtFiltrar.Focus()
        txtFiltrar_TextChanged_1(sender, e)
    End Sub

    Private Sub rdbDescr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbDescr.Click
        If dgvProveedores.Rows.Count > 0 Then
            'm_EditingRow = -1
            For x As Integer = 0 To dgvProveedores.RowCount - 1
                dgvProveedores.Rows.Remove(dgvProveedores.CurrentRow)
            Next
        End If
        rdbRuc.Checked = False
        rdbDescr.Checked = True
        'txtFiltrar.Clear()
        txtFiltrar.MaxLength = 150
        txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        txtFiltrar.Text = ""
        txtFiltrar.Focus()
        txtFiltrar_TextChanged_1(sender, e)
        'txtFiltrar.Focus()
    End Sub

    Private Sub btnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVer.Click
        Dim x As frmReporteFacxProv = frmReporteFacxProv.Instance
        x.Owner = Me
        x.ShowInTaskbar = False

        If chkTodos.Checked = True Then
            x.TodosUno = "T"
            x.RucDeUno = ""
            x.Moneda = ""
        ElseIf chkTodos.Checked = False Then
            x.TodosUno = "U"
            If dgvProveedores.Rows.Count > 0 Then
                x.RucDeUno = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column1").Value)
                x.Moneda = Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column7").Value)
            Else
                x.RucDeUno = ""
            End If
        End If

        If RadioButton2.Checked = True Then
            x.ResDet = "D"
        End If

        If RadioButton1.Checked = True Then
            x.ResDet = "R"
        End If
        x.ShowDialog()
        'End If
    End Sub

    Private Sub rdbDescr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDescr.CheckedChanged

    End Sub

    Private Sub GroupBox5_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox5.Enter

    End Sub

    Private Sub TabPersonalizado1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPersonalizado1.SelectedIndexChanged
        If TabPersonalizado1.TabPages(1).Controls.Owner.Focus = True Then
            dgvProveedores_DoubleClick1(sender, e)
        End If
    End Sub

    Private Sub btnVerTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerTodos.Click
        Dim x As frmReporteFacxProv2 = frmReporteFacxProv2.Instance
        x.Owner = Me
        x.ShowInTaskbar = False

        If chkTodos2.Checked = True Then
            x.TodosUno = "T"
            x.RucDeUno = ""
            x.Moneda = ""
        ElseIf chkTodos2.Checked = False Then
            x.TodosUno = "U"
            If cboTodosProv.SelectedIndex > -1 Then
                x.RucDeUno = cboTodosProv.SelectedValue
                x.NombreProveedor = Trim(cboTodosProv.Text)
                x.Moneda = "" 'Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column7").Value)
            Else
                MsgBox("Seleccione un Proveedor!", MsgBoxStyle.Information, glbNameSistema)
                cboTodosProv.Focus()
                x.RucDeUno = ""
                x.NombreProveedor = ""
                Exit Sub
            End If
        End If

        If rdbDetallado.Checked = True Then
            x.ResDet = "D"
        End If

        If rdbResumido.Checked = True Then
            x.ResDet = "R"
        End If

        x.FechaIni = dtpFechaIni.Value
        x.FechaFin = dtpFechaFin.Value

        x.ShowDialog()
    End Sub

    Private Sub chkTodos2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodos2.CheckedChanged
        If chkTodos2.Checked = True Then
            cboTodosProv.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboTodosProv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTodosProv.SelectedIndexChanged
        If cboTodosProv.SelectedIndex > -1 Then
            chkTodos2.Checked = False
        End If
    End Sub

    Private Sub btnVerPrestamos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerPrestamos.Click
        Dim x As frmReporteFacxProv3 = frmReporteFacxProv3.Instance
        x.Owner = Me
        x.ShowInTaskbar = False

        If Opcion1.Checked = True Then
            x.Opcion = "1"
        ElseIf Opcion1.Checked = False Then
            x.Opcion = "2"
        End If

        x.F1 = dtpF1.Value
        x.F2 = dtpF2.Value
        x.ShowDialog()

    End Sub

    Private Sub chkTodosxEmpr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodosxEmpr.CheckedChanged
        If chkTodosxEmpr.Checked = True Then
            cboTodosProvxEmpr.SelectedIndex = -1
        End If
    End Sub

    Private Sub btnVerTodosEmpr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerTodosEmpr.Click

        If chkTodosxEmpr.Checked = False Then
            If cboTodosProvxEmpr.SelectedIndex = -1 Then
                MsgBox("Seleccione un Proveedor!", MsgBoxStyle.Information, glbNameSistema)
                cboTodosProvxEmpr.Focus()
                Exit Sub
            End If
        End If

        Try
            Dim dtTable2 As DataTable
            Dim pIdRetencion As String = ""
            Dim pSerie As String = ""
            dtTable2 = New DataTable
            eGastosGenerales = New clsGastosGenerales

            If chkTodosxEmpr.Checked = True Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosxEmpresaAlaFecha(52, gEmpresa, Trim(cboTodosProvxEmpr.SelectedValue), dtpFechaFinal.Value, dtpFechaInicio.Value)
            End If

            If chkTodosxEmpr.Checked = False And cboTodosProvxEmpr.SelectedIndex > -1 Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosxEmpresaAlaFecha(53, gEmpresa, Trim(cboTodosProvxEmpr.SelectedValue), dtpFechaFinal.Value, dtpFechaInicio.Value)
            End If

            If dtTable2.Rows.Count > 0 Then
                'If chk4.Checked = True Then
                Dim RD As String = ""
                If rdbResumidoEmpr.Checked = True Then
                    RD = "R"
                ElseIf rdbDetalladoEmpr.Checked = True Then
                    RD = "D"
                End If
                Muestra_Reporte(RutaAppReportes & "rptFacturasPorPagarGlobalTodos2.rpt", dtTable2, "TblLibroDiario", "", "ResDet;" & Trim(RD), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Fecha;" & dtpFechaFinal.Value, "FechaIni;" & dtpFechaInicio.Value)
                'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                'End If
            ElseIf dtTable2.Rows.Count = 0 Then
                Dim mensaje As String = ""
                'If rdbCancelar.Checked = True Then
                mensaje = "No se Encontr� ningun Comprobante a Cancelar."
                'End If
                'If rdbCancelados.Checked = True Then
                '    mensaje = "No se Encontro ningun Comprobante Cancelado."
                'End If
                'If rdbCC.Checked = True Then
                '    mensaje = "No se Encontro Cuenta Corriente de Proveedores en este Centro de Costo."
                'End If
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub cboTodosProvxEmpr_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTodosProvxEmpr.SelectedIndexChanged
        If cboTodosProvxEmpr.SelectedIndex > -1 Then
            chkTodosxEmpr.Checked = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged

    End Sub

    Private Sub btnVerTodosSubC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerTodosSubC.Click
        Dim x As frmReporteFacxSubC1 = frmReporteFacxSubC1.Instance
        x.Owner = Me
        x.ShowInTaskbar = False

        If chkTodosSubC.Checked = True Then
            x.TodosUno = "T"
            x.RucDeUno = ""
            x.Moneda = ""
        ElseIf chkTodosSubC.Checked = False Then
            x.TodosUno = "U"
            If cboTodosSubC.SelectedIndex > -1 Then
                x.RucDeUno = cboTodosSubC.SelectedValue
                x.NombreProveedor = Trim(cboTodosSubC.Text)
                x.Moneda = "" 'Trim(dgvProveedores.Rows(dgvProveedores.CurrentRow.Index).Cells("Column7").Value)
            Else
                MsgBox("Seleccione un SubContratista!", MsgBoxStyle.Information, glbNameSistema)
                cboTodosSubC.Focus()
                x.RucDeUno = ""
                x.NombreProveedor = ""
                Exit Sub
            End If
        End If

        If rdbDetallado.Checked = True Then
            x.ResDet = "D"
        End If

        If rdbResumido.Checked = True Then
            x.ResDet = "R"
        End If

        x.FechaIni = dtpFechaIni.Value
        x.FechaFin = dtpFechaFin.Value

        x.ShowDialog()
    End Sub

    Private Sub chkTodosSubC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodosSubC.CheckedChanged
        If chkTodosSubC.Checked = True Then
            cboTodosSubC.SelectedIndex = -1
        End If
    End Sub

    Private Sub TabPage1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage1.Click

    End Sub

    Private Sub btnVerTodosEmprSubC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerTodosEmprSubC.Click
        If chkTodosEmprSubc.Checked = False Then
            If cboTodosSubCxEmpr.SelectedIndex = -1 Then
                MsgBox("Seleccione un Proveedor!", MsgBoxStyle.Information, glbNameSistema)
                cboTodosSubCxEmpr.Focus()
                Exit Sub
            End If
        End If

        Try
            Dim dtTable2 As DataTable
            Dim pIdRetencion As String = ""
            Dim pSerie As String = ""
            dtTable2 = New DataTable
            eGastosGenerales = New clsGastosGenerales

            If chkTodosEmprSubc.Checked = True Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosxEmpresaAlaFecha(54, gEmpresa, Trim(cboTodosProvxEmpr.SelectedValue), dtpFechaFinal.Value, dtpFechaInicio.Value)
            End If

            If chkTodosEmprSubc.Checked = False And cboTodosProvxEmpr.SelectedIndex > -1 Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosxEmpresaAlaFecha(55, gEmpresa, Trim(cboTodosProvxEmpr.SelectedValue), dtpFechaFinal.Value, dtpFechaInicio.Value)
            End If

            If dtTable2.Rows.Count > 0 Then
                'If chk4.Checked = True Then
                Dim RD As String = ""
                If rdbResumidoEmpr.Checked = True Then
                    RD = "R"
                ElseIf rdbDetalladoEmpr.Checked = True Then
                    RD = "D"
                End If
                Muestra_Reporte(RutaAppReportes & "rptFacturasPorPagarGlobalTodos2.rpt", dtTable2, "TblLibroDiario", "", "ResDet;" & Trim(RD), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Fecha;" & dtpFechaFinal.Value, "FechaIni;" & dtpFechaInicio.Value)
                'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                'End If
            ElseIf dtTable2.Rows.Count = 0 Then
                Dim mensaje As String = ""
                'If rdbCancelar.Checked = True Then
                mensaje = "No se Encontr� ningun Comprobante a Cancelar."
                'End If
                'If rdbCancelados.Checked = True Then
                '    mensaje = "No se Encontro ningun Comprobante Cancelado."
                'End If
                'If rdbCC.Checked = True Then
                '    mensaje = "No se Encontro Cuenta Corriente de Proveedores en este Centro de Costo."
                'End If
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub
End Class