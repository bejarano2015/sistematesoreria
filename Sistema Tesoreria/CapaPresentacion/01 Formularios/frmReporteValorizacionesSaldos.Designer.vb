<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteValorizacionesSaldos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnVer = New System.Windows.Forms.Button
        Me.rdbSaldoVal = New System.Windows.Forms.RadioButton
        Me.rdbSaldoValFac = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'btnVer
        '
        Me.btnVer.Location = New System.Drawing.Point(277, 60)
        Me.btnVer.Name = "btnVer"
        Me.btnVer.Size = New System.Drawing.Size(92, 23)
        Me.btnVer.TabIndex = 5
        Me.btnVer.Text = "Ver"
        Me.btnVer.UseVisualStyleBackColor = True
        '
        'rdbSaldoVal
        '
        Me.rdbSaldoVal.AutoSize = True
        Me.rdbSaldoVal.Location = New System.Drawing.Point(24, 18)
        Me.rdbSaldoVal.Name = "rdbSaldoVal"
        Me.rdbSaldoVal.Size = New System.Drawing.Size(187, 17)
        Me.rdbSaldoVal.TabIndex = 4
        Me.rdbSaldoVal.TabStop = True
        Me.rdbSaldoVal.Text = "Saldo de Valorizaciones por Pagar"
        Me.rdbSaldoVal.UseVisualStyleBackColor = True
        '
        'rdbSaldoValFac
        '
        Me.rdbSaldoValFac.AutoSize = True
        Me.rdbSaldoValFac.Location = New System.Drawing.Point(24, 41)
        Me.rdbSaldoValFac.Name = "rdbSaldoValFac"
        Me.rdbSaldoValFac.Size = New System.Drawing.Size(197, 17)
        Me.rdbSaldoValFac.TabIndex = 6
        Me.rdbSaldoValFac.TabStop = True
        Me.rdbSaldoValFac.Text = "Saldo de Valorizaciones Canceladas"
        Me.rdbSaldoValFac.UseVisualStyleBackColor = True
        '
        'frmReporteValorizacionesSaldos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(381, 95)
        Me.Controls.Add(Me.rdbSaldoValFac)
        Me.Controls.Add(Me.btnVer)
        Me.Controls.Add(Me.rdbSaldoVal)
        Me.MaximizeBox = False
        Me.Name = "frmReporteValorizacionesSaldos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte de Saldo de Valorizaciones por Pagar"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnVer As System.Windows.Forms.Button
    Friend WithEvents rdbSaldoVal As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSaldoValFac As System.Windows.Forms.RadioButton
End Class
