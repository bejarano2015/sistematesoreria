Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows
Imports System.Data.SqlClient

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common

Public Class frmReporteConsolidado

    Private eReporteConsolidado As clsReporteConsolidado

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteConsolidado = Nothing
    Public Shared Function Instance() As frmReporteConsolidado
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteConsolidado
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteConsolidado_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteConsolidado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        eReporteConsolidado = New clsReporteConsolidado
        cboUbicacion.DataSource = eReporteConsolidado.ConsultarConsolidado(4, gEmpresa, "", "", "", gDesEmpresa, gEmprRuc, glbSisCodigo)
        If eReporteConsolidado.iNroRegistros > 0 Then
            cboUbicacion.ValueMember = "ubtCodigo"
            cboUbicacion.DisplayMember = "ubtDescripcion"
            cboUbicacion.SelectedIndex = -1
        End If

        'Try
        '    'DtLibroDiario.Clear()

        '    'Me.Cursor = Cursors.WaitCursor
        '    'DtLibroDiario = cCapaReportes.Rep_LibroDiario(sEmpresa, cboMes.SelectedValue, sPeriodo, _
        '    '        cCapaFunctGen.AbreviaturaMoneda(cboMoneda.SelectedValue), Trim(txtSubInicial.Text), Trim(txtSubFinal.Text))

        '    'If DtLibroDiario.Rows.Count = 0 Then
        '    '    MessageBox.Show("No hay Comprobantes que mostrar", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    '    Me.Cursor = Cursors.Default
        '    '    Exit Sub
        '    'End If
        '    ''pbProgreso.Maximum = DtLibroDiario.Rows.Count - 1
        '    'pbProgreso.Maximum = 100
        '    'pbProgreso.Minimum = 0
        '    'pbProgreso.Value = 0

        '    'For x = pbProgreso.Minimum To pbProgreso.Maximum
        '    '    lblMeses.Text = Format(((pbProgreso.Value * 100) / pbProgreso.Maximum), "#,##0").ToString & " %"
        '    '    pbProgreso.Value = x
        '    '    Application.DoEvents()
        '    '    'System.Threading.Thread.Sleep(50)
        '    '    System.Threading.Thread.Sleep(5)
        '    'Next

        '    ObjReporte.Muestra_Reporte(STRRUTAREPORTES & "CrLibroDiario.rpt", DtLibroDiario, "TblLibroDiario", "", _
        '                            "StrEmpresa;" & cCapaFunctGen.DescripcionEmpresa(sEmpresa), _
        '                            "StrRUC;" & cCapaFunctGen.RUCempresa(sEmpresa), _
        '                            "StrPeriodo;" & "MES DE " & cboMes.Text & " DE " & sPeriodo, _
        '                            "StrDesMoneda;" & cboMoneda.Text)

        'Catch ex As Exception
        '    MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End Try
        'Me.Cursor = Cursors.Default


    End Sub


    'cCapaReportes = New clsReportes



    '--------------------------------------------------------

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Try
            'Next
            Dim dtTable2 As DataTable
            dtTable2 = New DataTable
            eReporteConsolidado = New clsReporteConsolidado
            If cboUbicacion.SelectedIndex > -1 And cboEspecialidad.SelectedIndex = -1 And cboTitulo.SelectedIndex = -1 Then
                dtTable2 = eReporteConsolidado.ConsultarConsolidado(0, gEmpresa, Trim(cboUbicacion.SelectedValue()), "", "", gDesEmpresa, gEmprRuc, glbSisCodigo)
            ElseIf cboUbicacion.SelectedIndex > -1 And cboEspecialidad.SelectedIndex > -1 And cboTitulo.SelectedIndex = -1 Then
                dtTable2 = eReporteConsolidado.ConsultarConsolidado(1, gEmpresa, Trim(cboUbicacion.SelectedValue()), Trim(cboEspecialidad.SelectedValue()), "", gDesEmpresa, gEmprRuc, glbSisCodigo)
            ElseIf cboUbicacion.SelectedIndex > -1 And cboEspecialidad.SelectedIndex = -1 And cboTitulo.SelectedIndex > -1 Then
                dtTable2 = eReporteConsolidado.ConsultarConsolidado(2, gEmpresa, Trim(cboUbicacion.SelectedValue()), "", Trim(cboTitulo.SelectedValue()), gDesEmpresa, gEmprRuc, glbSisCodigo)
            ElseIf cboUbicacion.SelectedIndex > -1 And cboEspecialidad.SelectedIndex > -1 And cboTitulo.SelectedIndex > -1 Then
                dtTable2 = eReporteConsolidado.ConsultarConsolidado(2, gEmpresa, Trim(cboUbicacion.SelectedValue()), Trim(cboEspecialidad.SelectedValue()), Trim(cboTitulo.SelectedValue()), gDesEmpresa, gEmprRuc, glbSisCodigo)
            End If
            

            'GL.RutaApp es solo una variable tipo String. Otro ejemplo para obtener rutas 
            

            If dtTable2.Rows.Count > 0 Then
                'Muestra_Reporte(STRRUTAREPORTES & "rptConsolidado.rpt", dtTable2, "TblLibroDiario", "", "StrEmpresa;" & cCapaFunctGen.DescripcionEmpresa(sEmpresa), "StrRUC;" & cCapaFunctGen.RUCempresa(sEmpresa), "StrPeriodo;" & "MES DE " & cboMes.Text & " DE " & sPeriodo, "StrDesMoneda;" & cboMoneda.Text)
                Muestra_Reporte(RutaAppReportes & "rptConsolidado.rpt", dtTable2, "TblLibroDiario", "", "Empresa;" & Trim(gDesEmpresa), "Ruc;" & Trim(gEmprRuc))
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cboUbicacion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUbicacion.SelectedIndexChanged

    End Sub

    Private Sub cboUbicacion_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUbicacion.SelectionChangeCommitted
        eReporteConsolidado = New clsReporteConsolidado
        cboEspecialidad.DataSource = eReporteConsolidado.ConsultarConsolidado(5, gEmpresa, Trim(cboUbicacion.SelectedValue), "", "", gDesEmpresa, gEmprRuc, glbSisCodigo)
        If eReporteConsolidado.iNroRegistros > 0 Then
            cboEspecialidad.ValueMember = "EspCodigo"
            cboEspecialidad.DisplayMember = "EspDescripcion"
            cboEspecialidad.SelectedIndex = -1
        End If

        If cboUbicacion.SelectedIndex > -1 Then
            chkTodosEsp.Checked = False
        End If
    End Sub

    Private Sub cboEspecialidad_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEspecialidad.SelectedIndexChanged

    End Sub

    Private Sub cboEspecialidad_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEspecialidad.SelectionChangeCommitted
        eReporteConsolidado = New clsReporteConsolidado
        cboTitulo.DataSource = eReporteConsolidado.ConsultarConsolidado(6, gEmpresa, Trim(cboUbicacion.SelectedValue), Trim(cboEspecialidad.SelectedValue), "", gDesEmpresa, gEmprRuc, glbSisCodigo)
        If eReporteConsolidado.iNroRegistros > 0 Then
            cboTitulo.ValueMember = "TitCodigo"
            cboTitulo.DisplayMember = "TitDescripcion"
            cboTitulo.SelectedIndex = -1
        End If

        If cboEspecialidad.SelectedIndex > -1 Then
            chkTodosTit.Checked = False
        End If
    End Sub

    Private Sub chkTodosEsp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodosEsp.CheckedChanged
        If chkTodosEsp.Checked = True Then
            cboEspecialidad.SelectedIndex = -1
        End If
    End Sub

    Private Sub chkTodosTit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodosTit.CheckedChanged
        If chkTodosTit.Checked = True Then
            cboTitulo.SelectedIndex = -1
        End If
    End Sub
End Class