Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class FrmREPView_PolCuotasXPagar

    Public strNombreFom As String

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As FrmREPView_PolCuotasXPagar = Nothing
    Public Shared Function Instance() As FrmREPView_PolCuotasXPagar
        If frmInstance Is Nothing Then
            frmInstance = New FrmREPView_PolCuotasXPagar
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub FrmREPView_PolCuotasXPagar_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region


    Private Sub FrmREPView_PolCuotasXPagar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = strNombreFom
    End Sub
End Class