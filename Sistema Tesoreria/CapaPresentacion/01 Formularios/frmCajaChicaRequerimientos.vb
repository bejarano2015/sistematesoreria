Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports CapaEntidad

Public Class frmCajaChicaRequerimientos2

    Private eCompraRequerimientos As clsCompraRequerimientos
    Private eOrdenCabecera As clsOrdenCabecera
    Private ePagoProveedores As clsPagoProveedores
    Public IdCaja As String = ""
    Public IdDetCaja As String = ""
    Public TipoDoc As String = ""
    Public NroDoc As String = ""
    Public FechaFac As DateTime
    Public RucDoc As String = ""
    Public RazonDoc As String = ""
    Public MonedaDoc As String = ""
    Dim dTipoCambio As Double = 0
    Dim dIgv As Double = 0


    Dim Grabar As Int16 = 0

    Dim objNegOtros As NegOtros = New NegOtros()
    ''
    Dim obj_SrvRegistroDocumento As SrvRegistroDocumento = New SrvRegistroDocumento()
    Dim obj_beanregistroDocumento As BeanRegistroDocumento = New BeanRegistroDocumento()

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCajaChicaRequerimientos2 = Nothing
    Public Shared Function Instance() As frmCajaChicaRequerimientos2
        If frmInstance Is Nothing Then
            frmInstance = New frmCajaChicaRequerimientos2
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmCajaChicaRequerimientos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region



    Private Sub frmCajaChicaRequerimientos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        BeLabel3.Text = ""
        BeLabel4.Text = ""

        eCompraRequerimientos = New clsCompraRequerimientos
        cboEmpresas.DataSource = eCompraRequerimientos.fListar(0, "", "", "", gPeriodo)
        If eCompraRequerimientos.iNroRegistros > 0 Then
            cboEmpresas.ValueMember = "EmprCodigo"
            cboEmpresas.DisplayMember = "EmprDescripcion"
            'cboEmpresas.SelectedValue = Trim(gEmpresa)
            cboEmpresas.Focus()
        End If
        BeLabel4.Text = TipoDoc & " / " & NroDoc
        'cboBanco.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
        'If eGastosGenerales.iNroRegistros > 0 Then
        '    cboBanco.ValueMember = "IdBanco"
        '    cboBanco.DisplayMember = "NombreBanco"
        '    cboBanco.SelectedIndex = -1
        'End If
        cboEmpresas.DropDownStyle = ComboBoxStyle.DropDownList
        cboCCostos.DropDownStyle = ComboBoxStyle.DropDownList

        For xCol As Integer = 0 To dgvReq.Columns.Count - 1
            dgvReq.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
            'dgvReq.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next
        For xCol As Integer = 0 To dgvReqAgregados.Columns.Count - 1
            dgvReqAgregados.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
            'dgvReqAgregados.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next


        Dim dtTableTipoCambio As DataTable
        dtTableTipoCambio = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, FechaFac)
        If dtTableTipoCambio.Rows.Count > 0 Then
            dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
            dTipoCambio = Format(dTipoCambio, "0.00")
        Else
            dTipoCambio = 0.0
        End If

        Dim dtTable As DataTable
        dtTable = New DataTable
        ePagoProveedores = New clsPagoProveedores
        'eTempo = New clsPlantTempo
        dtTable = ePagoProveedores.fListarParametroIGV(FechaFac)
        If dtTable.Rows.Count > 0 Then
            dIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
            'txtTasaIGV.Text = Format(dIgv, "0.00")
        Else
            dIgv = 0.0
            'txtTasaIGV.Text = Format(dIgv, "0.00")
        End If

        For x As Integer = 0 To dgvReqAgregados.RowCount - 1
            dgvReqAgregados.Rows.Remove(dgvReqAgregados.CurrentRow)
        Next

    End Sub

    Private Sub cboEmpresas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpresas.SelectedIndexChanged
        Dim IdEmpr As String
        If (cboEmpresas.SelectedIndex > -1) Then
            Try
                IdEmpr = cboEmpresas.SelectedValue.ToString
                If IdEmpr <> "System.Data.DataRowView" Then
                    eCompraRequerimientos = New clsCompraRequerimientos
                    cboCCostos.DataSource = eCompraRequerimientos.fListar(1, IdEmpr, "", "", gPeriodo)
                    If eCompraRequerimientos.iNroRegistros > 0 Then
                        cboCCostos.ValueMember = "CCosCodigo"
                        cboCCostos.DisplayMember = "CCosDescripcion"
                        cboCCostos.SelectedIndex = -1
                        cboCCostos.Focus()
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboCCostos.DataSource = Nothing
        End If
    End Sub

    Private Sub txtNroReq_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroReq.TextChanged
        If cboEmpresas.SelectedIndex > -1 And cboCCostos.SelectedIndex > -1 Then
            'If Len(Trim(BeLabel3.Text)) = 15 Then
            'BeLabel3.ForeColor = Color.Red
            'eCompraRequerimientos = New clsCompraRequerimientos
            'Dim dtReqDet As DataTable
            'dtReqDet = New DataTable
            'dtReqDet = eCompraRequerimientos.fListar(3, Trim(cboEmpresas.SelectedValue), Trim(cboCCostos.SelectedValue), Trim(BeLabel3.Text))
            'If dtReqDet.Rows.Count > 0 Then
            '    dgvReq.DataSource = dtReqDet
            'End If
            'Exit Sub
            'Else

            'Dim sSufijo As String = "0000000"
            'sCodFuturo = Trim(Microsoft.VisualBasic.Right(sSufijo & Convert.ToString(Trim(txtNroReq.Text)), sSufijo.Length))

            If Len(Trim(txtNroReq.Text)) > 0 Then
                BeLabel3.ForeColor = Color.Black
                BeLabel3.Text = ""
                eCompraRequerimientos = New clsCompraRequerimientos
                Dim dtReqCab As DataTable
                dtReqCab = New DataTable
                dtReqCab = eCompraRequerimientos.fListar(2, Trim(cboEmpresas.SelectedValue), Trim(cboCCostos.SelectedValue), Trim(txtNroReq.Text), gPeriodo)
                If dtReqCab.Rows.Count > 0 Then
                    BeLabel3.Text = Trim(dtReqCab.Rows(0).Item("ReqCodigo"))
                End If
            ElseIf Len(Trim(txtNroReq.Text)) = 0 Then
                BeLabel3.ForeColor = Color.Black
                BeLabel3.Text = ""
            End If

        End If
    End Sub


    Private Sub cboCCostos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCCostos.SelectedIndexChanged
        txtNroReq.Clear()
        txtNroReq.Focus()
    End Sub

    Private Sub BeLabel3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BeLabel3.TextChanged

        If cboEmpresas.SelectedIndex > -1 And cboCCostos.SelectedIndex > -1 Then
            If Len(Trim(BeLabel3.Text)) = 15 Then
                BeLabel3.ForeColor = Color.Red
                eCompraRequerimientos = New clsCompraRequerimientos
                Dim dtReqDet As DataTable
                dtReqDet = New DataTable
                dtReqDet = eCompraRequerimientos.fListar(3, Trim(cboEmpresas.SelectedValue), Trim(cboCCostos.SelectedValue), Trim(BeLabel3.Text), gPeriodo)
                If dtReqDet.Rows.Count > 0 Then
                    dgvReq.DataSource = dtReqDet
                End If
                Exit Sub
            ElseIf Len(Trim(BeLabel3.Text)) = 0 Then
                For x As Integer = 0 To dgvReq.RowCount - 1
                    dgvReq.Rows.Remove(dgvReq.CurrentRow)
                Next
            End If
        End If

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click

        Dim CountSeleccionados As Integer = 0
        If dgvReq.Rows.Count > 0 Then
            For x As Integer = 0 To dgvReq.Rows.Count - 1
                If Convert.ToBoolean(dgvReq.Rows(x).Cells("X").Value) = True Then
                    CountSeleccionados = CountSeleccionados + 1
                    If dgvReqAgregados.Rows.Count = 0 Then
                        dgvReqAgregados.Rows.Add()
                        dgvReqAgregados.Rows(0).Cells("Codigo2").Value = dgvReq.Rows(x).Cells("Codigo").Value
                        dgvReqAgregados.Rows(0).Cells("Descripcion2").Value = dgvReq.Rows(x).Cells("Descripcion").Value
                        dgvReqAgregados.Rows(0).Cells("CantGrupalPorComprar2").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                        dgvReqAgregados.Rows(0).Cells("CantGrupalPorComprarOculto").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                        dgvReqAgregados.Rows(0).Cells("CantGrupalPorComprarOculto2").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                        dgvReqAgregados.Rows(0).Cells("Precio2").Value = "0.00"
                        dgvReqAgregados.Rows(0).Cells("Importe").Value = "0.00"
                        dgvReqAgregados.Rows(0).Cells("EvlDescripcion2").Value = dgvReq.Rows(x).Cells("EvlDescripcion").Value
                        dgvReqAgregados.Rows(0).Cells("ReqCodigo2").Value = dgvReq.Rows(x).Cells("ReqCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("DrqCodigo2").Value = dgvReq.Rows(x).Cells("DrqCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("EmprCodigo2").Value = dgvReq.Rows(x).Cells("EmprCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("CCosCodigo2").Value = dgvReq.Rows(x).Cells("CCosCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("LinCodigo2").Value = dgvReq.Rows(x).Cells("LinCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("SubCodigo2").Value = dgvReq.Rows(x).Cells("SubCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("TipCodigo2").Value = dgvReq.Rows(x).Cells("TipCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("ProCodigo2").Value = dgvReq.Rows(x).Cells("ProCodigo").Value
                        dgvReqAgregados.Rows(0).Cells("UmdDescripcion2").Value = dgvReq.Rows(x).Cells("UmdDescripcion").Value
                        dgvReqAgregados.Rows(0).Cells("UmdNivel2").Value = dgvReq.Rows(x).Cells("UmdNivel").Value
                        dgvReqAgregados.Rows(0).Cells("UmdEquivalencia2").Value = dgvReq.Rows(x).Cells("UmdEquivalencia").Value
                        dgvReqAgregados.Rows(0).Cells("UmdDescripcionCombo").Value = dgvReq.Rows(x).Cells("UmdDescripcion").Value

                        dgvReqAgregados.Rows(0).Cells("ProNombreComercial2x").Value = dgvReq.Rows(x).Cells("ProNombreComercial2").Value
                        dgvReqAgregados.Rows(0).Cells("DrqAgregado2x").Value = dgvReq.Rows(x).Cells("DrqAgregado2").Value

                        'dgvReqAgregados.Rows(0).Cells("DescGrupal").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                        'CantGrupalPorComprar2
                        'DescGrupal
                        'Private Sub cargarCentrosDeCostoxEmpresa()
                        'For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
                        'If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(y).Cells("Column2")) = False Then

                        Dim CodeMat As String = ""
                        CodeMat = Trim(dgvReqAgregados.Rows(0).Cells("Codigo2").Value)
                        Dim my_strsql As String
                        Dim d_objds As New DataSet()
                        Dim dgrow As New DataGridViewComboBoxCell
                        my_strsql = "SELECT * FROM LOGISTICA.UNIDADESMATERIALES WHERE ProCodigoBarrasExt='" & Trim(CodeMat) & "' ORDER BY UmdDescripcion"
                        Dim objda As New SqlDataAdapter(my_strsql, conexionParaGastosGene)
                        objda.Fill(d_objds, "UmdCodigo")
                        objda.Fill(d_objds, "UmdDescripcion")
                        dgrow.DataSource = d_objds.Tables(0).DefaultView
                        dgrow.DisplayMember = "UmdDescripcion"
                        dgrow.ValueMember = "UmdCodigo"
                        Try
                            Me.dgvReqAgregados.Item("UndMed", 0) = dgrow
                        Catch ex As Exception
                            MessageBox.Show("Aqui un error: " & ex.ToString)
                        End Try
                        dgvReqAgregados.Rows(0).Cells("UndMed").Value = dgvReqAgregados.Rows(0).Cells("UmdDescripcion2").Value
                        ' End If
                        ' Next
                        'End Sub

                    ElseIf dgvReqAgregados.Rows.Count > 0 Then
                        Dim Existe As String = ""
                        For q As Integer = 0 To dgvReqAgregados.Rows.Count - 1
                            If (Trim(dgvReq.Rows(x).Cells("ReqCodigo").Value) = Trim(dgvReqAgregados.Rows(q).Cells("ReqCodigo2").Value)) And (Trim(dgvReq.Rows(x).Cells("DrqCodigo").Value) = Trim(dgvReqAgregados.Rows(q).Cells("DrqCodigo2").Value)) And (Trim(dgvReq.Rows(x).Cells("EmprCodigo").Value) = Trim(dgvReqAgregados.Rows(q).Cells("EmprCodigo2").Value)) Then
                                Existe = "1"
                                Exit For
                            Else
                                Existe = "0"
                            End If
                        Next
                        If Existe = "0" Then
                            dgvReqAgregados.Rows.Add()
                            Dim A As Integer
                            A = dgvReqAgregados.Rows.Count
                            'dgvReqAgregados.Rows(A - 1).Cells("Descripcion2").Value = dgvReq.Rows(x).Cells("Descripcion").Value
                            'dgvReqAgregados.Rows(A - 1).Cells("NombreComercial2").Value = dgvReq.Rows(x).Cells("NombreComercial").Value
                            'dgvReqAgregados.Rows(A - 1).Cells("CantGrupalPorComprar2").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                            'dgvReqAgregados.Rows(A - 1).Cells("CantxComprar").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                            'dgvReqAgregados.Rows(A - 1).Cells("ReqCodigo2").Value = dgvReq.Rows(x).Cells("ReqCodigo").Value
                            'dgvReqAgregados.Rows(A - 1).Cells("DrqCodigo2").Value = dgvReq.Rows(x).Cells("DrqCodigo").Value
                            'dgvReqAgregados.Rows(A - 1).Cells("EmprCodigo2").Value = dgvReq.Rows(x).Cells("EmprCodigo").Value

                            dgvReqAgregados.Rows(A - 1).Cells("Codigo2").Value = dgvReq.Rows(x).Cells("Codigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("Descripcion2").Value = dgvReq.Rows(x).Cells("Descripcion").Value
                            dgvReqAgregados.Rows(A - 1).Cells("CantGrupalPorComprar2").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                            dgvReqAgregados.Rows(A - 1).Cells("CantGrupalPorComprarOculto").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                            dgvReqAgregados.Rows(A - 1).Cells("CantGrupalPorComprarOculto2").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value
                            dgvReqAgregados.Rows(A - 1).Cells("Precio2").Value = "0.00"
                            dgvReqAgregados.Rows(A - 1).Cells("Importe").Value = "0.00"
                            dgvReqAgregados.Rows(A - 1).Cells("EvlDescripcion2").Value = dgvReq.Rows(x).Cells("EvlDescripcion").Value
                            dgvReqAgregados.Rows(A - 1).Cells("ReqCodigo2").Value = dgvReq.Rows(x).Cells("ReqCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("DrqCodigo2").Value = dgvReq.Rows(x).Cells("DrqCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("EmprCodigo2").Value = dgvReq.Rows(x).Cells("EmprCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("CCosCodigo2").Value = dgvReq.Rows(x).Cells("CCosCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("LinCodigo2").Value = dgvReq.Rows(x).Cells("LinCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("SubCodigo2").Value = dgvReq.Rows(x).Cells("SubCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("TipCodigo2").Value = dgvReq.Rows(x).Cells("TipCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("ProCodigo2").Value = dgvReq.Rows(x).Cells("ProCodigo").Value
                            dgvReqAgregados.Rows(A - 1).Cells("UmdDescripcion2").Value = dgvReq.Rows(x).Cells("UmdDescripcion").Value
                            dgvReqAgregados.Rows(A - 1).Cells("UmdNivel2").Value = dgvReq.Rows(x).Cells("UmdNivel").Value
                            dgvReqAgregados.Rows(A - 1).Cells("UmdEquivalencia2").Value = dgvReq.Rows(x).Cells("UmdEquivalencia").Value
                            dgvReqAgregados.Rows(A - 1).Cells("UmdDescripcionCombo").Value = dgvReq.Rows(x).Cells("UmdDescripcion").Value

                            dgvReqAgregados.Rows(A - 1).Cells("ProNombreComercial2x").Value = dgvReq.Rows(x).Cells("ProNombreComercial2").Value
                            dgvReqAgregados.Rows(A - 1).Cells("DrqAgregado2x").Value = dgvReq.Rows(x).Cells("DrqAgregado2").Value

                            'dgvReqAgregados.Rows(A - 1).Cells("DescGrupal").Value = dgvReq.Rows(x).Cells("CantGrupalPorComprar").Value

                            Dim CodeMat As String = ""
                            CodeMat = Trim(dgvReqAgregados.Rows(A - 1).Cells("Codigo2").Value)
                            Dim my_strsql As String
                            Dim d_objds As New DataSet()
                            Dim dgrow As New DataGridViewComboBoxCell
                            my_strsql = "SELECT * FROM LOGISTICA.UNIDADESMATERIALES WHERE ProCodigoBarrasExt='" & Trim(CodeMat) & "' ORDER BY UmdDescripcion"
                            Dim objda As New SqlDataAdapter(my_strsql, conexionParaGastosGene)
                            objda.Fill(d_objds, "UmdCodigo")
                            objda.Fill(d_objds, "UmdDescripcion")
                            dgrow.DataSource = d_objds.Tables(0).DefaultView
                            dgrow.DisplayMember = "UmdDescripcion"
                            dgrow.ValueMember = "UmdCodigo"
                            Try
                                Me.dgvReqAgregados.Item("UndMed", A - 1) = dgrow
                            Catch ex As Exception
                                MessageBox.Show("Aqui un error: " & ex.ToString)
                            End Try
                            dgvReqAgregados.Rows(A - 1).Cells("UndMed").Value = dgvReqAgregados.Rows(A - 1).Cells("UmdDescripcion2").Value
                        End If
                    End If
                End If
            Next
        End If

        If CountSeleccionados = 0 Then
            MessageBox.Show("No ha seleccionado ningun Material", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        If dgvReqAgregados.Rows.Count > 0 Then
            dgvReqAgregados.CurrentCell = dgvReqAgregados(3, 0)
            dgvReqAgregados.Focus()
        End If

    End Sub

    Private Sub dgvReqAgregados_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReqAgregados.CellContentClick
    End Sub

    Private Sub dgvReqAgregados_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReqAgregados.CellEndEdit
        If e.ColumnIndex = 3 Then

            Dim CodUnidMed As String = ""
            Dim CodProdX As String = ""
            'Dim CodNivel As String = ""
            Dim CodNivel As Integer = 0
            Dim CodNivelNew As Integer = 0
            Dim CodEquiNew As Double = 0

            Dim CodEquiNew2 As Integer = 0
            Dim sxUmdDescripcion As String = ""
            Dim sxUmdDescripcion2 As String = ""
            CodUnidMed = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value)
            dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcionCombo").Value = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value)
            CodNivel = Convert.ToInt64(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdNivel2").Value) 'Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdNivel2").Value)
            CodProdX = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("Codigo2").Value)

            'MessageBox.Show(Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value))

            If Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value) <> Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcion2").Value) Then
                'UmdEquivalencia2
                Dim dtTableNivelPro As DataTable
                dtTableNivelPro = New DataTable
                'ePagoProveedores = New clsPagoProveedores
                'dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, FechaFac)

                eCompraRequerimientos = New clsCompraRequerimientos
                dtTableNivelPro = eCompraRequerimientos.fListar(7, "", CodUnidMed, CodProdX, gPeriodo)
                'UmdDescripcionCombo
                If dtTableNivelPro.Rows.Count > 0 Then
                    'CodNivelNew = Val(dtTableNivelPro.Rows(0).Item("UmdNivel"))
                    CodNivelNew = Convert.ToInt64(dtTableNivelPro.Rows(0).Item("UmdNivel"))
                    CodEquiNew = Convert.ToDouble(dtTableNivelPro.Rows(0).Item("UmdEquivalencia"))
                    CodEquiNew2 = Convert.ToInt64(dtTableNivelPro.Rows(0).Item("UmdEquivalencia"))
                    sxUmdDescripcion = Trim(dtTableNivelPro.Rows(0).Item("UmdDescripcion"))
                    sxUmdDescripcion2 = Trim(dtTableNivelPro.Rows(0).Item("UmdDescripcion2"))

                    'Dim dtTableNivelPro2 As DataTable
                    'dtTableNivelPro2 = New DataTable
                    'eCompraRequerimientos = New clsCompraRequerimientos
                    'dtTableNivelPro2 = eCompraRequerimientos.fListar(7, "", dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcion2").Value, CodProdX, gPeriodo)
                    'If dtTableNivelPro2.Rows.Count > 0 Then
                    '    sxUmdDescripcion2 = Trim(dtTableNivelPro2.Rows(0).Item("UmdDescripcion2"))
                    'End If

                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("EvlDescripcion2").Value = sxUmdDescripcion & " de " & CodEquiNew2 '& " " & sxUmdDescripcion2 'Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value)
                End If

                If CodNivelNew > CodNivel Then
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value = Format((CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString) * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString)) / CodEquiNew, "#,##0.00")
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto").Value = Format((CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString)) / CodEquiNew), "#,##0.00")
                    'dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalenciaX").Value = Format(CDec(CodEquiNew), "#,##0.00")
                    'dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("DescGrupal").Value = Format((CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString)) / CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value)), "#,##0.00") 'Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString)), "#,##0.00")
                ElseIf CodNivelNew < CodNivel Then
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString) * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString), "#,##0.00")
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString)), "#,##0.00")
                    'dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalenciaX").Value = Format(CDec(CodEquiNew), "#,##0.00")
                    'dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("DescGrupal").Value = Format((CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString)) / CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value)), "#,##0.00") 'Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString * CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdEquivalencia2").Value.ToString)), "#,##0.00")
                End If

                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcionCombo").Value = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value)

            ElseIf Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value) = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcion2").Value) Then
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString), "#,##0.00")
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString), "#,##0.00")
                'dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("DescGrupal").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto2").Value.ToString), "#,##0.00")

                Dim dtTableNivelPro As DataTable
                dtTableNivelPro = New DataTable
                eCompraRequerimientos = New clsCompraRequerimientos
                dtTableNivelPro = eCompraRequerimientos.fListar(7, "", CodUnidMed, CodProdX, gPeriodo)
                If dtTableNivelPro.Rows.Count > 0 Then
                    sxUmdDescripcion = Trim(dtTableNivelPro.Rows(0).Item("UmdDescripcion"))
                    'sxUmdDescripcion2 = Trim(dtTableNivelPro.Rows(0).Item("UmdDescripcion2"))
                    CodEquiNew2 = Convert.ToInt64(dtTableNivelPro.Rows(0).Item("UmdEquivalencia"))

                    'Dim dtTableNivelPro2 As DataTable
                    'dtTableNivelPro2 = New DataTable
                    'eCompraRequerimientos = New clsCompraRequerimientos
                    'dtTableNivelPro2 = eCompraRequerimientos.fListar(7, "", dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcion2").Value, CodProdX, gPeriodo)
                    'If dtTableNivelPro2.Rows.Count > 0 Then
                    '    sxUmdDescripcion2 = Trim(dtTableNivelPro2.Rows(0).Item("UmdDescripcion2"))
                    'End If

                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("EvlDescripcion2").Value = sxUmdDescripcion & " de " & CodEquiNew2 '& " " & sxUmdDescripcion2 'Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value)
                End If

            End If

        End If
        If e.ColumnIndex = 4 Then

            'If Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value) <> Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcion2").Value) Then
            '    Dim CodUnidMed As String = ""
            '    Dim CodProdX As String = ""
            '    Dim CodNivel As Integer = 0
            '    Dim CodNivelNew As Integer = 0
            '    Dim CodEquiNew As Double = 0
            '    CodUnidMed = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value)
            '    CodNivel = Convert.ToInt64(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdNivel2").Value) 'Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdNivel2").Value)
            '    CodProdX = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("Codigo2").Value)
            '    Dim dtTableNivelPro As DataTable
            '    dtTableNivelPro = New DataTable
            '    eCompraRequerimientos = New clsCompraRequerimientos
            '    dtTableNivelPro = eCompraRequerimientos.fListar(7, "", CodUnidMed, CodProdX, gPeriodo)
            '    If dtTableNivelPro.Rows.Count > 0 Then
            '        CodNivelNew = Convert.ToInt64(dtTableNivelPro.Rows(0).Item("UmdNivel"))
            '        CodEquiNew = Convert.ToDouble(dtTableNivelPro.Rows(0).Item("UmdEquivalencia"))
            '    End If
            '    If CodNivelNew > CodNivel Then
            '        dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("DescGrupal").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString) * CodEquiNew, "#,##0.00")
            '    ElseIf CodNivelNew < CodNivel Then
            '        dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("DescGrupal").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString) / CodEquiNew, "#,##0.00")
            '    End If
            'ElseIf Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UndMed").Value) = Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdDescripcion2").Value) Then
            '    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("DescGrupal").Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
            'End If

            If Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")

                If Len(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value) > 0 Then
                    Dim dComprado As Double = 0
                    dComprado = Convert.ToDouble(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value)
                    If dComprado > dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto").Value Then
                        MessageBox.Show("La Cantidad Comprada supera la Cantidad por Atender.", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value = dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprarOculto").Value
                    End If
                End If

                If vb.IsDBNull(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If
        ElseIf e.ColumnIndex = 5 Then
            If Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If
        ElseIf e.ColumnIndex = 24 Then
            If Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                Else
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CU").Value = Format(((CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("Importe").Value.ToString)) / CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value.ToString)), "#,##0.00")
                    dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("Precio2").Value = (((CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("Importe").Value.ToString)) / 1.18) / CDec(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("CantGrupalPorComprar2").Value.ToString))

                    Dim VL_IMPORTETOTAL As Double = 0
                    For i As Integer = 0 To dgvReqAgregados.RowCount - 1
                        VL_IMPORTETOTAL = VL_IMPORTETOTAL + CDec(IIf(IsDBNull(dgvReqAgregados.Rows(i).Cells("Importe").Value.ToString()), 0, dgvReqAgregados.Rows(i).Cells("Importe").Value.ToString()))

                    Next
                    txtImporteTotal.Text = VL_IMPORTETOTAL

                End If
            Else
                dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If

        End If

    End Sub

    Private Sub dgvReqAgregados_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvReqAgregados.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvReqAgregados.CurrentCell.ColumnIndex
        If columna = 4 Or columna = 5 Then
            Dim caracter As Char = e.KeyChar
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub


    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        'Dim CantReg As Integer = 0
        If dgvReqAgregados.Rows.Count > 0 Then
            'CantReg = dgvReqAgregados.Rows.Count
            'If MessageBox.Show("� Desea Eliminar el Item ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            'For z As Integer = 0 To CantReg - 1
            '    If Convert.ToBoolean(dgvReqAgregados.Rows(z).Cells("X2").Value) = True Then
            '        dgvReqAgregados.Rows.RemoveAt(z)
            '    End If
            'Next

            For i As Integer = dgvReqAgregados.Rows.Count - 1 To 0 Step -1

                ' Si es la fila de nuevos registros, pasamos al siguiente �ndice
                ' 
                If (dgvReqAgregados.Rows(i).IsNewRow) Then Continue For
                If Convert.ToBoolean(dgvReqAgregados.Rows(i).Cells("X2").Value) = True Then
                    Me.dgvReqAgregados.Rows.RemoveAt(i)
                End If


            Next

            'End If
        End If
    End Sub

    Sub ValidarDatos2()
        Dim sCadenaMsj As String = ""
        Dim count = 0
        If dgvReqAgregados.Rows.Count = 0 Then
            MessageBox.Show("No hay Detalles para grabar la Compra", "Sistema de Tesorer�a")
            Grabar = 0
        Else
            dgvReqAgregados.CurrentCell = dgvReqAgregados(0, 0)
            For x As Integer = 0 To dgvReqAgregados.Rows.Count - 1
                If Convert.ToDouble(dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value) <= 0 Then
                    MessageBox.Show("Ingrese la cantidad comprada", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvReqAgregados.CurrentCell = dgvReqAgregados(4, x)
                    Grabar = 0
                    Exit Sub
                End If
                If Convert.ToDouble(dgvReqAgregados.Rows(x).Cells("Precio2").Value) <= 0 Then
                    MessageBox.Show("Ingrese el precio de compra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvReqAgregados.CurrentCell = dgvReqAgregados("Importe", x)
                    Grabar = 0
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click

        Grabar = 1

        ValidarDatos2()

        If Grabar = 0 Then
            dgvReqAgregados.Focus()
            Exit Sub
        End If

        If (MessageBox.Show("�Esta seguro de Confirmar la Compra?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
            If dgvReqAgregados.Rows.Count > 0 Then
                dgvReqAgregados.Sort(dgvReqAgregados.Columns("EmprCodigo2"), System.ComponentModel.ListSortDirection.Ascending)
                dgvReqAgregados.Sort(dgvReqAgregados.Columns("CCosCodigo2"), System.ComponentModel.ListSortDirection.Ascending)

                Dim emprOrdenAnterior As String = "X"
                Dim emprOrden As String = ""
                Dim CodReq As String = ""
                Dim CodCCAnterior As String = "X"
                Dim CodCC As String = ""

                Dim sPeriodo As String = ""
                Dim sPeriodoAnio As String = ""
                Dim snuevoOcoCodigo As String = ""
                Dim snuevoOcoNumero As String = ""
                Dim xLinCodigo As String = ""
                Dim xSubCodigo As String = ""
                Dim xTipCodigo As String = ""
                Dim xProCodigo As String = ""
                Dim xUmdDescripcion As String = ""
                Dim xEvlDescripcion As String = ""


                Dim xProNombreComercial2x As String = ""
                Dim xDrqAgregado2x As String = ""

                Dim xCantGrupalPorComprarUni As Decimal = 0
                Dim xCantGrupalPorComprarGrupo As Decimal = 0
                Dim xxCantCompr As Decimal = 0

                Dim xDocoPrecioGrupal As Decimal = 0
                Dim xCCosCodigo As String = ""
                Dim xDrqCodigo As String = ""

                For x As Integer = 0 To dgvReqAgregados.Rows.Count - 1
                    emprOrden = Trim(dgvReqAgregados.Rows(x).Cells("EmprCodigo2").Value)
                    CodReq = Trim(dgvReqAgregados.Rows(x).Cells("ReqCodigo2").Value)
                    CodCC = Trim(dgvReqAgregados.Rows(x).Cells("CCosCodigo2").Value)
                    xLinCodigo = Trim(dgvReqAgregados.Rows(x).Cells("LinCodigo2").Value)
                    xSubCodigo = Trim(dgvReqAgregados.Rows(x).Cells("SubCodigo2").Value)
                    xTipCodigo = Trim(dgvReqAgregados.Rows(x).Cells("TipCodigo2").Value)
                    xProCodigo = Trim(dgvReqAgregados.Rows(x).Cells("ProCodigo2").Value)
                    xUmdDescripcion = Trim(dgvReqAgregados.Rows(x).Cells("UndMed").Value)
                    xEvlDescripcion = Trim(dgvReqAgregados.Rows(x).Cells("EvlDescripcion2").Value)

                    xProNombreComercial2x = Trim(dgvReqAgregados.Rows(x).Cells("ProNombreComercial2x").Value)
                    xDrqAgregado2x = Trim(dgvReqAgregados.Rows(x).Cells("DrqAgregado2x").Value)


                    If Trim(dgvReqAgregados.Rows(x).Cells("UndMed").Value) = Trim(dgvReqAgregados.Rows(x).Cells("UmdDescripcion2").Value) Then
                        'uni
                        xCantGrupalPorComprarUni = dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value * dgvReqAgregados.Rows(x).Cells("UmdEquivalencia2").Value
                        'grupal
                        xCantGrupalPorComprarGrupo = dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value '* dgvReqAgregados.Rows(x).Cells("UmdEquivalencia2").Value
                        xxCantCompr = dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value
                    ElseIf Trim(dgvReqAgregados.Rows(x).Cells("UndMed").Value) <> Trim(dgvReqAgregados.Rows(x).Cells("UmdDescripcion2").Value) Then
                        Dim CodUnidMed As String = ""
                        Dim CodProdX As String = ""
                        Dim CodNivel As Integer = 0
                        CodUnidMed = Trim(dgvReqAgregados.Rows(x).Cells("UndMed").Value)
                        CodNivel = Convert.ToInt64(dgvReqAgregados.Rows(x).Cells("UmdNivel2").Value) 'Trim(dgvReqAgregados.Rows(dgvReqAgregados.CurrentRow.Index).Cells("UmdNivel2").Value)
                        CodProdX = Trim(dgvReqAgregados.Rows(x).Cells("Codigo2").Value)
                        Dim CodNivelNew As Integer = 0
                        Dim CodEquiNew As Double = 0
                        Dim dtTableNivelPro As DataTable
                        dtTableNivelPro = New DataTable
                        eCompraRequerimientos = New clsCompraRequerimientos
                        dtTableNivelPro = eCompraRequerimientos.fListar(7, "", CodUnidMed, CodProdX, gPeriodo)
                        If dtTableNivelPro.Rows.Count > 0 Then
                            CodNivelNew = Convert.ToInt64(dtTableNivelPro.Rows(0).Item("UmdNivel"))
                            CodEquiNew = Convert.ToDouble(dtTableNivelPro.Rows(0).Item("UmdEquivalencia"))
                        End If
                        If CodNivelNew > CodNivel Then
                            'uni
                            xCantGrupalPorComprarUni = Trim(dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value) * CodEquiNew
                            xxCantCompr = dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value
                            'grupal
                            xCantGrupalPorComprarGrupo = dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value * CodEquiNew 'dgvReqAgregados.Rows(x).Cells("UmdEquivalencia2").Value
                        ElseIf CodNivelNew < CodNivel Then
                            ''uni
                            xCantGrupalPorComprarUni = Trim(dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value)
                            xxCantCompr = dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value
                            'grupal
                            xCantGrupalPorComprarGrupo = dgvReqAgregados.Rows(x).Cells("CantGrupalPorComprar2").Value / dgvReqAgregados.Rows(x).Cells("UmdEquivalencia2").Value
                        End If
                    End If
                    xDocoPrecioGrupal = Trim(dgvReqAgregados.Rows(x).Cells("Precio2").Value)
                    xCCosCodigo = Trim(dgvReqAgregados.Rows(x).Cells("CCosCodigo2").Value)
                    xDrqCodigo = Trim(dgvReqAgregados.Rows(x).Cells("DrqCodigo2").Value)
                    If emprOrdenAnterior <> emprOrden Then
                        emprOrdenAnterior = emprOrden
                        Dim dtPeriodo As DataTable
                        Dim dnuevoOcoCodigo As DataTable
                        Dim dnuevoOcoNumero As DataTable
                        dtPeriodo = New DataTable
                        dnuevoOcoCodigo = New DataTable
                        dnuevoOcoNumero = New DataTable
                        eCompraRequerimientos = New clsCompraRequerimientos
                        dtPeriodo = eCompraRequerimientos.fListar(4, emprOrden, "", "", gPeriodo)
                        If dtPeriodo.Rows.Count > 0 Then
                            sPeriodo = dtPeriodo.Rows(0).Item("PdoCodigo")
                            sPeriodoAnio = dtPeriodo.Rows(0).Item("PdoDescripcion")
                        End If
                        dnuevoOcoCodigo = eCompraRequerimientos.fListar(5, emprOrden, "", "", sPeriodoAnio)
                        If dnuevoOcoCodigo.Rows.Count > 0 Then
                            snuevoOcoCodigo = dnuevoOcoCodigo.Rows(0).Item(0)
                        End If
                        dnuevoOcoNumero = eCompraRequerimientos.fListar(6, emprOrden, "", "", sPeriodoAnio)
                        If dnuevoOcoNumero.Rows.Count > 0 Then
                            snuevoOcoNumero = dnuevoOcoNumero.Rows(0).Item(0)
                        End If
                        'crea cab 
                        Dim iResultado As Integer = 0

                        If registrarDocumentos(gEmpresa, RucDoc, (NroDoc.Substring(0, InStr(NroDoc, "-") - 1)), (NroDoc.Substring(InStr(NroDoc, "-"))), FechaFac, txtImporteTotal.Text, sIdArea, gUsuario, gPC, snuevoOcoCodigo, CodCC, gPeriodo) = False Then
                            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
                            Exit Sub
                        End If


                        iResultado = eCompraRequerimientos.fGrabarOrden(8, snuevoOcoCodigo, snuevoOcoNumero, "CCHICA", sPeriodo, sPeriodoAnio, emprOrden, FechaFac, xCCosCodigo, "", "", RazonDoc, RucDoc, "", "", "", "", "", FechaFac, "", "", dTipoCambio, MonedaDoc, "", 0, 0, "", "", 0, 0, 0, 0, gUsuario, CodReq, "", "", 0, 0, "", 0, 0, 0, MonedaDoc, 0, dIgv, 0)



                        'crea det
                        CodCCAnterior = CodCC
                        Dim iResultadoDet As Integer = 0
                        iResultadoDet = eCompraRequerimientos.fGrabarOrdenDet(9, snuevoOcoCodigo, snuevoOcoNumero, "CCHICA", sPeriodo, sPeriodoAnio, emprOrden, FechaFac, "", "", "", RazonDoc, RucDoc, "", "", "", "", "", FechaFac, "", "", dTipoCambio, MonedaDoc, "", 0, 1, "", "", 0, 0, 0, 0, gUsuario, CodReq, "", "", 0, 0, "", 0, 0, 0, MonedaDoc, 0, 0, 0, "", "", xLinCodigo, "", xSubCodigo, "", xTipCodigo, "", xProCodigo, xProNombreComercial2x, "", xUmdDescripcion, xEvlDescripcion, xCantGrupalPorComprarUni, xxCantCompr, xCantGrupalPorComprarGrupo, 0, xDocoPrecioGrupal, 0, xDocoPrecioGrupal, xDocoPrecioGrupal, xDocoPrecioGrupal, 0, xCCosCodigo, "", "", "", 0, xDrqAgregado2x, "", 0, "", "", "", "", "", xDrqCodigo, IdCaja, IdDetCaja, gEmpresa)
                    ElseIf emprOrdenAnterior = emprOrden Then
                        emprOrdenAnterior = emprOrden
                        If CodCCAnterior <> CodCC Then
                            'crea cab 
                            Dim iResultado As Integer = 0
                            Dim dnuevoOcoCodigo As DataTable
                            Dim dnuevoOcoNumero As DataTable
                            dnuevoOcoCodigo = New DataTable
                            dnuevoOcoNumero = New DataTable
                            eCompraRequerimientos = New clsCompraRequerimientos
                            dnuevoOcoCodigo = eCompraRequerimientos.fListar(5, emprOrden, "", "", sPeriodoAnio)
                            If dnuevoOcoCodigo.Rows.Count > 0 Then
                                snuevoOcoCodigo = dnuevoOcoCodigo.Rows(0).Item(0)
                            End If
                            dnuevoOcoNumero = eCompraRequerimientos.fListar(6, emprOrden, "", "", sPeriodoAnio)
                            If dnuevoOcoNumero.Rows.Count > 0 Then
                                snuevoOcoNumero = dnuevoOcoNumero.Rows(0).Item(0)
                            End If
                            iResultado = eCompraRequerimientos.fGrabarOrden(8, snuevoOcoCodigo, snuevoOcoNumero, "CCHICA", sPeriodo, sPeriodoAnio, emprOrden, FechaFac, xCCosCodigo, "", "", RazonDoc, RucDoc, "", "", "", "", "", FechaFac, "", "", dTipoCambio, MonedaDoc, "", 0, 2, "", "", 0, 0, 0, 0, gUsuario, CodReq, "", "", 0, 0, "", 0, 0, 0, MonedaDoc, 0, dIgv, 0)
                            'crea det
                            CodCCAnterior = CodCC
                            Dim iResultadoDet As Integer = 0
                            iResultadoDet = eCompraRequerimientos.fGrabarOrdenDet(9, snuevoOcoCodigo, snuevoOcoNumero, "CCHICA", sPeriodo, sPeriodoAnio, emprOrden, FechaFac, "", "", "", RazonDoc, RucDoc, "", "", "", "", "", FechaFac, "", "", dTipoCambio, MonedaDoc, "", 0, 1, "", "", 0, 0, 0, 0, gUsuario, CodReq, "", "", 0, 0, "", 0, 0, 0, MonedaDoc, 0, 0, 0, "", "", xLinCodigo, "", xSubCodigo, "", xTipCodigo, "", xProCodigo, xProNombreComercial2x, "", xUmdDescripcion, xEvlDescripcion, xCantGrupalPorComprarUni, xxCantCompr, xCantGrupalPorComprarGrupo, 0, xDocoPrecioGrupal, 0, xDocoPrecioGrupal, xDocoPrecioGrupal, xDocoPrecioGrupal, 0, xCCosCodigo, "", "", "", 0, xDrqAgregado2x, "", 0, "", "", "", "", "", xDrqCodigo, IdCaja, IdDetCaja, gEmpresa)
                        ElseIf CodCCAnterior = CodCC Then
                            'crea det nomas
                            CodCCAnterior = CodCC
                            Dim iResultadoDet As Integer = 0
                            iResultadoDet = eCompraRequerimientos.fGrabarOrdenDet(9, snuevoOcoCodigo, snuevoOcoNumero, "CCHICA", sPeriodo, sPeriodoAnio, emprOrden, FechaFac, "", "", "", RazonDoc, RucDoc, "", "", "", "", "", FechaFac, "", "", dTipoCambio, MonedaDoc, "", 0, 1, "", "", 0, 0, 0, 0, gUsuario, CodReq, "", "", 0, 0, "", 0, 0, 0, MonedaDoc, 0, 0, 0, "", "", xLinCodigo, "", xSubCodigo, "", xTipCodigo, "", xProCodigo, xProNombreComercial2x, "", xUmdDescripcion, xEvlDescripcion, xCantGrupalPorComprarUni, xxCantCompr, xCantGrupalPorComprarGrupo, 0, xDocoPrecioGrupal, 0, xDocoPrecioGrupal, xDocoPrecioGrupal, xDocoPrecioGrupal, 0, xCCosCodigo, "", "", "", 0, xDrqAgregado2x, "", 0, "", "", "", "", "", xDrqCodigo, IdCaja, IdDetCaja, gEmpresa)
                        End If
                    End If

                Next
                MessageBox.Show("Se ha confirmado la compra con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Hide()
            End If
        End If

    End Sub
#Region "Procesos"
    Private Function registrarDocumentos(ByVal vlEmpresa As String, ByVal vlRucDoc As String, ByVal vlSerie As String, ByVal vlNroDoc As String, ByVal vlFechaFac As String, ByVal vlImporte As String, ByVal vlIdArea As String, ByVal vlUsuario As String, ByVal vlPC As String, ByVal vlOcoCodigo As String, ByVal vlCCosCodigo As String, ByVal vlgPeriodo As String) As Boolean

        Dim VL_BeanResultado As New CapaEntidad.BeanResultado.ResultadoTransaccion()

        obj_beanregistroDocumento = New BeanRegistroDocumento(vlEmpresa, vlRucDoc, vlSerie, vlNroDoc, vlFechaFac, vlImporte, vlIdArea, vlUsuario, vlPC, vlOcoCodigo, vlCCosCodigo, vlgPeriodo)

        VL_BeanResultado = obj_SrvRegistroDocumento.Fnc_Registrar_Documento(obj_beanregistroDocumento)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")

        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")

        End If
        Return VL_BeanResultado.blnResultado
    End Function

#End Region

End Class