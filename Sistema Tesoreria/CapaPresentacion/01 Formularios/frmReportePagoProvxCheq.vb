Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReportePagoProvxCheq

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    Dim rptPagosProvyCheq As New rptPagosProvyCheq
    'Dim rptPagosProvxCC As New rptPagosProvxCC
    'Dim rptPagosFacturasxSemana As New rptPagosFacturasxSemana
    'Dim rptPagosProvxBancos As New rptPagosProvxCuenta
    Private eGastosGenerales As clsGastosGenerales

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReportePagoProvxCheq = Nothing
    Public Shared Function Instance() As frmReportePagoProvxCheq
        If frmInstance Is Nothing Then
            frmInstance = New frmReportePagoProvxCheq
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReportePagoProvxCheq_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReportePagoProvxCheq_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        cboA�o.DropDownStyle = ComboBoxStyle.DropDownList
        cboSemana.DropDownStyle = ComboBoxStyle.DropDownList
        cboMoneda.DropDownStyle = ComboBoxStyle.DropDownList
        cboCategoria.DropDownStyle = ComboBoxStyle.DropDownList

        cboA�o.Text = Year(Now)

        eGastosGenerales = New clsGastosGenerales
        cboMoneda.DataSource = eGastosGenerales.fListarMonedas() 'eGastosGenerales.fListarCronogramas5(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = 0
        End If

        eGastosGenerales = New clsGastosGenerales
        cboCategoria.DataSource = eGastosGenerales.fListarCategoria() 'eGastosGenerales.fListarCronogramas5(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboCategoria.ValueMember = "IdGrupo"
            cboCategoria.DisplayMember = "DesGrupo"
            cboCategoria.SelectedIndex = 0
        End If


    End Sub


    Private Sub cboA�o_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboA�o.SelectedIndexChanged
        eGastosGenerales = New clsGastosGenerales
        'Dim dtTable As DataTable
        'dtTable = New DataTable
        cboSemana.DataSource = eGastosGenerales.fListarSemanasdePago(gEmpresa, Trim(cboA�o.Text)) 'eGastosGenerales.fListarCronogramas5(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboSemana.ValueMember = "NroSemana"
            cboSemana.DisplayMember = "NroSemana"
            cboSemana.SelectedIndex = 0
        End If

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cboA�o.Text = "" Then
            MessageBox.Show("Seleccione un A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If cboSemana.Text = "" Then
            MessageBox.Show("Seleccione un N�mero de Semana", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If cboMoneda.Text = "" Then
            MessageBox.Show("Seleccione un Tipo de Moneda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If cboCategoria.Text = "" Then
            MessageBox.Show("Seleccione una Categoria", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If


        rptPagosProvyCheq.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '1
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@NroSemana")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(cboSemana.SelectedValue)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '3
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(cboA�o.Text)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Moneda")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(cboMoneda.SelectedValue)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '5
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Grupo")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(cboCategoria.SelectedValue)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '6
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@GrupoCadena")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(cboCategoria.Text)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


        '7
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gDesEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '8
        crParameterFieldDefinitions = rptPagosProvyCheq.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmprRuc
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '-------------------------------------------------------------------------------------------
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = rptPagosProvyCheq


    End Sub
End Class