<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuentaBanco
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvLista = New ctrLibreria.Controles.BeDataGridView
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.chkAnexos = New System.Windows.Forms.CheckBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.dgBancos = New System.Windows.Forms.DataGridView
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboTipoCuenta = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cboTipCtaBanco = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.cboCtaContable = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDesMoneda = New ctrLibreria.Controles.BeTextBox
        Me.txtMoneda = New ctrLibreria.Controles.BeTextBox
        Me.txtDesBanco = New ctrLibreria.Controles.BeTextBox
        Me.txtCodigoBanco = New ctrLibreria.Controles.BeTextBox
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.cboEstado = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.txtObservacion = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.txtNumeroCuenta = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.chkLibro = New System.Windows.Forms.CheckBox
        Me.stsTotales = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.cboMonedas = New ctrLibreria.Controles.BeComboBox
        Me.txtNumero = New ctrLibreria.Controles.BeTextBox
        Me.cboBancos = New ctrLibreria.Controles.BeComboBox
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgBancos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.stsTotales.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(1032, 457)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Size = New System.Drawing.Size(1024, 431)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Size = New System.Drawing.Size(1024, 431)
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, Me.Column1, Me.Estado, Me.Column2, Me.Column3, Me.Column11, Me.Column12, Me.Column13})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLista.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-1, -1)
        Me.dgvLista.MultiSelect = False
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLista.Size = New System.Drawing.Size(3412, 402)
        Me.dgvLista.TabIndex = 0
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "IdCuenta"
        Me.Codigo.HeaderText = "C�digo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 130
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Banco"
        Me.Descripcion.HeaderText = "Banco"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 200
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "NumeroCuenta"
        Me.Column1.HeaderText = "Numero Cuenta"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 150
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Moneda"
        Me.Estado.HeaderText = "Moneda"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        Me.Estado.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "Observacion"
        Me.Column2.HeaderText = "Observaci�n"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 200
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Estados"
        Me.Column3.HeaderText = "Estado"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 80
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "EstadosLibro"
        Me.Column11.HeaderText = "Libro"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 80
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "TipoCuenta"
        Me.Column12.HeaderText = "TipoCuenta"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Visible = False
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "EstadosCargo"
        Me.Column13.HeaderText = "Cargo"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 60
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.BeLabel14)
        Me.Panel1.Controls.Add(Me.chkAnexos)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.cboTipoCuenta)
        Me.Panel1.Controls.Add(Me.BeLabel13)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.txtDesMoneda)
        Me.Panel1.Controls.Add(Me.txtMoneda)
        Me.Panel1.Controls.Add(Me.txtDesBanco)
        Me.Panel1.Controls.Add(Me.txtCodigoBanco)
        Me.Panel1.Controls.Add(Me.txtCodigo)
        Me.Panel1.Controls.Add(Me.cboEstado)
        Me.Panel1.Controls.Add(Me.BeLabel7)
        Me.Panel1.Controls.Add(Me.txtObservacion)
        Me.Panel1.Controls.Add(Me.BeLabel6)
        Me.Panel1.Controls.Add(Me.txtNumeroCuenta)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Controls.Add(Me.BeLabel5)
        Me.Panel1.Controls.Add(Me.BeLabel2)
        Me.Panel1.Controls.Add(Me.BeLabel1)
        Me.Panel1.Controls.Add(Me.chkLibro)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1016, 424)
        Me.Panel1.TabIndex = 0
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(439, 38)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(280, 13)
        Me.BeLabel14.TabIndex = 25
        Me.BeLabel14.Text = "(Doble Click o Enter para visualizar los Bancos)"
        '
        'chkAnexos
        '
        Me.chkAnexos.AutoSize = True
        Me.chkAnexos.Location = New System.Drawing.Point(128, 324)
        Me.chkAnexos.Name = "chkAnexos"
        Me.chkAnexos.Size = New System.Drawing.Size(197, 17)
        Me.chkAnexos.TabIndex = 24
        Me.chkAnexos.Text = "Asignar para Resumenes de Anexos"
        Me.chkAnexos.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.dgBancos)
        Me.Panel3.Location = New System.Drawing.Point(183, 56)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(634, 201)
        Me.Panel3.TabIndex = 18
        Me.Panel3.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(611, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "X"
        '
        'dgBancos
        '
        Me.dgBancos.AllowUserToAddRows = False
        Me.dgBancos.BackgroundColor = System.Drawing.Color.White
        Me.dgBancos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgBancos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10})
        Me.dgBancos.EnableHeadersVisualStyles = False
        Me.dgBancos.Location = New System.Drawing.Point(6, 18)
        Me.dgBancos.Name = "dgBancos"
        Me.dgBancos.RowHeadersVisible = False
        Me.dgBancos.Size = New System.Drawing.Size(623, 175)
        Me.dgBancos.TabIndex = 0
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "TipoAnaliticoCodigo"
        Me.Column4.HeaderText = "Anexo"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "IdBanco"
        Me.Column5.HeaderText = "Codigo"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "NombreBanco"
        Me.Column6.HeaderText = "Nombre"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 200
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "Abreviado"
        Me.Column7.HeaderText = "Abrev."
        Me.Column7.Name = "Column7"
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "MonAbreviado"
        Me.Column8.HeaderText = "Moneda"
        Me.Column8.Name = "Column8"
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "MonCodigo"
        Me.Column9.HeaderText = "MonCodigo"
        Me.Column9.Name = "Column9"
        Me.Column9.Visible = False
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "MonDescripcion"
        Me.Column10.HeaderText = "MonDescripcion"
        Me.Column10.Name = "Column10"
        Me.Column10.Visible = False
        '
        'cboTipoCuenta
        '
        Me.cboTipoCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboTipoCuenta.FormattingEnabled = True
        Me.cboTipoCuenta.KeyEnter = True
        Me.cboTipoCuenta.Location = New System.Drawing.Point(129, 218)
        Me.cboTipoCuenta.Name = "cboTipoCuenta"
        Me.cboTipoCuenta.Size = New System.Drawing.Size(221, 21)
        Me.cboTipoCuenta.TabIndex = 23
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(17, 226)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(65, 13)
        Me.BeLabel13.TabIndex = 22
        Me.BeLabel13.Text = "Tipo Cuenta"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboTipCtaBanco)
        Me.GroupBox2.Controls.Add(Me.BeLabel11)
        Me.GroupBox2.Controls.Add(Me.cboCtaContable)
        Me.GroupBox2.Controls.Add(Me.BeLabel12)
        Me.GroupBox2.Location = New System.Drawing.Point(117, 136)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(506, 64)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos Contables"
        '
        'cboTipCtaBanco
        '
        Me.cboTipCtaBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipCtaBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboTipCtaBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipCtaBanco.ForeColor = System.Drawing.Color.Black
        Me.cboTipCtaBanco.FormattingEnabled = True
        Me.cboTipCtaBanco.KeyEnter = True
        Me.cboTipCtaBanco.Location = New System.Drawing.Point(12, 37)
        Me.cboTipCtaBanco.Name = "cboTipCtaBanco"
        Me.cboTipCtaBanco.Size = New System.Drawing.Size(221, 21)
        Me.cboTipCtaBanco.TabIndex = 13
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(6, 16)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(65, 13)
        Me.BeLabel11.TabIndex = 12
        Me.BeLabel11.Text = "Tipo Cuenta"
        '
        'cboCtaContable
        '
        Me.cboCtaContable.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCtaContable.BackColor = System.Drawing.Color.Ivory
        Me.cboCtaContable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCtaContable.ForeColor = System.Drawing.Color.Black
        Me.cboCtaContable.FormattingEnabled = True
        Me.cboCtaContable.KeyEnter = True
        Me.cboCtaContable.Location = New System.Drawing.Point(239, 37)
        Me.cboCtaContable.Name = "cboCtaContable"
        Me.cboCtaContable.Size = New System.Drawing.Size(248, 21)
        Me.cboCtaContable.TabIndex = 15
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(236, 21)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(71, 13)
        Me.BeLabel12.TabIndex = 14
        Me.BeLabel12.Text = "Cta. Contable"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 282)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Estado Libro Bancos"
        '
        'txtDesMoneda
        '
        Me.txtDesMoneda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDesMoneda.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtDesMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDesMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDesMoneda.Enabled = False
        Me.txtDesMoneda.ForeColor = System.Drawing.Color.Black
        Me.txtDesMoneda.KeyEnter = True
        Me.txtDesMoneda.Location = New System.Drawing.Point(183, 83)
        Me.txtDesMoneda.Name = "txtDesMoneda"
        Me.txtDesMoneda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDesMoneda.ShortcutsEnabled = False
        Me.txtDesMoneda.Size = New System.Drawing.Size(166, 20)
        Me.txtDesMoneda.TabIndex = 9
        Me.txtDesMoneda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtMoneda
        '
        Me.txtMoneda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMoneda.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.Enabled = False
        Me.txtMoneda.ForeColor = System.Drawing.Color.Black
        Me.txtMoneda.KeyEnter = True
        Me.txtMoneda.Location = New System.Drawing.Point(129, 83)
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMoneda.ShortcutsEnabled = False
        Me.txtMoneda.Size = New System.Drawing.Size(48, 20)
        Me.txtMoneda.TabIndex = 8
        Me.txtMoneda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtDesBanco
        '
        Me.txtDesBanco.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDesBanco.BackColor = System.Drawing.Color.Ivory
        Me.txtDesBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDesBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDesBanco.ForeColor = System.Drawing.Color.Black
        Me.txtDesBanco.KeyEnter = True
        Me.txtDesBanco.Location = New System.Drawing.Point(183, 31)
        Me.txtDesBanco.Name = "txtDesBanco"
        Me.txtDesBanco.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDesBanco.ShortcutsEnabled = False
        Me.txtDesBanco.Size = New System.Drawing.Size(250, 20)
        Me.txtDesBanco.TabIndex = 4
        Me.txtDesBanco.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCodigoBanco
        '
        Me.txtCodigoBanco.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigoBanco.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigoBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoBanco.Enabled = False
        Me.txtCodigoBanco.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoBanco.KeyEnter = True
        Me.txtCodigoBanco.Location = New System.Drawing.Point(129, 31)
        Me.txtCodigoBanco.Name = "txtCodigoBanco"
        Me.txtCodigoBanco.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigoBanco.ShortcutsEnabled = False
        Me.txtCodigoBanco.Size = New System.Drawing.Size(48, 20)
        Me.txtCodigoBanco.TabIndex = 3
        Me.txtCodigoBanco.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(129, 7)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(133, 20)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboEstado
        '
        Me.cboEstado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstado.BackColor = System.Drawing.Color.Ivory
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.ForeColor = System.Drawing.Color.Black
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.KeyEnter = True
        Me.cboEstado.Location = New System.Drawing.Point(129, 245)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(133, 21)
        Me.cboEstado.TabIndex = 17
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(18, 253)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel7.TabIndex = 16
        Me.BeLabel7.Text = "Estado"
        '
        'txtObservacion
        '
        Me.txtObservacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtObservacion.BackColor = System.Drawing.Color.Ivory
        Me.txtObservacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.ForeColor = System.Drawing.Color.Black
        Me.txtObservacion.KeyEnter = True
        Me.txtObservacion.Location = New System.Drawing.Point(129, 110)
        Me.txtObservacion.MaxLength = 1000
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtObservacion.ShortcutsEnabled = False
        Me.txtObservacion.Size = New System.Drawing.Size(576, 20)
        Me.txtObservacion.TabIndex = 11
        Me.txtObservacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(18, 117)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel6.TabIndex = 10
        Me.BeLabel6.Text = "Observaci�n"
        '
        'txtNumeroCuenta
        '
        Me.txtNumeroCuenta.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumeroCuenta.BackColor = System.Drawing.Color.Ivory
        Me.txtNumeroCuenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumeroCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroCuenta.ForeColor = System.Drawing.Color.Black
        Me.txtNumeroCuenta.KeyEnter = True
        Me.txtNumeroCuenta.Location = New System.Drawing.Point(129, 57)
        Me.txtNumeroCuenta.MaxLength = 50
        Me.txtNumeroCuenta.Name = "txtNumeroCuenta"
        Me.txtNumeroCuenta.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumeroCuenta.ShortcutsEnabled = False
        Me.txtNumeroCuenta.Size = New System.Drawing.Size(220, 20)
        Me.txtNumeroCuenta.TabIndex = 6
        Me.txtNumeroCuenta.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(17, 64)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(97, 13)
        Me.BeLabel4.TabIndex = 5
        Me.BeLabel4.Text = "N�mero Cuenta"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(17, 92)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel5.TabIndex = 7
        Me.BeLabel5.Text = "Moneda"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(18, 40)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel2.TabIndex = 2
        Me.BeLabel2.Text = "Banco"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(18, 14)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "C�digo"
        '
        'chkLibro
        '
        Me.chkLibro.AutoSize = True
        Me.chkLibro.Location = New System.Drawing.Point(128, 282)
        Me.chkLibro.Name = "chkLibro"
        Me.chkLibro.Size = New System.Drawing.Size(254, 17)
        Me.chkLibro.TabIndex = 19
        Me.chkLibro.Text = "Activado ""ACTIVO"" / Desactivado ""INACTIVO"""
        Me.chkLibro.UseVisualStyleBackColor = True
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsTotales.Location = New System.Drawing.Point(3, 404)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(1016, 22)
        Me.stsTotales.TabIndex = 2
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(115, 17)
        Me.ToolStripStatusLabel1.Text = "Total de Registros= 0"
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.BeLabel10)
        Me.Panel2.Controls.Add(Me.GroupBox1)
        Me.Panel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(354, 151)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(370, 130)
        Me.Panel2.TabIndex = 1
        Me.Panel2.Visible = False
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Red
        Me.BeLabel10.Location = New System.Drawing.Point(354, -2)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(18, 16)
        Me.BeLabel10.TabIndex = 0
        Me.BeLabel10.Text = "X"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BeLabel9)
        Me.GroupBox1.Controls.Add(Me.BeLabel8)
        Me.GroupBox1.Controls.Add(Me.BeLabel3)
        Me.GroupBox1.Controls.Add(Me.cboMonedas)
        Me.GroupBox1.Controls.Add(Me.txtNumero)
        Me.GroupBox1.Controls.Add(Me.cboBancos)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(340, 114)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(6, 86)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel9.TabIndex = 2
        Me.BeLabel9.Text = "Moneda"
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(6, 25)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(97, 13)
        Me.BeLabel8.TabIndex = 4
        Me.BeLabel8.Text = "N�mero Cuenta"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(6, 57)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel3.TabIndex = 0
        Me.BeLabel3.Text = "Banco"
        '
        'cboMonedas
        '
        Me.cboMonedas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMonedas.BackColor = System.Drawing.Color.Ivory
        Me.cboMonedas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonedas.ForeColor = System.Drawing.Color.Black
        Me.cboMonedas.FormattingEnabled = True
        Me.cboMonedas.KeyEnter = True
        Me.cboMonedas.Location = New System.Drawing.Point(104, 80)
        Me.cboMonedas.Name = "cboMonedas"
        Me.cboMonedas.Size = New System.Drawing.Size(216, 21)
        Me.cboMonedas.TabIndex = 3
        '
        'txtNumero
        '
        Me.txtNumero.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumero.BackColor = System.Drawing.Color.Ivory
        Me.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumero.ForeColor = System.Drawing.Color.Black
        Me.txtNumero.KeyEnter = True
        Me.txtNumero.Location = New System.Drawing.Point(104, 20)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumero.ShortcutsEnabled = False
        Me.txtNumero.Size = New System.Drawing.Size(216, 21)
        Me.txtNumero.TabIndex = 5
        Me.txtNumero.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboBancos
        '
        Me.cboBancos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBancos.BackColor = System.Drawing.Color.Ivory
        Me.cboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBancos.ForeColor = System.Drawing.Color.Black
        Me.cboBancos.FormattingEnabled = True
        Me.cboBancos.KeyEnter = True
        Me.cboBancos.Location = New System.Drawing.Point(104, 51)
        Me.cboBancos.Name = "cboBancos"
        Me.cboBancos.Size = New System.Drawing.Size(216, 21)
        Me.cboBancos.TabIndex = 1
        '
        'frmCuentaBanco
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1032, 457)
        Me.MaximizeBox = False
        Me.Name = "frmCuentaBanco"
        Me.Text = "Cuentas de Banco"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgBancos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtObservacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumeroCuenta As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNumero As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboBancos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboMonedas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dgBancos As System.Windows.Forms.DataGridView
    Friend WithEvents txtCodigoBanco As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDesBanco As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMoneda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCtaContable As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboTipCtaBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtDesMoneda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkLibro As System.Windows.Forms.CheckBox
    Friend WithEvents cboTipoCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkAnexos As System.Windows.Forms.CheckBox
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel

End Class
