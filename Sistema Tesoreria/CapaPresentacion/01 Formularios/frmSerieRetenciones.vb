Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmSerieRetenciones
    Dim WithEvents cmr As CurrencyManager
    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Private eSeries As clsSeries

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmSerieRetenciones = Nothing
    Public Shared Function Instance() As frmSerieRetenciones
        If frmInstance Is Nothing Then
            frmInstance = New frmSerieRetenciones
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmSerieRetenciones_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmSerieRetenciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'cboEstado.DropDownStyle = ComboBoxStyle.DropDownList
        eSeries = New clsSeries
        mMostrarGrilla()
        eTempo = New clsPlantTempo
        'cboEstado.DataSource = eTempo.fColEstado
        'cboEstado.ValueMember = "Col02"
        'cboEstado.DisplayMember = "Col01"
        'cboEstado.SelectedIndex = -1
        dgvLista.Width = 529
        dgvLista.Height = 231

        For x As Integer = 0 To dgvLista.Columns.Count - 1
            'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
            dgvLista.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
        Next

    End Sub

    Private Sub mMostrarGrilla()
        eSeries = New clsSeries
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSeries.fListar(2, gEmpresa, "")
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTable
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eSeries.iNroRegistros
        If eSeries.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
        Timer1.Enabled = True

        Pintar()

    End Sub

    Private Sub Pintar()
        If dgvLista.Rows.Count > 0 Then
            For x As Integer = 0 To dgvLista.Rows.Count - 1
                If Trim(dgvLista.Rows(x).Cells("Column5").Value) = "Activo" Then
                    gestionaResaltados(dgvLista, x, Color.Aqua)
                ElseIf Trim(dgvLista.Rows(x).Cells("Column5").Value) = "Inactivo" Then
                    'gestionaResaltados(dgvLista, x, Color.Red)
                End If
            Next
        End If
    End Sub
    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        'If sTab = 0 Then
        iOpcion = 0
        sTab = 1
        LimpiarControles()
        HabilitarControles()
        'cboEstado.SelectedIndex = 0
        'cboEstado.Enabled = False
        eSeries = New clsSeries
        eSeries.fCodigo(1, gEmpresa)
        txtCodigo.Text = eSeries.sCodFuturo
        txtCodigo.Enabled = False
        'txtSerie.Focus()

        eSeries = New clsSeries
        eSeries.fNroSerieNew(8, gEmpresa)
        txtSerie.Text = eSeries.sCodFuturoNroSerie
        'txtCodigo.Enabled = False

        Timer1.Enabled = True
        'End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtSerie.Focus()
        Timer1.Enabled = False
    End Sub
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub
    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtSerie.Clear()
        txtNroIni.Clear()
    End Sub

    Private Sub DesabilitarControles()
        txtSerie.Enabled = False
        'cboEstado.Enabled = False
        txtNroIni.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        txtSerie.Enabled = True
        'cboEstado.Enabled = True
        txtNroIni.Enabled = True
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        'If sTab = 0 Then
        sTab = 1
        iOpcion = 6
        VerPosicion()
        HabilitarControles()
        'cboEstado.Enabled = False
        txtCodigo.Enabled = False
        txtSerie.Enabled = False
        txtNroIni.Enabled = True
        'End If
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdSerie").ToString
                Me.txtSerie.Text = Fila("Serie").ToString
                Me.txtNroIni.Text = Fila("NumeroIni").ToString
                'If Fila("Estado").ToString = 0 Then
                '    Me.cboEstado.SelectedIndex = 0
                'Else
                '    Me.cboEstado.SelectedIndex = 1
                'End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""

        eTempo = New clsPlantTempo
        eSeries = New clsSeries
        If sTab = 1 Then
            If Len(Trim(txtCodigo.Text)) > 0 And Len(Trim(txtSerie.Text)) > 0 And Len(Trim(txtNroIni.Text)) > 0 Then
                If iOpcion = 0 Then
                    Mensaje = "Grabar"
                    Dim iDuplicado As Int32
                    eSeries = New clsSeries
                    iDuplicado = eSeries.fBuscarDoble(3, Trim(txtSerie.Text), gEmpresa)
                    If iDuplicado >= 1 Then
                        MessageBox.Show("El N�mero de Serie " & Trim(txtSerie.Text) & " ya Existe, Ingrese Otro.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtSerie.Focus()
                        Exit Sub
                    End If
                ElseIf iOpcion = 6 Then
                    Mensaje = "Actualizar"
                    Dim iTotRetConSerie As Int32
                    eSeries = New clsSeries
                    iTotRetConSerie = eSeries.fBuscarDoble(7, Trim(txtSerie.Text), gEmpresa)
                    If iTotRetConSerie >= 1 Then
                        MessageBox.Show("No puede cambiar los datos de este n�mero de Serie (" & Trim(txtSerie.Text) & ") ya que cuenta con Retenciones creadas.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtSerie.Focus()
                        Exit Sub
                    End If

                    'Dim iDuplicado As Int32
                    'eSeries = New clsSeries
                    'iDuplicado = eSeries.fBuscarDoble(3, Trim(txtSerie.Text), gEmpresa)
                    'If iDuplicado >= 1 Then
                    '    MessageBox.Show("El N�mero de Serie " & Trim(txtSerie.Text) & " ya Existe, Ingrese Otro.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    '    txtSerie.Focus()
                    '    Exit Sub
                    'End If

                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 0 Then
                        eSeries = New clsSeries
                        eSeries.fCodigo(1, gEmpresa)
                        txtCodigo.Text = eSeries.sCodFuturo
                        sCodigoRegistro = eSeries.sCodFuturo
                        iResultado = eSeries.fGrabar(iOpcion, sCodigoRegistro, gEmpresa, Convert.ToString(txtSerie.Text.Trim), Convert.ToString(txtNroIni.Text.Trim), 1, gUsuario)
                        If iResultado > 0 Then
                            mMostrarGrilla()
                        End If
                        sTab = 0
                    ElseIf iOpcion = 6 Then
                        sCodigoRegistro = Trim(txtCodigo.Text)
                        iResultado = eSeries.fGrabar(iOpcion, sCodigoRegistro, gEmpresa, Convert.ToString(txtSerie.Text.Trim), Convert.ToString(txtNroIni.Text.Trim), 1, gUsuario)
                        If iResultado > 0 Then
                            mMostrarGrilla()
                        End If
                        sTab = 0
                    End If
                End If
            Else
                If Len(Trim(txtSerie.Text)) = 0 Then
                    MessageBox.Show("Ingrese el N�mero de Serie", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtSerie.Focus()
                    Exit Sub
                End If
                If Len(Trim(txtNroIni.Text)) = 0 Then
                    MessageBox.Show("Ingrese el N�mero Inicial de la Serie " & Trim(txtSerie.Text), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNroIni.Focus()
                    Exit Sub
                End If
                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        'If sTab = 0 Then
        sTab = 1
        VerPosicion()
        DesabilitarControles()
        'End If
    End Sub

    Private Sub btnPermisos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPermisos.Click
        Dim ContaActivar As Integer = 0
        Dim pIdSerie As String = ""
        Dim NumSerie As String = ""
        Dim NumSerieInicio As String = ""
        If dgvLista.Rows.Count > 0 Then

            For z As Integer = 0 To dgvLista.Rows.Count - 1
                If Convert.ToBoolean(dgvLista.Rows(z).Cells("Activar").Value) = True Then
                    ContaActivar = ContaActivar + 1
                    pIdSerie = Trim(dgvLista.Rows(z).Cells("IdSerie").Value)
                    NumSerie = Trim(dgvLista.Rows(z).Cells("cSerie").Value)
                    NumSerieInicio = Trim(dgvLista.Rows(z).Cells("NumeroIni").Value)
                End If
            Next

            If ContaActivar = 1 Then
                If (MessageBox.Show("�Esta seguro de Activar la Serie N� " & NumSerie & " ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    Dim iResultado As Integer = 0
                    iResultado = eSeries.fGrabar(4, pIdSerie, gEmpresa, "", "", 0, gUsuario)
                    If iResultado > 0 Then
                        gSerie = Trim(NumSerie)
                        gNumeroInicioSerie = Trim(NumSerieInicio)
                        mMostrarGrilla()
                    End If
                    sTab = 0
                End If
            ElseIf ContaActivar > 1 Then
                MessageBox.Show("Seleccione Solo una Serie Para Activar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ElseIf ContaActivar = 0 Then
                MessageBox.Show("Seleccione una Serie Para Activar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub

    Private Sub dgvLista_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLista.CellContentClick

    End Sub

    Private Sub dgvLista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLista.Click
        Pintar()
    End Sub

    Private Sub btnCCostos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCCostos.Click
        If dgvLista.Rows.Count > 0 Then
            'If Me.dgvLista.RowCount > 0 Then
            Dim x As frmSerieRetencionesCCostos = frmSerieRetencionesCCostos.Instance
            'VerPosicion()
            'x.strCodigoGrupo = Trim(Fila("IdGrupo").ToString)
            'x.strDesGrupo = Trim(Fila("DesGrupo").ToString)
            'x.strCodigoGrupoGeneral = Trim(cboCategoria.SelectedValue)
            'x.strNombreUsuario = Trim(Fila("UsuCodigo").ToString)

            'sIdMovimiento = IIf(Microsoft.VisualBasic.IsDBNull(dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("Column9").Value), "", dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("Column9").Value)

            x.strCodigoSerie = IIf(Microsoft.VisualBasic.IsDBNull(dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("IdSerie").Value), "", dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("IdSerie").Value)
            x.strDesSerie = IIf(Microsoft.VisualBasic.IsDBNull(dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("cSerie").Value), "", dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("cSerie").Value)

            x.Owner = Me
            x.ShowInTaskbar = False
            x.ShowDialog()
        End If
      
        'End If
    End Sub
End Class
