Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReporteAdministracion2

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Private eGastosGenerales As clsGastosGenerales
    Private ePagoProveedores As clsPagoProveedores

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAdministracion2 = Nothing
    Public Shared Function Instance() As frmReporteAdministracion2
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteAdministracion2
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteAdministracion2_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Dim dTipoCambio As Double = 0

    Private Sub frmReporteAdministracion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboA�o.Text = Year(Now)
        cboA�o.DropDownStyle = ComboBoxStyle.DropDownList
        cboCategoriaGastosGenerales.DropDownStyle = ComboBoxStyle.DropDownList
        cboMes.DropDownStyle = ComboBoxStyle.DropDownList
        cboMoneda.DropDownStyle = ComboBoxStyle.DropDownList
        Dim pMes As String = Format(Month(Now), "00")
        cboMes.Text = TraerNombreMes(pMes)
        eGastosGenerales = New clsGastosGenerales
        cboCategoriaGastosGenerales.DataSource = eGastosGenerales.CategoriaGastosGenerales() 'eGastosGenerales.fListarCronogramas5(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboCategoriaGastosGenerales.ValueMember = "IdGastosGenerales"
            cboCategoriaGastosGenerales.DisplayMember = "DescGastosGenerales"
            cboCategoriaGastosGenerales.SelectedIndex = 0
        End If
        eGastosGenerales = New clsGastosGenerales
        cboMoneda.DataSource = eGastosGenerales.fListarMonedas() 'eGastosGenerales.fListarCronogramas5(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = 0
        End If
        rdbRes.Checked = True
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Trim(cboCategoriaGastosGenerales.Text) = "" Then
            MessageBox.Show("Seleccione una Categoria", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboCategoriaGastosGenerales.Focus()
            Exit Sub
            'afiliacion de visa claro interbank cuanto cuesta?
        End If
        If Trim(cboMes.Text) = "" Then
            MessageBox.Show("Seleccione un Mes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboMes.Focus()
            Exit Sub
        End If
        If Trim(cboA�o.Text) = "" Then
            MessageBox.Show("Seleccione un A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboA�o.Focus()
            Exit Sub
        End If
        Dim RoD As String = ""
        If rdbRes.Checked = True And rdbDet.Checked = False Then
            RoD = "R"
        ElseIf rdbDet.Checked = True And rdbRes.Checked = False Then
            RoD = "D"
        End If
        If Trim(cboMoneda.Text) = "" Then
            MessageBox.Show("Seleccione una Moneda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboMoneda.Focus()
            Exit Sub
        End If
        Dim dtTableTipoCambio As DataTable
        dtTableTipoCambio = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, Today())
        If dtTableTipoCambio.Rows.Count > 0 Then
            dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
        Else
            dTipoCambio = 0.0
        End If
        Dim x As frmReporteAdministracionVista = frmReporteAdministracionVista.Instance
        'x.MdiParent = frmPrincipal
        x.MesLetras = Trim(cboMes.Text)
        x.A�o = Trim(cboA�o.Text)
        x.Categoria = Trim(cboCategoriaGastosGenerales.SelectedValue)
        x.Res_O_Det = Trim(RoD)
        x.dTipoCambio = dTipoCambio
        x.Moneda = Trim(cboMoneda.SelectedValue)
        x.MonedaDes = Trim(cboMoneda.Text)
        x.Show()
        Exit Sub

        'Dim rptInversionEnCapital As New rptGastosInversionEnCapital
        'Dim rptGastosAdmFijos As New rptGastosAdmFijos
        'Dim rptGastosAdmOcacionales As New rptGastosAdmOcacionales
        'Dim rptInversionEnCapitalDet As New rptGastosInversionEnCapitalDetallado
        'Dim rptGastosAdmFijosDet As New rptGastosAdmFijosDetallado
        'Dim rptGastosAdmOcacionalesDet As New rptGastosAdmOcacionalesDetallado
        'Dim rptGastosPorObra As New rptGastosPorObraCruzadas
        'If Trim(cboMes.Text) = "" Then
        '    MessageBox.Show("Seleccione un Mes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    cboMes.Focus()
        '    Exit Sub
        'End If
        'If Trim(cboA�o.Text) = "" Then
        '    MessageBox.Show("Seleccione un A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    cboA�o.Focus()
        '    Exit Sub
        'End If
        'If Trim(cboCategoriaGastosGenerales.Text) = "" Then
        '    MessageBox.Show("Seleccione una Categoria", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    cboCategoriaGastosGenerales.Focus()
        '    Exit Sub
        'End If
        'If Trim(cboCategoriaGastosGenerales.SelectedValue) = "1" And rdbRes.Checked = True Then
        '    rptInversionEnCapital.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '    '1
        '    crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(gEmpresa)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '2
        '    crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '3
        '    crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '4
        '    crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(cboA�o.Text)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '5
        '    crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gDesEmpresa
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '6
        '    crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gEmprRuc
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '7
        '    crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = dTipoCambio
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '----------------------------------------------------------------------------------
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    CrystalReportViewer1.ReportSource = rptInversionEnCapital
        'ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "1" And rdbDet.Checked = True Then
        '    rptInversionEnCapitalDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '    '1
        '    crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(gEmpresa)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '2
        '    crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '3
        '    crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '4
        '    crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(cboA�o.Text)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '5
        '    crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gDesEmpresa
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '6
        '    crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gEmprRuc
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '7
        '    crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = dTipoCambio
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '-------------------------------------------------------------------------------------------
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    CrystalReportViewer1.ReportSource = rptInversionEnCapitalDet
        'ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "2" And rdbRes.Checked = True Then
        '    rptGastosAdmFijos.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '    '1
        '    crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(gEmpresa)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '2
        '    crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '3
        '    'crParameterFieldDefiniti()
        '    crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '4
        '    crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(cboA�o.Text)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '5
        '    crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gDesEmpresa
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '6
        '    crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gEmprRuc
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '7
        '    crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = dTipoCambio
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '-------------------------------------------------------------------------------------------
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    CrystalReportViewer1.ReportSource = rptGastosAdmFijos

        'ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "2" And rdbDet.Checked = True Then

        '    rptGastosAdmFijosDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '    '1
        '    crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(gEmpresa)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '2
        '    crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '3
        '    crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '4
        '    crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(cboA�o.Text)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '5
        '    crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gDesEmpresa
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '6
        '    crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gEmprRuc
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '7
        '    crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = dTipoCambio
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '-------------------------------------------------------------------------------------------
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    CrystalReportViewer1.ReportSource = rptGastosAdmFijosDet

        'ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "3" And rdbRes.Checked = True Then

        '    rptGastosAdmOcacionales.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '    '1
        '    crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(gEmpresa)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '2
        '    crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '3
        '    crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '4
        '    crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(cboA�o.Text)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '5
        '    crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gDesEmpresa
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '6
        '    crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gEmprRuc
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '7
        '    crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = dTipoCambio
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '-------------------------------------------------------------------------------------------
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    CrystalReportViewer1.ReportSource = rptGastosAdmOcacionales

        'ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "3" And rdbDet.Checked = True Then


        '    rptGastosAdmOcacionalesDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '    '1
        '    crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(gEmpresa)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '2
        '    crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '3
        '    crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '4
        '    crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = Trim(cboA�o.Text)
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '5
        '    crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gDesEmpresa
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '6
        '    crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = gEmprRuc
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '7
        '    crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
        '    crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
        '    crParameterValues = crParameterFieldDefinition.CurrentValues
        '    crParameterDiscreteValue = New ParameterDiscreteValue()
        '    crParameterDiscreteValue.Value = dTipoCambio
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    '-------------------------------------------------------------------------------------------
        '    crParameterValues.Add(crParameterDiscreteValue)
        '    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '    CrystalReportViewer1.ReportSource = rptGastosAdmOcacionalesDet

        'ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "4" And rdbRes.Checked = True Then
        '    Try
        '        Dim dtTable2 As DataTable
        '        Dim pIdRetencion As String = ""
        '        Dim pSerie As String = ""
        '        dtTable2 = New DataTable
        '        eGastosGenerales = New clsGastosGenerales
        '        If cboMoneda.SelectedValue = "01" Then
        '            dtTable2 = eGastosGenerales.fListarGastosDeObra(0, gEmpresa, Trim(TraerNumeroMes(cboMes.Text)), UCase(Trim(cboMes.Text)), Trim(cboA�o.Text), gDesEmpresa, gEmpresa, 0)
        '        End If
        '        If cboMoneda.SelectedValue = "02" Then
        '            dtTable2 = eGastosGenerales.fListarGastosDeObra(1, gEmpresa, Trim(TraerNumeroMes(cboMes.Text)), UCase(Trim(cboMes.Text)), Trim(cboA�o.Text), gDesEmpresa, gEmpresa, 0)
        '        End If
        '        If dtTable2.Rows.Count > 0 Then
        '            Dim NumeroMes As String = ""
        '            NumeroMes = Trim(TraerNumeroMes(cboMes.Text))
        '            Muestra_Reporte(RutaAppReportes & "rptGastosPorObraCruzadas3.rpt", dtTable2, "TblLibroDiario", "", "@EmprCodigo;" & gEmpresa, "@Mes;" & NumeroMes, "@MesCadena;" & UCase(Trim(cboMes.Text)), "@Anio;" & Trim(cboA�o.Text), "@Empresa;" & gDesEmpresa, "@RUC;" & gEmprRuc, "@TC;" & dTipoCambio, "@MonedaDes;" & Trim(cboMoneda.Text))
        '            'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
        '        ElseIf dtTable2.Rows.Count = 0 Then
        '            Dim mensaje As String = ""
        '            MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        End If
        '    Catch ex As Exception
        '        MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End Try



        'ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "4" And rdbDet.Checked = True Then
        '    Try
        '        Dim dtTable2 As DataTable
        '        Dim pIdRetencion As String = ""
        '        Dim pSerie As String = ""
        '        dtTable2 = New DataTable
        '        eGastosGenerales = New clsGastosGenerales
        '        'If cboMoneda.SelectedValue = "01" Then
        '        dtTable2 = eGastosGenerales.fListarGastosDeObra(2, gEmpresa, Trim(TraerNumeroMes(cboMes.Text)), UCase(Trim(cboMes.Text)), Trim(cboA�o.Text), gDesEmpresa, gEmpresa, 0)
        '        If dtTable2.Rows.Count > 0 Then
        '            Dim NumeroMes As String = ""
        '            NumeroMes = Trim(TraerNumeroMes(cboMes.Text))
        '            Muestra_Reporte(RutaAppReportes & "rptGastosPorObraDetallado.rpt", dtTable2, "TblLibroDiario", "", "@EmprCodigo;" & gEmpresa, "@Mes;" & NumeroMes, "@MesCadena;" & UCase(Trim(cboMes.Text)), "@Anio;" & Trim(cboA�o.Text), "@Empresa;" & gDesEmpresa, "@RUC;" & gEmprRuc, "@TC;" & dTipoCambio, "@MonedaDes;" & Trim(cboMoneda.Text))
        '            'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
        '        ElseIf dtTable2.Rows.Count = 0 Then
        '            Dim mensaje As String = ""
        '            MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '        End If
        '    Catch ex As Exception
        '        MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End Try
        '    Me.Cursor = Cursors.Default
        'End If

    End Sub


    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            'Dim f As New frmReporteConsolidado2
            'Dim myReporte As New ReportDocument

            ''Cargo el reporte segun ruta
            'myReporte.Load(STRnombreReporte)

            ''Leo los parametros
            'If Parametros.Length > 0 Then
            '    f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            'End If

            'f.CRVisor.SelectionFormula = STRfiltro
            'myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument
            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)
            'Leo los parametros
            If Parametros.Length > 0 Then
                CrystalReportViewer1.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            CrystalReportViewer1.SelectionFormula = STRfiltro
            myReporte.SetDataSource(myDatos)
            CrystalReportViewer1.ReportSource = myReporte
            'f.CRVisor.DisplayGroupTree = False
            'f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields
        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If Me.dgvLista.RowCount > 0 Then
        Dim x As frmReporteAdministracionGrupCC = frmReporteAdministracionGrupCC.Instance
        'VerPosicion()
        'x.strCodigoGrupo = Trim(Fila("IdGrupo").ToString)
        'x.strDesGrupo = Trim(Fila("DesGrupo").ToString)
        'x.strCodigoGrupoGeneral = Trim(cboCategoria.SelectedValue)
        'x.strNombreUsuario = Trim(Fila("UsuCodigo").ToString)
        x.strCodigoReporte = Trim(cboCategoriaGastosGenerales.SelectedValue)
        x.strDesReporte = (Trim(cboCategoriaGastosGenerales.Text))
        x.Owner = Me
        x.ShowInTaskbar = False
        x.ShowDialog()
    End Sub

    Private Sub cboCategoriaGastosGenerales_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategoriaGastosGenerales.SelectedIndexChanged
        If cboCategoriaGastosGenerales.SelectedIndex = 0 Or cboCategoriaGastosGenerales.SelectedIndex = 1 Or cboCategoriaGastosGenerales.SelectedIndex = 2 Then
            GroupBox1.Enabled = True
            cboMoneda.Enabled = False
            CrystalReportViewer1.ReportSource = Nothing
        ElseIf cboCategoriaGastosGenerales.SelectedIndex = 3 And rdbRes.Checked = True Then
            GroupBox1.Enabled = True
            cboMoneda.Enabled = True
            CrystalReportViewer1.ReportSource = Nothing
        ElseIf cboCategoriaGastosGenerales.SelectedIndex = 3 And rdbDet.Checked = True Then
            GroupBox1.Enabled = True
            cboMoneda.Enabled = False
            CrystalReportViewer1.ReportSource = Nothing
        End If
    End Sub

    Private Sub rdbDet_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDet.CheckedChanged
        If rdbDet.Checked = True And cboCategoriaGastosGenerales.SelectedIndex = 3 Then
            cboMoneda.Enabled = False
        End If
    End Sub

    Private Sub rdbRes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbRes.CheckedChanged
        If rdbRes.Checked = True And cboCategoriaGastosGenerales.SelectedIndex = 3 Then
            cboMoneda.Enabled = True
        End If
    End Sub

    Private Sub rdbRes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRes.Click
        'btnAceptar_Click(sender, e)
    End Sub

    Private Sub rdbDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbDet.Click
        'btnAceptar_Click(sender, e)
    End Sub

    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged
    End Sub

End Class