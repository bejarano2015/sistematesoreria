Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmConciliacionBanc

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmConciliacionBanc = Nothing

    Public Shared Function Instance() As frmConciliacionBanc
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmConciliacionBanc
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmConciliacionBanc_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region


    Private eChequera As clsChequera
    Private eLibroBancos As clsLibroBancos
    Private eTempo As clsPlantTempo


    'Public CodEmpresa As String = ""
    Public CodCuenta As String = ""
    Public Mes As String = ""
    Public Anio As String = ""

    Public BancoDes As String = ""
    Public MonedaDes As String = ""
    Public NroCuentaDes As String = ""
    Public SaldoFinal As String = ""
    Public SaldoInicial As String = ""

    'Public SaldoFinal As String = ""
    Dim Fecha As DateTime

    Dim iOpcion As Int16 = 0
    Dim IdLibroCab As String = ""

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        'eArqueo = New clsArqueo
        'Dim dtIdArqueo As DataTable
        'dtIdArqueo = New DataTable
        'Dim IdArqueo As String

        'dtIdArqueo = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
        'If dtIdArqueo.Rows.Count = 1 Then
        Dim x As frmReporteLibroBancos = frmReporteLibroBancos.Instance
        'x.MdiParent = frmPrincipal
        'IdArqueo = Convert.ToString(dtIdArqueo.Rows(0).Item(0))
        x.IdLIbroCab = Trim(IdLibroCab)


        x.Empresa = Trim(gDesEmpresa)
        x.Banco = Trim(BancoDes)
        x.Moneda = Trim(MonedaDes)
        x.NroCuenta = Trim(NroCuentaDes)
        x.Mes = TraerNombreMes(Trim(Mes))
        x.Anio = Trim(Anio)
        x.IdCuenta = Trim(CodCuenta)
        x.FechaFinal = Fecha
        x.SaldoInicialRep = SaldoInicial
        'x.SaldoFinalRep = SaldoFinal
        x.SaldoFinalRep = (txtSaldoLibro.Text)
        x.SaldoSegunBanco = Trim(txtSaldoBanco.Text)
        x.Show()
        'Else
        'MessageBox.Show("No Existe Arqueo del Dia", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        ' End If

    End Sub

    Private Sub frmConciliacionBanc_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Timer1.Enabled = False
        If Timer1.Enabled = True Then
            txtSaldoBanco.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer1.Enabled = False
    End Sub

    Private Sub txtSaldoBanco_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSaldoBanco.KeyPress

        eLibroBancos = New clsLibroBancos
        Select Case Asc(e.KeyChar)
            Case 13
                Dim dtResumenLibro As DataTable
                dtResumenLibro = New DataTable
                dtResumenLibro = eLibroBancos.fResumenLibro(CodCuenta, gEmpresa, Mes, Anio)
                If dtResumenLibro.Rows.Count > 0 Then
                    Fecha = dtResumenLibro.Rows(0).Item("Fecha")
                End If
                'IdLibroCab = dtResumenLibro.Rows(0).Item("IdLibroCab")
                Dim dtCronogramas As DataTable
                dtCronogramas = New DataTable
                dtCronogramas = eLibroBancos.fListarCheqNoCobrados(CodCuenta, gEmpresa, Mes, Anio, Fecha)
                If dtResumenLibro.Rows.Count > 0 Then
                    IdLibroCab = dtResumenLibro.Rows(0).Item("IdLibroCab")
                    'txtSaldoBanco.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
                    txtCheNoCobra.Text = Format(dtResumenLibro.Rows(0).Item("CheCobradosNO"), "#,##0.00")
                    txtCheCobra.Text = Format(dtResumenLibro.Rows(0).Item("CheCobradosSI"), "#,##0.00")
                    txtTotal.Text = Format(dtResumenLibro.Rows(0).Item("CheCobradosNO") - dtResumenLibro.Rows(0).Item("CheCobradosSI"), "#,##0.00")
                    BancoDes = Trim(dtResumenLibro.Rows(0).Item("NombreBanco"))
                    MonedaDes = Trim(dtResumenLibro.Rows(0).Item("MonDescripcion"))
                    NroCuentaDes = Trim(dtResumenLibro.Rows(0).Item("NumeroCuenta"))
                    BeLabel2.Text = "(-) CHEQUES GIRADOS Y NO COBRADOS " & TraerNombreMes(Mes) & " / " & Anio
                    BeLabel3.Text = "(-) CHEQUES COBRADOS EN " & TraerNombreMes(Mes)
                    'fActSaldosExtractoLibro()
                    Dim MontoEscritox As String = ""
                    MontoEscritox = txtSaldoBanco.Text.Trim
                    If Len(MontoEscritox.Trim) = 0 Then
                        MontoEscritox = 0
                    End If
                    txtSaldoBanco.Text = Format(Convert.ToDouble(MontoEscritox), "#,##0.00")


                    ''''''''''''''''actualiza saldo extracto final del mes'''''''''''''''''
                    Dim iActSaldosExtractos As Integer = 0
                    eLibroBancos = New clsLibroBancos
                    iActSaldosExtractos = eLibroBancos.fActSaldosExtractoLibro(55, gEmpresa, IdLibroCab, 0, Format(Convert.ToDouble(MontoEscritox), "#,##0.00"), 0, 0, Today())
                    ''''''''''''''''actualiza saldo extracto inicial del sguiente mes, al mes actual'''''''''''''''''
                    Dim dtDtFechx As DataTable
                    dtDtFechx = New DataTable
                    Dim Fechax As DateTime
                    Dim FechaActCabecerax As DateTime
                    dtDtFechx = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, IdLibroCab)
                    If dtDtFechx.Rows.Count > 0 Then
                        Fechax = Trim(dtDtFechx.Rows(0).Item("Fecha"))
                        FechaActCabecerax = Fechax
                    End If
                    Dim dtLibrosSuperiores As DataTable
                    dtLibrosSuperiores = New DataTable
                    dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores2(56, Mes, Anio, CodCuenta, gEmpresa, FechaActCabecerax) 'trae los proximos libros
                    If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                        For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                            Dim CodigoLibroBD As String = ""
                            CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            Dim iActSaldosExtractosSup As Integer = 0
                            eLibroBancos = New clsLibroBancos
                            iActSaldosExtractosSup = eLibroBancos.fActSaldosExtractoLibro(57, gEmpresa, CodigoLibroBD, Format(Convert.ToDouble(MontoEscritox), "#,##0.00"), 0, 0, 0, Today())
                        Next
                    End If


                End If


                'If dtCronogramas.Rows.Count > 0 Then
                '    dgvDetalle.DataSource = dtCronogramas
                'End If



                If dgvDetalle.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDetalle.RowCount - 1
                        dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    Next
                End If

                Dim MontoChPagados As Double = 0
                If dtCronogramas.Rows.Count > 0 Then
                    For y As Integer = 0 To dtCronogramas.Rows.Count - 1
                        dgvDetalle.Rows.Add()
                        For z As Integer = 0 To dtCronogramas.Columns.Count - 1

                            If z = 0 Then ' chek
                                dgvDetalle.Rows(y).Cells(z).Value = "(-) CHEQUES GIRADOS Y NO COBRADOS " & TraerNombreMes(Trim(dtCronogramas.Rows(y).Item(z))) & " / " & Trim(dtCronogramas.Rows(y).Item(1))
                            End If

                            If z = 2 Then
                                dgvDetalle.Rows(y).Cells(1).Value = Format(dtCronogramas.Rows(y).Item(z), "#,##0.00")
                                MontoChPagados = MontoChPagados + dtCronogramas.Rows(y).Item(z).ToString
                            End If

                        Next
                    Next
                End If
                If Len(Trim(txtSaldoBanco.Text)) = 0 Then
                    txtSaldoBanco.Text = "0.00"
                End If
                txtSaldoLibro.Text = Format(Trim(txtSaldoBanco.Text) - MontoChPagados, "#,##0.00")
                txtTenerBanco.Text = Format(SaldoFinal + MontoChPagados, "#,##0.00")

                Dim MontoEscrito As String = ""
                MontoEscrito = txtSaldoBanco.Text.Trim
                If Len(MontoEscrito.Trim) = 0 Then
                    MontoEscrito = 0
                End If
                txtSaldoBanco.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")

                txtDif.Text = Format((SaldoFinal - txtSaldoLibro.Text), "#,##0.00")

        End Select
    End Sub

    Private Sub txtSaldoBanco_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSaldoBanco.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtSaldoBanco.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtSaldoBanco.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub txtSaldoBanco_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSaldoBanco.TextChanged

    End Sub

    Private Sub txtSaldoLibro_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSaldoLibro.TextChanged

    End Sub
End Class