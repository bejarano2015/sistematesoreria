Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReporteAdministracion

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    'Dim rptInversionEnCapital As New rptGastosInversionEnCapital
    'Dim rptGastosAdmFijos As New rptGastosAdmFijos
    'Dim rptGastosAdmOcacionales As New rptGastosAdmOcacionales

    'Dim rptInversionEnCapitalDet As New rptGastosInversionEnCapitalDetallado
    'Dim rptGastosAdmFijosDet As New rptGastosAdmFijosDetallado
    'Dim rptGastosAdmOcacionalesDet As New rptGastosAdmOcacionalesDetallado

    'Dim rptGastosPorObra As New rptGastosPorObraCruzadas
    'Dim rptGastosPorObra As New rptGastosPorObra

    Private eGastosGenerales As clsGastosGenerales
    Private ePagoProveedores As clsPagoProveedores

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAdministracion = Nothing
    Public Shared Function Instance() As frmReporteAdministracion
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteAdministracion
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteAdministracion_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteAdministracion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboA�o.Text = Year(Now)
        cboA�o.DropDownStyle = ComboBoxStyle.DropDownList
        cboCategoriaGastosGenerales.DropDownStyle = ComboBoxStyle.DropDownList
        cboMes.DropDownStyle = ComboBoxStyle.DropDownList


        'Dim sSufijo As String = "00"
        'Dim mes As String = ""
        'mes = Month(Now)
        'mes = Trim(Right(sSufijo & Convert.ToString(mes), sSufijo.Length))

        'iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
        'If iCodigo = "" Then
        '    iCodigo = 1
        'Else
        '    iCodigo += 1
        'End If
        Dim pMes As String = Format(Month(Now), "00")

        'mes = Trim(sSufijo & Convert.ToString(mes))

        cboMes.Text = TraerNombreMes(pMes)

        eGastosGenerales = New clsGastosGenerales
        cboCategoriaGastosGenerales.DataSource = eGastosGenerales.CategoriaGastosGenerales() 'eGastosGenerales.fListarCronogramas5(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboCategoriaGastosGenerales.ValueMember = "IdGastosGenerales"
            cboCategoriaGastosGenerales.DisplayMember = "DescGastosGenerales"
            cboCategoriaGastosGenerales.SelectedIndex = 0
        End If

        Dim dTipoCambio As Double
        Dim dtTable As DataTable
        dtTable = New DataTable
        ePagoProveedores = New clsPagoProveedores

        dtTable = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, Today())
        If dtTable.Rows.Count > 0 Then
            dTipoCambio = Val(dtTable.Rows(0).Item("TcaVenta"))
            'lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
        Else
            dTipoCambio = 0.0
            'lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
        End If

        rdbRes.Checked = True
    End Sub



    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click

        Dim rptInversionEnCapital As New rptGastosInversionEnCapital
        Dim rptGastosAdmFijos As New rptGastosAdmFijos
        Dim rptGastosAdmOcacionales As New rptGastosAdmOcacionales

        Dim rptInversionEnCapitalDet As New rptGastosInversionEnCapitalDetallado
        Dim rptGastosAdmFijosDet As New rptGastosAdmFijosDetallado
        Dim rptGastosAdmOcacionalesDet As New rptGastosAdmOcacionalesDetallado

        Dim rptGastosPorObra As New rptGastosPorObraCruzadas

        If Trim(cboMes.Text) = "" Then
            MessageBox.Show("Seleccione un Mes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboMes.Focus()
            Exit Sub
        End If

        If Trim(cboA�o.Text) = "" Then
            MessageBox.Show("Seleccione un A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboA�o.Focus()
            Exit Sub
        End If

        If Trim(cboCategoriaGastosGenerales.Text) = "" Then
            MessageBox.Show("Seleccione una Categoria", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboCategoriaGastosGenerales.Focus()
            Exit Sub
        End If

        If Trim(txtTipoCambio.Text) = "" Or Trim(txtTipoCambio.Text) = "0.00" Then
            MessageBox.Show("Ingrese un Tipo de Cambio", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtTipoCambio.Focus()
            Exit Sub
            'ElseIf Trim(txtTipoCambio.Text) <> "" Then
            '    If Convert.ToInt64(txtTipoCambio.Text) = 0 Then
            '        MessageBox.Show("Ingrese un Tipo de Cambio", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '        txtTipoCambio.Focus()
            '    Exit Sub
            'End If
        End If

        If Trim(cboCategoriaGastosGenerales.SelectedValue) = "1" And rdbRes.Checked = True Then

            rptInversionEnCapital.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4

            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Val(txtTipoCambio.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '----------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptInversionEnCapital

        ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "1" And rdbDet.Checked = True Then

            rptInversionEnCapitalDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Val(txtTipoCambio.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptInversionEnCapitalDet

        ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "2" And rdbRes.Checked = True Then

            rptGastosAdmFijos.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Val(txtTipoCambio.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmFijos

        ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "2" And rdbDet.Checked = True Then

            rptGastosAdmFijosDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Val(txtTipoCambio.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmFijosDet

        ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "3" And rdbRes.Checked = True Then

            rptGastosAdmOcacionales.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Val(txtTipoCambio.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmOcacionales

        ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "3" And rdbDet.Checked = True Then

            rptGastosAdmOcacionalesDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Val(txtTipoCambio.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmOcacionalesDet

        ElseIf Trim(cboCategoriaGastosGenerales.SelectedValue) = "4" Then

            rptGastosPorObra.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosPorObra.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosPorObra.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosPorObra.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(cboMes.Text))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosPorObra.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosPorObra.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosPorObra.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosPorObra.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Val(txtTipoCambio.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosPorObra

        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'If Me.dgvLista.RowCount > 0 Then
        Dim x As frmReporteAdministracionGrupCC = frmReporteAdministracionGrupCC.Instance
        'VerPosicion()
        'x.strCodigoGrupo = Trim(Fila("IdGrupo").ToString)
        'x.strDesGrupo = Trim(Fila("DesGrupo").ToString)
        'x.strCodigoGrupoGeneral = Trim(cboCategoria.SelectedValue)
        'x.strNombreUsuario = Trim(Fila("UsuCodigo").ToString)
        x.strCodigoReporte = Trim(cboCategoriaGastosGenerales.SelectedValue)
        x.strDesReporte = (Trim(cboCategoriaGastosGenerales.Text))
        x.Owner = Me
        x.ShowInTaskbar = False
        x.ShowDialog()
        'End If
    End Sub

    Private Sub cboCategoriaGastosGenerales_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategoriaGastosGenerales.SelectedIndexChanged

    End Sub
End Class