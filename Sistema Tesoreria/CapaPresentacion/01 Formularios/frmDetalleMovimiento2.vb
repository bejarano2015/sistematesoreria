Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Windows.Forms

Public Class frmDetalleMovimiento2

    Dim dtTable As DataTable

    Public sIdMovimiento As String = ""
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco

 

    Private Sub frmDetalleMovimiento2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If sIdMovimiento <> "" Then
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            dtTable = New DataTable
            dtTable = eMovimientoCajaBanco.fListarDetalle2(sIdMovimiento, gEmpresa)
            Dim MontoTotal As Double = 0
            If dtTable.Rows.Count > 0 Then
                For y As Integer = 0 To dtTable.Rows.Count - 1
                    dgvDetalle.Rows.Add()
                    For z As Integer = 0 To dtTable.Columns.Count - 1
                        If z = 4 Then
                            If dtTable.Rows(y).Item(4).ToString = 1 Then
                                dgvDetalle.Rows(y).Cells(z).Value = True
                            ElseIf dtTable.Rows(y).Item(4).ToString = 0 Then
                                dgvDetalle.Rows(y).Cells(z).Value = False
                            End If
                        ElseIf z = 7 Then
                            dgvDetalle.Rows(y).Cells(8).Value = dtTable.Rows(y).Item(z)
                        ElseIf z = 5 Then
                            MontoTotal = MontoTotal + dtTable.Rows(y).Item(z)
                            dgvDetalle.Rows(y).Cells(z).Value = dtTable.Rows(y).Item(z)
                        Else
                            dgvDetalle.Rows(y).Cells(z).Value = dtTable.Rows(y).Item(z)
                        End If
                        dgvDetalle.Rows(y).Cells(7).Value = "1"
                    Next
                Next
                txtTotal.Text = Format(MontoTotal, "0.00")
            End If

        End If
    End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub

    Private Sub dgvDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        Select Case e.KeyCode

            Case Keys.Escape
                Me.Hide()

        End Select
    End Sub
End Class