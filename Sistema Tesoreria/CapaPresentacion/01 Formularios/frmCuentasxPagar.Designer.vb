<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCuentasxPagar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.cboTipoPagare = New ctrLibreria.Controles.BeComboBox
        Me.txtBeneficiario = New ctrLibreria.Controles.BeTextBox
        Me.txtNumero = New ctrLibreria.Controles.BeTextBox
        Me.txtConcepto = New ctrLibreria.Controles.BeTextBox
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox
        Me.txtImporte = New ctrLibreria.Controles.BeTextBox
        Me.dtpFechaVcto = New ctrLibreria.Controles.BeDateTimePicker
        Me.txtTEA = New ctrLibreria.Controles.BeTextBox
        Me.txtInteres = New ctrLibreria.Controles.BeTextBox
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.dgvDocumentos = New System.Windows.Forms.DataGridView
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NroDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ImporteTotal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnEliminar = New ctrLibreria.Controles.BeButton
        Me.btnBuscar = New ctrLibreria.Controles.BeButton
        Me.btnNuevo = New ctrLibreria.Controles.BeButton
        Me.btnGrabar = New ctrLibreria.Controles.BeButton
        Me.btnCancelar = New ctrLibreria.Controles.BeButton
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboTipoPagare
        '
        Me.cboTipoPagare.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoPagare.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoPagare.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoPagare.ForeColor = System.Drawing.Color.Black
        Me.cboTipoPagare.FormattingEnabled = True
        Me.cboTipoPagare.Items.AddRange(New Object() {"PAGARES", "FINANCIAMIENTO"})
        Me.cboTipoPagare.KeyEnter = True
        Me.cboTipoPagare.Location = New System.Drawing.Point(94, 36)
        Me.cboTipoPagare.Name = "cboTipoPagare"
        Me.cboTipoPagare.Size = New System.Drawing.Size(276, 21)
        Me.cboTipoPagare.TabIndex = 3
        '
        'txtBeneficiario
        '
        Me.txtBeneficiario.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBeneficiario.BackColor = System.Drawing.Color.Ivory
        Me.txtBeneficiario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBeneficiario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBeneficiario.ForeColor = System.Drawing.Color.Black
        Me.txtBeneficiario.KeyEnter = True
        Me.txtBeneficiario.Location = New System.Drawing.Point(94, 63)
        Me.txtBeneficiario.Name = "txtBeneficiario"
        Me.txtBeneficiario.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBeneficiario.ShortcutsEnabled = False
        Me.txtBeneficiario.Size = New System.Drawing.Size(276, 21)
        Me.txtBeneficiario.TabIndex = 5
        Me.txtBeneficiario.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtNumero
        '
        Me.txtNumero.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumero.BackColor = System.Drawing.Color.Ivory
        Me.txtNumero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumero.ForeColor = System.Drawing.Color.Black
        Me.txtNumero.KeyEnter = True
        Me.txtNumero.Location = New System.Drawing.Point(94, 89)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumero.ShortcutsEnabled = False
        Me.txtNumero.Size = New System.Drawing.Size(276, 21)
        Me.txtNumero.TabIndex = 7
        Me.txtNumero.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtConcepto
        '
        Me.txtConcepto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtConcepto.BackColor = System.Drawing.Color.Ivory
        Me.txtConcepto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtConcepto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtConcepto.ForeColor = System.Drawing.Color.Black
        Me.txtConcepto.KeyEnter = True
        Me.txtConcepto.Location = New System.Drawing.Point(94, 115)
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtConcepto.ShortcutsEnabled = False
        Me.txtConcepto.Size = New System.Drawing.Size(276, 21)
        Me.txtConcepto.TabIndex = 9
        Me.txtConcepto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(94, 141)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(276, 21)
        Me.cboMoneda.TabIndex = 11
        '
        'txtImporte
        '
        Me.txtImporte.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporte.BackColor = System.Drawing.Color.Ivory
        Me.txtImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporte.ForeColor = System.Drawing.Color.Black
        Me.txtImporte.KeyEnter = True
        Me.txtImporte.Location = New System.Drawing.Point(94, 168)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporte.ShortcutsEnabled = False
        Me.txtImporte.Size = New System.Drawing.Size(276, 21)
        Me.txtImporte.TabIndex = 13
        Me.txtImporte.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'dtpFechaVcto
        '
        Me.dtpFechaVcto.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaVcto.CustomFormat = ""
        Me.dtpFechaVcto.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVcto.KeyEnter = True
        Me.dtpFechaVcto.Location = New System.Drawing.Point(537, 10)
        Me.dtpFechaVcto.Name = "dtpFechaVcto"
        Me.dtpFechaVcto.Size = New System.Drawing.Size(170, 21)
        Me.dtpFechaVcto.TabIndex = 15
        Me.dtpFechaVcto.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaVcto.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'txtTEA
        '
        Me.txtTEA.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTEA.BackColor = System.Drawing.Color.Ivory
        Me.txtTEA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTEA.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTEA.ForeColor = System.Drawing.Color.Black
        Me.txtTEA.KeyEnter = True
        Me.txtTEA.Location = New System.Drawing.Point(537, 36)
        Me.txtTEA.Name = "txtTEA"
        Me.txtTEA.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTEA.ShortcutsEnabled = False
        Me.txtTEA.Size = New System.Drawing.Size(170, 21)
        Me.txtTEA.TabIndex = 17
        Me.txtTEA.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtInteres
        '
        Me.txtInteres.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtInteres.BackColor = System.Drawing.Color.Ivory
        Me.txtInteres.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtInteres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtInteres.ForeColor = System.Drawing.Color.Black
        Me.txtInteres.KeyEnter = True
        Me.txtInteres.Location = New System.Drawing.Point(537, 64)
        Me.txtInteres.Name = "txtInteres"
        Me.txtInteres.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtInteres.ShortcutsEnabled = False
        Me.txtInteres.Size = New System.Drawing.Size(170, 21)
        Me.txtInteres.TabIndex = 19
        Me.txtInteres.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.Ivory
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(94, 9)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(276, 21)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Codigo"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Tipo Pagare"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Beneficiario"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(9, 122)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Concepto"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 149)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Moneda"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(9, 96)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Numero"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(9, 175)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 13)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Importe"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(415, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(114, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Fecha Vencimiento"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(415, 43)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(57, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "T.E.A. %"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(415, 71)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(64, 13)
        Me.Label10.TabIndex = 18
        Me.Label10.Text = "Interes %"
        '
        'dgvDocumentos
        '
        Me.dgvDocumentos.AllowUserToAddRows = False
        Me.dgvDocumentos.BackgroundColor = System.Drawing.Color.White
        Me.dgvDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDocumentos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.NroDocumento, Me.Column3, Me.Column4, Me.Column10, Me.ImporteTotal, Me.Column12, Me.Column11, Me.Column1, Me.Column20})
        Me.dgvDocumentos.EnableHeadersVisualStyles = False
        Me.dgvDocumentos.Location = New System.Drawing.Point(3, 194)
        Me.dgvDocumentos.Name = "dgvDocumentos"
        Me.dgvDocumentos.RowHeadersVisible = False
        Me.dgvDocumentos.Size = New System.Drawing.Size(981, 217)
        Me.dgvDocumentos.TabIndex = 21
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "AbrDoc"
        Me.Column2.HeaderText = "Codigo"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'NroDocumento
        '
        Me.NroDocumento.DataPropertyName = "NroDocumento"
        Me.NroDocumento.HeaderText = "Tipo"
        Me.NroDocumento.Name = "NroDocumento"
        Me.NroDocumento.ReadOnly = True
        Me.NroDocumento.Width = 90
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "FechaDocumento"
        Me.Column3.HeaderText = "Beneficiario"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 80
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "FechaVencimiento"
        Me.Column4.HeaderText = "Numero"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "MonSimbolo"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column10.HeaderText = "Concepto"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'ImporteTotal
        '
        Me.ImporteTotal.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.CornflowerBlue
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.ImporteTotal.DefaultCellStyle = DataGridViewCellStyle2
        Me.ImporteTotal.HeaderText = "Moneda"
        Me.ImporteTotal.Name = "ImporteTotal"
        Me.ImporteTotal.ReadOnly = True
        Me.ImporteTotal.Width = 80
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "Saldo"
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSteelBlue
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Column12.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column12.HeaderText = "Importe"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Width = 80
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "sPagar"
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Column11.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column11.HeaderText = "Fecha Vencimiento"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 150
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "sDP"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column1.HeaderText = "TEA %"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column20
        '
        Me.Column20.DataPropertyName = "MtoDP"
        Me.Column20.HeaderText = "Interes %"
        Me.Column20.Name = "Column20"
        Me.Column20.ReadOnly = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnEliminar)
        Me.GroupBox3.Controls.Add(Me.btnBuscar)
        Me.GroupBox3.Controls.Add(Me.btnNuevo)
        Me.GroupBox3.Controls.Add(Me.btnGrabar)
        Me.GroupBox3.Controls.Add(Me.btnCancelar)
        Me.GroupBox3.Location = New System.Drawing.Point(733, 138)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(246, 50)
        Me.GroupBox3.TabIndex = 20
        Me.GroupBox3.TabStop = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(199, 13)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(41, 30)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        Me.btnBuscar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnBuscar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuscar.Location = New System.Drawing.Point(152, 14)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(41, 30)
        Me.btnBuscar.TabIndex = 3
        Me.btnBuscar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnNuevo.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__File_new_
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(8, 14)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(41, 30)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(56, 14)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(41, 30)
        Me.btnGrabar.TabIndex = 1
        Me.btnGrabar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(104, 14)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(41, 30)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'frmCuentasxPagar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(987, 414)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.dgvDocumentos)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.txtInteres)
        Me.Controls.Add(Me.txtTEA)
        Me.Controls.Add(Me.dtpFechaVcto)
        Me.Controls.Add(Me.txtImporte)
        Me.Controls.Add(Me.cboMoneda)
        Me.Controls.Add(Me.txtConcepto)
        Me.Controls.Add(Me.txtNumero)
        Me.Controls.Add(Me.txtBeneficiario)
        Me.Controls.Add(Me.cboTipoPagare)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmCuentasxPagar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Cuentas Por Pagar"
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboTipoPagare As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtBeneficiario As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNumero As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtConcepto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtImporte As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dtpFechaVcto As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents txtTEA As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtInteres As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgvDocumentos As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnEliminar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnBuscar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnNuevo As ctrLibreria.Controles.BeButton
    Friend WithEvents btnGrabar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnCancelar As ctrLibreria.Controles.BeButton
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocumento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
