Option Explicit On
Imports System.Xml
Imports MSXML2

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad
Imports System.Math
Public Class frmCajaChica

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCajaChica = Nothing
    Public Shared Function Instance() As frmCajaChica
        If frmInstance Is Nothing Then
            frmInstance = New frmCajaChica
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmCajaChica_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub



    Private Sub frmLibroBancos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Dim sVieneDe As Int16
    Dim contar1 As Integer = 0
    Dim contar2 As Integer = 0
    Dim contar3 As Integer = 0
    Dim countGrabar As Integer = 0
    Dim CodigoIdDetalle As String
    Dim AceptoGrabar As Integer = 0
    Dim cadena As String
    Dim WithEvents cmr As CurrencyManager
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim TBL As New DataTable

    Private eSesionCajas As clsSesionCajas
    Private eCompraRequerimientos As clsCompraRequerimientos
    Private eArqueo As clsArqueo
    Private eTempo As clsPlantTempo
    Dim IdGastoCorto As String
    Dim IdDocumentoCorto As String
    Dim IdTipoMovCorto As String
    Dim IdCCostoCorto As String
    Dim sCodigoActual As String = ""
    Dim IntPrestamo As Integer
    Dim Resultado As Double
    Dim Resultado2 As Double
    Dim DblIngreso As Double
    Dim DblEgreso As Double
    Dim DblVuelto As Double
    Dim DblInicio As Double
    Dim DblGasto As Double
    Dim MontoIngresos As Double
    Dim MontoEgresos As Double
    Dim bolEditar As Boolean = False
    Dim Grabar As Int16 = 0
    Dim iResultadoActArq As Integer = 0

    Dim SumarSaldoAnterior As Integer = 0
    Dim SumarSaldoAnteriorMenosAporIni As Integer = 0
    Dim sumarDetalle As Integer = 0

    Private eRegistroDocumento As clsRegistroDocumento

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim x As Integer
        'Dim y As Integer
        'Dim z As Integer
        lblNumCaja.Text = Trim(sDesCaja)
        lblEmpresa.Text = Trim(gDesEmpresa)
        lblArea.Text = Trim(sDesArea)
        lblEmpleado.Text = Trim(sDesEmpleado)
        'Para los Combos del Datagrid-Detalle
        If ListarAporteInicialMasGastos = 1 Then
            'Call cargarTipoGastos1()
            cargarTipoGastoss(15)
        ElseIf ListarSaldoAnteriorMasGastos = 1 Then
            'Call cargarTipoGastos2()
            cargarTipoGastoss(26)
        ElseIf ListarSaldoAnteriorMasReembolso = 1 Then
            'Call cargarTipoGastos3()
            cargarTipoGastoss(28)
        ElseIf ListarAporteInicialMasGastos = 0 And ListarSaldoAnteriorMasGastos = 0 And ListarSaldoAnteriorMasReembolso = 0 Then
            cargarTipoGastoss(30)
        End If
        Call cargarCentroCosto()
        Call cargarTipoDocumento()
        Call cargarEmpresas()
        eSesionCajas = New clsSesionCajas
        cboEstado.DataSource = eSesionCajas.fListarEstadoArqueo
        If eSesionCajas.iNroRegistros > 0 Then
            cboEstado.ValueMember = "IdEstadoArqueo"
            cboEstado.DisplayMember = "Descripcion"
            cboEstado.SelectedIndex = -1
        End If
        txtAporteGeneral.Text = Format(Convert.ToDouble(sMontoBase), "#,##0.00")
        Try
            lblNumSesion.Text = sNumeroSesion 'Viene del Numero de Session Abierto o Generado
            If dtCabecera.Rows.Count > 0 Then
                MontoGeneralTotalGasto = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoTotalGas")), "#,##0.00")
                MontoGeneralFinal = Format(sMontoBase - MontoGeneralTotalGasto, "#,##0.00")
                txtGastoGeneral.Text = Format(MontoGeneralTotalGasto, "#,##0.00")
                txtSaldoGeneral.Text = Format(MontoGeneralFinal, "#,##0.00")
                bolEditar = True 'Se va a Editar
                iOpcion = 2
                btnIrArquear.Enabled = False
                'Llenar la Cabecera
                For x = 0 To dtCabecera.Rows.Count - 1
                    txtCodigo.Text = Trim(dtCabecera.Rows(x).Item("IdSesion"))
                    dtpFechaInicio.Value = dtCabecera.Rows(x).Item("FechaInicio")
                    If Microsoft.VisualBasic.IsDBNull(dtCabecera.Rows(x).Item("FechaFin")) = True Then
                        dtpFechaFin.Value = Now.Date().ToShortDateString()
                    Else
                        dtpFechaFin.Value = dtCabecera.Rows(x).Item("FechaFin")
                    End If
                    lblMoneda.Text = sDescripcionMoneda
                    'Pueda que este Cerrada la Caja y se Activa el ChekCerrarCaja
                    If vb.IsDBNull(dtCabecera.Rows(x).Item("CerrarCaja")) <> True Then
                        If dtCabecera.Rows(x).Item("CerrarCaja") = 1 Then
                            MontoGeneralFinal = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoInicioSoles")) - MontoGeneralTotalGasto, "#,##0.00")
                            'txtGastoGeneral.Text = Format(MontoGeneralTotalGasto, "#,##0.00")
                            txtSaldoGeneral.Text = Format(MontoGeneralFinal, "#,##0.00")
                            txtAporteGeneral.Text = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoInicioSoles")), "#,##0.00")
                            '***************************************
                            cargarTipoGastoss(30)
                            dgvDetalleCajas.Enabled = False
                            dtDetalle = New DataTable
                            dtDetalle = eSesionCajas.fListarDetCerrado(IdSesionCabecera, gEmpresa, iEstado01) 'txtCodigo.Text
                            cargarDetalle()
                            btnGrabar.Enabled = False
                            btnImprimir.Enabled = True
                            btnModificar.Enabled = False
                            chkCerrarCaja.Enabled = False
                            btnIrArquear.Enabled = False
                            'dgvDetalleCajas.Enabled = True
                            dgvDetalleCajas.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                            Exit Sub
                        Else
                            chkCerrarCaja.Checked = False
                            btnGrabar.Enabled = True
                            btnImprimir.Enabled = True
                            btnModificar.Enabled = True
                            chkCerrarCaja.Enabled = True
                            btnIrArquear.Enabled = True
                            dgvDetalleCajas.Enabled = True
                        End If
                    End If
                Next
            Else
                'SI NO HAY CABECERA
                txtCodigo.Text = ""
                'txtAporteGeneral.Text = "0.00"
                txtGastoGeneral.Text = "0.00"
                txtSaldoGeneral.Text = "0.00"
                If dtCabecera.Rows.Count > 1 Then
                    'dtpFechaInicio.Enabled = False
                Else
                    dtpFechaInicio.Enabled = True
                End If
                dtpFechaInicio.Value = Now.Date()
                dtpFechaFin.Value = Now.Date()
            End If

            If dtDetalle.Rows.Count > 0 Then 'Si Hay Detalle 
                If dtExisteAporteIni.Rows.Count = 1 Or sExisteReembolso = 1 Then
                    Dim rExisSaldoAntHoy As Integer
                    rExisSaldoAntHoy = eSesionCajas.ExisteSaldoAntHoy(Trim(txtCodigo.Text), Now.Date().ToShortDateString, sIdArea, sIdCaja, gEmpresa, gPeriodo).Rows.Count
                    If dtDetalle.Rows.Count > 0 Then
                        If dtExisteAporteIni.Rows.Count = 1 Then
                            cargarTipoGastoss(15)
                            SumarSaldoAnterior = 0
                        ElseIf sExisteReembolso = 1 Then
                            cargarTipoGastoss(28)
                            SumarSaldoAnterior = 0
                        End If
                        If rExisSaldoAntHoy = 1 Then
                            cargarTipoGastoss(26)
                            SumarSaldoAnterior = 2
                        End If
                        If rExisSaldoAntHoy = 1 And sExisteReembolso = 1 Then
                            cargarTipoGastoss(28)
                            SumarSaldoAnterior = 2
                        End If
                        'LLENO REGISTROS DEL DIA
                        Dim MontoFinalDia As Double = 0
                        Dim MontoInicioDia As Double = 0
                        Dim MontoTotalGastoDia As Double = 0
                        cargarDetalle()
                        btnIrArquear.Enabled = False
                        MontoFinalDia = MontoInicioDia - MontoTotalGastoDia
                        txtMontoIniSoles.Text = Format(MontoInicioDia, "#,##0.00")
                        txtGastos.Text = Format(MontoTotalGastoDia, "#,##0.00")
                        txtMontoFinSoles.Text = Format(MontoFinalDia, "#,##0.00")
                        Calcular()
                    Else
                        dgvDetalleCajas.Focus()
                        dgvDetalleCajas.Rows.Add()
                        dgvDetalleCajas.Rows(0).Cells("Fecha").Value = Now.Date().ToShortDateString
                        'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(8).Value = "0"
                        dgvDetalleCajas.Rows(0).Cells("Opcion").Value = "0"
                        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, 0)
                        btnIrArquear.Enabled = False
                    End If
                Else
                    'Llenar Detalles
                    cargarDetalle()
                    'hola ;) de lima y tu
                End If
                'Fin de la Detalle
                sTab = 1
                Call DesabilitarControles()
                btnNuevo.Enabled = True
                btnModificar.Enabled = True
                btnGrabar.Enabled = False
                btnImprimir.Enabled = False
                btnSalir.Enabled = True
                'Si ChekCerrarCaja fue Chekeado Desde BD?
                If chkCerrarCaja.Checked = True Then
                    ToolStrip1.Enabled = False
                End If
                If dtCabecera.Rows(0).Item("CerrarCaja") = 1 Then
                    'chkCerrarCaja.Checked = True
                    btnModificar.Enabled = False
                End If
            Else
                'SI NO HAY DETALLE
                SumarSaldoAnterior = 1
                HabilitarControles()
                txtMontoIniSoles.Enabled = False
                txtGastos.Enabled = False
                txtMontoFinSoles.Enabled = False
                dgvDetalleCajas.Focus()
                dgvDetalleCajas.Rows.Add()
                dgvDetalleCajas.Rows(0).Cells("Fecha").Value = Now.Date().ToShortDateString
                dgvDetalleCajas.Rows(0).Cells("IdGasto").Value = IdGasto.Items.Item(0).ToString  '"1 - APORTE INICIAL"

                If IdGasto.Items.Item(0).ToString.Trim = "1 - OTROS - APORTE INICIAL" Then
                    dgvDetalleCajas.Rows(0).Cells("Total").Value = sMontoBase
                    dgvDetalleCajas.Rows(0).Cells("Glosa").Value = "APORTE INICIAL"
                    dgvDetalleCajas.Rows(0).Cells("Total").ReadOnly = False
                End If
                If IdGasto.Items.Item(0).ToString = "2 - OTROS - SALDO ANTERIOR" Then
                    If Val(sSaldoAnterior) <= 0 Then
                        dgvDetalleCajas.Rows(0).Cells("Total").Value = sSaldoAnterior '"0.00"
                    Else
                        dgvDetalleCajas.Rows(0).Cells("Total").Value = sSaldoAnterior 'deberia traer la diferencia entre sMontoMaximo-sSaldoAnterior
                    End If
                    dgvDetalleCajas.Rows(0).Cells("Glosa").Value = "SALDO ANTERIOR"
                    dgvDetalleCajas.Rows(0).Cells("Total").ReadOnly = True
                End If

                dgvDetalleCajas.Rows(0).Cells("Opcion").Value = "0" ' ESTARA 0 PARA SER GRABADO EN EL DETALLE
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, 0)

                '-----------------------------------------------------------------------
                If IdGasto.Items.Item(0).ToString = "2 - OTROS - SALDO ANTERIOR" Then
                    '''''''''''''''''''
                    Dim CodigoSesion As String = ""
                    For x = 0 To dtCabecera.Rows.Count - 1
                        CodigoSesion = Trim(dtCabecera.Rows(x).Item("IdSesion"))
                    Next
                    Dim dtDetCajaReem As DataTable
                    dtDetCajaReem = New DataTable
                    eSesionCajas = New clsSesionCajas
                    dtDetCajaReem = eSesionCajas.fBuscarReembolso(CodigoSesion, gEmpresa)
                    If dtDetCajaReem.Rows.Count = 0 And sNumeroSesion > 1 Then
                        dgvDetalleCajas.Rows.Add()
                        dgvDetalleCajas.Rows(1).Cells("Fecha").Value = Now.Date().ToShortDateString
                        dgvDetalleCajas.Rows(1).Cells("IdGasto").Value = IdGasto.Items.Item(1).ToString
                        dgvDetalleCajas.Rows(1).Cells("Glosa").Value = "REEMBOLSO"
                        dgvDetalleCajas.Rows(1).Cells("Total").ReadOnly = True
                        dgvDetalleCajas.Rows(1).Cells("Total").Value = sSaldoReembolso
                        dgvDetalleCajas.Rows(1).Cells("Opcion").Value = "0" ' ESTARA 0 PARA SER GRABADO EN EL DETALLE
                        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(6, 1)
                        dgvDetalleCajas.Focus()
                    Else
                        sSaldoReembolso = "0"
                    End If
                    '''''''''''''''''''
                End If
                '-----------------------------------------------------------------------
                sTab = 1
                lblMoneda.Text = sDescripcionMoneda
                btnNuevo.Enabled = False
                iOpcion = 1
                If dtCabecera.Rows.Count > 0 Then
                    iOpcion = 2
                End If
                btnGrabar.Enabled = True
                btnModificar.Enabled = False
                btnImprimir.Enabled = True
                Calcular()
            End If
            'alinear el campo total a la derecha
            dgvDetalleCajas.Columns("Total").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Me.Width = 1259
        Me.Height = 572
    End Sub


    Private Sub cargarDetalle()
        Dim y As Integer
        Dim z As Integer
        If dtDetalle.Rows.Count > 0 Then
            For y = 0 To dtDetalle.Rows.Count - 1
                dgvDetalleCajas.Rows.Add()
                For z = 0 To dtDetalle.Columns.Count - 1
                    Dim StrCadCodigo As String = String.Empty
                    If z = 0 Then
                        'fecha
                        Me.dgvDetalleCajas.Rows(y).Cells("Fecha").Value = Microsoft.VisualBasic.Left(dtDetalle.Rows(y).Item("FechaDetMov"), 10)
                    ElseIf z = 1 Then
                        'tipo doc
                        For a As Integer = 0 To TipoDocumento.Items.Count - 1
                            Dim cad As String = TipoDocumento.Items.Item(a)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("IdDocumento")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("TipoDocumento").Value = TipoDocumento.Items.Item(a)
                            End If
                        Next
                    ElseIf z = 2 Then
                        'nro doc
                        Me.dgvDetalleCajas.Rows(y).Cells("DocumentoReferencia").Value = dtDetalle.Rows(y).Item("DocumentoReferencia")
                    ElseIf z = 3 Then
                        For a As Integer = 0 To IdGasto.Items.Count - 1
                            Dim cad As String = IdGasto.Items.Item(a)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("TipoGasto")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("IdGasto").Value = IdGasto.Items.Item(a)
                            End If
                        Next
                    ElseIf z = 4 Then
                        'glosa
                        Me.dgvDetalleCajas.Rows(y).Cells("Glosa").Value = dtDetalle.Rows(y).Item("Descripcion")
                    ElseIf z = 5 Then
                        For a As Integer = 0 To CentroCosto.Items.Count - 1
                            Dim cad As String = CentroCosto.Items.Item(a)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("CentroCosto").Value = CentroCosto.Items.Item(a)
                            End If
                        Next
                    ElseIf z = 6 Then
                        'total
                        Me.dgvDetalleCajas.Rows(y).Cells("Total").Value = dtDetalle.Rows(y).Item("Total")
                    ElseIf z = 7 Then
                        'nro doc
                        Me.dgvDetalleCajas.Rows(y).Cells("IdDetalleMovimiento").Value = dtDetalle.Rows(y).Item("IdDetalleMovimiento")
                        'ElseIf z = 8 Then
                        'Me.dgvDetalleCajas.Rows(y).Cells("Opcion").Value = "1"
                        'MessageBox.Show(dtDetalle.Rows(y).Item("IdDetalleMovimiento"))
                    ElseIf z = 9 Then
                        If Trim(dtDetalle.Rows(y).Item("NroVale").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("Anular").Value = False
                            dgvDetalleCajas.Rows(y).Cells("Desglozado").Value = dtDetalle.Rows(y).Item("NroVale").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("NroVale").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("Anular").Value = False
                            dgvDetalleCajas.Rows(y).Cells("Opcion").Value = ""
                        End If
                    ElseIf z = 10 Then
                        For w As Integer = 0 To Empresas.Items.Count - 1
                            Dim cad As String = Empresas.Items.Item(w)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("EmprPrestada")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("Empresas").Value = Empresas.Items.Item(w)
                            End If
                        Next
                    ElseIf z = 11 Then
                        If dtDetalle.Rows(y).Item("EstadoPrestamo") = "1" Then
                            dgvDetalleCajas.Rows(y).Cells("Prestamo").Value = True
                        ElseIf dtDetalle.Rows(y).Item("EstadoPrestamo") = "0" Then
                            dgvDetalleCajas.Rows(y).Cells("Prestamo").Value = False
                        End If
                    ElseIf z = 12 Then
                        If Trim(dtDetalle.Rows(y).Item("Ruc").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("Ruc").Value = dtDetalle.Rows(y).Item("Ruc").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("Ruc").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("Ruc").Value = ""
                        End If
                    ElseIf z = 13 Then
                        If Trim(dtDetalle.Rows(y).Item("RazonSocial").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("RazonSocial").Value = dtDetalle.Rows(y).Item("RazonSocial").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("RazonSocial").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("RazonSocial").Value = ""
                        End If
                    ElseIf z = 14 Then
                        If Trim(dtDetalle.Rows(y).Item("OtrosDatos").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("OtrosD").Value = dtDetalle.Rows(y).Item("OtrosDatos").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("OtrosDatos").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("OtrosD").Value = ""
                        End If
                    End If
                    If dtDetalle.Rows(y).Item("IdDocumento") = "04" Then
                        dgvDetalleCajas.Rows(y).Cells("Anular").Value = True
                        dgvDetalleCajas.Rows(y).Cells("Anular").ReadOnly = True
                    End If
                    Me.dgvDetalleCajas.Rows(y).Cells("Opcion").Value = "1"
                Next
            Next
        End If
    End Sub

    Sub cargarTBL()
        Dim toro As DataTable
        Dim DrFila As DataRow
        Dim CadenaCon As String = ""
        Dim RazonSocial As String = ""
        Dim iResultado As Integer = 0
        Dim i As Integer = 0

        For a As Integer = 0 To dgvDetalleCajas.Columns.Count - 1
            TBL.Columns.Add(a)
        Next

        For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1 'FILA
            ''''''''''''''''graba proveedores nuevos

            If Trim(dgvDetalleCajas.Rows(x).Cells("Ruc").Value) = "" Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(x).Cells("Ruc").Value
            End If

            If Trim(dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value) = "" Then
                RazonSocial = ""
            Else
                RazonSocial = dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value
            End If

            If Len(CadenaCon) = 11 Then
                Dim CodAcreedor As String = ""
                Dim CodRuc As String = ""
                Dim dtTable As DataTable
                dtTable = New DataTable
                CodRuc = Trim(CadenaCon)
                CodAcreedor = "P"
                eRegistroDocumento = New clsRegistroDocumento
                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)

                If dtTable.Rows.Count = 0 Then
                    'graba
                    If Trim(RazonSocial) <> "" Then
                        iResultado = eRegistroDocumento.GrabarAcreedores(1, gEmpresa, Trim(CodAcreedor), Trim(CodRuc), Trim(RazonSocial), "", Trim(CodRuc), "", "J", "", "")
                    End If
                End If
            End If
            ''''''''''''''''graba proveedores nuevos

            DrFila = TBL.NewRow
            For i = 0 To dgvDetalleCajas.Columns.Count - 1 'COLUMNA
                If i = 0 Then
                    'fecha
                    DrFila(0) = dgvDetalleCajas.Rows(x).Cells("Fecha").Value
                ElseIf i = 1 Then
                    'ruc
                    DrFila(13) = dgvDetalleCajas.Rows(x).Cells("Ruc").Value
                ElseIf i = 2 Then
                    'razon
                    DrFila(14) = dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value
                ElseIf i = 7 Then
                    '    'OtrosD
                    DrFila(15) = dgvDetalleCajas.Rows(x).Cells("OtrosD").Value
                ElseIf i = 3 Then
                    'iddoc
                    DrFila(1) = eSesionCajas.fDocumento(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").EditedFormattedValue)
                ElseIf i = 4 Then
                    'numero
                    DrFila(2) = dgvDetalleCajas.Rows(x).Cells("DocumentoReferencia").Value
                ElseIf i = 5 Then
                    'id gasto
                    DrFila(3) = eSesionCajas.fGasto(dgvDetalleCajas.Rows(x).Cells("IdGasto").EditedFormattedValue)
                ElseIf i = 6 Then
                    'glosa
                    DrFila(4) = dgvDetalleCajas.Rows(x).Cells("Glosa").Value
                ElseIf i = 8 Then
                    'centrocosto
                    DrFila(5) = eSesionCajas.fCentroCosto(dgvDetalleCajas.Rows(x).Cells("CentroCosto").EditedFormattedValue)
                ElseIf i = 9 Then
                    'total
                    DrFila(6) = dgvDetalleCajas.Rows(x).Cells("Total").Value

                    '''''''''''''''
                ElseIf i = 10 Then
                    'IdDetalleMovimiento
                    DrFila(7) = dgvDetalleCajas.Rows(x).Cells("IdDetalleMovimiento").Value
                    ''MessageBox.Show(dgvDetalleCajas.Rows(x).Cells("IdDetalleMovimiento").Value)
                ElseIf i = 11 Then
                    'opcion
                    DrFila(8) = dgvDetalleCajas.Rows(x).Cells("Opcion").Value
                    'MessageBox.Show(dgvDetalleCajas.Rows(x).Cells("Opcion").Value)
                    ''''''''''''''''

                ElseIf i = 12 Then
                    'anulado
                    ''DrFila(9) = dgvDetalleCajas.Rows(x).Cells("Opcion").Value

                    If Convert.ToBoolean(dgvDetalleCajas.Rows(x).Cells("Anular").Value) = False Then
                        DrFila(9) = 0
                    Else
                        DrFila(9) = 1
                    End If
                ElseIf i = 13 Then
                    'vale
                    DrFila(10) = dgvDetalleCajas.Rows(x).Cells("Desglozado").Value
                ElseIf i = 14 Then
                    'empresa
                    DrFila(11) = eSesionCajas.fEmpresaPrestada(dgvDetalleCajas.Rows(x).Cells("Empresas").EditedFormattedValue)
                ElseIf i = 15 Then
                    If dgvDetalleCajas.Rows(x).Cells("Empresas").Value = True Then
                        DrFila(12) = 1
                    ElseIf dgvDetalleCajas.Rows(x).Cells("Empresas").Value = False Then
                        DrFila(12) = 0
                    End If
                    ''Else
                    ''DrFila(i) = dgvDetalleCajas.Rows(x).Cells(i).Value
                End If

            Next
            TBL.Rows.Add(DrFila)
        Next
        toro = TBL
    End Sub

    Private Sub cargarTipoGastoss(ByVal Opcion As Integer)
        IdGasto.Items.Clear()
        eSesionCajas = New clsSesionCajas
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarTipoGastoss(Opcion, gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            'Dim xxx As String = ""
            'xxx = Trim(dtTable.Rows(i).Item("IdTipoGasto")) & " - " & Trim(dtTable.Rows(i).Item("DescTGasto") & " - " & dtTable.Rows(i).Item("Descripcion").ToString.Trim)
            IdGasto.Items.Add(dtTable.Rows(i).Item("IdTipoGasto").ToString.Trim & " - " & dtTable.Rows(i).Item("DescTGasto").ToString.Trim & " - " & dtTable.Rows(i).Item("Descripcion").ToString.Trim)
        Next
    End Sub

    Private Sub cargarCentroCosto()
        CentroCosto.Items.Clear()
        eSesionCajas = New clsSesionCajas
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarCCosto(gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            CentroCosto.Items.Add(Trim(dtTable.Rows(i).Item("CCosCodigo")) & " - " & dtTable.Rows(i).Item("CCosDescripcion"))
        Next
    End Sub

    Private Sub cargarTipoDocumento()
        TipoDocumento.Items.Clear()
        eSesionCajas = New clsSesionCajas
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarTipoDocumento()
        For i As Integer = 0 To dtTable.Rows.Count - 1
            TipoDocumento.Items.Add(Trim(dtTable.Rows(i).Item("IdDocumento")) & " - " & dtTable.Rows(i).Item("DescripDoc"))
        Next
    End Sub

    Private Sub cargarEmpresas()
        Empresas.Items.Clear()
        eSesionCajas = New clsSesionCajas
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarEmpresas(gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            Empresas.Items.Add(Trim(dtTable.Rows(i).Item("EmprCodigo")) & " - " & dtTable.Rows(i).Item("EmprDescripcion"))
        Next
    End Sub

    Private Sub DesabilitarControles()
        txtCodigo.Enabled = False
        txtMontoIniSoles.Enabled = False
        txtGastos.Enabled = False
        txtMontoFinSoles.Enabled = False
        dtpFechaInicio.Enabled = False
        dtpFechaFin.Enabled = False
        chkCerrarCaja.Enabled = False
        dgvDetalleCajas.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        txtCodigo.ReadOnly = True
        txtMontoIniSoles.ReadOnly = True
        txtGastos.ReadOnly = True
        txtMontoFinSoles.ReadOnly = True
        dtpFechaInicio.Enabled = True
        dtpFechaFin.Enabled = True
        chkCerrarCaja.Enabled = True
        dgvDetalleCajas.Enabled = True
    End Sub

    Private Sub btnModificar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        iOpcion = 2
        btnIrArquear.Enabled = True
        HabilitarControles()
        'dtpFechaInicio.Enabled = False
        Timer1.Enabled = True
        btnModificar.Enabled = False
        btnGrabar.Enabled = True
        btnImprimir.Enabled = True
        btnNuevo.Enabled = True
    End Sub

    Private Sub btnGrabar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim iResultado As Int32
        Dim iResultadoActCab As Int32
        'Dim iResultado2 As Int32
        'Dim DrFila As DataRow
        Dim i As Integer = 0
        Dim msg As String

        If iOpcion = 1 And countGrabar >= 1 Then
            iOpcion = 2
            sTab = 1
        End If

        If txtCodigo.Text <> "" Then
            iOpcion = 2
            sTab = 1
        End If

        If iOpcion = 2 Then
            iOpcion = 2
            sTab = 1
        End If

        If sTab = 1 Then
            sVieneDe = 1
            If chkCerrarCaja.Checked = True Then
                sVieneDe = 3
            End If

            ValidarDatos2()
            If Grabar = 1 Then
                countGrabar = countGrabar + 1
                If Me.dtpFechaInicio.Text.Trim.Length = 0 Then
                    eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                    frmPrincipal.sFlagGrabar = "0"
                Else
                    If iOpcion = 1 Then
                        msg = "�Esta seguro de Grabar la Caja?"
                    Else
                        msg = "�Esta seguro de Actualizar la Caja?"
                    End If
                    If chkCerrarCaja.Checked = True Then
                        msg = "�Esta seguro de Cerrar la Caja N� " & Trim(lblNumSesion.Text) & "?"
                    Else
                        If iOpcion = 1 Then
                            msg = "�Esta seguro de Grabar la Caja?"
                        Else
                            msg = "�Esta seguro de Actualizar la Caja?"
                        End If
                    End If
                    If (MessageBox.Show(msg, "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                        AceptoGrabar = 1
                        frmPrincipal.sFlagGrabar = "1"
                        If iOpcion = 1 Then
                            eSesionCajas.fCodigo(gEmpresa)
                            sCodigoActual = eSesionCajas.sCodFuturo
                            txtCodigo.Text = sCodigoActual
                        Else
                            sCodigoActual = Me.txtCodigo.Text
                        End If
                        cargarTBL()
                        If iOpcion = 1 Then
                            If sNumeroSesion = 1 Then
                                iResultado = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), iOpcion, TBL, sMontoBase, gPeriodo, sTPlaCodigo, sEmprResposable, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                            ElseIf sNumeroSesion > 1 Then
                                iResultado = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), iOpcion, TBL, 0, gPeriodo, sTPlaCodigo, sEmprResposable, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                            End If
                            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            If iResultado = 1 Then
                                'dtpFechaInicio.Enabled = False
                                If chkCerrarCaja.Checked = True Then
                                    chkCerrarCaja.Enabled = False
                                    dtpFechaInicio.Enabled = False
                                    eArqueo = New clsArqueo
                                    Dim SaldoFinalSesion As Double = 0
                                    Dim GastosDiaArqueo As Double = 0
                                    Dim dtIdArqueoAlCerrar As DataTable
                                    dtIdArqueoAlCerrar = New DataTable
                                    dtIdArqueoAlCerrar = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
                                    If dtIdArqueoAlCerrar.Rows.Count = 0 Then
                                        btnIrArquear.Enabled = True
                                        MessageBox.Show("Debe Arquear El Saldo del Dia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        eSesionCajas.fActualizarCabecera(3, sCodigoActual, Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, 0, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                                        chkCerrarCaja.Enabled = True
                                        chkCerrarCaja.Checked = False
                                        btnGrabar.Enabled = True
                                        btnImprimir.Enabled = True
                                        dgvDetalleCajas.Enabled = True
                                        btnIrArquear_Click(sender, e)
                                        sVieneDe = 3
                                    End If
                                Else
                                    btnGrabar.Enabled = True
                                    btnImprimir.Enabled = True
                                End If
                                Dim x As Integer = 0
                                TBL.Clear()
                                TBL.Reset()
                                TBL.Dispose()
                                For x = 0 To dgvDetalleCajas.RowCount - 1
                                    dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                                Next
                                dtDetalle = New DataTable
                                dtDetalle = eSesionCajas.fListarDet(txtCodigo.Text, gEmpresa, iEstado01)
                                cargarDetalle()
                                If sVieneDe <> 3 Then
                                    MessageBox.Show("Se ha Grabado la Caja N� " & Trim(lblNumSesion.Text), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                End If
                            End If
                            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        Else
                            Dim pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pIdSesion, pVale, pEmprPresta, pRuc, pRazonSocial, pOtrosDatos As String
                            Dim Cantidad, pAnular, pEstadoPrestamo As Integer
                            iResultadoActCab = eSesionCajas.fActualizarCabecera(3, sCodigoActual, Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                            If iResultadoActCab = 1 Then
                                If chkCerrarCaja.Checked = True Then
                                    chkCerrarCaja.Enabled = False
                                    dtpFechaInicio.Enabled = False
                                    'dgvDetalleCajas.Enabled = False
                                    '+++++++++++++++++++++++++++++++++++++++++++++++++++
                                    eArqueo = New clsArqueo
                                    Dim SaldoFinalSesion As Double = 0
                                    Dim GastosDiaArqueo As Double = 0
                                    Dim dtIdArqueoAlCerrar As DataTable
                                    dtIdArqueoAlCerrar = New DataTable
                                    dtIdArqueoAlCerrar = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
                                    If dtIdArqueoAlCerrar.Rows.Count = 0 Then
                                        btnIrArquear.Enabled = True
                                        MessageBox.Show("Debe Arquear el Saldo del Dia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        eSesionCajas.fActualizarCabecera(3, sCodigoActual, Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, 0, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                                        dtpFechaInicio.Enabled = True
                                        chkCerrarCaja.Enabled = True
                                        chkCerrarCaja.Checked = False
                                        btnGrabar.Enabled = True
                                        btnImprimir.Enabled = True
                                        dgvDetalleCajas.Enabled = True
                                        btnIrArquear_Click(sender, e)
                                        sVieneDe = 3
                                    Else
                                        eSesionCajas.fActualizarCabecera(5, sCodigoActual, Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                                        btnGrabar.Enabled = False
                                        btnImprimir.Enabled = False
                                        btnIrArquear.Enabled = False
                                        Panel2.Visible = False
                                        MessageBox.Show("Se ha Cerrado la Caja N� " & Trim(lblNumSesion.Text), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                        btnImprimir.Enabled = True
                                        dtpFechaFin.Enabled = False
                                        dgvDetalleCajas.Enabled = False
                                        'End If
                                    End If
                                    '+++++++++++++++++++++++++++++++++++++++++++++++++++
                                    'dgvDetalleCajas.Enabled = False
                                Else
                                    btnGrabar.Enabled = True
                                    btnImprimir.Enabled = True
                                End If
                            End If

                            For x As Integer = 0 To TBL.Rows.Count - 1
                                dtDataBD = New DataTable
                                If vb.IsDBNull(TBL.Rows(x).Item(0)) = True Then
                                    pFecha = Today()
                                Else
                                    pFecha = TBL.Rows(x).Item(0)
                                    'MessageBox.Show(TBL.Rows(x).Item(0))
                                End If
                                pTipoDoc = TBL.Rows(x).Item(1)
                                If vb.IsDBNull(TBL.Rows(x).Item(2)) = True Then
                                    pNumeroDoc = ""
                                Else
                                    pNumeroDoc = TBL.Rows(x).Item(2)
                                End If
                                pTipoGasto = TBL.Rows(x).Item(3)
                                If vb.IsDBNull(TBL.Rows(x).Item(4)) = True Then
                                    pDescripcion = ""
                                Else
                                    pDescripcion = TBL.Rows(x).Item(4)
                                End If
                                pCentroCosto = TBL.Rows(x).Item(5)
                                If vb.IsDBNull(TBL.Rows(x).Item(6)) = True Then
                                    pTotal = "00.00"
                                Else
                                    pTotal = TBL.Rows(x).Item(6)
                                End If
                                If vb.IsDBNull(TBL.Rows(x).Item(7)) = True Then
                                    pIdDetalleMovimiento = ""
                                Else
                                    pIdDetalleMovimiento = TBL.Rows(x).Item(7)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(9)) = True Then
                                    pAnular = ""
                                Else
                                    If dgvDetalleCajas.Rows(x).Cells("Anular").Value = False Then
                                        pAnular = 0
                                    Else
                                        pAnular = 1
                                    End If
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(10)) = True Then
                                    pVale = ""
                                Else
                                    pVale = TBL.Rows(x).Item(10)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(11)) = True Then
                                    pEmprPresta = ""
                                Else
                                    pEmprPresta = TBL.Rows(x).Item(11)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(12)) = True Then
                                    pEstadoPrestamo = ""
                                Else
                                    pEstadoPrestamo = TBL.Rows(x).Item(12)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(13)) = True Then
                                    pRuc = ""
                                Else
                                    pRuc = TBL.Rows(x).Item(13)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(14)) = True Then
                                    pRazonSocial = ""
                                Else
                                    pRazonSocial = TBL.Rows(x).Item(14)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(15)) = True Then
                                    pOtrosDatos = ""
                                Else
                                    pOtrosDatos = TBL.Rows(x).Item(15)
                                End If
                                'MessageBox.Show(pOtrosDatos)
                                pIdSesion = sCodigoActual
                                If TBL.Rows(x).Item(8) = "0" Then
                                    eSesionCajas.fGrabarItemNuevo(0, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdSesion, gEmpresa, pAnular, pVale, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                Else
                                    eSesionCajas.VerificarDatos(1, Convert.ToDateTime(pFecha), pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, Convert.ToDouble(pTotal), pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos)
                                    Cantidad = eSesionCajas.iNroRegistros
                                    If Cantidad = 0 Then
                                        eSesionCajas.ActualizarDatos(2, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                        If Trim(pTipoDoc) = "04" And Trim(pNumeroDoc) <> "" Then
                                            Dim nrovale As String = ""
                                            nrovale = Trim(pNumeroDoc)
                                            eSesionCajas.fActVale5(nrovale, Trim(txtCodigo.Text), gEmpresa, "", Convert.ToDouble(pTotal))
                                        End If
                                    End If
                                End If
                            Next
                            '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                            'Dim y As Integer
                            'Dim z As Integer
                            Dim b As Integer = 0
                            TBL.Clear()
                            TBL.Reset()
                            TBL.Dispose()
                            For b = 0 To dgvDetalleCajas.RowCount - 1
                                dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                            Next
                            dtDetalle = New DataTable
                            dtDetalle = eSesionCajas.fListarDet(txtCodigo.Text, gEmpresa, iEstado01)
                            cargarDetalle()
                            Timer2.Enabled = True
                            sTab = 0
                            If sVieneDe <> 3 Or sVieneDe = 10 Then
                                MessageBox.Show("Se ha Actualizado la Caja N� " & Trim(lblNumSesion.Text), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            End If
                        End If
                        btnNuevo.Enabled = True
                        btnSalir.Enabled = True
                        btnModificar.Enabled = False

                        If iResultado > 0 Then
                            Timer2.Enabled = True
                            sTab = 0
                        End If
                    Else
                        dtpFechaInicio.Enabled = True
                        dtpFechaFin.Enabled = True
                        AceptoGrabar = 0
                        chkCerrarCaja.Checked = False
                        countGrabar = 0
                        btnGrabar.Enabled = True
                        btnImprimir.Enabled = True
                    End If

                End If
            End If
        End If
        Calcular()
    End Sub

    Private Sub btnNuevo_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        If dtCabecera.Rows.Count > 0 Then
            If vb.IsDBNull(dtCabecera.Rows(0).Item("CerrarCaja")) <> True Then
                If dtCabecera.Rows(0).Item("CerrarCaja") = 1 Then
                    Close()
                    Dim z As frmSesionCajas = frmSesionCajas.Instance
                    z.MdiParent = frmPrincipal
                    z.Show()
                End If
            End If
        End If
        If Trim(txtCodigo.Text) <> "" Then
            Dim dtIdArqueoAlCerrar As DataTable
            dtIdArqueoAlCerrar = New DataTable
            eArqueo = New clsArqueo
            dtIdArqueoAlCerrar = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
            If dtIdArqueoAlCerrar.Rows.Count = 0 Then
                btnIrArquear.Enabled = True
                MessageBox.Show("Debe Arquear El Saldo del Dia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                btnIrArquear_Click(sender, e)
                Exit Sub
            ElseIf dtIdArqueoAlCerrar.Rows.Count = 1 Then
                Dim MontoInicioDia As String = ""
                Dim MontoGastosDia As String = ""
                Dim MontoMontoFinDia As String = ""
                MontoInicioDia = dtIdArqueoAlCerrar.Rows(0).Item("AperturaDia")
                MontoGastosDia = dtIdArqueoAlCerrar.Rows(0).Item("GastosDia")
                MontoMontoFinDia = dtIdArqueoAlCerrar.Rows(0).Item("SaldoDia")
                'If Trim(txtMontoIniSoles.Text) <> MontoInicioDia Or Trim(txtGastos.Text) <> MontoGastosDia Or Trim(txtMontoFinSoles.Text) <> MontoMontoFinDia Then
                If Trim(txtMontoIniSoles.Text) <> Format(Convert.ToDouble(MontoInicioDia), "#,##0.00") Or Trim(txtGastos.Text) <> Format(Convert.ToDouble(MontoGastosDia), "#,##0.00") Or Trim(txtMontoFinSoles.Text) <> Format(Convert.ToDouble(MontoMontoFinDia), "#,##0.00") Then
                    btnIrArquear.Enabled = True
                    MessageBox.Show("Debe Actualizar El Saldo del Dia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    btnIrArquear_Click(sender, e)
                    Exit Sub
                End If
            End If
        End If
        Close()
        Dim x As frmSesionCajas = frmSesionCajas.Instance
        x.MdiParent = frmPrincipal
        x.Show()
    End Sub

    Private Sub btnSalir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        If dtCabecera.Rows.Count > 0 Then
            If vb.IsDBNull(dtCabecera.Rows(0).Item("CerrarCaja")) <> True Then
                If dtCabecera.Rows(0).Item("CerrarCaja") = 1 Then
                    If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        Me.Close()
                    End If
                    Exit Sub
                End If
            End If
        End If
        If Trim(txtCodigo.Text) <> "" Then
            Dim dtIdArqueoAlCerrar As DataTable
            dtIdArqueoAlCerrar = New DataTable
            eArqueo = New clsArqueo
            dtIdArqueoAlCerrar = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
            If dtIdArqueoAlCerrar.Rows.Count = 0 Then
                btnIrArquear.Enabled = True
                MessageBox.Show("Debe Arquear El Saldo del Dia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                btnIrArquear_Click(sender, e)
                Exit Sub
            ElseIf dtIdArqueoAlCerrar.Rows.Count = 1 Then
                Dim MontoInicioDia As String = ""
                Dim MontoGastosDia As String = ""
                Dim MontoMontoFinDia As String = ""
                MontoInicioDia = dtIdArqueoAlCerrar.Rows(0).Item("AperturaDia")
                MontoGastosDia = dtIdArqueoAlCerrar.Rows(0).Item("GastosDia")
                MontoMontoFinDia = dtIdArqueoAlCerrar.Rows(0).Item("SaldoDia")
                If Trim(txtMontoIniSoles.Text) <> Format(Convert.ToDouble(MontoInicioDia), "#,##0.00") Or Trim(txtGastos.Text) <> Format(Convert.ToDouble(MontoGastosDia), "#,##0.00") Or Trim(txtMontoFinSoles.Text) <> Format(Convert.ToDouble(MontoMontoFinDia), "#,##0.00") Then
                    btnIrArquear.Enabled = True
                    MessageBox.Show("Debe Actualizar El Saldo del Dia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    btnIrArquear_Click(sender, e)
                    Exit Sub
                End If
            End If
        End If
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
        'Close()
    End Sub

    Sub Calcular()
        DblEgreso = 0
        DblIngreso = 0
        DblVuelto = 0
        For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1
            If Convert.ToBoolean(dgvDetalleCajas.Rows(x).Cells("Anular").Value) = False Then
                If UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Or UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Or UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                    If Len(dgvDetalleCajas.Rows(x).Cells("Total").Value) = 0 Then
                        DblIngreso = DblIngreso + 0
                    Else
                        DblIngreso = DblIngreso + Val((Convert.ToDouble(dgvDetalleCajas.Rows(x).Cells("Total").Value)))
                    End If
                ElseIf UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "9 - OTROS - INGRESO" Then

                Else
                    If Len(dgvDetalleCajas.Rows(x).Cells("Total").Value) = 0 Then
                        DblEgreso = DblEgreso + 0
                    Else
                        DblEgreso = DblEgreso + Val((Convert.ToDouble(dgvDetalleCajas.Rows(x).Cells("Total").Value)))
                    End If
                End If
            End If
            If vb.IsDBNull(dgvDetalleCajas.Rows(x).Cells("Ruc").Value) = False Then
                Dim CadenaCon As String = ""
                Dim CadenaRazon As String = ""
                If Trim(dgvDetalleCajas.Rows(x).Cells("Ruc").Value) = "" Then
                    CadenaCon = ""
                Else
                    CadenaCon = dgvDetalleCajas.Rows(x).Cells("Ruc").Value
                End If
                If Trim(dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value) = "" Then
                    CadenaRazon = ""
                Else
                    CadenaRazon = dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value
                End If
                If Not (EstadoServicioruc) Then
                    '-----------modificado 29/10/2014 sisjb
                    If Len(CadenaCon) = 11 Then
                        Dim Codigo As String = ""
                        Dim CodAcreedor As String = ""
                        Dim CodRuc As String = ""
                        dtTable = New DataTable
                        CodRuc = Trim(CadenaCon)
                        CodAcreedor = "P"
                        eRegistroDocumento = New clsRegistroDocumento
                        dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)
                        If dtTable.Rows.Count > 0 Then
                            dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value = vb.UCase(Trim(dtTable.Rows(0).Item("AnaliticoDescripcion")))
                        Else
                            If Len(Trim(CadenaRazon)) = 0 Then
                                dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value = ""
                            End If
                        End If
                    End If
                End If

            End If
        Next
        txtMontoIniSoles.Text = Format(DblIngreso, "#,##0.00")
        txtGastos.Text = Format(DblEgreso - DblVuelto, "#,##0.00")
        Resultado = (DblIngreso - DblEgreso)
        txtMontoFinSoles.Text = Format(Resultado + DblVuelto, "#,##0.00")
        If dtCabecera.Rows.Count > 0 Or Len(txtCodigo.Text.Trim) > 0 Then
            Dim iGastoTotalGeneral As Double = 0
            Dim idSesionCalc As String = ""
            If dtCabecera.Rows.Count > 0 Then
                idSesionCalc = Convert.ToString(dtCabecera.Rows(0).Item("IdSesion")).Trim
            End If
            If Len(txtCodigo.Text.Trim) > 0 Then
                idSesionCalc = Trim(txtCodigo.Text)
            End If
            Dim dtDetCaja As DataTable
            dtDetCaja = New DataTable
            eSesionCajas = New clsSesionCajas
            dtDetCaja = eSesionCajas.fListarGastosCaja(idSesionCalc, gEmpresa)
            If dtDetCaja.Rows.Count > 0 Or Len(txtCodigo.Text.Trim) > 0 Then
                For y As Integer = 0 To dtDetCaja.Rows.Count - 1
                    For z As Integer = 0 To dtDetCaja.Columns.Count - 1
                        If z = 6 Then
                            iGastoTotalGeneral = iGastoTotalGeneral + dtDetCaja.Rows(y).Item(z)
                        End If
                    Next
                Next
                txtGastoGeneral.Text = Format(iGastoTotalGeneral, "#,##0.00")
                If sSaldoReembolso.Trim = "" Then
                    sSaldoReembolso = "0"
                End If
                '''''''''''''''''''
                Dim dtDetCajaReem1 As DataTable
                dtDetCajaReem1 = New DataTable
                eSesionCajas = New clsSesionCajas
                dtDetCajaReem1 = eSesionCajas.fBuscarReembolso(idSesionCalc, gEmpresa)
                If dtDetCajaReem1.Rows.Count > 0 Then
                    sSaldoReembolso = "0"
                End If
                '''''''''''''''''''
                txtGastoGeneral.Text = Format(iGastoTotalGeneral, "#,##0.00")
                MontoGeneralFinal = Format(sMontoBase - iGastoTotalGeneral, "#,##0.00")
                txtSaldoGeneral.Text = Format(MontoGeneralFinal, "#,##0.00")
                MontoGeneralTotalGasto = Format(iGastoTotalGeneral, "#,##0.00")
                Dim iResActualizaCaja As Integer = 0
                iResActualizaCaja = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), 2, TBL, 0, gPeriodo, sTPlaCodigo, sEmprResposable, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
            End If
            If sSaldoReembolso.Trim = "" Then
                sSaldoReembolso = "0"
            End If
            '''''''''''''''''''
            Dim dtDetCajaReem As DataTable
            dtDetCajaReem = New DataTable
            eSesionCajas = New clsSesionCajas
            dtDetCajaReem = eSesionCajas.fBuscarReembolso(idSesionCalc, gEmpresa)
            If dtDetCajaReem.Rows.Count > 0 Then
                sSaldoReembolso = "0"
            End If
            '''''''''''''''''''
            txtGastoGeneral.Text = Format(iGastoTotalGeneral, "#,##0.00")
            If iGastoTotalGeneral = 0 Then
                txtSaldoGeneral.Text = Format(0, "#,##0.00")
            End If
            MontoGeneralFinal = Format(sMontoBase - iGastoTotalGeneral, "#,##0.00")
            txtSaldoGeneral.Text = Format(MontoGeneralFinal, "#,##0.00")
        Else
            MontoGeneralTotalGasto = Format(DblEgreso, "#,##0.00")
            MontoGeneralFinal = Format(Resultado, "#,##0.00")
            txtGastoGeneral.Text = Format(MontoGeneralTotalGasto, "#,##0.00")
            txtSaldoGeneral.Text = Format(MontoGeneralFinal + DblVuelto, "#,##0.00")
        End If
        'If dtCabecera.Rows.Count > 0 Then
        '    If vb.IsDBNull(dtCabecera.Rows(0).Item("CerrarCaja")) <> True Then
        '        If dtCabecera.Rows(0).Item("CerrarCaja") = 1 Then
        '            txtGastos.Text = ""
        '            txtMontoIniSoles.Text = ""
        '            txtMontoFinSoles.Text = ""
        '        End If
        '    End If
        'End If
    End Sub

    Private Sub dgvDetalleCajas_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleCajas.CellDoubleClick
        If dgvDetalleCajas.Rows.Count > 0 Then
            If dgvVales.Rows.Count > 0 Then
                For x As Integer = 0 To dgvVales.RowCount - 1
                    dgvVales.Rows.Remove(dgvVales.CurrentRow)
                Next
            End If
            Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
            If columna = 13 Then
                If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(3).Value) = "04 - VALE" Then
                    MessageBox.Show("No Puede Rendir un Vale", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    Dim tblBuscarVales As DataTable
                    tblBuscarVales = New DataTable
                    eSesionCajas = New clsSesionCajas
                    tblBuscarVales = eSesionCajas.BuscarVales(Trim(txtCodigo.Text), gEmpresa)
                    If tblBuscarVales.Rows.Count > 0 Then
                        For y As Integer = 0 To tblBuscarVales.Rows.Count - 1
                            dgvVales.Rows.Add()
                            For z As Integer = 0 To tblBuscarVales.Columns.Count - 1
                                If z = 4 Then
                                    If tblBuscarVales.Rows(y).Item(4).ToString = 1 Then
                                        dgvVales.Rows(y).Cells(z).Value = True
                                    ElseIf tblBuscarVales.Rows(y).Item(4).ToString = 0 Then
                                        dgvVales.Rows(y).Cells(z).Value = False
                                    End If
                                Else
                                    dgvVales.Rows(y).Cells(z).Value = tblBuscarVales.Rows(y).Item(z)
                                End If
                            Next
                        Next
                    End If
                    If tblBuscarVales.Rows.Count = 0 Then
                        MessageBox.Show("No Existe Vales a Rendir en esta Caja", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Panel4.Visible = False
                    ElseIf tblBuscarVales.Rows.Count > 0 Then
                        Panel4.Visible = True
                    End If
                End If
            End If
        End If
    End Sub



    Private Sub dgvDetalleCajas_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetalleCajas.KeyPress
         Dim A As Integer
        Select Case Asc(e.KeyChar)
            Case 13
                If dgvDetalleCajas.Rows.Count = 0 Then
                    dgvDetalleCajas.Rows.Add()
                    dgvDetalleCajas.Rows(0).Cells("Opcion").Value = "0"
                    dgvDetalleCajas.Rows(0).Cells("Fecha").Value = Now.Date().ToShortDateString
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, 0)
                Else
                    If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Selected = True Then 'Or dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(13).Selected = True Or dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(14).Selected = True Then
                        Dim tDocumento As String = dgvDetalleCajas.CurrentRow.Cells("TipoDocumento").Value

                        If tDocumento IsNot Nothing Then

                            If tDocumento.Substring(0, 2) = "02" Then
                                Dim importe As Decimal = Convert.ToDecimal(dgvDetalleCajas.CurrentRow.Cells("Total").Value)
                                Dim subtotal, igv, total As Decimal

                                subtotal = Round(importe / 1.18, 2, MidpointRounding.ToEven)
                                igv = Round(importe - subtotal, 2, MidpointRounding.ToEven)
                                total = Round(subtotal + igv, 2, MidpointRounding.ToEven)

                                If MessageBox.Show("Sub Total: " + subtotal.ToString + "  IGV: " + igv.ToString + "  Total: " + total.ToString + " �Seguro de grabar registro? ", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
                                    GrabarEnter()
                                End If
                            Else
                                GrabarEnter()
                            End If
                        Else
                            GrabarEnter()
                        End If
                    End If
                End If
        End Select

        Call Calcular()
    End Sub
    Private Sub GrabarEnter()
        Dim A As Integer

        Dim iResultado As Integer
        ValidarDatos2()
        If Grabar = 0 Then
            Exit Sub
        End If
        If txtCodigo.Text = "" Then
            If Grabar = 1 Then
                If iOpcion = 1 Then
                    eSesionCajas.fCodigo(gEmpresa)
                    sCodigoActual = eSesionCajas.sCodFuturo
                    txtCodigo.Text = sCodigoActual
                Else
                    sCodigoActual = txtCodigo.Text
                End If
                cargarTBL()

                If iOpcion = 1 Then
                    If sNumeroSesion = 1 Then
                        iResultado = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), iOpcion, TBL, sMontoBase, gPeriodo, sTPlaCodigo, sEmprResposable, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                    ElseIf sNumeroSesion > 1 Then
                        iResultado = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), iOpcion, TBL, 0, gPeriodo, sTPlaCodigo, sEmprResposable, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                    End If
                    If iResultado = 1 Then
                        Dim x As Integer = 0
                        TBL.Clear()
                        TBL.Reset()
                        TBL.Dispose()
                        For x = 0 To dgvDetalleCajas.RowCount - 1
                            dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                        Next
                        dtDetalle = New DataTable
                        dtDetalle = eSesionCajas.fListarDet(txtCodigo.Text, gEmpresa, iEstado01)
                        cargarDetalle()
                    End If
                End If
            End If
        Else
            If Grabar = 1 Then
                sCodigoActual = txtCodigo.Text.Trim
                Dim pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pIdSesion, pVale, pEmprPresta, pRuc, pRazonSocial, pOtrosDatos As String
                Dim Cantidad, pAnular, iResultadoActCab, pEstadoPrestamo As Integer
                iResultadoActCab = eSesionCajas.fActualizarCabecera(3, sCodigoActual, Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                cargarTBL()
                For v As Integer = 0 To TBL.Rows.Count - 1
                    dtDataBD = New DataTable
                    If vb.IsDBNull(TBL.Rows(v).Item(0)) = True Then
                        pFecha = Today()
                    Else
                        pFecha = TBL.Rows(v).Item(0)
                    End If
                    pTipoDoc = TBL.Rows(v).Item(1)
                    If vb.IsDBNull(TBL.Rows(v).Item(2)) = True Then
                        pNumeroDoc = ""
                    Else
                        pNumeroDoc = TBL.Rows(v).Item(2)
                    End If
                    pTipoGasto = TBL.Rows(v).Item(3)
                    If vb.IsDBNull(TBL.Rows(v).Item(4)) = True Then
                        pDescripcion = ""
                    Else
                        pDescripcion = TBL.Rows(v).Item(4)
                    End If
                    pCentroCosto = TBL.Rows(v).Item(5)
                    If vb.IsDBNull(TBL.Rows(v).Item(6)) = True Then
                        pTotal = "00.00"
                    Else
                        pTotal = TBL.Rows(v).Item(6)
                    End If
                    If vb.IsDBNull(TBL.Rows(v).Item(7)) = True Then
                        pIdDetalleMovimiento = ""
                    Else
                        pIdDetalleMovimiento = TBL.Rows(v).Item(7)
                    End If

                    If vb.IsDBNull(TBL.Rows(v).Item(9)) = True Then
                        pAnular = ""
                    Else
                        If dgvDetalleCajas.Rows(v).Cells("Anular").Value = False Then
                            pAnular = 0
                        Else
                            pAnular = 1
                        End If
                    End If

                    If vb.IsDBNull(TBL.Rows(v).Item(10)) = True Then
                        pVale = ""
                    Else
                        pVale = TBL.Rows(v).Item(10)
                    End If

                    If vb.IsDBNull(TBL.Rows(v).Item(11)) = True Then
                        pEmprPresta = ""
                        'MessageBox.Show(TBL.Rows(v).Item(11))
                    Else
                        pEmprPresta = TBL.Rows(v).Item(11)
                        'MessageBox.Show(TBL.Rows(v).Item(11))
                    End If

                    If vb.IsDBNull(TBL.Rows(v).Item(12)) = True Then
                        pEstadoPrestamo = ""
                        'MessageBox.Show(TBL.Rows(v).Item(12))
                    Else
                        pEstadoPrestamo = TBL.Rows(v).Item(12)
                        'MessageBox.Show(TBL.Rows(v).Item(12))
                    End If

                    If vb.IsDBNull(TBL.Rows(v).Item(13)) = True Then
                        pRuc = ""
                    Else
                        pRuc = TBL.Rows(v).Item(13)
                    End If

                    If vb.IsDBNull(TBL.Rows(v).Item(14)) = True Then
                        pRazonSocial = ""
                    Else
                        pRazonSocial = TBL.Rows(v).Item(14)
                    End If
                    If vb.IsDBNull(TBL.Rows(v).Item(15)) = True Then
                        pOtrosDatos = ""
                    Else
                        pOtrosDatos = TBL.Rows(v).Item(15)
                    End If

                    'MessageBox.Show(TBL.Rows(v).Item(8))
                    pIdSesion = sCodigoActual
                    If TBL.Rows(v).Item(8) = "0" Then
                        eSesionCajas.fGrabarItemNuevo(0, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdSesion, gEmpresa, pAnular, pVale, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                    Else
                        eSesionCajas.VerificarDatos(1, Convert.ToDateTime(pFecha), pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, Convert.ToDouble(pTotal), pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos)
                        Cantidad = eSesionCajas.iNroRegistros
                        If Cantidad = 0 Then
                            eSesionCajas.ActualizarDatos(2, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                            If Trim(pTipoDoc) = "04" And Trim(pNumeroDoc) <> "" Then
                                Dim nrovale As String = ""
                                nrovale = Trim(pNumeroDoc)
                                eSesionCajas.fActVale5(nrovale, Trim(txtCodigo.Text), gEmpresa, "", Convert.ToDouble(pTotal))
                            End If
                        End If
                    End If
                Next
                Dim x As Integer = 0
                TBL.Clear()
                TBL.Reset()
                TBL.Dispose()
                For x = 0 To dgvDetalleCajas.RowCount - 1
                    dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                Next
                dtDetalle = New DataTable
                dtDetalle = eSesionCajas.fListarDet(txtCodigo.Text, gEmpresa, iEstado01)
                cargarDetalle()
            End If 'If Grabar = 1 Then
        End If

        Dim IntCod As Integer = 0
        dgvDetalleCajas.Rows.Add()
        A = dgvDetalleCajas.Rows.Count
        If dgvDetalleCajas.Rows(A - 2).Cells("Opcion").Value = "0" Or dgvDetalleCajas.Rows(A - 2).Cells("Opcion").Value = "1" Then
            A = dgvDetalleCajas.Rows.Count
            dgvDetalleCajas.Rows(A - 1).Cells("Opcion").Value = "0"
            dgvDetalleCajas.Rows(A - 1).Cells(0).Value = Now.Date().ToShortDateString
            'If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index - 1).Cells(5).Value <> "" Then
            If dgvDetalleCajas.Rows(A - 2).Cells("CentroCosto").Value <> "" Then
                dgvDetalleCajas.Rows(A - 1).Cells("CentroCosto").Value = dgvDetalleCajas.Rows(A - 2).Cells("CentroCosto").Value
            End If
        Else
            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index + 1).Cells("Opcion").Value = "0"
            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value <> "" Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index + 1).Cells("IdGasto").Value = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
            End If
        End If
        dgvDetalleCajas.Rows(A - 1).Cells(0).Value = Now.Date().ToShortDateString
        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, A - 1)

    End Sub

    Private Sub dgvDetalleCajas_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalleCajas.KeyDown
        sVieneDe = 4
        Dim count As Integer = 0

        Select Case e.KeyCode
            Case Keys.Delete
                If dgvDetalleCajas.Rows.Count > 0 Then
                    If MessageBox.Show("� Desea Eliminar el Item ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        CodigoIdDetalle = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value
                        'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        'borrar compra de requerimientos
                        eCompraRequerimientos = New clsCompraRequerimientos
                        Dim dtDatosEnReq As DataTable
                        dtDatosEnReq = New DataTable
                        dtDatosEnReq = eCompraRequerimientos.fListar2(10, gEmpresa, "", "", "", Trim(CodigoIdDetalle), Trim(txtCodigo.Text))
                        If dtDatosEnReq.Rows.Count > 0 Then
                            'xDxDxD
                            Dim xOcoCodigo As String = ""
                            Dim xOcoNumero As String = ""
                            Dim xReqCodigo As String = ""
                            Dim xEmprCodigo As String = ""
                            Dim xIdReqDetalle As String = ""
                            Dim xCantidadUniSol As Decimal = 0
                            Dim xCantidadGrupoSol As Decimal = 0
                            For x As Integer = 0 To dtDatosEnReq.Rows.Count - 1
                                xOcoCodigo = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("OcoCodigo")))
                                xOcoNumero = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("OcoNumero")))
                                xReqCodigo = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("ReqCodigo")))
                                xEmprCodigo = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("EmprCodigo")))
                                xIdReqDetalle = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("IdReqDetalle")))
                                xCantidadUniSol = Convert.ToDecimal(dtDatosEnReq.Rows(x).Item("CantidadUniSol"))
                                xCantidadGrupoSol = Convert.ToDecimal(dtDatosEnReq.Rows(x).Item("CantidadGrupoSol"))
                                Dim iResultadoDet As Integer = 0
                                iResultadoDet = eCompraRequerimientos.fGrabarOrdenDet2(11, xOcoCodigo, xOcoNumero, xEmprCodigo, xReqCodigo, xIdReqDetalle, xCantidadUniSol, xCantidadGrupoSol)
                            Next
                        End If
                        'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx



                        If CodigoIdDetalle <> Nothing Then

                            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value <> "" Then
                                Dim nrovale As String = ""
                                nrovale = Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value)
                                eSesionCajas.fActVale3(nrovale, Trim(txtCodigo.Text), gEmpresa, "", 0)
                            End If

                            eSesionCajas.fEliminarItemDetalle(CodigoIdDetalle, Trim(txtCodigo.Text), gEmpresa, gUsuario)
                            '+++++++++++++++++++++
                            If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                                contar1 = 0
                            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                                contar2 = 0
                            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "04 - VALE" Then
                                Dim xxxNroVale As String = ""
                                xxxNroVale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                                If xxxNroVale <> Nothing Then
                                    eSesionCajas.fActVale4(Trim(xxxNroVale), Trim(txtCodigo.Text), gEmpresa, "", 0)
                                End If
                            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                                contar3 = 0
                            End If
                            '+++++++++++++++++++++
                            dgvDetalleCajas.Rows.RemoveAt(dgvDetalleCajas.CurrentRow.Index)
                            sCodigoActual = Me.txtCodigo.Text
                            Calcular()
                            eSesionCajas.fActualizarCabecera(3, sCodigoActual, Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), Convert.ToDecimal(txtMontoIniSoles.Text), Convert.ToDecimal(txtGastos.Text), Convert.ToDecimal(txtMontoFinSoles.Text), Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                        Else
                            'MsgBox(UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(1).Value))
                            If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "04 - VALE" Then
                                Dim xxxNroVale As String = ""
                                xxxNroVale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                                If xxxNroVale <> Nothing Then
                                    eSesionCajas.fActVale4(Trim(xxxNroVale), Trim(txtCodigo.Text), gEmpresa, "", 0)
                                End If
                            End If

                            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value <> "" Then
                                Dim nrovale As String = ""
                                nrovale = Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value)
                                eSesionCajas.fActVale3(nrovale, Trim(txtCodigo.Text), gEmpresa, "", 0)
                            End If

                            '+++++++++++++++++++++
                            If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                                contar1 = 0
                            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                                contar2 = 0
                            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                                contar3 = 0
                            End If
                            '+++++++++++++++++++++
                            dgvDetalleCajas.Rows.RemoveAt(dgvDetalleCajas.CurrentRow.Index)
                        End If

                    End If

                Else
                    MessageBox.Show("No Existe ningun registro para eliminar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Case Keys.F4

                If dgvDetalleCajas.Rows.Count > 0 Then
                    '-------------------------------------------
                    Dim strvale = ""
                    Dim strdocumento = ""
                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = 0 Then
                        strvale = ""
                    Else
                        strvale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
                    End If
                    If strvale = "04 - VALE" Then
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                            strdocumento = ""
                        Else
                            strdocumento = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                        End If
                        If strdocumento <> "" Then
                            Dim tblBuscarVale As DataTable
                            tblBuscarVale = New DataTable
                            eSesionCajas = New clsSesionCajas
                            tblBuscarVale = eSesionCajas.BuscarDatosdeVale(Trim(strdocumento), Trim(txtCodigo.Text), gEmpresa)
                            If tblBuscarVale.Rows.Count > 0 Then
                                If Microsoft.VisualBasic.IsDBNull(tblBuscarVale.Rows(0).Item("ANombre")) = True Then
                                    MessageBox.Show("Para Imprimir : Ingrese / Seleccione un Nombre", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                                    Panel3.Visible = True
                                    Dim tblBuscarNombres As DataTable
                                    tblBuscarNombres = New DataTable
                                    eSesionCajas = New clsSesionCajas
                                    tblBuscarNombres = eSesionCajas.BuscarNombres(sIdCaja, gEmpresa)

                                    'If dgvNombres.Rows.Count > 0 Then
                                    '    For x As Integer = 0 To dgvNombres.RowCount - 1
                                    '        dgvNombres.Rows.Remove(dgvNombres.CurrentRow)
                                    '    Next
                                    'End If

                                    'If tblBuscarNombres.Rows.Count > 0 Then
                                    dgvNombres.DataSource = tblBuscarNombres
                                    'End If
                                    txtNombre.Clear()
                                    txtNombre.Focus()
                                Else
                                    'q imprima nomas
                                    Dim x As frmReporteGasto = frmReporteGasto.Instance
                                    x.MdiParent = frmPrincipal
                                    x.Entregadoa = Trim(tblBuscarVale.Rows(0).Item("ANombre"))
                                    If vb.IsDBNull(gDesEmpresa) = True Then
                                        x.prmEmpresa = ""
                                    Else
                                        x.prmEmpresa = gDesEmpresa
                                    End If
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value) = 0 Then
                                        x.prmFecha = ""
                                    Else
                                        x.prmFecha = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
                                    End If
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = 0 Then
                                        x.prmDocumento = ""
                                    Else
                                        x.prmDocumento = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
                                    End If
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                                        x.prmNroDocumento = ""
                                    Else
                                        x.prmNroDocumento = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                                    End If
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = 0 Then
                                        x.prmTipoGasto = ""
                                    Else
                                        x.prmTipoGasto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
                                    End If
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value) = 0 Then
                                        x.prmDescGasto = ""
                                    Else
                                        x.prmDescGasto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value
                                    End If
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value) = 0 Then
                                        x.prmCentroCosto = ""
                                    Else
                                        x.prmCentroCosto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value
                                    End If
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) = 0 Then
                                        x.prmTotal = ""
                                    Else
                                        x.prmTotal = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value
                                    End If
                                    x.prmArea = Trim(sDesArea)
                                    x.prmResponsable = Trim(sDesEmpleado)
                                    x.prmNroCaja = Trim(sIdCaja)
                                    x.prmDesCaja = Trim(sDesCaja)
                                    x.prmNroSesion = Trim(sNumeroSesion)
                                    x.prmMonSimbolo = Trim(sMonSimbolo)
                                    If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                                        x.prmNroVale = ""
                                    Else
                                        x.prmNroVale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                                    End If
                                    x.Show()
                                End If
                            End If
                        End If

                    Else
                        Dim x As frmReporteGasto = frmReporteGasto.Instance
                        x.MdiParent = frmPrincipal
                        If vb.IsDBNull(gDesEmpresa) = True Then
                            x.prmEmpresa = ""
                        Else
                            x.prmEmpresa = gDesEmpresa
                        End If
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value) = 0 Then
                            x.prmFecha = ""
                        Else
                            x.prmFecha = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
                        End If
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = 0 Then
                            x.prmDocumento = ""
                        Else
                            x.prmDocumento = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
                        End If
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                            x.prmNroDocumento = ""
                        Else
                            x.prmNroDocumento = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                        End If
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = 0 Then
                            x.prmTipoGasto = ""
                        Else
                            x.prmTipoGasto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
                        End If
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value) = 0 Then
                            x.prmDescGasto = ""
                        Else
                            x.prmDescGasto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value
                        End If
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value) = 0 Then
                            x.prmCentroCosto = ""
                        Else
                            x.prmCentroCosto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value
                        End If
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) = 0 Then
                            x.prmTotal = ""
                        Else
                            x.prmTotal = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value
                        End If
                        x.prmArea = Trim(sDesArea)
                        x.prmResponsable = Trim(sDesEmpleado)
                        x.prmNroCaja = Trim(sIdCaja)
                        x.prmDesCaja = Trim(sDesCaja)
                        x.prmNroSesion = Trim(sNumeroSesion)
                        x.prmMonSimbolo = Trim(sMonSimbolo)
                        If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                            x.prmNroVale = ""
                        Else
                            x.prmNroVale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                        End If
                        x.Show()
                    End If
                End If
            Case Keys.F5
                '''''''''''
                If dgvDetalleCajas.Rows.Count > 0 Then
                    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value) = "" Then
                        MessageBox.Show("Este Registro no esta grabado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "" Then
                        MessageBox.Show("Este Registro no es un Comprobante", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = "" Then
                        MessageBox.Show("Este Comprobante no tiene Numero", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    eCompraRequerimientos = New clsCompraRequerimientos
                    Dim dtDatosEnReq As DataTable
                    dtDatosEnReq = New DataTable
                    dtDatosEnReq = eCompraRequerimientos.fListar2(10, gEmpresa, "", "", "", Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value), Trim(txtCodigo.Text))
                    If dtDatosEnReq.Rows.Count > 0 Then
                        MessageBox.Show("Este Comprobante ya esta enlazado con una compra de Requerimientos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    Dim x As frmCajaChicaRequerimientos2 = frmCajaChicaRequerimientos2.Instance
                    x.Owner = Me
                    'x.NroForm = 1
                    x.IdCaja = Trim(txtCodigo.Text) 'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                    x.IdDetCaja = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value
                    x.TipoDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
                    x.FechaFac = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
                    x.RucDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Ruc").Value
                    x.NroDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                    x.RazonDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value
                    x.MonedaDoc = Trim(sMonedaCaja)
                    x.ShowInTaskbar = False
                    x.ShowDialog()

                End If

            Case Keys.F6
                '''''''''''
                If dgvDetalleCajas.Rows.Count > 0 Then
                    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value) = "" Then
                        MessageBox.Show("Este Registro no esta grabado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "" Then
                        MessageBox.Show("Este Registro no es un Comprobante", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = "" Then
                        MessageBox.Show("Este Comprobante no tiene Numero", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    'eCompraRequerimientos = New clsCompraRequerimientos
                    'Dim dtDatosEnReq As DataTable
                    'dtDatosEnReq = New DataTable
                    'dtDatosEnReq = eCompraRequerimientos.fListar2(10, gEmpresa, "", "", "", Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value), Trim(txtCodigo.Text))
                    'If dtDatosEnReq.Rows.Count > 0 Then
                    '    MessageBox.Show("Este Comprobante ya esta enlazado con una compra de Requerimientos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    '    Exit Sub
                    'End If
                    eCompraRequerimientos = New clsCompraRequerimientos
                    Dim dtDatosEnReq As DataTable
                    dtDatosEnReq = New DataTable
                    dtDatosEnReq = eCompraRequerimientos.fListar2(12, gEmpresa, "", "", "", Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value), Trim(txtCodigo.Text))
                    If dtDatosEnReq.Rows.Count = 0 Then
                        MessageBox.Show("Este Comprobante no tiene Detalle de Requerimiento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                        'MessageBox.Show("Este Comprobante ya esta enlazado con una compra de Requerimientos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        'Exit Sub
                        'dgvReqAgregados.DataSource = dtDatosEnReq

                    End If

                    Dim x As frmCajaChicaRequerimientos3 = frmCajaChicaRequerimientos3.Instance
                    x.Owner = Me
                    'x.NroForm = 1
                    x.IdCaja = Trim(txtCodigo.Text) 'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                    x.IdDetCaja = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value

                    x.TipoDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
                    x.FechaFac = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
                    x.RucDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Ruc").Value
                    x.NroDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
                    x.RazonDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value
                    x.MonedaDoc = Trim(sMonedaCaja)
                    x.ShowInTaskbar = False
                    x.ShowDialog()

                End If

                'txtCodCC.Text = Trim(CodCC)
                'txtCodCC_TextChanged(sender, e)

        End Select
        Call Calcular()
    End Sub

    Private Sub dgvDetalleCajas_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleCajas.CellEndEdit
        Dim StrCad As String = String.Empty
        If e.ColumnIndex = 6 Then
            If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                contar1 = contar1 + 1
            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                contar2 = contar2 + 1
            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                contar3 = contar3 + 1
            End If
            If contar1 > 1 And UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                MessageBox.Show("Ya existe Registros con este tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
                Exit Sub
            End If
            If contar2 > 1 And UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                MessageBox.Show("Ya existe Registros con este tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
                Exit Sub
            End If
            If contar3 > 1 And UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                MessageBox.Show("Ya existe Registros con este tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
                Exit Sub
            End If
        End If

        If e.ColumnIndex = 4 Then
            StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
            If StrCad = Nothing Then Exit Sub
            IdDocumentoCorto = StrCad.Substring(0, StrCad.IndexOf("-")).Trim
            If IdDocumentoCorto = "04" Then
                If txtCodigo.Text <> "" Then
                    eSesionCajas.fCodigoVale(1, Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value = eSesionCajas.sCodVale
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").ReadOnly = True
                    'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(9).Value=
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Anular").Value() = True
                    eSesionCajas.fGrabarVale(eSesionCajas.sCodVale, 2, Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
                End If
            Else
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").ReadOnly = False
                'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value = ""
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Anular").Value() = False
            End If
        ElseIf e.ColumnIndex = 5 Then

            'Dim CadenaCon As String
            If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = "" Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value = ""
            End If

        ElseIf e.ColumnIndex = 6 Then
            StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
            If StrCad = Nothing Then Exit Sub
            IdGastoCorto = StrCad.Substring(0, StrCad.IndexOf("-")).Trim
            If IdGastoCorto = "1" Then
                If sCantidadSesiones = 0 Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = sMontoBase
                Else
                    'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(6).Value = Format(sMontoBase - Val(sSaldoAnterior), "#,##0.00")
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = Format(sMontoBase, "0.00")
                End If
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = "APORTE INICIAL"
            ElseIf IdGastoCorto = "2" Then
                If Val(sSaldoAnterior) <= 0 Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = "0.00"
                Else
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = sSaldoAnterior 'deberia traer la diferencia entre sMontoMaximo-sSaldoAnterior
                End If
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = "SALDO ANTERIOR"
            ElseIf IdGastoCorto = "3" Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = sSaldoReembolso
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = "REEMBOLSO"
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                Dim iResActReembolsoSesion As Int16 = 0
                eSesionCajas = New clsSesionCajas
                iResActReembolsoSesion = eSesionCajas.fActualizarCabecera2(6, Trim(txtCodigo.Text), Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), Val(sSaldoReembolso), MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), gUsuario)

            Else
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(4).Value = ""
            End If
        ElseIf e.ColumnIndex = 7 Then

            Dim CadenaCon As String
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value
            End If

            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = vb.UCase(CadenaCon)

        ElseIf e.ColumnIndex = 8 Then

            Dim CadenaOtrosD As String
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("OtrosD").Value) = True Then
                CadenaOtrosD = ""
            Else
                CadenaOtrosD = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("OtrosD").Value
            End If

            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("OtrosD").Value = vb.UCase(CadenaOtrosD)


        ElseIf e.ColumnIndex = 1 Then
            Dim CadenaCon As String = ""
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Ruc").Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Ruc").Value
            End If
            If Len(CadenaCon) = 11 Then
                'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value = ""
                ''--------------------------DESHABILITADO PARALA NO VALIDACION
                Dim Codigo As String = ""
                Dim CodAcreedor As String = ""
                Dim CodRuc As String = ""
                dtTable = New DataTable
                CodRuc = Trim(CadenaCon)
                CodAcreedor = "P"
                eRegistroDocumento = New clsRegistroDocumento
                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)
                If dtTable.Rows.Count > 0 Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value = vb.UCase(Trim(dtTable.Rows(0).Item("AnaliticoDescripcion")))
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").ReadOnly = False
                Else
                    MessageBox.Show("El Proveedor con RUC " & Trim(CodRuc) & " no esta Registrado, Registrelo ahora con su Raz�n Social.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value = ""
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").ReadOnly = False
                    'dgvDetalleCajas.CurrentCell = dgvDetalleCajas(2, dgvDetalleCajas.CurrentRow.Index)
                    'dgvDetalleCajas.BeginEdit(True)
                End If
            Else
                If Len(CadenaCon) > 0 Then
                    MessageBox.Show("Ingrese un Ruc V�lido.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value = ""
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").ReadOnly = False
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(1, dgvDetalleCajas.CurrentRow.Index)
                    dgvDetalleCajas.BeginEdit(True)
                End If
                Exit Sub
            End If

        ElseIf e.ColumnIndex = 3 Then

            Dim CadenaCon As String
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value
            End If



            '''''''''''''''''''''''''''''''''
            Dim CadenaRuc As String = ""
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Ruc").Value) = True Then
                CadenaRuc = ""
            Else
                CadenaRuc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Ruc").Value
            End If
            If Len(CadenaRuc) = 11 Then
                Dim Codigo As String = ""
                Dim CodAcreedor As String = ""
                Dim CodRuc As String = ""
                dtTable = New DataTable
                CodRuc = Trim(CadenaRuc)
                CodAcreedor = "P"
                eRegistroDocumento = New clsRegistroDocumento
                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)

                Dim dtTable2 As DataTable
                dtTable2 = New DataTable
                eRegistroDocumento = New clsRegistroDocumento
                dtTable2 = eRegistroDocumento.fListarProveedoresxRazonSocial(gEmpresa, CodRuc, CodAcreedor, Trim(vb.UCase(Trim(CadenaCon))))
                If dtTable2.Rows.Count > 0 Then
                    MessageBox.Show("Ya Existe el Proveedor " & Trim(vb.UCase(Trim(CadenaCon))) & " con Ruc " & Trim(dtTable2.Rows(0).Item("Ruc")) & ", Cambie de Raz�n Social para el actual Proveedor.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value = ""
                    Exit Sub
                End If

                If dtTable.Rows.Count > 0 Then
                    If vb.UCase(Trim(CadenaCon)) <> vb.UCase(Trim(dtTable.Rows(0).Item("AnaliticoDescripcion"))) Then
                        Dim iResultado As Integer = 0
                        iResultado = eRegistroDocumento.GrabarAcreedores(2, gEmpresa, Trim(CodAcreedor), Trim(CodRuc), Trim(vb.UCase(Trim(CadenaCon))), "", Trim(CodRuc), "", "J", "", "")
                    End If

                End If
            End If
            '''''''''''''''''''''''''''''''''
            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("RazonSocial").Value = vb.UCase(Trim(CadenaCon))

        ElseIf e.ColumnIndex = 7 Then
            'StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value
            'If StrCad = Nothing Then Exit Sub
            'IdCCostoCorto = StrCad.Substring(0, StrCad.IndexOf("-")).Trim
        ElseIf e.ColumnIndex = 10 Then
            If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) <> "" Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = Format(CDec(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) = True Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = Format(CDec(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value.ToString), "#,##0.00")
                End If
            Else
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = "0.00"
            End If
        ElseIf e.ColumnIndex = 0 Then
            StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
            If StrCad = Nothing Then Exit Sub
            Try
                If Convert.ToDateTime(StrCad) < Convert.ToDateTime(dtpFechaInicio.Text) Then
                    MessageBox.Show("La Fecha de Registro debe ser Mayor o igual a la Fecha de Apertura", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value = ""
                Else
                    'hacer que cuando se ingrese una fecha se compare con todas las del grid y colocar la fecha mayor en el datapicker final
                    '---------------------------------------
                    Dim i As Integer
                    Dim FechaMayor As DateTime
                    If dgvDetalleCajas.Rows.Count = 1 Then
                        FechaMayor = dgvDetalleCajas.Rows(0).Cells("Fecha").Value
                    Else
                        FechaMayor = dgvDetalleCajas.Rows(0).Cells("Fecha").Value
                        For i = 0 To dgvDetalleCajas.Rows.Count - 1
                            'MessageBox.Show(dgvDetalleCajas.Rows(i).Cells(0).Value)
                            If Trim(dgvDetalleCajas.Rows(i).Cells("Fecha").Value) <> "" Then
                                'dgvDetalleCajas.Rows(i).Cells(0).Value = FechaMayor
                                If (Convert.ToDateTime(dgvDetalleCajas.Rows(i).Cells("Fecha").Value) > FechaMayor) Then
                                    FechaMayor = Convert.ToDateTime(dgvDetalleCajas.Rows(i).Cells("Fecha").Value)
                                End If
                            End If

                        Next
                    End If
                    dtpFechaFin.Value = FechaMayor
                    '---------------------------------------
                End If
            Catch ex As Exception
                MessageBox.Show("Ingrese una Fecha Valida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index)
                'dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index - 1)
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value = ""
            End Try
        End If
        Call Calcular()
        'End If
    End Sub

    Private Sub dgvDetalleCajas_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalleCajas.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    'Dim xRuc As String = ""

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)


        Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
        If columna = 9 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = "-") Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

        If columna = 4 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter = ChrW(Keys.Space)) = False And (caracter.ToString <> "-") Then
                'Me.Text = e.KeyChar
                e.KeyChar = Chr(0)
            End If
        End If

        If columna = 0 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
                e.KeyChar = Chr(0)
            End If
        End If

        If columna = 1 Then

            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Then
                e.Handled = False
            Else
                e.Handled = True
            End If

            'If (Char.IsNumber(caracter)) Or (Len(Trim(xRuc))) <= 11 Then
            '    xRuc = xRuc & caracter
            '    e.Handled = False
            'ElseIf caracter = ChrW(Keys.Back) Then
            '    e.Handled = False
            'Else
            '    e.Handled = True
            'End If

        End If


    End Sub

    Private Sub chkCerrarCaja_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCerrarCaja.CheckedChanged
        'btnGrabar_Click_1(sender, e)
        'if Grabar=1then
        If chkCerrarCaja.Checked = True Then
            sVieneDe = 3
            btnGrabar_Click_1(sender, e)
            If Grabar = 0 Then
                chkCerrarCaja.Checked = False
            End If
        Else
            dtpFechaFin.Enabled = True
        End If
    End Sub

    Sub ValidarFechas()
        If chkCerrarCaja.Checked = True Then
            dtpFechaFin.Enabled = False

            Dim i As Integer
            Dim FechaGrid As DateTime
            Dim TipoDoc, Fecha As String
            For i = 0 To dgvDetalleCajas.Rows.Count - 1

                Fecha = Convert.ToString(dgvDetalleCajas.Rows(i).Cells(0).Value)
                If Trim(Fecha) = "" Then
                    MsgBox("Ingrese una Fecha")
                    chkCerrarCaja.Checked = False
                    dtpFechaFin.Enabled = True
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, i)
                    Exit For
                End If

                Try
                    FechaGrid = Convert.ToDateTime(dgvDetalleCajas.Rows(i).Cells(0).Value)
                Catch ex As Exception
                    MsgBox("Ingrese una Fecha Valida")
                    chkCerrarCaja.Checked = False
                    dtpFechaFin.Enabled = True
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, i)
                    Exit For
                End Try

                If FechaGrid > dtpFechaFin.Value Or FechaGrid < dtpFechaInicio.Value Then
                    MsgBox("Existe un Registro de Fecha que no esta en el rango de Apertura y Cierre de Caja")
                    chkCerrarCaja.Checked = False
                    dtpFechaFin.Enabled = True
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, i)
                    Exit For
                End If

                TipoDoc = Convert.ToString(dgvDetalleCajas.Rows(i).Cells(3).Value)
                If Trim(TipoDoc) = "" Then
                    MsgBox("Seleccione un Tipo de Documento")
                    chkCerrarCaja.Checked = False
                    dtpFechaFin.Enabled = True
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(3, i)
                    Exit For
                End If

            Next
        Else
            dtpFechaFin.Enabled = True
        End If
    End Sub

    Sub ValidarDatos()
        Dim count = 0

        If dgvDetalleCajas.Rows.Count = 0 Then
            MessageBox.Show("Para Arquear deben haber Detalles en la Caja", "Sistema de Tesorer�a")
            Grabar = 0
        Else
            For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1
                If dgvDetalleCajas.Rows(x).Cells("IdGasto").Value = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(5, x)
                    Exit Sub
                    Grabar = 0
                End If

                If dgvDetalleCajas.Rows(x).Cells("CentroCosto").Value = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(8, x)
                    Grabar = 0
                    Exit Sub
                End If

                If UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "1 - OTROS - APORTE INICIAL" And UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "2 - OTROS - SALDO ANTERIOR" And UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "3 - OTROS - REEMBOLSO" Then

                    Dim tDocumento As String = dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value

                    If Val(dgvDetalleCajas.Rows(x).Cells("Total").Value) <= 0 AndAlso tDocumento.Substring(0, 2) <> "05" Then
                        MessageBox.Show("Se ha encontrado Registros de Gasto sin Monto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(9, x)
                        Exit Sub
                        Grabar = 0
                    End If

                    If Val(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) = "05" Then
                        dgvDetalleCajas.Rows(x).Cells("Total").Value = IIf(Convert.ToDecimal(dgvDetalleCajas.Rows(x).Cells("Total").Value) < 0, Convert.ToDecimal(dgvDetalleCajas.Rows(x).Cells("Total").Value), Convert.ToDecimal(dgvDetalleCajas.Rows(x).Cells("Total").Value) * -1)
                    End If

                    Grabar = 1
                    count = count + 1
                Else
                    count = 1
                    Grabar = 0
                End If
            Next
            If count > 0 Then
                Panel2.Visible = True
                GroupBox2.Text = "Arqueo de Hoy: " & Now.Date().ToShortDateString
                txtEfectivoDia.Focus()
                Calcular2()
                txtEncargado.Text = lblEmpleado.Text
                txtRevisado.Text = lblEmpleado.Text
                Grabar = 1
            End If
        End If
    End Sub


    Sub ValidarDatos2()
        Dim sCadenaMsj As String = ""
        Dim count = 0
        If dgvDetalleCajas.Rows.Count = 0 Then
            If sVieneDe = 1 Then
                sCadenaMsj = "Grabar"
            ElseIf sVieneDe = 2 Then
                sCadenaMsj = "Arquear"
            ElseIf sVieneDe = 3 Then
                sCadenaMsj = "Cerrar la Caja"
            End If
            If sVieneDe <> 4 Then
                MessageBox.Show("Para " & sCadenaMsj & " deben haber Detalles en la Caja", "Sistema de Tesorer�a")
            End If
            Grabar = 0
        Else
            dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, 0)
            For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1

                If Len(Trim(dgvDetalleCajas.Rows(x).Cells("Ruc").Value)) = 11 And Len(Trim(dgvDetalleCajas.Rows(x).Cells("RazonSocial").Value)) = 0 Then
                    MessageBox.Show("Validar Ruc", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(2, x)
                    Grabar = 0
                    Exit Sub
                End If

                If (Trim(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) <> "" And Trim(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) <> "07 - OTROS") And Trim(dgvDetalleCajas.Rows(x).Cells("DocumentoReferencia").Value) = "" Then
                    MessageBox.Show("Ingrese un Numero de Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(4, x)
                    Grabar = 0
                    Exit Sub
                End If

                If Trim(dgvDetalleCajas.Rows(x).Cells("DocumentoReferencia").Value) <> "" And Trim(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) = "" Then
                    MessageBox.Show("Seleccione un Tipo de Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(3, x)
                    Grabar = 0
                    Exit Sub
                End If

                If Trim(dgvDetalleCajas.Rows(x).Cells("Fecha").Value) = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Fecha de Comprobante", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, x)
                    Grabar = 0
                    Exit Sub
                End If

                If dgvDetalleCajas.Rows(x).Cells("IdGasto").Value = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(5, x)
                    Grabar = 0
                    Exit Sub
                End If

                If dgvDetalleCajas.Rows(x).Cells("CentroCosto").Value = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(8, x)
                    Grabar = 0
                    Exit Sub
                End If

                If Convert.ToBoolean(dgvDetalleCajas.Rows(x).Cells("Empresas").Value) = True And Trim(dgvDetalleCajas.Rows(x).Cells("Empresas").Value) = "" Then
                    MessageBox.Show("Seleccione una Empresa a Prestar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(14, x)
                    Grabar = 0
                    Exit Sub
                End If

                If UCase(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) = "04 - VALE" Then
                    Dim xVale As String = ""
                    If Len(dgvDetalleCajas.Rows(x).Cells("DocumentoReferencia").Value) = 0 Then
                        xVale = ""
                    Else
                        xVale = dgvDetalleCajas.Rows(x).Cells("DocumentoReferencia").Value
                    End If
                    If xVale <> "" Then
                        Dim tblBuscarVale As DataTable
                        tblBuscarVale = New DataTable
                        eSesionCajas = New clsSesionCajas
                        tblBuscarVale = eSesionCajas.BuscarDatosdeVale(Trim(xVale), Trim(txtCodigo.Text), gEmpresa)
                        If tblBuscarVale.Rows.Count > 0 Then
                            If Microsoft.VisualBasic.IsDBNull(tblBuscarVale.Rows(0).Item("ANombre")) = True Then
                                MessageBox.Show("Imprima e Ingrese un responsable para el Vale " & dgvDetalleCajas.Rows(x).Cells("DocumentoReferencia").Value, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(4, x)
                                Grabar = 0
                                Exit Sub
                            End If
                        End If
                    End If
                End If

                If UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "1 - OTROS - APORTE INICIAL" And UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "2 - OTROS - SALDO ANTERIOR" And UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "3 - OTROS - REEMBOLSO" Then

                    Dim tDocumento As String = dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value

                    If Val(dgvDetalleCajas.Rows(x).Cells("Total").Value) <= 0 AndAlso tDocumento.Substring(0, 2) <> "05" Then
                        MessageBox.Show("Se ha encontrado Registros de Gasto sin Importe", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(9, x)
                        Grabar = 0
                        Exit Sub
                    End If

                    If Val(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) = "05" Then
                        dgvDetalleCajas.Rows(x).Cells("Total").Value = IIf(Convert.ToDecimal(dgvDetalleCajas.Rows(x).Cells("Total").Value) < 0, Convert.ToDecimal(dgvDetalleCajas.Rows(x).Cells("Total").Value), Convert.ToDecimal(dgvDetalleCajas.Rows(x).Cells("Total").Value) * -1)
                    End If

                    Grabar = 1
                    count = count + 1
                Else
                    count = 1
                End If
            Next
            If count > 0 Then
                Grabar = 1
            Else
                MessageBox.Show("No Existe Ningun Registro de Gasto en la Caja Actual", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Grabar = 0
            End If
        End If
    End Sub

    Private Sub btnIrArquear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIrArquear.Click
        txtEfectivoDia.Text = ""
        txtObservaciones.Text = ""
        cboEstado.SelectedIndex = -1

        sVieneDe = 2
        ValidarDatos()
    End Sub


    Sub Calcular2()
        DblEgresoDelDia = 0
        DblAperturaDelDia = 0
        DblSaldoCaja = 0
        For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1
            If Convert.ToBoolean(dgvDetalleCajas.Rows(x).Cells("Anular").Value) = False Then

                If UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Or UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Or UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                    DblAperturaDelDia = DblAperturaDelDia + Val(Convert.ToDouble(dgvDetalleCajas.Rows(x).Cells("Total").Value))
                ElseIf UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "9 - OTROS - INGRESO" Then

                Else
                    DblEgresoDelDia = DblEgresoDelDia + Val(Convert.ToDouble(dgvDetalleCajas.Rows(x).Cells("Total").Value))
                End If

            End If
        Next
        DblSaldoCaja = DblAperturaDelDia - DblEgresoDelDia
        txtGastosDia.Text = Format(DblEgresoDelDia, "#,##0.00")
        txtAperturaDia.Text = Format(DblAperturaDelDia, "#,##0.00")
        txtSaldoDia.Text = Format(DblSaldoCaja, "#,##0.00")
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        Panel2.Visible = False
    End Sub

    Private Sub txtEfectivoDia_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEfectivoDia.KeyPress
        Dim caracter As Char = e.KeyChar
        ' referencia a la celda   
        Dim txt As TextBox = CType(sender, TextBox)
        ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
        ' es el separador decimal, y que no contiene ya el separador   
        If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = "-") Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
            e.Handled = False
        ElseIf caracter = ChrW(Keys.Enter) Then
            txtObservaciones.Focus()
        Else
            e.Handled = True
        End If

        Select Case Asc(e.KeyChar)
            Case 13
                txtObservaciones.Focus()
        End Select
    End Sub

    'Private Sub txtEfectivoDia_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEfectivoDia.LostFocus
    '    Dim MontoEscrito As String = ""
    '    MontoEscrito = txtEfectivoDia.Text.Trim
    '    If MontoEscrito.Trim = "" Then
    '        MontoEscrito = 0
    '    End If
    '    txtEfectivoDia.Text = Format(Convert.ToDouble(MontoEscrito), "0.00")
    'End Sub

    Private Sub txtEfectivoDia_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEfectivoDia.TextChanged
        Dim DblEfectivoDia As Double
        Dim DblSobrante As Decimal
        Dim DblFaltante As Decimal

        'DblEfectivoDia = Val(txtEfectivoDia.Text)

        Try
            DblEfectivoDia = Format(Convert.ToDouble(Val(txtEfectivoDia.Text)), "#,##0.00")

            If DblEfectivoDia > Val(txtSaldoDia.Text) Then
                DblSobrante = Format(DblEfectivoDia - Val(Convert.ToDouble(txtSaldoDia.Text)), "#,##0.00")
            Else
                DblSobrante = 0
            End If

            If Val(txtSaldoDia.Text) > DblEfectivoDia Then
                DblFaltante = Format(Val(Convert.ToDouble(txtSaldoDia.Text)) - DblEfectivoDia, "#,##0.00")
            Else
                DblFaltante = 0
            End If

            txtSobrante.Text = Format(DblSobrante, "#,##0.00")
            txtFaltante.Text = Format(DblFaltante, "#,##0.00")

            If DblSobrante > 0 Then
                cboEstado.SelectedIndex = 2
            End If

            If DblFaltante > 0 Then
                cboEstado.SelectedIndex = 1
            End If

            If DblSobrante = 0 And DblFaltante = 0 Then
                cboEstado.SelectedIndex = 0
            End If

        Catch ex As Exception
        End Try



    End Sub

    Private Sub btnArquear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnArquear.Click
        Dim sCodigoArqueo As String
        Dim iResultado As Integer

        Dim iResultadoArqueo As Integer
        Dim dtIdArqueo As DataTable
        Dim IdArqueo As String

        If Len(txtEfectivoDia.Text) = 0 Or Len(txtAperturaDia.Text) = 0 Or Len(txtGastosDia.Text) = 0 Or Len(txtSaldoDia.Text) = 0 Or Len(txtSobrante.Text) = 0 Or Len(txtFaltante.Text) = 0 Or Len(txtSobrante.Text) = 0 Or Len(txtEncargado.Text) = 0 Or Len(txtRevisado.Text) = 0 Or Len(txtObservaciones.Text) = 0 Or Len(txtCodigo.Text) = 0 Or cboEstado.SelectedIndex = -1 Then
            If Len(txtEfectivoDia.Text) = 0 Then

                MessageBox.Show("Debe Ingresar el Efectivo de la Caja Actual", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtEfectivoDia.Focus()
                Exit Sub
            End If

            If Len(txtEncargado.Text) = 0 Then

                MessageBox.Show("Debe Ingresar el Nombre del Encargado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtEncargado.Focus()
                Exit Sub
            End If

            If Len(txtRevisado.Text) = 0 Then

                MessageBox.Show("Debe Ingresar el el Nombre del Supervisor", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtRevisado.Focus()
                Exit Sub
            End If

            If Len(txtObservaciones.Text) = 0 Then

                MessageBox.Show("Debe Ingresar una Observacion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtObservaciones.Focus()
                Exit Sub
            End If

            If Len(txtCodigo.Text) = 0 Then


                If (MessageBox.Show("�Esta seguro de Grabar la Caja y El Arqueo?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    'Dim DrFila As DataRow
                    'Dim i As Integer
                    AceptoGrabar = 1
                    frmPrincipal.sFlagGrabar = "1"
                    iOpcion = 1
                    If iOpcion = 1 Then
                        eSesionCajas.fCodigo(gEmpresa)
                        sCodigoActual = eSesionCajas.sCodFuturo
                        txtCodigo.Text = sCodigoActual
                    Else
                        sCodigoActual = Me.txtCodigo.Text
                    End If

                    cargarTBL()
                    If iOpcion = 1 Then
                        If sNumeroSesion = 1 Then
                            iResultado = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), iOpcion, TBL, sMontoBase, gPeriodo, sTPlaCodigo, sEmprResposable, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                        ElseIf sNumeroSesion > 1 Then
                            iResultado = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), iOpcion, TBL, 0, gPeriodo, sTPlaCodigo, sEmprResposable, txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                        End If
                        'iResultado = eSesionCajas.fGrabar(sCodigoActual, sNumeroSesion, Convert.ToString(sIdArea), sDesEmpleadoId, Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), MontoGeneralInicio, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), Convert.ToString(gEmpresa), iOpcion, TBL)
                        '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                        If iResultado = 1 Then
                            If chkCerrarCaja.Checked = True Then
                                btnGrabar.Enabled = False
                                btnImprimir.Enabled = False
                            Else
                                btnGrabar.Enabled = True
                                btnImprimir.Enabled = True
                            End If
                            'dtpFechaInicio.Enabled = False
                            'Dim y As Integer
                            'Dim z As Integer
                            Dim x As Integer = 0
                            TBL.Clear()
                            TBL.Reset()
                            TBL.Dispose()
                            For x = 0 To dgvDetalleCajas.RowCount - 1
                                dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                            Next
                            dtDetalle = New DataTable
                            dtDetalle = eSesionCajas.fListarDet(txtCodigo.Text, gEmpresa, iEstado01)
                            cargarDetalle()
                        End If
                    End If
                End If

                If AceptoGrabar = 1 Then
                    eArqueo = New clsArqueo
                    eArqueo.fCodigo()
                    sCodigoArqueo = eArqueo.sCodFuturo
                    txtCodigoArqueo.Text = sCodigoArqueo

                    iResultadoArqueo = eArqueo.fGrabar(sCodigoArqueo, Trim(txtCodigo.Text), Trim(cboEstado.SelectedValue), sIdCaja, sIdArea, gEmpresa, Trim(txtEfectivoDia.Text), Trim(txtAperturaDia.Text), Trim(txtGastosDia.Text), Trim(txtSaldoDia.Text), Trim(txtSobrante.Text), Trim(txtFaltante.Text), Trim(txtEncargado.Text), Trim(txtRevisado.Text), Trim(txtObservaciones.Text))
                    If iResultadoArqueo = 1 And iResultado = 1 Then
                        countGrabar = countGrabar + 1
                        MessageBox.Show("Se Grabo la Caja N�" & Trim(lblNumSesion.Text) & " y el Arqueo Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                End If

            End If
        Else
            eArqueo = New clsArqueo
            dtIdArqueo = New DataTable
            dtIdArqueo = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
            If dtIdArqueo.Rows.Count = 1 Then

                If (MessageBox.Show("�Esta seguro de Actualizar el Arqueo de Hoy?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    IdArqueo = Convert.ToString(dtIdArqueo.Rows(0).Item(0))
                    iResultadoActArq = eArqueo.ActualizarArqueoHoy(IdArqueo, Trim(cboEstado.SelectedValue), Trim(txtEfectivoDia.Text), Trim(txtAperturaDia.Text), Trim(txtGastosDia.Text), Trim(txtSaldoDia.Text), Trim(txtSobrante.Text), Trim(txtFaltante.Text), Trim(txtEncargado.Text), Trim(txtRevisado.Text), Trim(txtObservaciones.Text))

                    If iResultadoActArq = 1 Then
                        Dim pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pIdSesion, pVale, pEmprPresta, pRuc, pRazonSocial, pOtrosDatos As String
                        Dim Cantidad, pAnular, pEstadoPrestamo As Integer
                        Dim iResultadoActCab As Integer
                        iResultadoActCab = eSesionCajas.fActualizarCabecera(3, Trim(txtCodigo.Text), Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                        If iResultadoActCab = 1 Then
                            cargarTBL()
                            For x As Integer = 0 To TBL.Rows.Count - 1
                                dtDataBD = New DataTable
                                If vb.IsDBNull(TBL.Rows(x).Item(0)) = True Then
                                    pFecha = Today()
                                Else
                                    pFecha = TBL.Rows(x).Item(0)
                                End If
                                pTipoDoc = TBL.Rows(x).Item(1)
                                If vb.IsDBNull(TBL.Rows(x).Item(2)) = True Then
                                    pNumeroDoc = ""
                                Else
                                    pNumeroDoc = TBL.Rows(x).Item(2)
                                End If
                                pTipoGasto = TBL.Rows(x).Item(3)
                                If vb.IsDBNull(TBL.Rows(x).Item(4)) = True Then
                                    pDescripcion = ""
                                Else
                                    pDescripcion = TBL.Rows(x).Item(4)
                                End If
                                pCentroCosto = TBL.Rows(x).Item(5)
                                If vb.IsDBNull(TBL.Rows(x).Item(6)) = True Then
                                    pTotal = "00.00"
                                Else
                                    pTotal = TBL.Rows(x).Item(6)
                                End If
                                If vb.IsDBNull(TBL.Rows(x).Item(7)) = True Then
                                    pIdDetalleMovimiento = ""
                                Else
                                    pIdDetalleMovimiento = TBL.Rows(x).Item(7)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(9)) = True Then
                                    pAnular = ""
                                Else
                                    pAnular = TBL.Rows(x).Item(9)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(10)) = True Then
                                    pVale = ""
                                Else
                                    pVale = TBL.Rows(x).Item(10)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(11)) = True Then
                                    pEmprPresta = ""
                                Else
                                    pEmprPresta = TBL.Rows(x).Item(11)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(12)) = True Then
                                    pEstadoPrestamo = ""
                                Else
                                    pEstadoPrestamo = TBL.Rows(x).Item(12)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(13)) = True Then
                                    pRuc = ""
                                Else
                                    pRuc = TBL.Rows(x).Item(13)
                                End If

                                If vb.IsDBNull(TBL.Rows(x).Item(14)) = True Then
                                    pRazonSocial = ""
                                Else
                                    pRazonSocial = TBL.Rows(x).Item(14)
                                End If
                                If vb.IsDBNull(TBL.Rows(x).Item(15)) = True Then
                                    pOtrosDatos = ""
                                Else
                                    pOtrosDatos = TBL.Rows(x).Item(15)
                                End If

                                pIdSesion = Trim(txtCodigo.Text)
                                If TBL.Rows(x).Item(8) = "0" Then
                                    eSesionCajas.fGrabarItemNuevo(0, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdSesion, gEmpresa, pAnular, pVale, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                Else
                                    eSesionCajas.VerificarDatos(1, Convert.ToDateTime(pFecha), pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, Convert.ToDouble(pTotal), pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos)
                                    Cantidad = eSesionCajas.iNroRegistros
                                    If Cantidad = 0 Then
                                        eSesionCajas.ActualizarDatos(2, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                        If Trim(pTipoDoc) = "04" And Trim(pNumeroDoc) <> "" Then
                                            Dim nrovale As String = ""
                                            nrovale = Trim(pNumeroDoc)
                                            eSesionCajas.fActVale5(nrovale, Trim(txtCodigo.Text), gEmpresa, "", Convert.ToDouble(pTotal))
                                        End If
                                    End If
                                End If
                            Next

                            'Dim y As Integer
                            'Dim z As Integer
                            Dim b As Integer = 0
                            TBL.Clear()
                            TBL.Reset()
                            TBL.Dispose()
                            For b = 0 To dgvDetalleCajas.RowCount - 1
                                dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                            Next
                            dtDetalle = New DataTable
                            dtDetalle = eSesionCajas.fListarDet(txtCodigo.Text, gEmpresa, iEstado01)

                            cargarDetalle()
                            Timer2.Enabled = True
                            sTab = 0
                        End If
                        '++++++++++++++++FIN ACTUALIZAR LA SESION
                        MessageBox.Show("Se Actualizo El Arqueo Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Panel2.Visible = False
                    End If
                End If

            Else
                eArqueo.fCodigo()
                sCodigoArqueo = eArqueo.sCodFuturo
                txtCodigoArqueo.Text = sCodigoArqueo
                If (MessageBox.Show("�Esta seguro de Grabar el Arqueo de Hoy?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    iResultado = eArqueo.fGrabar(sCodigoArqueo, Trim(txtCodigo.Text), Trim(cboEstado.SelectedValue), sIdCaja, sIdArea, gEmpresa, Trim(txtEfectivoDia.Text), Trim(txtAperturaDia.Text), Trim(txtGastosDia.Text), Trim(txtSaldoDia.Text), Trim(txtSobrante.Text), Trim(txtFaltante.Text), Trim(txtEncargado.Text), Trim(txtRevisado.Text), Trim(txtObservaciones.Text))
                    If iResultado = 1 Then

                        If Len(txtCodigo.Text) > 0 Then
                            If dgvDetalleCajas.Rows.Count > 0 Then
                                Dim pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pIdSesion, pVale, pEmprPresta, pRuc, pRazonSocial, pOtrosDatos As String
                                Dim Cantidad, pAnular, pEstadoPrestamo As Integer
                                Dim iResultadoActCab As Integer
                                iResultadoActCab = eSesionCajas.fActualizarCabecera(3, Trim(txtCodigo.Text), Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), sMontoBase, MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), txtMontoIniSoles.Text, txtGastos.Text, txtMontoFinSoles.Text, gUsuario)
                                If iResultadoActCab = 1 Then
                                    '++++++++++++++++INICIO ACTUALIZAR LA SESION
                                    cargarTBL()
                                    For x As Integer = 0 To TBL.Rows.Count - 1
                                        dtDataBD = New DataTable
                                        If vb.IsDBNull(TBL.Rows(x).Item(0)) = True Then
                                            pFecha = Today()
                                        Else
                                            pFecha = TBL.Rows(x).Item(0)
                                        End If
                                        pTipoDoc = TBL.Rows(x).Item(1)
                                        If vb.IsDBNull(TBL.Rows(x).Item(2)) = True Then
                                            pNumeroDoc = ""
                                        Else
                                            pNumeroDoc = TBL.Rows(x).Item(2)
                                        End If
                                        pTipoGasto = TBL.Rows(x).Item(3)
                                        If vb.IsDBNull(TBL.Rows(x).Item(4)) = True Then
                                            pDescripcion = ""
                                        Else
                                            pDescripcion = TBL.Rows(x).Item(4)
                                        End If
                                        pCentroCosto = TBL.Rows(x).Item(5)
                                        If vb.IsDBNull(TBL.Rows(x).Item(6)) = True Then
                                            pTotal = "00.00"
                                        Else
                                            pTotal = TBL.Rows(x).Item(6)
                                        End If
                                        If vb.IsDBNull(TBL.Rows(x).Item(7)) = True Then
                                            pIdDetalleMovimiento = ""
                                        Else
                                            pIdDetalleMovimiento = TBL.Rows(x).Item(7)
                                        End If

                                        If vb.IsDBNull(TBL.Rows(x).Item(9)) = True Then
                                            pAnular = ""
                                        Else
                                            pAnular = TBL.Rows(x).Item(9)
                                        End If

                                        If vb.IsDBNull(TBL.Rows(x).Item(10)) = True Then
                                            pVale = ""
                                        Else
                                            pVale = TBL.Rows(x).Item(10)
                                        End If
                                        If vb.IsDBNull(TBL.Rows(x).Item(11)) = True Then
                                            pEmprPresta = ""
                                        Else
                                            pEmprPresta = TBL.Rows(x).Item(11)
                                        End If
                                        If vb.IsDBNull(TBL.Rows(x).Item(12)) = True Then
                                            pEstadoPrestamo = ""
                                        Else
                                            pEstadoPrestamo = TBL.Rows(x).Item(12)
                                        End If

                                        If vb.IsDBNull(TBL.Rows(x).Item(13)) = True Then
                                            pRuc = ""
                                        Else
                                            pRuc = TBL.Rows(x).Item(13)
                                        End If

                                        If vb.IsDBNull(TBL.Rows(x).Item(14)) = True Then
                                            pRazonSocial = ""
                                        Else
                                            pRazonSocial = TBL.Rows(x).Item(14)
                                        End If
                                        If vb.IsDBNull(TBL.Rows(x).Item(15)) = True Then
                                            pOtrosDatos = ""
                                        Else
                                            pOtrosDatos = TBL.Rows(x).Item(15)
                                        End If
                                        'pOtrosDatos

                                        pIdSesion = Trim(txtCodigo.Text)
                                        If TBL.Rows(x).Item(8) = "0" Then
                                            eSesionCajas.fGrabarItemNuevo(0, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdSesion, gEmpresa, pAnular, pVale, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                        Else
                                            eSesionCajas.VerificarDatos(1, Convert.ToDateTime(pFecha), pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, Convert.ToDouble(pTotal), pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos)
                                            Cantidad = eSesionCajas.iNroRegistros
                                            If Cantidad = 0 Then
                                                eSesionCajas.ActualizarDatos(2, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                                If Trim(pTipoDoc) = "04" And Trim(pNumeroDoc) <> "" Then
                                                    Dim nrovale As String = ""
                                                    nrovale = Trim(pNumeroDoc)
                                                    eSesionCajas.fActVale5(nrovale, Trim(txtCodigo.Text), gEmpresa, "", Convert.ToDouble(pTotal))
                                                End If
                                            End If
                                        End If
                                    Next

                                    'Dim y As Integer
                                    'Dim z As Integer
                                    Dim b As Integer = 0
                                    TBL.Clear()
                                    TBL.Reset()
                                    TBL.Dispose()
                                    For b = 0 To dgvDetalleCajas.RowCount - 1
                                        dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                                    Next
                                    dtDetalle = New DataTable
                                    dtDetalle = eSesionCajas.fListarDet(txtCodigo.Text, gEmpresa, iEstado01)
                                    cargarDetalle()
                                    Timer2.Enabled = True
                                    sTab = 0
                                End If
                            End If
                        End If
                        MessageBox.Show("Se Grabo El Arqueo Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Panel2.Visible = False
                    End If
                End If
            End If
        End If
        Calcular()
    End Sub

    Private Sub btnImprimirArqueo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimirArqueo.Click
        eArqueo = New clsArqueo
        Dim dtIdArqueo As DataTable
        dtIdArqueo = New DataTable
        Dim IdArqueo As String
        dtIdArqueo = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
        If dtIdArqueo.Rows.Count = 1 Then
            Dim x As frmReporteArqueo = frmReporteArqueo.Instance
            x.MdiParent = frmPrincipal
            IdArqueo = Convert.ToString(dtIdArqueo.Rows(0).Item(0))
            x.nroArqueo = Trim(IdArqueo)
            x.Show()
        Else
            MessageBox.Show("No Existe Arqueo del Dia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub

    Private Sub txtCodigoArqueo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigoArqueo.TextChanged
        If Len(txtCodigoArqueo.Text) > 0 Then
            btnImprimirArqueo.Enabled = True
        Else
            btnImprimirArqueo.Enabled = False
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Dim x As frmReporteCajaChica = frmReporteCajaChica.Instance
        x.MdiParent = frmPrincipal
        x.nroSesion = Trim(txtCodigo.Text)
        x.Show()
    End Sub

    Private Sub dgvDetalleCajas_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleCajas.CellContentClick
        'Calcular()
        Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
        If dgvDetalleCajas.Rows.Count > 0 Then
            Try
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index)
                'Calcular()
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
            Catch ex As Exception
            End Try
            'Calcular()
        End If
        If columna = 15 Then 'si el check prestamo es INACTIVADO la empresa prestada se blanquea
            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(columna).Value = False Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(14).Value = ""
            End If
        End If
        If columna <> 2 Then
            Calcular()
        Else
            If EstadoServicioruc = True Then

                objResultado = VerRuc(dgvDetalleCajas.CurrentRow.Cells("Ruc").Value.ToString())
                If objResultado.blnExiste = True Then
                    If objResultado.dtResultado.Rows(0)("Estado").ToString() = "ACTIVO" And objResultado.dtResultado.Rows(0)("Condicion").ToString() = "HABIDO" Then
                        dgvDetalleCajas.CurrentRow.Cells("RazonSocial").Value = objResultado.dtResultado.Rows(0)("RazonSocial").ToString()
                    Else
                        Dim V_frmValidarRuc As frmValidarRuc = New frmValidarRuc(objResultado.dtResultado.Rows(0)("Ruc").ToString(), objResultado.dtResultado.Rows(0)("RazonSocial").ToString(), objResultado.dtResultado.Rows(0)("Estado").ToString(), objResultado.dtResultado.Rows(0)("Condicion").ToString(), objResultado.dtResultado.Rows(0)("Direccion").ToString())
                        V_frmValidarRuc.ShowDialog()
                    End If
                Else
                    MessageBox.Show(objResultado.strMensaje, "Sistema Tesoreria")
                    dgvDetalleCajas.CurrentRow.Cells("RazonSocial").Value = ""
                End If
            Else
                MessageBox.Show("Por el Momento El Servicio De Sunat esta inhabilitado", "Sistema Tesoreria")
            End If
        End If
    End Sub


    Private Sub btnCanc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCanc.Click
        Panel3.Visible = False
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        If Trim(txtNombre.Text) <> "" Then
            Dim x As frmReporteGasto = frmReporteGasto.Instance
            x.MdiParent = frmPrincipal
            x.Entregadoa = Trim(txtNombre.Text)
            If vb.IsDBNull(gDesEmpresa) = True Then
                x.prmEmpresa = ""
            Else
                x.prmEmpresa = gDesEmpresa
            End If
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value) = 0 Then
                x.prmFecha = ""
            Else
                x.prmFecha = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
            End If
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = 0 Then
                x.prmDocumento = ""
            Else
                x.prmDocumento = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
            End If
            'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(1).Value = Trim(txtNombre.Text)
            '------------------------------------
            Dim strnrodoc As String = ""
            Dim dbltotal As Double = 0
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                strnrodoc = ""
            Else
                strnrodoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
            End If
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) = 0 Then
                dbltotal = 0
            Else
                dbltotal = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value
            End If
            eSesionCajas.fActVale(strnrodoc, Trim(txtCodigo.Text), gEmpresa, Trim(txtNombre.Text), Format(dbltotal, "#,##0.00"))
            '------------------------------------
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                x.prmNroDocumento = ""
            Else
                x.prmNroDocumento = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
            End If
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = 0 Then
                x.prmTipoGasto = ""
            Else
                x.prmTipoGasto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
            End If
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value) = 0 Then
                x.prmDescGasto = ""
            Else
                x.prmDescGasto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value
            End If
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value) = 0 Then
                x.prmCentroCosto = ""
            Else
                x.prmCentroCosto = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value
            End If
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) = 0 Then
                x.prmTotal = ""
            Else
                x.prmTotal = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value
            End If
            x.prmArea = Trim(sDesArea)
            x.prmResponsable = Trim(sDesEmpleado)
            x.prmNroCaja = Trim(sIdCaja)
            x.prmDesCaja = Trim(sDesCaja)
            x.prmNroSesion = Trim(sNumeroSesion)
            x.prmMonSimbolo = Trim(sMonSimbolo)
            If Len(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value) = 0 Then
                x.prmNroVale = ""
            Else
                x.prmNroVale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("DocumentoReferencia").Value
            End If
            x.Show()
            Panel3.Visible = False
        Else
            MessageBox.Show("Para Imprimir : Ingrese / Seleccione un Nombre", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtNombre.Focus()
        End If
    End Sub


    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click
        Label2.Focus()
        If dgvVales.Rows.Count > 0 Then
            For x As Integer = 0 To dgvVales.Rows.Count - 1
                Dim CodVale As String = ""
                If dgvVales.Rows(x).Cells(0).Value = Nothing Then
                    CodVale = ""
                Else
                    CodVale = dgvVales.Rows(x).Cells(0).Value
                End If

                Dim Descrip As String = ""
                If dgvVales.Rows(x).Cells(3).Value = Nothing Then
                    Descrip = ""
                Else
                    Descrip = dgvVales.Rows(x).Cells(3).Value
                End If

                Dim Estado As Integer = 0
                'Dim iOpxion As Integer = 0
                If Convert.ToBoolean(dgvVales.Rows(x).Cells(4).Value) = False Then
                    Estado = 0
                    'iOpxion = 7
                ElseIf Convert.ToBoolean(dgvVales.Rows(x).Cells(4).Value) = True Then
                    Estado = 1
                    ' iOpxion = 8
                End If
                eSesionCajas.fActVale2(Trim(CodVale), Trim(txtCodigo.Text), gEmpresa, Trim(Descrip), Estado)
            Next
        End If
        Panel4.Visible = False
    End Sub



    Private Sub dgvVales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvVales.Click
        If dgvVales.Rows.Count > 0 Then
            Dim vale As String = ""
            'vale = (dgvVales.Rows(dgvVales.CurrentRow.Index).Cells(2).Value)
            vale = IIf(Microsoft.VisualBasic.IsDBNull(dgvVales.Rows(dgvVales.CurrentRow.Index).Cells(0).Value), "", dgvVales.Rows(dgvVales.CurrentRow.Index).Cells(0).Value)
            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value = vale
        End If
    End Sub

    Private Sub dgvNombres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvNombres.Click
        If dgvNombres.Rows.Count > 0 Then
            Dim nombre As String = ""
            'vale = (dgvVales.Rows(dgvVales.CurrentRow.Index).Cells(2).Value)
            nombre = IIf(Microsoft.VisualBasic.IsDBNull(dgvNombres.Rows(dgvNombres.CurrentRow.Index).Cells(0).Value), "", dgvNombres.Rows(dgvNombres.CurrentRow.Index).Cells(0).Value)
            txtNombre.Text = nombre
        End If
    End Sub

    Private Sub dgvNombres_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvNombres.SelectionChanged
        If dgvNombres.Rows.Count > 0 Then
            Dim nombre As String = ""
            ' vale = (dgvVales.Rows(dgvVales.CurrentRow.Index).Cells(2).Value)

            nombre = IIf(Microsoft.VisualBasic.IsDBNull(dgvNombres.Rows(dgvNombres.CurrentRow.Index).Cells(0).Value), "", dgvNombres.Rows(dgvNombres.CurrentRow.Index).Cells(0).Value)

            txtNombre.Text = nombre
        End If
    End Sub




    Private Sub ToolStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

    End Sub
End Class