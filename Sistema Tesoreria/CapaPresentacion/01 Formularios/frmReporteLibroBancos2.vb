Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteLibroBancos2

    Public IdLIbroCab As String

    Public Empresa As String
    Public Banco As String
    Public Moneda As String
    Public NroCuenta As String
    Public Mes As String
    Public Anio As String
    Public IdCuenta As String
    Public FechaFinal As DateTime

    Public SaldoFinalRep As String
    Public SaldoInicialRep As String
    Public SaldoSegunBanco As String

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptF As New rptLibroBancos2

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteLibroBancos2 = Nothing
    Public Shared Function Instance() As frmReporteLibroBancos2
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteLibroBancos2
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteLibroBancos2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteLibroBancos2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rptF.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        '1
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@IdLibroCab")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = IdLIbroCab
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@CodEmpresa")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


        '3
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Empresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Banco")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Banco
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '5
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Moneda")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Moneda
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '6
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@NroCuenta")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = NroCuenta
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '7
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Mes
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '8
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Anio
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '9
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@IdCuenta")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = IdCuenta
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '10
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Fecha")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = FechaFinal
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '11
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@SaldoFinalRep")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = SaldoFinalRep
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '12
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@SaldoSegunBanco")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = SaldoSegunBanco
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '13
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@SaldoInicialRep")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = SaldoInicialRep
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)



        'Public Empresa As String
        'Public Banco As String
        'Public Moneda As String
        'Public NroCuenta As String
        'Public Mes As String
        'Public Anio As String

        CrystalReportViewer1.ReportSource = rptF

    End Sub

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

End Class