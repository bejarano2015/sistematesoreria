<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVePartidasContrato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvDetalle = New System.Windows.Forms.DataGridView
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel
        Me.txtSubTotal = New ctrLibreria.Controles.BeTextBox
        Me.txtIGV = New ctrLibreria.Controles.BeTextBox
        Me.txtPresupuesto = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Und = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.M1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_Metrado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PU = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_PrecioSubContra = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.M2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Parcial = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.idDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_TitCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_ParCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_ConsCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.M2Anterior = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.Cant, Me.Und, Me.M1, Me.Con_Metrado, Me.PU, Me.Con_PrecioSubContra, Me.M2, Me.Parcial, Me.idDetalle, Me.Estado, Me.Con_TitCodigo, Me.Con_ParCodigo, Me.Con_ConsCodigo, Me.M2Anterior})
        Me.dgvDetalle.Location = New System.Drawing.Point(12, 61)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(817, 131)
        Me.dgvDetalle.TabIndex = 34
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel18.Location = New System.Drawing.Point(9, 198)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(27, 13)
        Me.BeLabel18.TabIndex = 35
        Me.BeLabel18.Text = "xxxx"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotal.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotal.Enabled = False
        Me.txtSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotal.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotal.KeyEnter = True
        Me.txtSubTotal.Location = New System.Drawing.Point(727, 198)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotal.ShortcutsEnabled = False
        Me.txtSubTotal.Size = New System.Drawing.Size(102, 20)
        Me.txtSubTotal.TabIndex = 36
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtIGV
        '
        Me.txtIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIGV.BackColor = System.Drawing.Color.Lavender
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIGV.Enabled = False
        Me.txtIGV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.ForeColor = System.Drawing.Color.Black
        Me.txtIGV.KeyEnter = True
        Me.txtIGV.Location = New System.Drawing.Point(727, 224)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIGV.ShortcutsEnabled = False
        Me.txtIGV.Size = New System.Drawing.Size(102, 20)
        Me.txtIGV.TabIndex = 38
        Me.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtPresupuesto
        '
        Me.txtPresupuesto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPresupuesto.BackColor = System.Drawing.Color.Ivory
        Me.txtPresupuesto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPresupuesto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPresupuesto.Enabled = False
        Me.txtPresupuesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresupuesto.ForeColor = System.Drawing.Color.Black
        Me.txtPresupuesto.KeyEnter = True
        Me.txtPresupuesto.Location = New System.Drawing.Point(727, 250)
        Me.txtPresupuesto.Name = "txtPresupuesto"
        Me.txtPresupuesto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPresupuesto.ShortcutsEnabled = False
        Me.txtPresupuesto.Size = New System.Drawing.Size(102, 20)
        Me.txtPresupuesto.TabIndex = 40
        Me.txtPresupuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPresupuesto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(652, 200)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(50, 13)
        Me.BeLabel17.TabIndex = 41
        Me.BeLabel17.Text = "SubTotal"
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(652, 226)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel16.TabIndex = 42
        Me.BeLabel16.Text = "I.G.V."
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(652, 257)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel9.TabIndex = 43
        Me.BeLabel9.Text = "Tot. Contrato"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel1.Location = New System.Drawing.Point(726, 9)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(103, 13)
        Me.BeLabel1.TabIndex = 44
        Me.BeLabel1.Text = "ESC = Salir y Grabar"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.BeLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(323, 43)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(281, 15)
        Me.BeLabel2.TabIndex = 45
        Me.BeLabel2.Text = "                               Presupuesto Base                               "
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle1
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 310
        '
        'Cant
        '
        Me.Cant.DataPropertyName = "Cantidad"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Cant.DefaultCellStyle = DataGridViewCellStyle2
        Me.Cant.HeaderText = "Cant"
        Me.Cant.Name = "Cant"
        Me.Cant.ReadOnly = True
        Me.Cant.Visible = False
        Me.Cant.Width = 55
        '
        'Und
        '
        Me.Und.DataPropertyName = "UnidMed"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Und.DefaultCellStyle = DataGridViewCellStyle3
        Me.Und.HeaderText = "U.M."
        Me.Und.Name = "Und"
        Me.Und.ReadOnly = True
        Me.Und.Width = 55
        '
        'M1
        '
        Me.M1.DataPropertyName = "MetradoPor"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.M1.DefaultCellStyle = DataGridViewCellStyle4
        Me.M1.HeaderText = "Metr. %"
        Me.M1.Name = "M1"
        Me.M1.ReadOnly = True
        Me.M1.Width = 85
        '
        'Con_Metrado
        '
        Me.Con_Metrado.DataPropertyName = "Con_Metrado"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Con_Metrado.DefaultCellStyle = DataGridViewCellStyle5
        Me.Con_Metrado.HeaderText = "Mt."
        Me.Con_Metrado.Name = "Con_Metrado"
        Me.Con_Metrado.ReadOnly = True
        Me.Con_Metrado.Width = 50
        '
        'PU
        '
        Me.PU.DataPropertyName = "PU"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.PU.DefaultCellStyle = DataGridViewCellStyle6
        Me.PU.HeaderText = "PU"
        Me.PU.Name = "PU"
        Me.PU.ReadOnly = True
        Me.PU.Visible = False
        Me.PU.Width = 60
        '
        'Con_PrecioSubContra
        '
        Me.Con_PrecioSubContra.DataPropertyName = "Con_PrecioSubContra"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.Con_PrecioSubContra.DefaultCellStyle = DataGridViewCellStyle7
        Me.Con_PrecioSubContra.HeaderText = "PMO Int."
        Me.Con_PrecioSubContra.Name = "Con_PrecioSubContra"
        Me.Con_PrecioSubContra.ReadOnly = True
        Me.Con_PrecioSubContra.Width = 90
        '
        'M2
        '
        Me.M2.DataPropertyName = "MetradoImpor"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.M2.DefaultCellStyle = DataGridViewCellStyle8
        Me.M2.HeaderText = "Mt. Contratado"
        Me.M2.Name = "M2"
        Me.M2.Width = 120
        '
        'Parcial
        '
        Me.Parcial.DataPropertyName = "Parcial"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.Parcial.DefaultCellStyle = DataGridViewCellStyle9
        Me.Parcial.HeaderText = "Parcial"
        Me.Parcial.Name = "Parcial"
        Me.Parcial.Width = 85
        '
        'idDetalle
        '
        Me.idDetalle.DataPropertyName = "IdPartidaDet"
        Me.idDetalle.HeaderText = "IdDetalle"
        Me.idDetalle.Name = "idDetalle"
        Me.idDetalle.Visible = False
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.Visible = False
        '
        'Con_TitCodigo
        '
        Me.Con_TitCodigo.DataPropertyName = "Con_TitCodigo"
        Me.Con_TitCodigo.HeaderText = "Con_TitCodigo"
        Me.Con_TitCodigo.Name = "Con_TitCodigo"
        Me.Con_TitCodigo.Visible = False
        '
        'Con_ParCodigo
        '
        Me.Con_ParCodigo.DataPropertyName = "Con_ParCodigo"
        Me.Con_ParCodigo.HeaderText = "Con_ParCodigo"
        Me.Con_ParCodigo.Name = "Con_ParCodigo"
        Me.Con_ParCodigo.Visible = False
        '
        'Con_ConsCodigo
        '
        Me.Con_ConsCodigo.DataPropertyName = "Con_ConsCodigo"
        Me.Con_ConsCodigo.HeaderText = "Con_ConsCodigo"
        Me.Con_ConsCodigo.Name = "Con_ConsCodigo"
        Me.Con_ConsCodigo.Visible = False
        '
        'M2Anterior
        '
        Me.M2Anterior.DataPropertyName = "M2Anterior"
        Me.M2Anterior.HeaderText = "M2Anterior"
        Me.M2Anterior.Name = "M2Anterior"
        Me.M2Anterior.Visible = False
        '
        'frmVePartidasContrato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(840, 275)
        Me.ControlBox = False
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.BeLabel9)
        Me.Controls.Add(Me.BeLabel16)
        Me.Controls.Add(Me.BeLabel17)
        Me.Controls.Add(Me.txtPresupuesto)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.txtSubTotal)
        Me.Controls.Add(Me.BeLabel18)
        Me.Controls.Add(Me.dgvDetalle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.Name = "frmVePartidasContrato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Partidas del Contrato"
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSubTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPresupuesto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Und As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_Metrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_PrecioSubContra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Parcial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idDetalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_TitCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_ParCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_ConsCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M2Anterior As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
