<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPartidaAvanceDiario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPartidaAvanceDiario))
        Me.cboObra = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel()
        Me.cboContratista = New ctrLibreria.Controles.BeComboBox()
        Me.cboPartidas = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.txtSemana = New ctrLibreria.Controles.BeTextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.dtpEnvio = New ctrLibreria.Controles.BeDateTimePicker()
        Me.cboSemana = New ctrLibreria.Controles.BeComboBox()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Item = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Saldo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Avance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Semana = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdPartidaDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdPartidaAvanceDiario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AxMonthView1 = New AxMSComCtl2.AxMonthView()
        Me.AxMonthView2 = New AxMSComCtl2.AxMonthView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblUbi = New ctrLibreria.Controles.BeLabel()
        Me.lblNCon = New ctrLibreria.Controles.BeLabel()
        Me.lblNO = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.F2 = New ctrLibreria.Controles.BeDateTimePicker()
        Me.F1 = New ctrLibreria.Controles.BeDateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CCosto = New ctrLibreria.Controles.BeComboBox()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxMonthView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(86, 18)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(500, 21)
        Me.cboObra.TabIndex = 7
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(8, 26)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(30, 13)
        Me.BeLabel13.TabIndex = 6
        Me.BeLabel13.Text = "Obra"
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(8, 54)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel22.TabIndex = 46
        Me.BeLabel22.Text = "Contratista"
        '
        'cboContratista
        '
        Me.cboContratista.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboContratista.BackColor = System.Drawing.Color.Ivory
        Me.cboContratista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContratista.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboContratista.ForeColor = System.Drawing.Color.Black
        Me.cboContratista.FormattingEnabled = True
        Me.cboContratista.KeyEnter = True
        Me.cboContratista.Location = New System.Drawing.Point(86, 46)
        Me.cboContratista.Name = "cboContratista"
        Me.cboContratista.Size = New System.Drawing.Size(500, 21)
        Me.cboContratista.TabIndex = 45
        '
        'cboPartidas
        '
        Me.cboPartidas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPartidas.BackColor = System.Drawing.Color.Ivory
        Me.cboPartidas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartidas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPartidas.ForeColor = System.Drawing.Color.Black
        Me.cboPartidas.FormattingEnabled = True
        Me.cboPartidas.KeyEnter = True
        Me.cboPartidas.Location = New System.Drawing.Point(86, 102)
        Me.cboPartidas.Name = "cboPartidas"
        Me.cboPartidas.Size = New System.Drawing.Size(500, 21)
        Me.cboPartidas.TabIndex = 48
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(8, 109)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel5.TabIndex = 47
        Me.BeLabel5.Text = "Contrato"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.BeLabel4)
        Me.GroupBox3.Controls.Add(Me.txtSemana)
        Me.GroupBox3.Controls.Add(Me.btnBuscar)
        Me.GroupBox3.Controls.Add(Me.BeLabel2)
        Me.GroupBox3.Controls.Add(Me.dtpEnvio)
        Me.GroupBox3.Location = New System.Drawing.Point(86, 134)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(500, 46)
        Me.GroupBox3.TabIndex = 49
        Me.GroupBox3.TabStop = False
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(152, 24)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel4.TabIndex = 50
        Me.BeLabel4.Text = "Semana"
        '
        'txtSemana
        '
        Me.txtSemana.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSemana.BackColor = System.Drawing.Color.Ivory
        Me.txtSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSemana.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSemana.Enabled = False
        Me.txtSemana.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSemana.ForeColor = System.Drawing.Color.Black
        Me.txtSemana.KeyEnter = True
        Me.txtSemana.Location = New System.Drawing.Point(204, 17)
        Me.txtSemana.Name = "txtSemana"
        Me.txtSemana.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSemana.ReadOnly = True
        Me.txtSemana.ShortcutsEnabled = False
        Me.txtSemana.Size = New System.Drawing.Size(84, 20)
        Me.txtSemana.TabIndex = 51
        Me.txtSemana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtSemana.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Númerico
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(306, 16)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(80, 23)
        Me.btnBuscar.TabIndex = 49
        Me.btnBuscar.Text = "Grabar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(6, 21)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(37, 13)
        Me.BeLabel2.TabIndex = 0
        Me.BeLabel2.Text = "Fecha"
        '
        'dtpEnvio
        '
        Me.dtpEnvio.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpEnvio.CustomFormat = ""
        Me.dtpEnvio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnvio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnvio.KeyEnter = True
        Me.dtpEnvio.Location = New System.Drawing.Point(49, 17)
        Me.dtpEnvio.Name = "dtpEnvio"
        Me.dtpEnvio.Size = New System.Drawing.Size(84, 20)
        Me.dtpEnvio.TabIndex = 1
        Me.dtpEnvio.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpEnvio.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'cboSemana
        '
        Me.cboSemana.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboSemana.BackColor = System.Drawing.Color.Ivory
        Me.cboSemana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSemana.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSemana.ForeColor = System.Drawing.Color.Black
        Me.cboSemana.FormattingEnabled = True
        Me.cboSemana.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55"})
        Me.cboSemana.KeyEnter = True
        Me.cboSemana.Location = New System.Drawing.Point(62, 30)
        Me.cboSemana.Name = "cboSemana"
        Me.cboSemana.Size = New System.Drawing.Size(105, 21)
        Me.cboSemana.TabIndex = 53
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(62, 61)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(105, 23)
        Me.btnImprimir.TabIndex = 52
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Fecha, Me.Item, Me.UM, Me.Total, Me.Saldo, Me.Avance, Me.Semana, Me.IdPartidaDet, Me.Estado, Me.IdPartidaAvanceDiario})
        Me.dgvDetalle.Location = New System.Drawing.Point(5, 186)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(855, 216)
        Me.dgvDetalle.TabIndex = 50
        '
        'Fecha
        '
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 80
        '
        'Item
        '
        Me.Item.HeaderText = "Partida"
        Me.Item.Name = "Item"
        Me.Item.ReadOnly = True
        Me.Item.Width = 300
        '
        'UM
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.UM.DefaultCellStyle = DataGridViewCellStyle2
        Me.UM.HeaderText = "U.M."
        Me.UM.Name = "UM"
        Me.UM.ReadOnly = True
        Me.UM.Width = 50
        '
        'Total
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Total.DefaultCellStyle = DataGridViewCellStyle3
        Me.Total.HeaderText = "Mt. Contratado"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'Saldo
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Saldo.DefaultCellStyle = DataGridViewCellStyle4
        Me.Saldo.HeaderText = "Avanzado"
        Me.Saldo.Name = "Saldo"
        Me.Saldo.ReadOnly = True
        '
        'Avance
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Avance.DefaultCellStyle = DataGridViewCellStyle5
        Me.Avance.HeaderText = "Avance Dia"
        Me.Avance.Name = "Avance"
        '
        'Semana
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Semana.DefaultCellStyle = DataGridViewCellStyle6
        Me.Semana.HeaderText = "Semana"
        Me.Semana.Name = "Semana"
        Me.Semana.ReadOnly = True
        '
        'IdPartidaDet
        '
        Me.IdPartidaDet.DataPropertyName = "IdPartidaDet"
        Me.IdPartidaDet.HeaderText = "IdPartidaDet"
        Me.IdPartidaDet.Name = "IdPartidaDet"
        Me.IdPartidaDet.Visible = False
        '
        'Estado
        '
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.Visible = False
        '
        'IdPartidaAvanceDiario
        '
        Me.IdPartidaAvanceDiario.HeaderText = "IdPartidaAvanceDiario"
        Me.IdPartidaAvanceDiario.Name = "IdPartidaAvanceDiario"
        Me.IdPartidaAvanceDiario.Visible = False
        '
        'AxMonthView1
        '
        Me.AxMonthView1.Location = New System.Drawing.Point(1207, 64)
        Me.AxMonthView1.Name = "AxMonthView1"
        Me.AxMonthView1.OcxState = CType(resources.GetObject("AxMonthView1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxMonthView1.Size = New System.Drawing.Size(181, 158)
        Me.AxMonthView1.TabIndex = 52
        '
        'AxMonthView2
        '
        Me.AxMonthView2.Location = New System.Drawing.Point(1207, 228)
        Me.AxMonthView2.Name = "AxMonthView2"
        Me.AxMonthView2.OcxState = CType(resources.GetObject("AxMonthView2.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxMonthView2.Size = New System.Drawing.Size(181, 158)
        Me.AxMonthView2.TabIndex = 53
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BeLabel1)
        Me.GroupBox1.Controls.Add(Me.cboSemana)
        Me.GroupBox1.Controls.Add(Me.btnImprimir)
        Me.GroupBox1.Location = New System.Drawing.Point(865, 186)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(186, 97)
        Me.GroupBox1.TabIndex = 54
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Imprimir"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(10, 35)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel1.TabIndex = 54
        Me.BeLabel1.Text = "Semana"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblUbi)
        Me.GroupBox2.Controls.Add(Me.lblNCon)
        Me.GroupBox2.Controls.Add(Me.lblNO)
        Me.GroupBox2.Controls.Add(Me.BeLabel8)
        Me.GroupBox2.Controls.Add(Me.BeLabel19)
        Me.GroupBox2.Controls.Add(Me.BeLabel7)
        Me.GroupBox2.Controls.Add(Me.BeLabel6)
        Me.GroupBox2.Controls.Add(Me.BeLabel3)
        Me.GroupBox2.Controls.Add(Me.F2)
        Me.GroupBox2.Controls.Add(Me.F1)
        Me.GroupBox2.Location = New System.Drawing.Point(592, 83)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(460, 97)
        Me.GroupBox2.TabIndex = 55
        Me.GroupBox2.TabStop = False
        '
        'lblUbi
        '
        Me.lblUbi.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblUbi.AutoSize = True
        Me.lblUbi.BackColor = System.Drawing.Color.Transparent
        Me.lblUbi.ForeColor = System.Drawing.Color.Black
        Me.lblUbi.Location = New System.Drawing.Point(129, 74)
        Me.lblUbi.Name = "lblUbi"
        Me.lblUbi.Size = New System.Drawing.Size(33, 13)
        Me.lblUbi.TabIndex = 58
        Me.lblUbi.Text = "lblUbi"
        '
        'lblNCon
        '
        Me.lblNCon.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblNCon.AutoSize = True
        Me.lblNCon.BackColor = System.Drawing.Color.Transparent
        Me.lblNCon.ForeColor = System.Drawing.Color.Black
        Me.lblNCon.Location = New System.Drawing.Point(262, 47)
        Me.lblNCon.Name = "lblNCon"
        Me.lblNCon.Size = New System.Drawing.Size(44, 13)
        Me.lblNCon.TabIndex = 60
        Me.lblNCon.Text = "lblNCon"
        '
        'lblNO
        '
        Me.lblNO.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblNO.AutoSize = True
        Me.lblNO.BackColor = System.Drawing.Color.Transparent
        Me.lblNO.ForeColor = System.Drawing.Color.Black
        Me.lblNO.Location = New System.Drawing.Point(82, 47)
        Me.lblNO.Name = "lblNO"
        Me.lblNO.Size = New System.Drawing.Size(33, 13)
        Me.lblNO.TabIndex = 59
        Me.lblNO.Text = "lblNO"
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(8, 74)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(115, 13)
        Me.BeLabel8.TabIndex = 58
        Me.BeLabel8.Text = "Ubicación de Trabajo :"
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.ForeColor = System.Drawing.Color.Black
        Me.BeLabel19.Location = New System.Drawing.Point(175, 47)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(68, 13)
        Me.BeLabel19.TabIndex = 57
        Me.BeLabel19.Text = "Contrato Nº :"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(8, 47)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel7.TabIndex = 56
        Me.BeLabel7.Text = "Orden Nº :"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(175, 22)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(84, 13)
        Me.BeLabel6.TabIndex = 54
        Me.BeLabel6.Text = "Fecha Término :"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(3, 22)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(71, 13)
        Me.BeLabel3.TabIndex = 52
        Me.BeLabel3.Text = "Fecha Inicio :"
        '
        'F2
        '
        Me.F2.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.F2.CustomFormat = ""
        Me.F2.Enabled = False
        Me.F2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.F2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.F2.KeyEnter = True
        Me.F2.Location = New System.Drawing.Point(265, 19)
        Me.F2.Name = "F2"
        Me.F2.Size = New System.Drawing.Size(84, 20)
        Me.F2.TabIndex = 53
        Me.F2.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.F2.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'F1
        '
        Me.F1.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.F1.CustomFormat = ""
        Me.F1.Enabled = False
        Me.F1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.F1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.F1.KeyEnter = True
        Me.F1.Location = New System.Drawing.Point(79, 19)
        Me.F1.Name = "F1"
        Me.F1.Size = New System.Drawing.Size(84, 20)
        Me.F1.TabIndex = 52
        Me.F1.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.F1.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(8, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 57
        Me.Label1.Text = "C Costo"
        '
        'CCosto
        '
        Me.CCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.CCosto.BackColor = System.Drawing.Color.Ivory
        Me.CCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CCosto.ForeColor = System.Drawing.Color.Black
        Me.CCosto.FormattingEnabled = True
        Me.CCosto.KeyEnter = True
        Me.CCosto.Location = New System.Drawing.Point(86, 74)
        Me.CCosto.Name = "CCosto"
        Me.CCosto.Size = New System.Drawing.Size(500, 21)
        Me.CCosto.TabIndex = 56
        '
        'frmPartidaAvanceDiario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1063, 404)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CCosto)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.AxMonthView2)
        Me.Controls.Add(Me.AxMonthView1)
        Me.Controls.Add(Me.dgvDetalle)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.cboPartidas)
        Me.Controls.Add(Me.BeLabel5)
        Me.Controls.Add(Me.BeLabel22)
        Me.Controls.Add(Me.cboContratista)
        Me.Controls.Add(Me.cboObra)
        Me.Controls.Add(Me.BeLabel13)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmPartidaAvanceDiario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Registro de Avance Diario de Contrato"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxMonthView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboContratista As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboPartidas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpEnvio As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSemana As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents AxMonthView1 As AxMSComCtl2.AxMonthView
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents cboSemana As ctrLibreria.Controles.BeComboBox
    Friend WithEvents AxMonthView2 As AxMSComCtl2.AxMonthView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents F2 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents F1 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblUbi As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblNCon As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblNO As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Item As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Saldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Avance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Semana As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdPartidaDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdPartidaAvanceDiario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CCosto As ctrLibreria.Controles.BeComboBox
End Class
