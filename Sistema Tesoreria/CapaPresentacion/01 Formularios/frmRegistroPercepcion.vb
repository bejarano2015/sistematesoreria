Option Explicit On
Imports System.Xml
Imports MSXML2
Imports System.Math

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports Telerik.WinControls.Data
Imports Telerik.WinControls.UI.Export

Public Class frmRegistroPercepcion

    Private ePercepcion As clsPercepcion
    Dim odtPercepcion As New DataTable
    Dim odtPercepcionDet As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared idRegistroDocumentoCab As Decimal
    Dim strAccion As String
    Dim decImportePercepcion As Decimal
    Dim decImporteTotal As Decimal
    Private estado As Integer
    Private Loaded As Boolean = False

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptRetencion As New rptRetenciones

    Private cCapaDatos As clsCapaDatos

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Loaded = True
        VL_DOCUMENTO = "RETENCION"

        ePercepcion = New clsPercepcion
        cCapaDatos = New clsCapaDatos

        cboAnio.Text = Convert.ToString(DateTime.Now.Year)
        cboMes.SelectedIndex = DateTime.Now.Month - 1

        tabMantenimiento.SelectedTab = tabLista
        botonesMantenimiento(mnuMantenimiento, True)

        InitCombos()

        CargarPercepcionCab()
        CargarPercepcionDet()
    End Sub

    Private Sub tabMantenimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabMantenimiento.SelectedIndexChanged
        If tabMantenimiento.SelectedIndex = 0 Then
            strAccion = Accion.Grabar
            'grpFacturas.Visible = False
            botonesMantenimiento(mnuMantenimiento, True)
            mnuMantenimiento.Items("btnReservar").Enabled = True
        Else
            strAccion = Accion.Modifica
            cboDocumentoOrigen.Enabled = False
            grpFacturas.Visible = False
            btnBuscarAnexo.Enabled = False
            dtFecha.Enabled = False
            txtSerie.Enabled = False
            txtNroDocumento.Enabled = False

            Try
                If Convert.ToString(gcPercepcion.CurrentRow.Cells("colRevervado").Value) = "1" Then
                    btnBuscarAnexo.Enabled = True
                    dtFecha.Enabled = True
                End If
            Catch ex As Exception
            End Try

            botonesMantenimiento(mnuMantenimiento, False)
            mnuMantenimiento.Items("btnReservar").Enabled = False
        End If
    End Sub

    Private Sub cboAnio_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboAnio.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarPercepcionCab()
    End Sub

    Private Sub cboMes_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboMes.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarPercepcionCab()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarPercepcionCab()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        tabMantenimiento.SelectedTab = tabDetalle
        strAccion = Accion.Grabar
        LimpiarControles()
        cboDocumentoOrigen.SelectedIndex = 1
        cboDocumentoOrigen.Enabled = True
        btnBuscarAnexo.Enabled = True
        dtFecha.Enabled = True
        txtSerie.Text = ""
        txtSerie.Enabled = True
        txtNroDocumento.Enabled = True
        txtNroDocumento.Text = ""
        'txtRuc.ReadOnly = False
        odtPercepcionDet.Clear()
        dtFecha.Value = Date.Today
        'txtNombre.Focus()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        tabMantenimiento.SelectedTab = tabLista
        CargarPercepcionCab()
    End Sub

    Private Sub cboDocumentoOrigen_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDocumentoOrigen.SelectedValueChanged
        If Not Loaded Then Exit Sub

        odtPercepcionDet.Clear()
        gcPercepcionDetalle.DataSource = Nothing
        gcFacturas.DataSource = Nothing

        If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then 'Factura de Compra           
            gcPercepcionDetalle.Columns("colImporteTotal").ReadOnly = False

            'If strAccion = Accion.Grabar Then
            '    txtSerie.Text = "001"
            '    NumeroCorrelativo()
            'End If
        ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then ' Orden de Compra
            gcPercepcionDetalle.Columns("colImporteTotal").ReadOnly = True
        End If
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        ''Se valida los campos
        gcPercepcionDetalle.EndEdit()
        If txtRuc.Text.Trim.Length = 0 Then MessageBox.Show("El proveedor es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtRuc.Focus() : Exit Sub

        SumarImporte()

        If gcPercepcionDetalle.RowCount > 0 Then
            Select Case strAccion
                Case Accion.Grabar
                    Guardar()
                Case Accion.Modifica
                    If Convert.ToDecimal(Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1) Then
                        If Not ComprobarDato() Then Exit Sub
                    End If
                    Modificar()
            End Select

            CargarPercepcionCab()
            tabMantenimiento.SelectedTab = tabLista
        Else
            MessageBox.Show("No existe registro en detalle retenciones. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If gcPercepcion.RowCount > 0 Then
            If Not Eliminar() Then Exit Sub
            CargarPercepcionCab()
        Else
            MessageBox.Show("Seleccione registro a eliminar. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub gcPercepcion_CurrentRowChanged(sender As Object, e As Telerik.WinControls.UI.CurrentRowChangedEventArgs) Handles gcPercepcion.CurrentRowChanged
        Try
            idRegistroDocumentoCab = Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value)

            dtFecha.Value = Convert.ToDateTime(gcPercepcion.CurrentRow.Cells("colFecha").Value)
            txtCodigoAnexo.Text = Convert.ToString(gcPercepcion.CurrentRow.Cells("colCodProveedor").Value)
            txtRuc.Text = Convert.ToString(gcPercepcion.CurrentRow.Cells("colRuc").Value)
            lblNombreAnexo.Text = Convert.ToString(gcPercepcion.CurrentRow.Cells("colNombreProveedor").Value)
            lblDireccion.Text = Convert.ToString(gcPercepcion.CurrentRow.Cells("colDIRECCION").Value)
            txtSerie.Text = "" ' Convert.ToString(gcPercepcion.CurrentRow.Cells("colSerie").Value)
            txtNroDocumento.Text = "" ' Convert.ToString(gcPercepcion.CurrentRow.Cells("colNumero").Value)
            txtObservacion.Text = Convert.ToString(gcPercepcion.CurrentRow.Cells("colDescripcion").Value)
            cboDocumentoOrigen.SelectedValue = Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value)

            CargarPercepcionDet()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub gcPercepcion_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcPercepcion.CellDoubleClick
        If gcPercepcion.RowCount > 0 Then
            tabMantenimiento.SelectedTab = tabDetalle
        End If
    End Sub

    Private Sub txtRuc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRuc.KeyPress
        If Len(txtRuc.Text) = 11 Then
            Select Case Asc(e.KeyChar)
                Case 13
                    Try
                        grpFacturas.Visible = True
                        If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
                            odtPercepcion = ePercepcion.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
                            gcFacturas.DataSource = odtPercepcion
                        ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then
                            odtPercepcion = ePercepcion.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
                            gcFacturas.DataSource = odtPercepcion
                        End If
                    Catch ex As Exception
                        Exit Sub
                    End Try
            End Select
        End If
    End Sub

    Private Sub btnBuscarAnexo_Click(sender As Object, e As EventArgs) Handles btnBuscarAnexo.Click
        Dim Proveedores As New frmBuscaProveedor()
        If Proveedores.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtCodigoAnexo.Text = frmBuscaProveedor.strCodigo
            txtRuc.Text = frmBuscaProveedor.strRuc
            lblNombreAnexo.Text = frmBuscaProveedor.strNombre
            lblDireccion.Text = frmBuscaProveedor.strDireccion

            grpFacturas.Visible = True

            If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
                odtPercepcion = ePercepcion.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
                gcFacturas.DataSource = odtPercepcion
            ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then
                odtPercepcion = ePercepcion.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
                gcFacturas.DataSource = odtPercepcion
            End If
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        gcFacturas.CloseEditor()
        If gcFacturas.RowCount > 0 Then
            'Detalle
            For i As Integer = 0 To gcFacturas.RowCount - 1
                If CBool(gcFacturas.Rows(i).Cells("colChkEstado").Value) = True Then

                    Dim importeTipoCambio As Decimal = 0
                    If Convert.ToString(gcFacturas.CurrentRow.Cells("colIdMoneda").Value) = "02" Then
                        Dim iResultado As Decimal = 0

                        iResultado = ePercepcion.fImporteTipoCambio(CDate(dtFecha.Value), 13)
                        importeTipoCambio = iResultado
                    End If

                    Dim row As DataRow
                    row = odtPercepcionDet.NewRow
                    row("IdPercepcionDet") = 0
                    row("IdPercepcionCab") = 0
                    row("IdRegistroDocumento") = gcFacturas.Rows(i).Cells("colIdRegistro").Value
                    row("EmprCodigo") = gcFacturas.Rows(i).Cells("colEmprCodigo").Value
                    row("IdDocumento") = gcFacturas.Rows(i).Cells("colIdDocumento").Value
                    row("Serie") = gcFacturas.Rows(i).Cells("colSerie").Value
                    row("Numero") = gcFacturas.Rows(i).Cells("colNumero").Value
                    row("Fecha") = gcFacturas.Rows(i).Cells("colFecha").Value
                    row("Porcentaje") = iValorPorcentajePercepcion * 100
                    row("IdMoneda") = gcFacturas.Rows(i).Cells("colIdMoneda").Value
                    row("ImporteSoles") = If(gcFacturas.Rows(i).Cells("colIdMoneda").Value = "02", Round(gcFacturas.Rows(i).Cells("colImporteTotal").Value * importeTipoCambio, 2, MidpointRounding.ToEven), gcFacturas.Rows(i).Cells("colImporteTotal").Value)
                    row("ImporteTotal") = Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven)

                    Dim porcentajePercepcion As Decimal = 0
                    porcentajePercepcion = If(importeTipoCambio = 0, Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * iValorPorcentajePercepcion, (Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * iValorPorcentajePercepcion) * importeTipoCambio)
                    row("PorcentajeRetencion") = Round(porcentajePercepcion, 2, MidpointRounding.ToEven)

                    row("ImporteRetenido") = Round(Convert.ToDecimal(row("ImporteSoles")), 2, MidpointRounding.ToEven) + Round(Convert.ToDecimal(row("PorcentajeRetencion")), 2, MidpointRounding.ToEven)

                    odtPercepcionDet.Rows.Add(row)
                    'stigvDetFormulaCalculo.UpdateCurrentRow()
                    gcPercepcionDetalle.DataSource = odtPercepcionDet

                    'Me.DialogResult = Windows.Forms.DialogResult. 
                    ''Me.Close()
                End If
            Next i
            grpFacturas.Visible = False
        End If

    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        grpFacturas.Visible = False
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If gcPercepcionDetalle.RowCount > 0 Then
            'Dim intFila As Integer = gvTipoGastoDestino.CurrentRow.Index
            'odtTipDestinoGasto.Rows(intFila)("IDTIPOGASTODESTINO") = -1
            gcPercepcionDetalle.Rows.Remove(gcPercepcionDetalle.CurrentRow)
            odtPercepcionDet.AcceptChanges()
        End If
    End Sub

    Private Sub gcRetencionDetalle_CellValueChanged(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcPercepcionDetalle.CellValueChanged
        gcPercepcionDetalle.EndEdit()

        If e.Column.Name.ToString() = "colPorcentaje" Then
            Dim decTotal As Decimal = 0

            decTotal = Round(Convert.ToDecimal(gcPercepcionDetalle.CurrentRow.Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven) * (Round(Convert.ToDecimal(gcPercepcionDetalle.CurrentRow.Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven) / 100)
            gcPercepcionDetalle.CurrentRow.Cells("colPorcentajeRetencion").Value = Round(decTotal, 2, MidpointRounding.ToEven)

            gcPercepcionDetalle.CurrentRow.Cells("colImporteRetenido").Value = gcPercepcionDetalle.CurrentRow.Cells("colPorcentajeRetencion").Value + gcPercepcionDetalle.CurrentRow.Cells("colImporteSoles").Value

            'CalcularImporte()
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimirListado.Click
        Dim x As frmReporRetenciones = frmReporRetenciones.Instance
        x.MdiParent = frmPrincipal

        '' ''x.Show()  
        If gcPercepcion.RowCount > 0 Then
            'If Convert.ToString(gcRetencion.CurrentRow.Cells("colNumero").Value) = "" And Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 Then
            '    Dim imprimir As New RadAboutBox1()
            '    If imprimir.ShowDialog = Windows.Forms.DialogResult.OK Then
            '        Dim idDocumento = imprimir.cboDocumentos.SelectedValue.ToString()
            '        Dim serie = imprimir.cboSerie.Text

            '        'Esta funci�n ya no v�
            '        'Dim iResultado As Int32

            '        'iResultado = eRetenciones.fActualizaSeriNumero(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdPercepcionCab").Value), serie, "0", 6)

            '        rptRetencion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '        '1
            '        crParameterFieldDefinitions = rptRetencion.DataDefinition.ParameterFields()
            '        crParameterFieldDefinition = crParameterFieldDefinitions("@IDRETENCIONCAB")
            '        crParameterValues = crParameterFieldDefinition.CurrentValues
            '        crParameterDiscreteValue = New ParameterDiscreteValue()
            '        crParameterDiscreteValue.Value = frmRegistroRetencion.idRegistroDocumentoCab
            '        crParameterValues.Add(crParameterDiscreteValue)
            '        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '        x.CrystalReportViewer1.ReportSource = rptRetencion
            '        'rptRetencion.PrintToPrinter(1, False, 0, 1)

            '        CargarRetencionCab()

            '        x.Show()
            '    End If
            If Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 And Convert.ToString(gcPercepcion.CurrentRow.Cells("colNumero").Value) <> "" Then
                If MessageBox.Show("�Desea imprimir el documento?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    'Esta funci�n ya no v�
                    'Dim iResultado As Int32

                    'iResultado = eRetenciones.fActualizaSeriNumero(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdPercepcionCab").Value), "", "0", 10)

                    rptRetencion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
                    '1
                    crParameterFieldDefinitions = rptRetencion.DataDefinition.ParameterFields()
                    crParameterFieldDefinition = crParameterFieldDefinitions("@IDPERCEPCIONCAB")
                    crParameterValues = crParameterFieldDefinition.CurrentValues
                    crParameterDiscreteValue = New ParameterDiscreteValue()
                    crParameterDiscreteValue.Value = Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value)
                    crParameterValues.Add(crParameterDiscreteValue)
                    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

                    x.CrystalReportViewer1.ReportSource = rptRetencion
                    'rptRetencion.PrintToPrinter(1, False, 0, 1)
                    CargarPercepcionCab()

                    x.Show()
                End If
            End If
        End If
    End Sub

    Private Sub MECO_Reservar_Click(sender As Object, e As EventArgs) Handles MECO_Reservar.Click
        If gcPercepcion.RowCount > 0 Then
            If Convert.ToString(gcPercepcion.CurrentRow.Cells("colNumero").Value) = "" And Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 Then
                Dim imprimir As New RadAboutBox1()
                If imprimir.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim idDocumento = imprimir.cboDocumentos.SelectedValue.ToString()
                    Dim serie = imprimir.cboSerie.Text

                    Dim iResultado As Int32

                    iResultado = ePercepcion.fActualizaSeriNumero(Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), serie, "1", 6)

                    CargarPercepcionCab()
                End If
            Else
                MessageBox.Show("No se puede reservar este documento. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub MECO_Anular_Click(sender As Object, e As EventArgs) Handles MECO_Anular.Click
        If gcPercepcion.RowCount > 0 Then
            If MessageBox.Show("�Est� seguro que desea anular este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                If Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 And Convert.ToString(gcPercepcion.CurrentRow.Cells("colNumero").Value) <> "" Then
                    Dim iResultado As Int32
                    iResultado = ePercepcion.fAnular(Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value))
                    CargarPercepcionCab()
                Else
                    MessageBox.Show("No se puede anular este documento. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
    End Sub

#End Region

#Region " Metodos Privados "

    Private Sub LimpiarControles()
        txtCodigoAnexo.Text = "" : txtRuc.Text = "" : lblNombreAnexo.Text = "" : lblDireccion.Text = "" : txtSerie.Text = "" : txtNroDocumento.Text = "" : txtObservacion.Text = ""
        gcPercepcionDetalle.DataSource = Nothing
    End Sub

    Private Sub InitCombos()
        Try
            Dim odtDocumento As New DataTable
            odtDocumento = ePercepcion.fCargarDocumentos(7)
            cboDocumentoOrigen.DataSource = odtDocumento
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarPercepcionCab()
        Try
            'Dim odtData As New DataTable

            Dim VL_ANIO = Convert.ToString(cboAnio.Text)
            Dim VL_MES = Convert.ToString(cboMes.SelectedIndex + 1)

            odtPercepcion = ePercepcion.fCargarPercepcion(gEmpresa, VL_ANIO, VL_MES)
            gcPercepcion.DataSource = odtPercepcion
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarPercepcionDet()
        Try
            'Dim odtData As New DataTable
            Dim idPercepcionCab As Decimal = If(gcPercepcion.RowCount > 0, Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), 0)

            odtPercepcionDet = ePercepcion.fCargarPercepcionDetalle(idPercepcionCab)
            gcPercepcionDetalle.DataSource = odtPercepcionDet
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NumeroCorrelativo()
        Dim iResultado As String = ""

        iResultado = ePercepcion.fNroCorrelativo("001", 12)
        txtNroDocumento.Text = iResultado.ToString
    End Sub

#End Region

#Region " M�todos de BD "

    Private Sub SumarImporte()
        Try
            decImportePercepcion = 0
            decImporteTotal = 0

            For i As Integer = 0 To gcPercepcionDetalle.RowCount - 1
                decImportePercepcion += Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven)
                decImporteTotal += Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven)
            Next

        Catch ex As Exception
            MessageBox.Show("Error al sumar el importe. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Guardar()
        Try
            Dim iResultado As Int32
            Dim idPercepcionCab As Decimal = 0

            iResultado = ePercepcion.fGrabar(0, gEmpresa, Convert.ToDecimal(cboDocumentoOrigen.SelectedValue), txtSerie.Text.Trim, txtNroDocumento.Text.Trim, dtFecha.Value, txtCodigoAnexo.Text.Trim, lblNombreAnexo.Text, txtRuc.Text.Trim, lblDireccion.Text, txtObservacion.Text.Trim, 1, decImportePercepcion, decImporteTotal, "0", "0", 1)
            idPercepcionCab = iResultado

            For i As Integer = 0 To gcPercepcionDetalle.RowCount - 1
                iResultado = ePercepcion.fGrabarDet(0, idPercepcionCab, Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colNumero").Value), Convert.ToDateTime(gcPercepcionDetalle.Rows(i).Cells("colFecha").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajePercepcion, 1)
            Next
        Catch ex As Exception
            MessageBox.Show("No se pudo grabar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            Dim iResultado As Int32

            iResultado = ePercepcion.fEliminar(Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), 3)

            iResultado = ePercepcion.fGrabar(Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), gEmpresa, Convert.ToDecimal(cboDocumentoOrigen.SelectedValue), txtSerie.Text.Trim, txtNroDocumento.Text.Trim, dtFecha.Value, txtCodigoAnexo.Text.Trim, lblNombreAnexo.Text, txtRuc.Text.Trim, lblDireccion.Text, txtObservacion.Text.Trim, 1, decImportePercepcion, decImporteTotal, "0", "0", 2)

            For i As Integer = 0 To gcPercepcionDetalle.RowCount - 1
                If Convert.ToInt32(gcPercepcionDetalle.Rows(i).Cells("colIdPercepcionDet").Value) = 0 Then
                    iResultado = ePercepcion.fGrabarDet(0, Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colNumero").Value), Convert.ToDateTime(gcPercepcionDetalle.Rows(i).Cells("colFecha").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajePercepcion, 1)
                Else
                    iResultado = ePercepcion.fGrabarDet(Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdPercepcionDet").Value), Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colNumero").Value), Convert.ToDateTime(gcPercepcionDetalle.Rows(i).Cells("colFecha").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajePercepcion, 2)
                End If
            Next

        Catch ex As Exception
            MessageBox.Show("No se pudo modificar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function Eliminar() As Boolean
        Try
            If MessageBox.Show("�Est� seguro que desea eliminar este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then

                If Not ComprobarDato() Then Exit Function

                Dim iResultado As Int32
                iResultado = ePercepcion.fEliminar(Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), 3)

                If Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 1 Then ' Orden de Compra
                    For i As Integer = 0 To gcPercepcionDetalle.RowCount - 1
                        'If Convert.ToInt32(gcRetencionDetalle.Rows(i).Cells("colIdPercepcionDet").Value) = 0 Then
                        iResultado = ePercepcion.fGrabarDet(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colIdPercepcionDet").Value), Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colNumero").Value), dtFecha.Value, Convert.ToString(gcPercepcionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcPercepcionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajePercepcion, 3)
                        'End If
                    Next
                End If
                ' 
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo eliminar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Function ComprobarDato() As Boolean
        Dim iResultado As Int32 = 0
        Dim decId As Decimal = Convert.ToDecimal(gcPercepcion.CurrentRow.Cells("colIdPercepcionCab").Value)

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("SELECT 1 FROM Tesoreria.PercepcionCab RC INNER JOIN Tesoreria.PercepcionDet RD ON RC.IDPERCEPCIONCAB = RD.IDPERCEPCIONCAB AND RD.ESTADO = '1' " & _
                             "INNER JOIN Tesoreria.DOC_PENDIENTES DP ON RD.NROITEM = DP.ID_DOCORIGEN WHERE RC.IDPERCEPCIONCAB = " & decId & " AND DP.SALDO <> DP.TOTAL AND RC.ESTADO = '1'", CommandType.Text)

                iResultado = .fCmdExecuteScalar()
                If iResultado >= "1" Then
                    MessageBox.Show("No se puede modificar/eliminar esta orden de compra porque tiene pagos realizados. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                MessageBox.Show("No se pudo comprobar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            .sDesconectarSQL()
        End With

    End Function

#End Region

    'Private Sub chkResevarNumero_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs) Handles chkResevarNumero.ToggleStateChanged
    '    If Not Loaded Then Exit Sub
    '    If chkResevarNumero.Checked = True Then
    '        estado = "0"
    '    Else
    '        estado = "1"
    '    End If
    'End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        If gcPercepcion.RowCount > 0 Then
            Dim export As ExportToExcelML = New ExportToExcelML(Me.gcPercepcion)

            export.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport
            export.ExportVisualSettings = True
            export.SheetName = "Percepcion"
            export.SummariesExportOption = SummariesOption.DoNotExport

            Dim save As SaveFileDialog = New SaveFileDialog()
            save.Filter = "Excel|*.xls"
            save.Title = "Guardar como archivo de excel"
            save.ShowDialog()
            export.FileExtension = "xls"
            export.RunExport(save.FileName)
            'export.RunExport(save.FileName + ".xls")
        End If
    End Sub

    Private Sub btnReservar_Click(sender As Object, e As EventArgs) Handles btnReservar.Click
        Dim Reservas As New frmReservarRetencion()
        If Reservas.ShowDialog = Windows.Forms.DialogResult.OK Then
            'txtCodigoAnexo.Text = frmRegistroRetencion.strCodigo
            'txtRuc.Text = frmRegistroRetencion.strRuc
            'lblNombreAnexo.Text = frmRegistroRetencion.strNombre
            'lblDireccion.Text = frmRegistroRetencion.strDireccion

            '    grpFacturas.Visible = True

            '    If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
            '        odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
            '        gcFacturas.DataSource = odtRetencion
            '    ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then
            '        odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
            '        gcFacturas.DataSource = odtRetencion
            '    End If

            CargarPercepcionCab()
        End If
    End Sub

End Class