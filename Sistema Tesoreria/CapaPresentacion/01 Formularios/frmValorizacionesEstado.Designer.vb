<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValorizacionesEstado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvDetalle = New System.Windows.Forms.DataGridView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtBuscar = New ctrLibreria.Controles.BeTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboPartida = New ctrLibreria.Controles.BeComboBox
        Me.cboObra = New ctrLibreria.Controles.BeComboBox
        Me.cboContratista = New ctrLibreria.Controles.BeComboBox
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column10, Me.Column1, Me.Column2, Me.Column3, Me.Column9, Me.Column8, Me.Column5, Me.Column6, Me.Column7, Me.Column4})
        Me.dgvDetalle.Location = New System.Drawing.Point(2, 106)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(1080, 196)
        Me.dgvDetalle.TabIndex = 9
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtBuscar)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboPartida)
        Me.GroupBox1.Controls.Add(Me.cboObra)
        Me.GroupBox1.Controls.Add(Me.cboContratista)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(508, 98)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'txtBuscar
        '
        Me.txtBuscar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBuscar.BackColor = System.Drawing.Color.Ivory
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtBuscar.KeyEnter = True
        Me.txtBuscar.Location = New System.Drawing.Point(291, 43)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBuscar.ShortcutsEnabled = False
        Me.txtBuscar.Size = New System.Drawing.Size(209, 20)
        Me.txtBuscar.TabIndex = 6
        Me.txtBuscar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Partida"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Contratista"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(30, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Obra"
        '
        'cboPartida
        '
        Me.cboPartida.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPartida.BackColor = System.Drawing.Color.Ivory
        Me.cboPartida.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartida.ForeColor = System.Drawing.Color.Black
        Me.cboPartida.FormattingEnabled = True
        Me.cboPartida.KeyEnter = True
        Me.cboPartida.Location = New System.Drawing.Point(69, 69)
        Me.cboPartida.Name = "cboPartida"
        Me.cboPartida.Size = New System.Drawing.Size(216, 21)
        Me.cboPartida.TabIndex = 2
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(69, 19)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(216, 21)
        Me.cboObra.TabIndex = 1
        '
        'cboContratista
        '
        Me.cboContratista.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboContratista.BackColor = System.Drawing.Color.Ivory
        Me.cboContratista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContratista.ForeColor = System.Drawing.Color.Black
        Me.cboContratista.FormattingEnabled = True
        Me.cboContratista.KeyEnter = True
        Me.cboContratista.Location = New System.Drawing.Point(69, 43)
        Me.cboContratista.Name = "cboContratista"
        Me.cboContratista.Size = New System.Drawing.Size(216, 21)
        Me.cboContratista.TabIndex = 0
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "ObraDescripcion"
        Me.Column10.HeaderText = "Obra"
        Me.Column10.Name = "Column10"
        Me.Column10.Width = 150
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "Contratista"
        Me.Column1.HeaderText = "Contratista"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 170
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "ParDescripcion"
        Me.Column2.HeaderText = "Partida"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 170
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Descripcion"
        Me.Column3.HeaderText = "Valorización"
        Me.Column3.Name = "Column3"
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "Numero"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Column9.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column9.HeaderText = "Número"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 50
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "TotPagar"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column8.HeaderText = "A Pagar"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 70
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "TotPagado"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column5.HeaderText = "T. Pagado"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 90
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "TotRetenido"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column6.HeaderText = "T. Retenido"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 90
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "Saldo"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.Column7.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column7.HeaderText = "Saldo"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 70
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "CCosDescripcion"
        Me.Column4.HeaderText = "Centro C."
        Me.Column4.Name = "Column4"
        '
        'frmValorizacionesEstado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1083, 303)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvDetalle)
        Me.Name = "frmValorizacionesEstado"
        Me.Text = "Valorizaciones Por Pagar"
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboContratista As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboPartida As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtBuscar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
