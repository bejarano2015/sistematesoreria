Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmPeriodo
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer 'Opcion que elige desde el menu de controles de mantenimiento
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Private ePeriodo As clsPeriodo
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmPeriodo = Nothing
    Public Shared Function Instance() As frmPeriodo
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmPeriodo
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'Close()
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtPeriodo.Clear()
        dtpFechaInicio.Value = Now.Date().ToShortDateString
        dtpFechaFin.Value = Now.Date().ToShortDateString
    End Sub

    Private Sub DesabilitarControles()
        txtPeriodo.Enabled = False
        dtpFechaInicio.Enabled = False
        dtpFechaFin.Enabled = False
        cboEstado.Enabled = False
        cboAnio.Enabled = False
        cboMes.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        txtPeriodo.Enabled = True
        dtpFechaInicio.Enabled = True
        dtpFechaFin.Enabled = True
        cboEstado.Enabled = True
        cboAnio.Enabled = True
        cboMes.Enabled = True
    End Sub

    Private Sub DesactivarTeclas(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        e.SuppressKeyPress = True
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If
        Panel2.Visible = True
        cboEstados.SelectedIndex = -1
        cboAnios.SelectedIndex = -1
        txtBusqueda.Clear()
        txtBusqueda.Focus()

    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            cboAnio.Text = Now.Year().ToString
            cboEstado.SelectedIndex = 0
            cboEstado.Enabled = False
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            HabilitarControles()
        End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        ' Else
        ' Exit Sub
        ' End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        eTempo = New clsPlantTempo
        ePeriodo = New clsPeriodo
        Dim Mensaje As String = ""

        If sTab = 1 Then
            If Len(txtPeriodo.Text.Trim) > 0 And cboAnio.SelectedIndex > -1 And cboMes.SelectedIndex > -1 Then
                If iOpcion = 1 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 2 Then
                    Mensaje = "Actualizar"
                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then
                        ePeriodo.fCodigo(gEmpresa)
                        sCodigoRegistro = ePeriodo.sCodFuturo
                        Me.txtCodigo.Text = sCodigoRegistro
                    Else
                        sCodigoRegistro = Trim(Me.txtCodigo.Text)
                    End If
                    iResultado = ePeriodo.fGrabar(sCodigoRegistro, gEmpresa, txtPeriodo.Text.Trim, cboAnio.Text.Trim, TraerNumeroMes(Me.cboMes.Text), Me.dtpFechaInicio.Text.Trim, Me.dtpFechaFin.Text.Trim, Me.cboEstado.SelectedValue, iOpcion)
                    If iResultado > 0 Then
                        mMostrarGrilla()
                        Timer2.Enabled = True
                    End If
                    sTab = 0
                End If
            Else

                If Len(txtPeriodo.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese la Descripcion del Periodo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtPeriodo.Focus()
                    Exit Sub
                End If

                If cboAnio.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboAnio.Focus()
                    Exit Sub
                End If

                If cboMes.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Mes", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboMes.Focus()
                    Exit Sub
                End If
                frmPrincipal.sFlagGrabar = "0"
            End If

        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iActivo As Integer
            If sTab = 0 Then
                Try
                    If Me.dgvLista.Rows.Count > 0 Then
                        If Fila("Estados") = "Activo" Then
                            iActivo = 5
                            Mensaje = "�Desea Anular?"
                        End If
                        If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("IdPeriodo") & "   " & Chr(13) & "Descripci�n: " & Fila("DescripcionPeriodo"), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                            ePeriodo = New clsPeriodo
                            iResultado = ePeriodo.fEliminar(Fila("IdPeriodo"), gEmpresa, iActivo)
                            If iResultado = 1 Then
                                mMostrarGrilla()
                            End If
                        Else
                            Exit Sub
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()
        dtpFechaInicio.Value = Now.Date()
        dtpFechaFin.Value = Now.Date()
        ' cboAnio.DropDownStyle = ComboBoxStyle.Simple
        AddHandler cboAnio.KeyDown, AddressOf Me.DesactivarTeclas

        'cboMes.DropDownStyle = ComboBoxStyle.Simple
        AddHandler cboMes.KeyDown, AddressOf Me.DesactivarTeclas

        Me.cboEstado.DataSource = eTempo.fColEstado
        Me.cboEstado.ValueMember = "Col02"
        Me.cboEstado.DisplayMember = "Col01"

        Me.cboEstados.DataSource = eTempo.fColEstados
        Me.cboEstados.ValueMember = "Col02"
        Me.cboEstados.DisplayMember = "Col01"
        dgvLista.Focus()
    End Sub

    Private Sub mMostrarGrilla()

        ePeriodo = New clsPeriodo
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = ePeriodo.fListar(iEstado01, iEstado02, gEmpresa)
        Me.dgvLista.AutoGenerateColumns = False
        Me.dgvLista.DataSource = dtTableLista
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        'MsgBox(cmr.ToString)
        

        'VerPosicion()
        Me.stsTotales.Items(0).Text = "Total de Registros= " & ePeriodo.iNroRegistros
        If ePeriodo.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If

    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdPeriodo").ToString
                Me.txtPeriodo.Text = Fila("DescripcionPeriodo").ToString
                Me.cboAnio.Text = Fila("A�o").ToString
                Me.cboMes.Text = TraerNombreMes(Fila("Mes").ToString)
                Me.dtpFechaInicio.Text = Fila("FechaInicio").ToString
                Me.dtpFechaFin.Text = Fila("FechaFin").ToString
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLista.CellDoubleClick
        v_ModificaReg()
        frmPadre00.vModificaRegistro()
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtPeriodo.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    'Private Sub txtPeriodo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPeriodo.KeyPress
    '    MsgBox(Trim(e.KeyChar.ToString))
    '    If Trim(e.KeyChar.ToString) = "." Then
    '    End If
    'End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text.Trim) > 0 Then
            ePeriodo = New clsPeriodo
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = ePeriodo.fBuscarPeriodos(13, Trim(txtBusqueda.Text), "", gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & ePeriodo.iNroRegistros
            If ePeriodo.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text.Trim) = 0 Then
            ePeriodo = New clsPeriodo
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = ePeriodo.fBuscarPeriodos(14, "", "", gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & ePeriodo.iNroRegistros
            If ePeriodo.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If
    End Sub

    Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
        If (cboEstados.SelectedIndex > -1) Then
            Try
                ePeriodo = New clsPeriodo
                eTempo = New clsPlantTempo
                dtTable = New DataTable
                Dim sValorEstado As String = ""
                sValorEstado = cboEstados.SelectedValue.ToString
                If sValorEstado <> "CapaDatos.clsPlantTempo" Then
                    dtTable = ePeriodo.fBuscarPeriodos(15, "", "", gEmpresa, cboEstados.SelectedValue)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & ePeriodo.iNroRegistros
                    If ePeriodo.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub BeLabel8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel8.Click
        Panel2.Visible = False
    End Sub

    Private Sub cboAnios_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAnios.SelectedIndexChanged
        If (cboAnios.SelectedIndex > -1) Then
            Try
                ePeriodo = New clsPeriodo
                eTempo = New clsPlantTempo
                dtTable = New DataTable
                Dim sValorEstado As String = ""
                sValorEstado = cboAnios.Text
                If sValorEstado <> "CapaDatos.clsPlantTempo" Then
                    dtTable = ePeriodo.fBuscarPeriodos(16, "", Trim(cboAnios.Text), gEmpresa, 0)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & ePeriodo.iNroRegistros
                    If ePeriodo.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub
End Class
