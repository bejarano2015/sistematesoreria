Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Imports IWshRuntimeLibrary
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.OleDb

Public Class frmPartidaAvanceDiario

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmPartidaAvanceDiario = Nothing
    Public Shared Function Instance() As frmPartidaAvanceDiario
        If frmInstance Is Nothing Then
            frmInstance = New frmPartidaAvanceDiario
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmPartidaAvanceDiario_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eValorizaciones As clsValorizaciones

    Private ePartidas As clsPartidas
    Private Sub frmPartidaAvanceDiario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eValorizaciones = New clsValorizaciones
        'cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today())

        If Trim(glbUsuCategoria) = "A" Then
            cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today(), "")
        ElseIf Trim(glbUsuCategoria) = "U" Then
            cboObra.DataSource = eValorizaciones.fListarObras(73, gEmpresa, gPeriodo, "", gUsuario, Today(), "")
        End If

        If eValorizaciones.iNroRegistros > 0 Then
            cboObra.ValueMember = "ObraCodigo"
            cboObra.DisplayMember = "ObraDescripcion"
            cboObra.SelectedIndex = -1
        End If
        dtpEnvio.Value = Now.Date()

        lblNO.Text = ""
        lblNCon.Text = ""
        lblUbi.Text = ""
    End Sub


    Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObra.SelectedIndexChanged

    End Sub

    Private Sub cboObra_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboObra.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            'cboContratista.DataSource = eValorizaciones.fListarObras(36, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today())

            If Trim(glbUsuCategoria) = "A" Then
                'cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today())
                cboContratista.DataSource = eValorizaciones.fListarObras(36, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")

            ElseIf Trim(glbUsuCategoria) = "U" Then
                'cboObra.DataSource = eValorizaciones.fListarObras(73, gEmpresa, gPeriodo, "", gUsuario, Today())
                cboContratista.DataSource = eValorizaciones.fListarObras(74, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")
            End If

            If eValorizaciones.iNroRegistros > 0 Then
                cboContratista.ValueMember = "IdContratista"
                cboContratista.DisplayMember = "Nombre"
                cboContratista.SelectedIndex = -1

                CCosto.DataSource = Nothing
                CCosto.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
                'cboContratista.SelectedIndex = -1

            End If
        End If
    End Sub

    'Private Sub cboUbiTrabajo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboContratista.SelectedIndexChanged

    'End Sub



    Private Sub cboPartidas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectedIndexChanged
        If cboPartidas.SelectedIndex = -1 Then
            For x As Integer = 0 To dgvDetalle.RowCount - 1
                dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
            Next
            'BeLabel18.Text = "Total de Items : 0"
            'txtContratista.Clear()
            'txtCodigoContratista.Clear()
            'txtNumSesion.Clear()

            F1.Value = Today()
            F2.Value = Today()
            lblNO.Text = ""
            lblNCon.Text = ""
            lblUbi.Text = ""

        End If

        Try
            AxMonthView1.Value = dtpEnvio.Value
            txtSemana.Text = AxMonthView1.Week()

            AxMonthView2.Value = dtpEnvio.Value
            cboSemana.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboPartidas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectionChangeCommitted

        'If cboPartidas.SelectedIndex <> -1 Then
        '    eValorizaciones = New clsValorizaciones
        '    cboPartidas.DataSource = eValorizaciones.fListarObras(36, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue))
        '    If eValorizaciones.iNroRegistros > 0 Then
        '        cboPartidas.ValueMember = "ubtCodigo"
        '        cboPartidas.DisplayMember = "ubtDescripcion"
        '        cboPartidas.SelectedIndex = -1
        '    End If
        'End If

        If cboPartidas.SelectedIndex <> -1 Then
            Dim dtTable2 As DataTable
            dtTable2 = New DataTable
            eValorizaciones = New clsValorizaciones
            dtTable2 = eValorizaciones.fListarObras(2, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Convert.ToString(cboPartidas.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If dtTable2.Rows.Count = 1 Then
                'RetencionPorcentaje = dtTable2.Rows(0).Item("xret")
                'BeLabel12.Text = "Retenci�n Fondo De Garant�a (" & RetencionPorcentaje & "%)"
                'BeLabel11.Text = "              Valorizaci�n N�                 "
                'txtUbicacion.Text = UCase(dtTable2.Rows(0).Item("ubtDescripcion"))
                'txtNumeroOrden.Text = UCase(dtTable2.Rows(0).Item("NumOrden"))
                'txtNroContrato.Text = UCase(dtTable2.Rows(0).Item("NContrato"))
                'aqui se muestra el numero actual de la valorizacion o el siguiente a crear
                'txtNumSesion.Text = sMaxValor
                'txtSemana.Clear()
                'txtSemana.Focus()
                F1.Value = dtTable2.Rows(0).Item("FechaInicio")
                F2.Value = dtTable2.Rows(0).Item("FechaFinal")
                lblNO.Text = dtTable2.Rows(0).Item("NumOrden")
                lblNCon.Text = dtTable2.Rows(0).Item("NContrato")
                lblUbi.Text = dtTable2.Rows(0).Item("ubtDescripcion")
            Else
                F1.Value = Today()
                F2.Value = Today()
                lblNO.Text = ""
                lblNCon.Text = ""
                lblUbi.Text = ""
            End If
        Else
            'txtSemana.Clear()
            'txtSemana.Focus()
        End If

        If cboPartidas.SelectedIndex <> -1 Then
            ePartidas = New clsPartidas
            Dim dtDetalleDia As DataTable
            dtDetalleDia = New DataTable
            dtDetalleDia = ePartidas.fListarDetalleDia(4, gEmpresa, (cboPartidas.SelectedValue), dtpEnvio.Value, gPeriodo, Trim(CCosto.SelectedValue))

            If dtDetalleDia.Rows.Count > 0 Then
                dgvDetalle.AutoGenerateColumns = False
                If dtDetalleDia.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDetalle.RowCount - 1
                        dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    Next
                    For y As Integer = 0 To dtDetalleDia.Rows.Count - 1
                        dgvDetalle.Rows.Add()
                        dgvDetalle.Rows(y).Cells("Fecha").Value = dtpEnvio.Value
                        dgvDetalle.Rows(y).Cells("Item").Value = dtDetalleDia.Rows(y).Item("Descripcion").ToString
                        dgvDetalle.Rows(y).Cells("Total").Value = dtDetalleDia.Rows(y).Item("MetradoImpor").ToString
                        dgvDetalle.Rows(y).Cells("UM").Value = dtDetalleDia.Rows(y).Item("UnidMed").ToString
                        dgvDetalle.Rows(y).Cells("Avance").Value = dtDetalleDia.Rows(y).Item("AvanceDia").ToString
                        dgvDetalle.Rows(y).Cells("Saldo").Value = dtDetalleDia.Rows(y).Item("Avanzado").ToString
                        dgvDetalle.Rows(y).Cells("Semana").Value = Trim(txtSemana.Text)
                        dgvDetalle.Rows(y).Cells("IdPartidaDet").Value = dtDetalleDia.Rows(y).Item("IdPartidaDet").ToString
                        dgvDetalle.Rows(y).Cells("IdPartidaAvanceDiario").Value = dtDetalleDia.Rows(y).Item("IdPartidaAvanceDiario").ToString
                        dgvDetalle.Rows(y).Cells("Estado").Value = Trim(dtDetalleDia.Rows(y).Item("Estado").ToString)
                        If Trim(dgvDetalle.Rows(y).Cells("Estado").Value) = "1" Then
                            dgvDetalle.Rows(y).ReadOnly = True
                        ElseIf Trim(dgvDetalle.Rows(y).Cells("Estado").Value) = "0" Then
                            dgvDetalle.Rows(y).ReadOnly = False
                        End If
                        dgvDetalle.Rows(y).Cells("Estado").Value = "1"
                    Next
                ElseIf dtDetalleDia.Rows.Count = 0 Then

                    'BeLabel18.Text = "Total de Items : 0"
                    

                End If
                If dgvDetalle.Rows.Count > 0 Then
                    dgvDetalle.CurrentCell = dgvDetalle(5, 0)
                    dgvDetalle.Focus()
                End If
                Exit Sub


            End If

            ePartidas = New clsPartidas
            Dim dtDetalle As DataTable
            dtDetalle = New DataTable
            dtDetalle = ePartidas.fListarDetalle(21, gEmpresa, Trim(cboPartidas.SelectedValue), dtpEnvio.Value, gPeriodo, Trim(CCosto.SelectedValue))
            dgvDetalle.AutoGenerateColumns = False
            If dtDetalle.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
                For y As Integer = 0 To dtDetalle.Rows.Count - 1
                    dgvDetalle.Rows.Add()
                    dgvDetalle.Rows(y).Cells("Fecha").Value = dtpEnvio.Value
                    dgvDetalle.Rows(y).Cells("Item").Value = dtDetalle.Rows(y).Item("Descripcion").ToString
                    dgvDetalle.Rows(y).Cells("Total").Value = dtDetalle.Rows(y).Item("MetradoImpor").ToString
                    dgvDetalle.Rows(y).Cells("UM").Value = dtDetalle.Rows(y).Item("UnidMed").ToString
                    dgvDetalle.Rows(y).Cells("Avance").Value = "0.00"
                    dgvDetalle.Rows(y).Cells("Saldo").Value = dtDetalle.Rows(y).Item("Avanzado").ToString
                    'MessageBox.Show(dtDetalle.Rows(y).Item("Avanzado").ToString)
                    dgvDetalle.Rows(y).Cells("Semana").Value = Trim(txtSemana.Text)
                    dgvDetalle.Rows(y).Cells("IdPartidaDet").Value = dtDetalle.Rows(y).Item("IdPartidaDet").ToString
                    dgvDetalle.Rows(y).Cells("IdPartidaAvanceDiario").Value = ""
                    dgvDetalle.Rows(y).Cells("Estado").Value = "0"
                    dgvDetalle.Rows(y).ReadOnly = False
                Next
            ElseIf dtDetalle.Rows.Count = 0 Then
            End If

            Dim sCerrarValo As String = ""
            Dim dtValorizacion As DataTable
            dtValorizacion = New DataTable
            dtValorizacion = eValorizaciones.fBuscarValorizacion(38, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), Convert.ToString(Trim(txtSemana.Text)), Today(), Today(), gPeriodo, Trim(CCosto.SelectedValue))
            If dtValorizacion.Rows.Count > 0 Then
                sCerrarValo = dtValorizacion.Rows(0).Item("Cerrado")
                If sCerrarValo = "1" Then
                    For x As Integer = 0 To dgvDetalle.RowCount - 1
                        dgvDetalle.Rows(x).ReadOnly = True
                    Next
                End If
            End If

        End If

        If dgvDetalle.Rows.Count > 0 Then
            dgvDetalle.CurrentCell = dgvDetalle(5, 0)
            dgvDetalle.Focus()
        End If
    End Sub

    Private Sub dtpEnvio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpEnvio.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                cboPartidas_SelectionChangeCommitted(sender, e)
                If dgvDetalle.Rows.Count > 0 Then
                    dgvDetalle.CurrentCell = dgvDetalle(5, 0)
                    dgvDetalle.Focus()
                End If
                'ePartidas = New clsPartidas
                'Dim dtDetalleDia As DataTable
                'dtDetalleDia = New DataTable
                'dtDetalleDia = ePartidas.fListarDetalleDia(4, gEmpresa, (cboPartidas.SelectedValue), dtpEnvio.Value)

                'If dtDetalleDia.Rows.Count > 0 Then

                '    dgvDetalle.AutoGenerateColumns = False
                '    If dtDetalleDia.Rows.Count > 0 Then
                '        For x As Integer = 0 To dgvDetalle.RowCount - 1
                '            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                '        Next
                '        For y As Integer = 0 To dtDetalleDia.Rows.Count - 1
                '            dgvDetalle.Rows.Add()
                '            dgvDetalle.Rows(y).Cells("Fecha").Value = dtpEnvio.Value
                '            dgvDetalle.Rows(y).Cells("Item").Value = dtDetalleDia.Rows(y).Item("Descripcion").ToString
                '            dgvDetalle.Rows(y).Cells("Total").Value = dtDetalleDia.Rows(y).Item("MetradoImpor").ToString
                '            dgvDetalle.Rows(y).Cells("UM").Value = dtDetalleDia.Rows(y).Item("UnidMed").ToString
                '            dgvDetalle.Rows(y).Cells("Avance").Value = dtDetalleDia.Rows(y).Item("AvanceDia").ToString
                '            dgvDetalle.Rows(y).Cells("Saldo").Value = dtDetalleDia.Rows(y).Item("Saldo").ToString
                '            dgvDetalle.Rows(y).Cells("Semana").Value = Trim(txtSemana.Text)
                '            dgvDetalle.Rows(y).Cells("IdPartidaDet").Value = dtDetalleDia.Rows(y).Item("IdPartidaDet").ToString
                '            dgvDetalle.Rows(y).Cells("IdPartidaAvanceDiario").Value = dtDetalleDia.Rows(y).Item("IdPartidaAvanceDiario").ToString
                '            dgvDetalle.Rows(y).Cells("Estado").Value = "1"
                '        Next
                '    ElseIf dtDetalleDia.Rows.Count = 0 Then
                '        'BeLabel18.Text = "Total de Items : 0"
                '    End If
                '    If dgvDetalle.Rows.Count > 0 Then
                '        dgvDetalle.CurrentCell = dgvDetalle(3, 0)
                '        dgvDetalle.Focus()
                '    End If
                '    Exit Sub
                'End If
        End Select
    End Sub

   

    Private Sub dtpEnvio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpEnvio.ValueChanged
        Try
            AxMonthView1.Value = dtpEnvio.Value
            txtSemana.Text = AxMonthView1.Week()
            cboSemana.Text = AxMonthView1.Week()
            'AxMonthView2.Value = dtpEnvio.Value
            'txtSemana.Text = AxMonthView2.Week()
        Catch ex As Exception
        End Try

        'If cboPartidas.SelectedIndex > -1 Then
        '    If dtpEnvio.Value < F1.Value Or dtpEnvio.Value > F2.Value Then
        '        If dtpEnvio.Value < F1.Value Then
        '            MessageBox.Show("La Fecha de Avance es Menor a la fecha de inicio del contrato", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            dtpEnvio.Value = F1.Value
        '            Exit Sub
        '        End If
        '        If dtpEnvio.Value > F2.Value Then
        '            MessageBox.Show("La Fecha de Avance es Mayor a la fecha de t�rmino del contrato", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            dtpEnvio.Value = F1.Value
        '            Exit Sub
        '        End If
        '    End If
        'End If

        cboPartidas_SelectionChangeCommitted(sender, e)
        'If dgvDetalle.Rows.Count > 0 Then
        '    dgvDetalle.CurrentCell = dgvDetalle(4, 0)
        '    dgvDetalle.Focus()
        'End If

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        If cboPartidas.SelectedIndex > -1 Then
            If dtpEnvio.Value < F1.Value Or dtpEnvio.Value > F2.Value Then
                If dtpEnvio.Value < F1.Value Then
                    MessageBox.Show("La Fecha de Avance es Menor a la fecha de inicio del contrato", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'dtpEnvio.Value = F1.Value
                    Exit Sub
                End If
                If dtpEnvio.Value > F2.Value Then
                    MessageBox.Show("La Fecha de Avance es Mayor a la fecha de t�rmino del contrato", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'dtpEnvio.Value = F1.Value
                    Exit Sub
                End If
            End If
        End If

        If dgvDetalle.Rows.Count > 0 Then
            Dim iRes As Integer = 0
            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                Dim xIdPartidaDet As String
                Dim sCodigoRegistroDet As String = ""
                Dim xIdPartidaAvanceDiario As String = ""
                Dim xAvanceDia As Double
                Dim xFecha As DateTime
                xFecha = dgvDetalle.Rows(x).Cells("Fecha").Value
                xAvanceDia = dgvDetalle.Rows(x).Cells("Avance").Value
                xIdPartidaDet = dgvDetalle.Rows(x).Cells("IdPartidaDet").Value
                xIdPartidaAvanceDiario = dgvDetalle.Rows(x).Cells("IdPartidaAvanceDiario").Value
                If dgvDetalle.Rows(x).Cells("Estado").Value = "0" Then
                    ePartidas = New clsPartidas
                    ePartidas.fCodigoDetDiario(3, gEmpresa, (cboPartidas.SelectedValue), gPeriodo)
                    sCodigoRegistroDet = ePartidas.sCodFuturo
                    iRes = ePartidas.fGrabarDetalleAvanceDiario(1, sCodigoRegistroDet, (cboPartidas.SelectedValue), xIdPartidaDet, gEmpresa, xFecha, txtSemana.Text, xAvanceDia, gUsuario, gPeriodo, Trim(CCosto.SelectedValue))
                ElseIf dgvDetalle.Rows(x).Cells("Estado").Value = "1" Then
                    sCodigoRegistroDet = Trim(xIdPartidaAvanceDiario)
                    iRes = ePartidas.fGrabarDetalleAvanceDiario(2, sCodigoRegistroDet, (cboPartidas.SelectedValue), xIdPartidaDet, gEmpresa, xFecha, txtSemana.Text, xAvanceDia, gUsuario, gPeriodo, Trim(CCosto.SelectedValue))
                End If
            Next
            If iRes > 0 Then
                MessageBox.Show("Se Grabo con Exito el avance del d�a " & dtpEnvio.Value, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
            cboPartidas_SelectionChangeCommitted(sender, e)
        End If

    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        If e.ColumnIndex = 4 Or e.ColumnIndex = 5 Then
            If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If

            If e.ColumnIndex = 5 Then
                Dim avance_anteriores As Double = 0
                Dim avance_dia As Double = 0
                Dim avance_total As Double = 0
                Dim Contratado As Double = 0
                avance_anteriores = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Saldo").Value
                avance_dia = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Avance").Value
                avance_total = Format(avance_anteriores + avance_dia, "#,##0.00")
                Contratado = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Total").Value

                If avance_total > Contratado Then
                    MessageBox.Show("El metrado de avance es mayor al metrado del contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Avance").Value = "0.00"
                    dgvDetalle.CurrentCell = dgvDetalle(5, dgvDetalle.CurrentRow.Index)
                    Exit Sub
                End If

            End If

        End If
    End Sub

    Private Sub dgvDetalle_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalle.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If columna = 4 Or columna = 5 Then
            Dim caracter As Char = e.KeyChar
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        'If cboPartidas.SelectedIndex > -1 Then
        '    Dim x As frmReporteAvanceDiario = frmReporteAvanceDiario.Instance
        '    x.MdiParent = frmPrincipal
        '    'x.IdValorizacion = Trim(txtCodigoVal.Text)
        '    x.IdPartida = Trim(cboPartidas.SelectedValue)
        '    x.Fecha = dtpEnvio.Value
        '    x.Show()
        '    'x.ShowDialog()
        'ElseIf cboPartidas.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione Una Partida", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    cboPartidas.Focus()
        'End If

        If cboObra.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione Una Obra", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboObra.Focus()
            Exit Sub
        End If
        If cboContratista.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Contratista", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboContratista.Focus()
            Exit Sub
        End If
        If cboPartidas.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione Un Contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboPartidas.Focus()
            Exit Sub
        End If

        If cboSemana.SelectedIndex > 0 Then
            '   Try
            eValorizaciones = New clsValorizaciones
            Dim dtValorizaciones As DataTable
            dtValorizaciones = New DataTable
            dtValorizaciones = eValorizaciones.fBuscarValorizacion(72, gEmpresa, Trim(cboPartidas.SelectedValue), 0, AxMonthView2.SelStart, AxMonthView2.SelEnd, gPeriodo, Trim(CCosto.SelectedValue))
            If dtValorizaciones.Rows.Count > 0 Then
                Muestra_Reporte(RutaAppReportes & "rptAvanceDiarioPartidaSemana.rpt", dtValorizaciones, "TblLibroDiario", "", "@Empresa;" & gDesEmpresa, "@Ruc;" & gEmprRuc, "FLunes;" & AxMonthView2.SelStart, "FMartes;" & AxMonthView2.SelStart.AddDays(1), "FMiercoles;" & AxMonthView2.SelStart.AddDays(2), "FJueves;" & AxMonthView2.SelStart.AddDays(3), "FViernes;" & AxMonthView2.SelStart.AddDays(4), "FSabado;" & AxMonthView2.SelStart.AddDays(5), "FDomingo;" & AxMonthView2.SelEnd)
            ElseIf dtValorizaciones.Rows.Count = 0 Then
                Dim mensaje As String = ""
                mensaje = "No se Encontr� ning�n Avance."
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            'Catch ex As Exception
            '   MsgBox(ex.Message)
            ' MessageBox.Show("Ocurri� un problema en la transacci�n." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            ' End Try
            Me.Cursor = Cursors.Default
        Else
            MessageBox.Show("Seleccione una Semana", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboSemana.Focus()
        End If

    End Sub


    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function


  
    Private Sub cboSemana_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSemana.SelectedIndexChanged
        Try
            If cboSemana.SelectedIndex > -1 Then
                Try
                    Dim vFinal As Date
                    AxMonthView2.MultiSelect = False
                    AxMonthView2.Week = CInt(cboSemana.Text)
                    AxMonthView2.DayOfWeek = MSComCtl2.DayConstants.mvwMonday
                    AxMonthView2.MultiSelect = True
                    AxMonthView2.MaxSelCount = 7
                    vFinal = DateAdd(DateInterval.Day, 6, AxMonthView2.SelStart)
                    AxMonthView2.SelEnd = vFinal
                    'dtpFechaIni.Value = AxMonthView2.SelStart
                    'dtpFechaFin.Value = AxMonthView2.SelEnd
                    'AxMonthView2.ShowWeekNumbers(
                Catch ex As Exception
                End Try
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboContratista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboContratista.SelectedIndexChanged



        If cboContratista.SelectedIndex = -1 Then
            F1.Value = Today()
            F2.Value = Today()
            lblNO.Text = ""
            lblNCon.Text = ""
            lblUbi.Text = ""
        End If

    End Sub

    Private Sub cboContratista_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboContratista.SelectionChangeCommitted

        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            CCosto.DataSource = eValorizaciones.fListarObras(76, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                CCosto.ValueMember = "CCosCodigo"
                CCosto.DisplayMember = "CCosDescripcion"
                CCosto.SelectedIndex = -1

                'CCosto.DataSource = Nothing
                'CCosto.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
            End If
            'End If
        End If

        
    End Sub

   

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub

    Private Sub txtSemana_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSemana.TextChanged
        'cboSemana.Text = txtSemana.Text
    End Sub

    Private Sub CCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CCosto.SelectedIndexChanged

    End Sub

    Private Sub CCosto_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles CCosto.SelectionChangeCommitted
        If cboContratista.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            cboPartidas.DataSource = eValorizaciones.fListarObras(37, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If eValorizaciones.iNroRegistros > 0 Then
                cboPartidas.ValueMember = "IdPartida"
                cboPartidas.DisplayMember = "ParDescripcion"
                cboPartidas.SelectedIndex = -1
            End If
        End If
    End Sub
End Class