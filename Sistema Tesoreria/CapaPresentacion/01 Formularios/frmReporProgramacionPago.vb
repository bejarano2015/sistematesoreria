Imports CapaNegocios
Imports CapaEntidad

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporProgramacionPago

    'Public nroArqueo As String
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    Private eGestionProgramacionPagos As clsProgramacionPagos
    Private odtProgramacionPago As New DataTable
    Private rptPagos As New rptProgramacionPagos

    Private opcion As Decimal = 1
    Private docOrigen As Decimal = 1
    Private nombreorigen As String

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmReporProgramacionPago = Nothing
    Public Shared Function Instance() As frmReporProgramacionPago
        If frmInstance Is Nothing Then
            frmInstance = New frmReporProgramacionPago
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

#End Region

#Region " Prodedimientos de evento de controles "

    Private Sub frmReporGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eGestionProgramacionPagos = New clsProgramacionPagos()

        'cboDocumento.SelectedIndex = 0
        dtFechaInicio.Value = Date.Today
        dtFechaFin.Value = Date.Today

        InitCombos()
        CargarReportePagos()
    End Sub

    Private Sub frmReporGastos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub
    Private Sub frmReporGastos_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

    Private Sub cboDocumento_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboEmpresa.SelectedIndexChanged
        'If cboEmpresa.SelectedIndex = 0 Then
        '    opcion = 1 ' Orden de compra
        '    docOrigen = 1
        '    nombreorigen = "ORDEN DE COMPRA"
        'End If
    End Sub

    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        CargarReportePagos()
    End Sub

#End Region

#Region " M�todos privados "

    Private Sub InitCombos()
        Try
            Dim odtEmpresa As New DataTable
            odtEmpresa = eGestionProgramacionPagos.fCargarReporte(dtFechaInicio.Value, dtFechaFin.Value, 0)
            cboEmpresa.DataSource = odtEmpresa
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarReportePagos()
        Try
            'Dim odtData As New DataTable

            odtProgramacionPago = eGestionProgramacionPagos.fCargarReporte(dtFechaInicio.Value, dtFechaFin.Value, 1)
            Dim TipoCambio As Decimal = Convert.ToDecimal(txtTipoCambio.Text)

            Dim rptPagos As New rptProgramacionPagos()
            rptPagos.DataSource = odtProgramacionPago
            rptPagos.AddItems(gDesEmpresa, gEmprRuc)
            rptPagos.ReportParameters("paramTipoCambio").Value = TipoCambio

            Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
            instanceReportSource.ReportDocument = rptPagos
            rvReporte.ReportSource = instanceReportSource
            rvReporte.RefreshReport()
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

End Class