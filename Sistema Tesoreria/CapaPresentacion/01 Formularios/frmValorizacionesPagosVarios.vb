Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmValorizacionesPagosVarios

    Private eValorizaciones As clsValorizaciones
    Dim GrabarActualizar As String = ""

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmValorizacionesPagosVarios = Nothing
    Public Shared Function Instance() As frmValorizacionesPagosVarios
        If frmInstance Is Nothing Then
            frmInstance = New frmValorizacionesPagosVarios
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmValorizacionesPagosVarios_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmValorizacionesPagosVarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eValorizaciones = New clsValorizaciones
        'cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today())

        If Trim(glbUsuCategoria) = "A" Then
            cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today(), "")
        ElseIf Trim(glbUsuCategoria) = "U" Then
            cboObra.DataSource = eValorizaciones.fListarObras(73, gEmpresa, gPeriodo, "", gUsuario, Today(), "")
        End If

        If eValorizaciones.iNroRegistros > 0 Then
            cboObra.ValueMember = "ObraCodigo"
            cboObra.DisplayMember = "ObraDescripcion"
            cboObra.SelectedIndex = -1
        End If
        'dtpEnvio.Value = Now.Date().ToShortDateString()
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        'dtpFechaFin.Value = Now.Date().ToShortDateString()
        'dtpEnvio.Value = Now.Date()
    End Sub

    Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObra.SelectedIndexChanged

    End Sub

    Private Sub cboObra_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboObra.SelectionChangeCommitted
        If cboObra.SelectedIndex > -1 Then
            eValorizaciones = New clsValorizaciones
            'cboContratista.DataSource = eValorizaciones.fListarObras(1, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today())

            If Trim(glbUsuCategoria) = "A" Then
                'cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today())
                'cboContratista.DataSource = eValorizaciones.fListarObras(36, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today())
                cboContratista.DataSource = eValorizaciones.fListarObras(1, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")
            ElseIf Trim(glbUsuCategoria) = "U" Then
                'cboObra.DataSource = eValorizaciones.fListarObras(73, gEmpresa, gPeriodo, "", gUsuario, Today())
                'cboContratista.DataSource = eValorizaciones.fListarObras(74, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today())
                cboContratista.DataSource = eValorizaciones.fListarObras(74, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")
            End If

            If eValorizaciones.iNroRegistros > 0 Then
                cboContratista.ValueMember = "IdContratista"
                cboContratista.DisplayMember = "Nombre"
                cboContratista.SelectedIndex = -1

                CCosto.DataSource = Nothing
                CCosto.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1

            End If
        End If

        'cboPartidas.DataSource = Nothing
        'cboPartidas.SelectedIndex = -1

        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
        txtCodigoVal.Clear()
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        rdbT1.Checked = False
        rdbT2.Checked = False
        cboAnexo.DataSource = Nothing
        cboAnexo.SelectedIndex = -1
        txtDescripcion.Clear()
        txtTotPagar.Clear()
        GrabarActualizar = "Grabar"
    End Sub

    Private Sub cboContratista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboContratista.SelectedIndexChanged

    End Sub

    Private Sub cboContratista_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboContratista.SelectionChangeCommitted

        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            CCosto.DataSource = eValorizaciones.fListarObras(76, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                CCosto.ValueMember = "CCosCodigo"
                CCosto.DisplayMember = "CCosDescripcion"
                CCosto.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
            End If
            'End If
        End If

        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
        txtCodigoVal.Clear()
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        rdbT1.Checked = False
        rdbT2.Checked = False
        cboAnexo.DataSource = Nothing
        cboAnexo.SelectedIndex = -1
        txtDescripcion.Clear()
        txtTotPagar.Clear()
        GrabarActualizar = "Grabar"


        'If cboObra.SelectedIndex <> -1 Then
        '    eValorizaciones = New clsValorizaciones
        '    cboPartidas.DataSource = eValorizaciones.fListarObras(15, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today())
        '    If eValorizaciones.iNroRegistros > 0 Then
        '        cboPartidas.ValueMember = "IdPartida"
        '        cboPartidas.DisplayMember = "ParDescripcion"
        '        cboPartidas.SelectedIndex = -1
        '    End If
        '    'End If
        'End If

        'For x As Integer = 0 To dgvDetalle.RowCount - 1
        '    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        'Next
        'txtCodigoVal.Clear()
        'dtpFechaIni.Value = Now.Date().ToShortDateString()
        'rdbT1.Checked = False
        'rdbT2.Checked = False
        'cboAnexo.DataSource = Nothing
        'cboAnexo.SelectedIndex = -1
        'txtDescripcion.Clear()
        'txtTotPagar.Clear()
        'GrabarActualizar = "Grabar"
    End Sub

    Private Sub cboPartidas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectedIndexChanged
      
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbT1.CheckedChanged
        Try
            eValorizaciones = New clsValorizaciones
            cboAnexo.DataSource = eValorizaciones.fListarObras(32, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                cboAnexo.ValueMember = "IdTipoPago"
                cboAnexo.DisplayMember = "Descripcion"
                cboAnexo.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
        txtTotPagar.Clear()
        cboAnexo.Enabled = True
        'If rdbT2.Checked = False Then
        '    btnBuscarDet.Visible = False
        'End If

    End Sub

    Public hace As Integer = 0
    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbT2.CheckedChanged


        cboAnexo.Enabled = False

        Try
            eValorizaciones = New clsValorizaciones
            cboAnexo.DataSource = eValorizaciones.fListarObras(33, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If eValorizaciones.iNroRegistros > 0 Then
                cboAnexo.ValueMember = "IdTipoPago"
                cboAnexo.DisplayMember = "Descripcion"
                cboAnexo.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try

        'If rdbT2.Checked = True Then
        '    btnBuscarDet.Visible = True
        'End If

        If GrabarActualizar <> "Actualizar" Then
            If cboPartidas.SelectedIndex > -1 And rdbT2.Checked = True Then
                Dim x As frmVePartidasContrato = frmVePartidasContrato.Instance
                x.Owner = Me
                'x.MdiParent = frmPrincipal
                x.IdContrato = Trim(cboPartidas.SelectedValue)
                x.CCosto = Trim(CCosto.SelectedValue)
                x.ShowInTaskbar = False
                x.ShowDialog()
            End If
        End If

        If hace = 0 Then
            Exit Sub
        End If

        If cboPartidas.SelectedIndex > -1 Then
            'For x As Integer = 0 To dgvDetalle.RowCount - 1
            '    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
            'Next
            eValorizaciones = New clsValorizaciones
            Dim dtValorizacion As DataTable
            dtValorizacion = New DataTable
            Dim ContraOriginal As Double = 0
            Dim ContraActual As Double = 0
            dtValorizacion = eValorizaciones.fBuscarValorizacion(40, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), 0, dtpFechaIni.Value, Today(), gPeriodo, Trim(CCosto.SelectedValue))
            If dtValorizacion.Rows.Count > 0 Then
                If Microsoft.VisualBasic.IsDBNull(dtValorizacion.Rows(0).Item("Ori_ParValorizado")) = False Then
                    ContraOriginal = dtValorizacion.Rows(0).Item("Ori_ParValorizado")
                End If

                If Microsoft.VisualBasic.IsDBNull(dtValorizacion.Rows(0).Item("ParValorizado")) = False Then
                    ContraActual = dtValorizacion.Rows(0).Item("ParValorizado")
                End If

                'ContraOriginal = 


                If ContraOriginal < ContraActual Then
                    cboAnexo.SelectedValue = "00003"
                    txtTotPagar.Text = Format(ContraActual - ContraOriginal, "#,##0.00")
                ElseIf ContraOriginal > ContraActual Then
                    cboAnexo.SelectedValue = "00004"
                    txtTotPagar.Text = Format(ContraOriginal - ContraActual, "#,##0.00")
                End If

            End If
        End If
        txtDescripcion.Focus()
        'cboAnexo.Enabled = False

    End Sub

    Private Sub RadioButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbT1.Click
        
    End Sub

    Private Sub RadioButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbT2.Click
       
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim iResultado As Integer
        Dim iTipo2 As Integer
        If rdbT1.Checked = True Then
            iTipo2 = 1
        End If
        If rdbT2.Checked = True Then
            iTipo2 = 2
        End If

        If cboObra.SelectedIndex > -1 And cboContratista.SelectedIndex > -1 And cboPartidas.SelectedIndex > -1 And cboAnexo.SelectedIndex > -1 And Len(Trim(txtDescripcion.Text)) > 0 And Len(Trim(txtTotPagar.Text)) > 0 Then
            If Trim(cboAnexo.SelectedValue) = "00002" Then
                If Trim(GrabarActualizar) = "Grabar" Then
                    If Convert.ToDouble(txtTotPagar.Text) > Convert.ToDouble(lblRetenido.Text) Then
                        MessageBox.Show("El Importe a pagar como Fondo de Garantia es Mayor al Total Retenido del Contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtTotPagar.Focus()
                        Exit Sub
                    ElseIf Val(txtTotPagar.Text) <= 0 Then
                        MessageBox.Show("El Importe a pagar como Fondo de Garantia, No puede ser 0 o menor a este.", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtTotPagar.Focus()
                        Exit Sub
                    End If
                    'ElseIf Trim(GrabarActualizar) = "Actualizar" Then
                End If
            End If

            If Trim(GrabarActualizar) = "Grabar" Then
                eValorizaciones.fCodigo(6, gEmpresa, "", gPeriodo)
                txtCodigoVal.Text = Trim(eValorizaciones.sCodFuturo)
                iResultado = eValorizaciones.fGrabar(7, gEmpresa, Trim(cboPartidas.SelectedValue), Trim(txtCodigoVal.Text), 0, "", Trim(txtDescripcion.Text), dtpFechaIni.Value, dtpFechaIni.Value, dtpFechaIni.Value, "", "", 1, "", gUsuario, 0, 0, Trim(txtTotPagar.Text), 0, 0, Trim(txtTotPagar.Text), 0, 0, 0, 0, Trim(txtTotPagar.Text), Trim(cboAnexo.SelectedValue), iTipo2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, gPeriodo, Trim(CCosto.SelectedValue))
                If iResultado > 0 Then
                    MessageBox.Show("Se Grab� Correctamente", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'cboPartidas_SelectedIndexChanged(sender, e)
                    If cboPartidas.SelectedIndex > -1 Then
                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                        eValorizaciones = New clsValorizaciones
                        Dim dtValorizacion As DataTable
                        dtValorizacion = New DataTable
                        dtValorizacion = eValorizaciones.fBuscarValorizacion(34, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), 0, dtpFechaIni.Value, Today(), gPeriodo, Trim(CCosto.SelectedValue))
                        If dtValorizacion.Rows.Count > 0 Then
                            dgvDetalle.DataSource = dtValorizacion
                        End If
                        btnNuevo_Click(sender, e)
                    End If
                End If
            ElseIf Trim(GrabarActualizar) = "Actualizar" Then
                iResultado = eValorizaciones.fGrabar(10, gEmpresa, Trim(cboPartidas.SelectedValue), Trim(txtCodigoVal.Text), 0, "", Trim(txtDescripcion.Text), dtpFechaIni.Value, dtpFechaIni.Value, dtpFechaIni.Value, "", "", 1, "", gUsuario, 0, 0, Trim(txtTotPagar.Text), 0, 0, Trim(txtTotPagar.Text), 0, 0, 0, 0, Trim(txtTotPagar.Text), Trim(cboAnexo.SelectedValue), iTipo2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, gPeriodo, Trim(CCosto.SelectedValue))
                If iResultado > 0 Then
                    MessageBox.Show("Se Actualiz� Correctamente", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'cboPartidas_SelectedIndexChanged(sender, e)
                    If cboPartidas.SelectedIndex > -1 Then
                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                        eValorizaciones = New clsValorizaciones
                        Dim dtValorizacion As DataTable
                        dtValorizacion = New DataTable
                        dtValorizacion = eValorizaciones.fBuscarValorizacion(34, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), 0, dtpFechaIni.Value, Today(), gPeriodo, Trim(CCosto.SelectedValue))
                        If dtValorizacion.Rows.Count > 0 Then
                            dgvDetalle.DataSource = dtValorizacion
                        End If
                        btnNuevo_Click(sender, e)
                    End If
                End If
            End If
        Else
            If cboObra.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Obra", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboObra.Focus()
                Exit Sub
            End If
            If cboContratista.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Contratista", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboContratista.Focus()
                Exit Sub
            End If
            If cboPartidas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Partida", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboPartidas.Focus()
                Exit Sub
            End If

            If cboAnexo.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Tipo de Anexo", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboAnexo.Focus()
                Exit Sub
            End If
            If Len(Trim(txtDescripcion.Text)) = 0 Then
                MessageBox.Show("Ingrese una Descripci�n", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtDescripcion.Focus()
                Exit Sub
            End If
            If Len(Trim(txtTotPagar.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtTotPagar.Focus()
                Exit Sub
            End If
        End If


    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        txtCodigoVal.Clear()
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        rdbT1.Checked = False
        rdbT2.Checked = False
        cboAnexo.DataSource = Nothing
        cboAnexo.SelectedIndex = -1
        txtDescripcion.Clear()
        txtTotPagar.Clear()
        GrabarActualizar = "Grabar"
        dtpFechaIni.Focus()
        cboAnexo.Enabled = True
    End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub

    Private Sub dgvDetalle_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalle.SelectionChanged
        If dgvDetalle.Rows.Count > 0 Then
            GrabarActualizar = "Actualizar"
            Try
                If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Tipo1").Value) = True Then
                    'cboOpcion.SelectedIndex = -1

                    rdbT1.Checked = False
                    rdbT2.Checked = False
                    'cboTipoMov.Enabled = True
                    'rdbCargo_CheckedChanged(sender, e)
                Else
                    Dim opcionBD As String = ""
                    opcionBD = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Tipo1").Value)
                    If opcionBD = "1" Then
                        rdbT1.Checked = True
                        'rdbCargo_cha
                        ' rdbCargo.Checked = True
                    ElseIf opcionBD = "2" Then
                        ' rdbAbono.Checked = True
                        rdbT2.Checked = True
                        'rdbCargo_CheckedChanged(sender, e)
                    End If

                    'cboOpcion.SelectedValue = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column11").Value)
                End If

                dtpFechaIni.Value = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("FechaEnvio").Value)

                If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("DescripcionVal").Value) = True Then
                    txtDescripcion.Text = ""
                Else
                    txtDescripcion.Text = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("DescripcionVal").Value)
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("TotalVal").Value) = True Then
                    txtTotPagar.Text = "0.00"
                Else
                    txtTotPagar.Text = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("TotalVal").Value)
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("IdTipoPago").Value) = True Then
                    cboAnexo.SelectedIndex = -1
                Else
                    cboAnexo.SelectedValue = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("IdTipoPago").Value)
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("IdValorizacion").Value) = True Then
                    txtCodigoVal.Text = ""
                Else
                    txtCodigoVal.Text = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("IdValorizacion").Value)
                End If


                'If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column11").Value) = True Then
                '    rdbAbono.Checked = False
                '    rdbCargo.Checked = False
                'Else
                '    Dim opcionBD As String = ""
                '    opcionBD = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column11").Value)
                '    If opcionBD = "D" Then
                '        rdbAbono.Checked = True
                '    ElseIf opcionBD = "H" Then
                '        rdbCargo.Checked = True
                '        rdbCargo_CheckedChanged(sender, e)
                '    End If
                'End If

            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub cboPartidas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectionChangeCommitted
        If cboPartidas.SelectedIndex > -1 Then

            txtCodigoVal.Text = ""
            dtpFechaIni.Value = Today
            txtDescripcion.Text = ""
            rdbT1.Checked = False
            rdbT2.Checked = False
            cboAnexo.SelectedIndex = -1
            txtTotPagar.Text = ""
            For x As Integer = 0 To dgvDetalle.RowCount - 1
                dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
            Next

            eValorizaciones = New clsValorizaciones
            Dim dtValorizacion As DataTable
            dtValorizacion = New DataTable
            dtValorizacion = eValorizaciones.fBuscarValorizacion(34, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), 0, dtpFechaIni.Value, Today(), gPeriodo, Trim(CCosto.SelectedValue))
            If dtValorizacion.Rows.Count > 0 Then
                dgvDetalle.DataSource = dtValorizacion
            End If

            eValorizaciones = New clsValorizaciones
            Dim dtValorizacionx As DataTable
            dtValorizacionx = New DataTable
            Dim SumaRetenido As Double = 0
            'Dim ContraActual As Double = 0
            dtValorizacionx = eValorizaciones.fBuscarValorizacion(41, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), 0, dtpFechaIni.Value, Today(), gPeriodo, Trim(CCosto.SelectedValue))
            If dtValorizacionx.Rows.Count > 0 Then
                If Microsoft.VisualBasic.IsDBNull(dtValorizacionx.Rows(0).Item("SumaHistorialContra")) = False Then
                    SumaRetenido = dtValorizacionx.Rows(0).Item("SumaHistorialContra")
                End If
            End If
            lblRetenido.Text = Format(SumaRetenido, "#,##0.00")
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim iResultado As Integer = 0
        If dgvDetalle.Rows.Count > 0 Then
            If MessageBox.Show("� Desea Eliminar el Item ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                iResultado = eValorizaciones.fGrabar(35, gEmpresa, Trim(cboPartidas.SelectedValue), Trim(txtCodigoVal.Text), 0, "", Trim(txtDescripcion.Text), dtpFechaIni.Value, dtpFechaIni.Value, dtpFechaIni.Value, "", "", 1, "", gUsuario, 0, 0, Trim(txtTotPagar.Text), 0, 0, Trim(txtTotPagar.Text), 0, 0, 0, 0, Trim(txtTotPagar.Text), Trim(cboAnexo.SelectedValue), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, gPeriodo, Trim(CCosto.SelectedValue))
                If iResultado > 0 Then
                    MessageBox.Show("Se Elimino Correctamente", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'cboPartidas_SelectedIndexChanged(sender, e)
                    If cboPartidas.SelectedIndex > -1 Then
                        For x As Integer = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                        eValorizaciones = New clsValorizaciones
                        Dim dtValorizacion As DataTable
                        dtValorizacion = New DataTable
                        dtValorizacion = eValorizaciones.fBuscarValorizacion(34, gEmpresa, Convert.ToString(cboPartidas.SelectedValue), 0, dtpFechaIni.Value, Today(), gPeriodo, Trim(CCosto.SelectedValue))
                        If dtValorizacion.Rows.Count > 0 Then
                            dgvDetalle.DataSource = dtValorizacion
                        End If
                        btnNuevo_Click(sender, e)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cboAnexo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAnexo.SelectedIndexChanged

    End Sub

    Private Sub cboAnexo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAnexo.SelectionChangeCommitted

      

    End Sub

    Private Sub cboContratista_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboContratista.SizeChanged

    End Sub

    Private Sub CCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CCosto.SelectedIndexChanged

    End Sub

    Private Sub CCosto_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles CCosto.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 And CCosto.SelectedIndex > -1 Then
            eValorizaciones = New clsValorizaciones
            cboPartidas.DataSource = eValorizaciones.fListarObras(15, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If eValorizaciones.iNroRegistros > 0 Then
                cboPartidas.ValueMember = "IdPartida"
                cboPartidas.DisplayMember = "ParDescripcion"
                cboPartidas.SelectedIndex = -1

                

            End If
            'End If
        End If

        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
        txtCodigoVal.Clear()
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        rdbT1.Checked = False
        rdbT2.Checked = False
        cboAnexo.DataSource = Nothing
        cboAnexo.SelectedIndex = -1
        txtDescripcion.Clear()
        txtTotPagar.Clear()
        GrabarActualizar = "Grabar"
    End Sub
End Class