Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions


Public Class frmUsuarioAreasObras

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmUsuarioAreasObras = Nothing
    Public Shared Function Instance() As frmUsuarioAreasObras
        If frmInstance Is Nothing Then
            frmInstance = New frmUsuarioAreasObras
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmUsuarioAreasObras_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Public strUsuario As String = ""
    Private eUsuario As Usuarios

    Private Sub frmUsuarioAreasObras_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListarAreas(gEmpresa)
        ListarObras(gEmpresa)
        Me.Text = " Usuario : " & strUsuario
    End Sub

    Sub ListarAreas(ByVal Empresa As String)
        Try
            Dim dtTable As DataTable
            dtTable = New DataTable
            eUsuario = New Usuarios
            dtTable = eUsuario.Listar(10, gEmpresa, glbSisCodigo, strUsuario)
            Me.DgvListaAreas.AutoGenerateColumns = False
            Me.DgvListaAreas.DataSource = dtTable
            Me.DgvListaAreas.ReadOnly = False
            Me.DgvListaAreas.Columns(0).ReadOnly = True
            Me.DgvListaAreas.Columns(1).ReadOnly = True
            Me.DgvListaAreas.Columns(2).ReadOnly = False
            ActualizaValoresMenuAreas()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Sub ActualizaValoresMenuAreas()
        Dim dtPerUsu As DataTable
        dtPerUsu = New DataTable
        eUsuario = New Usuarios
        dtPerUsu = eUsuario.BuscaAreasxUsuario(13, gEmpresa, glbSisCodigo, Trim(strUsuario))
        If dtPerUsu.Rows.Count > 0 Then
            For z As Integer = 0 To dtPerUsu.Rows.Count - 1
                For x As Integer = 0 To DgvListaAreas.Rows.Count - 1
                    If Trim(dtPerUsu.Rows(z).Item("AreaCodigo")) = Trim(DgvListaAreas.Rows(x).Cells("Column2").Value) Then
                        DgvListaAreas.Rows(x).Cells("Column3").Value = True
                        Exit For
                    Else
                        DgvListaAreas.Rows(x).Cells("Column3").Value = False
                    End If
                Next
            Next
        End If
    End Sub

    Sub ListarObras(ByVal Empresa As String)
        Try
            Dim dtTable As DataTable
            dtTable = New DataTable
            eUsuario = New Usuarios
            dtTable = eUsuario.Listar(11, gEmpresa, glbSisCodigo, strUsuario)
            Me.DgvListaObras.AutoGenerateColumns = False
            Me.DgvListaObras.DataSource = dtTable
            Me.DgvListaObras.ReadOnly = False
            Me.DgvListaObras.Columns(0).ReadOnly = True
            Me.DgvListaObras.Columns(1).ReadOnly = True
            Me.DgvListaObras.Columns(2).ReadOnly = False
            ActualizaValoresMenuObras()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Sub ActualizaValoresMenuObras()
        Dim dtPerUsu As DataTable
        dtPerUsu = New DataTable
        eUsuario = New Usuarios
        dtPerUsu = eUsuario.BuscaAreasxUsuario(12, gEmpresa, glbSisCodigo, Trim(strUsuario))
        If dtPerUsu.Rows.Count > 0 Then
            For z As Integer = 0 To dtPerUsu.Rows.Count - 1
                For x As Integer = 0 To DgvListaObras.Rows.Count - 1
                    If Trim(dtPerUsu.Rows(z).Item("ObraCodigo")) = Trim(DgvListaObras.Rows(x).Cells("ObraCodigo").Value) Then
                        DgvListaObras.Rows(x).Cells("Activo").Value = True
                        Exit For
                    Else
                        DgvListaObras.Rows(x).Cells("Activo").Value = False
                    End If
                Next
            Next
        End If
    End Sub

    Private Sub DgvListaAreas_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvListaAreas.CellContentClick
        If DgvListaAreas.Rows.Count > 0 Then
            Dim columna As Integer = DgvListaAreas.CurrentCell.ColumnIndex
            If DgvListaAreas.Rows.Count > 0 Then
                Try
                    DgvListaAreas.CurrentCell = DgvListaAreas(0, DgvListaAreas.CurrentRow.Index)
                    DgvListaAreas.CurrentCell = DgvListaAreas(columna, DgvListaAreas.CurrentRow.Index)
                Catch ex As Exception
                End Try
            End If
            If columna = 2 Then
                Try
                    Dim dtTable As DataTable
                    dtTable = New DataTable
                    eUsuario = New Usuarios
                    Dim AreaCod As String = ""
                    AreaCod = Trim(DgvListaAreas.Rows(DgvListaAreas.CurrentRow.Index).Cells("Column2").Value)
                    dtTable = eUsuario.ListarExiste(14, gEmpresa, glbSisCodigo, strUsuario, AreaCod)
                    If dtTable.Rows.Count > 0 Then
                        If DgvListaAreas.Rows(DgvListaAreas.CurrentRow.Index).Cells(columna).Value = True Then
                            '1
                            eUsuario.fGrabarUsuarioOpciones(16, gEmpresa, glbSisCodigo, strUsuario, AreaCod, 1, gUsuario)
                        Else
                            '0
                            eUsuario.fGrabarUsuarioOpciones(16, gEmpresa, glbSisCodigo, strUsuario, AreaCod, 0, gUsuario)
                        End If
                    Else
                        'crea
                        eUsuario.fGrabarUsuarioOpciones(15, gEmpresa, glbSisCodigo, strUsuario, AreaCod, 1, gUsuario)
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
                End Try
            End If
        End If
    End Sub

    Private Sub DgvListaObras_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvListaObras.CellContentClick
        If DgvListaObras.Rows.Count > 0 Then
            Dim columna As Integer = DgvListaObras.CurrentCell.ColumnIndex
            If DgvListaObras.Rows.Count > 0 Then
                Try
                    DgvListaObras.CurrentCell = DgvListaObras(0, DgvListaObras.CurrentRow.Index)
                    DgvListaObras.CurrentCell = DgvListaObras(columna, DgvListaObras.CurrentRow.Index)
                Catch ex As Exception
                End Try
            End If
            If columna = 2 Then
                Try
                    Dim dtTable As DataTable
                    dtTable = New DataTable
                    eUsuario = New Usuarios
                    Dim ObraCod As String = ""
                    ObraCod = Trim(DgvListaObras.Rows(DgvListaObras.CurrentRow.Index).Cells("ObraCodigo").Value)
                    dtTable = eUsuario.ListarExiste(17, gEmpresa, glbSisCodigo, strUsuario, ObraCod)
                    If dtTable.Rows.Count > 0 Then
                        If DgvListaObras.Rows(DgvListaObras.CurrentRow.Index).Cells(columna).Value = True Then
                            '1
                            eUsuario.fGrabarUsuarioOpciones(19, gEmpresa, glbSisCodigo, strUsuario, ObraCod, 1, gUsuario)
                        Else
                            '0
                            eUsuario.fGrabarUsuarioOpciones(19, gEmpresa, glbSisCodigo, strUsuario, ObraCod, 0, gUsuario)
                        End If
                    Else
                        'crea
                        eUsuario.fGrabarUsuarioOpciones(18, gEmpresa, glbSisCodigo, strUsuario, ObraCod, 1, gUsuario)
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
                End Try
            End If
        End If
    End Sub
End Class