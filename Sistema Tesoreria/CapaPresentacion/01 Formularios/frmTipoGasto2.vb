Option Explicit On
Imports System.Xml
Imports MSXML2

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad

Public Class frmTipoGasto2

    Private eTipoGasto As clsTipoGasto
    Dim odtTipoGasto As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared decImporte As Decimal
    Dim strAccion As String

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eTipoGasto = New clsTipoGasto

        tabMantenimiento.SelectedTab = tabLista
        botonesMantenimiento(mnuMantenimiento, True)

        CargarTipoGasto()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarTipoGasto()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

#End Region

#Region " M�todos privados "

    Private Sub CargarTipoGasto()
        Try
            'Dim odtData As New DataTable
            odtTipoGasto = eTipoGasto.fCargarTipoGasto()
            gvTipoGasto.DataSource = odtTipoGasto
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        tabMantenimiento.SelectedTab = tabDetalle
        strAccion = Accion.Grabar
        LimpiarControles()
        txtNombre.Focus()
    End Sub

    Private Sub gvIngresoBancario_CurrentCellChanged(sender As Object, e As EventArgs) Handles gvTipoGasto.CurrentCellChanged
        Try
            txtNombre.Text = Convert.ToString(gvTipoGasto.CurrentRow.Cells("NOMBRE").Value)
            txtAbreviatura.Text = Convert.ToString(gvTipoGasto.CurrentRow.Cells("ABREAVIATURA").Value)
            chkEstado.Checked = CBool(Convert.ToString(gvTipoGasto.CurrentRow.Cells("FLGESTADO").Value))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub gvIngresoBancario_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles gvTipoGasto.CellDoubleClick
        If gvTipoGasto.RowCount > 0 Then
            tabMantenimiento.SelectedTab = tabDetalle
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        tabMantenimiento.SelectedTab = tabLista
        CargarTipoGasto()
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        'Se valida los campos
        If txtNombre.Text.Trim.Length = 0 Then MessageBox.Show("El nombre es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtNombre.Focus() : Exit Sub

        Select Case strAccion
            Case Accion.Grabar
                Guardar()
            Case Accion.Modifica
                Modificar()
        End Select

        CargarTipoGasto()
        tabMantenimiento.SelectedTab = tabLista
    End Sub

    Private Sub tabMantenimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabMantenimiento.SelectedIndexChanged
        If tabMantenimiento.SelectedIndex = 0 Then
            strAccion = Accion.Grabar
            botonesMantenimiento(mnuMantenimiento, True)
        Else
            strAccion = Accion.Modifica
            botonesMantenimiento(mnuMantenimiento, False)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If gvTipoGasto.RowCount > 0 Then
            If Not Eliminar() Then Exit Sub
            CargarTipoGasto()
        Else
            MessageBox.Show("Seleccione registro a eliminar. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

#Region " Metodos Privados "

    Private Sub LimpiarControles()
        txtAbreviatura.Text = "" : txtNombre.Text = ""
    End Sub

#End Region

#Region " M�todos de BD "

    Private Sub Guardar()
        Try
            Dim iResultado As Int32
            iResultado = eTipoGasto.fGrabar(0, txtNombre.Text.Trim, txtAbreviatura.Text.Trim, chkEstado.CheckState, 1)
        Catch ex As Exception
            MessageBox.Show("No se pudo grabar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            Dim iResultado As Int32

            iResultado = eTipoGasto.fGrabar(Convert.ToDecimal(gvTipoGasto.CurrentRow.Cells("IDTIPOGASTO").Value), txtNombre.Text.Trim, txtAbreviatura.Text.Trim, chkEstado.CheckState, 2)
        Catch ex As Exception
            MessageBox.Show("No se pudo modificar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function Eliminar() As Boolean
        Try
            If MessageBox.Show("�Est� seguro que desea eliminar este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then

                Dim iResultado As Int32
                iResultado = eTipoGasto.fEliminar(Convert.ToDecimal(gvTipoGasto.CurrentRow.Cells("IDTIPOGASTO").Value), 3)

                Return True
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo eliminar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

#End Region

End Class