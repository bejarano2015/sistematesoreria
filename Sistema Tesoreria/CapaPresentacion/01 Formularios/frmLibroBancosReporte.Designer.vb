<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroBancosReporte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.cboAno = New ctrLibreria.Controles.BeComboBox
        Me.cboMes = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnNuevo = New ctrLibreria.Controles.BeButton
        Me.btnGrabar = New ctrLibreria.Controles.BeButton
        Me.btnCancelar = New ctrLibreria.Controles.BeButton
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.dgvCuentasyLibros = New System.Windows.Forms.DataGridView
        Me.frmExcel = New System.Windows.Forms.Button
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvCuentasyLibros, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboAno
        '
        Me.cboAno.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAno.BackColor = System.Drawing.Color.Ivory
        Me.cboAno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAno.ForeColor = System.Drawing.Color.Black
        Me.cboAno.FormattingEnabled = True
        Me.cboAno.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040"})
        Me.cboAno.KeyEnter = True
        Me.cboAno.Location = New System.Drawing.Point(48, 19)
        Me.cboAno.Name = "cboAno"
        Me.cboAno.Size = New System.Drawing.Size(128, 21)
        Me.cboAno.TabIndex = 11
        '
        'cboMes
        '
        Me.cboMes.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMes.BackColor = System.Drawing.Color.Ivory
        Me.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMes.ForeColor = System.Drawing.Color.Black
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cboMes.KeyEnter = True
        Me.cboMes.Location = New System.Drawing.Point(234, 19)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(128, 21)
        Me.cboMes.TabIndex = 12
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(13, 27)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel15.TabIndex = 13
        Me.BeLabel15.Text = "Año"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(199, 27)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel3.TabIndex = 14
        Me.BeLabel3.Text = "Mes"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnNuevo)
        Me.GroupBox3.Controls.Add(Me.btnGrabar)
        Me.GroupBox3.Controls.Add(Me.btnCancelar)
        Me.GroupBox3.Location = New System.Drawing.Point(864, 343)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(134, 50)
        Me.GroupBox3.TabIndex = 26
        Me.GroupBox3.TabStop = False
        '
        'btnNuevo
        '
        Me.btnNuevo.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnNuevo.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__File_new_
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(9, 14)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(35, 30)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(50, 14)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(35, 30)
        Me.btnGrabar.TabIndex = 2
        Me.btnGrabar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(91, 14)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(35, 30)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'dgvCuentasyLibros
        '
        Me.dgvCuentasyLibros.AllowUserToAddRows = False
        Me.dgvCuentasyLibros.BackgroundColor = System.Drawing.Color.White
        Me.dgvCuentasyLibros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCuentasyLibros.EnableHeadersVisualStyles = False
        Me.dgvCuentasyLibros.Location = New System.Drawing.Point(6, 52)
        Me.dgvCuentasyLibros.Name = "dgvCuentasyLibros"
        Me.dgvCuentasyLibros.RowHeadersVisible = False
        Me.dgvCuentasyLibros.Size = New System.Drawing.Size(994, 285)
        Me.dgvCuentasyLibros.TabIndex = 27
        '
        'frmExcel
        '
        Me.frmExcel.Location = New System.Drawing.Point(383, 17)
        Me.frmExcel.Name = "frmExcel"
        Me.frmExcel.Size = New System.Drawing.Size(98, 23)
        Me.frmExcel.TabIndex = 28
        Me.frmExcel.Text = "Imprimir Excel"
        Me.frmExcel.UseVisualStyleBackColor = True
        '
        'frmLibroBancosReporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1005, 401)
        Me.Controls.Add(Me.frmExcel)
        Me.Controls.Add(Me.dgvCuentasyLibros)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.BeLabel15)
        Me.Controls.Add(Me.BeLabel3)
        Me.Controls.Add(Me.cboAno)
        Me.Controls.Add(Me.cboMes)
        Me.Name = "frmLibroBancosReporte"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte de Libro Bancos"
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvCuentasyLibros, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboAno As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboMes As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnNuevo As ctrLibreria.Controles.BeButton
    Friend WithEvents btnGrabar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnCancelar As ctrLibreria.Controles.BeButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents dgvCuentasyLibros As System.Windows.Forms.DataGridView
    Friend WithEvents frmExcel As System.Windows.Forms.Button
End Class
