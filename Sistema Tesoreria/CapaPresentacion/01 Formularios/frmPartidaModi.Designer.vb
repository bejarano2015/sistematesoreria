<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPartidaModi
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel
        Me.txtSubTotal = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel
        Me.txtIGV = New ctrLibreria.Controles.BeTextBox
        Me.dgvDetalle = New System.Windows.Forms.DataGridView
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cant = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Und = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.M1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_Metrado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PU = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_PrecioSubContra = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.M2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Parcial = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.idDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_TitCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_ParCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Con_ConsCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.txtRetencion = New ctrLibreria.Controles.BeTextBox
        Me.txtPresupuesto = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(328, 207)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(50, 13)
        Me.BeLabel17.TabIndex = 43
        Me.BeLabel17.Text = "SubTotal"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotal.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotal.Enabled = False
        Me.txtSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotal.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotal.KeyEnter = True
        Me.txtSubTotal.Location = New System.Drawing.Point(392, 200)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotal.ShortcutsEnabled = False
        Me.txtSubTotal.Size = New System.Drawing.Size(80, 20)
        Me.txtSubTotal.TabIndex = 44
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(479, 207)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(25, 13)
        Me.BeLabel16.TabIndex = 45
        Me.BeLabel16.Text = "IGV"
        '
        'txtIGV
        '
        Me.txtIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIGV.BackColor = System.Drawing.Color.Lavender
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIGV.Enabled = False
        Me.txtIGV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.ForeColor = System.Drawing.Color.Black
        Me.txtIGV.KeyEnter = True
        Me.txtIGV.Location = New System.Drawing.Point(561, 200)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIGV.ShortcutsEnabled = False
        Me.txtIGV.Size = New System.Drawing.Size(80, 20)
        Me.txtIGV.TabIndex = 46
        Me.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.Cant, Me.Und, Me.M1, Me.Con_Metrado, Me.PU, Me.Con_PrecioSubContra, Me.M2, Me.Parcial, Me.idDetalle, Me.Estado, Me.Con_TitCodigo, Me.Con_ParCodigo, Me.Con_ConsCodigo})
        Me.dgvDetalle.Location = New System.Drawing.Point(15, 56)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(817, 133)
        Me.dgvDetalle.TabIndex = 42
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle10
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Width = 310
        '
        'Cant
        '
        Me.Cant.DataPropertyName = "Cantidad"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.Cant.DefaultCellStyle = DataGridViewCellStyle11
        Me.Cant.HeaderText = "Cant"
        Me.Cant.Name = "Cant"
        Me.Cant.Visible = False
        Me.Cant.Width = 55
        '
        'Und
        '
        Me.Und.DataPropertyName = "UnidMed"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Und.DefaultCellStyle = DataGridViewCellStyle12
        Me.Und.HeaderText = "U.M."
        Me.Und.Name = "Und"
        Me.Und.Width = 55
        '
        'M1
        '
        Me.M1.DataPropertyName = "MetradoPor"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.M1.DefaultCellStyle = DataGridViewCellStyle13
        Me.M1.HeaderText = "Metr. %"
        Me.M1.Name = "M1"
        Me.M1.Width = 85
        '
        'Con_Metrado
        '
        Me.Con_Metrado.DataPropertyName = "Con_Metrado"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.Con_Metrado.DefaultCellStyle = DataGridViewCellStyle14
        Me.Con_Metrado.HeaderText = "Mt."
        Me.Con_Metrado.Name = "Con_Metrado"
        Me.Con_Metrado.Width = 50
        '
        'PU
        '
        Me.PU.DataPropertyName = "PU"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.Format = "N2"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.PU.DefaultCellStyle = DataGridViewCellStyle15
        Me.PU.HeaderText = "PU"
        Me.PU.Name = "PU"
        Me.PU.Visible = False
        Me.PU.Width = 60
        '
        'Con_PrecioSubContra
        '
        Me.Con_PrecioSubContra.DataPropertyName = "Con_PrecioSubContra"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.Format = "N2"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.Con_PrecioSubContra.DefaultCellStyle = DataGridViewCellStyle16
        Me.Con_PrecioSubContra.HeaderText = "P. Sub."
        Me.Con_PrecioSubContra.Name = "Con_PrecioSubContra"
        Me.Con_PrecioSubContra.Width = 90
        '
        'M2
        '
        Me.M2.DataPropertyName = "MetradoImpor"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.Format = "N2"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.M2.DefaultCellStyle = DataGridViewCellStyle17
        Me.M2.HeaderText = "Mt. Contratar"
        Me.M2.Name = "M2"
        Me.M2.Width = 120
        '
        'Parcial
        '
        Me.Parcial.DataPropertyName = "Parcial"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.Format = "N2"
        DataGridViewCellStyle18.NullValue = Nothing
        Me.Parcial.DefaultCellStyle = DataGridViewCellStyle18
        Me.Parcial.HeaderText = "Parcial"
        Me.Parcial.Name = "Parcial"
        Me.Parcial.ReadOnly = True
        Me.Parcial.Width = 85
        '
        'idDetalle
        '
        Me.idDetalle.DataPropertyName = "IdPartidaDet"
        Me.idDetalle.HeaderText = "IdDetalle"
        Me.idDetalle.Name = "idDetalle"
        Me.idDetalle.Visible = False
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.Visible = False
        '
        'Con_TitCodigo
        '
        Me.Con_TitCodigo.DataPropertyName = "Con_TitCodigo"
        Me.Con_TitCodigo.HeaderText = "Con_TitCodigo"
        Me.Con_TitCodigo.Name = "Con_TitCodigo"
        Me.Con_TitCodigo.Visible = False
        '
        'Con_ParCodigo
        '
        Me.Con_ParCodigo.DataPropertyName = "Con_ParCodigo"
        Me.Con_ParCodigo.HeaderText = "Con_ParCodigo"
        Me.Con_ParCodigo.Name = "Con_ParCodigo"
        Me.Con_ParCodigo.Visible = False
        '
        'Con_ConsCodigo
        '
        Me.Con_ConsCodigo.DataPropertyName = "Con_ConsCodigo"
        Me.Con_ConsCodigo.HeaderText = "Con_ConsCodigo"
        Me.Con_ConsCodigo.Name = "Con_ConsCodigo"
        Me.Con_ConsCodigo.Visible = False
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(647, 233)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel10.TabIndex = 49
        Me.BeLabel10.Text = "% Retencion"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(647, 207)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(66, 13)
        Me.BeLabel9.TabIndex = 47
        Me.BeLabel9.Text = "Presupuesto"
        '
        'txtRetencion
        '
        Me.txtRetencion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRetencion.BackColor = System.Drawing.Color.Ivory
        Me.txtRetencion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRetencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRetencion.ForeColor = System.Drawing.Color.Black
        Me.txtRetencion.KeyEnter = True
        Me.txtRetencion.Location = New System.Drawing.Point(730, 226)
        Me.txtRetencion.Name = "txtRetencion"
        Me.txtRetencion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRetencion.ShortcutsEnabled = False
        Me.txtRetencion.Size = New System.Drawing.Size(80, 20)
        Me.txtRetencion.TabIndex = 50
        Me.txtRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRetencion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtPresupuesto
        '
        Me.txtPresupuesto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPresupuesto.BackColor = System.Drawing.Color.Ivory
        Me.txtPresupuesto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPresupuesto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPresupuesto.Enabled = False
        Me.txtPresupuesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresupuesto.ForeColor = System.Drawing.Color.Black
        Me.txtPresupuesto.KeyEnter = True
        Me.txtPresupuesto.Location = New System.Drawing.Point(730, 200)
        Me.txtPresupuesto.Name = "txtPresupuesto"
        Me.txtPresupuesto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPresupuesto.ShortcutsEnabled = False
        Me.txtPresupuesto.Size = New System.Drawing.Size(80, 20)
        Me.txtPresupuesto.TabIndex = 48
        Me.txtPresupuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPresupuesto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(12, 31)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel14.TabIndex = 51
        Me.BeLabel14.Text = "DETALLE"
        '
        'frmPartidaModi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(847, 302)
        Me.Controls.Add(Me.BeLabel14)
        Me.Controls.Add(Me.BeLabel17)
        Me.Controls.Add(Me.txtSubTotal)
        Me.Controls.Add(Me.BeLabel16)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.dgvDetalle)
        Me.Controls.Add(Me.BeLabel10)
        Me.Controls.Add(Me.BeLabel9)
        Me.Controls.Add(Me.txtRetencion)
        Me.Controls.Add(Me.txtPresupuesto)
        Me.Name = "frmPartidaModi"
        Me.Text = "frmPartidaModi"
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSubTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Und As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_Metrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_PrecioSubContra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Parcial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idDetalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_TitCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_ParCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_ConsCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtRetencion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPresupuesto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
End Class
