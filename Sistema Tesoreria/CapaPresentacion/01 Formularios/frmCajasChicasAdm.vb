Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic

Public Class frmCajasChicasAdm

    Private eSesionCajas As clsSesionCajas

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCajasChicasAdm = Nothing
    Public Shared Function Instance() As frmCajasChicasAdm
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmCajasChicasAdm
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmCajasChicasAdm_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub
    Private Sub frmCajasChicasAdm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmCajasChicasAdm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            eSesionCajas = New clsSesionCajas
            'LimpiarControles()
            cboArea.DataSource = eSesionCajas.fListarArea(gEmpresa)
            cboArea.ValueMember = "AreaCodigo"
            cboArea.DisplayMember = "AreaNombre"
            cboArea.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        Dim IdArea As String
        'txtNumSesion.Clear()
        Try
            IdArea = cboArea.SelectedValue.ToString
            If IdArea <> "System.Data.DataRowView" Then
                eSesionCajas = New clsSesionCajas
                cboCaja.DataSource = eSesionCajas.fListarCajasPorArea(cboArea.SelectedValue, gEmpresa, gPeriodo)
                cboCaja.ValueMember = "IdCaja"
                cboCaja.DisplayMember = "DescripcionCaja"
                cboCaja.SelectedIndex = -1

                If dgvDetalle.Rows.Count > 0 Then
                    Dim x As Integer = 0
                    For x = 0 To dgvDetalle.RowCount - 1
                        dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    Next
                End If

            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub cboCaja_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCaja.SelectionChangeCommitted
        Dim NombreEmpleado As String = ""
        Dim NombreMoneda As String = ""
        Dim SimboloMoneda As String = ""
        Dim UserCaja As String = ""

        If cboCaja.SelectedIndex <> -1 Then
            Dim dtTable2 As DataTable
            dtTable2 = New DataTable
            eSesionCajas = New clsSesionCajas
            dtTable2 = eSesionCajas.fTraerResponsable(Convert.ToString(cboCaja.SelectedValue), gEmpresa)
            If dtTable2.Rows.Count > 0 Then
                NombreEmpleado = UCase(dtTable2.Rows(0).Item("Nombres"))
                NombreMoneda = dtTable2.Rows(0).Item("MonDescripcion")
                SimboloMoneda = dtTable2.Rows(0).Item("MonSimbolo")
                UserCaja = Trim(dtTable2.Rows(0).Item("UsuCodigo"))
                If Trim(glbUsuCategoria) = "A" Then

                    
                    ActualizaValoresMenu()

                Else
                    MessageBox.Show("No Tiene Permisos para Ingresar a la Caja Seleccionada", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboCaja.SelectedIndex = -1
                End If
            ElseIf dtTable2.Rows.Count = 0 Then
                If dgvDetalle.Rows.Count > 0 Then
                    Dim x As Integer = 0
                    For x = 0 To dgvDetalle.RowCount - 1
                        dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    Next
                End If
            End If


        End If
    End Sub


    Sub ActualizaValoresMenu()

        Dim dtCajasCerradas As DataTable
        dtCajasCerradas = New DataTable
        eSesionCajas = New clsSesionCajas
        dtCajasCerradas = eSesionCajas.fTraerCajasCerradas(gEmpresa, gPeriodo, Convert.ToString(cboArea.SelectedValue), Convert.ToString(cboCaja.SelectedValue))
        dgvDetalle.DataSource = dtCajasCerradas

        'Dim n As Integer
        'Dim Cadena As String
        'Me.dgvDetalle.ClearSelection()
        Try
            'Using scope As TransactionScope = New TransactionScope
            '    n = 1
            '    For Each fila As DataGridViewRow In Me.dgvDetalle.Rows
            '        Cadena = Trim(Mid(strHabilita, n, 1))
            '        If Cadena <> "" Then
            '            If Cadena = "1" Then
            '                fila.Cells("Column3").Value = True
            '            Else
            '                fila.Cells("Column3").Value = False
            '            End If
            '        End If
            '        n = n + 1
            '    Next
            '    scope.Complete()
            'End Using

            If dtCajasCerradas.Rows.Count > 0 Then
                For y As Integer = 0 To dtCajasCerradas.Rows.Count - 1
                    'dgvDetalle.Rows.Add()
                    For z As Integer = 0 To dtCajasCerradas.Columns.Count - 1
                        If z = 2 Then
                            If dtCajasCerradas.Rows(y).Item(2).ToString = 1 Then
                                dgvDetalle.Rows(y).Cells("Column4").Value = True
                            ElseIf dtCajasCerradas.Rows(y).Item(2).ToString = 0 Then
                                dgvDetalle.Rows(y).Cells("Column4").Value = False
                            End If
                        End If
                    Next
                Next
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub




    'Private Sub dgvDetalle_CellContentClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick
    '    Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
    '    If dgvDetalle.Rows.Count > 0 Then
    '        dgvDetalle.CurrentCell = dgvDetalle(0, dgvDetalle.CurrentRow.Index)
    '        dgvDetalle.CurrentCell = dgvDetalle(columna, dgvDetalle.CurrentRow.Index)
    '    End If

    '    If columna = 3 Then
    '        Dim itfVoF As String = ""
    '        If dgvDetalle.Rows.Count > 0 Then
    '            If Convert.ToBoolean(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Column4").Value) = False Then
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").Value = False
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").ReadOnly = True
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value = False
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").ReadOnly = True
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Value() = "A Rendir"
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Style.ForeColor = Color.Blue
    '                MessageBox.Show("Se va ha habilitar la Caja #", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                dgvDetalle.CurrentCell = dgvDetalle(3, dgvDetalle.CurrentRow.Index)
    '                'dgvGastos.BeginEdit(True)
    '            ElseIf Convert.ToBoolean(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Column4").Value) = True Then
    '                MessageBox.Show("Se va ha Cerrar la Caja #", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                dgvDetalle.CurrentCell = dgvDetalle(3, dgvDetalle.CurrentRow.Index)
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").Value = False
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").ReadOnly = False
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Value() = ""
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value = False
    '                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").ReadOnly = False
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If dgvDetalle.Rows.Count > 0 Then
            dgvDetalle.CurrentCell = dgvDetalle(0, dgvDetalle.CurrentRow.Index)
            dgvDetalle.CurrentCell = dgvDetalle(columna, dgvDetalle.CurrentRow.Index)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If MsgBox("Esta seguro de aplicar los cambios?", MsgBoxStyle.YesNo, glbNameSistema) = MsgBoxResult.No Then
            Exit Sub
        End If
        Me.dgvDetalle.ClearSelection()
        For z As Integer = 0 To dgvDetalle.Rows.Count - 1
            If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(z).Cells("Column4").Value) = True Then
                'Habilita = Habilita & "0"
            Else
                eSesionCajas = New clsSesionCajas
                If Convert.ToBoolean(dgvDetalle.Rows(z).Cells("Column4").Value) = False Then
                    'Habilita = Habilita & "0"
                    eSesionCajas.fActivaryDesActivarSesion(34, dgvDetalle.Rows(z).Cells("Column1").Value, gEmpresa)
                Else
                    'Habilita = Habilita & "1"
                    eSesionCajas.fActivaryDesActivarSesion(33, dgvDetalle.Rows(z).Cells("Column1").Value, gEmpresa)
                End If
            End If
        Next
        cboCaja_SelectionChangeCommitted(sender, e)
        MessageBox.Show("Datos Actualizados", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub
End Class