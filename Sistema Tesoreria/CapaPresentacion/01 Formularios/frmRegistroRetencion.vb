Option Explicit On
Imports System.Xml
Imports MSXML2
Imports System.Math

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports Telerik.WinControls.Data
Imports Telerik.WinControls.UI.Export

Public Class frmRegistroRetencion

    Private eRetenciones As clsRetencion
    Dim odtRetencion As New DataTable
    Dim odtRetencionDet As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared idRegistroDocumentoCab As Decimal
    Dim strAccion As String
    Dim decImporteRetenido As Decimal
    Dim decImporteTotal As Decimal
    Private estado As Integer
    Private Loaded As Boolean = False

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptRetencion As New rptRetenciones

    Private cCapaDatos As clsCapaDatos

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Loaded = True
        VL_DOCUMENTO = "RETENCION"

        eRetenciones = New clsRetencion
        cCapaDatos = New clsCapaDatos

        cboAnio.Text = Convert.ToString(DateTime.Now.Year)
        cboMes.SelectedIndex = DateTime.Now.Month - 1

        tabMantenimiento.SelectedTab = tabLista
        botonesMantenimiento(mnuMantenimiento, True)

        InitCombos()

        CargarRetencionCab()
        CargarRetencionDet()
    End Sub

    Private Sub tabMantenimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabMantenimiento.SelectedIndexChanged
        If tabMantenimiento.SelectedIndex = 0 Then
            strAccion = Accion.Grabar
            'grpFacturas.Visible = False
            botonesMantenimiento(mnuMantenimiento, True)
            mnuMantenimiento.Items("btnReservar").Enabled = True
        Else
            strAccion = Accion.Modifica
            cboDocumentoOrigen.Enabled = False
            grpFacturas.Visible = False
            btnBuscarAnexo.Enabled = False
            dtFecha.Enabled = False
            txtSerie.Enabled = False
            txtNroDocumento.Enabled = False

            Try
                If Convert.ToString(gcRetencion.CurrentRow.Cells("colRevervado").Value) = "1" Then
                    btnBuscarAnexo.Enabled = True
                    dtFecha.Enabled = True
                End If
            Catch ex As Exception
            End Try

            botonesMantenimiento(mnuMantenimiento, False)
            mnuMantenimiento.Items("btnReservar").Enabled = False
        End If
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarRetencionCab()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        tabMantenimiento.SelectedTab = tabDetalle
        strAccion = Accion.Grabar
        LimpiarControles()
        cboDocumentoOrigen.SelectedIndex = 1
        cboDocumentoOrigen.Enabled = True
        btnBuscarAnexo.Enabled = True
        dtFecha.Enabled = True
        txtSerie.Text = "001"
        txtSerie.Enabled = True
        txtNroDocumento.Enabled = True
        NumeroCorrelativo()
        'txtRuc.ReadOnly = False
        odtRetencionDet.Clear()
        dtFecha.Value = Date.Today
        'txtNombre.Focus()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        tabMantenimiento.SelectedTab = tabLista
        CargarRetencionCab()
    End Sub

    Private Sub cboDocumentoOrigen_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDocumentoOrigen.SelectedValueChanged
        If Not Loaded Then Exit Sub

        odtRetencionDet.Clear()
        gcRetencionDetalle.DataSource = Nothing
        gcFacturas.DataSource = Nothing

        If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then 'Factura de Compra           
            gcRetencionDetalle.Columns("colImporteTotal").ReadOnly = False

            'If strAccion = Accion.Grabar Then
            '    txtSerie.Text = "001"
            '    NumeroCorrelativo()
            'End If
        ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then ' Orden de Compra
            txtSerie.Text = "000"
            txtNroDocumento.Text = "0000000"
            gcRetencionDetalle.Columns("colImporteTotal").ReadOnly = True
        End If
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        ''Se valida los campos
        gcRetencionDetalle.EndEdit()
        If txtRuc.Text.Trim.Length = 0 Then MessageBox.Show("El proveedor es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtRuc.Focus() : Exit Sub

        If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then 'Factura de Compra  
            If Not ComprobarNroCorrelativo() Then Exit Sub
        End If

        SumarImporte()

        If gcRetencionDetalle.RowCount > 0 Then
            Select Case strAccion
                Case Accion.Grabar
                    Guardar()
                Case Accion.Modifica
                    If Convert.ToDecimal(Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1) Then
                        If Not ComprobarDato() Then Exit Sub
                    End If
                    Modificar()
            End Select

            CargarRetencionCab()
            tabMantenimiento.SelectedTab = tabLista
        Else
            MessageBox.Show("No existe registro en detalle retenciones. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If gcRetencion.RowCount > 0 Then
            If Not Eliminar() Then Exit Sub
            CargarRetencionCab()
        Else
            MessageBox.Show("Seleccione registro a eliminar. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub dgvRetencion_CurrentRowChanged(sender As Object, e As Telerik.WinControls.UI.CurrentRowChangedEventArgs) Handles gcRetencion.CurrentRowChanged
        Try
            idRegistroDocumentoCab = Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value)

            dtFecha.Value = Convert.ToDateTime(gcRetencion.CurrentRow.Cells("colFecha").Value)
            txtCodigoAnexo.Text = Convert.ToString(gcRetencion.CurrentRow.Cells("colCodProveedor").Value)
            txtRuc.Text = Convert.ToString(gcRetencion.CurrentRow.Cells("colRuc").Value)
            lblNombreAnexo.Text = Convert.ToString(gcRetencion.CurrentRow.Cells("colNombreProveedor").Value)
            lblDireccion.Text = Convert.ToString(gcRetencion.CurrentRow.Cells("colDIRECCION").Value)
            txtSerie.Text = Convert.ToString(gcRetencion.CurrentRow.Cells("colSerie").Value)
            txtNroDocumento.Text = Convert.ToString(gcRetencion.CurrentRow.Cells("colNumero").Value)
            txtObservacion.Text = Convert.ToString(gcRetencion.CurrentRow.Cells("colDescripcion").Value)
            cboDocumentoOrigen.SelectedValue = Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value)

            CargarRetencionDet()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvRetencion_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcRetencion.CellDoubleClick
        If gcRetencion.RowCount > 0 Then
            tabMantenimiento.SelectedTab = tabDetalle
        End If
    End Sub

    Private Sub txtRuc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRuc.KeyPress
        If Len(txtRuc.Text) = 11 Then
            Select Case Asc(e.KeyChar)
                Case 13
                    Try
                        grpFacturas.Visible = True
                        If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
                            odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
                            gcFacturas.DataSource = odtRetencion
                        ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then
                            odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
                            gcFacturas.DataSource = odtRetencion
                        End If
                    Catch ex As Exception
                        Exit Sub
                    End Try
            End Select
        End If
    End Sub

    Private Sub btnBuscarAnexo_Click(sender As Object, e As EventArgs) Handles btnBuscarAnexo.Click
        Dim Proveedores As New frmBuscaProveedor()
        If Proveedores.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtCodigoAnexo.Text = frmBuscaProveedor.strCodigo
            txtRuc.Text = frmBuscaProveedor.strRuc
            lblNombreAnexo.Text = frmBuscaProveedor.strNombre
            lblDireccion.Text = frmBuscaProveedor.strDireccion

            grpFacturas.Visible = True

            If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
                odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
                gcFacturas.DataSource = odtRetencion
            ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then
                odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
                gcFacturas.DataSource = odtRetencion
            End If
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        gcFacturas.CloseEditor()
        If gcFacturas.RowCount > 0 Then
            'Detalle
            For i As Integer = 0 To gcFacturas.RowCount - 1
                If CBool(gcFacturas.Rows(i).Cells("colChkEstado").Value) = True Then

                    ' Traemos el valor porcentaje dependiendo de la fecha de emisi�n de la factura
                    Dim dtImpuestosxDocumentos As DataTable
                    dtImpuestosxDocumentos = New DataTable
                    eRetenciones = New clsRetencion
                    dtImpuestosxDocumentos = eRetenciones.fBuscarImpuestosxDocumentos(0, 2, gcFacturas.Rows(i).Cells("colFecha").Value)
                    If dtImpuestosxDocumentos.Rows.Count > 0 Then
                        iValorPorcentajeRetencion = Round(Convert.ToDecimal(dtImpuestosxDocumentos.Rows(0).Item("VALOR_PORCENTAJE")), 2, MidpointRounding.ToEven) / 100
                    Else
                        iValorPorcentajeRetencion = 0
                    End If

                    Dim importeTipoCambio As Decimal = 0
                    If Convert.ToString(gcFacturas.CurrentRow.Cells("colIdMoneda").Value) = "02" Then
                        Dim iResultado As Decimal = 0

                        iResultado = eRetenciones.fImporteTipoCambio(CDate(dtFecha.Value), 13)
                        importeTipoCambio = iResultado
                    End If

                    Dim row As DataRow
                    row = odtRetencionDet.NewRow
                    row("IdRetencionDet") = 0
                    row("IdRetencionCab") = 0
                    row("IdRegistroDocumento") = gcFacturas.Rows(i).Cells("colIdRegistro").Value
                    row("EmprCodigo") = gcFacturas.Rows(i).Cells("colEmprCodigo").Value
                    row("IdDocumento") = gcFacturas.Rows(i).Cells("colIdDocumento").Value
                    row("Serie") = gcFacturas.Rows(i).Cells("colSerie").Value
                    row("Numero") = gcFacturas.Rows(i).Cells("colNumero").Value
                    row("Fecha") = gcFacturas.Rows(i).Cells("colFecha").Value
                    row("IdMoneda") = gcFacturas.Rows(i).Cells("colIdMoneda").Value
                    row("ImporteSoles") = If(gcFacturas.Rows(i).Cells("colIdMoneda").Value = "02", Round(gcFacturas.Rows(i).Cells("colImporteTotal").Value * importeTipoCambio, 2, MidpointRounding.ToEven), gcFacturas.Rows(i).Cells("colImporteTotal").Value)
                    row("ImporteTotal") = Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven)

                    Dim porcentajeRetencion As Decimal = 0
                    porcentajeRetencion = If(importeTipoCambio = 0, Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * iValorPorcentajeRetencion, (Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * iValorPorcentajeRetencion) * importeTipoCambio)
                    row("PorcentajeRetencion") = Round(porcentajeRetencion, 2, MidpointRounding.ToEven)

                    row("ImporteRetenido") = Round(Convert.ToDecimal(row("ImporteSoles")), 2, MidpointRounding.ToEven) - Round(Convert.ToDecimal(row("PorcentajeRetencion")), 2, MidpointRounding.ToEven)

                    odtRetencionDet.Rows.Add(row)
                    'stigvDetFormulaCalculo.UpdateCurrentRow()
                    gcRetencionDetalle.DataSource = odtRetencionDet

                    'Me.DialogResult = Windows.Forms.DialogResult. 
                    ''Me.Close()  
                End If
            Next i
            grpFacturas.Visible = False
        End If

    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        grpFacturas.Visible = False
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If gcRetencionDetalle.RowCount > 0 Then
            'Dim intFila As Integer = gvTipoGastoDestino.CurrentRow.Index
            'odtTipDestinoGasto.Rows(intFila)("IDTIPOGASTODESTINO") = -1
            gcRetencionDetalle.Rows.Remove(gcRetencionDetalle.CurrentRow)
            odtRetencionDet.AcceptChanges()
        End If
    End Sub

    Private Sub gcRetencionDetalle_CellValueChanged(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcRetencionDetalle.CellValueChanged
        gcRetencionDetalle.EndEdit()

        'If e.Column.Name.ToString() = "colImporteRetenido" Then
        '    Dim decTotal As Decimal = 0

        '    decTotal = Round(Convert.ToDecimal(gcRetencionDetalle.CurrentRow.Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven) * (iValorPorcentaje * 100)
        '    gcRetencionDetalle.CurrentRow.Cells("colPorcentajeRetencion").Value = Round(decTotal / (100 - (iValorPorcentaje * 100)), 2, MidpointRounding.ToEven)
        '    gcRetencionDetalle.CurrentRow.Cells("colImporteTotal").Value = Round(Convert.ToDecimal(gcRetencionDetalle.CurrentRow.Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven) + Round(Convert.ToDecimal(gcRetencionDetalle.CurrentRow.Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven)
        'End If

        If e.Column.Name.ToString() = "colImporteTotal" Then
            Dim importePagar As Decimal = 0

            Dim dtImpuestosxDocumentos As DataTable
            dtImpuestosxDocumentos = New DataTable
            eRetenciones = New clsRetencion
            dtImpuestosxDocumentos = eRetenciones.fBuscarImpuestosxDocumentos(0, 2, gcRetencionDetalle.CurrentRow.Cells("colFecha").Value)
            If dtImpuestosxDocumentos.Rows.Count > 0 Then
                iValorPorcentajeRetencion = Round(Convert.ToDecimal(dtImpuestosxDocumentos.Rows(0).Item("VALOR_PORCENTAJE")), 2, MidpointRounding.ToEven) / 100
            Else
                iValorPorcentajeRetencion = 0
            End If

            gcRetencionDetalle.CurrentRow.Cells("colPorcentajeRetencion").Value = Round(Convert.ToDecimal(gcRetencionDetalle.CurrentRow.Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * iValorPorcentajeRetencion
            gcRetencionDetalle.CurrentRow.Cells("colImporteRetenido").Value = Round(Convert.ToDecimal(gcRetencionDetalle.CurrentRow.Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) + Round(gcRetencionDetalle.CurrentRow.Cells("colPorcentajeRetencion").Value, 2, MidpointRounding.ToEven)
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimirListado.Click
        Dim x As frmReporRetenciones = frmReporRetenciones.Instance
        x.MdiParent = frmPrincipal

        '' ''x.Show()  
        If gcRetencion.RowCount > 0 Then
            'If Convert.ToString(gcRetencion.CurrentRow.Cells("colNumero").Value) = "" And Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 Then
            '    Dim imprimir As New RadAboutBox1()
            '    If imprimir.ShowDialog = Windows.Forms.DialogResult.OK Then
            '        Dim idDocumento = imprimir.cboDocumentos.SelectedValue.ToString()
            '        Dim serie = imprimir.cboSerie.Text

            '        'Esta funci�n ya no v�
            '        'Dim iResultado As Int32

            '        'iResultado = eRetenciones.fActualizaSeriNumero(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), serie, "0", 6)

            '        rptRetencion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '        '1
            '        crParameterFieldDefinitions = rptRetencion.DataDefinition.ParameterFields()
            '        crParameterFieldDefinition = crParameterFieldDefinitions("@IDRETENCIONCAB")
            '        crParameterValues = crParameterFieldDefinition.CurrentValues
            '        crParameterDiscreteValue = New ParameterDiscreteValue()
            '        crParameterDiscreteValue.Value = frmRegistroRetencion.idRegistroDocumentoCab
            '        crParameterValues.Add(crParameterDiscreteValue)
            '        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '        x.CrystalReportViewer1.ReportSource = rptRetencion
            '        'rptRetencion.PrintToPrinter(1, False, 0, 1)

            '        CargarRetencionCab()

            '        x.Show()
            '    End If
            If Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 And Convert.ToString(gcRetencion.CurrentRow.Cells("colNumero").Value) <> "" Then
                If MessageBox.Show("�Desea imprimir el documento?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    'Esta funci�n ya no v�
                    'Dim iResultado As Int32

                    'iResultado = eRetenciones.fActualizaSeriNumero(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), "", "0", 10)

                    rptRetencion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
                    '1
                    crParameterFieldDefinitions = rptRetencion.DataDefinition.ParameterFields()
                    crParameterFieldDefinition = crParameterFieldDefinitions("@IDRETENCIONCAB")
                    crParameterValues = crParameterFieldDefinition.CurrentValues
                    crParameterDiscreteValue = New ParameterDiscreteValue()
                    crParameterDiscreteValue.Value = Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value)
                    crParameterValues.Add(crParameterDiscreteValue)
                    crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

                    x.CrystalReportViewer1.ReportSource = rptRetencion
                    'rptRetencion.PrintToPrinter(1, False, 0, 1)
                    CargarRetencionCab()

                    x.Show()
                End If
            End If
        End If
    End Sub

    Private Sub MECO_Reservar_Click(sender As Object, e As EventArgs) Handles MECO_Reservar.Click
        If gcRetencion.RowCount > 0 Then
            If Convert.ToString(gcRetencion.CurrentRow.Cells("colNumero").Value) = "" And Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 Then
                Dim imprimir As New RadAboutBox1()
                If imprimir.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim idDocumento = imprimir.cboDocumentos.SelectedValue.ToString()
                    Dim serie = imprimir.cboSerie.Text

                    Dim iResultado As Int32

                    iResultado = eRetenciones.fActualizaSeriNumero(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), serie, "1", 6)

                    CargarRetencionCab()
                End If
            Else
                MessageBox.Show("No se puede reservar este documento. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub MECO_Anular_Click(sender As Object, e As EventArgs) Handles MECO_Anular.Click
        If gcRetencion.RowCount > 0 Then
            If MessageBox.Show("�Est� seguro que desea anular este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                If Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 And Convert.ToString(gcRetencion.CurrentRow.Cells("colNumero").Value) <> "" Then
                    Dim iResultado As Int32
                    iResultado = eRetenciones.fAnular(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value))
                    CargarRetencionCab()
                Else
                    MessageBox.Show("No se puede anular este documento. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
    End Sub

#End Region

#Region " Metodos Privados "

    Private Sub LimpiarControles()
        txtCodigoAnexo.Text = "" : txtRuc.Text = "" : lblNombreAnexo.Text = "" : lblDireccion.Text = "" : txtSerie.Text = "" : txtNroDocumento.Text = "" : txtObservacion.Text = ""
        gcRetencionDetalle.DataSource = Nothing
    End Sub

    Private Sub InitCombos()
        Try
            Dim odtDocumento As New DataTable
            odtDocumento = eRetenciones.fCargarDocumentos(7)
            cboDocumentoOrigen.DataSource = odtDocumento
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarRetencionCab()
        Try
            'Dim odtData As New DataTable

            Dim VL_ANIO = Convert.ToString(cboAnio.Text)
            Dim VL_MES = Convert.ToString(cboMes.SelectedIndex + 1)

            odtRetencion = eRetenciones.fCargarRetencion(gEmpresa, VL_ANIO, VL_MES)
            gcRetencion.DataSource = odtRetencion
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarRetencionDet()
        Try
            'Dim odtData As New DataTable
            Dim idRetencionCab As Decimal = If(gcRetencion.RowCount > 0, Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), 0)

            odtRetencionDet = eRetenciones.fCargarRetencionDetalle(idRetencionCab)
            gcRetencionDetalle.DataSource = odtRetencionDet
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NumeroCorrelativo()
        Dim iResultado As String = ""

        iResultado = eRetenciones.fNroCorrelativo("001", 12)
        txtNroDocumento.Text = iResultado.ToString
    End Sub

    Private Function ComprobarNroCorrelativo() As Boolean
        Dim iResultado As Int32 = 0
        Dim serie As String = txtSerie.Text.Trim
        Dim numero As String = txtNroDocumento.Text.Trim
        Dim idRetencionCab As Decimal = If(strAccion = Accion.Grabar, 0, Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value))

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("SELECT 1 FROM Tesoreria.RetencionesCab WHERE SERIE = " & serie & " AND NUMERO = " & numero & " AND IDRETENCIONCAB <> " & idRetencionCab & " AND ESTADO = '1'", CommandType.Text)

                iResultado = .fCmdExecuteScalar()
                If iResultado >= "1" Then
                    MessageBox.Show("La serie y n�mero ya se encuentra registrado. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                MessageBox.Show("No se pudo comprobar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            .sDesconectarSQL()
        End With

    End Function

#End Region

#Region " M�todos de BD "

    Private Sub SumarImporte()
        Try
            decImporteRetenido = 0
            decImporteTotal = 0

            For i As Integer = 0 To gcRetencionDetalle.RowCount - 1
                decImporteRetenido += Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven)
                decImporteTotal += Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven)
            Next

        Catch ex As Exception
            MessageBox.Show("Error al sumar el importe. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Guardar()
        Try
            Dim iResultado As Int32
            Dim idRetencionCab As Decimal = 0

            iResultado = eRetenciones.fGrabar(0, gEmpresa, Convert.ToDecimal(cboDocumentoOrigen.SelectedValue), txtSerie.Text.Trim, txtNroDocumento.Text.Trim, dtFecha.Value, txtCodigoAnexo.Text.Trim, lblNombreAnexo.Text, txtRuc.Text.Trim, lblDireccion.Text, txtObservacion.Text.Trim, 1, decImporteRetenido, decImporteTotal, "0", "0", 1)
            idRetencionCab = iResultado

            For i As Integer = 0 To gcRetencionDetalle.RowCount - 1
                iResultado = eRetenciones.fGrabarDet(0, idRetencionCab, Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colNumero").Value), Convert.ToDateTime(gcRetencionDetalle.Rows(i).Cells("colFecha").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajeRetencion, 1)
            Next
        Catch ex As Exception
            MessageBox.Show("No se pudo grabar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            Dim iResultado As Int32

            iResultado = eRetenciones.fEliminar(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), 3)

            iResultado = eRetenciones.fGrabar(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), gEmpresa, Convert.ToDecimal(cboDocumentoOrigen.SelectedValue), txtSerie.Text.Trim, txtNroDocumento.Text.Trim, dtFecha.Value, txtCodigoAnexo.Text.Trim, lblNombreAnexo.Text, txtRuc.Text.Trim, lblDireccion.Text, txtObservacion.Text.Trim, 1, decImporteRetenido, decImporteTotal, "0", "0", 2)

            For i As Integer = 0 To gcRetencionDetalle.RowCount - 1
                If Convert.ToInt32(gcRetencionDetalle.Rows(i).Cells("colIdRetencionDet").Value) = 0 Then
                    iResultado = eRetenciones.fGrabarDet(0, Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colNumero").Value), Convert.ToDateTime(gcRetencionDetalle.Rows(i).Cells("colFecha").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajeRetencion, 1)
                Else
                    iResultado = eRetenciones.fGrabarDet(Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdRetencionDet").Value), Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colNumero").Value), Convert.ToDateTime(gcRetencionDetalle.Rows(i).Cells("colFecha").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajeRetencion, 2)
                End If
            Next

        Catch ex As Exception
            MessageBox.Show("No se pudo modificar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function Eliminar() As Boolean
        Try
            If MessageBox.Show("�Est� seguro que desea eliminar este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then

                If Not ComprobarDato() Then Exit Function

                Dim iResultado As Int32
                iResultado = eRetenciones.fEliminar(Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), 3)

                If Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 1 Then ' Orden de Compra
                    For i As Integer = 0 To gcRetencionDetalle.RowCount - 1
                        'If Convert.ToInt32(gcRetencionDetalle.Rows(i).Cells("colIdRetencionDet").Value) = 0 Then
                        iResultado = eRetenciones.fGrabarDet(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colIdRetencionDet").Value), Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colNumero").Value), dtFecha.Value, Convert.ToString(gcRetencionDetalle.Rows(i).Cells("colIdMoneda").Value), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colPorcentajeRetencion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteRetenido").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcRetencionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), iValorPorcentajeRetencion, 3)
                        'End If
                    Next
                End If

                Return True
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo eliminar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Function ComprobarDato() As Boolean
        Dim iResultado As Int32 = 0
        Dim decId As Decimal = Convert.ToDecimal(gcRetencion.CurrentRow.Cells("colIdRetencionCab").Value)

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("SELECT 1 FROM Tesoreria.RetencionesCab RC INNER JOIN Tesoreria.RetencionesDET RD ON RC.IDRETENCIONCAB = RD.IDRETENCIONCAB AND RD.ESTADO = '1' " & _
                             "INNER JOIN Tesoreria.DOC_PENDIENTES DP ON RD.NROITEM = DP.ID_DOCORIGEN WHERE RC.IDRETENCIONCAB = " & decId & " AND DP.SALDO <> DP.TOTAL AND RC.ESTADO = '1'", CommandType.Text)

                iResultado = .fCmdExecuteScalar()
                If iResultado >= "1" Then
                    MessageBox.Show("No se puede modificar/eliminar esta orden de compra porque tiene pagos realizados. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                MessageBox.Show("No se pudo comprobar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            .sDesconectarSQL()
        End With

    End Function

#End Region

    'Private Sub chkResevarNumero_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs) Handles chkResevarNumero.ToggleStateChanged
    '    If Not Loaded Then Exit Sub
    '    If chkResevarNumero.Checked = True Then
    '        estado = "0"
    '    Else
    '        estado = "1"
    '    End If
    'End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        If gcRetencion.RowCount > 0 Then
            Dim export As ExportToExcelML = New ExportToExcelML(Me.gcRetencion)

            export.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport
            export.ExportVisualSettings = True
            export.SheetName = "Retenciones"
            export.SummariesExportOption = SummariesOption.DoNotExport

            Dim save As SaveFileDialog = New SaveFileDialog()
            save.Filter = "Excel|*.xls"
            save.Title = "Guardar como archivo de excel"
            save.ShowDialog()
            export.FileExtension = "xls"
            export.RunExport(save.FileName)
            'export.RunExport(save.FileName + ".xls")
        End If
    End Sub

    Private Sub btnReservar_Click(sender As Object, e As EventArgs) Handles btnReservar.Click
        Dim Reservas As New frmReservarRetencion()
        If Reservas.ShowDialog = Windows.Forms.DialogResult.OK Then
            'txtCodigoAnexo.Text = frmRegistroRetencion.strCodigo
            'txtRuc.Text = frmRegistroRetencion.strRuc
            'lblNombreAnexo.Text = frmRegistroRetencion.strNombre
            'lblDireccion.Text = frmRegistroRetencion.strDireccion

            '    grpFacturas.Visible = True

            '    If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
            '        odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
            '        gcFacturas.DataSource = odtRetencion
            '    ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then
            '        odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
            '        gcFacturas.DataSource = odtRetencion
            '    End If

            CargarRetencionCab()
        End If
    End Sub

    Private Sub cboAnio_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboAnio.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarRetencionCab()
    End Sub

    Private Sub cboMes_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboMes.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarRetencionCab()
    End Sub

End Class