Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmSaldoDocumento
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer '18/06/2007: Modificacion Realizada
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eSaldoDocumento As clsSaldoDocumento
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmSaldoDocumento = Nothing
    Public Shared Function Instance() As frmSaldoDocumento
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmSaldoDocumento
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        'cboMovimiento.Clear()
        'cboempresa.Clear()
        'cboproveedor.Clear()
        'cboDocOrigen.Clear()
        txtSaldoDoc.Clear()
        'cboEstado.Clear()
    End Sub

    Private Sub DesabilitarControles()
        cboMovimiento.Enabled = False
        cboEmpresa.Enabled = False
        cboProveedor.Enabled = False
        cboDocOrigen.Enabled = False
        txtSaldoDoc.ReadOnly = True
        cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        cboMovimiento.Enabled = True
        cboEmpresa.Enabled = True
        cboProveedor.Enabled = True
        cboDocOrigen.Enabled = True
        txtSaldoDoc.ReadOnly = False
        cboMovimiento.SelectedIndex = 0
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = True
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        If sTab = 0 Then
            'MessageBox.Show("Buscar")
        End If
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            DesabilitarControles()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2 '18/06/2007: Modificacion Realizada
            VerPosicion()
            HabilitarControles()
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        Close()
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        'F: Transaccion

        Dim iResultado As Int32
        'Dim iDuplicado As Int32 'Modificado el 05/06
        Dim sCodigoActual As String = ""
        eTempo = New clsPlantTempo
        eSaldoDocumento = New clsSaldoDocumento 'Movido el 05/06

        If sTab = 1 Then

            If Me.cboMovimiento.Text.Trim.Length = 0 Or Me.cboDocOrigen.Text.Trim.Length = 0 Or Me.txtSaldoDoc.Text.Trim.Length = 0 Or Me.cboEstado.Text.Trim.Length = 0 Then
                eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                frmPrincipal.sFlagGrabar = "0"
            Else

                'iDuplicado = eSaldoDocumento.fBuscarDoble(Convert.ToString(Me.txtDescripcion.Text.Trim), Convert.ToString(Me.txtCodigo.Text.Trim)) 'Modificado el 05/06
                'If iDuplicado > 0 Then 'Modificado el 05/06
                '    Exit Sub 'Modificado el 05/06
                'End If 'Modificado el 05/06

                If (MessageBox.Show("�Esta seguro de GRABAR ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then '18/06/2007: Modificacion Realizada
                        eSaldoDocumento.fCodigo()
                        sCodigoActual = eSaldoDocumento.sCodFuturo
                    Else
                        sCodigoActual = Me.txtCodigo.Text
                    End If
                    iResultado = eSaldoDocumento.fGrabar(sCodigoActual, Convert.ToString(Me.cboMovimiento.SelectedValue), Convert.ToString(Me.cboEmpresa.SelectedValue), Convert.ToString(Me.cboProveedor.SelectedValue), Convert.ToString(Me.cboDocOrigen.SelectedValue), Convert.ToString(Me.txtSaldoDoc.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue), iOpcion) '18/06/2007: Modificacion   

                    If iResultado > 0 Then
                        mMostrarGrilla()
                        Timer2.Enabled = True
                    End If
                    sTab = 0
                End If

            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'F: Transaccion

        Dim iResultado As Int32
        Dim Mensaje As String
        Dim iActivo As Integer '18/06/2007: Modificacion Realizada

        If sTab = 0 Then
            Try
                If Me.dgvLista.Rows.Count > 0 Then

                    If Fila("Estados") = "ACTIVO" Then
                        iActivo = 5 '18/06/2007: Modificacion Realizada
                        Mensaje = "Desea ANULAR: "
                    Else
                        iActivo = 3 '18/06/2007: Modificacion Realizada
                        Mensaje = "Desea ELIMINAR: "
                    End If

                    If (MessageBox.Show(Mensaje & Chr(13) & Chr(13) & "Codigo: " & Fila("IdSaldoDocumento") & "   " & Chr(13), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        'Using scope As TransactionScope = New TransactionScope '(MSDTC)
                        eSaldoDocumento = New clsSaldoDocumento
                        iResultado = eSaldoDocumento.fEliminar(Fila("IdSaldoDocumento"), iActivo) '18/06/2007: Modificacion Realizada
                        'scope.Complete()
                        'End Using
                        If iResultado = 1 Then
                            mMostrarGrilla()
                        End If
                    Else
                        Exit Sub
                    End If

                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()
        frmPrincipal.fBuscar = False 'Desabilita los botones de buscar
        frmPrincipal.fExportar = False 'Desabilita los botones de exportar
        frmPrincipal.fImprimir = False 'Desabilita los botones de imprimir
    End Sub

    Private Sub mMostrarGrilla()
        eSaldoDocumento = New clsSaldoDocumento
        eTempo = New clsPlantTempo

        dtTable = New DataTable
        dtTable = eSaldoDocumento.fListar(iEstado01, iEstado02)

        Me.dgvLista.AutoGenerateColumns = False
        Me.dgvLista.DataSource = dtTable
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        VerPosicion()
        Me.stsTotales.Items(0).Text = "Total de Registros= " & eSaldoDocumento.iNroRegistros

        If eSaldoDocumento.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If

        Me.cboEstado.DataSource = eTempo.fColEstado
        Me.cboEstado.ValueMember = "Col02"
        Me.cboEstado.DisplayMember = "Col01"

        Me.cboEmpresa.DataSource = eSaldoDocumento.fListarEmpresa
        Me.cboEmpresa.ValueMember = "EmprCodigo"
        Me.cboEmpresa.DisplayMember = "EmprDescripcion"

        Me.cboProveedor.DataSource = eSaldoDocumento.fListarProveedor
        Me.cboProveedor.ValueMember = "PrvCodigo"
        Me.cboProveedor.DisplayMember = "PrvRazonSocial"


    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception

        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdSaldoDocumento").ToString
                Me.cboMovimiento.Text = Fila("IdMovimiento").ToString
                Me.cboEmpresa.Text = Fila("EmprCodigo").ToString
                Me.cboProveedor.Text = Fila("PrvCodigo").ToString
                Me.cboDocOrigen.Text = Fila("DocumentoOrigen").ToString
                Me.txtSaldoDoc.Text = Fila("SaldoDocumento").ToString
                Me.cboEstado.Text = Fila("Estado").ToString

            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.cboMovimiento.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

End Class
