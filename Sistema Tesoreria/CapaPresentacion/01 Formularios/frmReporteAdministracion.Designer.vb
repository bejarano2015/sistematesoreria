<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteAdministracion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.cboMes = New System.Windows.Forms.ComboBox
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboAño = New System.Windows.Forms.ComboBox
        Me.cboCategoriaGastosGenerales = New System.Windows.Forms.ComboBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.txtTipoCambio = New ctrLibreria.Controles.BeTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rdbDet = New System.Windows.Forms.RadioButton
        Me.rdbRes = New System.Windows.Forms.RadioButton
        Me.Button1 = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(-1, 41)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(1060, 555)
        Me.CrystalReportViewer1.TabIndex = 10
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'cboMes
        '
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cboMes.Location = New System.Drawing.Point(361, 10)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(91, 21)
        Me.cboMes.TabIndex = 3
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(326, 16)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(30, 13)
        Me.BeLabel3.TabIndex = 2
        Me.BeLabel3.Text = "Mes"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(461, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Año : "
        '
        'cboAño
        '
        Me.cboAño.FormattingEnabled = True
        Me.cboAño.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040"})
        Me.cboAño.Location = New System.Drawing.Point(508, 10)
        Me.cboAño.Name = "cboAño"
        Me.cboAño.Size = New System.Drawing.Size(69, 21)
        Me.cboAño.TabIndex = 5
        '
        'cboCategoriaGastosGenerales
        '
        Me.cboCategoriaGastosGenerales.FormattingEnabled = True
        Me.cboCategoriaGastosGenerales.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cboCategoriaGastosGenerales.Location = New System.Drawing.Point(77, 10)
        Me.cboCategoriaGastosGenerales.Name = "cboCategoriaGastosGenerales"
        Me.cboCategoriaGastosGenerales.Size = New System.Drawing.Size(239, 21)
        Me.cboCategoriaGastosGenerales.TabIndex = 1
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(8, 15)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Categoría"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(884, 9)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(47, 23)
        Me.btnAceptar.TabIndex = 9
        Me.btnAceptar.Text = "Ver"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.txtTipoCambio.KeyEnter = True
        Me.txtTipoCambio.Location = New System.Drawing.Point(670, 10)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTipoCambio.ShortcutsEnabled = False
        Me.txtTipoCambio.Size = New System.Drawing.Size(43, 20)
        Me.txtTipoCambio.TabIndex = 7
        Me.txtTipoCambio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(587, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Tipo Cambio"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbDet)
        Me.GroupBox1.Controls.Add(Me.rdbRes)
        Me.GroupBox1.Location = New System.Drawing.Point(721, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(157, 36)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Opción"
        '
        'rdbDet
        '
        Me.rdbDet.AutoSize = True
        Me.rdbDet.Location = New System.Drawing.Point(84, 13)
        Me.rdbDet.Name = "rdbDet"
        Me.rdbDet.Size = New System.Drawing.Size(70, 17)
        Me.rdbDet.TabIndex = 1
        Me.rdbDet.TabStop = True
        Me.rdbDet.Text = "Detallado"
        Me.rdbDet.UseVisualStyleBackColor = True
        '
        'rdbRes
        '
        Me.rdbRes.AutoSize = True
        Me.rdbRes.Location = New System.Drawing.Point(6, 13)
        Me.rdbRes.Name = "rdbRes"
        Me.rdbRes.Size = New System.Drawing.Size(72, 17)
        Me.rdbRes.TabIndex = 0
        Me.rdbRes.TabStop = True
        Me.rdbRes.Text = "Resumido"
        Me.rdbRes.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(937, 9)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 23)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Centro de Costos"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmReporteAdministracion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1058, 596)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.cboCategoriaGastosGenerales)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboAño)
        Me.Controls.Add(Me.cboMes)
        Me.Controls.Add(Me.BeLabel3)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.MaximizeBox = False
        Me.Name = "frmReporteAdministracion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reportes Generales de Administración"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents cboMes As System.Windows.Forms.ComboBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboAño As System.Windows.Forms.ComboBox
    Friend WithEvents cboCategoriaGastosGenerales As System.Windows.Forms.ComboBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtTipoCambio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbDet As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRes As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
