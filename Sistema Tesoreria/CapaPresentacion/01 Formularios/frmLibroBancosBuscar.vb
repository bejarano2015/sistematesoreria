Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmLibroBancosBuscar

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmLibroBancosBuscar = Nothing
    Public Shared Function Instance() As frmLibroBancosBuscar
        If frmInstance Is Nothing Then
            frmInstance = New frmLibroBancosBuscar
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmLibroBancosBuscar_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub
#End Region


    Private eChequera As clsChequera
    Private eLibroBancos As clsLibroBancos
    Private eTempo As clsPlantTempo
    Dim iOpcion As Int16 = 0
    Dim sVisualizarDatos As String = ""
    Public sCuentaBuscar As String = ""
    Private eITF As clsITF
    Dim sCodigoDetPosicion As String = ""

    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
    End Sub

    
    Private Sub dgvLibroDet_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLibroDet.CellContentClick
        Dim columna As Integer = dgvLibroDet.CurrentCell.ColumnIndex
        If dgvLibroDet.Rows.Count > 0 Then
            dgvLibroDet.CurrentCell = dgvLibroDet(0, dgvLibroDet.CurrentRow.Index)
            dgvLibroDet.CurrentCell = dgvLibroDet(columna, dgvLibroDet.CurrentRow.Index)
        End If
        Calcular()
        If columna = 7 Then
            Dim itfVoF As String = ""
            Dim IdTipoMov As String = ""
            If dgvLibroDet.Rows.Count > 0 Then
                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value) = True Then
                    itfVoF = ""
                Else
                    itfVoF = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value)
                End If

                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value) = True Then
                    IdTipoMov = ""
                Else
                    IdTipoMov = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value)
                End If
            End If

            If itfVoF = "0" Then
                If IdTipoMov = "00002" Then
                    MessageBox.Show("Este Cheque ya Tiene ITF", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                If dgvLibroDet.Rows.Count > 0 Then
                    dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value = False
                    dgvLibroDet.CurrentCell = dgvLibroDet(7, dgvLibroDet.CurrentRow.Index)
                    Calcular()
                End If
                Exit Sub
            End If

            If IdTipoMov <> "" And txtMarca.Text = "0" Then 'And itfVoF = "1" Then
                Panel5.Visible = True
                'If cboTipoMov.SelectedValue = "00002" Then
                '    Panel3.Visible = True
                '    dtpFechaItf.Value = Now.Date().ToShortDateString
                '    txtGlosaItf.Text = "ITF CHEQ " & txtNroDoc.Text.Trim
                '    txtITF1.Text = Format(PorcentajeITF, "0.00000")
                '    'txtImporteItf.Text = Format((Val(txtImporte.Text) * PorcentajeITF) / 100, "0.00")
                '    'txtImporteItf.Focus()
                '    Dim ItfMontoSumado As Double = 0
                '    ItfMontoSumado = (Val(txtImporte.Text) * PorcentajeITF) / 100
                '    txtImporteItf.Text = Format(ItfMontoSumado, "0.00")
                '    txtImporteItf.Focus()
                'End If
            ElseIf IdTipoMov = "" And txtMarca.Text = "1" Then
                Panel3.Visible = False
                dtpFechaItf.Value = Now.Date().ToShortDateString
                txtGlosaItf.Clear()
                txtImporteItf.Clear()
            ElseIf IdTipoMov = "" Then
                Panel3.Visible = False
                dtpFechaItf.Value = Now.Date().ToShortDateString
                txtGlosaItf.Clear()
                txtImporteItf.Clear()
            End If

        End If
    End Sub

    Sub Calcular()
        If dgvLibroDet.Rows.Count > 0 Then
            If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = True Then
                txtMarca.Text = "1"
            Else
                txtMarca.Text = "0"
            End If
        End If
    End Sub

    Private Sub dgvLibroDet_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLibroDet.CellEndEdit
        Calcular()
    End Sub

    Private Sub dgvLibroDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLibroDet.Click
        Calcular()
    End Sub

    Private Sub btnGenerarITF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarITF.Click
        Panel5.Visible = False
        Panel3.Visible = True

        Dim itfVoF As String = ""
        Dim IdTipoMov As String = ""
        Dim NroCheque As String = ""
        Dim Importe As String = ""
        If dgvLibroDet.Rows.Count > 0 Then
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value) = True Then
                itfVoF = ""
            Else
                itfVoF = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value) = True Then
                IdTipoMov = ""
            Else
                IdTipoMov = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(14).Value) = True Then
                NroCheque = ""
            Else
                NroCheque = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(14).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value) = True Then
                Importe = 0
            Else
                Importe = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value)
            End If
        End If

        If IdTipoMov = "00002" Then
            'Panel3.Visible = True
            dtpFechaItf.Value = Now.Date().ToShortDateString

            Dim dtDoble As DataTable
            dtDoble = New DataTable
            eITF = New clsITF
            dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpFechaItf.Value)
            If dtDoble.Rows.Count > 0 Then
                PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
            End If

            txtGlosaItf.Text = "ITF CHEQ " & NroCheque
            txtITF1.Text = Format(PorcentajeITF, "0.00000")
            'txtImporteItf.Text = Format((Val(txtImporte.Text) * PorcentajeITF) / 100, "0.00")
            'txtImporteItf.Focus()
            Dim ItfMontoSumado As Double = 0

            'ItfMontoSumado = (Val(Importe) * PorcentajeITF) / 100

            Dim MontoParaItf As Double = 0
            MontoParaItf = ReturnMontoParaCalcItf(Importe)
            ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100

            txtImporteItf.Text = Format(ItfMontoSumado, "0.00")
            dtpFechaItf.Focus()
        End If
    End Sub

    Private Sub btnCancelarItf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarItf.Click
        If dgvLibroDet.Rows.Count > 0 Then
            dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value = True
        End If
        Panel3.Visible = False
        dtpFechaItf.Value = Now.Date().ToShortDateString
        txtGlosaItf.Clear()
        txtImporteItf.Clear()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If dgvLibroDet.Rows.Count > 0 Then
            dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value = True
            Panel5.Visible = False
            dtpFechaItf.Value = Now.Date().ToShortDateString
            txtGlosaItf.Clear()
            txtImporteItf.Clear()
        End If
        Panel5.Visible = False
        Panel3.Visible = False
    End Sub

    Private Sub btnGrabarItf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarItf.Click
        Dim Cobrados As Double = 0
        Dim NoCobrados As Double = 0

        Dim FechaCheque As DateTime

        Dim IdTipoMov As String = ""
        Dim NroCheque As String = ""
        Dim Importe As String = ""
        Dim Concepto As String = ""
        Dim Descripcion As String = ""

        If dgvLibroDet.Rows.Count > 0 Then
            FechaCheque = Convert.ToDateTime(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column1").Value))

            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value) = True Then
                IdTipoMov = ""
            Else
                IdTipoMov = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(14).Value) = True Then
                NroCheque = ""
            Else
                NroCheque = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(14).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value) = True Then
                Importe = 0
            Else
                Importe = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(2).Value) = True Then
                Concepto = ""
            Else
                Concepto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(2).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(3).Value) = True Then
                Descripcion = ""
            Else
                Descripcion = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(3).Value)
            End If
        End If

        Dim iResultadoDetx As Int16 = 0
        Dim IdCuenta As String = ""
        Dim CountLibrosSuperiores As Integer = 0

        If dtpFechaItf.Value >= FechaCheque Then
            If dgvLibroDet.Rows.Count > 0 Then
                If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
                    eLibroBancos = New clsLibroBancos
                    Dim sCDetalle As String = ""
                    Dim sCCabecera As String = ""
                    Dim iActCheckDet As Integer = 0
                    sCDetalle = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value
                    sCCabecera = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value
                    'DESACTIVA CHEK DEL CHEQUE EN BD
                    iActCheckDet = eLibroBancos.fActMarcaDet(gEmpresa, Trim(sCDetalle), Trim(sCCabecera), dtpFechaItf.Value, Trim(IdTipoMov), Trim(NroCheque), Trim(Concepto), Trim(Descripcion), "", 0, 0, 0, 0)
                    Dim dtDataLibro As DataTable
                    Dim xMes As String = ""
                    Dim xAno As String = ""
                    dtDataLibro = New DataTable
                    dtDataLibro = eLibroBancos.TraerMesyAnoLibro(gEmpresa, sCCabecera)
                    If dtDataLibro.Rows.Count > 0 Then
                        xMes = Trim(dtDataLibro.Rows(0).Item("Mes"))
                        xAno = Trim(dtDataLibro.Rows(0).Item("A�o"))
                        IdCuenta = Trim(dtDataLibro.Rows(0).Item("IdCuenta"))
                    End If
                    Dim pMes As String = Format(dtpFechaItf.Value.Month, "00")
                    Dim pA�o As String = dtpFechaItf.Value.Year
                    If xMes = pMes And xAno = pA�o Then
                        'GRABA REGISTRO DE ITF EN BD, SI EL ITF PERTENECE AL MES ACTUAL
                        Dim sCodigoDetx As String = ""
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigoDet(gEmpresa, sCCabecera)
                        sCodigoDetx = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro actual
                        sCodigoDetPosicion = Trim(sCodigoDetx)
                        iResultadoDetx = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDetx, sCCabecera, dtpFechaItf.Value, "00011", "", Trim(txtGlosaItf.Text.Trim), "", "H", Convert.ToDouble(txtImporteItf.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", sCCabecera, sCDetalle)
                    End If
                    If iActCheckDet = 1 Then
                        '--------------
                        Dim dtDatos As DataTable
                        dtDatos = New DataTable
                        dtDatos = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                        Dim dSaldoInicial As Double = 0
                        If dtDatos.Rows.Count > 0 Then
                            dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                            Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                            NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                        End If
                        '--------------
                        'ACTUALIZAR EL SALDO DEL LIBRO
                        Dim iResultadoActSaldoLibroActual As Int16 = 0
                        Dim dtLibroDet As DataTable
                        dtLibroDet = New DataTable
                        Dim saldo As Double = 0
                        saldo = dSaldoInicial
                        'Dim Cobrados As Double = 0
                        'Dim NoCobrados As Double = 0
                        dtLibroDet = eLibroBancos.TraerLibroDet(37, sCCabecera, gEmpresa)
                        If dtLibroDet.Rows.Count > 0 Then
                            For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                    If z = 11 Then 'lee el importe
                                        If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                            saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                        ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                            saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then

                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                            '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                        End If
                                    End If
                                Next
                            Next
                            Dim dtDtFech As DataTable
                            dtDtFech = New DataTable
                            Dim Fecha As DateTime
                            Dim FechaActCabecera As DateTime
                            dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCCabecera)
                            If dtDtFech.Rows.Count > 0 Then
                                Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                            End If
                            'If dtpFechaItf.Value > Fecha Then
                            'FechaActCabecera = dtpFechaItf.Value
                            'Else
                            FechaActCabecera = Fecha
                            Cobrados = Cobrados + Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                            'ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "1" Then
                            NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))

                            'End If
                            iResultadoActSaldoLibroActual = eLibroBancos.fActSaldoLibro(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                            '----------------------------------------
                            'saber si existe libros proximos al mes DEL CHEKE DEL Q SE GENERO EL ITF
                            Dim dtDatosLibro As DataTable
                            dtDatosLibro = New DataTable
                            dtDatosLibro = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                            Dim sIdCuenta As String = ""
                            If dtDatosLibro.Rows.Count > 0 Then
                                sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                            End If
                            Dim dtLibrosSuperiores As DataTable
                            dtLibrosSuperiores = New DataTable
                            'Dim A�o As String = dtpFechaItf.Value.Year
                            'Dim Mes As String = Format(dtpFechaItf.Value.Month, "00")
                            dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(xMes, xAno, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros

                            CountLibrosSuperiores = dtLibrosSuperiores.Rows.Count
                            If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                    Dim iResultadoActLibroSup As Int16 = 0
                                    Dim CodigoLibroBD As String = ""
                                    Dim SaldoInicialBD As Double ' = 0
                                    Dim SaldoFinalBD As Double '= 0
                                    CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    SaldoInicialBD = Format(saldo, "0.00")
                                    SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtImporteItf.Text)
                                    'graba saldos de uno de los libro proximos

                                    '''''''''''''''''''''''''''''''''''''''''
                                    Dim SaldoPrendaS As Double
                                    Dim SaldoGarantiaS As Double
                                    Dim SaldoDepPlazS As Double
                                    Dim SaldoRetencionS As Double
                                    SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                    SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                    SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                    SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                    'If cboTipoCargo.SelectedIndex > -1 Then
                                    'If cboTipoCargo.SelectedValue = "00002" Then
                                    '    SaldoPrendaS = SaldoPrendaS + Convert.ToDouble(txtImporte.Text)
                                    'ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                    '    SaldoGarantiaS = SaldoGarantiaS + Convert.ToDouble(txtImporte.Text)
                                    'ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                    '    SaldoDepPlazS = SaldoDepPlazS + Convert.ToDouble(txtImporte.Text)
                                    'ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                    '    SaldoRetencionS = SaldoRetencionS + Convert.ToDouble(txtImporte.Text)
                                    'End If
                                    'SaldoPrendaS,SaldoGarantiaS,SaldoDepPlazS,SaldoRetencionS
                                    'End If
                                    '''''''''''''''''''''''''''''''''''''''''

                                    iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)

                                    saldo = SaldoFinalBD
                                Next
                            End If
                            '----------------------------------------
                        End If
                    End If



                    If iResultadoDetx = 0 Then 'SI NO SE GRABO EL ITF ENTONCES
                        sCodigoDetPosicion = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value
                        '**************LIBRO BANCOS CUENTA ORIGEN**************************************************************
                        '******************************************************************************************************
                        Dim sCodigoLibro As String = ""
                        Dim sCodigoDet As String = ""
                        eLibroBancos = New clsLibroBancos
                        Dim dtDatos As DataTable
                        dtDatos = New DataTable
                        Dim IdLibro As String = ""
                        Dim A�o As String = dtpFechaItf.Value.Year
                        Dim Mes As String = Format(dtpFechaItf.Value.Month, "00")
                        dtDatos = eLibroBancos.TraerIdLibro(A�o, Mes, Trim(IdCuenta), gEmpresa)
                        If dtDatos.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO
                            Dim iResultadoDet As Int16 = 0
                            Dim iResultadoActSaldoLibro As Int16 = 0
                            sCodigoLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                            Dim dSaldoInicial As Double = 0
                            Dim dSaldoFinal As Double = 0
                            Dim dCheCobradosNO As Double = 0
                            Dim dCheCobradosSI As Double = 0
                            dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                            dSaldoFinal = Trim(dtDatos.Rows(0).Item("Saldo")) 'Saldo Final
                            dCheCobradosNO = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                            dCheCobradosSI = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                            'Agregar un nuevo detalle de libro Actual
                            eLibroBancos = New clsLibroBancos
                            eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro.Trim)
                            sCodigoDet = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro creado
                            Dim MarcaCheqNoCobrado As Integer = 0
                            'Dim Cobrados As Double = 0
                            'Dim NoCobrados As Double = 0
                            MarcaCheqNoCobrado = 0
                            Cobrados = dCheCobradosSI
                            NoCobrados = dCheCobradosNO
                            iResultadoDet = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpFechaItf.Value, "00011", "", Trim(txtGlosaItf.Text.Trim), "", "H", Convert.ToDouble(txtImporteItf.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", sCCabecera, sCDetalle)
                            Dim saldo As Double = 0
                            If CountLibrosSuperiores = 0 Then
                                saldo = dSaldoFinal - Convert.ToDouble(txtImporteItf.Text)
                            Else
                                saldo = dSaldoFinal
                            End If

                            'actualiza el nuevo libro
                            If iResultadoDet = 1 Then
                                Dim iResultadoActDetalleLibro As Int16 = 0
                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                                Dim dtDtFech As DataTable
                                dtDtFech = New DataTable
                                Dim Fecha As DateTime
                                Dim FechaActCabecera As DateTime
                                dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCodigoLibro)
                                If dtDtFech.Rows.Count > 0 Then
                                    Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                                End If
                                If dtpFechaItf.Value > Fecha Then
                                    FechaActCabecera = dtpFechaItf.Value
                                Else
                                    FechaActCabecera = Fecha
                                End If
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                                '----------------------------------------
                                'saber si existe libros proximos al mes actual de la cuenta
                                Dim dtDatosLibro As DataTable
                                dtDatosLibro = New DataTable
                                dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigoLibro, gEmpresa)
                                Dim sIdCuenta As String = ""
                                If dtDatosLibro.Rows.Count > 0 Then
                                    sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                                End If
                                Dim dtLibrosSuperiores As DataTable
                                dtLibrosSuperiores = New DataTable
                                dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                                If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                    For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                        Dim iResultadoActLibroSup As Int16 = 0
                                        Dim CodigoLibroBD As String = ""
                                        Dim SaldoInicialBD As Double ' = 0
                                        Dim SaldoFinalBD As Double '= 0
                                        CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                        SaldoInicialBD = Format(saldo, "0.00")
                                        SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtImporteItf.Text)
                                        'graba saldos de uno de los libro proximos

                                        '''''''''''''''''''''''''''''''''''''''''
                                        Dim SaldoPrendaS As Double
                                        Dim SaldoGarantiaS As Double
                                        Dim SaldoDepPlazS As Double
                                        Dim SaldoRetencionS As Double
                                        SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                        SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                        SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                        SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                        'If cboTipoCargo.SelectedIndex > -1 Then
                                        'If cboTipoCargo.SelectedValue = "00002" Then
                                        '    SaldoPrendaS = SaldoPrendaS + Convert.ToDouble(txtImporte.Text)
                                        'ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                        '    SaldoGarantiaS = SaldoGarantiaS + Convert.ToDouble(txtImporte.Text)
                                        'ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                        '    SaldoDepPlazS = SaldoDepPlazS + Convert.ToDouble(txtImporte.Text)
                                        'ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                        '    SaldoRetencionS = SaldoRetencionS + Convert.ToDouble(txtImporte.Text)
                                        'End If
                                        'SaldoPrendaS,SaldoGarantiaS,SaldoDepPlazS,SaldoRetencionS
                                        'End If
                                        '''''''''''''''''''''''''''''''''''''''''

                                        iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                        saldo = SaldoFinalBD
                                    Next
                                End If
                                '----------------------------------------
                            End If
                        ElseIf dtDatos.Rows.Count = 0 Then ' NO EXISTE LIBRO DEL MES DE PAGO
                            Dim MesAnt As String = ""
                            Dim Anio As String = ""
                            MesAnt = Format(Mes - 1, "00") 'resto el mes actual, para usar el mes anterior
                            Anio = Trim(dtpFechaItf.Value.Year)
                            If Trim(Mes) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y a�o el ACTUAL -1
                                MesAnt = "12"
                                Anio = Trim(A�o) - 1
                            End If
                            Dim dtLibroAnterior As DataTable
                            dtLibroAnterior = New DataTable
                            dtLibroAnterior = eLibroBancos.TraerIdLibro(Anio, MesAnt, Trim(IdCuenta), gEmpresa)
                            Dim iResultado As Int16 = 0
                            Dim iResultadoDet2 As Int16 = 0
                            Dim SaldoAnterior As Double = 0
                            If dtLibroAnterior.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigo(gEmpresa)
                                sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                                'graba la cabecera de libro
                                'saldo inicial = saldo final del libro anterior
                                SaldoAnterior = Trim(dtLibroAnterior.Rows(0).Item("Saldo"))
                                Dim SaldoPrenda As Double = 0
                                Dim SaldoGarantia As Double = 0
                                Dim SaldoDepPlaz As Double = 0
                                Dim SaldoRetencion As Double = 0
                                Dim SaldoIniExtracto As Double = 0
                                SaldoPrenda = Trim(dtLibroAnterior.Rows(0).Item("SaldoPrenda"))
                                SaldoGarantia = Trim(dtLibroAnterior.Rows(0).Item("SaldoGarantia"))
                                SaldoDepPlaz = Trim(dtLibroAnterior.Rows(0).Item("SaldoDepPlaz"))
                                SaldoRetencion = Trim(dtLibroAnterior.Rows(0).Item("SaldoRetencion"))
                                SaldoIniExtracto = Trim(dtLibroAnterior.Rows(0).Item("SaldoFinExtracto"))

                                'SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion
                                iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(IdCuenta), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtImporteItf.Text), dtpFechaItf.Value, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                            ElseIf dtLibroAnterior.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigo(gEmpresa)
                                sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                                'graba la cabecera de libro
                                'saldo anterior =0
                                SaldoAnterior = 0
                                iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(IdCuenta), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtImporteItf.Text), dtpFechaItf.Value, 0, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                            End If

                            If iResultado = 2627 Then
                                MessageBox.Show("No se Grabo el Registro Intente grabar otra vez", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Exit Sub
                            End If

                            If iResultado > 0 And iResultado <> 2627 Then
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro)
                                sCodigoDet = eLibroBancos.sCodFuturoDet
                                Dim MarcaCheqNoCobrado As Integer = 0
                                Cobrados = 0
                                NoCobrados = 0


                                iResultadoDet2 = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpFechaItf.Value, "00011", "", Trim(txtGlosaItf.Text.Trim), "", "H", Convert.ToDouble(txtImporteItf.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", sCCabecera, sCDetalle)
                                Dim saldo As Double = 0
                                saldo = SaldoAnterior - Convert.ToDouble(txtImporteItf.Text)
                                'actualiza el nuevo libro
                                If iResultadoDet2 = 1 Then
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                                    Dim iResultadoActSaldoLibro As Int16 = 0
                                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(SaldoAnterior, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), dtpFechaItf.Value) 'graba el nuevo saldo del libro
                                End If
                            End If
                        End If
                        '************** FIN LIBRO BANCOS CUENTA ORIGEN ********************************************************
                        '******************************************************************************************************
                    End If


                    
                    'cboMes_SelectedIndexChanged(sender, e)
                    'MessageBox.Show("Se Agrego el ITF con Exito!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Panel3.Visible = False
                    dtpFechaItf.Value = Now.Date().ToShortDateString
                    txtGlosaItf.Clear()
                    txtImporteItf.Clear()
                    txtTexto.Clear()
                    txtTexto.Focus()
                    txtTexto_TextChanged_1(sender, e)
                    Panel3.Visible = False
                    Panel5.Visible = False
                    Panel3.Visible = False

                    Dim _formInterface As frmLibroBancos = CType(Me.Owner, frmLibroBancos)
                    _formInterface.hace = 1
                    _formInterface.sCodigoDetPosicion = sCodigoDetPosicion
                    Me.Close()

                    'frmLibroBancos.
                    'frmLibroBancos.cboMes.SelectedIndex = -1
                    'frmLibroBancos.Activate()
                    'frmLibroBancos.cboMes_SelectedIndexChanged(sender, e)
                End If ' If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
            End If 'If dgvLibroDet.Rows.Count > 0 Then
        Else
            MessageBox.Show("La Fecha del ITF debe ser Mayor o Igual a la del Cheque", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaItf.Focus()
        End If
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        Dim c As Cursor = Me.Cursor
        Try
           
            If (MessageBox.Show("�Esta seguro de Confirmar el Cheque como Cobrado con fecha " & dtpFechaCobro.Value & "?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then

                Me.Cursor = Cursors.WaitCursor
                Dim Cobrados As Double = 0
                Dim NoCobrados As Double = 0

                Dim iResultadoDetx As Int16 = 0
                Dim iResultadoActSaldoLibroActual As Int16 = 0
                If dgvLibroDet.Rows.Count > 0 Then
                    If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
                        eLibroBancos = New clsLibroBancos
                        Dim sCDetalle As String = ""
                        Dim sCCabecera As String = ""
                        Dim iActCheckDet As Integer = 0
                        Dim FechaCheque As DateTime
                        Dim TipoMov As String = ""
                        Dim NroDoc As String = ""
                        Dim Concepto As String = ""
                        Dim Descripcion As String = ""
                        sCDetalle = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value)
                        sCodigoDetPosicion = Trim(sCDetalle)
                        sCCabecera = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value)
                        FechaCheque = Convert.ToDateTime(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column1").Value))
                        TipoMov = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column11").Value)
                        NroDoc = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("NroDocumento2").Value)
                        Concepto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column5").Value)
                        Descripcion = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column4").Value)

                        'DESACTIVA CHEK DEL CHEQUE EN BD
                        iActCheckDet = eLibroBancos.fActMarcaDet(gEmpresa, Trim(sCDetalle), Trim(sCCabecera), dtpFechaCobro.Value, Trim(TipoMov), Trim(NroDoc), Trim(Concepto), Trim(Descripcion), "", 0, 0, 0, 0)
                        If iActCheckDet = 1 Then
                            '--------------
                            Dim dtDatos As DataTable
                            dtDatos = New DataTable
                            dtDatos = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                            Dim dSaldoInicial As Double = 0
                            If dtDatos.Rows.Count > 0 Then
                                dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                                Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                                NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                            End If
                            '--------------
                            'ACTUALIZAR EL SALDO DEL LIBRO
                            Dim dtLibroDet As DataTable
                            dtLibroDet = New DataTable
                            Dim saldo As Double = 0
                            saldo = dSaldoInicial
                            'Dim Cobrados As Double = 0
                            'Dim NoCobrados As Double = 0
                            dtLibroDet = eLibroBancos.TraerLibroDet(37, sCCabecera, gEmpresa)
                            If dtLibroDet.Rows.Count > 0 Then
                                For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                    For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                        If z = 11 Then 'lee el importe
                                            If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                                saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                                Dim iResultadoActDetalleLibro As Int16 = 0
                                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                            ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                                saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                                Dim iResultadoActDetalleLibro As Int16 = 0
                                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                                'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                                'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                                '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                                'End If
                                                'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                                '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                                'End If
                                            End If
                                        End If
                                    Next
                                Next
                                Dim dtDtFech As DataTable
                                dtDtFech = New DataTable
                                Dim Fecha As DateTime
                                Dim FechaActCabecera As DateTime
                                dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCCabecera)
                                If dtDtFech.Rows.Count > 0 Then
                                    Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                                End If
                                If dtpFechaItf.Value > Fecha Then
                                    FechaActCabecera = dtpFechaItf.Value
                                Else
                                    FechaActCabecera = Fecha
                                End If
                                Cobrados = Cobrados + Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                'ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "1" Then
                                NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))

                                iResultadoActSaldoLibroActual = eLibroBancos.fActSaldoLibro(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro

                            End If 'If dtLibroDet.Rows.Count > 0 Then
                        End If 'If iActCheckDet = 1 Then
                    End If
                End If 'If dgvLibroDet.Rows.Count > 0 Then
                If iResultadoActSaldoLibroActual = 1 Then

                    'MessageBox.Show("Se confirm� el cobro del Cheque", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Panel5.Visible = False
                    dtpFechaItf.Value = Now.Date().ToShortDateString
                    txtGlosaItf.Clear()
                    txtImporteItf.Clear()
                    txtTexto_TextChanged_1(sender, e)
                    Panel5.Visible = False
                    Panel3.Visible = False

                    Dim _formInterface As frmLibroBancos = CType(Me.Owner, frmLibroBancos)
                    _formInterface.hace = 1
                    _formInterface.sCodigoDetPosicion = sCodigoDetPosicion
                    Me.Close()

                End If
                'Panel3.Visible = False
                ''Panel4.Visible = False
                'Panel5.Visible = False
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Cursor = c
        End Try


        
    End Sub

    Private Sub frmLibroBancosBuscar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Dim _formInterface As frmLibroBancos = CType(Me.Owner, frmLibroBancos)
            _formInterface.hace = 0
            Me.Close()
        End If
    End Sub

    Private Sub frmLibroBancosBuscar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboCriterio.SelectedIndex = 0
        Timer1.Enabled = True
        dtpFechaCobro.Value = Now.Date().ToShortDateString
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Timer1.Enabled = False
        If Timer1.Enabled = True Then
            txtTexto.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer1.Enabled = False
    End Sub

    Private Sub txtTexto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTexto.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Dim _formInterface As frmLibroBancos = CType(Me.Owner, frmLibroBancos)
            _formInterface.hace = 0
            Me.Close()
        End If
    End Sub

   
    Private Sub txtTexto_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTexto.TextChanged
        Dim dtBusqueda As DataTable
        eLibroBancos = New clsLibroBancos
        dtBusqueda = New DataTable

        If cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCriterio.Focus()
            Exit Sub
        End If

        If Len(txtTexto.Text.Trim) > 0 Then
            If cboCriterio.Text = "NUMERO" Then
                dtBusqueda = eLibroBancos.fConsultaCheque(38, txtTexto.Text.Trim, gEmpresa, sCuentaBuscar)
            ElseIf cboCriterio.Text = "CONCEPTO" Then
                dtBusqueda = eLibroBancos.fConsultaCheque(39, txtTexto.Text.Trim, gEmpresa, sCuentaBuscar)
            ElseIf cboCriterio.Text = "DESCRIPCION" Then
                dtBusqueda = eLibroBancos.fConsultaCheque(40, txtTexto.Text.Trim, gEmpresa, sCuentaBuscar)
                'ElseIf cboCriterio.Text = "CARGO" Then
                'dtBusqueda = eLibroBancos.fConsultaCheque(41, txtTexto.Text.Trim, gEmpresa)
            End If
        End If

        'For x As Integer = 0 To dgvLibroDet.RowCount - 1
        '    dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
        'Next

        dgvLibroDet.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLibroDet.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgvLibroDet.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

        If dgvLibroDet.Rows.Count > 0 Then
            For x As Integer = 0 To dgvLibroDet.RowCount - 1
                dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
            Next
        End If


        'dtBusqueda = eLibroBancos.TraerLibroDet(2, IdLibro, gEmpresa)
        If dtBusqueda.Rows.Count > 0 Then
            For y As Integer = 0 To dtBusqueda.Rows.Count - 1
                dgvLibroDet.Rows.Add()
                For z As Integer = 0 To dtBusqueda.Columns.Count - 1
                    If z = 7 Then ' chek
                        If dtBusqueda.Rows(y).Item(7).ToString = "1" Then
                            dgvLibroDet.Rows(y).Cells(7).Value = True
                            dgvLibroDet.Rows(y).Cells(13).Value = "1"
                            dgvLibroDet.Rows(y).Cells(7).ReadOnly = False
                            If Trim(dtBusqueda.Rows(y).Item(9).ToString) = "00002" Then
                                gestionaResaltados(dgvLibroDet, y, Color.Aqua)
                            End If
                            'dgvLibroDet.Rows(y).Cells(15).ReadOnly = False
                        ElseIf dtBusqueda.Rows(y).Item(7).ToString = "0" Then
                            dgvLibroDet.Rows(y).Cells(7).Value = False
                            dgvLibroDet.Rows(y).Cells(13).Value = "0"
                            dgvLibroDet.Rows(y).Cells(7).ReadOnly = True
                            If Trim(dtBusqueda.Rows(y).Item(9).ToString) = "00002" Then
                                gestionaResaltados(dgvLibroDet, y, Color.Silver)
                            End If
                            'dgvLibroDet.Rows(y).Cells(15).ReadOnly = True
                        End If

                    ElseIf z = 13 Then
                        dgvLibroDet.Rows(y).Cells(12).Value = dtBusqueda.Rows(y).Item(z)
                    ElseIf z = 14 Then
                        dgvLibroDet.Rows(y).Cells(14).Value = dtBusqueda.Rows(y).Item(z)
                    ElseIf z = 0 Then 'fecha
                        dgvLibroDet.Rows(y).Cells(0).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(0), 10)
                    ElseIf z = 11 Then 'importe
                        dgvLibroDet.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                        'If dtBusqueda.Rows(y).Item(10).ToString = "D" Then
                        '    saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                        'ElseIf dtBusqueda.Rows(y).Item(10).ToString = "H" Then
                        '    saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                        '    If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtBusqueda.Rows(y).Item(9).ToString) = "00002" Then
                        '        Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                        '    End If
                        '    If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtBusqueda.Rows(y).Item(9).ToString) = "00002" Then
                        '        NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                        '    End If
                        'End If
                        'dgvLibroDet.Rows(y).Cells(6).Value = Format(saldo, "#,##0.00")
                    ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 8 Or z = 9 Or z = 10 Then '1 2 3 4 5 
                        dgvLibroDet.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                    End If
                Next
            Next
        End If

    End Sub

    Private Sub dtpFechaItf_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaItf.ValueChanged
        'Dim dtDoble As DataTable
        'dtDoble = New DataTable
        'eITF = New clsITF
        'dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpFechaItf.Value)
        'If dtDoble.Rows.Count > 0 Then
        '    PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
        'End If

        'txtITF1.Text = Format(PorcentajeITF, "#,##0.0000")
        'Dim ItfMontoSumado As Double = 0
        'If Len(Trim(txtImporteItf.Text)) = 0 Then
        '    txtImporteItf.Text = "0.00"
        'End If
        ''SumaCheques = Val(txtImporte.Text)
        'ItfMontoSumado = (txtImporteItf.Text * PorcentajeITF) / 100
        ''txtImporITF.Text = Format(ItfMontoSumado, "#,##0.00")
        'txtImporteItf.Text = Format(ItfMontoSumado, "#,##0.00")
        'dtpFechaItf.Focus()




        Panel5.Visible = False
        Panel3.Visible = True

        Dim itfVoF As String = ""
        Dim IdTipoMov As String = ""
        Dim NroCheque As String = ""
        Dim Importe As String = ""
        If dgvLibroDet.Rows.Count > 0 Then
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value) = True Then
                itfVoF = ""
            Else
                itfVoF = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value) = True Then
                IdTipoMov = ""
            Else
                IdTipoMov = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(9).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(14).Value) = True Then
                NroCheque = ""
            Else
                NroCheque = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(14).Value)
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value) = True Then
                Importe = 0
            Else
                Importe = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value)
            End If
        End If

        If IdTipoMov = "00002" Then
            'Panel3.Visible = True
            'dtpFechaItf.Value = Now.Date().ToShortDateString

            Dim dtDoble As DataTable
            dtDoble = New DataTable
            eITF = New clsITF
            dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpFechaItf.Value)
            If dtDoble.Rows.Count > 0 Then
                PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
            End If

            txtGlosaItf.Text = "ITF CHEQ " & NroCheque
            txtITF1.Text = Format(PorcentajeITF, "0.00000")
            'txtImporteItf.Text = Format((Val(txtImporte.Text) * PorcentajeITF) / 100, "0.00")
            'txtImporteItf.Focus()
            Dim ItfMontoSumado As Double = 0
            'ItfMontoSumado = (Val(Importe) * PorcentajeITF) / 100

            Dim MontoParaItf As Double = 0
            MontoParaItf = ReturnMontoParaCalcItf(Importe)
            ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100

            txtImporteItf.Text = Format(ItfMontoSumado, "0.00")
            dtpFechaItf.Focus()
        End If

    End Sub

    Private Sub txtImporteItf_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtImporteItf.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                btnGrabarItf_Click(sender, e)
        End Select
    End Sub

    Private Sub txtImporteItf_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporteItf.TextChanged

    End Sub

    Private Sub dgvLibroDet_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvLibroDet.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Dim _formInterface As frmLibroBancos = CType(Me.Owner, frmLibroBancos)
            _formInterface.hace = 0
            Me.Close()
        End If
    End Sub
End Class