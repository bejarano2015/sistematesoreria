<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMovimientoCajaBanco
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.stsTotales = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.dgvLista = New ctrLibreria.Controles.BeDataGridView
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Abreviado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Direccion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.BeButton1 = New ctrLibreria.Controles.BeButton
        Me.cboPrestamo = New ctrLibreria.Controles.BeButton
        Me.BeButton3 = New ctrLibreria.Controles.BeButton
        Me.BeButton2 = New ctrLibreria.Controles.BeButton
        Me.txtNumVoucher = New ctrLibreria.Controles.BeTextBox
        Me.txtMonto = New ctrLibreria.Controles.BeTextBox
        Me.txtTipoCambio = New ctrLibreria.Controles.BeTextBox
        Me.txtConceptoGasto = New ctrLibreria.Controles.BeTextBox
        Me.txtResponsable = New ctrLibreria.Controles.BeTextBox
        Me.txtNumTransf = New ctrLibreria.Controles.BeTextBox
        Me.ckRendir = New System.Windows.Forms.CheckBox
        Me.BeLabel34 = New ctrLibreria.Controles.BeLabel
        Me.txtNVoucher = New ctrLibreria.Controles.BeLabel
        Me.BeLabel32 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel31 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel30 = New ctrLibreria.Controles.BeLabel
        Me.cboTipoGasto = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel27 = New ctrLibreria.Controles.BeLabel
        Me.cboTipoMov = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel26 = New ctrLibreria.Controles.BeLabel
        Me.cboCentrodeCosto = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel
        Me.cboBanco = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel24 = New ctrLibreria.Controles.BeLabel
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.cboFormaPago = New ctrLibreria.Controles.BeComboBox
        Me.cboRendicion = New ctrLibreria.Controles.BeComboBox
        Me.cboEstadoMovimiento = New ctrLibreria.Controles.BeComboBox
        Me.cboEstado = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.cboCentroCosto = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel
        Me.cboProveedor = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel
        Me.txtMontoSoles = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.cboTipoComprobante = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel
        Me.cboEmpresa = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel
        Me.cboPeriodo = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel
        Me.cboSesion = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel
        Me.txtObservacion = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.txtMontoDolares = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.txtDocumentoOrigen = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.txtDocumentoReferencia = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.cboCheque = New ctrLibreria.Controles.BeComboBox
        Me.dtpFechaMovimiento = New ctrLibreria.Controles.BeDateTimePicker
        Me.txtNombreUsuario = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.txtNumeroCuenta = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.stsTotales.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(883, 646)
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Size = New System.Drawing.Size(875, 620)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Size = New System.Drawing.Size(875, 620)
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsTotales.Location = New System.Drawing.Point(3, 593)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(867, 22)
        Me.stsTotales.TabIndex = 11
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(115, 17)
        Me.ToolStripStatusLabel1.Text = "Total de Registros= 0"
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, Me.Abreviado, Me.Direccion, Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Estado, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column12, Me.Column13, Me.Column14, Me.Column15, Me.Column16, Me.Column17, Me.Column18})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLista.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(1, 1)
        Me.dgvLista.MultiSelect = False
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLista.Size = New System.Drawing.Size(3598, 4204)
        Me.dgvLista.TabIndex = 10
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "IdMovimiento"
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "FechaMovimiento"
        Me.Descripcion.HeaderText = "Fecha del Movimiento"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'Abreviado
        '
        Me.Abreviado.DataPropertyName = "MontoSoles"
        Me.Abreviado.HeaderText = "Monto en Soles"
        Me.Abreviado.Name = "Abreviado"
        Me.Abreviado.ReadOnly = True
        '
        'Direccion
        '
        Me.Direccion.DataPropertyName = "NombreUsuario"
        Me.Direccion.HeaderText = "Nombre de Usuario"
        Me.Direccion.Name = "Direccion"
        Me.Direccion.ReadOnly = True
        Me.Direccion.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "IdCheque"
        Me.Column1.HeaderText = "Id Cheque"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "IdEstadoMovimiento"
        Me.Column2.HeaderText = "Id EstadoMovimiento"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "NumeroCuenta"
        Me.Column3.HeaderText = "Numero de Cuenta"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "IdRendicion"
        Me.Column4.HeaderText = "Id Rendicion"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "IdFormaPago"
        Me.Estado.HeaderText = "Id Forma de Pago"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "DocumentoReferencia"
        Me.Column5.HeaderText = "Documento de Referencia"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "DocumentoOrigen"
        Me.Column6.HeaderText = "Documento de Origen"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "MontoDolares"
        Me.Column7.HeaderText = "Monto en Dolares"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "IdSesion"
        Me.Column8.HeaderText = "Id Sesion"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "Observacion"
        Me.Column9.HeaderText = "Observacion"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "IdPeriodo"
        Me.Column10.HeaderText = "Id Periodo"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "MonCodigo"
        Me.Column11.HeaderText = "Id Codigo"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "EmprCodigo"
        Me.Column12.HeaderText = "Id Empresa"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "TCompCodigo"
        Me.Column13.HeaderText = "Id Tipo Cambio"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "PrvCodigo"
        Me.Column14.HeaderText = "Id Proveedor"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "CCosCodigo"
        Me.Column15.HeaderText = "Id Centro de Costo"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "EdocCodigo"
        Me.Column16.HeaderText = "Estado "
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        '
        'Column17
        '
        Me.Column17.HeaderText = "Column17"
        Me.Column17.Name = "Column17"
        Me.Column17.ReadOnly = True
        '
        'Column18
        '
        Me.Column18.HeaderText = "Column18"
        Me.Column18.Name = "Column18"
        Me.Column18.ReadOnly = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.BeButton1)
        Me.Panel1.Controls.Add(Me.cboPrestamo)
        Me.Panel1.Controls.Add(Me.BeButton3)
        Me.Panel1.Controls.Add(Me.BeButton2)
        Me.Panel1.Controls.Add(Me.txtNumVoucher)
        Me.Panel1.Controls.Add(Me.txtMonto)
        Me.Panel1.Controls.Add(Me.txtTipoCambio)
        Me.Panel1.Controls.Add(Me.txtConceptoGasto)
        Me.Panel1.Controls.Add(Me.txtResponsable)
        Me.Panel1.Controls.Add(Me.txtNumTransf)
        Me.Panel1.Controls.Add(Me.ckRendir)
        Me.Panel1.Controls.Add(Me.BeLabel34)
        Me.Panel1.Controls.Add(Me.txtNVoucher)
        Me.Panel1.Controls.Add(Me.BeLabel32)
        Me.Panel1.Controls.Add(Me.BeLabel31)
        Me.Panel1.Controls.Add(Me.BeLabel30)
        Me.Panel1.Controls.Add(Me.cboTipoGasto)
        Me.Panel1.Controls.Add(Me.BeLabel29)
        Me.Panel1.Controls.Add(Me.BeLabel28)
        Me.Panel1.Controls.Add(Me.BeLabel27)
        Me.Panel1.Controls.Add(Me.cboTipoMov)
        Me.Panel1.Controls.Add(Me.BeLabel26)
        Me.Panel1.Controls.Add(Me.cboCentrodeCosto)
        Me.Panel1.Controls.Add(Me.BeLabel25)
        Me.Panel1.Controls.Add(Me.cboCuenta)
        Me.Panel1.Controls.Add(Me.BeLabel23)
        Me.Panel1.Controls.Add(Me.cboBanco)
        Me.Panel1.Controls.Add(Me.BeLabel24)
        Me.Panel1.Controls.Add(Me.txtCodigo)
        Me.Panel1.Controls.Add(Me.cboFormaPago)
        Me.Panel1.Controls.Add(Me.cboRendicion)
        Me.Panel1.Controls.Add(Me.cboEstadoMovimiento)
        Me.Panel1.Controls.Add(Me.cboEstado)
        Me.Panel1.Controls.Add(Me.BeLabel9)
        Me.Panel1.Controls.Add(Me.cboCentroCosto)
        Me.Panel1.Controls.Add(Me.BeLabel22)
        Me.Panel1.Controls.Add(Me.cboProveedor)
        Me.Panel1.Controls.Add(Me.BeLabel21)
        Me.Panel1.Controls.Add(Me.txtMontoSoles)
        Me.Panel1.Controls.Add(Me.BeLabel3)
        Me.Panel1.Controls.Add(Me.cboTipoComprobante)
        Me.Panel1.Controls.Add(Me.BeLabel20)
        Me.Panel1.Controls.Add(Me.cboEmpresa)
        Me.Panel1.Controls.Add(Me.BeLabel19)
        Me.Panel1.Controls.Add(Me.cboMoneda)
        Me.Panel1.Controls.Add(Me.BeLabel18)
        Me.Panel1.Controls.Add(Me.cboPeriodo)
        Me.Panel1.Controls.Add(Me.BeLabel17)
        Me.Panel1.Controls.Add(Me.cboSesion)
        Me.Panel1.Controls.Add(Me.BeLabel16)
        Me.Panel1.Controls.Add(Me.txtObservacion)
        Me.Panel1.Controls.Add(Me.BeLabel15)
        Me.Panel1.Controls.Add(Me.txtMontoDolares)
        Me.Panel1.Controls.Add(Me.BeLabel14)
        Me.Panel1.Controls.Add(Me.txtDocumentoOrigen)
        Me.Panel1.Controls.Add(Me.BeLabel13)
        Me.Panel1.Controls.Add(Me.txtDocumentoReferencia)
        Me.Panel1.Controls.Add(Me.BeLabel12)
        Me.Panel1.Controls.Add(Me.BeLabel11)
        Me.Panel1.Controls.Add(Me.cboCheque)
        Me.Panel1.Controls.Add(Me.dtpFechaMovimiento)
        Me.Panel1.Controls.Add(Me.txtNombreUsuario)
        Me.Panel1.Controls.Add(Me.BeLabel8)
        Me.Panel1.Controls.Add(Me.txtNumeroCuenta)
        Me.Panel1.Controls.Add(Me.BeLabel7)
        Me.Panel1.Controls.Add(Me.BeLabel6)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Controls.Add(Me.BeLabel5)
        Me.Panel1.Controls.Add(Me.BeLabel2)
        Me.Panel1.Controls.Add(Me.BeLabel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(867, 620)
        Me.Panel1.TabIndex = 3
        '
        'BeButton1
        '
        Me.BeButton1.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton1.Location = New System.Drawing.Point(295, 304)
        Me.BeButton1.Name = "BeButton1"
        Me.BeButton1.Size = New System.Drawing.Size(70, 23)
        Me.BeButton1.TabIndex = 223
        Me.BeButton1.Text = "Seleccione"
        Me.BeButton1.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton1.UseVisualStyleBackColor = True
        '
        'cboPrestamo
        '
        Me.cboPrestamo.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.cboPrestamo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cboPrestamo.Location = New System.Drawing.Point(295, 277)
        Me.cboPrestamo.Name = "cboPrestamo"
        Me.cboPrestamo.Size = New System.Drawing.Size(70, 23)
        Me.cboPrestamo.TabIndex = 222
        Me.cboPrestamo.Text = "Prestamo"
        Me.cboPrestamo.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.cboPrestamo.UseVisualStyleBackColor = True
        '
        'BeButton3
        '
        Me.BeButton3.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton3.Location = New System.Drawing.Point(279, 400)
        Me.BeButton3.Name = "BeButton3"
        Me.BeButton3.Size = New System.Drawing.Size(75, 23)
        Me.BeButton3.TabIndex = 221
        Me.BeButton3.Text = "Quitar"
        Me.BeButton3.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton3.UseVisualStyleBackColor = True
        '
        'BeButton2
        '
        Me.BeButton2.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton2.Location = New System.Drawing.Point(198, 400)
        Me.BeButton2.Name = "BeButton2"
        Me.BeButton2.Size = New System.Drawing.Size(75, 23)
        Me.BeButton2.TabIndex = 220
        Me.BeButton2.Text = "Agregar"
        Me.BeButton2.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton2.UseVisualStyleBackColor = True
        '
        'txtNumVoucher
        '
        Me.txtNumVoucher.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumVoucher.BackColor = System.Drawing.Color.Ivory
        Me.txtNumVoucher.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumVoucher.ForeColor = System.Drawing.Color.Black
        Me.txtNumVoucher.KeyEnter = True
        Me.txtNumVoucher.Location = New System.Drawing.Point(156, 352)
        Me.txtNumVoucher.MaxLength = 20
        Me.txtNumVoucher.Name = "txtNumVoucher"
        Me.txtNumVoucher.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumVoucher.ShortcutsEnabled = False
        Me.txtNumVoucher.Size = New System.Drawing.Size(133, 20)
        Me.txtNumVoucher.TabIndex = 218
        Me.txtNumVoucher.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtMonto
        '
        Me.txtMonto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMonto.BackColor = System.Drawing.Color.Ivory
        Me.txtMonto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto.ForeColor = System.Drawing.Color.Black
        Me.txtMonto.KeyEnter = True
        Me.txtMonto.Location = New System.Drawing.Point(156, 327)
        Me.txtMonto.MaxLength = 20
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMonto.ShortcutsEnabled = False
        Me.txtMonto.Size = New System.Drawing.Size(133, 20)
        Me.txtMonto.TabIndex = 217
        Me.txtMonto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.txtTipoCambio.KeyEnter = True
        Me.txtTipoCambio.Location = New System.Drawing.Point(156, 300)
        Me.txtTipoCambio.MaxLength = 20
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTipoCambio.ShortcutsEnabled = False
        Me.txtTipoCambio.Size = New System.Drawing.Size(133, 20)
        Me.txtTipoCambio.TabIndex = 216
        Me.txtTipoCambio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtConceptoGasto
        '
        Me.txtConceptoGasto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtConceptoGasto.BackColor = System.Drawing.Color.Ivory
        Me.txtConceptoGasto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtConceptoGasto.ForeColor = System.Drawing.Color.Black
        Me.txtConceptoGasto.KeyEnter = True
        Me.txtConceptoGasto.Location = New System.Drawing.Point(156, 274)
        Me.txtConceptoGasto.MaxLength = 20
        Me.txtConceptoGasto.Name = "txtConceptoGasto"
        Me.txtConceptoGasto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtConceptoGasto.ShortcutsEnabled = False
        Me.txtConceptoGasto.Size = New System.Drawing.Size(133, 20)
        Me.txtConceptoGasto.TabIndex = 214
        Me.txtConceptoGasto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtResponsable
        '
        Me.txtResponsable.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtResponsable.BackColor = System.Drawing.Color.Ivory
        Me.txtResponsable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtResponsable.ForeColor = System.Drawing.Color.Black
        Me.txtResponsable.KeyEnter = True
        Me.txtResponsable.Location = New System.Drawing.Point(156, 195)
        Me.txtResponsable.MaxLength = 20
        Me.txtResponsable.Name = "txtResponsable"
        Me.txtResponsable.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtResponsable.ShortcutsEnabled = False
        Me.txtResponsable.Size = New System.Drawing.Size(209, 20)
        Me.txtResponsable.TabIndex = 213
        Me.txtResponsable.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtNumTransf
        '
        Me.txtNumTransf.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumTransf.BackColor = System.Drawing.Color.Ivory
        Me.txtNumTransf.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumTransf.ForeColor = System.Drawing.Color.Black
        Me.txtNumTransf.KeyEnter = True
        Me.txtNumTransf.Location = New System.Drawing.Point(156, 168)
        Me.txtNumTransf.MaxLength = 20
        Me.txtNumTransf.Name = "txtNumTransf"
        Me.txtNumTransf.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumTransf.ShortcutsEnabled = False
        Me.txtNumTransf.Size = New System.Drawing.Size(209, 20)
        Me.txtNumTransf.TabIndex = 212
        Me.txtNumTransf.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'ckRendir
        '
        Me.ckRendir.AutoSize = True
        Me.ckRendir.Location = New System.Drawing.Point(156, 380)
        Me.ckRendir.Name = "ckRendir"
        Me.ckRendir.Size = New System.Drawing.Size(15, 14)
        Me.ckRendir.TabIndex = 211
        Me.ckRendir.UseVisualStyleBackColor = True
        '
        'BeLabel34
        '
        Me.BeLabel34.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel34.AutoSize = True
        Me.BeLabel34.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel34.ForeColor = System.Drawing.Color.White
        Me.BeLabel34.Location = New System.Drawing.Point(18, 376)
        Me.BeLabel34.Name = "BeLabel34"
        Me.BeLabel34.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel34.TabIndex = 210
        Me.BeLabel34.Text = "Cta. a Rendir"
        '
        'txtNVoucher
        '
        Me.txtNVoucher.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.txtNVoucher.AutoSize = True
        Me.txtNVoucher.BackColor = System.Drawing.Color.Transparent
        Me.txtNVoucher.ForeColor = System.Drawing.Color.White
        Me.txtNVoucher.Location = New System.Drawing.Point(18, 351)
        Me.txtNVoucher.Name = "txtNVoucher"
        Me.txtNVoucher.Size = New System.Drawing.Size(77, 13)
        Me.txtNVoucher.TabIndex = 208
        Me.txtNVoucher.Text = "N� de Voucher"
        '
        'BeLabel32
        '
        Me.BeLabel32.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel32.AutoSize = True
        Me.BeLabel32.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel32.ForeColor = System.Drawing.Color.White
        Me.BeLabel32.Location = New System.Drawing.Point(18, 329)
        Me.BeLabel32.Name = "BeLabel32"
        Me.BeLabel32.Size = New System.Drawing.Size(37, 13)
        Me.BeLabel32.TabIndex = 206
        Me.BeLabel32.Text = "Monto"
        '
        'BeLabel31
        '
        Me.BeLabel31.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel31.AutoSize = True
        Me.BeLabel31.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel31.ForeColor = System.Drawing.Color.White
        Me.BeLabel31.Location = New System.Drawing.Point(17, 304)
        Me.BeLabel31.Name = "BeLabel31"
        Me.BeLabel31.Size = New System.Drawing.Size(81, 13)
        Me.BeLabel31.TabIndex = 204
        Me.BeLabel31.Text = "Tipo de Cambio"
        '
        'BeLabel30
        '
        Me.BeLabel30.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel30.AutoSize = True
        Me.BeLabel30.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel30.ForeColor = System.Drawing.Color.White
        Me.BeLabel30.Location = New System.Drawing.Point(16, 277)
        Me.BeLabel30.Name = "BeLabel30"
        Me.BeLabel30.Size = New System.Drawing.Size(99, 13)
        Me.BeLabel30.TabIndex = 202
        Me.BeLabel30.Text = "Concepto de Gasto"
        '
        'cboTipoGasto
        '
        Me.cboTipoGasto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoGasto.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoGasto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoGasto.ForeColor = System.Drawing.Color.Black
        Me.cboTipoGasto.FormattingEnabled = True
        Me.cboTipoGasto.KeyEnter = True
        Me.cboTipoGasto.Location = New System.Drawing.Point(156, 247)
        Me.cboTipoGasto.Name = "cboTipoGasto"
        Me.cboTipoGasto.Size = New System.Drawing.Size(133, 21)
        Me.cboTipoGasto.TabIndex = 199
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.ForeColor = System.Drawing.Color.White
        Me.BeLabel29.Location = New System.Drawing.Point(17, 250)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(74, 13)
        Me.BeLabel29.TabIndex = 200
        Me.BeLabel29.Text = "Tipo de Gasto"
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.ForeColor = System.Drawing.Color.White
        Me.BeLabel28.Location = New System.Drawing.Point(18, 202)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel28.TabIndex = 198
        Me.BeLabel28.Text = "Responsable"
        '
        'BeLabel27
        '
        Me.BeLabel27.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel27.AutoSize = True
        Me.BeLabel27.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel27.ForeColor = System.Drawing.Color.White
        Me.BeLabel27.Location = New System.Drawing.Point(18, 175)
        Me.BeLabel27.Name = "BeLabel27"
        Me.BeLabel27.Size = New System.Drawing.Size(102, 13)
        Me.BeLabel27.TabIndex = 196
        Me.BeLabel27.Text = "N� de Transferencia"
        '
        'cboTipoMov
        '
        Me.cboTipoMov.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoMov.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoMov.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoMov.ForeColor = System.Drawing.Color.Black
        Me.cboTipoMov.FormattingEnabled = True
        Me.cboTipoMov.KeyEnter = True
        Me.cboTipoMov.Location = New System.Drawing.Point(156, 140)
        Me.cboTipoMov.Name = "cboTipoMov"
        Me.cboTipoMov.Size = New System.Drawing.Size(133, 21)
        Me.cboTipoMov.TabIndex = 193
        '
        'BeLabel26
        '
        Me.BeLabel26.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel26.AutoSize = True
        Me.BeLabel26.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel26.ForeColor = System.Drawing.Color.White
        Me.BeLabel26.Location = New System.Drawing.Point(17, 143)
        Me.BeLabel26.Name = "BeLabel26"
        Me.BeLabel26.Size = New System.Drawing.Size(100, 13)
        Me.BeLabel26.TabIndex = 194
        Me.BeLabel26.Text = "Tipo de Movimiento"
        '
        'cboCentrodeCosto
        '
        Me.cboCentrodeCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCentrodeCosto.BackColor = System.Drawing.Color.Ivory
        Me.cboCentrodeCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentrodeCosto.ForeColor = System.Drawing.Color.Black
        Me.cboCentrodeCosto.FormattingEnabled = True
        Me.cboCentrodeCosto.KeyEnter = True
        Me.cboCentrodeCosto.Location = New System.Drawing.Point(156, 113)
        Me.cboCentrodeCosto.Name = "cboCentrodeCosto"
        Me.cboCentrodeCosto.Size = New System.Drawing.Size(133, 21)
        Me.cboCentrodeCosto.TabIndex = 191
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.ForeColor = System.Drawing.Color.White
        Me.BeLabel25.Location = New System.Drawing.Point(17, 116)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(83, 13)
        Me.BeLabel25.TabIndex = 192
        Me.BeLabel25.Text = "Centro de Costo"
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(156, 86)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(133, 21)
        Me.cboCuenta.TabIndex = 186
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.ForeColor = System.Drawing.Color.White
        Me.BeLabel23.Location = New System.Drawing.Point(17, 89)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(101, 13)
        Me.BeLabel23.TabIndex = 188
        Me.BeLabel23.Text = "N� Cuenta Corriente"
        '
        'cboBanco
        '
        Me.cboBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBanco.ForeColor = System.Drawing.Color.Black
        Me.cboBanco.FormattingEnabled = True
        Me.cboBanco.KeyEnter = True
        Me.cboBanco.Location = New System.Drawing.Point(156, 59)
        Me.cboBanco.Name = "cboBanco"
        Me.cboBanco.Size = New System.Drawing.Size(133, 21)
        Me.cboBanco.TabIndex = 185
        '
        'BeLabel24
        '
        Me.BeLabel24.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel24.AutoSize = True
        Me.BeLabel24.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel24.ForeColor = System.Drawing.Color.White
        Me.BeLabel24.Location = New System.Drawing.Point(17, 67)
        Me.BeLabel24.Name = "BeLabel24"
        Me.BeLabel24.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel24.TabIndex = 187
        Me.BeLabel24.Text = "Banco"
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(157, 7)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(133, 20)
        Me.txtCodigo.TabIndex = 184
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboFormaPago
        '
        Me.cboFormaPago.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboFormaPago.BackColor = System.Drawing.Color.Ivory
        Me.cboFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormaPago.ForeColor = System.Drawing.Color.Black
        Me.cboFormaPago.FormattingEnabled = True
        Me.cboFormaPago.KeyEnter = True
        Me.cboFormaPago.Location = New System.Drawing.Point(512, 129)
        Me.cboFormaPago.Name = "cboFormaPago"
        Me.cboFormaPago.Size = New System.Drawing.Size(132, 21)
        Me.cboFormaPago.TabIndex = 6
        '
        'cboRendicion
        '
        Me.cboRendicion.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboRendicion.BackColor = System.Drawing.Color.Ivory
        Me.cboRendicion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRendicion.ForeColor = System.Drawing.Color.Black
        Me.cboRendicion.FormattingEnabled = True
        Me.cboRendicion.KeyEnter = True
        Me.cboRendicion.Location = New System.Drawing.Point(512, 103)
        Me.cboRendicion.Name = "cboRendicion"
        Me.cboRendicion.Size = New System.Drawing.Size(132, 21)
        Me.cboRendicion.TabIndex = 5
        '
        'cboEstadoMovimiento
        '
        Me.cboEstadoMovimiento.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstadoMovimiento.BackColor = System.Drawing.Color.Ivory
        Me.cboEstadoMovimiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstadoMovimiento.ForeColor = System.Drawing.Color.Black
        Me.cboEstadoMovimiento.FormattingEnabled = True
        Me.cboEstadoMovimiento.KeyEnter = True
        Me.cboEstadoMovimiento.Location = New System.Drawing.Point(511, 53)
        Me.cboEstadoMovimiento.Name = "cboEstadoMovimiento"
        Me.cboEstadoMovimiento.Size = New System.Drawing.Size(132, 21)
        Me.cboEstadoMovimiento.TabIndex = 3
        '
        'cboEstado
        '
        Me.cboEstado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstado.BackColor = System.Drawing.Color.Ivory
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.ForeColor = System.Drawing.Color.Black
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.KeyEnter = True
        Me.cboEstado.Location = New System.Drawing.Point(511, 449)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(132, 21)
        Me.cboEstado.TabIndex = 20
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.White
        Me.BeLabel9.Location = New System.Drawing.Point(384, 457)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel9.TabIndex = 183
        Me.BeLabel9.Text = "Estado"
        '
        'cboCentroCosto
        '
        Me.cboCentroCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCentroCosto.BackColor = System.Drawing.Color.Ivory
        Me.cboCentroCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroCosto.ForeColor = System.Drawing.Color.Black
        Me.cboCentroCosto.FormattingEnabled = True
        Me.cboCentroCosto.KeyEnter = True
        Me.cboCentroCosto.Location = New System.Drawing.Point(511, 422)
        Me.cboCentroCosto.Name = "cboCentroCosto"
        Me.cboCentroCosto.Size = New System.Drawing.Size(132, 21)
        Me.cboCentroCosto.TabIndex = 19
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.ForeColor = System.Drawing.Color.White
        Me.BeLabel22.Location = New System.Drawing.Point(381, 430)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(83, 13)
        Me.BeLabel22.TabIndex = 181
        Me.BeLabel22.Text = "Centro de Costo"
        '
        'cboProveedor
        '
        Me.cboProveedor.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboProveedor.BackColor = System.Drawing.Color.Ivory
        Me.cboProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProveedor.ForeColor = System.Drawing.Color.Black
        Me.cboProveedor.FormattingEnabled = True
        Me.cboProveedor.KeyEnter = True
        Me.cboProveedor.Location = New System.Drawing.Point(511, 395)
        Me.cboProveedor.Name = "cboProveedor"
        Me.cboProveedor.Size = New System.Drawing.Size(132, 21)
        Me.cboProveedor.TabIndex = 18
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.ForeColor = System.Drawing.Color.White
        Me.BeLabel21.Location = New System.Drawing.Point(381, 398)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(56, 13)
        Me.BeLabel21.TabIndex = 179
        Me.BeLabel21.Text = "Proveedor"
        '
        'txtMontoSoles
        '
        Me.txtMontoSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMontoSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtMontoSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoSoles.ForeColor = System.Drawing.Color.Black
        Me.txtMontoSoles.KeyEnter = True
        Me.txtMontoSoles.Location = New System.Drawing.Point(512, 210)
        Me.txtMontoSoles.MaxLength = 20
        Me.txtMontoSoles.Name = "txtMontoSoles"
        Me.txtMontoSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMontoSoles.ShortcutsEnabled = False
        Me.txtMontoSoles.Size = New System.Drawing.Size(244, 20)
        Me.txtMontoSoles.TabIndex = 10
        Me.txtMontoSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.White
        Me.BeLabel3.Location = New System.Drawing.Point(376, 213)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(81, 13)
        Me.BeLabel3.TabIndex = 177
        Me.BeLabel3.Text = "Monto en Soles"
        '
        'cboTipoComprobante
        '
        Me.cboTipoComprobante.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoComprobante.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoComprobante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoComprobante.ForeColor = System.Drawing.Color.Black
        Me.cboTipoComprobante.FormattingEnabled = True
        Me.cboTipoComprobante.KeyEnter = True
        Me.cboTipoComprobante.Location = New System.Drawing.Point(511, 368)
        Me.cboTipoComprobante.Name = "cboTipoComprobante"
        Me.cboTipoComprobante.Size = New System.Drawing.Size(132, 21)
        Me.cboTipoComprobante.TabIndex = 17
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.ForeColor = System.Drawing.Color.White
        Me.BeLabel20.Location = New System.Drawing.Point(380, 371)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(109, 13)
        Me.BeLabel20.TabIndex = 175
        Me.BeLabel20.Text = "Tipo de Comprobante"
        '
        'cboEmpresa
        '
        Me.cboEmpresa.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEmpresa.BackColor = System.Drawing.Color.Ivory
        Me.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpresa.ForeColor = System.Drawing.Color.Black
        Me.cboEmpresa.FormattingEnabled = True
        Me.cboEmpresa.KeyEnter = True
        Me.cboEmpresa.Location = New System.Drawing.Point(157, 33)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(133, 21)
        Me.cboEmpresa.TabIndex = 16
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.ForeColor = System.Drawing.Color.White
        Me.BeLabel19.Location = New System.Drawing.Point(17, 41)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel19.TabIndex = 173
        Me.BeLabel19.Text = "Empresa"
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(512, 341)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(133, 21)
        Me.cboMoneda.TabIndex = 15
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.White
        Me.BeLabel18.Location = New System.Drawing.Point(378, 349)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel18.TabIndex = 171
        Me.BeLabel18.Text = "Moneda"
        '
        'cboPeriodo
        '
        Me.cboPeriodo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPeriodo.BackColor = System.Drawing.Color.Ivory
        Me.cboPeriodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodo.ForeColor = System.Drawing.Color.Black
        Me.cboPeriodo.FormattingEnabled = True
        Me.cboPeriodo.KeyEnter = True
        Me.cboPeriodo.Location = New System.Drawing.Point(512, 314)
        Me.cboPeriodo.Name = "cboPeriodo"
        Me.cboPeriodo.Size = New System.Drawing.Size(133, 21)
        Me.cboPeriodo.TabIndex = 14
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.White
        Me.BeLabel17.Location = New System.Drawing.Point(378, 317)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(43, 13)
        Me.BeLabel17.TabIndex = 169
        Me.BeLabel17.Text = "Periodo"
        '
        'cboSesion
        '
        Me.cboSesion.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboSesion.BackColor = System.Drawing.Color.Ivory
        Me.cboSesion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSesion.ForeColor = System.Drawing.Color.Black
        Me.cboSesion.FormattingEnabled = True
        Me.cboSesion.KeyEnter = True
        Me.cboSesion.Location = New System.Drawing.Point(512, 287)
        Me.cboSesion.Name = "cboSesion"
        Me.cboSesion.Size = New System.Drawing.Size(133, 21)
        Me.cboSesion.TabIndex = 12
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.White
        Me.BeLabel16.Location = New System.Drawing.Point(376, 290)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(39, 13)
        Me.BeLabel16.TabIndex = 167
        Me.BeLabel16.Text = "Sesion"
        '
        'txtObservacion
        '
        Me.txtObservacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtObservacion.BackColor = System.Drawing.Color.Ivory
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.ForeColor = System.Drawing.Color.Black
        Me.txtObservacion.KeyEnter = True
        Me.txtObservacion.Location = New System.Drawing.Point(511, 262)
        Me.txtObservacion.MaxLength = 1000
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtObservacion.ShortcutsEnabled = False
        Me.txtObservacion.Size = New System.Drawing.Size(333, 20)
        Me.txtObservacion.TabIndex = 13
        Me.txtObservacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.White
        Me.BeLabel15.Location = New System.Drawing.Point(377, 265)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel15.TabIndex = 165
        Me.BeLabel15.Text = "Observacion"
        '
        'txtMontoDolares
        '
        Me.txtMontoDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMontoDolares.BackColor = System.Drawing.Color.Ivory
        Me.txtMontoDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoDolares.ForeColor = System.Drawing.Color.Black
        Me.txtMontoDolares.KeyEnter = True
        Me.txtMontoDolares.Location = New System.Drawing.Point(512, 236)
        Me.txtMontoDolares.MaxLength = 20
        Me.txtMontoDolares.Name = "txtMontoDolares"
        Me.txtMontoDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMontoDolares.ShortcutsEnabled = False
        Me.txtMontoDolares.Size = New System.Drawing.Size(244, 20)
        Me.txtMontoDolares.TabIndex = 11
        Me.txtMontoDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.White
        Me.BeLabel14.Location = New System.Drawing.Point(377, 239)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(76, 13)
        Me.BeLabel14.TabIndex = 163
        Me.BeLabel14.Text = "Monto Dolares"
        '
        'txtDocumentoOrigen
        '
        Me.txtDocumentoOrigen.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDocumentoOrigen.BackColor = System.Drawing.Color.Ivory
        Me.txtDocumentoOrigen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocumentoOrigen.ForeColor = System.Drawing.Color.Black
        Me.txtDocumentoOrigen.KeyEnter = True
        Me.txtDocumentoOrigen.Location = New System.Drawing.Point(511, 184)
        Me.txtDocumentoOrigen.MaxLength = 20
        Me.txtDocumentoOrigen.Name = "txtDocumentoOrigen"
        Me.txtDocumentoOrigen.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDocumentoOrigen.ShortcutsEnabled = False
        Me.txtDocumentoOrigen.Size = New System.Drawing.Size(333, 20)
        Me.txtDocumentoOrigen.TabIndex = 9
        Me.txtDocumentoOrigen.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.White
        Me.BeLabel13.Location = New System.Drawing.Point(377, 187)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(96, 13)
        Me.BeLabel13.TabIndex = 161
        Me.BeLabel13.Text = "Documento Origen"
        '
        'txtDocumentoReferencia
        '
        Me.txtDocumentoReferencia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDocumentoReferencia.BackColor = System.Drawing.Color.Ivory
        Me.txtDocumentoReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDocumentoReferencia.ForeColor = System.Drawing.Color.Black
        Me.txtDocumentoReferencia.KeyEnter = True
        Me.txtDocumentoReferencia.Location = New System.Drawing.Point(511, 158)
        Me.txtDocumentoReferencia.MaxLength = 20
        Me.txtDocumentoReferencia.Name = "txtDocumentoReferencia"
        Me.txtDocumentoReferencia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDocumentoReferencia.ShortcutsEnabled = False
        Me.txtDocumentoReferencia.Size = New System.Drawing.Size(235, 20)
        Me.txtDocumentoReferencia.TabIndex = 8
        Me.txtDocumentoReferencia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.White
        Me.BeLabel12.Location = New System.Drawing.Point(377, 161)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(132, 13)
        Me.BeLabel12.TabIndex = 159
        Me.BeLabel12.Text = "Documento de Referencia"
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.White
        Me.BeLabel11.Location = New System.Drawing.Point(377, 137)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(79, 13)
        Me.BeLabel11.TabIndex = 157
        Me.BeLabel11.Text = "Forma de Pago"
        '
        'cboCheque
        '
        Me.cboCheque.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCheque.BackColor = System.Drawing.Color.Ivory
        Me.cboCheque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheque.ForeColor = System.Drawing.Color.Black
        Me.cboCheque.FormattingEnabled = True
        Me.cboCheque.KeyEnter = True
        Me.cboCheque.Location = New System.Drawing.Point(511, 31)
        Me.cboCheque.Name = "cboCheque"
        Me.cboCheque.Size = New System.Drawing.Size(133, 21)
        Me.cboCheque.TabIndex = 2
        '
        'dtpFechaMovimiento
        '
        Me.dtpFechaMovimiento.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaMovimiento.CustomFormat = ""
        Me.dtpFechaMovimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaMovimiento.KeyEnter = True
        Me.dtpFechaMovimiento.Location = New System.Drawing.Point(156, 221)
        Me.dtpFechaMovimiento.Name = "dtpFechaMovimiento"
        Me.dtpFechaMovimiento.Size = New System.Drawing.Size(132, 20)
        Me.dtpFechaMovimiento.TabIndex = 0
        Me.dtpFechaMovimiento.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaMovimiento.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'txtNombreUsuario
        '
        Me.txtNombreUsuario.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNombreUsuario.BackColor = System.Drawing.Color.Ivory
        Me.txtNombreUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombreUsuario.ForeColor = System.Drawing.Color.Black
        Me.txtNombreUsuario.KeyEnter = True
        Me.txtNombreUsuario.Location = New System.Drawing.Point(511, 7)
        Me.txtNombreUsuario.MaxLength = 100
        Me.txtNombreUsuario.Name = "txtNombreUsuario"
        Me.txtNombreUsuario.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNombreUsuario.ShortcutsEnabled = False
        Me.txtNombreUsuario.Size = New System.Drawing.Size(333, 20)
        Me.txtNombreUsuario.TabIndex = 1
        Me.txtNombreUsuario.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.White
        Me.BeLabel8.Location = New System.Drawing.Point(377, 111)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel8.TabIndex = 149
        Me.BeLabel8.Text = "Rendicion"
        '
        'txtNumeroCuenta
        '
        Me.txtNumeroCuenta.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumeroCuenta.BackColor = System.Drawing.Color.Ivory
        Me.txtNumeroCuenta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroCuenta.ForeColor = System.Drawing.Color.Black
        Me.txtNumeroCuenta.KeyEnter = True
        Me.txtNumeroCuenta.Location = New System.Drawing.Point(512, 78)
        Me.txtNumeroCuenta.MaxLength = 20
        Me.txtNumeroCuenta.Name = "txtNumeroCuenta"
        Me.txtNumeroCuenta.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumeroCuenta.ShortcutsEnabled = False
        Me.txtNumeroCuenta.Size = New System.Drawing.Size(244, 20)
        Me.txtNumeroCuenta.TabIndex = 4
        Me.txtNumeroCuenta.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.White
        Me.BeLabel7.Location = New System.Drawing.Point(377, 85)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(96, 13)
        Me.BeLabel7.TabIndex = 147
        Me.BeLabel7.Text = "Numero de Cuenta"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.White
        Me.BeLabel6.Location = New System.Drawing.Point(376, 61)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(97, 13)
        Me.BeLabel6.TabIndex = 145
        Me.BeLabel6.Text = "Estado Movimiento"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.White
        Me.BeLabel4.Location = New System.Drawing.Point(377, 39)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(44, 13)
        Me.BeLabel4.TabIndex = 143
        Me.BeLabel4.Text = "Cheque"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.White
        Me.BeLabel5.Location = New System.Drawing.Point(377, 14)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(98, 13)
        Me.BeLabel5.TabIndex = 141
        Me.BeLabel5.Text = "Nombre de Usuario"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.White
        Me.BeLabel2.Location = New System.Drawing.Point(16, 228)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(109, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Fecha de Movimiento"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.White
        Me.BeLabel1.Location = New System.Drawing.Point(18, 14)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Codigo"
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'frmMovimientoCajaBanco
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(883, 646)
        Me.Name = "frmMovimientoCajaBanco"
        Me.Text = "Cuenta Movimiento Caja Banco"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents dgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtNombreUsuario As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumeroCuenta As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Abreviado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Direccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dtpFechaMovimiento As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents cboCheque As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtDocumentoOrigen As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtDocumentoReferencia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboTipoComprobante As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEmpresa As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboPeriodo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtObservacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtMontoDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtMontoSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCentroCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboProveedor As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstadoMovimiento As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboSesion As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents cboRendicion As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboFormaPago As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboTipoGasto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel27 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboTipoMov As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel26 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCentrodeCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel24 As ctrLibreria.Controles.BeLabel
    Friend WithEvents ckRendir As System.Windows.Forms.CheckBox
    Friend WithEvents BeLabel34 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNVoucher As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel32 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel31 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel30 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtResponsable As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNumTransf As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNumVoucher As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtMonto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTipoCambio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtConceptoGasto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeButton3 As ctrLibreria.Controles.BeButton
    Friend WithEvents BeButton2 As ctrLibreria.Controles.BeButton
    Friend WithEvents BeButton1 As ctrLibreria.Controles.BeButton
    Friend WithEvents cboPrestamo As ctrLibreria.Controles.BeButton

End Class
