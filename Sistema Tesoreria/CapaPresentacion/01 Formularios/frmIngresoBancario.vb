Option Explicit On
Imports System.Xml
Imports MSXML2

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad
Imports System.Math

Public Class frmIngresoBancario

    Private eGastosGenerales As clsGastosGenerales
    Private eIngresoBancario As clsIngresoBancario

    Dim odtIngresoBancario As New DataTable
    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared decImporte As Decimal

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eGastosGenerales = New clsGastosGenerales
        eIngresoBancario = New clsIngresoBancario

        dtpFechaDeposito.Value = Date.Today
        InitCombos()
        odtIngresoBancario = eIngresoBancario.fCargarIngresoBancario(txtCodigoCajaExtraordinaria.Text, txtCodigoEmpresa.Text)
        If odtIngresoBancario.Rows.Count > 0 Then
            dtpFechaDeposito.Value = odtIngresoBancario.Rows(0).Item("FechaDeposito")
            cboBanco.SelectedValue = Convert.ToString(odtIngresoBancario.Rows(0).Item("IdBanco"))
            cboCuenta.SelectedValue = Convert.ToString(odtIngresoBancario.Rows(0).Item("IdCtaCorriente"))
            txtNroOperacion.Text = odtIngresoBancario.Rows(0).Item("NroOperacion")
            txtImporte.Text = odtIngresoBancario.Rows(0).Item("Importe")
            txtGlosa.Text = Convert.ToString(odtIngresoBancario.Rows(0).Item("Glosa"))
        End If
    End Sub

    Private Sub InitCombos()
        cboBanco.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboBanco.ValueMember = "IdBanco"
            cboBanco.DisplayMember = "NombreBanco"
            cboBanco.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboBanco_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBanco.SelectedIndexChanged
        Dim IdBanco As String = ""
        Dim dtCuenta As New DataTable

        If (cboBanco.SelectedIndex > -1) Then
            Try
                IdBanco = cboBanco.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    dtCuenta = eGastosGenerales.fListarCuentasxBancoyEmpresa2(gEmpresa, IdBanco)

                    cboCuenta.DataSource = dtCuenta
                    If dtCuenta.Rows.Count > 0 Then
                        cboCuenta.ValueMember = "IdCuenta"
                        cboCuenta.DisplayMember = "NumeroCuenta"
                        cboCuenta.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboCuenta.DataSource = Nothing
        End If
    End Sub

    Private Sub cboCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCuenta.SelectedIndexChanged
        Dim IdCuenta As String
        Dim dtCodMoneda As DataTable
        Dim sDescripcionMoneda As String

        If (cboCuenta.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuenta.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtCodMoneda = New DataTable
                    dtCodMoneda = eGastosGenerales.fTraerCodMonedaCuenta(IdCuenta, gEmpresa)
                    If dtCodMoneda.Rows.Count > 0 Then
                        sDescripcionMoneda = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonDescripcion")))
                        'sMonCodigo = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonCodigo")))
                        txtCodMoneda.Text = Trim(sDescripcionMoneda)
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            txtCodMoneda.Clear()
        End If
        'cboTipoPago.SelectedIndex = -1
        txtNroOperacion.Text = ""
        txtImporte.Text = ""
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        Dim iResultado As Int32

        'Se valida los campos
        If cboBanco.SelectedIndex = -1 Then MessageBox.Show("Seleccione un banco.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : cboBanco.Focus() : Exit Sub
        If cboCuenta.SelectedIndex = -1 Then MessageBox.Show("Seleccione una cuenta bancaria.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : cboCuenta.Focus() : Exit Sub
        If txtNroOperacion.Text.Trim.Length = 0 Then MessageBox.Show("El n�mero de operaci�n es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtNroOperacion.Focus() : Exit Sub
        If txtImporte.Text.Trim.Length = 0 Then MessageBox.Show("El importe es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtNroOperacion.Focus() : Exit Sub

        iResultado = eIngresoBancario.fGrabar(txtCodigoCajaExtraordinaria.Text, txtCodigoEmpresa.Text, Convert.ToDateTime(dtpFechaDeposito.Value), Convert.ToString(cboBanco.SelectedValue), Convert.ToString(cboCuenta.SelectedValue), txtNroOperacion.Text.Trim, txtImporte.Text.Trim, txtGlosa.Text.Trim, 1)

        decImporte = Round(CDec(txtImporte.Text), 2, MidpointRounding.ToEven)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

End Class