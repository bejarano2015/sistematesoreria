<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporProgramacionPago
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.cboEmpresa = New Telerik.WinControls.UI.RadDropDownList()
        Me.dtFechaInicio = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.radLabel6 = New Telerik.WinControls.UI.RadLabel()
        Me.dtFechaFin = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.radLabel8 = New Telerik.WinControls.UI.RadLabel()
        Me.btnGenerar = New Telerik.WinControls.UI.RadButton()
        Me.pnCabecera = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.rvReporte = New Telerik.ReportViewer.WinForms.ReportViewer()
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel()
        Me.txtTipoCambio = New System.Windows.Forms.TextBox()
        CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.radGroupBox1.SuspendLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFechaInicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGenerar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnCabecera.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'radGroupBox1
        '
        Me.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.radGroupBox1.Controls.Add(Me.RadLabel1)
        Me.radGroupBox1.Controls.Add(Me.cboEmpresa)
        Me.radGroupBox1.Controls.Add(Me.dtFechaInicio)
        Me.radGroupBox1.Controls.Add(Me.radLabel6)
        Me.radGroupBox1.Controls.Add(Me.dtFechaFin)
        Me.radGroupBox1.Controls.Add(Me.radLabel8)
        Me.radGroupBox1.HeaderText = ""
        Me.radGroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.radGroupBox1.Name = "radGroupBox1"
        Me.radGroupBox1.Size = New System.Drawing.Size(600, 38)
        Me.radGroupBox1.TabIndex = 40
        '
        'RadLabel1
        '
        Me.RadLabel1.Location = New System.Drawing.Point(5, 10)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(49, 18)
        Me.RadLabel1.TabIndex = 181
        Me.RadLabel1.Text = "Empresa"
        '
        'cboEmpresa
        '
        Me.cboEmpresa.AutoCompleteDisplayMember = "EmprDescripcion"
        Me.cboEmpresa.AutoCompleteValueMember = "EmprCodigo"
        Me.cboEmpresa.DisplayMember = "EmprDescripcion"
        Me.cboEmpresa.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboEmpresa.Location = New System.Drawing.Point(76, 10)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(268, 20)
        Me.cboEmpresa.TabIndex = 180
        Me.cboEmpresa.ValueMember = "EmprCodigo"
        '
        'dtFechaInicio
        '
        Me.dtFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaInicio.Location = New System.Drawing.Point(379, 10)
        Me.dtFechaInicio.Name = "dtFechaInicio"
        Me.dtFechaInicio.Size = New System.Drawing.Size(94, 20)
        Me.dtFechaInicio.TabIndex = 31
        Me.dtFechaInicio.TabStop = False
        Me.dtFechaInicio.Text = "20/08/2014"
        Me.dtFechaInicio.Value = New Date(2014, 8, 20, 17, 32, 6, 60)
        '
        'radLabel6
        '
        Me.radLabel6.Location = New System.Drawing.Point(476, 8)
        Me.radLabel6.Name = "radLabel6"
        Me.radLabel6.Size = New System.Drawing.Size(15, 18)
        Me.radLabel6.TabIndex = 34
        Me.radLabel6.Text = "al"
        '
        'dtFechaFin
        '
        Me.dtFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaFin.Location = New System.Drawing.Point(492, 10)
        Me.dtFechaFin.Name = "dtFechaFin"
        Me.dtFechaFin.Size = New System.Drawing.Size(94, 20)
        Me.dtFechaFin.TabIndex = 32
        Me.dtFechaFin.TabStop = False
        Me.dtFechaFin.Text = "20/08/2014"
        Me.dtFechaFin.Value = New Date(2014, 8, 20, 17, 32, 6, 60)
        '
        'radLabel8
        '
        Me.radLabel8.Location = New System.Drawing.Point(350, 8)
        Me.radLabel8.Name = "radLabel8"
        Me.radLabel8.Size = New System.Drawing.Size(23, 18)
        Me.radLabel8.TabIndex = 33
        Me.radLabel8.Text = "Del"
        '
        'btnGenerar
        '
        Me.btnGenerar.Location = New System.Drawing.Point(756, 17)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(66, 25)
        Me.btnGenerar.TabIndex = 41
        Me.btnGenerar.Text = "Generar"
        '
        'pnCabecera
        '
        Me.pnCabecera.Controls.Add(Me.BeLabel18)
        Me.pnCabecera.Controls.Add(Me.txtTipoCambio)
        Me.pnCabecera.Controls.Add(Me.radGroupBox1)
        Me.pnCabecera.Controls.Add(Me.btnGenerar)
        Me.pnCabecera.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnCabecera.Location = New System.Drawing.Point(0, 0)
        Me.pnCabecera.Name = "pnCabecera"
        Me.pnCabecera.Size = New System.Drawing.Size(837, 59)
        Me.pnCabecera.TabIndex = 42
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.rvReporte)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 59)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(837, 537)
        Me.Panel1.TabIndex = 43
        '
        'rvReporte
        '
        Me.rvReporte.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rvReporte.Location = New System.Drawing.Point(0, 0)
        Me.rvReporte.Name = "rvReporte"
        Me.rvReporte.Size = New System.Drawing.Size(837, 537)
        Me.rvReporte.TabIndex = 1
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(618, 29)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel18.TabIndex = 64
        Me.BeLabel18.Text = "T.C Especial"
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoCambio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTipoCambio.Location = New System.Drawing.Point(691, 22)
        Me.txtTipoCambio.MaxLength = 11
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.Size = New System.Drawing.Size(48, 20)
        Me.txtTipoCambio.TabIndex = 63
        Me.txtTipoCambio.Text = "0.00"
        '
        'frmReporProgramacionPago
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(837, 596)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnCabecera)
        Me.Name = "frmReporProgramacionPago"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte de Gestion de Documentos - Pagos"
        CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.radGroupBox1.ResumeLayout(False)
        Me.radGroupBox1.PerformLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFechaInicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFechaFin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGenerar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnCabecera.ResumeLayout(False)
        Me.pnCabecera.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Private WithEvents dtFechaInicio As Telerik.WinControls.UI.RadDateTimePicker
    Private WithEvents radLabel6 As Telerik.WinControls.UI.RadLabel
    Private WithEvents dtFechaFin As Telerik.WinControls.UI.RadDateTimePicker
    Private WithEvents radLabel8 As Telerik.WinControls.UI.RadLabel
    Private WithEvents btnGenerar As Telerik.WinControls.UI.RadButton
    Private WithEvents cboEmpresa As Telerik.WinControls.UI.RadDropDownList
    Private WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents pnCabecera As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents rvReporte As Telerik.ReportViewer.WinForms.ReportViewer
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTipoCambio As System.Windows.Forms.TextBox
End Class
