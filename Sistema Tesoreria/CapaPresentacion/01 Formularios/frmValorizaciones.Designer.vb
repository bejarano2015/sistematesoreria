<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValorizaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle39 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle40 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle41 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle42 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle43 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle44 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle45 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle46 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle47 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle48 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle49 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle50 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle51 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle52 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle53 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle54 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmValorizaciones))
        Dim DataGridViewCellStyle55 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle56 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle57 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Und = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.M1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.M2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Parcial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AA1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AA2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AA3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.V1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.V2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.V3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.vapor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.vametrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MONTacumulado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdPartidaDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CCosto = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel52 = New ctrLibreria.Controles.BeLabel()
        Me.cboContratista = New ctrLibreria.Controles.BeComboBox()
        Me.txtUbicacion = New ctrLibreria.Controles.BeTextBox()
        Me.cboPartidas = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroContrato = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel()
        Me.cboObra = New ctrLibreria.Controles.BeComboBox()
        Me.txtNumeroOrden = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnVerAvanceSemanal = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.dtpEnvio = New ctrLibreria.Controles.BeDateTimePicker()
        Me.chkCerrar = New System.Windows.Forms.CheckBox()
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel()
        Me.txtCodigoVal = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.txtSemana = New ctrLibreria.Controles.BeTextBox()
        Me.txtNumSesion = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker()
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimirSunat = New System.Windows.Forms.ToolStripButton()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.dgvDescuentos = New System.Windows.Forms.DataGridView()
        Me.Des_Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_Anterior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_Actual = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_Acumulado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_IdDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Des_Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BeLabel44 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel43 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel42 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel31 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel()
        Me.txtCuadrillaTotales = New ctrLibreria.Controles.BeTextBox()
        Me.txtCuadrilla3T = New ctrLibreria.Controles.BeTextBox()
        Me.txtCuadrilla2T = New ctrLibreria.Controles.BeTextBox()
        Me.txtCuadrilla1T = New ctrLibreria.Controles.BeTextBox()
        Me.txtCuadrilla3Pre = New ctrLibreria.Controles.BeTextBox()
        Me.txtCuadrilla2Pre = New ctrLibreria.Controles.BeTextBox()
        Me.txtCuadrilla1Pre = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel27 = New ctrLibreria.Controles.BeLabel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.txtTotDes = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel26 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel39 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel35 = New ctrLibreria.Controles.BeLabel()
        Me.txtAdelanto1 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel34 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel40 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel36 = New ctrLibreria.Controles.BeLabel()
        Me.txtAmorti1 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel33 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel41 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel37 = New ctrLibreria.Controles.BeLabel()
        Me.txtAmorti2 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel32 = New ctrLibreria.Controles.BeLabel()
        Me.txtCuadrilla4 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel38 = New ctrLibreria.Controles.BeLabel()
        Me.txtAdelanto2 = New ctrLibreria.Controles.BeTextBox()
        Me.txtCuadrilla3 = New ctrLibreria.Controles.BeTextBox()
        Me.txtFG1 = New ctrLibreria.Controles.BeTextBox()
        Me.txtAdelanto3 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel30 = New ctrLibreria.Controles.BeLabel()
        Me.txtCuadrilla2 = New ctrLibreria.Controles.BeTextBox()
        Me.txtAmorti3 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel()
        Me.txtCuadrilla1 = New ctrLibreria.Controles.BeTextBox()
        Me.txtAmorti4 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel()
        Me.txtFG4 = New ctrLibreria.Controles.BeTextBox()
        Me.txtFG2 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.txtFG3 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel45 = New ctrLibreria.Controles.BeLabel()
        Me.txtAcumuladoIGV = New ctrLibreria.Controles.BeTextBox()
        Me.txtAcumuladoTotal = New ctrLibreria.Controles.BeTextBox()
        Me.txtAcumuladoSubtotal = New ctrLibreria.Controles.BeTextBox()
        Me.AxMonthView1 = New AxMSComCtl2.AxMonthView()
        Me.BeLabel24 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.txtTotPagar = New ctrLibreria.Controles.BeTextBox()
        Me.txtAdelanto = New ctrLibreria.Controles.BeTextBox()
        Me.txtOtroDes = New ctrLibreria.Controles.BeTextBox()
        Me.txtRetPor = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel()
        Me.txtSubTotalIGV = New ctrLibreria.Controles.BeTextBox()
        Me.txtSubTotalTot = New ctrLibreria.Controles.BeTextBox()
        Me.txtSubTotalSaldo = New ctrLibreria.Controles.BeTextBox()
        Me.txtSubIGVVal = New ctrLibreria.Controles.BeTextBox()
        Me.txtTotalVal = New ctrLibreria.Controles.BeTextBox()
        Me.txtSubTotalVal = New ctrLibreria.Controles.BeTextBox()
        Me.txtAA2 = New ctrLibreria.Controles.BeTextBox()
        Me.txtAA3 = New ctrLibreria.Controles.BeTextBox()
        Me.txtAA1 = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel()
        Me.txtTotal = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel()
        Me.txtIGV = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.txtSubTotal = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel47 = New ctrLibreria.Controles.BeLabel()
        Me.txtBaseSaldoActaR = New ctrLibreria.Controles.BeTextBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.BeLabel49 = New ctrLibreria.Controles.BeLabel()
        Me.txtTotalFactura = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel48 = New ctrLibreria.Controles.BeLabel()
        Me.txtIGVFactura = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel46 = New ctrLibreria.Controles.BeLabel()
        Me.txtSubTotalFactura = New ctrLibreria.Controles.BeTextBox()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgvDescuentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.Cant, Me.Und, Me.M1, Me.M2, Me.PU, Me.Parcial, Me.AA1, Me.AA2, Me.AA3, Me.V1, Me.V2, Me.V3, Me.vapor, Me.vametrado, Me.MONTacumulado, Me.S1, Me.S2, Me.S3, Me.idDetalle, Me.Estado, Me.Column1, Me.Column2, Me.Column3, Me.IdPartidaDet})
        Me.dgvDetalle.Location = New System.Drawing.Point(10, 210)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(1343, 156)
        Me.dgvDetalle.TabIndex = 7
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        DataGridViewCellStyle39.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle39
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 310
        '
        'Cant
        '
        Me.Cant.DataPropertyName = "Cantidad"
        DataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle40.Format = "N2"
        DataGridViewCellStyle40.NullValue = Nothing
        Me.Cant.DefaultCellStyle = DataGridViewCellStyle40
        Me.Cant.HeaderText = "Cant"
        Me.Cant.Name = "Cant"
        Me.Cant.ReadOnly = True
        Me.Cant.Visible = False
        Me.Cant.Width = 47
        '
        'Und
        '
        Me.Und.DataPropertyName = "UnidMed"
        DataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle41.BackColor = System.Drawing.Color.Moccasin
        DataGridViewCellStyle41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Und.DefaultCellStyle = DataGridViewCellStyle41
        Me.Und.HeaderText = "Und"
        Me.Und.Name = "Und"
        Me.Und.ReadOnly = True
        Me.Und.Width = 47
        '
        'M1
        '
        Me.M1.DataPropertyName = "MetradoPor"
        DataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle42.BackColor = System.Drawing.Color.Moccasin
        DataGridViewCellStyle42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle42.Format = "N1"
        DataGridViewCellStyle42.NullValue = Nothing
        Me.M1.DefaultCellStyle = DataGridViewCellStyle42
        Me.M1.HeaderText = "% M"
        Me.M1.Name = "M1"
        Me.M1.ReadOnly = True
        Me.M1.Width = 55
        '
        'M2
        '
        Me.M2.DataPropertyName = "MetradoImpor"
        DataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle43.BackColor = System.Drawing.Color.Moccasin
        DataGridViewCellStyle43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle43.Format = "N2"
        DataGridViewCellStyle43.NullValue = Nothing
        Me.M2.DefaultCellStyle = DataGridViewCellStyle43
        Me.M2.HeaderText = "Metrado"
        Me.M2.Name = "M2"
        Me.M2.ReadOnly = True
        Me.M2.Width = 60
        '
        'PU
        '
        Me.PU.DataPropertyName = "60"
        DataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle44.BackColor = System.Drawing.Color.Moccasin
        DataGridViewCellStyle44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle44.Format = "N2"
        DataGridViewCellStyle44.NullValue = Nothing
        Me.PU.DefaultCellStyle = DataGridViewCellStyle44
        Me.PU.HeaderText = "PMO Int."
        Me.PU.Name = "PU"
        Me.PU.ReadOnly = True
        Me.PU.Width = 72
        '
        'Parcial
        '
        Me.Parcial.DataPropertyName = "60"
        DataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle45.BackColor = System.Drawing.Color.Moccasin
        DataGridViewCellStyle45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle45.Format = "N2"
        DataGridViewCellStyle45.NullValue = Nothing
        Me.Parcial.DefaultCellStyle = DataGridViewCellStyle45
        Me.Parcial.HeaderText = "Parcial"
        Me.Parcial.Name = "Parcial"
        Me.Parcial.ReadOnly = True
        Me.Parcial.Width = 60
        '
        'AA1
        '
        DataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle46.BackColor = System.Drawing.Color.Honeydew
        DataGridViewCellStyle46.Format = "N1"
        DataGridViewCellStyle46.NullValue = Nothing
        Me.AA1.DefaultCellStyle = DataGridViewCellStyle46
        Me.AA1.HeaderText = "% M"
        Me.AA1.Name = "AA1"
        Me.AA1.ReadOnly = True
        Me.AA1.Width = 55
        '
        'AA2
        '
        DataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle47.BackColor = System.Drawing.Color.Honeydew
        Me.AA2.DefaultCellStyle = DataGridViewCellStyle47
        Me.AA2.HeaderText = "Metrado"
        Me.AA2.Name = "AA2"
        Me.AA2.ReadOnly = True
        Me.AA2.Width = 60
        '
        'AA3
        '
        DataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle48.BackColor = System.Drawing.Color.Honeydew
        Me.AA3.DefaultCellStyle = DataGridViewCellStyle48
        Me.AA3.HeaderText = "Monto"
        Me.AA3.Name = "AA3"
        Me.AA3.ReadOnly = True
        Me.AA3.Width = 60
        '
        'V1
        '
        DataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle49.BackColor = System.Drawing.Color.AliceBlue
        DataGridViewCellStyle49.Format = "N1"
        DataGridViewCellStyle49.NullValue = Nothing
        Me.V1.DefaultCellStyle = DataGridViewCellStyle49
        Me.V1.HeaderText = "% M"
        Me.V1.Name = "V1"
        Me.V1.ReadOnly = True
        Me.V1.Width = 55
        '
        'V2
        '
        DataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle50.BackColor = System.Drawing.Color.Cyan
        Me.V2.DefaultCellStyle = DataGridViewCellStyle50
        Me.V2.HeaderText = "Metrado"
        Me.V2.Name = "V2"
        Me.V2.ReadOnly = True
        Me.V2.Width = 60
        '
        'V3
        '
        DataGridViewCellStyle51.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle51.BackColor = System.Drawing.Color.AliceBlue
        Me.V3.DefaultCellStyle = DataGridViewCellStyle51
        Me.V3.HeaderText = "Monto"
        Me.V3.Name = "V3"
        Me.V3.ReadOnly = True
        Me.V3.Width = 60
        '
        'vapor
        '
        Me.vapor.HeaderText = "vapor"
        Me.vapor.Name = "vapor"
        Me.vapor.Width = 50
        '
        'vametrado
        '
        Me.vametrado.HeaderText = "vametrado"
        Me.vametrado.Name = "vametrado"
        Me.vametrado.Width = 50
        '
        'MONTacumulado
        '
        Me.MONTacumulado.HeaderText = "MONTacumulado"
        Me.MONTacumulado.Name = "MONTacumulado"
        Me.MONTacumulado.Width = 50
        '
        'S1
        '
        DataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle52.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle52.Format = "N1"
        DataGridViewCellStyle52.NullValue = Nothing
        Me.S1.DefaultCellStyle = DataGridViewCellStyle52
        Me.S1.HeaderText = "% M"
        Me.S1.Name = "S1"
        Me.S1.ReadOnly = True
        Me.S1.Width = 55
        '
        'S2
        '
        DataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle53.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.S2.DefaultCellStyle = DataGridViewCellStyle53
        Me.S2.HeaderText = "Metrado"
        Me.S2.Name = "S2"
        Me.S2.ReadOnly = True
        Me.S2.Width = 60
        '
        'S3
        '
        DataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle54.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.S3.DefaultCellStyle = DataGridViewCellStyle54
        Me.S3.HeaderText = "Monto"
        Me.S3.Name = "S3"
        Me.S3.ReadOnly = True
        Me.S3.Width = 60
        '
        'idDetalle
        '
        Me.idDetalle.DataPropertyName = "IdPartidaDet"
        Me.idDetalle.HeaderText = "IdDetalle"
        Me.idDetalle.Name = "idDetalle"
        Me.idDetalle.ReadOnly = True
        Me.idDetalle.Visible = False
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        Me.Estado.Visible = False
        '
        'Column1
        '
        Me.Column1.HeaderText = "P1"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'Column2
        '
        Me.Column2.HeaderText = "P2"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Visible = False
        '
        'Column3
        '
        Me.Column3.HeaderText = "P3"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Visible = False
        '
        'IdPartidaDet
        '
        Me.IdPartidaDet.DataPropertyName = "IdPartidaDet"
        Me.IdPartidaDet.HeaderText = "IdPartidaDet"
        Me.IdPartidaDet.Name = "IdPartidaDet"
        Me.IdPartidaDet.ReadOnly = True
        Me.IdPartidaDet.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.CCosto)
        Me.GroupBox1.Controls.Add(Me.BeLabel25)
        Me.GroupBox1.Controls.Add(Me.BeLabel52)
        Me.GroupBox1.Controls.Add(Me.cboContratista)
        Me.GroupBox1.Controls.Add(Me.txtUbicacion)
        Me.GroupBox1.Controls.Add(Me.cboPartidas)
        Me.GroupBox1.Controls.Add(Me.BeLabel5)
        Me.GroupBox1.Controls.Add(Me.BeLabel19)
        Me.GroupBox1.Controls.Add(Me.txtNroContrato)
        Me.GroupBox1.Controls.Add(Me.BeLabel13)
        Me.GroupBox1.Controls.Add(Me.cboObra)
        Me.GroupBox1.Controls.Add(Me.txtNumeroOrden)
        Me.GroupBox1.Controls.Add(Me.BeLabel3)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 29)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(585, 151)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Partida"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(10, 62)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "C Costo"
        '
        'CCosto
        '
        Me.CCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.CCosto.BackColor = System.Drawing.Color.Ivory
        Me.CCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CCosto.ForeColor = System.Drawing.Color.Black
        Me.CCosto.FormattingEnabled = True
        Me.CCosto.KeyEnter = True
        Me.CCosto.Location = New System.Drawing.Point(75, 55)
        Me.CCosto.Name = "CCosto"
        Me.CCosto.Size = New System.Drawing.Size(500, 21)
        Me.CCosto.TabIndex = 36
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.ForeColor = System.Drawing.Color.Black
        Me.BeLabel25.Location = New System.Drawing.Point(10, 41)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel25.TabIndex = 2
        Me.BeLabel25.Text = "Contratista"
        '
        'BeLabel52
        '
        Me.BeLabel52.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel52.AutoSize = True
        Me.BeLabel52.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel52.ForeColor = System.Drawing.Color.Black
        Me.BeLabel52.Location = New System.Drawing.Point(9, 106)
        Me.BeLabel52.Name = "BeLabel52"
        Me.BeLabel52.Size = New System.Drawing.Size(66, 13)
        Me.BeLabel52.TabIndex = 16
        Me.BeLabel52.Text = "Presupuesto"
        '
        'cboContratista
        '
        Me.cboContratista.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboContratista.BackColor = System.Drawing.Color.Ivory
        Me.cboContratista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContratista.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboContratista.ForeColor = System.Drawing.Color.Black
        Me.cboContratista.FormattingEnabled = True
        Me.cboContratista.KeyEnter = True
        Me.cboContratista.Location = New System.Drawing.Point(75, 33)
        Me.cboContratista.Name = "cboContratista"
        Me.cboContratista.Size = New System.Drawing.Size(500, 21)
        Me.cboContratista.TabIndex = 3
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtUbicacion.BackColor = System.Drawing.Color.Ivory
        Me.txtUbicacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUbicacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUbicacion.Enabled = False
        Me.txtUbicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUbicacion.ForeColor = System.Drawing.Color.Black
        Me.txtUbicacion.KeyEnter = True
        Me.txtUbicacion.Location = New System.Drawing.Point(75, 102)
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtUbicacion.ShortcutsEnabled = False
        Me.txtUbicacion.Size = New System.Drawing.Size(500, 20)
        Me.txtUbicacion.TabIndex = 17
        Me.txtUbicacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfanúmerico
        '
        'cboPartidas
        '
        Me.cboPartidas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPartidas.BackColor = System.Drawing.Color.Ivory
        Me.cboPartidas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartidas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPartidas.ForeColor = System.Drawing.Color.Black
        Me.cboPartidas.FormattingEnabled = True
        Me.cboPartidas.KeyEnter = True
        Me.cboPartidas.Location = New System.Drawing.Point(75, 77)
        Me.cboPartidas.Name = "cboPartidas"
        Me.cboPartidas.Size = New System.Drawing.Size(500, 21)
        Me.cboPartidas.TabIndex = 5
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(10, 82)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel5.TabIndex = 4
        Me.BeLabel5.Text = "Contrato"
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.ForeColor = System.Drawing.Color.Black
        Me.BeLabel19.Location = New System.Drawing.Point(305, 132)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel19.TabIndex = 10
        Me.BeLabel19.Text = "Contrato Nº"
        '
        'txtNroContrato
        '
        Me.txtNroContrato.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroContrato.BackColor = System.Drawing.Color.Ivory
        Me.txtNroContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroContrato.Enabled = False
        Me.txtNroContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroContrato.ForeColor = System.Drawing.Color.Black
        Me.txtNroContrato.KeyEnter = True
        Me.txtNroContrato.Location = New System.Drawing.Point(373, 125)
        Me.txtNroContrato.Name = "txtNroContrato"
        Me.txtNroContrato.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroContrato.ShortcutsEnabled = False
        Me.txtNroContrato.Size = New System.Drawing.Size(202, 20)
        Me.txtNroContrato.TabIndex = 11
        Me.txtNroContrato.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfanúmerico
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(10, 19)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(30, 13)
        Me.BeLabel13.TabIndex = 0
        Me.BeLabel13.Text = "Obra"
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(75, 11)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(500, 21)
        Me.cboObra.TabIndex = 1
        '
        'txtNumeroOrden
        '
        Me.txtNumeroOrden.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumeroOrden.BackColor = System.Drawing.Color.Ivory
        Me.txtNumeroOrden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumeroOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroOrden.Enabled = False
        Me.txtNumeroOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumeroOrden.ForeColor = System.Drawing.Color.Black
        Me.txtNumeroOrden.KeyEnter = True
        Me.txtNumeroOrden.Location = New System.Drawing.Point(75, 125)
        Me.txtNumeroOrden.Name = "txtNumeroOrden"
        Me.txtNumeroOrden.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumeroOrden.ShortcutsEnabled = False
        Me.txtNumeroOrden.Size = New System.Drawing.Size(203, 20)
        Me.txtNumeroOrden.TabIndex = 7
        Me.txtNumeroOrden.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfanúmerico
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(10, 127)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel3.TabIndex = 6
        Me.BeLabel3.Text = "Orden Nº"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnVerAvanceSemanal)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.BeLabel20)
        Me.GroupBox2.Controls.Add(Me.txtCodigoVal)
        Me.GroupBox2.Controls.Add(Me.BeLabel8)
        Me.GroupBox2.Controls.Add(Me.BeLabel4)
        Me.GroupBox2.Controls.Add(Me.txtSemana)
        Me.GroupBox2.Controls.Add(Me.txtNumSesion)
        Me.GroupBox2.Controls.Add(Me.BeLabel7)
        Me.GroupBox2.Controls.Add(Me.BeLabel6)
        Me.GroupBox2.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox2.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox2.Location = New System.Drawing.Point(607, 29)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(523, 151)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Valorización"
        '
        'btnVerAvanceSemanal
        '
        Me.btnVerAvanceSemanal.Location = New System.Drawing.Point(355, 108)
        Me.btnVerAvanceSemanal.Name = "btnVerAvanceSemanal"
        Me.btnVerAvanceSemanal.Size = New System.Drawing.Size(131, 23)
        Me.btnVerAvanceSemanal.TabIndex = 19
        Me.btnVerAvanceSemanal.Text = "Ver Avance Semanal"
        Me.btnVerAvanceSemanal.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.BeLabel2)
        Me.GroupBox3.Controls.Add(Me.dtpEnvio)
        Me.GroupBox3.Controls.Add(Me.chkCerrar)
        Me.GroupBox3.Location = New System.Drawing.Point(72, 97)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(247, 46)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Envio"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(29, 21)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(37, 13)
        Me.BeLabel2.TabIndex = 0
        Me.BeLabel2.Text = "Fecha"
        '
        'dtpEnvio
        '
        Me.dtpEnvio.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpEnvio.CustomFormat = ""
        Me.dtpEnvio.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEnvio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEnvio.KeyEnter = True
        Me.dtpEnvio.Location = New System.Drawing.Point(74, 17)
        Me.dtpEnvio.Name = "dtpEnvio"
        Me.dtpEnvio.Size = New System.Drawing.Size(84, 20)
        Me.dtpEnvio.TabIndex = 1
        Me.dtpEnvio.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpEnvio.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'chkCerrar
        '
        Me.chkCerrar.AutoSize = True
        Me.chkCerrar.Location = New System.Drawing.Point(166, 20)
        Me.chkCerrar.Name = "chkCerrar"
        Me.chkCerrar.Size = New System.Drawing.Size(56, 17)
        Me.chkCerrar.TabIndex = 2
        Me.chkCerrar.Text = "Enviar"
        Me.chkCerrar.UseVisualStyleBackColor = True
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.ForeColor = System.Drawing.Color.Black
        Me.BeLabel20.Location = New System.Drawing.Point(15, 24)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel20.TabIndex = 1
        Me.BeLabel20.Text = "Código"
        '
        'txtCodigoVal
        '
        Me.txtCodigoVal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigoVal.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCodigoVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoVal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoVal.Enabled = False
        Me.txtCodigoVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigoVal.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoVal.KeyEnter = True
        Me.txtCodigoVal.Location = New System.Drawing.Point(72, 17)
        Me.txtCodigoVal.Name = "txtCodigoVal"
        Me.txtCodigoVal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigoVal.ShortcutsEnabled = False
        Me.txtCodigoVal.Size = New System.Drawing.Size(248, 20)
        Me.txtCodigoVal.TabIndex = 0
        Me.txtCodigoVal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(15, 78)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel8.TabIndex = 12
        Me.BeLabel8.Text = "Nº Val."
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(168, 78)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel4.TabIndex = 14
        Me.BeLabel4.Text = "Semana"
        '
        'txtSemana
        '
        Me.txtSemana.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSemana.BackColor = System.Drawing.Color.Ivory
        Me.txtSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSemana.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSemana.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSemana.ForeColor = System.Drawing.Color.Black
        Me.txtSemana.KeyEnter = True
        Me.txtSemana.Location = New System.Drawing.Point(235, 71)
        Me.txtSemana.MaxLength = 2
        Me.txtSemana.Name = "txtSemana"
        Me.txtSemana.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSemana.ShortcutsEnabled = False
        Me.txtSemana.Size = New System.Drawing.Size(84, 20)
        Me.txtSemana.TabIndex = 15
        Me.txtSemana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtSemana.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'txtNumSesion
        '
        Me.txtNumSesion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumSesion.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtNumSesion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumSesion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumSesion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumSesion.ForeColor = System.Drawing.Color.Black
        Me.txtNumSesion.KeyEnter = True
        Me.txtNumSesion.Location = New System.Drawing.Point(72, 71)
        Me.txtNumSesion.Name = "txtNumSesion"
        Me.txtNumSesion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumSesion.ShortcutsEnabled = False
        Me.txtNumSesion.Size = New System.Drawing.Size(84, 20)
        Me.txtNumSesion.TabIndex = 13
        Me.txtNumSesion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNumSesion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(168, 50)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel7.TabIndex = 4
        Me.BeLabel7.Text = "F. Termino"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(14, 50)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(44, 13)
        Me.BeLabel6.TabIndex = 2
        Me.BeLabel6.Text = "F. Inicio"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(235, 43)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaFin.TabIndex = 5
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(72, 43)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaIni.TabIndex = 3
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnModificar, Me.btnImprimir, Me.btnSalir, Me.btnImprimirSunat})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1364, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnModificar
        '
        Me.btnModificar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(23, 22)
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.ToolTipText = "Editar"
        '
        'btnImprimir
        '
        Me.btnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimir.Text = "ToolStripButton1"
        Me.btnImprimir.ToolTipText = "Imprimir Valorización"
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'btnImprimirSunat
        '
        Me.btnImprimirSunat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimirSunat.Image = Global.CapaPreTesoreria.My.Resources.Resources.Sunat
        Me.btnImprimirSunat.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimirSunat.Name = "btnImprimirSunat"
        Me.btnImprimirSunat.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimirSunat.Text = "ToolStripButton1"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(108, 195)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 13)
        Me.Label6.TabIndex = 52
        Me.Label6.Text = "Avance Sobrepasado"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Red
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(242, 194)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(33, 14)
        Me.Panel1.TabIndex = 53
        '
        'Timer1
        '
        '
        'dgvDescuentos
        '
        Me.dgvDescuentos.AllowUserToAddRows = False
        Me.dgvDescuentos.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDescuentos.BackgroundColor = System.Drawing.Color.White
        Me.dgvDescuentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDescuentos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Des_Descripcion, Me.Des_Anterior, Me.Des_Actual, Me.Des_Acumulado, Me.Des_IdDetalle, Me.Des_Estado})
        Me.dgvDescuentos.Location = New System.Drawing.Point(4, 195)
        Me.dgvDescuentos.Name = "dgvDescuentos"
        Me.dgvDescuentos.RowHeadersVisible = False
        Me.dgvDescuentos.Size = New System.Drawing.Size(457, 71)
        Me.dgvDescuentos.TabIndex = 54
        '
        'Des_Descripcion
        '
        Me.Des_Descripcion.HeaderText = "Descuentos Otros"
        Me.Des_Descripcion.Name = "Des_Descripcion"
        Me.Des_Descripcion.Width = 242
        '
        'Des_Anterior
        '
        DataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.Des_Anterior.DefaultCellStyle = DataGridViewCellStyle55
        Me.Des_Anterior.HeaderText = "Anterior"
        Me.Des_Anterior.Name = "Des_Anterior"
        Me.Des_Anterior.Width = 63
        '
        'Des_Actual
        '
        DataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.Des_Actual.DefaultCellStyle = DataGridViewCellStyle56
        Me.Des_Actual.HeaderText = "Actual"
        Me.Des_Actual.Name = "Des_Actual"
        Me.Des_Actual.Width = 63
        '
        'Des_Acumulado
        '
        DataGridViewCellStyle57.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.Des_Acumulado.DefaultCellStyle = DataGridViewCellStyle57
        Me.Des_Acumulado.HeaderText = "Acumulado"
        Me.Des_Acumulado.Name = "Des_Acumulado"
        Me.Des_Acumulado.Width = 65
        '
        'Des_IdDetalle
        '
        Me.Des_IdDetalle.HeaderText = "Des_IdDetalle"
        Me.Des_IdDetalle.Name = "Des_IdDetalle"
        Me.Des_IdDetalle.Visible = False
        '
        'Des_Estado
        '
        Me.Des_Estado.HeaderText = "Estado"
        Me.Des_Estado.Name = "Des_Estado"
        Me.Des_Estado.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.BeLabel44)
        Me.Panel2.Controls.Add(Me.BeLabel43)
        Me.Panel2.Controls.Add(Me.BeLabel42)
        Me.Panel2.Controls.Add(Me.BeLabel31)
        Me.Panel2.Controls.Add(Me.BeLabel23)
        Me.Panel2.Controls.Add(Me.txtCuadrillaTotales)
        Me.Panel2.Controls.Add(Me.txtCuadrilla3T)
        Me.Panel2.Controls.Add(Me.txtCuadrilla2T)
        Me.Panel2.Controls.Add(Me.txtCuadrilla1T)
        Me.Panel2.Controls.Add(Me.txtCuadrilla3Pre)
        Me.Panel2.Controls.Add(Me.txtCuadrilla2Pre)
        Me.Panel2.Controls.Add(Me.txtCuadrilla1Pre)
        Me.Panel2.Controls.Add(Me.BeLabel27)
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Controls.Add(Me.btnAgregar)
        Me.Panel2.Controls.Add(Me.txtTotDes)
        Me.Panel2.Controls.Add(Me.BeLabel26)
        Me.Panel2.Controls.Add(Me.dgvDescuentos)
        Me.Panel2.Controls.Add(Me.BeLabel39)
        Me.Panel2.Controls.Add(Me.BeLabel35)
        Me.Panel2.Controls.Add(Me.txtAdelanto1)
        Me.Panel2.Controls.Add(Me.BeLabel34)
        Me.Panel2.Controls.Add(Me.BeLabel40)
        Me.Panel2.Controls.Add(Me.BeLabel36)
        Me.Panel2.Controls.Add(Me.txtAmorti1)
        Me.Panel2.Controls.Add(Me.BeLabel33)
        Me.Panel2.Controls.Add(Me.BeLabel41)
        Me.Panel2.Controls.Add(Me.BeLabel37)
        Me.Panel2.Controls.Add(Me.txtAmorti2)
        Me.Panel2.Controls.Add(Me.BeLabel32)
        Me.Panel2.Controls.Add(Me.txtCuadrilla4)
        Me.Panel2.Controls.Add(Me.BeLabel38)
        Me.Panel2.Controls.Add(Me.txtAdelanto2)
        Me.Panel2.Controls.Add(Me.txtCuadrilla3)
        Me.Panel2.Controls.Add(Me.txtFG1)
        Me.Panel2.Controls.Add(Me.txtAdelanto3)
        Me.Panel2.Controls.Add(Me.BeLabel30)
        Me.Panel2.Controls.Add(Me.txtCuadrilla2)
        Me.Panel2.Controls.Add(Me.txtAmorti3)
        Me.Panel2.Controls.Add(Me.BeLabel29)
        Me.Panel2.Controls.Add(Me.txtCuadrilla1)
        Me.Panel2.Controls.Add(Me.txtAmorti4)
        Me.Panel2.Controls.Add(Me.BeLabel28)
        Me.Panel2.Controls.Add(Me.txtFG4)
        Me.Panel2.Controls.Add(Me.txtFG2)
        Me.Panel2.Controls.Add(Me.BeLabel1)
        Me.Panel2.Controls.Add(Me.txtFG3)
        Me.Panel2.Location = New System.Drawing.Point(3, 370)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(466, 292)
        Me.Panel2.TabIndex = 102
        '
        'BeLabel44
        '
        Me.BeLabel44.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel44.AutoSize = True
        Me.BeLabel44.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel44.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel44.ForeColor = System.Drawing.Color.Black
        Me.BeLabel44.Location = New System.Drawing.Point(319, 136)
        Me.BeLabel44.Name = "BeLabel44"
        Me.BeLabel44.Size = New System.Drawing.Size(30, 12)
        Me.BeLabel44.TabIndex = 117
        Me.BeLabel44.Text = "PMO"
        '
        'BeLabel43
        '
        Me.BeLabel43.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel43.AutoSize = True
        Me.BeLabel43.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel43.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel43.ForeColor = System.Drawing.Color.Black
        Me.BeLabel43.Location = New System.Drawing.Point(215, 136)
        Me.BeLabel43.Name = "BeLabel43"
        Me.BeLabel43.Size = New System.Drawing.Size(30, 12)
        Me.BeLabel43.TabIndex = 116
        Me.BeLabel43.Text = "PMO"
        '
        'BeLabel42
        '
        Me.BeLabel42.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel42.AutoSize = True
        Me.BeLabel42.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel42.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel42.ForeColor = System.Drawing.Color.Black
        Me.BeLabel42.Location = New System.Drawing.Point(111, 137)
        Me.BeLabel42.Name = "BeLabel42"
        Me.BeLabel42.Size = New System.Drawing.Size(30, 12)
        Me.BeLabel42.TabIndex = 115
        Me.BeLabel42.Text = "PMO"
        '
        'BeLabel31
        '
        Me.BeLabel31.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel31.AutoSize = True
        Me.BeLabel31.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel31.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel31.ForeColor = System.Drawing.Color.Black
        Me.BeLabel31.Location = New System.Drawing.Point(353, 155)
        Me.BeLabel31.Name = "BeLabel31"
        Me.BeLabel31.Size = New System.Drawing.Size(11, 12)
        Me.BeLabel31.TabIndex = 114
        Me.BeLabel31.Text = "="
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(353, 176)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(11, 12)
        Me.BeLabel23.TabIndex = 113
        Me.BeLabel23.Text = "="
        '
        'txtCuadrillaTotales
        '
        Me.txtCuadrillaTotales.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrillaTotales.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrillaTotales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrillaTotales.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrillaTotales.Enabled = False
        Me.txtCuadrillaTotales.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrillaTotales.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrillaTotales.KeyEnter = True
        Me.txtCuadrillaTotales.Location = New System.Drawing.Point(368, 172)
        Me.txtCuadrillaTotales.Name = "txtCuadrillaTotales"
        Me.txtCuadrillaTotales.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrillaTotales.ShortcutsEnabled = False
        Me.txtCuadrillaTotales.Size = New System.Drawing.Size(93, 20)
        Me.txtCuadrillaTotales.TabIndex = 112
        Me.txtCuadrillaTotales.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrillaTotales.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCuadrilla3T
        '
        Me.txtCuadrilla3T.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla3T.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla3T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla3T.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla3T.Enabled = False
        Me.txtCuadrilla3T.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla3T.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla3T.KeyEnter = True
        Me.txtCuadrilla3T.Location = New System.Drawing.Point(256, 172)
        Me.txtCuadrilla3T.Name = "txtCuadrilla3T"
        Me.txtCuadrilla3T.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla3T.ShortcutsEnabled = False
        Me.txtCuadrilla3T.Size = New System.Drawing.Size(93, 20)
        Me.txtCuadrilla3T.TabIndex = 111
        Me.txtCuadrilla3T.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla3T.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCuadrilla2T
        '
        Me.txtCuadrilla2T.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla2T.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla2T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla2T.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla2T.Enabled = False
        Me.txtCuadrilla2T.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla2T.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla2T.KeyEnter = True
        Me.txtCuadrilla2T.Location = New System.Drawing.Point(152, 172)
        Me.txtCuadrilla2T.Name = "txtCuadrilla2T"
        Me.txtCuadrilla2T.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla2T.ShortcutsEnabled = False
        Me.txtCuadrilla2T.Size = New System.Drawing.Size(93, 20)
        Me.txtCuadrilla2T.TabIndex = 110
        Me.txtCuadrilla2T.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla2T.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCuadrilla1T
        '
        Me.txtCuadrilla1T.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla1T.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla1T.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla1T.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla1T.Enabled = False
        Me.txtCuadrilla1T.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla1T.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla1T.KeyEnter = True
        Me.txtCuadrilla1T.Location = New System.Drawing.Point(48, 172)
        Me.txtCuadrilla1T.Name = "txtCuadrilla1T"
        Me.txtCuadrilla1T.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla1T.ShortcutsEnabled = False
        Me.txtCuadrilla1T.Size = New System.Drawing.Size(93, 20)
        Me.txtCuadrilla1T.TabIndex = 109
        Me.txtCuadrilla1T.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla1T.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCuadrilla3Pre
        '
        Me.txtCuadrilla3Pre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla3Pre.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla3Pre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla3Pre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla3Pre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla3Pre.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla3Pre.KeyEnter = True
        Me.txtCuadrilla3Pre.Location = New System.Drawing.Point(304, 151)
        Me.txtCuadrilla3Pre.Name = "txtCuadrilla3Pre"
        Me.txtCuadrilla3Pre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla3Pre.ShortcutsEnabled = False
        Me.txtCuadrilla3Pre.Size = New System.Drawing.Size(45, 20)
        Me.txtCuadrilla3Pre.TabIndex = 108
        Me.txtCuadrilla3Pre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla3Pre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCuadrilla2Pre
        '
        Me.txtCuadrilla2Pre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla2Pre.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla2Pre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla2Pre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla2Pre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla2Pre.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla2Pre.KeyEnter = True
        Me.txtCuadrilla2Pre.Location = New System.Drawing.Point(200, 151)
        Me.txtCuadrilla2Pre.Name = "txtCuadrilla2Pre"
        Me.txtCuadrilla2Pre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla2Pre.ShortcutsEnabled = False
        Me.txtCuadrilla2Pre.Size = New System.Drawing.Size(45, 20)
        Me.txtCuadrilla2Pre.TabIndex = 107
        Me.txtCuadrilla2Pre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla2Pre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCuadrilla1Pre
        '
        Me.txtCuadrilla1Pre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla1Pre.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla1Pre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla1Pre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla1Pre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla1Pre.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla1Pre.KeyEnter = True
        Me.txtCuadrilla1Pre.Location = New System.Drawing.Point(96, 151)
        Me.txtCuadrilla1Pre.Name = "txtCuadrilla1Pre"
        Me.txtCuadrilla1Pre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla1Pre.ShortcutsEnabled = False
        Me.txtCuadrilla1Pre.Size = New System.Drawing.Size(45, 20)
        Me.txtCuadrilla1Pre.TabIndex = 106
        Me.txtCuadrilla1Pre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla1Pre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel27
        '
        Me.BeLabel27.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel27.AutoSize = True
        Me.BeLabel27.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel27.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel27.ForeColor = System.Drawing.Color.Black
        Me.BeLabel27.Location = New System.Drawing.Point(-2, 124)
        Me.BeLabel27.Name = "BeLabel27"
        Me.BeLabel27.Size = New System.Drawing.Size(473, 12)
        Me.BeLabel27.TabIndex = 105
        Me.BeLabel27.Text = "______________________________________________________________________________"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(70, 266)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(65, 23)
        Me.Button1.TabIndex = 104
        Me.Button1.Text = "Quitar -"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(3, 266)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(65, 23)
        Me.btnAgregar.TabIndex = 103
        Me.btnAgregar.Text = "Agregar +"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'txtTotDes
        '
        Me.txtTotDes.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotDes.BackColor = System.Drawing.Color.Lavender
        Me.txtTotDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotDes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotDes.Enabled = False
        Me.txtTotDes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotDes.ForeColor = System.Drawing.Color.Black
        Me.txtTotDes.KeyEnter = True
        Me.txtTotDes.Location = New System.Drawing.Point(376, 268)
        Me.txtTotDes.Name = "txtTotDes"
        Me.txtTotDes.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotDes.ShortcutsEnabled = False
        Me.txtTotDes.Size = New System.Drawing.Size(59, 20)
        Me.txtTotDes.TabIndex = 102
        Me.txtTotDes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotDes.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel26
        '
        Me.BeLabel26.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel26.AutoSize = True
        Me.BeLabel26.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel26.ForeColor = System.Drawing.Color.Black
        Me.BeLabel26.Location = New System.Drawing.Point(334, 271)
        Me.BeLabel26.Name = "BeLabel26"
        Me.BeLabel26.Size = New System.Drawing.Size(36, 13)
        Me.BeLabel26.TabIndex = 101
        Me.BeLabel26.Text = "Total"
        '
        'BeLabel39
        '
        Me.BeLabel39.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel39.AutoSize = True
        Me.BeLabel39.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel39.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel39.ForeColor = System.Drawing.Color.Black
        Me.BeLabel39.Location = New System.Drawing.Point(254, 137)
        Me.BeLabel39.Name = "BeLabel39"
        Me.BeLabel39.Size = New System.Drawing.Size(19, 12)
        Me.BeLabel39.TabIndex = 100
        Me.BeLabel39.Text = "AY"
        '
        'BeLabel35
        '
        Me.BeLabel35.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel35.AutoSize = True
        Me.BeLabel35.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel35.ForeColor = System.Drawing.Color.Black
        Me.BeLabel35.Location = New System.Drawing.Point(372, 4)
        Me.BeLabel35.Name = "BeLabel35"
        Me.BeLabel35.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel35.TabIndex = 83
        Me.BeLabel35.Text = "Acumulado"
        '
        'txtAdelanto1
        '
        Me.txtAdelanto1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAdelanto1.BackColor = System.Drawing.Color.Lavender
        Me.txtAdelanto1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAdelanto1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdelanto1.Enabled = False
        Me.txtAdelanto1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdelanto1.ForeColor = System.Drawing.Color.Black
        Me.txtAdelanto1.KeyEnter = True
        Me.txtAdelanto1.Location = New System.Drawing.Point(256, 20)
        Me.txtAdelanto1.Name = "txtAdelanto1"
        Me.txtAdelanto1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAdelanto1.ShortcutsEnabled = False
        Me.txtAdelanto1.Size = New System.Drawing.Size(59, 20)
        Me.txtAdelanto1.TabIndex = 55
        Me.txtAdelanto1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAdelanto1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel34
        '
        Me.BeLabel34.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel34.AutoSize = True
        Me.BeLabel34.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel34.ForeColor = System.Drawing.Color.Black
        Me.BeLabel34.Location = New System.Drawing.Point(324, 4)
        Me.BeLabel34.Name = "BeLabel34"
        Me.BeLabel34.Size = New System.Drawing.Size(43, 13)
        Me.BeLabel34.TabIndex = 82
        Me.BeLabel34.Text = "Actual"
        '
        'BeLabel40
        '
        Me.BeLabel40.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel40.AutoSize = True
        Me.BeLabel40.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel40.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel40.ForeColor = System.Drawing.Color.Black
        Me.BeLabel40.Location = New System.Drawing.Point(150, 136)
        Me.BeLabel40.Name = "BeLabel40"
        Me.BeLabel40.Size = New System.Drawing.Size(20, 12)
        Me.BeLabel40.TabIndex = 99
        Me.BeLabel40.Text = "OF"
        '
        'BeLabel36
        '
        Me.BeLabel36.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel36.AutoSize = True
        Me.BeLabel36.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel36.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel36.ForeColor = System.Drawing.Color.Black
        Me.BeLabel36.Location = New System.Drawing.Point(340, 69)
        Me.BeLabel36.Name = "BeLabel36"
        Me.BeLabel36.Size = New System.Drawing.Size(33, 12)
        Me.BeLabel36.TabIndex = 84
        Me.BeLabel36.Text = "Saldo"
        '
        'txtAmorti1
        '
        Me.txtAmorti1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAmorti1.BackColor = System.Drawing.Color.Lavender
        Me.txtAmorti1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAmorti1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAmorti1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmorti1.ForeColor = System.Drawing.Color.Black
        Me.txtAmorti1.KeyEnter = True
        Me.txtAmorti1.Location = New System.Drawing.Point(256, 41)
        Me.txtAmorti1.Name = "txtAmorti1"
        Me.txtAmorti1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAmorti1.ShortcutsEnabled = False
        Me.txtAmorti1.Size = New System.Drawing.Size(59, 20)
        Me.txtAmorti1.TabIndex = 56
        Me.txtAmorti1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmorti1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel33
        '
        Me.BeLabel33.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel33.AutoSize = True
        Me.BeLabel33.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel33.ForeColor = System.Drawing.Color.Black
        Me.BeLabel33.Location = New System.Drawing.Point(262, 4)
        Me.BeLabel33.Name = "BeLabel33"
        Me.BeLabel33.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel33.TabIndex = 81
        Me.BeLabel33.Text = "Anterior"
        '
        'BeLabel41
        '
        Me.BeLabel41.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel41.AutoSize = True
        Me.BeLabel41.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel41.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel41.ForeColor = System.Drawing.Color.Black
        Me.BeLabel41.Location = New System.Drawing.Point(46, 136)
        Me.BeLabel41.Name = "BeLabel41"
        Me.BeLabel41.Size = New System.Drawing.Size(20, 12)
        Me.BeLabel41.TabIndex = 98
        Me.BeLabel41.Text = "OP"
        '
        'BeLabel37
        '
        Me.BeLabel37.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel37.AutoSize = True
        Me.BeLabel37.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel37.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel37.ForeColor = System.Drawing.Color.Black
        Me.BeLabel37.Location = New System.Drawing.Point(328, 111)
        Me.BeLabel37.Name = "BeLabel37"
        Me.BeLabel37.Size = New System.Drawing.Size(45, 12)
        Me.BeLabel37.TabIndex = 85
        Me.BeLabel37.Text = "A Pagar"
        '
        'txtAmorti2
        '
        Me.txtAmorti2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAmorti2.BackColor = System.Drawing.Color.Lavender
        Me.txtAmorti2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAmorti2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAmorti2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmorti2.ForeColor = System.Drawing.Color.Black
        Me.txtAmorti2.KeyEnter = True
        Me.txtAmorti2.Location = New System.Drawing.Point(316, 41)
        Me.txtAmorti2.Name = "txtAmorti2"
        Me.txtAmorti2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAmorti2.ShortcutsEnabled = False
        Me.txtAmorti2.Size = New System.Drawing.Size(59, 20)
        Me.txtAmorti2.TabIndex = 57
        Me.txtAmorti2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmorti2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel32
        '
        Me.BeLabel32.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel32.AutoSize = True
        Me.BeLabel32.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel32.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel32.ForeColor = System.Drawing.Color.Black
        Me.BeLabel32.Location = New System.Drawing.Point(-2, 158)
        Me.BeLabel32.Name = "BeLabel32"
        Me.BeLabel32.Size = New System.Drawing.Size(50, 12)
        Me.BeLabel32.TabIndex = 80
        Me.BeLabel32.Text = "Cuadrilla"
        '
        'txtCuadrilla4
        '
        Me.txtCuadrilla4.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla4.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla4.Enabled = False
        Me.txtCuadrilla4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla4.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla4.KeyEnter = True
        Me.txtCuadrilla4.Location = New System.Drawing.Point(368, 151)
        Me.txtCuadrilla4.Name = "txtCuadrilla4"
        Me.txtCuadrilla4.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla4.ShortcutsEnabled = False
        Me.txtCuadrilla4.Size = New System.Drawing.Size(93, 20)
        Me.txtCuadrilla4.TabIndex = 97
        Me.txtCuadrilla4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla4.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel38
        '
        Me.BeLabel38.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel38.AutoSize = True
        Me.BeLabel38.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel38.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel38.ForeColor = System.Drawing.Color.Black
        Me.BeLabel38.Location = New System.Drawing.Point(374, 137)
        Me.BeLabel38.Name = "BeLabel38"
        Me.BeLabel38.Size = New System.Drawing.Size(77, 12)
        Me.BeLabel38.TabIndex = 86
        Me.BeLabel38.Text = "Total Personal"
        '
        'txtAdelanto2
        '
        Me.txtAdelanto2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAdelanto2.BackColor = System.Drawing.Color.Lavender
        Me.txtAdelanto2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAdelanto2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdelanto2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdelanto2.ForeColor = System.Drawing.Color.Black
        Me.txtAdelanto2.KeyEnter = True
        Me.txtAdelanto2.Location = New System.Drawing.Point(316, 20)
        Me.txtAdelanto2.Name = "txtAdelanto2"
        Me.txtAdelanto2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAdelanto2.ShortcutsEnabled = False
        Me.txtAdelanto2.Size = New System.Drawing.Size(59, 20)
        Me.txtAdelanto2.TabIndex = 58
        Me.txtAdelanto2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAdelanto2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCuadrilla3
        '
        Me.txtCuadrilla3.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla3.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla3.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla3.KeyEnter = True
        Me.txtCuadrilla3.Location = New System.Drawing.Point(256, 151)
        Me.txtCuadrilla3.Name = "txtCuadrilla3"
        Me.txtCuadrilla3.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla3.ShortcutsEnabled = False
        Me.txtCuadrilla3.Size = New System.Drawing.Size(45, 20)
        Me.txtCuadrilla3.TabIndex = 96
        Me.txtCuadrilla3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla3.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtFG1
        '
        Me.txtFG1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFG1.BackColor = System.Drawing.Color.Lavender
        Me.txtFG1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFG1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFG1.Enabled = False
        Me.txtFG1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFG1.ForeColor = System.Drawing.Color.Black
        Me.txtFG1.KeyEnter = True
        Me.txtFG1.Location = New System.Drawing.Point(256, 83)
        Me.txtFG1.Name = "txtFG1"
        Me.txtFG1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFG1.ShortcutsEnabled = False
        Me.txtFG1.Size = New System.Drawing.Size(59, 20)
        Me.txtFG1.TabIndex = 87
        Me.txtFG1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFG1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtAdelanto3
        '
        Me.txtAdelanto3.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAdelanto3.BackColor = System.Drawing.Color.Lavender
        Me.txtAdelanto3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAdelanto3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdelanto3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdelanto3.ForeColor = System.Drawing.Color.Black
        Me.txtAdelanto3.KeyEnter = True
        Me.txtAdelanto3.Location = New System.Drawing.Point(376, 20)
        Me.txtAdelanto3.Name = "txtAdelanto3"
        Me.txtAdelanto3.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAdelanto3.ShortcutsEnabled = False
        Me.txtAdelanto3.Size = New System.Drawing.Size(59, 20)
        Me.txtAdelanto3.TabIndex = 59
        Me.txtAdelanto3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAdelanto3.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel30
        '
        Me.BeLabel30.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel30.AutoSize = True
        Me.BeLabel30.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel30.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel30.ForeColor = System.Drawing.Color.Black
        Me.BeLabel30.Location = New System.Drawing.Point(81, 90)
        Me.BeLabel30.Name = "BeLabel30"
        Me.BeLabel30.Size = New System.Drawing.Size(171, 12)
        Me.BeLabel30.TabIndex = 78
        Me.BeLabel30.Text = "Saldo Contra Acta de Recepción "
        '
        'txtCuadrilla2
        '
        Me.txtCuadrilla2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla2.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla2.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla2.KeyEnter = True
        Me.txtCuadrilla2.Location = New System.Drawing.Point(152, 151)
        Me.txtCuadrilla2.Name = "txtCuadrilla2"
        Me.txtCuadrilla2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla2.ShortcutsEnabled = False
        Me.txtCuadrilla2.Size = New System.Drawing.Size(45, 20)
        Me.txtCuadrilla2.TabIndex = 95
        Me.txtCuadrilla2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtAmorti3
        '
        Me.txtAmorti3.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAmorti3.BackColor = System.Drawing.Color.Lavender
        Me.txtAmorti3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAmorti3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAmorti3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmorti3.ForeColor = System.Drawing.Color.Black
        Me.txtAmorti3.KeyEnter = True
        Me.txtAmorti3.Location = New System.Drawing.Point(376, 41)
        Me.txtAmorti3.Name = "txtAmorti3"
        Me.txtAmorti3.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAmorti3.ShortcutsEnabled = False
        Me.txtAmorti3.Size = New System.Drawing.Size(59, 20)
        Me.txtAmorti3.TabIndex = 60
        Me.txtAmorti3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmorti3.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel29.ForeColor = System.Drawing.Color.Black
        Me.BeLabel29.Location = New System.Drawing.Point(181, 48)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(72, 12)
        Me.BeLabel29.TabIndex = 77
        Me.BeLabel29.Text = "Amortización"
        '
        'txtCuadrilla1
        '
        Me.txtCuadrilla1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCuadrilla1.BackColor = System.Drawing.Color.Lavender
        Me.txtCuadrilla1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCuadrilla1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCuadrilla1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCuadrilla1.ForeColor = System.Drawing.Color.Black
        Me.txtCuadrilla1.KeyEnter = True
        Me.txtCuadrilla1.Location = New System.Drawing.Point(48, 151)
        Me.txtCuadrilla1.Name = "txtCuadrilla1"
        Me.txtCuadrilla1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCuadrilla1.ShortcutsEnabled = False
        Me.txtCuadrilla1.Size = New System.Drawing.Size(45, 20)
        Me.txtCuadrilla1.TabIndex = 94
        Me.txtCuadrilla1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCuadrilla1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtAmorti4
        '
        Me.txtAmorti4.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAmorti4.BackColor = System.Drawing.Color.Lavender
        Me.txtAmorti4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAmorti4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAmorti4.Enabled = False
        Me.txtAmorti4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmorti4.ForeColor = System.Drawing.Color.Black
        Me.txtAmorti4.KeyEnter = True
        Me.txtAmorti4.Location = New System.Drawing.Point(376, 62)
        Me.txtAmorti4.Name = "txtAmorti4"
        Me.txtAmorti4.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAmorti4.ShortcutsEnabled = False
        Me.txtAmorti4.Size = New System.Drawing.Size(59, 20)
        Me.txtAmorti4.TabIndex = 61
        Me.txtAmorti4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAmorti4.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel28.ForeColor = System.Drawing.Color.Black
        Me.BeLabel28.Location = New System.Drawing.Point(203, 27)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(50, 12)
        Me.BeLabel28.TabIndex = 76
        Me.BeLabel28.Text = "Adelanto"
        '
        'txtFG4
        '
        Me.txtFG4.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFG4.BackColor = System.Drawing.Color.Lavender
        Me.txtFG4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFG4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFG4.Enabled = False
        Me.txtFG4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFG4.ForeColor = System.Drawing.Color.Black
        Me.txtFG4.KeyEnter = True
        Me.txtFG4.Location = New System.Drawing.Point(376, 104)
        Me.txtFG4.Name = "txtFG4"
        Me.txtFG4.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFG4.ShortcutsEnabled = False
        Me.txtFG4.Size = New System.Drawing.Size(59, 20)
        Me.txtFG4.TabIndex = 93
        Me.txtFG4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFG4.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtFG2
        '
        Me.txtFG2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFG2.BackColor = System.Drawing.Color.Lavender
        Me.txtFG2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFG2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFG2.Enabled = False
        Me.txtFG2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFG2.ForeColor = System.Drawing.Color.Black
        Me.txtFG2.KeyEnter = True
        Me.txtFG2.Location = New System.Drawing.Point(316, 83)
        Me.txtFG2.Name = "txtFG2"
        Me.txtFG2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFG2.ShortcutsEnabled = False
        Me.txtFG2.Size = New System.Drawing.Size(59, 20)
        Me.txtFG2.TabIndex = 90
        Me.txtFG2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFG2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(176, 4)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(74, 13)
        Me.BeLabel1.TabIndex = 73
        Me.BeLabel1.Text = "Descripción"
        '
        'txtFG3
        '
        Me.txtFG3.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFG3.BackColor = System.Drawing.Color.Lavender
        Me.txtFG3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFG3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFG3.Enabled = False
        Me.txtFG3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFG3.ForeColor = System.Drawing.Color.Black
        Me.txtFG3.KeyEnter = True
        Me.txtFG3.Location = New System.Drawing.Point(376, 83)
        Me.txtFG3.Name = "txtFG3"
        Me.txtFG3.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFG3.ShortcutsEnabled = False
        Me.txtFG3.Size = New System.Drawing.Size(59, 20)
        Me.txtFG3.TabIndex = 91
        Me.txtFG3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFG3.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel45
        '
        Me.BeLabel45.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel45.AutoSize = True
        Me.BeLabel45.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel45.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeLabel45.ForeColor = System.Drawing.Color.Black
        Me.BeLabel45.Location = New System.Drawing.Point(962, 193)
        Me.BeLabel45.Name = "BeLabel45"
        Me.BeLabel45.Size = New System.Drawing.Size(176, 15)
        Me.BeLabel45.TabIndex = 106
        Me.BeLabel45.Text = "                Acumulado                      "
        '
        'txtAcumuladoIGV
        '
        Me.txtAcumuladoIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAcumuladoIGV.BackColor = System.Drawing.Color.Lavender
        Me.txtAcumuladoIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAcumuladoIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAcumuladoIGV.Enabled = False
        Me.txtAcumuladoIGV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcumuladoIGV.ForeColor = System.Drawing.Color.Black
        Me.txtAcumuladoIGV.KeyEnter = True
        Me.txtAcumuladoIGV.Location = New System.Drawing.Point(1028, 390)
        Me.txtAcumuladoIGV.Name = "txtAcumuladoIGV"
        Me.txtAcumuladoIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAcumuladoIGV.ShortcutsEnabled = False
        Me.txtAcumuladoIGV.Size = New System.Drawing.Size(80, 20)
        Me.txtAcumuladoIGV.TabIndex = 104
        Me.txtAcumuladoIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAcumuladoIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtAcumuladoTotal
        '
        Me.txtAcumuladoTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAcumuladoTotal.BackColor = System.Drawing.Color.Lavender
        Me.txtAcumuladoTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAcumuladoTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAcumuladoTotal.Enabled = False
        Me.txtAcumuladoTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcumuladoTotal.ForeColor = System.Drawing.Color.Black
        Me.txtAcumuladoTotal.KeyEnter = True
        Me.txtAcumuladoTotal.Location = New System.Drawing.Point(1028, 411)
        Me.txtAcumuladoTotal.Name = "txtAcumuladoTotal"
        Me.txtAcumuladoTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAcumuladoTotal.ShortcutsEnabled = False
        Me.txtAcumuladoTotal.Size = New System.Drawing.Size(80, 20)
        Me.txtAcumuladoTotal.TabIndex = 105
        Me.txtAcumuladoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAcumuladoTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtAcumuladoSubtotal
        '
        Me.txtAcumuladoSubtotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAcumuladoSubtotal.BackColor = System.Drawing.Color.Lavender
        Me.txtAcumuladoSubtotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAcumuladoSubtotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAcumuladoSubtotal.Enabled = False
        Me.txtAcumuladoSubtotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAcumuladoSubtotal.ForeColor = System.Drawing.Color.Black
        Me.txtAcumuladoSubtotal.KeyEnter = True
        Me.txtAcumuladoSubtotal.Location = New System.Drawing.Point(1028, 369)
        Me.txtAcumuladoSubtotal.Name = "txtAcumuladoSubtotal"
        Me.txtAcumuladoSubtotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAcumuladoSubtotal.ShortcutsEnabled = False
        Me.txtAcumuladoSubtotal.Size = New System.Drawing.Size(80, 20)
        Me.txtAcumuladoSubtotal.TabIndex = 103
        Me.txtAcumuladoSubtotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAcumuladoSubtotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'AxMonthView1
        '
        Me.AxMonthView1.Location = New System.Drawing.Point(1171, 40)
        Me.AxMonthView1.Name = "AxMonthView1"
        Me.AxMonthView1.OcxState = CType(resources.GetObject("AxMonthView1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxMonthView1.Size = New System.Drawing.Size(181, 158)
        Me.AxMonthView1.TabIndex = 51
        Me.AxMonthView1.Visible = False
        '
        'BeLabel24
        '
        Me.BeLabel24.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel24.AutoSize = True
        Me.BeLabel24.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel24.ForeColor = System.Drawing.Color.Black
        Me.BeLabel24.Location = New System.Drawing.Point(786, 624)
        Me.BeLabel24.Name = "BeLabel24"
        Me.BeLabel24.Size = New System.Drawing.Size(84, 13)
        Me.BeLabel24.TabIndex = 32
        Me.BeLabel24.Text = "Total a Pagar"
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(773, 463)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(102, 13)
        Me.BeLabel22.TabIndex = 28
        Me.BeLabel22.Text = "Descuento Otros"
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel21.ForeColor = System.Drawing.Color.Black
        Me.BeLabel21.Location = New System.Drawing.Point(724, 505)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(151, 13)
        Me.BeLabel21.TabIndex = 26
        Me.BeLabel21.Text = "Amortización de Adelanto"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(647, 484)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(228, 13)
        Me.BeLabel12.TabIndex = 24
        Me.BeLabel12.Text = "Saldo Contra Acta de Recepción (10%)"
        '
        'txtTotPagar
        '
        Me.txtTotPagar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotPagar.BackColor = System.Drawing.Color.Lavender
        Me.txtTotPagar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotPagar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotPagar.Enabled = False
        Me.txtTotPagar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotPagar.ForeColor = System.Drawing.Color.Black
        Me.txtTotPagar.KeyEnter = True
        Me.txtTotPagar.Location = New System.Drawing.Point(877, 617)
        Me.txtTotPagar.Name = "txtTotPagar"
        Me.txtTotPagar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotPagar.ShortcutsEnabled = False
        Me.txtTotPagar.Size = New System.Drawing.Size(80, 20)
        Me.txtTotPagar.TabIndex = 33
        Me.txtTotPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotPagar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtAdelanto
        '
        Me.txtAdelanto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAdelanto.BackColor = System.Drawing.Color.Lavender
        Me.txtAdelanto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAdelanto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAdelanto.Enabled = False
        Me.txtAdelanto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdelanto.ForeColor = System.Drawing.Color.Black
        Me.txtAdelanto.KeyEnter = True
        Me.txtAdelanto.Location = New System.Drawing.Point(878, 498)
        Me.txtAdelanto.Name = "txtAdelanto"
        Me.txtAdelanto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAdelanto.ShortcutsEnabled = False
        Me.txtAdelanto.Size = New System.Drawing.Size(80, 20)
        Me.txtAdelanto.TabIndex = 27
        Me.txtAdelanto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAdelanto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtOtroDes
        '
        Me.txtOtroDes.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtOtroDes.BackColor = System.Drawing.Color.Lavender
        Me.txtOtroDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtOtroDes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOtroDes.Enabled = False
        Me.txtOtroDes.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOtroDes.ForeColor = System.Drawing.Color.Black
        Me.txtOtroDes.KeyEnter = True
        Me.txtOtroDes.Location = New System.Drawing.Point(878, 456)
        Me.txtOtroDes.Name = "txtOtroDes"
        Me.txtOtroDes.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtOtroDes.ShortcutsEnabled = False
        Me.txtOtroDes.Size = New System.Drawing.Size(80, 20)
        Me.txtOtroDes.TabIndex = 29
        Me.txtOtroDes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOtroDes.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtRetPor
        '
        Me.txtRetPor.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRetPor.BackColor = System.Drawing.Color.Lavender
        Me.txtRetPor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRetPor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRetPor.Enabled = False
        Me.txtRetPor.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRetPor.ForeColor = System.Drawing.Color.Black
        Me.txtRetPor.KeyEnter = True
        Me.txtRetPor.Location = New System.Drawing.Point(878, 477)
        Me.txtRetPor.Name = "txtRetPor"
        Me.txtRetPor.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRetPor.ShortcutsEnabled = False
        Me.txtRetPor.Size = New System.Drawing.Size(80, 20)
        Me.txtRetPor.TabIndex = 25
        Me.txtRetPor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRetPor.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(1, 195)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel18.TabIndex = 8
        Me.BeLabel18.Text = "Ubicación"
        '
        'txtSubTotalIGV
        '
        Me.txtSubTotalIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotalIGV.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotalIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotalIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotalIGV.Enabled = False
        Me.txtSubTotalIGV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalIGV.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotalIGV.KeyEnter = True
        Me.txtSubTotalIGV.Location = New System.Drawing.Point(1202, 390)
        Me.txtSubTotalIGV.Name = "txtSubTotalIGV"
        Me.txtSubTotalIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotalIGV.ShortcutsEnabled = False
        Me.txtSubTotalIGV.Size = New System.Drawing.Size(80, 20)
        Me.txtSubTotalIGV.TabIndex = 22
        Me.txtSubTotalIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotalIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSubTotalTot
        '
        Me.txtSubTotalTot.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotalTot.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotalTot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotalTot.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotalTot.Enabled = False
        Me.txtSubTotalTot.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalTot.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotalTot.KeyEnter = True
        Me.txtSubTotalTot.Location = New System.Drawing.Point(1202, 411)
        Me.txtSubTotalTot.Name = "txtSubTotalTot"
        Me.txtSubTotalTot.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotalTot.ShortcutsEnabled = False
        Me.txtSubTotalTot.Size = New System.Drawing.Size(80, 20)
        Me.txtSubTotalTot.TabIndex = 23
        Me.txtSubTotalTot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotalTot.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSubTotalSaldo
        '
        Me.txtSubTotalSaldo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotalSaldo.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotalSaldo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotalSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotalSaldo.Enabled = False
        Me.txtSubTotalSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalSaldo.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotalSaldo.KeyEnter = True
        Me.txtSubTotalSaldo.Location = New System.Drawing.Point(1202, 369)
        Me.txtSubTotalSaldo.Name = "txtSubTotalSaldo"
        Me.txtSubTotalSaldo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotalSaldo.ShortcutsEnabled = False
        Me.txtSubTotalSaldo.Size = New System.Drawing.Size(80, 20)
        Me.txtSubTotalSaldo.TabIndex = 21
        Me.txtSubTotalSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotalSaldo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSubIGVVal
        '
        Me.txtSubIGVVal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubIGVVal.BackColor = System.Drawing.Color.Lavender
        Me.txtSubIGVVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubIGVVal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubIGVVal.Enabled = False
        Me.txtSubIGVVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubIGVVal.ForeColor = System.Drawing.Color.Black
        Me.txtSubIGVVal.KeyEnter = True
        Me.txtSubIGVVal.Location = New System.Drawing.Point(878, 390)
        Me.txtSubIGVVal.Name = "txtSubIGVVal"
        Me.txtSubIGVVal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubIGVVal.ShortcutsEnabled = False
        Me.txtSubIGVVal.Size = New System.Drawing.Size(80, 20)
        Me.txtSubIGVVal.TabIndex = 19
        Me.txtSubIGVVal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubIGVVal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtTotalVal
        '
        Me.txtTotalVal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotalVal.BackColor = System.Drawing.Color.Lavender
        Me.txtTotalVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotalVal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalVal.Enabled = False
        Me.txtTotalVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalVal.ForeColor = System.Drawing.Color.Black
        Me.txtTotalVal.KeyEnter = True
        Me.txtTotalVal.Location = New System.Drawing.Point(878, 411)
        Me.txtTotalVal.Name = "txtTotalVal"
        Me.txtTotalVal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotalVal.ShortcutsEnabled = False
        Me.txtTotalVal.Size = New System.Drawing.Size(80, 20)
        Me.txtTotalVal.TabIndex = 20
        Me.txtTotalVal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalVal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSubTotalVal
        '
        Me.txtSubTotalVal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotalVal.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotalVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotalVal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotalVal.Enabled = False
        Me.txtSubTotalVal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalVal.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotalVal.KeyEnter = True
        Me.txtSubTotalVal.Location = New System.Drawing.Point(878, 369)
        Me.txtSubTotalVal.Name = "txtSubTotalVal"
        Me.txtSubTotalVal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotalVal.ShortcutsEnabled = False
        Me.txtSubTotalVal.Size = New System.Drawing.Size(80, 20)
        Me.txtSubTotalVal.TabIndex = 18
        Me.txtSubTotalVal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotalVal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtAA2
        '
        Me.txtAA2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAA2.BackColor = System.Drawing.Color.Lavender
        Me.txtAA2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAA2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAA2.Enabled = False
        Me.txtAA2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAA2.ForeColor = System.Drawing.Color.Black
        Me.txtAA2.KeyEnter = True
        Me.txtAA2.Location = New System.Drawing.Point(703, 390)
        Me.txtAA2.Name = "txtAA2"
        Me.txtAA2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAA2.ShortcutsEnabled = False
        Me.txtAA2.Size = New System.Drawing.Size(80, 20)
        Me.txtAA2.TabIndex = 16
        Me.txtAA2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAA2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtAA3
        '
        Me.txtAA3.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAA3.BackColor = System.Drawing.Color.Lavender
        Me.txtAA3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAA3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAA3.Enabled = False
        Me.txtAA3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAA3.ForeColor = System.Drawing.Color.Black
        Me.txtAA3.KeyEnter = True
        Me.txtAA3.Location = New System.Drawing.Point(703, 411)
        Me.txtAA3.Name = "txtAA3"
        Me.txtAA3.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAA3.ShortcutsEnabled = False
        Me.txtAA3.Size = New System.Drawing.Size(80, 20)
        Me.txtAA3.TabIndex = 17
        Me.txtAA3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAA3.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtAA1
        '
        Me.txtAA1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAA1.BackColor = System.Drawing.Color.Lavender
        Me.txtAA1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAA1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAA1.Enabled = False
        Me.txtAA1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAA1.ForeColor = System.Drawing.Color.Black
        Me.txtAA1.KeyEnter = True
        Me.txtAA1.Location = New System.Drawing.Point(703, 369)
        Me.txtAA1.Name = "txtAA1"
        Me.txtAA1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAA1.ShortcutsEnabled = False
        Me.txtAA1.Size = New System.Drawing.Size(80, 20)
        Me.txtAA1.TabIndex = 15
        Me.txtAA1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAA1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(1157, 192)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(168, 15)
        Me.BeLabel14.TabIndex = 6
        Me.BeLabel14.Text = "                      Saldo                      "
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(784, 193)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(174, 15)
        Me.BeLabel11.TabIndex = 5
        Me.BeLabel11.Text = "              Valorización Nº 1              "
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(609, 193)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(171, 15)
        Me.BeLabel10.TabIndex = 4
        Me.BeLabel10.Text = "     Avance Anterior Acumulado     "
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.LightSteelBlue
        Me.BeLabel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(318, 193)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(287, 15)
        Me.BeLabel9.TabIndex = 3
        Me.BeLabel9.Text = "                                Presupuesto Base                                "
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(467, 376)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(58, 13)
        Me.BeLabel17.TabIndex = 9
        Me.BeLabel17.Text = "SubTotal"
        '
        'txtTotal
        '
        Me.txtTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotal.BackColor = System.Drawing.Color.Lavender
        Me.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.Enabled = False
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.ForeColor = System.Drawing.Color.Black
        Me.txtTotal.KeyEnter = True
        Me.txtTotal.Location = New System.Drawing.Point(533, 411)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotal.ShortcutsEnabled = False
        Me.txtTotal.Size = New System.Drawing.Size(80, 20)
        Me.txtTotal.TabIndex = 14
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(469, 397)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(37, 12)
        Me.BeLabel16.TabIndex = 11
        Me.BeLabel16.Text = "I.G.V."
        '
        'txtIGV
        '
        Me.txtIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIGV.BackColor = System.Drawing.Color.Lavender
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIGV.Enabled = False
        Me.txtIGV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.ForeColor = System.Drawing.Color.Black
        Me.txtIGV.KeyEnter = True
        Me.txtIGV.Location = New System.Drawing.Point(533, 390)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIGV.ShortcutsEnabled = False
        Me.txtIGV.Size = New System.Drawing.Size(80, 20)
        Me.txtIGV.TabIndex = 12
        Me.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(468, 418)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(36, 13)
        Me.BeLabel15.TabIndex = 13
        Me.BeLabel15.Text = "Total"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotal.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotal.Enabled = False
        Me.txtSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotal.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotal.KeyEnter = True
        Me.txtSubTotal.Location = New System.Drawing.Point(533, 369)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotal.ShortcutsEnabled = False
        Me.txtSubTotal.Size = New System.Drawing.Size(80, 20)
        Me.txtSubTotal.TabIndex = 10
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel47
        '
        Me.BeLabel47.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel47.AutoSize = True
        Me.BeLabel47.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel47.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel47.ForeColor = System.Drawing.Color.Black
        Me.BeLabel47.Location = New System.Drawing.Point(606, 442)
        Me.BeLabel47.Name = "BeLabel47"
        Me.BeLabel47.Size = New System.Drawing.Size(269, 13)
        Me.BeLabel47.TabIndex = 109
        Me.BeLabel47.Text = "Base Para el Saldo Contra Acta de Recepción"
        '
        'txtBaseSaldoActaR
        '
        Me.txtBaseSaldoActaR.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBaseSaldoActaR.BackColor = System.Drawing.Color.Lavender
        Me.txtBaseSaldoActaR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBaseSaldoActaR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBaseSaldoActaR.Enabled = False
        Me.txtBaseSaldoActaR.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBaseSaldoActaR.ForeColor = System.Drawing.Color.Black
        Me.txtBaseSaldoActaR.KeyEnter = True
        Me.txtBaseSaldoActaR.Location = New System.Drawing.Point(878, 435)
        Me.txtBaseSaldoActaR.Name = "txtBaseSaldoActaR"
        Me.txtBaseSaldoActaR.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBaseSaldoActaR.ShortcutsEnabled = False
        Me.txtBaseSaldoActaR.Size = New System.Drawing.Size(80, 20)
        Me.txtBaseSaldoActaR.TabIndex = 110
        Me.txtBaseSaldoActaR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtBaseSaldoActaR.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.DimGray
        Me.GroupBox5.Controls.Add(Me.BeLabel49)
        Me.GroupBox5.Controls.Add(Me.txtTotalFactura)
        Me.GroupBox5.Controls.Add(Me.BeLabel48)
        Me.GroupBox5.Controls.Add(Me.txtIGVFactura)
        Me.GroupBox5.Controls.Add(Me.BeLabel46)
        Me.GroupBox5.Controls.Add(Me.txtSubTotalFactura)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(718, 527)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(270, 80)
        Me.GroupBox5.TabIndex = 116
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Datos de factura"
        '
        'BeLabel49
        '
        Me.BeLabel49.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel49.AutoSize = True
        Me.BeLabel49.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel49.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel49.ForeColor = System.Drawing.Color.Black
        Me.BeLabel49.Location = New System.Drawing.Point(54, 59)
        Me.BeLabel49.Name = "BeLabel49"
        Me.BeLabel49.Size = New System.Drawing.Size(98, 13)
        Me.BeLabel49.TabIndex = 119
        Me.BeLabel49.Text = "Total a Facturar"
        '
        'txtTotalFactura
        '
        Me.txtTotalFactura.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotalFactura.BackColor = System.Drawing.Color.Lavender
        Me.txtTotalFactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotalFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalFactura.Enabled = False
        Me.txtTotalFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalFactura.ForeColor = System.Drawing.Color.Black
        Me.txtTotalFactura.KeyEnter = True
        Me.txtTotalFactura.Location = New System.Drawing.Point(159, 54)
        Me.txtTotalFactura.Name = "txtTotalFactura"
        Me.txtTotalFactura.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotalFactura.ShortcutsEnabled = False
        Me.txtTotalFactura.Size = New System.Drawing.Size(80, 20)
        Me.txtTotalFactura.TabIndex = 120
        Me.txtTotalFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalFactura.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel48
        '
        Me.BeLabel48.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel48.AutoSize = True
        Me.BeLabel48.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel48.ForeColor = System.Drawing.Color.Black
        Me.BeLabel48.Location = New System.Drawing.Point(113, 39)
        Me.BeLabel48.Name = "BeLabel48"
        Me.BeLabel48.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel48.TabIndex = 117
        Me.BeLabel48.Text = "I.G.V."
        '
        'txtIGVFactura
        '
        Me.txtIGVFactura.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIGVFactura.BackColor = System.Drawing.Color.Lavender
        Me.txtIGVFactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGVFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIGVFactura.Enabled = False
        Me.txtIGVFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGVFactura.ForeColor = System.Drawing.Color.Black
        Me.txtIGVFactura.KeyEnter = True
        Me.txtIGVFactura.Location = New System.Drawing.Point(159, 33)
        Me.txtIGVFactura.Name = "txtIGVFactura"
        Me.txtIGVFactura.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIGVFactura.ShortcutsEnabled = False
        Me.txtIGVFactura.Size = New System.Drawing.Size(80, 20)
        Me.txtIGVFactura.TabIndex = 118
        Me.txtIGVFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIGVFactura.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel46
        '
        Me.BeLabel46.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel46.AutoSize = True
        Me.BeLabel46.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel46.ForeColor = System.Drawing.Color.Black
        Me.BeLabel46.Location = New System.Drawing.Point(32, 19)
        Me.BeLabel46.Name = "BeLabel46"
        Me.BeLabel46.Size = New System.Drawing.Size(124, 13)
        Me.BeLabel46.TabIndex = 115
        Me.BeLabel46.Text = "Sub Total a Facturar"
        '
        'txtSubTotalFactura
        '
        Me.txtSubTotalFactura.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotalFactura.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotalFactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotalFactura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotalFactura.Enabled = False
        Me.txtSubTotalFactura.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotalFactura.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotalFactura.KeyEnter = True
        Me.txtSubTotalFactura.Location = New System.Drawing.Point(159, 12)
        Me.txtSubTotalFactura.Name = "txtSubTotalFactura"
        Me.txtSubTotalFactura.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotalFactura.ShortcutsEnabled = False
        Me.txtSubTotalFactura.Size = New System.Drawing.Size(80, 20)
        Me.txtSubTotalFactura.TabIndex = 116
        Me.txtSubTotalFactura.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotalFactura.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'frmValorizaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1364, 668)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.BeLabel47)
        Me.Controls.Add(Me.txtBaseSaldoActaR)
        Me.Controls.Add(Me.BeLabel45)
        Me.Controls.Add(Me.txtAcumuladoIGV)
        Me.Controls.Add(Me.txtAcumuladoTotal)
        Me.Controls.Add(Me.txtAcumuladoSubtotal)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.AxMonthView1)
        Me.Controls.Add(Me.BeLabel24)
        Me.Controls.Add(Me.BeLabel22)
        Me.Controls.Add(Me.BeLabel21)
        Me.Controls.Add(Me.BeLabel12)
        Me.Controls.Add(Me.txtTotPagar)
        Me.Controls.Add(Me.txtAdelanto)
        Me.Controls.Add(Me.txtOtroDes)
        Me.Controls.Add(Me.txtRetPor)
        Me.Controls.Add(Me.BeLabel18)
        Me.Controls.Add(Me.txtSubTotalIGV)
        Me.Controls.Add(Me.txtSubTotalTot)
        Me.Controls.Add(Me.txtSubTotalSaldo)
        Me.Controls.Add(Me.txtSubIGVVal)
        Me.Controls.Add(Me.txtTotalVal)
        Me.Controls.Add(Me.txtSubTotalVal)
        Me.Controls.Add(Me.txtAA2)
        Me.Controls.Add(Me.txtAA3)
        Me.Controls.Add(Me.txtAA1)
        Me.Controls.Add(Me.BeLabel14)
        Me.Controls.Add(Me.BeLabel11)
        Me.Controls.Add(Me.BeLabel10)
        Me.Controls.Add(Me.BeLabel9)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.BeLabel17)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.BeLabel16)
        Me.Controls.Add(Me.txtIGV)
        Me.Controls.Add(Me.BeLabel15)
        Me.Controls.Add(Me.txtSubTotal)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvDetalle)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmValorizaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Valorizaciones"
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgvDescuentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSubTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumeroOrden As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSemana As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNumSesion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboPartidas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtUbicacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroContrato As ctrLibreria.Controles.BeTextBox
    Friend WithEvents chkCerrar As System.Windows.Forms.CheckBox
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigoVal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAA2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAA3 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAA1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSubIGVVal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTotalVal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSubTotalVal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSubTotalIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSubTotalTot As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSubTotalSaldo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel52 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtAdelanto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtOtroDes As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtRetPor As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTotPagar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel24 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboContratista As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpEnvio As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents AxMonthView1 As AxMSComCtl2.AxMonthView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents dgvDescuentos As System.Windows.Forms.DataGridView
    Friend WithEvents txtAdelanto1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAmorti1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAmorti2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAdelanto2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAdelanto3 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAmorti3 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAmorti4 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel30 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel32 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel33 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel34 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel35 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel36 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel37 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel38 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtFG4 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtFG3 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtFG2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtFG1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla4 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla3 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel39 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel40 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel41 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtTotDes As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel26 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents BeLabel27 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Des_Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_Anterior As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_Actual As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_Acumulado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_IdDetalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Des_Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnVerAvanceSemanal As System.Windows.Forms.Button
    Friend WithEvents txtCuadrilla1T As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla3Pre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla2Pre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla1Pre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla3T As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCuadrilla2T As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel31 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCuadrillaTotales As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel44 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel43 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel42 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Und As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Parcial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AA1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AA2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AA3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents V1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents V2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents V3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents vapor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents vametrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MONTacumulado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idDetalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdPartidaDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtAcumuladoIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAcumuladoTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAcumuladoSubtotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel45 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnImprimirSunat As System.Windows.Forms.ToolStripButton
    Friend WithEvents BeLabel47 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtBaseSaldoActaR As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel49 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotalFactura As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel48 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtIGVFactura As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel46 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSubTotalFactura As ctrLibreria.Controles.BeTextBox
End Class
