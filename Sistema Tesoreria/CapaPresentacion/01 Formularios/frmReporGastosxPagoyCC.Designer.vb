<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporGastosxPagoyCC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkTTM = New System.Windows.Forms.CheckBox
        Me.chkTCC = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.rdbDet = New System.Windows.Forms.RadioButton
        Me.rdbRes = New System.Windows.Forms.RadioButton
        Me.cboTipoGasto = New ctrLibreria.Controles.BeComboBox
        Me.cboCentroCosto = New ctrLibreria.Controles.BeComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnBuscar = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkTTM)
        Me.GroupBox1.Controls.Add(Me.chkTCC)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.rdbDet)
        Me.GroupBox1.Controls.Add(Me.rdbRes)
        Me.GroupBox1.Controls.Add(Me.cboTipoGasto)
        Me.GroupBox1.Controls.Add(Me.cboCentroCosto)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox1.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(550, 90)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        '
        'chkTTM
        '
        Me.chkTTM.AutoSize = True
        Me.chkTTM.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkTTM.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTTM.Location = New System.Drawing.Point(490, 15)
        Me.chkTTM.Name = "chkTTM"
        Me.chkTTM.Size = New System.Drawing.Size(49, 16)
        Me.chkTTM.TabIndex = 20
        Me.chkTTM.Text = "Todos"
        Me.chkTTM.UseVisualStyleBackColor = True
        '
        'chkTCC
        '
        Me.chkTCC.AutoSize = True
        Me.chkTCC.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkTCC.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTCC.Location = New System.Drawing.Point(173, 15)
        Me.chkTCC.Name = "chkTCC"
        Me.chkTCC.Size = New System.Drawing.Size(49, 16)
        Me.chkTCC.TabIndex = 19
        Me.chkTCC.Text = "Todos"
        Me.chkTCC.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(98, 13)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Centro de Costo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(263, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(87, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Tipo de Gasto"
        '
        'rdbDet
        '
        Me.rdbDet.AutoSize = True
        Me.rdbDet.Checked = True
        Me.rdbDet.Location = New System.Drawing.Point(99, 64)
        Me.rdbDet.Name = "rdbDet"
        Me.rdbDet.Size = New System.Drawing.Size(70, 17)
        Me.rdbDet.TabIndex = 16
        Me.rdbDet.TabStop = True
        Me.rdbDet.Text = "Detallado"
        Me.rdbDet.UseVisualStyleBackColor = True
        '
        'rdbRes
        '
        Me.rdbRes.AutoSize = True
        Me.rdbRes.Location = New System.Drawing.Point(10, 64)
        Me.rdbRes.Name = "rdbRes"
        Me.rdbRes.Size = New System.Drawing.Size(72, 17)
        Me.rdbRes.TabIndex = 15
        Me.rdbRes.Text = "Resumido"
        Me.rdbRes.UseVisualStyleBackColor = True
        '
        'cboTipoGasto
        '
        Me.cboTipoGasto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoGasto.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoGasto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoGasto.ForeColor = System.Drawing.Color.Black
        Me.cboTipoGasto.FormattingEnabled = True
        Me.cboTipoGasto.KeyEnter = True
        Me.cboTipoGasto.Location = New System.Drawing.Point(266, 32)
        Me.cboTipoGasto.Name = "cboTipoGasto"
        Me.cboTipoGasto.Size = New System.Drawing.Size(273, 21)
        Me.cboTipoGasto.TabIndex = 14
        '
        'cboCentroCosto
        '
        Me.cboCentroCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCentroCosto.BackColor = System.Drawing.Color.Ivory
        Me.cboCentroCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroCosto.ForeColor = System.Drawing.Color.Black
        Me.cboCentroCosto.FormattingEnabled = True
        Me.cboCentroCosto.KeyEnter = True
        Me.cboCentroCosto.Location = New System.Drawing.Point(10, 32)
        Me.cboCentroCosto.Name = "cboCentroCosto"
        Me.cboCentroCosto.Size = New System.Drawing.Size(212, 21)
        Me.cboCentroCosto.TabIndex = 11
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(353, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Al"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(464, 61)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 4
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(237, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Del"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(375, 62)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaFin.TabIndex = 3
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(266, 62)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaIni.TabIndex = 2
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(2009, 7, 15, 0, 0, 0, 0)
        '
        'frmReporGastosxPagoyCC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(565, 99)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "frmReporGastosxPagoyCC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboCentroCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents cboTipoGasto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents rdbRes As System.Windows.Forms.RadioButton
    Friend WithEvents rdbDet As System.Windows.Forms.RadioButton
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkTTM As System.Windows.Forms.CheckBox
    Friend WithEvents chkTCC As System.Windows.Forms.CheckBox
End Class
