Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmReporteValorizacionesSaldos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteValorizacionesSaldos = Nothing
    Public Shared Function Instance() As frmReporteValorizacionesSaldos
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteValorizacionesSaldos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteValorizacionesSaldos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eValorizaciones As clsValorizaciones

    Private Sub frmReporteValorizacionesSaldos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    'ListarValorizacionesxPagar
    Private Sub btnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVer.Click
        Try
            Dim dtTable2 As DataTable
            dtTable2 = New DataTable
            eValorizaciones = New clsValorizaciones
            If rdbSaldoVal.Checked = True Then
                dtTable2 = eValorizaciones.ListarValorizacionesxPagar(62, gEmpresa, gPeriodo)
            End If
            If rdbSaldoValFac.Checked = True Then
                dtTable2 = eValorizaciones.ListarValorizacionesxPagar(63, gEmpresa, gPeriodo)
            End If
            If dtTable2.Rows.Count > 0 Then
                If rdbSaldoVal.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptValorizacionesxPagar.rpt", dtTable2, "TblLibroDiario", "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa)
                End If
                If rdbSaldoValFac.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptValorizacionesxPagarFac2.rpt", dtTable2, "TblLibroDiario", "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa)
                End If
            ElseIf dtTable2.Rows.Count = 0 Then
                Dim mensaje As String = ""
                If rdbSaldoVal.Checked = True Then
                    mensaje = "No se Encontro ningun Comprobante a Cancelar."
                End If
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transacción." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
    End Sub


    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If

            f.CRVisor.SelectionFormula = STRfiltro
            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields
        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class