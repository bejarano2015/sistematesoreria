Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteDocRecibidos

    Public envOpcion As Integer
    Public envTexto As String
    Public envFIni As DateTime
    Public envFFin As DateTime
    Public envCampoOrd As String
    Public envAscDesc As String

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptF As New rptDocRecibidos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteDocRecibidos = Nothing
    Public Shared Function Instance() As frmReporteDocRecibidos
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteDocRecibidos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteDocRecibidos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteDocRecibidos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rptF.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        '1
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Opcion")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = envOpcion
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Texto")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = envTexto
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


        '3
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@xRUC")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmprRuc
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@xDESEMPRESA")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gDesEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaInicio")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = envFIni
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '5
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaFinal")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = envFFin
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '6
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@CampoOrdenar")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = envCampoOrd
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '7
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@AscDesc")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = envAscDesc
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = rptF
    End Sub

End Class