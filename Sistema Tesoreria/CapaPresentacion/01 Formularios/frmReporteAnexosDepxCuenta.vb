Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReporteAnexosDepxCuenta

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAnexosDepxCuenta = Nothing
    Public Shared Function Instance() As frmReporteAnexosDepxCuenta
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteAnexosDepxCuenta
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteAnexosDepxCuenta_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eChequera As clsChequera

    Private Sub frmReporteAnexosDepxCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eChequera = New clsChequera
        cboBancos.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        If eChequera.iNroRegistros > 0 Then
            cboBancos.ValueMember = "IdBanco"
            cboBancos.DisplayMember = "NombreBanco"
            cboBancos.SelectedIndex = -1
        End If

    End Sub

    Private Sub cboBancos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBancos.SelectedIndexChanged
        Dim IdBanco As String = ""
        If (cboBancos.SelectedIndex > -1) Then
            Try
                IdBanco = cboBancos.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuenta.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    cboCuenta.ValueMember = "IdCuenta"
                    cboCuenta.DisplayMember = "NumeroCuenta"
                    cboCuenta.SelectedIndex = -1
                    'lblMoneda.Text = ""
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            'lblMoneda.Text = ""
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If cboBancos.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboBancos.Focus()
            Exit Sub
        End If
        If cboCuenta.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCuenta.Focus()
            Exit Sub
        End If


        Dim x As frmReporteAnexos4 = frmReporteAnexos4.Instance
        x.MdiParent = frmPrincipal
        x.xRepAnexosCuentaDep = "00004"
        'x.RepAnexosIdBancoDep = Trim(cboBancos2.SelectedValue)
        x.xRepAnexosTipoCuenta = Trim(cboCuenta.SelectedValue)
        x.Show()

        
    End Sub
End Class