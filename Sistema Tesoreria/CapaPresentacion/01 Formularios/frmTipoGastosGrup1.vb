Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmTipoGastosGrup1
    Dim WithEvents cmr As CurrencyManager
    Private eTipoGastosG1 As clsTipoGastosG1
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Dim NumFila As Integer
    Dim Fila As DataRow
    Dim iOpcion As Integer

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmTipoGastosGrup1 = Nothing
    Public Shared Function Instance() As frmTipoGastosGrup1
        If frmInstance Is Nothing Then
            frmInstance = New frmTipoGastosGrup1
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmTipoGastosGrup1_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmTipoGastosGrup1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        eTipoGastosG1 = New clsTipoGastosG1

        cboCategoria.DataSource = eTipoGastosG1.fListarCategoria()

        If eTipoGastosG1.iNroRegistros > 0 Then
            cboCategoria.ValueMember = "IdGastosGenerales"
            cboCategoria.DisplayMember = "DescGastosGenerales"
            cboCategoria.SelectedIndex = -1
        End If

        eTipoGastosG1 = New clsTipoGastosG1
        eTempo = New clsPlantTempo


        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1

        cboEstados.DataSource = eTempo.fColEstados
        cboEstados.ValueMember = "Col02"
        cboEstados.DisplayMember = "Col01"
        cboEstados.SelectedIndex = -1
        dgvLista.Focus()
        dgvLista.Width = 426
        dgvLista.Height = 303

    End Sub

    Private Sub mMostrarGrilla(ByVal IdGrupo As String)

        eTipoGastosG1 = New clsTipoGastosG1
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = eTipoGastosG1.fListar(IdGrupo)
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTableLista
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastosG1.iNroRegistros
        If eTipoGastosG1.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If

    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        Panel2.Visible = True
        cboEstados.SelectedIndex = -1
        txtBusqueda.Clear()
        txtBusqueda.Focus()
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        iOpcion = 1
        LimpiarControles()
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False
        txtDescripcion.Enabled = True


        eTipoGastosG1 = New clsTipoGastosG1
        eTipoGastosG1.fCodigo()
        txtCodigo.Text = Trim(eTipoGastosG1.sCodFuturo)
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Timer1.Enabled = True Then
            txtDescripcion.Focus()
        End If
        Timer1.Enabled = False
    End Sub

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtDescripcion.Clear()
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        iOpcion = 2
        VerPosicion()
        txtDescripcion.Enabled = True
        cboEstado.Enabled = True
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        VerPosicion()
        txtDescripcion.Enabled = False
        cboEstado.Enabled = False
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                txtCodigo.Text = Trim(Fila("IdSubGasto").ToString)
                txtDescripcion.Text = Trim(Fila("DescSubGasto").ToString)
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""
        eTempo = New clsPlantTempo
        eTipoGastosG1 = New clsTipoGastosG1
        If Len(txtDescripcion.Text.Trim) > 0 And cboGrupos.SelectedIndex > -1 Then
            If iOpcion = 1 Then
                Mensaje = "Grabar"
            ElseIf iOpcion = 2 Then
                Mensaje = "Actualizar"
            End If
            If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                frmPrincipal.sFlagGrabar = "1"
                If iOpcion = 1 Then
                    eTipoGastosG1.fCodigo()
                    sCodigoRegistro = eTipoGastosG1.sCodFuturo
                    txtCodigo.Text = sCodigoRegistro
                Else
                    sCodigoRegistro = Trim(txtCodigo.Text)
                End If
                iResultado = eTipoGastosG1.fGrabar(iOpcion, sCodigoRegistro, Trim(txtDescripcion.Text), cboEstado.SelectedValue, cboGrupos.SelectedValue)
                'If iResultado > 0 Then
                'mMostrarGrilla()
                'End If
            End If
        Else
            If Len(txtDescripcion.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese la Descripcion", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtDescripcion.Focus()
                Exit Sub
            End If
            'eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
            frmPrincipal.sFlagGrabar = "0"
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iOpcionDel As Integer
            Try
                If dgvLista.Rows.Count > 0 Then
                    If Fila("Estados") = "Activo" Then
                        iOpcionDel = 6
                        Mensaje = "�Desea Anular?"
                    End If
                    If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Trim(Fila("IdSubGasto").ToString) & "   " & Chr(13) & "Descripci�n: " & Trim(Fila("DescSubGasto").ToString), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        eTipoGastosG1 = New clsTipoGastosG1
                        iResultado = eTipoGastosG1.fEliminar(iOpcionDel, Trim(Fila("IdGrupo1").ToString))
                        '---iResultado = eTipoGastosG1.fEliminar(iOpcionDel, Trim(Fila("Idgrupo1").ToString))
                        'If iResultado = 1 Then
                        '------------ 
                        '------------ 
                        '------------ 
                        '    mMostrarGrilla()
                        'End If
                    Else
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
                'MessageBox.Show("vaca")
            End Try
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        ' Else
        'Exit Sub
        'End If
    End Sub

    Private Sub BeLabel7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel7.Click
        Panel2.Visible = False
    End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text.Trim) > 0 Then
            eTipoGastosG1 = New clsTipoGastosG1
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoGastosG1.fBuscaTipoGastos(4, Trim(txtBusqueda.Text), 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastosG1.iNroRegistros
            If eTipoGastosG1.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text.Trim) = 0 Then
            eTipoGastosG1 = New clsTipoGastosG1
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoGastosG1.fBuscaTipoGastos(5, "", 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastosG1.iNroRegistros
            If eTipoGastosG1.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If
    End Sub

    Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
        If (cboEstados.SelectedIndex > -1) Then
            Try
                eTipoGastosG1 = New clsTipoGastosG1
                eTempo = New clsPlantTempo
                dtTable = New DataTable
                Dim sValorEstado As String = ""
                sValorEstado = cboEstados.SelectedValue.ToString
                If sValorEstado <> "CapaDatos.clsPlantTempo" Then
                    dtTable = eTipoGastosG1.fBuscaTipoGastos(7, "", cboEstados.SelectedValue)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastosG1.iNroRegistros
                    If eTipoGastosG1.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        If Me.dgvLista.RowCount > 0 Then
            Dim x As frmTipoGastosGrup1Anexos = frmTipoGastosGrup1Anexos.Instance
            VerPosicion()
            'Dim dtPerUsu As DataTable
            'dtPerUsu = New DataTable
            'eUsuario = New Usuarios
            'dtPerUsu = eUsuario.BuscaUsuario(gEmpresa, glbSisCodigo, Trim(txtCodigo.Text))
            ''dtPerUsu = eUsuario.BuscaUsuario(gEmpresa, glbSisCodigo, trim (txtCodigo.Text )Trim(Fila("UsuCodigo").ToString))
            'If dtPerUsu.Rows.Count = 0 Then 'Microsoft.VisualBasic.IsDBNull(dtPerUsu.Rows(0).Item("Habilita")) = True Then
            '    x.strHabilita = ""
            'ElseIf dtPerUsu.Rows.Count = 1 Then
            '    If Microsoft.VisualBasic.IsDBNull(dtPerUsu.Rows(0).Item("Habilita")) = True Then
            '        x.strHabilita = ""
            '    Else
            '        x.strHabilita = Trim(dtPerUsu.Rows(0).Item("Habilita"))
            '    End If
            '    'x.strHabilita = Trim(dtPerUsu.Rows(0).Item("Habilita"))
            'End If
            'x.strHabilita = Trim(dtPerUsu.Rows(0).Item("Habilita")) 'Trim(Fila("Habilita").ToString)
            x.strCodigoSubGrupo = Trim(Fila("IdSubGasto").ToString)
            x.strTipoGasto = Trim(Fila("DescSubGasto").ToString)
            x.strCodigoGrupoGeneral = Trim(cboCategoria.SelectedValue)
            'x.strCodigoGrupoGeneral = trim(cbocatwgoria)
            'x.strNombreUsuario = Trim(Fila("UsuCodigo").ToString)
            x.Owner = Me
            x.ShowInTaskbar = False
            x.ShowDialog()
        End If

    End Sub

    Private Sub cboCategoria_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCategoria.SelectionChangeCommitted
        If cboCategoria.SelectedIndex <> -1 Then
            Dim IdCategoria As Integer = 0
            IdCategoria = cboCategoria.SelectedValue
            eTipoGastosG1 = New clsTipoGastosG1
            cboGrupos.DataSource = eTipoGastosG1.fListarGrupoXCategoria(IdCategoria)
            If eTipoGastosG1.iNroRegistros > 0 Then
                cboGrupos.ValueMember = "IdGrupo"
                cboGrupos.DisplayMember = "DescGrupoGastos"
                cboGrupos.SelectedIndex = -1
                dgvLista.DataSource = ""
            End If
        End If
    End Sub

    Private Sub cboGrupos_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrupos.SelectionChangeCommitted
        If cboGrupos.SelectedIndex <> -1 Then
            Dim IdGrupo As String = ""
            IdGrupo = cboGrupos.SelectedValue
            mMostrarGrilla(IdGrupo)
        End If
    End Sub

    Private Sub cboGrupos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGrupos.SelectedIndexChanged

    End Sub
End Class
