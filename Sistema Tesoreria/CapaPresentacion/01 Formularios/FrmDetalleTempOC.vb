Imports CapaNegocios
Imports System.Data
Public Class FrmDetalleTempOC

    Private eRegistroDocumento As New clsRegistroDocumento
    Friend FrmRegDocumentos As New frmRegistroDocumento
    Dim sCodOrden As String
    Dim sNumOrden As String
    Dim iColum As Integer
    

    Private Sub FrmDetalleTempOC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            eRegistroDocumento = New clsRegistroDocumento
            FrmRegDocumentos.DT_DetalleOC = New DataTable
            sCodOrden = Trim(FrmRegDocumentos.txtCodOrden.Text)
            sNumOrden = Trim(FrmRegDocumentos.txtNroOrden.Text)

            dgvDetalleOC.DataSource = eRegistroDocumento.TempDetalleOC(gEmpresa, gPeriodo, sCodOrden, sNumOrden)
            dgvDetalleOC.Columns(1).Width = 30
            dgvDetalleOC.Columns(2).Width = 280
            dgvDetalleOC.Columns(1).ReadOnly = True
            dgvDetalleOC.Columns(2).ReadOnly = True
            dgvDetalleOC.Columns(3).ReadOnly = False
            dgvDetalleOC.Columns(4).Width = 55
            dgvDetalleOC.Columns(5).Width = 80
            dgvDetalleOC.Columns(6).Visible = False
            dgvDetalleOC.Columns(7).Visible = False
            dgvDetalleOC.Columns(8).Visible = False
            dgvDetalleOC.Columns(9).Visible = False

            dgvDetalleOC.SelectionMode = DataGridViewSelectionMode.CellSelect
            Call Ajustar_Celdas()

            For i As Integer = 0 To FrmRegDocumentos.dMontosDetTemp.Length - 1
                If FrmRegDocumentos.dMontosDetTemp(i) > 0 Then
                    FrmRegDocumentos.BolCondicion = True
                ElseIf FrmRegDocumentos.dMontosDetTemp(i) <= 0 Then
                    FrmRegDocumentos.BolCondicion = False
                End If
            Next

            If FrmRegDocumentos.BolCondicion = False Then
                ReDim FrmRegDocumentos.dMontosDetTemp(0 To dgvDetalleOC.Rows.Count - 1)
                ReDim FrmRegDocumentos.dCantDetTemp(0 To dgvDetalleOC.Rows.Count - 1)
            End If

            For z As Integer = 0 To FrmRegDocumentos.dMontosDetTemp.Length - 1
                If FrmRegDocumentos.dMontosDetTemp(z) > 0 Then
                    dgvDetalleOC.Rows(z).Cells("Cantidad").Value = FrmRegDocumentos.dCantDetTemp(z)
                    dgvDetalleOC.Rows(z).Cells("Importe").Value = FrmRegDocumentos.dMontosDetTemp(z)
                Else
                    dgvDetalleOC.Rows(z).Cells("Cantidad").Value = 0
                    dgvDetalleOC.Rows(z).Cells("Importe").Value = 0
                End If
                dgvDetalleOC.Rows(z).Cells("sel").Value = "1"
            Next

            Dim dmontoFormReg As Double = FrmRegDocumentos.txtValorCompra.Text
            lblValCompra.Text = Format(dmontoFormReg, "#,##0.00")

            Dim dmonto As Double = 0
            For x As Integer = 0 To dgvDetalleOC.Rows.Count - 1
                If dgvDetalleOC.Rows(x).Cells(0).Value = "1" Then
                    dmonto += Convert.ToDouble(dgvDetalleOC.Rows(x).Cells("Importe").Value)
                End If
            Next
            txtTotalDet.Text = Format(dmonto, "#,##0.00")

            dgvDetalleOC.Focus()
            dgvDetalleOC.CurrentCell = dgvDetalleOC(4, dgvDetalleOC.CurrentRow.Index)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            FrmRegDocumentos.txtImporteTotal.Text = 0
        End Try

    End Sub

    Private Sub Ajustar_Celdas()
        For z As Integer = 0 To dgvDetalleOC.Columns.Count - 1
            If (dgvDetalleOC.Columns(z).Name = "Importe" Or dgvDetalleOC.Columns(z).Name = "Cantidad" Or dgvDetalleOC.Columns(z).Name = "Precio") Then
                dgvDetalleOC.Columns(z).DefaultCellStyle.Format = "n2"
                dgvDetalleOC.Columns(z).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            ElseIf dgvDetalleOC.Columns(z).Name = "Item" Then
                dgvDetalleOC.Columns(z).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            End If
            dgvDetalleOC.Columns(z).SortMode = DataGridViewColumnSortMode.NotSortable
        Next
    End Sub

    Private Sub dgvDetalleOC_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleOC.CellClick
        If e.ColumnIndex = 0 Then
            Dim dMontoDet As Double = 0
            Dim s As String = dgvDetalleOC.Rows(dgvDetalleOC.CurrentRow.Index).Cells(0).Value
            If s = "1" Then
                dgvDetalleOC.Rows(dgvDetalleOC.CurrentRow.Index).Cells("Importe").Value = Format(0, "#,##0.00")
            End If
            For x As Integer = 0 To dgvDetalleOC.Rows.Count - 1
                If dgvDetalleOC.Rows(x).Cells(0).Value = "1" Then
                    dMontoDet += Convert.ToDouble(dgvDetalleOC.Rows(x).Cells("Importe").Value)
                End If
            Next
            txtTotalDet.Text = Format(dMontoDet, "#,##0.00")
        End If
    End Sub

    Private Sub AsignaMontosArray()
        ReDim FrmRegDocumentos.dMontosDetTemp(0 To dgvDetalleOC.Rows.Count - 1)
        For iFilasDet As Integer = 0 To dgvDetalleOC.Rows.Count - 1
            FrmRegDocumentos.dMontosDetTemp(iFilasDet) = Convert.ToDouble(dgvDetalleOC.Rows(iFilasDet).Cells("Importe").Value)
            FrmRegDocumentos.dCantDetTemp(iFilasDet) = Convert.ToDouble(dgvDetalleOC.Rows(iFilasDet).Cells("Cantidad").Value)
        Next
    End Sub

    Private Function SumarDetalleTemp() As Boolean
        Dim dMontos As Double = 0
        Dim IntFila As Integer = dgvDetalleOC.CurrentRow.Index

        'If iColum <> 4 Then
        '    dgvDetalleOC.CurrentCell = dgvDetalleOC(dgvDetalleOC.Columns.Count - 1, IntFila)
        '    dgvDetalleOC.CurrentCell = dgvDetalleOC(0, IntFila)
        'End If

        For xFila As Integer = 0 To dgvDetalleOC.Rows.Count - 1
            If dgvDetalleOC.Rows(xFila).Cells(0).Value = "1" Then
                dMontos += Val(Convert.ToDouble(dgvDetalleOC.Rows(xFila).Cells("Importe").Value))
            End If
        Next
        txtTotalDet.Text = Format(dMontos, "#,##0.00")
        If (Math.Round(dMontos, 2) = CDbl(lblValCompra.Text) Or dMontos = 0) Then
            Return True
        ElseIf (Math.Round(dMontos, 2) > CDbl(lblValCompra.Text) Or Math.Round(dMontos, 2) < CDbl(lblValCompra.Text)) Then
            MessageBox.Show("Los Montos ingresados en el detalle no cuadran con el Valor de Compra de la Factura", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'MsgBox(dMontos)
            Return False
        End If
    End Function

    Private Sub dgvDetalleOC_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalleOC.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                e.SuppressKeyPress = True
            Case Keys.F5
                If SumarDetalleTemp() = False Then Exit Sub
                FrmRegDocumentos.DT_DetalleOC = dgvDetalleOC.DataSource
                Call AsignaMontosArray()
                FrmRegDocumentos.Focus()
                Me.Close()
                FrmRegDocumentos.cboDP.Focus()
            Case Keys.Space
                Dim dMontoDet As Double = 0
                Dim s As String = dgvDetalleOC.Rows(dgvDetalleOC.CurrentRow.Index).Cells(0).Value
                If s = "1" Then
                    dgvDetalleOC.Rows(dgvDetalleOC.CurrentRow.Index).Cells("Importe").Value = Format(0, "#,##0.00")
                End If
                For x As Integer = 0 To dgvDetalleOC.Rows.Count - 1
                    If dgvDetalleOC.Rows(x).Cells(0).Value = "1" Then
                        dMontoDet += Convert.ToDouble(dgvDetalleOC.Rows(x).Cells("Importe").Value)
                    End If
                Next
                txtTotalDet.Text = Format(dMontoDet, "#,##0.00")
        End Select
    End Sub

    Private Sub dgvDetalleOC_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalleOC.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvDetalleOC.CurrentCell.ColumnIndex
        If (columna = 3 Or columna = 4 Or columna = 5) Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvDetalleOC_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleOC.CellValueChanged
        Dim dMontoDet As Double = 0
        Dim dCant As Double = 0
        Dim dPrecio As Double = 0
        Dim dImporte As Double = 0

        If e.ColumnIndex = 3 Or e.ColumnIndex = 4 Then
            dCant = dgvDetalleOC.Rows(dgvDetalleOC.CurrentRow.Index).Cells("Cantidad").Value
            dPrecio = dgvDetalleOC.Rows(dgvDetalleOC.CurrentRow.Index).Cells("Precio").Value
            dImporte = Math.Round(dCant * dPrecio, 2)

            dgvDetalleOC.Rows(dgvDetalleOC.CurrentRow.Index).Cells("Importe").Value = dImporte

        ElseIf e.ColumnIndex = 5 Then
            For x As Integer = 0 To dgvDetalleOC.Rows.Count - 1
                If dgvDetalleOC.Rows(x).Cells(0).Value = "1" Then
                    dMontoDet += Convert.ToDouble(dgvDetalleOC.Rows(x).Cells("Importe").Value)
                End If
            Next
            txtTotalDet.Text = Format(dMontoDet, "#,##0.00")

        End If
    End Sub

    Private Sub dgvDetalleOC_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleOC.CellContentClick

    End Sub
End Class