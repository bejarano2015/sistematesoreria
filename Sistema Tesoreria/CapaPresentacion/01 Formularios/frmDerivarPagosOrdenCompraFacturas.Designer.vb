﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDerivarPagosOrdenCompraFacturas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvLista = New System.Windows.Forms.DataGridView()
        Me.btnDerivar = New System.Windows.Forms.Button()
        Me.Seleccion = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IdRegistro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Moneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.emprcodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Seleccion, Me.IdRegistro, Me.NroDocumento, Me.ImporteTotal, Me.Moneda, Me.FechaDocumento, Me.FechaVencimiento, Me.emprcodigo})
        Me.dgvLista.Location = New System.Drawing.Point(12, 12)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.Size = New System.Drawing.Size(740, 217)
        Me.dgvLista.TabIndex = 0
        '
        'btnDerivar
        '
        Me.btnDerivar.Location = New System.Drawing.Point(352, 235)
        Me.btnDerivar.Name = "btnDerivar"
        Me.btnDerivar.Size = New System.Drawing.Size(75, 23)
        Me.btnDerivar.TabIndex = 1
        Me.btnDerivar.Text = "Derivar"
        Me.btnDerivar.UseVisualStyleBackColor = True
        '
        'Seleccion
        '
        Me.Seleccion.HeaderText = ""
        Me.Seleccion.Name = "Seleccion"
        Me.Seleccion.Width = 40
        '
        'IdRegistro
        '
        Me.IdRegistro.DataPropertyName = "IdRegistro"
        Me.IdRegistro.HeaderText = "IdRegistro"
        Me.IdRegistro.Name = "IdRegistro"
        Me.IdRegistro.ReadOnly = True
        Me.IdRegistro.Visible = False
        '
        'NroDocumento
        '
        Me.NroDocumento.DataPropertyName = "NroDocumento"
        Me.NroDocumento.HeaderText = "N° Documento"
        Me.NroDocumento.Name = "NroDocumento"
        Me.NroDocumento.ReadOnly = True
        Me.NroDocumento.Width = 130
        '
        'ImporteTotal
        '
        Me.ImporteTotal.DataPropertyName = "ImporteTotal"
        Me.ImporteTotal.HeaderText = "Total"
        Me.ImporteTotal.Name = "ImporteTotal"
        Me.ImporteTotal.ReadOnly = True
        '
        'Moneda
        '
        Me.Moneda.DataPropertyName = "Moneda"
        Me.Moneda.HeaderText = "Moneda"
        Me.Moneda.Name = "Moneda"
        Me.Moneda.ReadOnly = True
        '
        'FechaDocumento
        '
        Me.FechaDocumento.DataPropertyName = "FechaDocumento"
        Me.FechaDocumento.HeaderText = "Fec. Documento"
        Me.FechaDocumento.Name = "FechaDocumento"
        Me.FechaDocumento.ReadOnly = True
        Me.FechaDocumento.Width = 140
        '
        'FechaVencimiento
        '
        Me.FechaVencimiento.DataPropertyName = "FechaVencimiento"
        Me.FechaVencimiento.HeaderText = "Fec. Vencimiento"
        Me.FechaVencimiento.Name = "FechaVencimiento"
        Me.FechaVencimiento.ReadOnly = True
        Me.FechaVencimiento.Width = 140
        '
        'emprcodigo
        '
        Me.emprcodigo.DataPropertyName = "emprcodigo"
        Me.emprcodigo.HeaderText = "empresa"
        Me.emprcodigo.Name = "emprcodigo"
        Me.emprcodigo.Visible = False
        '
        'frmDerivarPagosOrdenCompraFacturas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(760, 265)
        Me.Controls.Add(Me.btnDerivar)
        Me.Controls.Add(Me.dgvLista)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDerivarPagosOrdenCompraFacturas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Facturas"
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents btnDerivar As System.Windows.Forms.Button
    Friend WithEvents Seleccion As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IdRegistro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocumento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Moneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDocumento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaVencimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents emprcodigo As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
