<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporGestionDocumentoPagos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rvReporte = New Telerik.ReportViewer.WinForms.ReportViewer()
        Me.SuspendLayout()
        '
        'rvReporte
        '
        Me.rvReporte.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rvReporte.Location = New System.Drawing.Point(0, 0)
        Me.rvReporte.Name = "rvReporte"
        Me.rvReporte.Size = New System.Drawing.Size(1065, 602)
        Me.rvReporte.TabIndex = 0
        '
        'frmReporGestionDocumentoPagos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1065, 602)
        Me.Controls.Add(Me.rvReporte)
        Me.Name = "frmReporGestionDocumentoPagos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte de Gestion de Documentos - Pagos"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rvReporte As Telerik.ReportViewer.WinForms.ReportViewer
End Class
