Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmLibroBancosReporte

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmLibroBancosReporte = Nothing
    Public Shared Function Instance() As frmLibroBancosReporte
        If frmInstance Is Nothing Then
            frmInstance = New frmLibroBancosReporte
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmLibroBancosReporte_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub
    Private Sub frmLibroBancosReporte_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eLibroBancos As clsLibroBancos
    'Private eTempo As clsPlantTempo

    Dim iOpcion As Int16 = 0
    Dim sVisualizarDatos As String = ""

    Private Sub frmLibroBancosReporte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'eTempo = New clsPlantTempo
        'cboOpcion.DataSource = eTempo.fColOpcion
        'cboOpcion.ValueMember = "Col02"
        'cboOpcion.DisplayMember = "Col01"
        'cboOpcion.SelectedIndex = -1
        btnGrabar.Enabled = False
        btnCancelar.Enabled = False
        btnGrabar.Enabled = False
        btnNuevo.Enabled = False
        'btnEliminar.Enabled = False
        'Me.ToolTip1.SetToolTip(Me.btnEliminar, "Eliminar")
        Me.ToolTip1.SetToolTip(Me.btnNuevo, "Nuevo")
        Me.ToolTip1.SetToolTip(Me.btnGrabar, "Grabar")
        Me.ToolTip1.SetToolTip(Me.btnCancelar, "Cancelar")
        'Me.ToolTip1.SetToolTip(Me.btnEliminar, "Eliminar")
        cboAno.Text = Year(Now)
    End Sub

    Private Sub cboMes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMes.SelectedIndexChanged

        Dim dtBusqueda As DataTable
        eLibroBancos = New clsLibroBancos
        dtBusqueda = New DataTable

        dtBusqueda = eLibroBancos.fConsultaCuentasLibros(42, TraerNumeroMes(cboMes.Text), Trim(cboAno.Text), gEmpresa)
        If dgvCuentasyLibros.Rows.Count > 0 Then
            For x As Integer = 0 To dgvCuentasyLibros.RowCount - 1
                dgvCuentasyLibros.Rows.Remove(dgvCuentasyLibros.CurrentRow)
            Next
        End If
        dgvCuentasyLibros.DataSource = dtBusqueda

    End Sub

    Dim m_Excel As Excel.Application

    Private Sub frmExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles frmExcel.Click
        If cboAno.SelectedIndex > -1 And cboMes.SelectedIndex > -1 Then
            Dim Tab1 As New DataTable
            Dim objLibroExcel As Excel.Workbook '' Creamos un objeto WorkBook 
            Dim objHojaExcel As Excel.Worksheet '' Creamos un objeto WorkSheet    
            m_Excel = New Excel.Application
            m_Excel.DisplayFullScreen = False
            objLibroExcel = m_Excel.Workbooks.Add()
            objHojaExcel = objLibroExcel.Worksheets(1)
            objHojaExcel.Name = "LibroBancos"
            objHojaExcel.Visible = Excel.XlSheetVisibility.xlSheetVisible
            objHojaExcel.Activate()


            Dim j As Integer = 1

            Dim nReg As Integer = 0
            Dim dtBusqueda As DataTable
            dtBusqueda = eLibroBancos.fConsultaCuentasLibros(42, TraerNumeroMes(cboMes.Text), Trim(cboAno.Text), gEmpresa) 'ObjCli.ResumenPagoAfp(glbNameBD, glbEmpCodigo, Me.cmbTplanilla.Codigo, Me.cmbAnio.Text, Format(Me.cmbPeriodo.SelectedIndex + 1, "00"), glbUsuCodigo, CodIni, CodFin)

            objHojaExcel.Cells.Font.Name = "arial"
            objHojaExcel.Cells.Font.Size = 7

            '' Seleccionas el rango q quieres combinar
            'objHojaExcel.Range("D2:E2").Select()
            '' Estableces MergeCells a True para combinar
            '' Y false para descombinar las celdas
            'objHojaExcel.Selection.MergeCells = True
            'Dim xlLefth As Integer = 0

            Dim xlCenter As Integer = -4108

            objHojaExcel.Cells(1, "A") = Now.Date
            'objHojaExcel.Range("A1").HorizontalAlignment = -120

            objHojaExcel.Cells(2, "D") = "N� CTA. CTE"
            objHojaExcel.Range("D2:E2").HorizontalAlignment = xlCenter
            objHojaExcel.Range("D2:E2").MergeCells = True
            'objHojaExcel.Selection.MergeCells = True

            objHojaExcel.Cells(2, "F") = "IMPORTE"
            objHojaExcel.Range("F2:G2").HorizontalAlignment = xlCenter
            objHojaExcel.Range("F2:G2").MergeCells = True
            'objHojaExcel.Selection.MergeCells = True

            objHojaExcel.Cells(2, "H") = "PRENDA"
            objHojaExcel.Range("H2:I2").HorizontalAlignment = xlCenter
            objHojaExcel.Range("H2:I2").MergeCells = True
            'objHojaExcel.Selection.MergeCells = True

            objHojaExcel.Cells(2, "J") = "GARANTIA"
            objHojaExcel.Range("J2:K2").HorizontalAlignment = xlCenter
            objHojaExcel.Range("J2:K2").MergeCells = True
            'objHojaExcel.Selection.MergeCells = True

            objHojaExcel.Cells(2, "L") = "DEP. A PLAZO"
            objHojaExcel.Range("L2:M2").HorizontalAlignment = xlCenter
            objHojaExcel.Range("L2:M2").MergeCells = True
            'objHojaExcel.Selection.MergeCells = True

            objHojaExcel.Cells(2, "O") = "RETENCION"
            objHojaExcel.Range("N2:O2").HorizontalAlignment = xlCenter
            objHojaExcel.Range("N2:O2").MergeCells = True
            'objHojaExcel.Selection.MergeCells = True

            objHojaExcel.Cells(3, "A") = "EMPRESAS"
            objHojaExcel.Cells(3, "B") = "BANCOS"
            objHojaExcel.Cells(3, "C") = "CUENTAS"
            objHojaExcel.Cells(3, "D") = "S/."
            objHojaExcel.Cells(3, "E") = "US$"
            objHojaExcel.Cells(3, "F") = "S/."
            objHojaExcel.Cells(3, "G") = "US$"
            objHojaExcel.Cells(3, "H") = "S/."
            objHojaExcel.Cells(3, "I") = "US$"
            objHojaExcel.Cells(3, "J") = "S/."
            objHojaExcel.Cells(3, "K") = "US$"
            objHojaExcel.Cells(3, "L") = "S/."
            objHojaExcel.Cells(3, "M") = "US$"
            objHojaExcel.Cells(3, "N") = "S/."
            objHojaExcel.Cells(3, "O") = "US$"

            objHojaExcel.Range("A3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("B3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("C3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("D3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("E3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("F3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("G3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("H3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("I3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("J3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("K3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("L3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("M3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("N3").HorizontalAlignment = xlCenter
            objHojaExcel.Range("O3").HorizontalAlignment = xlCenter

            Dim i As Integer = 4
            If dtBusqueda.Rows.Count > 0 Then
                nReg = dtBusqueda.Rows.Count
                For Each fila As DataRow In dtBusqueda.Rows
                    objHojaExcel.Cells(i, "A") = fila("Empresa") 'ProductID'
                    objHojaExcel.Cells(i, "B") = fila("Banco") 'ProductID'
                    objHojaExcel.Cells(i, "C") = fila("Descripcion") 'ProductName'
                    objHojaExcel.Cells(i, "D") = fila("CTAS")  'UnitPrice'
                    objHojaExcel.Cells(i, "E") = fila("CTA$")  'UnitPrice'
                    objHojaExcel.Cells(i, "F") = fila("IMPORTES")  'UnitPrice'
                    objHojaExcel.Cells(i, "G") = fila("IMPORTE$")  'UnitPrice'
                    'If IsDBNull(fila("Fecha")) Then
                    '    objHojaExcel.Cells(i, "H") = ""  'UnitPrice'
                    'Else
                    '    objHojaExcel.Cells(i, "H") = fila("Fecha")
                    'End If
                    objHojaExcel.Cells(i, "H") = fila("PRENDAS")  'UnitPrice'
                    objHojaExcel.Cells(i, "I") = fila("PRENDA$")  'UnitPrice'
                    objHojaExcel.Cells(i, "J") = fila("GARANTIAS")  'UnitPrice'
                    objHojaExcel.Cells(i, "K") = fila("GARANTIA$")  'UnitPrice'
                    objHojaExcel.Cells(i, "L") = fila("DEPPLAZOS")  'UnitPrice'
                    objHojaExcel.Cells(i, "M") = fila("DEPPLAZO$")  'UnitPrice'
                    objHojaExcel.Cells(i, "N") = fila("RETENCIONS")  'UnitPrice'
                    objHojaExcel.Cells(i, "O") = fila("RETENCION$")  'UnitPrice'
                    'objHojaExcel.Cells(i, "K") = fila("NUmero")  'UnitPrice'
                    'objHojaExcel.Cells(i, "L") = "C"  'UnitPrice'
                    'Avanzamos una fila
                    i += 1
                Next
            End If

        

            m_Excel.Visible = True
            objHojaExcel = Nothing
            objLibroExcel = Nothing
        Else
            If cboAno.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione el A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboAno.Focus()
            End If

            If cboMes.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione el Mes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboMes.Focus()
            End If

        End If
       
    End Sub


    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click

    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click

    End Sub
End Class