Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmCuentaBanco
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eCuenta As clsCuenta
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Private eChequera As clsChequera

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCuentaBanco = Nothing
    Public Shared Function Instance() As frmCuentaBanco
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmCuentaBanco
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtNumeroCuenta.Clear()
        txtObservacion.Clear()
        cboEstado.SelectedIndex = -1
        txtCodigoBanco.Clear()
        txtDesBanco.Clear()
        txtMoneda.Clear()
        txtDesMoneda.Clear()
        cboTipCtaBanco.SelectedIndex = -1
        cboCtaContable.SelectedIndex = -1
        'Me.cboBanco.SelectedIndex = -1
        'Me.cboMoneda.SelectedIndex = -1
    End Sub

    Private Sub DesabilitarControles()
        'cboBanco.Enabled = False
        'cboMoneda.Enabled = False
        txtNumeroCuenta.Enabled = False
        txtObservacion.Enabled = False
        cboEstado.Enabled = False
        'txtCodigoBanco.Enabled = False
        txtDesBanco.Enabled = False
        'txtMoneda.Enabled = False
        'txtDesMoneda.Enabled = False
        cboTipCtaBanco.Enabled = False
        cboCtaContable.Enabled = False
        cboTipoCuenta.Enabled = False
        chkLibro.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        'cboBanco.Enabled = True
        'cboMoneda.Enabled = True
        txtNumeroCuenta.Enabled = True
        txtObservacion.Enabled = True
        cboEstado.Enabled = True

        'txtCodigoBanco.Enabled = False
        txtDesBanco.Enabled = True
        'txtMoneda.Enabled = False
        'txtDesMoneda.Enabled = True
        cboTipCtaBanco.Enabled = True
        cboCtaContable.Enabled = True
        cboTipoCuenta.Enabled = True
        chkLibro.Enabled = True

    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If
        Panel2.Visible = True
        txtNumero.Clear()
        cboMonedas.SelectedIndex = -1
        cboBancos.SelectedIndex = -1
        'cboBancos.Focus()
        txtNumero.Focus()
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            cboEstado.SelectedIndex = 0
            cboEstado.Enabled = False
            cboTipoCuenta.SelectedIndex = -1
            chkLibro.Checked = False
            chkAnexos.Checked = False
            txtDesBanco.Focus()
            'Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            HabilitarControles()
            cboEstado.Enabled = True
            'Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        ' If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        'Else
        'Exit Sub
        ' End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""
        Dim EstadoLibro As Integer = 0
        Dim EstadoCargo As Integer = 0

        If chkLibro.Checked = True Then
            EstadoLibro = 1
        ElseIf chkLibro.Checked = False Then
            EstadoLibro = 0
        End If

        If chkAnexos.Checked = True Then
            EstadoCargo = 1
        ElseIf chkAnexos.Checked = False Then
            EstadoCargo = 0
        End If

        eTempo = New clsPlantTempo
        eCuenta = New clsCuenta
        If sTab = 1 Then
            If Len(txtCodigoBanco.Text.Trim) > 0 And Len(Trim(txtNumeroCuenta.Text)) > 0 And Len(Trim(txtMoneda.Text)) > 0 And Len(Trim(txtObservacion.Text)) > 0 And cboTipCtaBanco.SelectedIndex <> -1 And cboTipoCuenta.SelectedIndex <> -1 Then
                If iOpcion = 1 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 2 Then
                    Mensaje = "Actualizar"
                End If


                If EstadoCargo = 1 Then
                    Dim dtCuentaEn As DataTable
                    dtCuentaEn = New DataTable
                    eChequera = New clsChequera
                    dtCuentaEn = eChequera.fListarCuentasEncargada(gEmpresa, Trim(txtCodigoBanco.Text))
                    If dtCuentaEn.Rows.Count = 1 Then
                        Dim NumeroCuentaEncargada As String = ""
                        NumeroCuentaEncargada = dtCuentaEn.Rows(0).Item("NumeroCuenta")
                        MessageBox.Show("Los Resumenes de Anexos en este grupo ya se encuentran asignados a la cuenta " & NumeroCuentaEncargada, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    ElseIf dtCuentaEn.Rows.Count = 0 Then
                        
                    End If
                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"

                    Dim CtaCotble As String = ""
                    If cboCtaContable.SelectedIndex = -1 Then
                        CtaCotble = ""
                    ElseIf cboCtaContable.SelectedIndex > -1 Then
                        CtaCotble = Trim(cboCtaContable.SelectedValue)
                    End If

                    If iOpcion = 1 Then
                        eCuenta.fCodigo(gEmpresa)
                        sCodigoRegistro = eCuenta.sCodFuturo
                        Me.txtCodigo.Text = sCodigoRegistro
                    Else
                        sCodigoRegistro = Me.txtCodigo.Text
                    End If
                    iResultado = eCuenta.fGrabar(sCodigoRegistro, Trim(txtCodigoBanco.Text), Me.txtNumeroCuenta.Text.Trim, Trim(txtMoneda.Text), Me.txtObservacion.Text.Trim, gEmpresa, Me.cboEstado.SelectedValue, iOpcion, Trim(cboTipCtaBanco.SelectedValue), CtaCotble, EstadoLibro, Trim(cboTipoCuenta.SelectedValue), EstadoCargo)
                    If iResultado > 0 Then
                        mMostrarGrilla()
                        ' Timer2.Enabled = True
                    End If
                    sTab = 0
                End If
            Else
                If Len(txtCodigoBanco.Text.Trim) = 0 Then
                    MessageBox.Show("Debe Ingresar un C�digo de Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDesBanco.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtNumeroCuenta.Text)) = 0 Then
                    MessageBox.Show("Ingrese un N�mero de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNumeroCuenta.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtMoneda.Text)) = 0 Then
                    MessageBox.Show("Debe Ingresar un C�digo de Moneda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDesBanco.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtObservacion.Text)) = 0 Then
                    MessageBox.Show("Ingrese una Observacion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtObservacion.Focus()
                    Exit Sub
                End If

                If cboTipCtaBanco.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Cuenta Contable", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipCtaBanco.Focus()
                    Exit Sub
                End If

                If cboTipoCuenta.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoCuenta.Focus()
                    Exit Sub
                End If


                'If cboCtaContable.SelectedIndex = -1 Then
                '    MessageBox.Show("Seleccione una Cuenta Contable", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    cboTipCtaBanco.Focus()
                '    Exit Sub
                'End If

                'eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iActivo As Integer
            'If sTab = 0 Then
            Try
                If dgvLista.Rows.Count > 0 Then
                    If Fila("Estados") = "Activo" Then
                        iActivo = 5
                        Mensaje = "�Desea Anular?"
                    End If
                    If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("Idcuenta") & "   " & Chr(13) & "Descripci�n: " & Fila("numerocuenta"), "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        eCuenta = New clsCuenta
                        iResultado = eCuenta.fEliminar(Fila("Idcuenta"), iActivo, gEmpresa)
                        If iResultado = 1 Then
                            mMostrarGrilla()
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
            End Try
            'End If
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()
        'Me.cboBanco.DataSource = eCuenta.fListarBanco(gEmpresa)
        'Me.cboBanco.ValueMember = "IdBanco"
        'Me.cboBanco.DisplayMember = "NombreBanco"
        'cboBanco.SelectedIndex = -1

        Me.cboBancos.DataSource = eCuenta.fListarBanco(gEmpresa)
        Me.cboBancos.ValueMember = "IdBanco"
        Me.cboBancos.DisplayMember = "NombreBanco"
        cboBancos.SelectedIndex = -1

        'Me.cboMoneda.DataSource = eCuenta.fListarMoneda
        'Me.cboMoneda.ValueMember = "MonCodigo"
        'Me.cboMoneda.DisplayMember = "MonDescripcion"
        'cboMoneda.SelectedIndex = -1

        Me.cboMonedas.DataSource = eCuenta.fListarMoneda
        Me.cboMonedas.ValueMember = "MonCodigo"
        Me.cboMonedas.DisplayMember = "MonDescripcion"
        cboMonedas.SelectedIndex = -1

        cboTipCtaBanco.DataSource = eCuenta.fListarTipCuentaBanco()
        cboTipCtaBanco.ValueMember = "CodTipoCuentaBan"
        cboTipCtaBanco.DisplayMember = "DesTipoCuentaBan"
        cboTipCtaBanco.SelectedIndex = -1

        cboCtaContable.DataSource = eCuenta.fListarCtaContable(Trim(gPeriodo), gEmpresa)
        cboCtaContable.ValueMember = "cuentacodigo"
        cboCtaContable.DisplayMember = "Descr"
        cboCtaContable.SelectedIndex = -1

        cboTipoCuenta.DataSource = eCuenta.fListarTipoCtaContable()
        cboTipoCuenta.ValueMember = "IdTipoCuenta"
        cboTipoCuenta.DisplayMember = "Descripcion"
        cboTipoCuenta.SelectedIndex = -1

        Me.cboEstado.DataSource = eTempo.fColEstado
        Me.cboEstado.ValueMember = "Col02"
        Me.cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        dgvLista.Focus()
        dgvLista.Width = 1024
        dgvLista.Height = 402


    End Sub

    Private Sub mMostrarGrilla()
        eCuenta = New clsCuenta
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eCuenta.fListar(iEstado01, iEstado02, gEmpresa)
        Me.dgvLista.AutoGenerateColumns = False
        Me.dgvLista.DataSource = dtTable
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        'VerPosicion()
        Me.stsTotales.Items(0).Text = "Total de Registros= " & eCuenta.iNroRegistros
        If eCuenta.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
        
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Trim(Fila("IdCuenta").ToString)
                Me.txtNumeroCuenta.Text = Trim(Fila("NumeroCuenta").ToString)
                Me.txtObservacion.Text = Trim(Fila("Observacion").ToString)
                txtCodigoBanco.Text = Trim(Fila("IdBanco").ToString)
                txtMoneda.Text = Trim(Fila("IdMoneda").ToString)
                cboTipCtaBanco.SelectedValue = Trim(Fila("TipoCtaBank").ToString)
                cboCtaContable.SelectedValue = Trim(Fila("cuentacodigo").ToString)

                txtDesBanco.Text = Trim(Fila("Banco").ToString)
                txtDesMoneda.Text = Trim(Fila("Moneda").ToString)

                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If

                If Fila("EstadoLibro").ToString = 0 Then
                    chkLibro.Checked = False
                Else
                    chkLibro.Checked = True
                End If

                If Fila("EstadoCargo").ToString = 0 Then
                    chkAnexos.Checked = False
                Else
                    chkAnexos.Checked = True
                End If

                'MessageBox.Show(Trim(Fila("TipoCuenta").ToString))
                cboTipoCuenta.SelectedValue = Trim(Fila("TipoCuenta").ToString)
                If Len(Trim(Fila("TipoCuenta").ToString)) = 0 Then
                    cboTipoCuenta.SelectedIndex = -1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    'Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
    '    Try
    '        VerPosicion()
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtNumeroCuenta.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub


    Private Sub BeLabel10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel10.Click
        Panel2.Visible = False
    End Sub

    Private Sub txtNumero_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNumero.TextChanged

        'cboBancos.SelectedIndex = -1
        'cboMonedas.SelectedIndex = -1
        'cboBancos.DataSource = Nothing
        'cboMonedas.DataSource = Nothing
        If Len(Trim(txtNumero.Text)) > 0 Then
            eCuenta = New clsCuenta
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eCuenta.fBuscarCuentasBanco(1, gEmpresa, "", Trim(txtNumero.Text), "", 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eCuenta.iNroRegistros
            If eCuenta.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(Trim(txtNumero.Text)) = 0 Then
            eCuenta = New clsCuenta
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eCuenta.fBuscarCuentasBanco(4, gEmpresa, "", "", "", 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eCuenta.iNroRegistros
            If eCuenta.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If

    End Sub

    Private Sub txtNumeroCuenta_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumeroCuenta.KeyPress
        'en la siguiente l�nea de c�digo se comprueba si el caracter es d�gito...
        If (Not Char.IsDigit(e.KeyChar)) Then
            Dim caracter As Char = e.KeyChar
            If e.KeyChar.ToString <> "-" And (caracter = ChrW(Keys.Space)) = False Then
                If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                    e.Handled = True 'esto invalida la tecla pulsada
                End If
            End If

        End If
    End Sub

    Private Sub txtDesBanco_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDesBanco.DoubleClick
        dtTable = New DataTable
        dtTable = eCuenta.fListarBancos(gEmpresa) 'Bancos
        If dtTable.Rows.Count > 0 Then
            dgBancos.DataSource = dtTable
            Panel3.Visible = True
            dgBancos.Focus()
        End If
    End Sub

    Private Sub BeTextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDesBanco.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                dtTable = New DataTable
                dtTable = eCuenta.fListarBancos(gEmpresa) 'Bancos
                If dtTable.Rows.Count > 0 Then
                    dgBancos.DataSource = dtTable
                    Panel3.Visible = True
                    dgBancos.Focus()
                End If
        End Select
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        Panel3.Visible = False
    End Sub

    Private Sub dgBancos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgBancos.DoubleClick

        Try
            txtCodigoBanco.Text = Trim(dgBancos.Rows(dgBancos.CurrentRow.Index).Cells("Column5").Value)
            txtDesBanco.Text = Trim(dgBancos.Rows(dgBancos.CurrentRow.Index).Cells("Column6").Value)
            txtMoneda.Text = Trim(dgBancos.Rows(dgBancos.CurrentRow.Index).Cells("Column9").Value)
            txtDesMoneda.Text = Trim(dgBancos.Rows(dgBancos.CurrentRow.Index).Cells("Column10").Value)
            Panel3.Visible = False
            txtNumeroCuenta.Focus()
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub


    Private Sub cboMonedas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMonedas.SelectionChangeCommitted
        Dim IdMoneda As String = ""
        If cboMonedas.SelectedIndex > -1 Then
            cboBancos.SelectedIndex = -1
            txtNumero.Clear()
            Try
                IdMoneda = cboMonedas.SelectedValue
                If IdMoneda <> "System.Data.DataRowView" Then
                    '----------------------------->
                    eCuenta = New clsCuenta
                    eTempo = New clsPlantTempo
                    dtTable = New DataTable
                    'If cboBancos.SelectedIndex = -1 Then
                    dtTable = eCuenta.fBuscarCuentasBanco(2, gEmpresa, "", "", IdMoneda, 0)
                    'ElseIf cboBancos.SelectedIndex > -1 Then
                    'dtTable = eCuenta.fBuscarCuentasBanco(3, gEmpresa, Trim(cboBancos.SelectedValue), "", IdMoneda, 0)
                    'Else
                    'Exit Sub
                    'End If
                    Me.dgvLista.AutoGenerateColumns = False
                    Me.dgvLista.DataSource = dtTable
                    cmr = Me.BindingContext(Me.dgvLista.DataSource)
                    Me.stsTotales.Items(0).Text = "Total de Registros= " & eCuenta.iNroRegistros
                    If eCuenta.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                    '----------------------------->
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub cboBancos_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBancos.SelectionChangeCommitted
        Dim IdBanco As String = ""
        If (cboBancos.SelectedIndex > -1) Then
            cboMonedas.SelectedIndex = -1
            txtNumero.Clear()
            Try
                IdBanco = cboBancos.SelectedValue
                If IdBanco <> "System.Data.DataRowView" Then
                    '----------------------------->
                    eCuenta = New clsCuenta
                    eTempo = New clsPlantTempo
                    dtTable = New DataTable
                    dtTable = eCuenta.fBuscarCuentasBanco(0, gEmpresa, IdBanco, "", "", 0)
                    Me.dgvLista.AutoGenerateColumns = False
                    Me.dgvLista.DataSource = dtTable
                    cmr = Me.BindingContext(Me.dgvLista.DataSource)
                    Me.stsTotales.Items(0).Text = "Total de Registros= " & eCuenta.iNroRegistros
                    If eCuenta.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                    '----------------------------->
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub txtDesBanco_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDesBanco.TextChanged

    End Sub

    Private Sub dgBancos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgBancos.CellContentClick

    End Sub
End Class
