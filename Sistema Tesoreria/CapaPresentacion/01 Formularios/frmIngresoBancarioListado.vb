Option Explicit On
Imports System.Xml
Imports MSXML2

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad

Public Class frmIngresoBancarioListado

    Private eGastosGenerales As clsGastosGenerales
    Private eIngresoBancario As clsIngresoBancario
    Dim odtIngresoBancario As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared decImporte As Decimal
    Dim strAccion As String

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eIngresoBancario = New clsIngresoBancario
        eGastosGenerales = New clsGastosGenerales

        tabMantenimiento.SelectedTab = tabLista
        botonesMantenimiento(mnuMantenimiento, True)

        InitCombos()
        CargarIngresoBancario()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarIngresoBancario()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub cboBanco_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboBanco.SelectedIndexChanged
        Dim IdBanco As String = ""
        If (cboBanco.SelectedIndex > -1) Then
            Try
                IdBanco = cboBanco.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa2(gEmpresa, IdBanco)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCuenta.ValueMember = "IdCuenta"
                        cboCuenta.DisplayMember = "NumeroCuenta"
                        cboCuenta.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboCuenta.DataSource = Nothing
        End If
    End Sub

    Private Sub cboCuenta_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCuenta.SelectedIndexChanged
        Dim IdCuenta As String
        Dim dtCodMoneda As DataTable
        Dim sDescripcionMoneda As String
        If (cboCuenta.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuenta.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtCodMoneda = New DataTable
                    dtCodMoneda = eGastosGenerales.fTraerCodMonedaCuenta(IdCuenta, gEmpresa)
                    If dtCodMoneda.Rows.Count > 0 Then
                        sDescripcionMoneda = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonDescripcion")))
                        'sMonCodigo = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonCodigo")))
                        txtCodMoneda.Text = Trim(sDescripcionMoneda)
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            txtCodMoneda.Clear()
        End If
        'cboTipoPago.SelectedIndex = -1
        txtNroOperacion.Text = ""
        txtImporte.Text = ""
    End Sub

#End Region

#Region " M�todos privados "

    Private Sub InitCombos()
        cboBanco.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboBanco.ValueMember = "IdBanco"
            cboBanco.DisplayMember = "NombreBanco"
            cboBanco.SelectedIndex = -1
        End If
    End Sub

    Private Sub CargarIngresoBancario()
        Try
            'Dim odtData As New DataTable
            odtIngresoBancario = eIngresoBancario.fCargarIngresoBancarioListado(gEmpresa)
            gvIngresoBancario.DataSource = odtIngresoBancario
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        tabMantenimiento.SelectedTab = tabDetalle
        strAccion = Accion.Grabar
        LimpiarControles()
        dtpFechaDeposito.Value = Date.Today
        txtNroOperacion.Focus()
    End Sub

    Private Sub gvIngresoBancario_CurrentCellChanged(sender As Object, e As EventArgs) Handles gvIngresoBancario.CurrentCellChanged
        Try
            dtpFechaDeposito.Value = Convert.ToDateTime(gvIngresoBancario.CurrentRow.Cells("FechaDeposito").Value)
            cboBanco.SelectedValue = Convert.ToString(gvIngresoBancario.CurrentRow.Cells("IdBanco").Value)
            cboCuenta.SelectedValue = Convert.ToString(gvIngresoBancario.CurrentRow.Cells("IdCtaCorriente").Value)
            txtNroOperacion.Text = Convert.ToString(gvIngresoBancario.CurrentRow.Cells("NroOperacion").Value)
            txtImporte.Text = Convert.ToString(gvIngresoBancario.CurrentRow.Cells("Importe").Value)
            txtGlosa.Text = Convert.ToString(gvIngresoBancario.CurrentRow.Cells("Glosa").Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub gvIngresoBancario_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles gvIngresoBancario.CellDoubleClick
        If gvIngresoBancario.RowCount > 0 Then
            tabMantenimiento.SelectedTab = tabDetalle
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        tabMantenimiento.SelectedTab = tabLista
        CargarIngresoBancario()
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        'Se valida los campos
        If cboBanco.SelectedIndex = -1 Then MessageBox.Show("Seleccione un banco.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : cboBanco.Focus() : Exit Sub
        If cboCuenta.SelectedIndex = -1 Then MessageBox.Show("Seleccione una cuenta bancaria.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : cboCuenta.Focus() : Exit Sub
        If txtNroOperacion.Text.Trim.Length = 0 Then MessageBox.Show("El n�mero de operaci�n es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtNroOperacion.Focus() : Exit Sub
        If txtImporte.Text.Trim.Length = 0 Then MessageBox.Show("El importe es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtNroOperacion.Focus() : Exit Sub

        Select Case strAccion
            Case Accion.Grabar
                Guardar()
            Case Accion.Modifica
                Modificar()
        End Select

        CargarIngresoBancario()
        tabMantenimiento.SelectedTab = tabLista
    End Sub

    Private Sub tabMantenimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabMantenimiento.SelectedIndexChanged
        If tabMantenimiento.SelectedIndex = 0 Then
            strAccion = Accion.Grabar
            botonesMantenimiento(mnuMantenimiento, True)
        Else
            strAccion = Accion.Modifica
            botonesMantenimiento(mnuMantenimiento, False)
        End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If gvIngresoBancario.RowCount > 0 Then
            If Not Eliminar() Then Exit Sub
            CargarIngresoBancario()
        Else
            MessageBox.Show("Seleccione registro a eliminar. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

#Region " Metodos Privados "

    Private Sub LimpiarControles()
        cboBanco.SelectedIndex = -1 : cboCuenta.SelectedIndex = -1 : txtNroOperacion.Text = "" : txtImporte.Text = "" : txtGlosa.Text = ""
    End Sub

#End Region

#Region " M�todos de BD "

    Private Sub Guardar()
        Try
            Dim iResultado As Int32
            iResultado = eIngresoBancario.fGrabarListado(0, gEmpresa, Convert.ToDateTime(dtpFechaDeposito.Value), Convert.ToString(cboBanco.SelectedValue), Convert.ToString(cboCuenta.SelectedValue), txtNroOperacion.Text.Trim, txtImporte.Text.Trim, txtGlosa.Text.Trim, 1)
        Catch ex As Exception
            MessageBox.Show("No se pudo grabar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            Dim iResultado As Int32

            iResultado = eIngresoBancario.fGrabarListado(Convert.ToDecimal(gvIngresoBancario.CurrentRow.Cells("IdIngresoBancario").Value), gEmpresa, Convert.ToDateTime(dtpFechaDeposito.Value), Convert.ToString(cboBanco.SelectedValue), Convert.ToString(cboCuenta.SelectedValue), txtNroOperacion.Text.Trim, txtImporte.Text.Trim, txtGlosa.Text.Trim, 2)
        Catch ex As Exception
            MessageBox.Show("No se pudo modificar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function Eliminar() As Boolean
        Try
            If MessageBox.Show("�Est� seguro que desea eliminar este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then

                Dim iResultado As Int32
                iResultado = eIngresoBancario.fGrabarListado(Convert.ToDecimal(gvIngresoBancario.CurrentRow.Cells("IdIngresoBancario").Value), gEmpresa, Convert.ToDateTime(dtpFechaDeposito.Value), Convert.ToString(cboBanco.SelectedValue), Convert.ToString(cboCuenta.SelectedValue), txtNroOperacion.Text.Trim, txtImporte.Text.Trim, txtGlosa.Text.Trim, 3)

                Return True
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo eliminar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

#End Region

End Class