Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Windows.Forms


Public Class FrmUsuarioMenu
    Friend frm As New frmUsuario

    'Private strCodigoPadron As String
    Public strHabilita As String 'Opciones cadena 200
    Public strUsuario As String
    Dim dtTable As DataTable
    Private eUsuario As Usuarios

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As FrmUsuarioMenu = Nothing

    Public Shared Function Instance() As FrmUsuarioMenu
        If frmInstance Is Nothing Then
            frmInstance = New FrmUsuarioMenu
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub FrmUsuarioMenu_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub FrmUsuarioMenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListaPermisoMenu()
        lblUser.Text = " Usuario : " & strUsuario
        'HabilitaBotones(False)
        For x As Integer = 0 To DgvLista.Columns.Count - 1
            'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
            DgvLista.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
        Next
    End Sub

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

    Sub ListaPermisoMenu()
        Try
            'Dim ds As DataSet

            dtTable = New DataTable

            eUsuario = New Usuarios

            dtTable = eUsuario.ListaPermisoMenu(gEmpresa, glbSisCodigo, strUsuario)

            'ds = ObjCli.ListaPermisoMenu(gEmpresa, glbSisCodigo, strUsuario)
            'TAB = ds.Tables(0)

            Me.DgvLista.AutoGenerateColumns = False
            Me.DgvLista.DataSource = dtTable 'TAB()
            Me.DgvLista.ReadOnly = False
            Me.DgvLista.Columns(0).ReadOnly = True
            Me.DgvLista.Columns(1).ReadOnly = True
            Me.DgvLista.Columns(2).ReadOnly = True

            ActualizaValoresMenu()

            'cmr = Me.BindingContext(Me.DgvLista.DataSource)

            'VerPosicion()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try

    End Sub

    Sub ActualizaValoresMenu()
        Dim n As Integer
        Dim Cadena As String
        Me.DgvLista.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                n = 1
                For Each fila As DataGridViewRow In Me.DgvLista.Rows
                    Cadena = Trim(Mid(strHabilita, n, 1))
                    If Cadena <> "" Then
                        If Cadena = "1" Then
                            fila.Cells("Column3").Value = True
                        Else
                            fila.Cells("Column3").Value = False
                        End If
                    End If
                    n = n + 1
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.DgvLista.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                For Each fila As DataGridViewRow In Me.DgvLista.Rows
                    If Me.CheckBox1.Checked = True Then
                        fila.Cells("Column3").Value = True
                    Else
                        fila.Cells("Column3").Value = False
                    End If
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    'Sub CargarFila(ByVal NumFila As Long)
    '    Try
    '        If Tab.Rows.Count > 0 Then
    '            Fila = Tab.Rows(NumFila)
    '        End If

    '    Catch ex As System.IndexOutOfRangeException
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
    '        Exit Sub
    '    End Try
    'End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim Habilita As String
        ' Dim x As Integer

        If MsgBox("Esta seguro de aplicar los cambios?", MsgBoxStyle.YesNo, glbNameSistema) = MsgBoxResult.No Then
            Exit Sub
        End If

        Me.DgvLista.ClearSelection()
        Habilita = ""

        'For Each fila As DataGridViewRow In Me.DgvLista.Rows
        '    If fila.Cells("Column1").Value Is Nothing _
        '    OrElse fila.Cells Is Nothing Then
        '        Me.CargarFila(0)
        '        Continue For
        '    End If
        '    Habilita = Habilita & Val(fila.Cells("Column4").Value.ToString)
        'Next

        'If DgvLista.Rows.Count > 0 Then
        '    For z As Integer = 1 To DgvLista.Rows.Count '- 1
        '        If Microsoft.VisualBasic.IsDBNull(DgvLista.Rows(z).Cells(3).Value) = True Or Convert.ToBoolean(DgvLista.Rows(z).Cells(3).Value) = False Then
        '            Habilita = Habilita & "0"
        '        ElseIf Convert.ToBoolean(DgvLista.Rows(z).Cells(3).Value) = True Then
        '            Habilita = Habilita & "1"
        '        End If
        '    Next
        'End If


        For z As Integer = 0 To DgvLista.Rows.Count - 1
            If Microsoft.VisualBasic.IsDBNull(DgvLista.Rows(z).Cells(3).Value) = True Then
                Habilita = Habilita & "0"
            Else
                If Convert.ToBoolean(DgvLista.Rows(z).Cells(3).Value) = False Then
                    Habilita = Habilita & "0"
                Else
                    Habilita = Habilita & "1"
                End If
            End If

        Next

        eUsuario = New Usuarios

        eUsuario.ActualizaPermisoMenu(gEmpresa, glbSisCodigo, strUsuario, Habilita)
        'strHabilita = Habilita
        'ListaPermisoMenu()

        'If x <> 1 Then
        'MsgBox("Hubo un error en la actualizacion!", MsgBoxStyle.Information, glbNameSistema)
        'Exit Sub
        'End If
        frm.mMostrarGrilla()
        'Close()
        Me.Close()

    End Sub
End Class