Imports CapaNegocios
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows

Public Class frmConsultaPolizasCuotas
    Private cSegurosPolizas As clsSegurosPolizas
    Public Event SalidaReg()
    Public Event CancelarReg()
    Dim DT_Lista As DataTable
    Dim DTable As DataTable

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaPolizasCuotas = Nothing

    Public Shared Function Instance() As frmConsultaPolizasCuotas
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaPolizasCuotas
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmConsultaPolizasCuotas_CancelarReg() Handles Me.CancelarReg
        Timer1.Enabled = True
    End Sub
    Private Sub frmConsultaPolizasCuotas_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub rbPolizas_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles rbPolizas.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                chkVencidas.Focus()
        End Select
    End Sub
    Private Sub rbCuotas_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles rbCuotas.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtPoliza.Focus()
        End Select
    End Sub
    Private Sub txtPoliza_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPoliza.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                chkVencidas.Focus()
        End Select
    End Sub
    Private Sub chkVencidas_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles chkVencidas.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                chkPorVencer.Focus()
        End Select
    End Sub
    Private Sub chkPorVencer_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles chkPorVencer.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtDias.Focus()
        End Select
    End Sub
    Private Sub rbPolizas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbPolizas.CheckedChanged
        txtPoliza.Text = ""
        txtPoliza.Enabled = False
    End Sub
    Private Sub rbCuotas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCuotas.CheckedChanged
        txtPoliza.Text = ""
        txtPoliza.Enabled = True
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        rbPolizas.Focus()
        Timer1.Enabled = False
    End Sub
    Private Sub frmConsultaPolizasCuotas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DT_Lista = New DataTable
        Timer1.Enabled = True
        lblTotalRegistro.Text = "Total Registros : 0"
    End Sub
    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Dim sOpcion As String
        Dim sOpVencidos As String
        DTable = New DataTable
        cSegurosPolizas = New clsSegurosPolizas


        If rbCuotas.Checked = True Then
            'If txtPoliza.Text = "" Then
            '    MessageBox.Show("Debe ingresar el Numero de Poliza", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    txtPoliza.Focus()
            '    Exit Sub
            'End If
        ElseIf txtDias.Text = "" Then
            MessageBox.Show("Debe ingresar el Parametro Dias", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtDias.Focus()
            Exit Sub
        End If
        If chkVencidas.Checked = False And chkPorVencer.Checked = False Then
            MessageBox.Show("Elija las casillas de verificacion", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            chkPorVencer.Focus()
            Exit Sub
        End If

        sOpVencidos = ""

        If (chkVencidas.Checked = True And chkPorVencer.Checked = True) Then
            sOpVencidos = "3"
        ElseIf chkVencidas.Checked = True Then
            sOpVencidos = "1"
        ElseIf chkPorVencer.Checked = True Then
            sOpVencidos = "2"
        End If

        If rbPolizas.Checked = True Then
            sOpcion = "A"
            DTable = cSegurosPolizas.fPolizas_Vencidos_PorVencer(gEmpresa, "", txtDias.Text, sOpcion, sOpVencidos)
            Try
                dgvLista2.DataSource = DTable
            Catch ex As Exception

            End Try

        ElseIf rbCuotas.Checked = True Then
            sOpcion = "B"
            DTable = cSegurosPolizas.fPolizas_Vencidos_PorVencer(gEmpresa, txtPoliza.Text.Trim, txtDias.Text, sOpcion, sOpVencidos)

            Try
                dgvLista2.DataSource = DTable
            Catch ex As Exception

            End Try
        End If

        'For xCol As Integer = 0 To dgvLista2.Columns.Count - 1
        '    dgvLista2.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
        '    dgvLista2.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        'Next

        'btnImprimir.Focus()
        lblTotalRegistro.Text = "Total Registros : " & dgvLista2.Rows.Count
    End Sub

    'Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
    '    cSegurosPolizas = New clsSegurosPolizas

    '    'DTable.Clear()
    '    Me.Cursor = Cursors.WaitCursor
    '    If (txtDias.Text.Length = 0 Or dgvLista.Rows.Count = 0) Then
    '        MessageBox.Show("Ingrese los Dias que desea recuperar la informacion", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        txtDias.Focus()
    '        Exit Sub
    '    End If
    '    'DTable = cSegurosPolizas.fREP_Cuotas_Canceladas(gEmpresa, IIf(cboCia.SelectedValue = Nothing, "", cboCia.SelectedValue), mtbFechaIni.Text, mtbFechaFin.Text)
    '    If DTable.Rows.Count = 0 Then
    '        MessageBox.Show("No hay Informacion que mostrar", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        Me.Cursor = Cursors.Default
    '        Exit Sub
    '    End If

    '    'Muestra_Reporte(My.Application.Info.DirectoryPath & "\CrPolizasCuotas_x_Pagar.rpt", DT_CuotasXPagar, "tblCuotasXPagar", "", _
    '    Muestra_Reporte("D:\MIGUEL\FUENTES TESORERIA\Sistema Tesoreria21\CapaPresentacion\Reportes\" & "CrPolizaCuotas_Canceladas.rpt", DTable, "TblCuotasCanceladas", "", _
    '    "StrEmpresa;" & cSegurosPolizas.DescripcionEmpresa(gEmpresa), _
    '    "StrRUC;" & cSegurosPolizas.RucEmpresa(gEmpresa)) ', _
    '    '"Criterio;" & "Desde: " & mtbFechaIni.Text & " Hasta:" & mtbFechaFin.Text)

    '    Me.Cursor = Cursors.Default
    'End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, _
               ByVal myDatos As Object, ByVal STRnombreTabla As String, _
               ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New FrmREPView_PolCuotasXPagar
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)
            'myReporte.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CrystalReportViewer1.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CrystalReportViewer1.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CrystalReportViewer1.ReportSource = myReporte
            f.CrystalReportViewer1.DisplayGroupTree = False
            f.strNombreFom = "Reporte de Cuotas Vencidas y/o Por Vencer"
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

  

    Private Sub dgvLista2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLista2.SelectionChanged
        Dim sOpcion As Integer
        Dim DTable2 As DataTable
        DTable2 = New DataTable
        If dgvLista2.Rows.Count > 0 Then

            If Len(Trim(txtDias.Text)) > 0 Then
                Dim CodPoliza As String = ""
                Try
                    CodPoliza = Trim(dgvLista2.Rows(dgvLista2.CurrentRow.Index).Cells(2).Value())

                Catch ex As Exception
                    CodPoliza = ""
                End Try

                If (chkVencidas.Checked = True And chkPorVencer.Checked = True) Then
                    sOpcion = 3
                ElseIf chkVencidas.Checked = True Then
                    sOpcion = 1
                ElseIf chkPorVencer.Checked = True Then
                    sOpcion = 2
                End If

                cSegurosPolizas = New clsSegurosPolizas
                'DTable2 = cSegurosPolizas.fListarCentrosSubPoliza(39, gEmpresa, CodPoliza, txtDias.Text, sOpcion)

                If rbPolizas.Checked = True Then
                    Label4.Text = "Endosos de la Poliza # " & CodPoliza
                    DTable2 = cSegurosPolizas.fListarCentrosSubPoliza(38, gEmpresa, CodPoliza, txtDias.Text, sOpcion)

                ElseIf rbCuotas.Checked = True Then
                    Label4.Text = "Cuotas de los Endosos de la Poliza # " & CodPoliza
                    DTable2 = cSegurosPolizas.fListarCentrosSubPoliza(39, gEmpresa, CodPoliza, txtDias.Text, sOpcion)
                End If

                dgvLista3.DataSource = DTable2
            ElseIf Len(Trim(txtDias.Text)) = 0 Then
                MessageBox.Show("Ingrese el Parametro Dias", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtDias.Focus()
                Exit Sub
            End If
        End If
        Label3.Text = "Total Registros : " & dgvLista3.Rows.Count
    End Sub

    Private Sub dgvLista2_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLista2.CellContentClick

    End Sub
End Class