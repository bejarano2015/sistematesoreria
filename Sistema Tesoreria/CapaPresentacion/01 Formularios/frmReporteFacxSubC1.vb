Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Public Class frmReporteFacxSubC1
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Public NombreProveedor As String = ""
    Public ResDet As String = ""
    Public TodosUno As String = ""
    Public RucDeUno As String = ""
    Public Moneda As String = ""

    Public FechaIni As DateTime
    Public FechaFin As DateTime

    Dim rptFacPorPagar3 As New rptFacturasPorPagarSubCGlobal
    Dim rptFacPorPagar4 As New rptFacturasPorPagarSubCGlobalTotal

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteFacxSubC1 = Nothing
    Public Shared Function Instance() As frmReporteFacxSubC1
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteFacxSubC1
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteFacxProv2_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region


    Private Sub frmReporteFacxSubC1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If TodosUno = "T" Then
            rptFacPorPagar4.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NombreEmpr")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gDesEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmprRuc)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("ResDet")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(ResDet)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@CodProv")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(RucDeUno)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@IdMoneda")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(Moneda)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '6
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaIni")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = FechaIni
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '7
            crParameterFieldDefinitions = rptFacPorPagar4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaFin")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = FechaFin
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptFacPorPagar4
        ElseIf TodosUno = "U" Then
            rptFacPorPagar3.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NombreEmpr")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(NombreProveedor)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmprRuc)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("ResDet")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(ResDet)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@CodProv")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(RucDeUno)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@IdMoneda")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(Moneda)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '6
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaIni")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = FechaIni
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '7
            crParameterFieldDefinitions = rptFacPorPagar3.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaFin")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = FechaFin
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptFacPorPagar3
        End If
    End Sub
End Class