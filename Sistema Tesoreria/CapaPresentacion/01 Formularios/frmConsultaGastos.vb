Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data
Imports System.Data.SqlClient

Public Class frmConsultaGastos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaGastos = Nothing
    Public Shared Function Instance() As frmConsultaGastos
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaGastos
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function
    Private Sub frmConsultaGastos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    'El adapatador para obtener los datos
    Private da As SqlDataAdapter
    Private eBusquedaGastos As clsBusquedaGastos
    Private eTempo As clsPlantTempo
    Dim ir As String = ""

   
    Private Sub txtTexto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTexto.TextChanged
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        If cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCriterio.Focus()
            Exit Sub
        End If
        Dim dt As DataTable
        dt = New DataTable
        Dim cadenaBusqueda As String
        Dim cadenaBusqueda2 As String = ""
        If ir = "SI" And Len(txtTexto.Text.Trim) > 0 Then
            If cboCriterio.Text = "CONCEPTO" Then
                cadenaBusqueda2 = "	WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.Glosa LIKE '%" & Trim(txtTexto.Text) & "%' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd")
            ElseIf cboCriterio.Text = "RESPONSABLE" Then
                cadenaBusqueda2 = " WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.Beneficiario LIKE '%" & Trim(txtTexto.Text) & "%' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd")
            ElseIf cboCriterio.Text = "CHEQUE" Then
                cadenaBusqueda2 = " WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.IdFormaPago='00002' AND TM.NroFormaPago LIKE '%" & Trim(txtTexto.Text) & "%' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd")
            ElseIf cboCriterio.Text = "MACRO" Then
                cadenaBusqueda2 = " WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.IdFormaPago='00008' AND TM.NroFormaPago LIKE '%" & Trim(txtTexto.Text) & "%' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd")
            ElseIf cboCriterio.Text = "CARTA" Then
                cadenaBusqueda2 = " WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.IdFormaPago='00007' AND TM.NroFormaPago LIKE '%" & Trim(txtTexto.Text) & "%' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd")
            ElseIf cboCriterio.Text = "TRANSFERENCIA" Then
                cadenaBusqueda2 = " WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.IdFormaPago='00005' AND TM.NroFormaPago LIKE '%" & Trim(txtTexto.Text) & "%' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd")
            ElseIf cboCriterio.Text = "OTROS" Then
                cadenaBusqueda2 = " WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.IdFormaPago='00006' AND TM.NroFormaPago LIKE '%" & Trim(txtTexto.Text) & "%' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd")
            End If
            cadenaBusqueda = "SELECT TM.FechaMovimiento,TM.Glosa,TM.Beneficiario, " & _
              "CC.CCosDescripcion, B.Abreviado,CB.NumeroCuenta, TM.NroFormaPago,TTM.Abreviado as 'DescripcionRendicion', " & _
              "CM.MonSimbolo, TM.Monto,TM.Prestamo,TM.IdMovimiento, " & _
              "Prestamo2= CASE TM.Prestamo WHEN '1' THEN  'P' WHEN '2' THEN  'PV' END,TM.CantDet as 'CountDet',ISNULL(TM.CheqEntregado,'') as 'CheqEntregado'  " & _
              "FROM Tesoreria.MovimientoCajaBanco TM 	" & _
              "INNER JOIN Tesoreria.CuentaBanco CB ON TM.NumeroCuenta=CB.IdCuenta AND TM.EmprCodigo=CB.EmprCodigo " & _
              "INNER JOIN Comun.CCosto CC ON TM.EmprCodigo=CC.EmprCodigo AND TM.CCosCodigo=CC.CCosCodigo " & _
              "INNER JOIN Tesoreria.Banco B ON TM.EmprCodigo=B.EmprCodigo AND TM.IdBanco=B.IdBanco " & _
              "INNER JOIN Tesoreria.TipoMovimiento TTM ON TTM.IdRendicion=TM.IdFormaPago " & _
              "INNER JOIN Comun.Moneda CM ON TM.MonCodigo=CM.MonCodigo " & cadenaBusqueda2 & _
              "' ORDER BY TM.FechaMovimiento DESC "
            da = New SqlDataAdapter(cadenaBusqueda, conexion)
            da.Fill(dt)
            'dgvGastos.DataSource = dtBusqueda

            If dgvGastos.Rows.Count > 0 Then
                For x As Integer = 0 To dgvGastos.RowCount - 1
                    'MostrarDetalles = "NO"
                    dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
                Next
            End If

            If dt.Rows.Count > 0 Then
                For y As Integer = 0 To dt.Rows.Count - 1
                    dgvGastos.Rows.Add()
                    dgvGastos.Rows(y).Cells("Fecha").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("FechaMovimiento").ToString) = True, "", dt.Rows(y).Item("FechaMovimiento").ToString)
                    dgvGastos.Rows(y).Cells("Concepto").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Glosa").ToString) = True, "", dt.Rows(y).Item("Glosa"))
                    dgvGastos.Rows(y).Cells("Column11").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Beneficiario").ToString) = True, "", dt.Rows(y).Item("Beneficiario").ToString)
                    dgvGastos.Rows(y).Cells("Column1").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CCosDescripcion").ToString) = True, "", dt.Rows(y).Item("CCosDescripcion").ToString)
                    dgvGastos.Rows(y).Cells("Column2").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Abreviado").ToString) = True, "", dt.Rows(y).Item("Abreviado").ToString)
                    dgvGastos.Rows(y).Cells("NumeroCuenta").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("NumeroCuenta").ToString) = True, "", dt.Rows(y).Item("NumeroCuenta").ToString)
                    dgvGastos.Rows(y).Cells("Column3").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("NroFormaPago").ToString) = True, "", dt.Rows(y).Item("NroFormaPago").ToString)
                    dgvGastos.Rows(y).Cells("Column4").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("DescripcionRendicion").ToString) = True, "", dt.Rows(y).Item("DescripcionRendicion").ToString)
                    dgvGastos.Rows(y).Cells("Column5").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("MonSimbolo").ToString) = True, "", dt.Rows(y).Item("MonSimbolo").ToString)
                    dgvGastos.Rows(y).Cells("Column6").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Monto").ToString) = True, "", dt.Rows(y).Item("Monto").ToString)
                    dgvGastos.Rows(y).Cells("Column7").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Prestamo").ToString) = True, "", dt.Rows(y).Item("Prestamo").ToString)
                    dgvGastos.Rows(y).Cells("Column8").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CountDet").ToString) = True, "", dt.Rows(y).Item("CountDet").ToString)
                    dgvGastos.Rows(y).Cells("Column9").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("IdMovimiento").ToString) = True, "", dt.Rows(y).Item("IdMovimiento").ToString)
                    dgvGastos.Rows(y).Cells("Column10").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Prestamo2").ToString) = True, "", dt.Rows(y).Item("Prestamo2").ToString)
                    dgvGastos.Rows(y).Cells("CheqEntregado").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CheqEntregado").ToString) = True, "", dt.Rows(y).Item("CheqEntregado").ToString)


                    If dgvGastos.Rows(y).Cells("Column7").Value = "1" Or dgvGastos.Rows(y).Cells("Column7").Value = "2" Then
                        gestionaResaltados(dgvGastos, y, Color.Orange)
                    End If

                    If dgvGastos.Rows(y).Cells("Column8").Value > "1" Then
                        gestionaResaltados2(dgvGastos, y, Color.Orange)
                    End If

                    If dgvGastos.Rows(y).Cells("CheqEntregado").Value = "1" Then
                        dgvGastos.Rows(y).Cells("EntregadoChk").Value = True
                    ElseIf dgvGastos.Rows(y).Cells("CheqEntregado").Value = "0" Then
                        dgvGastos.Rows(y).Cells("EntregadoChk").Value = False
                    End If

                Next
                Label5.Text = "Total de Registros : " & dt.Rows.Count
            Else

                Label5.Text = "Total de Registros : 0"
            End If

            da.Dispose()
        ElseIf Len(txtTexto.Text) = 0 Then
            dtpFechaIni_TextChanged(sender, e)
        End If
        'Pintar()
        Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString
        txtTexto.Focus()
    End Sub

    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(1).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(2).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(3).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(4).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(5).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(6).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(7).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(8).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(9).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(10).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(11).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(12).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(14).Style.BackColor = Color.Silver
    End Sub

    Private Sub gestionaResaltados2(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(9).Style.BackColor = Color.Aqua
    End Sub

    Private Sub frmConsultaGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ir = "NO"
        dtpFechaFin.Text = Now.Date()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        ir = "SI"
        dtpFechaIni_TextChanged(sender, e)
        Timer2.Enabled = True
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If Timer2.Enabled = True Then
            cboCriterio.Focus()
        End If
        Timer2.Enabled = False
    End Sub

    Private Sub dgvGastos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvGastos.KeyDown
        Select Case e.KeyCode
            Case Keys.F2
                If dgvGastos.Rows.Count > 0 Then
                    Dim sIdMovimiento As String = ""
                    Dim sFormaDet As String = ""
                    Dim xEmpresaAPrestar As String = ""
                    Dim dblMonto As Double = 0
                    sIdMovimiento = IIf(Microsoft.VisualBasic.IsDBNull(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value), "", dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value)
                    If Trim(sIdMovimiento) <> "" Then
                        Dim frm As New frmDetalleMovimiento2
                        frm.Owner = Me
                        frm.sIdMovimiento = Trim(sIdMovimiento)
                        frm.ShowInTaskbar = False
                        frm.ShowDialog()
                    Else
                        MessageBox.Show("No Existe Detalle", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    MessageBox.Show("Seleccione un Registro de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
        End Select

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCriterio.Focus()
            Exit Sub
        End If
        Dim x As frmReporteConsultaGas = frmReporteConsultaGas.Instance
        x.MdiParent = frmPrincipal
        If Len(txtTexto.Text) > 0 Then
            If cboCriterio.Text = "CONCEPTO" Then
                x.prmOpcionx = 1
                x.prmTextox = Trim(txtTexto.Text)
                x.prmFIni = dtpFechaIni.Value
                x.prmFFin = dtpFechaFin.Value
                x.prmCampoOrd = ""
                x.prmAscDesc = ""
            ElseIf cboCriterio.Text = "RESPONSABLE" Then
                x.prmOpcionx = 2
                x.prmTextox = Trim(txtTexto.Text)
                x.prmFIni = dtpFechaIni.Value
                x.prmFFin = dtpFechaFin.Value
                x.prmCampoOrd = ""
                x.prmAscDesc = ""
            ElseIf cboCriterio.Text = "CHEQUE" Then
                x.prmOpcionx = 3
                x.prmTextox = Trim(txtTexto.Text)
                x.prmFIni = dtpFechaIni.Value
                x.prmFFin = dtpFechaFin.Value
                x.prmCampoOrd = ""
                x.prmAscDesc = ""
            ElseIf cboCriterio.Text = "MACRO" Then
                x.prmOpcionx = 4
                x.prmTextox = Trim(txtTexto.Text)
                x.prmFIni = dtpFechaIni.Value
                x.prmFFin = dtpFechaFin.Value
                x.prmCampoOrd = ""
                x.prmAscDesc = ""
            ElseIf cboCriterio.Text = "CARTA" Then
                x.prmOpcionx = 5
                x.prmTextox = Trim(txtTexto.Text)
                x.prmFIni = dtpFechaIni.Value
                x.prmFFin = dtpFechaFin.Value
                x.prmCampoOrd = ""
                x.prmAscDesc = ""
            ElseIf cboCriterio.Text = "TRANSFERENCIA" Then
                x.prmOpcionx = 6
                x.prmTextox = Trim(txtTexto.Text)
                x.prmFIni = dtpFechaIni.Value
                x.prmFFin = dtpFechaFin.Value
                x.prmCampoOrd = ""
                x.prmAscDesc = ""
            ElseIf cboCriterio.Text = "OTROS" Then
                x.prmOpcionx = 7
                x.prmTextox = Trim(txtTexto.Text)
                x.prmFIni = dtpFechaIni.Value
                x.prmFFin = dtpFechaFin.Value
                x.prmCampoOrd = ""
                x.prmAscDesc = ""
            End If

        Else 'cboCriterio.SelectedIndex >= -1 And cboCriterio.Text <> "CONCEPTO" Then
            x.prmOpcionx = 0
            x.prmTextox = Trim(txtTexto.Text)
            x.prmFIni = dtpFechaIni.Value
            x.prmFFin = dtpFechaFin.Value
            x.prmCampoOrd = ""
            x.prmAscDesc = ""
        End If
        x.Show()
    End Sub



    Private Sub dtpFechaIni_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFechaIni.TextChanged
        If Len(Trim(txtTexto.Text)) > 0 Then
            txtTexto_TextChanged(sender, e)
        Else
            If dtpFechaFin.Value < dtpFechaIni.Value Then
                MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dtpFechaIni.Focus()
                Exit Sub
            End If
            If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCriterio.Focus()
                Exit Sub
            End If
            Dim dt As DataTable
            dt = New DataTable
            If ir = "SI" Then
                Dim cadenaBusqueda As String
                cadenaBusqueda = "SELECT TM.FechaMovimiento,TM.Glosa,TM.Beneficiario, " & _
                "CC.CCosDescripcion, B.Abreviado, CB.NumeroCuenta, TM.NroFormaPago,TTM.Abreviado as 'DescripcionRendicion', " & _
                "CM.MonSimbolo, TM.Monto,TM.Prestamo,TM.IdMovimiento, " & _
                "Prestamo2= CASE TM.Prestamo WHEN '1' THEN  'P' WHEN '2' THEN  'PV' END,TM.CantDet as 'CountDet',ISNULL(TM.CheqEntregado,'') as 'CheqEntregado' " & _
                "FROM Tesoreria.MovimientoCajaBanco TM 	" & _
                "INNER JOIN Tesoreria.CuentaBanco CB ON TM.NumeroCuenta=CB.IdCuenta AND TM.EmprCodigo=CB.EmprCodigo " & _
                "INNER JOIN Comun.CCosto CC ON TM.EmprCodigo=CC.EmprCodigo AND TM.CCosCodigo=CC.CCosCodigo " & _
                "INNER JOIN Tesoreria.Banco B ON TM.EmprCodigo=B.EmprCodigo AND TM.IdBanco=B.IdBanco " & _
                "INNER JOIN Tesoreria.TipoMovimiento TTM ON TTM.IdRendicion=TM.IdFormaPago " & _
                "INNER JOIN Comun.Moneda CM ON TM.MonCodigo=CM.MonCodigo " & _
                "WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd") & _
                "' ORDER BY TM.FechaMovimiento DESC "


                '  cadenaBusqueda = "SELECT TM.FechaMovimiento,TM.Glosa,TM.Beneficiario, " & _
                '"CC.CCosDescripcion, B.Abreviado,CB.NumeroCuenta, TM.NroFormaPago,TTM.Abreviado as 'DescripcionRendicion', " & _
                '"CM.MonSimbolo, TM.Monto,TM.Prestamo,TM.IdMovimiento, " & _
                '"Prestamo2= CASE TM.Prestamo WHEN '1' THEN  'P' WHEN '2' THEN  'PV' END,TM.CantDet as 'CountDet' " & _
                '"FROM Tesoreria.MovimientoCajaBanco TM 	" & _
                '"INNER JOIN Tesoreria.CuentaBanco CB ON TM.NumeroCuenta=CB.IdCuenta AND TM.EmprCodigo=CB.EmprCodigo " & _
                '"INNER JOIN Comun.CCosto CC ON TM.EmprCodigo=CC.EmprCodigo AND TM.CCosCodigo=CC.CCosCodigo " & _
                '"INNER JOIN Tesoreria.Banco B ON TM.EmprCodigo=B.EmprCodigo AND TM.IdBanco=B.IdBanco " & _
                '"INNER JOIN Tesoreria.TipoMovimiento TTM ON TTM.IdRendicion=TM.IdFormaPago " & _
                ''"INNER JOIN Comun.Moneda CM ON TM.MonCodigo=CM.MonCodigo " & cadenaBusqueda2 & _
                '  "INNER JOIN Comun.Moneda CM ON TM.MonCodigo=CM.MonCodigo " & _
                '"WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd") & _

                '"' ORDER BY TM.FechaMovimiento DESC "

                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dt)

                'dgvGastos.DataSource = dt
                If dgvGastos.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvGastos.RowCount - 1
                        'MostrarDetalles = "NO"
                        dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
                    Next
                End If
                If dt.Rows.Count > 0 Then
                    For y As Integer = 0 To dt.Rows.Count - 1
                        dgvGastos.Rows.Add()
                        dgvGastos.Rows(y).Cells("Fecha").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("FechaMovimiento").ToString) = True, "", dt.Rows(y).Item("FechaMovimiento").ToString)
                        dgvGastos.Rows(y).Cells("Concepto").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Glosa").ToString) = True, "", dt.Rows(y).Item("Glosa"))
                        dgvGastos.Rows(y).Cells("Column11").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Beneficiario").ToString) = True, "", dt.Rows(y).Item("Beneficiario").ToString)
                        dgvGastos.Rows(y).Cells("Column1").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CCosDescripcion").ToString) = True, "", dt.Rows(y).Item("CCosDescripcion").ToString)
                        dgvGastos.Rows(y).Cells("Column2").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Abreviado").ToString) = True, "", dt.Rows(y).Item("Abreviado").ToString)
                        dgvGastos.Rows(y).Cells("NumeroCuenta").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("NumeroCuenta").ToString) = True, "", dt.Rows(y).Item("NumeroCuenta").ToString)
                        dgvGastos.Rows(y).Cells("Column3").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("NroFormaPago").ToString) = True, "", dt.Rows(y).Item("NroFormaPago").ToString)
                        dgvGastos.Rows(y).Cells("Column4").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("DescripcionRendicion").ToString) = True, "", dt.Rows(y).Item("DescripcionRendicion").ToString)
                        dgvGastos.Rows(y).Cells("Column5").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("MonSimbolo").ToString) = True, "", dt.Rows(y).Item("MonSimbolo").ToString)
                        dgvGastos.Rows(y).Cells("Column6").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Monto").ToString) = True, "", dt.Rows(y).Item("Monto").ToString)
                        dgvGastos.Rows(y).Cells("Column7").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Prestamo").ToString) = True, "", dt.Rows(y).Item("Prestamo").ToString)
                        dgvGastos.Rows(y).Cells("Column8").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CountDet").ToString) = True, "", dt.Rows(y).Item("CountDet").ToString)
                        dgvGastos.Rows(y).Cells("Column9").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("IdMovimiento").ToString) = True, "", dt.Rows(y).Item("IdMovimiento").ToString)
                        dgvGastos.Rows(y).Cells("Column10").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Prestamo2").ToString) = True, "", dt.Rows(y).Item("Prestamo2").ToString)
                        dgvGastos.Rows(y).Cells("CheqEntregado").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CheqEntregado").ToString) = True, "", dt.Rows(y).Item("CheqEntregado").ToString)

                        If dgvGastos.Rows(y).Cells("Column7").Value = "1" Or dgvGastos.Rows(y).Cells("Column7").Value = "2" Then
                            gestionaResaltados(dgvGastos, y, Color.Orange)
                        End If

                        If dgvGastos.Rows(y).Cells("Column8").Value > "1" Then
                            gestionaResaltados2(dgvGastos, y, Color.Orange)
                        End If

                        If dgvGastos.Rows(y).Cells("CheqEntregado").Value = "1" Then
                            dgvGastos.Rows(y).Cells("EntregadoChk").Value = True
                        ElseIf dgvGastos.Rows(y).Cells("CheqEntregado").Value = "0" Then
                            dgvGastos.Rows(y).Cells("EntregadoChk").Value = False
                        End If
                    Next
                    Label5.Text = "Total de Registros : " & dt.Rows.Count
                Else
                    
                    Label5.Text = "Total de Registros : 0"
                End If


                da.Dispose()
            End If

            'Pintar()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString
        End If
    End Sub

    Sub Pintar()
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1

                If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                    gestionaResaltados(dgvGastos, x, Color.Orange)
                End If

                If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                    gestionaResaltados2(dgvGastos, x, Color.Orange)
                End If

                If dgvGastos.Rows(x).Cells("CheqEntregado").Value = "1" Then
                    dgvGastos.Rows(x).Cells("EntregadoChk").Value = True
                ElseIf dgvGastos.Rows(x).Cells("CheqEntregado").Value = "0" Then
                    dgvGastos.Rows(x).Cells("EntregadoChk").Value = False
                End If

            Next
        End If
    End Sub
    
    Private Sub dtpFechaFin_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFechaFin.TextChanged
        dtpFechaIni_TextChanged(sender, e)
    End Sub


    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged

    End Sub

    'Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
    'Dim a As DateTime

    'MessageBox.Show(Format(Now, "yyyy-MM-dd hh:mm:ss"))
    'MessageBox.Show(Format(dtpFechaFin.Value))
    'MessageBox.Show(Format(Today()))
    'MessageBox.Show(Now())
    'End Sub

    Private Sub dgvGastos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGastos.CellContentClick

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Len(Trim(txtTexto.Text)) > 0 Then
            txtTexto_TextChanged(sender, e)
        Else
            If dtpFechaFin.Value < dtpFechaIni.Value Then
                MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dtpFechaIni.Focus()
                Exit Sub
            End If
            If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCriterio.Focus()
                Exit Sub
            End If
            Dim dt As DataTable
            dt = New DataTable
            If ir = "SI" Then
                Dim cadenaBusqueda As String
                cadenaBusqueda = "SELECT TM.FechaMovimiento,TM.Glosa,TM.Beneficiario, " & _
                "CC.CCosDescripcion, B.Abreviado, CB.NumeroCuenta, TM.NroFormaPago,TTM.Abreviado as 'DescripcionRendicion', " & _
                "CM.MonSimbolo, TM.Monto,TM.Prestamo,TM.IdMovimiento, " & _
                "Prestamo2= CASE TM.Prestamo WHEN '1' THEN  'P' WHEN '2' THEN  'PV' END,TM.CantDet as 'CountDet',ISNULL(TM.CheqEntregado,'') as 'CheqEntregado' " & _
                "FROM Tesoreria.MovimientoCajaBanco TM 	" & _
                "INNER JOIN Tesoreria.CuentaBanco CB ON TM.NumeroCuenta=CB.IdCuenta AND TM.EmprCodigo=CB.EmprCodigo " & _
                "INNER JOIN Comun.CCosto CC ON TM.EmprCodigo=CC.EmprCodigo AND TM.CCosCodigo=CC.CCosCodigo " & _
                "INNER JOIN Tesoreria.Banco B ON TM.EmprCodigo=B.EmprCodigo AND TM.IdBanco=B.IdBanco " & _
                "INNER JOIN Tesoreria.TipoMovimiento TTM ON TTM.IdRendicion=TM.IdFormaPago " & _
                "INNER JOIN Comun.Moneda CM ON TM.MonCodigo=CM.MonCodigo " & _
                "WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd") & _
                "' ORDER BY TM.FechaMovimiento DESC "


                '  cadenaBusqueda = "SELECT TM.FechaMovimiento,TM.Glosa,TM.Beneficiario, " & _
                '"CC.CCosDescripcion, B.Abreviado,CB.NumeroCuenta, TM.NroFormaPago,TTM.Abreviado as 'DescripcionRendicion', " & _
                '"CM.MonSimbolo, TM.Monto,TM.Prestamo,TM.IdMovimiento, " & _
                '"Prestamo2= CASE TM.Prestamo WHEN '1' THEN  'P' WHEN '2' THEN  'PV' END,TM.CantDet as 'CountDet' " & _
                '"FROM Tesoreria.MovimientoCajaBanco TM 	" & _
                '"INNER JOIN Tesoreria.CuentaBanco CB ON TM.NumeroCuenta=CB.IdCuenta AND TM.EmprCodigo=CB.EmprCodigo " & _
                '"INNER JOIN Comun.CCosto CC ON TM.EmprCodigo=CC.EmprCodigo AND TM.CCosCodigo=CC.CCosCodigo " & _
                '"INNER JOIN Tesoreria.Banco B ON TM.EmprCodigo=B.EmprCodigo AND TM.IdBanco=B.IdBanco " & _
                '"INNER JOIN Tesoreria.TipoMovimiento TTM ON TTM.IdRendicion=TM.IdFormaPago " & _
                ''"INNER JOIN Comun.Moneda CM ON TM.MonCodigo=CM.MonCodigo " & cadenaBusqueda2 & _
                '  "INNER JOIN Comun.Moneda CM ON TM.MonCodigo=CM.MonCodigo " & _
                '"WHERE TM.EmprCodigo='" & gEmpresa & "' AND TM.FechaMovimiento BETWEEN '" & Format(dtpFechaIni.Value, "yyyy-MM-dd") & "' AND '" & Format(dtpFechaFin.Value, "yyyy-MM-dd") & _

                '"' ORDER BY TM.FechaMovimiento DESC "

                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dt)

                'dgvGastos.DataSource = dt
                If dgvGastos.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvGastos.RowCount - 1
                        'MostrarDetalles = "NO"
                        dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
                    Next
                End If
                If dt.Rows.Count > 0 Then
                    For y As Integer = 0 To dt.Rows.Count - 1
                        dgvGastos.Rows.Add()
                        dgvGastos.Rows(y).Cells("Fecha").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("FechaMovimiento").ToString) = True, "", dt.Rows(y).Item("FechaMovimiento").ToString)
                        dgvGastos.Rows(y).Cells("Concepto").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Glosa").ToString) = True, "", dt.Rows(y).Item("Glosa"))
                        dgvGastos.Rows(y).Cells("Column11").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Beneficiario").ToString) = True, "", dt.Rows(y).Item("Beneficiario").ToString)
                        dgvGastos.Rows(y).Cells("Column1").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CCosDescripcion").ToString) = True, "", dt.Rows(y).Item("CCosDescripcion").ToString)
                        dgvGastos.Rows(y).Cells("Column2").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Abreviado").ToString) = True, "", dt.Rows(y).Item("Abreviado").ToString)
                        dgvGastos.Rows(y).Cells("NumeroCuenta").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("NumeroCuenta").ToString) = True, "", dt.Rows(y).Item("NumeroCuenta").ToString)
                        dgvGastos.Rows(y).Cells("Column3").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("NroFormaPago").ToString) = True, "", dt.Rows(y).Item("NroFormaPago").ToString)
                        dgvGastos.Rows(y).Cells("Column4").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("DescripcionRendicion").ToString) = True, "", dt.Rows(y).Item("DescripcionRendicion").ToString)
                        dgvGastos.Rows(y).Cells("Column5").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("MonSimbolo").ToString) = True, "", dt.Rows(y).Item("MonSimbolo").ToString)
                        dgvGastos.Rows(y).Cells("Column6").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Monto").ToString) = True, "", dt.Rows(y).Item("Monto").ToString)
                        dgvGastos.Rows(y).Cells("Column7").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Prestamo").ToString) = True, "", dt.Rows(y).Item("Prestamo").ToString)
                        dgvGastos.Rows(y).Cells("Column8").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CountDet").ToString) = True, "", dt.Rows(y).Item("CountDet").ToString)
                        dgvGastos.Rows(y).Cells("Column9").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("IdMovimiento").ToString) = True, "", dt.Rows(y).Item("IdMovimiento").ToString)
                        dgvGastos.Rows(y).Cells("Column10").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("Prestamo2").ToString) = True, "", dt.Rows(y).Item("Prestamo2").ToString)
                        dgvGastos.Rows(y).Cells("CheqEntregado").Value = IIf(Microsoft.VisualBasic.IsDBNull(dt.Rows(y).Item("CheqEntregado").ToString) = True, "", dt.Rows(y).Item("CheqEntregado").ToString)

                        If dgvGastos.Rows(y).Cells("Column7").Value = "1" Or dgvGastos.Rows(y).Cells("Column7").Value = "2" Then
                            gestionaResaltados(dgvGastos, y, Color.Orange)
                        End If

                        If dgvGastos.Rows(y).Cells("Column8").Value > "1" Then
                            gestionaResaltados2(dgvGastos, y, Color.Orange)
                        End If

                        If dgvGastos.Rows(y).Cells("CheqEntregado").Value = "1" Then
                            dgvGastos.Rows(y).Cells("EntregadoChk").Value = True
                        ElseIf dgvGastos.Rows(y).Cells("CheqEntregado").Value = "0" Then
                            dgvGastos.Rows(y).Cells("EntregadoChk").Value = False
                        End If
                    Next
                    Label5.Text = "Total de Registros : " & dt.Rows.Count
                Else

                    Label5.Text = "Total de Registros : 0"
                End If


                da.Dispose()
            End If

            'Pintar()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString
        End If
    End Sub
End Class