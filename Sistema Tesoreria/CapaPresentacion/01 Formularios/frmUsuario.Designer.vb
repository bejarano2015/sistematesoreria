<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuario
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.LblRegistros = New ctrLibreria.Controles.BeLabel
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.txtNombre = New ctrLibreria.Controles.BeTextBox
        Me.txtPwd = New ctrLibreria.Controles.BeTextBox
        Me.txtConPwd = New ctrLibreria.Controles.BeTextBox
        Me.cmbCategoria = New ctrLibreria.Controles.BeComboBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.dgvLista = New ctrLibreria.Controles.BeDataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.btnPermisos = New ctrLibreria.Controles.BeButton
        Me.btnPermisosAreasObras = New ctrLibreria.Controles.BeButton
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(634, 341)
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnPermisosAreasObras)
        Me.TabPage1.Controls.Add(Me.btnPermisos)
        Me.TabPage1.Controls.Add(Me.LblRegistros)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Size = New System.Drawing.Size(626, 315)
        Me.TabPage1.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.BeLabel5)
        Me.TabPage2.Controls.Add(Me.BeLabel4)
        Me.TabPage2.Controls.Add(Me.BeLabel3)
        Me.TabPage2.Controls.Add(Me.BeLabel2)
        Me.TabPage2.Controls.Add(Me.BeLabel1)
        Me.TabPage2.Controls.Add(Me.cmbCategoria)
        Me.TabPage2.Controls.Add(Me.txtConPwd)
        Me.TabPage2.Controls.Add(Me.txtPwd)
        Me.TabPage2.Controls.Add(Me.txtNombre)
        Me.TabPage2.Controls.Add(Me.txtCodigo)
        Me.TabPage2.Size = New System.Drawing.Size(626, 315)
        Me.TabPage2.UseVisualStyleBackColor = False
        '
        'LblRegistros
        '
        Me.LblRegistros.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.LblRegistros.AutoSize = True
        Me.LblRegistros.BackColor = System.Drawing.Color.Transparent
        Me.LblRegistros.ForeColor = System.Drawing.Color.Black
        Me.LblRegistros.Location = New System.Drawing.Point(7, 290)
        Me.LblRegistros.Name = "LblRegistros"
        Me.LblRegistros.Size = New System.Drawing.Size(52, 13)
        Me.LblRegistros.TabIndex = 1
        Me.LblRegistros.Text = "BeLabel1"
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.LightYellow
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(210, 25)
        Me.txtCodigo.MaxLength = 10
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(161, 20)
        Me.txtCodigo.TabIndex = 0
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtNombre
        '
        Me.txtNombre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNombre.BackColor = System.Drawing.Color.Ivory
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.ForeColor = System.Drawing.Color.Black
        Me.txtNombre.KeyEnter = True
        Me.txtNombre.Location = New System.Drawing.Point(210, 51)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNombre.ShortcutsEnabled = False
        Me.txtNombre.Size = New System.Drawing.Size(161, 20)
        Me.txtNombre.TabIndex = 1
        Me.txtNombre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtPwd
        '
        Me.txtPwd.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPwd.BackColor = System.Drawing.Color.Ivory
        Me.txtPwd.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtPwd.ForeColor = System.Drawing.Color.Black
        Me.txtPwd.KeyEnter = True
        Me.txtPwd.Location = New System.Drawing.Point(210, 77)
        Me.txtPwd.MaxLength = 10
        Me.txtPwd.Name = "txtPwd"
        Me.txtPwd.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPwd.ShortcutsEnabled = False
        Me.txtPwd.Size = New System.Drawing.Size(161, 20)
        Me.txtPwd.TabIndex = 2
        Me.txtPwd.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtConPwd
        '
        Me.txtConPwd.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtConPwd.BackColor = System.Drawing.Color.Ivory
        Me.txtConPwd.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower
        Me.txtConPwd.ForeColor = System.Drawing.Color.Black
        Me.txtConPwd.KeyEnter = True
        Me.txtConPwd.Location = New System.Drawing.Point(210, 103)
        Me.txtConPwd.MaxLength = 10
        Me.txtConPwd.Name = "txtConPwd"
        Me.txtConPwd.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtConPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtConPwd.ShortcutsEnabled = False
        Me.txtConPwd.Size = New System.Drawing.Size(161, 20)
        Me.txtConPwd.TabIndex = 3
        Me.txtConPwd.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cmbCategoria
        '
        Me.cmbCategoria.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cmbCategoria.BackColor = System.Drawing.Color.Ivory
        Me.cmbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCategoria.ForeColor = System.Drawing.Color.Black
        Me.cmbCategoria.FormattingEnabled = True
        Me.cmbCategoria.Items.AddRange(New Object() {"Administrador", "Usuario"})
        Me.cmbCategoria.KeyEnter = True
        Me.cmbCategoria.Location = New System.Drawing.Point(210, 133)
        Me.cmbCategoria.Name = "cmbCategoria"
        Me.cmbCategoria.Size = New System.Drawing.Size(161, 21)
        Me.cmbCategoria.TabIndex = 4
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLista.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-1, -1)
        Me.dgvLista.MultiSelect = False
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLista.Size = New System.Drawing.Size(626, 277)
        Me.dgvLista.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "UsuCodigo"
        Me.Column1.HeaderText = "Codigo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "UsuNombre"
        Me.Column2.HeaderText = "Nombre"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 300
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "categoria"
        Me.Column3.HeaderText = "Categoria"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(45, 32)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel1.TabIndex = 5
        Me.BeLabel1.Text = "Codigo"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(45, 58)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel2.TabIndex = 6
        Me.BeLabel2.Text = "Nombre"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(45, 84)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(61, 13)
        Me.BeLabel3.TabIndex = 7
        Me.BeLabel3.Text = "Password"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(45, 110)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(159, 13)
        Me.BeLabel4.TabIndex = 8
        Me.BeLabel4.Text = "Confirmacion de Password"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(45, 141)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel5.TabIndex = 9
        Me.BeLabel5.Text = "Categor�a"
        '
        'btnPermisos
        '
        Me.btnPermisos.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnPermisos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPermisos.Location = New System.Drawing.Point(471, 282)
        Me.btnPermisos.Name = "btnPermisos"
        Me.btnPermisos.Size = New System.Drawing.Size(146, 23)
        Me.btnPermisos.TabIndex = 2
        Me.btnPermisos.Text = "Ver Permisos de Menu"
        Me.btnPermisos.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnPermisos.UseVisualStyleBackColor = True
        '
        'btnPermisosAreasObras
        '
        Me.btnPermisosAreasObras.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnPermisosAreasObras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPermisosAreasObras.Location = New System.Drawing.Point(305, 282)
        Me.btnPermisosAreasObras.Name = "btnPermisosAreasObras"
        Me.btnPermisosAreasObras.Size = New System.Drawing.Size(160, 23)
        Me.btnPermisosAreasObras.TabIndex = 3
        Me.btnPermisosAreasObras.Text = "Ver Permisos de Areas y Obras"
        Me.btnPermisosAreasObras.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnPermisosAreasObras.UseVisualStyleBackColor = True
        '
        'frmUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(634, 341)
        Me.MaximizeBox = False
        Me.Name = "frmUsuario"
        Me.Text = "Mantenimiento de Usuarios"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LblRegistros As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNombre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtConPwd As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPwd As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cmbCategoria As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents dgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnPermisos As ctrLibreria.Controles.BeButton
    Friend WithEvents btnPermisosAreasObras As ctrLibreria.Controles.BeButton

End Class
