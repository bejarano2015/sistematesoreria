Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteFacxProv3

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    Public Opcion As String = ""
    Public F1 As DateTime
    Public F2 As DateTime

    Dim Reporte1 As New rptReporteFacturasPagadasPrestamoaOtras
    Dim Reporte2 As New rptReporteFacturasPagadasPrestamoPorOtras

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteFacxProv3 = Nothing
    Public Shared Function Instance() As frmReporteFacxProv3
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteFacxProv3
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteFacxProv3_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteFacxProv3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Opcion = "1" Then

            Reporte1.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = Reporte1.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = Reporte1.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gDesEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = Reporte1.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmprRuc)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = Reporte1.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@F1")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(F1)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = Reporte1.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@F2")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(F2)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = Reporte1

        ElseIf Opcion = "2" Then

            Reporte2.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = Reporte2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = Reporte2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gDesEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = Reporte2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmprRuc)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = Reporte2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@F1")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(F1)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = Reporte2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@F2")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(F2)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = Reporte2

        End If
    End Sub
End Class