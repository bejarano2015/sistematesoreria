Imports CapaNegocios
Imports CapaDatos
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports IWshRuntimeLibrary
Imports CapaEntidad

'Imports CapaNegocios
'Imports CapaDatos
'Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
'Imports vb = Microsoft.VisualBasic
'Imports System.Data.SqlClient
Imports System.Data

'Imports Microsoft.Office.Interop.Excel
'Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Data.OleDb

Public Class frmRegistroDocumento
    Dim WithEvents cmr As CurrencyManager
    Private eValorizaciones As clsValorizaciones

    Private cCapadatos As clsCapaDatos
    Private eGastosGenerales As clsGastosGenerales

    Dim NumFila As Integer
    Dim dIgv As Double
    Dim iOpcion As Integer
    Dim sCodRegistro As String
    Dim ImporteDoc As Double = 0
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Dim CodProveedor As String = ""
    Dim NombreProveedor As String = ""
    Dim strRuc As String = ""
    Private ePagoProveedores As clsPagoProveedores
    Private eRegistroDocumento As clsRegistroDocumento
    Private eClsOtros As ClsOtros
    'Private eCheque As clsCheque
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Dim moncondigo As String
    Dim t1 As Double
    Dim t2 As Double
    Dim iResultado As Int32 = 0
    Dim sMonedaDoc As String = ""
    Dim sImporteTotal As Double = 0

    Dim NomPc As New WshNetwork
    Dim sCodGestion As String
    Dim dImporteOC As Double = 0
    Friend dMontosDetTemp(0) As Double
    Friend dCantDetTemp(0) As Double
    Friend BolCondicion As Boolean
    Friend DT_DetalleOC As DataTable
    Dim dblSaldoOC As Double = 0

    Dim bolEdit As Boolean
    Dim Edit_sCodGestion As String
    Dim Edit_CodOrden As String
    Dim Edit_NroOrden As String
    Dim Edit_ImporteReg As Double
    Dim Edit_SaldoOC As Double
    Dim bolNuevaMod As Boolean = False

    Dim CountDocumentos As Integer = 0
    Dim dblMonto As Double = 0
    Dim CountFacturas As Integer = 0

    'VALIDACION RUC
    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()
    Private eUsuario As Usuarios
    Dim VL_PdoCodigoOC As String

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmRegistroDocumento = Nothing

    Public Shared Function Instance() As frmRegistroDocumento
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmRegistroDocumento
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
        'If (MessageBox.Show("� Esta seguro de cerrar la ventana ?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        '    frmInstance = Nothing
        'Else
        '    frmInstance = Nothing
        '    Exit Sub
        'End If
    End Sub

#End Region

    Private Sub LimpiarControles()
        cboTipoAcreedor.SelectedIndex = -1
        txtCodigo.Clear()
        lblDescripcionCodigo.Text = ""
        cboDocumento.SelectedIndex = -1
        dtpFechaBase.Value = Now.Date().ToShortDateString()
        txtDias.Clear()
        txtNroDocumento.Clear()
        dtpFechaDoc.Value = Now.Date().ToShortDateString()
        dtpFechaVen.Value = Now.Date().ToShortDateString()
        cboMoneda.SelectedIndex = -1
        txtImporteIGV.Clear()
        txtTasaIGV.Clear()
        txtImporteTotal.Clear()
        'txtValorDeclarado.Clear()
        'txtValorCalculado.Clear()
        txtValorCompra.Clear()
        cboDepartamentos.SelectedIndex = -1
        txtDescripcion.Clear()
        'txtSituacion.Clear()
        cboFechaCreacion.Value = Now.Date().ToShortDateString()
        cboFechaRecepcion.Value = Now.Date().ToShortDateString()
        txtUsuario.Clear()
        txtSerie.Clear()
        cboDP.SelectedIndex = -1
        txtDP.Clear()
        txtNroOrden.Clear()
        txtCodOrden.Clear()
        txtCodOrden2.Clear()
        txtBuscarFactura.Clear()
    End Sub

    Private Sub DesabilitarControles()
        ' cboTipoAcreedor.Enabled = False
        'txtCodigo.Enabled = False
        lblDescripcionCodigo.Enabled = False
        cboDocumento.Enabled = False
        dtpFechaBase.Enabled = False
        txtDias.Enabled = False
        txtNroDocumento.Enabled = False
        dtpFechaDoc.Enabled = False
        dtpFechaVen.Enabled = False
        cboMoneda.Enabled = False
        txtImporteIGV.Enabled = False
        txtTasaIGV.Enabled = False
        txtImporteTotal.Enabled = False
        'txtValorDeclarado.Enabled = False
        'txtValorCalculado.Enabled = False
        txtValorCompra.Enabled = False
        cboDepartamentos.Enabled = False
        txtDescripcion.Enabled = False
        'txtSituacion.Enabled = False
        cboFechaCreacion.Enabled = False
        cboFechaRecepcion.Enabled = False
        txtUsuario.Enabled = False
        btnBuscar.Enabled = False
        txtSerie.Enabled = False
        'cboDP.SelectedIndex = -1
        txtDP.Enabled = False
        txtNroOrden.Enabled = False
        txtCodOrden.Enabled = False
        txtCodOrden2.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        cboTipoAcreedor.Enabled = True
        txtCodigo.Enabled = True
        lblDescripcionCodigo.Enabled = True
        cboDocumento.Enabled = True
        dtpFechaBase.Enabled = True
        txtDias.Enabled = True
        txtNroDocumento.Enabled = True
        dtpFechaDoc.Enabled = True
        dtpFechaVen.Enabled = True
        cboMoneda.Enabled = True
        'txtImporteIGV.Enabled = True
        'txtTasaIGV.Enabled = True
        txtImporteTotal.Enabled = True
        'txtValorDeclarado.Enabled = True
        'txtValorCalculado.Enabled = True
        'txtInafecto.Enabled = True
        cboDepartamentos.Enabled = True
        txtDescripcion.Enabled = True
        'txtSituacion.Enabled = True
        cboFechaCreacion.Enabled = True
        cboFechaRecepcion.Enabled = True
        txtUsuario.Enabled = True
        btnBuscar.Enabled = True
        txtSerie.Enabled = True
        cboDP.Enabled = True
        txtDP.Enabled = True

        txtNroOrden.Enabled = True
        txtCodOrden.Enabled = True
        txtCodOrden2.Enabled = True

    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        Panel5.Visible = True
        txtNroDocumentoBuscar.Clear()
        txtNroDocumentoBuscar.Focus()
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            If Panel6.Visible = False Then
                Panel6.Visible = True
            End If
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        txtCodProvLog.Clear()
        bolEdit = False
        CountFacturas = 0
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            txtImporteIGV.BackColor = Color.Ivory
            txtImporteIGV.Enabled = True
            txtValorCompra.BackColor = Color.Ivory
            txtValorCompra.Enabled = True

        End If

        'cboTipoAcreedor.SelectedValue = "P"

        cboSituacion.SelectedIndex = 0
        cboSituacion.Enabled = False

        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False

        txtUsuario.Text = gUsuario

        chkIGV.Enabled = True
        chkIGV.Checked = False
        btnImportar.Enabled = True


        cboTipoAcreedor.SelectedValue = "P"
        Panel4.Visible = False
        Panel7.Visible = False
        Panel8.Visible = False

        'txtCodigo.Focus()
        Timer1.Enabled = True
        'chkArchivado.Checked = False
        For x As Integer = 0 To dgvValorizaciones.RowCount - 1
            dgvValorizaciones.Rows.Remove(dgvValorizaciones.CurrentRow)
        Next

        btnVerValorizaciones.Enabled = True
        'panelDetalle.Visible = False

    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg

        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
            cboSituacion.Enabled = False
            chkIGV.Enabled = False
            btnImportar.Enabled = False
            txtCodigo.Enabled = False
            cboEstado.Enabled = False
            cboDP.Enabled = False
            Panel4.Visible = False
            Panel7.Visible = False
        End If
        btnVerValorizaciones.Enabled = False
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        Panel7.Visible = False
        btnVerValorizaciones.Enabled = False
        VerPosicion()
        If Fila("SituacionId") <> "00001" Then
            Dim Mensaje As String = ""
            If Fila("SituacionId") = "00002" Then
                Mensaje = "No se puede modificar el documento, por que se encuentra Aprovado."
            ElseIf Fila("SituacionId") = "00003" Then
                Mensaje = "No se puede modificar el documento, por que se encuentra Provisionado."
            ElseIf Fila("SituacionId") = "00004" Then
                Mensaje = "No se puede modificar el documento, por que se encuentra Cancelado."
            End If
            MessageBox.Show(Mensaje, "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            DesabilitarControles()
            cboSituacion.Enabled = False
            chkIGV.Enabled = False
            btnImportar.Enabled = False
            txtCodigo.Enabled = False
            cboEstado.Enabled = False
            cboDP.Enabled = False
            Panel4.Visible = False
            btnVerValorizaciones.Enabled = False
            Exit Sub
        End If

        'VerPosicion()
        'If Fila("IdOC") <> "" Then
        '    MessageBox.Show("La Factura esta anexada a una Orden de Compra, por lo tanto se generaron Asientos para Contabilidad " & vbCrLf & _
        '                    "Si desea Modificar, Elimine el Registro y vuelva a Ingresarlo", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    DesabilitarControles()
        '    cboSituacion.Enabled = False
        '    chkIGV.Enabled = False
        '    btnImportar.Enabled = False
        '    txtCodigo.Enabled = False
        '    cboEstado.Enabled = False
        '    cboDP.Enabled = False
        '    Panel4.Visible = False
        '    btnVerValorizaciones.Enabled = False
        '    Exit Sub
        'End If

        Dim DreadDatosGestion As SqlDataReader
        Dim DreadDatosFact As SqlDataReader

        bolEdit = True

        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            LimpiarControles()
            HabilitarControles()
            VerPosicion()
            btnBuscar.Enabled = False
            IIf(gUsuario = "gnovoa", cboTipoAcreedor.Enabled = True, cboTipoAcreedor.Enabled = False)
            txtCodigo.Enabled = False
            cboSituacion.Enabled = True
            cboDocumento.Enabled = True
            txtSerie.Enabled = True
            txtNroDocumento.Enabled = True
            cboMoneda.Enabled = True
            cboEstado.Enabled = True
            cboSituacion.Enabled = False
            cboDocumento.Focus()
            btnImportar.Enabled = False
            txtNroOrden.Enabled = True
            Panel4.Visible = False

            txtImporteIGV.BackColor = Color.Ivory
            txtImporteIGV.Enabled = True
            txtValorCompra.BackColor = Color.Ivory
            txtValorCompra.Enabled = True

            Timer7.Enabled = True
        End If

        'XXXXXXXXXXXXX DEVUELVE DATOS DE REGISTRO XXXXXXXXXXXXX
        'Dread = eRegistroDocumento.Consulta_OC(gEmpresa, gPeriodo, Trim(txtCodOrden.Text), Trim(txtNroOrden.Text), txtCodigo.Text)
        Dim sCodRegistro As String = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(2).Value

        DreadDatosFact = eRegistroDocumento.DatosFactura_Teso(gEmpresa, sCodRegistro)
        If DreadDatosFact.HasRows = True Then
            DreadDatosFact.Read()
            DreadDatosGestion = eRegistroDocumento.CODIGO_GESTION(gEmpresa, Trim(DreadDatosFact("Tipo")), _
                                Trim(DreadDatosFact("Serie")), Trim(DreadDatosFact("Numero")), sCodRegistro)
        Else
            DreadDatosGestion = Nothing
        End If

        Edit_CodOrden = Trim(txtCodOrden.Text)
        Edit_NroOrden = Trim(txtNroOrden.Text)

        'If Edit_CodOrden <> String.Empty And Edit_NroOrden <> String.Empty Then
        '    MessageBox.Show("La Factura esta anexada a una Orden de Compra, por lo tanto se generaron Asientos para Contabilidad " & vbCrLf & _
        '                    "Si desea Modificar, Elimine el Registro y vuelva a Ingresarlo", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        'Else

        Edit_SaldoOC = eRegistroDocumento.Saldo_OC(gEmpresa, Edit_CodOrden, Edit_NroOrden)
        If DreadDatosGestion.HasRows = True Then
            DreadDatosGestion.Read()
            Edit_sCodGestion = DreadDatosGestion("CodigoGestion")
            Edit_ImporteReg = DreadDatosGestion("Total_Teso")
        End If
        'End If
        DreadDatosFact.Close()
        DreadDatosGestion.Close()

    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        bolEdit = False
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            'Timer2.Enabled = True
        End If
        mMostrarGrilla()
        btnImportar.Enabled = True
        Timer9.Enabled = True

        ReDim dMontosDetTemp(0)
        ReDim dCantDetTemp(0)
        dMontosDetTemp(0) = 0
        dCantDetTemp(0) = 0
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        'Else
        'Exit Sub
        'End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        If iOpcion = 2 Then

            If Fila("FechaCreacion2") <> DateTime.Today And gUsuario <> "RGIL" And gUsuario <> "JNOVOA" Then
                MessageBox.Show("No es posible modificar  " & vbCrLf & _
                                "Si desea Modificar, Consulte con su administrador", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                DesabilitarControles()
                cboSituacion.Enabled = False
                chkIGV.Enabled = False
                btnImportar.Enabled = False
                txtCodigo.Enabled = False
                cboEstado.Enabled = False
                cboDP.Enabled = False
                Panel4.Visible = False
                Exit Sub
            ElseIf Fila("SituacionId") <> "00001" Then
                Dim Mensaje As String = ""
                If Fila("SituacionId") = "00002" Then
                    Mensaje = "No se Actualizar� este documento, porque se encuentra Aprovado."
                ElseIf Fila("SituacionId") = "00003" Then
                    Mensaje = "No se Actualizar� este documento, porque se encuentra Provisionado."
                ElseIf Fila("SituacionId") = "00004" Then
                    Mensaje = "No se Actualizar� este documento, porque se encuentra Cancelado."
                End If
                MessageBox.Show(Mensaje, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If
        End If


        Dim ImporteDP As String
        Dim iEditOrdenCompra As Int32 = 0
        Dim iResultadoSaldo As Int32 = 0
        Dim iResultadoGrabLinea As Int32 = 0
        Dim sTipoRegistro As String = ""
        Dim sCodigoActual As String = ""
        Dim sCodigoActualLinea As String = ""
        Dim DP As String = ""
        Dim sCodigoSaldoDoc As String = ""
        Dim iDias As Integer = 0
        Dim dtBuscarLinea As DataTable
        eTempo = New clsPlantTempo
        eRegistroDocumento = New clsRegistroDocumento


        If txtValorCompra.Focused = True Then Exit Sub

        If sTab = 1 Then

            Dim dblSumaTotal As Double = 0
            dblSumaTotal = Math.Round(CDbl(txtImporteIGV.Text) + CDbl(txtValorCompra.Text), 2)
            If dblSumaTotal <> CDbl(txtImporteTotal.Text) Then
                MessageBox.Show("El IGV y la BASE no cuadran con el TOTAL, Verifique", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtImporteIGV.BackColor = Color.Ivory
                txtValorCompra.BackColor = Color.Ivory
                txtImporteIGV.Enabled = True
                txtValorCompra.Enabled = True
                txtImporteIGV.Focus()
                Exit Sub
            End If

            If cboTipoAcreedor.SelectedIndex <> -1 And Len(Trim(txtCodigo.Text)) > 0 And Len(Trim(lblDescripcionCodigo.Text)) > 0 And cboDocumento.SelectedIndex <> -1 And Len(Trim(txtSerie.Text)) > 0 And Len(Trim(txtNroDocumento.Text)) > 0 And cboMoneda.SelectedIndex <> -1 And Len(Trim(txtImporteIGV.Text)) > 0 And Len(Trim(txtTasaIGV.Text)) > 0 And Len(Trim(txtImporteTotal.Text)) > 0 And Len(Trim(txtValorCompra.Text)) > 0 And cboDepartamentos.SelectedIndex <> -1 And cboSituacion.SelectedIndex > -1 And Len(Trim(txtUsuario.Text)) > 0 Then
                Dim NroSerie As String = ""
                Dim NroDocumento As String = ""
                Dim TipoDoc As String = ""
                NroSerie = Trim(txtSerie.Text)
                NroDocumento = Trim(txtNroDocumento.Text)
                TipoDoc = Trim(cboDocumento.SelectedValue)
                If iOpcion = 1 Then
                    sTipoRegistro = "Grabar"
                ElseIf iOpcion = 2 Then
                    sTipoRegistro = "Actualizar"
                End If

                If cboDP.SelectedIndex = 0 Then
                    DP = "D"
                ElseIf cboDP.SelectedIndex = 1 Then
                    DP = "P"
                End If

                If Len(Trim(txtNroOrden.Text)) > 0 And Len(Trim(txtCod.Text)) > 0 Then
                    Dim dtBuscarCodProvLog As DataTable
                    dtBuscarCodProvLog = New DataTable
                    dtBuscarCodProvLog = eRegistroDocumento.fListarOrdenes3(gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
                    If dtBuscarCodProvLog.Rows.Count = 1 Then
                        txtCodProvLog.Text = Trim(dtBuscarCodProvLog.Rows(0).Item("PrvCodigo"))
                        txtCodOrden.Text = Trim(dtBuscarCodProvLog.Rows(0).Item("IdOrden"))
                        VL_PdoCodigoOC = Trim(dtBuscarCodProvLog.Rows(0).Item("PdoCodigo"))
                    ElseIf dtBuscarCodProvLog.Rows.Count = 0 Then
                        txtCodProvLog.Text = ""
                        txtCodOrden.Text = ""
                        txtNroOrden.Text = ""
                        VL_PdoCodigoOC = ""
                    End If
                ElseIf Len(Trim(txtNroOrden.Text)) = 0 And Len(Trim(txtCod.Text)) > 0 Then
                    txtCodProvLog.Text = ""
                    txtCodOrden.Text = ""
                    txtNroOrden.Text = ""
                    VL_PdoCodigoOC = ""
                End If

                If iOpcion = 1 Then
                    ' a grabar
                    dtTable = New DataTable
                    dtTable = eRegistroDocumento.VerificarDocExiste(TipoDoc, NroSerie, NroDocumento, Trim(txtCod.Text))
                    If dtTable.Rows.Count > 0 Then 'ya existe doc registrado
                        MessageBox.Show("El n�mero de Documento " & Trim(txtSerie.Text) & "-" & Trim(txtNroDocumento.Text) & " ya esta Registrado. Ingrese otro Numero.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtNroDocumento.Clear()
                        txtNroDocumento.Focus()
                    Else
                        'grabar
                        If (MessageBox.Show("�Esta seguro de " & sTipoRegistro & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                            If Len(Trim(txtNroOrden.Text)) > 0 And Len(Trim(txtCod.Text)) > 0 And Len(Trim(txtCodProvLog.Text)) > 0 Then
                                dtBuscarLinea = New DataTable
                                dtBuscarLinea = eRegistroDocumento.VerificarLinea(Trim(txtCod.Text), gEmpresa)
                                If dtBuscarLinea.Rows.Count = 0 Then
                                    eRegistroDocumento.fCodigoLinea(gEmpresa)
                                    sCodigoActualLinea = eRegistroDocumento.sCodFuturoLinea
                                    iResultadoGrabLinea = eRegistroDocumento.fGrabarLinea(39, sCodigoActualLinea, Trim(txtCodProvLog.Text), gEmpresa, 0, 0, 0, Trim(txtCod.Text))
                                End If
                            End If
                            frmPrincipal.sFlagGrabar = "1"
                            ' Dim eCheque As New clsCheque
                            If iOpcion = 1 Then
                                eRegistroDocumento.fCodigo(gEmpresa)
                                sCodigoActual = eRegistroDocumento.sCodFuturo
                                'eRegistroDocumento.fCodigoSaldoDo(gEmpresa)
                                'sCodigoSaldoDoc = eRegistroDocumento.sCodFuturoSaldoDoc
                            Else
                                sCodigoActual = sCodRegistro
                            End If
                            If Len(Trim(txtDias.Text)) > 0 Then
                                iDias = Val(txtDias.Text)
                            Else
                                iDias = 0
                            End If

                            Dim conIGV As Integer = 0
                            If chkIGV.Checked = False Then
                                conIGV = 0
                            ElseIf chkIGV.Checked = True Then
                                conIGV = 1
                            End If
                            If Len(Trim(txtDP.Text)) = 0 Then
                                ImporteDP = 0
                            Else
                                ImporteDP = Trim(txtDP.Text)
                            End If
                            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX GENERA ASIENTO DE COMPRAS AXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                            'sCodigoActual: Codigo Tesoreria

                            'jesusb comentado por analisis

                            'If txtNroOrden.Text <> String.Empty And txtNroOrden.Text.Length > 0 Then ' And bolNuevaMod = True Then
                            '    Dim AbreMoneda As String = eRegistroDocumento.AbreviaturaMoneda(cboMoneda.SelectedValue)
                            '    If GENERA_ASIENTO_COMPRAS(gEmpresa, gPeriodo, AbreMoneda, cboMoneda.SelectedValue, _
                            '                            Trim(txtCodOrden.Text), Trim(txtNroOrden.Text), sCodigoActual) = False Then
                            '        MessageBox.Show("Se Cancelo el Proceso de Asiento Contable Automatico, Verifique", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '        frmPrincipal.sFlagGrabar = "0"
                            '        Exit Sub
                            '    End If

                            'Else
                            '    Dim sMes As String = Month(dtpFechaDoc.Value)

                            '    If Convert.ToInt32(sMes) < 10 Then sMes = "0" & sMes
                            '    If eRegistroDocumento.InsertCabecera_SinOC(gEmpresa, sMes, cboMoneda.SelectedValue, gPeriodo, _
                            '                        dtpFechaBase.Value, Trim(eRegistroDocumento.AbrevDocumento(cboDocumento.SelectedValue)), _
                            '                        Trim(txtSerie.Text), Trim(txtNroDocumento.Text), Format(dtpFechaDoc.Value, "dd/MM/yyyy"), Format(dtpFechaVen.Value, "dd/MM/yyyy"), _
                            '                        CDbl(txtValorCompra.Text), CDbl(txtImporteIGV.Text), CDbl(txtImporteTotal.Text), Trim(txtCodigo.Text), String.Empty, CDbl(txtTasaIGV.Text), _
                            '                        IIf(chkIGV.Checked = True, "1", "0"), IIf(cboDP.SelectedIndex = 0, "1", "0"), IIf(cboDP.SelectedIndex = 1, "1", "0"), _
                            '                        gUsuario, NomPc.ComputerName, Format(Today, "dd/MM/yyyy"), sCodigoActual, Trim(txtCodOrden.Text), Trim(txtNroOrden.Text)) = False Then
                            '        MessageBox.Show("Se Cancelo el Proceso de Asiento Contable Automatico, Verifique", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            '        frmPrincipal.sFlagGrabar = "0"
                            '        Exit Sub
                            '    Else
                            '        If bolNuevaMod = False Then
                            '            'ACTUALIZA SALDO O/C
                            '            Call eRegistroDocumento.UPDATE_SALDO_OC(gEmpresa, Trim(txtCodOrden.Text), Trim(txtNroOrden.Text), Convert.ToDouble(txtImporteTotal.Text))
                            '        End If
                            '    End If
                            'End If

                            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                            '------------------------------------------------------------------------------------->
                            'Cuando CountFacturas>0 es por que se ingresa NA asi que se graba el codigo y empresa de la factura como campo agregado en la NA y la factura actualiza sus campo de Dscto de NA y GlosaNA
                            Dim NCCodFT As String = ""
                            Dim NCCodFTEmpr As String = ""
                            Dim NCGlosa As String = ""
                            Dim NCImporte As Double = 0
                            Dim NCGlosaFactura As String = ""
                            Dim NCImporteFactura As String = ""
                            Dim TIngreso As String = ""
                            TIngreso = "OF"
                            If CountFacturas > 0 Then
                                If dgvFacturas.Rows.Count > 0 Then
                                    For x As Integer = 0 To dgvFacturas.Rows.Count - 1
                                        If Convert.ToBoolean(dgvFacturas.Rows(x).Cells("XF").Value) = True Then
                                            NCCodFT = dgvFacturas.Rows(x).Cells("IdRegistroF").Value
                                            NCCodFTEmpr = dgvFacturas.Rows(x).Cells("EmprCodigoF").Value
                                            NCGlosa = ""
                                            NCImporte = 0
                                            TIngreso = Trim(dgvFacturas.Rows(x).Cells("TIngreso").Value)
                                            NCGlosaFactura = dgvFacturas.Rows(x).Cells("NCGlosa").Value & " NA" & Trim(txtSerie.Text) & "-" & Trim(txtNroDocumento.Text)
                                            NCImporteFactura = dgvFacturas.Rows(x).Cells("NCImporte").Value + txtImporteTotal.Text
                                            iResultado = eRegistroDocumento.fActualizarCamposFactura(48, Trim(NCCodFT), Trim(NCCodFTEmpr), Trim(NCGlosaFactura), NCImporteFactura)
                                        End If
                                    Next
                                End If
                            ElseIf CountFacturas = 0 Then
                                NCCodFT = ""
                                NCCodFTEmpr = ""
                                NCGlosa = ""
                                NCImporte = 0
                            End If
                            '------------------------------------------------------------------------------------->
                            Dim CantVal As Integer = 0
                            If dgvValorizaciones.Rows.Count > 0 Then
                                For x As Integer = 0 To dgvValorizaciones.Rows.Count - 1
                                    If Convert.ToBoolean(dgvValorizaciones.Rows(x).Cells("X").Value) = True Then
                                        CantVal = CantVal + 1
                                    End If
                                Next
                            End If
                            If CantVal > 0 Then
                                TIngreso = "OB"
                            End If

                            iResultado = eRegistroDocumento.fGrabar(iOpcion, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), Trim(lblDescripcionCodigo.Text), cboDocumento.SelectedValue, Trim(txtSerie.Text), Trim(txtNroDocumento.Text), dtpFechaBase.Value, iDias, dtpFechaDoc.Value, dtpFechaVen.Value, Trim(cboMoneda.SelectedValue), Trim(txtImporteIGV.Text), Trim(txtTasaIGV.Text), Trim(txtImporteTotal.Text), 0, 0, Trim(txtInafecto.Text), cboDepartamentos.SelectedValue, Trim(txtDescripcion.Text), Trim(cboSituacion.SelectedValue), cboFechaCreacion.Value, cboFechaRecepcion.Value, Trim(txtUsuario.Text), sCodigoActual, gEmpresa, 0, 0, Trim(txtValorCompra.Text), conIGV, DP, ImporteDP, Trim(txtCodOrden.Text), Trim(txtNroOrden.Text), 0, NCCodFT, NCCodFTEmpr, NCGlosa, NCImporte, gPeriodo, TIngreso, VL_PdoCodigoOC)
                            iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, Trim(txtCodOrden.Text), gEmpresa)

                            If iResultado = 1 And dgvValorizaciones.Rows.Count > 0 Then
                                For x As Integer = 0 To dgvValorizaciones.Rows.Count - 1
                                    If Convert.ToBoolean(dgvValorizaciones.Rows(x).Cells("X").Value) = True Then
                                        Dim IdValorizacion As String = ""
                                        Dim IdEmprValor As String = ""
                                        Dim SubTotal As Double = 0
                                        IdValorizacion = Trim(dgvValorizaciones.Rows(x).Cells("IdValorizacion").Value)
                                        IdEmprValor = Trim(dgvValorizaciones.Rows(x).Cells("EmprCodigo").Value)
                                        'SubTotal = Convert.ToDouble(dgvValorizaciones.Rows(x).Cells("TotalVal").Value)
                                        Dim DetraccionFac As Double = 0
                                        'If cboDP.SelectedIndex = 0 And Len(Trim(txtDP.Text)) > 0 Then
                                        'DetraccionFac = Convert.ToDouble(SubTotal) * (Convert.ToDouble((txtDP.Text)) / 100)
                                        'End If
                                        Dim iResultadoMod As Integer = 0
                                        eValorizaciones = New clsValorizaciones
                                        iResultadoMod = eValorizaciones.fActualizarSaldos(64, DetraccionFac, 0, 0, Today(), Trim(sCodigoActual), gEmpresa, Trim(IdEmprValor), Trim(IdValorizacion), "", "", "", 0, gPeriodo)
                                    End If
                                Next
                            End If

                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            Dim iResultado1 As Integer = 0
                            ePagoProveedores = New clsPagoProveedores
                            If Trim(cboMoneda.SelectedValue) = "01" Then
                                Dim dtTable As DataTable
                                dtTable = New DataTable
                                dtTable = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "01")
                                If dtTable.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "01")
                                ElseIf dtTable.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda soles ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoSolesProv As Double = 0
                                    Dim MontoSaldoSolesNew As Double = 0
                                    IdSaldoProv = dtTable.Rows(0).Item("IdSaldo")
                                    MontoSaldoSolesProv = dtTable.Rows(0).Item("MontoSaldo")
                                    MontoSaldoSolesNew = MontoSaldoSolesProv + Val(txtImporteTotal.Text)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoSolesNew, "01")
                                End If
                            ElseIf Trim(cboMoneda.SelectedValue) = "02" Then
                                Dim dtTable2 As DataTable
                                dtTable2 = New DataTable
                                dtTable2 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "02")
                                If dtTable2.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "02")
                                ElseIf dtTable2.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda dolares ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoDolaresProv As Double = 0
                                    Dim MontoSaldoDolaresNew As Double = 0
                                    IdSaldoProv = dtTable2.Rows(0).Item("IdSaldo")
                                    MontoSaldoDolaresProv = dtTable2.Rows(0).Item("MontoSaldo")
                                    MontoSaldoDolaresNew = MontoSaldoDolaresProv + Val(txtImporteTotal.Text)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoDolaresNew, "02")
                                End If
                            End If
                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            iResultadoSaldo = eRegistroDocumento.fGrabarSaldoDoc(29, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "", cboDocumento.SelectedValue, Trim(txtSerie.Text), Trim(txtNroDocumento.Text), dtpFechaBase.Value, 0, dtpFechaDoc.Value, dtpFechaVen.Value, cboMoneda.SelectedValue, Trim(txtImporteIGV.Text), Trim(txtTasaIGV.Text), Trim(txtImporteTotal.Text), 0, 0, Trim(txtValorCompra.Text), cboDepartamentos.SelectedValue, Trim(txtDescripcion.Text), Trim(cboSituacion.SelectedValue), cboFechaCreacion.Value, cboFechaRecepcion.Value, Trim(txtUsuario.Text), sCodigoActual, gEmpresa)
                            If iResultado > 0 Then
                                CountFacturas = 0
                                mMostrarGrilla()
                                'Timer2.Enabled = True
                            End If
                            sTab = 0
                        End If
                    End If
                Else 'ACTUALIZAR
                    If (MessageBox.Show("�Esta seguro de " & sTipoRegistro & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then

                        If Len(Trim(txtNroOrden.Text)) > 0 And Len(Trim(txtCod.Text)) > 0 Then
                            'dtBuscarLinea = New DataTable
                            'dtBuscarLinea = eRegistroDocumento.VerificarLinea(Trim(txtCod.Text), gEmpresa)
                            'If dtBuscarLinea.Rows.Count = 0 Then
                            '    eRegistroDocumento.fCodigoLinea(gEmpresa)
                            '    sCodigoActualLinea = eRegistroDocumento.sCodFuturoLinea
                            '    iResultadoGrabLinea = eRegistroDocumento.fGrabarLinea(39, sCodigoActualLinea, Trim(txtCodProvLog.Text), gEmpresa, 0, 0, 0, Trim(txtCod.Text))
                            'End If

                            If Trim(txtCodOrden.Text) <> Trim(txtCodOrden2.Text) And Len(Trim(txtCodOrden2.Text)) > 0 Then
                                dtBuscarLinea = New DataTable
                                dtBuscarLinea = eRegistroDocumento.VerificarLinea(Trim(txtCod.Text), gEmpresa)
                                If dtBuscarLinea.Rows.Count = 1 Then
                                    'eRegistroDocumento.fCodigoLinea(gEmpresa)
                                    'sCodigoActualLinea = eRegistroDocumento.sCodFuturoLinea
                                    iResultadoGrabLinea = eRegistroDocumento.fGrabarLinea(40, "", Trim(txtCodProvLog.Text), gEmpresa, 0, 0, 0, Trim(txtCod.Text))
                                End If
                            Else
                                dtBuscarLinea = New DataTable
                                dtBuscarLinea = eRegistroDocumento.VerificarLinea(Trim(txtCod.Text), gEmpresa)
                                If dtBuscarLinea.Rows.Count = 0 Then
                                    eRegistroDocumento.fCodigoLinea(gEmpresa)
                                    sCodigoActualLinea = eRegistroDocumento.sCodFuturoLinea
                                    iResultadoGrabLinea = eRegistroDocumento.fGrabarLinea(39, sCodigoActualLinea, Trim(txtCodProvLog.Text), gEmpresa, 0, 0, 0, Trim(txtCod.Text))
                                End If
                            End If

                        End If


                        frmPrincipal.sFlagGrabar = "1"
                        'Dim eCheque As New clsCheque
                        If iOpcion = 1 Then
                            eRegistroDocumento.fCodigo(gEmpresa)
                            sCodigoActual = eRegistroDocumento.sCodFuturo
                        Else
                            sCodigoActual = sCodRegistro 'aki
                        End If
                        If Len(Trim(txtDias.Text)) > 0 Then
                            iDias = Val(txtDias.Text)
                        Else
                            iDias = 0
                        End If

                        Dim dtTableD As DataTable
                        Dim EstadoPrePro, EstadoPro As Integer
                        dtTableD = New DataTable
                        dtTableD = eRegistroDocumento.fBuscarDocumento(sCodigoActual, gEmpresa)
                        If dtTableD.Rows.Count = 1 Then
                            EstadoPrePro = dtTableD.Rows(0).Item("EstadoPrePro")
                            EstadoPro = dtTableD.Rows(0).Item("EstadoPro")
                        End If

                        'actualiza el importe del doc
                        Dim conIGV As Integer = 0
                        If chkIGV.Checked = False Then
                            conIGV = 0
                        ElseIf chkIGV.Checked = True Then
                            conIGV = 1
                        End If

                        If Len(Trim(txtDP.Text)) = 0 Then
                            ImporteDP = 0
                        Else
                            ImporteDP = Trim(txtDP.Text)
                        End If


                        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX ACTUALIZA ASIENTO DE COMPRAS AXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                        'sCodigoActual: Codigo Tesoreria
                        'If txtNroOrden.Text <> String.Empty And txtNroOrden.Text.Length > 0 Then
                        '    Dim AbreMoneda As String = eRegistroDocumento.AbreviaturaMoneda(cboMoneda.SelectedValue)
                        '    If GENERA_ASIENTO_COMPRAS(gEmpresa, gPeriodo, AbreMoneda, cboMoneda.SelectedValue, _
                        '                            Trim(txtCodOrden.Text), Trim(txtNroOrden.Text), sCodigoActual) = False Then
                        '        MessageBox.Show("Se Cancelo la Actualizacion y el Proceso de Asiento Contable Automatico, Verifique", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '        frmPrincipal.sFlagGrabar = "0"
                        '        Exit Sub
                        '    End If
                        'Else
                        '    Dim sMes As String = Month(dtpFechaDoc.Value)
                        '    Dim sTipoDoc As String = Trim(eRegistroDocumento.AbrevDocumento(cboDocumento.SelectedValue))
                        '    If Convert.ToInt32(sMes) < 10 Then sMes = "0" & sMes

                        '    If eRegistroDocumento.UPDATECabecera_SinOC(Edit_sCodGestion, gEmpresa, gPeriodo, sMes, _
                        '                cboMoneda.SelectedValue, Format(dtpFechaBase.Value, "dd/MM/yyyy"), sTipoDoc, _
                        '                Trim(txtSerie.Text), Trim(txtNroDocumento.Text), Format(dtpFechaDoc.Value, "dd/MM/yyyy"), _
                        '                Format(dtpFechaVen.Value, "dd/MM/yyyy"), CDbl(txtValorCompra.Text), CDbl(txtImporteIGV.Text), _
                        '                CDbl(txtImporteTotal.Text), Trim(txtCodigo.Text), IIf(chkIGV.Checked = True, "1", "0"), _
                        '                CDbl(txtTasaIGV.Text), IIf(cboDP.SelectedIndex = 0, "1", "0"), IIf(cboDP.SelectedIndex = 1, "1", "0"), _
                        '                 gUsuario, NomPc.ComputerName, Format(Today, "dd/MM/yyyy"), sCodigoActual) = False Then

                        '        MessageBox.Show("Se Cancelo la Actualizacion y el Proceso de Asiento Contable Automatico, Verifique", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '        frmPrincipal.sFlagGrabar = "0"
                        '        Exit Sub
                        '    End If
                        'End If
                        'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                        '------------------------------------------------------------------------------------->
                        'Cuando CountFacturas>0 es por que se ingresa NA asi que se graba el codigo y empresa de la factura como campo agregado en la NA y la factura actualiza sus campo de Dscto de NA y GlosaNA
                        Dim NCCodFT As String = ""
                        Dim NCCodFTEmpr As String = ""
                        Dim NCGlosa As String = ""
                        Dim NCImporte As Double = 0
                        Dim NCGlosaFactura As String = ""
                        Dim NCImporteFactura As String = ""
                        If CountFacturas > 0 Then
                            If dgvFacturas.Rows.Count > 0 Then
                                For x As Integer = 0 To dgvFacturas.Rows.Count - 1
                                    If Convert.ToBoolean(dgvFacturas.Rows(x).Cells("XF").Value) = True Then
                                        NCCodFT = dgvFacturas.Rows(x).Cells("IdRegistroF").Value
                                        NCCodFTEmpr = dgvFacturas.Rows(x).Cells("EmprCodigoF").Value
                                        NCGlosa = ""
                                        NCImporte = 0
                                        NCGlosaFactura = dgvFacturas.Rows(x).Cells("NCGlosa").Value & " NA" & Trim(txtSerie.Text) & "-" & Trim(txtNroDocumento.Text)
                                        NCImporteFactura = dgvFacturas.Rows(x).Cells("NCImporte").Value + txtImporteTotal.Text
                                        iResultado = eRegistroDocumento.fActualizarCamposFactura(48, Trim(NCCodFT), Trim(NCCodFTEmpr), Trim(NCGlosaFactura), NCImporteFactura)
                                    End If
                                Next
                            End If
                        ElseIf CountFacturas = 0 Then
                            NCCodFT = ""
                            NCCodFTEmpr = ""
                            NCGlosa = ""
                            NCImporte = 0
                        End If
                        '------------------------------------------------------------------------------------->

                        iResultado = eRegistroDocumento.fGrabar(iOpcion, cboTipoAcreedor.SelectedValue, Trim(txtCod.Text), Trim(lblDescripcionCodigo.Text), cboDocumento.SelectedValue, Trim(txtSerie.Text), Trim(txtNroDocumento.Text), dtpFechaBase.Value, iDias, dtpFechaDoc.Value, dtpFechaVen.Value, cboMoneda.SelectedValue, Trim(txtImporteIGV.Text), Trim(txtTasaIGV.Text), Trim(txtImporteTotal.Text), 0, 0, Trim(txtInafecto.Text), cboDepartamentos.SelectedValue, Trim(txtDescripcion.Text), Trim(cboSituacion.SelectedValue), cboFechaCreacion.Value, cboFechaRecepcion.Value, Trim(txtUsuario.Text), sCodigoActual, gEmpresa, cboEstado.SelectedValue, cboEstado.SelectedValue, Trim(txtValorCompra.Text), conIGV, DP, ImporteDP, Trim(txtCodOrden.Text), Trim(txtNroOrden.Text), 1, NCCodFT, NCCodFTEmpr, NCGlosa, NCImporte, gPeriodo, "", VL_PdoCodigoOC)


                        iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, Trim(txtCodOrden.Text), gEmpresa)

                        If (Trim(txtCodOrden.Text) <> Trim(txtCodOrden2.Text) And Len(Trim(txtCodOrden2.Text)) > 0) Or Len(Trim(txtNroOrden.Text)) = 0 Then
                            iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(0, Trim(txtCodOrden2.Text), gEmpresa)
                        End If

                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If Trim(sMonedaDoc) = Trim(cboMoneda.SelectedValue) Then
                            Dim iResultado1 As Integer = 0
                            ePagoProveedores = New clsPagoProveedores
                            If Trim(cboMoneda.SelectedValue) = "01" Then
                                Dim dtTable As DataTable
                                dtTable = New DataTable
                                dtTable = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "01")
                                If dtTable.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "01")
                                ElseIf dtTable.Rows.Count = 1 Then
                                    'ImporteDoc = Trim(Fila("ImporteTotal").ToString)
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoSolesProv As Double = 0
                                    Dim MontoSaldoSolesNew As Double = 0
                                    Dim ImporteSoles As Double = 0
                                    Dim dtTableDoc As DataTable
                                    dtTableDoc = New DataTable
                                    dtTableDoc = eRegistroDocumento.fBuscarDocumento(sCodigoActual, gEmpresa)
                                    If dtTableDoc.Rows.Count = 1 Then
                                        ImporteSoles = dtTableDoc.Rows(0).Item("ImporteTotal")
                                    End If
                                    IdSaldoProv = dtTable.Rows(0).Item("IdSaldo")
                                    MontoSaldoSolesProv = dtTable.Rows(0).Item("MontoSaldo")
                                    '''''''''''''''''''''''''''''''''''''''''
                                    If cboEstado.SelectedIndex = 0 Then
                                        If EstadoPrePro = 1 And EstadoPro = 1 Then
                                            MontoSaldoSolesNew = MontoSaldoSolesProv + ImporteSoles '(ImporteDoc)
                                        Else
                                            MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                        End If
                                        'MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    Else

                                        If EstadoPrePro = 0 And EstadoPro = 0 Then
                                            MontoSaldoSolesNew = MontoSaldoSolesProv - ImporteDoc
                                            'MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                        Else
                                            MontoSaldoSolesNew = MontoSaldoSolesProv '- ImporteDoc
                                        End If

                                        'MontoSaldoSolesNew = MontoSaldoSolesProv - ImporteDoc
                                    End If
                                    '''''''''''''''''''''''''''''''''''''''
                                    'MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoSolesNew, "01")
                                End If
                            ElseIf Trim(cboMoneda.SelectedValue) = "02" Then
                                Dim dtTable2 As DataTable
                                dtTable2 = New DataTable
                                dtTable2 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "02")
                                If dtTable2.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "02")
                                ElseIf dtTable2.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda dolares ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoDolaresProv As Double = 0
                                    Dim MontoSaldoDolaresNew As Double = 0
                                    Dim ImporteDolares As Double = 0
                                    Dim dtTableDoc As DataTable
                                    dtTableDoc = New DataTable
                                    dtTableDoc = eRegistroDocumento.fBuscarDocumento(sCodigoActual, gEmpresa)
                                    If dtTableDoc.Rows.Count = 1 Then
                                        ImporteDolares = dtTableDoc.Rows(0).Item("ImporteTotal")
                                    End If
                                    IdSaldoProv = dtTable2.Rows(0).Item("IdSaldo")
                                    MontoSaldoDolaresProv = dtTable2.Rows(0).Item("MontoSaldo")
                                    '''''''''''''''''''''''''''''''''''''''''''''
                                    If cboEstado.SelectedIndex = 0 Then
                                        If EstadoPrePro = 1 And EstadoPro = 1 Then
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv + ImporteDolares 'ImporteDoc
                                        Else
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                        End If
                                        'MontoSaldoDolaresNew = MontoSaldoDolaresProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    Else
                                        If EstadoPrePro = 0 And EstadoPro = 0 Then
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv - ImporteDoc
                                        Else
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv
                                        End If
                                        'MontoSaldoDolaresNew = MontoSaldoDolaresProv - ImporteDoc
                                    End If
                                    '''''''''''''''''''''''''''''''''''''''
                                    'MontoSaldoDolaresNew = MontoSaldoDolaresProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoDolaresNew, "02")
                                End If
                            End If

                        ElseIf Trim(sMonedaDoc) <> Trim(cboMoneda.SelectedValue) Then

                            Dim iResultado1 As Integer = 0
                            ePagoProveedores = New clsPagoProveedores
                            If Trim(cboMoneda.SelectedValue) = "01" Then
                                Dim dtTable As DataTable
                                dtTable = New DataTable
                                dtTable = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "01")
                                If dtTable.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "01")
                                ElseIf dtTable.Rows.Count = 1 Then
                                    'ImporteDoc = Trim(Fila("ImporteTotal").ToString)
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoSolesProv As Double = 0
                                    Dim MontoSaldoSolesNew As Double = 0
                                    Dim ImporteSoles As Double = 0
                                    Dim dtTableDoc As DataTable
                                    dtTableDoc = New DataTable
                                    dtTableDoc = eRegistroDocumento.fBuscarDocumento(sCodigoActual, gEmpresa)
                                    If dtTableDoc.Rows.Count = 1 Then
                                        ImporteSoles = dtTableDoc.Rows(0).Item("ImporteTotal")
                                    End If
                                    IdSaldoProv = dtTable.Rows(0).Item("IdSaldo")
                                    MontoSaldoSolesProv = dtTable.Rows(0).Item("MontoSaldo")
                                    '''''''''''''''''''''''''''''''''''''''''
                                    If cboEstado.SelectedIndex = 0 Then
                                        If EstadoPrePro = 1 And EstadoPro = 1 Then
                                            MontoSaldoSolesNew = MontoSaldoSolesProv + ImporteSoles '(ImporteDoc)
                                        Else
                                            MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                        End If
                                        'MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    Else

                                        If EstadoPrePro = 0 And EstadoPro = 0 Then
                                            MontoSaldoSolesNew = MontoSaldoSolesProv - ImporteDoc
                                            'MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                        Else
                                            MontoSaldoSolesNew = MontoSaldoSolesProv '- ImporteDoc
                                        End If

                                        'MontoSaldoSolesNew = MontoSaldoSolesProv - ImporteDoc
                                    End If
                                    '''''''''''''''''''''''''''''''''''''''
                                    'MontoSaldoSolesNew = MontoSaldoSolesProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoSolesNew, "01")
                                End If

                                Dim dtTable3 As DataTable
                                dtTable3 = New DataTable
                                dtTable3 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "02")
                                If dtTable3.Rows.Count = 0 Then
                                    ''crear
                                    'Dim IdSaldoProv As String = ""
                                    'ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    'IdSaldoProv = ePagoProveedores.sCodFuturo
                                    'iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "02")
                                ElseIf dtTable3.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda soles ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoDolaresProv As Double = 0
                                    Dim MontoSaldoDolaresNew As Double = 0
                                    Dim ImporteDolares As Double = 0
                                    IdSaldoProv = dtTable3.Rows(0).Item("IdSaldo")
                                    MontoSaldoDolaresProv = dtTable3.Rows(0).Item("MontoSaldo")
                                    MontoSaldoDolaresNew = MontoSaldoDolaresProv - sImporteTotal
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoDolaresNew, "02")
                                End If

                            ElseIf Trim(cboMoneda.SelectedValue) = "02" Then
                                Dim dtTable2 As DataTable
                                dtTable2 = New DataTable
                                dtTable2 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "02")
                                If dtTable2.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "02")
                                ElseIf dtTable2.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda dolares ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoDolaresProv As Double = 0
                                    Dim MontoSaldoDolaresNew As Double = 0
                                    Dim ImporteDolares As Double = 0
                                    Dim dtTableDoc As DataTable
                                    dtTableDoc = New DataTable
                                    dtTableDoc = eRegistroDocumento.fBuscarDocumento(sCodigoActual, gEmpresa)
                                    If dtTableDoc.Rows.Count = 1 Then
                                        ImporteDolares = dtTableDoc.Rows(0).Item("ImporteTotal")
                                    End If
                                    IdSaldoProv = dtTable2.Rows(0).Item("IdSaldo")
                                    MontoSaldoDolaresProv = dtTable2.Rows(0).Item("MontoSaldo")
                                    '''''''''''''''''''''''''''''''''''''''''''''
                                    If cboEstado.SelectedIndex = 0 Then
                                        If EstadoPrePro = 1 And EstadoPro = 1 Then
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv + ImporteDolares 'ImporteDoc
                                        Else
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                        End If
                                        'MontoSaldoDolaresNew = MontoSaldoDolaresProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    Else
                                        If EstadoPrePro = 0 And EstadoPro = 0 Then
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv - ImporteDoc
                                        Else
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv
                                        End If
                                        'MontoSaldoDolaresNew = MontoSaldoDolaresProv - ImporteDoc
                                    End If
                                    '''''''''''''''''''''''''''''''''''''''
                                    'MontoSaldoDolaresNew = MontoSaldoDolaresProv + (Val(txtImporteTotal.Text) - ImporteDoc)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoDolaresNew, "02")
                                End If

                                Dim dtTable3 As DataTable
                                dtTable3 = New DataTable
                                dtTable3 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "01")
                                If dtTable3.Rows.Count = 0 Then
                                    ''crear
                                    'Dim IdSaldoProv As String = ""
                                    'ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    'IdSaldoProv = ePagoProveedores.sCodFuturo
                                    'iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, Val(txtImporteTotal.Text), "02")
                                ElseIf dtTable3.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda soles ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoDolaresProv As Double = 0
                                    Dim MontoSaldoDolaresNew As Double = 0
                                    Dim ImporteDolares As Double = 0
                                    IdSaldoProv = dtTable3.Rows(0).Item("IdSaldo")
                                    MontoSaldoDolaresProv = dtTable3.Rows(0).Item("MontoSaldo")
                                    MontoSaldoDolaresNew = MontoSaldoDolaresProv - sImporteTotal
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), gEmpresa, MontoSaldoDolaresNew, "01")
                                End If
                            End If

                        End If
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        iResultadoSaldo = eRegistroDocumento.fActuSaldoDoc(31, Trim(cboTipoAcreedor.SelectedValue), Trim(txtCod.Text), "", Trim(cboDocumento.SelectedValue), Trim(txtSerie.Text), Trim(txtNroDocumento.Text), dtpFechaBase.Value, 0, dtpFechaDoc.Value, dtpFechaVen.Value, cboMoneda.SelectedValue, Trim(txtImporteIGV.Text), Trim(txtTasaIGV.Text), Trim(txtImporteTotal.Text), 0, 0, Trim(txtValorCompra.Text), cboDepartamentos.SelectedValue, Trim(txtDescripcion.Text), Trim(cboSituacion.SelectedValue), cboFechaCreacion.Value, cboFechaRecepcion.Value, Trim(txtUsuario.Text), sCodigoActual, gEmpresa, cboEstado.SelectedValue)
                        If iResultado > 0 Then
                            mMostrarGrilla()
                            'Timer2.Enabled = True
                        End If
                        sTab = 0
                    End If
                End If
            Else
                If cboTipoAcreedor.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoAcreedor.Focus()
                    Exit Sub
                End If
                If Len(Trim(txtCodigo.Text)) = 0 Then
                    MessageBox.Show("Ingrese un Numero de Ruc Existente", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodigo.Focus()
                    Exit Sub
                End If
                If cboDocumento.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Documento", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboDocumento.Focus()
                    Exit Sub
                End If
                If Len(Trim(txtSerie.Text)) = 0 Then
                    MessageBox.Show("Ingrese el numero de Serie", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtSerie.Focus()
                    Exit Sub
                End If
                If Len(Trim(txtNroDocumento.Text)) = 0 Then
                    MessageBox.Show("Ingrese el numero de Documento", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNroDocumento.Focus()
                    Exit Sub
                End If
                If cboMoneda.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Moneda", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboMoneda.Focus()
                    Exit Sub
                End If
                If Len(Trim(txtImporteTotal.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Importe Total", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtImporteTotal.Focus()
                    Exit Sub
                End If
                If cboDepartamentos.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Area", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboDepartamentos.Focus()
                    Exit Sub
                End If
                If Len(Trim(txtImporteTotal.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Importe Total", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtImporteTotal.Focus()
                    Exit Sub
                End If

                'If Len(Trim(txtDescripcion.Text)) = 0 Then
                '    MessageBox.Show("Ingrese una Descripcion", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    txtDescripcion.Focus()
                '    Exit Sub
                'End If
            End If
            If Len(Trim(txtUsuario.Text)) = 0 Then
                MessageBox.Show("Ingrese un Usuario", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtUsuario.Focus()
                Exit Sub
            End If
            'frmPrincipal.sFlagGrabar = "0"
        End If
        'End If

    End Sub

    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    Private Function GENERA_ASIENTO_COMPRAS(ByVal pEMPRESA As String, ByVal pPERIODO As String, ByVal pABRVMONEDA As String, _
                                    ByVal pCODMONEDA As String, ByVal pCODIGO_OC As String, ByVal pNUMERO_OC As String, _
                                    ByVal pID_REGISTRO As String) As Boolean

        Try
            Dim DT_Plantilla As New DataTable
            Dim DT_Monedas As New DataTable
            Dim DT_Documentos As New DataTable
            Dim StrAbrevDoc As String = String.Empty
            Dim D_o_H As String
            Dim StrTabla As String
            Dim StrAbrevMoneda As String
            Dim StrCodCuenta As String
            Dim sCtaPadre As String
            Dim STRmonSist As String

            Dim TCvoucher As Double
            Dim myTC As Double

            Dim DrFilaCab As DataRow
            Dim MonedaVuelta As String
            Dim i As Integer = 0
            Dim IntFilas As Integer = 0
            Dim Dst_OrdenCompra As New DataSet
            Dim StrCODGESTION As String = String.Empty
            Dim iContPlan As Integer = 0

            Dim sSubdiario As String = String.Empty
            Dim dValorTC As Double
            Dim DTCambioActual As DataTable
            Dim TablaImportes As DataTable
            Dim DTtiposCambio As DataTable

            DTCambioActual = New DataTable
            DTCambioActual = eRegistroDocumento.TC_Actual_X_Moneda(Format(dtpFechaDoc.Value, "dd/MM/yyyy"))

            TablaImportes = New DataTable
            ' TablaImportes = eRegistroDocumento.ImportesAsientos(DstAsientos, DT_Monedas)

            sSubdiario = String.Empty
            'If Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Tipo")) = "1" Then
            '    sSubdiario = "11"
            'ElseIf Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Tipo")) = "0" Then
            '    sSubdiario = "12"
            'End If
            DT_Monedas = eRegistroDocumento.CargarMonedas
            STRmonSist = eRegistroDocumento.MonedaSistema(DT_Monedas)
            If cboMoneda.SelectedValue = "01" Then
                dValorTC = 1
            Else
                dValorTC = eRegistroDocumento.TipoCambio_Actual(Format(dtpFechaDoc.Value, "dd/MM/yyyy"), cboMoneda.SelectedValue)
            End If

            DTtiposCambio = New DataTable
            DTtiposCambio = Devuelve_ValorTC("2", DT_Monedas, STRmonSist)

            Dst_OrdenCompra = eRegistroDocumento.Datos_OC(pEMPRESA, pPERIODO, pCODMONEDA, pCODIGO_OC, pNUMERO_OC, sCodGestion)
            If Dst_OrdenCompra.Tables(0).Rows.Count = 0 Then
                Dst_OrdenCompra = eRegistroDocumento.Datos_OC_LOGIST(pEMPRESA, txtCodigo.Text.Trim, pCODIGO_OC, pNUMERO_OC)
            End If


            Dim sMes As String
            Dim sCCosto As String = Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("CCosCodigo"))
            Dim dFechaOC As Date = Convert.ToDateTime(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("OcoFechaEmision"))

            sMes = Month(dFechaOC)
            If Convert.ToInt32(sMes) < 10 Then sMes = "0" & sMes


            Dim iExist As Integer

            iExist = eRegistroDocumento.CondicionIngresoOC(gEmpresa, gPeriodo, cboMoneda.SelectedValue, pCODIGO_OC, pNUMERO_OC)
            If iExist = 0 And bolNuevaMod = True Then
                'sMesOC = eRegistroDocumento.Devuelve_MesOC(sCodGestion, gEmpresa, pCODIGO_OC, pNUMERO_OC)
                'sMes = sMesOC
                StrCODGESTION = eRegistroDocumento.Insertar_Voucher_Compras_Tempo(gEmpresa, sSubdiario, sMes, eRegistroDocumento.AbreviaturaMoneda(cboMoneda.SelectedValue), _
                             gPeriodo, Format(dtpFechaBase.Value, "dd/MM/yyyy"), 0, 0, dValorTC, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), _
                            "2", Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("OcoParaQue")), "1", STRmonSist, _
                            eRegistroDocumento.AbreviaturaMoneda(cboMoneda.SelectedValue), gUsuario, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), 1, NomPc.ComputerName, _
                            DstAsientos, DT_Monedas, DTCambioActual, TablaImportes, _
                            DTtiposCambio, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), 2, "2", 0, String.Empty, _
                            Trim(txtCodAcreedor.Text), cboDocumento.SelectedValue, Trim(txtSerie.Text), Trim(txtNroDocumento.Text), _
                            Math.Round(Convert.ToDouble(txtValorCompra.Text), 2), Math.Round(Convert.ToDouble(txtImporteIGV.Text), 2), Math.Round(Convert.ToDouble(txtImporteTotal.Text), 2), _
                            Format(dtpFechaDoc.Value, "dd/MM/yyyy"), Format(dtpFechaVen.Value, "dd/MM/yyyy"), pCODIGO_OC, pNUMERO_OC, sCodGestion, _
                            bolEdit, Edit_sCodGestion, sCCosto)
                If StrCODGESTION = String.Empty Then
                    MessageBox.Show("Ocurrio un Error al Generar el Asiento de Compras Automatico", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                End If

            ElseIf (iExist > 0 Or bolNuevaMod = False) Then

                Dim f_Igv As String
                Dim f_Detr As String = "0"
                Dim f_Perc As String = "0"

                If chkIGV.Checked = True Then
                    f_Igv = "1"
                Else
                    f_Igv = "0"
                End If
                If cboDP.SelectedIndex = 0 Then
                    f_Detr = "1"
                ElseIf cboDP.SelectedIndex = 1 Then
                    f_Perc = "1"
                ElseIf cboDP.SelectedIndex = -1 Then
                    f_Detr = "0"
                    f_Perc = "0"
                End If

                'sCCosto = Trim(eRegistroDocumento.CenCostoOrigen_OC_varias(gEmpresa, gPeriodo, pCODIGO_OC, pNUMERO_OC))

                StrCODGESTION = eRegistroDocumento.VariosIngresosOC(gEmpresa, gPeriodo, sMes, cboMoneda.SelectedValue, pCODIGO_OC, pNUMERO_OC, Format(dtpFechaBase.Value, "dd/MM/yyyy"), _
                     Trim(eRegistroDocumento.AbrevDocumento(cboDocumento.SelectedValue)), Trim(txtSerie.Text), Trim(txtNroDocumento.Text), Format(dtpFechaDoc.Value, "dd/MM/yyyy"), _
                     Format(dtpFechaVen.Value, "dd/MM/yyyy"), Math.Round(Convert.ToDouble(txtValorCompra.Text), 2), Math.Round(Convert.ToDouble(txtImporteIGV.Text), 2), _
                     Math.Round(Convert.ToDouble(txtImporteTotal.Text), 2), sCCosto, Trim(txtCodigo.Text), Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("OcoParaQue")), Math.Round(Convert.ToDouble(txtTasaIGV.Text), 2), _
                     f_Igv, f_Detr, f_Perc, gUsuario, NomPc.ComputerName, Format(Today, "dd/MM/yyyy"), DT_Monedas, DstAsientos, bolEdit, Edit_sCodGestion)
                If StrCODGESTION = String.Empty Then
                    MessageBox.Show("Ocurrio un Error al Generar el Asiento de Compras Automatico", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                End If
            End If


            Dim pMes As String
            Dim dFechaOC1 As Date = Convert.ToDateTime(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("OcoFechaEmision"))
            pMes = Month(dFechaOC1)
            If Convert.ToInt32(pMes) < 10 Then pMes = "0" & pMes

            If dImporteOC > CDbl(txtImporteTotal.Text) And bolNuevaMod = True Then
                If Valida_GridDetOC() = False Then Return False
                '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                For XFil As Integer = 0 To DT_DetalleOC.Rows.Count - 1
                    If CDbl(DT_DetalleOC.Rows(XFil).Item("Importe")) <> 0 Then
                        eRegistroDocumento.fInsertarDetalleTemp(pEMPRESA, StrCODGESTION, pPERIODO, pMes, pCODMONEDA, pCODIGO_OC, pNUMERO_OC, _
                                DT_DetalleOC.Rows(XFil).Item("Item"), DT_DetalleOC.Rows(XFil).Item("Codigo"), DT_DetalleOC.Rows(XFil).Item("Cuenta").ToString, _
                                DT_DetalleOC.Rows(XFil).Item("Importe"), dFechaOC1, gUsuario)
                    End If
                Next
                '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
            Else
                For XFil As Integer = 0 To DT_DetalleOC.Rows.Count - 1
                    eRegistroDocumento.fInsertarDetalleTemp(pEMPRESA, StrCODGESTION, pPERIODO, pMes, pCODMONEDA, pCODIGO_OC, pNUMERO_OC, _
                            DT_DetalleOC.Rows(XFil).Item("Item"), DT_DetalleOC.Rows(XFil).Item("Codigo"), DT_DetalleOC.Rows(XFil).Item("Cuenta"), _
                            Math.Round(DT_DetalleOC.Rows(XFil).Item("CantidadGrupoSol") * DT_DetalleOC.Rows(XFil).Item("Precio"), 2), dFechaOC1, gUsuario)
                Next
            End If


            'ACTUALIZA SALDO O/C
            Call eRegistroDocumento.UPDATE_SALDO_OC(gEmpresa, pCODIGO_OC, pNUMERO_OC, Convert.ToDouble(txtImporteTotal.Text))

            'AMARRE DE GESTION CON CODIGO DE REGISTRO
            If bolEdit = False Then
                Call eRegistroDocumento.UPDATE_COD_TESO(StrCODGESTION, gEmpresa, gPeriodo, sMes, cboMoneda.SelectedValue, pID_REGISTRO)
            Else
                Call eRegistroDocumento.UPDATE_COD_TESO(Edit_sCodGestion, gEmpresa, gPeriodo, sMes, cboMoneda.SelectedValue, pID_REGISTRO)
            End If




            iContPlan = eRegistroDocumento.ExistePlanCuentas(pEMPRESA)
            DT_Documentos = eRegistroDocumento.DocumentosCompra(pEMPRESA)
            StrAbrevDoc = Trim(eRegistroDocumento.AbrevDocumento(cboDocumento.SelectedValue))
            DT_Plantilla = eRegistroDocumento.PlantillaCompras(pEMPRESA, pPERIODO, pABRVMONEDA, pCODMONEDA, pCODIGO_OC, pNUMERO_OC, sCodGestion, IIf(chkIGV.Checked = True, "1", "0"))
            D_o_H = Asiento_Segun_Doc(DT_Documentos, StrAbrevDoc)

            If cboDP.SelectedIndex = -1 Then

                For z As Integer = 0 To DT_Monedas.Rows.Count - 1
                    CrearTablasAsientos(DT_Monedas.Rows(z).Item("Abreviado"))
                Next

                StrAbrevMoneda = eRegistroDocumento.AbreviaturaMoneda(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Moneda"))

                If iContPlan > 0 Then
                    i = 0
                    Do While DT_Monedas.Rows.Count > i

                        Do While DT_Plantilla.Rows.Count > IntFilas

                            StrTabla = "TmpAsiento" & DT_Monedas.Rows(i).Item("Abreviado")
                            MonedaVuelta = DT_Monedas.Rows(i).Item("Abreviado")
                            DrFilaCab = DstAsientos.Tables(StrTabla).NewRow

                            sCtaPadre = Trim(DT_Plantilla.Rows(IntFilas).Item("CtaPadre"))
                            StrCodCuenta = DT_Plantilla.Rows(IntFilas).Item(0).ToString

                            DrFilaCab("Secuencia") = Generar_Secuencia(DstAsientos.Tables(StrTabla))
                            DrFilaCab("Codigo_Cuenta") = StrCodCuenta

                            If eRegistroDocumento.Cta_Con_Anexo(pEMPRESA, StrCodCuenta, pPERIODO) = True Then
                                DrFilaCab("Codigo_Anexo") = Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("PrvCodigo")
                                DrFilaCab("Cod_Tipo_Anexo") = Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Cod_TipoAnexo")
                            Else
                                DrFilaCab("Codigo_Anexo") = String.Empty
                                DrFilaCab("Cod_Tipo_Anexo") = String.Empty
                            End If

                            If sCtaPadre = "42" Then
                                DrFilaCab("Tipo_Doc") = StrAbrevDoc
                                DrFilaCab("Serie_Doc") = Trim(txtSerie.Text)
                                DrFilaCab("Nro_Doc") = Trim(txtNroDocumento.Text)

                            ElseIf (sCtaPadre = "40" Or sCtaPadre = "60") Then
                                DrFilaCab("Tipo_Doc") = String.Empty
                                DrFilaCab("Serie_Doc") = String.Empty
                                DrFilaCab("Nro_Doc") = String.Empty
                            End If

                            DrFilaCab("MonedaTrans") = MonedaVuelta

                            If StrAbrevMoneda = STRmonSist Then
                                TCvoucher = 1
                            Else
                                TCvoucher = eRegistroDocumento.TipoCambio_Actual(Format(dtpFechaDoc.Value, "dd/MM/yyyy"), cboMoneda.SelectedValue)
                                If TCvoucher = 0 Then
                                    MessageBox.Show("No Existe Tipo de Cambio Registrado para la Fecha : " & Format(dtpFechaDoc.Value, "dd/MM/yyyy") & "", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Return False
                                End If
                            End If

                            If MonedaVuelta = STRmonSist Then
                                myTC = 1
                                DrFilaCab("MontoTC") = Format(myTC, "#,##0.00")
                                DrFilaCab("FechaTC") = Format(dtpFechaDoc.Value, "dd/MM/yyyy")

                                If D_o_H = "H" Then
                                    If sCtaPadre = "42" Then
                                        DrFilaCab("Debe") = Format(0, "#,##0.00")
                                        DrFilaCab("Haber") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
                                    ElseIf sCtaPadre = "40" Then
                                        DrFilaCab("Debe") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
                                        DrFilaCab("Haber") = Format(0, "#,##0.00")
                                    ElseIf sCtaPadre = "60" Then

                                        Dim dblMonto60 As Double = 0
                                        If dImporteOC > CDbl(txtImporteTotal.Text) Then
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                        Else
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                            'dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
                                        End If
                                        DrFilaCab("Debe") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)
                                        DrFilaCab("Haber") = Format(0, "#,##0.00")

                                    End If
                                ElseIf D_o_H = "D" Then
                                    If sCtaPadre = "42" Then
                                        DrFilaCab("Debe") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
                                        DrFilaCab("Haber") = Format(0, "#,##0.00")
                                    ElseIf sCtaPadre = "40" Then
                                        DrFilaCab("Debe") = Format(0, "#,##0.00")
                                        DrFilaCab("Haber") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
                                    ElseIf sCtaPadre = "60" Then
                                        Dim dblMonto60 As Double = 0
                                        If dImporteOC > CDbl(txtImporteTotal.Text) Then
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                        Else
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                            'dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
                                        End If
                                        DrFilaCab("Debe") = Format(0, "#,##0.00")
                                        DrFilaCab("Haber") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)
                                    End If
                                End If

                            Else
                                myTC = eRegistroDocumento.TipoCambio_Actual(Format(dtpFechaDoc.Value, "dd/MM/yyyy"), DT_Monedas.Rows(i).Item("Codigo"))

                                If myTC = 0 Then
                                    MessageBox.Show("No ingreso Tipo de Cambio para la Moneda en " & MonedaVuelta, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    Return False
                                End If

                                DrFilaCab("MontoTC") = Format(myTC, "#,##0.000")
                                DrFilaCab("FechaTC") = Format(dtpFechaDoc.Value, "dd/MM/yyyy")

                                If D_o_H = "H" Then
                                    If sCtaPadre = "42" Then
                                        DrFilaCab("Debe") = Format(0, "#,##0.00")
                                        DrFilaCab("Haber") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
                                    ElseIf sCtaPadre = "40" Then
                                        DrFilaCab("Debe") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
                                        DrFilaCab("Haber") = Format(0, "#,##0.00")
                                    ElseIf sCtaPadre = "60" Then

                                        Dim dblMonto60 As Double = 0
                                        If dImporteOC > CDbl(txtImporteTotal.Text) Then
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                        Else
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                            'dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
                                        End If
                                        DrFilaCab("Debe") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)
                                        DrFilaCab("Haber") = Format(0, "#,##0.00")

                                    End If
                                ElseIf D_o_H = "D" Then
                                    If sCtaPadre = "42" Then
                                        DrFilaCab("Debe") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
                                        DrFilaCab("Haber") = Format(0, "#,##0.00")
                                    ElseIf sCtaPadre = "40" Then
                                        DrFilaCab("Debe") = Format(0, "#,##0.00")
                                        DrFilaCab("Haber") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
                                    ElseIf sCtaPadre = "60" Then

                                        Dim dblMonto60 As Double = 0
                                        If dImporteOC > CDbl(txtImporteTotal.Text) Then
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                        Else
                                            dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
                                            'dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
                                        End If
                                        DrFilaCab("Debe") = Format(0, "#,##0.00")
                                        DrFilaCab("Haber") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)

                                    End If
                                End If
                            End If

                            DrFilaCab("FechaDoc") = Format(dtpFechaDoc.Value, "dd/MM/yyyy")
                            DrFilaCab("Caduca_Fecha") = Format(dtpFechaVen.Value, "dd/MM/yyyy")

                            DrFilaCab("Ind_Trans_Dif") = "0"
                            DrFilaCab("Gen_Cta_Destino") = ""
                            'DrFilaCab("ComentarioCab") = Trim(Dst_OrdenCompra.Tables("OC_Detalle").Rows(IntFilas).Item("DrqAgregado"))
                            DrFilaCab("ComentarioCab") = String.Empty

                            If (sCtaPadre = "60") Then
                                DrFilaCab("ID_CentroCosto") = Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("CCosCodigo"))
                            Else
                                DrFilaCab("ID_CentroCosto") = String.Empty
                            End If

                            '**************** VERIFICAR AREA Y CENTRO DE COSTO
                            'If Trim(eRegistroDocumento.Con_CenCosto(pEMPRESA, pPERIODO, StrCodCuenta)) = "1" Then
                            '    DrFilaCab("ID_CentroCosto") = Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("CCosCodigo"))
                            'Else
                            '    DrFilaCab("ID_CentroCosto") = String.Empty
                            'End If
                            '****************************************************************************************************************

                            DrFilaCab("Area") = String.Empty

                            If MonedaVuelta = STRmonSist Then
                                DrFilaCab("Moneda_Sist") = "1"
                            Else
                                DrFilaCab("Moneda_Sist") = "0"
                            End If
                            If DT_Monedas.Rows(i).Item("Codigo") = cboMoneda.SelectedValue Then
                                DrFilaCab("DocTransact") = "1"
                            Else
                                DrFilaCab("DocTransact") = "0"
                            End If

                            DrFilaCab("MontoOriginal") = 0
                            DrFilaCab("ExisteDoc") = "0"

                            DstAsientos.Tables(StrTabla).Rows.Add(DrFilaCab)
                            IntFilas += 1
                        Loop

                        IntFilas = 0
                        i += 1
                    Loop
                End If

            End If
            '***********************************************************************

            eRegistroDocumento.Insertar_Asiento_Temp(DstAsientos, DT_Monedas, StrCODGESTION, gEmpresa, gPeriodo, sMes, pCODIGO_OC, pNUMERO_OC)

            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    'Private Function GENERA_ASIENTO_COMPRAS(ByVal pEMPRESA As String, ByVal pPERIODO As String, ByVal pABRVMONEDA As String, _
    '                                ByVal pCODMONEDA As String, ByVal pCODIGO_OC As String, ByVal pNUMERO_OC As String, _
    '                                ByVal pID_REGISTRO As String) As Boolean

    '    Try
    '        Dim DT_Plantilla As New DataTable
    '        Dim DT_Monedas As New DataTable
    '        Dim DT_Documentos As New DataTable
    '        Dim StrAbrevDoc As String = String.Empty
    '        Dim D_o_H As String
    '        Dim StrTabla As String
    '        Dim StrAbrevMoneda As String
    '        Dim StrCodCuenta As String
    '        Dim sCtaPadre As String
    '        Dim STRmonSist As String

    '        Dim TCvoucher As Double
    '        Dim myTC As Double

    '        Dim DrFilaCab As DataRow
    '        Dim MonedaVuelta As String
    '        Dim i As Integer = 0
    '        Dim IntFilas As Integer = 0
    '        Dim Dst_OrdenCompra As New DataSet
    '        Dim StrCODGESTION As String = String.Empty
    '        Dim iContPlan As Integer = 0



    '        If dImporteOC > CDbl(txtImporteTotal.Text) And bolNuevaMod = True Then
    '            If Valida_GridDetOC() = False Then Return False
    '        End If


    '        iContPlan = eRegistroDocumento.ExistePlanCuentas(pEMPRESA)
    '        DT_Monedas = eRegistroDocumento.CargarMonedas
    '        DT_Documentos = eRegistroDocumento.DocumentosCompra(pEMPRESA)
    '        StrAbrevDoc = Trim(eRegistroDocumento.AbrevDocumento(cboDocumento.SelectedValue))
    '        DT_Plantilla = eRegistroDocumento.PlantillaCompras(pEMPRESA, pPERIODO, pABRVMONEDA, pCODMONEDA, pCODIGO_OC, pNUMERO_OC, sCodGestion, IIf(chkIGV.Checked = True, "1", "0"))
    '        Dst_OrdenCompra = eRegistroDocumento.Datos_OC(pEMPRESA, pPERIODO, pCODMONEDA, pCODIGO_OC, pNUMERO_OC, sCodGestion)
    '        If Dst_OrdenCompra.Tables(0).Rows.Count = 0 Then
    '            Dst_OrdenCompra = eRegistroDocumento.Datos_OC_LOGIST(pEMPRESA, txtCodigo.Text.Trim, pCODIGO_OC, pNUMERO_OC)
    '        End If
    '        STRmonSist = eRegistroDocumento.MonedaSistema(DT_Monedas)
    '        D_o_H = Asiento_Segun_Doc(DT_Documentos, StrAbrevDoc)

    '        If cboDP.SelectedIndex = -1 Then

    '            For z As Integer = 0 To DT_Monedas.Rows.Count - 1
    '                CrearTablasAsientos(DT_Monedas.Rows(z).Item("Abreviado"))
    '            Next

    '            StrAbrevMoneda = eRegistroDocumento.AbreviaturaMoneda(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Moneda"))

    '            If iContPlan > 0 Then
    '                i = 0
    '                Do While DT_Monedas.Rows.Count > i

    '                    Do While DT_Plantilla.Rows.Count > IntFilas

    '                        StrTabla = "TmpAsiento" & DT_Monedas.Rows(i).Item("Abreviado")
    '                        MonedaVuelta = DT_Monedas.Rows(i).Item("Abreviado")
    '                        DrFilaCab = DstAsientos.Tables(StrTabla).NewRow

    '                        sCtaPadre = Trim(DT_Plantilla.Rows(IntFilas).Item("CtaPadre"))
    '                        StrCodCuenta = DT_Plantilla.Rows(IntFilas).Item(0).ToString

    '                        DrFilaCab("Secuencia") = Generar_Secuencia(DstAsientos.Tables(StrTabla))
    '                        DrFilaCab("Codigo_Cuenta") = StrCodCuenta

    '                        If eRegistroDocumento.Cta_Con_Anexo(pEMPRESA, StrCodCuenta, pPERIODO) = True Then
    '                            DrFilaCab("Codigo_Anexo") = Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("PrvCodigo")
    '                            DrFilaCab("Cod_Tipo_Anexo") = Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Cod_TipoAnexo")
    '                        Else
    '                            DrFilaCab("Codigo_Anexo") = String.Empty
    '                            DrFilaCab("Cod_Tipo_Anexo") = String.Empty
    '                        End If

    '                        If sCtaPadre = "42" Then
    '                            DrFilaCab("Tipo_Doc") = StrAbrevDoc
    '                            DrFilaCab("Serie_Doc") = Trim(txtSerie.Text)
    '                            DrFilaCab("Nro_Doc") = Trim(txtNroDocumento.Text)

    '                        ElseIf (sCtaPadre = "40" Or sCtaPadre = "60") Then
    '                            DrFilaCab("Tipo_Doc") = String.Empty
    '                            DrFilaCab("Serie_Doc") = String.Empty
    '                            DrFilaCab("Nro_Doc") = String.Empty
    '                        End If

    '                        DrFilaCab("MonedaTrans") = MonedaVuelta

    '                        If StrAbrevMoneda = STRmonSist Then
    '                            TCvoucher = 1
    '                        Else
    '                            TCvoucher = eRegistroDocumento.TipoCambio_Actual(Format(dtpFechaDoc.Value, "dd/MM/yyyy"), cboMoneda.SelectedValue)
    '                            If TCvoucher = 0 Then
    '                                MessageBox.Show("No Existe Tipo de Cambio Registrado para la Fecha : " & Format(dtpFechaDoc.Value, "dd/MM/yyyy") & "", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                                Return False
    '                            End If
    '                        End If

    '                        If MonedaVuelta = STRmonSist Then
    '                            myTC = 1
    '                            DrFilaCab("MontoTC") = Format(myTC, "#,##0.00")
    '                            DrFilaCab("FechaTC") = Format(dtpFechaDoc.Value, "dd/MM/yyyy")

    '                            If D_o_H = "H" Then
    '                                If sCtaPadre = "42" Then
    '                                    DrFilaCab("Debe") = Format(0, "#,##0.00")
    '                                    DrFilaCab("Haber") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
    '                                ElseIf sCtaPadre = "40" Then
    '                                    DrFilaCab("Debe") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
    '                                    DrFilaCab("Haber") = Format(0, "#,##0.00")
    '                                ElseIf sCtaPadre = "60" Then

    '                                    Dim dblMonto60 As Double = 0
    '                                    If dImporteOC > CDbl(txtImporteTotal.Text) Then
    '                                        dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
    '                                    Else
    '                                        dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
    '                                    End If
    '                                    DrFilaCab("Debe") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)
    '                                    DrFilaCab("Haber") = Format(0, "#,##0.00")

    '                                End If
    '                            ElseIf D_o_H = "D" Then
    '                                If sCtaPadre = "42" Then
    '                                    DrFilaCab("Debe") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
    '                                    DrFilaCab("Haber") = Format(0, "#,##0.00")
    '                                ElseIf sCtaPadre = "40" Then
    '                                    DrFilaCab("Debe") = Format(0, "#,##0.00")
    '                                    DrFilaCab("Haber") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
    '                                ElseIf sCtaPadre = "60" Then
    '                                    Dim dblMonto60 As Double = 0
    '                                    If dImporteOC > CDbl(txtImporteTotal.Text) Then
    '                                        dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
    '                                    Else
    '                                        dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
    '                                    End If
    '                                    DrFilaCab("Debe") = Format(0, "#,##0.00")
    '                                    DrFilaCab("Haber") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)
    '                                End If
    '                            End If

    '                        Else
    '                            myTC = eRegistroDocumento.TipoCambio_Actual(Format(dtpFechaDoc.Value, "dd/MM/yyyy"), DT_Monedas.Rows(i).Item("Codigo"))

    '                            If myTC = 0 Then
    '                                MessageBox.Show("No ingreso Tipo de Cambio para la Moneda en " & MonedaVuelta, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                                Return False
    '                            End If

    '                            DrFilaCab("MontoTC") = Format(myTC, "#,##0.000")
    '                            DrFilaCab("FechaTC") = Format(dtpFechaDoc.Value, "dd/MM/yyyy")

    '                            If D_o_H = "H" Then
    '                                If sCtaPadre = "42" Then
    '                                    DrFilaCab("Debe") = Format(0, "#,##0.00")
    '                                    DrFilaCab("Haber") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
    '                                ElseIf sCtaPadre = "40" Then
    '                                    DrFilaCab("Debe") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
    '                                    DrFilaCab("Haber") = Format(0, "#,##0.00")
    '                                ElseIf sCtaPadre = "60" Then

    '                                    Dim dblMonto60 As Double = 0
    '                                    If dImporteOC > CDbl(txtImporteTotal.Text) Then
    '                                        dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
    '                                    Else
    '                                        dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
    '                                    End If
    '                                    DrFilaCab("Debe") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)
    '                                    DrFilaCab("Haber") = Format(0, "#,##0.00")

    '                                End If
    '                            ElseIf D_o_H = "D" Then
    '                                If sCtaPadre = "42" Then
    '                                    DrFilaCab("Debe") = Math.Round(CDbl(txtImporteTotal.Text) * TCvoucher / myTC, 2)
    '                                    DrFilaCab("Haber") = Format(0, "#,##0.00")
    '                                ElseIf sCtaPadre = "40" Then
    '                                    DrFilaCab("Debe") = Format(0, "#,##0.00")
    '                                    DrFilaCab("Haber") = Math.Round(CDbl(txtImporteIGV.Text) * TCvoucher / myTC, 2)
    '                                ElseIf sCtaPadre = "60" Then

    '                                    Dim dblMonto60 As Double = 0
    '                                    If dImporteOC > CDbl(txtImporteTotal.Text) Then
    '                                        dblMonto60 = Monto60_AcumXCuentaGrillaDetOC(StrCodCuenta)
    '                                    Else
    '                                        dblMonto60 = Monto60_AcumXCuenta(Dst_OrdenCompra.Tables("OC_Detalle"), StrCodCuenta)
    '                                    End If
    '                                    DrFilaCab("Debe") = Format(0, "#,##0.00")
    '                                    DrFilaCab("Haber") = Math.Round(CDbl(dblMonto60) * TCvoucher / myTC, 2)

    '                                End If
    '                            End If
    '                        End If

    '                        DrFilaCab("FechaDoc") = Format(dtpFechaDoc.Value, "dd/MM/yyyy")
    '                        DrFilaCab("Caduca_Fecha") = Format(dtpFechaVen.Value, "dd/MM/yyyy")

    '                        DrFilaCab("Ind_Trans_Dif") = "0"
    '                        DrFilaCab("Gen_Cta_Destino") = ""
    '                        'DrFilaCab("ComentarioCab") = Trim(Dst_OrdenCompra.Tables("OC_Detalle").Rows(IntFilas).Item("DrqAgregado"))
    '                        DrFilaCab("ComentarioCab") = String.Empty

    '                        If (sCtaPadre = "60") Then
    '                            DrFilaCab("ID_CentroCosto") = Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("CCosCodigo"))
    '                        Else
    '                            DrFilaCab("ID_CentroCosto") = String.Empty
    '                        End If

    '                        '**************** VERIFICAR AREA Y CENTRO DE COSTO
    '                        'If Trim(eRegistroDocumento.Con_CenCosto(pEMPRESA, pPERIODO, StrCodCuenta)) = "1" Then
    '                        '    DrFilaCab("ID_CentroCosto") = Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("CCosCodigo"))
    '                        'Else
    '                        '    DrFilaCab("ID_CentroCosto") = String.Empty
    '                        'End If
    '                        '****************************************************************************************************************

    '                        DrFilaCab("Area") = String.Empty

    '                        If MonedaVuelta = STRmonSist Then
    '                            DrFilaCab("Moneda_Sist") = "1"
    '                        Else
    '                            DrFilaCab("Moneda_Sist") = "0"
    '                        End If
    '                        If DT_Monedas.Rows(i).Item("Codigo") = cboMoneda.SelectedValue Then
    '                            DrFilaCab("DocTransact") = "1"
    '                        Else
    '                            DrFilaCab("DocTransact") = "0"
    '                        End If

    '                        DrFilaCab("MontoOriginal") = 0
    '                        DrFilaCab("ExisteDoc") = "0"

    '                        DstAsientos.Tables(StrTabla).Rows.Add(DrFilaCab)
    '                        IntFilas += 1
    '                    Loop

    '                    IntFilas = 0
    '                    i += 1
    '                Loop
    '            End If

    '        End If
    '        '***********************************************************************

    '        Dim sSubdiario As String = String.Empty
    '        Dim dValorTC As Double
    '        Dim DTCambioActual As DataTable
    '        Dim TablaImportes As DataTable
    '        Dim DTtiposCambio As DataTable

    '        DTCambioActual = New DataTable
    '        DTCambioActual = eRegistroDocumento.TC_Actual_X_Moneda(Format(dtpFechaDoc.Value, "dd/MM/yyyy"))

    '        TablaImportes = New DataTable
    '        ' TablaImportes = eRegistroDocumento.ImportesAsientos(DstAsientos, DT_Monedas)


    '        sSubdiario = String.Empty
    '        'If Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Tipo")) = "1" Then
    '        '    sSubdiario = "11"
    '        'ElseIf Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("Tipo")) = "0" Then
    '        '    sSubdiario = "12"
    '        'End If

    '        If cboMoneda.SelectedValue = "01" Then
    '            dValorTC = 1
    '        Else
    '            dValorTC = eRegistroDocumento.TipoCambio_Actual(Format(dtpFechaDoc.Value, "dd/MM/yyyy"), cboMoneda.SelectedValue)
    '        End If

    '        DTtiposCambio = New DataTable
    '        DTtiposCambio = Devuelve_ValorTC("2", DT_Monedas, STRmonSist)

    '        Dim sMes As String
    '        Dim sCCosto As String = Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("CCosCodigo"))
    '        Dim dFechaOC As Date = Convert.ToDateTime(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("OcoFechaEmision"))

    '        sMes = Month(dFechaOC)
    '        If Convert.ToInt32(sMes) < 10 Then sMes = "0" & sMes


    '        Dim iExist As Integer

    '        iExist = eRegistroDocumento.CondicionIngresoOC(gEmpresa, gPeriodo, cboMoneda.SelectedValue, pCODIGO_OC, pNUMERO_OC)
    '        If iExist = 0 And bolNuevaMod = True Then
    '            'sMesOC = eRegistroDocumento.Devuelve_MesOC(sCodGestion, gEmpresa, pCODIGO_OC, pNUMERO_OC)
    '            'sMes = sMesOC
    '            StrCODGESTION = eRegistroDocumento.Insertar_Voucher_Compras_Tempo(gEmpresa, sSubdiario, sMes, eRegistroDocumento.AbreviaturaMoneda(cboMoneda.SelectedValue), _
    '                         gPeriodo, Format(dtpFechaBase.Value, "dd/MM/yyyy"), 0, 0, dValorTC, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), _
    '                        "2", Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("OcoParaQue")), "1", STRmonSist, _
    '                        eRegistroDocumento.AbreviaturaMoneda(cboMoneda.SelectedValue), gUsuario, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), 1, NomPc.ComputerName, _
    '                        DstAsientos, DT_Monedas, DTCambioActual, TablaImportes, _
    '                        DTtiposCambio, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), 2, "2", 0, String.Empty, _
    '                        Trim(txtCodAcreedor.Text), cboDocumento.SelectedValue, Trim(txtSerie.Text), Trim(txtNroDocumento.Text), _
    '                        Math.Round(Convert.ToDouble(txtValorCompra.Text), 2), Math.Round(Convert.ToDouble(txtImporteIGV.Text), 2), Math.Round(Convert.ToDouble(txtImporteTotal.Text), 2), _
    '                        Format(dtpFechaDoc.Value, "dd/MM/yyyy"), Format(dtpFechaVen.Value, "dd/MM/yyyy"), pCODIGO_OC, pNUMERO_OC, sCodGestion, _
    '                        bolEdit, Edit_sCodGestion, sCCosto)
    '            If StrCODGESTION = String.Empty Then
    '                MessageBox.Show("Ocurrio un Error al Generar el Asiento de Compras Automatico", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                Return False
    '            End If

    '        ElseIf (iExist > 0 Or bolNuevaMod = False) Then

    '            Dim f_Igv As String
    '            Dim f_Detr As String = "0"
    '            Dim f_Perc As String = "0"

    '            If chkIGV.Checked = True Then
    '                f_Igv = "1"
    '            Else
    '                f_Igv = "0"
    '            End If
    '            If cboDP.SelectedIndex = 0 Then
    '                f_Detr = "1"
    '            ElseIf cboDP.SelectedIndex = 1 Then
    '                f_Perc = "1"
    '            ElseIf cboDP.SelectedIndex = -1 Then
    '                f_Detr = "0"
    '                f_Perc = "0"
    '            End If

    '            'sCCosto = Trim(eRegistroDocumento.CenCostoOrigen_OC_varias(gEmpresa, gPeriodo, pCODIGO_OC, pNUMERO_OC))

    '            StrCODGESTION = eRegistroDocumento.VariosIngresosOC(gEmpresa, gPeriodo, sMes, cboMoneda.SelectedValue, pCODIGO_OC, pNUMERO_OC, Format(dtpFechaBase.Value, "dd/MM/yyyy"), _
    '                 Trim(eRegistroDocumento.AbrevDocumento(cboDocumento.SelectedValue)), Trim(txtSerie.Text), Trim(txtNroDocumento.Text), Format(dtpFechaDoc.Value, "dd/MM/yyyy"), _
    '                 Format(dtpFechaVen.Value, "dd/MM/yyyy"), Math.Round(Convert.ToDouble(txtValorCompra.Text), 2), Math.Round(Convert.ToDouble(txtImporteIGV.Text), 2), _
    '                 Math.Round(Convert.ToDouble(txtImporteTotal.Text), 2), sCCosto, Trim(txtCodigo.Text), Trim(Dst_OrdenCompra.Tables("OC_Cabecera").Rows(0).Item("OcoParaQue")), Math.Round(Convert.ToDouble(txtTasaIGV.Text), 2), _
    '                 f_Igv, f_Detr, f_Perc, gUsuario, NomPc.ComputerName, Format(Today, "dd/MM/yyyy"), DT_Monedas, DstAsientos, bolEdit, Edit_sCodGestion)
    '            If StrCODGESTION = String.Empty Then
    '                MessageBox.Show("Ocurrio un Error al Generar el Asiento de Compras Automatico", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                Return False
    '            End If
    '        End If

    '        'ACTUALIZA SALDO O/C
    '        Call eRegistroDocumento.UPDATE_SALDO_OC(gEmpresa, pCODIGO_OC, pNUMERO_OC, Convert.ToDouble(txtImporteTotal.Text))

    '        'AMARRE DE GESTION CON CODIGO DE REGISTRO
    '        If bolEdit = False Then
    '            Call eRegistroDocumento.UPDATE_COD_TESO(StrCODGESTION, gEmpresa, gPeriodo, sMes, cboMoneda.SelectedValue, pID_REGISTRO)
    '        Else
    '            Call eRegistroDocumento.UPDATE_COD_TESO(Edit_sCodGestion, gEmpresa, gPeriodo, sMes, cboMoneda.SelectedValue, pID_REGISTRO)
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '        Return False
    '    End Try
    'End Function

    Private Function Valida_GridDetOC() As Boolean
        Dim dMontoDeta As Double = 0
        For iFil As Integer = 0 To DT_DetalleOC.Rows.Count - 1
            dMontoDeta += DT_DetalleOC.Rows(iFil).Item("importe")
        Next
        If Math.Round(dMontoDeta, 2) <> Convert.ToDouble(txtValorCompra.Text) Then
            MessageBox.Show("Verifique los montos del Detalle de la Factura", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If
        Return True
    End Function

    Private Function Devuelve_ValorTC(ByVal TCambio As String, ByVal TablaMonedas As DataTable, ByVal MonedaSistema As String) As DataTable
        Dim TablaTC As New DataTable
        Dim DrTC As DataRow
        Dim ValorTC As Double = 0

        TablaTC.Columns.Clear()
        TablaTC.Columns.Add("Moneda")
        TablaTC.Columns.Add("MontoTC")

        For x As Integer = 0 To TablaMonedas.Rows.Count - 1
            DrTC = TablaTC.NewRow
            If TablaMonedas.Rows(x).Item("Abreviado") <> MonedaSistema Then
                Select Case TCambio
                    Case "1"    'Compra
                        ValorTC = eRegistroDocumento.DevuelveMontoTCxMoneda(gEmpresa, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), TablaMonedas.Rows(x).Item("Abreviado"), TCambio)
                        DrTC("Moneda") = TablaMonedas.Rows(x).Item("Abreviado")
                        DrTC("MontoTC") = ValorTC
                    Case "2"    'Venta
                        ValorTC = eRegistroDocumento.DevuelveMontoTCxMoneda(gEmpresa, Format(dtpFechaDoc.Value, "dd/MM/yyyy"), TablaMonedas.Rows(x).Item("Abreviado"), TCambio)
                        DrTC("Moneda") = TablaMonedas.Rows(x).Item("Abreviado")
                        DrTC("MontoTC") = ValorTC
                End Select
                TablaTC.Rows.Add(DrTC)
            End If
        Next
        Return TablaTC
    End Function

    Private Function Monto60_AcumXCuenta(ByVal TablaDetalle As DataTable, ByVal CuentaDet As String) As Double
        Dim DrFilasDet() As DataRow
        Dim AcumImporte As Double = 0
        Try
            DrFilasDet = TablaDetalle.Select("Codigo_CtaContab='" & CuentaDet & "'")
            If DrFilasDet.Length > 0 Then
                For X As Integer = 0 To DrFilasDet.Length - 1
                    AcumImporte += DrFilasDet(X).Item("importe")
                Next
            End If
            Return AcumImporte
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return AcumImporte
    End Function

    'Private Function Monto60_AcumXCuentaGrillaDetOC(ByVal CuentaDet As String) As Double
    '    Dim DrFilasDet() As DataRow
    '    Dim AcumImporte As Double = 0

    '    Try
    '        DrFilasDet = DT_DetalleOC.Select("Cuenta='" & CuentaDet & "'")
    '        If DrFilasDet.Length > 0 Then
    '            For X As Integer = 0 To DrFilasDet.Length - 1
    '                AcumImporte += DrFilasDet(X).Item("importe")
    '            Next
    '        End If
    '        Return AcumImporte
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    '    Return AcumImporte
    'End Function
    Private Function Monto60_AcumXCuentaGrillaDetOC(ByVal CuentaDet As String) As Double
        Dim DrFilasDet() As DataRow
        Dim AcumImporte As Double = 0

        Try
            DrFilasDet = DT_DetalleOC.Select("Cuenta='" & CuentaDet & "'")
            If DrFilasDet.Length > 0 Then
                For X As Integer = 0 To DrFilasDet.Length - 1
                    If CDbl(DrFilasDet(X).Item("importe")) > 0 Then
                        AcumImporte += DrFilasDet(X).Item("Importe")
                    Else
                        AcumImporte += Math.Round(DT_DetalleOC.Rows(X).Item("CantidadGrupoSol") * DT_DetalleOC.Rows(X).Item("Precio"), 2)
                    End If
                Next
            End If
            Return AcumImporte
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return AcumImporte
    End Function

    Private Function Asiento_Segun_Doc(ByVal TablaPar As DataTable, ByVal ComboDoc As String) As String
        Dim DoH As String = String.Empty

        For i As Integer = 0 To TablaPar.Rows.Count - 1
            If TablaPar.Rows(i).Item("Codigo") = ComboDoc Then
                DoH = TablaPar.Rows(i).Item("Otros")
                Exit For
            End If
        Next
        Return DoH
    End Function

    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    '

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Fila("SituacionId") = "00001" Then
            Dim iResultado As Int32
            Dim iResultadoDesSaldoDoc As Int32
            Dim iActivo As Integer
            Dim sCodigoActual2 As String = ""
            If sTab = 0 Then
                Try
                    If Me.dgvDetalle.Rows.Count > 0 Then
                        iActivo = 3
                        If (MessageBox.Show("�Desea Eliminar el Registro?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                            'VerPosicion()
                            eRegistroDocumento = New clsRegistroDocumento
                            'encuestadoras compradas, apoyando el fraude, le daran el favoritismo a keiko hasta el dia de las elecciones, y como alan le confirmara la victoria diran gano en las encuestas y gano en la 2da vuelta! no a la FARSA, SI a las barricadas, SI a la marcha de los 4 SUYOS!!!
                            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                            'ELIMINAR(UPDATE) REGISTROS DE LAS TABLAS COMUN
                            If eRegistroDocumento.Delete_Registro(Fila("IdRegistro"), gEmpresa, gPeriodo) = False Then
                                MessageBox.Show("Ocurrio un Error en el Proceso para Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Exit Sub
                            End If
                            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                            iResultado = eRegistroDocumento.fEliminar(Fila("IdRegistro"), iActivo, gEmpresa) '18/
                            If Trim(Fila("IdDocumento")) = "05" Then
                                Dim NCGlosaFactura As String = ""
                                Dim iResultadoActFacdeNC As Integer = 0
                                NCGlosaFactura = Trim("NA" & Fila("Serie") & "-" & Fila("NroDocumento"))
                                iResultadoActFacdeNC = eRegistroDocumento.fActualizarCamposFactura(49, Fila("NCCodFT"), Fila("NCCodFTEmpr"), Trim(NCGlosaFactura), Fila("ImporteTotal"))
                            End If

                            ''en caso de adelanto de valorizacion q hareeee�
                            iResultadoDesSaldoDoc = eRegistroDocumento.fEliminar2(Fila("IdRegistro"), 4, gEmpresa) '18/

                            'eValorizaciones = New clsValorizaciones
                            'iResultadoMod = eValorizaciones.fActualizarSaldos(52, 0, 0, 0, Today(), Trim(sCodigoActual), gEmpresa, Trim(IdEmprValor), Trim(IdValorizacion), "")


                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                            Dim iResultado1 As Integer = 0
                            ePagoProveedores = New clsPagoProveedores
                            'If Trim(cboMoneda.SelectedValue) = "01" Then

                            If Fila("IdMoneda") = "01" Then
                                Dim dtTable As DataTable
                                dtTable = New DataTable
                                'cboTipoAcreedor.SelectedValue = Fila("TipoAcreedorId").ToString
                                'txtCod.Text = Fila("CodProveedor").ToString
                                dtTable = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), "01")
                                If dtTable.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, Val(Fila("ImporteTotal").ToString), "01")
                                ElseIf dtTable.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda soles ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoSolesProv As Double = 0
                                    Dim MontoSaldoSolesNew As Double = 0
                                    IdSaldoProv = dtTable.Rows(0).Item("IdSaldo")
                                    MontoSaldoSolesProv = dtTable.Rows(0).Item("MontoSaldo")
                                    MontoSaldoSolesNew = MontoSaldoSolesProv - Val(Trim(Fila("ImporteTotal").ToString)) 'Val(txtImporteTotal.Text)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, MontoSaldoSolesNew, "01")
                                End If
                            ElseIf Fila("IdMoneda") = "02" Then
                                Dim dtTable2 As DataTable
                                dtTable2 = New DataTable
                                dtTable2 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), "02")
                                If dtTable2.Rows.Count = 0 Then
                                    'crear
                                    Dim IdSaldoProv As String = ""
                                    ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                    IdSaldoProv = ePagoProveedores.sCodFuturo
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, Val(Fila("ImporteTotal").ToString), "02")
                                ElseIf dtTable2.Rows.Count = 1 Then
                                    'actualizar saldo prove moneda dolares ++++++++++
                                    Dim IdSaldoProv As String = ""
                                    Dim MontoSaldoDolaresProv As Double = 0
                                    Dim MontoSaldoDolaresNew As Double = 0
                                    IdSaldoProv = dtTable2.Rows(0).Item("IdSaldo")
                                    MontoSaldoDolaresProv = dtTable2.Rows(0).Item("MontoSaldo")
                                    MontoSaldoDolaresNew = MontoSaldoDolaresProv - Trim(Fila("ImporteTotal").ToString) 'Val(txtImporteTotal.Text)
                                    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, MontoSaldoDolaresNew, "02")
                                End If
                            End If

                            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                            If iResultado > 0 Then
                                mMostrarGrilla()
                            End If
                        Else
                            Exit Sub
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        ElseIf Fila("SituacionId") <> "00001" Then
            Dim Mensaje As String = ""
            If Fila("SituacionId") = "00002" Then
                Mensaje = "No se puede eliminar el documento, porque se encuentra Aprovado."
            ElseIf Fila("SituacionId") = "00003" Then
                Mensaje = "No se puede eliminar el documento, porque se encuentra Provisionado."
            ElseIf Fila("SituacionId") = "00004" Then
                Mensaje = "No se puede eliminar el documento, porque se encuentra Cancelado."
            End If
            MessageBox.Show(Mensaje, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If

        'If Fila("Estados") = "Activo" Then
        'Else
        'MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End If
    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'ERPDHMONTDataSet.Area' Puede moverla o quitarla seg�n sea necesario.
        'Me.AreaTableAdapter.Fill(Me.ERPDHMONTDataSet.Area)
        '       Dim a As Integer = 0
        '        for a to dgvDetalle.Columns.Count-1 
      

        dtpF1.Value = Now.Date().ToShortDateString()
        dtpF2.Value = Now.Date().ToShortDateString()

        eGastosGenerales = New clsGastosGenerales

        cboTodosProv.DataSource = eGastosGenerales.fListarProveedores2(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboTodosProv.ValueMember = "RUC"
            cboTodosProv.DisplayMember = "AnaliticoDescripcion"
            cboTodosProv.SelectedIndex = -1
            'txtUsuario.Focus()
        End If

        eUsuario = New Usuarios

        cboUsuarios.DataSource = eUsuario.ListaUsuario(gEmpresa, glbSisCodigo)
        If eUsuario.iNroRegistros > 0 Then
            cboUsuarios.ValueMember = "UsuCodigo"
            cboUsuarios.DisplayMember = "UsuCodigo"
            cboUsuarios.SelectedIndex = -1
        End If

        For x As Integer = 0 To dgvDetalle.Columns.Count - 1
            'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
            dgvDetalle.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        'For x As Integer = 0 To dgvDetalleOC.Columns.Count - 1
        '    'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
        '    dgvDetalleOC.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
        'Next

        'dgvDetalle.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable


        mMostrarGrilla()
        'dtpFechaInicio.Value = Now.Date().ToShortDateString()
        'dtpFechaFinal.Value = Now.Date().ToShortDateString()

        dtpFechaBase.Value = Now.Date().ToShortDateString()
        dtpFechaDoc.Value = Now.Date().ToShortDateString()
        dtpFechaVen.Value = Now.Date().ToShortDateString()
        cboFechaCreacion.Value = Now.Date().ToShortDateString()
        cboFechaRecepcion.Value = Now.Date().ToShortDateString()

        cboTipoAcreedor.DataSource = eRegistroDocumento.fListarAcreedor2(gEmpresa)
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboTipoAcreedor.ValueMember = "TipoAnaliticoCodigo"
            cboTipoAcreedor.DisplayMember = "Descrip"
            cboTipoAcreedor.SelectedIndex = -1
        End If


        'cboTipoAnexo.DataSource = eRegistroDocumento.fListarAcreedor(gEmpresa)
        'If eRegistroDocumento.iNroRegistros > 0 Then
        '    cboTipoAnexo.ValueMember = "TipoAcreedorId"
        '    cboTipoAnexo.DisplayMember = "TipoAnaliticoDescripcion"
        '    cboTipoAnexo.SelectedIndex = -1
        'End If


        cboDocumento.DataSource = eRegistroDocumento.fListarTipDoc()

        If eRegistroDocumento.iNroRegistros > 0 Then
            cboDocumento.ValueMember = "IdDocumento"
            cboDocumento.DisplayMember = "DescripDoc"
            cboDocumento.SelectedIndex = -1
        End If


        cboTipoDocIdent.DataSource = eRegistroDocumento.fListarTipDocIdentidad()
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboTipoDocIdent.ValueMember = "DIdeCodigo"
            cboTipoDocIdent.DisplayMember = "DIdeDescripcion"
            cboTipoDocIdent.SelectedIndex = -1
        End If


        cboMoneda.DataSource = eRegistroDocumento.fListarTipMoneda()
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = -1
        End If


        'cboMonedaAcree.DataSource = eRegistroDocumento.fListarTipMoneda()
        'cboMonedaAcree.ValueMember = "MonCodigo"
        'cboMonedaAcree.DisplayMember = "MonDescripcion"
        'cboMonedaAcree.SelectedIndex = -1

        cboDepartamentos.DataSource = eRegistroDocumento.fListarDepartamentos(gEmpresa)
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboDepartamentos.ValueMember = "AreaCodigo"
            cboDepartamentos.DisplayMember = "AreaNombre"
            cboDepartamentos.SelectedIndex = -1
        End If


        'cboAreas.DataSource = eRegistroDocumento.fListarDepartamentos(gEmpresa)
        'If eRegistroDocumento.iNroRegistros > 0 Then
        '    cboAreas.ValueMember = "AreaCodigo"
        '    cboAreas.DisplayMember = "AreaNombre"
        '    cboAreas.SelectedIndex = -1
        'End If


        'cboTipoMoneda.DataSource = eRegistroDocumento.fListarTipMoneda()
        'If eRegistroDocumento.iNroRegistros > 0 Then
        '    cboTipoMoneda.ValueMember = "MonCodigo"
        '    cboTipoMoneda.DisplayMember = "MonDescripcion"
        '    cboTipoMoneda.SelectedIndex = -1
        'End If


        'cboTipoDocumento.DataSource = eRegistroDocumento.fListarTipDoc()
        'If eRegistroDocumento.iNroRegistros > 0 Then
        '    cboTipoDocumento.ValueMember = "IdDocumento"
        '    cboTipoDocumento.DisplayMember = "DescripDoc"
        '    cboTipoDocumento.SelectedIndex = -1
        'End If


        cboSituacion.DataSource = eRegistroDocumento.fListarSituacion()
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboSituacion.ValueMember = "SituacionId"
            cboSituacion.DisplayMember = "Descripcion"
            cboSituacion.SelectedIndex = -1
        End If


        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        dgvDetalle.Width = 1247
        dgvDetalle.Height = 420

        dgvDetalle.Focus()

        dMontosDetTemp(0) = 0
        dCantDetTemp(0) = 0

        Timer9.Enabled = True
        chkTodosUsuarios.Checked = True

       
        If (EstadoServicioruc = True) Then
            btnValidarRuc.Visible = True
            btnBuscar.Visible = False
        Else
            btnValidarRuc.Visible = False
            btnBuscar.Visible = True
        End If

        dgvDetalle.ReadOnly = False

    End Sub


    Private Sub mMostrarGrilla()

        eRegistroDocumento = New clsRegistroDocumento
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable

        If Trim(glbUsuCategoria) = "A" Then
            dtTableLista = eRegistroDocumento.fListar(0, gEmpresa, Trim(gUsuario), gPeriodo, "")
            dgvDetalle.Columns("VISTO").Visible = True
        ElseIf Trim(glbUsuCategoria) = "U" Then
            dtTableLista = eRegistroDocumento.fListar(50, gEmpresa, Trim(gUsuario), gPeriodo, "")
            dgvDetalle.Columns("VISTO").Visible = False
        End If

        'dtTableLista = eRegistroDocumento.fListar(gEmpresa)
        dgvDetalle.AutoGenerateColumns = False
        dgvDetalle.DataSource = dtTableLista
        cmr = BindingContext(dgvDetalle.DataSource)

        stsTotales.Items(0).Text = "Total de Registros= " & eRegistroDocumento.iNroRegistros

        If eRegistroDocumento.iNroRegistros > 0 Then
            frmPrincipal.Modifica.Enabled = True
            frmPrincipal.Elimina.Enabled = True
            frmPrincipal.Visualiza.Enabled = True
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If

    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                sCodRegistro = Trim(Fila("IdRegistro").ToString)
                cboTipoAcreedor.SelectedValue = Fila("TipoAcreedorId").ToString
                txtCod.Text = Fila("CodProveedor").ToString
                txtCodigo.Text = Fila("RUC").ToString
                lblDescripcionCodigo.Text = Fila("NombreProveedor").ToString
                cboDocumento.SelectedValue = Fila("IdDocumento").ToString
                txtSerie.Text = Fila("Serie").ToString()
                txtNroDocumento.Text = Fila("NroDocumento").ToString()
                dtpFechaBase.Value = Fila("FechaBase").ToString()
                txtDias.Text = Fila("Dias").ToString()
                dtpFechaDoc.Value = Fila("FechaDocumento").ToString()
                dtpFechaVen.Value = Fila("FechaVencimiento").ToString()
                cboMoneda.SelectedValue = Fila("IdMoneda").ToString()

                sMonedaDoc = Fila("IdMoneda").ToString()
                sImporteTotal = Fila("ImporteTotal").ToString()

                ImporteDoc = Trim(Fila("ImporteTotal").ToString)
                txtImporteIGV.Text = Fila("ImporteIgv").ToString()
                txtTasaIGV.Text = Fila("TasaIgv").ToString()

                'txtValorDeclarado.Text = Fila("ValorDeclarado").ToString()
                'txtValorCalculado.Text = Fila("ValorCalculado").ToString()
                txtInafecto.Text = Fila("Inafecto").ToString()
                cboDepartamentos.SelectedValue = Fila("IdArea").ToString()
                txtDescripcion.Text = Fila("Descripcion").ToString()
                'txtSituacion.Text = Fila("Situacion").ToString()
                cboSituacion.SelectedValue = Fila("SituacionId").ToString()
                cboFechaCreacion.Value = Fila("FechaCreacion").ToString()
                cboFechaRecepcion.Value = Fila("FechaRecepcion").ToString()
                txtUsuario.Text = Fila("Usuario").ToString()
                cboEstado.SelectedValue = Fila("EstadoPrePro").ToString()
                txtValorCompra.Text = Fila("ValorCompra").ToString()
                'MessageBox.Show(Trim(Fila("IGV").ToString()))
                If Trim(Fila("IGV").ToString()) = "0" Or Trim(Fila("IGV").ToString()) = "" Then
                    chkIGV.Checked = False
                ElseIf Trim(Fila("IGV").ToString()) = "1" Then
                    chkIGV.Checked = True
                End If

                If Trim(Fila("DP").ToString()) = "P" Then
                    cboDP.SelectedIndex = 1
                ElseIf Trim(Fila("DP").ToString()) = "D" Then
                    cboDP.SelectedIndex = 0
                ElseIf Trim(Fila("DP").ToString()) = "" Then
                    cboDP.SelectedIndex = -1
                End If

                'If Trim(Fila("Archivado").ToString()) = "0" Then
                '    chkArchivado.Checked = False
                'ElseIf Trim(Fila("Archivado").ToString()) = "1" Then
                '    chkArchivado.Checked = True
                'End If

                'If Trim(Fila("C").ToString()) = "0" Then
                '    chkCancelado.Checked = False
                'ElseIf Trim(Fila("C").ToString()) = "1" Then
                '    chkCancelado.Checked = True
                'End If

                txtDP.Text = Fila("MtoDP").ToString()

                txtCodOrden.Text = Fila("IdOC").ToString()
                txtCodOrden2.Text = Fila("IdOC").ToString()
                txtNroOrden.Text = Fila("NroOC").ToString()

                txtImporteTotal.Text = Fila("ImporteTotal").ToString()

                ' Cargar nombre de la caja origen
                txtCajaOrigen.Text = ""
                txtCajaOrigen.Visible = False
                CargarOrigenCaja()

            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub CargarOrigenCaja()
        Try
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrigenCaja As New DataTable

            dtOrigenCaja = eRegistroDocumento.fListarOrigenCaja(60, gEmpresa, Convert.ToString(dgvDetalle.CurrentRow.Cells("IdRegistro").Value))

            If dtOrigenCaja.Rows.Count > 0 Then
                txtCajaOrigen.Text = dtOrigenCaja.Rows(0).Item("NOMBRECAJA").ToString()
                txtCajaOrigen.Visible = IIf(txtCajaOrigen.Text = "", False, True)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Timer1.Enabled = False
        If Timer1.Enabled = True Then
            txtCodigo.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer1.Enabled = False
    End Sub

    'Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    '    Me.cboTipoAcreedor.Focus()
    '    Timer1.Enabled = False
    'End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If Timer2.Enabled = True Then
            'If Len(Trim(txtNroOrden.Text)) > 0 Then
            'dgvOrdenes.Focus()
            'ElseIf Len(Trim(txtNroOrden.Text)) = 0 Then
            cboMoneda.Focus()
            'End If

            'BeButton4_Click(sender, e)
        End If
        Timer2.Enabled = False
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        If cboTipoAcreedor.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboTipoAcreedor.Focus()
        Else
            rdbRuc.Checked = True
            Panel2.Visible = True
            txtFiltrar.Clear()
            txtFiltrar.Focus()
            txtFiltrar_TextChanged_1(sender, e)
            'rdbRuc.Checked = True
        End If
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        Panel2.Visible = False
        txtCodigo.Focus()
    End Sub

    Private Sub cboTipoAcreedor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoAcreedor.SelectedIndexChanged
        'If cboTipoAcreedor.SelectedIndex <> -1 Then
        '    btnBuscar.Enabled = True
        'End If
        'txtRucAcree.Clear()
        If gUsuario <> "JNOVOA" Then
            txtCod.Clear()
            txtCodigo.Clear()
            lblDescripcionCodigo.Text = ""
            If dvgListProveedores.Rows.Count > 0 Then
                For x As Integer = 0 To dvgListProveedores.Rows.Count - 1
                    dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
                Next
            End If
        End If

        'For x As Integer = 0 To dvgListProveedores.Rows.Count - 1
        '    dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
        'Next


        'txtCodigo.Focus()
    End Sub

    'Private Sub txtFiltrar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If (Len(txtFiltrar.Text) > 0 And rdbDescr.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
    '        Dim CodAcreedor As String = ""
    '        Dim sDescripcion As String = ""
    '        eTempo = New clsPlantTempo
    '        dtTable = New DataTable
    '        CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
    '        sDescripcion = Trim(txtFiltrar.Text)
    '        dtTable = eRegistroDocumento.fListarProveedores4(gEmpresa, sDescripcion, CodAcreedor)
    '        dgvDetalle.AutoGenerateColumns = False
    '        dvgListProveedores.DataSource = dtTable
    '    ElseIf (Len(txtFiltrar.Text) > 0 And rdbCodigo.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
    '        Dim CodAcreedor As String = ""
    '        Dim Codigo As String = ""
    '        eTempo = New clsPlantTempo
    '        dtTable = New DataTable
    '        CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
    '        Codigo = Trim(txtFiltrar.Text)
    '        dtTable = eRegistroDocumento.fListarProveedores3(gEmpresa, Codigo, CodAcreedor)
    '        dgvDetalle.AutoGenerateColumns = False
    '        dvgListProveedores.DataSource = dtTable
    '    ElseIf (Len(txtFiltrar.Text) > 0 And rdbNumDoc.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
    '        Dim CodAcreedor As String = ""
    '        Dim NroIdent As String = ""
    '        eTempo = New clsPlantTempo
    '        dtTable = New DataTable
    '        CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
    '        NroIdent = Trim(txtFiltrar.Text)
    '        dtTable = eRegistroDocumento.fListarProveedores2(gEmpresa, NroIdent, CodAcreedor)
    '        dgvDetalle.AutoGenerateColumns = False
    '        dvgListProveedores.DataSource = dtTable
    '    ElseIf (Len(txtFiltrar.Text) > 0 And rdbRuc.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
    '        Dim CodAcreedor As String = ""
    '        Dim Ruc As String = ""
    '        eTempo = New clsPlantTempo
    '        dtTable = New DataTable
    '        CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
    '        Ruc = Trim(txtFiltrar.Text)
    '        dtTable = eRegistroDocumento.fListarProveedores(gEmpresa, Ruc, CodAcreedor)
    '        dgvDetalle.AutoGenerateColumns = False
    '        dvgListProveedores.DataSource = dtTable
    '    End If
    'End Sub

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click
        If Trim(txtFiltrar.Text) <> "" Then
            txtCodAcreedor.Text = Trim(txtFiltrar.Text)
            txtRucAcree.Text = Trim(txtFiltrar.Text)
            txtNroDocIdent.Text = Trim(txtFiltrar.Text)
            Panel2.Visible = False
            Panel3.Visible = True
            txtDescripcionAcreedor.Focus()
        Else
            Panel2.Visible = False
            Panel3.Visible = True
            txtCodAcreedor.Focus()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Panel2.Visible = True
        Panel3.Visible = False
        LimpiarControles2()
        If dvgListProveedores.Rows.Count > 0 Then
            For x As Integer = 0 To dvgListProveedores.Rows.Count - 1
                dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
            Next
        End If
        rdbRuc.Checked = True
        txtFiltrar.Clear()
        txtFiltrar.Focus()

        LimpiarControles2()

        Panel2.Visible = False
        Panel3.Visible = False
        txtCodigo.Clear()
        txtCodigo.Focus()


    End Sub

    Private Sub LimpiarControles2()
        txtCodAcreedor.Clear()
        txtDescripcionAcreedor.Clear()
        txtDireccionAcree.Clear()
        txtRucAcree.Clear()
        'cboMonedaAcree.SelectedIndex = -1
        rdbJuridica.Checked = False
        rdbNatural.Checked = False
        cboTipoDocIdent.SelectedIndex = -1
        txtNroDocIdent.Clear()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim iResultado As Int16 = 0
        eTempo = New clsPlantTempo
        eRegistroDocumento = New clsRegistroDocumento
        '        If (objResultado.dtResultado.Rows(0)("Estado").ToString() = "ACTIVO" And objResultado.dtResultado.Rows(0)("Condicion").ToString() = "HABIDO") Then
        If True Then
            If cboTipoAcreedor.SelectedIndex <> -1 And Len(txtCodAcreedor.Text) > 0 And Len(txtDescripcionAcreedor.Text) > 0 And Len(txtDireccionAcree.Text) > 0 And Len(txtRucAcree.Text) > 0 Then
                Dim sCodigo As String = ""
                Dim iRes As Integer = 0
                Dim iRes2 As Integer = 0
                eTempo = New clsPlantTempo
                sCodigo = Trim(txtCodAcreedor.Text)

                iRes = eRegistroDocumento.fBuscarDoble(20, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), sCodigo)
                iRes2 = eRegistroDocumento.fBuscarDoble2(24, gEmpresa, Trim(txtRucAcree.Text), Trim(cboTipoAcreedor.SelectedValue.ToString))

                Dim dtTable2 As DataTable
                dtTable2 = New DataTable
                eRegistroDocumento = New clsRegistroDocumento
                dtTable2 = eRegistroDocumento.fListarProveedoresxRazonSocial(gEmpresa, "", Trim(cboTipoAcreedor.SelectedValue), Trim(vb.UCase(Trim(txtDescripcionAcreedor.Text))))
                If dtTable2.Rows.Count > 0 Then
                    MessageBox.Show("Ya Existe el Proveedor " & Trim(vb.UCase(Trim(txtDescripcionAcreedor.Text))) & " con Ruc " & Trim(dtTable2.Rows(0).Item("Ruc")) & ", Cambie de Raz�n Social para el actual Proveedor.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value = ""
                    Exit Sub
                End If

                If iRes >= 1 Then
                    MessageBox.Show("El C�digo Nro: " & sCodigo & " ya Existe para este Anexo!, Ingrese Otro C�digo.", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtCodAcreedor.Text = ""
                    txtCodAcreedor.Focus()
                ElseIf iRes2 >= 1 Then
                    MessageBox.Show("El Nro de RUC: " & Trim(txtRucAcree.Text) & " ya esta Registrado para este Anexo, Ingrese Otro Numero.", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtRucAcree.Text = ""
                    txtRucAcree.Focus()
                ElseIf iRes = 0 And iRes2 = 0 Then
                    If (MessageBox.Show("�Esta seguro de Grabar ahora el Proveedor?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                        Dim sTipoPersona As String = ""
                        Dim sCodMoneda As String = ""
                        Dim sCodTipoDocIndentidad As String = ""
                        If rdbJuridica.Checked = True Then
                            sTipoPersona = "J"
                        ElseIf rdbNatural.Checked = True Then
                            sTipoPersona = "N"
                        Else
                            sTipoPersona = ""
                        End If
                        'If cboMonedaAcree.SelectedIndex = -1 Then
                        ' sCodMoneda = ""
                        'Else
                        'sCodMoneda = cboMonedaAcree.SelectedValue
                        'End If
                        If cboTipoDocIdent.SelectedIndex = -1 Then
                            sCodTipoDocIndentidad = ""
                        Else
                            sCodTipoDocIndentidad = cboTipoDocIdent.SelectedValue
                        End If
                        iResultado = eRegistroDocumento.GrabarAcreedores(1, gEmpresa, cboTipoAcreedor.SelectedValue, Trim(txtCodAcreedor.Text), Trim(txtDescripcionAcreedor.Text), Trim(txtDireccionAcree.Text), Trim(txtRucAcree.Text), "", sTipoPersona, sCodTipoDocIndentidad, Trim(txtNroDocIdent.Text))
                        If iResultado = 1 Then
                            txtCodigo.Text = Trim(txtRucAcree.Text)
                            LimpiarControles2()
                            Panel3.Visible = False
                            If dvgListProveedores.Rows.Count > 0 Then
                                For x As Integer = 0 To dvgListProveedores.Rows.Count - 1
                                    dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
                                Next
                            End If
                            'For x As Integer = 0 To dvgListProveedores.Rows.Count - 1
                            '    dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
                            'Next
                            Panel2.Visible = True
                            txtFiltrar.Clear()
                            rdbRuc.Checked = True
                            txtFiltrar.Focus()
                            Panel2.Visible = False
                            Panel3.Visible = False
                            txtCodigo.Focus()
                        End If
                    Else

                        LimpiarControles2()

                        Panel2.Visible = False
                        Panel3.Visible = False
                        txtCodigo.Clear()
                        txtCodigo.Focus()

                    End If
                End If
            Else
                'MessageBox.Show("Faltan Datos Principales")
                If cboTipoAcreedor.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoAcreedor.Focus()
                    Exit Sub
                End If
                If Len(txtCodAcreedor.Text) = 0 Then
                    MessageBox.Show("Ingrese un Codigo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodAcreedor.Focus()
                    Exit Sub
                End If
                If Len(txtDescripcionAcreedor.Text) = 0 Then
                    MessageBox.Show("Ingrese el Campo Descripcion", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDescripcionAcreedor.Focus()
                    Exit Sub
                End If
                If Len(txtDireccionAcree.Text) = 0 Then
                    MessageBox.Show("Ingrese el Campo Direcci�n", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDireccionAcree.Focus()
                    Exit Sub
                End If
                If Len(txtRucAcree.Text) = 0 Then
                    MessageBox.Show("Ingrese el Campo RUC", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtRucAcree.Focus()
                    Exit Sub
                End If
            End If
        Else
            MessageBox.Show("EL PROVEEDOR SE ENCUENTRA INACTIVO EN LA SUNAT IMPOSIBLE DE GRABAR", "Sistema Tesoreria")
            Button2_Click(sender, e)
        End If

    End Sub

    Private Sub txtCodigo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodigo.TextChanged
        ReDim dMontosDetTemp(0)
        ReDim dCantDetTemp(0)
        dMontosDetTemp(0) = 0
        dCantDetTemp(0) = 0

        txtCodOrden.Text = String.Empty
        txtNroOrden.Text = String.Empty
        Panel4.Visible = False
        cboMoneda.SelectedIndex = -1
    End Sub

      Private Sub txtImporteTotal_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtImporteTotal.LostFocus
        'Try
        Dim MontoEscrito As String = ""

        MontoEscrito = txtImporteTotal.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtImporteTotal.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")

        '    'dImporteOC =====> MONTO ORIGEN DE LA O/C
        '    'dblSaldoOC =====> MONTO SALDO DE LA O/C
        '    'VALIDAR:
        '    'CAPTURAR EL MONTO DEL SALDO,para q al digitar el monto de la Factura no sobrepase el Monto Total
        '    'EL MONTO TOTAL INGRESADO DEBE SER MENOR O IGUAL AL SALDO Y MENOR Q EL MONTO TOTAL DE LA O/C
        '    'LAS O/C SE DEBEN VISUALIZAR SIEMPRE Y CUANDO EL SALDO NO SEA 0

        '    If CDbl(txtImporteTotal.Text) > 0 Then
        '        If txtNroOrden.Text <> String.Empty And txtCodOrden.Text <> String.Empty Then
        '            If CDbl(txtImporteTotal.Text) < dImporteOC And CDbl(txtImporteTotal.Text) <= dblSaldoOC Then
        '                Dim frmDetalleOC As New FrmDetalleTempOC
        '                frmDetalleOC.FrmRegDocumentos = Me
        '                ''frmDetalleOC.Close()
        '                'If My.Application.OpenForms.Item("FrmDetalleTempOC").Focused Then
        '                '    Dim x As Integer = My.Application.OpenForms.Count
        '                '    My.Application.OpenForms.Item("FrmDetalleTempOC").Close()
        '                '    'My.Application.OpenForms.Item("frmRegistroDocumento").Close()
        '                'End If
        '                frmDetalleOC.ShowDialog()
        '            ElseIf CDbl(txtImporteTotal.Text) > dImporteOC Then
        '                MessageBox.Show("El Importe ingresado no puede ser mayor que el importe de la O/C", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                txtImporteTotal.Focus()
        '            ElseIf CDbl(txtImporteTotal.Text) > dblSaldoOC Then
        '                MessageBox.Show("El Importe ingresado no puede ser mayor al SALDO de la O/C" & vbCrLf & _
        '                                "El SALDO ACTUAL para la seleccionada Orden es: " & dblSaldoOC, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                txtImporteTotal.Focus()
        '            End If
        '        End If
        '    End If

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    txtImporteTotal.Text = 0
        'End Try
    End Sub

    Private Sub txtImporteTotal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporteTotal.TextChanged
        Try
            If bolEdit = False Then
                If (chkIGV.Checked = False And cboDocumento.SelectedValue = "02") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "05") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "08") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "10") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "11") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "13") Then
                    If Len(Trim(txtImporteTotal.Text)) > 0 Then
                        txtImporteIGV.Text = Format(0, "0.00")
                        txtValorCompra.Text = Format(Convert.ToDouble(txtImporteTotal.Text), "#,##0.00")
                        txtInafecto.Text = Format(0, "0.00")
                    End If
                ElseIf (chkIGV.Checked = True And cboDocumento.SelectedValue = "02") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "05") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "08") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "10") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "11") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "13") Then
                    If Len(Trim(txtImporteTotal.Text)) > 0 Then
                        Dim ImpBase As Double = Convert.ToDouble(txtImporteTotal.Text) / (1 + (dIgv / 100))
                        Dim ImpIGV As Double = ImpBase * 0.19
                        txtValorCompra.Text = Format(ImpBase, "#,##0.00")
                        txtImporteIGV.Text = Format(ImpIGV, "#,##0.00")
                    End If
                ElseIf (chkIGV.Checked = False And cboDocumento.SelectedValue <> "02") Or (chkIGV.Checked = False And cboDocumento.SelectedValue <> "05") Or (chkIGV.Checked = False And cboDocumento.SelectedValue <> "08") Or (chkIGV.Checked = False And cboDocumento.SelectedValue <> "10") Or (chkIGV.Checked = False And cboDocumento.SelectedValue <> "11") Or (chkIGV.Checked = False And cboDocumento.SelectedValue <> "13") Then
                    If Len(Trim(txtImporteTotal.Text)) > 0 Then
                        txtImporteIGV.Text = Format(0, "0.00")
                        txtValorCompra.Text = Format(0, "0.00")
                        txtInafecto.Text = Format(0, "0.00")
                    End If
                ElseIf (chkIGV.Checked = True And cboDocumento.SelectedValue <> "02") Or (chkIGV.Checked = True And cboDocumento.SelectedValue <> "05") Or (chkIGV.Checked = True And cboDocumento.SelectedValue <> "08") Or (chkIGV.Checked = True And cboDocumento.SelectedValue <> "10") Or (chkIGV.Checked = True And cboDocumento.SelectedValue <> "11") Or (chkIGV.Checked = True And cboDocumento.SelectedValue <> "13") Then
                    If Len(Trim(txtImporteTotal.Text)) > 0 Then
                        'dImporteIGV = Val(txtImporteTotal.Text) * (dIgv / 100)
                        txtImporteIGV.Text = Format(0, "0.00")
                        txtValorCompra.Text = Format(0, "0.00")
                        txtInafecto.Text = Format(0, "0.00")
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub



    Private Sub txtDias_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDias.TextChanged
        If Len(Trim(txtDias.Text)) > 0 And Trim(txtDias.Text) <> "0" Then
            dtpFechaVen.Value = dtpFechaBase.Value.AddDays(Val(Trim(txtDias.Text)))
        Else
            dtpFechaVen.Value = dtpFechaBase.Value
        End If
    End Sub

    Private Sub txtCodigo_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        btnBuscar_Click(sender, e)
    End Sub

    Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Select Case Asc(e.KeyChar)
            Case 13
                If (Len(txtCodigo.Text) = 11 And cboTipoAcreedor.SelectedIndex <> -1) Then
                    Dim Codigo As String = ""
                    Dim CodAcreedor As String = ""
                    Dim CodRuc As String = ""
                    dtTable = New DataTable
                    CodRuc = Trim(txtCodigo.Text)
                    CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
                    dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)

                    If dtTable.Rows.Count > 0 Then
                        If Trim(dtTable.Rows(0).Item("Estado")) = "1" Then
                            MessageBox.Show("Este Anexo esta Deshabilitado en el Sistema", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        End If
                    End If

                    If dtTable.Rows.Count > 0 Then
                        cboDocumento.Focus()
                        txtCod.Text = Trim(dtTable.Rows(0).Item("AnaliticoCodigo"))
                        lblDescripcionCodigo.Text = Trim(dtTable.Rows(0).Item("AnaliticoDescripcion"))
                        txtCodigo.Text = Trim(dtTable.Rows(0).Item("Ruc"))
                        'cboDocumento.Focus()
                    Else
                        MessageBox.Show("El numero de RUC " & Trim(txtCodigo.Text) & " no esta Registrado en este tipo de Acreedor. Puede Registrarlo.", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Panel3.Visible = True
                        txtCodAcreedor.Text = Trim(txtCodigo.Text)
                        txtRucAcree.Text = Trim(txtCodigo.Text)
                        'txtNroDocIdent.Text = Trim(txtCodigo.Text)
                        txtCod.Text = ""
                        txtCodigo.Text = ""
                        lblDescripcionCodigo.Text = ""
                        txtCodAcreedor.Focus()
                    End If
                Else
                    If cboTipoAcreedor.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un Tipo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboTipoAcreedor.Focus()
                        Exit Sub
                    End If
                    If Len(txtCodigo.Text) = 0 Then
                        MessageBox.Show("Ingrese un numero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtCodigo.Focus()
                        Exit Sub
                    End If
                    If Len(txtCodigo.Text) < 11 Then
                        MessageBox.Show("Ingrese un Numero de Ruc Completo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtCodigo.Focus()
                        Exit Sub
                    End If
                    If Len(txtCodigo.Text) = 0 And cboTipoAcreedor.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un Tipo de Acreedor y Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboTipoAcreedor.Focus()
                        Exit Sub
                    End If
                End If
        End Select
    End Sub

    'Private Sub dvgListProveedores_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dvgListProveedores.KeyDown
    '    If e.KeyCode = Keys.Enter Then
    '        e.SuppressKeyPress = True
    '    End If
    'End Sub

    Private Sub dvgListProveedores_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvgListProveedores.DoubleClick
        If dvgListProveedores.Rows.Count > 0 Then
            Try
                CodProveedor = dvgListProveedores.Rows(dvgListProveedores.CurrentRow.Index).Cells(1).Value
                NombreProveedor = dvgListProveedores.Rows(dvgListProveedores.CurrentRow.Index).Cells(2).Value
                strRuc = dvgListProveedores.Rows(dvgListProveedores.CurrentRow.Index).Cells(3).Value
                txtCod.Text = Trim(CodProveedor)
                lblDescripcionCodigo.Text = Trim(NombreProveedor)
                txtCodigo.Text = Trim(strRuc)
                Panel2.Visible = False
                cboDocumento.Focus()
            Catch ex As Exception
                Exit Sub
            End Try
        End If

    End Sub

    Private m_EditingRow As Integer = -1

    Private Sub dvgListProveedores_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dvgListProveedores.EditingControlShowing
        m_EditingRow = dvgListProveedores.CurrentRow.Index
    End Sub

    Private Sub dvgListProveedores_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dvgListProveedores.SelectionChanged
        If m_EditingRow >= 0 Then
            Dim new_row As Integer = m_EditingRow
            m_EditingRow = -1
            dvgListProveedores.CurrentCell = dvgListProveedores.Rows(new_row).Cells(dvgListProveedores.CurrentCell.ColumnIndex)
        End If
    End Sub

    Private Sub dvgListProveedores_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dvgListProveedores.KeyDown
        If dvgListProveedores.Rows.Count > 0 Then
            If e.KeyCode = Keys.Return Then
                Dim cur_cell As DataGridViewCell = dvgListProveedores.CurrentCell
                Dim col As Integer = cur_cell.ColumnIndex
                col = (col + 1) Mod dvgListProveedores.Columns.Count
                cur_cell = dvgListProveedores.CurrentRow.Cells(col)
                dvgListProveedores.CurrentCell = cur_cell
                e.Handled = True
            End If
        Else
            txtFiltrar.Focus()
        End If
    End Sub

    Private Sub dvgListProveedores_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dvgListProveedores.KeyPress
        If dvgListProveedores.Rows.Count > 0 Then
            Select Case Asc(e.KeyChar)
                Case 13
                    Try
                        CodProveedor = dvgListProveedores.Rows(dvgListProveedores.CurrentRow.Index).Cells(1).Value
                        NombreProveedor = dvgListProveedores.Rows(dvgListProveedores.CurrentRow.Index).Cells(2).Value
                        strRuc = dvgListProveedores.Rows(dvgListProveedores.CurrentRow.Index).Cells(3).Value
                        txtCod.Text = Trim(CodProveedor)
                        lblDescripcionCodigo.Text = Trim(NombreProveedor)
                        txtCodigo.Text = Trim(strRuc)
                        Panel2.Visible = False
                        cboDocumento.Focus()
                    Catch ex As Exception
                        Exit Sub
                    End Try
            End Select
        End If
    End Sub

    Private Sub rdbNatural_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbNatural.CheckedChanged
        If rdbNatural.Checked = True Then
            cboTipoDocIdent.Enabled = True
            txtNroDocIdent.Enabled = True
        Else
            cboTipoDocIdent.SelectedIndex = -1
            cboTipoDocIdent.Enabled = False
            txtNroDocIdent.Clear()
            txtNroDocIdent.Enabled = False
        End If
    End Sub

    Private Sub BeLabel35_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel35.Click
        Panel5.Visible = False
        Timer9.Enabled = True
    End Sub


    Private Sub cboDocumento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDocumento.SelectedIndexChanged
        If cboDocumento.Text = "FACTURA" Or cboDocumento.Text = "NOTA DE CREDITO" Or cboDocumento.Text = "TICKET" Or cboDocumento.Text = "BOL VIAJE TERRESTRE" Or cboDocumento.Text = "BOL VIAJE AEREO" Or cboDocumento.Text = "REC. SERV.PUBLICOS" Then

            'chkIGV.Checked = False
            chkIGV.Enabled = True
            Dim dtTable As DataTable
            dtTable = New DataTable
            ePagoProveedores = New clsPagoProveedores
            eTempo = New clsPlantTempo
            dtTable = ePagoProveedores.fListarParametroIGV(dtpFechaBase.Value)
            If dtTable.Rows.Count > 0 Then
                dIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
                txtTasaIGV.Text = Format(dIgv, "0.00")
            Else
                dIgv = 0.0
                txtTasaIGV.Text = Format(dIgv, "0.00")
            End If

            If Len(txtImporteTotal.Text.Trim) > 0 Then
                txtImporteTotal_TextChanged(sender, e)
            End If

            If cboDocumento.Text = "FACTURA" Then
                btnVerValorizaciones.Visible = True
                btnVerFacturas.Visible = False
                btnVerValorizaciones.Enabled = True
                btnVerFacturas.Enabled = False
                Panel7.Visible = False
                Panel8.Visible = False
            ElseIf cboDocumento.Text = "NOTA DE CREDITO" Then
                btnVerValorizaciones.Visible = False
                btnVerFacturas.Visible = True
                btnVerValorizaciones.Enabled = False
                btnVerFacturas.Enabled = True
                Panel7.Visible = False
                Panel8.Visible = False
            End If

        Else
            chkIGV.Checked = False
            chkIGV.Enabled = False

            dIgv = 0.0
            txtTasaIGV.Text = Format(dIgv, "0.00")

            If Len(txtImporteTotal.Text.Trim) > 0 Then
                txtImporteTotal_TextChanged(sender, e)
            Else
                txtImporteIGV.Text = Format(dIgv, "0.00")
                txtTasaIGV.Text = Format(dIgv, "0.00")
                txtValorCompra.Text = Format(dIgv, "0.00")
                txtInafecto.Text = Format(dIgv, "0.00")
            End If

            btnVerValorizaciones.Visible = False
            btnVerFacturas.Visible = False
            btnVerValorizaciones.Enabled = False
            btnVerFacturas.Enabled = False
        End If
    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        If cboDepartamentos.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Area", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboDepartamentos.Focus()
        ElseIf cboDepartamentos.SelectedIndex > -1 Then
            cCapadatos = New clsCapaDatos
            Dim DRlector As SqlDataReader
            Dim fichero As String = ""
            OpenFileDialog1.InitialDirectory = "C:\"
            If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                fichero = OpenFileDialog1.FileName
            End If

            Dim cs As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fichero & ";Extended Properties=""Excel 8.0;HDR=YES"""
            Try
                Dim cn As New OleDbConnection(cs)
                If Not System.IO.File.Exists(fichero) Then
                    MsgBox("No se encontr� el Libro: " & fichero, MsgBoxStyle.Critical, "Ruta inv�lida")
                    Exit Sub
                End If

                Dim dAdapter As New OleDbDataAdapter("Select * From [" & "Hoja1" & "$]", cs)
                Dim datos As New DataTable
                dAdapter.Fill(datos)

                Dim sCodigoGeneradoReg As String = ""
                Dim TipoAnexo As String = ""
                Dim CodAnexo As String = ""
                Dim NombreAnexo As String = ""
                Dim NroDoc As String = ""
                Dim TipoDoc As String = ""
                Dim TipoDo As String = ""
                Dim FechaRecepcion As DateTime
                Dim Fecha As DateTime
                Dim FechaV As DateTime
                Dim Importe As Double = 0
                Dim conIVG As String = ""
                Dim DP As String = ""
                Dim PorcDP As Double = 0
                Dim ValorCompra As Double = 0
                Dim IGV As Integer
                Dim CodArea As String = ""
                Dim DescripDoc As String = ""
                Dim Moneda As String = ""
                Dim MonedaDoc As String = ""
                Dim iResGrabaProv As Int32 = 0
                Dim iResGrabaDoc As Int32 = 0

                If datos.Rows.Count > 0 Then
                    For i As Integer = 0 To datos.Rows.Count - 1
                        eRegistroDocumento = New clsRegistroDocumento

                        'IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(0)), "", datos.Rows(i).Item(0))
                        FechaRecepcion = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(0)), Today(), datos.Rows(i).Item(0)) 'datos.Rows(i).Item(0)
                        Fecha = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(1)), Today(), datos.Rows(i).Item(1)) 'datos.Rows(i).Item(0)
                        'FechaVcto = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(1)), Today(), datos.Rows(i).Item(1)) 'datos.Rows(i).Item(0)
                        FechaV = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(2)), Today(), datos.Rows(i).Item(2))

                        TipoAnexo = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(3)), "", datos.Rows(i).Item(3)) 'datos.Rows(i).Item(1)
                        CodAnexo = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(4)), "", datos.Rows(i).Item(4)) 'datos.Rows(i).Item(3)
                        NombreAnexo = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(5)), "", datos.Rows(i).Item(5)) 'datos.Rows(i).Item(4)
                        TipoDo = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(6)), "", datos.Rows(i).Item(6)) 'datos.Rows(i).Item(5)
                        NroDoc = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(7)), "", datos.Rows(i).Item(7)) 'datos.Rows(i).Item(6)

                        'FechaV = Fecha.AddDays(15) 'dtpFechaBase.Value.AddDays(Val(Trim(txtDias.Text)))

                        Moneda = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(8)), "", datos.Rows(i).Item(8)) ' datos.Rows(i).Item(7)
                        Importe = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(9)), "", datos.Rows(i).Item(9)) 'datos.Rows(i).Item(8)

                        'conIVG = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(9)), "", datos.Rows(i).Item(9)) 'datos.Rows(i).Item(9)
                        'DP = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(10)), "", datos.Rows(i).Item(10)) 'datos.Rows(i).Item(10)
                        'PorcDP = IIf(Microsoft.VisualBasic.IsDBNull(datos.Rows(i).Item(11)), 0, datos.Rows(i).Item(11)) 'datos.Rows(i).Item(11)

                        DescripDoc = ""
                        CodArea = Trim(cboDepartamentos.SelectedValue)

                        If TipoDo = "FT" Then
                            TipoDoc = "02"
                        End If

                        If Moneda.Trim = "MN" Then
                            MonedaDoc = "01"
                        ElseIf Moneda.Trim = "US" Then
                            MonedaDoc = "02"
                        End If


                        'If TipoDo.Trim = "FT" And conIVG.trim = "S" Then
                        Dim dtTable1 As DataTable
                        dtTable1 = New DataTable
                        ePagoProveedores = New clsPagoProveedores
                        eTempo = New clsPlantTempo
                        dtTable1 = ePagoProveedores.fListarParametroIGV(Fecha)
                        'dtTable = eRegistroDocumento.fListarParametroIgv(gEmpresa)
                        If dtTable1.Rows.Count > 0 Then
                            dIgv = Val(dtTable1.Rows(0).Item("TcaVenta"))
                        Else
                            dIgv = 0.0
                        End If

                        With cCapadatos
                            .sConectarSQL()
                            '.sComandoSQL("SELECT * FROM Contabilidad.ct_analitico WHERE TipoAnaliticoCodigo='" & TipoAnexo.Trim & "' and RUC='" & CodAnexo.Trim & "'", CommandType.Text)
                            .sComandoSQL("SELECT * FROM Contabilidad.ct_analitico WHERE TipoAnaliticoCodigo='" & TipoAnexo.Trim & "' and RUC='" & CodAnexo.Trim & "' and EmprCodigo='" & gEmpresa & "'", CommandType.Text)
                            DRlector = .fCmdExecuteReader
                            If DRlector.HasRows = True Then
                                '''''''''''''Graba Doc
                                Dim iResultadoSaldo As Int32 = 0
                                Dim p As Integer = 0
                                Dim Serie As String = ""
                                Dim NroDocumento As String = ""
                                Dim CantidadCar As Integer = 0
                                Dim iDias As Integer = 0
                                Dim Tasa As Double = 0
                                Dim ImporteIgv As Double = 0
                                Dim Inafecto As Double = 0
                                eRegistroDocumento.fCodigo(gEmpresa)
                                sCodigoGeneradoReg = eRegistroDocumento.sCodFuturo
                                p = NroDoc.LastIndexOf("-")
                                Serie = NroDoc.Substring(0, p)
                                CantidadCar = NroDoc.Length
                                NroDocumento = Mid(NroDoc, p + 2, CantidadCar)



                                iDias = DateDiff(DateInterval.Day, Fecha, FechaV, FirstDayOfWeek.Friday, FirstWeekOfYear.System)
                                Tasa = Format(dIgv, "0.00")

                                'ImporteIgv = Format(Importe * (dIgv / 100), "0.00")
                                'Inafecto = 0 'Format(Importe - ImporteIgv, "0.00")
                                'Importe = 400
                                ImporteIgv = Format((Importe / (1 + (dIgv / 100))) * (dIgv / 100), "#,##0.00")

                                Inafecto = Format(Importe / (1 + (dIgv / 100)), "#,##0.00")
                                ValorCompra = Format(Importe - ImporteIgv, "0.00")
                                IGV = 1
                                'ElseIf TipoDo.Trim = "FT" And conIVG.Trim = "N" Then
                                '    Dim dtTable As DataTable
                                '    dtTable = New DataTable

                                '    ePagoProveedores = New clsPagoProveedores
                                '    eTempo = New clsPlantTempo
                                '    dtTable = ePagoProveedores.fListarParametroTipoCambio(gEmpresa)

                                '    'dtTable = eRegistroDocumento.fListarParametroIgv(gEmpresa)
                                '    If dtTable.Rows.Count > 0 Then
                                '        dIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
                                '    Else
                                '        dIgv = 0.0
                                '    End If
                                '    iDias = DateDiff(DateInterval.Day, Fecha, FechaV, FirstDayOfWeek.Friday, FirstWeekOfYear.System)
                                '    Tasa = Format(dIgv, "0.00")
                                '    ImporteIgv = 0 'Format(Importe * (dIgv / 100), "0.00")
                                '    'Inafecto = Importe 'Format(Importe - ImporteIgv, "0.00")
                                '    Inafecto = Format(Importe / (1 + (dIgv / 100)), "#,##0.00")
                                '    'Inafecto = 0 'Format(Importe - ImporteIgv, "0.00")
                                '    ValorCompra = 0 'Format(Importe - ImporteIgv, "0.00")
                                '    IGV = 0
                                'End If

                                dtTable = New DataTable
                                dtTable = eRegistroDocumento.VerificarDocExiste(TipoDoc.Trim, Serie.Trim, NroDocumento.Trim, CodAnexo.Trim)
                                If dtTable.Rows.Count > 0 Then
                                    'ya existe el doc
                                Else
                                    'como no existe grabo el doc y actualizao saldo del proveedor
                                    iResGrabaDoc = eRegistroDocumento.fGrabar2(1, TipoAnexo.Trim, CodAnexo.Trim, NombreAnexo.Trim, TipoDoc, Serie, NroDocumento, Fecha, iDias, Fecha, FechaV, MonedaDoc, ImporteIgv, Tasa, Importe, 0, 0, Inafecto, CodArea, DescripDoc, "00001", Fecha, FechaRecepcion, gUsuario, sCodigoGeneradoReg, gEmpresa, 0, 0, ValorCompra, IGV, DP, PorcDP)
                                    Dim iResultado1 As Integer = 0
                                    ePagoProveedores = New clsPagoProveedores
                                    If Trim(MonedaDoc) = "01" Then
                                        Dim dtTable As DataTable
                                        dtTable = New DataTable
                                        dtTable = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, TipoAnexo.Trim, CodAnexo.Trim, "01")
                                        If dtTable.Rows.Count = 0 Then
                                            'crear saldo proveedor en soles
                                            Dim IdSaldoProv As String = ""
                                            ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                            IdSaldoProv = ePagoProveedores.sCodFuturo
                                            iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, Importe, "01")
                                        ElseIf dtTable.Rows.Count = 1 Then
                                            'actualizar saldo prove en moneda soles
                                            Dim IdSaldoProv As String = ""
                                            Dim MontoSaldoSolesProv As Double = 0
                                            Dim MontoSaldoSolesNew As Double = 0
                                            IdSaldoProv = dtTable.Rows(0).Item("IdSaldo")
                                            MontoSaldoSolesProv = dtTable.Rows(0).Item("MontoSaldo")
                                            MontoSaldoSolesNew = MontoSaldoSolesProv + Importe
                                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, MontoSaldoSolesNew, "01")
                                        End If
                                    ElseIf Trim(MonedaDoc) = "02" Then
                                        Dim dtTable2 As DataTable
                                        dtTable2 = New DataTable
                                        dtTable2 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, TipoAnexo.Trim, CodAnexo.Trim, "02")
                                        If dtTable2.Rows.Count = 0 Then
                                            'crear saldo proveedor en dolares
                                            Dim IdSaldoProv As String = ""
                                            ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                            IdSaldoProv = ePagoProveedores.sCodFuturo
                                            iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, Importe, "02")
                                        ElseIf dtTable2.Rows.Count = 1 Then
                                            'actualizar saldo prove en moneda dolares
                                            Dim IdSaldoProv As String = ""
                                            Dim MontoSaldoDolaresProv As Double = 0
                                            Dim MontoSaldoDolaresNew As Double = 0
                                            IdSaldoProv = dtTable2.Rows(0).Item("IdSaldo")
                                            MontoSaldoDolaresProv = dtTable2.Rows(0).Item("MontoSaldo")
                                            MontoSaldoDolaresNew = MontoSaldoDolaresProv + Importe
                                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, MontoSaldoDolaresNew, "02")
                                        End If
                                    End If
                                    iResultadoSaldo = eRegistroDocumento.fGrabarSaldoDoc(29, TipoAnexo.Trim, CodAnexo.Trim, "", TipoDoc, Serie.Trim, NroDocumento.Trim, Fecha, 0, Fecha, FechaV, MonedaDoc, ImporteIgv, Tasa, Importe, 0, 0, Inafecto, CodArea.Trim, DescripDoc.Trim, "00001", Fecha, Fecha, gUsuario, sCodigoGeneradoReg, gEmpresa)
                                End If
                                '''''''''''''
                            Else
                                '''''''''''''Graba Proveedor
                                iResGrabaProv = eRegistroDocumento.GrabarAcreedores3(1, gEmpresa, TipoAnexo.Trim, CodAnexo.Trim, NombreAnexo.Trim, "", CodAnexo.Trim, MonedaDoc, "", "", "")
                                '''''''''''''Graba Doc
                                Dim iResultadoSaldo As Int32 = 0
                                Dim p As Integer = 0
                                Dim Serie As String = ""
                                Dim NroDocumento As String = ""
                                Dim CantidadCar As Integer = 0
                                Dim iDias As Integer = 0
                                Dim Tasa As Double = 0
                                Dim ImporteIgv As Double = 0
                                Dim Inafecto As Double = 0
                                eRegistroDocumento.fCodigo(gEmpresa)
                                sCodigoGeneradoReg = eRegistroDocumento.sCodFuturo
                                p = NroDoc.LastIndexOf("-")
                                Serie = NroDoc.Substring(0, p)
                                CantidadCar = NroDoc.Length
                                NroDocumento = Mid(NroDoc, p + 2, CantidadCar)
                                'If TipoDo.Trim = "FT" Then
                                '    Dim dtTable As DataTable
                                '    dtTable = New DataTable
                                '    dtTable = eRegistroDocumento.fListarParametroIgv(gEmpresa)
                                '    If dtTable.Rows.Count > 0 Then
                                '        dIgv = Val(dtTable.Rows(0).Item("Igv"))
                                '    Else
                                '        dIgv = 0.0
                                '    End If
                                '    iDias = DateDiff(DateInterval.Day, Fecha, FechaV, FirstDayOfWeek.Friday, FirstWeekOfYear.System)
                                '    Tasa = Format(dIgv, "0.00")
                                '    ImporteIgv = Format(Importe * (dIgv / 100), "0.00")
                                '    Inafecto = Format(Importe - ImporteIgv, "0.00")
                                'End If

                                'If TipoDo.Trim = "FT" And conIVG.Trim = "S" Then
                                'Dim dtTable1 As DataTable
                                'dtTable1 = New DataTable

                                'ePagoProveedores = New clsPagoProveedores
                                'eTempo = New clsPlantTempo
                                'dtTable1 = ePagoProveedores.fListarParametroTipoCambio(gEmpresa)

                                ''dtTable = eRegistroDocumento.fListarParametroIgv(gEmpresa)
                                'If dtTable1.Rows.Count > 0 Then
                                '    dIgv = Val(dtTable1.Rows(0).Item("TcaVenta"))
                                'Else
                                '    dIgv = 0.0
                                'End If
                                iDias = DateDiff(DateInterval.Day, Fecha, FechaV, FirstDayOfWeek.Friday, FirstWeekOfYear.System)
                                Tasa = Format(dIgv, "0.00")
                                'ImporteIgv = Format(Importe * (dIgv / 100), "0.00")
                                'Inafecto = 0 'Format(Importe - ImporteIgv, "0.00")

                                ImporteIgv = Format((Importe / (1 + (dIgv / 100))) * (dIgv / 100), "#,##0.00")
                                Inafecto = Format(Importe / (1 + (dIgv / 100)), "#,##0.00")

                                ValorCompra = Format(Importe - ImporteIgv, "0.00")
                                IGV = 1
                                'ElseIf TipoDo.Trim = "FT" And conIVG.Trim = "N" Then
                                '    Dim dtTable As DataTable
                                '    dtTable = New DataTable

                                '    ePagoProveedores = New clsPagoProveedores
                                '    eTempo = New clsPlantTempo
                                '    dtTable = ePagoProveedores.fListarParametroTipoCambio(gEmpresa)

                                '    'dtTable = eRegistroDocumento.fListarParametroIgv(gEmpresa)
                                '    If dtTable.Rows.Count > 0 Then
                                '        dIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
                                '    Else
                                '        dIgv = 0.0
                                '    End If
                                '    iDias = DateDiff(DateInterval.Day, Fecha, FechaV, FirstDayOfWeek.Friday, FirstWeekOfYear.System)
                                '    Tasa = Format(dIgv, "0.00")
                                '    ImporteIgv = 0 'Format(Importe * (dIgv / 100), "0.00")
                                '    'Inafecto = Importe 'Format(Importe - ImporteIgv, "0.00")
                                '    Inafecto = Format(Importe / (1 + (dIgv / 100)), "#,##0.00")
                                '    ValorCompra = 0 'Format(Importe - ImporteIgv, "0.00")
                                '    IGV = 0
                                'End If

                                iResGrabaDoc = eRegistroDocumento.fGrabar2(1, TipoAnexo.Trim, CodAnexo.Trim, NombreAnexo.Trim, TipoDoc, Serie, NroDocumento, Fecha, iDias, Fecha, FechaV, MonedaDoc, ImporteIgv, Tasa, Importe, 0, 0, Inafecto, CodArea, DescripDoc, "00001", Fecha, FechaRecepcion, gUsuario, sCodigoGeneradoReg, gEmpresa, 0, 0, ValorCompra, IGV, DP, PorcDP)

                                Dim iResultado1 As Integer = 0
                                ePagoProveedores = New clsPagoProveedores
                                If Trim(MonedaDoc) = "01" Then
                                    Dim dtTable As DataTable
                                    dtTable = New DataTable
                                    dtTable = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, TipoAnexo.Trim, CodAnexo.Trim, "01")
                                    If dtTable.Rows.Count = 0 Then
                                        'crear saldo proveedor en soles
                                        Dim IdSaldoProv As String = ""
                                        ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                        IdSaldoProv = ePagoProveedores.sCodFuturo
                                        iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, Importe, "01")
                                    ElseIf dtTable.Rows.Count = 1 Then
                                        'actualizar saldo prove en moneda soles
                                        Dim IdSaldoProv As String = ""
                                        Dim MontoSaldoSolesProv As Double = 0
                                        Dim MontoSaldoSolesNew As Double = 0
                                        IdSaldoProv = dtTable.Rows(0).Item("IdSaldo")
                                        MontoSaldoSolesProv = dtTable.Rows(0).Item("MontoSaldo")
                                        MontoSaldoSolesNew = MontoSaldoSolesProv + Importe
                                        iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, MontoSaldoSolesNew, "01")
                                    End If
                                ElseIf Trim(MonedaDoc) = "02" Then
                                    Dim dtTable2 As DataTable
                                    dtTable2 = New DataTable
                                    dtTable2 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, TipoAnexo.Trim, CodAnexo.Trim, "02")
                                    If dtTable2.Rows.Count = 0 Then
                                        'crear saldo proveedor en dolares
                                        Dim IdSaldoProv As String = ""
                                        ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
                                        IdSaldoProv = ePagoProveedores.sCodFuturo
                                        iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, Importe, "02")
                                    ElseIf dtTable2.Rows.Count = 1 Then
                                        'actualizar saldo prove en moneda dolares
                                        Dim IdSaldoProv As String = ""
                                        Dim MontoSaldoDolaresProv As Double = 0
                                        Dim MontoSaldoDolaresNew As Double = 0
                                        IdSaldoProv = dtTable2.Rows(0).Item("IdSaldo")
                                        MontoSaldoDolaresProv = dtTable2.Rows(0).Item("MontoSaldo")
                                        MontoSaldoDolaresNew = MontoSaldoDolaresProv + Importe
                                        iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, TipoAnexo.Trim, CodAnexo.Trim, gEmpresa, MontoSaldoDolaresNew, "02")
                                    End If
                                End If
                                iResultadoSaldo = eRegistroDocumento.fGrabarSaldoDoc(29, TipoAnexo.Trim, CodAnexo.Trim, "", TipoDoc, Serie.Trim, NroDocumento.Trim, Fecha, 0, Fecha, FechaV, MonedaDoc, ImporteIgv, Tasa, Importe, 0, 0, Inafecto, CodArea.Trim, DescripDoc.Trim, "00001", Fecha, Fecha, gUsuario, sCodigoGeneradoReg, gEmpresa)
                                '''''''''''''
                            End If

                        End With
                    Next
                    MessageBox.Show("Importacion con Exito", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'Timer2.Enabled = True
                    'frmPrincipal.sFlagGrabar = "1"
                    'sTab = 0

                    'If sTab = 1 Then
                    '    sTab = 0
                    '    TabControl1.TabPages(0).Focus()
                    '    Timer2.Enabled = True
                    'End If

                End If
            Catch oMsg As Exception
                'oMsg.number()
                MsgBox(oMsg.Message, MsgBoxStyle.Critical)

            End Try
        End If

        'Cargar(dvgListProveedores, fichero, "Hoja1")
    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub


    Sub Cargar(ByVal dgView As DataGridView, ByVal SLibro As String, ByVal sHoja As String)
        'HDR=YES : Con encabezado   
        Dim cs As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & SLibro & ";Extended Properties=""Excel 8.0;HDR=YES"""
        Try
            ' cadena de conexi�n   
            Dim cn As New OleDbConnection(cs)
            If Not System.IO.File.Exists(SLibro) Then
                MsgBox("No se encontr� el Libro: " & SLibro, MsgBoxStyle.Critical, "Ruta inv�lida")
                Exit Sub
            End If

            'se conecta con la hoja sheet 1   
            Dim dAdapter As New OleDbDataAdapter("Select * From [" & sHoja & "$]", cs)
            Dim datos As New DataSet
            ' agrega los datos   
            dAdapter.Fill(datos)

            With dvgListProveedores
                ' llena el DataGridView   
                .DataSource = datos.Tables(0)
                ' DefaultCellStyle: formato currency    
                'para los encabezados 1,2 y 3 del DataGrid   
                .Columns(1).DefaultCellStyle.Format = "c"
                .Columns(2).DefaultCellStyle.Format = "c"
                .Columns(3).DefaultCellStyle.Format = "c"
            End With
            Panel2.Visible = True

        Catch oMsg As Exception
            MsgBox(oMsg.Message, MsgBoxStyle.Critical)
        End Try
    End Sub


    Private Sub chkIGV_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIGV.CheckedChanged
        Try
            If (chkIGV.Checked = False And cboDocumento.SelectedValue = "02") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "05") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "08") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "10") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "11") Or (chkIGV.Checked = False And cboDocumento.SelectedValue = "13") Then
                If Len(Trim(txtImporteTotal.Text)) > 0 Then
                    txtImporteIGV.Text = Format(0, "0.00")
                    txtValorCompra.Text = Format(Convert.ToDouble(txtImporteTotal.Text), "#,##0.00")
                    txtInafecto.Text = Format(0, "0.00")
                End If
            ElseIf (chkIGV.Checked = True And cboDocumento.SelectedValue = "02") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "05") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "08") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "10") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "11") Or (chkIGV.Checked = True And cboDocumento.SelectedValue = "13") Then
                If Len(Trim(txtImporteTotal.Text)) > 0 Then
                    Dim ImpBase As Double = Convert.ToDouble(txtImporteTotal.Text) / (1 + (dIgv / 100))
                    Dim ImpIGV As Double = ImpBase * (dIgv / 100)
                    txtValorCompra.Text = Format(ImpBase, "#,##0.00")
                    txtImporteIGV.Text = Format(ImpIGV, "#,##0.00")
                End If
            ElseIf (chkIGV.Checked = False And cboDocumento.SelectedValue <> "02") Or (chkIGV.Checked = False And cboDocumento.SelectedValue <> "05") Then
                If Len(Trim(txtImporteTotal.Text)) > 0 Then
                    txtImporteIGV.Text = Format(0, "0.00")
                    txtValorCompra.Text = Format(0, "0.00")
                    txtInafecto.Text = Format(0, "0.00")
                End If
            ElseIf (chkIGV.Checked = True And cboDocumento.SelectedValue <> "02") Or (chkIGV.Checked = True And cboDocumento.SelectedValue <> "05") Then
                If Len(Trim(txtImporteTotal.Text)) > 0 Then
                    txtImporteIGV.Text = Format(0, "0.00")
                    txtValorCompra.Text = Format(0, "0.00")
                    txtInafecto.Text = Format(0, "0.00")
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub cboDP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDP.SelectedIndexChanged
        If cboDP.SelectedIndex = 1 Then
            txtDP.Text = "2"
        ElseIf cboDP.SelectedIndex = 0 Then
            txtDP.Text = "5"
        End If
    End Sub

    Private Sub BeTextBox1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNroOrden.DoubleClick
        If (Len(txtCodigo.Text) = 11 And cboTipoAcreedor.SelectedIndex <> -1) Then
            Panel4.Visible = True
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenes(gEmpresa, Trim(txtCodigo.Text))
            dgvOrdenes.DataSource = dtOrdenes
        Else
            If cboTipoAcreedor.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Tipo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipoAcreedor.Focus()
                Exit Sub
            End If
            If Len(txtCodigo.Text) = 0 Then
                MessageBox.Show("Ingrese un numero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCodigo.Focus()
                Exit Sub
            End If
            If Len(txtCodigo.Text) < 11 Then
                MessageBox.Show("Ingrese un Numero de Ruc Completo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCodigo.Focus()
                Exit Sub
            End If
            If Len(txtCodigo.Text) = 0 And cboTipoAcreedor.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Tipo de Acreedor y Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipoAcreedor.Focus()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub BeLabel11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel11.Click
        Panel4.Visible = False
    End Sub

    Private Sub dgvOrdenes_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.DoubleClick
        If dgvOrdenes.Rows.Count > 0 Then
            Dim CodOrden As String = ""
            Dim NroOrden As String = ""
            Dim sCodProvLog As String = ""

            If Len(Trim(txtCodigo.Text)) = 0 Then
                MessageBox.Show("Ingrese un N�mero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCodigo.Focus()
                txtNroOrden.Clear()
                Panel4.Visible = False
                Exit Sub
            End If

            Dim sCodProvRUC As String = ""
            sCodProvRUC = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(6).Value
            If Len(Trim(sCodProvRUC)) = 11 And Len(Trim(txtCodigo.Text)) = 11 Then
                If Trim(sCodProvRUC) <> Trim(txtCodigo.Text) Then
                    MessageBox.Show("Este N�mero de Orden de Compra no Pertenece al Proveedor " & Trim(lblDescripcionCodigo.Text), "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNroOrden.Focus()
                    Exit Sub
                End If
            End If

            Try
                sCodProvLog = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells("PrvCodigo").Value
                CodOrden = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells("IdOrden").Value
                NroOrden = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells("Numero").Value
                VL_PdoCodigoOC = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells("PdoCodigo").Value
                'esto modifica los montos al seleccionar una orden de compra modifica los montos de 
                'Call LLena_OrdenCompra(CodOrden, NroOrden)
                ' Call LLena_OrdenCompra(CodOrden, NroOrden)
                ' Call LLena_OrdenCompra(CodOrden, NroOrden)
                'Call LLena_OrdenCompra(CodOrden, NroOrden)

                txtCodOrden.Text = Trim(CodOrden)
                txtNroOrden.Text = Trim(NroOrden)
                txtCodProvLog.Text = Trim(sCodProvLog)
                Panel4.Visible = False
                cboDP.Focus()
            Catch ex As Exception
                Exit Sub
            End Try
        End If

    End Sub

    Private Sub txtNroOrden_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroOrden.TextChanged
        Dim dtOrdenes As DataTable

        If (Len(txtCodigo.Text) = 11 And cboTipoAcreedor.SelectedIndex <> -1) Then
            Panel4.Visible = True
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.Listar_Ordenes(gEmpresa, Trim(txtCodigo.Text), Trim(txtNroOrden.Text))
            dgvOrdenes.DataSource = dtOrdenes
            BeLabel31.Text = "N� Items : " & dtOrdenes.Rows.Count
        Else
            If Len(txtNroOrden.Text) = 0 Then
                Exit Sub
            End If

            If Len(txtCodigo.Text) = 0 Then
                MessageBox.Show("Ingrese un numero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCodigo.Focus()
                Exit Sub
            End If
            If Len(txtCodigo.Text) < 11 Then
                MessageBox.Show("Ingrese un Numero de Ruc Completo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCodigo.Focus()
                Exit Sub
            End If
            If Len(txtCodigo.Text) = 0 And cboTipoAcreedor.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Tipo de Acreedor y Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipoAcreedor.Focus()
                Exit Sub
            End If

            If cboTipoAcreedor.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Tipo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipoAcreedor.Focus()
                Exit Sub
            End If
        End If

    End Sub

    Private Sub txtCodigo_DoubleClick1(ByVal sender As Object, ByVal e As System.EventArgs)
        btnBuscar_Click(sender, e)
    End Sub

    Private Sub txtCodigo_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress

        'Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        'KeyAscii = CShort(SoloNumeros(KeyAscii))
        'If KeyAscii = 0 Then
        '    e.Handled = True
        'End If

        'Dim caracter As Char = e.KeyChar
        '' referencia a la celda   
        'Dim txt As TextBox = CType(sender, TextBox)
        '' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
        '' es el separador decimal, y que no contiene ya el separador   
        'If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Then
        '    e.Handled = False
        'ElseIf caracter = ChrW(Keys.Enter) Then
        '    'txtBeneficiario.Focus()
        '    If (Len(txtCodigo.Text) = 11 And cboTipoAcreedor.SelectedIndex <> -1) Then
        '        Dim Codigo As String = ""
        '        Dim CodAcreedor As String = ""
        '        Dim CodRuc As String = ""
        '        dtTable = New DataTable
        '        CodRuc = Trim(txtCodigo.Text)
        '        CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
        '        dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)

        '        If dtTable.Rows.Count > 0 Then
        '            If Trim(dtTable.Rows(0).Item("Estado")) = "1" Then
        '                MessageBox.Show("Este Anexo esta Deshabilitado en el Sistema", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                Exit Sub
        '            End If
        '        End If

        '        If dtTable.Rows.Count > 0 Then
        '            cboDocumento.Focus()
        '            txtCod.Text = Trim(dtTable.Rows(0).Item("AnaliticoCodigo"))
        '            lblDescripcionCodigo.Text = Trim(dtTable.Rows(0).Item("AnaliticoDescripcion"))
        '            txtCodigo.Text = Trim(dtTable.Rows(0).Item("Ruc"))
        '            'cboDocumento.Focus()
        '        Else
        '            MessageBox.Show("El numero de RUC " & Trim(txtCodigo.Text) & " no esta Registrado en este tipo de Acreedor. Puede Registrarlo.", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            Panel3.Visible = True
        '            txtCodAcreedor.Text = Trim(txtCodigo.Text)
        '            txtRucAcree.Text = Trim(txtCodigo.Text)
        '            'txtNroDocIdent.Text = Trim(txtCodigo.Text)
        '            txtCod.Text = ""
        '            txtCodigo.Text = ""
        '            lblDescripcionCodigo.Text = ""
        '            txtCodAcreedor.Focus()
        '        End If
        '    Else
        '        If cboTipoAcreedor.SelectedIndex = -1 Then
        '            MessageBox.Show("Seleccione un Tipo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            cboTipoAcreedor.Focus()
        '            Exit Sub
        '        End If
        '        If Len(txtCodigo.Text) = 0 Then
        '            MessageBox.Show("Ingrese un numero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            txtCodigo.Focus()
        '            Exit Sub
        '        End If
        '        If Len(txtCodigo.Text) < 11 Then
        '            MessageBox.Show("Ingrese un Numero de Ruc Completo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            txtCodigo.Focus()
        '            Exit Sub
        '        End If
        '        If Len(txtCodigo.Text) = 0 And cboTipoAcreedor.SelectedIndex = -1 Then
        '            MessageBox.Show("Seleccione un Tipo de Acreedor y Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            cboTipoAcreedor.Focus()
        '            Exit Sub
        '        End If
        '    End If
        'Else
        '    e.Handled = True
        'End If

        'JESUS-----
        'CODIGO PARA VALIDAR RUC 
        'Select Asc(e.KeyChar)
        '    Case 13

        '        Dim objBeanProveedor As BeanProveedor = New BeanProveedor()
        '        objBeanProveedor = VerRuc(txtCodigo.Text)
        '        If (objBeanProveedor.Estado = "ACTIVO" And objBeanProveedor.Condicion = "HABIDO") Then

        '        Else
        '            txtCodigo.Text = ""
        '            Dim OB_frmValidarRuc As frmValidarRuc = New frmValidarRuc(objBeanProveedor.Ruc, objBeanProveedor.RazonSocial, objBeanProveedor.Estado, objBeanProveedor.Condicion, objBeanProveedor.Direccion)
        '            OB_frmValidarRuc.ShowDialog()
        '        End If

        'End Select


        Select Case Asc(e.KeyChar)
            Case 13

                If EstadoServicioruc = True Then
                    objResultado = VerRuc(txtCodigo.Text)
                    If objResultado.blnExiste = True Then
                        If (objResultado.dtResultado.Rows(0)("Estado").ToString() = "ACTIVO" And objResultado.dtResultado.Rows(0)("Condicion").ToString() = "HABIDO") Then
                            If (Len(txtCodigo.Text) = 11 And cboTipoAcreedor.SelectedIndex <> -1) Then
                                Dim Codigo As String = ""
                                Dim CodAcreedor As String = ""
                                Dim CodRuc As String = ""
                                dtTable = New DataTable
                                CodRuc = Trim(txtCodigo.Text)
                                CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
                                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)
                                If dtTable.Rows.Count > 0 Then
                                    If objResultado.dtResultado.Rows(0)("RazonSocial").ToString() <> dtTable.Rows(0).Item("AnaliticoDescripcion") Then
                                        'If False Then
                                        eRegistroDocumento.ActualizarAcreedor(gEmpresa, "P", dtTable.Rows(0).Item("Analiticocodigo"), objResultado.dtResultado.Rows(0)("RazonSocial").ToString())
                                        dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)
                                    End If
                                End If


                                If dtTable.Rows.Count > 0 Then
                                    If Trim(dtTable.Rows(0).Item("Estado")) = "1" Then
                                        MessageBox.Show("Este Anexo esta Deshabilitado en el Sistema", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        Exit Sub
                                    End If
                                End If

                                If dtTable.Rows.Count > 0 Then
                                    cboDocumento.Focus()
                                    txtCod.Text = Trim(dtTable.Rows(0).Item("AnaliticoCodigo"))
                                    lblDescripcionCodigo.Text = Trim(dtTable.Rows(0).Item("AnaliticoDescripcion"))
                                    txtCodigo.Text = Trim(dtTable.Rows(0).Item("Ruc"))
                                    'cboDocumento.Focus()
                                Else


                                    MessageBox.Show("El numero de RUC " & Trim(txtCodigo.Text) & " no esta Registrado en este tipo de Acreedor. Puede Registrarlo.", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Panel3.Visible = True
                                    'VerRuc()
                                    txtCodAcreedor.Text = Trim(txtCodigo.Text)
                                    txtRucAcree.Text = Trim(txtCodigo.Text)
                                    txtNroDocIdent.Text = Trim(txtCodigo.Text)

                                    If objResultado.blnExiste = True Then
                                        'If True Then
                                        txtDescripcionAcreedor.Text = objResultado.dtResultado.Rows(0)("RazonSocial").ToString()
                                        txtDireccionAcree.Text = objResultado.dtResultado.Rows(0)("Direccion").ToString()

                                        txtCod.Text = ""
                                        txtCodigo.Text = ""
                                        lblDescripcionCodigo.Text = ""
                                        txtCodAcreedor.Focus()
                                    Else
                                        MessageBox.Show(objResultado.strMensaje, "Sistema Tesoreria")
                                        Button2_Click(Nothing, Nothing)
                                    End If

                                End If

                            Else
                                If cboTipoAcreedor.SelectedIndex = -1 Then
                                    MessageBox.Show("Seleccione un Tipo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    cboTipoAcreedor.Focus()
                                    Exit Sub
                                End If
                                If Len(txtCodigo.Text) = 0 Then
                                    MessageBox.Show("Ingrese un numero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    txtCodigo.Focus()
                                    Exit Sub
                                End If
                                If Len(txtCodigo.Text) < 11 Then
                                    MessageBox.Show("Ingrese un Numero de Ruc Completo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    txtCodigo.Focus()
                                    Exit Sub
                                End If
                                If Len(txtCodigo.Text) = 0 And cboTipoAcreedor.SelectedIndex = -1 Then
                                    MessageBox.Show("Seleccione un Tipo de Acreedor y Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    cboTipoAcreedor.Focus()
                                    Exit Sub
                                End If
                            End If
                        Else
                            txtCodigo.Text = ""

                            Dim OB_frmValidarRuc As frmValidarRuc = New frmValidarRuc(objResultado.dtResultado.Rows(0)("Ruc").ToString(), objResultado.dtResultado.Rows(0)("RazonSocial").ToString(), objResultado.dtResultado.Rows(0)("Estado").ToString(), objResultado.dtResultado.Rows(0)("Condicion").ToString(), objResultado.dtResultado.Rows(0)("Direccion").ToString())
                            OB_frmValidarRuc.ShowDialog()

                        End If
                    Else
                        MessageBox.Show(objResultado.strMensaje, "Sistema Tesoreria")

                    End If

                Else


                    
                            If (Len(txtCodigo.Text) = 11 And cboTipoAcreedor.SelectedIndex <> -1) Then
                                Dim Codigo As String = ""
                                Dim CodAcreedor As String = ""
                                Dim CodRuc As String = ""
                                dtTable = New DataTable
                                CodRuc = Trim(txtCodigo.Text)
                                CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
                                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)

                                If dtTable.Rows.Count > 0 Then
                                    If Trim(dtTable.Rows(0).Item("Estado")) = "1" Then
                                        MessageBox.Show("Este Anexo esta Deshabilitado en el Sistema", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        Exit Sub
                                    End If
                                End If

                                If dtTable.Rows.Count > 0 Then
                                    cboDocumento.Focus()
                                    txtCod.Text = Trim(dtTable.Rows(0).Item("AnaliticoCodigo"))
                                    lblDescripcionCodigo.Text = Trim(dtTable.Rows(0).Item("AnaliticoDescripcion"))
                                    txtCodigo.Text = Trim(dtTable.Rows(0).Item("Ruc"))
                                    'cboDocumento.Focus()
                                Else


                                    MessageBox.Show("El numero de RUC " & Trim(txtCodigo.Text) & " no esta Registrado en este tipo de Acreedor. Puede Registrarlo.", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Panel3.Visible = True
                                    'VerRuc()
                                    txtCodAcreedor.Text = Trim(txtCodigo.Text)
                                    txtRucAcree.Text = Trim(txtCodigo.Text)
                                    txtNroDocIdent.Text = Trim(txtCodigo.Text)

                               
                            txtDescripcionAcreedor.Text = ""
                            txtDireccionAcree.Text = ""

                                        txtCod.Text = ""
                                        txtCodigo.Text = ""
                                        lblDescripcionCodigo.Text = ""
                                        txtCodAcreedor.Focus()
                                    

                                End If

                            Else
                                If cboTipoAcreedor.SelectedIndex = -1 Then
                                    MessageBox.Show("Seleccione un Tipo de Acreedor", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    cboTipoAcreedor.Focus()
                                    Exit Sub
                                End If
                                If Len(txtCodigo.Text) = 0 Then
                                    MessageBox.Show("Ingrese un numero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    txtCodigo.Focus()
                                    Exit Sub
                                End If
                                If Len(txtCodigo.Text) < 11 Then
                                    MessageBox.Show("Ingrese un Numero de Ruc Completo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    txtCodigo.Focus()
                                    Exit Sub
                                End If
                                If Len(txtCodigo.Text) = 0 And cboTipoAcreedor.SelectedIndex = -1 Then
                                    MessageBox.Show("Seleccione un Tipo de Acreedor y Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    cboTipoAcreedor.Focus()
                                    Exit Sub
                                End If
                            End If
                       
                   

                End If
               

        End Select
    End Sub

    Private Sub txtFiltrar_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFiltrar.TextChanged, txtCodigo.DoubleClick
        'If (Len(txtFiltrar.Text) > 0 And rdbDescr.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
        '    Dim CodAcreedor As String = ""
        '    Dim sDescripcion As String = ""
        '    eTempo = New clsPlantTempo
        '    dtTable = New DataTable
        '    CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
        '    sDescripcion = Trim(txtFiltrar.Text)
        '    dtTable = eRegistroDocumento.fListarProveedores4(gEmpresa, sDescripcion, CodAcreedor)
        '    dgvDetalle.AutoGenerateColumns = False
        '    dvgListProveedores.DataSource = dtTable
        'ElseIf (Len(txtFiltrar.Text) > 0 And rdbCodigo.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
        '    Dim CodAcreedor As String = ""
        '    Dim Codigo As String = ""
        '    eTempo = New clsPlantTempo
        '    dtTable = New DataTable
        '    CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
        '    Codigo = Trim(txtFiltrar.Text)
        '    dtTable = eRegistroDocumento.fListarProveedores3(gEmpresa, Codigo, CodAcreedor)
        '    dgvDetalle.AutoGenerateColumns = False
        '    dvgListProveedores.DataSource = dtTable
        'ElseIf (Len(txtFiltrar.Text) > 0 And rdbNumDoc.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
        '    Dim CodAcreedor As String = ""
        '    Dim NroIdent As String = ""
        '    eTempo = New clsPlantTempo
        '    dtTable = New DataTable
        '    CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
        '    NroIdent = Trim(txtFiltrar.Text)
        '    dtTable = eRegistroDocumento.fListarProveedores2(gEmpresa, NroIdent, CodAcreedor)
        '    dgvDetalle.AutoGenerateColumns = False
        '    dvgListProveedores.DataSource = dtTable
        'ElseIf (Len(txtFiltrar.Text) > 0 And rdbRuc.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
        '    Dim CodAcreedor As String = ""
        '    Dim Ruc As String = ""
        '    eTempo = New clsPlantTempo
        '    dtTable = New DataTable
        '    CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
        '    Ruc = Trim(txtFiltrar.Text)
        '    dtTable = eRegistroDocumento.fListarProveedores(gEmpresa, Ruc, CodAcreedor)
        '    dgvDetalle.AutoGenerateColumns = False
        '    dvgListProveedores.DataSource = dtTable
        'End If

        If (Len(txtFiltrar.Text) > 0 And rdbDescr.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
            Dim CodAcreedor As String = ""
            Dim sDescripcion As String = ""
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
            sDescripcion = Trim(txtFiltrar.Text)
            dtTable = eRegistroDocumento.fListarProveedores4(gEmpresa, sDescripcion, CodAcreedor)
            dgvDetalle.AutoGenerateColumns = False
            dvgListProveedores.DataSource = dtTable
        ElseIf (Len(txtFiltrar.Text) > 0 And rdbCodigo.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
            Dim CodAcreedor As String = ""
            Dim Codigo As String = ""
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
            Codigo = Trim(txtFiltrar.Text)
            dtTable = eRegistroDocumento.fListarProveedores3(gEmpresa, Codigo, CodAcreedor)
            dgvDetalle.AutoGenerateColumns = False
            dvgListProveedores.DataSource = dtTable
        ElseIf (Len(txtFiltrar.Text) > 0 And rdbNumDoc.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
            Dim CodAcreedor As String = ""
            Dim NroIdent As String = ""
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
            NroIdent = Trim(txtFiltrar.Text)
            dtTable = eRegistroDocumento.fListarProveedores2(gEmpresa, NroIdent, CodAcreedor)
            dgvDetalle.AutoGenerateColumns = False
            dvgListProveedores.DataSource = dtTable
        ElseIf (Len(txtFiltrar.Text) > 0 And rdbRuc.Checked = True And cboTipoAcreedor.SelectedIndex <> -1) Then
            Dim CodAcreedor As String = ""
            Dim Ruc As String = ""
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
            Ruc = Trim(txtFiltrar.Text)
            dtTable = eRegistroDocumento.fListarProveedores(gEmpresa, Ruc, CodAcreedor)
            dgvDetalle.AutoGenerateColumns = False
            dvgListProveedores.DataSource = dtTable

        ElseIf (Len(txtFiltrar.Text) = 0 And cboTipoAcreedor.SelectedIndex <> -1) Then
            Dim CodAcreedor As String = ""
            Dim Ruc As String = ""
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)
            Ruc = Trim(txtFiltrar.Text)
            dtTable = eRegistroDocumento.fListarProveedores5(gEmpresa, Ruc, CodAcreedor)
            dgvDetalle.AutoGenerateColumns = False
            dvgListProveedores.DataSource = dtTable
        End If

    End Sub

    Private Sub txtSerie_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSerie.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        'If KeyAscii = "102" Or KeyAscii = "70" Then
        '    KeyAscii = "70"
        '    If txtSerie.Text.Contains("F") Then
        '        KeyAscii = "0"
        '    End If
        'Else
        '    KeyAscii = CShort(SoloNumeros(KeyAscii))
        'End If


        If KeyAscii = 0 Then
            e.Handled = True
        ElseIf KeyAscii = 13 Then
            txtNroDocumento.Focus()
        End If

        'Select Case Asc(e.KeyChar)
        '    Case 13
        '        txtNroDocumento.Focus()
        'End Select

    End Sub

    Private Sub txtNroDocumento_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroDocumento.KeyPress
        'Select Case Asc(e.KeyChar)
        '    Case 13
        '        dtpFechaBase.Focus()
        'End Select

        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        ElseIf KeyAscii = 13 Or KeyAscii = 9 Then
            'dtpFechaBase.Focus()
            btnVerValorizaciones.Focus()
        End If

    End Sub

    Private Sub chkIGV_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles chkIGV.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtImporteIGV.Focus()
        End Select
    End Sub

    Private Sub cboDP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboDP.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                Timer3.Enabled = True
        End Select
    End Sub

    Private Sub txtDP_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDP.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                Timer4.Enabled = True
        End Select
    End Sub

    Private Sub txtNroOrden_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroOrden.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtNroOrden.Text)) > 0 Then
                    dgvOrdenes.Focus()
                ElseIf Len(Trim(txtNroOrden.Text)) = 0 Then
                    'cboMoneda.Focus()
                    Timer2.Enabled = True
                End If
        End Select
    End Sub

    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        If Timer3.Enabled = True Then
            txtDP.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer3.Enabled = False
    End Sub

    Private Sub Timer4_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer4.Tick
        If Timer4.Enabled = True Then
            cboDepartamentos.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer4.Enabled = False
    End Sub

    Private Sub dtpFechaBase_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpFechaBase.KeyPress
        dtpFechaDoc.Value = dtpFechaBase.Value
        Select Case Asc(e.KeyChar)
            Case 13
                'dtpFechaDoc.Value = dtpFechaBase.Value
                txtDias.Focus()
        End Select
    End Sub

    Private Sub txtDias_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDias.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                Timer5.Enabled = True
        End Select
    End Sub
    Private Sub Timer5_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer5.Tick
        If Timer5.Enabled = True Then
            dtpFechaDoc.Focus()
        End If
        Timer5.Enabled = False
    End Sub

    Private Sub dtpFechaDoc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpFechaDoc.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                dtpFechaVen.Focus()
        End Select
    End Sub

    Private Sub dtpFechaVen_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpFechaVen.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtNroOrden.Focus()
        End Select
    End Sub

    Private Sub txtDescripcion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcion.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                Timer6.Enabled = True
        End Select
    End Sub
    Private Sub Timer6_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer6.Tick
        If Timer6.Enabled = True Then
            cboFechaCreacion.Focus()
        End If
        Timer6.Enabled = False
    End Sub

    Private Sub txtUsuario_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUsuario.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                v_GrabaReg()
                If iResultado = 1 Then
                    v_NuevoReg()
                    iResultado = 0
                End If
        End Select
    End Sub

    Private Sub Timer7_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer7.Tick
        'Timer1.Enabled = False
        If Timer7.Enabled = True Then
            dtpFechaBase.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer7.Enabled = False
    End Sub

    Private Sub txtCodAcreedor_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodAcreedor.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            txtDescripcionAcreedor.Focus()
        End If
    End Sub

    Private Sub txtDescripcionAcreedor_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcionAcreedor.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            txtDireccionAcree.Focus()
        End If
    End Sub

    Private Sub txtDireccionAcree_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDireccionAcree.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            txtRucAcree.Focus()
        End If
    End Sub

    Private Sub txtRucAcree_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRucAcree.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            If rdbJuridica.Checked = True Then
                Button1.Focus()
            ElseIf rdbNatural.Checked = True Then
                cboTipoDocIdent.Focus()
            End If
        End If
    End Sub

    Private Sub cboTipoDocIdent_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboTipoDocIdent.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            Timer8.Enabled = True
        End If
    End Sub
    Private Sub Timer8_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer8.Tick
        If Timer8.Enabled = True Then
            txtNroDocIdent.Focus()
        End If
        Timer8.Enabled = False
    End Sub

    Private Sub txtNroDocIdent_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroDocIdent.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            Button1.Focus()
        End If
    End Sub

    Private Sub txtFiltrar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFiltrar.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
            dvgListProveedores.Focus()
        End If
    End Sub

    Private Sub rdbJuridica_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbJuridica.Click
        txtNroDocIdent.Focus()
    End Sub

    Private Sub rdbNatural_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbNatural.Click
        cboTipoDocIdent.Focus()
    End Sub

    Private Sub rdbCodigo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbCodigo.Click
        txtFiltrar.Focus()
    End Sub

    Private Sub Timer9_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer9.Tick
        'Timer1.Enabled = False
        If Timer9.Enabled = True Then
            dgvDetalle.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer9.Enabled = False
    End Sub

    Private Sub txtNroDocumentoBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroDocumentoBuscar.TextChanged
        eRegistroDocumento = New clsRegistroDocumento
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        If Len(Trim(txtNroDocumentoBuscar.Text)) > 0 Then
            'dtTableLista = eRegistroDocumento.fListarConsultaDoc(0, gEmpresa, "", Trim(txtNroDocumentoBuscar.Text), "", Today(), Today(), "", "")

            If Trim(glbUsuCategoria) = "A" Then
                dtTableLista = eRegistroDocumento.fListarConsultaDoc(0, gEmpresa, "", Trim(txtNroDocumentoBuscar.Text), "", Today(), Today(), "", "", gPeriodo, gUsuario)
            ElseIf Trim(glbUsuCategoria) = "U" Then
                dtTableLista = eRegistroDocumento.fListarConsultaDoc(1, gEmpresa, "", Trim(txtNroDocumentoBuscar.Text), "", Today(), Today(), "", "", gPeriodo, gUsuario)
            End If

            dgvDetalle.AutoGenerateColumns = False
            dgvDetalle.DataSource = dtTableLista
            cmr = BindingContext(dgvDetalle.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eRegistroDocumento.iNroRegistros
            If eRegistroDocumento.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(Trim(txtNroDocumentoBuscar.Text)) = 0 Then
            mMostrarGrilla()
        End If
    End Sub

    Private Sub rdbDescr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbDescr.Click
        txtFiltrar.Focus()
    End Sub

    Private Sub rdbRuc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRuc.Click
        txtFiltrar.Focus()
    End Sub

    Private Sub rdbNumDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbNumDoc.Click
        txtFiltrar.Focus()
    End Sub

    Private Sub dtpFechaBase_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFechaBase.LostFocus
        dtpFechaDoc.Value = dtpFechaBase.Value
        cboDocumento_SelectedIndexChanged(sender, e)
    End Sub

    'Private Sub dtpFechaBase_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFechaBase.TextChanged
    '  If Len(Trim(dtpFechaBase.Text)) > 0 Then
    '      dtpFechaVen.Value = dtpFechaBase.Value 'dtpFechaBase.Value.AddDays(Val(Trim(txtDias.Text)))
    '  'Else
    '  '    dtpFechaVen.Value = dtpFechaBase.Value
    '  End If
    'End Sub

    'Private Sub dtpFechaBase_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaBase.ValueChanged
    'Timer10.Enabled = False
    'End Sub

    'Private Sub Timer9_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer9.Tick
    '    'Timer1.Enabled = False
    '    If Timer9.Enabled = True Then
    '        dgvDetalle.Focus()
    '        'BeButton4_Click(sender, e)
    '    End If
    '    Timer9.Enabled = False
    'End Sub

    'Private Sub Timer10_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer10.Tick
    '  If Timer10.Enabled = True Then
    '    dtpFechaDoc.Value = dtpFechaBase.Value
    '  End If
    '  Timer10.Enabled = False
    'End Sub

    Private m_EditingRow2 As Integer = -1
    Private Sub dgvOrdenes_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvOrdenes.EditingControlShowing
        m_EditingRow2 = dgvOrdenes.CurrentRow.Index
    End Sub

    Private Sub dgvOrdenes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvOrdenes.KeyDown
        Try
            If dgvOrdenes.Rows.Count > 0 Then
                If e.KeyCode = Keys.Return Then
                    Dim cur_cell As DataGridViewCell = dgvOrdenes.CurrentCell
                    Dim col As Integer = cur_cell.ColumnIndex
                    col = (col + 1) Mod dgvOrdenes.Columns.Count
                    cur_cell = dgvOrdenes.CurrentRow.Cells(col)
                    dgvOrdenes.CurrentCell = cur_cell
                    e.Handled = True
                End If
                'Else   
                'txtFiltrar.Focus()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub dgvOrdenes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvOrdenes.KeyPress
        If dgvOrdenes.Rows.Count > 0 Then


            Dim CodOrden As String = ""
            Dim NroOrden As String = ""
            Dim sCodProvLog As String = ""

            If Len(Trim(txtCodigo.Text)) = 0 Then
                MessageBox.Show("Ingrese un N�mero de Ruc", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtCodigo.Focus()
                txtNroOrden.Clear()
                Panel4.Visible = False
                Exit Sub
            End If

            Dim sCodProvRUC As String = ""
            sCodProvRUC = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(6).Value
            If Len(Trim(sCodProvRUC)) = 11 And Len(Trim(txtCodigo.Text)) = 11 Then
                If Trim(sCodProvRUC) <> Trim(txtCodigo.Text) Then
                    MessageBox.Show("Este N�mero de Orden de Compra no Pertenece al Proveedor " & Trim(lblDescripcionCodigo.Text), "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNroOrden.Focus()
                    Exit Sub
                End If
            End If
            Select Case Asc(e.KeyChar)
                Case 13
                    Try
                        sCodProvLog = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(5).Value
                        CodOrden = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(4).Value
                        NroOrden = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(0).Value
                        Call LLena_OrdenCompra(CodOrden, NroOrden)
                        txtCodOrden.Text = Trim(CodOrden)
                        txtNroOrden.Text = Trim(NroOrden)
                        txtCodProvLog.Text = Trim(sCodProvLog)
                        Panel4.Visible = False
                        cboMoneda.Focus()
                    Catch ex As Exception
                        Exit Sub
                    End Try
            End Select
        End If
    End Sub

    Private Sub LLena_OrdenCompra(ByVal pCodigoOC As String, ByVal pNumeroOC As String)
        Try
            Dim Dread As SqlDataReader
            sCodGestion = String.Empty
            Dread = eRegistroDocumento.Consulta_OC(gEmpresa, gPeriodo, pCodigoOC, pNumeroOC, txtCodigo.Text)
            dblSaldoOC = eRegistroDocumento.Saldo_OC(gEmpresa, pCodigoOC, pNumeroOC)
            If Dread.HasRows = True Then
                Dread.Read()
                cboMoneda.SelectedValue = Dread("Moneda")
                cboMoneda.Enabled = False
                'txtImporteTotal.Text = Dread("Total_Importe")
                If bolEdit = False Then txtImporteTotal.Text = Format(dblSaldoOC, "#,##0.00")
                If Dread("Flag_IGV") = "1" Then chkIGV.Checked = True
                If Dread("Flag_Percepcion") = "1" Then
                    cboDP.SelectedIndex = 1
                ElseIf Dread("Flag_Detraccion") = "1" Then
                    cboDP.SelectedIndex = 0
                ElseIf Dread("Flag_Percepcion") = "0" And Dread("Flag_Detraccion") = "0" Then
                    cboDP.SelectedIndex = -1
                End If
                sCodGestion = Trim(Dread("CodigoGestion"))
                If Dread("Total_Importe") = 0 Then
                    dImporteOC = dblSaldoOC
                Else
                    dImporteOC = Dread("Total_Importe")
                End If
                bolNuevaMod = True

            Else
                'ASIGNAR EL MONTO DE LA O/C (traer el monto de la orden, Tabla OrdenCabecera)
                Dread = eRegistroDocumento.OC_Pasadas(gEmpresa, txtCodigo.Text.Trim, pCodigoOC, pNumeroOC)
                If Dread.HasRows = True Then
                    Dread.Read()
                    dImporteOC = Dread("Total")
                    cboMoneda.SelectedValue = Dread("MonCodigo")
                    cboMoneda.Enabled = False
                    txtImporteTotal.Text = Format(dblSaldoOC, "#,##0.00")
                    'txtImporteTotal.Text = Format(Dread("Total"), "#,##0.00")
                    If Convert.ToDouble(Dread("IGV")) > 0 Then
                        chkIGV.Checked = True
                    Else
                        chkIGV.Checked = False
                    End If
                    cboDP.SelectedIndex = -1
                    sCodGestion = String.Empty
                    bolNuevaMod = False
                End If
            End If
            txtDP.Text = String.Empty
            Dread.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub dgvOrdenes_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.SelectionChanged
        If m_EditingRow2 >= 0 Then
            Dim new_row As Integer = m_EditingRow2
            m_EditingRow2 = -1
            dgvOrdenes.CurrentCell = dgvOrdenes.Rows(new_row).Cells(dgvOrdenes.CurrentCell.ColumnIndex)
        End If
    End Sub

    Private Sub txtNroDocumento_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNroDocumento.LostFocus
        Dim NroSerie As String = ""
        Dim NroDocumento As String = ""
        Dim TipoDoc As String = ""
        NroSerie = Trim(txtSerie.Text)
        NroDocumento = Trim(txtNroDocumento.Text)
        TipoDoc = Trim(cboDocumento.SelectedValue)
        eRegistroDocumento = New clsRegistroDocumento
        dtTable = New DataTable
        dtTable = eRegistroDocumento.VerificarDocExiste(TipoDoc, NroSerie, NroDocumento, Trim(txtCod.Text))
        If dtTable.Rows.Count > 0 And iOpcion <> 2 Then 'ya existe doc registrado
            MessageBox.Show("El n�mero de Documento " & Trim(txtSerie.Text) & "-" & Trim(txtNroDocumento.Text) & " ya esta Registrado. Ingrese otro Numero.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtNroDocumento.Clear()
            txtNroDocumento.Focus()
        End If
    End Sub

    Private Sub cboMoneda_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboMoneda.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        If KeyAscii = 13 Then
        End If
    End Sub


    Private Sub Timer10_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer10.Tick
        If Timer10.Enabled = True Then
            txtImporteTotal.Focus()
        End If
        Timer10.Enabled = False
    End Sub

    Private Sub txtValorCompra_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValorCompra.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                'Timer11.Enabled = True
        End Select
    End Sub

    Private Sub Timer11_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer11.Tick
        If Timer11.Enabled = True Then
            txtImporteIGV.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer11.Enabled = False
    End Sub

    Private Sub txtValorCompra_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValorCompra.LostFocus
        Try
            Dim MontoEscrito As String = ""
            MontoEscrito = txtImporteTotal.Text.Trim
            If MontoEscrito.Trim = "" Then
                MontoEscrito = 0
            End If
            'txtImporteTotal.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")

            'dImporteOC =====> MONTO ORIGEN DE LA O/C
            'dblSaldoOC =====> MONTO SALDO DE LA O/C
            'VALIDAR:
            'CAPTURAR EL MONTO DEL SALDO,para q al digitar el monto de la Factura no sobrepase el Monto Total
            'EL MONTO TOTAL INGRESADO DEBE SER MENOR O IGUAL AL SALDO Y MENOR Q EL MONTO TOTAL DE LA O/C
            'LAS O/C SE DEBEN VISUALIZAR SIEMPRE Y CUANDO EL SALDO NO SEA 0

            '*****************
            'deshabilitado 07/11/2014

            'If CDbl(txtImporteTotal.Text) > 0 Then
            '    If txtNroOrden.Text <> String.Empty And txtCodOrden.Text <> String.Empty Then
            '        If CDbl(txtImporteTotal.Text) < dImporteOC And CDbl(txtImporteTotal.Text) <= dblSaldoOC Then
            '            Dim frmDetalleOC As New FrmDetalleTempOC
            '            frmDetalleOC.FrmRegDocumentos = Me
            '            cboDP.Focus()
            '            If bolNuevaMod = True Then
            '                frmDetalleOC.ShowDialog()
            '            End If

            '        ElseIf CDbl(txtImporteTotal.Text) > dImporteOC Then
            '            MessageBox.Show("El Importe ingresado no puede ser mayor que el importe de la O/C", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '            txtImporteTotal.Focus()
            '        ElseIf CDbl(txtImporteTotal.Text) > dblSaldoOC Then
            '            MessageBox.Show("El Importe ingresado no puede ser mayor al SALDO de la O/C" & vbCrLf & _
            '                            "El SALDO ACTUAL para la seleccionada Orden es: " & dblSaldoOC, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '            txtImporteTotal.Focus()

            '        Else
            '            DT_DetalleOC = New DataTable
            '            Dim frmDetalleOC As New FrmDetalleTempOC
            '            frmDetalleOC.FrmRegDocumentos = Me
            '            frmDetalleOC.dgvDetalleOC.DataSource = eRegistroDocumento.TempDetalleOC(gEmpresa, gPeriodo, txtCodOrden.Text, txtNroOrden.Text)
            '            DT_DetalleOC = eRegistroDocumento.TempDetalleOC(gEmpresa, gPeriodo, txtCodOrden.Text, txtNroOrden.Text)

            '        End If
            '    End If
            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtImporteTotal.Text = 0
        End Try
    End Sub

    'Private Sub chkCancelado_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    eRegistroDocumento = New clsRegistroDocumento
    '    If chkCancelado.Checked = True Then
    '        iResultado = eRegistroDocumento.fCancelarFactura(45, Trim(sCodRegistro), gEmpresa, "00004", 1, 1, gUsuario, Convert.ToString(IIf(Me.chkArchivado.Checked = True, 1, 0)))
    '    ElseIf chkCancelado.Checked = False Then
    '        iResultado = eRegistroDocumento.fCancelarFactura(45, Trim(sCodRegistro), gEmpresa, "00001", 0, 0, gUsuario, Convert.ToString(IIf(Me.chkArchivado.Checked = True, 1, 0)))
    '    End If
    'End Sub

    'Private Sub chkArchivado_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    chkCancelado_Click(sender, e)
    'End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If rdb1.Checked = True Then
            Dim x As frmReporteDocsxDia = frmReporteDocsxDia.Instance
            x.MdiParent = frmPrincipal
            x.Show()
            Exit Sub
        End If

        'If rdb2.Checked = True And (chkTodosUsuarios.Checked = False Or cboUsuarios.SelectedIndex = -1) Then MessageBox.Show("Seleccione Todos o un usuario", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : Exit Sub

        If rdb2.Checked = True And (chkTodos2.Checked = True Or cboTodosProv.SelectedIndex > -1) Then
            'Dim x As frmReporteDocsxDia2 = frmReporteDocsxDia2.Instance
            'x.F1 = dtpF1.Value
            'x.F2 = dtpF2.Value

            'If chkTodos2.Checked = True Then
            '    x.iOpcion = "Todos"
            '    x.CodProv = ""
            'ElseIf chkTodos2.Checked = False And cboTodosProv.SelectedIndex > -1 Then
            '    x.iOpcion = "Uno"
            '    x.CodProv = Trim(cboTodosProv.SelectedValue)
            'End If

            'x.MdiParent = frmPrincipal
            'x.Show()


            Try
                Dim dtTable2 As DataTable
                Dim pIdRetencion As String = ""
                Dim pSerie As String = ""
                dtTable2 = New DataTable
                'eGastosGenerales = New clsGastosGenerales

                eRegistroDocumento = New clsRegistroDocumento
                eTempo = New clsPlantTempo
                dtTable2 = New DataTable
                If Trim(glbUsuCategoria) = "A" Then
                    If chkTodos2.Checked = True Then
                        dtTable2 = eRegistroDocumento.fListarDocsPorRango(0, dtpF1.Value, dtpF2.Value, gEmpresa, gDesEmpresa, gEmprRuc, "", gUsuario, gPeriodo)
                    ElseIf chkTodos2.Checked = False And cboTodosProv.SelectedIndex > -1 Then
                        dtTable2 = eRegistroDocumento.fListarDocsPorRango(1, dtpF1.Value, dtpF2.Value, gEmpresa, gDesEmpresa, gEmprRuc, cboTodosProv.SelectedValue, gUsuario, gPeriodo)
                    End If

                    If chkTodosUsuarios.Checked = False And cboUsuarios.SelectedIndex > -1 Then
                        dtTable2 = eRegistroDocumento.fListarDocsPorRango(4, dtpF1.Value, dtpF2.Value, gEmpresa, gDesEmpresa, gEmprRuc, "", cboUsuarios.SelectedValue, gPeriodo)
                    End If
                ElseIf Trim(glbUsuCategoria) = "U" Then
                    If chkTodos2.Checked = True Then
                        dtTable2 = eRegistroDocumento.fListarDocsPorRango(2, dtpF1.Value, dtpF2.Value, gEmpresa, gDesEmpresa, gEmprRuc, "", gUsuario, gPeriodo)
                    ElseIf chkTodos2.Checked = False And cboTodosProv.SelectedIndex > -1 Then
                        dtTable2 = eRegistroDocumento.fListarDocsPorRango(3, dtpF1.Value, dtpF2.Value, gEmpresa, gDesEmpresa, gEmprRuc, cboTodosProv.SelectedValue, gUsuario, gPeriodo)
                    End If
                End If

                If dtTable2.Rows.Count > 0 Then

                    'If rdbCancelar.Checked = True Then
                    'Dim glosa As String = ""
                    'glosa = "Fecha Recepci�n Entre : " & dtpF1.Value & " y " & dtpF2.Value
                    Muestra_Reporte(RutaAppReportes & "rptDocumentosPorDia5.rpt", dtTable2, "TblLibroDiario", "", "FechaRecepcion1;" & dtpF1.Value, "FechaRecepcion2;" & dtpF2.Value, "NombreEmpr;" & gDesEmpresa, "RucEmpr;" & gEmprRuc, "Glosa;" & "VACA")
                    'End If

                    'If rdbCancelados.Checked = True Then
                    '    Muestra_Reporte(RutaAppReportes & "rptDocPagados.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & Trim(cboCentroCosto.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & Trim(cboCentroCosto.SelectedValue))
                    'End If

                    'If rdbCC.Checked = True Then
                    '    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & Trim(cboCentroCosto.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & Trim(cboCentroCosto.SelectedValue))
                    'End If
                    'If chk3.Checked = True Then
                    '    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                    'End If
                    'If chk4.Checked = True Then
                    '    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente3.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                    'End If
                ElseIf dtTable2.Rows.Count = 0 Then
                    Dim mensaje As String = ""
                    'If rdbCancelar.Checked = True Then
                    mensaje = "No se Encontr� ning�n Comprobante."
                    'End If
                    'If rdbCancelados.Checked = True Then
                    '    mensaje = "No se Encontro ningun Comprobante Cancelado."
                    'End If
                    'If rdbCC.Checked = True Then
                    '    mensaje = "No se Encontro Cuenta Corriente de Proveedores en este Centro de Costo."
                    'End If
                    MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            Catch ex As Exception
                MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
            Me.Cursor = Cursors.Default


        Else
            MessageBox.Show("Seleccione Todos o un Proveedor", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
    End Sub


    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub BeLabel33_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel33.Click
        Panel6.Visible = False
    End Sub

    Private Sub chkTodos2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodos2.CheckedChanged
        If chkTodos2.Checked = True Then
            cboTodosProv.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboTodosProv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTodosProv.SelectedIndexChanged
        If cboTodosProv.SelectedIndex > -1 Then
            chkTodos2.Checked = False
        End If
    End Sub

    Private Sub rdb1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdb1.Click
        If rdb1.Checked = True Then
            rdb2.Checked = False
        End If
    End Sub

    Private Sub rdb2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdb2.Click
        If rdb2.Checked = True Then
            rdb1.Checked = False
        End If
    End Sub

    Private Sub BeLabel36_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel36.Click
        Panel7.Visible = False
    End Sub

    Private Sub btnVerValorizaciones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerValorizaciones.Click
        If Panel7.Visible = False Then
            Panel7.Visible = True
            Panel8.Visible = False
            eValorizaciones = New clsValorizaciones
            'cboObra.DataSource = eValorizaciones.fListarObras(50, gEmpresa, gPeriodo, "", "", Today())

            If Trim(glbUsuCategoria) = "A" Then
                cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today(), "")
            ElseIf Trim(glbUsuCategoria) = "U" Then
                cboObra.DataSource = eValorizaciones.fListarObras(73, gEmpresa, gPeriodo, "", gUsuario, Today(), "")
            End If

            If eValorizaciones.iNroRegistros > 0 Then
                cboObra.ValueMember = "ObraCodigo"
                cboObra.DisplayMember = "ObraDescripcion"
                cboObra.SelectedIndex = -1
                cboObra.Focus()
            End If
        End If
    End Sub

    Private Sub cboObra_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboObra.SelectionChangeCommitted
        If cboObra.SelectedIndex > -1 Then
            eValorizaciones = New clsValorizaciones
            Dim dtValorizaciones As DataTable
            dtValorizaciones = New DataTable
            dtValorizaciones = eValorizaciones.fListarObras(51, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(txtCodigo.Text), Today(), "")
            For x As Integer = 0 To dgvValorizaciones.RowCount - 1
                dgvValorizaciones.Rows.Remove(dgvValorizaciones.CurrentRow)
            Next
            If dtValorizaciones.Rows.Count > 0 Then
                dgvValorizaciones.DataSource = dtValorizaciones
            End If
        End If
    End Sub

    Private Sub dgvValorizaciones_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvValorizaciones.CellContentClick
        If dgvValorizaciones.Rows.Count > 0 Then
            Dim columna As Integer = dgvValorizaciones.CurrentCell.ColumnIndex
            Try
                dgvValorizaciones.CurrentCell = dgvValorizaciones(3, dgvValorizaciones.CurrentRow.Index)
                dgvValorizaciones.CurrentCell = dgvValorizaciones(columna, dgvValorizaciones.CurrentRow.Index)
                If dgvValorizaciones.Rows.Count > 0 Then
                    Dim iFila As Integer
                    Calcular()
                    If dgvValorizaciones.Rows.Count > 0 And dgvValorizaciones.CurrentCell.ColumnIndex = 0 Then
                        iFila = dgvValorizaciones.CurrentRow.Index
                        dgvValorizaciones.CurrentCell = dgvValorizaciones(3, 0)
                        dgvValorizaciones.CurrentCell = dgvValorizaciones(0, iFila)
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
        Calcular()
    End Sub

    Sub Calcular()
        CountDocumentos = 0
        dblMonto = 0
        If dgvValorizaciones.Rows.Count > 0 Then
            For x As Integer = 0 To dgvValorizaciones.Rows.Count - 1
                If Convert.ToBoolean(dgvValorizaciones.Rows(x).Cells("X").Value) = True Then
                    dblMonto = dblMonto + dgvValorizaciones.Rows(x).Cells("TotalVal").Value
                    CountDocumentos = CountDocumentos + 1
                End If
            Next
            txtImporteTotal.Text = Format(dblMonto, "#,##0.00")
        End If
    End Sub

    Private Sub dgvValorizaciones_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvValorizaciones.MouseUp
        If dgvValorizaciones.Rows.Count > 0 Then
            Calcular()
        End If
    End Sub

    Private Sub btnVerFacturas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerFacturas.Click
        If Panel8.Visible = False Then
            If CountFacturas = 0 Then
                Panel8.Visible = True
                Panel7.Visible = False
                txtBuscarFactura.Clear()
                txtBuscarFactura_TextChanged(sender, e)
                txtBuscarFactura.Focus()
            ElseIf CountFacturas > 0 Then
                Panel8.Visible = True
                Panel7.Visible = False
                txtBuscarFactura.Focus()
            End If
        End If
    End Sub

    Private Sub BeLabel37_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel37.Click
        Panel8.Visible = False
    End Sub

    Private Sub dgvFacturas_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFacturas.CellContentClick
        If dgvFacturas.Rows.Count > 0 Then
            Dim columna As Integer = dgvFacturas.CurrentCell.ColumnIndex
            Try
                dgvFacturas.CurrentCell = dgvFacturas(3, dgvFacturas.CurrentRow.Index)
                dgvFacturas.CurrentCell = dgvFacturas(columna, dgvFacturas.CurrentRow.Index)
                If dgvFacturas.Rows.Count > 0 Then
                    Dim iFila As Integer
                    CalcularFacturas()
                    If dgvFacturas.Rows.Count > 0 And dgvFacturas.CurrentCell.ColumnIndex = 0 Then
                        iFila = dgvFacturas.CurrentRow.Index
                        dgvFacturas.CurrentCell = dgvFacturas(3, 0)
                        dgvFacturas.CurrentCell = dgvFacturas(0, iFila)
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
        CalcularFacturas()
    End Sub

    Sub CalcularFacturas()
        CountFacturas = 0
        If dgvFacturas.Rows.Count > 0 Then
            For x As Integer = 0 To dgvFacturas.Rows.Count - 1
                If Convert.ToBoolean(dgvFacturas.Rows(x).Cells("XF").Value) = True Then
                    CountFacturas = CountFacturas + 1
                End If
            Next
        End If
    End Sub

    Private Sub dgvFacturas_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvFacturas.MouseUp
        If dgvFacturas.Rows.Count > 0 Then
            CalcularFacturas()
        End If
    End Sub

    Private Sub txtBuscarFactura_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscarFactura.TextChanged
        eRegistroDocumento = New clsRegistroDocumento
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        If Len(Trim(txtBuscarFactura.Text)) = 0 Then
            dtTableLista = eRegistroDocumento.fListarFacturas(46, gEmpresa, Trim(txtCod.Text), "")
        ElseIf Len(Trim(txtBuscarFactura.Text)) > 0 Then
            dtTableLista = eRegistroDocumento.fListarFacturas(47, gEmpresa, Trim(txtCod.Text), Trim(txtBuscarFactura.Text))
        End If
        dgvFacturas.AutoGenerateColumns = False
        dgvFacturas.DataSource = dtTableLista
        'If dtTableLista.Rows.Count > 0 Then
        CountFacturas = 0
        'End If
        'cmr = BindingContext(dgvDetalle.DataSource)
    End Sub


    Private Sub txtBuscaGuiaRemision_TextChanged(sender As Object, e As EventArgs) Handles txtBuscaGuiaRemision.TextChanged
        eClsOtros = New ClsOtros
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = eClsOtros.fListarNroreferenciaAlmacenIngreso("C000000156", "001-0534088")

        dgvGuiaRemision.AutoGenerateColumns = False
        dgvGuiaRemision.DataSource = dtTableLista
        'If dtTableLista.Rows.Count > 0 Then
        CountFacturas = 0
    End Sub

    Private Sub btnValidarRuc_Click(sender As Object, e As EventArgs) Handles btnValidarRuc.Click
        Dim kea As New KeyPressEventArgs(Convert.ToChar(13))

        Call txtCodigo_KeyPress1(sender, kea)

        'objResultado = VerRuc(txtCodigo.Text)
        'If objResultado.blnExiste = True Then

        '    If (objResultado.dtResultado.Rows(0)("Estado").ToString() = "ACTIVO" And objResultado.dtResultado.Rows(0)("Condicion").ToString() = "HABIDO") Then

        '    Else
        '        txtCodigo.Text = ""
        '        Dim OB_frmValidarRuc As frmValidarRuc = New frmValidarRuc(objResultado.dtResultado.Rows(0)("Ruc").ToString(), objResultado.dtResultado.Rows(0)("RazonSocial").ToString(), objResultado.dtResultado.Rows(0)("Estado").ToString(), objResultado.dtResultado.Rows(0)("Condicion").ToString(), objResultado.dtResultado.Rows(0)("Direccion").ToString())
        '        OB_frmValidarRuc.ShowDialog()
        '    End If
        'Else
        '    MessageBox.Show(objResultado.strMensaje, "Sistema Tesoreria")

        'End If
    End Sub

    
    Private Sub chkTodosUsuarios_CheckedChanged(sender As Object, e As EventArgs) Handles chkTodosUsuarios.CheckedChanged
        If chkTodosUsuarios.Checked = True Then
            cboUsuarios.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboUsuarios_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboUsuarios.SelectedIndexChanged
        If cboUsuarios.SelectedIndex > -1 Then
            chkTodosUsuarios.Checked = False
        End If
    End Sub

    Private Sub dgvDetalle_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick
        Dim VL_RESULT As Integer
        If dgvDetalle.CurrentRow.Cells("VISTO").Value = 1 Then

            VL_RESULT = eRegistroDocumento.fGrabar_recepcion_conta(gEmpresa, dgvDetalle.CurrentRow.Cells("IdRegistro").Value, gPeriodo, 0)
        Else
            VL_RESULT = eRegistroDocumento.fGrabar_recepcion_conta(gEmpresa, dgvDetalle.CurrentRow.Cells("IdRegistro").Value, gPeriodo, 1)
        End If


    End Sub

End Class