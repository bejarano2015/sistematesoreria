Option Explicit On
Imports System.Xml
Imports MSXML2

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad

Public Class frmTipoDestinoGasto

    Private eTipoGasto As clsTipoGasto
    Private eTipoDestino As clsTipoDestino
    Private eTipoGastoDestino As clsTipoGastoDestino
    Private eLogin As clsLogin
    Dim odtTipDestinoGasto As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared decImporte As Decimal
    Dim strAccion As String

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eLogin = New clsLogin
        eTipoGasto = New clsTipoGasto
        eTipoDestino = New clsTipoDestino
        eTipoGastoDestino = New clsTipoGastoDestino

        tabMantenimiento.SelectedTab = tabDetalle
        'botonesMantenimiento(mnuMantenimiento, True)

        InitCombos()
        CargarGastoDestino()
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        ''Se valida los campos
        'If txtNombre.Text.Trim.Length = 0 Then MessageBox.Show("El nombre es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtNombre.Focus() : Exit Sub
        If gvTipoGastoDestino.RowCount > 0 Then
            Guardar()

            CargarGastoDestino()
            MessageBox.Show("Se grab� correctamente. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        tabMantenimiento.SelectedTab = tabDetalle
        CargarGastoDestino()
    End Sub

    Private Sub btnAgregar_Click(sender As Object, e As EventArgs) Handles btnAgregar.Click
        'gvTipoGastoDestino.Rows.Add(0, gEmpresa, Convert.ToDecimal(cboGastos.SelectedValue), Convert.ToDecimal(cboDestino.SelectedValue), Convert.ToString(cboCuentaContable.SelectedValue))

        Dim row As DataRow
        row = odtTipDestinoGasto.NewRow
        row("IDTIPOGASTODESTINO") = 0
        row("IDEMPRESA") = gEmpresa
        row("IDTIPOGASTO") = Convert.ToDecimal(cboGastos.SelectedValue)
        row("IDTIPODESTINO") = Convert.ToDecimal(cboDestino.SelectedValue)
        row("IDCUENTACONTABLE") = Convert.ToString(cboCuentaContable.SelectedValue)
        odtTipDestinoGasto.Rows.Add(row)
        gvTipoGastoDestino.DataSource = odtTipDestinoGasto
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If gvTipoGastoDestino.RowCount > 0 Then
            'Dim intFila As Integer = gvTipoGastoDestino.CurrentRow.Index
            'odtTipDestinoGasto.Rows(intFila)("IDTIPOGASTODESTINO") = -1
            gvTipoGastoDestino.Rows.Remove(gvTipoGastoDestino.CurrentRow)
            odtTipDestinoGasto.AcceptChanges()
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

#End Region

#Region " M�todos privados "

    Private Sub CargarGastoDestino()
        Try
            ' ''Dim odtData As New DataTable
            odtTipDestinoGasto = eTipoGastoDestino.fCargarTipoGastoDestino()
            gvTipoGastoDestino.DataSource = odtTipDestinoGasto
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub InitCombos()
        cboEmpresa.DataSource = eLogin.fListarEmpresas()
        If eLogin.iNroRegistros > 0 Then
            cboEmpresa.ValueMember = "EmprCodigo"
            cboEmpresa.DisplayMember = "Descripcion"
            cboEmpresa.SelectedValue = gEmpresa
            'txtUsuario.Focus()
        End If

        cboGastos.DataSource = eTipoGasto.fCargarTipoGasto()
        If eTipoGasto.iNroRegistros > 0 Then
            cboGastos.ValueMember = "IDTIPOGASTO"
            cboGastos.DisplayMember = "NOMBRE"
        End If

        cboDestino.DataSource = eTipoDestino.fCargarTipoDestino()
        If eTipoDestino.iNroRegistros > 0 Then
            cboDestino.ValueMember = "IDTIPODESTINO"
            cboDestino.DisplayMember = "NOMBRE"
        End If

        cboCuentaContable.DataSource = eTipoGastoDestino.fCargarPlanCuentas()
        If eTipoGastoDestino.iNroRegistros > 0 Then
            cboCuentaContable.ValueMember = "ID_CUENTA"
            cboCuentaContable.DisplayMember = "CUENTA"
        End If

        cbogrillaIdTipoGasto.DataSource = eTipoGasto.fCargarTipoGasto()
        If eTipoGasto.iNroRegistros > 0 Then
            cbogrillaIdTipoGasto.ValueMember = "IDTIPOGASTO"
            cbogrillaIdTipoGasto.DisplayMember = "NOMBRE"
        End If

        cbogrillaIdtipoDestino.DataSource = eTipoDestino.fCargarTipoDestino()
        If eTipoDestino.iNroRegistros > 0 Then
            cbogrillaIdtipoDestino.ValueMember = "IDTIPODESTINO"
            cbogrillaIdtipoDestino.DisplayMember = "NOMBRE"
        End If

        cbogrillaIdCuentaContable.DataSource = eTipoGastoDestino.fCargarPlanCuentas()
        If eTipoGastoDestino.iNroRegistros > 0 Then
            cbogrillaIdCuentaContable.ValueMember = "ID_CUENTA"
            cbogrillaIdCuentaContable.DisplayMember = "CUENTA"
        End If
    End Sub

    Private Sub LimpiarControles()
        'txtAbreviatura.Text = "" : txtNombre.Text = ""
    End Sub

#End Region

#Region " M�todos de BD "

    Private Sub Guardar()
        Try
            Dim iResultado As Int32
            gvTipoGastoDestino.EndEdit()

            iResultado = eTipoGastoDestino.fEliminar(gEmpresa, 3)

            For i As Integer = 0 To gvTipoGastoDestino.RowCount - 1
                If Convert.ToInt32(gvTipoGastoDestino.Rows(i).Cells("IDTIPOGASTODESTINO").Value) = 0 Then
                    iResultado = eTipoGastoDestino.fGrabar(0, gEmpresa, Convert.ToDecimal(gvTipoGastoDestino.Rows(i).Cells("cbogrillaIdTipoGasto").Value), Convert.ToDecimal(gvTipoGastoDestino.Rows(i).Cells("cbogrillaIdtipoDestino").Value), Convert.ToString(gvTipoGastoDestino.Rows(i).Cells("cbogrillaIdCuentaContable").Value), 1)
                Else
                    iResultado = eTipoGastoDestino.fGrabar(Convert.ToDecimal(gvTipoGastoDestino.Rows(i).Cells("IDTIPOGASTODESTINO").Value), gEmpresa, Convert.ToDecimal(gvTipoGastoDestino.Rows(i).Cells("cbogrillaIdTipoGasto").Value), Convert.ToDecimal(gvTipoGastoDestino.Rows(i).Cells("cbogrillaIdtipoDestino").Value), Convert.ToString(gvTipoGastoDestino.Rows(i).Cells("cbogrillaIdCuentaContable").Value), 2)
                End If
            Next
        Catch ex As Exception
            MessageBox.Show("No se pudo grabar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            Dim iResultado As Int32

            'iResultado = eTipoGasto.fGrabar(Convert.ToDecimal(gvTipoGasto.CurrentRow.Cells("IDTIPOGASTO").Value), txtNombre.Text.Trim, txtAbreviatura.Text.Trim, chkEstado.CheckState, 2)
        Catch ex As Exception
            MessageBox.Show("No se pudo modificar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

End Class