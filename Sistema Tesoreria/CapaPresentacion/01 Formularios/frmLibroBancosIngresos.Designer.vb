<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroBancosIngresos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboBanco = New ctrLibreria.Controles.BeComboBox
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.cboEmpresas = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.chkTodasEmpr = New System.Windows.Forms.CheckBox
        Me.chkTodosBanc = New System.Windows.Forms.CheckBox
        Me.chkTodosCuen = New System.Windows.Forms.CheckBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.btnVerPrestamos = New System.Windows.Forms.Button
        Me.r1 = New System.Windows.Forms.RadioButton
        Me.r2 = New System.Windows.Forms.RadioButton
        Me.r3 = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboBanco
        '
        Me.cboBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBanco.ForeColor = System.Drawing.Color.Black
        Me.cboBanco.FormattingEnabled = True
        Me.cboBanco.KeyEnter = True
        Me.cboBanco.Location = New System.Drawing.Point(82, 92)
        Me.cboBanco.Name = "cboBanco"
        Me.cboBanco.Size = New System.Drawing.Size(294, 21)
        Me.cboBanco.TabIndex = 7
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(82, 118)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(294, 21)
        Me.cboCuenta.TabIndex = 10
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(9, 96)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel9.TabIndex = 6
        Me.BeLabel9.Text = "Banco"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(9, 121)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel4.TabIndex = 9
        Me.BeLabel4.Text = "Nº Cta Cte"
        '
        'cboEmpresas
        '
        Me.cboEmpresas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEmpresas.BackColor = System.Drawing.Color.Ivory
        Me.cboEmpresas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpresas.ForeColor = System.Drawing.Color.Black
        Me.cboEmpresas.FormattingEnabled = True
        Me.cboEmpresas.KeyEnter = True
        Me.cboEmpresas.Location = New System.Drawing.Point(82, 66)
        Me.cboEmpresas.Name = "cboEmpresas"
        Me.cboEmpresas.Size = New System.Drawing.Size(294, 21)
        Me.cboEmpresas.TabIndex = 4
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(9, 69)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel1.TabIndex = 3
        Me.BeLabel1.Text = "Empresa"
        '
        'chkTodasEmpr
        '
        Me.chkTodasEmpr.AutoSize = True
        Me.chkTodasEmpr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTodasEmpr.Location = New System.Drawing.Point(382, 73)
        Me.chkTodasEmpr.Name = "chkTodasEmpr"
        Me.chkTodasEmpr.Size = New System.Drawing.Size(125, 17)
        Me.chkTodasEmpr.TabIndex = 5
        Me.chkTodasEmpr.Text = "Todas Las Empresas"
        Me.chkTodasEmpr.UseVisualStyleBackColor = True
        '
        'chkTodosBanc
        '
        Me.chkTodosBanc.AutoSize = True
        Me.chkTodosBanc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTodosBanc.Location = New System.Drawing.Point(382, 96)
        Me.chkTodosBanc.Name = "chkTodosBanc"
        Me.chkTodosBanc.Size = New System.Drawing.Size(115, 17)
        Me.chkTodosBanc.TabIndex = 8
        Me.chkTodosBanc.Text = "Todos Los Bancos"
        Me.chkTodosBanc.UseVisualStyleBackColor = True
        '
        'chkTodosCuen
        '
        Me.chkTodosCuen.AutoSize = True
        Me.chkTodosCuen.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTodosCuen.Location = New System.Drawing.Point(382, 122)
        Me.chkTodosCuen.Name = "chkTodosCuen"
        Me.chkTodosCuen.Size = New System.Drawing.Size(118, 17)
        Me.chkTodosCuen.TabIndex = 11
        Me.chkTodosCuen.Text = "Todas Las Cuentas"
        Me.chkTodosCuen.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(189, 152)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(12, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "y"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 147)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Entre"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(207, 145)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(92, 20)
        Me.dtpFechaFin.TabIndex = 15
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(82, 145)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(92, 20)
        Me.dtpFechaIni.TabIndex = 13
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'btnVerPrestamos
        '
        Me.btnVerPrestamos.Location = New System.Drawing.Point(432, 177)
        Me.btnVerPrestamos.Name = "btnVerPrestamos"
        Me.btnVerPrestamos.Size = New System.Drawing.Size(75, 23)
        Me.btnVerPrestamos.TabIndex = 16
        Me.btnVerPrestamos.Text = "Ver"
        Me.btnVerPrestamos.UseVisualStyleBackColor = True
        '
        'r1
        '
        Me.r1.AutoSize = True
        Me.r1.Checked = True
        Me.r1.Location = New System.Drawing.Point(21, 19)
        Me.r1.Name = "r1"
        Me.r1.Size = New System.Drawing.Size(122, 17)
        Me.r1.TabIndex = 0
        Me.r1.TabStop = True
        Me.r1.Text = "Abonos Por Cuentas"
        Me.r1.UseVisualStyleBackColor = True
        '
        'r2
        '
        Me.r2.AutoSize = True
        Me.r2.Location = New System.Drawing.Point(172, 19)
        Me.r2.Name = "r2"
        Me.r2.Size = New System.Drawing.Size(120, 17)
        Me.r2.TabIndex = 1
        Me.r2.TabStop = True
        Me.r2.Text = "Cheques Por Cobrar"
        Me.r2.UseVisualStyleBackColor = True
        '
        'r3
        '
        Me.r3.AutoSize = True
        Me.r3.Location = New System.Drawing.Point(321, 19)
        Me.r3.Name = "r3"
        Me.r3.Size = New System.Drawing.Size(151, 17)
        Me.r3.TabIndex = 2
        Me.r3.TabStop = True
        Me.r3.Text = "Movimientos por Identificar"
        Me.r3.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.r1)
        Me.GroupBox1.Controls.Add(Me.r3)
        Me.GroupBox1.Controls.Add(Me.r2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(495, 48)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Seleccione un Reporte"
        '
        'frmLibroBancosIngresos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(530, 212)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpFechaFin)
        Me.Controls.Add(Me.dtpFechaIni)
        Me.Controls.Add(Me.btnVerPrestamos)
        Me.Controls.Add(Me.chkTodosCuen)
        Me.Controls.Add(Me.chkTodosBanc)
        Me.Controls.Add(Me.chkTodasEmpr)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.cboEmpresas)
        Me.Controls.Add(Me.BeLabel9)
        Me.Controls.Add(Me.BeLabel4)
        Me.Controls.Add(Me.cboBanco)
        Me.Controls.Add(Me.cboCuenta)
        Me.MaximizeBox = False
        Me.Name = "frmLibroBancosIngresos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Varios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEmpresas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents chkTodasEmpr As System.Windows.Forms.CheckBox
    Friend WithEvents chkTodosBanc As System.Windows.Forms.CheckBox
    Friend WithEvents chkTodosCuen As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnVerPrestamos As System.Windows.Forms.Button
    Friend WithEvents r1 As System.Windows.Forms.RadioButton
    Friend WithEvents r2 As System.Windows.Forms.RadioButton
    Friend WithEvents r3 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
