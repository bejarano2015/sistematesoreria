<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegistroDocumentoEstado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.txtFiltrar = New ctrLibreria.Controles.BeTextBox
        Me.rdbDescr = New System.Windows.Forms.RadioButton
        Me.rdbNumDoc = New System.Windows.Forms.RadioButton
        Me.rdbRuc = New System.Windows.Forms.RadioButton
        Me.dvgListDocs = New System.Windows.Forms.DataGridView
        Me.RUC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AnaliticoDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaBase = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AbrDoc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Ndoc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ImporteTotal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cancelar = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Archivar = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.IdRegistro = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnImportar = New ctrLibreria.Controles.BeButton
        Me.stsTotales = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox5.SuspendLayout()
        CType(Me.dvgListDocs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stsTotales.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtFiltrar)
        Me.GroupBox5.Controls.Add(Me.rdbDescr)
        Me.GroupBox5.Controls.Add(Me.rdbNumDoc)
        Me.GroupBox5.Controls.Add(Me.rdbRuc)
        Me.GroupBox5.Location = New System.Drawing.Point(12, 11)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(457, 43)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Buscar Por:"
        '
        'txtFiltrar
        '
        Me.txtFiltrar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFiltrar.BackColor = System.Drawing.Color.Ivory
        Me.txtFiltrar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFiltrar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFiltrar.ForeColor = System.Drawing.Color.Black
        Me.txtFiltrar.KeyEnter = True
        Me.txtFiltrar.Location = New System.Drawing.Point(233, 17)
        Me.txtFiltrar.Name = "txtFiltrar"
        Me.txtFiltrar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFiltrar.ShortcutsEnabled = False
        Me.txtFiltrar.Size = New System.Drawing.Size(211, 20)
        Me.txtFiltrar.TabIndex = 4
        Me.txtFiltrar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'rdbDescr
        '
        Me.rdbDescr.AutoSize = True
        Me.rdbDescr.Location = New System.Drawing.Point(11, 17)
        Me.rdbDescr.Name = "rdbDescr"
        Me.rdbDescr.Size = New System.Drawing.Size(88, 17)
        Me.rdbDescr.TabIndex = 1
        Me.rdbDescr.TabStop = True
        Me.rdbDescr.Text = "Razón Social"
        Me.rdbDescr.UseVisualStyleBackColor = True
        '
        'rdbNumDoc
        '
        Me.rdbNumDoc.AutoSize = True
        Me.rdbNumDoc.Checked = True
        Me.rdbNumDoc.Location = New System.Drawing.Point(156, 18)
        Me.rdbNumDoc.Name = "rdbNumDoc"
        Me.rdbNumDoc.Size = New System.Drawing.Size(71, 17)
        Me.rdbNumDoc.TabIndex = 3
        Me.rdbNumDoc.TabStop = True
        Me.rdbNumDoc.Text = "Nro. Doc."
        Me.rdbNumDoc.UseVisualStyleBackColor = True
        '
        'rdbRuc
        '
        Me.rdbRuc.AutoSize = True
        Me.rdbRuc.Location = New System.Drawing.Point(105, 18)
        Me.rdbRuc.Name = "rdbRuc"
        Me.rdbRuc.Size = New System.Drawing.Size(45, 17)
        Me.rdbRuc.TabIndex = 2
        Me.rdbRuc.TabStop = True
        Me.rdbRuc.Text = "Ruc"
        Me.rdbRuc.UseVisualStyleBackColor = True
        '
        'dvgListDocs
        '
        Me.dvgListDocs.AllowUserToAddRows = False
        Me.dvgListDocs.BackgroundColor = System.Drawing.Color.White
        Me.dvgListDocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvgListDocs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RUC, Me.AnaliticoDescripcion, Me.FechaBase, Me.AbrDoc, Me.Ndoc, Me.ImporteTotal, Me.Cancelar, Me.Archivar, Me.IdRegistro})
        Me.dvgListDocs.Location = New System.Drawing.Point(12, 60)
        Me.dvgListDocs.Name = "dvgListDocs"
        Me.dvgListDocs.RowHeadersVisible = False
        Me.dvgListDocs.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dvgListDocs.Size = New System.Drawing.Size(683, 203)
        Me.dvgListDocs.TabIndex = 2
        '
        'RUC
        '
        Me.RUC.DataPropertyName = "RUC"
        Me.RUC.HeaderText = "Ruc"
        Me.RUC.Name = "RUC"
        Me.RUC.ReadOnly = True
        Me.RUC.Width = 90
        '
        'AnaliticoDescripcion
        '
        Me.AnaliticoDescripcion.DataPropertyName = "AnaliticoDescripcion"
        Me.AnaliticoDescripcion.HeaderText = "Razon Social"
        Me.AnaliticoDescripcion.Name = "AnaliticoDescripcion"
        Me.AnaliticoDescripcion.ReadOnly = True
        Me.AnaliticoDescripcion.Width = 150
        '
        'FechaBase
        '
        Me.FechaBase.DataPropertyName = "FechaBase"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.FechaBase.DefaultCellStyle = DataGridViewCellStyle1
        Me.FechaBase.HeaderText = "Fecha"
        Me.FechaBase.Name = "FechaBase"
        Me.FechaBase.Width = 70
        '
        'AbrDoc
        '
        Me.AbrDoc.DataPropertyName = "AbrDoc"
        Me.AbrDoc.HeaderText = "TD"
        Me.AbrDoc.Name = "AbrDoc"
        Me.AbrDoc.Width = 30
        '
        'Ndoc
        '
        Me.Ndoc.DataPropertyName = "Ndoc"
        Me.Ndoc.HeaderText = "Nro"
        Me.Ndoc.Name = "Ndoc"
        Me.Ndoc.Width = 80
        '
        'ImporteTotal
        '
        Me.ImporteTotal.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.ImporteTotal.DefaultCellStyle = DataGridViewCellStyle2
        Me.ImporteTotal.HeaderText = "Importe"
        Me.ImporteTotal.Name = "ImporteTotal"
        Me.ImporteTotal.Width = 80
        '
        'Cancelar
        '
        Me.Cancelar.HeaderText = "Cancelar"
        Me.Cancelar.Name = "Cancelar"
        Me.Cancelar.Width = 80
        '
        'Archivar
        '
        Me.Archivar.HeaderText = "Archivar"
        Me.Archivar.Name = "Archivar"
        Me.Archivar.Width = 80
        '
        'IdRegistro
        '
        Me.IdRegistro.DataPropertyName = "IdRegistro"
        Me.IdRegistro.HeaderText = "IdRegistro"
        Me.IdRegistro.Name = "IdRegistro"
        Me.IdRegistro.Visible = False
        '
        'btnImportar
        '
        Me.btnImportar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnImportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImportar.Location = New System.Drawing.Point(620, 31)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(75, 23)
        Me.btnImportar.TabIndex = 11
        Me.btnImportar.Text = "Grabar"
        Me.btnImportar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsTotales.Location = New System.Drawing.Point(0, 266)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(703, 22)
        Me.stsTotales.TabIndex = 13
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(115, 17)
        Me.ToolStripStatusLabel1.Text = "Total de Registros= 0"
        '
        'frmRegistroDocumentoEstado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(703, 288)
        Me.Controls.Add(Me.stsTotales)
        Me.Controls.Add(Me.btnImportar)
        Me.Controls.Add(Me.dvgListDocs)
        Me.Controls.Add(Me.GroupBox5)
        Me.MaximizeBox = False
        Me.Name = "frmRegistroDocumentoEstado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Estado de Comprobantes de Pago"
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.dvgListDocs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbDescr As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNumDoc As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRuc As System.Windows.Forms.RadioButton
    Friend WithEvents dvgListDocs As System.Windows.Forms.DataGridView
    Friend WithEvents btnImportar As ctrLibreria.Controles.BeButton
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtFiltrar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents RUC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AnaliticoDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaBase As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AbrDoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ndoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cancelar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Archivar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IdRegistro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
