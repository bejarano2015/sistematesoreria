<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCajaChicaExtraordinaria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCajaChicaExtraordinaria))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblNroCaja = New ctrLibreria.Controles.BeLabel()
        Me.lblCodigoEmpresa = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel27 = New ctrLibreria.Controles.BeLabel()
        Me.txtSaldoGeneral = New ctrLibreria.Controles.BeTextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dgvVales = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnCanc = New ctrLibreria.Controles.BeButton()
        Me.btnOk = New ctrLibreria.Controles.BeButton()
        Me.txtNombre = New ctrLibreria.Controles.BeTextBox()
        Me.dgvNombres = New System.Windows.Forms.DataGridView()
        Me.BeLabel26 = New ctrLibreria.Controles.BeLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.BeLabel30 = New ctrLibreria.Controles.BeLabel()
        Me.txtTotalSaldo = New ctrLibreria.Controles.BeTextBox()
        Me.btnIngresoBancario = New ctrLibreria.Controles.BeButton()
        Me.txtImporteDepositoBancario = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel31 = New ctrLibreria.Controles.BeLabel()
        Me.txtGastoGeneral = New ctrLibreria.Controles.BeTextBox()
        Me.txtAporteGeneral = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel24 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.txtMontoIniSoles = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.txtMontoFinSoles = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.txtGastos = New ctrLibreria.Controles.BeTextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblEmpresa = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.lblCodigoCajaExtraordinaria = New ctrLibreria.Controles.BeLabel()
        Me.lblEmpleado = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.lblMoneda = New ctrLibreria.Controles.BeLabel()
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.lblNumCaja = New ctrLibreria.Controles.BeLabel()
        Me.lblArea = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnImprimirArqueo = New ctrLibreria.Controles.BeButton()
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel()
        Me.txtCodigoArqueo = New ctrLibreria.Controles.BeTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel()
        Me.txtObservaciones = New ctrLibreria.Controles.BeTextBox()
        Me.txtRevisado = New ctrLibreria.Controles.BeTextBox()
        Me.txtEncargado = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel()
        Me.cboEstado = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.btnArquear = New ctrLibreria.Controles.BeButton()
        Me.txtSobrante = New ctrLibreria.Controles.BeTextBox()
        Me.txtFaltante = New ctrLibreria.Controles.BeTextBox()
        Me.txtSaldoDia = New ctrLibreria.Controles.BeTextBox()
        Me.txtAperturaDia = New ctrLibreria.Controles.BeTextBox()
        Me.txtGastosDia = New ctrLibreria.Controles.BeTextBox()
        Me.txtEfectivoDia = New ctrLibreria.Controles.BeTextBox()
        Me.btnIrArquear = New ctrLibreria.Controles.BeButton()
        Me.dgvDetalleCajas = New System.Windows.Forms.DataGridView()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ruc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Validar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.RazonSocial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoDocumento = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.DocumentoReferencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdGasto = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Glosa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OtrosD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CentroCosto = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdDetalleMovimiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Opcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Anular = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Desglozado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Empresas = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Prestamo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.grpCierre = New System.Windows.Forms.GroupBox()
        Me.chkCerrarCaja = New System.Windows.Forms.CheckBox()
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker()
        Me.dtpFechaInicio = New ctrLibreria.Controles.BeDateTimePicker()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.lblNumSesion = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvVales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvNombres, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvDetalleCajas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpCierre.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblNroCaja)
        Me.Panel1.Controls.Add(Me.lblCodigoEmpresa)
        Me.Panel1.Controls.Add(Me.BeLabel29)
        Me.Panel1.Controls.Add(Me.BeLabel28)
        Me.Panel1.Controls.Add(Me.BeLabel27)
        Me.Panel1.Controls.Add(Me.txtSaldoGeneral)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.BeLabel26)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.GroupBox5)
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.btnIrArquear)
        Me.Panel1.Controls.Add(Me.dgvDetalleCajas)
        Me.Panel1.Controls.Add(Me.BeLabel3)
        Me.Panel1.Controls.Add(Me.grpCierre)
        Me.Panel1.Controls.Add(Me.lblNumSesion)
        Me.Panel1.Controls.Add(Me.BeLabel13)
        Me.Panel1.Controls.Add(Me.ToolStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1260, 604)
        Me.Panel1.TabIndex = 171
        '
        'lblNroCaja
        '
        Me.lblNroCaja.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblNroCaja.AutoSize = True
        Me.lblNroCaja.BackColor = System.Drawing.Color.Transparent
        Me.lblNroCaja.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroCaja.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblNroCaja.Location = New System.Drawing.Point(491, 31)
        Me.lblNroCaja.Name = "lblNroCaja"
        Me.lblNroCaja.Size = New System.Drawing.Size(75, 13)
        Me.lblNroCaja.TabIndex = 172
        Me.lblNroCaja.Text = "lblNroCaja"
        '
        'lblCodigoEmpresa
        '
        Me.lblCodigoEmpresa.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCodigoEmpresa.AutoSize = True
        Me.lblCodigoEmpresa.BackColor = System.Drawing.Color.Transparent
        Me.lblCodigoEmpresa.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodigoEmpresa.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblCodigoEmpresa.Location = New System.Drawing.Point(966, 80)
        Me.lblCodigoEmpresa.Name = "lblCodigoEmpresa"
        Me.lblCodigoEmpresa.Size = New System.Drawing.Size(101, 13)
        Me.lblCodigoEmpresa.TabIndex = 35
        Me.lblCodigoEmpresa.Text = "Código Empresa"
        Me.lblCodigoEmpresa.Visible = False
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel29.ForeColor = System.Drawing.Color.Red
        Me.BeLabel29.Location = New System.Drawing.Point(963, 239)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(281, 13)
        Me.BeLabel29.TabIndex = 34
        Me.BeLabel29.Text = "[F6=Ver Detalle de Compra Por Requerimiento]"
        Me.BeLabel29.Visible = False
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel28.ForeColor = System.Drawing.Color.Red
        Me.BeLabel28.Location = New System.Drawing.Point(963, 224)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(277, 13)
        Me.BeLabel28.TabIndex = 33
        Me.BeLabel28.Text = "[F5=Detallar Comprobante con Requerimiento]"
        Me.BeLabel28.Visible = False
        '
        'BeLabel27
        '
        Me.BeLabel27.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel27.AutoSize = True
        Me.BeLabel27.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel27.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel27.ForeColor = System.Drawing.Color.Black
        Me.BeLabel27.Location = New System.Drawing.Point(474, 237)
        Me.BeLabel27.Name = "BeLabel27"
        Me.BeLabel27.Size = New System.Drawing.Size(124, 13)
        Me.BeLabel27.TabIndex = 11
        Me.BeLabel27.Text = "[F4=Imprimir Vales]"
        Me.BeLabel27.Visible = False
        '
        'txtSaldoGeneral
        '
        Me.txtSaldoGeneral.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoGeneral.BackColor = System.Drawing.Color.Ivory
        Me.txtSaldoGeneral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoGeneral.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoGeneral.Enabled = False
        Me.txtSaldoGeneral.ForeColor = System.Drawing.Color.Black
        Me.txtSaldoGeneral.KeyEnter = True
        Me.txtSaldoGeneral.Location = New System.Drawing.Point(788, 171)
        Me.txtSaldoGeneral.Name = "txtSaldoGeneral"
        Me.txtSaldoGeneral.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoGeneral.ShortcutsEnabled = False
        Me.txtSaldoGeneral.Size = New System.Drawing.Size(138, 20)
        Me.txtSaldoGeneral.TabIndex = 5
        Me.txtSaldoGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoGeneral.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        Me.txtSaldoGeneral.Visible = False
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label2)
        Me.Panel4.Controls.Add(Me.dgvVales)
        Me.Panel4.Location = New System.Drawing.Point(281, 320)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(421, 183)
        Me.Panel4.TabIndex = 32
        Me.Panel4.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(401, 1)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(15, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "X"
        '
        'dgvVales
        '
        Me.dgvVales.AllowUserToAddRows = False
        Me.dgvVales.BackgroundColor = System.Drawing.Color.White
        Me.dgvVales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVales.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column4, Me.Column6, Me.Column8})
        Me.dgvVales.Location = New System.Drawing.Point(3, 16)
        Me.dgvVales.Name = "dgvVales"
        Me.dgvVales.RowHeadersVisible = False
        Me.dgvVales.Size = New System.Drawing.Size(413, 162)
        Me.dgvVales.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.HeaderText = "Vale"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 50
        '
        'Column2
        '
        Me.Column2.HeaderText = "Responsable"
        Me.Column2.Name = "Column2"
        '
        'Column4
        '
        Me.Column4.HeaderText = "Importe"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 60
        '
        'Column6
        '
        Me.Column6.HeaderText = "Detalle Rendicion"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 150
        '
        'Column8
        '
        Me.Column8.HeaderText = "X"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 25
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.btnCanc)
        Me.Panel3.Controls.Add(Me.btnOk)
        Me.Panel3.Controls.Add(Me.txtNombre)
        Me.Panel3.Controls.Add(Me.dgvNombres)
        Me.Panel3.Location = New System.Drawing.Point(281, 321)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(421, 184)
        Me.Panel3.TabIndex = 31
        Me.Panel3.Visible = False
        '
        'btnCanc
        '
        Me.btnCanc.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnCanc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCanc.Location = New System.Drawing.Point(341, 151)
        Me.btnCanc.Name = "btnCanc"
        Me.btnCanc.Size = New System.Drawing.Size(75, 23)
        Me.btnCanc.TabIndex = 3
        Me.btnCanc.Text = "Cancelar"
        Me.btnCanc.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnCanc.UseVisualStyleBackColor = True
        '
        'btnOk
        '
        Me.btnOk.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOk.Location = New System.Drawing.Point(260, 151)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 2
        Me.btnOk.Text = "Imprimir"
        Me.btnOk.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'txtNombre
        '
        Me.txtNombre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNombre.BackColor = System.Drawing.Color.Ivory
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.ForeColor = System.Drawing.Color.Black
        Me.txtNombre.KeyEnter = True
        Me.txtNombre.Location = New System.Drawing.Point(3, 3)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNombre.ShortcutsEnabled = False
        Me.txtNombre.Size = New System.Drawing.Size(413, 20)
        Me.txtNombre.TabIndex = 1
        Me.txtNombre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'dgvNombres
        '
        Me.dgvNombres.AllowUserToAddRows = False
        Me.dgvNombres.BackgroundColor = System.Drawing.Color.White
        Me.dgvNombres.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNombres.Location = New System.Drawing.Point(3, 27)
        Me.dgvNombres.Name = "dgvNombres"
        Me.dgvNombres.RowHeadersVisible = False
        Me.dgvNombres.Size = New System.Drawing.Size(413, 118)
        Me.dgvNombres.TabIndex = 0
        '
        'BeLabel26
        '
        Me.BeLabel26.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel26.AutoSize = True
        Me.BeLabel26.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel26.ForeColor = System.Drawing.Color.White
        Me.BeLabel26.Location = New System.Drawing.Point(627, 173)
        Me.BeLabel26.Name = "BeLabel26"
        Me.BeLabel26.Size = New System.Drawing.Size(91, 13)
        Me.BeLabel26.TabIndex = 2
        Me.BeLabel26.Text = "Saldo detalle caja"
        Me.BeLabel26.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.ErrorImage = CType(resources.GetObject("PictureBox1.ErrorImage"), System.Drawing.Image)
        Me.PictureBox1.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Print_
        Me.PictureBox1.InitialImage = CType(resources.GetObject("PictureBox1.InitialImage"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(604, 234)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(27, 19)
        Me.PictureBox1.TabIndex = 30
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.BeLabel30)
        Me.GroupBox5.Controls.Add(Me.txtTotalSaldo)
        Me.GroupBox5.Controls.Add(Me.btnIngresoBancario)
        Me.GroupBox5.Controls.Add(Me.txtImporteDepositoBancario)
        Me.GroupBox5.Controls.Add(Me.BeLabel31)
        Me.GroupBox5.Controls.Add(Me.txtGastoGeneral)
        Me.GroupBox5.Controls.Add(Me.txtAporteGeneral)
        Me.GroupBox5.Controls.Add(Me.BeLabel25)
        Me.GroupBox5.Controls.Add(Me.BeLabel24)
        Me.GroupBox5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(615, 43)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(345, 121)
        Me.GroupBox5.TabIndex = 29
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Total General"
        '
        'BeLabel30
        '
        Me.BeLabel30.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel30.AutoSize = True
        Me.BeLabel30.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel30.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel30.ForeColor = System.Drawing.Color.Black
        Me.BeLabel30.Location = New System.Drawing.Point(12, 87)
        Me.BeLabel30.Name = "BeLabel30"
        Me.BeLabel30.Size = New System.Drawing.Size(141, 13)
        Me.BeLabel30.TabIndex = 41
        Me.BeLabel30.Text = "Saldo Actual de Caja"
        '
        'txtTotalSaldo
        '
        Me.txtTotalSaldo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotalSaldo.BackColor = System.Drawing.Color.Ivory
        Me.txtTotalSaldo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotalSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalSaldo.Enabled = False
        Me.txtTotalSaldo.ForeColor = System.Drawing.Color.Black
        Me.txtTotalSaldo.KeyEnter = True
        Me.txtTotalSaldo.Location = New System.Drawing.Point(173, 85)
        Me.txtTotalSaldo.Name = "txtTotalSaldo"
        Me.txtTotalSaldo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotalSaldo.ShortcutsEnabled = False
        Me.txtTotalSaldo.Size = New System.Drawing.Size(138, 21)
        Me.txtTotalSaldo.TabIndex = 40
        Me.txtTotalSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalSaldo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'btnIngresoBancario
        '
        Me.btnIngresoBancario.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnIngresoBancario.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIngresoBancario.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Edit_
        Me.btnIngresoBancario.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnIngresoBancario.Location = New System.Drawing.Point(313, 61)
        Me.btnIngresoBancario.Name = "btnIngresoBancario"
        Me.btnIngresoBancario.Size = New System.Drawing.Size(26, 22)
        Me.btnIngresoBancario.TabIndex = 36
        Me.btnIngresoBancario.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnIngresoBancario.UseVisualStyleBackColor = True
        '
        'txtImporteDepositoBancario
        '
        Me.txtImporteDepositoBancario.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporteDepositoBancario.BackColor = System.Drawing.Color.Ivory
        Me.txtImporteDepositoBancario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteDepositoBancario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteDepositoBancario.Enabled = False
        Me.txtImporteDepositoBancario.ForeColor = System.Drawing.Color.Black
        Me.txtImporteDepositoBancario.KeyEnter = True
        Me.txtImporteDepositoBancario.Location = New System.Drawing.Point(173, 61)
        Me.txtImporteDepositoBancario.Name = "txtImporteDepositoBancario"
        Me.txtImporteDepositoBancario.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporteDepositoBancario.ShortcutsEnabled = False
        Me.txtImporteDepositoBancario.Size = New System.Drawing.Size(138, 21)
        Me.txtImporteDepositoBancario.TabIndex = 38
        Me.txtImporteDepositoBancario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteDepositoBancario.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel31
        '
        Me.BeLabel31.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel31.AutoSize = True
        Me.BeLabel31.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel31.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel31.ForeColor = System.Drawing.Color.Black
        Me.BeLabel31.Location = New System.Drawing.Point(12, 65)
        Me.BeLabel31.Name = "BeLabel31"
        Me.BeLabel31.Size = New System.Drawing.Size(129, 13)
        Me.BeLabel31.TabIndex = 39
        Me.BeLabel31.Text = "Monto Reintegrado"
        '
        'txtGastoGeneral
        '
        Me.txtGastoGeneral.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtGastoGeneral.BackColor = System.Drawing.Color.Ivory
        Me.txtGastoGeneral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGastoGeneral.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGastoGeneral.Enabled = False
        Me.txtGastoGeneral.ForeColor = System.Drawing.Color.Black
        Me.txtGastoGeneral.KeyEnter = True
        Me.txtGastoGeneral.Location = New System.Drawing.Point(173, 37)
        Me.txtGastoGeneral.Name = "txtGastoGeneral"
        Me.txtGastoGeneral.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtGastoGeneral.ShortcutsEnabled = False
        Me.txtGastoGeneral.Size = New System.Drawing.Size(138, 21)
        Me.txtGastoGeneral.TabIndex = 4
        Me.txtGastoGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtGastoGeneral.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtAporteGeneral
        '
        Me.txtAporteGeneral.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAporteGeneral.BackColor = System.Drawing.Color.Ivory
        Me.txtAporteGeneral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAporteGeneral.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAporteGeneral.Enabled = False
        Me.txtAporteGeneral.ForeColor = System.Drawing.Color.Black
        Me.txtAporteGeneral.KeyEnter = True
        Me.txtAporteGeneral.Location = New System.Drawing.Point(173, 13)
        Me.txtAporteGeneral.Name = "txtAporteGeneral"
        Me.txtAporteGeneral.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAporteGeneral.ShortcutsEnabled = False
        Me.txtAporteGeneral.Size = New System.Drawing.Size(138, 21)
        Me.txtAporteGeneral.TabIndex = 3
        Me.txtAporteGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAporteGeneral.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.ForeColor = System.Drawing.Color.White
        Me.BeLabel25.Location = New System.Drawing.Point(12, 39)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(138, 13)
        Me.BeLabel25.TabIndex = 1
        Me.BeLabel25.Text = "Gasto General de Caja"
        '
        'BeLabel24
        '
        Me.BeLabel24.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel24.AutoSize = True
        Me.BeLabel24.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel24.ForeColor = System.Drawing.Color.White
        Me.BeLabel24.Location = New System.Drawing.Point(12, 17)
        Me.BeLabel24.Name = "BeLabel24"
        Me.BeLabel24.Size = New System.Drawing.Size(143, 13)
        Me.BeLabel24.TabIndex = 0
        Me.BeLabel24.Text = "Aporte General de Caja"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.BeLabel6)
        Me.GroupBox4.Controls.Add(Me.txtMontoIniSoles)
        Me.GroupBox4.Controls.Add(Me.BeLabel10)
        Me.GroupBox4.Controls.Add(Me.txtMontoFinSoles)
        Me.GroupBox4.Controls.Add(Me.BeLabel9)
        Me.GroupBox4.Controls.Add(Me.txtGastos)
        Me.GroupBox4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(11, 132)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(291, 91)
        Me.GroupBox4.TabIndex = 28
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Total Diario"
        Me.GroupBox4.Visible = False
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(6, 19)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(95, 13)
        Me.BeLabel6.TabIndex = 4
        Me.BeLabel6.Text = "Aportes del Día"
        '
        'txtMontoIniSoles
        '
        Me.txtMontoIniSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMontoIniSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtMontoIniSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoIniSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoIniSoles.Enabled = False
        Me.txtMontoIniSoles.ForeColor = System.Drawing.Color.DarkBlue
        Me.txtMontoIniSoles.KeyEnter = True
        Me.txtMontoIniSoles.Location = New System.Drawing.Point(149, 14)
        Me.txtMontoIniSoles.MaxLength = 20
        Me.txtMontoIniSoles.Name = "txtMontoIniSoles"
        Me.txtMontoIniSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMontoIniSoles.ShortcutsEnabled = False
        Me.txtMontoIniSoles.Size = New System.Drawing.Size(133, 21)
        Me.txtMontoIniSoles.TabIndex = 9
        Me.txtMontoIniSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMontoIniSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(6, 67)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(135, 13)
        Me.BeLabel10.TabIndex = 20
        Me.BeLabel10.Text = "Saldo a Rendir del Día"
        '
        'txtMontoFinSoles
        '
        Me.txtMontoFinSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMontoFinSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtMontoFinSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoFinSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoFinSoles.Enabled = False
        Me.txtMontoFinSoles.ForeColor = System.Drawing.Color.DarkBlue
        Me.txtMontoFinSoles.KeyEnter = True
        Me.txtMontoFinSoles.Location = New System.Drawing.Point(149, 64)
        Me.txtMontoFinSoles.MaxLength = 20
        Me.txtMontoFinSoles.Name = "txtMontoFinSoles"
        Me.txtMontoFinSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMontoFinSoles.ShortcutsEnabled = False
        Me.txtMontoFinSoles.Size = New System.Drawing.Size(133, 21)
        Me.txtMontoFinSoles.TabIndex = 22
        Me.txtMontoFinSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMontoFinSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(6, 44)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(90, 13)
        Me.BeLabel9.TabIndex = 15
        Me.BeLabel9.Text = "Gastos del Día"
        '
        'txtGastos
        '
        Me.txtGastos.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtGastos.BackColor = System.Drawing.Color.Ivory
        Me.txtGastos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGastos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGastos.Enabled = False
        Me.txtGastos.ForeColor = System.Drawing.Color.DarkBlue
        Me.txtGastos.KeyEnter = True
        Me.txtGastos.Location = New System.Drawing.Point(149, 39)
        Me.txtGastos.MaxLength = 20
        Me.txtGastos.Name = "txtGastos"
        Me.txtGastos.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtGastos.ShortcutsEnabled = False
        Me.txtGastos.Size = New System.Drawing.Size(133, 21)
        Me.txtGastos.TabIndex = 18
        Me.txtGastos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtGastos.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblEmpresa)
        Me.GroupBox3.Controls.Add(Me.BeLabel15)
        Me.GroupBox3.Controls.Add(Me.lblCodigoCajaExtraordinaria)
        Me.GroupBox3.Controls.Add(Me.lblEmpleado)
        Me.GroupBox3.Controls.Add(Me.BeLabel5)
        Me.GroupBox3.Controls.Add(Me.lblMoneda)
        Me.GroupBox3.Controls.Add(Me.txtCodigo)
        Me.GroupBox3.Controls.Add(Me.BeLabel1)
        Me.GroupBox3.Controls.Add(Me.BeLabel2)
        Me.GroupBox3.Controls.Add(Me.lblNumCaja)
        Me.GroupBox3.Controls.Add(Me.lblArea)
        Me.GroupBox3.Controls.Add(Me.BeLabel4)
        Me.GroupBox3.Controls.Add(Me.BeLabel12)
        Me.GroupBox3.Location = New System.Drawing.Point(11, 43)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(579, 88)
        Me.GroupBox3.TabIndex = 27
        Me.GroupBox3.TabStop = False
        '
        'lblEmpresa
        '
        Me.lblEmpresa.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblEmpresa.AutoSize = True
        Me.lblEmpresa.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpresa.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpresa.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblEmpresa.Location = New System.Drawing.Point(71, 39)
        Me.lblEmpresa.Name = "lblEmpresa"
        Me.lblEmpresa.Size = New System.Drawing.Size(70, 13)
        Me.lblEmpresa.TabIndex = 6
        Me.lblEmpresa.Text = "lblEmpresa"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(6, 39)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel15.TabIndex = 1
        Me.BeLabel15.Text = "Empresa"
        '
        'lblCodigoCajaExtraordinaria
        '
        Me.lblCodigoCajaExtraordinaria.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCodigoCajaExtraordinaria.AutoSize = True
        Me.lblCodigoCajaExtraordinaria.BackColor = System.Drawing.Color.LemonChiffon
        Me.lblCodigoCajaExtraordinaria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCodigoCajaExtraordinaria.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodigoCajaExtraordinaria.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblCodigoCajaExtraordinaria.Location = New System.Drawing.Point(74, 16)
        Me.lblCodigoCajaExtraordinaria.Name = "lblCodigoCajaExtraordinaria"
        Me.lblCodigoCajaExtraordinaria.Size = New System.Drawing.Size(72, 15)
        Me.lblCodigoCajaExtraordinaria.TabIndex = 22
        Me.lblCodigoCajaExtraordinaria.Text = "lblCodCaja"
        '
        'lblEmpleado
        '
        Me.lblEmpleado.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblEmpleado.AutoSize = True
        Me.lblEmpleado.BackColor = System.Drawing.Color.Transparent
        Me.lblEmpleado.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpleado.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblEmpleado.Location = New System.Drawing.Point(71, 60)
        Me.lblEmpleado.Name = "lblEmpleado"
        Me.lblEmpleado.Size = New System.Drawing.Size(76, 13)
        Me.lblEmpleado.TabIndex = 7
        Me.lblEmpleado.Text = "lblEmpleado"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(6, 61)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel5.TabIndex = 2
        Me.BeLabel5.Text = "Empleado"
        '
        'lblMoneda
        '
        Me.lblMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblMoneda.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoneda.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblMoneda.Location = New System.Drawing.Point(367, 60)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(64, 13)
        Me.lblMoneda.TabIndex = 21
        Me.lblMoneda.Text = "lblMoneda"
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(212, 16)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(99, 20)
        Me.txtCodigo.TabIndex = 8
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCodigo.Visible = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(6, 18)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel1.TabIndex = 3
        Me.BeLabel1.Text = "Código"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(317, 60)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel2.TabIndex = 19
        Me.BeLabel2.Text = "Moneda"
        '
        'lblNumCaja
        '
        Me.lblNumCaja.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblNumCaja.AutoSize = True
        Me.lblNumCaja.BackColor = System.Drawing.Color.Transparent
        Me.lblNumCaja.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumCaja.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblNumCaja.Location = New System.Drawing.Point(367, 18)
        Me.lblNumCaja.Name = "lblNumCaja"
        Me.lblNumCaja.Size = New System.Drawing.Size(75, 13)
        Me.lblNumCaja.TabIndex = 16
        Me.lblNumCaja.Text = "lblDescCaja"
        '
        'lblArea
        '
        Me.lblArea.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblArea.AutoSize = True
        Me.lblArea.BackColor = System.Drawing.Color.Transparent
        Me.lblArea.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArea.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblArea.Location = New System.Drawing.Point(367, 39)
        Me.lblArea.Name = "lblArea"
        Me.lblArea.Size = New System.Drawing.Size(47, 13)
        Me.lblArea.TabIndex = 17
        Me.lblArea.Text = "lblArea"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(317, 39)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel4.TabIndex = 14
        Me.BeLabel4.Text = "Area"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(317, 19)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel12.TabIndex = 13
        Me.BeLabel12.Text = "Caja"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Location = New System.Drawing.Point(663, 266)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(326, 308)
        Me.Panel2.TabIndex = 0
        Me.Panel2.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GroupBox2.Controls.Add(Me.btnImprimirArqueo)
        Me.GroupBox2.Controls.Add(Me.BeLabel23)
        Me.GroupBox2.Controls.Add(Me.txtCodigoArqueo)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.BeLabel19)
        Me.GroupBox2.Controls.Add(Me.txtObservaciones)
        Me.GroupBox2.Controls.Add(Me.txtRevisado)
        Me.GroupBox2.Controls.Add(Me.txtEncargado)
        Me.GroupBox2.Controls.Add(Me.BeLabel20)
        Me.GroupBox2.Controls.Add(Me.BeLabel21)
        Me.GroupBox2.Controls.Add(Me.cboEstado)
        Me.GroupBox2.Controls.Add(Me.BeLabel22)
        Me.GroupBox2.Controls.Add(Me.BeLabel18)
        Me.GroupBox2.Controls.Add(Me.BeLabel17)
        Me.GroupBox2.Controls.Add(Me.BeLabel16)
        Me.GroupBox2.Controls.Add(Me.BeLabel14)
        Me.GroupBox2.Controls.Add(Me.BeLabel11)
        Me.GroupBox2.Controls.Add(Me.BeLabel7)
        Me.GroupBox2.Controls.Add(Me.btnArquear)
        Me.GroupBox2.Controls.Add(Me.txtSobrante)
        Me.GroupBox2.Controls.Add(Me.txtFaltante)
        Me.GroupBox2.Controls.Add(Me.txtSaldoDia)
        Me.GroupBox2.Controls.Add(Me.txtAperturaDia)
        Me.GroupBox2.Controls.Add(Me.txtGastosDia)
        Me.GroupBox2.Controls.Add(Me.txtEfectivoDia)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(320, 300)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Arqueo del Día"
        '
        'btnImprimirArqueo
        '
        Me.btnImprimirArqueo.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnImprimirArqueo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimirArqueo.Location = New System.Drawing.Point(177, 275)
        Me.btnImprimirArqueo.Name = "btnImprimirArqueo"
        Me.btnImprimirArqueo.Size = New System.Drawing.Size(130, 23)
        Me.btnImprimirArqueo.TabIndex = 24
        Me.btnImprimirArqueo.Text = "Imprimir"
        Me.btnImprimirArqueo.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnImprimirArqueo.UseVisualStyleBackColor = True
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(12, 23)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(66, 13)
        Me.BeLabel23.TabIndex = 0
        Me.BeLabel23.Text = "Nº Arqueo"
        '
        'txtCodigoArqueo
        '
        Me.txtCodigoArqueo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigoArqueo.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCodigoArqueo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoArqueo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoArqueo.Enabled = False
        Me.txtCodigoArqueo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoArqueo.KeyEnter = True
        Me.txtCodigoArqueo.Location = New System.Drawing.Point(84, 16)
        Me.txtCodigoArqueo.Name = "txtCodigoArqueo"
        Me.txtCodigoArqueo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigoArqueo.ShortcutsEnabled = False
        Me.txtCodigoArqueo.Size = New System.Drawing.Size(145, 21)
        Me.txtCodigoArqueo.TabIndex = 12
        Me.txtCodigoArqueo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCodigoArqueo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(306, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(15, 13)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "X"
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel19.ForeColor = System.Drawing.Color.Black
        Me.BeLabel19.Location = New System.Drawing.Point(12, 227)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel19.TabIndex = 9
        Me.BeLabel19.Text = "Observación"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtObservaciones.BackColor = System.Drawing.Color.Ivory
        Me.txtObservaciones.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservaciones.ForeColor = System.Drawing.Color.Black
        Me.txtObservaciones.KeyEnter = True
        Me.txtObservaciones.Location = New System.Drawing.Point(102, 224)
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtObservaciones.ShortcutsEnabled = False
        Me.txtObservaciones.Size = New System.Drawing.Size(205, 21)
        Me.txtObservaciones.TabIndex = 21
        Me.txtObservaciones.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        '
        'txtRevisado
        '
        Me.txtRevisado.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRevisado.BackColor = System.Drawing.Color.Ivory
        Me.txtRevisado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRevisado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRevisado.ForeColor = System.Drawing.Color.Black
        Me.txtRevisado.KeyEnter = True
        Me.txtRevisado.Location = New System.Drawing.Point(102, 196)
        Me.txtRevisado.Name = "txtRevisado"
        Me.txtRevisado.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRevisado.ShortcutsEnabled = False
        Me.txtRevisado.Size = New System.Drawing.Size(205, 21)
        Me.txtRevisado.TabIndex = 20
        Me.txtRevisado.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        '
        'txtEncargado
        '
        Me.txtEncargado.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtEncargado.BackColor = System.Drawing.Color.Ivory
        Me.txtEncargado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEncargado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEncargado.ForeColor = System.Drawing.Color.Black
        Me.txtEncargado.KeyEnter = True
        Me.txtEncargado.Location = New System.Drawing.Point(102, 168)
        Me.txtEncargado.Name = "txtEncargado"
        Me.txtEncargado.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtEncargado.ShortcutsEnabled = False
        Me.txtEncargado.Size = New System.Drawing.Size(205, 21)
        Me.txtEncargado.TabIndex = 19
        Me.txtEncargado.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel20.ForeColor = System.Drawing.Color.Black
        Me.BeLabel20.Location = New System.Drawing.Point(12, 199)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel20.TabIndex = 8
        Me.BeLabel20.Text = "Revisado Por"
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel21.ForeColor = System.Drawing.Color.Black
        Me.BeLabel21.Location = New System.Drawing.Point(12, 171)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(90, 13)
        Me.BeLabel21.TabIndex = 7
        Me.BeLabel21.Text = "Encargado Por"
        '
        'cboEstado
        '
        Me.cboEstado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstado.BackColor = System.Drawing.Color.Ivory
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.Enabled = False
        Me.cboEstado.ForeColor = System.Drawing.Color.Black
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.KeyEnter = True
        Me.cboEstado.Location = New System.Drawing.Point(102, 250)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(205, 21)
        Me.cboEstado.TabIndex = 22
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(12, 253)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel22.TabIndex = 10
        Me.BeLabel22.Text = "Estado"
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(161, 122)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(103, 13)
        Me.BeLabel18.TabIndex = 6
        Me.BeLabel18.Text = "Sobrante del Día"
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(12, 122)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(96, 13)
        Me.BeLabel17.TabIndex = 5
        Me.BeLabel17.Text = "Faltante del Día"
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(161, 80)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(109, 13)
        Me.BeLabel16.TabIndex = 4
        Me.BeLabel16.Text = "Saldo Caja Actual"
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(12, 80)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(90, 13)
        Me.BeLabel14.TabIndex = 3
        Me.BeLabel14.Text = "Gastos del Día"
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(161, 41)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(101, 13)
        Me.BeLabel11.TabIndex = 2
        Me.BeLabel11.Text = "Apertura del Día"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(12, 41)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(122, 13)
        Me.BeLabel7.TabIndex = 1
        Me.BeLabel7.Text = "Efectivo Caja Actual"
        '
        'btnArquear
        '
        Me.btnArquear.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnArquear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnArquear.Location = New System.Drawing.Point(19, 275)
        Me.btnArquear.Name = "btnArquear"
        Me.btnArquear.Size = New System.Drawing.Size(125, 23)
        Me.btnArquear.TabIndex = 23
        Me.btnArquear.Text = "Arquear"
        Me.btnArquear.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnArquear.UseVisualStyleBackColor = True
        '
        'txtSobrante
        '
        Me.txtSobrante.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSobrante.BackColor = System.Drawing.Color.Ivory
        Me.txtSobrante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSobrante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSobrante.Enabled = False
        Me.txtSobrante.ForeColor = System.Drawing.Color.Black
        Me.txtSobrante.KeyEnter = True
        Me.txtSobrante.Location = New System.Drawing.Point(164, 139)
        Me.txtSobrante.Name = "txtSobrante"
        Me.txtSobrante.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSobrante.ShortcutsEnabled = False
        Me.txtSobrante.Size = New System.Drawing.Size(143, 21)
        Me.txtSobrante.TabIndex = 18
        Me.txtSobrante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSobrante.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtFaltante
        '
        Me.txtFaltante.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFaltante.BackColor = System.Drawing.Color.Ivory
        Me.txtFaltante.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFaltante.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFaltante.Enabled = False
        Me.txtFaltante.ForeColor = System.Drawing.Color.Black
        Me.txtFaltante.KeyEnter = True
        Me.txtFaltante.Location = New System.Drawing.Point(15, 139)
        Me.txtFaltante.Name = "txtFaltante"
        Me.txtFaltante.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFaltante.ShortcutsEnabled = False
        Me.txtFaltante.Size = New System.Drawing.Size(143, 21)
        Me.txtFaltante.TabIndex = 15
        Me.txtFaltante.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFaltante.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtSaldoDia
        '
        Me.txtSaldoDia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoDia.BackColor = System.Drawing.Color.Ivory
        Me.txtSaldoDia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoDia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoDia.Enabled = False
        Me.txtSaldoDia.ForeColor = System.Drawing.Color.Black
        Me.txtSaldoDia.KeyEnter = True
        Me.txtSaldoDia.Location = New System.Drawing.Point(164, 96)
        Me.txtSaldoDia.Name = "txtSaldoDia"
        Me.txtSaldoDia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoDia.ShortcutsEnabled = False
        Me.txtSaldoDia.Size = New System.Drawing.Size(143, 21)
        Me.txtSaldoDia.TabIndex = 17
        Me.txtSaldoDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoDia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtAperturaDia
        '
        Me.txtAperturaDia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAperturaDia.BackColor = System.Drawing.Color.Ivory
        Me.txtAperturaDia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAperturaDia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAperturaDia.Enabled = False
        Me.txtAperturaDia.ForeColor = System.Drawing.Color.Black
        Me.txtAperturaDia.KeyEnter = True
        Me.txtAperturaDia.Location = New System.Drawing.Point(164, 57)
        Me.txtAperturaDia.Name = "txtAperturaDia"
        Me.txtAperturaDia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAperturaDia.ShortcutsEnabled = False
        Me.txtAperturaDia.Size = New System.Drawing.Size(143, 21)
        Me.txtAperturaDia.TabIndex = 16
        Me.txtAperturaDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAperturaDia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtGastosDia
        '
        Me.txtGastosDia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtGastosDia.BackColor = System.Drawing.Color.Ivory
        Me.txtGastosDia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGastosDia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGastosDia.Enabled = False
        Me.txtGastosDia.ForeColor = System.Drawing.Color.Black
        Me.txtGastosDia.KeyEnter = True
        Me.txtGastosDia.Location = New System.Drawing.Point(15, 95)
        Me.txtGastosDia.Name = "txtGastosDia"
        Me.txtGastosDia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtGastosDia.ShortcutsEnabled = False
        Me.txtGastosDia.Size = New System.Drawing.Size(143, 21)
        Me.txtGastosDia.TabIndex = 14
        Me.txtGastosDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtGastosDia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtEfectivoDia
        '
        Me.txtEfectivoDia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtEfectivoDia.BackColor = System.Drawing.Color.Ivory
        Me.txtEfectivoDia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEfectivoDia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEfectivoDia.ForeColor = System.Drawing.Color.Black
        Me.txtEfectivoDia.KeyEnter = True
        Me.txtEfectivoDia.Location = New System.Drawing.Point(15, 57)
        Me.txtEfectivoDia.Name = "txtEfectivoDia"
        Me.txtEfectivoDia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtEfectivoDia.ShortcutsEnabled = False
        Me.txtEfectivoDia.Size = New System.Drawing.Size(143, 21)
        Me.txtEfectivoDia.TabIndex = 13
        Me.txtEfectivoDia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEfectivoDia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'btnIrArquear
        '
        Me.btnIrArquear.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnIrArquear.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIrArquear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnIrArquear.Location = New System.Drawing.Point(966, 182)
        Me.btnIrArquear.Name = "btnIrArquear"
        Me.btnIrArquear.Size = New System.Drawing.Size(145, 22)
        Me.btnIrArquear.TabIndex = 26
        Me.btnIrArquear.Text = "Arquear Caja Chica"
        Me.btnIrArquear.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnIrArquear.UseVisualStyleBackColor = True
        Me.btnIrArquear.Visible = False
        '
        'dgvDetalleCajas
        '
        Me.dgvDetalleCajas.AllowUserToAddRows = False
        Me.dgvDetalleCajas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetalleCajas.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalleCajas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleCajas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Fecha, Me.Ruc, Me.Validar, Me.RazonSocial, Me.TipoDocumento, Me.DocumentoReferencia, Me.IdGasto, Me.Glosa, Me.OtrosD, Me.CentroCosto, Me.Total, Me.IdDetalleMovimiento, Me.Opcion, Me.Anular, Me.Desglozado, Me.Empresas, Me.Prestamo})
        Me.dgvDetalleCajas.Location = New System.Drawing.Point(0, 253)
        Me.dgvDetalleCajas.Name = "dgvDetalleCajas"
        Me.dgvDetalleCajas.RowHeadersVisible = False
        Me.dgvDetalleCajas.Size = New System.Drawing.Size(1259, 346)
        Me.dgvDetalleCajas.TabIndex = 25
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "FechaDetMov"
        Me.Fecha.Frozen = True
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Width = 70
        '
        'Ruc
        '
        Me.Ruc.Frozen = True
        Me.Ruc.HeaderText = "Ruc"
        Me.Ruc.MaxInputLength = 11
        Me.Ruc.Name = "Ruc"
        Me.Ruc.Width = 75
        '
        'Validar
        '
        Me.Validar.HeaderText = "Validar"
        Me.Validar.MinimumWidth = 10
        Me.Validar.Name = "Validar"
        Me.Validar.ReadOnly = True
        Me.Validar.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Validar.Text = "Validar"
        Me.Validar.UseColumnTextForButtonValue = True
        Me.Validar.Width = 50
        '
        'RazonSocial
        '
        Me.RazonSocial.HeaderText = "Razón Social"
        Me.RazonSocial.Name = "RazonSocial"
        Me.RazonSocial.Width = 120
        '
        'TipoDocumento
        '
        Me.TipoDocumento.DataPropertyName = "IdDocumento"
        Me.TipoDocumento.HeaderText = "Tipo Documento"
        Me.TipoDocumento.Name = "TipoDocumento"
        Me.TipoDocumento.Width = 92
        '
        'DocumentoReferencia
        '
        Me.DocumentoReferencia.DataPropertyName = "DocumentoReferencia"
        Me.DocumentoReferencia.HeaderText = "Número"
        Me.DocumentoReferencia.Name = "DocumentoReferencia"
        Me.DocumentoReferencia.Width = 70
        '
        'IdGasto
        '
        Me.IdGasto.DataPropertyName = "TipoGasto"
        Me.IdGasto.HeaderText = "Tipo Gasto"
        Me.IdGasto.Name = "IdGasto"
        Me.IdGasto.Width = 250
        '
        'Glosa
        '
        Me.Glosa.DataPropertyName = "Descripcion"
        Me.Glosa.HeaderText = "Concepto"
        Me.Glosa.Name = "Glosa"
        Me.Glosa.Width = 150
        '
        'OtrosD
        '
        Me.OtrosD.HeaderText = "Otros Datos"
        Me.OtrosD.Name = "OtrosD"
        '
        'CentroCosto
        '
        Me.CentroCosto.DataPropertyName = "CCosCodigo"
        Me.CentroCosto.HeaderText = "Centro de Costo"
        Me.CentroCosto.Name = "CentroCosto"
        Me.CentroCosto.Width = 170
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        '
        'IdDetalleMovimiento
        '
        Me.IdDetalleMovimiento.DataPropertyName = "IdDetalleMovimiento"
        Me.IdDetalleMovimiento.HeaderText = "IdDetalleMovimiento"
        Me.IdDetalleMovimiento.Name = "IdDetalleMovimiento"
        Me.IdDetalleMovimiento.Visible = False
        Me.IdDetalleMovimiento.Width = 35
        '
        'Opcion
        '
        Me.Opcion.HeaderText = "Opción"
        Me.Opcion.Name = "Opcion"
        Me.Opcion.Visible = False
        Me.Opcion.Width = 35
        '
        'Anular
        '
        Me.Anular.HeaderText = "Rendir"
        Me.Anular.Name = "Anular"
        Me.Anular.Visible = False
        Me.Anular.Width = 40
        '
        'Desglozado
        '
        Me.Desglozado.HeaderText = "Vale"
        Me.Desglozado.Name = "Desglozado"
        Me.Desglozado.Visible = False
        Me.Desglozado.Width = 40
        '
        'Empresas
        '
        Me.Empresas.HeaderText = "Empresa a Prestar"
        Me.Empresas.Name = "Empresas"
        Me.Empresas.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Empresas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Empresas.Visible = False
        Me.Empresas.Width = 120
        '
        'Prestamo
        '
        Me.Prestamo.HeaderText = "Prestamo"
        Me.Prestamo.Name = "Prestamo"
        Me.Prestamo.Visible = False
        Me.Prestamo.Width = 40
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.DarkBlue
        Me.BeLabel3.Location = New System.Drawing.Point(3, 238)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(238, 13)
        Me.BeLabel3.TabIndex = 24
        Me.BeLabel3.Text = "DETALLE DE CAJA EXTRAORDINARIA"
        '
        'grpCierre
        '
        Me.grpCierre.Controls.Add(Me.chkCerrarCaja)
        Me.grpCierre.Controls.Add(Me.dtpFechaFin)
        Me.grpCierre.Controls.Add(Me.dtpFechaInicio)
        Me.grpCierre.Controls.Add(Me.BeLabel8)
        Me.grpCierre.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpCierre.Location = New System.Drawing.Point(308, 151)
        Me.grpCierre.Name = "grpCierre"
        Me.grpCierre.Size = New System.Drawing.Size(282, 72)
        Me.grpCierre.TabIndex = 23
        Me.grpCierre.TabStop = False
        Me.grpCierre.Text = "Cierre"
        '
        'chkCerrarCaja
        '
        Me.chkCerrarCaja.AutoSize = True
        Me.chkCerrarCaja.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCerrarCaja.ForeColor = System.Drawing.Color.Black
        Me.chkCerrarCaja.Location = New System.Drawing.Point(10, 47)
        Me.chkCerrarCaja.Name = "chkCerrarCaja"
        Me.chkCerrarCaja.Size = New System.Drawing.Size(95, 17)
        Me.chkCerrarCaja.TabIndex = 0
        Me.chkCerrarCaja.Text = "Cerrar Caja"
        Me.chkCerrarCaja.UseVisualStyleBackColor = True
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Enabled = False
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(141, 44)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(131, 21)
        Me.dtpFechaFin.TabIndex = 1
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaInicio.CustomFormat = ""
        Me.dtpFechaInicio.Enabled = False
        Me.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicio.KeyEnter = True
        Me.dtpFechaInicio.Location = New System.Drawing.Point(141, 15)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(131, 21)
        Me.dtpFechaInicio.TabIndex = 10
        Me.dtpFechaInicio.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaInicio.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(7, 22)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(112, 13)
        Me.BeLabel8.TabIndex = 5
        Me.BeLabel8.Text = "Fecha de Apertura"
        '
        'lblNumSesion
        '
        Me.lblNumSesion.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblNumSesion.AutoSize = True
        Me.lblNumSesion.BackColor = System.Drawing.Color.Transparent
        Me.lblNumSesion.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumSesion.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblNumSesion.Location = New System.Drawing.Point(187, 31)
        Me.lblNumSesion.Name = "lblNumSesion"
        Me.lblNumSesion.Size = New System.Drawing.Size(95, 13)
        Me.lblNumSesion.TabIndex = 12
        Me.lblNumSesion.Text = "lblNumSesion"
        Me.lblNumSesion.Visible = False
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.DarkBlue
        Me.BeLabel13.Location = New System.Drawing.Point(288, 30)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(197, 14)
        Me.BeLabel13.TabIndex = 11
        Me.BeLabel13.Text = "CAJA EXTRAORDINARIA Nº :"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnModificar, Me.btnImprimir, Me.btnSalir})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1258, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnModificar
        '
        Me.btnModificar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(23, 22)
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.ToolTipText = "Editar"
        '
        'btnImprimir
        '
        Me.btnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimir.Text = "ToolStripButton1"
        Me.btnImprimir.ToolTipText = "Imprimir Caja"
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'frmCajaChicaExtraordinaria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1260, 604)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmCajaChicaExtraordinaria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Sesión de Caja Chica Extraordinaria"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvVales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvNombres, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvDetalleCajas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpCierre.ResumeLayout(False)
        Me.grpCierre.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblArea As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents txtGastos As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblEmpleado As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtMontoFinSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblEmpresa As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblNumCaja As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblNumSesion As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaInicio As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents txtMontoIniSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents chkCerrarCaja As System.Windows.Forms.CheckBox
    Friend WithEvents dgvDetalleCajas As System.Windows.Forms.DataGridView
    Friend WithEvents grpCierre As System.Windows.Forms.GroupBox
    Friend WithEvents lblMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnIrArquear As ctrLibreria.Controles.BeButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnArquear As ctrLibreria.Controles.BeButton
    Friend WithEvents txtFaltante As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSaldoDia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAperturaDia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtGastosDia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtEfectivoDia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtObservaciones As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtRevisado As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtEncargado As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigoArqueo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSobrante As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSaldoGeneral As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtGastoGeneral As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtAporteGeneral As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel26 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel24 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnImprimirArqueo As ctrLibreria.Controles.BeButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnCanc As ctrLibreria.Controles.BeButton
    Friend WithEvents btnOk As ctrLibreria.Controles.BeButton
    Friend WithEvents txtNombre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dgvNombres As System.Windows.Forms.DataGridView
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents dgvVales As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel27 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnIngresoBancario As ctrLibreria.Controles.BeButton
    Friend WithEvents txtImporteDepositoBancario As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel31 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblCodigoCajaExtraordinaria As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblCodigoEmpresa As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotalSaldo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel30 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblNroCaja As ctrLibreria.Controles.BeLabel
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ruc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Validar As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents RazonSocial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoDocumento As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DocumentoReferencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdGasto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Glosa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OtrosD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CentroCosto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdDetalleMovimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Opcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Anular As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Desglozado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Empresas As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Prestamo As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
