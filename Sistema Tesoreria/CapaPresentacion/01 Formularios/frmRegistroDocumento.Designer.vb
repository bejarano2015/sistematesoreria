<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRegistroDocumento
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.stsTotales = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtCajaOrigen = New System.Windows.Forms.TextBox()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.txtBuscaGuiaRemision = New System.Windows.Forms.TextBox()
        Me.BeLabel43 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel44 = New ctrLibreria.Controles.BeLabel()
        Me.dgvGuiaRemision = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtBuscarFactura = New System.Windows.Forms.TextBox()
        Me.BeLabel37 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel38 = New ctrLibreria.Controles.BeLabel()
        Me.dgvFacturas = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdRegistroF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmprCodigoF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NCGlosa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NCImporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.XF = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TIngreso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BeLabel36 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel34 = New ctrLibreria.Controles.BeLabel()
        Me.cboObra = New ctrLibreria.Controles.BeComboBox()
        Me.dgvValorizaciones = New System.Windows.Forms.DataGridView()
        Me.IdValorizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmprCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ParDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaInicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalVal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.X = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.txtCodProvLog = New System.Windows.Forms.TextBox()
        Me.BeLabel30 = New ctrLibreria.Controles.BeLabel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtRucAcree = New System.Windows.Forms.TextBox()
        Me.txtDireccionAcree = New System.Windows.Forms.TextBox()
        Me.txtDescripcionAcreedor = New System.Windows.Forms.TextBox()
        Me.txtCodAcreedor = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtNroDocIdent = New System.Windows.Forms.TextBox()
        Me.rdbJuridica = New System.Windows.Forms.RadioButton()
        Me.rdbNatural = New System.Windows.Forms.RadioButton()
        Me.cboTipoDocIdent = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel26 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel24 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.cboSituacion = New ctrLibreria.Controles.BeComboBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.BeLabel31 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel40 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.dgvOrdenes = New System.Windows.Forms.DataGridView()
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdOrden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrvCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrvRUC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaOC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PdoCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtFiltrar = New System.Windows.Forms.TextBox()
        Me.rdbDescr = New System.Windows.Forms.RadioButton()
        Me.rdbCodigo = New System.Windows.Forms.RadioButton()
        Me.BeButton1 = New ctrLibreria.Controles.BeButton()
        Me.rdbNumDoc = New System.Windows.Forms.RadioButton()
        Me.rdbRuc = New System.Windows.Forms.RadioButton()
        Me.dvgListProveedores = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AnaliticoCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AnaliticoDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RUC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroDocIdentidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.cboFechaRecepcion = New ctrLibreria.Controles.BeDateTimePicker()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.txtUsuario = New ctrLibreria.Controles.BeTextBox()
        Me.cboFechaCreacion = New ctrLibreria.Controles.BeDateTimePicker()
        Me.cboDepartamentos = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.BeLabel39 = New ctrLibreria.Controles.BeLabel()
        Me.txtCodOrden2 = New ctrLibreria.Controles.BeTextBox()
        Me.txtCodOrden = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNroOrden = New ctrLibreria.Controles.BeTextBox()
        Me.txtDP = New ctrLibreria.Controles.BeTextBox()
        Me.cboDP = New ctrLibreria.Controles.BeComboBox()
        Me.chkIGV = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtInafecto = New ctrLibreria.Controles.BeTextBox()
        Me.txtValorCompra = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.txtImporteTotal = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.txtImporteIGV = New ctrLibreria.Controles.BeTextBox()
        Me.txtTasaIGV = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnVerFacturas = New ctrLibreria.Controles.BeButton()
        Me.btnVerValorizaciones = New ctrLibreria.Controles.BeButton()
        Me.dtpFechaVen = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaDoc = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaBase = New System.Windows.Forms.DateTimePicker()
        Me.txtNroDocumento = New System.Windows.Forms.TextBox()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel()
        Me.txtDias = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel()
        Me.cboDocumento = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnValidarRuc = New ctrLibreria.Controles.BeButton()
        Me.txtGuiaRemision = New System.Windows.Forms.TextBox()
        Me.BeLabel42 = New ctrLibreria.Controles.BeLabel()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.BeLabel41 = New ctrLibreria.Controles.BeLabel()
        Me.btnImportar = New ctrLibreria.Controles.BeButton()
        Me.cboEstado = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel27 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel32 = New ctrLibreria.Controles.BeLabel()
        Me.txtCod = New System.Windows.Forms.TextBox()
        Me.btnBuscar = New ctrLibreria.Controles.BeButton()
        Me.cboTipoAcreedor = New ctrLibreria.Controles.BeComboBox()
        Me.lblDescripcionCodigo = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNroDocumentoBuscar = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel35 = New ctrLibreria.Controles.BeLabel()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer4 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer5 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer6 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer7 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer8 = New System.Windows.Forms.Timer(Me.components)
        Me.dgvDetalle = New ctrLibreria.Controles.BeDataGridView()
        Me.Anexo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdRegistro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VISTO = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Timer9 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer10 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer11 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.chkTodosUsuarios = New System.Windows.Forms.CheckBox()
        Me.cboUsuarios = New ctrLibreria.Controles.BeComboBox()
        Me.dtpF1 = New ctrLibreria.Controles.BeDateTimePicker()
        Me.chkTodos2 = New System.Windows.Forms.CheckBox()
        Me.rdb2 = New System.Windows.Forms.RadioButton()
        Me.cboTodosProv = New ctrLibreria.Controles.BeComboBox()
        Me.dtpF2 = New ctrLibreria.Controles.BeDateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.rdb1 = New System.Windows.Forms.RadioButton()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.BeLabel33 = New ctrLibreria.Controles.BeLabel()
        Me.AreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.stsTotales.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel9.SuspendLayout()
        CType(Me.dgvGuiaRemision, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvFacturas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvValorizaciones, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvOrdenes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dvgListProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(1255, 472)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel6)
        Me.TabPage1.Controls.Add(Me.Panel5)
        Me.TabPage1.Controls.Add(Me.dgvDetalle)
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Size = New System.Drawing.Size(1247, 446)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Size = New System.Drawing.Size(1247, 446)
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsTotales.Location = New System.Drawing.Point(3, 417)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(1239, 24)
        Me.stsTotales.TabIndex = 12
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(122, 19)
        Me.ToolStripStatusLabel1.Text = "Total de Registros= 0"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtCajaOrigen)
        Me.Panel1.Controls.Add(Me.Panel7)
        Me.Panel1.Controls.Add(Me.txtCodProvLog)
        Me.Panel1.Controls.Add(Me.BeLabel30)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.cboSituacion)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.BeLabel7)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.ForeColor = System.Drawing.Color.Black
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1239, 716)
        Me.Panel1.TabIndex = 0
        '
        'txtCajaOrigen
        '
        Me.txtCajaOrigen.BackColor = System.Drawing.Color.Ivory
        Me.txtCajaOrigen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCajaOrigen.Location = New System.Drawing.Point(914, 4)
        Me.txtCajaOrigen.MaxLength = 400
        Me.txtCajaOrigen.Multiline = True
        Me.txtCajaOrigen.Name = "txtCajaOrigen"
        Me.txtCajaOrigen.Size = New System.Drawing.Size(262, 68)
        Me.txtCajaOrigen.TabIndex = 174
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Silver
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Panel8)
        Me.Panel7.Controls.Add(Me.BeLabel36)
        Me.Panel7.Controls.Add(Me.BeLabel34)
        Me.Panel7.Controls.Add(Me.cboObra)
        Me.Panel7.Controls.Add(Me.dgvValorizaciones)
        Me.Panel7.Location = New System.Drawing.Point(398, 134)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(750, 254)
        Me.Panel7.TabIndex = 9
        Me.Panel7.Visible = False
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Silver
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Controls.Add(Me.Panel9)
        Me.Panel8.Controls.Add(Me.txtBuscarFactura)
        Me.Panel8.Controls.Add(Me.BeLabel37)
        Me.Panel8.Controls.Add(Me.BeLabel38)
        Me.Panel8.Controls.Add(Me.dgvFacturas)
        Me.Panel8.Location = New System.Drawing.Point(14, 4)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(475, 254)
        Me.Panel8.TabIndex = 15
        Me.Panel8.Visible = False
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Silver
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Controls.Add(Me.txtBuscaGuiaRemision)
        Me.Panel9.Controls.Add(Me.BeLabel43)
        Me.Panel9.Controls.Add(Me.BeLabel44)
        Me.Panel9.Controls.Add(Me.dgvGuiaRemision)
        Me.Panel9.Location = New System.Drawing.Point(78, -112)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(40, 30)
        Me.Panel9.TabIndex = 16
        Me.Panel9.Visible = False
        '
        'txtBuscaGuiaRemision
        '
        Me.txtBuscaGuiaRemision.BackColor = System.Drawing.Color.Ivory
        Me.txtBuscaGuiaRemision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBuscaGuiaRemision.Location = New System.Drawing.Point(80, 11)
        Me.txtBuscaGuiaRemision.Name = "txtBuscaGuiaRemision"
        Me.txtBuscaGuiaRemision.Size = New System.Drawing.Size(213, 21)
        Me.txtBuscaGuiaRemision.TabIndex = 15
        '
        'BeLabel43
        '
        Me.BeLabel43.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel43.AutoSize = True
        Me.BeLabel43.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel43.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel43.ForeColor = System.Drawing.Color.Red
        Me.BeLabel43.Location = New System.Drawing.Point(455, 3)
        Me.BeLabel43.Name = "BeLabel43"
        Me.BeLabel43.Size = New System.Drawing.Size(15, 13)
        Me.BeLabel43.TabIndex = 14
        Me.BeLabel43.Text = "X"
        '
        'BeLabel44
        '
        Me.BeLabel44.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel44.AutoSize = True
        Me.BeLabel44.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel44.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel44.ForeColor = System.Drawing.Color.Black
        Me.BeLabel44.Location = New System.Drawing.Point(3, 15)
        Me.BeLabel44.Name = "BeLabel44"
        Me.BeLabel44.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel44.TabIndex = 13
        Me.BeLabel44.Text = "Buscar N� : "
        '
        'dgvGuiaRemision
        '
        Me.dgvGuiaRemision.AllowUserToAddRows = False
        Me.dgvGuiaRemision.BackgroundColor = System.Drawing.Color.White
        Me.dgvGuiaRemision.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGuiaRemision.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewTextBoxColumn15})
        Me.dgvGuiaRemision.EnableHeadersVisualStyles = False
        Me.dgvGuiaRemision.Location = New System.Drawing.Point(4, 43)
        Me.dgvGuiaRemision.Name = "dgvGuiaRemision"
        Me.dgvGuiaRemision.RowHeadersVisible = False
        Me.dgvGuiaRemision.Size = New System.Drawing.Size(465, 205)
        Me.dgvGuiaRemision.TabIndex = 13
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "FechaCreacion"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 80
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "AbrDoc"
        Me.DataGridViewTextBoxColumn7.HeaderText = "TD"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Width = 50
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Ndoc"
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn8.HeaderText = "N� Doc"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 110
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "MonSimbolo"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 60
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "ImporteTotal"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Importe"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "IdRegistro"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DataGridViewTextBoxColumn11.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn11.HeaderText = "IdRegistro"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "EmprCodigo"
        Me.DataGridViewTextBoxColumn12.HeaderText = "EmprCodigo"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "NCGlosa"
        Me.DataGridViewTextBoxColumn13.HeaderText = "NCGlosa"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "NCImporte"
        Me.DataGridViewTextBoxColumn14.HeaderText = "NCImporte"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.HeaderText = "X"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.Width = 40
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "TIngreso"
        Me.DataGridViewTextBoxColumn15.HeaderText = "TIngreso"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'txtBuscarFactura
        '
        Me.txtBuscarFactura.BackColor = System.Drawing.Color.Ivory
        Me.txtBuscarFactura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBuscarFactura.Location = New System.Drawing.Point(80, 11)
        Me.txtBuscarFactura.Name = "txtBuscarFactura"
        Me.txtBuscarFactura.Size = New System.Drawing.Size(213, 21)
        Me.txtBuscarFactura.TabIndex = 15
        '
        'BeLabel37
        '
        Me.BeLabel37.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel37.AutoSize = True
        Me.BeLabel37.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel37.ForeColor = System.Drawing.Color.Red
        Me.BeLabel37.Location = New System.Drawing.Point(455, 3)
        Me.BeLabel37.Name = "BeLabel37"
        Me.BeLabel37.Size = New System.Drawing.Size(15, 13)
        Me.BeLabel37.TabIndex = 14
        Me.BeLabel37.Text = "X"
        '
        'BeLabel38
        '
        Me.BeLabel38.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel38.AutoSize = True
        Me.BeLabel38.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel38.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel38.ForeColor = System.Drawing.Color.Black
        Me.BeLabel38.Location = New System.Drawing.Point(3, 15)
        Me.BeLabel38.Name = "BeLabel38"
        Me.BeLabel38.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel38.TabIndex = 13
        Me.BeLabel38.Text = "Buscar N� : "
        '
        'dgvFacturas
        '
        Me.dgvFacturas.AllowUserToAddRows = False
        Me.dgvFacturas.BackgroundColor = System.Drawing.Color.White
        Me.dgvFacturas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFacturas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.IdRegistroF, Me.EmprCodigoF, Me.NCGlosa, Me.NCImporte, Me.XF, Me.TIngreso})
        Me.dgvFacturas.EnableHeadersVisualStyles = False
        Me.dgvFacturas.Location = New System.Drawing.Point(4, 43)
        Me.dgvFacturas.Name = "dgvFacturas"
        Me.dgvFacturas.RowHeadersVisible = False
        Me.dgvFacturas.Size = New System.Drawing.Size(465, 205)
        Me.dgvFacturas.TabIndex = 13
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "FechaCreacion"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 80
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "AbrDoc"
        Me.DataGridViewTextBoxColumn2.HeaderText = "TD"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 50
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Ndoc"
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.White
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn3.HeaderText = "N� Doc"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 110
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "MonSimbolo"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 60
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "ImporteTotal"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Importe"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'IdRegistroF
        '
        Me.IdRegistroF.DataPropertyName = "IdRegistro"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.IdRegistroF.DefaultCellStyle = DataGridViewCellStyle8
        Me.IdRegistroF.HeaderText = "IdRegistro"
        Me.IdRegistroF.Name = "IdRegistroF"
        Me.IdRegistroF.ReadOnly = True
        Me.IdRegistroF.Visible = False
        '
        'EmprCodigoF
        '
        Me.EmprCodigoF.DataPropertyName = "EmprCodigo"
        Me.EmprCodigoF.HeaderText = "EmprCodigo"
        Me.EmprCodigoF.Name = "EmprCodigoF"
        Me.EmprCodigoF.Visible = False
        '
        'NCGlosa
        '
        Me.NCGlosa.DataPropertyName = "NCGlosa"
        Me.NCGlosa.HeaderText = "NCGlosa"
        Me.NCGlosa.Name = "NCGlosa"
        Me.NCGlosa.Visible = False
        '
        'NCImporte
        '
        Me.NCImporte.DataPropertyName = "NCImporte"
        Me.NCImporte.HeaderText = "NCImporte"
        Me.NCImporte.Name = "NCImporte"
        Me.NCImporte.Visible = False
        '
        'XF
        '
        Me.XF.HeaderText = "X"
        Me.XF.Name = "XF"
        Me.XF.Width = 40
        '
        'TIngreso
        '
        Me.TIngreso.DataPropertyName = "TIngreso"
        Me.TIngreso.HeaderText = "TIngreso"
        Me.TIngreso.Name = "TIngreso"
        Me.TIngreso.Visible = False
        '
        'BeLabel36
        '
        Me.BeLabel36.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel36.AutoSize = True
        Me.BeLabel36.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel36.ForeColor = System.Drawing.Color.Red
        Me.BeLabel36.Location = New System.Drawing.Point(734, 0)
        Me.BeLabel36.Name = "BeLabel36"
        Me.BeLabel36.Size = New System.Drawing.Size(15, 13)
        Me.BeLabel36.TabIndex = 14
        Me.BeLabel36.Text = "X"
        '
        'BeLabel34
        '
        Me.BeLabel34.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel34.AutoSize = True
        Me.BeLabel34.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel34.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel34.ForeColor = System.Drawing.Color.Black
        Me.BeLabel34.Location = New System.Drawing.Point(1, 15)
        Me.BeLabel34.Name = "BeLabel34"
        Me.BeLabel34.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel34.TabIndex = 13
        Me.BeLabel34.Text = "Obra"
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(38, 12)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(327, 21)
        Me.cboObra.TabIndex = 13
        '
        'dgvValorizaciones
        '
        Me.dgvValorizaciones.AllowUserToAddRows = False
        Me.dgvValorizaciones.BackgroundColor = System.Drawing.Color.White
        Me.dgvValorizaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvValorizaciones.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdValorizacion, Me.EmprCodigo, Me.ParDescripcion, Me.Concepto, Me.FechaInicio, Me.TotalVal, Me.X})
        Me.dgvValorizaciones.EnableHeadersVisualStyles = False
        Me.dgvValorizaciones.Location = New System.Drawing.Point(4, 44)
        Me.dgvValorizaciones.Name = "dgvValorizaciones"
        Me.dgvValorizaciones.RowHeadersVisible = False
        Me.dgvValorizaciones.Size = New System.Drawing.Size(741, 205)
        Me.dgvValorizaciones.TabIndex = 13
        '
        'IdValorizacion
        '
        Me.IdValorizacion.DataPropertyName = "IdValorizacion"
        Me.IdValorizacion.HeaderText = "IdValorizacion"
        Me.IdValorizacion.Name = "IdValorizacion"
        Me.IdValorizacion.Visible = False
        '
        'EmprCodigo
        '
        Me.EmprCodigo.DataPropertyName = "EmprCodigo"
        Me.EmprCodigo.HeaderText = "EmprCodigo"
        Me.EmprCodigo.Name = "EmprCodigo"
        Me.EmprCodigo.Visible = False
        '
        'ParDescripcion
        '
        Me.ParDescripcion.DataPropertyName = "ParDescripcion"
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.White
        Me.ParDescripcion.DefaultCellStyle = DataGridViewCellStyle9
        Me.ParDescripcion.HeaderText = "Contrato"
        Me.ParDescripcion.Name = "ParDescripcion"
        Me.ParDescripcion.ReadOnly = True
        Me.ParDescripcion.Width = 255
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Concepto"
        Me.Concepto.HeaderText = "Pago De"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.ReadOnly = True
        Me.Concepto.Width = 255
        '
        'FechaInicio
        '
        Me.FechaInicio.DataPropertyName = "FechaInicio"
        Me.FechaInicio.HeaderText = "Fecha"
        Me.FechaInicio.Name = "FechaInicio"
        Me.FechaInicio.ReadOnly = True
        Me.FechaInicio.Width = 80
        '
        'TotalVal
        '
        Me.TotalVal.DataPropertyName = "TotalVal"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.TotalVal.DefaultCellStyle = DataGridViewCellStyle10
        Me.TotalVal.HeaderText = "Importe"
        Me.TotalVal.Name = "TotalVal"
        Me.TotalVal.ReadOnly = True
        '
        'X
        '
        Me.X.HeaderText = "X"
        Me.X.Name = "X"
        Me.X.Width = 30
        '
        'txtCodProvLog
        '
        Me.txtCodProvLog.Location = New System.Drawing.Point(311, 457)
        Me.txtCodProvLog.Name = "txtCodProvLog"
        Me.txtCodProvLog.Size = New System.Drawing.Size(100, 21)
        Me.txtCodProvLog.TabIndex = 8
        Me.txtCodProvLog.Visible = False
        '
        'BeLabel30
        '
        Me.BeLabel30.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel30.AutoSize = True
        Me.BeLabel30.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel30.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel30.ForeColor = System.Drawing.Color.Black
        Me.BeLabel30.Location = New System.Drawing.Point(660, 423)
        Me.BeLabel30.Name = "BeLabel30"
        Me.BeLabel30.Size = New System.Drawing.Size(111, 13)
        Me.BeLabel30.TabIndex = 7
        Me.BeLabel30.Text = "F9 : VER LISTADO"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.SlateGray
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.txtRucAcree)
        Me.Panel3.Controls.Add(Me.txtDireccionAcree)
        Me.Panel3.Controls.Add(Me.txtDescripcionAcreedor)
        Me.Panel3.Controls.Add(Me.txtCodAcreedor)
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Controls.Add(Me.Button1)
        Me.Panel3.Controls.Add(Me.GroupBox6)
        Me.Panel3.Controls.Add(Me.BeLabel24)
        Me.Panel3.Controls.Add(Me.BeLabel13)
        Me.Panel3.Controls.Add(Me.BeLabel9)
        Me.Panel3.Controls.Add(Me.BeLabel5)
        Me.Panel3.Location = New System.Drawing.Point(178, 82)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(522, 245)
        Me.Panel3.TabIndex = 2
        Me.Panel3.Visible = False
        '
        'txtRucAcree
        '
        Me.txtRucAcree.BackColor = System.Drawing.Color.Ivory
        Me.txtRucAcree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRucAcree.Location = New System.Drawing.Point(100, 91)
        Me.txtRucAcree.MaxLength = 11
        Me.txtRucAcree.Name = "txtRucAcree"
        Me.txtRucAcree.Size = New System.Drawing.Size(213, 21)
        Me.txtRucAcree.TabIndex = 7
        '
        'txtDireccionAcree
        '
        Me.txtDireccionAcree.BackColor = System.Drawing.Color.Ivory
        Me.txtDireccionAcree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDireccionAcree.Location = New System.Drawing.Point(100, 67)
        Me.txtDireccionAcree.Name = "txtDireccionAcree"
        Me.txtDireccionAcree.Size = New System.Drawing.Size(356, 21)
        Me.txtDireccionAcree.TabIndex = 5
        '
        'txtDescripcionAcreedor
        '
        Me.txtDescripcionAcreedor.BackColor = System.Drawing.Color.Ivory
        Me.txtDescripcionAcreedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcionAcreedor.Location = New System.Drawing.Point(100, 41)
        Me.txtDescripcionAcreedor.Name = "txtDescripcionAcreedor"
        Me.txtDescripcionAcreedor.Size = New System.Drawing.Size(356, 21)
        Me.txtDescripcionAcreedor.TabIndex = 3
        '
        'txtCodAcreedor
        '
        Me.txtCodAcreedor.BackColor = System.Drawing.Color.Ivory
        Me.txtCodAcreedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodAcreedor.Location = New System.Drawing.Point(100, 15)
        Me.txtCodAcreedor.Name = "txtCodAcreedor"
        Me.txtCodAcreedor.Size = New System.Drawing.Size(213, 21)
        Me.txtCodAcreedor.TabIndex = 1
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(410, 219)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(72, 23)
        Me.Button2.TabIndex = 10
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(343, 219)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(61, 23)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "&Grabar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtNroDocIdent)
        Me.GroupBox6.Controls.Add(Me.rdbJuridica)
        Me.GroupBox6.Controls.Add(Me.rdbNatural)
        Me.GroupBox6.Controls.Add(Me.cboTipoDocIdent)
        Me.GroupBox6.Controls.Add(Me.BeLabel26)
        Me.GroupBox6.Controls.Add(Me.BeLabel25)
        Me.GroupBox6.Location = New System.Drawing.Point(14, 118)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(323, 103)
        Me.GroupBox6.TabIndex = 8
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Tipo Persona"
        '
        'txtNroDocIdent
        '
        Me.txtNroDocIdent.BackColor = System.Drawing.Color.Ivory
        Me.txtNroDocIdent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroDocIdent.Location = New System.Drawing.Point(101, 63)
        Me.txtNroDocIdent.Name = "txtNroDocIdent"
        Me.txtNroDocIdent.Size = New System.Drawing.Size(213, 21)
        Me.txtNroDocIdent.TabIndex = 5
        '
        'rdbJuridica
        '
        Me.rdbJuridica.AutoSize = True
        Me.rdbJuridica.Checked = True
        Me.rdbJuridica.Location = New System.Drawing.Point(24, 12)
        Me.rdbJuridica.Name = "rdbJuridica"
        Me.rdbJuridica.Size = New System.Drawing.Size(68, 17)
        Me.rdbJuridica.TabIndex = 0
        Me.rdbJuridica.TabStop = True
        Me.rdbJuridica.Text = "Juridica"
        Me.rdbJuridica.UseVisualStyleBackColor = True
        '
        'rdbNatural
        '
        Me.rdbNatural.AutoSize = True
        Me.rdbNatural.Location = New System.Drawing.Point(108, 12)
        Me.rdbNatural.Name = "rdbNatural"
        Me.rdbNatural.Size = New System.Drawing.Size(66, 17)
        Me.rdbNatural.TabIndex = 1
        Me.rdbNatural.TabStop = True
        Me.rdbNatural.Text = "Natural"
        Me.rdbNatural.UseVisualStyleBackColor = True
        '
        'cboTipoDocIdent
        '
        Me.cboTipoDocIdent.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoDocIdent.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoDocIdent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDocIdent.Enabled = False
        Me.cboTipoDocIdent.ForeColor = System.Drawing.Color.Black
        Me.cboTipoDocIdent.FormattingEnabled = True
        Me.cboTipoDocIdent.KeyEnter = True
        Me.cboTipoDocIdent.Location = New System.Drawing.Point(101, 37)
        Me.cboTipoDocIdent.Name = "cboTipoDocIdent"
        Me.cboTipoDocIdent.Size = New System.Drawing.Size(215, 21)
        Me.cboTipoDocIdent.TabIndex = 3
        '
        'BeLabel26
        '
        Me.BeLabel26.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel26.AutoSize = True
        Me.BeLabel26.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel26.ForeColor = System.Drawing.Color.Black
        Me.BeLabel26.Location = New System.Drawing.Point(14, 65)
        Me.BeLabel26.Name = "BeLabel26"
        Me.BeLabel26.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel26.TabIndex = 4
        Me.BeLabel26.Text = "N�mero"
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.ForeColor = System.Drawing.Color.Black
        Me.BeLabel25.Location = New System.Drawing.Point(13, 40)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(72, 13)
        Me.BeLabel25.TabIndex = 2
        Me.BeLabel25.Text = "Documento"
        '
        'BeLabel24
        '
        Me.BeLabel24.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel24.AutoSize = True
        Me.BeLabel24.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel24.ForeColor = System.Drawing.Color.Black
        Me.BeLabel24.Location = New System.Drawing.Point(11, 97)
        Me.BeLabel24.Name = "BeLabel24"
        Me.BeLabel24.Size = New System.Drawing.Size(32, 13)
        Me.BeLabel24.TabIndex = 6
        Me.BeLabel24.Text = "RUC"
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(10, 71)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(60, 13)
        Me.BeLabel13.TabIndex = 4
        Me.BeLabel13.Text = "Direcci�n"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(10, 44)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(80, 13)
        Me.BeLabel9.TabIndex = 2
        Me.BeLabel9.Text = "Raz�n Social"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(10, 18)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(86, 13)
        Me.BeLabel5.TabIndex = 0
        Me.BeLabel5.Text = "C�digo (RUC)"
        '
        'cboSituacion
        '
        Me.cboSituacion.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboSituacion.BackColor = System.Drawing.Color.Ivory
        Me.cboSituacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSituacion.ForeColor = System.Drawing.Color.Black
        Me.cboSituacion.FormattingEnabled = True
        Me.cboSituacion.KeyEnter = True
        Me.cboSituacion.Location = New System.Drawing.Point(94, 457)
        Me.cboSituacion.Name = "cboSituacion"
        Me.cboSituacion.Size = New System.Drawing.Size(211, 21)
        Me.cboSituacion.TabIndex = 2
        Me.cboSituacion.Visible = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.SlateGray
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.BeLabel31)
        Me.Panel4.Controls.Add(Me.BeLabel40)
        Me.Panel4.Controls.Add(Me.BeLabel11)
        Me.Panel4.Controls.Add(Me.dgvOrdenes)
        Me.Panel4.Location = New System.Drawing.Point(523, 220)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(433, 191)
        Me.Panel4.TabIndex = 5
        Me.Panel4.Visible = False
        '
        'BeLabel31
        '
        Me.BeLabel31.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel31.AutoSize = True
        Me.BeLabel31.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel31.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel31.ForeColor = System.Drawing.Color.Black
        Me.BeLabel31.Location = New System.Drawing.Point(335, 170)
        Me.BeLabel31.Name = "BeLabel31"
        Me.BeLabel31.Size = New System.Drawing.Size(11, 13)
        Me.BeLabel31.TabIndex = 12
        Me.BeLabel31.Text = "."
        '
        'BeLabel40
        '
        Me.BeLabel40.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel40.AutoSize = True
        Me.BeLabel40.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel40.ForeColor = System.Drawing.Color.Black
        Me.BeLabel40.Location = New System.Drawing.Point(3, 4)
        Me.BeLabel40.Name = "BeLabel40"
        Me.BeLabel40.Size = New System.Drawing.Size(137, 13)
        Me.BeLabel40.TabIndex = 0
        Me.BeLabel40.Text = "ORDENES DE COMPRA"
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel11.ForeColor = System.Drawing.Color.Red
        Me.BeLabel11.Location = New System.Drawing.Point(418, 0)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(15, 13)
        Me.BeLabel11.TabIndex = 2
        Me.BeLabel11.Text = "X"
        '
        'dgvOrdenes
        '
        Me.dgvOrdenes.AllowUserToAddRows = False
        Me.dgvOrdenes.BackgroundColor = System.Drawing.Color.White
        Me.dgvOrdenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOrdenes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Numero, Me.Column13, Me.Column16, Me.IdOrden, Me.PrvCodigo, Me.PrvRUC, Me.Column21, Me.Column22, Me.FechaOC, Me.PdoCodigo})
        Me.dgvOrdenes.Location = New System.Drawing.Point(3, 20)
        Me.dgvOrdenes.Name = "dgvOrdenes"
        Me.dgvOrdenes.RowHeadersVisible = False
        Me.dgvOrdenes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOrdenes.Size = New System.Drawing.Size(427, 145)
        Me.dgvOrdenes.TabIndex = 1
        '
        'Numero
        '
        Me.Numero.DataPropertyName = "Numero"
        DataGridViewCellStyle11.BackColor = System.Drawing.Color.White
        Me.Numero.DefaultCellStyle = DataGridViewCellStyle11
        Me.Numero.HeaderText = "N�mero"
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Width = 75
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "CCosto"
        Me.Column13.HeaderText = "Proveedor"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 130
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "Moneda"
        Me.Column16.HeaderText = "Moneda"
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        '
        'IdOrden
        '
        Me.IdOrden.DataPropertyName = "IdOrden"
        Me.IdOrden.HeaderText = "IdOrden"
        Me.IdOrden.Name = "IdOrden"
        Me.IdOrden.ReadOnly = True
        Me.IdOrden.Visible = False
        '
        'PrvCodigo
        '
        Me.PrvCodigo.DataPropertyName = "PrvCodigo"
        Me.PrvCodigo.HeaderText = "PrvCodigo"
        Me.PrvCodigo.Name = "PrvCodigo"
        Me.PrvCodigo.ReadOnly = True
        Me.PrvCodigo.Visible = False
        '
        'PrvRUC
        '
        Me.PrvRUC.DataPropertyName = "PrvRUC"
        Me.PrvRUC.HeaderText = "PrvRUC"
        Me.PrvRUC.Name = "PrvRUC"
        Me.PrvRUC.ReadOnly = True
        Me.PrvRUC.Visible = False
        '
        'Column21
        '
        Me.Column21.DataPropertyName = "Saldo"
        Me.Column21.HeaderText = "Saldo"
        Me.Column21.Name = "Column21"
        Me.Column21.ReadOnly = True
        '
        'Column22
        '
        Me.Column22.DataPropertyName = "CCosCodigo"
        Me.Column22.HeaderText = "CCosCodigo"
        Me.Column22.Name = "Column22"
        Me.Column22.ReadOnly = True
        Me.Column22.Visible = False
        '
        'FechaOC
        '
        Me.FechaOC.DataPropertyName = "FechaOC"
        Me.FechaOC.HeaderText = "FechaOC"
        Me.FechaOC.Name = "FechaOC"
        Me.FechaOC.Visible = False
        '
        'PdoCodigo
        '
        Me.PdoCodigo.DataPropertyName = "PdoCodigo"
        Me.PdoCodigo.HeaderText = "PdoCodigo"
        Me.PdoCodigo.Name = "PdoCodigo"
        Me.PdoCodigo.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.SlateGray
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.GroupBox5)
        Me.Panel2.Controls.Add(Me.dvgListProveedores)
        Me.Panel2.Location = New System.Drawing.Point(388, 17)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(522, 245)
        Me.Panel2.TabIndex = 1
        Me.Panel2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(506, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(15, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "X"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtFiltrar)
        Me.GroupBox5.Controls.Add(Me.rdbDescr)
        Me.GroupBox5.Controls.Add(Me.rdbCodigo)
        Me.GroupBox5.Controls.Add(Me.BeButton1)
        Me.GroupBox5.Controls.Add(Me.rdbNumDoc)
        Me.GroupBox5.Controls.Add(Me.rdbRuc)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 9)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(516, 43)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Buscar Por:"
        '
        'txtFiltrar
        '
        Me.txtFiltrar.BackColor = System.Drawing.Color.Ivory
        Me.txtFiltrar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFiltrar.Location = New System.Drawing.Point(285, 15)
        Me.txtFiltrar.Name = "txtFiltrar"
        Me.txtFiltrar.Size = New System.Drawing.Size(165, 21)
        Me.txtFiltrar.TabIndex = 4
        '
        'rdbDescr
        '
        Me.rdbDescr.AutoSize = True
        Me.rdbDescr.Location = New System.Drawing.Point(69, 17)
        Me.rdbDescr.Name = "rdbDescr"
        Me.rdbDescr.Size = New System.Drawing.Size(98, 17)
        Me.rdbDescr.TabIndex = 1
        Me.rdbDescr.TabStop = True
        Me.rdbDescr.Text = "Raz�n Social"
        Me.rdbDescr.UseVisualStyleBackColor = True
        '
        'rdbCodigo
        '
        Me.rdbCodigo.AutoSize = True
        Me.rdbCodigo.Location = New System.Drawing.Point(7, 17)
        Me.rdbCodigo.Name = "rdbCodigo"
        Me.rdbCodigo.Size = New System.Drawing.Size(65, 17)
        Me.rdbCodigo.TabIndex = 0
        Me.rdbCodigo.TabStop = True
        Me.rdbCodigo.Text = "C�digo"
        Me.rdbCodigo.UseVisualStyleBackColor = True
        '
        'BeButton1
        '
        Me.BeButton1.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton1.Location = New System.Drawing.Point(452, 14)
        Me.BeButton1.Name = "BeButton1"
        Me.BeButton1.Size = New System.Drawing.Size(57, 23)
        Me.BeButton1.TabIndex = 5
        Me.BeButton1.Text = "Nuevo"
        Me.BeButton1.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton1.UseVisualStyleBackColor = True
        '
        'rdbNumDoc
        '
        Me.rdbNumDoc.AutoSize = True
        Me.rdbNumDoc.Location = New System.Drawing.Point(216, 18)
        Me.rdbNumDoc.Name = "rdbNumDoc"
        Me.rdbNumDoc.Size = New System.Drawing.Size(71, 17)
        Me.rdbNumDoc.TabIndex = 3
        Me.rdbNumDoc.TabStop = True
        Me.rdbNumDoc.Text = "Doc. Id."
        Me.rdbNumDoc.UseVisualStyleBackColor = True
        '
        'rdbRuc
        '
        Me.rdbRuc.AutoSize = True
        Me.rdbRuc.Location = New System.Drawing.Point(169, 18)
        Me.rdbRuc.Name = "rdbRuc"
        Me.rdbRuc.Size = New System.Drawing.Size(46, 17)
        Me.rdbRuc.TabIndex = 2
        Me.rdbRuc.TabStop = True
        Me.rdbRuc.Text = "Ruc"
        Me.rdbRuc.UseVisualStyleBackColor = True
        '
        'dvgListProveedores
        '
        Me.dvgListProveedores.AllowUserToAddRows = False
        Me.dvgListProveedores.BackgroundColor = System.Drawing.Color.White
        Me.dvgListProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvgListProveedores.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.AnaliticoCodigo, Me.AnaliticoDescripcion, Me.RUC, Me.NroDocIdentidad})
        Me.dvgListProveedores.Location = New System.Drawing.Point(3, 80)
        Me.dvgListProveedores.Name = "dvgListProveedores"
        Me.dvgListProveedores.RowHeadersVisible = False
        Me.dvgListProveedores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dvgListProveedores.Size = New System.Drawing.Size(516, 170)
        Me.dvgListProveedores.TabIndex = 2
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "TipoAnaliticoCodigo"
        Me.Column1.HeaderText = "Anexo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 50
        '
        'AnaliticoCodigo
        '
        Me.AnaliticoCodigo.DataPropertyName = "AnaliticoCodigo"
        Me.AnaliticoCodigo.HeaderText = "C�digo"
        Me.AnaliticoCodigo.Name = "AnaliticoCodigo"
        Me.AnaliticoCodigo.ReadOnly = True
        Me.AnaliticoCodigo.Width = 85
        '
        'AnaliticoDescripcion
        '
        Me.AnaliticoDescripcion.DataPropertyName = "AnaliticoDescripcion"
        Me.AnaliticoDescripcion.HeaderText = "Descripci�n"
        Me.AnaliticoDescripcion.Name = "AnaliticoDescripcion"
        Me.AnaliticoDescripcion.ReadOnly = True
        Me.AnaliticoDescripcion.Width = 150
        '
        'RUC
        '
        Me.RUC.DataPropertyName = "RUC"
        Me.RUC.HeaderText = "Ruc"
        Me.RUC.Name = "RUC"
        Me.RUC.ReadOnly = True
        '
        'NroDocIdentidad
        '
        Me.NroDocIdentidad.DataPropertyName = "NroDocIdentidad"
        Me.NroDocIdentidad.HeaderText = "Doc. Ide."
        Me.NroDocIdentidad.Name = "NroDocIdentidad"
        Me.NroDocIdentidad.ReadOnly = True
        Me.NroDocIdentidad.Width = 110
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtDescripcion)
        Me.GroupBox4.Controls.Add(Me.cboFechaRecepcion)
        Me.GroupBox4.Controls.Add(Me.BeLabel4)
        Me.GroupBox4.Controls.Add(Me.txtUsuario)
        Me.GroupBox4.Controls.Add(Me.cboFechaCreacion)
        Me.GroupBox4.Controls.Add(Me.cboDepartamentos)
        Me.GroupBox4.Controls.Add(Me.BeLabel14)
        Me.GroupBox4.Controls.Add(Me.BeLabel6)
        Me.GroupBox4.Controls.Add(Me.BeLabel16)
        Me.GroupBox4.Controls.Add(Me.BeLabel15)
        Me.GroupBox4.Location = New System.Drawing.Point(5, 314)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(768, 108)
        Me.GroupBox4.TabIndex = 6
        Me.GroupBox4.TabStop = False
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.Ivory
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.Location = New System.Drawing.Point(89, 42)
        Me.txtDescripcion.MaxLength = 400
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(327, 21)
        Me.txtDescripcion.TabIndex = 1
        '
        'cboFechaRecepcion
        '
        Me.cboFechaRecepcion.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.cboFechaRecepcion.CustomFormat = ""
        Me.cboFechaRecepcion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.cboFechaRecepcion.KeyEnter = True
        Me.cboFechaRecepcion.Location = New System.Drawing.Point(637, 44)
        Me.cboFechaRecepcion.Name = "cboFechaRecepcion"
        Me.cboFechaRecepcion.Size = New System.Drawing.Size(120, 21)
        Me.cboFechaRecepcion.TabIndex = 4
        Me.cboFechaRecepcion.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.cboFechaRecepcion.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(536, 75)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(50, 13)
        Me.BeLabel4.TabIndex = 10
        Me.BeLabel4.Text = "Usuario"
        '
        'txtUsuario
        '
        Me.txtUsuario.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtUsuario.BackColor = System.Drawing.Color.Ivory
        Me.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUsuario.ForeColor = System.Drawing.Color.Black
        Me.txtUsuario.KeyEnter = True
        Me.txtUsuario.Location = New System.Drawing.Point(637, 69)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtUsuario.ShortcutsEnabled = False
        Me.txtUsuario.Size = New System.Drawing.Size(120, 21)
        Me.txtUsuario.TabIndex = 5
        Me.txtUsuario.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboFechaCreacion
        '
        Me.cboFechaCreacion.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.cboFechaCreacion.CustomFormat = ""
        Me.cboFechaCreacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.cboFechaCreacion.KeyEnter = True
        Me.cboFechaCreacion.Location = New System.Drawing.Point(637, 19)
        Me.cboFechaCreacion.Name = "cboFechaCreacion"
        Me.cboFechaCreacion.Size = New System.Drawing.Size(120, 21)
        Me.cboFechaCreacion.TabIndex = 3
        Me.cboFechaCreacion.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.cboFechaCreacion.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'cboDepartamentos
        '
        Me.cboDepartamentos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboDepartamentos.BackColor = System.Drawing.Color.Ivory
        Me.cboDepartamentos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDepartamentos.ForeColor = System.Drawing.Color.Black
        Me.cboDepartamentos.FormattingEnabled = True
        Me.cboDepartamentos.KeyEnter = True
        Me.cboDepartamentos.Location = New System.Drawing.Point(89, 13)
        Me.cboDepartamentos.Name = "cboDepartamentos"
        Me.cboDepartamentos.Size = New System.Drawing.Size(326, 21)
        Me.cboDepartamentos.TabIndex = 0
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(6, 21)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel14.TabIndex = 0
        Me.BeLabel14.Text = "Area"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(6, 48)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(73, 13)
        Me.BeLabel6.TabIndex = 2
        Me.BeLabel6.Text = "Descripci�n"
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(536, 50)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(102, 13)
        Me.BeLabel16.TabIndex = 8
        Me.BeLabel16.Text = "Fecha Recepci�n"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(536, 23)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(95, 13)
        Me.BeLabel15.TabIndex = 6
        Me.BeLabel15.Text = "Fecha Creaci�n"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.BeLabel39)
        Me.GroupBox3.Controls.Add(Me.txtCodOrden2)
        Me.GroupBox3.Controls.Add(Me.txtCodOrden)
        Me.GroupBox3.Controls.Add(Me.BeLabel10)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.txtNroOrden)
        Me.GroupBox3.Controls.Add(Me.txtDP)
        Me.GroupBox3.Controls.Add(Me.cboDP)
        Me.GroupBox3.Controls.Add(Me.chkIGV)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.txtInafecto)
        Me.GroupBox3.Controls.Add(Me.txtValorCompra)
        Me.GroupBox3.Controls.Add(Me.BeLabel3)
        Me.GroupBox3.Controls.Add(Me.txtImporteTotal)
        Me.GroupBox3.Controls.Add(Me.BeLabel2)
        Me.GroupBox3.Controls.Add(Me.BeLabel1)
        Me.GroupBox3.Controls.Add(Me.cboMoneda)
        Me.GroupBox3.Controls.Add(Me.BeLabel12)
        Me.GroupBox3.Controls.Add(Me.txtImporteIGV)
        Me.GroupBox3.Controls.Add(Me.txtTasaIGV)
        Me.GroupBox3.Controls.Add(Me.BeLabel8)
        Me.GroupBox3.Location = New System.Drawing.Point(5, 181)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(822, 132)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        '
        'BeLabel39
        '
        Me.BeLabel39.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel39.AutoSize = True
        Me.BeLabel39.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel39.ForeColor = System.Drawing.Color.Black
        Me.BeLabel39.Location = New System.Drawing.Point(370, 106)
        Me.BeLabel39.Name = "BeLabel39"
        Me.BeLabel39.Size = New System.Drawing.Size(143, 13)
        Me.BeLabel39.TabIndex = 9
        Me.BeLabel39.Text = "Detracci�n / Percepci�n"
        '
        'txtCodOrden2
        '
        Me.txtCodOrden2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodOrden2.BackColor = System.Drawing.Color.Ivory
        Me.txtCodOrden2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodOrden2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodOrden2.ForeColor = System.Drawing.Color.Black
        Me.txtCodOrden2.KeyEnter = True
        Me.txtCodOrden2.Location = New System.Drawing.Point(736, 42)
        Me.txtCodOrden2.Name = "txtCodOrden2"
        Me.txtCodOrden2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodOrden2.ShortcutsEnabled = False
        Me.txtCodOrden2.Size = New System.Drawing.Size(50, 21)
        Me.txtCodOrden2.TabIndex = 20
        Me.txtCodOrden2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCodOrden2.Visible = False
        '
        'txtCodOrden
        '
        Me.txtCodOrden.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodOrden.BackColor = System.Drawing.Color.Ivory
        Me.txtCodOrden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodOrden.ForeColor = System.Drawing.Color.Black
        Me.txtCodOrden.KeyEnter = True
        Me.txtCodOrden.Location = New System.Drawing.Point(772, 16)
        Me.txtCodOrden.Name = "txtCodOrden"
        Me.txtCodOrden.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodOrden.ShortcutsEnabled = False
        Me.txtCodOrden.Size = New System.Drawing.Size(50, 21)
        Me.txtCodOrden.TabIndex = 19
        Me.txtCodOrden.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCodOrden.Visible = False
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(425, 25)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(30, 13)
        Me.BeLabel10.TabIndex = 10
        Me.BeLabel10.Text = "O/C"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(658, 107)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "(%)"
        '
        'txtNroOrden
        '
        Me.txtNroOrden.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroOrden.BackColor = System.Drawing.Color.Ivory
        Me.txtNroOrden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroOrden.ForeColor = System.Drawing.Color.Black
        Me.txtNroOrden.KeyEnter = True
        Me.txtNroOrden.Location = New System.Drawing.Point(518, 17)
        Me.txtNroOrden.MaxLength = 15
        Me.txtNroOrden.Name = "txtNroOrden"
        Me.txtNroOrden.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroOrden.ShortcutsEnabled = False
        Me.txtNroOrden.Size = New System.Drawing.Size(212, 21)
        Me.txtNroOrden.TabIndex = 0
        Me.txtNroOrden.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        '
        'txtDP
        '
        Me.txtDP.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDP.BackColor = System.Drawing.Color.Ivory
        Me.txtDP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDP.ForeColor = System.Drawing.Color.Black
        Me.txtDP.KeyEnter = True
        Me.txtDP.Location = New System.Drawing.Point(688, 100)
        Me.txtDP.Name = "txtDP"
        Me.txtDP.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDP.ShortcutsEnabled = False
        Me.txtDP.Size = New System.Drawing.Size(43, 21)
        Me.txtDP.TabIndex = 7
        Me.txtDP.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboDP
        '
        Me.cboDP.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboDP.BackColor = System.Drawing.Color.Ivory
        Me.cboDP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDP.ForeColor = System.Drawing.Color.Black
        Me.cboDP.FormattingEnabled = True
        Me.cboDP.Items.AddRange(New Object() {"DETRACCION", "PERCEPCION"})
        Me.cboDP.KeyEnter = True
        Me.cboDP.Location = New System.Drawing.Point(518, 102)
        Me.cboDP.Name = "cboDP"
        Me.cboDP.Size = New System.Drawing.Size(136, 21)
        Me.cboDP.TabIndex = 6
        '
        'chkIGV
        '
        Me.chkIGV.AutoSize = True
        Me.chkIGV.Location = New System.Drawing.Point(329, 46)
        Me.chkIGV.Name = "chkIGV"
        Me.chkIGV.Size = New System.Drawing.Size(48, 17)
        Me.chkIGV.TabIndex = 3
        Me.chkIGV.Text = "IGV"
        Me.chkIGV.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 105)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Valor Compra"
        '
        'txtInafecto
        '
        Me.txtInafecto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtInafecto.BackColor = System.Drawing.Color.Gainsboro
        Me.txtInafecto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtInafecto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtInafecto.Enabled = False
        Me.txtInafecto.ForeColor = System.Drawing.Color.Black
        Me.txtInafecto.KeyEnter = True
        Me.txtInafecto.Location = New System.Drawing.Point(518, 44)
        Me.txtInafecto.Name = "txtInafecto"
        Me.txtInafecto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtInafecto.ShortcutsEnabled = False
        Me.txtInafecto.Size = New System.Drawing.Size(212, 21)
        Me.txtInafecto.TabIndex = 7
        Me.txtInafecto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInafecto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtValorCompra
        '
        Me.txtValorCompra.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtValorCompra.BackColor = System.Drawing.Color.Gainsboro
        Me.txtValorCompra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtValorCompra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtValorCompra.Enabled = False
        Me.txtValorCompra.ForeColor = System.Drawing.Color.Black
        Me.txtValorCompra.KeyEnter = True
        Me.txtValorCompra.Location = New System.Drawing.Point(112, 102)
        Me.txtValorCompra.Name = "txtValorCompra"
        Me.txtValorCompra.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtValorCompra.ShortcutsEnabled = False
        Me.txtValorCompra.Size = New System.Drawing.Size(212, 21)
        Me.txtValorCompra.TabIndex = 5
        Me.txtValorCompra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtValorCompra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(425, 50)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(87, 13)
        Me.BeLabel3.TabIndex = 14
        Me.BeLabel3.Text = "Valor Inafecto"
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporteTotal.BackColor = System.Drawing.Color.Ivory
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteTotal.ForeColor = System.Drawing.Color.Black
        Me.txtImporteTotal.KeyEnter = True
        Me.txtImporteTotal.Location = New System.Drawing.Point(112, 44)
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporteTotal.ShortcutsEnabled = False
        Me.txtImporteTotal.Size = New System.Drawing.Size(211, 21)
        Me.txtImporteTotal.TabIndex = 2
        Me.txtImporteTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(425, 79)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(75, 13)
        Me.BeLabel2.TabIndex = 4
        Me.BeLabel2.Text = "Tasa IGV %"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(6, 77)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(79, 13)
        Me.BeLabel1.TabIndex = 2
        Me.BeLabel1.Text = "Importe IGV"
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(112, 17)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(211, 21)
        Me.cboMoneda.TabIndex = 1
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(6, 22)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel12.TabIndex = 0
        Me.BeLabel12.Text = "Moneda"
        '
        'txtImporteIGV
        '
        Me.txtImporteIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporteIGV.BackColor = System.Drawing.Color.Gainsboro
        Me.txtImporteIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteIGV.Enabled = False
        Me.txtImporteIGV.ForeColor = System.Drawing.Color.Black
        Me.txtImporteIGV.KeyEnter = True
        Me.txtImporteIGV.Location = New System.Drawing.Point(112, 73)
        Me.txtImporteIGV.MaxLength = 20
        Me.txtImporteIGV.Name = "txtImporteIGV"
        Me.txtImporteIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporteIGV.ShortcutsEnabled = False
        Me.txtImporteIGV.Size = New System.Drawing.Size(212, 21)
        Me.txtImporteIGV.TabIndex = 4
        Me.txtImporteIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtTasaIGV
        '
        Me.txtTasaIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTasaIGV.BackColor = System.Drawing.Color.Gainsboro
        Me.txtTasaIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTasaIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTasaIGV.Enabled = False
        Me.txtTasaIGV.ForeColor = System.Drawing.Color.Black
        Me.txtTasaIGV.KeyEnter = True
        Me.txtTasaIGV.Location = New System.Drawing.Point(518, 73)
        Me.txtTasaIGV.MaxLength = 1000
        Me.txtTasaIGV.Name = "txtTasaIGV"
        Me.txtTasaIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTasaIGV.ShortcutsEnabled = False
        Me.txtTasaIGV.Size = New System.Drawing.Size(212, 21)
        Me.txtTasaIGV.TabIndex = 3
        Me.txtTasaIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTasaIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(6, 48)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(97, 13)
        Me.BeLabel8.TabIndex = 6
        Me.BeLabel8.Text = "Importe Total"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnVerFacturas)
        Me.GroupBox2.Controls.Add(Me.btnVerValorizaciones)
        Me.GroupBox2.Controls.Add(Me.dtpFechaVen)
        Me.GroupBox2.Controls.Add(Me.dtpFechaDoc)
        Me.GroupBox2.Controls.Add(Me.dtpFechaBase)
        Me.GroupBox2.Controls.Add(Me.txtNroDocumento)
        Me.GroupBox2.Controls.Add(Me.txtSerie)
        Me.GroupBox2.Controls.Add(Me.BeLabel29)
        Me.GroupBox2.Controls.Add(Me.BeLabel28)
        Me.GroupBox2.Controls.Add(Me.txtDias)
        Me.GroupBox2.Controls.Add(Me.BeLabel23)
        Me.GroupBox2.Controls.Add(Me.BeLabel22)
        Me.GroupBox2.Controls.Add(Me.BeLabel21)
        Me.GroupBox2.Controls.Add(Me.cboDocumento)
        Me.GroupBox2.Controls.Add(Me.BeLabel20)
        Me.GroupBox2.Controls.Add(Me.BeLabel19)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.DarkRed
        Me.GroupBox2.Location = New System.Drawing.Point(5, 107)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(822, 73)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'btnVerFacturas
        '
        Me.btnVerFacturas.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnVerFacturas.Enabled = False
        Me.btnVerFacturas.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_2
        Me.btnVerFacturas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVerFacturas.Location = New System.Drawing.Point(508, 17)
        Me.btnVerFacturas.Name = "btnVerFacturas"
        Me.btnVerFacturas.Size = New System.Drawing.Size(73, 23)
        Me.btnVerFacturas.TabIndex = 13
        Me.btnVerFacturas.Text = "    &Factura"
        Me.btnVerFacturas.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnVerFacturas.UseVisualStyleBackColor = True
        Me.btnVerFacturas.Visible = False
        '
        'btnVerValorizaciones
        '
        Me.btnVerValorizaciones.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnVerValorizaciones.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_1
        Me.btnVerValorizaciones.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVerValorizaciones.Location = New System.Drawing.Point(507, 17)
        Me.btnVerValorizaciones.Name = "btnVerValorizaciones"
        Me.btnVerValorizaciones.Size = New System.Drawing.Size(97, 23)
        Me.btnVerValorizaciones.TabIndex = 12
        Me.btnVerValorizaciones.Text = "Valorizaci�n"
        Me.btnVerValorizaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnVerValorizaciones.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnVerValorizaciones.UseVisualStyleBackColor = True
        Me.btnVerValorizaciones.Visible = False
        '
        'dtpFechaVen
        '
        Me.dtpFechaVen.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVen.Location = New System.Drawing.Point(692, 44)
        Me.dtpFechaVen.Name = "dtpFechaVen"
        Me.dtpFechaVen.Size = New System.Drawing.Size(90, 21)
        Me.dtpFechaVen.TabIndex = 6
        '
        'dtpFechaDoc
        '
        Me.dtpFechaDoc.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaDoc.Location = New System.Drawing.Point(508, 45)
        Me.dtpFechaDoc.Name = "dtpFechaDoc"
        Me.dtpFechaDoc.Size = New System.Drawing.Size(90, 21)
        Me.dtpFechaDoc.TabIndex = 5
        '
        'dtpFechaBase
        '
        Me.dtpFechaBase.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaBase.Location = New System.Drawing.Point(89, 44)
        Me.dtpFechaBase.Name = "dtpFechaBase"
        Me.dtpFechaBase.Size = New System.Drawing.Size(176, 21)
        Me.dtpFechaBase.TabIndex = 3
        '
        'txtNroDocumento
        '
        Me.txtNroDocumento.BackColor = System.Drawing.Color.Ivory
        Me.txtNroDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroDocumento.Location = New System.Drawing.Point(414, 18)
        Me.txtNroDocumento.Name = "txtNroDocumento"
        Me.txtNroDocumento.Size = New System.Drawing.Size(87, 21)
        Me.txtNroDocumento.TabIndex = 2
        '
        'txtSerie
        '
        Me.txtSerie.BackColor = System.Drawing.Color.Ivory
        Me.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerie.Location = New System.Drawing.Point(349, 18)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(52, 21)
        Me.txtSerie.TabIndex = 1
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.ForeColor = System.Drawing.Color.Black
        Me.BeLabel29.Location = New System.Drawing.Point(293, 22)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel29.TabIndex = 2
        Me.BeLabel29.Text = "N�mero"
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.ForeColor = System.Drawing.Color.Black
        Me.BeLabel28.Location = New System.Drawing.Point(402, 24)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(12, 13)
        Me.BeLabel28.TabIndex = 4
        Me.BeLabel28.Text = "-"
        '
        'txtDias
        '
        Me.txtDias.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDias.BackColor = System.Drawing.Color.Ivory
        Me.txtDias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDias.ForeColor = System.Drawing.Color.Black
        Me.txtDias.KeyEnter = True
        Me.txtDias.Location = New System.Drawing.Point(349, 44)
        Me.txtDias.MaxLength = 4
        Me.txtDias.Name = "txtDias"
        Me.txtDias.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDias.ShortcutsEnabled = False
        Me.txtDias.Size = New System.Drawing.Size(53, 21)
        Me.txtDias.TabIndex = 4
        Me.txtDias.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(267, 49)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel23.TabIndex = 8
        Me.BeLabel23.Text = "Dias Vencto."
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(603, 48)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(86, 13)
        Me.BeLabel22.TabIndex = 12
        Me.BeLabel22.Text = "Fecha Vencto."
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.ForeColor = System.Drawing.Color.Black
        Me.BeLabel21.Location = New System.Drawing.Point(411, 48)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(88, 13)
        Me.BeLabel21.TabIndex = 10
        Me.BeLabel21.Text = "Fecha Emision"
        '
        'cboDocumento
        '
        Me.cboDocumento.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboDocumento.BackColor = System.Drawing.Color.Ivory
        Me.cboDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumento.ForeColor = System.Drawing.Color.Black
        Me.cboDocumento.FormattingEnabled = True
        Me.cboDocumento.KeyEnter = True
        Me.cboDocumento.Location = New System.Drawing.Point(89, 16)
        Me.cboDocumento.Name = "cboDocumento"
        Me.cboDocumento.Size = New System.Drawing.Size(176, 21)
        Me.cboDocumento.TabIndex = 0
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.ForeColor = System.Drawing.Color.Black
        Me.BeLabel20.Location = New System.Drawing.Point(6, 48)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(72, 13)
        Me.BeLabel20.TabIndex = 6
        Me.BeLabel20.Text = "Fecha Base"
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.ForeColor = System.Drawing.Color.Black
        Me.BeLabel19.Location = New System.Drawing.Point(6, 19)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(72, 13)
        Me.BeLabel19.TabIndex = 0
        Me.BeLabel19.Text = "Documento"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnValidarRuc)
        Me.GroupBox1.Controls.Add(Me.txtGuiaRemision)
        Me.GroupBox1.Controls.Add(Me.BeLabel42)
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.BeLabel41)
        Me.GroupBox1.Controls.Add(Me.btnImportar)
        Me.GroupBox1.Controls.Add(Me.cboEstado)
        Me.GroupBox1.Controls.Add(Me.BeLabel27)
        Me.GroupBox1.Controls.Add(Me.BeLabel32)
        Me.GroupBox1.Controls.Add(Me.txtCod)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.cboTipoAcreedor)
        Me.GroupBox1.Controls.Add(Me.lblDescripcionCodigo)
        Me.GroupBox1.Controls.Add(Me.BeLabel17)
        Me.GroupBox1.Controls.Add(Me.BeLabel18)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(5, -2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(822, 109)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'btnValidarRuc
        '
        Me.btnValidarRuc.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnValidarRuc.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Edit_
        Me.btnValidarRuc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnValidarRuc.Location = New System.Drawing.Point(313, 75)
        Me.btnValidarRuc.Name = "btnValidarRuc"
        Me.btnValidarRuc.Size = New System.Drawing.Size(103, 23)
        Me.btnValidarRuc.TabIndex = 14
        Me.btnValidarRuc.Text = "ValidarRuc"
        Me.btnValidarRuc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnValidarRuc.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnValidarRuc.UseVisualStyleBackColor = True
        '
        'txtGuiaRemision
        '
        Me.txtGuiaRemision.BackColor = System.Drawing.Color.Ivory
        Me.txtGuiaRemision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGuiaRemision.Location = New System.Drawing.Point(495, 42)
        Me.txtGuiaRemision.MaxLength = 11
        Me.txtGuiaRemision.Name = "txtGuiaRemision"
        Me.txtGuiaRemision.Size = New System.Drawing.Size(110, 21)
        Me.txtGuiaRemision.TabIndex = 12
        Me.txtGuiaRemision.Visible = False
        '
        'BeLabel42
        '
        Me.BeLabel42.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel42.AutoSize = True
        Me.BeLabel42.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel42.ForeColor = System.Drawing.Color.Black
        Me.BeLabel42.Location = New System.Drawing.Point(402, 48)
        Me.BeLabel42.Name = "BeLabel42"
        Me.BeLabel42.Size = New System.Drawing.Size(89, 13)
        Me.BeLabel42.TabIndex = 13
        Me.BeLabel42.Text = "Guia Remision"
        Me.BeLabel42.Visible = False
        '
        'txtCodigo
        '
        Me.txtCodigo.BackColor = System.Drawing.Color.Ivory
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.Location = New System.Drawing.Point(89, 75)
        Me.txtCodigo.MaxLength = 11
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(217, 21)
        Me.txtCodigo.TabIndex = 1
        '
        'BeLabel41
        '
        Me.BeLabel41.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel41.AutoSize = True
        Me.BeLabel41.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel41.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel41.ForeColor = System.Drawing.Color.Black
        Me.BeLabel41.Location = New System.Drawing.Point(712, 58)
        Me.BeLabel41.Name = "BeLabel41"
        Me.BeLabel41.Size = New System.Drawing.Size(44, 13)
        Me.BeLabel41.TabIndex = 11
        Me.BeLabel41.Text = "(*.xls)"
        '
        'btnImportar
        '
        Me.btnImportar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnImportar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImportar.Location = New System.Drawing.Point(633, 51)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(75, 23)
        Me.btnImportar.TabIndex = 10
        Me.btnImportar.Text = "Importar"
        Me.btnImportar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'cboEstado
        '
        Me.cboEstado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstado.BackColor = System.Drawing.Color.Ivory
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.ForeColor = System.Drawing.Color.Black
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.KeyEnter = True
        Me.cboEstado.Location = New System.Drawing.Point(633, 18)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(127, 21)
        Me.cboEstado.TabIndex = 9
        '
        'BeLabel27
        '
        Me.BeLabel27.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel27.AutoSize = True
        Me.BeLabel27.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel27.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel27.ForeColor = System.Drawing.Color.Black
        Me.BeLabel27.Location = New System.Drawing.Point(584, 22)
        Me.BeLabel27.Name = "BeLabel27"
        Me.BeLabel27.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel27.TabIndex = 8
        Me.BeLabel27.Text = "Estado"
        '
        'BeLabel32
        '
        Me.BeLabel32.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel32.AutoSize = True
        Me.BeLabel32.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel32.ForeColor = System.Drawing.Color.Black
        Me.BeLabel32.Location = New System.Drawing.Point(6, 79)
        Me.BeLabel32.Name = "BeLabel32"
        Me.BeLabel32.Size = New System.Drawing.Size(32, 13)
        Me.BeLabel32.TabIndex = 4
        Me.BeLabel32.Text = "RUC"
        '
        'txtCod
        '
        Me.txtCod.BackColor = System.Drawing.Color.Gainsboro
        Me.txtCod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCod.Enabled = False
        Me.txtCod.Location = New System.Drawing.Point(89, 46)
        Me.txtCod.Name = "txtCod"
        Me.txtCod.Size = New System.Drawing.Size(217, 21)
        Me.txtCod.TabIndex = 3
        '
        'btnBuscar
        '
        Me.btnBuscar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnBuscar.Enabled = False
        Me.btnBuscar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_2
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuscar.Location = New System.Drawing.Point(313, 74)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(73, 23)
        Me.btnBuscar.TabIndex = 2
        Me.btnBuscar.Text = "    &Buscar"
        Me.btnBuscar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnBuscar.UseVisualStyleBackColor = True
        Me.btnBuscar.Visible = False
        '
        'cboTipoAcreedor
        '
        Me.cboTipoAcreedor.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoAcreedor.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoAcreedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoAcreedor.ForeColor = System.Drawing.Color.Black
        Me.cboTipoAcreedor.FormattingEnabled = True
        Me.cboTipoAcreedor.KeyEnter = True
        Me.cboTipoAcreedor.Location = New System.Drawing.Point(89, 18)
        Me.cboTipoAcreedor.Name = "cboTipoAcreedor"
        Me.cboTipoAcreedor.Size = New System.Drawing.Size(217, 21)
        Me.cboTipoAcreedor.TabIndex = 0
        '
        'lblDescripcionCodigo
        '
        Me.lblDescripcionCodigo.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblDescripcionCodigo.AutoSize = True
        Me.lblDescripcionCodigo.BackColor = System.Drawing.Color.Transparent
        Me.lblDescripcionCodigo.ForeColor = System.Drawing.Color.Black
        Me.lblDescripcionCodigo.Location = New System.Drawing.Point(315, 51)
        Me.lblDescripcionCodigo.Name = "lblDescripcionCodigo"
        Me.lblDescripcionCodigo.Size = New System.Drawing.Size(0, 13)
        Me.lblDescripcionCodigo.TabIndex = 6
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(6, 22)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(71, 13)
        Me.BeLabel17.TabIndex = 0
        Me.BeLabel17.Text = "Tipo Anexo"
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(6, 50)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel18.TabIndex = 2
        Me.BeLabel18.Text = "C�digo"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(11, 465)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(59, 13)
        Me.BeLabel7.TabIndex = 4
        Me.BeLabel7.Text = "Situaci�n"
        Me.BeLabel7.Visible = False
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.txtNroDocumentoBuscar)
        Me.Panel5.Controls.Add(Me.BeLabel35)
        Me.Panel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel5.Location = New System.Drawing.Point(448, 192)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(419, 63)
        Me.Panel5.TabIndex = 15
        Me.Panel5.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(7, 25)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "N� Doc."
        '
        'txtNroDocumentoBuscar
        '
        Me.txtNroDocumentoBuscar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroDocumentoBuscar.BackColor = System.Drawing.Color.Ivory
        Me.txtNroDocumentoBuscar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroDocumentoBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroDocumentoBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtNroDocumentoBuscar.KeyEnter = True
        Me.txtNroDocumentoBuscar.Location = New System.Drawing.Point(68, 17)
        Me.txtNroDocumentoBuscar.Name = "txtNroDocumentoBuscar"
        Me.txtNroDocumentoBuscar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroDocumentoBuscar.ShortcutsEnabled = False
        Me.txtNroDocumentoBuscar.Size = New System.Drawing.Size(319, 21)
        Me.txtNroDocumentoBuscar.TabIndex = 3
        Me.txtNroDocumentoBuscar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel35
        '
        Me.BeLabel35.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel35.AutoSize = True
        Me.BeLabel35.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel35.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel35.ForeColor = System.Drawing.Color.Red
        Me.BeLabel35.Location = New System.Drawing.Point(398, 0)
        Me.BeLabel35.Name = "BeLabel35"
        Me.BeLabel35.Size = New System.Drawing.Size(18, 16)
        Me.BeLabel35.TabIndex = 2
        Me.BeLabel35.Text = "X"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Timer3
        '
        '
        'Timer4
        '
        '
        'Timer5
        '
        '
        'Timer6
        '
        '
        'Timer7
        '
        '
        'Timer8
        '
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvDetalle.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetalle.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Anexo, Me.Column3, Me.IdRegistro, Me.Codigo, Me.Descripcion, Me.Column7, Me.Column8, Me.VISTO, Me.Column9, Me.Column10, Me.Column11, Me.Column23, Me.Column24, Me.IdDoc, Me.IdMoneda, Me.Column14, Me.Column15, Me.Column2, Me.Column4, Me.Column5, Me.Column6, Me.Column20, Me.Column17, Me.Column25, Me.Column26, Me.Column27})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalle.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDetalle.EnableHeadersVisualStyles = False
        Me.dgvDetalle.Location = New System.Drawing.Point(-1, -1)
        Me.dgvDetalle.MultiSelect = False
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(65535, 2190)
        Me.dgvDetalle.TabIndex = 17
        '
        'Anexo
        '
        Me.Anexo.DataPropertyName = "TipoAcreedorId"
        Me.Anexo.HeaderText = "Anexo"
        Me.Anexo.Name = "Anexo"
        Me.Anexo.ReadOnly = True
        Me.Anexo.Visible = False
        Me.Anexo.Width = 40
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "FechaCreacion2"
        Me.Column3.HeaderText = "F.Emision"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 70
        '
        'IdRegistro
        '
        Me.IdRegistro.DataPropertyName = "IdRegistro"
        Me.IdRegistro.HeaderText = "IdRegistro"
        Me.IdRegistro.Name = "IdRegistro"
        Me.IdRegistro.ReadOnly = True
        Me.IdRegistro.Visible = False
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "CodProveedor"
        Me.Codigo.HeaderText = "Ruc"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 80
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "NombreProveedor"
        Me.Descripcion.HeaderText = "Razon Social"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 130
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "DescripDoc"
        Me.Column7.HeaderText = "Documento"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 80
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Ndoc"
        Me.Column8.HeaderText = "Num. Doc."
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 90
        '
        'VISTO
        '
        Me.VISTO.DataPropertyName = "RECE_CONTA"
        Me.VISTO.HeaderText = "VB"
        Me.VISTO.Name = "VISTO"
        Me.VISTO.ReadOnly = True
        Me.VISTO.Width = 50
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "MonDescripcion"
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Column9.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column9.HeaderText = "Moneda"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 80
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column10.HeaderText = "Importe"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 75
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "Registrado"
        Me.Column11.HeaderText = "Registrado"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 90
        '
        'Column23
        '
        Me.Column23.DataPropertyName = "Provisionado"
        Me.Column23.HeaderText = "Provisionado"
        Me.Column23.Name = "Column23"
        Me.Column23.ReadOnly = True
        Me.Column23.Width = 80
        '
        'Column24
        '
        Me.Column24.DataPropertyName = "Cancelado"
        Me.Column24.HeaderText = "Cancelado"
        Me.Column24.Name = "Column24"
        Me.Column24.ReadOnly = True
        Me.Column24.Width = 80
        '
        'IdDoc
        '
        Me.IdDoc.DataPropertyName = "IdDocumento"
        Me.IdDoc.HeaderText = "IdDoc"
        Me.IdDoc.Name = "IdDoc"
        Me.IdDoc.ReadOnly = True
        Me.IdDoc.Visible = False
        '
        'IdMoneda
        '
        Me.IdMoneda.DataPropertyName = "IdMoneda"
        Me.IdMoneda.HeaderText = "IdMoneda"
        Me.IdMoneda.Name = "IdMoneda"
        Me.IdMoneda.ReadOnly = True
        Me.IdMoneda.Visible = False
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "RUC"
        Me.Column14.HeaderText = "RUC"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Visible = False
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "Estados"
        Me.Column15.HeaderText = "Estado"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Width = 65
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "SituacionId"
        Me.Column2.HeaderText = "SituacionId"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Visible = False
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "EstadoPrePro"
        Me.Column4.HeaderText = "IdEstado"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Visible = False
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "DP"
        Me.Column5.HeaderText = "DP"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 35
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "MtoDP"
        Me.Column6.HeaderText = "%"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 40
        '
        'Column20
        '
        Me.Column20.DataPropertyName = "IdOC"
        Me.Column20.HeaderText = "Codigo"
        Me.Column20.Name = "Column20"
        Me.Column20.ReadOnly = True
        Me.Column20.Visible = False
        '
        'Column17
        '
        Me.Column17.DataPropertyName = "NroOC"
        Me.Column17.HeaderText = "Nro Orden"
        Me.Column17.Name = "Column17"
        Me.Column17.ReadOnly = True
        Me.Column17.Width = 90
        '
        'Column25
        '
        Me.Column25.DataPropertyName = "Numero"
        Me.Column25.HeaderText = "Cargo"
        Me.Column25.Name = "Column25"
        Me.Column25.ReadOnly = True
        '
        'Column26
        '
        Me.Column26.DataPropertyName = "NCCodFT"
        Me.Column26.HeaderText = "NCCodFT"
        Me.Column26.Name = "Column26"
        Me.Column26.ReadOnly = True
        Me.Column26.Visible = False
        '
        'Column27
        '
        Me.Column27.DataPropertyName = "NCCodFTEmpr"
        Me.Column27.HeaderText = "NCCodFTEmpr"
        Me.Column27.Name = "Column27"
        Me.Column27.ReadOnly = True
        Me.Column27.Visible = False
        '
        'Timer9
        '
        '
        'Timer10
        '
        '
        'Timer11
        '
        '
        'Panel6
        '
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Controls.Add(Me.GroupBox7)
        Me.Panel6.Controls.Add(Me.BeLabel33)
        Me.Panel6.Location = New System.Drawing.Point(427, 141)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(457, 232)
        Me.Panel6.TabIndex = 18
        Me.Panel6.Visible = False
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.GroupBox8)
        Me.GroupBox7.Controls.Add(Me.rdb1)
        Me.GroupBox7.Controls.Add(Me.btnImprimir)
        Me.GroupBox7.Location = New System.Drawing.Point(10, 19)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(442, 196)
        Me.GroupBox7.TabIndex = 10
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Reporte de Documentos Registrados"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.chkTodosUsuarios)
        Me.GroupBox8.Controls.Add(Me.cboUsuarios)
        Me.GroupBox8.Controls.Add(Me.dtpF1)
        Me.GroupBox8.Controls.Add(Me.chkTodos2)
        Me.GroupBox8.Controls.Add(Me.rdb2)
        Me.GroupBox8.Controls.Add(Me.cboTodosProv)
        Me.GroupBox8.Controls.Add(Me.dtpF2)
        Me.GroupBox8.Controls.Add(Me.Label5)
        Me.GroupBox8.Location = New System.Drawing.Point(7, 42)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(429, 119)
        Me.GroupBox8.TabIndex = 12
        Me.GroupBox8.TabStop = False
        '
        'chkTodosUsuarios
        '
        Me.chkTodosUsuarios.AutoSize = True
        Me.chkTodosUsuarios.Checked = True
        Me.chkTodosUsuarios.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTodosUsuarios.Location = New System.Drawing.Point(6, 75)
        Me.chkTodosUsuarios.Name = "chkTodosUsuarios"
        Me.chkTodosUsuarios.Size = New System.Drawing.Size(109, 17)
        Me.chkTodosUsuarios.TabIndex = 22
        Me.chkTodosUsuarios.Text = "Todos lo usuarios"
        Me.chkTodosUsuarios.UseVisualStyleBackColor = True
        '
        'cboUsuarios
        '
        Me.cboUsuarios.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboUsuarios.BackColor = System.Drawing.Color.Ivory
        Me.cboUsuarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUsuarios.ForeColor = System.Drawing.Color.Black
        Me.cboUsuarios.FormattingEnabled = True
        Me.cboUsuarios.KeyEnter = True
        Me.cboUsuarios.Location = New System.Drawing.Point(122, 71)
        Me.cboUsuarios.Name = "cboUsuarios"
        Me.cboUsuarios.Size = New System.Drawing.Size(278, 21)
        Me.cboUsuarios.TabIndex = 20
        '
        'dtpF1
        '
        Me.dtpF1.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpF1.CustomFormat = ""
        Me.dtpF1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpF1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF1.KeyEnter = True
        Me.dtpF1.Location = New System.Drawing.Point(122, 16)
        Me.dtpF1.Name = "dtpF1"
        Me.dtpF1.Size = New System.Drawing.Size(84, 20)
        Me.dtpF1.TabIndex = 7
        Me.dtpF1.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpF1.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'chkTodos2
        '
        Me.chkTodos2.AutoSize = True
        Me.chkTodos2.Checked = True
        Me.chkTodos2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTodos2.Location = New System.Drawing.Point(6, 46)
        Me.chkTodos2.Name = "chkTodos2"
        Me.chkTodos2.Size = New System.Drawing.Size(56, 17)
        Me.chkTodos2.TabIndex = 11
        Me.chkTodos2.Text = "Todos"
        Me.chkTodos2.UseVisualStyleBackColor = True
        '
        'rdb2
        '
        Me.rdb2.AutoSize = True
        Me.rdb2.Location = New System.Drawing.Point(6, 19)
        Me.rdb2.Name = "rdb2"
        Me.rdb2.Size = New System.Drawing.Size(113, 17)
        Me.rdb2.TabIndex = 6
        Me.rdb2.TabStop = True
        Me.rdb2.Text = "Documentos Entre"
        Me.rdb2.UseVisualStyleBackColor = True
        '
        'cboTodosProv
        '
        Me.cboTodosProv.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTodosProv.BackColor = System.Drawing.Color.Ivory
        Me.cboTodosProv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTodosProv.ForeColor = System.Drawing.Color.Black
        Me.cboTodosProv.FormattingEnabled = True
        Me.cboTodosProv.KeyEnter = True
        Me.cboTodosProv.Location = New System.Drawing.Point(122, 44)
        Me.cboTodosProv.Name = "cboTodosProv"
        Me.cboTodosProv.Size = New System.Drawing.Size(278, 21)
        Me.cboTodosProv.TabIndex = 10
        '
        'dtpF2
        '
        Me.dtpF2.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpF2.CustomFormat = ""
        Me.dtpF2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpF2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF2.KeyEnter = True
        Me.dtpF2.Location = New System.Drawing.Point(235, 17)
        Me.dtpF2.Name = "dtpF2"
        Me.dtpF2.Size = New System.Drawing.Size(84, 20)
        Me.dtpF2.TabIndex = 8
        Me.dtpF2.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpF2.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(217, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(12, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "y"
        '
        'rdb1
        '
        Me.rdb1.AutoSize = True
        Me.rdb1.Checked = True
        Me.rdb1.Location = New System.Drawing.Point(13, 19)
        Me.rdb1.Name = "rdb1"
        Me.rdb1.Size = New System.Drawing.Size(123, 17)
        Me.rdb1.TabIndex = 5
        Me.rdb1.TabStop = True
        Me.rdb1.Text = "Documentos del D�a"
        Me.rdb1.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(352, 167)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 4
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'BeLabel33
        '
        Me.BeLabel33.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel33.AutoSize = True
        Me.BeLabel33.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel33.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel33.ForeColor = System.Drawing.Color.Red
        Me.BeLabel33.Location = New System.Drawing.Point(438, 2)
        Me.BeLabel33.Name = "BeLabel33"
        Me.BeLabel33.Size = New System.Drawing.Size(18, 16)
        Me.BeLabel33.TabIndex = 3
        Me.BeLabel33.Text = "X"
        '
        'AreaBindingSource
        '
        Me.AreaBindingSource.DataMember = "Area"
        '
        'frmRegistroDocumento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1255, 472)
        Me.ForeColor = System.Drawing.Color.Black
        Me.MaximizeBox = False
        Me.Name = "frmRegistroDocumento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
        Me.Text = "Registro de Comprobantes de Pago"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.dgvGuiaRemision, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvFacturas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvValorizaciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvOrdenes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.dvgListProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTasaIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtImporteIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboDepartamentos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblDescripcionCodigo As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboTipoAcreedor As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDias As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboDocumento As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtImporteTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtValorCompra As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cboFechaCreacion As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents cboFechaRecepcion As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtUsuario As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnBuscar As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel32 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCod As System.Windows.Forms.TextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel35 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboSituacion As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboEstado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel27 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnImportar As ctrLibreria.Controles.BeButton
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtInafecto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents chkIGV As System.Windows.Forms.CheckBox
    Friend WithEvents txtDP As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboDP As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroOrden As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents dgvOrdenes As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodOrden As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodOrden2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel39 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel40 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel41 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNroDocumentoBuscar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents txtNroDocumento As System.Windows.Forms.TextBox
    Friend WithEvents txtSerie As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents Timer4 As System.Windows.Forms.Timer
    Friend WithEvents dtpFechaVen As System.Windows.Forms.DateTimePicker
    Friend WithEvents Timer5 As System.Windows.Forms.Timer
    Friend WithEvents Timer6 As System.Windows.Forms.Timer
    Friend WithEvents Timer7 As System.Windows.Forms.Timer
    Friend WithEvents Timer8 As System.Windows.Forms.Timer
    Friend WithEvents dgvDetalle As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents Timer9 As System.Windows.Forms.Timer
    Friend WithEvents BeLabel30 As ctrLibreria.Controles.BeLabel
    Public WithEvents dtpFechaBase As System.Windows.Forms.DateTimePicker
    Public WithEvents dtpFechaDoc As System.Windows.Forms.DateTimePicker
    Friend WithEvents Timer10 As System.Windows.Forms.Timer
    Friend WithEvents txtCodProvLog As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents txtRucAcree As System.Windows.Forms.TextBox
    Friend WithEvents txtDireccionAcree As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcionAcreedor As System.Windows.Forms.TextBox
    Friend WithEvents txtCodAcreedor As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNroDocIdent As System.Windows.Forms.TextBox
    Friend WithEvents rdbJuridica As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNatural As System.Windows.Forms.RadioButton
    Friend WithEvents cboTipoDocIdent As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel26 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel24 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFiltrar As System.Windows.Forms.TextBox
    Friend WithEvents rdbDescr As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCodigo As System.Windows.Forms.RadioButton
    Friend WithEvents BeButton1 As ctrLibreria.Controles.BeButton
    Friend WithEvents rdbNumDoc As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRuc As System.Windows.Forms.RadioButton
    Friend WithEvents dvgListProveedores As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AnaliticoCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AnaliticoDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RUC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocIdentidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Timer11 As System.Windows.Forms.Timer
    Friend WithEvents BeLabel31 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel33 As ctrLibreria.Controles.BeLabel
    Friend WithEvents rdb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rdb1 As System.Windows.Forms.RadioButton
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpF2 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpF1 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents cboTodosProv As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkTodos2 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents dgvValorizaciones As System.Windows.Forms.DataGridView
    Friend WithEvents btnVerValorizaciones As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel34 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel36 As ctrLibreria.Controles.BeLabel
    Friend WithEvents IdValorizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmprCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ParDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaInicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalVal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents X As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnVerFacturas As ctrLibreria.Controles.BeButton
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel37 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel38 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtBuscarFactura As System.Windows.Forms.TextBox
    Friend WithEvents dgvFacturas As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdRegistroF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmprCodigoF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NCGlosa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NCImporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents XF As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents TIngreso As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents txtBuscaGuiaRemision As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel43 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel44 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvGuiaRemision As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtGuiaRemision As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel42 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnValidarRuc As ctrLibreria.Controles.BeButton
    Friend WithEvents cboUsuarios As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkTodosUsuarios As System.Windows.Forms.CheckBox
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdOrden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrvCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrvRUC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaOC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PdoCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents ERPDHMONTDataSet As CapaPreTesoreria.ERPDHMONTDataSet
    Friend WithEvents AreaBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents AreaTableAdapter As CapaPreTesoreria.ERPDHMONTDataSetTableAdapters.AreaTableAdapter
    Friend WithEvents Anexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdRegistro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VISTO As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdDoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtCajaOrigen As System.Windows.Forms.TextBox

End Class
