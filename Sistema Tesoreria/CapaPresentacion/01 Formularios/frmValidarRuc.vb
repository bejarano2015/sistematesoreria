﻿Public Class frmValidarRuc

    Dim oRuc As String
    Dim RasonSocial As String
    Dim eSTADO As String
    Dim Condicion As String
    Dim dIRECCION As String
#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmValidarRuc = Nothing
    'Public Shared Function Instance() As frmValidarRuc
    '    If frmInstance Is Nothing Then
    '        frmInstance = New frmValidarRuc
    '    End If
    '    frmInstance.BringToFront()
    '    Return frmInstance
    'End Function
    Private Sub frmValidarRuc_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region
    Public Sub New(ByVal v_NUmeroRuc As String, ByVal v_RasonSocial As String, ByVal v_eSTADO As String, ByVal v_Condicion As String, ByVal v_dIRECCION As String)
        InitializeComponent()
        txtRuc.Text = v_NUmeroRuc
        txtRazSoc.Text = v_RasonSocial
        txtEst.Text = v_eSTADO
        txtCon.Text = v_Condicion
        txtDir.Text = v_dIRECCION
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class