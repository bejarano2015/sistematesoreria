Imports CapaNegocios
Imports CapaEntidad

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporGestionDocumentoPagosTodo

    'Public nroArqueo As String
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    'Dim rptGastos As New rptGasGene
    'Dim rptGastos As New rptGasGeneDetallado

    Private eGestionDocumentosPagos As clsGestionDocumentosPagos
    Private odtGestionPago As New DataTable
    Private rptPagos As New rptGestionDocumentosPagosTodo

    Private opcion As Decimal = 1
    Private docOrigen As Decimal = 1
    Private nombreorigen As String

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmReporGestionDocumentoPagosTodo = Nothing
    Public Shared Function Instance() As frmReporGestionDocumentoPagosTodo
        If frmInstance Is Nothing Then
            frmInstance = New frmReporGestionDocumentoPagosTodo
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

#End Region

#Region " Prodedimientos de evento de controles "

    Private Sub frmReporGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eGestionDocumentosPagos = New clsGestionDocumentosPagos()

        cboDocumento.SelectedIndex = 0
        dtFechaInicio.Value = New DateTime(DateTime.Today.Year, DateTime.Today.Month, 1)
        dtFechaFin.Value = dtFechaInicio.Value.AddMonths(1).AddDays(-1)
    End Sub

    Private Sub frmReporGastos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub
    Private Sub frmReporGastos_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

    Private Sub cboDocumento_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboDocumento.SelectedIndexChanged
        If cboDocumento.SelectedIndex = 0 Then
            opcion = 1 ' Orden de compra
            docOrigen = 1
            nombreorigen = "ORDEN DE COMPRA"
        ElseIf cboDocumento.SelectedIndex = 1 Then
            opcion = 2 ' Entrega a rendir
            docOrigen = 2 '  2 y 6
            nombreorigen = "ENTREGA A RENDIR"
        ElseIf cboDocumento.SelectedIndex = 2 Then
            opcion = 3 ' Valorización
            docOrigen = 4
            nombreorigen = "VALORIZACIÓN"
        ElseIf cboDocumento.SelectedIndex = 3 Then
            opcion = 4 ' Caja chica
            docOrigen = 3
            nombreorigen = "CAJA CHICA"
        ElseIf cboDocumento.SelectedIndex = 4 Then
            opcion = 5 ' Planillas
            docOrigen = 5
            nombreorigen = "PLANILLAS"
        End If
    End Sub

    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        CargarReportePagos()
    End Sub

#End Region

#Region " Métodos privados "

    Private Sub CargarReportePagos()
        Try
            'Dim odtData As New DataTable

            odtGestionPago = eGestionDocumentosPagos.fCargarReporteTodo(docOrigen, dtFechaInicio.Value, dtFechaFin.Value, opcion)

            Dim rptPagos As New rptGestionDocumentosPagosTodo()
            rptPagos.DataSource = odtGestionPago
            rptPagos.AddItems(gDesEmpresa, gEmprRuc, nombreorigen)

            Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
            instanceReportSource.ReportDocument = rptPagos
            rvReporte.ReportSource = instanceReportSource
            rvReporte.RefreshReport()
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

End Class