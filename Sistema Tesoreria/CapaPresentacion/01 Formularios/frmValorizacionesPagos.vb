Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Windows.Forms

Public Class frmValorizacionesPagos

    Dim dtTable As DataTable
    Public sIdValorizacion As String = ""
    'Public sFormaDetalle As String = ""
    'Public sEmpresaAPrestar As String = ""
    Public Monto As Double = 0
    'Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    'Private eGastosGenerales As clsGastosGenerales
    Dim Importe As Double = 0

    Private eValorizaciones As clsValorizaciones

    Private Sub frmValorizacionesPagos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMonto.Text = Format(Monto, "#,##0.0000")
        lblMonto2.Text = Format(Monto, "0.0000")

        If Trim(sIdValorizacion) <> "" Then
            eValorizaciones = New clsValorizaciones
            dtTable = New DataTable
            dtTable = eValorizaciones.fListarDetPagoValorizacion(Trim(sIdValorizacion), gEmpresa, gPeriodo)

            If dtTable.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
                For y As Integer = 0 To dtTable.Rows.Count - 1
                    dgvDetalle.Rows.Add()
                    dgvDetalle.Rows(y).Cells("FechaPago").Value = Microsoft.VisualBasic.Left(dtTable.Rows(y).Item("FechaPago"), 10) 'dtTable.Rows(y).Item("FechaPago").ToString
                    dgvDetalle.Rows(y).Cells("Numero").Value = dtTable.Rows(y).Item("NumeroPago").ToString
                    dgvDetalle.Rows(y).Cells("Impo").Value = dtTable.Rows(y).Item("ImportePago").ToString
                    dgvDetalle.Rows(y).Cells("Estado").Value = dtTable.Rows(y).Item("Estados").ToString 'dtTable.Rows(y).Item("MetradoPor").ToString
                    dgvDetalle.Rows(y).Cells("EstadoOculto").Value = "1" 'dtTable.Rows(y).Item("MetradoImpor").ToString
                    dgvDetalle.Rows(y).Cells("IdDetallePago").Value = dtTable.Rows(y).Item("IdPago").ToString
                    dgvDetalle.Rows(y).Cells("EstadoPagoOculto").Value = dtTable.Rows(y).Item("EstadoPago").ToString

                    If dtTable.Rows(y).Item("EstadoPago").ToString.Trim = "0" Then
                        'por cancelar
                        dgvDetalle.Rows(y).Cells("FechaPago").ReadOnly = False
                        dgvDetalle.Rows(y).Cells("Numero").ReadOnly = False
                        dgvDetalle.Rows(y).Cells("Impo").ReadOnly = False
                        dgvDetalle.Rows(y).Cells("Estado").ReadOnly = False
                        dgvDetalle.Rows(y).Cells("EstadoOculto").ReadOnly = False
                        dgvDetalle.Rows(y).Cells("IdDetallePago").ReadOnly = False
                        dgvDetalle.Rows(y).Cells("EstadoPagoOculto").ReadOnly = False
                    ElseIf dtTable.Rows(y).Item("EstadoPago").ToString.Trim = "1" Then
                        'cancelados
                        dgvDetalle.Rows(y).Cells("FechaPago").ReadOnly = True
                        dgvDetalle.Rows(y).Cells("Numero").ReadOnly = True
                        dgvDetalle.Rows(y).Cells("Impo").ReadOnly = True
                        dgvDetalle.Rows(y).Cells("Estado").ReadOnly = True
                        dgvDetalle.Rows(y).Cells("EstadoOculto").ReadOnly = True
                        dgvDetalle.Rows(y).Cells("IdDetallePago").ReadOnly = True
                        dgvDetalle.Rows(y).Cells("EstadoPagoOculto").ReadOnly = True

                        dgvDetalle.Rows(y).Cells("FechaPago").Style.ForeColor = Color.Blue
                        dgvDetalle.Rows(y).Cells("Numero").Style.ForeColor = Color.Blue
                        dgvDetalle.Rows(y).Cells("Impo").Style.ForeColor = Color.Blue
                        dgvDetalle.Rows(y).Cells("Estado").Style.ForeColor = Color.Blue
                        dgvDetalle.Rows(y).Cells("EstadoOculto").Style.ForeColor = Color.Blue
                        dgvDetalle.Rows(y).Cells("IdDetallePago").Style.ForeColor = Color.Blue
                        dgvDetalle.Rows(y).Cells("EstadoPagoOculto").Style.ForeColor = Color.Blue
                        'dgvDetalle.Rows(y).Cells("Estado").
                    End If

                Next
                'BeLabel18.Text = "Total de Items : " & dtDetalle.Rows.Count
                Calcular()
            ElseIf dtTable.Rows.Count = 0 Then
                'BeLabel18.Text = "Total de Items : 0"
            End If
        End If

        dgvDetalle.Focus()
        'txtTotal.Text = Format(Monto, "#,##0.00")
    End Sub

    Sub Calcular()
        Importe = 0
        If dgvDetalle.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                Importe = Importe + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("Impo").Value)))
            Next
            txtTotal.Text = Format(Importe, "#,##0.0000")
            Importe = Format(Importe, "#,##0.0000")
        End If
    End Sub


    Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        If dgvDetalle.Rows.Count > 0 Then
            Dim StrCad As String = String.Empty
            If e.ColumnIndex = 2 Then
                If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Value) <> "" Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Value.ToString), "#,##0.0000")
                    If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Value) = True Then
                        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Value.ToString), "#,##0.0000")
                    End If
                Else
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Value = "0.00"
                End If

            ElseIf e.ColumnIndex = 0 Then
                StrCad = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("FechaPago").Value
                Dim FechPago As DateTime
                If StrCad = Nothing Then Exit Sub
                Try
                    FechPago = Convert.ToDateTime(StrCad)
                    'If Convert.ToDateTime(StrCad) Then
                    '    MessageBox.Show("La Fecha de Registro debe ser Mayor o igual a la Fecha de Apertura", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    '    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index)
                    '    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value = ""
                    'Else
                    '    'hacer que cuando se ingrese una fecha se compare con todas las del grid y colocar la fecha mayor en el datapicker final
                    '    '---------------------------------------
                    '    Dim i As Integer
                    '    Dim FechaMayor As DateTime
                    '    If dgvDetalleCajas.Rows.Count = 1 Then
                    '        FechaMayor = dgvDetalleCajas.Rows(0).Cells("Fecha").Value
                    '    Else
                    '        FechaMayor = dgvDetalleCajas.Rows(0).Cells("Fecha").Value
                    '        For i = 0 To dgvDetalleCajas.Rows.Count - 1
                    '            'MessageBox.Show(dgvDetalleCajas.Rows(i).Cells(0).Value)
                    '            If Trim(dgvDetalleCajas.Rows(i).Cells("Fecha").Value) <> "" Then
                    '                'dgvDetalleCajas.Rows(i).Cells(0).Value = FechaMayor
                    '                If (Convert.ToDateTime(dgvDetalleCajas.Rows(i).Cells("Fecha").Value) > FechaMayor) Then
                    '                    FechaMayor = Convert.ToDateTime(dgvDetalleCajas.Rows(i).Cells("Fecha").Value)
                    '                End If
                    '            End If

                    '        Next
                    '    End If
                    '    dtpFechaFin.Value = FechaMayor
                    '    '---------------------------------------
                    'End If
                Catch ex As Exception
                    MessageBox.Show("Ingrese una Fecha Valida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(0, dgvDetalle.CurrentRow.Index)
                    'dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index - 1)
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("FechaPago").Value = ""
                End Try
            End If
            Calcular()
        End If
    End Sub

    Private Sub dgvDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalle.Click
        Calcular()
    End Sub

    Private Sub dgvDetalle_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalle.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex

        If columna = 0 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
                e.KeyChar = Chr(0)
            End If
        End If

        If columna = 1 Then

            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

        If columna = 2 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        'If dgvDetalle.Rows.Count > 0 Then
        Select Case e.KeyCode
            Case Keys.Delete
                If dgvDetalle.Rows.Count > 0 Then
                    Dim CodigoIdDetalle As String = ""
                    Dim CanceladoSiNo As String = ""
                    'CanceladoSiNo = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("EstadoPagoOculto").Value

                    CanceladoSiNo = IIf(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("EstadoPagoOculto").Value = Nothing, "", dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("EstadoPagoOculto").Value)

                    If Trim(CanceladoSiNo) = "1" Then
                        MessageBox.Show("Este N�mero de Pago esta Cancelado y no Podra Borrarse", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    Else
                        If MessageBox.Show("�Desea Eliminar el Item?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            CodigoIdDetalle = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("IdDetallePago").Value
                            If CodigoIdDetalle <> "" Then
                                eValorizaciones = New clsValorizaciones
                                eValorizaciones.fEliminarDetallePagoVal(Trim(CodigoIdDetalle), Trim(sIdValorizacion), gEmpresa, gPeriodo)
                                dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                            Else
                                dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                            End If
                        End If
                        Calcular()
                    End If
                End If
            Case Keys.Escape
                If dgvDetalle.Rows.Count > 0 Then
                    Dim ImporteGrabar As Double = 0
                    Dim ImporteGrabar2 As Double = 0
                    If dgvDetalle.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                            If dgvDetalle.Rows(x).Cells("FechaPago").Value = Nothing Then
                                MessageBox.Show("Ingrese la Fecha", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgvDetalle.CurrentCell = dgvDetalle(0, x)
                                Exit Sub
                            End If
                            If dgvDetalle.Rows(x).Cells("Numero").Value = Nothing Then
                                MessageBox.Show("Ingrese el N�mero de Pago", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgvDetalle.CurrentCell = dgvDetalle(1, x)
                                Exit Sub
                            End If
                            If dgvDetalle.Rows(x).Cells("Impo").Value = Nothing Then
                                MessageBox.Show("Ingrese el Importe", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgvDetalle.CurrentCell = dgvDetalle(2, x)
                                Exit Sub
                            End If
                            ImporteGrabar = ImporteGrabar + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("Impo").Value)))
                        Next
                        ImporteGrabar2 = Format(ImporteGrabar, "#,##0.0000")
                    End If
                    If ImporteGrabar2 <= Convert.ToDouble(Val(lblMonto2.Text)) Then
                        For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                            Dim IdDetDetallePago As String = ""
                            Dim CCosto As String = ""
                            Dim Gasto As String = ""
                            Dim Descripcion As String = ""
                            Dim EmprCodPre As String = ""
                            Dim MontoGrabar As Double = 0
                            Dim CCosto2 As String = ""
                            Dim Opcion As String = ""
                            Dim IdDetalle As String = ""
                            Dim Numero As String = ""
                            Dim FechaPago As DateTime
                            Dim iResultadoGrabaGastos As Integer = 0
                            Opcion = IIf(dgvDetalle.Rows(x).Cells("EstadoOculto").Value = Nothing, "", dgvDetalle.Rows(x).Cells("EstadoOculto").Value)
                            IdDetDetallePago = IIf(dgvDetalle.Rows(x).Cells("IdDetallePago").Value = Nothing, "", dgvDetalle.Rows(x).Cells("IdDetallePago").Value)
                            FechaPago = IIf(dgvDetalle.Rows(x).Cells("FechaPago").Value = Nothing, Today(), dgvDetalle.Rows(x).Cells("FechaPago").Value)
                            Numero = IIf(dgvDetalle.Rows(x).Cells("Numero").Value = Nothing, 0, dgvDetalle.Rows(x).Cells("Numero").Value)
                            MontoGrabar = IIf(dgvDetalle.Rows(x).Cells("Impo").Value = Nothing, 0, dgvDetalle.Rows(x).Cells("Impo").Value)
                            If Opcion = "1" Then
                                'actualizar
                                eValorizaciones = New clsValorizaciones
                                iResultadoGrabaGastos = eValorizaciones.fGrabarDetallePagoVal(24, IdDetDetallePago, Trim(sIdValorizacion), gEmpresa, FechaPago, Numero, MontoGrabar, 1, 0, "", "", gUsuario, gPeriodo)
                            ElseIf Opcion = "0" Then
                                'grabar
                                eValorizaciones = New clsValorizaciones
                                eValorizaciones.fCodigoPagoVal(sIdValorizacion, gEmpresa, gPeriodo)
                                IdDetDetallePago = eValorizaciones.sCodFuturo
                                iResultadoGrabaGastos = eValorizaciones.fGrabarDetallePagoVal(23, IdDetDetallePago, Trim(sIdValorizacion), gEmpresa, FechaPago, Numero, MontoGrabar, 1, 0, "", "", gUsuario, gPeriodo)
                            End If
                        Next

                        Dim iResActCantDetenCab As Integer = 0
                        'iResActCantDetenCab = eValorizaciones.fActCantDetCabecera(43, Trim(sIdMovimiento), gEmpresa, dgvDetalle.Rows.Count)
                        MessageBox.Show("Se Actualizo el Detalle de Pagos Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Me.Hide()
                    Else
                        'MessageBox.Show("El Total del Detalle de Pagos Excede el Monto a pagar de la Valorizacion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        MessageBox.Show("El Total del Detalle de Pagos Excede el Monto a Pagar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    End If

                    Calcular()
                ElseIf dgvDetalle.Rows.Count = 0 Then
                    Me.Hide()
                End If

        End Select


        'End If
    End Sub

    Private Sub dgvDetalle_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetalle.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                'Importe = 0
                Calcular()

                If Importe = Convert.ToDouble(Val(lblMonto2.Text)) Then
                    MessageBox.Show("El Total a Pagar del Detalle esta completo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                ElseIf Importe > Convert.ToDouble(Val(lblMonto2.Text)) Then
                    'MessageBox.Show("El Total a Pagar del Detalle es Mayor a la deuda.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    MessageBox.Show("El Total del Detalle de Pagos Excede el Monto a Pagar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                Else
                    If dgvDetalle.Rows.Count = 0 Then
                        dgvDetalle.Rows.Add()
                        dgvDetalle.Rows(0).Cells("EstadoOculto").Value = "0"
                        dgvDetalle.Rows(0).Cells("FechaPago").Value = Now.Date().ToShortDateString
                        dgvDetalle.Rows(0).Cells("Numero").Value = "1"
                        dgvDetalle.Rows(0).Cells("Impo").Value = Format(Monto, "#,##0.0000")
                        dgvDetalle.Rows(0).Cells("Estado").Value = "PENDIENTE"
                        dgvDetalle.CurrentCell = dgvDetalle(2, 0)
                        dgvDetalle.BeginEdit(True)
                    Else
                        If dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Impo").Selected = True Then
                            Dim A As Integer
                            dgvDetalle.Rows.Add()
                            A = dgvDetalle.Rows.Count
                            If dgvDetalle.Rows(A - 2).Cells("EstadoOculto").Value = "0" Or dgvDetalle.Rows(A - 2).Cells("EstadoOculto").Value = "1" Then
                                A = dgvDetalle.Rows.Count
                                dgvDetalle.Rows(A - 1).Cells("EstadoOculto").Value = "0"
                                dgvDetalle.Rows(A - 1).Cells("FechaPago").Value = Now.Date().ToShortDateString
                                '''''''''''''''''''''''
                                Dim NumeroMayor As Integer = 0
                                Dim ImportePorPagar As Double = 0
                                Dim ImporteProgramado As Double = 0
                                NumeroMayor = dgvDetalle.Rows(0).Cells("Numero").Value
                                For i As Integer = 0 To dgvDetalle.Rows.Count - 1
                                    If Trim(dgvDetalle.Rows(i).Cells("Numero").Value) <> "" Then
                                        If (dgvDetalle.Rows(i).Cells("Numero").Value > NumeroMayor) Then
                                            NumeroMayor = dgvDetalle.Rows(i).Cells("Numero").Value
                                        End If
                                    End If
                                    If Trim(dgvDetalle.Rows(i).Cells("Impo").Value) <> "" Then
                                        ImporteProgramado = ImporteProgramado + dgvDetalle.Rows(i).Cells("Impo").Value
                                    End If
                                Next
                                dgvDetalle.Rows(A - 1).Cells("Numero").Value = NumeroMayor + 1 'dgvDetalle.Rows.Count
                                ImportePorPagar = Monto - ImporteProgramado
                                dgvDetalle.Rows(A - 1).Cells("Impo").Value = Format(ImportePorPagar, "#,##0.0000")
                                ''''''''''''''''''''''
                                dgvDetalle.Rows(A - 1).Cells("Estado").Value = "PENDIENTE"
                            Else
                                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index + 1).Cells("EstadoOculto").Value = "0"
                            End If
                            dgvDetalle.CurrentCell = dgvDetalle(2, A - 1)
                            dgvDetalle.BeginEdit(True)
                        End If
                    End If
                End If
        End Select
    End Sub
End Class