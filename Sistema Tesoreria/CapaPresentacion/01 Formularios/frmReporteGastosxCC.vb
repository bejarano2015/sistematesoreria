Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReporteGastosxCC

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptGastos As New rptGasGeneDetallado4
    Private eGastosGenerales As clsGastosGenerales

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteGastosxCC = Nothing
    Public Shared Function Instance() As frmReporteGastosxCC
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteGastosxCC
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmReporteGastosxCC_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub
    Private Sub frmReporteGastosxCC_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteGastosxCC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            eGastosGenerales = New clsGastosGenerales
            cboCentroCosto.DataSource = eGastosGenerales.fListarCentrosCostos(gEmpresa)
            If eGastosGenerales.iNroRegistros > 0 Then
                cboCentroCosto.ValueMember = "CCosCodigo"
                cboCentroCosto.DisplayMember = "CCosDescripcion"
                cboCentroCosto.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
        'dtpFechaIni.Value = "01/01" & "/" & Now.Year()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        dtpFechaFin.Value = Now.Date()
        rdbRes.Checked = True
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        rptGastos.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '1
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaIni")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = dtpFechaIni.Value
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaFin")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = dtpFechaFin.Value
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '3
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@CCosto")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(cboCentroCosto.SelectedValue)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("ResDet")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()

        If rdbRes.Checked = True Then
            crParameterDiscreteValue.Value = "R"
        End If
        If rdbDet.Checked = True Then
            crParameterDiscreteValue.Value = ""
        End If

        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = rptGastos
    End Sub
End Class