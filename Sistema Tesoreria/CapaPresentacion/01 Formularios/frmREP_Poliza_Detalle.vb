Imports CapaNegocios
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows

Public Class frmREP_Poliza_Detalle
    Private cSegurosPolizas As clsSegurosPolizas
    Dim DT_Polizas_Detalle As DataTable
#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmREP_Poliza_Detalle = Nothing
    Public Shared Function Instance() As frmREP_Poliza_Detalle
        If frmInstance Is Nothing Then
            frmInstance = New frmREP_Poliza_Detalle
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmREP_Poliza_Detalle_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub frmREP_Poliza_Detalle_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cSegurosPolizas = New clsSegurosPolizas
        cboCia.DataSource = cSegurosPolizas.ListarAseguradoras(gEmpresa)
        cboCia.ValueMember = "Codigo"
        cboCia.DisplayMember = "Descripcion"
        cboCia.SelectedIndex = -1

        cboCia.Enabled = True

        mtbFechaIni.Text = Convert.ToDateTime("01/01/" & Year(Today))
        mtbFechaFin.Text = Today
        mtbFechaIni.Focus()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        cSegurosPolizas = New clsSegurosPolizas
        DT_Polizas_Detalle = New DataTable

        Try
            DT_Polizas_Detalle.Clear()
            Me.Cursor = Cursors.WaitCursor

            'If Not IsDate(Convert.ToDateTime(mtbFechaIni.Text)) Or Not IsDate(Convert.ToDateTime(mtbFechaFin.Text)) Then
            'MessageBox.Show("Verifique las Fechas Ingresadas", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'mtbFechaIni.Focus()
            '    Exit Sub
            'End If
            If rdb1.Checked = True Then
                DT_Polizas_Detalle = cSegurosPolizas.fREP_Polizas_Detalle(12, gEmpresa, IIf(cboCia.SelectedValue = Nothing, "", cboCia.SelectedValue), mtbFechaIni.Text, mtbFechaFin.Text)

            ElseIf rdb2.Checked = True Then
                DT_Polizas_Detalle = cSegurosPolizas.fREP_Polizas_Detalle(40, gEmpresa, IIf(cboCia.SelectedValue = Nothing, "", cboCia.SelectedValue), mtbFechaIni.Text, mtbFechaFin.Text)

            End If
            'DT_Polizas_Detalle = cSegurosPolizas.fREP_Polizas_Detalle(gEmpresa, IIf(cboCia.SelectedValue = Nothing, "", cboCia.SelectedValue), mtbFechaIni.Text, mtbFechaFin.Text)

            If DT_Polizas_Detalle.Rows.Count = 0 Then
                MessageBox.Show("No hay Informacion que mostrar", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            'Muestra_Reporte("D:\MIGUEL\FUENTES TESORERIA\Sistema Tesoreria21\CapaPresentacion\Reportes\" & "CrPoliza_Detalle.rpt", DT_Polizas_Detalle, "TblPoliza_Detalle", "", _
            'Muestra_Reporte(RutaAppReportes & "CrPoliza_Detalle.rpt", DT_Polizas_Detalle, "TblPoliza_Detalle", "", _
            '"StrEmpresa;" & cSegurosPolizas.DescripcionEmpresa(gEmpresa), _
            '"StrRUC;" & cSegurosPolizas.RucEmpresa(gEmpresa), _
            '"Criterio;" & "Desde: " & mtbFechaIni.Text & " Hasta:" & mtbFechaFin.Text)

            If rdb1.Checked = True Then
                Muestra_Reporte(RutaAppReportes & "CrPoliza_Detalle.rpt", DT_Polizas_Detalle, "TblPoliza_Detalle", "", _
            "StrEmpresa;" & cSegurosPolizas.DescripcionEmpresa(gEmpresa), _
            "StrRUC;" & cSegurosPolizas.RucEmpresa(gEmpresa), _
            "Criterio;" & "Desde: " & mtbFechaIni.Text & " Hasta:" & mtbFechaFin.Text)
            ElseIf rdb2.Checked = True Then
                Muestra_Reporte(RutaAppReportes & "CrPoliza_Detalle3.rpt", DT_Polizas_Detalle, "TblPoliza_Detalle", "", _
             "StrEmpresa;" & cSegurosPolizas.DescripcionEmpresa(gEmpresa), _
             "StrRUC;" & cSegurosPolizas.RucEmpresa(gEmpresa), _
             "Criterio;" & "Desde: " & mtbFechaIni.Text & " Hasta:" & mtbFechaFin.Text)
            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MessageBox.Show("Verifique las Fechas Ingresadas", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            mtbFechaIni.Focus()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, _
            ByVal myDatos As Object, ByVal STRnombreTabla As String, _
            ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New FrmREPView_PolCuotasXPagar
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)
            'myReporte.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CrystalReportViewer1.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CrystalReportViewer1.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CrystalReportViewer1.ReportSource = myReporte
            f.CrystalReportViewer1.DisplayGroupTree = False
            f.strNombreFom = "Reporte de Polizas Y Detalle"
            f.Show()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub chkTodos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodos.CheckedChanged
        If chkTodos.Checked = True Then
            cboCia.SelectedIndex = -1
            cboCia.Enabled = False
        ElseIf chkTodos.Checked = False Then
            cboCia.Enabled = True
        End If
    End Sub
End Class