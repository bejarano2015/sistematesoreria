Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmCajaChicaConsultas

    Private eSesionCajas As clsSesionCajas

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmCajaChicaConsultas = Nothing
    Public Shared Function Instance() As frmCajaChicaConsultas
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmCajaChicaConsultas
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

   
    Private Sub frmCajaChicaConsultas_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub frmCajaChicaConsultas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtpFechaFin.Text = Now.Date()
        'Dim Mes As String = ""
        'Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & "01" & "/" & Now.Year()
        chkContable.Checked = True
    End Sub


    'Private Sub txtTexto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTexto.TextChanged
    '    If Len(Trim(txtTexto.Text)) > 0 Then
    '        Dim dtDetalle As DataTable
    '        dtDetalle = New DataTable
    '        dtDetalle = eSesionCajas.fBuscarComprob(41, Trim(txtTexto.Text), gEmpresa, iEstado01)
    '        'cargarDetalle()
    '        dgvComprobantes.DataSource = dtDetalle
    '    ElseIf Len(Trim(txtTexto.Text)) = 0 Then
    '        For x As Integer = 0 To dgvComprobantes.RowCount - 1
    '            dgvComprobantes.Rows.Remove(dgvComprobantes.CurrentRow)
    '        Next
    '    End If
    'End Sub

 
    Private Sub txtTexto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTexto.TextChanged


        If Len(Trim(txtTexto.Text)) > 0 Then

            If cboCriterio.SelectedIndex > -1 Then

                If Len(Trim(txtTexto.Text)) > 0 Then
                    Dim iOption As Integer = 0
                    Dim iContable As Integer = 0
                    Dim iPrestado As Integer = 0

                    If chkContable.Checked = True Then
                        iContable = 1
                    ElseIf chkContable.Checked = False Then
                        iContable = 0
                    End If

                    If chkPrestado.Checked = True Then
                        iPrestado = 1
                    ElseIf chkPrestado.Checked = False Then
                        iPrestado = 0
                    End If

                    Dim dtDetalle As DataTable
                    dtDetalle = New DataTable

                    If Trim(cboCriterio.Text) = "DESCRIPCION" Then
                        iOption = 1
                    ElseIf Trim(cboCriterio.Text) = "TIPO GASTO" Then
                        iOption = 2
                    ElseIf Trim(cboCriterio.Text) = "PROVEEDOR" Then
                        iOption = 3
                    ElseIf Trim(cboCriterio.Text) = "NUM. DOC." Then
                        iOption = 4
                    End If
                    eSesionCajas = New clsSesionCajas
                    dtDetalle = eSesionCajas.fBuscarComprob(iOption, gEmpresa, Trim(txtTexto.Text), dtpFechaIni.Value, dtpFechaFin.Value, iContable, iPrestado, gPeriodo)

                    dgvComprobantes.DataSource = dtDetalle

                ElseIf Len(Trim(txtTexto.Text)) = 0 Then

                    For x As Integer = 0 To dgvComprobantes.RowCount - 1
                        dgvComprobantes.Rows.Remove(dgvComprobantes.CurrentRow)
                    Next

                End If

            ElseIf cboCriterio.SelectedIndex = -1 Then

                MessageBox.Show("Seleccione un Criterio de B�squeda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCriterio.Focus()
                Exit Sub
            End If
        ElseIf Len(Trim(txtTexto.Text)) = 0 Then
            For x As Integer = 0 To dgvComprobantes.RowCount - 1
                dgvComprobantes.Rows.Remove(dgvComprobantes.CurrentRow)
            Next
        End If
    End Sub
    Private Sub cboCriterio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCriterio.SelectedIndexChanged
        If cboCriterio.SelectedIndex > -1 Then
            txtTexto_TextChanged(sender, e)
        End If

    End Sub

    Private Sub dtpFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        txtTexto_TextChanged(sender, e)
    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        txtTexto_TextChanged(sender, e)
    End Sub

    Private Sub chkContable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkContable.CheckedChanged
        txtTexto_TextChanged(sender, e)
    End Sub

    Private Sub chkPrestado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPrestado.CheckedChanged
        txtTexto_TextChanged(sender, e)
    End Sub
End Class