<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngresoBancarioListado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIngresoBancarioListado))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.mnuMantenimiento = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnRefrescar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.pnCuerpo = New System.Windows.Forms.Panel()
        Me.pnPie = New System.Windows.Forms.Panel()
        Me.tabMantenimiento = New System.Windows.Forms.TabControl()
        Me.tabLista = New System.Windows.Forms.TabPage()
        Me.gvIngresoBancario = New System.Windows.Forms.DataGridView()
        Me.IdIngresoBancario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmprCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Glosa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaDeposito = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCtaCorriente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MonDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroOperacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabDetalle = New System.Windows.Forms.TabPage()
        Me.pnDetalle = New System.Windows.Forms.Panel()
        Me.txtGlosa = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.txtImporte = New System.Windows.Forms.TextBox()
        Me.cboBanco = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.dtpFechaDeposito = New ctrLibreria.Controles.BeDateTimePicker()
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroOperacion = New ctrLibreria.Controles.BeTextBox()
        Me.txtCodMoneda = New ctrLibreria.Controles.BeTextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.cboEstados = New ctrLibreria.Controles.BeComboBox()
        Me.txtBusqueda = New ctrLibreria.Controles.BeTextBox()
        Me.Panel1.SuspendLayout()
        Me.mnuMantenimiento.SuspendLayout()
        Me.pnCuerpo.SuspendLayout()
        Me.tabMantenimiento.SuspendLayout()
        Me.tabLista.SuspendLayout()
        CType(Me.gvIngresoBancario, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDetalle.SuspendLayout()
        Me.pnDetalle.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.mnuMantenimiento)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1023, 28)
        Me.Panel1.TabIndex = 171
        '
        'mnuMantenimiento
        '
        Me.mnuMantenimiento.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnCancelar, Me.btnRefrescar, Me.btnEliminar, Me.btnImprimir, Me.btnSalir})
        Me.mnuMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.mnuMantenimiento.Name = "mnuMantenimiento"
        Me.mnuMantenimiento.Size = New System.Drawing.Size(1021, 25)
        Me.mnuMantenimiento.TabIndex = 0
        Me.mnuMantenimiento.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnCancelar
        '
        Me.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(23, 22)
        Me.btnCancelar.Text = "ToolStripButton1"
        Me.btnCancelar.ToolTipText = "Cancelar"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRefrescar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Grid_properties_
        Me.btnRefrescar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(23, 22)
        Me.btnRefrescar.Text = "&Refrescar"
        Me.btnRefrescar.ToolTipText = "Refrescar"
        '
        'btnEliminar
        '
        Me.btnEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.ToolTipText = "Eliminar"
        '
        'btnImprimir
        '
        Me.btnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimir.Text = "ToolStripButton1"
        Me.btnImprimir.ToolTipText = "Imprimir Caja"
        Me.btnImprimir.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'pnCuerpo
        '
        Me.pnCuerpo.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnCuerpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnCuerpo.Controls.Add(Me.pnPie)
        Me.pnCuerpo.Controls.Add(Me.tabMantenimiento)
        Me.pnCuerpo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnCuerpo.Location = New System.Drawing.Point(0, 28)
        Me.pnCuerpo.Name = "pnCuerpo"
        Me.pnCuerpo.Size = New System.Drawing.Size(1023, 521)
        Me.pnCuerpo.TabIndex = 173
        '
        'pnPie
        '
        Me.pnPie.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnPie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnPie.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnPie.Location = New System.Drawing.Point(0, 500)
        Me.pnPie.Name = "pnPie"
        Me.pnPie.Size = New System.Drawing.Size(1021, 19)
        Me.pnPie.TabIndex = 174
        '
        'tabMantenimiento
        '
        Me.tabMantenimiento.Controls.Add(Me.tabLista)
        Me.tabMantenimiento.Controls.Add(Me.tabDetalle)
        Me.tabMantenimiento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.tabMantenimiento.Name = "tabMantenimiento"
        Me.tabMantenimiento.SelectedIndex = 0
        Me.tabMantenimiento.Size = New System.Drawing.Size(1021, 519)
        Me.tabMantenimiento.TabIndex = 28
        '
        'tabLista
        '
        Me.tabLista.Controls.Add(Me.Panel2)
        Me.tabLista.Controls.Add(Me.gvIngresoBancario)
        Me.tabLista.Location = New System.Drawing.Point(4, 22)
        Me.tabLista.Name = "tabLista"
        Me.tabLista.Padding = New System.Windows.Forms.Padding(3)
        Me.tabLista.Size = New System.Drawing.Size(1013, 493)
        Me.tabLista.TabIndex = 0
        Me.tabLista.Text = "Lista"
        Me.tabLista.UseVisualStyleBackColor = True
        '
        'gvIngresoBancario
        '
        Me.gvIngresoBancario.AllowUserToAddRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.gvIngresoBancario.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.gvIngresoBancario.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gvIngresoBancario.BackgroundColor = System.Drawing.Color.White
        Me.gvIngresoBancario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvIngresoBancario.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdIngresoBancario, Me.EmprCodigo, Me.Glosa, Me.FechaDeposito, Me.IdBanco, Me.NombreBanco, Me.IdCtaCorriente, Me.MonDescripcion, Me.Importe, Me.NroOperacion})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvIngresoBancario.DefaultCellStyle = DataGridViewCellStyle4
        Me.gvIngresoBancario.Location = New System.Drawing.Point(7, 6)
        Me.gvIngresoBancario.Name = "gvIngresoBancario"
        Me.gvIngresoBancario.ReadOnly = True
        Me.gvIngresoBancario.RowHeadersVisible = False
        Me.gvIngresoBancario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvIngresoBancario.Size = New System.Drawing.Size(999, 466)
        Me.gvIngresoBancario.TabIndex = 27
        '
        'IdIngresoBancario
        '
        Me.IdIngresoBancario.DataPropertyName = "IdIngresoBancario"
        Me.IdIngresoBancario.HeaderText = "IdIngresoBancario"
        Me.IdIngresoBancario.Name = "IdIngresoBancario"
        Me.IdIngresoBancario.ReadOnly = True
        Me.IdIngresoBancario.Visible = False
        '
        'EmprCodigo
        '
        Me.EmprCodigo.DataPropertyName = "EmprCodigo"
        Me.EmprCodigo.HeaderText = "EmprCodigo"
        Me.EmprCodigo.Name = "EmprCodigo"
        Me.EmprCodigo.ReadOnly = True
        Me.EmprCodigo.Visible = False
        '
        'Glosa
        '
        Me.Glosa.DataPropertyName = "Glosa"
        Me.Glosa.HeaderText = "Glosa"
        Me.Glosa.Name = "Glosa"
        Me.Glosa.ReadOnly = True
        Me.Glosa.Width = 200
        '
        'FechaDeposito
        '
        Me.FechaDeposito.DataPropertyName = "FechaDeposito"
        Me.FechaDeposito.HeaderText = "Fecha Deposito"
        Me.FechaDeposito.Name = "FechaDeposito"
        Me.FechaDeposito.ReadOnly = True
        Me.FechaDeposito.Width = 120
        '
        'IdBanco
        '
        Me.IdBanco.DataPropertyName = "IdBanco"
        Me.IdBanco.HeaderText = "IdBanco"
        Me.IdBanco.Name = "IdBanco"
        Me.IdBanco.ReadOnly = True
        Me.IdBanco.Visible = False
        '
        'NombreBanco
        '
        Me.NombreBanco.DataPropertyName = "NombreBanco"
        Me.NombreBanco.HeaderText = "Banco"
        Me.NombreBanco.Name = "NombreBanco"
        Me.NombreBanco.ReadOnly = True
        Me.NombreBanco.Width = 150
        '
        'IdCtaCorriente
        '
        Me.IdCtaCorriente.DataPropertyName = "IdCtaCorriente"
        Me.IdCtaCorriente.HeaderText = "Cta Corriente"
        Me.IdCtaCorriente.Name = "IdCtaCorriente"
        Me.IdCtaCorriente.ReadOnly = True
        Me.IdCtaCorriente.Width = 150
        '
        'MonDescripcion
        '
        Me.MonDescripcion.DataPropertyName = "MonDescripcion"
        Me.MonDescripcion.HeaderText = "Moneda"
        Me.MonDescripcion.Name = "MonDescripcion"
        Me.MonDescripcion.ReadOnly = True
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        '
        'NroOperacion
        '
        Me.NroOperacion.DataPropertyName = "NroOperacion"
        Me.NroOperacion.HeaderText = "Nro Operación"
        Me.NroOperacion.Name = "NroOperacion"
        Me.NroOperacion.ReadOnly = True
        '
        'tabDetalle
        '
        Me.tabDetalle.Controls.Add(Me.pnDetalle)
        Me.tabDetalle.Location = New System.Drawing.Point(4, 22)
        Me.tabDetalle.Name = "tabDetalle"
        Me.tabDetalle.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDetalle.Size = New System.Drawing.Size(1013, 493)
        Me.tabDetalle.TabIndex = 1
        Me.tabDetalle.Text = "Detalle"
        Me.tabDetalle.UseVisualStyleBackColor = True
        '
        'pnDetalle
        '
        Me.pnDetalle.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnDetalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnDetalle.Controls.Add(Me.txtGlosa)
        Me.pnDetalle.Controls.Add(Me.BeLabel3)
        Me.pnDetalle.Controls.Add(Me.BeLabel5)
        Me.pnDetalle.Controls.Add(Me.txtImporte)
        Me.pnDetalle.Controls.Add(Me.cboBanco)
        Me.pnDetalle.Controls.Add(Me.BeLabel11)
        Me.pnDetalle.Controls.Add(Me.dtpFechaDeposito)
        Me.pnDetalle.Controls.Add(Me.cboCuenta)
        Me.pnDetalle.Controls.Add(Me.BeLabel12)
        Me.pnDetalle.Controls.Add(Me.BeLabel15)
        Me.pnDetalle.Controls.Add(Me.BeLabel9)
        Me.pnDetalle.Controls.Add(Me.txtNroOperacion)
        Me.pnDetalle.Controls.Add(Me.txtCodMoneda)
        Me.pnDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnDetalle.Location = New System.Drawing.Point(3, 3)
        Me.pnDetalle.Name = "pnDetalle"
        Me.pnDetalle.Size = New System.Drawing.Size(1007, 487)
        Me.pnDetalle.TabIndex = 173
        '
        'txtGlosa
        '
        Me.txtGlosa.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtGlosa.BackColor = System.Drawing.Color.Ivory
        Me.txtGlosa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGlosa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGlosa.ForeColor = System.Drawing.Color.Black
        Me.txtGlosa.KeyEnter = False
        Me.txtGlosa.Location = New System.Drawing.Point(130, 144)
        Me.txtGlosa.MaxLength = 20
        Me.txtGlosa.Multiline = True
        Me.txtGlosa.Name = "txtGlosa"
        Me.txtGlosa.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtGlosa.ShortcutsEnabled = False
        Me.txtGlosa.Size = New System.Drawing.Size(318, 87)
        Me.txtGlosa.TabIndex = 56
        Me.txtGlosa.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(23, 144)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel3.TabIndex = 55
        Me.BeLabel3.Text = "Glosa"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(23, 18)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(65, 13)
        Me.BeLabel5.TabIndex = 26
        Me.BeLabel5.Text = "Fecha Pago"
        '
        'txtImporte
        '
        Me.txtImporte.BackColor = System.Drawing.Color.Ivory
        Me.txtImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporte.Location = New System.Drawing.Point(130, 116)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(106, 20)
        Me.txtImporte.TabIndex = 48
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboBanco
        '
        Me.cboBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBanco.ForeColor = System.Drawing.Color.Black
        Me.cboBanco.FormattingEnabled = True
        Me.cboBanco.KeyEnter = True
        Me.cboBanco.Location = New System.Drawing.Point(130, 38)
        Me.cboBanco.Name = "cboBanco"
        Me.cboBanco.Size = New System.Drawing.Size(212, 21)
        Me.cboBanco.TabIndex = 32
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(23, 46)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel11.TabIndex = 31
        Me.BeLabel11.Text = "Banco"
        '
        'dtpFechaDeposito
        '
        Me.dtpFechaDeposito.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaDeposito.CustomFormat = ""
        Me.dtpFechaDeposito.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaDeposito.KeyEnter = True
        Me.dtpFechaDeposito.Location = New System.Drawing.Point(130, 11)
        Me.dtpFechaDeposito.Name = "dtpFechaDeposito"
        Me.dtpFechaDeposito.Size = New System.Drawing.Size(106, 20)
        Me.dtpFechaDeposito.TabIndex = 27
        Me.dtpFechaDeposito.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaDeposito.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(130, 63)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(212, 21)
        Me.cboCuenta.TabIndex = 34
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(23, 69)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel12.TabIndex = 33
        Me.BeLabel12.Text = "Nº Cta Cte"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(23, 122)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel15.TabIndex = 47
        Me.BeLabel15.Text = "Importe"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(23, 98)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(71, 13)
        Me.BeLabel9.TabIndex = 44
        Me.BeLabel9.Text = "Nº Operacion"
        '
        'txtNroOperacion
        '
        Me.txtNroOperacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroOperacion.BackColor = System.Drawing.Color.Ivory
        Me.txtNroOperacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroOperacion.ForeColor = System.Drawing.Color.Black
        Me.txtNroOperacion.KeyEnter = False
        Me.txtNroOperacion.Location = New System.Drawing.Point(130, 90)
        Me.txtNroOperacion.MaxLength = 20
        Me.txtNroOperacion.Name = "txtNroOperacion"
        Me.txtNroOperacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroOperacion.ShortcutsEnabled = False
        Me.txtNroOperacion.Size = New System.Drawing.Size(106, 20)
        Me.txtNroOperacion.TabIndex = 45
        Me.txtNroOperacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCodMoneda
        '
        Me.txtCodMoneda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodMoneda.BackColor = System.Drawing.Color.Ivory
        Me.txtCodMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodMoneda.Enabled = False
        Me.txtCodMoneda.ForeColor = System.Drawing.Color.Black
        Me.txtCodMoneda.KeyEnter = True
        Me.txtCodMoneda.Location = New System.Drawing.Point(348, 63)
        Me.txtCodMoneda.MaxLength = 20
        Me.txtCodMoneda.Name = "txtCodMoneda"
        Me.txtCodMoneda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodMoneda.ShortcutsEnabled = False
        Me.txtCodMoneda.Size = New System.Drawing.Size(100, 20)
        Me.txtCodMoneda.TabIndex = 35
        Me.txtCodMoneda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.BeLabel1)
        Me.Panel2.Controls.Add(Me.BeLabel6)
        Me.Panel2.Controls.Add(Me.BeLabel4)
        Me.Panel2.Controls.Add(Me.cboEstados)
        Me.Panel2.Controls.Add(Me.txtBusqueda)
        Me.Panel2.Location = New System.Drawing.Point(242, 188)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(295, 83)
        Me.Panel2.TabIndex = 28
        Me.Panel2.Visible = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Red
        Me.BeLabel1.Location = New System.Drawing.Point(273, -1)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(18, 16)
        Me.BeLabel1.TabIndex = 4
        Me.BeLabel1.Text = "X"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.White
        Me.BeLabel6.Location = New System.Drawing.Point(7, 54)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel6.TabIndex = 3
        Me.BeLabel6.Text = "Estado"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.White
        Me.BeLabel4.Location = New System.Drawing.Point(7, 27)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel4.TabIndex = 2
        Me.BeLabel4.Text = "Descripción"
        '
        'cboEstados
        '
        Me.cboEstados.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstados.BackColor = System.Drawing.Color.Ivory
        Me.cboEstados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstados.ForeColor = System.Drawing.Color.Black
        Me.cboEstados.FormattingEnabled = True
        Me.cboEstados.KeyEnter = True
        Me.cboEstados.Location = New System.Drawing.Point(76, 46)
        Me.cboEstados.Name = "cboEstados"
        Me.cboEstados.Size = New System.Drawing.Size(121, 21)
        Me.cboEstados.TabIndex = 1
        '
        'txtBusqueda
        '
        Me.txtBusqueda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBusqueda.BackColor = System.Drawing.Color.Ivory
        Me.txtBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBusqueda.ForeColor = System.Drawing.Color.Black
        Me.txtBusqueda.KeyEnter = True
        Me.txtBusqueda.Location = New System.Drawing.Point(76, 20)
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBusqueda.ShortcutsEnabled = False
        Me.txtBusqueda.Size = New System.Drawing.Size(196, 20)
        Me.txtBusqueda.TabIndex = 0
        Me.txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'frmIngresoBancarioListado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1023, 549)
        Me.Controls.Add(Me.pnCuerpo)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmIngresoBancarioListado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso bancario"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.mnuMantenimiento.ResumeLayout(False)
        Me.mnuMantenimiento.PerformLayout()
        Me.pnCuerpo.ResumeLayout(False)
        Me.tabMantenimiento.ResumeLayout(False)
        Me.tabLista.ResumeLayout(False)
        CType(Me.gvIngresoBancario, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDetalle.ResumeLayout(False)
        Me.pnDetalle.ResumeLayout(False)
        Me.pnDetalle.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents mnuMantenimiento As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnRefrescar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnCuerpo As System.Windows.Forms.Panel
    Friend WithEvents gvIngresoBancario As System.Windows.Forms.DataGridView
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabMantenimiento As System.Windows.Forms.TabControl
    Friend WithEvents tabLista As System.Windows.Forms.TabPage
    Friend WithEvents tabDetalle As System.Windows.Forms.TabPage
    Friend WithEvents pnPie As System.Windows.Forms.Panel
    Friend WithEvents pnDetalle As System.Windows.Forms.Panel
    Friend WithEvents txtGlosa As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtImporte As System.Windows.Forms.TextBox
    Friend WithEvents cboBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaDeposito As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroOperacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodMoneda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents IdIngresoBancario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmprCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Glosa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDeposito As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCtaCorriente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MonDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroOperacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstados As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtBusqueda As ctrLibreria.Controles.BeTextBox
End Class
