﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNotaCredito
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewCheckBoxColumn1 As Telerik.WinControls.UI.GridViewCheckBoxColumn = New Telerik.WinControls.UI.GridViewCheckBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgvNotaCredito = New Telerik.WinControls.UI.RadGridView()
        Me.btnAplicar = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvNotaCredito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNotaCredito.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvNotaCredito)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(814, 296)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Notas de Crédito Proveedor"
        '
        'dgvNotaCredito
        '
        Me.dgvNotaCredito.BackColor = System.Drawing.Color.Transparent
        Me.dgvNotaCredito.Cursor = System.Windows.Forms.Cursors.Default
        Me.dgvNotaCredito.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvNotaCredito.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.dgvNotaCredito.ForeColor = System.Drawing.SystemColors.ControlText
        Me.dgvNotaCredito.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.dgvNotaCredito.Location = New System.Drawing.Point(3, 16)
        '
        'dgvNotaCredito
        '
        Me.dgvNotaCredito.MasterTemplate.AllowAddNewRow = False
        Me.dgvNotaCredito.MasterTemplate.AllowDeleteRow = False
        Me.dgvNotaCredito.MasterTemplate.AllowRowReorder = True
        GridViewTextBoxColumn1.FieldName = "CodProveedor"
        GridViewTextBoxColumn1.HeaderText = "RUC"
        GridViewTextBoxColumn1.Name = "CodProveedor"
        GridViewTextBoxColumn1.ReadOnly = True
        GridViewTextBoxColumn1.Width = 120
        GridViewTextBoxColumn2.FieldName = "NombreProveedor"
        GridViewTextBoxColumn2.HeaderText = "Proveedor"
        GridViewTextBoxColumn2.Name = "NombreProveedor"
        GridViewTextBoxColumn2.ReadOnly = True
        GridViewTextBoxColumn2.Width = 250
        GridViewTextBoxColumn3.FieldName = "Descripcion"
        GridViewTextBoxColumn3.HeaderText = "Descripcion"
        GridViewTextBoxColumn3.Name = "Descripcion"
        GridViewTextBoxColumn3.ReadOnly = True
        GridViewTextBoxColumn3.Width = 200
        GridViewTextBoxColumn4.FieldName = "Moneda"
        GridViewTextBoxColumn4.HeaderText = "Moneda"
        GridViewTextBoxColumn4.Name = "Moneda"
        GridViewTextBoxColumn4.Width = 68
        GridViewTextBoxColumn5.FieldName = "ImporteTotal"
        GridViewTextBoxColumn5.HeaderText = "Total"
        GridViewTextBoxColumn5.Name = "ImporteTotal"
        GridViewTextBoxColumn5.ReadOnly = True
        GridViewTextBoxColumn5.Width = 100
        GridViewTextBoxColumn6.FieldName = "IdCorrelativo"
        GridViewTextBoxColumn6.HeaderText = "IdCorrelativo"
        GridViewTextBoxColumn6.IsVisible = False
        GridViewTextBoxColumn6.Name = "IdCorrelativo"
        GridViewTextBoxColumn6.ReadOnly = True
        GridViewCheckBoxColumn1.HeaderText = ""
        GridViewCheckBoxColumn1.Name = "Seleccionar"
        Me.dgvNotaCredito.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewCheckBoxColumn1})
        Me.dgvNotaCredito.MasterTemplate.EnableFiltering = True
        Me.dgvNotaCredito.MasterTemplate.EnableGrouping = False
        Me.dgvNotaCredito.Name = "dgvNotaCredito"
        Me.dgvNotaCredito.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.dgvNotaCredito.Size = New System.Drawing.Size(808, 277)
        Me.dgvNotaCredito.TabIndex = 5
        Me.dgvNotaCredito.Text = "xxxx"
        '
        'btnAplicar
        '
        Me.btnAplicar.Location = New System.Drawing.Point(360, 314)
        Me.btnAplicar.Name = "btnAplicar"
        Me.btnAplicar.Size = New System.Drawing.Size(81, 23)
        Me.btnAplicar.TabIndex = 14
        Me.btnAplicar.Text = "Aplicar"
        Me.btnAplicar.UseVisualStyleBackColor = True
        '
        'frmNotaCredito
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(838, 354)
        Me.Controls.Add(Me.btnAplicar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmNotaCredito"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nota de Crédito"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvNotaCredito.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNotaCredito, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Private WithEvents dgvNotaCredito As Telerik.WinControls.UI.RadGridView
    Friend WithEvents btnAplicar As System.Windows.Forms.Button
End Class
