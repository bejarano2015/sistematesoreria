<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTipoDestinoGasto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTipoDestinoGasto))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.mnuMantenimiento = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnRefrescar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimirListado = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.pnCuerpo = New System.Windows.Forms.Panel()
        Me.pnPie = New System.Windows.Forms.Panel()
        Me.tabMantenimiento = New System.Windows.Forms.TabControl()
        Me.tabDetalle = New System.Windows.Forms.TabPage()
        Me.pnDetalle = New System.Windows.Forms.Panel()
        Me.cboEmpresa = New ctrLibreria.Controles.BeComboBox()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.cboCuentaContable = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.cboDestino = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.cboGastos = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.cboEstados = New ctrLibreria.Controles.BeComboBox()
        Me.txtBusqueda = New ctrLibreria.Controles.BeTextBox()
        Me.gvTipoGastoDestino = New System.Windows.Forms.DataGridView()
        Me.IDTIPOGASTODESTINO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colIdEmpresa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbogrillaIdTipoGasto = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.cbogrillaIdtipoDestino = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.cbogrillaIdCuentaContable = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.ESTADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        Me.mnuMantenimiento.SuspendLayout()
        Me.pnCuerpo.SuspendLayout()
        Me.tabMantenimiento.SuspendLayout()
        Me.tabDetalle.SuspendLayout()
        Me.pnDetalle.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.gvTipoGastoDestino, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.mnuMantenimiento)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(782, 28)
        Me.Panel1.TabIndex = 171
        '
        'mnuMantenimiento
        '
        Me.mnuMantenimiento.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnCancelar, Me.btnRefrescar, Me.btnEliminar, Me.btnImprimirListado, Me.btnSalir})
        Me.mnuMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.mnuMantenimiento.Name = "mnuMantenimiento"
        Me.mnuMantenimiento.Size = New System.Drawing.Size(780, 25)
        Me.mnuMantenimiento.TabIndex = 0
        Me.mnuMantenimiento.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        Me.btnNuevo.Visible = False
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnCancelar
        '
        Me.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(23, 22)
        Me.btnCancelar.Text = "ToolStripButton1"
        Me.btnCancelar.ToolTipText = "Cancelar"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRefrescar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Grid_properties_
        Me.btnRefrescar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(23, 22)
        Me.btnRefrescar.Text = "&Refrescar"
        Me.btnRefrescar.ToolTipText = "Refrescar"
        Me.btnRefrescar.Visible = False
        '
        'btnEliminar
        '
        Me.btnEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.ToolTipText = "Eliminar"
        Me.btnEliminar.Visible = False
        '
        'btnImprimirListado
        '
        Me.btnImprimirListado.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimirListado.Image = CType(resources.GetObject("btnImprimirListado.Image"), System.Drawing.Image)
        Me.btnImprimirListado.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimirListado.Name = "btnImprimirListado"
        Me.btnImprimirListado.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimirListado.Text = "ToolStripButton1"
        Me.btnImprimirListado.ToolTipText = "Imprimir Caja"
        Me.btnImprimirListado.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'pnCuerpo
        '
        Me.pnCuerpo.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnCuerpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnCuerpo.Controls.Add(Me.pnPie)
        Me.pnCuerpo.Controls.Add(Me.tabMantenimiento)
        Me.pnCuerpo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnCuerpo.Location = New System.Drawing.Point(0, 28)
        Me.pnCuerpo.Name = "pnCuerpo"
        Me.pnCuerpo.Size = New System.Drawing.Size(782, 465)
        Me.pnCuerpo.TabIndex = 173
        '
        'pnPie
        '
        Me.pnPie.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnPie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnPie.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnPie.Location = New System.Drawing.Point(0, 444)
        Me.pnPie.Name = "pnPie"
        Me.pnPie.Size = New System.Drawing.Size(780, 19)
        Me.pnPie.TabIndex = 174
        '
        'tabMantenimiento
        '
        Me.tabMantenimiento.Controls.Add(Me.tabDetalle)
        Me.tabMantenimiento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.tabMantenimiento.Name = "tabMantenimiento"
        Me.tabMantenimiento.SelectedIndex = 0
        Me.tabMantenimiento.Size = New System.Drawing.Size(780, 463)
        Me.tabMantenimiento.TabIndex = 28
        '
        'tabDetalle
        '
        Me.tabDetalle.Controls.Add(Me.pnDetalle)
        Me.tabDetalle.Location = New System.Drawing.Point(4, 22)
        Me.tabDetalle.Name = "tabDetalle"
        Me.tabDetalle.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDetalle.Size = New System.Drawing.Size(772, 437)
        Me.tabDetalle.TabIndex = 1
        Me.tabDetalle.Text = "Detalle"
        Me.tabDetalle.UseVisualStyleBackColor = True
        '
        'pnDetalle
        '
        Me.pnDetalle.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnDetalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnDetalle.Controls.Add(Me.cboEmpresa)
        Me.pnDetalle.Controls.Add(Me.btnQuitar)
        Me.pnDetalle.Controls.Add(Me.btnAgregar)
        Me.pnDetalle.Controls.Add(Me.cboCuentaContable)
        Me.pnDetalle.Controls.Add(Me.BeLabel2)
        Me.pnDetalle.Controls.Add(Me.cboDestino)
        Me.pnDetalle.Controls.Add(Me.BeLabel3)
        Me.pnDetalle.Controls.Add(Me.BeLabel11)
        Me.pnDetalle.Controls.Add(Me.cboGastos)
        Me.pnDetalle.Controls.Add(Me.BeLabel12)
        Me.pnDetalle.Controls.Add(Me.Panel2)
        Me.pnDetalle.Controls.Add(Me.gvTipoGastoDestino)
        Me.pnDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnDetalle.Location = New System.Drawing.Point(3, 3)
        Me.pnDetalle.Name = "pnDetalle"
        Me.pnDetalle.Size = New System.Drawing.Size(766, 431)
        Me.pnDetalle.TabIndex = 173
        '
        'cboEmpresa
        '
        Me.cboEmpresa.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEmpresa.BackColor = System.Drawing.Color.Ivory
        Me.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmpresa.Enabled = False
        Me.cboEmpresa.ForeColor = System.Drawing.Color.Black
        Me.cboEmpresa.FormattingEnabled = True
        Me.cboEmpresa.KeyEnter = True
        Me.cboEmpresa.Location = New System.Drawing.Point(105, 12)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(293, 21)
        Me.cboEmpresa.TabIndex = 71
        '
        'btnQuitar
        '
        Me.btnQuitar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnQuitar.Location = New System.Drawing.Point(716, 102)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(35, 23)
        Me.btnQuitar.TabIndex = 70
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__File_new_
        Me.btnAgregar.Location = New System.Drawing.Point(683, 102)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(27, 23)
        Me.btnAgregar.TabIndex = 69
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'cboCuentaContable
        '
        Me.cboCuentaContable.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuentaContable.BackColor = System.Drawing.Color.Ivory
        Me.cboCuentaContable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentaContable.ForeColor = System.Drawing.Color.Black
        Me.cboCuentaContable.FormattingEnabled = True
        Me.cboCuentaContable.KeyEnter = True
        Me.cboCuentaContable.Location = New System.Drawing.Point(105, 93)
        Me.cboCuentaContable.Name = "cboCuentaContable"
        Me.cboCuentaContable.Size = New System.Drawing.Size(212, 21)
        Me.cboCuentaContable.TabIndex = 68
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(14, 96)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(85, 13)
        Me.BeLabel2.TabIndex = 67
        Me.BeLabel2.Text = "Cuenta contable"
        '
        'cboDestino
        '
        Me.cboDestino.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboDestino.BackColor = System.Drawing.Color.Ivory
        Me.cboDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDestino.ForeColor = System.Drawing.Color.Black
        Me.cboDestino.FormattingEnabled = True
        Me.cboDestino.KeyEnter = True
        Me.cboDestino.Location = New System.Drawing.Point(105, 66)
        Me.cboDestino.Name = "cboDestino"
        Me.cboDestino.Size = New System.Drawing.Size(212, 21)
        Me.cboDestino.TabIndex = 66
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(51, 69)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel3.TabIndex = 65
        Me.BeLabel3.Text = "Destinos"
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(51, 15)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel11.TabIndex = 61
        Me.BeLabel11.Text = "Empresa"
        '
        'cboGastos
        '
        Me.cboGastos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboGastos.BackColor = System.Drawing.Color.Ivory
        Me.cboGastos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGastos.ForeColor = System.Drawing.Color.Black
        Me.cboGastos.FormattingEnabled = True
        Me.cboGastos.KeyEnter = True
        Me.cboGastos.Location = New System.Drawing.Point(105, 39)
        Me.cboGastos.Name = "cboGastos"
        Me.cboGastos.Size = New System.Drawing.Size(212, 21)
        Me.cboGastos.TabIndex = 64
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(59, 42)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel12.TabIndex = 63
        Me.BeLabel12.Text = "Gastos"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.BeLabel1)
        Me.Panel2.Controls.Add(Me.BeLabel6)
        Me.Panel2.Controls.Add(Me.BeLabel4)
        Me.Panel2.Controls.Add(Me.cboEstados)
        Me.Panel2.Controls.Add(Me.txtBusqueda)
        Me.Panel2.Location = New System.Drawing.Point(267, 221)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(295, 83)
        Me.Panel2.TabIndex = 60
        Me.Panel2.Visible = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Red
        Me.BeLabel1.Location = New System.Drawing.Point(273, -1)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(18, 16)
        Me.BeLabel1.TabIndex = 4
        Me.BeLabel1.Text = "X"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.White
        Me.BeLabel6.Location = New System.Drawing.Point(7, 54)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel6.TabIndex = 3
        Me.BeLabel6.Text = "Estado"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.White
        Me.BeLabel4.Location = New System.Drawing.Point(7, 27)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel4.TabIndex = 2
        Me.BeLabel4.Text = "Descripción"
        '
        'cboEstados
        '
        Me.cboEstados.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstados.BackColor = System.Drawing.Color.Ivory
        Me.cboEstados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstados.ForeColor = System.Drawing.Color.Black
        Me.cboEstados.FormattingEnabled = True
        Me.cboEstados.KeyEnter = True
        Me.cboEstados.Location = New System.Drawing.Point(76, 46)
        Me.cboEstados.Name = "cboEstados"
        Me.cboEstados.Size = New System.Drawing.Size(121, 21)
        Me.cboEstados.TabIndex = 1
        '
        'txtBusqueda
        '
        Me.txtBusqueda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBusqueda.BackColor = System.Drawing.Color.Ivory
        Me.txtBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBusqueda.ForeColor = System.Drawing.Color.Black
        Me.txtBusqueda.KeyEnter = True
        Me.txtBusqueda.Location = New System.Drawing.Point(76, 20)
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBusqueda.ShortcutsEnabled = False
        Me.txtBusqueda.Size = New System.Drawing.Size(196, 20)
        Me.txtBusqueda.TabIndex = 0
        Me.txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'gvTipoGastoDestino
        '
        Me.gvTipoGastoDestino.AllowUserToAddRows = False
        Me.gvTipoGastoDestino.AllowUserToOrderColumns = True
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.gvTipoGastoDestino.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.gvTipoGastoDestino.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gvTipoGastoDestino.BackgroundColor = System.Drawing.Color.White
        Me.gvTipoGastoDestino.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvTipoGastoDestino.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDTIPOGASTODESTINO, Me.colIdEmpresa, Me.cbogrillaIdTipoGasto, Me.cbogrillaIdtipoDestino, Me.cbogrillaIdCuentaContable, Me.ESTADO})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvTipoGastoDestino.DefaultCellStyle = DataGridViewCellStyle2
        Me.gvTipoGastoDestino.Location = New System.Drawing.Point(4, 131)
        Me.gvTipoGastoDestino.Name = "gvTipoGastoDestino"
        Me.gvTipoGastoDestino.RowHeadersVisible = False
        Me.gvTipoGastoDestino.Size = New System.Drawing.Size(758, 281)
        Me.gvTipoGastoDestino.TabIndex = 59
        '
        'IDTIPOGASTODESTINO
        '
        Me.IDTIPOGASTODESTINO.DataPropertyName = "IDTIPOGASTODESTINO"
        Me.IDTIPOGASTODESTINO.HeaderText = "IDTIPOGASTODESTINO"
        Me.IDTIPOGASTODESTINO.Name = "IDTIPOGASTODESTINO"
        Me.IDTIPOGASTODESTINO.ReadOnly = True
        Me.IDTIPOGASTODESTINO.Visible = False
        '
        'colIdEmpresa
        '
        Me.colIdEmpresa.DataPropertyName = "IDEMPRESA"
        Me.colIdEmpresa.HeaderText = "Empresa"
        Me.colIdEmpresa.Name = "colIdEmpresa"
        Me.colIdEmpresa.ReadOnly = True
        Me.colIdEmpresa.Visible = False
        Me.colIdEmpresa.Width = 200
        '
        'cbogrillaIdTipoGasto
        '
        Me.cbogrillaIdTipoGasto.DataPropertyName = "IDTIPOGASTO"
        Me.cbogrillaIdTipoGasto.HeaderText = "Gasto"
        Me.cbogrillaIdTipoGasto.Name = "cbogrillaIdTipoGasto"
        Me.cbogrillaIdTipoGasto.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cbogrillaIdTipoGasto.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.cbogrillaIdTipoGasto.Width = 150
        '
        'cbogrillaIdtipoDestino
        '
        Me.cbogrillaIdtipoDestino.DataPropertyName = "IDTIPODESTINO"
        Me.cbogrillaIdtipoDestino.HeaderText = "Destino"
        Me.cbogrillaIdtipoDestino.Name = "cbogrillaIdtipoDestino"
        Me.cbogrillaIdtipoDestino.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cbogrillaIdtipoDestino.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.cbogrillaIdtipoDestino.Width = 150
        '
        'cbogrillaIdCuentaContable
        '
        Me.cbogrillaIdCuentaContable.DataPropertyName = "IDCUENTACONTABLE"
        Me.cbogrillaIdCuentaContable.HeaderText = "Cuenta Contable"
        Me.cbogrillaIdCuentaContable.Name = "cbogrillaIdCuentaContable"
        Me.cbogrillaIdCuentaContable.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cbogrillaIdCuentaContable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.cbogrillaIdCuentaContable.Width = 200
        '
        'ESTADO
        '
        Me.ESTADO.DataPropertyName = "ESTADO"
        Me.ESTADO.HeaderText = "ESTADO"
        Me.ESTADO.Name = "ESTADO"
        Me.ESTADO.ReadOnly = True
        Me.ESTADO.Visible = False
        '
        'frmTipoDestinoGasto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 493)
        Me.Controls.Add(Me.pnCuerpo)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmTipoDestinoGasto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Asignación de Gastos"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.mnuMantenimiento.ResumeLayout(False)
        Me.mnuMantenimiento.PerformLayout()
        Me.pnCuerpo.ResumeLayout(False)
        Me.tabMantenimiento.ResumeLayout(False)
        Me.tabDetalle.ResumeLayout(False)
        Me.pnDetalle.ResumeLayout(False)
        Me.pnDetalle.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.gvTipoGastoDestino, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents mnuMantenimiento As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnRefrescar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimirListado As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnCuerpo As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabMantenimiento As System.Windows.Forms.TabControl
    Friend WithEvents tabDetalle As System.Windows.Forms.TabPage
    Friend WithEvents pnPie As System.Windows.Forms.Panel
    Friend WithEvents pnDetalle As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstados As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtBusqueda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents gvTipoGastoDestino As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboGastos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboDestino As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCuentaContable As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents cboEmpresa As ctrLibreria.Controles.BeComboBox
    Friend WithEvents IDTIPOGASTODESTINO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIdEmpresa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbogrillaIdTipoGasto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents cbogrillaIdtipoDestino As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents cbogrillaIdCuentaContable As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents ESTADO As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
