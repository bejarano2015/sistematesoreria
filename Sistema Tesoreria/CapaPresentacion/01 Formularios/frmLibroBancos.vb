Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmLibroBancos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmLibroBancos = Nothing
    Public Shared Function Instance() As frmLibroBancos
        If frmInstance Is Nothing Then
            frmInstance = New frmLibroBancos
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmLibroBancos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub

  

    Private Sub frmLibroBancos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eChequera As clsChequera
    Private eLibroBancos As clsLibroBancos
    Private eTempo As clsPlantTempo

    Dim iOpcion As Int16 = 0
    Dim sVisualizarDatos As String = ""
    Dim sCargarCargos As String = ""
    Private eITF As clsITF

    Public sCodigoDetPosicion As String = ""
    Dim sVieneDe As Int16
    Dim MesCerrado As Integer = 0
    'Dim SaldoInicialExtracto As Double = 0
    'Dim SaldoFinalExtracto As Double = 0



    Private Sub frmLibroBancos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eTempo = New clsPlantTempo
       
        eChequera = New clsChequera
        cboBancos.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        If eChequera.iNroRegistros > 0 Then
            cboBancos.ValueMember = "IdBanco"
            cboBancos.DisplayMember = "NombreBanco"
            cboBancos.SelectedIndex = -1
        End If

        eChequera = New clsChequera
        cboTipoCargo.DataSource = eChequera.fListarTiposCuenta()
        If eChequera.iNroRegistros > 0 Then
            cboTipoCargo.ValueMember = "IdTipoCuenta"
            cboTipoCargo.DisplayMember = "Abreviado"
            cboTipoCargo.SelectedIndex = -1
        End If


        btnGrabar.Enabled = False
        'btnCancelar.Enabled = False
        btnGrabar.Enabled = False
        btnNuevo.Enabled = False
        btnBuscar.Enabled = False
        btnEliminar.Enabled = False


        Me.ToolTip1.SetToolTip(Me.btnNuevo, "Nuevo")
        Me.ToolTip1.SetToolTip(Me.btnGrabar, "Grabar")
        'Me.ToolTip1.SetToolTip(Me.btnCancelar, "Cancelar")
        Me.ToolTip1.SetToolTip(Me.btnBuscar, "Buscar")
        Me.ToolTip1.SetToolTip(Me.btnEliminar, "Eliminar")
        Me.ToolTip1.SetToolTip(Me.txtImporte, "Enter para Grabar")
        cboAno.Text = Year(Now)
        Label1.Text = ""
        Label2.Text = ""

        dtpFechaCobro.Value = Now.Date().ToShortDateString

    End Sub

    Dim CuentaEncargada As String = ""
    Dim sTipoCuenta As String = ""
    Dim NumeroCuentaEncargada As String = ""
    Private Sub cboBancos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBancos.SelectedIndexChanged
        Dim IdBanco As String = ""
        If (cboBancos.SelectedIndex > -1) Then
            Try
                IdBanco = cboBancos.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuentas.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    cboCuentas.ValueMember = "IdCuenta"
                    cboCuentas.DisplayMember = "NumeroCuenta"
                    cboCuentas.SelectedIndex = -1

                    ''cboCargoAnex.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    ''cboCargoAnex.ValueMember = "IdCuenta"
                    ''cboCargoAnex.DisplayMember = "NumeroCuenta"
                    ''cboCargoAnex.SelectedIndex = -1

                    lblMoneda.Text = ""
                    lblCodMoneda.Text = ""
                    lblTipoCta.Text = ""


                    Dim dtCuentaEn As DataTable
                    dtCuentaEn = New DataTable
                    dtCuentaEn = eChequera.fListarCuentasEncargada(gEmpresa, IdBanco)
                    If dtCuentaEn.Rows.Count = 1 Then
                        CuentaEncargada = dtCuentaEn.Rows(0).Item("IdCuenta")
                        NumeroCuentaEncargada = dtCuentaEn.Rows(0).Item("NumeroCuenta")
                        lblCtaEn.Text = NumeroCuentaEncargada 'CuentaEncargada & "  - " & NumeroCuentaEncargada
                    ElseIf dtCuentaEn.Rows.Count = 0 Then
                        CuentaEncargada = ""
                        NumeroCuentaEncargada = ""
                        lblCtaEn.Text = ""
                    End If

                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            CuentaEncargada = ""
            NumeroCuentaEncargada = ""
            lblCtaEn.Text = ""
        End If
        Limpiar()
    End Sub

    Private Sub cboCuentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuentas.SelectedIndexChanged
        Dim dtTable2 As DataTable
        Dim IdCuenta As String = ""
        If (cboCuentas.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuentas.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtTable2 = New DataTable
                    dtTable2 = eChequera.fListarMoneda(IdCuenta, gEmpresa)
                    lblMoneda.Text = dtTable2.Rows(0).Item("MonDescripcion")
                    lblTipoCta.Text = dtTable2.Rows(0).Item("Descripcion")
                    lblCodMoneda.Text = dtTable2.Rows(0).Item("MonCodigo")
                    sTipoCuenta = dtTable2.Rows(0).Item("TipoCuenta")
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
        Limpiar()
    End Sub

    Private Sub gestionaResaltados2(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(7).Style.BackColor = c
        'visor.Rows(fila).Cells(15).Style.BackColor = c
    End Sub

    Private Sub gestionaResaltados3(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        'visor.Rows(fila).Cells(15).Style.BackColor = c
    End Sub

    Private Sub gestionaResaltados4(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
    End Sub

    Private Sub Limpiar()
        If dgvLibroDet.Rows.Count > 0 Then
            For x As Integer = 0 To dgvLibroDet.RowCount - 1
                dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
            Next
        End If
        LimpiarControlesDetalle()
        DeshabilitarControlesDetalle()
        txtSaldo.Clear()
        txtSaldoInicial.Clear()
        txtCobradosSi.Clear()
        txtCobradosNo.Clear()
        txtCodDet.Clear()
        txtMarca.Clear()
        txtMarca2.Clear()
        txtCod.Clear()

        'btnCancelar.Enabled = False
        btnGrabar.Enabled = False
        btnNuevo.Enabled = False
        btnEliminar.Enabled = False
        btnBuscar.Enabled = False

        btnConciliacion.Enabled = False
        cboMes.SelectedIndex = -1
    End Sub

    ' 
    Dim sCodigoLibroAsignado As String = ""
    Dim sCuentaAsignado As String = ""
    Dim SaldoPrendaBD As Double = 0
    Dim SaldoGarantiaBD As Double = 0
    Dim SaldoDepPlazBD As Double = 0
    Dim SaldoRetencionBD As Double = 0



    Private Sub cboMes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMes.SelectedIndexChanged
        Dim Cobrados As Double = 0
        Dim NoCobrados As Double = 0

        Label1.Text = ""
        If cboBancos.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Banco", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboBancos.Focus()
            Exit Sub
        End If
        If cboCuentas.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCuentas.Focus()
            Exit Sub
        End If
        If cboAno.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboAno.Focus()
            Exit Sub
        End If

        sVisualizarDatos = "NO"

        If cboMes.SelectedIndex > -1 Then
            Dim IdLibro As String = ""
            Dim SaldoInicial As Double = 0
            eLibroBancos = New clsLibroBancos
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            Dim dtLibroDet As DataTable
            dtLibroDet = New DataTable
            dtDatos = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
            If dtDatos.Rows.Count > 0 Then
                IdLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab"))
                txtCod.Text = IdLibro
                SaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial"))

                MesCerrado = Trim(dtDatos.Rows(0).Item("ConciliadoConExtracto"))
                If MesCerrado = 1 Then
                    chkConciliacionMes.Checked = True
                    chkConciliacionMes.Enabled = False

                ElseIf MesCerrado = 0 Then
                    chkConciliacionMes.Checked = False
                    chkConciliacionMes.Enabled = True
                End If


                txtSaldoInicial.Text = Format(Convert.ToDouble(SaldoInicial), "#,##0.00")
                dgvLibroDet.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLibroDet.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLibroDet.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                If dgvLibroDet.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvLibroDet.RowCount - 1
                        dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
                    Next
                    'dtLibroDet.Clear()
                    'dgvLibroDet.DataSource = ""
                End If

                Dim saldo As Double = 0
                saldo = SaldoInicial
                'Dim Cobrados As Double = 0
                'Dim NoCobrados As Double = 0
                'dtLibroDet.Clear()

                dtLibroDet = eLibroBancos.TraerLibroDet(2, IdLibro, gEmpresa)
                If dtLibroDet.Rows.Count > 0 Then
                    Label1.Text = "Total de Registros : " & dtLibroDet.Rows.Count
                    'Dim SaldoPrenda As Double = 0
                    'Dim SaldoGarantia As Double = 0
                    'Dim SaldoDepPla As Double = 0
                    'Dim SaldoRetencion As Double = 0
                    For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                        dgvLibroDet.Rows.Add()

                        'dgvLibroDet.Rows(y).Cells("Column3").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdMovimiento").ToString) = True, "", dtTable.Rows(y).Item("IdMovimiento").ToString) 'Microsoft.VisualBasic.Left(dtDetalle.Rows(y).Item("FechaDetMov"), 10)


                        For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                            If z = 7 Then ' chek
                                If dtLibroDet.Rows(y).Item(7).ToString = "1" Then
                                    dgvLibroDet.Rows(y).Cells(7).Value = True
                                    dgvLibroDet.Rows(y).Cells(13).Value = "1"
                                    dgvLibroDet.Rows(y).Cells(7).ReadOnly = False
                                    dgvLibroDet.Rows(y).Cells(15).ReadOnly = False
                                    If Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        gestionaResaltados2(dgvLibroDet, y, Color.Aqua)
                                    End If
                                ElseIf dtLibroDet.Rows(y).Item(7).ToString = "0" Then
                                    dgvLibroDet.Rows(y).Cells(7).Value = False
                                    dgvLibroDet.Rows(y).Cells(13).Value = "0"
                                    dgvLibroDet.Rows(y).Cells(7).ReadOnly = True
                                    dgvLibroDet.Rows(y).Cells(15).ReadOnly = True
                                    If Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        gestionaResaltados2(dgvLibroDet, y, Color.Silver)
                                    End If
                                End If
                            ElseIf z = 13 Then
                                dgvLibroDet.Rows(y).Cells(12).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 14 Then
                                dgvLibroDet.Rows(y).Cells(14).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 15 Then
                                dgvLibroDet.Rows(y).Cells(16).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 16 Then
                                dgvLibroDet.Rows(y).Cells(17).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 17 Then
                                dgvLibroDet.Rows(y).Cells(18).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 18 Then
                                dgvLibroDet.Rows(y).Cells(19).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 19 Then
                                dgvLibroDet.Rows(y).Cells(20).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 20 Then
                                dgvLibroDet.Rows(y).Cells(21).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 21 Then
                                dgvLibroDet.Rows(y).Cells(22).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 22 Then
                                dgvLibroDet.Rows(y).Cells(23).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 23 Then
                                dgvLibroDet.Rows(y).Cells(24).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 24 Then
                                dgvLibroDet.Rows(y).Cells(25).Value = dtLibroDet.Rows(y).Item(z)
                                Dim TipoCargox As String = Trim(dtLibroDet.Rows(y).Item(z).ToString)
                                If TipoCargox = "00001" Or TipoCargox = "00002" Or TipoCargox = "00003" Or TipoCargox = "00004" Or TipoCargox = "00005" Then
                                    gestionaResaltados3(dgvLibroDet, y, Color.MistyRose)
                                End If
                            ElseIf z = 25 Then
                                dgvLibroDet.Rows(y).Cells(26).Value = dtLibroDet.Rows(y).Item(z)
                                If dgvLibroDet.Rows(y).Cells(26).Value = "1" Then
                                    gestionaResaltados4(dgvLibroDet, y, Color.Yellow)
                                End If
                            ElseIf z = 26 Then
                                'dgvLibroDet.Rows(y).Cells(27).Value = dtLibroDet.Rows(y).Item(z)
                                'If dgvLibroDet.Rows(y).Cells(27).Value = "1" Then
                                '    gestionaResaltados4(dgvLibroDet, y, Color.Yellow)
                                'End If

                                If dtLibroDet.Rows(y).Item(z) = "1" Then
                                    dgvLibroDet.Rows(y).Cells(27).Value = True
                                    dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("ConciliadoS").Value = 1
                                Else
                                    dgvLibroDet.Rows(y).Cells(27).Value = False
                                    dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("ConciliadoS").Value = 0
                                End If
                            ElseIf z = 27 Then
                                dgvLibroDet.Rows(y).Cells("FechaPago").Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 0 Then 'fecha
                                dgvLibroDet.Rows(y).Cells(0).Value = Microsoft.VisualBasic.Left(dtLibroDet.Rows(y).Item(0), 10)
                            ElseIf z = 11 Then 'importe
                                dgvLibroDet.Rows(y).Cells(z).Value = dtLibroDet.Rows(y).Item(z)
                                If dtLibroDet.Rows(y).Item(10).ToString = "D" Then
                                    saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then
                                    saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    If Trim(dtLibroDet.Rows(y).Item("Marca").ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    End If
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    If Trim(dtLibroDet.Rows(y).Item("Marca").ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    End If
                                End If
                                dgvLibroDet.Rows(y).Cells(6).Value = Format(saldo, "#,##0.00")
                            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 8 Or z = 9 Or z = 10 Then '1 2 3 4 5 
                                dgvLibroDet.Rows(y).Cells(z).Value = dtLibroDet.Rows(y).Item(z)
                            End If

                        Next
                    Next


                    'aqui tengo q sumar los cheques cobrados y no cobrados para la conciliacion mensual
                    txtSaldo.Text = Format(saldo, "#,##0.00")
                    txtCobradosSi.Text = Format(Cobrados, "#,##0.00")
                    txtCobradosNo.Text = Format(NoCobrados, "#,##0.00")
                    Dim iresxxxx As Integer = 0
                    iresxxxx = eLibroBancos.fActSaldoLibro(gEmpresa, IdLibro, Format(SaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today()) 'graba el nuevo saldo del libro


                    'txtSaldoInicial.Text = Format(Convert.ToDouble(SaldoInicial), "#,##0.00")
                    btnNuevo.Enabled = True
                    btnBuscar.Enabled = True
                    btnGrabar.Enabled = False
                    'btnCancelar.Enabled = False
                    LimpiarControlesDetalle()
                    DeshabilitarControlesDetalle()
                    iOpcion = 0
                    btnConciliacion.Enabled = True
                    dgvLibroDet.Focus()
                    'Exit Sub
                Else
                    txtSaldo.Text = Format(saldo, "#,##0.00")
                    txtCobradosSi.Text = Format(Cobrados, "#,##0.00")
                    txtCobradosNo.Text = Format(NoCobrados, "#,##0.00")

                    Dim iresxxxx As Integer = 0
                    iresxxxx = eLibroBancos.fActSaldoLibro(gEmpresa, IdLibro, Format(SaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today()) 'graba el nuevo saldo del libro


                    'txtSaldoInicial.Text = Format(Convert.ToDouble(SaldoInicial), "#,##0.00")
                    btnNuevo.Enabled = True
                    btnBuscar.Enabled = True
                    btnGrabar.Enabled = False
                    'btnCancelar.Enabled = False
                    LimpiarControlesDetalle()
                    DeshabilitarControlesDetalle()
                    iOpcion = 0
                    btnConciliacion.Enabled = True
                    dgvLibroDet.Focus()
                    'Exit Sub
                End If
                btnNuevo_Click(sender, e)

                dgvLibroDet.Focus()
            Else
                If dgvLibroDet.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvLibroDet.RowCount - 1
                        dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
                    Next
                End If
                LimpiarControlesDetalle()
                DeshabilitarControlesDetalle()
                txtSaldo.Clear()
                txtSaldoInicial.Clear()
                txtCobradosSi.Clear()
                txtCobradosNo.Clear()
                txtCodDet.Clear()
                txtMarca.Clear()
                txtMarca2.Clear()
                txtCod.Clear()
                'btnCancelar.Enabled = False
                btnGrabar.Enabled = False
                btnNuevo.Enabled = False
                btnBuscar.Enabled = False
                btnEliminar.Enabled = False
                btnConciliacion.Enabled = False

                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                If (MessageBox.Show("No Existe Libro Bancos del Mes de " & cboMes.Text & " / " & cboAno.Text & " �Desea crearlo?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                    Dim Mes As String = ""
                    Dim Anio As String = ""
                    Dim MesAnt As String = ""
                    Dim AnioAnt As String = ""
                    Mes = TraerNumeroMes(Trim(cboMes.Text))
                    MesAnt = Format(Mes - 1, "00") 'resto el mes actual, para usar el mes anterior
                    Anio = Trim(cboAno.Text)
                    If Trim(Mes) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y a�o el ACTUAL -1
                        MesAnt = "12"
                        AnioAnt = Trim(Anio) - 1
                    Else
                        AnioAnt = Anio
                    End If

                    Dim dtLibroAnterior As DataTable
                    dtLibroAnterior = New DataTable
                    dtLibroAnterior = eLibroBancos.TraerIdLibro(AnioAnt, MesAnt, Trim(cboCuentas.SelectedValue), gEmpresa)
                    Dim iResultado As Int16 = 0
                    Dim iResultadoDet2 As Int16 = 0
                    Dim iResultadoActSaldoLibro As Int16 = 0
                    Dim SaldoAnterior As Double = 0
                    Dim sCodigoLibro As String = ""
                    Dim FechaLibro As String = "01/" & Mes & "/" & Anio
                    If dtLibroAnterior.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigo(gEmpresa)
                        sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                        SaldoAnterior = Trim(dtLibroAnterior.Rows(0).Item("Saldo"))

                        Dim SaldoPrenda As Double = 0
                        Dim SaldoGarantia As Double = 0
                        Dim SaldoDepPlaz As Double = 0
                        Dim SaldoRetencion As Double = 0
                        Dim SaldoIniExtracto As Double = 0

                        SaldoPrenda = Trim(dtLibroAnterior.Rows(0).Item("SaldoPrenda"))
                        SaldoGarantia = Trim(dtLibroAnterior.Rows(0).Item("SaldoGarantia"))
                        SaldoDepPlaz = Trim(dtLibroAnterior.Rows(0).Item("SaldoDepPlaz"))
                        SaldoRetencion = Trim(dtLibroAnterior.Rows(0).Item("SaldoRetencion"))

                        SaldoIniExtracto = Trim(dtLibroAnterior.Rows(0).Item("SaldoFinExtracto"))


                        If sTipoCuenta = "00001" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00002" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoAnterior, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00003" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoAnterior, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00004" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoAnterior, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00005" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoAnterior, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        End If


                        'iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                    ElseIf dtLibroAnterior.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                        Dim x As String = ""
                        x = InputBox("Ingrese Saldo Inicial del Libro Bancos", "Libro Bancos", "0.00")
                        If x.Length = 0 Then
                            MessageBox.Show("Se ha Cancelado la Creacion del Libro Bancos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            cboMes.SelectedIndex = -1
                            Exit Sub
                        End If
                        While IsNumeric(Trim(x)) = False
                            MessageBox.Show("Ingrese solo d�gitos como Saldo Inicial", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            x = InputBox("Ingrese Saldo Inicial del Libro Bancos", "Libro Bancos", "0.00")
                            If x.Length = 0 Then
                                MessageBox.Show("Se ha Cancelado la Creacion del Libro Bancos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                cboMes.SelectedIndex = -1
                                Exit Sub
                            End If
                        End While
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigo(gEmpresa)
                        sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                        SaldoAnterior = 0

                        If sTipoCuenta = "00001" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00002" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, x, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00003" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, x, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00004" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, 0, x, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf sTipoCuenta = "00005" Then
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, 0, 0, x, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        End If
                    End If
                    If iResultado = 1 Then
                        cboMes_SelectedIndexChanged(sender, e)
                        MessageBox.Show("Se ha Creado el Libro con Exito!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Else
                    cboMes.SelectedIndex = -1
                    cboMes.Focus()
                    Exit Sub
                End If
                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            End If

            'traemos los saldos de los anexos en el libro asignado
            Dim dtLibroAsi As DataTable
            dtLibroAsi = New DataTable
            dtLibroAsi = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(CuentaEncargada), gEmpresa)
            If dtLibroAsi.Rows.Count = 1 Then
                sCodigoLibroAsignado = Trim(dtLibroAsi.Rows(0).Item("IdLibroCab"))
                SaldoPrendaBD = Trim(dtLibroAsi.Rows(0).Item("SaldoPrenda"))
                SaldoGarantiaBD = Trim(dtLibroAsi.Rows(0).Item("SaldoGarantia"))
                SaldoDepPlazBD = Trim(dtLibroAsi.Rows(0).Item("SaldoDepPlaz"))
                SaldoRetencionBD = Trim(dtLibroAsi.Rows(0).Item("SaldoRetencion"))
            End If

        End If 'If cboMes.SelectedIndex > -1

        ' btnNuevo_Click(sender, e)
        BloquearControles()
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim Cobrados As Double = 0
        Dim NoCobrados As Double = 0
        Dim tipOpe As String = ""
        If rdbAbono.Checked = True Then
            tipOpe = "D"
        End If
        If rdbCargo.Checked = True Then
            tipOpe = "H"
        End If
        If cboTipoMov.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Tipo de Movimiento", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboTipoMov.Focus()
            Exit Sub
        End If
        Dim sCodigoz As String = ""
        Dim dtDatosx As DataTable
        dtDatosx = New DataTable
        If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboAno.SelectedIndex > -1 And cboMes.SelectedIndex > -1 And Len(txtConcepto.Text.Trim) > 0 And Len(tipOpe.Trim) > 0 And Len(txtImporte.Text.Trim) > 0 Then
            dtDatosx = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
        Else
            If cboBancos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Banco", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboBancos.Focus()
                Exit Sub
            End If
            If cboCuentas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCuentas.Focus()
                Exit Sub
            End If
            If cboAno.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboAno.Focus()
                Exit Sub
            End If
            If cboMes.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Mes", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboMes.Focus()
                Exit Sub
            End If
            If Len(tipOpe.Trim) = 0 Then
                MessageBox.Show("Seleccione un tipo de Operacion", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'cboOpcion.Focus()
                Exit Sub
            End If

            If Len(txtConcepto.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese un Concepto", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtConcepto.Focus()
                Exit Sub
            End If

            If Len(txtImporte.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese un Importe", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtImporte.Focus()
                Exit Sub
            End If
        End If

        If dtDatosx.Rows.Count = 1 Then 'si
            sCodigoz = Trim(dtDatosx.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
            'Cobrados = Trim(dtDatosx.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
            'NoCobrados = Trim(dtDatosx.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
        End If

        Dim dtDataLibro As DataTable
        Dim xMes As String = ""
        Dim xAno As String = ""
        dtDataLibro = New DataTable
        dtDataLibro = eLibroBancos.TraerMesyAnoLibro(gEmpresa, sCodigoz)
        If dtDataLibro.Rows.Count > 0 Then
            xMes = Trim(dtDataLibro.Rows(0).Item("Mes"))
            xAno = Trim(dtDataLibro.Rows(0).Item("A�o"))
        End If
        Dim pMes As String = Format(dtpFecha.Value.Month, "00")
        Dim pA�o As String = dtpFecha.Value.Year

        'If xMes <> pMes Or xAno <> pA�o Then
        'MessageBox.Show("El Mes/A�o del registro a Grabar/Actualizar no Pertenece al Mes/A�o del Libro Bancos Actual", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'dtpFecha.Focus()
        'Exit Sub
        'Else
        If xMes <> pMes Then
            MessageBox.Show("El Mes de la Fecha del registro a Grabar/Actualizar no Pertenece al Mes del Libro Bancos Actual", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFecha.Focus()
            Exit Sub
        End If
        If xAno <> pA�o Then
            MessageBox.Show("El A�o de la Fecha del registro a Grabar/Actualizar no Pertenece al A�o del Libro Bancos Actual", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFecha.Focus()
            Exit Sub
        End If
        'End If

        'Dim StrCad As String = ""
        Dim sCodigo As String = ""
        Dim sCodigoDet As String = ""
        Dim sCodigoDetAnterior As String = ""
        Dim iResultado As Int32 = 0
        Dim iResultadoDet As Int32 = 0
        Dim iResultadoActSaldoLibro As Int32 = 0
        Dim SaldoNew As Double = 0

        If iOpcion = 4 Then
            'If (MessageBox.Show("�Esta seguro de Grabar?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
            If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboAno.SelectedIndex > -1 And cboMes.SelectedIndex > -1 And Len(txtConcepto.Text.Trim) > 0 And Len(tipOpe.Trim) > 0 And Len(txtImporte.Text.Trim) > 0 Then
                Dim dtDatos As DataTable
                dtDatos = New DataTable
                dtDatos = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
                'pregunta si ya existe libro de ese mes y a�o, de la cuenta...
                If dtDatos.Rows.Count = 0 Then 'no
                ElseIf dtDatos.Rows.Count = 1 Then 'si
                    sCodigo = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro

                    eLibroBancos = New clsLibroBancos
                    eLibroBancos.fCodigoDet(gEmpresa, sCodigo)
                    sCodigoDet = eLibroBancos.sCodFuturoDet 'genera codigo_detalle nuevo
                    sCodigoDetPosicion = Trim(sCodigoDet)
                    '---------------------

                    Dim dSaldoInicial As Double = 0
                    dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                    Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                    NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial

                    'graba el detalle de libro
                    Dim MarcaCheqNoCobrado As Integer = 0
                    If Trim(cboTipoMov.SelectedValue) = "00002" Then
                        MarcaCheqNoCobrado = 1
                    ElseIf Trim(cboTipoMov.SelectedValue) <> "00002" Then
                        MarcaCheqNoCobrado = 0
                    End If

                    Dim NroMovimiento As String = ""
                    If Len(txtNroMov.Text.Trim) = 0 Then
                        NroMovimiento = ""
                    ElseIf Len(txtNroMov.Text.Trim) > 0 Then
                        NroMovimiento = txtNroMov.Text.Trim
                    End If

                    Dim Cliente As String = ""
                    If Len(txtCliente.Text.Trim) = 0 Then
                        Cliente = ""
                    ElseIf Len(txtCliente.Text.Trim) > 0 Then
                        Cliente = txtCliente.Text.Trim
                    End If

                    Dim CtaCliente As String = ""
                    If Len(txtCtaCliente.Text.Trim) = 0 Then
                        CtaCliente = ""
                    ElseIf Len(txtCtaCliente.Text.Trim) > 0 Then
                        CtaCliente = txtCtaCliente.Text.Trim
                    End If

                    Dim Obra As String = ""
                    If Len(txtObra.Text.Trim) = 0 Then
                        Obra = ""
                    ElseIf Len(txtObra.Text.Trim) > 0 Then
                        Obra = txtObra.Text.Trim
                    End If

                    Dim TipoCargo As String = ""
                    If cboTipoCargo.SelectedIndex = -1 Then
                        TipoCargo = ""
                    ElseIf cboTipoCargo.SelectedIndex > -1 Then
                        TipoCargo = cboTipoCargo.SelectedValue
                    End If

                    iResultadoDet = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigo, dtpFecha.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtDescripcion.Text), Trim(tipOpe), Convert.ToDouble(txtImporte.Text), SaldoNew, MarcaCheqNoCobrado, 0, NroMovimiento, dtpFIni.Value, dtpFfin.Value, Convert.ToDouble(txtImporte.Text), lblCodMoneda.Text.Trim, cboCuentas.SelectedValue, Cliente, CtaCliente, Obra, TipoCargo, "", "")

                    If Len(Trim(sCodigoDet)) > 0 Then
                        Dim iResultadox As Integer = 0
                        iResultadox = eLibroBancos.fIdenti(52, gEmpresa, sCodigoDet, sCodigo, Convert.ToString(IIf(Me.chkFecha.Checked = True, 1, 0)), dtpFechaPago.Value)
                    End If

                    If iResultadoDet = 1 Then
                        If cboTipoCargo.SelectedIndex > -1 Then
                            If cboTipoCargo.SelectedValue = "00002" Then
                                SaldoPrendaBD = SaldoPrendaBD + Convert.ToDouble(txtImporte.Text)
                            ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                SaldoGarantiaBD = SaldoGarantiaBD + Convert.ToDouble(txtImporte.Text)
                            ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                SaldoDepPlazBD = SaldoDepPlazBD + Convert.ToDouble(txtImporte.Text)
                            ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                SaldoRetencionBD = SaldoRetencionBD + Convert.ToDouble(txtImporte.Text)
                            End If
                            Dim iResLibroAsignado As Integer = 0
                            iResLibroAsignado = eLibroBancos.fActualizarLibroAsignado(43, sCodigoLibroAsignado, gEmpresa, SaldoPrendaBD, SaldoGarantiaBD, SaldoDepPlazBD, SaldoRetencionBD)
                        End If
                    End If


                    If iResultadoDet = 1 Then
                        'ACTUALIZAR EL SALDO DEL LIBRO
                        Dim dtLibroDet As DataTable
                        dtLibroDet = New DataTable
                        Dim saldo As Double = 0
                        saldo = dSaldoInicial
                        'Dim Cobrados As Double = 0
                        'Dim NoCobrados As Double = 0
                        dtLibroDet = eLibroBancos.TraerLibroDet(37, sCodigo, gEmpresa)
                        If dtLibroDet.Rows.Count > 0 Then
                            For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                    If z = 11 Then 'lee el importe
                                        If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                            saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCodigo), saldo)
                                        ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                            saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCodigo), saldo)
                               
                                        End If
                                    End If
                                Next
                            Next
                            Dim dtDtFechx As DataTable
                            dtDtFechx = New DataTable
                            Dim Fechax As DateTime
                            Dim FechaActCabecerax As DateTime
                            dtDtFechx = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCodigo)
                            If dtDtFechx.Rows.Count > 0 Then
                                Fechax = Trim(dtDtFechx.Rows(0).Item("Fecha"))
                            End If
                            If dtpFecha.Value > Fechax Then
                                FechaActCabecerax = dtpFecha.Value
                            Else
                                FechaActCabecerax = Fechax
                            End If


                            If Trim(cboTipoMov.SelectedValue) = "00002" Then
                                NoCobrados = NoCobrados + Convert.ToDouble(txtImporte.Text)
                            End If

                            If sTipoCuenta = "00002" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibroPrenda(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00003" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibroGarantia(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00004" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoDepPlaz(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00005" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoRetencion(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libroelse
                            Else
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax) 'graba el nuevo saldo del libro
                            End If


                            '----------------------------------------
                            'saber si existe libros proximos al mes actual de la cuenta
                            Dim dtDatosLibro As DataTable
                            dtDatosLibro = New DataTable
                            dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigo, gEmpresa)
                            Dim sIdCuenta As String = ""
                            If dtDatosLibro.Rows.Count > 0 Then
                                sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                            End If
                            Dim dtLibrosSuperiores As DataTable
                            dtLibrosSuperiores = New DataTable
                            Dim A�o As String = dtpFecha.Value.Year
                            Dim Mes As String = Format(dtpFecha.Value.Month, "00")
                            dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecerax) 'trae los proximos libros
                            If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                    Dim iResultadoActLibroSup As Int16 = 0
                                    Dim CodigoLibroBD As String = ""
                                    Dim SaldoInicialBD As Double ' = 0
                                    Dim SaldoFinalBD As Double '= 0
                                    CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    SaldoInicialBD = Format(saldo, "0.00")
                                    If tipOpe = "D" Then
                                        SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + Convert.ToDouble(txtImporte.Text)
                                    ElseIf tipOpe = "H" Then
                                        SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtImporte.Text)
                                    End If
                                    Dim SaldoPrendaS As Double
                                    Dim SaldoGarantiaS As Double
                                    Dim SaldoDepPlazS As Double
                                    Dim SaldoRetencionS As Double
                                    If CuentaEncargada <> "" Then
                                        If sIdCuenta <> CuentaEncargada Then
                                            SaldoPrendaS = 0
                                            SaldoGarantiaS = 0
                                            SaldoDepPlazS = 0
                                            SaldoRetencionS = 0
                                        ElseIf sIdCuenta = CuentaEncargada Then
                                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                            If cboTipoCargo.SelectedIndex > -1 Then
                                                If cboTipoCargo.SelectedValue = "00002" Then
                                                    SaldoPrendaS = SaldoPrendaS + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                    SaldoGarantiaS = SaldoGarantiaS + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                    SaldoDepPlazS = SaldoDepPlazS + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                    SaldoRetencionS = SaldoRetencionS + Convert.ToDouble(txtImporte.Text)
                                                End If
                                            End If
                                        End If
                                    Else
                                        If sTipoCuenta = "00001" Then
                                        ElseIf sTipoCuenta = "00002" Then
                                            SaldoPrendaS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00003" Then
                                            SaldoGarantiaS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00004" Then
                                            SaldoDepPlazS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00005" Then
                                            SaldoRetencionS = SaldoFinalBD
                                        End If
                                    End If
                                    '''''''''''''''''''''''''''''''''''''''''
                                    'graba saldos de uno de los libro proximos
                                    iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                    saldo = SaldoFinalBD
                                Next

                                If CuentaEncargada <> "" Then
                                    If sIdCuenta <> CuentaEncargada Then
                                        Dim dtLibrosSuperiores2 As DataTable
                                        dtLibrosSuperiores2 = New DataTable
                                        Dim A�o2 As String = dtpFecha.Value.Year
                                        Dim Mes2 As String = Format(dtpFecha.Value.Month, "00")
                                        dtLibrosSuperiores2 = eLibroBancos.TraerLibrosSuperiores(Mes2, A�o2, CuentaEncargada, gEmpresa, dtpFecha.Value) 'trae los proximos libros
                                        If dtLibrosSuperiores2.Rows.Count > 0 Then
                                            For y As Integer = 0 To dtLibrosSuperiores2.Rows.Count - 1
                                                Dim iResultadoActLibroSup As Int16 = 0
                                                Dim CodigoLibroBD As String = ""
                                                Dim SaldoInicialBD As Double ' = 0
                                                Dim SaldoFinalBD As Double '= 0
                                                CodigoLibroBD = Trim(dtLibrosSuperiores2.Rows(y).Item(0).ToString)
                                                SaldoInicialBD = Trim(dtLibrosSuperiores2.Rows(y).Item(1).ToString)
                                                SaldoFinalBD = Trim(dtLibrosSuperiores2.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                                Cobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(9).ToString)
                                                NoCobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(10).ToString)
                                                '''''''''''''''''''''''''''''''''''''''''
                                                Dim SaldoPrendaS As Double
                                                Dim SaldoGarantiaS As Double
                                                Dim SaldoDepPlazS As Double
                                                Dim SaldoRetencionS As Double
                                                SaldoPrendaS = Trim(dtLibrosSuperiores2.Rows(y).Item(5).ToString)
                                                SaldoGarantiaS = Trim(dtLibrosSuperiores2.Rows(y).Item(6).ToString)
                                                SaldoDepPlazS = Trim(dtLibrosSuperiores2.Rows(y).Item(7).ToString)
                                                SaldoRetencionS = Trim(dtLibrosSuperiores2.Rows(y).Item(8).ToString)
                                                If cboTipoCargo.SelectedIndex > -1 Then
                                                    If cboTipoCargo.SelectedValue = "00002" Then
                                                        SaldoPrendaS = SaldoPrendaS + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                        SaldoGarantiaS = SaldoGarantiaS + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                        SaldoDepPlazS = SaldoDepPlazS + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                        SaldoRetencionS = SaldoRetencionS + Convert.ToDouble(txtImporte.Text)
                                                    End If
                                                End If
                                                'graba saldos de uno de los libro proximos
                                                iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                                'saldo = SaldoFinalBD
                                            Next
                                        End If
                                    End If 'If sIdCuenta <> CuentaEncargada Then
                                End If 'If CuentaEncargada <> "" Then
                            Else
                                If CuentaEncargada <> "" Then
                                    Dim dtLibrosSuperiores2 As DataTable
                                    dtLibrosSuperiores2 = New DataTable
                                    Dim A�o2 As String = dtpFecha.Value.Year
                                    Dim Mes2 As String = Format(dtpFecha.Value.Month, "00")
                                    dtLibrosSuperiores2 = eLibroBancos.TraerLibrosSuperiores(Mes2, A�o2, CuentaEncargada, gEmpresa, dtpFecha.Value) 'trae los proximos libros
                                    If dtLibrosSuperiores2.Rows.Count > 0 Then
                                        For y As Integer = 0 To dtLibrosSuperiores2.Rows.Count - 1
                                            Dim iResultadoActLibroSup As Int16 = 0
                                            Dim CodigoLibroBD As String = ""
                                            Dim SaldoInicialBD As Double ' = 0
                                            Dim SaldoFinalBD As Double '= 0
                                            CodigoLibroBD = Trim(dtLibrosSuperiores2.Rows(y).Item(0).ToString)
                                            SaldoInicialBD = Trim(dtLibrosSuperiores2.Rows(y).Item(1).ToString)
                                            SaldoFinalBD = Trim(dtLibrosSuperiores2.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                            Cobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(9).ToString)
                                            NoCobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(10).ToString)
                                            '''''''''''''''''''''''''''''''''''''''''
                                            Dim SaldoPrendaS As Double
                                            Dim SaldoGarantiaS As Double
                                            Dim SaldoDepPlazS As Double
                                            Dim SaldoRetencionS As Double
                                            SaldoPrendaS = Trim(dtLibrosSuperiores2.Rows(y).Item(5).ToString)
                                            SaldoGarantiaS = Trim(dtLibrosSuperiores2.Rows(y).Item(6).ToString)
                                            SaldoDepPlazS = Trim(dtLibrosSuperiores2.Rows(y).Item(7).ToString)
                                            SaldoRetencionS = Trim(dtLibrosSuperiores2.Rows(y).Item(8).ToString)
                                            If cboTipoCargo.SelectedIndex > -1 Then
                                                If cboTipoCargo.SelectedValue = "00002" Then
                                                    SaldoPrendaS = SaldoPrendaS + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                    SaldoGarantiaS = SaldoGarantiaS + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                    SaldoDepPlazS = SaldoDepPlazS + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                    SaldoRetencionS = SaldoRetencionS + Convert.ToDouble(txtImporte.Text)
                                                End If
                                            End If
                                            '''''''''''''''''''''''''''''''''''''''''
                                            'graba saldos de uno de los libro proximos
                                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                            'saldo = SaldoFinalBD
                                        Next
                                    End If
                                End If 'If CuentaEncargada <> "" Then

                            End If
                            '----------------------------------------
                        End If
                        cboMes_SelectedIndexChanged(sender, e) 'lista el nuevo detalle y calcula el saldo al final
                        If iResultadoActSaldoLibro = 1 Then
                            'MessageBox.Show("Se Agrego con Exito!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            btnNuevo.Enabled = True
                            'btnCancelar.Enabled = False
                            btnGrabar.Enabled = False

                            btnNuevo_Click(sender, e)
                            'dgvLibroDet.Focus()

                            '''''''''''''Posicionar''''''''''''''''''''''''
                            Posicionar()
                            '''''''''''''Posicionar''''''''''''''''''''''''

                            dtpFecha.Focus()
                        End If
                    End If
                End If 'If dtDatos.Rows.Count = 0 Then 'no
            Else 'campos no rellenados
                If cboBancos.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Banco", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboBancos.Focus()
                    Exit Sub
                End If
                If cboCuentas.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboCuentas.Focus()
                    Exit Sub
                End If
                If cboAno.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboAno.Focus()
                    Exit Sub
                End If
                If cboMes.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Mes", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboMes.Focus()
                    Exit Sub
                End If
                If Len(txtConcepto.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese un Concepto", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtConcepto.Focus()
                    Exit Sub
                End If
                If Len(tipOpe.Trim) = 0 Then
                    MessageBox.Show("Seleccione un tipo sw Operacion", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'cboOpcion.Focus()
                    Exit Sub
                End If
                If Len(txtImporte.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese un Importe", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtImporte.Focus()
                    Exit Sub
                End If
            End If 'campos no rellenados
            'End If 'pregunta Grabar
        ElseIf iOpcion = 5 Then
            If dgvLibroDet.Rows.Count > 0 Then
                If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = True Then
                    MessageBox.Show("Este registro ya esta conciliado y no se puede modificar!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                    'ElseIf Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = False Then
                    '    btnGrabar.Enabled = True
                    '    btnEliminar.Enabled = True
                End If
            End If
            Dim iResultadoActDetalle As Int32 = 0
            'If (MessageBox.Show("�Esta seguro de Actualizar?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
            If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboAno.SelectedIndex > -1 And cboMes.SelectedIndex > -1 And Len(txtConcepto.Text.Trim) > 0 And Len(tipOpe.Trim) > 0 And Len(txtImporte.Text.Trim) > 0 Then
                Dim dtDatos As DataTable
                dtDatos = New DataTable
                dtDatos = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
                If dtDatos.Rows.Count = 1 Then 'si
                    sCodigo = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                    Dim dSaldoInicial As Double = 0
                    dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial

                    Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                    NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial

                    Dim MarcaCheqNoCobrado As Integer = 0
                    If Trim(cboTipoMov.SelectedValue) = "00002" Then
                        MarcaCheqNoCobrado = 1
                    ElseIf Trim(cboTipoMov.SelectedValue) <> "00002" Then
                        MarcaCheqNoCobrado = 0

                        If dgvLibroDet.Rows.Count > 0 Then
                            If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
                                MarcaCheqNoCobrado = 0
                            Else
                                MarcaCheqNoCobrado = 1
                            End If
                        End If
                        'Calcular()

                    End If

                    Dim NroMovimiento As String = ""
                    If Len(txtNroMov.Text.Trim) = 0 Then
                        NroMovimiento = ""
                    ElseIf Len(txtNroMov.Text.Trim) > 0 Then
                        NroMovimiento = txtNroMov.Text.Trim
                    End If

                    Dim Cliente As String = ""
                    If Len(txtCliente.Text.Trim) = 0 Then
                        Cliente = ""
                    ElseIf Len(txtCliente.Text.Trim) > 0 Then
                        Cliente = txtCliente.Text.Trim
                    End If

                    Dim CtaCliente As String = ""
                    If Len(txtCtaCliente.Text.Trim) = 0 Then
                        CtaCliente = ""
                    ElseIf Len(txtCtaCliente.Text.Trim) > 0 Then
                        CtaCliente = txtCtaCliente.Text.Trim
                    End If

                    Dim Obra As String = ""
                    If Len(txtObra.Text.Trim) = 0 Then
                        Obra = ""
                    ElseIf Len(txtObra.Text.Trim) > 0 Then
                        Obra = txtObra.Text.Trim
                    End If

                    Dim TipoCargo As String = ""
                    If cboTipoCargo.SelectedIndex = -1 Then
                        TipoCargo = ""
                    ElseIf cboTipoCargo.SelectedIndex > -1 Then
                        TipoCargo = cboTipoCargo.SelectedValue
                    End If

                    sCodigoDetPosicion = Trim(txtCodDet.Text)

                    iResultadoActDetalle = eLibroBancos.fActDet(gEmpresa, Trim(txtCodDet.Text), sCodigo, dtpFecha.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtDescripcion.Text), Trim(tipOpe), Convert.ToDouble(txtImporte.Text), 0, MarcaCheqNoCobrado, 0, NroMovimiento, dtpFIni.Value, dtpFfin.Value, Convert.ToDouble(txtImporte.Text), lblCodMoneda.Text.Trim, cboCuentas.SelectedValue, Cliente, CtaCliente, Obra, TipoCargo)
                    'If Len(Trim(sCodigoDet)) > 0 Then
                    Dim iResultadox As Integer = 0
                    iResultadox = eLibroBancos.fIdenti(52, gEmpresa, Trim(txtCodDet.Text), sCodigo, Convert.ToString(IIf(Me.chkFecha.Checked = True, 1, 0)), dtpFechaPago.Value)
                    'End If
                    ''''''''''''''''''genera itf''''''''''''''''''''''''''''''
       
                    If iResultadoActDetalle > 0 Then
                        If cboTipoCargo.SelectedIndex > -1 Then

                            Dim ImporteOculto As Double = 0
                            Dim CargoOculto As String = ""
                            If dgvLibroDet.Rows.Count > 0 Then
                                ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                            End If

                            Dim SaldoPrenda As Double = 0
                            Dim SaldoGarantia As Double = 0
                            Dim SaldoDepPla As Double = 0
                            Dim SaldoRetencion As Double = 0

                            If CargoOculto <> cboTipoCargo.SelectedValue Then
                                If CargoOculto = "00002" Then
                                    SaldoPrendaBD = SaldoPrendaBD - ImporteOculto
                                ElseIf CargoOculto = "00003" Then
                                    SaldoGarantiaBD = SaldoGarantiaBD - ImporteOculto
                                ElseIf CargoOculto = "00004" Then
                                    SaldoDepPlazBD = SaldoDepPlazBD - ImporteOculto
                                ElseIf CargoOculto = "00005" Then
                                    SaldoRetencionBD = SaldoRetencionBD - ImporteOculto
                                End If

                                If cboTipoCargo.SelectedValue = "00002" Then
                                    SaldoPrendaBD = SaldoPrendaBD + Convert.ToDouble(txtImporte.Text)
                                ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                    SaldoGarantiaBD = SaldoGarantiaBD + Convert.ToDouble(txtImporte.Text)
                                ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                    SaldoDepPlazBD = SaldoDepPlazBD + Convert.ToDouble(txtImporte.Text)
                                ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                    SaldoRetencionBD = SaldoRetencionBD + Convert.ToDouble(txtImporte.Text)
                                End If

                            ElseIf CargoOculto = cboTipoCargo.SelectedValue Then
                                Dim importetex = Format(txtImporte.Text, "0.00")
                                If txtImporte.Text = ImporteOculto Then
                                    If cboTipoCargo.SelectedValue = "00002" Then
                                        SaldoPrendaBD = SaldoPrendaBD
                                    ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                        SaldoGarantiaBD = SaldoGarantiaBD
                                    ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                        SaldoDepPlazBD = SaldoDepPlazBD
                                    ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                        SaldoRetencionBD = SaldoRetencionBD
                                    End If
                                Else
                                    If cboTipoCargo.SelectedValue = "00002" Then
                                        SaldoPrendaBD = (SaldoPrendaBD + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                    ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                        SaldoGarantiaBD = (SaldoGarantiaBD + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                    ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                        SaldoDepPlazBD = (SaldoDepPlazBD + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                    ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                        SaldoRetencionBD = (SaldoRetencionBD + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                    End If
                                End If
                            End If
                            Dim iResLibroAsignado As Integer = 0
                            iResLibroAsignado = eLibroBancos.fActualizarLibroAsignado(43, sCodigoLibroAsignado, gEmpresa, SaldoPrendaBD, SaldoGarantiaBD, SaldoDepPlazBD, SaldoRetencionBD)
                        End If
                    End If
                    'End If
                    If iResultadoActDetalle > 0 Then
                        Dim dtLibroDet As DataTable
                        dtLibroDet = New DataTable
                        Dim saldo As Double = 0
                        saldo = dSaldoInicial
                        'Dim Cobrados As Double = 0
                        'Dim NoCobrados As Double = 0
                        dtLibroDet = eLibroBancos.TraerLibroDet(37, sCodigo, gEmpresa)
                        If dtLibroDet.Rows.Count > 0 Then
                            For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                    If z = 11 Then 'lee el importe
                                        If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                            saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCodigo), saldo)
                                        ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                            saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCodigo), saldo)
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                            '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                        End If
                                    End If
                                Next
                            Next
                            Dim dtDtFechx As DataTable
                            dtDtFechx = New DataTable
                            Dim Fechax As DateTime
                            Dim FechaActCabecerax As DateTime
                            dtDtFechx = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCodigo)
                            If dtDtFechx.Rows.Count > 0 Then
                                Fechax = Trim(dtDtFechx.Rows(0).Item("Fecha"))
                            End If
                            If dtpFecha.Value > Fechax Then
                                FechaActCabecerax = dtpFecha.Value
                            Else
                                FechaActCabecerax = Fechax
                            End If
                            'iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax) 'graba el nuevo saldo del libro
                            'If sTipoCuenta = "00003" Then
                            '    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibroGarantia(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            'Else
                            '    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax) 'graba el nuevo saldo del libro
                            '    'iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax) 'graba el nuevo saldo del libro
                            'End If

                            'Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                            'NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial

                            'If sTipoCuenta = "00001" Then
                            '    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax) 'graba el nuevo saldo del libro
                            'Else
                            If sTipoCuenta = "00002" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibroPrenda(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00003" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibroGarantia(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00004" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoDepPlaz(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00005" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoRetencion(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            Else
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigo, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax) 'graba el nuevo saldo del libro
                            End If

                            '----------------------------------------
                            'saber si existe libros proximos al mes actual de la cuenta
                            Dim dtDatosLibro As DataTable
                            dtDatosLibro = New DataTable
                            dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigo, gEmpresa)
                            Dim sIdCuenta As String = ""
                            If dtDatosLibro.Rows.Count > 0 Then
                                sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                            End If
                            Dim dtLibrosSuperiores As DataTable
                            dtLibrosSuperiores = New DataTable
                            Dim A�o As String = dtpFecha.Value.Year
                            Dim Mes As String = Format(dtpFecha.Value.Month, "00")
                            dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecerax) 'trae los proximos libros
                            If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                    Dim iResultadoActLibroSup As Int16 = 0
                                    Dim CodigoLibroBD As String = ""
                                    Dim SaldoInicialBD As Double ' = 0
                                    Dim SaldoFinalBD As Double '= 0
                                    CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    SaldoInicialBD = Format(saldo, "0.00")

                                    Dim ImporteOculto As Double = 0
                                    If dgvLibroDet.Rows.Count > 0 Then
                                        ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                    End If
                                    Dim TipoOpeOculto As String = ""
                                    If dgvLibroDet.Rows.Count > 0 Then
                                        TipoOpeOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(10).Value)
                                    End If
                                    'CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    'SaldoInicialBD = Format(saldo, "0.00")
                                    Dim MontoNuevo As Double = Format(Convert.ToDouble(txtImporte.Text), "0.00")
                                    If tipOpe = TipoOpeOculto Then
                                        If tipOpe = "D" Then
                                            If MontoNuevo = ImporteOculto Then
                                                SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                            Else
                                                SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - ImporteOculto) + Convert.ToDouble(txtImporte.Text)
                                            End If
                                            '    SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - ImporteOculto) + Convert.ToDouble(txtImporte.Text)
                                        ElseIf tipOpe = "H" Then
                                            '    SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - ImporteOculto) - Convert.ToDouble(txtImporte.Text)
                                            If MontoNuevo = ImporteOculto Then
                                                SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                            Else
                                                SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto) - Convert.ToDouble(txtImporte.Text)
                                            End If
                                        End If
                                    ElseIf tipOpe <> TipoOpeOculto Then
                                        If tipOpe = "D" Then
                                            If MontoNuevo = ImporteOculto Then
                                                SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto) + Convert.ToDouble(txtImporte.Text)
                                                'SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                            Else
                                                SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto) + Convert.ToDouble(txtImporte.Text)
                                            End If
                                        ElseIf tipOpe = "H" Then
                                            If MontoNuevo = ImporteOculto Then
                                                SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - ImporteOculto) - Convert.ToDouble(txtImporte.Text)
                                                'SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                            Else
                                                SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - ImporteOculto) - Convert.ToDouble(txtImporte.Text)
                                            End If
                                        End If
                                    End If
                                    Dim SaldoPrendaS As Double
                                    Dim SaldoGarantiaS As Double
                                    Dim SaldoDepPlazS As Double
                                    Dim SaldoRetencionS As Double
                                    If CuentaEncargada <> "" Then
                                        If sIdCuenta <> CuentaEncargada Then
                                            SaldoPrendaS = 0
                                            SaldoGarantiaS = 0
                                            SaldoDepPlazS = 0
                                            SaldoRetencionS = 0
                                        ElseIf sIdCuenta = CuentaEncargada Then
                                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                            If cboTipoCargo.SelectedIndex > -1 Then

                                                'Dim ImporteOculto As Double = 0
                                                Dim CargoOculto As String = ""
                                                If dgvLibroDet.Rows.Count > 0 Then
                                                    ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                                    CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                                                End If

                                                If CargoOculto <> cboTipoCargo.SelectedValue Then
                                                    If CargoOculto = "00002" Then
                                                        SaldoPrendaS = SaldoPrendaS - ImporteOculto
                                                    ElseIf CargoOculto = "00003" Then
                                                        SaldoGarantiaS = SaldoGarantiaS - ImporteOculto
                                                    ElseIf CargoOculto = "00004" Then
                                                        SaldoDepPlazS = SaldoDepPlazS - ImporteOculto
                                                    ElseIf CargoOculto = "00005" Then
                                                        SaldoRetencionS = SaldoRetencionS - ImporteOculto
                                                    End If

                                                    If cboTipoCargo.SelectedValue = "00002" Then
                                                        SaldoPrendaS = SaldoPrendaS + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                        SaldoGarantiaS = SaldoGarantiaS + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                        SaldoDepPlazS = SaldoDepPlazS + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                        SaldoRetencionS = SaldoRetencionS + Convert.ToDouble(txtImporte.Text)
                                                    End If

                                                ElseIf CargoOculto = cboTipoCargo.SelectedValue Then
                                                    Dim importetex = Format(txtImporte.Text, "0.00")
                                                    If txtImporte.Text = ImporteOculto Then
                                                        If cboTipoCargo.SelectedValue = "00002" Then
                                                            SaldoPrendaS = SaldoPrendaS
                                                        ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                            SaldoGarantiaS = SaldoGarantiaS
                                                        ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                            SaldoDepPlazS = SaldoDepPlazS
                                                        ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                            SaldoRetencionS = SaldoRetencionS
                                                        End If
                                                    Else
                                                        If cboTipoCargo.SelectedValue = "00002" Then
                                                            SaldoPrendaS = (SaldoPrendaS + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                            SaldoGarantiaS = (SaldoGarantiaS + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                            SaldoDepPlazS = (SaldoDepPlazS + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                            SaldoRetencionS = (SaldoRetencionS + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        End If
                                                    End If
                                                End If

                                            End If
                                        End If
                                    Else
                                        If sTipoCuenta = "00001" Then
                                        ElseIf sTipoCuenta = "00002" Then
                                            SaldoPrendaS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00003" Then
                                            SaldoGarantiaS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00004" Then
                                            SaldoDepPlazS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00005" Then
                                            SaldoRetencionS = SaldoFinalBD
                                        End If
                                    End If
                                    '''''''''''''''''''''''''''''''''''''''''
                                    'graba saldos de uno de los libro proximos
                                    iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)

                                    'graba saldos de uno de los libro proximos
                                    'iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax)
                                    saldo = SaldoFinalBD
                                Next

                                If CuentaEncargada <> "" Then
                                    If sIdCuenta <> CuentaEncargada Then
                                        Dim dtLibrosSuperiores2 As DataTable
                                        dtLibrosSuperiores2 = New DataTable
                                        Dim A�o2 As String = dtpFecha.Value.Year
                                        Dim Mes2 As String = Format(dtpFecha.Value.Month, "00")
                                        dtLibrosSuperiores2 = eLibroBancos.TraerLibrosSuperiores(Mes2, A�o2, CuentaEncargada, gEmpresa, dtpFecha.Value) 'trae los proximos libros
                                        If dtLibrosSuperiores2.Rows.Count > 0 Then
                                            For y As Integer = 0 To dtLibrosSuperiores2.Rows.Count - 1
                                                Dim iResultadoActLibroSup As Int16 = 0
                                                Dim CodigoLibroBD As String = ""
                                                Dim SaldoInicialBD As Double ' = 0
                                                Dim SaldoFinalBD As Double '= 0
                                                CodigoLibroBD = Trim(dtLibrosSuperiores2.Rows(y).Item(0).ToString)
                                                SaldoInicialBD = Trim(dtLibrosSuperiores2.Rows(y).Item(1).ToString)
                                                SaldoFinalBD = Trim(dtLibrosSuperiores2.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                                Cobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(9).ToString)
                                                NoCobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(10).ToString)

                                                Dim SaldoPrenda As Double
                                                Dim SaldoGarantia As Double
                                                Dim SaldoDepPla As Double
                                                Dim SaldoRetencion As Double
                                                SaldoPrenda = Trim(dtLibrosSuperiores2.Rows(y).Item(5).ToString)
                                                SaldoGarantia = Trim(dtLibrosSuperiores2.Rows(y).Item(6).ToString)
                                                SaldoDepPla = Trim(dtLibrosSuperiores2.Rows(y).Item(7).ToString)
                                                SaldoRetencion = Trim(dtLibrosSuperiores2.Rows(y).Item(8).ToString)

                                                Dim ImporteOculto As Double = 0
                                                Dim CargoOculto As String = ""
                                                If dgvLibroDet.Rows.Count > 0 Then
                                                    ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                                    CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                                                End If

                                                'Dim SaldoPrenda As Double = 0
                                                'Dim SaldoGarantia As Double = 0
                                                'Dim SaldoDepPla As Double = 0
                                                'Dim SaldoRetencion As Double = 0

                                                If CargoOculto <> cboTipoCargo.SelectedValue Then
                                                    If CargoOculto = "00002" Then
                                                        SaldoPrenda = SaldoPrenda - ImporteOculto
                                                    ElseIf CargoOculto = "00003" Then
                                                        SaldoGarantia = SaldoGarantia - ImporteOculto
                                                    ElseIf CargoOculto = "00004" Then
                                                        SaldoDepPla = SaldoDepPla - ImporteOculto
                                                    ElseIf CargoOculto = "00005" Then
                                                        SaldoRetencion = SaldoRetencion - ImporteOculto
                                                    End If

                                                    If cboTipoCargo.SelectedValue = "00002" Then
                                                        SaldoPrenda = SaldoPrenda + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                        SaldoGarantia = SaldoGarantia + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                        SaldoDepPla = SaldoDepPla + Convert.ToDouble(txtImporte.Text)
                                                    ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                        SaldoRetencion = SaldoRetencion + Convert.ToDouble(txtImporte.Text)
                                                    End If

                                                ElseIf CargoOculto = cboTipoCargo.SelectedValue Then
                                                    Dim importetex = Format(txtImporte.Text, "0.00")
                                                    If txtImporte.Text = ImporteOculto Then
                                                        If cboTipoCargo.SelectedValue = "00002" Then
                                                            SaldoPrenda = SaldoPrenda
                                                        ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                            SaldoGarantia = SaldoGarantia
                                                        ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                            SaldoDepPla = SaldoDepPla
                                                        ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                            SaldoRetencion = SaldoRetencion
                                                        End If
                                                    Else
                                                        If cboTipoCargo.SelectedValue = "00002" Then
                                                            SaldoPrenda = (SaldoPrenda + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                            SaldoGarantia = (SaldoGarantia + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                            SaldoDepPla = (SaldoDepPla + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                            SaldoRetencion = (SaldoRetencion + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                        End If
                                                    End If
                                                End If

                                                '''''''''''''''''''''''''''''''''''''''''
                                                'graba saldos de uno de los libro proximos
                                                iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today(), SaldoPrenda, SaldoGarantia, SaldoDepPla, SaldoRetencion)

                                            Next
                                        End If
                                    End If 'If sIdCuenta <> CuentaEncargada Then
                                End If 'If CuentaEncargada <> "" Then

                            Else
                                If CuentaEncargada <> "" Then
                                    Dim dtLibrosSuperiores2 As DataTable
                                    dtLibrosSuperiores2 = New DataTable
                                    Dim A�o2 As String = dtpFecha.Value.Year
                                    Dim Mes2 As String = Format(dtpFecha.Value.Month, "00")
                                    dtLibrosSuperiores2 = eLibroBancos.TraerLibrosSuperiores(Mes2, A�o2, CuentaEncargada, gEmpresa, dtpFecha.Value) 'trae los proximos libros
                                    If dtLibrosSuperiores2.Rows.Count > 0 Then
                                        For y As Integer = 0 To dtLibrosSuperiores2.Rows.Count - 1
                                            Dim iResultadoActLibroSup As Int16 = 0
                                            Dim CodigoLibroBD As String = ""
                                            Dim SaldoInicialBD As Double ' = 0
                                            Dim SaldoFinalBD As Double '= 0
                                            CodigoLibroBD = Trim(dtLibrosSuperiores2.Rows(y).Item(0).ToString)
                                            SaldoInicialBD = Trim(dtLibrosSuperiores2.Rows(y).Item(1).ToString)
                                            SaldoFinalBD = Trim(dtLibrosSuperiores2.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                            Cobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(9).ToString)
                                            NoCobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(10).ToString)
                                            '''''''''''''''''''''''''''''''''''''''''
                                            Dim SaldoPrenda As Double
                                            Dim SaldoGarantia As Double
                                            Dim SaldoDepPla As Double
                                            Dim SaldoRetencion As Double
                                            SaldoPrenda = Trim(dtLibrosSuperiores2.Rows(y).Item(5).ToString)
                                            SaldoGarantia = Trim(dtLibrosSuperiores2.Rows(y).Item(6).ToString)
                                            SaldoDepPla = Trim(dtLibrosSuperiores2.Rows(y).Item(7).ToString)
                                            SaldoRetencion = Trim(dtLibrosSuperiores2.Rows(y).Item(8).ToString)

                                            Dim ImporteOculto As Double = 0
                                            Dim CargoOculto As String = ""
                                            If dgvLibroDet.Rows.Count > 0 Then
                                                ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                                CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                                            End If

                                            If CargoOculto <> cboTipoCargo.SelectedValue Then
                                                If CargoOculto = "00002" Then
                                                    SaldoPrenda = SaldoPrenda - ImporteOculto
                                                ElseIf CargoOculto = "00003" Then
                                                    SaldoGarantia = SaldoGarantia - ImporteOculto
                                                ElseIf CargoOculto = "00004" Then
                                                    SaldoDepPla = SaldoDepPla - ImporteOculto
                                                ElseIf CargoOculto = "00005" Then
                                                    SaldoRetencion = SaldoRetencion - ImporteOculto
                                                End If

                                                If cboTipoCargo.SelectedValue = "00002" Then
                                                    SaldoPrenda = SaldoPrenda + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                    SaldoGarantia = SaldoGarantia + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                    SaldoDepPla = SaldoDepPla + Convert.ToDouble(txtImporte.Text)
                                                ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                    SaldoRetencion = SaldoRetencion + Convert.ToDouble(txtImporte.Text)
                                                End If

                                            ElseIf CargoOculto = cboTipoCargo.SelectedValue Then
                                                Dim importetex = Format(txtImporte.Text, "0.00")
                                                If txtImporte.Text = ImporteOculto Then
                                                    If cboTipoCargo.SelectedValue = "00002" Then
                                                        SaldoPrenda = SaldoPrenda
                                                    ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                        SaldoGarantia = SaldoGarantia
                                                    ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                        SaldoDepPla = SaldoDepPla
                                                    ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                        SaldoRetencion = SaldoRetencion
                                                    End If
                                                Else
                                                    If cboTipoCargo.SelectedValue = "00002" Then
                                                        SaldoPrenda = (SaldoPrenda + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                    ElseIf cboTipoCargo.SelectedValue = "00003" Then
                                                        SaldoGarantia = (SaldoGarantia + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                    ElseIf cboTipoCargo.SelectedValue = "00004" Then
                                                        SaldoDepPla = (SaldoDepPla + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                    ElseIf cboTipoCargo.SelectedValue = "00005" Then
                                                        SaldoRetencion = (SaldoRetencion + Convert.ToDouble(txtImporte.Text)) - ImporteOculto
                                                    End If
                                                End If
                                            End If

                                            '''''''''''''''''''''''''''''''''''''''''
                                            'graba saldos de uno de los libro proximos
                                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today(), SaldoPrenda, SaldoGarantia, SaldoDepPla, SaldoRetencion)
                                            'saldo = SaldoFinalBD
                                        Next
                                    End If
                                End If 'If CuentaEncargada <> "" Then
                            End If
                            '----------------------------------------

                        End If
                        cboMes_SelectedIndexChanged(sender, e)
                        If iResultadoActSaldoLibro = 1 Then
                            ''MessageBox.Show("Se Actualizo con Exito!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            'btnNuevo.Enabled = True
                            'btnCancelar.Enabled = False
                            'btnGrabar.Enabled = False
                            'dtpFecha.Focus()
                            btnNuevo.Enabled = True
                            'btnCancelar.Enabled = False
                            btnGrabar.Enabled = False
                            btnNuevo_Click(sender, e)

                            '''''''''''''Posicionar''''''''''''''''''''''''
                            Posicionar()
                            '''''''''''''Posicionar''''''''''''''''''''''''

                            dtpFecha.Focus()
                        End If
                    End If
                End If
            Else 'campos no rellenos
                If cboBancos.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Banco", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboBancos.Focus()
                    Exit Sub
                End If
                If cboCuentas.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboCuentas.Focus()
                    Exit Sub
                End If
                If cboAno.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboAno.Focus()
                    Exit Sub
                End If
                If cboMes.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Mes", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cboMes.Focus()
                    Exit Sub
                End If
                If Len(txtConcepto.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese un Concepto", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtConcepto.Focus()
                    Exit Sub
                End If
                If Len(tipOpe.Trim) = 0 Then
                    MessageBox.Show("Seleccione un tipo de Operacion", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'cboOpcion.Focus()
                    Exit Sub
                End If
                If Len(txtImporte.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese un Importe", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtImporte.Focus()
                    Exit Sub
                End If
            End If 'campos no rellenos
            'End If 'pregunta Actualizar
        End If 'iopcion

    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        sCargarCargos = "ALGUNOS"
        LimpiarControlesDetalle()
        HabilitarControlesDetalle()

        btnGrabar.Enabled = True
        'btnCancelar.Enabled = True
        btnNuevo.Enabled = False

        iOpcion = 4

        rdbCargo.Checked = True
        rdbCargo_CheckedChanged(sender, e)
        chkFecha.Checked = False
        dtpFecha.Focus()
    End Sub

    Private Sub LimpiarControlesDetalle()
        rdbAbono.Checked = False
        rdbCargo.Checked = False
        dtpFecha.Value = Date.Now
        dtpFechaPago.Value = Date.Now
        cboTipoMov.DataSource = Nothing
        cboTipoMov.SelectedIndex = -1
        txtNroDoc.Clear()
        txtConcepto.Clear()
        txtDescripcion.Clear()
        txtImporte.Clear()
        txtTipoCambio.Clear()
        '''''''''''''''''''''''''''
        txtCodDet.Clear()
        txtMarca.Clear()
        txtMarca2.Clear()

        txtNroMov.Clear()
        dtpFIni.Value = Date.Now
        dtpFfin.Value = Date.Now
        cboTipoCargo.SelectedIndex = -1
        'cboCargoAnex.SelectedIndex = -1
        txtCliente.Clear()
        txtCtaCliente.Clear()
        txtObra.Clear()
    End Sub

    Private Sub DeshabilitarControlesDetalle()
        ' rdbAbono.Enabled = False
        ' rdbCargo.Enabled = False

        'dtpFecha.Enabled = False
        cboTipoMov.Enabled = False
        txtNroDoc.Enabled = False
        txtConcepto.Enabled = False
        txtDescripcion.Enabled = False
        txtImporte.Enabled = False
        txtTipoCambio.Enabled = False
        '''''''''''''''''''''
        txtNroMov.Enabled = False
        dtpFIni.Enabled = False
        dtpFfin.Enabled = False
        cboTipoCargo.Enabled = False
        'cboCargoAnex.Enabled = False
        txtCliente.Enabled = False
        txtCtaCliente.Enabled = False
        txtObra.Enabled = False
    End Sub

    Private Sub HabilitarControlesDetalle()

        rdbAbono.Enabled = True
        rdbCargo.Enabled = True

        dtpFecha.Enabled = True
        cboTipoMov.Enabled = True
        txtNroDoc.Enabled = True
        txtConcepto.Enabled = True
        txtDescripcion.Enabled = True
        'cboOpcion.Enabled = True

        rdbAbono.Enabled = True
        rdbCargo.Enabled = True
        cboTipoMov.Enabled = True

        txtImporte.Enabled = True
        '''''''''''''''''
        txtNroMov.Enabled = True
        dtpFIni.Enabled = True
        dtpFfin.Enabled = True
        cboTipoCargo.Enabled = True
        'cboCargoAnex.Enabled = True
        txtCliente.Enabled = True
        txtCtaCliente.Enabled = True
        txtObra.Enabled = True

    End Sub

    Public Sub Salir()

        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If

    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'LimpiarControlesDetalle()
        LimpiarControlesDetalle()
        DeshabilitarControlesDetalle()
        iOpcion = 0
        btnNuevo.Enabled = True
        btnGrabar.Enabled = False
        'btnCancelar.Enabled = False
        btnEliminar.Enabled = False
    End Sub

    Private Sub btnConciliacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConciliacion.Click

        If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboAno.SelectedIndex > -1 And cboMes.SelectedIndex > -1 Then
            Dim x As frmConciliacionBanc = frmConciliacionBanc.Instance
            x.Owner = Me
            x.CodCuenta = Trim(cboCuentas.SelectedValue)
            x.Mes = TraerNumeroMes(cboMes.Text)
            x.Anio = Trim(cboAno.Text)
            x.SaldoFinal = Trim(txtSaldo.Text)
            x.SaldoInicial = Trim(txtSaldoInicial.Text)
            x.ShowInTaskbar = False
            x.ShowDialog()
        Else
            If cboBancos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Banco", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboBancos.Focus()
                Exit Sub
            End If
            If cboCuentas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCuentas.Focus()
                Exit Sub
            End If

            If cboAno.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboAno.Focus()
                Exit Sub
            End If

            If cboMes.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Mes", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboMes.Focus()
                Exit Sub
            End If

        End If
    End Sub

    Private Sub txtImporte_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtImporte.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                btnGrabar_Click(sender, e)
        End Select
    End Sub


    Private Sub rdbAbono_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbAbono.CheckedChanged

        If rdbAbono.Checked = True Then
            eLibroBancos = New clsLibroBancos
            cboTipoMov.DataSource = eLibroBancos.fListarTipoMov(0)
            If eLibroBancos.iNroRegistros > 0 Then
                cboTipoMov.ValueMember = "IdRendicion"
                cboTipoMov.DisplayMember = "DescripcionRendicion"
                cboTipoMov.SelectedIndex = -1
            End If
            rdbAbono.Focus()
        End If

    End Sub

    Private Sub rdbCargo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbCargo.CheckedChanged
        If sCargarCargos = "TODOS" Then
            If rdbCargo.Checked = True Then
                eLibroBancos = New clsLibroBancos
                cboTipoMov.DataSource = eLibroBancos.fListarTipoMov(28)
                If eLibroBancos.iNroRegistros > 0 Then
                    cboTipoMov.ValueMember = "IdRendicion"
                    cboTipoMov.DisplayMember = "DescripcionRendicion"
                    cboTipoMov.SelectedIndex = -1
                End If
                rdbCargo.Focus()
            End If
        End If

        If sCargarCargos = "ALGUNOS" Then
            If rdbCargo.Checked = True Then
                eLibroBancos = New clsLibroBancos
                cboTipoMov.DataSource = eLibroBancos.fListarTipoMov(51)
                If eLibroBancos.iNroRegistros > 0 Then
                    cboTipoMov.ValueMember = "IdRendicion"
                    cboTipoMov.DisplayMember = "DescripcionRendicion"
                    cboTipoMov.SelectedIndex = -1
                End If
                rdbCargo.Focus()
            End If
        End If

    End Sub

    Private Sub dgvLibroDet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLibroDet.KeyDown
        Select Case e.KeyCode
            Case Keys.Delete
                If dgvLibroDet.Rows.Count > 0 Then
                    If chkConciliacionMes.Enabled = False And chkConciliacionMes.Checked = True Then
                        'MessageBox.Show("No se puede eliminar el detalle por que la Conciliaci�n de Mes est� Cerrado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        MessageBox.Show("No se puede eliminar el detalle por que la Conciliaci�n de Mes est� Cerrado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    btnEliminar_Click_1(sender, e)
                End If
        End Select
    End Sub

    Private Sub dgvLibroDet_SelectionChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLibroDet.SelectionChanged

        Label2.Text = ""
        If sVisualizarDatos = "SI" Then

            If dgvLibroDet.Rows.Count > 0 Then
                sCargarCargos = "TODOS"
                iOpcion = 5
                Try
                    dtpFecha.Value = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column1").Value)
                    dtpFechaPago.Value = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("FechaPago").Value)

                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("NroDocumento2").Value) = True Then
                        txtNroDoc.Text = ""
                    Else
                        txtNroDoc.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("NroDocumento2").Value)
                    End If

                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column5").Value) = True Then
                        txtConcepto.Text = ""
                    Else
                        txtConcepto.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column5").Value)
                    End If

                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column4").Value) = True Then
                        txtDescripcion.Text = ""
                    Else
                        txtDescripcion.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column4").Value)
                    End If

                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column11").Value) = True Then
                        'cboOpcion.SelectedIndex = -1

                        rdbAbono.Checked = False
                        rdbCargo.Checked = False
                        'cboTipoMov.Enabled = True
                        'rdbCargo_CheckedChanged(sender, e)
                    Else
                        Dim opcionBD As String = ""
                        opcionBD = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column11").Value)
                        If opcionBD = "D" Then
                            rdbAbono.Checked = True
                            'rdbCargo_cha
                            ' rdbCargo.Checked = True
                        ElseIf opcionBD = "H" Then
                            ' rdbAbono.Checked = True
                            rdbCargo.Checked = True
                            rdbCargo_CheckedChanged(sender, e)
                        End If

                        'cboOpcion.SelectedValue = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column11").Value)
                    End If

                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = True Then
                        cboTipoMov.SelectedIndex = -1
                    Else
                        cboTipoMov.SelectedValue = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value)
                    End If

                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column12").Value) = True Then
                        txtImporte.Text = "0.00"
                    Else
                        txtImporte.Text = Format(Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column12").Value)), "#,##0.00")
                    End If

                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column2").Value) = True Then
                        txtCodDet.Text = ""
                    Else
                        txtCodDet.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column2").Value)
                    End If
                    '''''''''''''''''''''''''''''''Anexos
                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column13").Value) = True Then
                        txtNroMov.Text = ""
                    Else
                        txtNroMov.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column13").Value)
                    End If
                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column14").Value) = True Then
                        dtpFIni.Value = Now.Date
                    Else
                        Dim a As String = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column14").Value)
                        If a = "" Then
                            dtpFIni.Value = Now.Date
                        Else
                            dtpFIni.Value = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column14").Value)
                        End If
                    End If
                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column15").Value) = True Then
                        dtpFfin.Value = Now.Date
                    Else
                        'dtpFfin.Value = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column15").Value)
                        Dim b As String = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column15").Value)
                        If b = "" Then
                            dtpFfin.Value = Now.Date
                        Else
                            dtpFfin.Value = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column15").Value)
                        End If
                    End If
                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column19").Value) = True Then
                        txtCliente.Clear()
                    Else
                        txtCliente.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column19").Value)
                    End If
                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column20").Value) = True Then
                        txtCtaCliente.Clear()
                    Else
                        txtCtaCliente.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column20").Value)
                    End If
                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column21").Value) = True Then
                        txtObra.Clear()
                    Else
                        txtObra.Text = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column21").Value)
                    End If
                    If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column22").Value) = True Then
                        cboTipoCargo.SelectedIndex = -1
                    Else
                        cboTipoCargo.SelectedValue = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column22").Value)
                    End If
                    '''''''''''''''''''''''''''''''Fin Anexos

                    btnGrabar.Enabled = True
                    btnNuevo.Enabled = True
                    'btnCancelar.Enabled = True
                    btnEliminar.Enabled = True
                    'Calcular()
                    'LimpiarControlesDetalle()
                    HabilitarControlesDetalle()
                    dtpFecha.Enabled = True

                    If cboTipoMov.SelectedValue = "00002" Or cboTipoMov.SelectedValue = "00005" Or cboTipoMov.SelectedValue = "00007" Then
                        'btnGrabar.Enabled = False
                        'btnNuevo.Enabled = True
                        'btnCancelar.Enabled = False
                        'btnEliminar.Enabled = False
                        'btnBuscar.Enabled = True
                        'DeshabilitarControlesDetalle()
                        'dtpFecha.Enabled = False
                    End If
                    If cboTipoMov.SelectedValue = "00011" Then
                        cboTipoMov.Enabled = False
                    End If


                    If Len(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Identificarx").Value)) > 0 Then
                        If Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Identificarx").Value) = "1" Then
                            chkFecha.Checked = True
                        ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Identificarx").Value) = "0" Then
                            chkFecha.Checked = False
                        End If
                    End If
                Catch ex As Exception
                    Exit Sub
                End Try

                Label2.Text = "Registro " & Convert.ToString(dgvLibroDet.CurrentRow.Index + 1) & " de " & Convert.ToString(dgvLibroDet.Rows.Count)

                If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = True Then
                    btnGrabar.Enabled = False
                    btnEliminar.Enabled = False
                ElseIf Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = False Then
                    btnGrabar.Enabled = True
                    btnEliminar.Enabled = True
                End If
            End If 'If dgvLibroDet.Rows.Count > 0 Then

        End If 'If sVisualizarDatos = "SI" Then
        dgvLibroDet.Focus()
        BloquearControles()
    End Sub


    Private Sub dgvLibroDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLibroDet.Click
        Calcular()
        'dgvLibroDet_CellContentClick(sender, e)
        sVisualizarDatos = "SI"
    End Sub


    Private Sub dgvLibroDet_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLibroDet.CellEndEdit
        Calcular()
    End Sub

    Private Sub dgvLibroDet_CellContentClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLibroDet.CellContentClick

        If chkConciliacionMes.Enabled = False And chkConciliacionMes.Checked = True Then
            Exit Sub
        End If
        'Hector 
        'If dgvLibroDet.Rows.Count = 0 Then Exit Sub
        '''''''''''''''''''''hector

        Dim columna As Integer = 0
        If dgvLibroDet.Rows.Count > 0 Then
            columna = dgvLibroDet.CurrentCell.ColumnIndex
        End If

        If dgvLibroDet.Rows.Count > 0 Then
            dgvLibroDet.CurrentCell = dgvLibroDet(0, dgvLibroDet.CurrentRow.Index)
            'Calcular()
            dgvLibroDet.CurrentCell = dgvLibroDet(columna, dgvLibroDet.CurrentRow.Index)
            'Calcular()
        End If
        Calcular()
        If columna = 27 Then
            Dim consiVoF As String = ""
            If dgvLibroDet.Rows.Count > 0 Then
                Dim sCDetalle As String = ""
                Dim sCCabecera As String = ""
                Dim iActCheckDet As Integer = 0
                'If dgvLibroDet.Rows.Count > 0 Then
                sCDetalle = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value
                sCCabecera = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value
                'End If
                If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = True Then
                    consiVoF = 1
                ElseIf Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = False Then
                    consiVoF = 0
                End If

                Dim iResultado As Integer = 0
                eLibroBancos = New clsLibroBancos
                iResultado = eLibroBancos.fIdenti(53, gEmpresa, sCDetalle, sCCabecera, consiVoF, dtpFechaPago.Value)
                'dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(0).Style.BackColor = Color.Yellow
                dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("ConciliadoS").Value = consiVoF

            End If

        ElseIf columna = 7 Then

            Dim itfVoF As String = ""
            If dgvLibroDet.Rows.Count > 0 Then
                'verifico si este registro ya tiene ITF ,capturando su estado en la columna 13
                If Microsoft.VisualBasic.IsDBNull(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value) = True Then
                    itfVoF = ""
                Else
                    itfVoF = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(13).Value)
                End If
            End If
            'if el estado es 0
            If itfVoF = "0" Then
                If cboTipoMov.SelectedValue = "00002" Then
                    MessageBox.Show("Este Cheque ya Tiene ITF", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
                If dgvLibroDet.Rows.Count > 0 Then
                    'dgvLibroDet.CurrentCell = dgvLibroDet(0, dgvLibroDet.CurrentRow.Index)
                    dgvLibroDet.CurrentCell = dgvLibroDet(6, dgvLibroDet.CurrentRow.Index)
                    dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value = False
                    dgvLibroDet.CurrentCell = dgvLibroDet(7, dgvLibroDet.CurrentRow.Index)
                    Calcular()
                End If
                Exit Sub
            End If

            If cboTipoMov.SelectedIndex > -1 And txtMarca.Text = "0" And cboTipoMov.SelectedValue = "00002" Then 'And itfVoF = "1" Then
                Panel5.Visible = True

            ElseIf cboTipoMov.SelectedIndex > -1 And txtMarca.Text = "0" And cboTipoMov.SelectedValue <> "00002" Then 'And itfVoF = "1" Then
                If (MessageBox.Show("�Confirmar Cobro?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    eLibroBancos = New clsLibroBancos
                    Dim sCDetalle As String = ""
                    Dim sCCabecera As String = ""
                    Dim iActCheckDet As Integer = 0
                    If dgvLibroDet.Rows.Count > 0 Then
                        sCDetalle = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value
                        sCCabecera = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value
                    End If
                    Dim iResultadoActSaldoLibroActual As Int16 = 0
                    'DESACTIVA CHEK DEL CHEQUE EN BD
                    iActCheckDet = eLibroBancos.fActMarcaDet(gEmpresa, Trim(sCDetalle), Trim(sCCabecera), dtpFecha.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtDescripcion.Text), "", 0, 0, 0, 0)

                    ''''''''''''''''genera itf si es que no es un movimiento de cuenta a cuenta'''''
                    Dim dtDetLibroTieneCuentaDestino As DataTable
                    Dim CodigoLibroDestino As String = ""
                    Dim CodigoDetDestino As String = ""
                    'Dim FechaDetDestino As DateTime
                    'Dim sCodigoDetLibroItf As String = ""
                    'Dim iResultadoDetGrabaItf As Integer = 0
                    dtDetLibroTieneCuentaDestino = New DataTable
                    dtDetLibroTieneCuentaDestino = eLibroBancos.TraerSiTieneCuentaDestino(gEmpresa, Trim(sCCabecera), Trim(sCDetalle))
                    If dtDetLibroTieneCuentaDestino.Rows.Count = 1 Then
                        CodigoLibroDestino = Trim(dtDetLibroTieneCuentaDestino.Rows(0).Item("CodigoLibroDestino"))
                        CodigoDetDestino = Trim(dtDetLibroTieneCuentaDestino.Rows(0).Item("CodigoDetDestino"))
                        'FechaDetDestino = dtDetLibroTieneCuentaDestino.Rows(0).Item("FechaMovimiento")
                    End If
                    If Trim(CodigoLibroDestino) = "" And Trim(CodigoDetDestino) = "" Then

                    End If
                    ''''''''''''''''fin de genera itf si es que no es un movimiento de cuenta a cuenta'''''

                    If iActCheckDet = 1 Then
                        '--------------
                        Dim dtDatos As DataTable
                        dtDatos = New DataTable
                        dtDatos = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                        Dim dSaldoInicial As Double = 0
                        If dtDatos.Rows.Count > 0 Then
                            dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                        End If
                        '--------------
                        'ACTUALIZAR EL SALDO DEL LIBRO

                        Dim dtLibroDet As DataTable
                        dtLibroDet = New DataTable
                        Dim saldo As Double = 0
                        saldo = dSaldoInicial
                        Dim Cobrados As Double = 0
                        Dim NoCobrados As Double = 0
                        dtLibroDet = eLibroBancos.TraerLibroDet(37, sCCabecera, gEmpresa)
                        If dtLibroDet.Rows.Count > 0 Then
                            For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                    If z = 11 Then 'lee el importe
                                        If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                            saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                        ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                            saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                                NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            End If
                                            If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                                Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            End If
                                        End If
                                    End If
                                Next
                            Next
                            Dim dtDtFech As DataTable
                            dtDtFech = New DataTable
                            Dim Fecha As DateTime
                            Dim FechaActCabecera As DateTime
                            dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCCabecera)
                            If dtDtFech.Rows.Count > 0 Then
                                Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                            End If
                            FechaActCabecera = Fecha
                            iResultadoActSaldoLibroActual = eLibroBancos.fActSaldoLibro(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                        End If
                    End If
                    If iResultadoActSaldoLibroActual = 1 Then
                        cboMes_SelectedIndexChanged(sender, e) 'lista el nuevo detalle y calcula el saldo al final
                        btnNuevo.Enabled = True
                        'btnCancelar.Enabled = False
                        btnGrabar.Enabled = False
                    End If
                Else
                    If dgvLibroDet.Rows.Count > 0 Then
                        'Calcular()
                        If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
                            dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value = True
                            Dim columna2 As Integer = dgvLibroDet.CurrentCell.ColumnIndex
                            If dgvLibroDet.Rows.Count > 0 Then
                                dgvLibroDet.CurrentCell = dgvLibroDet(0, dgvLibroDet.CurrentRow.Index)
                                'Calcular()
                                dgvLibroDet.CurrentCell = dgvLibroDet(columna2, dgvLibroDet.CurrentRow.Index)
                                'Calcular()
                            End If

                        End If
                    End If
                End If
            ElseIf cboTipoMov.SelectedIndex > -1 And txtMarca.Text = "1" Then
                Panel3.Visible = False
                dtpFechaItf.Value = Now.Date().ToShortDateString
                txtGlosaItf.Clear()
                txtImporteItf.Clear()
            ElseIf cboTipoMov.SelectedIndex = -1 Then
                Panel3.Visible = False
                dtpFechaItf.Value = Now.Date().ToShortDateString
                txtGlosaItf.Clear()
                txtImporteItf.Clear()
            End If

        End If

    End Sub

    Sub Calcular()
        If dgvLibroDet.Rows.Count > 0 Then
            If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = True Then
                txtMarca.Text = "1"
            Else
                txtMarca.Text = "0"
            End If
        End If
    End Sub

    Private Sub btnCancelarItf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarItf.Click
        If dgvLibroDet.Rows.Count > 0 Then
            dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value = True
            Panel3.Visible = False
            dtpFechaItf.Value = Now.Date().ToShortDateString
            txtGlosaItf.Clear()
            txtImporteItf.Clear()
        End If

        Panel3.Visible = False
        Panel4.Visible = False
        Panel5.Visible = False

    End Sub

    Private Sub btnGrabarItf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarItf.Click
        Dim Cobrados As Double = 0
        Dim NoCobrados As Double = 0

        Dim iResultadoDetx As Int16 = 0
        Dim CountLibrosSuperiores As Integer = 0
        If dtpFechaItf.Value >= dtpFecha.Value Then
            If dgvLibroDet.Rows.Count > 0 Then
                If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
                    eLibroBancos = New clsLibroBancos
                    Dim sCDetalle As String = ""
                    Dim sCCabecera As String = ""
                    Dim iActCheckDet As Integer = 0
                    sCDetalle = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value
                    sCCabecera = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value
                    'DESACTIVA CHEK DEL CHEQUE EN BD
                    iActCheckDet = eLibroBancos.fActMarcaDet(gEmpresa, Trim(sCDetalle), Trim(sCCabecera), dtpFechaItf.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtDescripcion.Text), "", 0, 0, 0, 0)

                    Dim dtDataLibro As DataTable
                    Dim xMes As String = ""
                    Dim xAno As String = ""
                    dtDataLibro = New DataTable
                    dtDataLibro = eLibroBancos.TraerMesyAnoLibro(gEmpresa, sCCabecera)
                    If dtDataLibro.Rows.Count > 0 Then
                        xMes = Trim(dtDataLibro.Rows(0).Item("Mes"))
                        xAno = Trim(dtDataLibro.Rows(0).Item("A�o"))
                    End If

                    Dim pMes As String = Format(dtpFechaItf.Value.Month, "00")
                    Dim pA�o As String = dtpFechaItf.Value.Year

                    If xMes = pMes And xAno = pA�o Then
                        'GRABA REGISTRO DE ITF EN BD, SI EL ITF PERTENECE AL MES ACTUAL
                        Dim sCodigoDetx As String = ""
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigoDet(gEmpresa, sCCabecera)
                        sCodigoDetx = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro actual
                        sCodigoDetPosicion = Trim(sCodigoDetx)
                        'sCodigoDetPosicion = Trim(sCodigoDetx)
                        iResultadoDetx = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDetx, sCCabecera, dtpFechaItf.Value, "00011", "", Trim(txtGlosaItf.Text.Trim), "", "H", Convert.ToDouble(txtImporteItf.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", Trim(sCCabecera), Trim(sCDetalle))
                    End If

                    If iActCheckDet = 1 Then
                        '--------------
                        Dim dtDatos As DataTable
                        dtDatos = New DataTable
                        dtDatos = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                        Dim dSaldoInicial As Double = 0
                        If dtDatos.Rows.Count > 0 Then
                            dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                            Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                            NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                        End If
                        '--------------
                        'ACTUALIZAR EL SALDO DEL LIBRO
                        Dim iResultadoActSaldoLibroActual As Int16 = 0
                        Dim dtLibroDet As DataTable
                        dtLibroDet = New DataTable
                        Dim saldo As Double = 0
                        saldo = dSaldoInicial
                        'Dim Cobrados As Double = 0
                        'Dim NoCobrados As Double = 0
                        dtLibroDet = eLibroBancos.TraerLibroDet(37, sCCabecera, gEmpresa)
                        If dtLibroDet.Rows.Count > 0 Then
                            For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                    If z = 11 Then 'lee el importe
                                        If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                            saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                        ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                            saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                            '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                        End If
                                    End If
                                Next
                            Next
                            Dim dtDtFech As DataTable
                            dtDtFech = New DataTable
                            Dim Fecha As DateTime
                            Dim FechaActCabecera As DateTime
                            dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCCabecera)
                            If dtDtFech.Rows.Count > 0 Then
                                Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                            End If
                            'If dtpFechaItf.Value > Fecha Then
                            'FechaActCabecera = dtpFechaItf.Value
                            'Else
                            FechaActCabecera = Fecha
                            'End If

                            Cobrados = Cobrados + Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                            'ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "1" Then
                            NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))


                            iResultadoActSaldoLibroActual = eLibroBancos.fActSaldoLibro(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                            '----------------------------------------
                            'saber si existe libros proximos al mes actual de la cuenta
                            Dim dtDatosLibro As DataTable
                            dtDatosLibro = New DataTable
                            dtDatosLibro = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                            Dim sIdCuenta As String = ""
                            If dtDatosLibro.Rows.Count > 0 Then
                                sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                            End If
                            Dim dtLibrosSuperiores As DataTable
                            dtLibrosSuperiores = New DataTable
                            'Dim A�o As String = dtpFechaItf.Value.Year
                            'Dim Mes As String = Format(dtpFechaItf.Value.Month, "00")
                            dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(xMes, xAno, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                            CountLibrosSuperiores = dtLibrosSuperiores.Rows.Count
                            If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                    Dim iResultadoActLibroSup As Int16 = 0
                                    Dim CodigoLibroBD As String = ""
                                    Dim SaldoInicialBD As Double ' = 0
                                    Dim SaldoFinalBD As Double '= 0
                                    CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    SaldoInicialBD = Format(saldo, "0.00")
                                    SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtImporteItf.Text)
                                    'graba saldos de uno de los libro proximos
                                    '''''''''''''''''''''''''''''''''''''''''
                                    Dim SaldoPrendaS As Double
                                    Dim SaldoGarantiaS As Double
                                    Dim SaldoDepPlazS As Double
                                    Dim SaldoRetencionS As Double
                                    SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                    SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                    SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                    SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                    '''''''''''''''''''''''''''''''''''''''''
                                    iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                    saldo = SaldoFinalBD
                                Next
                            End If
                            '----------------------------------------
                        End If
                    End If

                    If iResultadoDetx = 0 Then 'SI NO SE GRABO EL ITF ENTONCES ' porque la fecha de itf no es del mes actual
                        'sCDetalle
                        sCodigoDetPosicion = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value)
                        '**************LIBRO BANCOS CUENTA ORIGEN**************************************************************
                        '******************************************************************************************************
                        Dim sCodigoLibro As String = ""
                        Dim sCodigoDet As String = ""
                        eLibroBancos = New clsLibroBancos
                        Dim dtDatos As DataTable
                        dtDatos = New DataTable
                        Dim IdLibro As String = ""
                        Dim A�o As String = dtpFechaItf.Value.Year
                        Dim Mes As String = Format(dtpFechaItf.Value.Month, "00")
                        dtDatos = eLibroBancos.TraerIdLibro(A�o, Mes, Trim(cboCuentas.SelectedValue), gEmpresa)
                        If dtDatos.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO
                            Dim iResultadoDet As Int16 = 0
                            Dim iResultadoActSaldoLibro As Int16 = 0
                            sCodigoLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                            Dim dSaldoInicial As Double = 0
                            Dim dSaldoFinal As Double = 0
                            Dim dCheCobradosNO As Double = 0
                            Dim dCheCobradosSI As Double = 0
                            dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                            dSaldoFinal = Trim(dtDatos.Rows(0).Item("Saldo")) 'Saldo Final
                            dCheCobradosNO = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                            dCheCobradosSI = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                            'Agregar un nuevo detalle de libro Actual
                            eLibroBancos = New clsLibroBancos
                            eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro.Trim)
                            sCodigoDet = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro creado
                            Dim MarcaCheqNoCobrado As Integer = 0
                            'Dim Cobrados As Double = 0
                            'Dim NoCobrados As Double = 0
                            MarcaCheqNoCobrado = 0
                            Cobrados = dCheCobradosSI
                            NoCobrados = dCheCobradosNO

                            iResultadoDet = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpFechaItf.Value, "00011", "", Trim(txtGlosaItf.Text.Trim), "", "H", Convert.ToDouble(txtImporteItf.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", Trim(sCCabecera), Trim(sCDetalle))
                            Dim saldo As Double = 0
                            If CountLibrosSuperiores = 0 Then
                                saldo = dSaldoFinal - Convert.ToDouble(txtImporteItf.Text)
                            Else
                                saldo = dSaldoFinal
                            End If
                            'saldo = dSaldoFinal - Convert.ToDouble(txtImporteItf.Text)
                            'actualiza el nuevo libro
                            If iResultadoDet = 1 Then
                                Dim iResultadoActDetalleLibro As Int16 = 0
                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                                Dim dtDtFech As DataTable
                                dtDtFech = New DataTable
                                Dim Fecha As DateTime
                                Dim FechaActCabecera As DateTime
                                dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCodigoLibro)
                                If dtDtFech.Rows.Count > 0 Then
                                    Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                                End If
                                If dtpFechaItf.Value > Fecha Then
                                    FechaActCabecera = dtpFechaItf.Value
                                Else
                                    FechaActCabecera = Fecha
                                End If
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                                '----------------------------------------
                                'saber si existe libros proximos al mes actual de la cuenta
                                Dim dtDatosLibro As DataTable
                                dtDatosLibro = New DataTable
                                dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigoLibro, gEmpresa)
                                Dim sIdCuenta As String = ""
                                If dtDatosLibro.Rows.Count > 0 Then
                                    sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                                End If
                                Dim dtLibrosSuperiores As DataTable
                                dtLibrosSuperiores = New DataTable
                                dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                                If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                    For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                        Dim iResultadoActLibroSup As Int16 = 0
                                        Dim CodigoLibroBD As String = ""
                                        Dim SaldoInicialBD As Double ' = 0
                                        Dim SaldoFinalBD As Double '= 0
                                        CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                        SaldoInicialBD = Format(saldo, "0.00")
                                        SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtImporteItf.Text)
                                        'graba saldos de uno de los libro proximos
                                        '''''''''''''''''''''''''''''''''''''''''
                                        Dim SaldoPrendaS As Double
                                        Dim SaldoGarantiaS As Double
                                        Dim SaldoDepPlazS As Double
                                        Dim SaldoRetencionS As Double
                                        SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                        SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                        SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                        SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                        ''''''''''''''''''''''''''''''''''''''''''
                                        iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                        saldo = SaldoFinalBD
                                    Next
                                End If
                                '----------------------------------------
                            End If
                        ElseIf dtDatos.Rows.Count = 0 Then ' NO EXISTE LIBRO DEL MES DE PAGO
                            Dim MesAnt As String = ""
                            Dim Anio As String = ""
                            MesAnt = Format(Mes - 1, "00") 'resto el mes actual, para usar el mes anterior
                            Anio = Trim(dtpFechaItf.Value.Year)
                            If Trim(Mes) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y a�o el ACTUAL -1
                                MesAnt = "12"
                                Anio = Trim(A�o) - 1
                            End If
                            Dim dtLibroAnterior As DataTable
                            dtLibroAnterior = New DataTable
                            dtLibroAnterior = eLibroBancos.TraerIdLibro(Anio, MesAnt, Trim(cboCuentas.SelectedValue), gEmpresa)
                            Dim iResultado As Int16 = 0
                            Dim iResultadoDet2 As Int16 = 0
                            Dim SaldoAnterior As Double = 0
                            If dtLibroAnterior.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigo(gEmpresa)
                                sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                                'graba la cabecera de libro
                                'saldo inicial = saldo final del libro anterior
                                SaldoAnterior = Trim(dtLibroAnterior.Rows(0).Item("Saldo"))
                                Dim SaldoPrenda As Double = 0
                                Dim SaldoGarantia As Double = 0
                                Dim SaldoDepPlaz As Double = 0
                                Dim SaldoRetencion As Double = 0
                                Dim SaldoIniExtracto As Double = 0
                                SaldoPrenda = Trim(dtLibroAnterior.Rows(0).Item("SaldoPrenda"))
                                SaldoGarantia = Trim(dtLibroAnterior.Rows(0).Item("SaldoGarantia"))
                                SaldoDepPlaz = Trim(dtLibroAnterior.Rows(0).Item("SaldoDepPlaz"))
                                SaldoRetencion = Trim(dtLibroAnterior.Rows(0).Item("SaldoRetencion"))
                                SaldoIniExtracto = Trim(dtLibroAnterior.Rows(0).Item("SaldoFinExtracto"))

                                'SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion
                                iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtImporteItf.Text), dtpFechaItf.Value, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                            ElseIf dtLibroAnterior.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigo(gEmpresa)
                                sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                                'graba la cabecera de libro
                                'saldo anterior =0
                                SaldoAnterior = 0
                                iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtImporteItf.Text), dtpFechaItf.Value, 0, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                            End If

                            If iResultado = 2627 Then
                                MessageBox.Show("No se Grabo el Registro Intente grabar otra vez", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Exit Sub
                            End If

                            If iResultado > 0 And iResultado <> 2627 Then
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro)
                                sCodigoDet = eLibroBancos.sCodFuturoDet

                                Dim MarcaCheqNoCobrado As Integer = 0
                                Cobrados = 0
                                NoCobrados = 0

                                iResultadoDet2 = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpFechaItf.Value, "00011", "", Trim(txtGlosaItf.Text.Trim), "", "H", Convert.ToDouble(txtImporteItf.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", Trim(sCCabecera), Trim(sCDetalle))
                                Dim saldo As Double = 0
                                saldo = SaldoAnterior - Convert.ToDouble(txtImporteItf.Text)
                                'actualiza el nuevo libro
                                If iResultadoDet2 = 1 Then
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                                    Dim iResultadoActSaldoLibro As Int16 = 0
                                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(SaldoAnterior, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), dtpFechaItf.Value) 'graba el nuevo saldo del libro
                                End If
                            End If
                        End If
                        '************** FIN LIBRO BANCOS CUENTA ORIGEN ********************************************************
                        '******************************************************************************************************
                    End If


                    'Panel3.Visible = False
                    dtpFechaItf.Value = Now.Date().ToShortDateString
                    txtGlosaItf.Clear()
                    txtImporteItf.Clear()
                    cboMes_SelectedIndexChanged(sender, e)
                    Panel3.Visible = False

                    '''''''''''''Posicionar''''''''''''''''''''''''
                    Posicionar()
                    '''''''''''''Posicionar''''''''''''''''''''''''

                    'MessageBox.Show("Se Agrego el ITF con Exito!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'Panel3.Visible = False
                End If ' If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
            End If 'If dgvLibroDet.Rows.Count > 0 Then
            Panel3.Visible = False
            Panel4.Visible = False
            Panel5.Visible = False
        Else
            MessageBox.Show("La Fecha del ITF debe ser Mayor o Igual a la del Cheque", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaItf.Focus()
        End If
    End Sub

    Private Sub cboAno_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAno.SelectedIndexChanged
        Limpiar()
    End Sub

    Private Sub btnItfCant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnItfCant.Click
        Dim SumaCheques As Double = 0
        Dim GlosaCheques As String = ""
        If dgvLibroDet.Rows.Count > 0 Then
            For z As Integer = 0 To dgvLibroDet.Rows.Count - 1
                If Convert.ToBoolean(dgvLibroDet.Rows(z).Cells("ITF").Value) = True Then
                    SumaCheques = SumaCheques + dgvLibroDet.Rows(z).Cells("Column10").Value
                    GlosaCheques = GlosaCheques & " " & dgvLibroDet.Rows(z).Cells("NroDocumento2").Value
                End If
            Next

            

            dtpITFCant.Value = Now.Date().ToShortDateString

            Dim dtDoble As DataTable
            dtDoble = New DataTable
            eITF = New clsITF
            dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpITFCant.Value)
            If dtDoble.Rows.Count > 0 Then
                PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
            End If

            txtITF2.Text = Format(PorcentajeITF, "#,##0.0000")
            txtGlosaITFCant.Text = "ITF :" & GlosaCheques
            txtSumaChe.Text = Format(SumaCheques, "#,##0.00")

            Dim ItfMontoSumado As Double = 0

            Dim MontoParaItf As Double = 0
            MontoParaItf = ReturnMontoParaCalcItf(SumaCheques)
            ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100

            'ItfMontoSumado = (SumaCheques * PorcentajeITF) / 100
            txtImporITF.Text = Format(ItfMontoSumado, "#,##0.00")
        End If
        If SumaCheques = 0 Then
            MessageBox.Show("Seleccione Cheques para Generar el ITF!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Panel4.Visible = True
        dtpITFCant.Focus()
    End Sub

    Private Sub btnCancelarITFCant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarITFCant.Click
        If dgvLibroDet.Rows.Count > 0 Then

            For z As Integer = 0 To dgvLibroDet.Rows.Count - 1
                If Convert.ToBoolean(dgvLibroDet.Rows(z).Cells("ITF").Value) = True Then
                    dgvLibroDet.Rows(z).Cells("ITF").Value = False
                End If
            Next

            Panel4.Visible = False
            dtpITFCant.Value = Now.Date().ToShortDateString
            txtSumaChe.Clear()
            txtImporITF.Clear()
            txtGlosaITFCant.Clear()
        End If

        Panel3.Visible = False
        Panel4.Visible = False
        Panel5.Visible = False

    End Sub


    Private Sub btnGrabarITFCant_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarITFCant.Click
        Dim Cobrados As Double = 0
        Dim NoCobrados As Double = 0

        Dim iActCheckDet As Integer = 0
        Dim iResultadoDetx As Integer = 0
        Dim iResultadoActSaldoLibroActual As Int16 = 0

        Dim sCCabecera As String = ""
        Dim CountLibrosSuperiores As Integer = 0
        If dgvLibroDet.Rows.Count > 0 Then
            For z As Integer = 0 To dgvLibroDet.Rows.Count - 1
                If Convert.ToBoolean(dgvLibroDet.Rows(z).Cells("ITF").Value) = True Then
                    Dim sCDetalle As String = ""
                    sCDetalle = dgvLibroDet.Rows(z).Cells(8).Value
                    sCCabecera = dgvLibroDet.Rows(z).Cells(12).Value
                    'DESACTIVA LOS CHEQUES SELECCIONADOS EN BD
                    iActCheckDet = eLibroBancos.fActMarcaDet(gEmpresa, Trim(sCDetalle), Trim(sCCabecera), dtpFecha.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtDescripcion.Text), "", 0, 0, 0, 0)
                End If
            Next

            'Dim dtDataLibro As DataTable
            'Dim xMes As String = ""
            'Dim xAno As String = ""
            'dtDataLibro = New DataTable
            'dtDataLibro = eLibroBancos.TraerMesyAnoLibro(gEmpresa, sCCabecera)
            'If dtDataLibro.Rows.Count > 0 Then
            '    xMes = Trim(dtDataLibro.Rows(0).Item("Mes"))
            '    xAno = Trim(dtDataLibro.Rows(0).Item("A�o"))
            'End If
            'Dim pMes As String = Format(dtpITFCant.Value.Month, "00")
            'Dim pA�o As String = dtpITFCant.Value.Year
            'If xMes = pMes And xAno = pA�o Then
            '    'GRABA REGISTRO DE ITF EN BD, SI EL ITF PERTENECE AL MES ACTUAL
            '    Dim sCodigoDetx As String = ""
            '    eLibroBancos = New clsLibroBancos
            '    eLibroBancos.fCodigoDet(gEmpresa, sCCabecera)
            '    sCodigoDetx = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro actual
            '    'sCodigoDetPosicion = Trim(sCodigoDetx)
            '    iResultadoDetx = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDetx, sCCabecera, dtpITFCant.Value, "00011", "", Trim(txtGlosaItf.Text.Trim), "", "H", Convert.ToDouble(txtImporITF.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
            'End If

            If iActCheckDet = 1 Then

                Dim dtDataLibro As DataTable
                Dim xMes As String = ""
                Dim xAno As String = ""
                dtDataLibro = New DataTable
                dtDataLibro = eLibroBancos.TraerMesyAnoLibro(gEmpresa, sCCabecera)
                If dtDataLibro.Rows.Count > 0 Then
                    xMes = Trim(dtDataLibro.Rows(0).Item("Mes"))
                    xAno = Trim(dtDataLibro.Rows(0).Item("A�o"))
                End If
                Dim pMes As String = Format(dtpITFCant.Value.Month, "00")
                Dim pA�o As String = dtpITFCant.Value.Year
                If xMes = pMes And xAno = pA�o Then
                    'If iActCheckDet = 1 Then
                    '--------------
                    Dim dtDatos As DataTable
                    dtDatos = New DataTable
                    dtDatos = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                    Dim dSaldoInicial As Double = 0
                    If dtDatos.Rows.Count > 0 Then
                        dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                        Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                        NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                    End If
                    '--------------

                    Dim sCodigoDetx As String = ""
                    eLibroBancos = New clsLibroBancos
                    eLibroBancos.fCodigoDet(gEmpresa, sCCabecera)
                    sCodigoDetx = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro actual
                    sCodigoDetPosicion = Trim(sCodigoDetx)
                    iResultadoDetx = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDetx, sCCabecera, dtpITFCant.Value, "00011", "", Trim(txtGlosaITFCant.Text.Trim), "", "H", Convert.ToDouble(txtImporITF.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                    'iResultadoDetx
                    'ACTUALIZAR EL SALDO DEL LIBRO
                    Dim dtLibroDet As DataTable
                    dtLibroDet = New DataTable
                    Dim saldo As Double = 0
                    saldo = dSaldoInicial
                    'Dim Cobrados As Double = 0
                    'Dim NoCobrados As Double = 0
                    dtLibroDet = eLibroBancos.TraerLibroDet(37, sCCabecera, gEmpresa)
                    If dtLibroDet.Rows.Count > 0 Then
                        For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                            For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                If z = 11 Then 'lee el importe
                                    If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                        saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                        Dim iResultadoActDetalleLibro As Int16 = 0
                                        iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                    ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                        saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                        Dim iResultadoActDetalleLibro As Int16 = 0
                                        iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                        'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                        '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                        'End If
                                        'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                        'End If
                                    End If
                                End If
                            Next
                        Next
                        Dim dtDtFech As DataTable
                        dtDtFech = New DataTable
                        Dim Fecha As DateTime
                        Dim FechaActCabecera As DateTime
                        dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCCabecera)
                        If dtDtFech.Rows.Count > 0 Then
                            Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                        End If
                        If dtpITFCant.Value > Fecha Then
                            FechaActCabecera = dtpITFCant.Value
                        Else
                            FechaActCabecera = Fecha
                        End If

                        Cobrados = Cobrados + Convert.ToDouble(txtSumaChe.Text)
                        'ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "1" Then
                        NoCobrados = NoCobrados - Convert.ToDouble(txtSumaChe.Text)
                        'End If

                        iResultadoActSaldoLibroActual = eLibroBancos.fActSaldoLibro(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                        '----------------------------------------
                        'saber si existe libros proximos al mes actual de la cuenta
                        Dim dtDatosLibro As DataTable
                        dtDatosLibro = New DataTable
                        dtDatosLibro = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                        Dim sIdCuenta As String = ""
                        If dtDatosLibro.Rows.Count > 0 Then
                            sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                        End If
                        Dim dtLibrosSuperiores As DataTable
                        dtLibrosSuperiores = New DataTable
                        Dim A�o As String = dtpFechaItf.Value.Year
                        Dim Mes As String = Format(dtpFechaItf.Value.Month, "00")
                        dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                        CountLibrosSuperiores = dtLibrosSuperiores.Rows.Count
                        If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                            For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                Dim iResultadoActLibroSup As Int16 = 0
                                Dim CodigoLibroBD As String = ""
                                Dim SaldoInicialBD As Double ' = 0
                                Dim SaldoFinalBD As Double '= 0
                                CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                SaldoInicialBD = Format(saldo, "0.00")
                                SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtImporITF.Text)
                                'graba saldos de uno de los libro proximos
                                '''''''''''''''''''''''''''''''''''''''''
                                Dim SaldoPrendaS As Double
                                Dim SaldoGarantiaS As Double
                                Dim SaldoDepPlazS As Double
                                Dim SaldoRetencionS As Double
                                SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                '''''''''''''''''''''''''''''''''''''''''
                                iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                saldo = SaldoFinalBD
                            Next
                        End If

                    End If

                    If iResultadoDetx = 0 Then 'SI NO SE GRABO EL ITF ENTONCES ' porque la fecha de itf no es del mes actual
                        'sCDetalle
                        sCodigoDetPosicion = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value)
                        '**************LIBRO BANCOS CUENTA ORIGEN**************************************************************
                        '******************************************************************************************************
                        Dim sCodigoLibro As String = ""
                        Dim sCodigoDet As String = ""
                        eLibroBancos = New clsLibroBancos
                        'Dim dtDatos As DataTable
                        dtDatos = New DataTable
                        Dim IdLibro As String = ""
                        Dim A�o As String = dtpITFCant.Value.Year
                        Dim Mes As String = Format(dtpITFCant.Value.Month, "00")
                        dtDatos = eLibroBancos.TraerIdLibro(A�o, Mes, Trim(cboCuentas.SelectedValue), gEmpresa)
                        If dtDatos.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO
                            Dim iResultadoDet As Int16 = 0
                            Dim iResultadoActSaldoLibro As Int16 = 0
                            sCodigoLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                            'Dim dSaldoInicial As Double = 0
                            Dim dSaldoFinal As Double = 0
                            Dim dCheCobradosNO As Double = 0
                            Dim dCheCobradosSI As Double = 0
                            dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                            dSaldoFinal = Trim(dtDatos.Rows(0).Item("Saldo")) 'Saldo Final
                            dCheCobradosNO = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                            dCheCobradosSI = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                            'Agregar un nuevo detalle de libro Actual
                            eLibroBancos = New clsLibroBancos
                            eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro.Trim)
                            sCodigoDet = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro creado
                            Dim MarcaCheqNoCobrado As Integer = 0
                            'Dim Cobrados As Double = 0
                            'Dim NoCobrados As Double = 0
                            MarcaCheqNoCobrado = 0
                            Cobrados = dCheCobradosSI
                            NoCobrados = dCheCobradosNO

                            iResultadoDet = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpITFCant.Value, "00011", "", Trim(txtGlosaITFCant.Text.Trim), "", "H", Convert.ToDouble(txtImporITF.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                            'iResultadoDetx = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDetx, sCCabecera, dtpITFCant.Value, "00011", "", Trim(txtGlosaITFCant.Text.Trim), "", "H", Convert.ToDouble(txtImporITF.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")

                            'Dim saldo As Double = 0
                            If CountLibrosSuperiores = 0 Then
                                saldo = dSaldoFinal - Convert.ToDouble(txtImporITF.Text)
                            Else
                                saldo = dSaldoFinal
                            End If
                            'saldo = dSaldoFinal - Convert.ToDouble(txtImporteItf.Text)
                            'actualiza el nuevo libro
                            If iResultadoDet = 1 Then
                                Dim iResultadoActDetalleLibro As Int16 = 0
                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                                Dim dtDtFech As DataTable
                                dtDtFech = New DataTable
                                Dim Fecha As DateTime
                                Dim FechaActCabecera As DateTime
                                dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCodigoLibro)
                                If dtDtFech.Rows.Count > 0 Then
                                    Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                                End If
                                If dtpFechaItf.Value > Fecha Then
                                    FechaActCabecera = dtpFechaItf.Value
                                Else
                                    FechaActCabecera = Fecha
                                End If
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                                '----------------------------------------
                                'saber si existe libros proximos al mes actual de la cuenta
                                Dim dtDatosLibro As DataTable
                                dtDatosLibro = New DataTable
                                dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigoLibro, gEmpresa)
                                Dim sIdCuenta As String = ""
                                If dtDatosLibro.Rows.Count > 0 Then
                                    sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                                End If
                                Dim dtLibrosSuperiores As DataTable
                                dtLibrosSuperiores = New DataTable
                                dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                                If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                    For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                        Dim iResultadoActLibroSup As Int16 = 0
                                        Dim CodigoLibroBD As String = ""
                                        Dim SaldoInicialBD As Double ' = 0
                                        Dim SaldoFinalBD As Double '= 0
                                        CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                        SaldoInicialBD = Format(saldo, "0.00")
                                        SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtImporITF.Text)
                                        'txtImporITF
                                        'graba saldos de uno de los libro proximos
                                        '''''''''''''''''''''''''''''''''''''''''
                                        Dim SaldoPrendaS As Double
                                        Dim SaldoGarantiaS As Double
                                        Dim SaldoDepPlazS As Double
                                        Dim SaldoRetencionS As Double
                                        SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                        SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                        SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                        SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                        ''''''''''''''''''''''''''''''''''''''''''
                                        iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                        saldo = SaldoFinalBD
                                    Next
                                End If
                                '----------------------------------------
                            End If
                        ElseIf dtDatos.Rows.Count = 0 Then ' NO EXISTE LIBRO DEL MES DE PAGO
                            Dim MesAnt As String = ""
                            Dim Anio As String = ""
                            MesAnt = Format(Mes - 1, "00") 'resto el mes actual, para usar el mes anterior
                            Anio = Trim(dtpITFCant.Value.Year)
                            If Trim(Mes) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y a�o el ACTUAL -1
                                MesAnt = "12"
                                Anio = Trim(A�o) - 1
                            End If
                            Dim dtLibroAnterior As DataTable
                            dtLibroAnterior = New DataTable
                            dtLibroAnterior = eLibroBancos.TraerIdLibro(Anio, MesAnt, Trim(cboCuentas.SelectedValue), gEmpresa)
                            Dim iResultado As Int16 = 0
                            Dim iResultadoDet2 As Int16 = 0
                            Dim SaldoAnterior As Double = 0
                            If dtLibroAnterior.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigo(gEmpresa)
                                sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                                'graba la cabecera de libro
                                'saldo inicial = saldo final del libro anterior
                                SaldoAnterior = Trim(dtLibroAnterior.Rows(0).Item("Saldo"))
                                Dim SaldoPrenda As Double = 0
                                Dim SaldoGarantia As Double = 0
                                Dim SaldoDepPlaz As Double = 0
                                Dim SaldoRetencion As Double = 0
                                Dim SaldoIniExtracto As Double = 0
                                SaldoPrenda = Trim(dtLibroAnterior.Rows(0).Item("SaldoPrenda"))
                                SaldoGarantia = Trim(dtLibroAnterior.Rows(0).Item("SaldoGarantia"))
                                SaldoDepPlaz = Trim(dtLibroAnterior.Rows(0).Item("SaldoDepPlaz"))
                                SaldoRetencion = Trim(dtLibroAnterior.Rows(0).Item("SaldoRetencion"))
                                SaldoIniExtracto = Trim(dtLibroAnterior.Rows(0).Item("SaldoFinExtracto"))

                                'SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion
                                iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtImporITF.Text), dtpITFCant.Value, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                            ElseIf dtLibroAnterior.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigo(gEmpresa)
                                sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                                'graba la cabecera de libro
                                'saldo anterior =0
                                SaldoAnterior = 0
                                iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtImporITF.Text), dtpITFCant.Value, 0, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                            End If

                            If iResultado = 2627 Then
                                MessageBox.Show("No se Grabo el Registro Intente grabar otra vez", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                Exit Sub
                            End If

                            If iResultado > 0 And iResultado <> 2627 Then
                                eLibroBancos = New clsLibroBancos
                                eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro)
                                sCodigoDet = eLibroBancos.sCodFuturoDet

                                Dim MarcaCheqNoCobrado As Integer = 0
                                Cobrados = 0
                                NoCobrados = 0

                                iResultadoDet2 = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpITFCant.Value, "00011", "", Trim(txtGlosaITFCant.Text.Trim), "", "H", Convert.ToDouble(txtImporITF.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                                'iResultadoDet = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpITFCant.Value, "00011", "", Trim(txtGlosaITFCant.Text.Trim), "", "H", Convert.ToDouble(txtImporITF.Text), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")

                                'Dim saldo As Double = 0
                                saldo = SaldoAnterior - Convert.ToDouble(txtImporITF.Text)
                                'actualiza el nuevo libro
                                If iResultadoDet2 = 1 Then
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                                    Dim iResultadoActSaldoLibro As Int16 = 0
                                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(SaldoAnterior, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), dtpFechaItf.Value) 'graba el nuevo saldo del libro
                                End If
                            End If
                        End If
                        '************** FIN LIBRO BANCOS CUENTA ORIGEN ********************************************************
                        '******************************************************************************************************
                    End If

                    '----------------------------------------
                End If
            End If 'If iActCheckDet = 1 Then

        End If 'If dgvLibroDet.Rows.Count > 0 Then

        If iActCheckDet = 1 And iResultadoDetx = 1 And iResultadoActSaldoLibroActual = 1 Then
            cboMes_SelectedIndexChanged(sender, e) 'lista el nuevo detalle y calcula el saldo al final
            'MessageBox.Show("Se Agrego el ITF con Exito!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Panel4.Visible = False
            btnNuevo.Enabled = True
            'btnCancelar.Enabled = False
            btnGrabar.Enabled = False
            '''''''''''''Posicionar''''''''''''''''''''''''
            Posicionar()
            '''''''''''''Posicionar''''''''''''''''''''''''
        End If
        Panel3.Visible = False
        Panel4.Visible = False
        Panel5.Visible = False
    End Sub

    Public hace As Integer = 0
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        hace = 0

        Dim x As frmLibroBancosBuscar = frmLibroBancosBuscar.Instance
        x.Owner = Me
        x.sCuentaBuscar = Trim(cboCuentas.SelectedValue)
        x.ShowInTaskbar = False
        x.ShowDialog()

        If hace = 0 Then
            btnNuevo_Click(sender, e)
            Exit Sub
        End If

        If hace = 1 Then
            cboMes_SelectedIndexChanged(sender, e)
            '''''''''''''Posicionar''''''''''''''''''''''''
            Posicionar()
            '''''''''''''Posicionar''''''''''''''''''''''''
            btnNuevo_Click(sender, e)
            Exit Sub
        End If

        'Dim x As frmVePartidasContrato = frmVePartidasContrato.Instance
        'x.Owner = Me
        ''x.MdiParent = frmPrincipal
        'x.IdContrato = Trim(cboPartidas.SelectedValue)
        'x.CCosto = Trim(CCosto.SelectedValue)
        'x.ShowInTaskbar = False
        'x.ShowDialog()

    End Sub

    Private Sub txtImporte_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtImporte.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtImporte.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtImporte.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub btnGenerarITF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerarITF.Click
        Panel5.Visible = False
        Panel3.Visible = True
        'if el tipo de Mov. es Cheque
        If cboTipoMov.SelectedValue = "00002" Then
            dtpFechaItf.Value = Now.Date().ToShortDateString
            txtGlosaItf.Text = "ITF CHEQ " & txtNroDoc.Text.Trim
            Dim dtDoble As DataTable
            dtDoble = New DataTable
            eITF = New clsITF
            'Capturo el ITF
            dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpFechaItf.Value)
            If dtDoble.Rows.Count > 0 Then
                PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
            End If
            txtITF1.Text = Format(PorcentajeITF, "#,##0.0000")
            Dim ItfMontoSumado As Double = 0
            Dim MontoParaItf As Double = 0
            MontoParaItf = ReturnMontoParaCalcItf(txtImporte.Text)
            ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100

            txtImporteItf.Text = Format(ItfMontoSumado, "#,##0.00")
            dtpFechaItf.Focus()
        End If
    End Sub

    Private Sub btnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        Dim c As Cursor = Me.Cursor
        Try

            If (MessageBox.Show("�Esta seguro de Confirmar el Cheque como Cobrado con fecha " & dtpFechaCobro.Value & "?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                Me.Cursor = Cursors.WaitCursor
                Dim Cobrados As Double = 0
                Dim NoCobrados As Double = 0

                Dim iResultadoDetx As Int16 = 0
                Dim iResultadoActSaldoLibroActual As Int16 = 0
                If dgvLibroDet.Rows.Count > 0 Then
                    If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value) = False Then
                        eLibroBancos = New clsLibroBancos
                        Dim sCDetalle As String = ""
                        Dim sCCabecera As String = ""
                        Dim iActCheckDet As Integer = 0
                        sCDetalle = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value
                        sCodigoDetPosicion = Trim(sCDetalle)
                        sCCabecera = dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value
                        'DESACTIVA CHEK DEL CHEQUE EN BD
                        iActCheckDet = eLibroBancos.fActMarcaDet(gEmpresa, Trim(sCDetalle), Trim(sCCabecera), dtpFechaCobro.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtDescripcion.Text), "", 0, 0, 0, 0)
                        If iActCheckDet = 1 Then
                            '--------------
                            Dim dtDatos As DataTable
                            dtDatos = New DataTable
                            dtDatos = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                            Dim dSaldoInicial As Double = 0
                            If dtDatos.Rows.Count > 0 Then
                                dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                                Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                                NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                            End If
                            '--------------
                            'ACTUALIZAR EL SALDO DEL LIBRO
                            Dim dtLibroDet As DataTable
                            dtLibroDet = New DataTable
                            Dim saldo As Double = 0
                            saldo = dSaldoInicial
                            'Dim Cobrados As Double = 0
                            'Dim NoCobrados As Double = 0
                            dtLibroDet = eLibroBancos.TraerLibroDet(37, sCCabecera, gEmpresa)
                            If dtLibroDet.Rows.Count > 0 Then
                                For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                    For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                        If z = 11 Then 'lee el importe
                                            If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                                saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                                Dim iResultadoActDetalleLibro As Int16 = 0
                                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                            ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                                saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                                Dim iResultadoActDetalleLibro As Int16 = 0
                                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                                'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                                'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                                '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                                'End If
                                                'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                                '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                                'End If
                                            End If
                                        End If
                                    Next
                                Next
                                Dim dtDtFech As DataTable
                                dtDtFech = New DataTable
                                Dim Fecha As DateTime
                                Dim FechaActCabecera As DateTime
                                dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCCabecera)
                                If dtDtFech.Rows.Count > 0 Then
                                    Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                                End If
                                If dtpFechaItf.Value > Fecha Then
                                    FechaActCabecera = dtpFechaItf.Value
                                Else
                                    FechaActCabecera = Fecha
                                End If

                                'If Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "0" Then
                                Cobrados = Cobrados + Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                'ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "1" Then
                                NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                'End If

                                iResultadoActSaldoLibroActual = eLibroBancos.fActSaldoLibro(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro

                            End If 'If dtLibroDet.Rows.Count > 0 Then
                        End If 'If iActCheckDet = 1 Then
                    End If
                End If 'If dgvLibroDet.Rows.Count > 0 Then
                If iResultadoActSaldoLibroActual = 1 Then
                    'Panel5.Visible = False

                    dtpFechaItf.Value = Now.Date().ToShortDateString
                    txtGlosaItf.Clear()
                    txtImporteItf.Clear()
                    cboMes_SelectedIndexChanged(sender, e)
                    Panel5.Visible = False

                    '''''''''''''Posicionar''''''''''''''''''''''''
                    Posicionar()
                    '''''''''''''Posicionar''''''''''''''''''''''''

                    'MessageBox.Show("Se confirm� el cobro del Cheque", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    'Panel5.Visible = False
                End If
                Panel3.Visible = False
                Panel4.Visible = False
                Panel5.Visible = False
            End If

      
           Catch ex As Exception
            MessageBox.Show(ex.Message)

        Finally
            Me.Cursor = c
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If dgvLibroDet.Rows.Count > 0 Then
            dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column9").Value = True
            'Panel3.Visible = False
            'Panel4.Visible = False
            'Panel5.Visible = False
            dtpFechaItf.Value = Now.Date().ToShortDateString
            txtGlosaItf.Clear()
            txtImporteItf.Clear()
            'Panel3.Visible = False
            'Panel4.Visible = False
            'Panel5.Visible = False
        End If
        Panel3.Visible = False
        Panel4.Visible = False
        Panel5.Visible = False
    End Sub


    Private Sub btnEliminar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim Cobrados As Double = 0
        Dim NoCobrados As Double = 0
        If dgvLibroDet.Rows.Count > 0 Then
            If Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = True Then
                MessageBox.Show("Este registro ya esta conciliado y no se puede eliminar!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
                'ElseIf Convert.ToBoolean(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Conciliado").Value) = False Then
                '    btnGrabar.Enabled = True
                '    btnEliminar.Enabled = True
            End If
        End If
        

        If (MessageBox.Show("�Esta seguro de Eliminar el Registro?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            eLibroBancos = New clsLibroBancos
            If Len(txtCodDet.Text.Trim) > 0 Then
                If dgvLibroDet.Rows.Count > 0 Then
                    Dim sCDetalle As String = ""
                    Dim sCCabecera As String = ""
                    sCDetalle = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value)

                    'If dgvLibroDet.CurrentRow.Index > 0 Then
                    '    sCodigoDetPosicion = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index - 1).Cells(8).Value)
                    'ElseIf dgvLibroDet.CurrentRow.Index = 0 Then
                    '    'sCodigoDetPosicion = Trim(dgvLibroDet.Rows(0).Cells(8).Value)
                    'End If

                    sCCabecera = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value)

                    Dim dSaldoInicial As Double = 0
                    Dim dtDatos As DataTable
                    dtDatos = New DataTable
                    dtDatos = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
                    If dtDatos.Rows.Count = 1 Then 'si
                        dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                        Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                        NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                    End If
                    'DESACTIVA CHEK DEL CHEQUE EN BD
                    If Trim(sCCabecera) <> "" And Trim(sCDetalle) <> "" And gEmpresa <> "" Then
                        eLibroBancos = New clsLibroBancos
                        Dim iResultadoActDetalle As Int16 = 0
                        iResultadoActDetalle = eLibroBancos.fActDelete2(gEmpresa, Trim(sCDetalle), Trim(sCCabecera), dtpFecha.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtDescripcion.Text), "", 0, 0, 0, 0)

                        ''''''''''''''''''QUITAR ITF''''''''''''''''''''''''''''''
                        'If (Trim(cboTipoMov.SelectedValue) = "00005" Or Trim(cboTipoMov.SelectedValue) = "00007") And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 And Len(txtMontoAutomatico.Text.Trim) > 0 Then
                        'no graba por que es de cuenta a cuenta
                        'Else
                        'If Trim(cboTipoPago.SelectedValue) <> "00002" Then
                        'Dim dtExisteItf As DataTable
                        'dtExisteItf = New DataTable
                        'dtExisteItf = eLibroBancos.TraerSiExisteItf(gEmpresa, Trim(sCCabecera), Trim(sCCabecera), Trim(sCDetalle))
                        'Dim iResultadoEliminarItf As Integer = 0
                        'Dim IdDetLibroItf As String = ""
                        'Dim IdCabLibroItf As String = ""
                        'If dtExisteItf.Rows.Count = 1 Then
                        '    IdDetLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroDet")) 'Saldo Inicial
                        '    IdCabLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroCab")) 'Saldo Inicial
                        '    'iResultadoEliminarItf = eLibroBancos.fActDet(gEmpresa, IdDetLibroItf, IdCabLibroItf, dtpFechaGasto.Value, "00011", "", TipoMovCadena, "", "H", Convert.ToDouble(ItfMontoSumado), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "")
                        '    iResultadoEliminarItf = eLibroBancos.fActDelete2(gEmpresa, Trim(IdDetLibroItf), Trim(IdCabLibroItf), dtpFecha.Value, Trim(cboTipoMov.SelectedValue), Trim(txtNroDoc.Text), Trim(txtConcepto.Text), Trim(txtConcepto.Text), "", Convert.ToDouble(txtImporte.Text), 0, 0, 0)
                        'End If
                        'End If
                        'End If
                        ''''''''''''''''''fin QUITAR itf''''''''''''''''''''''''''''''


                        If iResultadoActDetalle > 0 Then
                            If cboTipoCargo.SelectedIndex > -1 Then
                                Dim ImporteOculto As Double = 0
                                Dim CargoOculto As String = ""
                                If dgvLibroDet.Rows.Count > 0 Then
                                    ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                    CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                                End If
                                Dim SaldoPrenda As Double = 0
                                Dim SaldoGarantia As Double = 0
                                Dim SaldoDepPla As Double = 0
                                Dim SaldoRetencion As Double = 0
                                'If CargoOculto <> cboTipoCargo.SelectedValue Then
                                If CargoOculto = "00002" Then
                                    SaldoPrendaBD = SaldoPrendaBD - ImporteOculto
                                ElseIf CargoOculto = "00003" Then
                                    SaldoGarantiaBD = SaldoGarantiaBD - ImporteOculto
                                ElseIf CargoOculto = "00004" Then
                                    SaldoDepPlazBD = SaldoDepPlazBD - ImporteOculto
                                ElseIf CargoOculto = "00005" Then
                                    SaldoRetencionBD = SaldoRetencionBD - ImporteOculto
                                End If
                                Dim iResLibroAsignado As Integer = 0
                                iResLibroAsignado = eLibroBancos.fActualizarLibroAsignado(43, sCodigoLibroAsignado, gEmpresa, SaldoPrendaBD, SaldoGarantiaBD, SaldoDepPlazBD, SaldoRetencionBD)
                            End If
                        End If

                        'falta actualizar saldo de libro
                        '--------------
                        Dim iResultadoActSaldoLibro As Int16 = 0
                        Dim dtLibroDet As DataTable
                        dtLibroDet = New DataTable
                        Dim saldo As Double = 0
                        saldo = dSaldoInicial
                        'Dim Cobrados As Double = 0
                        'Dim NoCobrados As Double = 0
                        dtLibroDet = eLibroBancos.TraerLibroDet(37, Trim(sCCabecera), gEmpresa)
                        If dtLibroDet.Rows.Count > 0 Then
                            For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                    If z = 11 Then 'lee el importe
                                        If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                            saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                        ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                            saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sCCabecera), saldo)
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                            '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                        End If
                                    End If
                                Next
                            Next
                            Dim dtDtFech As DataTable
                            dtDtFech = New DataTable
                            Dim Fecha As DateTime
                            Dim FechaActCabecera As DateTime
                            dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCCabecera)
                            If dtDtFech.Rows.Count > 0 Then
                                Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                            End If
                            If dtpFecha.Value > Fecha Then
                                FechaActCabecera = dtpFecha.Value
                            Else
                                FechaActCabecera = Fecha
                            End If

                            If Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "0" Then
                                Cobrados = Cobrados - Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                            ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "1" Then
                                NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                            End If


                            If sTipoCuenta = "00002" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibroPrenda(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00003" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibroGarantia(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00004" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoDepPlaz(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            ElseIf sTipoCuenta = "00005" Then
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoRetencion(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, Format(saldo, "0.00")) 'graba el nuevo saldo del libro
                            Else
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCCabecera, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                            End If
                            '----------------------------------------
                            'saber si existe libros proximos al mes actual de la cuenta
                            Dim dtDatosLibro As DataTable
                            dtDatosLibro = New DataTable
                            dtDatosLibro = eLibroBancos.TraerIdLibro2(sCCabecera, gEmpresa)
                            Dim sIdCuenta As String = ""
                            If dtDatosLibro.Rows.Count > 0 Then
                                sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                            End If
                            Dim dtLibrosSuperiores As DataTable
                            dtLibrosSuperiores = New DataTable
                            Dim A�o As String = dtpFecha.Value.Year
                            Dim Mes As String = Format(dtpFecha.Value.Month, "00")
                            dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, dtpFecha.Value) 'trae los proximos libros
                            If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                    Dim iResultadoActLibroSup As Int16 = 0
                                    Dim CodigoLibroBD As String = ""
                                    Dim SaldoInicialBD As Double ' = 0
                                    Dim SaldoFinalBD As Double '= 0
                                    CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    SaldoInicialBD = Format(saldo, "0.00")
                                    Dim ImporteOculto As Double = 0
                                    If dgvLibroDet.Rows.Count > 0 Then
                                        ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                    End If
                                    Dim TipoOpeOculto As String = ""
                                    If dgvLibroDet.Rows.Count > 0 Then
                                        TipoOpeOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(10).Value)
                                    End If
                                    If TipoOpeOculto = "H" Then
                                        SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto ') - Convert.ToDouble(txtMonto.Text)
                                    ElseIf TipoOpeOculto = "D" Then
                                        SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - ImporteOculto ') - Convert.ToDouble(txtMonto.Text)
                                    End If
                                    'SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto ') - Convert.ToDouble(txtMonto.Text)
                                    Dim SaldoPrendaS As Double
                                    Dim SaldoGarantiaS As Double
                                    Dim SaldoDepPlazS As Double
                                    Dim SaldoRetencionS As Double
                                    If CuentaEncargada <> "" Then
                                        If sIdCuenta <> CuentaEncargada Then
                                            SaldoPrendaS = 0
                                            SaldoGarantiaS = 0
                                            SaldoDepPlazS = 0
                                            SaldoRetencionS = 0
                                        ElseIf sIdCuenta = CuentaEncargada Then
                                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                            If cboTipoCargo.SelectedIndex > -1 Then
                                                'Dim ImporteOculto As Double = 0
                                                Dim CargoOculto As String = ""
                                                If dgvLibroDet.Rows.Count > 0 Then
                                                    ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                                    CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                                                End If
                                                If CargoOculto <> cboTipoCargo.SelectedValue Then
                                                    If CargoOculto = "00002" Then
                                                        SaldoPrendaS = SaldoPrendaS - ImporteOculto
                                                    ElseIf CargoOculto = "00003" Then
                                                        SaldoGarantiaS = SaldoGarantiaS - ImporteOculto
                                                    ElseIf CargoOculto = "00004" Then
                                                        SaldoDepPlazS = SaldoDepPlazS - ImporteOculto
                                                    ElseIf CargoOculto = "00005" Then
                                                        SaldoRetencionS = SaldoRetencionS - ImporteOculto
                                                    End If
                                                End If

                                            End If
                                        End If
                                    Else
                                        If sTipoCuenta = "00001" Then
                                        ElseIf sTipoCuenta = "00002" Then
                                            SaldoPrendaS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00003" Then
                                            SaldoGarantiaS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00004" Then
                                            SaldoDepPlazS = SaldoFinalBD
                                        ElseIf sTipoCuenta = "00005" Then
                                            SaldoRetencionS = SaldoFinalBD
                                        End If
                                    End If
                                    '''''''''''''''''''''''''''''''''''''''''
                                    'graba saldos de uno de los libro proximos
                                    iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today(), SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                    'graba saldos de uno de los libro proximos
                                    'iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax)
                                    saldo = SaldoFinalBD
                                Next
                                If CuentaEncargada <> "" Then
                                    If sIdCuenta <> CuentaEncargada Then
                                        Dim dtLibrosSuperiores2 As DataTable
                                        dtLibrosSuperiores2 = New DataTable
                                        Dim A�o2 As String = dtpFecha.Value.Year
                                        Dim Mes2 As String = Format(dtpFecha.Value.Month, "00")
                                        dtLibrosSuperiores2 = eLibroBancos.TraerLibrosSuperiores(Mes2, A�o2, CuentaEncargada, gEmpresa, dtpFecha.Value) 'trae los proximos libros
                                        If dtLibrosSuperiores2.Rows.Count > 0 Then
                                            For y As Integer = 0 To dtLibrosSuperiores2.Rows.Count - 1
                                                Dim iResultadoActLibroSup As Int16 = 0
                                                Dim CodigoLibroBD As String = ""
                                                Dim SaldoInicialBD As Double ' = 0
                                                Dim SaldoFinalBD As Double '= 0
                                                CodigoLibroBD = Trim(dtLibrosSuperiores2.Rows(y).Item(0).ToString)
                                                SaldoInicialBD = Trim(dtLibrosSuperiores2.Rows(y).Item(1).ToString)
                                                SaldoFinalBD = Trim(dtLibrosSuperiores2.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                                Cobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(9).ToString)
                                                NoCobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(10).ToString)
                                                Dim SaldoPrenda As Double
                                                Dim SaldoGarantia As Double
                                                Dim SaldoDepPla As Double
                                                Dim SaldoRetencion As Double
                                                SaldoPrenda = Trim(dtLibrosSuperiores2.Rows(y).Item(5).ToString)
                                                SaldoGarantia = Trim(dtLibrosSuperiores2.Rows(y).Item(6).ToString)
                                                SaldoDepPla = Trim(dtLibrosSuperiores2.Rows(y).Item(7).ToString)
                                                SaldoRetencion = Trim(dtLibrosSuperiores2.Rows(y).Item(8).ToString)
                                                Dim ImporteOculto As Double = 0
                                                Dim CargoOculto As String = ""
                                                If dgvLibroDet.Rows.Count > 0 Then
                                                    ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                                    CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                                                End If
                                                If CargoOculto = "00002" Then
                                                    SaldoPrenda = SaldoPrenda - ImporteOculto
                                                ElseIf CargoOculto = "00003" Then
                                                    SaldoGarantia = SaldoGarantia - ImporteOculto
                                                ElseIf CargoOculto = "00004" Then
                                                    SaldoDepPla = SaldoDepPla - ImporteOculto
                                                ElseIf CargoOculto = "00005" Then
                                                    SaldoRetencion = SaldoRetencion - ImporteOculto
                                                End If
                                                '''''''''''''''''''''''''''''''''''''''''
                                                'graba saldos de uno de los libro proximos
                                                iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today(), SaldoPrenda, SaldoGarantia, SaldoDepPla, SaldoRetencion)
                                            Next
                                        End If
                                    End If 'If sIdCuenta <> CuentaEncargada Then
                                End If 'If CuentaEncargada <> "" Then
                            Else
                                If CuentaEncargada <> "" Then
                                    Dim dtLibrosSuperiores2 As DataTable
                                    dtLibrosSuperiores2 = New DataTable
                                    Dim A�o2 As String = dtpFecha.Value.Year
                                    Dim Mes2 As String = Format(dtpFecha.Value.Month, "00")
                                    dtLibrosSuperiores2 = eLibroBancos.TraerLibrosSuperiores(Mes2, A�o2, CuentaEncargada, gEmpresa, dtpFecha.Value) 'trae los proximos libros
                                    If dtLibrosSuperiores2.Rows.Count > 0 Then
                                        For y As Integer = 0 To dtLibrosSuperiores2.Rows.Count - 1
                                            Dim iResultadoActLibroSup As Int16 = 0
                                            Dim CodigoLibroBD As String = ""
                                            Dim SaldoInicialBD As Double ' = 0
                                            Dim SaldoFinalBD As Double '= 0
                                            CodigoLibroBD = Trim(dtLibrosSuperiores2.Rows(y).Item(0).ToString)
                                            SaldoInicialBD = Trim(dtLibrosSuperiores2.Rows(y).Item(1).ToString)
                                            SaldoFinalBD = Trim(dtLibrosSuperiores2.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                            Cobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(9).ToString)
                                            NoCobrados = Trim(dtLibrosSuperiores2.Rows(y).Item(10).ToString)
                                            '''''''''''''''''''''''''''''''''''''''''
                                            Dim SaldoPrenda As Double
                                            Dim SaldoGarantia As Double
                                            Dim SaldoDepPla As Double
                                            Dim SaldoRetencion As Double
                                            SaldoPrenda = Trim(dtLibrosSuperiores2.Rows(y).Item(5).ToString)
                                            SaldoGarantia = Trim(dtLibrosSuperiores2.Rows(y).Item(6).ToString)
                                            SaldoDepPla = Trim(dtLibrosSuperiores2.Rows(y).Item(7).ToString)
                                            SaldoRetencion = Trim(dtLibrosSuperiores2.Rows(y).Item(8).ToString)
                                            Dim ImporteOculto As Double = 0
                                            Dim CargoOculto As String = ""
                                            If dgvLibroDet.Rows.Count > 0 Then
                                                ImporteOculto = Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                                                CargoOculto = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(25).Value)
                                            End If
                                            'If CargoOculto <> cboTipoCargo.SelectedValue Then
                                            If CargoOculto = "00002" Then
                                                SaldoPrenda = SaldoPrenda - ImporteOculto
                                            ElseIf CargoOculto = "00003" Then
                                                SaldoGarantia = SaldoGarantia - ImporteOculto
                                            ElseIf CargoOculto = "00004" Then
                                                SaldoDepPla = SaldoDepPla - ImporteOculto
                                            ElseIf CargoOculto = "00005" Then
                                                SaldoRetencion = SaldoRetencion - ImporteOculto
                                            End If
                                            '''''''''''''''''''''''''''''''''''''''''
                                            'graba saldos de uno de los libro proximos
                                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today(), SaldoPrenda, SaldoGarantia, SaldoDepPla, SaldoRetencion)
                                            'saldo = SaldoFinalBD
                                        Next
                                    End If
                                End If 'If CuentaEncargada <> "" Then
                            End If
                            '----------------------------------------

                            'If dgvLibroDet.CurrentRow.Index > 0 Then
                            '    sCodigoDetPosicion = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index - 1).Cells(8).Value)
                            'ElseIf dgvLibroDet.CurrentRow.Index = 0 Then
                            '    'sCodigoDetPosicion = Trim(dgvLibroDet.Rows(0).Cells(8).Value)
                            'End If

                            cboMes_SelectedIndexChanged(sender, e) 'lista el nuevo detalle y calcula el saldo al final

                            If iResultadoActSaldoLibro = 1 Then
                                'MessageBox.Show("Se Elimino el Registro con Exito!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                'btnNuevo.Enabled = True
                                'btnCancelar.Enabled = False
                                'btnGrabar.Enabled = False

                                'MessageBox.Show("Se Agrego con Exito!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                btnNuevo.Enabled = True
                                'btnCancelar.Enabled = False
                                btnGrabar.Enabled = False

                                btnNuevo_Click(sender, e)
                                'dgvLibroDet.Focus()
                                dtpFecha.Focus()

                                

                            End If

                        End If
                    End If
                End If
            End If 'If Len(txtCodDet.Text.Trim) > 0 Then
            cboMes_SelectedIndexChanged(sender, e)
        Else
            btnNuevo.Enabled = True
            'btnCancelar.Enabled = False
            btnGrabar.Enabled = False
        End If
    End Sub

  
Private Sub txtImporITF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporITF.TextChanged

End Sub

    Private Sub dtpFechaItf_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaItf.ValueChanged
        'btnGenerarITF_Click(sender, e)

        Panel5.Visible = False
        Panel3.Visible = True
        'Dim SumaCheques As Double = 0
        If cboTipoMov.SelectedValue = "00002" Then
            'dtpFechaItf.Value = Now.Date().ToShortDateString
            txtGlosaItf.Text = "ITF CHEQ " & txtNroDoc.Text.Trim

            Dim dtDoble As DataTable
            dtDoble = New DataTable
            eITF = New clsITF
            dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpFechaItf.Value)
            If dtDoble.Rows.Count > 0 Then
                PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
            End If

            txtITF1.Text = Format(PorcentajeITF, "#,##0.0000")
            Dim ItfMontoSumado As Double = 0

            'SumaCheques = Val(txtImporte.Text)
            'ItfMontoSumado = (txtImporte.Text * PorcentajeITF) / 100

            Dim MontoParaItf As Double = 0
            MontoParaItf = ReturnMontoParaCalcItf(txtImporte.Text)
            ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100

            'txtImporITF.Text = Format(ItfMontoSumado, "#,##0.00")
            txtImporteItf.Text = Format(ItfMontoSumado, "#,##0.00")
            dtpFechaItf.Focus()
        End If

    End Sub

    Private Sub dtpITFCant_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpITFCant.ValueChanged
        'btnItfCant_Click(sender, e)

        Dim SumaCheques As Double = 0
        Dim GlosaCheques As String = ""
        If dgvLibroDet.Rows.Count > 0 Then
            For z As Integer = 0 To dgvLibroDet.Rows.Count - 1
                If Convert.ToBoolean(dgvLibroDet.Rows(z).Cells("ITF").Value) = True Then
                    SumaCheques = SumaCheques + dgvLibroDet.Rows(z).Cells("Column10").Value
                    GlosaCheques = GlosaCheques & " " & dgvLibroDet.Rows(z).Cells("NroDocumento2").Value
                End If
            Next



            'dtpITFCant.Value = Now.Date().ToShortDateString

            Dim dtDoble As DataTable
            dtDoble = New DataTable
            eITF = New clsITF
            dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpITFCant.Value)
            If dtDoble.Rows.Count > 0 Then
                PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
            End If

            txtITF2.Text = Format(PorcentajeITF, "#,##0.0000")
            txtGlosaITFCant.Text = "ITF :" & GlosaCheques
            txtSumaChe.Text = Format(SumaCheques, "#,##0.00")

            Dim ItfMontoSumado As Double = 0
            'ItfMontoSumado = (SumaCheques * PorcentajeITF) / 100

            Dim MontoParaItf As Double = 0
            MontoParaItf = ReturnMontoParaCalcItf(SumaCheques)
            ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100

            txtImporITF.Text = Format(ItfMontoSumado, "#,##0.00")
        End If
        If SumaCheques = 0 Then
            MessageBox.Show("Seleccione Cheques para Generar el ITF!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Panel4.Visible = True
        dtpITFCant.Focus()

    End Sub

    Private Sub lblTipoCta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblTipoCta.Click

    End Sub

    Private Sub txtImporte_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporte.TextChanged

    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        Dim np As Integer = 0 'dgvLibroDet.CurrentRow.Index
        
        Select Case e.KeyCode
            Case Keys.Enter
                If dgvLibroDet.Rows.Count > 0 Then

                    'Dim row As Integer = 1
                    'row = dgvLibroDet.CurrentRow.Index + 1
                    Dim x As Integer
                    'x = dgvLibroDet.FirstDisplayedScrollingRowIndex + 1
                    'For x As Integer = PosicionGrilla To dgvLibroDet.Rows.Count
                    'If x = dgvLibroDet.Rows.Count Then
                    '    x = 0
                    '    'dgvLibroDet.FirstDisplayedScrollingRowIndex = 0
                    'Else
                    '    x = dgvLibroDet.FirstDisplayedScrollingRowIndex + 1
                    'End If

                    For x = dgvLibroDet.FirstDisplayedScrollingRowIndex + 1 To dgvLibroDet.Rows.Count

                        If x = dgvLibroDet.Rows.Count Then
                            x = 0
                            dgvLibroDet.FirstDisplayedScrollingRowIndex = 0
                            TextBox1.Focus()
                            Exit For
                        End If

                        If (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column3").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column4").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column6").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column10").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Then 'Or (UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column4").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Then


                            Dim row As DataGridViewRow
                            For Each row In dgvLibroDet.Rows
                                row.Selected = False
                                dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                            Next

                            'row.Selected = True
                            dgvLibroDet.Rows(x).Selected = True
                            dgvLibroDet.FirstDisplayedScrollingRowIndex = x

                            x = x + 1
                            'dgvLibroDet.Rows(x).Cells("Column3").Value)
                            'TextBox1.Focus()
                            'PosicionGrilla = row.Index + 1
                            'row
                            If x = dgvLibroDet.Rows.Count Then
                                x = 0
                            End If
                            TextBox1.Focus()
                            Exit For
                        End If



                    Next
                    'If x = dgvLibroDet.Rows.Count Then
                    '    x = 0
                    'End If
                    'For x = 0 To 3

                    'Next



                End If
        End Select
    End Sub


    'Private Sub txtBuscarGrid_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscarGrid.TextChanged
    'Dataview1 = New DataView(DataSet1.Tables("TLista"))

    'DataGridView1.DataSource = Dataview1

    'Dataview1.Sort = "Nombre"

    'wbusqueda = UCase(TextBox1.Text)

    'Dim myCurrencyManager As CurrencyManager

    'myCurrencyManager = CType(Me.BindingContext(Dataview1), CurrencyManager)

    'Dim j As Integer

    'j = Dataview1.Find(wbusqueda)

    'myCurrencyManager.Position = j




    'Dim row As DataGridViewRow
    'For Each row In dgvLibroDet.Rows
    '    If row.Cells("Column5").Value Like ("*" & txtBuscarGrid.Text & "*") Then
    '        'row.Selected = True
    '        dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
    '        Exit For
    '    End If
    'Next

    'End Sub

    'Dim PosicionGrilla As Integer = 0

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        Dim row As DataGridViewRow

        If Len(Trim(TextBox1.Text)) > 0 Then

            For Each row In dgvLibroDet.Rows
                'If UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*") Then
                row.Selected = False
                dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                'Exit For
                'End If
            Next

            For Each row In dgvLibroDet.Rows
                If (UCase(Trim(row.Cells("Column3").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column4").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column6").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column10").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Then
                    row.Selected = True
                    dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                    'PosicionGrilla = row.Index + 1
                    TextBox1.Focus()
                    Exit For
                End If
            Next

            TextBox1.Focus()

        ElseIf Len(Trim(TextBox1.Text)) = 0 Then
            For Each row In dgvLibroDet.Rows
                'If UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*") Then
                row.Selected = False
                dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                'Exit For
                'End If
            Next

            TextBox1.Focus()
        End If

    End Sub


    Private Sub Posicionar()
        Dim row As DataGridViewRow
        For Each row In dgvLibroDet.Rows
            row.Selected = False
            dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
        Next
        For Each row In dgvLibroDet.Rows
            If (UCase(Trim(row.Cells("Column2").Value)) Like ("*" & UCase(Trim(sCodigoDetPosicion)) & "*")) Then
                row.Selected = True
                dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                'TextBox1.Focus()
                Exit For
            End If
        Next
    End Sub


    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub txtImporteItf_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporteItf.TextChanged

    End Sub

    Private Sub chkIdentificar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub chkIdentificar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIdentificar.Click
    '    eRegistroDocumento = New clsRegistroDocumento
    '    If chkCancelado.Checked = True Then
    '        iResultado = eRegistroDocumento.fCancelarFactura(45, Trim(sCodRegistro), gEmpresa, "00004", 1, 1, gUsuario, Convert.ToString(IIf(Me.chkArchivado.Checked = True, 1, 0)))
    '    ElseIf chkCancelado.Checked = False Then
    '        iResultado = eRegistroDocumento.fCancelarFactura(45, Trim(sCodRegistro), gEmpresa, "00001", 0, 0, gUsuario, Convert.ToString(IIf(Me.chkArchivado.Checked = True, 1, 0)))
    '    End If
    'End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged

    End Sub

    Private Sub chkFecha_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFecha.Click

        If dgvLibroDet.Rows.Count > 0 Then
            If cboTipoMov.SelectedIndex > -1 Then
                Dim sCDetalle As String = ""
                Dim sCCabecera As String = ""
                sCDetalle = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(8).Value)
                sCCabecera = Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(12).Value)
                eLibroBancos = New clsLibroBancos
                If Len(Trim(sCDetalle)) > 0 Then
                    Dim iResultado As Integer = 0
                    iResultado = eLibroBancos.fIdenti(52, gEmpresa, sCDetalle, sCCabecera, Convert.ToString(IIf(Me.chkFecha.Checked = True, 1, 0)), dtpFechaPago.Value)
                    If chkFecha.Checked = True Then
                        dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(0).Style.BackColor = Color.Yellow
                        dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Identificarx").Value = "1"
                    ElseIf chkFecha.Checked = False Then
                        dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(0).Style.BackColor = Color.White
                        dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Identificarx").Value = "0"
                    End If
                End If
            End If
        End If
        
    End Sub

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click
        If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboAno.SelectedIndex > -1 And cboMes.SelectedIndex > -1 Then
            Dim x As frmLibroBancosExtractoBancario = frmLibroBancosExtractoBancario.Instance
            'x.Owner = Me
            x.MdiParent = frmPrincipal
            x.Anio = Trim(cboAno.Text)
            x.Mes = TraerNumeroMes(cboMes.Text)
            x.CodCuenta = Trim(cboCuentas.SelectedValue)



            Dim SaldoInicialExtracto As Double = 0
            Dim SaldoFinalExtracto As Double = 0
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            eLibroBancos = New clsLibroBancos
            dtDatos = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
            If dtDatos.Rows.Count > 0 Then
                SaldoInicialExtracto = Trim(dtDatos.Rows(0).Item("SaldoIniExtracto"))
                SaldoFinalExtracto = Trim(dtDatos.Rows(0).Item("SaldoFinExtracto"))
            End If

            x.SaldoInicialExtracto = Format(SaldoInicialExtracto, "#,##0.00")
            x.SaldoFinalExtracto = Format(SaldoFinalExtracto, "#,##0.00")
            x.SaldoExtracto = Format(SaldoInicialExtracto, "#,##0.00")

            x.BancoDes = Trim(cboBancos.Text)
            x.MonedaDes = Trim(lblMoneda.Text)
            x.NroCuentaDes = Trim(cboCuentas.Text)
            x.MesDes = Trim(cboMes.Text)

            'x.ShowInTaskbar = False
            'x.ShowDialog()

            'Dim x As frmSesionCajas = frmSesionCajas.Instance
            'x.MdiParent = Me
            'x.Show()


            'Dim x As New frmCajaChica ' 
            'x.MdiParent = frmPrincipal
            x.Show()

        Else
            If cboBancos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Banco", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboBancos.Focus()
                Exit Sub
            End If
            If cboCuentas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCuentas.Focus()
                Exit Sub
            End If
            If cboAno.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboAno.Focus()
                Exit Sub
            End If
            If cboMes.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Mes", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboMes.Focus()
                Exit Sub
            End If
        End If

        'Try
        '    eLibroBancos = New clsLibroBancos
        '    Dim dtDatos As DataTable
        '    dtDatos = New DataTable
        '    dtDatos = eLibroBancos.TraerExtracto(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
        '    '------------------------------------------------------------>
        '    If dtDatos.Rows.Count > 0 Then
        '        Dim TBL As New DataTable
        '        Dim DrFila As DataRow
        '        Dim CadenaCon As String = ""
        '        Dim RazonSocial As String = ""
        '        Dim iResultado As Integer = 0
        '        'For a As Integer = 0 To dtDatos.Columns.Count - 1
        '        '    TBL.Columns.Add(a)
        '        'Next
        '        TBL.Columns.Add("Fecha")
        '        TBL.Columns.Add("NroDocumento")
        '        TBL.Columns.Add("Concepto")
        '        TBL.Columns.Add("Descripcion")
        '        TBL.Columns.Add("Abono")
        '        TBL.Columns.Add("Cargo")
        '        TBL.Columns.Add("IdLibroDet")
        '        TBL.Columns.Add("IdLibroCab")
        '        TBL.Columns.Add("FechaCobro")
        '        TBL.Columns.Add("Saldo")
        '        TBL.Columns.Add("TipoOperacion")
        '        TBL.Columns.Add("Importe")
        '        Dim SaldoIniStr As Double = 50000
        '        Dim SaldoIni As Double = 50000
        '        Dim SaldoFinal As Double = 0
        '        For x As Integer = 0 To dtDatos.Rows.Count - 1
        '            DrFila = TBL.NewRow
        '            'DrFila(0) = dtDatos.Rows(x).Item("Fecha")
        '            DrFila("Fecha") = dtDatos.Rows(x).Item("Fecha")

        '            'DrFila(1) = dtDatos.Rows(x).Item("NroDocumento")
        '            DrFila("NroDocumento") = dtDatos.Rows(x).Item("NroDocumento")

        '            'DrFila(2) = dtDatos.Rows(x).Item("Concepto")
        '            DrFila("Concepto") = dtDatos.Rows(x).Item("Concepto")

        '            'DrFila(3) = dtDatos.Rows(x).Item("Descripcion")
        '            DrFila("Descripcion") = dtDatos.Rows(x).Item("Descripcion")

        '            'DrFila(4) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Abono")), "#,##0.00")
        '            DrFila("Abono") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Abono")), "#,##0.00")

        '            'DrFila(5) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Cargo")), "#,##0.00")
        '            DrFila("Cargo") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Cargo")), "#,##0.00")

        '            'DrFila(6) = dtDatos.Rows(x).Item("IdLibroDet")
        '            DrFila("IdLibroDet") = dtDatos.Rows(x).Item("IdLibroDet")

        '            'DrFila(7) = dtDatos.Rows(x).Item("IdLibroCab")
        '            DrFila("IdLibroCab") = dtDatos.Rows(x).Item("IdLibroCab")

        '            'DrFila(8) = dtDatos.Rows(x).Item("FechaCobro")
        '            DrFila("FechaCobro") = dtDatos.Rows(x).Item("FechaCobro")

        '            If dtDatos.Rows(x).Item("TipoOperacion") = "D" Then
        '                SaldoIni = SaldoIni + dtDatos.Rows(x).Item("Importe")
        '            ElseIf dtDatos.Rows(x).Item("TipoOperacion") = "H" Then
        '                SaldoIni = SaldoIni - dtDatos.Rows(x).Item("Importe")
        '            End If
        '            'DrFila(9) = Format(Convert.ToDouble(SaldoIni), "#,##0.00")
        '            DrFila("Saldo") = Format(Convert.ToDouble(SaldoIni), "#,##0.00")

        '            'DrFila(10) = dtDatos.Rows(x).Item("TipoOperacion")
        '            DrFila("TipoOperacion") = dtDatos.Rows(x).Item("TipoOperacion")

        '            'DrFila(11) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Importe")), "#,##0.00")
        '            DrFila("Importe") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Importe")), "#,##0.00")

        '            TBL.Rows.Add(DrFila)
        '        Next
        '        '------------------------------------------------------------>
        '        Muestra_Reporte(RutaAppReportes & "rptLibroBancosEstractoMes.rpt", TBL, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFecha.Value & " al " & dtpFecha.Value, "Titulo;" & "ESTRACTO BANCARIO", "SIni;" & SaldoIniStr, "Banco;" & cboBancos.Text, "Mon;" & lblMoneda.Text, "Cta;" & cboCuentas.Text, "Anio;" & cboAno.Text, "Mes;" & cboMes.Text)
        '        'Muestra_Reporte(RutaAppReportes & "rptLibroBancosEstractoMes.rpt", dtDatos, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFecha.Value & " al " & dtpFecha.Value, "Titulo;" & "ABONOS POR CUENTAS")
        '        '----Muestra_Reporte(RutaAppReportes & "rptGastosEnviados.rpt", dtTable2, "TblLibroDiario", "", "Banco;" & Trim(cboBanco.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Cuenta;" & Trim(cboCuenta.Text), "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value & " - Usuario: " & UCase(Trim(glbNameUsuario)))
        '    End If
        '    If dtDatos.Rows.Count = 0 Then
        '        Dim mensaje As String = ""
        '        mensaje = "No Se Encontraron Registros."
        '        MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    End If
        'Catch ex As Exception
        '    MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End Try
        'Me.Cursor = Cursors.Default
    End Sub


    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub dtpFecha_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFecha.LostFocus
        If iOpcion = 4 Then
            dtpFechaPago.Value = dtpFecha.Value
        End If

    End Sub

    Private Sub dtpFecha_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFecha.ValueChanged

    End Sub

    Private Sub chkConciliacionMes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConciliacionMes.CheckedChanged
        If Len(Trim(txtCod.Text)) > 0 Then

            If chkConciliacionMes.Checked = True Then
                If MesCerrado = 0 Then
                    If (MessageBox.Show("�Esta seguro de Cerrar la Conciliacion del Mes Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                        eLibroBancos = New clsLibroBancos
                        Dim iResultadoActDetalle As Int16 = 0
                        iResultadoActDetalle = eLibroBancos.fActCierreMesLibro(58, gEmpresa, Trim(txtCod.Text), 1)
                        If iResultadoActDetalle > 0 Then
                            chkConciliacionMes.Enabled = False
                            cboMes_SelectedIndexChanged(sender, e)
                        End If
                    Else
                        chkConciliacionMes.Checked = False
                        chkConciliacionMes.Enabled = True
                    End If
                End If
            End If

        End If
    End Sub



    Private Sub BloquearControles()
        If chkConciliacionMes.Enabled = False And chkConciliacionMes.Checked = True Then
            dtpFecha.Enabled = False
            rdbAbono.Enabled = False
            rdbCargo.Enabled = False
            cboTipoMov.Enabled = False
            txtNroDoc.Enabled = False
            txtConcepto.Enabled = False
            chkFecha.Enabled = False
            txtDescripcion.Enabled = False
            txtImporte.Enabled = False
            dtpFechaPago.Enabled = False

            txtNroMov.Enabled = False
            dtpFIni.Enabled = False
            dtpFfin.Enabled = False
            cboTipoCargo.Enabled = False
            txtCliente.Enabled = False
            txtCtaCliente.Enabled = False
            txtObra.Enabled = False
            btnNuevo.Enabled = False
            btnGrabar.Enabled = False
            btnBuscar.Enabled = False
            btnEliminar.Enabled = False
            btnItfCant.Enabled = False


            For z As Integer = 0 To dgvLibroDet.Rows.Count - 1
                dgvLibroDet.Rows(z).Cells("Column9").ReadOnly = True
                dgvLibroDet.Rows(z).Cells("Conciliado").ReadOnly = True
            Next

        ElseIf chkConciliacionMes.Enabled = True And chkConciliacionMes.Checked = False Then
            dtpFecha.Enabled = True
            rdbAbono.Enabled = True
            rdbCargo.Enabled = True
            cboTipoMov.Enabled = True
            txtNroDoc.Enabled = True
            txtConcepto.Enabled = True
            chkFecha.Enabled = True
            txtDescripcion.Enabled = True
            txtImporte.Enabled = True
            dtpFechaPago.Enabled = True

            txtNroMov.Enabled = True
            dtpFIni.Enabled = True
            dtpFfin.Enabled = True
            cboTipoCargo.Enabled = True
            txtCliente.Enabled = True
            txtCtaCliente.Enabled = True
            txtObra.Enabled = True
            btnNuevo.Enabled = True
            btnGrabar.Enabled = True
            btnBuscar.Enabled = True
            btnEliminar.Enabled = True
            btnItfCant.Enabled = True

            For z As Integer = 0 To dgvLibroDet.Rows.Count - 1
                dgvLibroDet.Rows(z).Cells("Column9").ReadOnly = False
                dgvLibroDet.Rows(z).Cells("Conciliado").ReadOnly = False
            Next
        End If

    End Sub

    Private Sub txtSaldoRetenido_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSaldoRetenido.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtSaldo.Text)) > 0 Then
                    'ePagoProveedores = New clsPagoProveedores
                    Dim ActualizaLineaCre As Integer = 0
                    'Dim Dias As Integer = 0
                    Dim SaldoContable As Double = 0
                    Dim SaldoRetenido As Double = 0
                    Dim SaldoDisponible As Double = 0
                    'If Len(Trim(txtDiasCred.Text)) = 0 Then
                    '    Dias = 0
                    'Else
                    '    Dias = Trim(txtDiasCred.Text)
                    'End If
                    'If Len(Trim(txtLineaCre.Text)) = 0 Then
                    '    Credito = 0
                    'Else
                    '    Credito = Trim(txtLineaCre.Text)
                    'End If
                    'If sMonCodigo = "01" Then
                    '    ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito2(29, Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), Credito, 0)
                    'ElseIf sMonCodigo = "02" Then
                    '    ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito2(28, Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), Credito, 0)
                    'End If

                    If Len(Trim(txtSaldoRetenido.Text)) = 0 Then
                        txtSaldoRetenido.Text = "0.00"
                    End If
                    txtSaldoRetenido.Text = Format(Convert.ToDouble(txtSaldoRetenido.Text), "#,##0.00")

                    txtSaldoDisponible.Text = Format(Trim(txtSaldo.Text) - Trim(txtSaldoRetenido.Text), "#,##0.00")
                    'txtTenerBanco.Text = Format(SaldoFinal + MontoChPagados, "#,##0.00")

                    'iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecerax, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)

                End If
        End Select
    End Sub

  
    Private Sub txtSaldoRetenido_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSaldoRetenido.TextChanged

    End Sub
End Class




