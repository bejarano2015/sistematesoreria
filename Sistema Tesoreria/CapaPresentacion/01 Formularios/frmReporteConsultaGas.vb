Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteConsultaGas

    Public prmOpcionx As Integer
    Public prmTextox As String
    Public prmFIni As DateTime
    Public prmFFin As DateTime
    Public prmCampoOrd As String
    Public prmAscDesc As String

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptF As New rptConsultaGastos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteConsultaGas = Nothing
    Public Shared Function Instance() As frmReporteConsultaGas
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteConsultaGas
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteConsultaGas_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteConsultaGas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rptF.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        '1
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Opcion")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = prmOpcionx
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Texto")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = prmTextox
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


        '3
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@xRUC")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmprRuc
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@xDESEMPRESA")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gDesEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaInicio")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = prmFIni
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '5
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaFinal")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = prmFFin
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '6
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@CampoOrdenar")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = prmCampoOrd
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '7
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@AscDesc")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = prmAscDesc
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = rptF
    End Sub

End Class