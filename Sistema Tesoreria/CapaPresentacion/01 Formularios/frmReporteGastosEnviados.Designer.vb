<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteGastosEnviados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboBanco = New ctrLibreria.Controles.BeComboBox
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.RD1 = New System.Windows.Forms.RadioButton
        Me.RD2 = New System.Windows.Forms.RadioButton
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.SuspendLayout()
        '
        'cboBanco
        '
        Me.cboBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBanco.ForeColor = System.Drawing.Color.Black
        Me.cboBanco.FormattingEnabled = True
        Me.cboBanco.KeyEnter = True
        Me.cboBanco.Location = New System.Drawing.Point(87, 8)
        Me.cboBanco.Name = "cboBanco"
        Me.cboBanco.Size = New System.Drawing.Size(262, 21)
        Me.cboBanco.TabIndex = 10
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(87, 33)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(262, 21)
        Me.cboCuenta.TabIndex = 11
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(8, 16)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel9.TabIndex = 12
        Me.BeLabel9.Text = "Banco"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(8, 41)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel4.TabIndex = 13
        Me.BeLabel4.Text = "Nº Cta Cte"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(260, 60)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(89, 20)
        Me.dtpFechaFin.TabIndex = 15
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(123, 60)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(88, 20)
        Me.dtpFechaIni.TabIndex = 14
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Location = New System.Drawing.Point(274, 89)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 18
        Me.btnImprimir.Text = "Ver"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'RD1
        '
        Me.RD1.AutoSize = True
        Me.RD1.Location = New System.Drawing.Point(192, 92)
        Me.RD1.Name = "RD1"
        Me.RD1.Size = New System.Drawing.Size(55, 17)
        Me.RD1.TabIndex = 61
        Me.RD1.Text = "Todos"
        Me.RD1.UseVisualStyleBackColor = True
        '
        'RD2
        '
        Me.RD2.AutoSize = True
        Me.RD2.Checked = True
        Me.RD2.Location = New System.Drawing.Point(87, 92)
        Me.RD2.Name = "RD2"
        Me.RD2.Size = New System.Drawing.Size(80, 17)
        Me.RD2.TabIndex = 62
        Me.RD2.Text = "Por Usuario"
        Me.RD2.UseVisualStyleBackColor = True
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(8, 64)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel1.TabIndex = 63
        Me.BeLabel1.Text = "Fecha Envío"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(84, 64)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(23, 13)
        Me.BeLabel2.TabIndex = 64
        Me.BeLabel2.Text = "Del"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(229, 64)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(16, 13)
        Me.BeLabel3.TabIndex = 65
        Me.BeLabel3.Text = "Al"
        '
        'frmReporteGastosEnviados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(366, 124)
        Me.Controls.Add(Me.BeLabel3)
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.RD2)
        Me.Controls.Add(Me.RD1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.dtpFechaFin)
        Me.Controls.Add(Me.dtpFechaIni)
        Me.Controls.Add(Me.BeLabel9)
        Me.Controls.Add(Me.BeLabel4)
        Me.Controls.Add(Me.cboBanco)
        Me.Controls.Add(Me.cboCuenta)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmReporteGastosEnviados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Gastos Enviados"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents RD1 As System.Windows.Forms.RadioButton
    Friend WithEvents RD2 As System.Windows.Forms.RadioButton
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
End Class
