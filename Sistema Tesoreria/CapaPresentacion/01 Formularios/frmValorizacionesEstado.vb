Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmValorizacionesEstado

    Private eValorizaciones As clsValorizaciones

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmValorizacionesEstado = Nothing
    Public Shared Function Instance() As frmValorizacionesEstado
        If frmInstance Is Nothing Then
            frmInstance = New frmValorizacionesEstado
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmValorizacionesEstado_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmValorizacionesEstado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtBuscar_TextChanged(sender, e)
    End Sub

    Private Sub txtBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscar.TextChanged
        Dim dtListadeVal As DataTable

        If Len(Trim(txtBuscar.Text)) = 0 Then
            eValorizaciones = New clsValorizaciones
            dtListadeVal = New DataTable
            dtListadeVal = eValorizaciones.fListarObras(14, gEmpresa, gPeriodo, "", "", Today(), "")
            If dtListadeVal.Rows.Count > 0 Then
                dgvDetalle.DataSource = dtListadeVal
            End If
        End If

    End Sub
End Class