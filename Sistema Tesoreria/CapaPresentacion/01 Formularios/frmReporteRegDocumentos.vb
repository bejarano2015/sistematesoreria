Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteRegDocumentos

    Public nroArqueo As String
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    'Dim rptDoc As New rptRegDocumentos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteRegDocumentos = Nothing
    Public Shared Function Instance() As frmReporteRegDocumentos
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteRegDocumentos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteRegDocumentos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteRegDocumentos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        'crParameterFieldDefinition = crParameterFieldDefinitions("@IdArqueo")
        'crParameterValues = crParameterFieldDefinition.CurrentValues

        'crParameterDiscreteValue = New ParameterDiscreteValue()
        'crParameterDiscreteValue.Value = nroArqueo
        'crParameterValues.Add(crParameterDiscreteValue)
        'crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        'CrystalReportViewer1.ReportSource = rptF

    End Sub

End Class