Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data


Public Class frmCajaChicaRequerimientos3

    Private eCompraRequerimientos As clsCompraRequerimientos
    Private eOrdenCabecera As clsOrdenCabecera
    Private ePagoProveedores As clsPagoProveedores
    Public IdCaja As String = ""
    Public IdDetCaja As String = ""
    Public TipoDoc As String = ""
    Public NroDoc As String = ""
    Public FechaFac As DateTime
    Public RucDoc As String = ""
    Public RazonDoc As String = ""
    Public MonedaDoc As String = ""
    Dim dTipoCambio As Double = 0
    Dim dIgv As Double = 0

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCajaChicaRequerimientos3 = Nothing
    Public Shared Function Instance() As frmCajaChicaRequerimientos3
        If frmInstance Is Nothing Then
            frmInstance = New frmCajaChicaRequerimientos3
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmCajaChicaRequerimientos3_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmCajaChicaRequerimientos3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'BeLabel3.Text = ""
        BeLabel4.Text = ""

        'eCompraRequerimientos = New clsCompraRequerimientos
        'cboEmpresas.DataSource = eCompraRequerimientos.fListar(0, "", "", "", gPeriodo)
        'If eCompraRequerimientos.iNroRegistros > 0 Then
        '    cboEmpresas.ValueMember = "EmprCodigo"
        '    cboEmpresas.DisplayMember = "EmprDescripcion"
        '    'cboEmpresas.SelectedValue = Trim(gEmpresa)
        '    cboEmpresas.Focus()
        'End If
        BeLabel4.Text = TipoDoc & " / " & NroDoc
        'cboBanco.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
        'If eGastosGenerales.iNroRegistros > 0 Then
        '    cboBanco.ValueMember = "IdBanco"
        '    cboBanco.DisplayMember = "NombreBanco"
        '    cboBanco.SelectedIndex = -1
        'End If
        'cboEmpresas.DropDownStyle = ComboBoxStyle.DropDownList
        'cboCCostos.DropDownStyle = ComboBoxStyle.DropDownList

        'For xCol As Integer = 0 To dgvReq.Columns.Count - 1
        '    dgvReq.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
        '    'dgvReq.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        'Next

        For xCol As Integer = 0 To dgvReqAgregados.Columns.Count - 1
            dgvReqAgregados.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
            'dgvReqAgregados.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next


        'Dim dtTableTipoCambio As DataTable
        'dtTableTipoCambio = New DataTable
        'ePagoProveedores = New clsPagoProveedores
        'dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, FechaFac)
        'If dtTableTipoCambio.Rows.Count > 0 Then
        '    dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
        '    dTipoCambio = Format(dTipoCambio, "0.00")
        'Else
        '    dTipoCambio = 0.0
        'End If

        'Dim dtTable As DataTable
        'dtTable = New DataTable
        'ePagoProveedores = New clsPagoProveedores
        ''eTempo = New clsPlantTempo
        'dtTable = ePagoProveedores.fListarParametroIGV(FechaFac)
        'If dtTable.Rows.Count > 0 Then
        '    dIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
        '    'txtTasaIGV.Text = Format(dIgv, "0.00")
        'Else
        '    dIgv = 0.0
        '    'txtTasaIGV.Text = Format(dIgv, "0.00")
        'End If

        For x As Integer = 0 To dgvReqAgregados.RowCount - 1
            dgvReqAgregados.Rows.Remove(dgvReqAgregados.CurrentRow)
        Next

        eCompraRequerimientos = New clsCompraRequerimientos
        Dim dtDatosEnReq As DataTable
        dtDatosEnReq = New DataTable
        dtDatosEnReq = eCompraRequerimientos.fListar2(12, gEmpresa, "", "", "", Trim(IdDetCaja), Trim(IdCaja))
        If dtDatosEnReq.Rows.Count > 0 Then
            'MessageBox.Show("Este Comprobante ya esta enlazado con una compra de Requerimientos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'Exit Sub
            dgvReqAgregados.DataSource = dtDatosEnReq

        End If

        dgvReqAgregados.Focus()

    End Sub

    Private Sub dgvReqAgregados_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvReqAgregados.CellContentClick

    End Sub

    Private Sub dgvReqAgregados_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvReqAgregados.KeyDown
        If dgvReqAgregados.Rows.Count > 0 Then
            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Hide()
            End Select
        End If
    End Sub

End Class