<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCajaChicaRequerimientos3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.dgvReqAgregados = New System.Windows.Forms.DataGridView
        Me.ReqCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Codigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UndMed = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CantGrupalPorComprar2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Precio2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvReqAgregados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(9, 10)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel4.TabIndex = 13
        Me.BeLabel4.Text = "Glosa Fac"
        '
        'dgvReqAgregados
        '
        Me.dgvReqAgregados.AllowUserToAddRows = False
        Me.dgvReqAgregados.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReqAgregados.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvReqAgregados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReqAgregados.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ReqCodigo, Me.Codigo2, Me.Descripcion2, Me.UndMed, Me.CantGrupalPorComprar2, Me.Precio2})
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvReqAgregados.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvReqAgregados.Location = New System.Drawing.Point(12, 34)
        Me.dgvReqAgregados.Name = "dgvReqAgregados"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReqAgregados.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvReqAgregados.RowHeadersVisible = False
        Me.dgvReqAgregados.Size = New System.Drawing.Size(695, 192)
        Me.dgvReqAgregados.TabIndex = 14
        '
        'ReqCodigo
        '
        Me.ReqCodigo.DataPropertyName = "ReqCodigo"
        Me.ReqCodigo.HeaderText = "Req."
        Me.ReqCodigo.Name = "ReqCodigo"
        '
        'Codigo2
        '
        Me.Codigo2.DataPropertyName = "Codigo"
        Me.Codigo2.HeaderText = "Cod. Mat"
        Me.Codigo2.Name = "Codigo2"
        '
        'Descripcion2
        '
        Me.Descripcion2.DataPropertyName = "Descripcion"
        Me.Descripcion2.HeaderText = "Descripcion"
        Me.Descripcion2.Name = "Descripcion2"
        Me.Descripcion2.ReadOnly = True
        Me.Descripcion2.Width = 220
        '
        'UndMed
        '
        Me.UndMed.DataPropertyName = "UmdFinalDescripcion"
        Me.UndMed.HeaderText = "UndMed"
        Me.UndMed.Name = "UndMed"
        Me.UndMed.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UndMed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.UndMed.Width = 80
        '
        'CantGrupalPorComprar2
        '
        Me.CantGrupalPorComprar2.DataPropertyName = "CantidadUniContada"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.CantGrupalPorComprar2.DefaultCellStyle = DataGridViewCellStyle7
        Me.CantGrupalPorComprar2.HeaderText = "Cant Comprada"
        Me.CantGrupalPorComprar2.Name = "CantGrupalPorComprar2"
        Me.CantGrupalPorComprar2.Width = 110
        '
        'Precio2
        '
        Me.Precio2.DataPropertyName = "DocoPrecioGrupal"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.Precio2.DefaultCellStyle = DataGridViewCellStyle8
        Me.Precio2.HeaderText = "Precio"
        Me.Precio2.Name = "Precio2"
        Me.Precio2.Width = 60
        '
        'frmCajaChicaRequerimientos3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 237)
        Me.Controls.Add(Me.BeLabel4)
        Me.Controls.Add(Me.dgvReqAgregados)
        Me.MaximizeBox = False
        Me.Name = "frmCajaChicaRequerimientos3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Requerimientos de Factura"
        CType(Me.dgvReqAgregados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvReqAgregados As System.Windows.Forms.DataGridView
    Friend WithEvents ReqCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Codigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UndMed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantGrupalPorComprar2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
