<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMantePolizaSub
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.gbVigencia = New System.Windows.Forms.GroupBox
        Me.lblCompa = New ctrLibreria.Controles.BeLabel
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.lblCliente = New ctrLibreria.Controles.BeLabel
        Me.lblMoneda = New ctrLibreria.Controles.BeLabel
        Me.lblRamo = New ctrLibreria.Controles.BeLabel
        Me.lblVigencia = New ctrLibreria.Controles.BeLabel
        Me.lblNroPoliza = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cboRamo = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.mtbVigenciaFin = New ctrLibreria.Controles.BeMaskedTextBox
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel
        Me.mtbVigenciaIni = New ctrLibreria.Controles.BeMaskedTextBox
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel
        Me.txtPoliza = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel24 = New ctrLibreria.Controles.BeLabel
        Me.gbOtros = New System.Windows.Forms.GroupBox
        Me.btngrabar = New System.Windows.Forms.Button
        Me.mtbFechaLetraIni = New ctrLibreria.Controles.BeMaskedTextBox
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel
        Me.cboFormaPago = New ctrLibreria.Controles.BeComboBox
        Me.txtLetras = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel
        Me.txtSumaAseguradora = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel
        Me.gbConceptos = New System.Windows.Forms.GroupBox
        Me.txtTotal = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.txtIGV = New ctrLibreria.Controles.BeTextBox
        Me.txtInteres = New ctrLibreria.Controles.BeTextBox
        Me.txtDerechoEmision = New ctrLibreria.Controles.BeTextBox
        Me.txtPrima = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.gbVigencia.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.gbOtros.SuspendLayout()
        Me.gbConceptos.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbVigencia
        '
        Me.gbVigencia.Controls.Add(Me.lblCompa)
        Me.gbVigencia.Controls.Add(Me.BeLabel12)
        Me.gbVigencia.Controls.Add(Me.lblCliente)
        Me.gbVigencia.Controls.Add(Me.lblMoneda)
        Me.gbVigencia.Controls.Add(Me.lblRamo)
        Me.gbVigencia.Controls.Add(Me.lblVigencia)
        Me.gbVigencia.Controls.Add(Me.lblNroPoliza)
        Me.gbVigencia.Controls.Add(Me.BeLabel5)
        Me.gbVigencia.Controls.Add(Me.BeLabel4)
        Me.gbVigencia.Controls.Add(Me.BeLabel3)
        Me.gbVigencia.Controls.Add(Me.BeLabel2)
        Me.gbVigencia.Controls.Add(Me.BeLabel1)
        Me.gbVigencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbVigencia.Location = New System.Drawing.Point(12, 10)
        Me.gbVigencia.Name = "gbVigencia"
        Me.gbVigencia.Size = New System.Drawing.Size(339, 207)
        Me.gbVigencia.TabIndex = 3
        Me.gbVigencia.TabStop = False
        Me.gbVigencia.Text = "Datos de Poliza"
        '
        'lblCompa
        '
        Me.lblCompa.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCompa.AutoSize = True
        Me.lblCompa.BackColor = System.Drawing.Color.Transparent
        Me.lblCompa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompa.ForeColor = System.Drawing.Color.Black
        Me.lblCompa.Location = New System.Drawing.Point(101, 163)
        Me.lblCompa.Name = "lblCompa"
        Me.lblCompa.Size = New System.Drawing.Size(56, 13)
        Me.lblCompa.TabIndex = 24
        Me.lblCompa.Text = "Vigencia"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(11, 163)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(84, 13)
        Me.BeLabel12.TabIndex = 23
        Me.BeLabel12.Text = "Compañia Aseg."
        '
        'lblCliente
        '
        Me.lblCliente.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCliente.AutoSize = True
        Me.lblCliente.BackColor = System.Drawing.Color.Transparent
        Me.lblCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente.ForeColor = System.Drawing.Color.Black
        Me.lblCliente.Location = New System.Drawing.Point(101, 136)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(56, 13)
        Me.lblCliente.TabIndex = 22
        Me.lblCliente.Text = "Vigencia"
        '
        'lblMoneda
        '
        Me.lblMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoneda.ForeColor = System.Drawing.Color.Black
        Me.lblMoneda.Location = New System.Drawing.Point(101, 82)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(56, 13)
        Me.lblMoneda.TabIndex = 21
        Me.lblMoneda.Text = "Vigencia"
        '
        'lblRamo
        '
        Me.lblRamo.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblRamo.AutoSize = True
        Me.lblRamo.BackColor = System.Drawing.Color.Transparent
        Me.lblRamo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRamo.ForeColor = System.Drawing.Color.Black
        Me.lblRamo.Location = New System.Drawing.Point(101, 108)
        Me.lblRamo.Name = "lblRamo"
        Me.lblRamo.Size = New System.Drawing.Size(56, 13)
        Me.lblRamo.TabIndex = 20
        Me.lblRamo.Text = "Vigencia"
        '
        'lblVigencia
        '
        Me.lblVigencia.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblVigencia.AutoSize = True
        Me.lblVigencia.BackColor = System.Drawing.Color.Transparent
        Me.lblVigencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVigencia.ForeColor = System.Drawing.Color.Black
        Me.lblVigencia.Location = New System.Drawing.Point(101, 48)
        Me.lblVigencia.Name = "lblVigencia"
        Me.lblVigencia.Size = New System.Drawing.Size(56, 13)
        Me.lblVigencia.TabIndex = 19
        Me.lblVigencia.Text = "Vigencia"
        '
        'lblNroPoliza
        '
        Me.lblNroPoliza.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblNroPoliza.AutoSize = True
        Me.lblNroPoliza.BackColor = System.Drawing.Color.Transparent
        Me.lblNroPoliza.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroPoliza.ForeColor = System.Drawing.Color.Black
        Me.lblNroPoliza.Location = New System.Drawing.Point(101, 21)
        Me.lblNroPoliza.Name = "lblNroPoliza"
        Me.lblNroPoliza.Size = New System.Drawing.Size(65, 13)
        Me.lblNroPoliza.TabIndex = 18
        Me.lblNroPoliza.Text = "Nro Poliza"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(11, 82)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel5.TabIndex = 17
        Me.BeLabel5.Text = "Moneda"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(11, 136)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(39, 13)
        Me.BeLabel4.TabIndex = 15
        Me.BeLabel4.Text = "Cliente"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(11, 108)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel3.TabIndex = 14
        Me.BeLabel3.Text = "Ramo"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(11, 48)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel2.TabIndex = 13
        Me.BeLabel2.Text = "Vigencia"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(11, 21)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel1.TabIndex = 12
        Me.BeLabel1.Text = "Nro Poliza"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboRamo)
        Me.GroupBox1.Controls.Add(Me.BeLabel6)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtPoliza)
        Me.GroupBox1.Controls.Add(Me.BeLabel24)
        Me.GroupBox1.Controls.Add(Me.gbOtros)
        Me.GroupBox1.Controls.Add(Me.gbConceptos)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(364, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(566, 207)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos de Endoso"
        '
        'cboRamo
        '
        Me.cboRamo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboRamo.BackColor = System.Drawing.Color.Ivory
        Me.cboRamo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRamo.ForeColor = System.Drawing.Color.Black
        Me.cboRamo.FormattingEnabled = True
        Me.cboRamo.KeyEnter = True
        Me.cboRamo.Location = New System.Drawing.Point(332, 55)
        Me.cboRamo.Name = "cboRamo"
        Me.cboRamo.Size = New System.Drawing.Size(214, 21)
        Me.cboRamo.TabIndex = 15
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(262, 58)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel6.TabIndex = 16
        Me.BeLabel6.Text = "Ramo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.mtbVigenciaFin)
        Me.GroupBox2.Controls.Add(Me.BeLabel22)
        Me.GroupBox2.Controls.Add(Me.mtbVigenciaIni)
        Me.GroupBox2.Controls.Add(Me.BeLabel23)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(256, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(302, 41)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Vigencia"
        '
        'mtbVigenciaFin
        '
        Me.mtbVigenciaFin.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbVigenciaFin.BackColor = System.Drawing.Color.Ivory
        Me.mtbVigenciaFin.BeepOnError = True
        Me.mtbVigenciaFin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbVigenciaFin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbVigenciaFin.HidePromptOnLeave = True
        Me.mtbVigenciaFin.KeyEnter = True
        Me.mtbVigenciaFin.Location = New System.Drawing.Point(224, 16)
        Me.mtbVigenciaFin.Mask = "00/00/0000"
        Me.mtbVigenciaFin.Name = "mtbVigenciaFin"
        Me.mtbVigenciaFin.Size = New System.Drawing.Size(66, 20)
        Me.mtbVigenciaFin.TabIndex = 1
        Me.mtbVigenciaFin.ValidatingType = GetType(Date)
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(146, 19)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel22.TabIndex = 11
        Me.BeLabel22.Text = "Fecha Termino"
        '
        'mtbVigenciaIni
        '
        Me.mtbVigenciaIni.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbVigenciaIni.BackColor = System.Drawing.Color.Ivory
        Me.mtbVigenciaIni.BeepOnError = True
        Me.mtbVigenciaIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbVigenciaIni.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbVigenciaIni.HidePromptOnLeave = True
        Me.mtbVigenciaIni.KeyEnter = True
        Me.mtbVigenciaIni.Location = New System.Drawing.Point(76, 15)
        Me.mtbVigenciaIni.Mask = "00/00/0000"
        Me.mtbVigenciaIni.Name = "mtbVigenciaIni"
        Me.mtbVigenciaIni.Size = New System.Drawing.Size(66, 20)
        Me.mtbVigenciaIni.TabIndex = 0
        Me.mtbVigenciaIni.ValidatingType = GetType(Date)
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(6, 18)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(65, 13)
        Me.BeLabel23.TabIndex = 8
        Me.BeLabel23.Text = "Fecha Inicio"
        '
        'txtPoliza
        '
        Me.txtPoliza.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPoliza.BackColor = System.Drawing.Color.Ivory
        Me.txtPoliza.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPoliza.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPoliza.ForeColor = System.Drawing.Color.Black
        Me.txtPoliza.KeyEnter = True
        Me.txtPoliza.Location = New System.Drawing.Point(79, 25)
        Me.txtPoliza.Name = "txtPoliza"
        Me.txtPoliza.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPoliza.ShortcutsEnabled = False
        Me.txtPoliza.Size = New System.Drawing.Size(162, 20)
        Me.txtPoliza.TabIndex = 12
        Me.txtPoliza.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel24
        '
        Me.BeLabel24.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel24.AutoSize = True
        Me.BeLabel24.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel24.ForeColor = System.Drawing.Color.Black
        Me.BeLabel24.Location = New System.Drawing.Point(18, 30)
        Me.BeLabel24.Name = "BeLabel24"
        Me.BeLabel24.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel24.TabIndex = 11
        Me.BeLabel24.Text = "Nro Endoso"
        '
        'gbOtros
        '
        Me.gbOtros.Controls.Add(Me.btngrabar)
        Me.gbOtros.Controls.Add(Me.mtbFechaLetraIni)
        Me.gbOtros.Controls.Add(Me.BeLabel18)
        Me.gbOtros.Controls.Add(Me.cboFormaPago)
        Me.gbOtros.Controls.Add(Me.txtLetras)
        Me.gbOtros.Controls.Add(Me.BeLabel19)
        Me.gbOtros.Controls.Add(Me.txtSumaAseguradora)
        Me.gbOtros.Controls.Add(Me.BeLabel20)
        Me.gbOtros.Controls.Add(Me.BeLabel21)
        Me.gbOtros.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtros.Location = New System.Drawing.Point(256, 78)
        Me.gbOtros.Name = "gbOtros"
        Me.gbOtros.Size = New System.Drawing.Size(302, 123)
        Me.gbOtros.TabIndex = 10
        Me.gbOtros.TabStop = False
        Me.gbOtros.Text = "Otros"
        '
        'btngrabar
        '
        Me.btngrabar.Location = New System.Drawing.Point(224, 91)
        Me.btngrabar.Name = "btngrabar"
        Me.btngrabar.Size = New System.Drawing.Size(66, 23)
        Me.btngrabar.TabIndex = 20
        Me.btngrabar.Text = "Grabar"
        Me.btngrabar.UseVisualStyleBackColor = True
        '
        'mtbFechaLetraIni
        '
        Me.mtbFechaLetraIni.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaLetraIni.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaLetraIni.BeepOnError = True
        Me.mtbFechaLetraIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbFechaLetraIni.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaLetraIni.HidePromptOnLeave = True
        Me.mtbFechaLetraIni.KeyEnter = True
        Me.mtbFechaLetraIni.Location = New System.Drawing.Point(153, 92)
        Me.mtbFechaLetraIni.Mask = "00/00/0000"
        Me.mtbFechaLetraIni.Name = "mtbFechaLetraIni"
        Me.mtbFechaLetraIni.Size = New System.Drawing.Size(66, 20)
        Me.mtbFechaLetraIni.TabIndex = 4
        Me.mtbFechaLetraIni.ValidatingType = GetType(Date)
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(16, 98)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(98, 13)
        Me.BeLabel18.TabIndex = 19
        Me.BeLabel18.Text = "Fecha Inicial Cuota"
        '
        'cboFormaPago
        '
        Me.cboFormaPago.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboFormaPago.BackColor = System.Drawing.Color.Ivory
        Me.cboFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormaPago.ForeColor = System.Drawing.Color.Black
        Me.cboFormaPago.FormattingEnabled = True
        Me.cboFormaPago.KeyEnter = True
        Me.cboFormaPago.Location = New System.Drawing.Point(153, 40)
        Me.cboFormaPago.Name = "cboFormaPago"
        Me.cboFormaPago.Size = New System.Drawing.Size(137, 21)
        Me.cboFormaPago.TabIndex = 2
        '
        'txtLetras
        '
        Me.txtLetras.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtLetras.BackColor = System.Drawing.Color.Ivory
        Me.txtLetras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLetras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLetras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLetras.ForeColor = System.Drawing.Color.Black
        Me.txtLetras.KeyEnter = True
        Me.txtLetras.Location = New System.Drawing.Point(153, 66)
        Me.txtLetras.Name = "txtLetras"
        Me.txtLetras.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtLetras.ShortcutsEnabled = False
        Me.txtLetras.Size = New System.Drawing.Size(137, 20)
        Me.txtLetras.TabIndex = 3
        Me.txtLetras.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLetras.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel19.ForeColor = System.Drawing.Color.Black
        Me.BeLabel19.Location = New System.Drawing.Point(15, 71)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel19.TabIndex = 13
        Me.BeLabel19.Text = "Nro. de Cuotas"
        '
        'txtSumaAseguradora
        '
        Me.txtSumaAseguradora.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSumaAseguradora.BackColor = System.Drawing.Color.Ivory
        Me.txtSumaAseguradora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSumaAseguradora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSumaAseguradora.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSumaAseguradora.ForeColor = System.Drawing.Color.Red
        Me.txtSumaAseguradora.KeyEnter = True
        Me.txtSumaAseguradora.Location = New System.Drawing.Point(153, 14)
        Me.txtSumaAseguradora.Name = "txtSumaAseguradora"
        Me.txtSumaAseguradora.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSumaAseguradora.ShortcutsEnabled = False
        Me.txtSumaAseguradora.Size = New System.Drawing.Size(137, 20)
        Me.txtSumaAseguradora.TabIndex = 1
        Me.txtSumaAseguradora.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSumaAseguradora.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.ForeColor = System.Drawing.Color.Black
        Me.BeLabel20.Location = New System.Drawing.Point(16, 45)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(79, 13)
        Me.BeLabel20.TabIndex = 11
        Me.BeLabel20.Text = "Forma de Pago"
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.ForeColor = System.Drawing.Color.Black
        Me.BeLabel21.Location = New System.Drawing.Point(16, 19)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(117, 13)
        Me.BeLabel21.TabIndex = 9
        Me.BeLabel21.Text = "Interes de Financiacion"
        '
        'gbConceptos
        '
        Me.gbConceptos.Controls.Add(Me.txtTotal)
        Me.gbConceptos.Controls.Add(Me.BeLabel13)
        Me.gbConceptos.Controls.Add(Me.txtIGV)
        Me.gbConceptos.Controls.Add(Me.txtInteres)
        Me.gbConceptos.Controls.Add(Me.txtDerechoEmision)
        Me.gbConceptos.Controls.Add(Me.txtPrima)
        Me.gbConceptos.Controls.Add(Me.BeLabel14)
        Me.gbConceptos.Controls.Add(Me.BeLabel15)
        Me.gbConceptos.Controls.Add(Me.BeLabel16)
        Me.gbConceptos.Controls.Add(Me.BeLabel17)
        Me.gbConceptos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbConceptos.Location = New System.Drawing.Point(6, 49)
        Me.gbConceptos.Name = "gbConceptos"
        Me.gbConceptos.Size = New System.Drawing.Size(244, 152)
        Me.gbConceptos.TabIndex = 9
        Me.gbConceptos.TabStop = False
        Me.gbConceptos.Text = "Conceptos"
        '
        'txtTotal
        '
        Me.txtTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotal.BackColor = System.Drawing.Color.Ivory
        Me.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.ForeColor = System.Drawing.Color.Black
        Me.txtTotal.KeyEnter = True
        Me.txtTotal.Location = New System.Drawing.Point(120, 121)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotal.ShortcutsEnabled = False
        Me.txtTotal.Size = New System.Drawing.Size(115, 20)
        Me.txtTotal.TabIndex = 4
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(12, 123)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel13.TabIndex = 13
        Me.BeLabel13.Text = "Total Importe"
        '
        'txtIGV
        '
        Me.txtIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIGV.BackColor = System.Drawing.Color.Ivory
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIGV.ForeColor = System.Drawing.Color.Black
        Me.txtIGV.KeyEnter = True
        Me.txtIGV.Location = New System.Drawing.Point(120, 94)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIGV.ShortcutsEnabled = False
        Me.txtIGV.Size = New System.Drawing.Size(115, 20)
        Me.txtIGV.TabIndex = 3
        Me.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtInteres
        '
        Me.txtInteres.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtInteres.BackColor = System.Drawing.Color.Ivory
        Me.txtInteres.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtInteres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtInteres.ForeColor = System.Drawing.Color.Black
        Me.txtInteres.KeyEnter = True
        Me.txtInteres.Location = New System.Drawing.Point(120, 68)
        Me.txtInteres.Name = "txtInteres"
        Me.txtInteres.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtInteres.ShortcutsEnabled = False
        Me.txtInteres.Size = New System.Drawing.Size(115, 20)
        Me.txtInteres.TabIndex = 2
        Me.txtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInteres.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtDerechoEmision
        '
        Me.txtDerechoEmision.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDerechoEmision.BackColor = System.Drawing.Color.Ivory
        Me.txtDerechoEmision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDerechoEmision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDerechoEmision.ForeColor = System.Drawing.Color.Black
        Me.txtDerechoEmision.KeyEnter = True
        Me.txtDerechoEmision.Location = New System.Drawing.Point(120, 42)
        Me.txtDerechoEmision.Name = "txtDerechoEmision"
        Me.txtDerechoEmision.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDerechoEmision.ShortcutsEnabled = False
        Me.txtDerechoEmision.Size = New System.Drawing.Size(115, 20)
        Me.txtDerechoEmision.TabIndex = 1
        Me.txtDerechoEmision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDerechoEmision.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtPrima
        '
        Me.txtPrima.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPrima.BackColor = System.Drawing.Color.Ivory
        Me.txtPrima.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPrima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrima.ForeColor = System.Drawing.Color.Black
        Me.txtPrima.KeyEnter = True
        Me.txtPrima.Location = New System.Drawing.Point(120, 17)
        Me.txtPrima.Name = "txtPrima"
        Me.txtPrima.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPrima.ShortcutsEnabled = False
        Me.txtPrima.Size = New System.Drawing.Size(115, 20)
        Me.txtPrima.TabIndex = 0
        Me.txtPrima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrima.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(12, 96)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel14.TabIndex = 11
        Me.BeLabel14.Text = "I.G.V."
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(12, 70)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(50, 13)
        Me.BeLabel15.TabIndex = 10
        Me.BeLabel15.Text = "Intereses"
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(12, 44)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(102, 13)
        Me.BeLabel16.TabIndex = 9
        Me.BeLabel16.Text = "Derecho de Emision"
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(12, 19)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(33, 13)
        Me.BeLabel17.TabIndex = 8
        Me.BeLabel17.Text = "Prima"
        '
        'Timer1
        '
        '
        'frmMantePolizaSub
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(936, 222)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gbVigencia)
        Me.MaximizeBox = False
        Me.Name = "frmMantePolizaSub"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.gbVigencia.ResumeLayout(False)
        Me.gbVigencia.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbOtros.ResumeLayout(False)
        Me.gbOtros.PerformLayout()
        Me.gbConceptos.ResumeLayout(False)
        Me.gbConceptos.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbVigencia As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblCliente As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblRamo As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblVigencia As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblNroPoliza As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblCompa As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents gbConceptos As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtInteres As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDerechoEmision As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPrima As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents gbOtros As System.Windows.Forms.GroupBox
    Friend WithEvents mtbFechaLetraIni As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboFormaPago As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtLetras As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSumaAseguradora As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents mtbVigenciaFin As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents mtbVigenciaIni As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtPoliza As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel24 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btngrabar As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents cboRamo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
End Class
