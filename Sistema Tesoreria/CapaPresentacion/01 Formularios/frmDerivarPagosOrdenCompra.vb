﻿Imports CapaNegocios
Imports CapaDatos
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports IWshRuntimeLibrary
Imports BusinessLogicLayer.Procesos
Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad
Imports CapaEntidad.Proceso
Imports CapaEntidad.Mantenimiento
Imports Telerik.WinControls.UI
Imports Telerik.WinControls

Public Class frmDerivarPagosOrdenCompra

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmDerivarPagosOrdenCompra = Nothing

    Public Shared Function Instance() As frmDerivarPagosOrdenCompra
        If frmInstance Is Nothing Then
            frmInstance = New frmDerivarPagosOrdenCompra
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmDerivarPagosOrdenCompra_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

#Region "Declaraciones"

    Dim VL_OCOCODIGO As String = ""
    Dim VL_PDOCODIGO As String = ""
    Dim VL_OCONUMERO As String = ""
    Dim VL_FECHAINICIO As String
    Dim VL_FECHAFIN As String
    Dim VL_EMP As String
    Dim VL_AREACODIGO As String = ""
    Dim V_ID_EntregaRendir As String
    Dim VL_ID_EntregaRendir As String
    Dim VL_ID_Estado As String
    Dim VL_ID_DocOrigen As String
    Dim VL_EnProceso As String
    Dim VL_Saldo As String
    Dim VL_FECHAINICIO_1 As Date
    Dim VL_FECHAFIN_1 As Date

    Public Shared VL_RUC As String
    Public Shared VL_EMPRESA As String
    Public Shared VL_DOCPENDIENTE As Decimal
    Public Shared VL_MONEDA As String
    Public Shared VL_NETOPAGAR As Decimal

    Dim objProgramacionPagos As BeanProgramacionPagos
    Dim objProgramacionPagosDetalle As BeanProgramacionPagosDetalle
    Dim objEntregaRendir As BeanEntregaRendir
    Dim objOrdenesCompra As BeanOrdenesCompra


    'ADMINISTRACION DERIVAR CARLOS TELLO
    Dim CM_OCOCODIGO As String = ""
    Dim CM_PDOCODIGO As String = ""
    Dim CM_PROVEEDOR As String = ""
    Dim CM_PRVRUC As String = ""
    Dim CM_OCONUMERO As String = ""

    Dim CM_DERI_ESTADO As String = ""



    Dim NRO_DOC_ORIGEN As String
    Dim ID_MONEDA
    Dim IMPORTE As String
    Dim ESTADO1 As Integer
    Dim ID_DOC_ORIGEN1 As String
    Dim SERIE_DOC_ORIGEN As String = "0"

    Dim VL_PARAM_ACTA As Integer
    Dim decIdOC As Decimal
    Dim VL_PARAM_ACTA_1 As Integer
    Dim ID_pd As Decimal


    ' Reporte pago
    Public Shared docOrigen As Decimal
    Public Shared id_DocPendiente As Decimal
    Public Shared opcion As Integer
    Public Shared nombreorigen As String
    Dim idUsuario As String
    Dim valor As String

#End Region

#Region "Procedimientos"


    Private Sub ListarVBValidacion(ByVal ID_DOCUMENTO As String, ByVal id_DocPendiente As String)

        Dim VL_SrvOrdenesCompra As SrvOrdenesCompra = New SrvOrdenesCompra()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Listar_Validacion(ID_DOCUMENTO, id_DocPendiente)


            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                valor = VL_BeanResultado.dtResultado.Rows(0).Item(0)

            Else

                MessageBox.Show("Usuario no puede dar VB°", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ListarIdUsuario()

        Dim VL_SrvOrdenesCompra As SrvOrdenesCompra = New SrvOrdenesCompra()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Listar_ID_USUARIO(gEmpresa, glbSisCodigo, gUsuario)


            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                idUsuario = VL_BeanResultado.dtResultado.Rows(0).Item(0)

            Else

                MessageBox.Show("Usuario no puede dar VB°", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ListarPlanillas()

        Dim VL_SrvOrdenesCompra As SrvOrdenesCompra = New SrvOrdenesCompra()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Listar_Planillas(VL_FECHAINICIO, VL_FECHAFIN, cboEmpresas.SelectedValue.ToString())
            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                dgvPlanillas.DataSource = VL_BeanResultado.dtResultado

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ListarCajaChicas()

        Dim VL_SrvOrdenesCompra As SrvOrdenesCompra = New SrvOrdenesCompra()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Listar_Caja_Chicas(VL_FECHAINICIO, VL_FECHAFIN, cboEmpresas.SelectedValue.ToString())
            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                dgvCajaChica.DataSource = VL_BeanResultado.dtResultado

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ListarConsultaVBCantidad(ByVal VL_OPCION As Integer, ByVal VL_ID_DOCPENDIENTE As Decimal, ByVal VL_ID_DOCUMENTO As Decimal)

        Dim VL_SrvVistoBueno As SrvVistoBueno = New SrvVistoBueno()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion

        Try
            VL_BeanResultado = VL_SrvVistoBueno.Fnc_Listar_Consulta_VB(VL_OPCION, VL_ID_DOCPENDIENTE, VL_ID_DOCUMENTO)
            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                txtCantidad.Text = VL_BeanResultado.dtResultado.Rows(0).Item(0)

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try
    End Sub
    Private Sub ListarConsultaVB(ByVal VL_OPCION As Integer, ByVal VL_ID_DOCPENDIENTE As Decimal, ByVal VL_ID_DOCUMENTO As Decimal)

        Dim VL_SrvVistoBueno As SrvVistoBueno = New SrvVistoBueno()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion

        Try
            VL_BeanResultado = VL_SrvVistoBueno.Fnc_Listar_Consulta_VB(VL_OPCION, VL_ID_DOCPENDIENTE, VL_ID_DOCUMENTO)
            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                dgvVistoBueno.DataSource = VL_BeanResultado.dtResultado

            Else

                'MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try
    End Sub
    Private Sub ListarConsultaVBDocumentos(ByVal VL_OPCION As Integer, ByVal VL_ID_DOCPENDIENTE As Decimal, ByVal VL_ID_DOCUMENTO As Decimal)

        Dim VL_SrvVistoBueno As SrvVistoBueno = New SrvVistoBueno()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion

        Try
            VL_BeanResultado = VL_SrvVistoBueno.Fnc_Listar_Consulta_VB(VL_OPCION, VL_ID_DOCPENDIENTE, VL_ID_DOCUMENTO)
            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                dgvDocumentos.DataSource = VL_BeanResultado.dtResultado

            Else

                'MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try
    End Sub
    Private Sub ListarValorizacion()

        Dim VL_SrvOrdenesCompra As SrvOrdenesCompra = New SrvOrdenesCompra()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Listar_Valorizacion(VL_FECHAINICIO, VL_FECHAFIN, cboEmpresas.SelectedValue.ToString())
            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                dgvValorizacion.DataSource = VL_BeanResultado.dtResultado

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try
    End Sub
    Private Sub ListarOrdenesDerivadas()

        Dim VL_SrvDerivar As SrvDerivar = New SrvDerivar()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvDerivar.Fnc_Listar_Ordenes_Derivadas(VL_FECHAINICIO, VL_FECHAFIN, cboEmpresas.SelectedValue.ToString())
            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                dgvLista.DataSource = VL_BeanResultado.dtResultado


            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ListarFacturas()
        Dim VL_SrvDerivar As SrvDerivar = New SrvDerivar()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion


        'VL_OCOCODIGO = dgvLista.CurrentRow.Cells("OCOCODIGO").Value
        'VL_PDOCODIGO = dgvLista.CurrentRow.Cells("PDOCODIGO").Value


        Try
            VL_BeanResultado = VL_SrvDerivar.Fnc_Listar_Facturas_derivadas(VL_OCOCODIGO, VL_PDOCODIGO)
            If VL_BeanResultado.blnExiste = True Then

                dgvFacturas.DataSource = VL_BeanResultado.dtResultado

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ListarBEmpresas()
        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_BEmpresas()

            If VL_BeanResultado.blnExiste = True Then
                cboEmpresas.DataSource = VL_BeanResultado.dtResultado
                cboEmpresas.DisplayMember = "EmprDescripcion"
                cboEmpresas.ValueMember = "EmprCodigo"
                'cboReferencia.Text = "Seleccione..."
                cboEmpresas.SelectedIndex = 5

            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub
    Private Sub FiltrarTodaEntregaRendir()

        Dim VL_SrvEnregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvEnregaRendir.Fnc_Listar_EntregaRendir_1(VL_FECHAINICIO_1, VL_FECHAFIN_1, cboEmpresas.SelectedValue.ToString())

            If VL_BeanResultado.blnExiste = True Then
                'dgvEntregaRendir.DataSource = VL_BeanResultado.dtResultado
                rgvER.DataSource = VL_BeanResultado.dtResultado

            Else
                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString())
        End Try
    End Sub

    Private Sub ListarArea()
        Dim VL_SrvDerivar As SrvDerivar = New SrvDerivar()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvDerivar.Fnc_Listar_Areas()
            If VL_BeanResultado.blnExiste = True Then

                ' cboArea.DataSource = VL_BeanResultado.dtResultado
                ' cboArea.DisplayMember = "AreaNombre"
                '  cboArea.ValueMember = "AreaCodigo"

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub RegistrarProgramacionPagosSoles()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        Dim Fecha = DateTime.Now
        Dim Id_moneda As String = "01"
        Dim total = txtTotalSoles.Text
        Dim estado = "1"

        objProgramacionPagos = New BeanProgramacionPagos(Fecha, Fecha, Id_moneda, total, estado)

        VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Registrar_Programacion_Pagos(objProgramacionPagos)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            VL_PARAM_ACTA = VL_BeanResultado.ParametroOutPut4
            decIdOC = Convert.ToDecimal(VL_PARAM_ACTA)


            If dgvProgramacionPagos.RowCount > 0 Then

                For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1

                    NRO_DOC_ORIGEN = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("PID_DocOrigen").Value)
                    Id_moneda = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("PMonCodigo").Value)
                    Dim IMPORTE1 As Decimal = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("TotalaPagar").Value)
                    ESTADO1 = 2
                    ID_DOC_ORIGEN1 = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("ID_DOC_ORIGEN").Value)
                    Dim ptotal As Decimal = dgvProgramacionPagos.Rows(i).Cells("PTotal").Value


                    If dgvProgramacionPagos.Rows(i).Cells("PMonCodigo").Value = "01" Then

                        Dim VL_SrvProgramacionPagosDetalle As New SrvProgramacionPagosDetalle
                        Dim VL_BeanResultado_1 As New BeanResultado.ResultadoTransaccion()

                        objProgramacionPagosDetalle = New BeanProgramacionPagosDetalle(decIdOC, ID_DOC_ORIGEN1, SERIE_DOC_ORIGEN, NRO_DOC_ORIGEN, Id_moneda, IMPORTE1, ESTADO1, ptotal)
                        VL_BeanResultado_1 = VL_SrvProgramacionPagosDetalle.Fnc_Registrar_Programacion_Pagos_Detalle(objProgramacionPagosDetalle)

                        If VL_BeanResultado_1.blnResultado = False Then
                            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
                            Return
                        Else

                            Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra
                            Dim VL_BeanResultado_2 As New BeanResultado.ResultadoTransaccion()
                            Dim VL_PARAM_ACTA_2 As Decimal = VL_BeanResultado_1.ParametroOutPut3
                            ID_pd = VL_PARAM_ACTA_2

                            If dgvProgramacionPagos.Rows(i).Cells("IdCorrelativo").Value <> Nothing Then
                                Dim id_factu As String = dgvProgramacionPagos.Rows(i).Cells("IdCorrelativo").Value

                                VL_BeanResultado_2 = VL_SrvOrdenesCompra.Fnc_Insertar_detalle_Factura(ID_pd, id_factu, IMPORTE1)
                                Return
                            End If
                        End If
                    End If

                Next

            End If

            ' MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub RegistrarProgramacionPagosDolares()

        Dim VL_SrvProgramacionPagos As New SrvProgramacionPagos()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        Dim Fecha = DateTime.Now
        Dim Id_moneda = "02"
        Dim total = txtDolares.Text
        Dim estado = "1"

        objProgramacionPagos = New BeanProgramacionPagos(Fecha, Fecha, Id_moneda, total, estado)

        VL_BeanResultado = VL_SrvProgramacionPagos.Fnc_Registrar_Programacion_Pagos(objProgramacionPagos)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            VL_PARAM_ACTA = VL_BeanResultado.ParametroOutPut4
            decIdOC = Convert.ToDecimal(VL_PARAM_ACTA)


            If dgvProgramacionPagos.RowCount > 0 Then

                For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1

                    NRO_DOC_ORIGEN = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("PID_DocOrigen").Value)
                    Id_moneda = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("PMonCodigo").Value)
                    Dim IMPORTE1 As Decimal = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("TotalaPagar").Value)
                    ESTADO1 = 2
                    ID_DOC_ORIGEN1 = Convert.ToString(dgvProgramacionPagos.Rows(i).Cells("ID_DOC_ORIGEN").Value)
                    Dim ptotal As Decimal = dgvProgramacionPagos.Rows(i).Cells("PTotal").Value


                    If dgvProgramacionPagos.Rows(i).Cells("PMonCodigo").Value = "02" Then

                        Dim VL_SrvProgramacionPagosDetalle As New SrvProgramacionPagosDetalle
                        Dim VL_BeanResultado_1 As New BeanResultado.ResultadoTransaccion()

                        objProgramacionPagosDetalle = New BeanProgramacionPagosDetalle(decIdOC, ID_DOC_ORIGEN1, SERIE_DOC_ORIGEN, NRO_DOC_ORIGEN, Id_moneda, IMPORTE1, ESTADO1, ptotal)
                        VL_BeanResultado_1 = VL_SrvProgramacionPagosDetalle.Fnc_Registrar_Programacion_Pagos_Detalle(objProgramacionPagosDetalle)

                        If VL_BeanResultado_1.blnResultado = False Then
                            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
                            Return
                        Else

                            Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra
                            Dim VL_BeanResultado_2 As New BeanResultado.ResultadoTransaccion()
                            VL_PARAM_ACTA_1 = VL_BeanResultado_1.ParametroOutPut3
                            ID_pd = VL_PARAM_ACTA_1

                            If dgvProgramacionPagos.Rows(i).Cells("IdCorrelativo").Value <> Nothing Then
                                Dim id_factu As String = dgvProgramacionPagos.Rows(i).Cells("IdCorrelativo").Value

                                VL_BeanResultado_2 = VL_SrvOrdenesCompra.Fnc_Insertar_detalle_Factura(Convert.ToString(ID_pd), id_factu, IMPORTE)
                                Return
                            End If
                        End If
                    End If

                Next

            End If

            ' MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub ActualizarER(ByVal id As Integer)

        Dim VL_SrvEntregaRendir As New SrvEntregaRendir()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim idmoneda As String = "1"
        'objProgramacionPagos = New BeanProgramacionPagos("2")
        VL_ID_EntregaRendir = dgvProgramacionPagos.Rows(id).Cells("NroER").Value
        VL_ID_Estado = "2"

        objEntregaRendir = New BeanEntregaRendir(VL_ID_EntregaRendir, VL_ID_Estado, idmoneda)

        VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Actualizar_ER(objEntregaRendir)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub ActualizarOC(ByVal id As Integer)

        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_OCOCODIGO = dgvLista.Rows(id).Cells("OCOCODIGO").Value
        VL_PDOCODIGO = dgvLista.Rows(id).Cells("PDOCODIGO").Value
        VL_OCONUMERO = dgvLista.Rows(id).Cells("OCONUMERO").Value
        VL_ID_Estado = "1"

        objOrdenesCompra = New BeanOrdenesCompra(VL_OCOCODIGO, VL_PDOCODIGO, VL_OCONUMERO, VL_ID_Estado)

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Actualizar_OC(objOrdenesCompra)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub Actualizard_Estado_DocPendientes(ByVal id As String, ByVal estado As String)

        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Actualizar_Estado_Doc_Pendientes(id, estado)
        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub
    Private Sub Actualizard_DocPendientes(ByVal id As String, ByVal enproceso As String, ByVal saldo As String)

        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim num As Decimal = 0

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Actualizar_Doc_Pendientes(Convert.ToDecimal(id), Convert.ToDecimal(enproceso), Convert.ToDecimal(saldo), num)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub InsertarVB(ByVal usuario As Decimal, ByVal docOrigen As Decimal, ByVal id_DocPendiente As Decimal)
        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Insertar_Visto_Bueno(usuario, docOrigen, id_DocPendiente)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "ASISTENTE DE SISTEMA", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
    End Sub

    Private Sub EliminarNotaCredito(ByVal VL_IDDOCPENDIENTE As Decimal)
        Dim VL_SrvNotaCredito As SrvNotaCredito = New SrvNotaCredito()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()



        VL_BeanResultado = VL_SrvNotaCredito.Fnc_Eliminar_NotaCredito(VL_IDDOCPENDIENTE)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
    End Sub
 
    Private Sub Actualizard_Revisado_DocPendientes(ByVal ID_DocPendiente As String, ByVal RevisadoDetraccion As String, ByVal RevisadoRetencion As String)

        Dim VL_SrvOrdenesCompra As New SrvOrdenesCompra()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SrvOrdenesCompra.Fnc_Actualizar_Revisado_Doc_Pendientes(ID_DocPendiente, RevisadoDetraccion, RevisadoRetencion)
        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

#End Region

    Private Sub frmDerivarPagosOrdenCompra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListarBEmpresas()
        dtpFin.Value = Date.Today
        dtpInicio.Value = Date.Today
        VL_FECHAINICIO = dtpInicio.Text
        VL_FECHAFIN = dtpFin.Text
        VL_FECHAINICIO_1 = Convert.ToDateTime(dtpInicio.Text)
        VL_FECHAFIN_1 = Convert.ToDateTime(dtpFin.Text)
        ' VL_EMP = cboEmpresas.SelectedValue.ToString()
        ' VL_AREACODIGO = cboArea.SelectedValue

        ListarOrdenesDerivadas()
        ListarArea()
        FiltrarTodaEntregaRendir()
        ListarCajaChicas()
        ListarValorizacion()
        ListarPlanillas()


    End Sub

    Private Function b(ByVal ER As String) As Boolean

        For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1
            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = ER Then
                Return True

            End If
        Next

        Return False

    End Function

    Private Function a(ByVal OC As String) As Boolean

        'Dim b As Integer = 0

        'For Each row As DataGridViewRow In dgvProgramacionPagos.Rows
        '    If dgvProgramacionPagos.Rows(b).Cells("NroER").Value = OC Then
        '        Return True

        '    End If
        '    b = b + 1
        'Next

        For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1
            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = OC Then
                Return True

            End If
        Next
        Return False
    End Function
    Private Function c(ByVal CC As String) As Boolean

        'Dim b As Integer = 0
        'For Each row As DataGridViewRow In dgvProgramacionPagos.Rows
        '    If dgvProgramacionPagos.Rows(b).Cells("NroER").Value = CC Then
        '        Return True

        '    End If
        '    b = b + 1
        'Next

        For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1
            If dgvProgramacionPagos.Rows(i).Cells("Id_DocPendiente").Value = CC Then
                Return True

            End If
        Next
        Return False
    End Function

    Private Function w(ByVal CC As String) As Boolean

        'Dim b As Integer = 0
        'For Each row As DataGridViewRow In dgvProgramacionPagos.Rows
        '    If dgvProgramacionPagos.Rows(b).Cells("NroER").Value = CC Then
        '        Return True

        '    End If
        '    b = b + 1
        'Next

        For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1
            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = CC Then
                Return True

            End If
        Next
        Return False
    End Function

    Private Function x(ByVal CC As String) As Boolean

        'Dim b As Integer = 0
        'For Each row As DataGridViewRow In dgvProgramacionPagos.Rows
        '    If dgvProgramacionPagos.Rows(b).Cells("NroER").Value = CC Then
        '        Return True

        '    End If
        '    b = b + 1
        'Next

        For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1
            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = CC And dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value = "5" Then
                Return True

            End If
        Next
        Return False
    End Function

    Private Sub sumatotalProgramacion()

        Dim soles As Decimal = 0D
        Dim dolares As Decimal = 0D

        For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1

            If dgvProgramacionPagos.Rows(i).Cells("PMonCodigo").Value = "01" Then
                soles += Convert.ToDecimal(dgvProgramacionPagos.Rows(i).Cells("TotalaPagar").Value)
            End If

            If dgvProgramacionPagos.Rows(i).Cells("PMonCodigo").Value = "02" Then
                dolares += Convert.ToDecimal(dgvProgramacionPagos.Rows(i).Cells("TotalaPagar").Value)
            End If

        Next
        '(Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value), "#,##0.00")
        txtTotalSoles.Text = Format(soles, "#,##0.00")
        txtDolares.Text = Format(dolares, "#,##0.00")
    End Sub

    Private Function calcularTotales() As Decimal

        Dim total As Decimal = dgvLista.CurrentRow.Cells("TOTAL").Value
        Dim totalF As Decimal = 0D
        Dim resulF As Decimal

        For i As Integer = 0 To dgvFacturas.RowCount - 1
            If dgvFacturas.Rows(i).Cells("Seleccion").Value = True Then
                totalF += Convert.ToDecimal(dgvFacturas.Rows(i).Cells("ImporteTotal").Value)
            End If

        Next

        If totalF <> total Then
            Return resulF = total - totalF

        Else

            Return Convert.ToDecimal(total)

        End If

    End Function

    Private Sub CalcularPercepcion(ByVal monto As Decimal, ByVal porcentaje As Decimal)
        Dim resultado As Decimal

        resultado = monto + (monto * porcentaje)
        Return

    End Sub

    Private Sub CacularDetraReten(ByVal monto As Decimal, ByVal porcentaje As Decimal)
        Dim resultado As Decimal

        resultado = monto - (monto * porcentaje)
    End Sub

    Private Sub btnDerivar_Click(sender As Object, e As EventArgs) Handles btnDerivar.Click

        Dim s As Integer = 0
        s = dgvFacturas.Rows.Count
        Dim b As Integer = 0
        For i As Integer = 0 To s - 1
            If dgvFacturas.Rows(i).Cells("Seleccion").Value = True Then
                b = b + 1
            End If
        Next

        If b > 0 Then

            If a(dgvLista.CurrentRow.Cells("OCONUMERO").Value) = False Then
                dgvProgramacionPagos.Rows.Add(dgvLista.CurrentRow.Cells("OCONUMERO").Value, dgvLista.CurrentRow.Cells("PRVRAZONSOCIAL").Value, dgvLista.CurrentRow.Cells("DERI_OBSERVACION").Value, _
                dgvLista.CurrentRow.Cells("MONEDA").Value, calcularTotales(), dgvLista.CurrentRow.Cells("OCOCODIGO").Value, dgvLista.CurrentRow.Cells("PDOCODIGO").Value, dgvLista.CurrentRow.Cells("MONEDA").Value, _
                "1", "1", "1")

                Dim fila = dgvLista.CurrentRow.Index
                'dgvLista.Rows(fila).DefaultCellStyle.BackColor = Color.Red

            Else
                MessageBox.Show("Ya se realizo la selección")
            End If
        Else
            MessageBox.Show("No selecciono Factura")

        End If

        sumatotalProgramacion()

        'For i As Integer = 0 To s - 1
        '    dgvFacturas.Rows(i).Cells("Seleccion").Value = False
        'Next

    End Sub

    Private Sub btnProgramar_Click(sender As Object, e As EventArgs) Handles btnProgramar.Click
        dgvProgramacionPagos.EndEdit()
        dgvFacturas.EndEdit()

      

        If dgvProgramacionPagos.RowCount - 1 = -1 Then
            MessageBox.Show("No existen documento para realizar la Programación de Pagos", "ERROR...!", MessageBoxButtons.OK)
        Else

            Dim yesno As Integer
            yesno = MessageBox.Show("¿Esta seguro que desea realizar estra programación?", "INFORMACION", MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If yesno = vbYes Then

                Dim falso As Boolean

                For i As Integer = 0 To dgvProgramacionPagos.RowCount - 1

                    Dim docOrigen As String = dgvProgramacionPagos.Rows(i).Cells("ID_DOC_ORIGEN").Value

                    If docOrigen = "2" Or docOrigen = "6" Then

                        For a As Integer = 0 To rgvER.RowCount - 1
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = rgvER.Rows(a).Cells("Nro").Value Then
                                rgvER.Rows(a).Cells("CODE").Value = False

                                Dim VL_docpendiente_ER As String = rgvER.Rows(a).Cells("ID_DocPendiente").Value
                                Dim VL_enproceso_ER As String = rgvER.Rows(a).Cells("EnProceso").Value
                                Dim VL_saldo_ER As Decimal = Convert.ToDecimal(rgvER.Rows(a).Cells("Saldo").Value) - Convert.ToDecimal(dgvProgramacionPagos.Rows(i).Cells("PTotal").Value)

                                Actualizard_DocPendientes(VL_docpendiente_ER, VL_enproceso_ER, VL_saldo_ER.ToString())

                                If rgvER.Rows(a).Cells("Saldo").Value = "0" Or rgvER.Rows(a).Cells("Saldo").Value = rgvER.Rows(a).Cells("Monto").Value Then
                                    'ActualizarER(a)
                                    'Dim VL_docpendiente_ER As String = rgvER.Rows(a).Cells("ID_DocPendiente").Value

                                    Actualizard_Estado_DocPendientes(VL_docpendiente_ER, "1")
                                    Actualizard_DocPendientes(VL_docpendiente_ER, "0", "0")

                                End If
                            End If
                            'If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = rgvER.Rows(a).Cells("Nro").Value Then

                            'End If
                        Next
                        falso = True
                    End If



                    If docOrigen = "1" Then

                        

                        For b As Integer = 0 To dgvLista.RowCount - 1

                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvLista.Rows(b).Cells("OCONUMERO").Value Then
                                dgvLista.Rows(b).Cells("CODE").Value = False
                                Dim VL_docpendiente_OC As String = dgvLista.Rows(b).Cells("ID_DocPendiente").Value
                                Dim VL_enproceso_OC As String = dgvProgramacionPagos.Rows(i).Cells("PTotal").Value
                                'dgvLista.Rows(b).Cells("enproceso").Value
                                Dim VL_saldo_OC As String = dgvLista.Rows(b).Cells("saldo").Value

                                Actualizard_DocPendientes(VL_docpendiente_OC, VL_enproceso_OC, VL_saldo_OC)
                            End If
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvLista.Rows(b).Cells("OCONUMERO").Value Then
                                If dgvLista.Rows(b).Cells("saldo").Value = "0" Or dgvLista.Rows(b).Cells("saldo").Value + dgvLista.Rows(b).Cells("Detraccion").Value + dgvLista.Rows(b).Cells("MRetenido").Value = dgvLista.Rows(b).Cells("TOTAL").Value Then
                                    'ActualizarOC(b)
                                    Dim VL_docpendiente_OC As String = dgvLista.Rows(b).Cells("ID_DocPendiente").Value
                                    Actualizard_Estado_DocPendientes(VL_docpendiente_OC, "1")
                                    Actualizard_DocPendientes(VL_docpendiente_OC, "0", "0")
                                End If

                            End If

                        Next
                        falso = True

                    End If


                    If docOrigen = "3" Then
                       

                        For c As Integer = 0 To dgvCajaChica.RowCount - 1
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvCajaChica.Rows(c).Cells("IdCaja").Value Then

                                dgvCajaChica.Rows(c).Cells("CODE").Value = False
                                Dim VL_docpendiente_CC As String = dgvCajaChica.Rows(c).Cells("ID_DocPendiente").Value
                                Dim VL_enproceso_CC As String = dgvCajaChica.Rows(c).Cells("EnProceso").Value
                                Dim VL_saldo_CC As String = dgvCajaChica.Rows(c).Cells("Saldo").Value - dgvProgramacionPagos.Rows(i).Cells("PTotal").Value

                                Actualizard_DocPendientes(VL_docpendiente_CC, VL_enproceso_CC, VL_saldo_CC)

                            End If
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvCajaChica.Rows(c).Cells("IdCaja").Value Then
                                If dgvCajaChica.Rows(c).Cells("Saldo").Value = "0" Or dgvCajaChica.Rows(c).Cells("Saldo").Value = dgvCajaChica.Rows(c).Cells("TOTAL").Value Then

                                    Dim VL_docpendiente_CC As String = dgvCajaChica.Rows(c).Cells("ID_DocPendiente").Value
                                    Actualizard_Estado_DocPendientes(VL_docpendiente_CC, "1")
                                    Actualizard_DocPendientes(VL_docpendiente_CC, "0", "0")

                                End If

                            End If
                        Next
                        falso = True

                    End If

                    If docOrigen = "4" Then
                       
                        For d As Integer = 0 To dgvValorizacion.RowCount - 1
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvValorizacion.Rows(d).Cells("IdValorizacion").Value Then

                                dgvValorizacion.Rows(d).Cells("CODE").Value = False
                                Dim VL_docpendiente_V As String = dgvValorizacion.Rows(d).Cells("ID_DocPendiente").Value
                                Dim VL_enproceso_V As String = dgvValorizacion.Rows(d).Cells("EnProceso").Value
                                Dim VL_saldo_V As String = dgvValorizacion.Rows(d).Cells("Saldo").Value

                                Actualizard_DocPendientes(VL_docpendiente_V, VL_enproceso_V, VL_saldo_V)

                            End If
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvValorizacion.Rows(d).Cells("IdValorizacion").Value Then
                                If dgvValorizacion.Rows(d).Cells("Saldo").Value = "0" Or dgvValorizacion.Rows(d).Cells("Saldo").Value + dgvValorizacion.Rows(d).Cells("RevisadoDetraccion").Value + dgvValorizacion.Rows(d).Cells("RevisadoRetencion").Value = dgvValorizacion.Rows(d).Cells("TotalVal").Value Then

                                    Dim VL_docpendiente_CC As String = dgvValorizacion.Rows(d).Cells("ID_DocPendiente").Value
                                    Actualizard_Estado_DocPendientes(VL_docpendiente_CC, "1")
                                    Actualizard_DocPendientes(VL_docpendiente_CC, "0", "0")

                                End If

                            End If
                        Next

                        falso = True
                    End If

                    If docOrigen = "5" Then
                       

                        For d As Integer = 0 To dgvPlanillas.RowCount - 1
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvPlanillas.Rows(d).Cells("ID_PAGO_PLANILLA").Value Then

                                dgvPlanillas.Rows(d).Cells("CODE").Value = False
                                Dim VL_docpendiente_V As String = dgvPlanillas.Rows(d).Cells("ID_DocPendiente").Value
                                Dim VL_enproceso_V As String = dgvPlanillas.Rows(d).Cells("EnProceso").Value
                                Dim VL_saldo_V As String = dgvPlanillas.Rows(d).Cells("Saldo").Value - dgvProgramacionPagos.Rows(i).Cells("PTotal").Value

                                Actualizard_DocPendientes(VL_docpendiente_V, VL_enproceso_V, VL_saldo_V)

                            End If
                            If dgvProgramacionPagos.Rows(i).Cells("NroER").Value = dgvPlanillas.Rows(d).Cells("ID_PAGO_PLANILLA").Value Then
                                If dgvPlanillas.Rows(d).Cells("Saldo").Value = "0" Or dgvPlanillas.Rows(d).Cells("Saldo").Value = dgvPlanillas.Rows(d).Cells("Total").Value Then

                                    Dim VL_docpendiente_CC As String = dgvPlanillas.Rows(d).Cells("ID_DocPendiente").Value
                                    Actualizard_Estado_DocPendientes(VL_docpendiente_CC, "1")
                                    Actualizard_DocPendientes(VL_docpendiente_CC, "0", "0")

                                End If

                            End If
                        Next
                        falso = True

                    End If

                Next


                If falso = True Then

                    If txtDolares.Text > 0 Then
                        RegistrarProgramacionPagosDolares()

                    End If

                    If txtTotalSoles.Text > 0 Then
                        RegistrarProgramacionPagosSoles()

                    End If

                    MessageBox.Show("Se realizó operación Exitosamente!")


                    'ELIMINA TODAS LAS FILAS DE LA GRILLA PROGRAMACION DE PAGO
                    If dgvProgramacionPagos.RowCount > 0 Then

                        For i As Integer = dgvProgramacionPagos.RowCount - 1 To 0 Step -1
                            dgvProgramacionPagos.Rows.RemoveAt(i)

                        Next
                    End If
                    '---------------------------------------------------------

                    FiltrarTodaEntregaRendir()
                    ListarOrdenesDerivadas()
                    ListarCajaChicas()
                    ListarPlanillas()
                    ListarValorizacion()

                    txtTotalSoles.Text = "0"
                    txtDolares.Text = "0"

                End If


            Else

            End If

        End If
       

    End Sub

    Private Sub tbValorizado_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tbValorizado.SelectedIndexChanged


        If tbValorizado.SelectedIndex <> 0 Then
            gbFacturas.Visible = False
        End If
        'ELIMINA TODAS LAS FILAS DE LA GRILLA PROGRAMACION DE PAGO
        If dgvFacturas.RowCount > 0 Then

            For i As Integer = dgvFacturas.RowCount - 1 To 0 Step -1
                dgvFacturas.Rows.RemoveAt(i)

            Next
        End If
        '---------------------------------------------------------


    End Sub

#Region "ORDEN DE COMPRAS"

    Private Sub dgvLista_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)

        Dim s As Integer = 0
        s = dgvFacturas.Rows.Count

        'For i As Integer = 0 To s - 1
        '    'dgvFacturas.CurrentRow.Cells("Seleccion").Value
        '    If dgvFacturas.Rows(i).Cells("Seleccion").Value = False Then
        '        MessageBox.Show("No selecciono Factura")
        '    End If
        'Next

        If s = 0 Then

            If a(dgvLista.CurrentRow.Cells("OCONUMERO").Value) = False Then

                dgvProgramacionPagos.Rows.Add(dgvLista.CurrentRow.Cells("OCONUMERO").Value, dgvLista.CurrentRow.Cells("PRVRAZONSOCIAL").Value, dgvLista.CurrentRow.Cells("DERI_OBSERVACION").Value, _
                dgvLista.CurrentRow.Cells("MONEDA").Value, dgvLista.CurrentRow.Cells("saldo").Value, dgvLista.CurrentRow.Cells("OCOCODIGO").Value, dgvLista.CurrentRow.Cells("PDOCODIGO").Value, dgvLista.CurrentRow.Cells("OCMonCodigo").Value, _
                "1", "1")

                Dim fila = dgvLista.CurrentRow.Index
                'dgvLista.Rows(fila).DefaultCellStyle.BackColor = Color.Red

                'Else
                '    MessageBox.Show("Ya se realizo la selección")
            End If


        Else
            MessageBox.Show("OC tiene Facturas Asociadas")

        End If

        sumatotalProgramacion()

    End Sub

    Function calcularMontoPorcentaje(ByVal monto As Decimal)
        Dim j As Integer
        Dim retorno As Decimal
        Dim idfactura As String = dgvProgramacionPagos.CurrentRow.Cells("IdCorrelativo").Value
        'For i = 0 To dgvProgramacionPagos.RowCount - 1
        For j = 0 To dgvLista.RowCount - 1
            If dgvProgramacionPagos.CurrentRow.Cells("PID_DocOrigen").Value = dgvLista.Rows(j).Cells("ID_DocOrigen").Value Then
                Dim porcentaje As Decimal = Convert.ToDecimal(monto * Convert.ToDecimal(dgvLista.Rows(j).Cells("PorcentajeRetDetPer").Value))
                retorno = monto - porcentaje
                Return retorno

            End If

        Next

        Return retorno = "0.00"

    End Function

    Private Sub rgvOC_CellDoubleClick(sender As Object, e As GridViewCellEventArgs) Handles dgvLista.CellDoubleClick
        Try

            dgvLista.EndEdit()

            Dim s As Integer = 0
            s = dgvFacturas.Rows.Count

            If dgvLista.CurrentRow.Cells("REVISION").Value = "SI" Then
                dgvLista.CurrentRow.Cells("CODE").Value = True

            End If

            ' '' ''For i As Integer = 0 To s - 1
            ' '' ''    'dgvFacturas.CurrentRow.Cells("Seleccion").Value
            ' '' ''    If dgvFacturas.Rows(i).Cells("Seleccion").Value = False Then
            ' '' ''        MessageBox.Show("No selecciono Factura")
            ' '' ''    End If


            '' '' ''Next


            ' '' ''If s = 0 Then
            Dim id_docpendiente As String = dgvLista.CurrentRow.Cells("ID_DocPendiente").Value

            If dgvLista.CurrentRow.Cells("REVISION").Value = "SI" Then



                If dgvLista.CurrentRow.Cells("EnProceso").Value <> "0.00" Then
                    MessageBox.Show("'Orden de Compra' tiene un pago pendiente asociado, por favor eliminar dicha pago pendiente o realizar el pago del documento", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'falso = False
                Else
                    If c(id_docpendiente) = False Then

                        Dim OCONUMERO = dgvLista.CurrentRow.Cells("OCONUMERO").Value
                        Dim PRVRAZONSOCIAL = dgvLista.CurrentRow.Cells("PRVRAZONSOCIAL").Value
                        Dim DERI_OBSERVACION = dgvLista.CurrentRow.Cells("DERI_OBSERVACION").Value
                        Dim MONEDA = dgvLista.CurrentRow.Cells("MONEDA").Value
                        Dim TOTAL = dgvLista.CurrentRow.Cells("TOTAL").Value
                        Dim OCOCODIGO = dgvLista.CurrentRow.Cells("OCOCODIGO").Value
                        Dim PDOCODIGO = dgvLista.CurrentRow.Cells("PDOCODIGO").Value
                        Dim MonCodigo = dgvLista.CurrentRow.Cells("MonCodigo").Value
                        Dim SALDO = dgvLista.CurrentRow.Cells("saldo").Value
                        Dim ID_DocOrigen As String = dgvLista.CurrentRow.Cells("ID_DocOrigen").Value
                        Dim detraccion As String = dgvLista.CurrentRow.Cells("Detraccion").Value
                        Dim retencion As String = dgvLista.CurrentRow.Cells("MRetenido").Value
                        Dim percepcion As String = dgvLista.CurrentRow.Cells("ImportePercepcion").Value


                        dgvProgramacionPagos.Rows.Add(OCONUMERO, PRVRAZONSOCIAL, DERI_OBSERVACION, _
                        MONEDA, TOTAL, OCOCODIGO, PDOCODIGO, MonCodigo, _
                        "1", "1", ID_DocOrigen, "", IIf(SALDO = TOTAL Or SALDO = TOTAL - detraccion, detraccion, "0.00"), retencion, percepcion, "0.00", IIf(SALDO = TOTAL, SALDO - detraccion - retencion + percepcion, SALDO), id_docpendiente)

                        Dim fila = dgvLista.CurrentRow.Index
                        '                dgvLista.Rows(fila).DefaultCellStyle.BackColor = Color.Red

                    Else
                        MessageBox.Show("Ya se realizo la selección")
                    End If

                    sumatotalProgramacion()

                End If

               
            Else
                MessageBox.Show("Orden de Compra no fue revisado")
            End If

        Catch ex As Exception

        End Try


    End Sub

    Private Sub dgvFacturas_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvFacturas.CellDoubleClick
        dgvFacturas.EndEdit()

        VL_OCOCODIGO = dgvLista.CurrentRow.Cells("OCOCODIGO").Value.ToString()
        VL_PDOCODIGO = dgvLista.CurrentRow.Cells("PDOCODIGO").Value.ToString()
        Dim F_ococodigo As String = dgvFacturas.CurrentRow.Cells("IdOC").Value
        Dim F_pdocodigo As String = dgvFacturas.CurrentRow.Cells("PdoCodigoOC").Value

        Dim OCONUMERO = dgvLista.CurrentRow.Cells("OCONUMERO").Value
        Dim PRVRAZONSOCIAL = dgvLista.CurrentRow.Cells("PRVRAZONSOCIAL").Value
        Dim DERI_OBSERVACION = dgvLista.CurrentRow.Cells("DERI_OBSERVACION").Value
        Dim MONEDA = dgvLista.CurrentRow.Cells("MONEDA").Value
        Dim TOTAL = dgvFacturas.CurrentRow.Cells("ImporteTotal").Value
        Dim MonCodigo = dgvLista.CurrentRow.Cells("MonCodigo").Value
        Dim SALDO = dgvLista.CurrentRow.Cells("saldo").Value
        Dim porReDet = IIf(dgvLista.CurrentRow.Cells("PorcentajeRetDet").Value.ToString() <> Nothing, dgvLista.CurrentRow.Cells("PorcentajeRetDet").Value, "0")
        Dim Totalimporte = dgvFacturas.CurrentRow.Cells("ImporteTotal").Value
        Dim ID_DocOrigen As String = dgvLista.CurrentRow.Cells("ID_DocOrigen").Value
        Dim IdCorrelativo As String = dgvFacturas.CurrentRow.Cells("IdCorrelativo").Value
        Dim detraccion As String = SALDO * porReDet
        Dim detraccionF As String = Totalimporte * porReDet
        Dim retencion As String = SALDO * porReDet
        Dim retencionF As String = Totalimporte * porReDet
        Dim percepcion As String = dgvLista.CurrentRow.Cells("ImportePercepcion").Value
        Dim importeA As String = dgvFacturas.CurrentRow.Cells("Saldo").Value


        If F_ococodigo = VL_OCOCODIGO And F_pdocodigo = VL_PDOCODIGO Then
            If dgvLista.CurrentRow.Cells("REVISION").Value = "SI" Then
                dgvLista.CurrentRow.Cells("CODE").Value = True

                If TOTAL = dgvLista.CurrentRow.Cells("TOTAL").Value Then
                    dgvProgramacionPagos.Rows.Add(OCONUMERO, PRVRAZONSOCIAL, DERI_OBSERVACION, _
              MONEDA, IIf(dgvFacturas.CurrentRow.Cells("Saldo").Value <> "0.00", importeA, Totalimporte), VL_OCOCODIGO, VL_PDOCODIGO, MonCodigo, _
              "1", "1", ID_DocOrigen, IdCorrelativo, IIf(dgvLista.CurrentRow.Cells("Detraccion").Value > 0, detraccion, "0.00"), IIf(dgvLista.CurrentRow.Cells("MRetenido").Value <> "0.00", retencion, "0.00"), percepcion, (SALDO - detraccion - retencion) + percepcion)
                Else
                    Dim montoPer As String = SALDO * (dgvLista.CurrentRow.Cells("PorcentajePercepcion").Value + 1)
                    Dim motoDetraRetra As String = SALDO * dgvLista.CurrentRow.Cells("PorcentajeRetDet").Value

                    dgvProgramacionPagos.Rows.Add(OCONUMERO, PRVRAZONSOCIAL, DERI_OBSERVACION, _
              MONEDA, TOTAL, VL_OCOCODIGO, VL_PDOCODIGO, MonCodigo, _
              "1", "1", ID_DocOrigen, IdCorrelativo, IIf(dgvLista.CurrentRow.Cells("Detraccion").Value > 0, detraccionF, "0.00"), IIf(dgvLista.CurrentRow.Cells("MRetenido").Value <> "0.00", retencionF, "0.00"), percepcion, montoPer - motoDetraRetra)
                End If


                'dgvLista.CurrentRow.Cells("saldo").Value = SALDO - dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value
                'dgvLista.CurrentRow.Cells("").Value = 
                'dgvLista.CurrentRow.Cells("enproceso").Value = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value

                sumatotalProgramacion()

            Else
                MessageBox.Show("Orden de Compra no fue revisado")
            End If

        End If

    End Sub

    Private Sub dgvLista_CellClick(sender As Object, e As GridViewCellEventArgs) Handles dgvLista.CellClick
        dgvFacturas.EndEdit()
        dgvProgramacionPagos.EndEdit()

        Dim i As Integer
        Dim j As Integer
        Try

            If e.RowIndex >= 0 Then
                gbFacturas.Visible = True
                VL_OCOCODIGO = dgvLista.CurrentRow.Cells("OCOCODIGO").Value.ToString()
                VL_PDOCODIGO = dgvLista.CurrentRow.Cells("PDOCODIGO").Value.ToString()
                Dim VL_DOCPENDIENTE As Decimal = dgvLista.CurrentRow.Cells("ID_DocPendiente").Value.ToString()
                Dim VL_IDDOC As Decimal = dgvLista.CurrentRow.Cells("DocOrigen").Value.ToString()
                ListarFacturas()
                ListarConsultaVBCantidad(1, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVBDocumentos(3, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVB(2, VL_DOCPENDIENTE, VL_IDDOC)


                For i = 0 To dgvProgramacionPagos.RowCount - 1
                    If dgvProgramacionPagos.Rows(i).Cells("IdCorrelativo").Value <> Nothing Then
                        For j = 0 To dgvFacturas.RowCount - 1

                            Dim P_id As String = dgvProgramacionPagos.Rows(i).Cells("IdCorrelativo").Value.ToString()
                            Dim F_id As String = dgvFacturas.Rows(j).Cells("IdCorrelativo").Value.ToString()

                            If P_id = F_id Then

                                Dim PTotal As Decimal = dgvProgramacionPagos.Rows(i).Cells("PTotal").Value
                                Dim importe As Decimal = dgvFacturas.Rows(j).Cells("ImporteTotal").Value
                                dgvFacturas.Rows(j).Cells("saldo").Value = importe - PTotal

                            End If
                        Next
                    End If

                Next
                If dgvFacturas.RowCount = Nothing Then

                    gbFacturas.Visible = False
                End If


            End If
        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region "ENTREGA A RENDIR"
    Private Sub rgvER_CellDoubleClick(sender As Object, e As GridViewCellEventArgs) Handles rgvER.CellDoubleClick

        Dim OCOCODIGO As String = "0"
        Dim PDOCODIGO As String = "0"
        Dim Nro = rgvER.CurrentRow.Cells("Nro").Value.ToString
        Dim Solicitante = rgvER.CurrentRow.Cells("Solicitante").Value.ToString
        Dim Concepto = rgvER.CurrentRow.Cells("Concepto").Value.ToString
        Dim MonDescrpcion = rgvER.CurrentRow.Cells("MonDescripcion").Value
        Dim monto = rgvER.CurrentRow.Cells("Saldo").Value.ToString
        Dim MonCodigo = rgvER.CurrentRow.Cells("MonCodigo").Value.ToString
        Dim Id_estado = rgvER.CurrentRow.Cells("ID_ESTADO").Value.ToString
        Dim ID_DocOrigen As String = rgvER.CurrentRow.Cells("ID_DocOrigen").Value
        Dim DocOrigen As String = rgvER.CurrentRow.Cells("DocOrigen").Value
        Dim id_docpendiente As String = rgvER.CurrentRow.Cells("ID_DocPendiente").Value

        If rgvER.CurrentRow.Cells("REVISION").Value = "SI" Then

            If rgvER.CurrentRow.Cells("EnProceso").Value <> "0.00" Then
                MessageBox.Show("'Entrega a Rendir' tiene un pago pendiente asociado, por favor eliminar dicha pago pendiente o realizar el pago del documento", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                'falso = False
            Else

                rgvER.CurrentRow.Cells("CODE").Value = True

                If c(id_docpendiente) = False Then

                    dgvProgramacionPagos.Rows.Add(Nro, Solicitante, Concepto, _
                                                  MonDescrpcion, monto, OCOCODIGO, PDOCODIGO, MonCodigo, Id_estado, DocOrigen, ID_DocOrigen, "", "0.00", "0.00", "0.00", "0.00", monto, id_docpendiente)
                    'Dim fila = rgvER.CurrentRow.Index
                    'rgvER.Rows(fila).DefaultCellStyle.BackColor = Color.Red
                    'rgvER.Rows(fila).
                Else
                    MessageBox.Show("La OC o la ER ya fue Programada")
                End If

                sumatotalProgramacion()

            End If

           
        Else
            MessageBox.Show("Entrega a Rendir no fue revisada")
        End If

    End Sub
#End Region

#Region "VALORIZACION"
    Private Sub dgvValorizacion_CellDoubleClick(sender As Object, e As GridViewCellEventArgs) Handles dgvValorizacion.CellDoubleClick


        Dim OCOCODIGO As String = "0"
        Dim PDOCODIGO As String = "0"
        Dim IdValorizacion = dgvValorizacion.CurrentRow.Cells("IdValorizacion").Value.ToString
        Dim ParDescripcion = dgvValorizacion.CurrentRow.Cells("ParDescripcion").Value.ToString
        Dim Concepto = dgvValorizacion.CurrentRow.Cells("Concepto").Value.ToString
        Dim MonDescripcion = dgvValorizacion.CurrentRow.Cells("MonDescripcion").Value
        Dim TotalVal = dgvValorizacion.CurrentRow.Cells("TotalVal").Value.ToString
        Dim MonCodigo = dgvValorizacion.CurrentRow.Cells("MonCodigo").Value.ToString
        Dim Estado = dgvValorizacion.CurrentRow.Cells("Estado").Value.ToString
        Dim ID_DocOrigen As String = dgvValorizacion.CurrentRow.Cells("ID_DocOrigen").Value
        Dim saldo = dgvValorizacion.CurrentRow.Cells("Saldo").Value.ToString
        Dim detraccion = dgvValorizacion.CurrentRow.Cells("RevisadoDetraccion").Value.ToString
        Dim retencion = dgvValorizacion.CurrentRow.Cells("RevisadoRetencion").Value.ToString
        Dim ID_DocPendiente = dgvValorizacion.CurrentRow.Cells("ID_DocPendiente").Value.ToString


        If dgvValorizacion.CurrentRow.Cells("REVISION").Value = "SI" Then

            If dgvValorizacion.CurrentRow.Cells("EnProceso").Value <> "0.00" Then
                MessageBox.Show("'Valorizacion' tiene un pago pendiente asociado, por favor eliminar dicha pago pendiente o realizar el pago del documento", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                'falso = False
            Else
                dgvValorizacion.CurrentRow.Cells("CODE").Value = True

                If c(ID_DocPendiente) = False Then

                    dgvProgramacionPagos.Rows.Add(IdValorizacion, ParDescripcion, Concepto, _
                                                  MonDescripcion, TotalVal, OCOCODIGO, PDOCODIGO, MonCodigo, "1", "4", ID_DocOrigen, "", IIf(saldo = TotalVal Or saldo = TotalVal - detraccion, detraccion, "0.00"), retencion, "0.00", "0.00", IIf(saldo = TotalVal, saldo - detraccion - retencion, saldo), ID_DocPendiente)
                    'Dim fila = rgvER.CurrentRow.Index
                    'rgvER.Rows(fila).DefaultCellStyle.BackColor = Color.Red
                    'rgvER.Rows(fila).
                Else
                    MessageBox.Show("La Valorización ya fue Programada")
                End If

                sumatotalProgramacion()

            End If

        Else
            MessageBox.Show("Valorizacion no fue revisada")
        End If


    End Sub
#End Region

#Region "CAJA CHICA"
    Private Sub dgvCajaChica_CellDoubleClick(sender As Object, e As GridViewCellEventArgs) Handles dgvCajaChica.CellDoubleClick
        Dim OCOCODIGO As String = "0"
        Dim PDOCODIGO As String = "0"
        Dim Nro = dgvCajaChica.CurrentRow.Cells("IdCaja").Value.ToString
        Dim Solicitante = dgvCajaChica.CurrentRow.Cells("NomResp").Value.ToString
        Dim Concepto = dgvCajaChica.CurrentRow.Cells("DescripcionCaja").Value.ToString
        Dim MonDescrpcion = dgvCajaChica.CurrentRow.Cells("MonDescripcion").Value
        Dim monto = dgvCajaChica.CurrentRow.Cells("TOTAL").Value.ToString
        Dim MonCodigo = dgvCajaChica.CurrentRow.Cells("IdMoneda").Value.ToString
        Dim CORRELATIVO As String = dgvCajaChica.CurrentRow.Cells("IdCorrelativoCajaChica").Value
        Dim ID_DocPendiente As String = dgvCajaChica.CurrentRow.Cells("ID_DocPendiente").Value


        If dgvCajaChica.CurrentRow.Cells("REVISION").Value = "SI" Then


            If dgvCajaChica.CurrentRow.Cells("EnProceso").Value <> "0.00" Then
                MessageBox.Show("'Caja Chica' tiene un pago pendiente asociado, por favor eliminar dicha pago pendiente o realizar el pago del documento", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                'falso = False
            Else
                dgvCajaChica.CurrentRow.Cells("CODE").Value = True

                If c(ID_DocPendiente) = False Then

                    dgvProgramacionPagos.Rows.Add(Nro, Solicitante, Concepto, _
                                                  MonDescrpcion, monto, OCOCODIGO, PDOCODIGO, MonCodigo, "1", "3", CORRELATIVO, "", "0.00", "0.00", "0.00", "0.00", monto, ID_DocPendiente)
                    'Dim fila = rgvER.CurrentRow.Index
                    'rgvER.Rows(fila).DefaultCellStyle.BackColor = Color.Red
                    'rgvER.Rows(fila).
                Else
                    MessageBox.Show("Caja chica ya fue Programada")
                End If

                sumatotalProgramacion()
            End If
            
        Else
            MessageBox.Show("Caja chica no fue revisada")
        End If

    End Sub
#End Region

#Region "PLANILLAS"

    Private Sub dgvPlanillas_CellDoubleClick(sender As Object, e As GridViewCellEventArgs) Handles dgvPlanillas.CellDoubleClick

        Dim OCOCODIGO As String = "0"
        Dim PDOCODIGO As String = "0"
        Dim Nro = dgvPlanillas.CurrentRow.Cells("ID_PAGO_PLANILLA").Value.ToString
        Dim Solicitante = dgvPlanillas.CurrentRow.Cells("EmprDescripcion").Value.ToString
        Dim Concepto = dgvPlanillas.CurrentRow.Cells("CONCEPTO").Value.ToString
        Dim MonDescrpcion = dgvPlanillas.CurrentRow.Cells("MonDescripcion").Value
        Dim monto = dgvPlanillas.CurrentRow.Cells("Total").Value.ToString
        Dim MonCodigo = dgvPlanillas.CurrentRow.Cells("MonCodigo").Value.ToString
        Dim CORRELATIVO As String = dgvPlanillas.CurrentRow.Cells("ID_PAGO_PLANILLA").Value
        Dim ID_DocPendiente As String = dgvPlanillas.CurrentRow.Cells("ID_DocPendiente").Value

        If dgvPlanillas.CurrentRow.Cells("REVISION").Value = "SI" Then

            If dgvPlanillas.CurrentRow.Cells("EnProceso").Value <> "0.00" Then
                MessageBox.Show("'Valorizacion' tiene un pago pendiente asociado, por favor eliminar dicha pago pendiente o realizar el pago del documento", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                'falso = False
            Else

                dgvPlanillas.CurrentRow.Cells("CODE").Value = True

                If c(ID_DocPendiente) = False Then

                    dgvProgramacionPagos.Rows.Add(Nro, Solicitante, Concepto, _
                                                  MonDescrpcion, monto, OCOCODIGO, PDOCODIGO, MonCodigo, "1", "5", CORRELATIVO, "", "0.00", "0.00", "0.00", "0.00", monto, ID_DocPendiente)
                    'Dim fila = rgvER.CurrentRow.Index
                    'rgvER.Rows(fila).DefaultCellStyle.BackColor = Color.Red
                    'rgvER.Rows(fila).
                Else
                    MessageBox.Show("Planilla ya fue Programada")
                End If

                sumatotalProgramacion()

            End If

            
        Else
            MessageBox.Show("Planilla no fue revisada")
        End If

    End Sub

#End Region

#Region "PROGRAMACION DE PAGOS"
    Private Sub RadGridView2_CellValueChanged(sender As Object, e As GridViewCellEventArgs) Handles dgvProgramacionPagos.CellValueChanged
        dgvProgramacionPagos.EndEdit()
        'verifica si existe algun datos en la grilla "dgvProgramacionPagos" si no existe no se ejecuta
        Dim i As Integer
        Dim f As Integer
        Dim valor As Decimal = "1.00"


        If dgvProgramacionPagos.Columns(e.ColumnIndex).Name = "PTotal" Then

            'CAPTURA LA MODIFICACION SOBRE LA COLUMNA "PTotal" DE LA GRILLA dgvProgramacionPagos
            Dim VL_NUMERO As Decimal = Convert.ToDecimal(dgvProgramacionPagos.Rows(e.RowIndex).Cells("PTotal").Value)
            'variables  VL_NUMERO para poder capturar la modificacion de la columna "PTotal", variable VL_ID captura indice de la grilla dgvprogramacion de pagos
            Dim VL_ID As Decimal = dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value
            Dim id_programacion As Decimal = dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value
            'VL_NUMERO = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value.ToString()
            Dim C As Decimal = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value)
            Dim docOrigen As String = dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value


            If docOrigen = "1" Then
                'FOR RECORRE LA GRILLA dgvLista PARA PODER VERIFICAR QUE LA FILA MODIFICADA EN LA GRILLA DE dgvProgramacionPagos SEA IGUAL A LA GRILLA dgvLista
                For i = 0 To dgvLista.RowCount - 1
                    'SI LOS INDICES DE LAS 2 GRILLAS SON IGUALES 
                    If dgvLista.Rows(i).Cells("ID_DocPendiente").Value = id_programacion Then
                        'EDITA LA COLUMNA "enproceso" POR EL VALOR QUE ESTA EN LA VARIABLE VL_NUMERO-+
                        'dgvLista.Rows(i).Cells("enproceso").Value = VL_NUMERO
                        'REALIZA EL CALCULO DEL SALDO = TOTAL - ENPROCESO

                        Dim A As Decimal = IIf(dgvLista.Rows(i).Cells("saldo2").Value <> dgvLista.Rows(i).Cells("TOTAL").Value, Convert.ToDecimal(dgvLista.Rows(i).Cells("saldo2").Value), Convert.ToDecimal(dgvLista.Rows(i).Cells("saldo2").Value) - Convert.ToDecimal(dgvLista.Rows(i).Cells("Detraccion").Value))
                        Dim B As Decimal = Convert.ToDecimal(dgvLista.Rows(i).Cells("enproceso").Value)


                        Dim VL_NUMERO2 As Decimal = A - dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value
                        'Dim VL_NUMERO2 As Decimal = (A + B) - C
                        'ASIGA EL VALOR EN LA COLUMNA SALDO DE LA GRILLA dgvLista 
                        dgvLista.Rows(i).Cells("SALDO").Value = VL_NUMERO2
                        dgvProgramacionPagos.CurrentRow.Cells("Detraccion").Value = "0.00"
                        'dgvLista.Rows(i).Cells("enproceso").Value = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value

                        '****************RETENCION DETRACCION *********************************
                        If dgvLista.Rows(i).Cells("PorcentajeRetDet").Value <> "0.00" Then
                            Dim porcT As Decimal = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value) * Convert.ToDecimal(dgvLista.Rows(i).Cells("PorcentajeRetDet").Value)
                            Dim percepcion As Decimal = (dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value) * Convert.ToDecimal(dgvLista.Rows(i).Cells("PorcentajePercepcion").Value)

                            If dgvLista.Rows(i).Cells("MRetenido").Value <> "0.00" Then
                                dgvProgramacionPagos.CurrentRow.Cells("Retencion").Value = porcT.ToString

                            End If
                            'If dgvLista.Rows(i).Cells("Detraccion").Value <> "0.00" Then
                            '    dgvProgramacionPagos.CurrentRow.Cells("Detraccion").Value = porcT.ToString
                            'End If
                            If dgvLista.Rows(i).Cells("ImportePercepcion").Value <> "0.00" Then
                                dgvProgramacionPagos.CurrentRow.Cells("Percepion").Value = percepcion
                            End If
                            dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = (dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value - dgvProgramacionPagos.CurrentRow.Cells("Retencion").Value) + dgvProgramacionPagos.CurrentRow.Cells("Percepion").Value - dgvProgramacionPagos.CurrentRow.Cells("NotaCredito").Value
                        Else
                            dgvProgramacionPagos.CurrentRow.Cells("Retencion").Value = "0.00"
                            dgvProgramacionPagos.CurrentRow.Cells("Detraccion").Value = "0.00"
                            dgvProgramacionPagos.CurrentRow.Cells("Percepion").Value = "0.00"
                            dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value - dgvProgramacionPagos.CurrentRow.Cells("NotaCredito").Value


                        End If
                        '****************************************************************


                        '******************************************** FACTURAS********************************************************
                        If dgvProgramacionPagos.CurrentRow.Cells("IdCorrelativo").Value <> "" Then
                            For f = 0 To dgvFacturas.RowCount - 1
                                If dgvFacturas.Rows(f).Cells("IdCorrelativo").Value = dgvProgramacionPagos.CurrentRow.Cells("IdCorrelativo").Value Then

                                    Dim subT As Decimal = Convert.ToDecimal(dgvFacturas.Rows(f).Cells("ImporteTotal").Value) - Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value)

                                    dgvFacturas.Rows(f).Cells("Saldo").Value = subT
                                End If
                            Next
                        End If

                        '*************************************************************************************************************


                    End If
                Next
            End If
            'dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value

            If docOrigen = "2" Then
                For i = 0 To rgvER.RowCount - 1
                    If rgvER.Rows(i).Cells("ID_DocPendiente").Value = VL_ID Then

                        Dim er1 As Decimal = Convert.ToDecimal(rgvER.Rows(i).Cells("saldo1").Value)
                        Dim er2 As Decimal = Convert.ToDecimal(rgvER.Rows(i).Cells("enproceso").Value)
                        'Dim er3 As Decimal = Convert.ToDecimal(rgvER.Rows(i).Cells("TotalaPagar").Value)
                        Dim VL_ER As Decimal = er1 - dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value

                        rgvER.Rows(i).Cells("Saldo").Value = VL_ER
                        'rgvER.Rows(i).Cells("enproceso").Value = C
                        dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value
                        'dgvPlanillas_RowFormatting.Rows(i).Cells("TotalaPagar").Value = rgvER.Rows(i).Cells("PTotal").Value

                    End If
                Next
            End If
            If docOrigen = "3" Then
                For i = 0 To dgvCajaChica.RowCount - 1
                    If dgvCajaChica.Rows(i).Cells("ID_DocPendiente").Value = VL_ID Then

                        Dim cc1 As Decimal = Convert.ToDecimal(dgvCajaChica.Rows(i).Cells("saldo1").Value)
                        Dim cc2 As Decimal = Convert.ToDecimal(dgvCajaChica.Rows(i).Cells("enproceso").Value)

                        Dim VL_CC As Decimal = cc1 - dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value
                        dgvCajaChica.Rows(i).Cells("Saldo").Value = VL_CC
                        ' dgvCajaChica.Rows(i).Cells("EnProceso").Value = C
                        dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value
                    End If
                Next
            End If
            If docOrigen = "4" Then
                For i = 0 To dgvValorizacion.RowCount - 1
                    If dgvValorizacion.Rows(i).Cells("ID_DocPendiente").Value = VL_ID Then

                        Dim v1 As Decimal = IIf(dgvValorizacion.Rows(i).Cells("saldo1").Value <> dgvValorizacion.Rows(i).Cells("TotalVal").Value, Convert.ToDecimal(dgvValorizacion.Rows(i).Cells("saldo1").Value), Convert.ToDecimal(dgvValorizacion.Rows(i).Cells("saldo1").Value))
                        Dim v2 As Decimal = Convert.ToDecimal(dgvValorizacion.Rows(i).Cells("enproceso").Value)

                        Dim VL_V As Decimal = v1 - dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value
                        dgvValorizacion.Rows(i).Cells("Saldo").Value = VL_V
                        'dgvValorizacion.Rows(i).Cells("enproceso").Value = C

                        '****************RETENCION DETRACCION *********************************
                        If dgvValorizacion.Rows(i).Cells("PorcentajeRetDet").Value <> "0.00" Then
                            Dim porcT As Decimal = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value) * Convert.ToDecimal(dgvLista.Rows(i).Cells("PorcentajeRetDet").Value)
                            Dim percepcion As Decimal = (dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value) * Convert.ToDecimal(dgvValorizacion.Rows(i).Cells("PorcentajePercepcion").Value)

                            If dgvValorizacion.Rows(i).Cells("RevisadoRetencion").Value <> "0.00" Then
                                dgvProgramacionPagos.CurrentRow.Cells("Retencion").Value = porcT.ToString

                            End If
                            'If dgvLista.Rows(i).Cells("Detraccion").Value <> "0.00" Then
                            '    dgvProgramacionPagos.CurrentRow.Cells("Detraccion").Value = porcT.ToString
                            'End If
                            If dgvValorizacion.Rows(i).Cells("ImportePercepcion").Value <> "0.00" Then
                                dgvProgramacionPagos.CurrentRow.Cells("Percepion").Value = percepcion
                            End If
                            dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = (dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value - dgvProgramacionPagos.CurrentRow.Cells("Retencion").Value) + dgvProgramacionPagos.CurrentRow.Cells("Percepion").Value
                        Else
                            dgvProgramacionPagos.CurrentRow.Cells("Retencion").Value = "0.00"
                            dgvProgramacionPagos.CurrentRow.Cells("Detraccion").Value = "0.00"
                            dgvProgramacionPagos.CurrentRow.Cells("Percepion").Value = "0.00"
                            dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value

                        End If
                        '****************************************************************


                    End If
                Next
            End If
            If docOrigen = "5" Then
                For i = 0 To dgvPlanillas.RowCount - 1
                    If dgvPlanillas.Rows(i).Cells("ID_DocPendiente").Value = VL_ID Then

                        Dim P1 As Decimal = Convert.ToDecimal(dgvPlanillas.Rows(i).Cells("saldo1").Value)
                        Dim P2 As Decimal = Convert.ToDecimal(dgvPlanillas.Rows(i).Cells("enproceso").Value)
                        Dim VL_V As Decimal = P1 - dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value

                        dgvPlanillas.Rows(i).Cells("Saldo").Value = VL_V
                        'dgvPlanillas.Rows(i).Cells("enproceso").Value = C
                        dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value

                    End If
                Next
            End If

            sumatotalProgramacion()
        End If



        If dgvProgramacionPagos.Columns(e.ColumnIndex).Name = "NotaCredito" Then

            Dim NetoPagar As Decimal = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value)
            Dim NotaCredito As Decimal = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("NotaCredito").Value)
            Dim pago As Decimal = NetoPagar - NotaCredito

            dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value = pago

            For i = 0 To dgvLista.RowCount - 1
                If dgvLista.Rows(i).Cells("ID_DocPendiente").Value = dgvProgramacionPagos.Rows(e.RowIndex).Cells("ID_DocPendiente").Value Then
                    dgvLista.Rows(i).Cells("SALDO").Value = pago
                End If
            Next


            sumatotalProgramacion()
        End If

    End Sub
    Private Sub dgvProgramacionPagos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
0:
        If e.ColumnIndex.ToString = "0" Then
            Dim PPE = dgvProgramacionPagos.CurrentRow.Cells("NroER").Value
            Dim DOCORIGEN = dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value

            If DOCORIGEN = "2" Then
                For i As Integer = 0 To rgvER.RowCount - 1
                    If rgvER.Rows(i).Cells("Nro").Value = PPE Then
                        rgvER.Rows(i).Cells("CODE").Value = "Z"

                        Dim saldoER = rgvER.Rows(i).Cells("Saldo").Value
                        Dim enprocesoER = rgvER.Rows(i).Cells("EnProceso").Value

                        rgvER.Rows(i).Cells("Saldo").Value = saldoER + enprocesoER
                        rgvER.Rows(i).Cells("EnProceso").Value = "0"

                    End If

                Next
            End If
            'dgvEntregaRendir.Rows(PP).DefaultCellStyle.BackColor = Color.White
            If DOCORIGEN = "1" Then
                For i As Integer = 0 To dgvLista.RowCount - 1
                    If dgvLista.Rows(i).Cells("OCONUMERO").Value = PPE Then
                        'dgvLista.Rows(i).DefaultCellStyle.BackColor = Color.White

                        'dgvProgramacionPagos.Rows.Remove(dgvLista.Rows(i).Cells("OCONUMERO").Value)
                        Dim saldoOC = dgvLista.Rows(i).Cells("saldo").Value
                        Dim enprocesoOC = dgvLista.Rows(i).Cells("enproceso").Value

                        dgvLista.Rows(i).Cells("CODE").Value = "Z"
                        dgvLista.Rows(i).Cells("SALDO").Value = saldoOC + enprocesoOC
                        dgvLista.Rows(i).Cells("enproceso").Value = "0"

                    End If
                Next
            End If
            If DOCORIGEN = "3" Then
                For i As Integer = 0 To dgvCajaChica.RowCount - 1
                    If dgvCajaChica.Rows(i).Cells("IdCaja").Value = PPE Then
                        dgvCajaChica.Rows(i).Cells("CODE").Value = "Z"

                        Dim saldoCC = dgvCajaChica.Rows(i).Cells("Saldo").Value
                        Dim enprocesoCC = dgvCajaChica.Rows(i).Cells("EnProceso").Value

                        dgvCajaChica.Rows(i).Cells("Saldo").Value = saldoCC + enprocesoCC
                        dgvCajaChica.Rows(i).Cells("EnProceso").Value = "0"

                    End If
                Next

            End If



            dgvProgramacionPagos.Rows.Remove(dgvProgramacionPagos.CurrentRow)
            sumatotalProgramacion()
        End If

    End Sub
    Private Sub RadGridView2_CellDoubleClick(sender As Object, e As GridViewCellEventArgs) Handles dgvProgramacionPagos.CellDoubleClick
        Try
            If e.ColumnIndex.ToString = "0" Then
                Dim PPE = dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value
                Dim DOCORIGEN = dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value

                If DOCORIGEN = "2" Then
                    For i As Integer = 0 To rgvER.RowCount - 1
                        If rgvER.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                            rgvER.Rows(i).Cells("CODE").Value = False

                            Dim saldoER = rgvER.Rows(i).Cells("saldo1").Value
                            Dim enprocesoER = rgvER.Rows(i).Cells("EnProceso").Value

                            rgvER.Rows(i).Cells("Saldo").Value = saldoER + enprocesoER
                            rgvER.Rows(i).Cells("EnProceso").Value = "0"

                        End If

                    Next
                End If
                'dgvEntregaRendir.Rows(PP).DefaultCellStyle.BackColor = Color.White
                If DOCORIGEN = "1" Then
                    For i As Integer = 0 To dgvLista.RowCount - 1
                        If dgvLista.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                            'dgvLista.Rows(i).DefaultCellStyle.BackColor = Color.White

                            'dgvProgramacionPagos.Rows.Remove(dgvLista.Rows(i).Cells("OCONUMERO").Value)
                            Dim saldoOC = dgvLista.Rows(i).Cells("saldo").Value
                            Dim saldopro = dgvLista.Rows(i).Cells("TOTAL").Value - dgvLista.Rows(i).Cells("Amortizado").Value

                            dgvLista.Rows(i).Cells("CODE").Value = False
                            dgvLista.Rows(i).Cells("SALDO").Value = saldopro
                            dgvLista.Rows(i).Cells("enproceso").Value = "0"

                            EliminarNotaCredito(Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value))

                            For h As Integer = 0 To dgvFacturas.RowCount - 1
                                If dgvProgramacionPagos.CurrentRow.Cells("IdCorrelativo").Value <> "" Then
                                    If dgvProgramacionPagos.CurrentRow.Cells("IdCorrelativo").Value = dgvFacturas.Rows(h).Cells("IdCorrelativo").Value Then
                                        dgvFacturas.Rows(h).Cells("Saldo").Value = "0.00"

                                    End If
                                End If
                            Next


                        End If


                    Next
                End If
                If DOCORIGEN = "3" Then
                    For i As Integer = 0 To dgvCajaChica.RowCount - 1
                        If dgvCajaChica.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                            dgvCajaChica.Rows(i).Cells("CODE").Value = False

                            Dim saldoCC = dgvCajaChica.Rows(i).Cells("Saldo").Value
                            Dim enprocesoCC = dgvCajaChica.Rows(i).Cells("EnProceso").Value

                            dgvCajaChica.Rows(i).Cells("Saldo").Value = saldoCC + enprocesoCC
                            dgvCajaChica.Rows(i).Cells("EnProceso").Value = "0"

                        End If
                    Next

                End If

                If DOCORIGEN = "4" Then
                    For i As Integer = 0 To dgvValorizacion.RowCount - 1
                        If dgvValorizacion.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                            dgvValorizacion.Rows(i).Cells("CODE").Value = False

                            Dim saldoCC = dgvValorizacion.Rows(i).Cells("Saldo").Value
                            Dim enprocesoCC = dgvValorizacion.Rows(i).Cells("EnProceso").Value

                            dgvValorizacion.Rows(i).Cells("Saldo").Value = saldoCC + enprocesoCC
                            dgvValorizacion.Rows(i).Cells("EnProceso").Value = "0"

                        End If
                    Next

                End If

                If DOCORIGEN = "5" Then
                    For i As Integer = 0 To dgvPlanillas.RowCount - 1
                        If dgvPlanillas.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                            dgvPlanillas.Rows(i).Cells("CODE").Value = False

                            Dim saldoP = dgvPlanillas.Rows(i).Cells("Saldo").Value
                            Dim enprocesoP = dgvPlanillas.Rows(i).Cells("EnProceso").Value

                            dgvPlanillas.Rows(i).Cells("Saldo").Value = saldoP + enprocesoP
                            dgvPlanillas.Rows(i).Cells("EnProceso").Value = "0"

                        End If
                    Next

                End If


                dgvProgramacionPagos.Rows.Remove(dgvProgramacionPagos.CurrentRow)
                sumatotalProgramacion()
            End If
        Catch ex As Exception

        End Try

    End Sub
    Private Sub DataGridView_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs)
        dgvProgramacionPagos.EndEdit()
        'verifica si existe algun datos en la grilla "dgvProgramacionPagos" si no existe no se ejecuta
        Dim i As Integer
        Dim valor As Decimal = "1.00"

        If dgvProgramacionPagos.Columns(e.ColumnIndex).Name = "PTotal" Then

            'CAPTURA LA MODIFICACION SOBRE LA COLUMNA "PTotal" DE LA GRILLA dgvProgramacionPagos
            Dim VL_NUMERO As Decimal = Convert.ToDecimal(dgvProgramacionPagos.Rows(e.RowIndex).Cells("PTotal").Value)
            'variables  VL_NUMERO para poder capturar la modificacion de la columna "PTotal", variable VL_ID captura indice de la grilla dgvprogramacion de pagos
            Dim VL_ID As String = dgvProgramacionPagos.Rows(e.RowIndex).Cells("NroER").Value
            'VL_NUMERO = dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value.ToString()
            Dim C As Decimal = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("PTotal").Value)

            If dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value = "1" Then
                'FOR RECORRE LA GRILLA dgvLista PARA PODER VERIFICAR QUE LA FILA MODIFICADA EN LA GRILLA DE dgvProgramacionPagos SEA IGUAL A LA GRILLA dgvLista
                For i = 0 To dgvLista.RowCount - 1
                    'SI LOS INDICES DE LAS 2 GRILLAS SON IGUALES 
                    If dgvLista.Rows(i).Cells("OCONUMERO").Value = VL_ID Then
                        'EDITA LA COLUMNA "enproceso" POR EL VALOR QUE ESTA EN LA VARIABLE VL_NUMERO-+
                        'dgvLista.Rows(i).Cells("enproceso").Value = VL_NUMERO
                        'REALIZA EL CALCULO DEL SALDO = TOTAL - ENPROCESO

                        Dim A As Decimal = Convert.ToDecimal(dgvLista.Rows(i).Cells("SALDO").Value)
                        Dim B As Decimal = Convert.ToDecimal(dgvLista.Rows(i).Cells("enproceso").Value)


                        Dim VL_NUMERO2 As Decimal = (A + B) - (C + B)
                        'ASIGA EL VALOR EN LA COLUMNA SALDO DE LA GRILLA dgvLista 
                        dgvLista.Rows(i).Cells("SALDO").Value = VL_NUMERO2
                        dgvLista.Rows(i).Cells("enproceso").Value = C * valor

                    End If
                Next
            End If
            'dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value

            If dgvProgramacionPagos.Rows(e.RowIndex).Cells("ID_DOC_ORIGEN").Value = "2" Then
                For i = 0 To rgvER.RowCount - 1
                    If rgvER.Rows(i).Cells("Nro").Value = VL_ID Then

                        Dim er1 As Decimal = Convert.ToDecimal(rgvER.Rows(i).Cells("Saldo").Value)
                        Dim er2 As Decimal = Convert.ToDecimal(rgvER.Rows(i).Cells("enproceso").Value)

                        Dim VL_ER As Decimal = (er1 + er2) - (C + er2)
                        rgvER.Rows(i).Cells("Saldo").Value = VL_ER
                        rgvER.Rows(i).Cells("enproceso").Value = C * valor

                    End If
                Next
            End If
            If dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value = "3" Then
                For i = 0 To dgvCajaChica.RowCount - 1
                    If dgvCajaChica.Rows(i).Cells("IdCaja").Value = VL_ID Then

                        Dim cc1 As Decimal = Convert.ToDecimal(dgvCajaChica.Rows(i).Cells("Saldo").Value)
                        Dim cc2 As Decimal = Convert.ToDecimal(dgvCajaChica.Rows(i).Cells("enproceso").Value)

                        Dim VL_CC As Decimal = (cc1 + cc2) - (C + cc2)
                        dgvCajaChica.Rows(i).Cells("Saldo").Value = VL_CC
                        dgvCajaChica.Rows(i).Cells("EnProceso").Value = C * valor
                    End If
                Next
            End If

            sumatotalProgramacion()
        End If

    End Sub
#End Region

#Region "COLOR EN LAS CELDAS"
    Private Sub dgvLista_RowFormatting(sender As Object, e As RowFormattingEventArgs) Handles dgvLista.RowFormatting
        'dgvLista.EndEdit()

        'If e.RowElement.RowInfo.Cells("REVISION").Value = "SI" Then

        If e.RowElement.RowInfo.Cells("CODE").Value = True Then
            e.RowElement.DrawFill = True
            e.RowElement.GradientStyle = GradientStyles.Solid
            e.RowElement.BackColor = Color.Red
        Else
            e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local)

        End If
        'If e.RowElement.RowInfo.Cells("CODE").Value = False Then
        '    e.RowElement.DrawFill = True
        '    e.RowElement.BackColor = System.Drawing.Color.White
        'End If
        'If e.RowElement.RowInfo.Cells("CODE").Value = Nothing Then
        '    e.RowElement.DrawFill = True
        '    e.RowElement.BackColor = System.Drawing.Color.White
        'End If

        'End If

    End Sub

    Private Sub rgvER_RowFormatting(sender As Object, e As RowFormattingEventArgs) Handles rgvER.RowFormatting
        'rgvER.EndEdit()

        If e.RowElement.RowInfo.Cells("CODE").Value = True Then
            e.RowElement.DrawFill = True
            e.RowElement.GradientStyle = GradientStyles.Solid
            e.RowElement.BackColor = Color.Red
        Else
            e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local)

        End If
    End Sub

    Private Sub dgvCajaChica_RowFormatting(sender As Object, e As RowFormattingEventArgs) Handles dgvCajaChica.RowFormatting
        'dgvCajaChica.EndEdit()

        If e.RowElement.RowInfo.Cells("CODE").Value = True Then
            e.RowElement.DrawFill = True
            e.RowElement.GradientStyle = GradientStyles.Solid
            e.RowElement.BackColor = Color.Red
        Else
            e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local)

        End If
    End Sub

    Private Sub dgvValorizacion_RowFormatting(sender As Object, e As RowFormattingEventArgs) Handles dgvValorizacion.RowFormatting
        'dgvValorizacion.EndEdit()

        If e.RowElement.RowInfo.Cells("CODE").Value = True Then
            e.RowElement.DrawFill = True
            e.RowElement.GradientStyle = GradientStyles.Solid
            e.RowElement.BackColor = Color.Red
        Else
            e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local)

        End If
    End Sub

    Private Sub dgvPlanillas_RowFormatting(sender As Object, e As RowFormattingEventArgs) Handles dgvPlanillas.RowFormatting
        'dgvPlanillas.EndEdit()

        If e.RowElement.RowInfo.Cells("CODE").Value = True Then
            e.RowElement.DrawFill = True
            e.RowElement.GradientStyle = GradientStyles.Solid
            e.RowElement.BackColor = Color.Red
        Else
            e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local)
            e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local)

        End If
    End Sub
#End Region

#Region "MENU TOOL"
    Private Sub tsEliminar_Click(sender As Object, e As EventArgs) Handles tsEliminar.Click

        If dgvProgramacionPagos.RowCount <> Nothing Then

            Dim PPE = dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value
            Dim DOCORIGEN = dgvProgramacionPagos.CurrentRow.Cells("ID_DOC_ORIGEN").Value

            If DOCORIGEN = "2" Then
                For i As Integer = 0 To rgvER.RowCount - 1
                    If rgvER.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                        rgvER.Rows(i).Cells("CODE").Value = False

                        Dim saldoER = rgvER.Rows(i).Cells("saldo1").Value
                        Dim enprocesoER = rgvER.Rows(i).Cells("EnProceso").Value

                        rgvER.Rows(i).Cells("Saldo").Value = saldoER + enprocesoER
                        rgvER.Rows(i).Cells("EnProceso").Value = "0"

                    End If

                Next
            End If
            'dgvEntregaRendir.Rows(PP).DefaultCellStyle.BackColor = Color.White
            If DOCORIGEN = "1" Then
                For i As Integer = 0 To dgvLista.RowCount - 1
                    If dgvLista.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                        'dgvLista.Rows(i).DefaultCellStyle.BackColor = Color.White

                        'dgvProgramacionPagos.Rows.Remove(dgvLista.Rows(i).Cells("OCONUMERO").Value)
                        Dim saldoOC = dgvLista.Rows(i).Cells("saldo").Value
                        Dim saldopro = dgvLista.Rows(i).Cells("TOTAL").Value - dgvLista.Rows(i).Cells("Amortizado").Value

                        dgvLista.Rows(i).Cells("CODE").Value = False
                        dgvLista.Rows(i).Cells("SALDO").Value = saldopro
                        dgvLista.Rows(i).Cells("enproceso").Value = "0"

                        EliminarNotaCredito(Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value))

                        For h As Integer = 0 To dgvFacturas.RowCount - 1
                            If dgvProgramacionPagos.CurrentRow.Cells("IdCorrelativo").Value <> "" Then
                                If dgvProgramacionPagos.CurrentRow.Cells("IdCorrelativo").Value = dgvFacturas.Rows(h).Cells("IdCorrelativo").Value Then
                                    dgvFacturas.Rows(h).Cells("Saldo").Value = "0.00"

                                End If
                            End If
                        Next


                    End If


                Next
            End If
            If DOCORIGEN = "3" Then
                For i As Integer = 0 To dgvCajaChica.RowCount - 1
                    If dgvCajaChica.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                        dgvCajaChica.Rows(i).Cells("CODE").Value = False

                        Dim saldoCC = dgvCajaChica.Rows(i).Cells("Saldo").Value
                        Dim enprocesoCC = dgvCajaChica.Rows(i).Cells("EnProceso").Value

                        dgvCajaChica.Rows(i).Cells("Saldo").Value = saldoCC + enprocesoCC
                        dgvCajaChica.Rows(i).Cells("EnProceso").Value = "0"

                    End If
                Next

            End If

            If DOCORIGEN = "4" Then
                For i As Integer = 0 To dgvValorizacion.RowCount - 1
                    If dgvValorizacion.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                        dgvValorizacion.Rows(i).Cells("CODE").Value = False

                        Dim saldoCC = dgvValorizacion.Rows(i).Cells("Saldo").Value
                        Dim enprocesoCC = dgvValorizacion.Rows(i).Cells("EnProceso").Value

                        dgvValorizacion.Rows(i).Cells("Saldo").Value = saldoCC + enprocesoCC
                        dgvValorizacion.Rows(i).Cells("EnProceso").Value = "0"

                    End If
                Next

            End If

            If DOCORIGEN = "5" Then
                For i As Integer = 0 To dgvPlanillas.RowCount - 1
                    If dgvPlanillas.Rows(i).Cells("ID_DocPendiente").Value = PPE Then
                        dgvPlanillas.Rows(i).Cells("CODE").Value = False

                        Dim saldoP = dgvPlanillas.Rows(i).Cells("Saldo").Value
                        Dim enprocesoP = dgvPlanillas.Rows(i).Cells("EnProceso").Value

                        dgvPlanillas.Rows(i).Cells("Saldo").Value = saldoP + enprocesoP
                        dgvPlanillas.Rows(i).Cells("EnProceso").Value = "0"

                    End If
                Next

            End If


            dgvProgramacionPagos.Rows.Remove(dgvProgramacionPagos.CurrentRow)
            sumatotalProgramacion()
        End If

    End Sub


    Private Sub Revisado_Click(sender As Object, e As EventArgs) Handles Revisado.Click

        Dim ID_DocPendiente As Decimal = dgvLista.CurrentRow.Cells("ID_DocPendiente").Value
        Dim REVISION As String = dgvLista.CurrentRow.Cells("REVISION").Value


        Dim VL_IDDOC As Decimal = dgvLista.CurrentRow.Cells("DocOrigen").Value.ToString()


        'If REVISION = "NO" Then
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "1", "1")
        '    dgvLista.CurrentRow.Cells("REVISION").Value = "SI"
        'Else
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "0", "0")
        '    dgvLista.CurrentRow.Cells("REVISION").Value = "NO"
        'End If
        ListarIdUsuario()
        InsertarVB(idUsuario, 1, ID_DocPendiente)
        ListarVBValidacion("1", ID_DocPendiente.ToString())
        dgvLista.CurrentRow.Cells("REVISION").Value = valor
        ListarConsultaVB(2, ID_DocPendiente.ToString(), VL_IDDOC)



    End Sub

    Private Sub RevisadoCC_Click(sender As Object, e As EventArgs) Handles RevisadoCC.Click
        Dim ID_DocPendiente As String = dgvCajaChica.CurrentRow.Cells("ID_DocPendiente").Value
        Dim REVISION As String = dgvCajaChica.CurrentRow.Cells("REVISION").Value
        Dim VL_IDDOC As Decimal = dgvCajaChica.CurrentRow.Cells("DocOrigen").Value

        'If REVISION = "NO" Then
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "1", "1")
        '    dgvCajaChica.CurrentRow.Cells("REVISION").Value = "SI"
        'Else
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "0", "0")
        '    dgvCajaChica.CurrentRow.Cells("REVISION").Value = "NO"
        'End If
        ListarIdUsuario()
        InsertarVB(idUsuario, 3, ID_DocPendiente)
        ListarVBValidacion("3", ID_DocPendiente.ToString())
        dgvCajaChica.CurrentRow.Cells("REVISION").Value = valor
        ListarConsultaVB(2, ID_DocPendiente, VL_IDDOC)

    End Sub

    Private Sub RevisadoER_Click(sender As Object, e As EventArgs) Handles RevisadoER.Click
        Dim ID_DocPendiente As String = rgvER.CurrentRow.Cells("ID_DocPendiente").Value
        Dim REVISION As String = rgvER.CurrentRow.Cells("REVISION").Value
        Dim VL_IDDOC As Decimal = rgvER.CurrentRow.Cells("DocOrigen").Value
        'If REVISION = "NO" Then
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "1", "1")
        '    rgvER.CurrentRow.Cells("REVISION").Value = "SI"
        'Else
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "0", "0")
        '    rgvER.CurrentRow.Cells("REVISION").Value = "NO"
        'End If
        ListarIdUsuario()
        InsertarVB(idUsuario, 2, ID_DocPendiente)
        ListarVBValidacion("2", ID_DocPendiente.ToString())
        rgvER.CurrentRow.Cells("REVISION").Value = valor
        ListarConsultaVB(2, ID_DocPendiente.ToString(), VL_IDDOC)

    End Sub

    Private Sub RevisadoP_Click(sender As Object, e As EventArgs) Handles RevisadoP.Click
        Dim ID_DocPendiente As String = dgvPlanillas.CurrentRow.Cells("ID_DocPendiente").Value
        Dim REVISION As String = dgvPlanillas.CurrentRow.Cells("REVISION").Value
        Dim VL_IDDOC As Decimal = dgvPlanillas.CurrentRow.Cells("DocOrigen").Value

        'If REVISION = "NO" Then
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "1", "1")
        '    dgvPlanillas.CurrentRow.Cells("REVISION").Value = "SI"
        'Else
        '    Actualizard_Revisado_DocPendientes(ID_DocPendiente, "0", "0")
        '    dgvPlanillas.CurrentRow.Cells("REVISION").Value = "NO"
        'End If
        ListarIdUsuario()
        InsertarVB(idUsuario, 5, ID_DocPendiente)
        ListarVBValidacion("5", ID_DocPendiente.ToString())
        dgvPlanillas.CurrentRow.Cells("REVISION").Value = valor
        ListarConsultaVB(2, ID_DocPendiente, VL_IDDOC)
    End Sub

    Private Sub RevisadoV_Click(sender As Object, e As EventArgs) Handles RevisadoV.Click
        Dim ID_DocPendiente As String = dgvValorizacion.CurrentRow.Cells("ID_DocPendiente").Value
        Dim REVISION As String = dgvValorizacion.CurrentRow.Cells("REVISION").Value
        Dim VL_IDDOC As Decimal = dgvValorizacion.CurrentRow.Cells("DocOrigen").Value

        'If dgvValorizacion.RowCount <> Nothing And dgvValorizacion.RowCount > 0 Then
        '    Dim ID_DocPendiente As String = dgvValorizacion.CurrentRow.Cells("ID_DocPendiente").Value
        '    Dim REVISION As String = dgvValorizacion.CurrentRow.Cells("REVISION").Value

        '    If REVISION = "NO" Then
        '        Actualizard_Revisado_DocPendientes(ID_DocPendiente, "1", "1")
        '        dgvValorizacion.CurrentRow.Cells("REVISION").Value = "SI"
        '    Else
        '        Actualizard_Revisado_DocPendientes(ID_DocPendiente, "0", "0")
        '        dgvValorizacion.CurrentRow.Cells("REVISION").Value = "NO"
        '    End If

        'End If

        ListarIdUsuario()
        InsertarVB(idUsuario, 4, ID_DocPendiente)
        ListarVBValidacion("4", ID_DocPendiente.ToString())
        dgvValorizacion.CurrentRow.Cells("REVISION").Value = valor
        ListarConsultaVB(2, ID_DocPendiente, VL_IDDOC)

    End Sub

#End Region

#Region "MENU OPENING"
    Private Sub rgvER_ContextMenuOpening(sender As Object, e As ContextMenuOpeningEventArgs) Handles rgvER.ContextMenuOpening
        e.Cancel = True
    End Sub

    Private Sub dgvCajaChica_ContextMenuOpening(sender As Object, e As ContextMenuOpeningEventArgs) Handles dgvCajaChica.ContextMenuOpening
        e.Cancel = True
    End Sub

    Private Sub dgvValorizacion_ContextMenuOpening(sender As Object, e As ContextMenuOpeningEventArgs) Handles dgvValorizacion.ContextMenuOpening
        e.Cancel = True
    End Sub

    Private Sub dgvPlanillas_ContextMenuOpening(sender As Object, e As ContextMenuOpeningEventArgs) Handles dgvPlanillas.ContextMenuOpening
        e.Cancel = True
    End Sub


    Private Sub dgvLista_ContextMenuOpening(sender As Object, e As ContextMenuOpeningEventArgs) Handles dgvLista.ContextMenuOpening
        e.Cancel = True
    End Sub

    Private Sub dgvProgramacionPagos_ContextMenuOpening(sender As Object, e As ContextMenuOpeningEventArgs) Handles dgvProgramacionPagos.ContextMenuOpening

        e.Cancel = True

    End Sub

#End Region

#Region "OPENING"

    Private Sub MenuRevisadoPlanillas_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MenuRevisadoPlanillas.Opening

        If dgvPlanillas.RowCount > 0 Then
            MenuRevisadoPlanillas.Visible = True
            RevisadoP.Visible = True
        Else
            MenuRevisadoPlanillas.Visible = False
            RevisadoP.Visible = False

        End If

    End Sub

    Private Sub MenuRevisadoValorizacion_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MenuRevisadoValorizacion.Opening

        If dgvValorizacion.RowCount > 0 Then
            MenuRevisadoValorizacion.Visible = True
            RevisadoV.Visible = True
        Else
            MenuRevisadoValorizacion.Visible = False
            RevisadoV.Visible = False

        End If

    End Sub

    Private Sub MenuRevisadoCC_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MenuRevisadoCC.Opening

        If dgvCajaChica.RowCount > 0 Then
            MenuRevisadoCC.Visible = True
            RevisadoCC.Visible = True
        Else
            MenuRevisadoCC.Visible = False
            RevisadoCC.Visible = False

        End If
    End Sub

    Private Sub MenuRevisadoER_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MenuRevisadoER.Opening

        If rgvER.RowCount > 0 Then
            MenuRevisadoER.Visible = True
            RevisadoER.Visible = True
        Else
            MenuRevisadoER.Visible = False
            RevisadoER.Visible = False

        End If
    End Sub

    Private Sub MenuRevisadoOC_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MenuRevisadoOC.Opening


        If dgvLista.RowCount > 0 Then
            MenuRevisadoOC.Visible = True
            Revisado.Visible = True
        Else
            MenuRevisadoOC.Visible = False
            Revisado.Visible = False

        End If
    End Sub

#End Region


#Region "JORGE VARGAS"

    Private Sub mnuOrdenCompra_Click(sender As Object, e As EventArgs) Handles mnuOrdenCompra.Click
        If dgvLista.RowCount > 0 Then
            opcion = 1 ' Orden de compra / servicio
            nombreorigen = "ORDEN DE COMPRA / SERVICIO"
            docOrigen = Convert.ToDecimal(dgvLista.CurrentRow.Cells("DocOrigen").Value)
            id_DocPendiente = Convert.ToDecimal(dgvLista.CurrentRow.Cells("ID_DocPendiente").Value)

            Dim x As frmReporGestionDocumentoPagos = frmReporGestionDocumentoPagos.Instance
            x.MdiParent = frmPrincipal
            x.Show()
        End If
    End Sub

    Private Sub mnuEntregaRendir_Click(sender As Object, e As EventArgs) Handles mnuEntregaRendir.Click
        If rgvER.RowCount > 0 Then
            opcion = 2 ' Entrega rendir
            nombreorigen = "ENTREGA A RENDIR"
            docOrigen = Convert.ToDecimal(rgvER.CurrentRow.Cells("DocOrigen").Value)
            id_DocPendiente = Convert.ToDecimal(rgvER.CurrentRow.Cells("ID_DocPendiente").Value)

            Dim x As frmReporGestionDocumentoPagos = frmReporGestionDocumentoPagos.Instance
            x.MdiParent = frmPrincipal
            x.Show()
        End If
    End Sub

    Private Sub mnuValorizacion_Click(sender As Object, e As EventArgs) Handles mnuValorizacion.Click
        If dgvValorizacion.RowCount > 0 Then
            opcion = 3 ' VALORIZACIÓN
            nombreorigen = "VALORIZACIÓN"
            docOrigen = Convert.ToDecimal(rgvER.CurrentRow.Cells("DocOrigen").Value)
            id_DocPendiente = Convert.ToDecimal(rgvER.CurrentRow.Cells("ID_DocPendiente").Value)

            Dim x As frmReporGestionDocumentoPagos = frmReporGestionDocumentoPagos.Instance
            x.MdiParent = frmPrincipal
            x.Show()
        End If
    End Sub

    Private Sub mnuCajaChica_Click(sender As Object, e As EventArgs) Handles mnuCajaChica.Click
        If dgvCajaChica.RowCount > 0 Then
            opcion = 4 ' CAJA CHICA
            nombreorigen = "CAJA CHICA"
            docOrigen = Convert.ToDecimal(rgvER.CurrentRow.Cells("DocOrigen").Value)
            id_DocPendiente = Convert.ToDecimal(rgvER.CurrentRow.Cells("ID_DocPendiente").Value)

            Dim x As frmReporGestionDocumentoPagos = frmReporGestionDocumentoPagos.Instance
            x.MdiParent = frmPrincipal
            x.Show()
        End If
    End Sub

    Private Sub mnuPlanillas_Click(sender As Object, e As EventArgs) Handles mnuPlanillas.Click
        If dgvPlanillas.RowCount > 0 Then
            opcion = 5 ' PLANILLAS
            nombreorigen = "PLANILLAS"
            docOrigen = Convert.ToDecimal(rgvER.CurrentRow.Cells("DocOrigen").Value)
            id_DocPendiente = Convert.ToDecimal(rgvER.CurrentRow.Cells("ID_DocPendiente").Value)

            Dim x As frmReporGestionDocumentoPagos = frmReporGestionDocumentoPagos.Instance
            x.MdiParent = frmPrincipal
            x.Show()
        End If
    End Sub

#End Region



    Private Sub rgvER_CellClick(sender As Object, e As GridViewCellEventArgs) Handles rgvER.CellClick

        Try
            Dim VL_DOCPENDIENTE As Decimal = rgvER.CurrentRow.Cells("ID_DocPendiente").Value
            Dim VL_IDDOC As Decimal = rgvER.CurrentRow.Cells("DocOrigen").Value

            If e.RowIndex >= 0 Then
                ListarConsultaVBCantidad(1, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVBDocumentos(3, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVB(2, VL_DOCPENDIENTE, VL_IDDOC)
            End If
        Catch ex As Exception

        End Try


    End Sub

    Private Sub dgvValorizacion_CellClick(sender As Object, e As GridViewCellEventArgs) Handles dgvValorizacion.CellClick


        Try

            Dim VL_DOCPENDIENTE As Decimal = dgvValorizacion.CurrentRow.Cells("ID_DocPendiente").Value
            Dim VL_IDDOC As Decimal = dgvValorizacion.CurrentRow.Cells("DocOrigen").Value
            If e.RowIndex >= 0 Then
                ListarConsultaVBCantidad(1, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVBDocumentos(3, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVB(2, VL_DOCPENDIENTE, VL_IDDOC)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvCajaChica_CellClick(sender As Object, e As GridViewCellEventArgs) Handles dgvCajaChica.CellClick


        Try
            Dim VL_DOCPENDIENTE As Decimal = dgvCajaChica.CurrentRow.Cells("ID_DocPendiente").Value
            Dim VL_IDDOC As Decimal = dgvCajaChica.CurrentRow.Cells("DocOrigen").Value
            If e.RowIndex >= 0 Then
                ListarConsultaVBCantidad(1, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVBDocumentos(3, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVB(2, VL_DOCPENDIENTE, VL_IDDOC)

            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvPlanillas_CellClick(sender As Object, e As GridViewCellEventArgs) Handles dgvPlanillas.CellClick


        Try
            Dim VL_DOCPENDIENTE As Decimal = dgvPlanillas.CurrentRow.Cells("ID_DocPendiente").Value
            Dim VL_IDDOC As Decimal = dgvPlanillas.CurrentRow.Cells("DocOrigen").Value
            If e.RowIndex >= 0 Then
                ListarConsultaVBCantidad(1, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVBDocumentos(3, VL_DOCPENDIENTE, VL_IDDOC)
                ListarConsultaVB(2, VL_DOCPENDIENTE, VL_IDDOC)
            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        dgvLista.EndEdit()
        dgvFacturas.EndEdit()
        rgvER.EndEdit()
        dgvCajaChica.EndEdit()
        dgvPlanillas.EndEdit()

        VL_FECHAINICIO = dtpInicio.Text
        VL_FECHAFIN = dtpFin.Text
        Dim i, j, x, z As Integer

        If tbValorizado.SelectedIndex <> 0 Then
            gbFacturas.Visible = False
        End If


        If cboEmpresas.SelectedValue.ToString() = "-1" Then

        End If

        'VL_AREACODIGO = cboArea.SelectedValue

        If tbValorizado.SelectedIndex = 0 Then

            ListarOrdenesDerivadas()

            For i = 0 To dgvProgramacionPagos.RowCount - 1
                For j = 0 To dgvLista.RowCount - 1
                    If dgvProgramacionPagos.Rows(i).Cells("PID_DocOrigen").Value = dgvLista.Rows(j).Cells("ID_DocOrigen").Value Then

                        dgvLista.Rows(j).Cells("CODE").Value = True


                    End If
                Next

            Next

        ElseIf tbValorizado.SelectedIndex = 1 Then

            VL_FECHAINICIO_1 = dtpInicio.Text
            FiltrarTodaEntregaRendir()

            For x = 0 To dgvProgramacionPagos.RowCount - 1
                For z = 0 To rgvER.RowCount - 1
                    If dgvProgramacionPagos.Rows(x).Cells("PID_DocOrigen").Value = rgvER.Rows(z).Cells("ID_DocOrigen").Value Then

                        rgvER.Rows(z).Cells("CODE").Value = True

                    End If
                Next

            Next
        ElseIf tbValorizado.SelectedIndex = 2 Then
            ListarValorizacion()

            For i = 0 To dgvProgramacionPagos.RowCount - 1
                For j = 0 To dgvValorizacion.RowCount - 1
                    If dgvProgramacionPagos.Rows(i).Cells("PID_DocOrigen").Value = dgvValorizacion.Rows(j).Cells("ID_DocOrigen").Value Then

                        dgvValorizacion.Rows(j).Cells("CODE").Value = True

                    End If
                Next

            Next
        ElseIf tbValorizado.SelectedIndex = 3 Then

            ListarCajaChicas()

            For i = 0 To dgvProgramacionPagos.RowCount - 1
                For j = 0 To dgvCajaChica.RowCount - 1
                    If dgvProgramacionPagos.Rows(i).Cells("PID_DocOrigen").Value = dgvCajaChica.Rows(j).Cells("IdCorrelativoCajaChica").Value Then

                        dgvCajaChica.Rows(j).Cells("CODE").Value = True

                    End If
                Next

            Next
        Else
            ListarPlanillas()

            For i = 0 To dgvProgramacionPagos.RowCount - 1
                For j = 0 To dgvPlanillas.RowCount - 1
                    If dgvProgramacionPagos.Rows(i).Cells("Id_DocPendiente").Value = dgvPlanillas.Rows(j).Cells("ID_DocPendiente").Value Then

                        dgvPlanillas.Rows(j).Cells("CODE").Value = True

                    End If
                Next

            Next

        End If

    End Sub


    Private Sub NotaCredito_Click(sender As Object, e As EventArgs)

        Dim NotaCredito As New frmNotaCredito()

        If dgvLista.RowCount > 0 Then

            VL_RUC = dgvLista.CurrentRow.Cells("PRVRUC").Value.ToString()
            VL_EMPRESA = cboEmpresas.SelectedValue.ToString()
            VL_DOCPENDIENTE = Convert.ToDecimal(dgvLista.CurrentRow.Cells("ID_DocPendiente").Value)
            NotaCredito.ShowDialog()
        End If


    End Sub

    Private Sub tsNotaCredito_Click(sender As Object, e As EventArgs) Handles tsNotaCredito.Click


        Dim NotaCredito As New frmNotaCredito()



        If dgvLista.RowCount > 0 Then

            For d As Integer = 0 To dgvLista.RowCount - 1


                If dgvLista.Rows(d).Cells("ID_DocPendiente").Value = dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value Then

                    VL_RUC = dgvLista.Rows(d).Cells("PRVRUC").Value.ToString()
                    VL_EMPRESA = cboEmpresas.SelectedValue.ToString()
                    VL_DOCPENDIENTE = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("ID_DocPendiente").Value)
                    VL_NETOPAGAR = Convert.ToDecimal(dgvProgramacionPagos.CurrentRow.Cells("TotalaPagar").Value)
                    VL_MONEDA = IIf(dgvProgramacionPagos.CurrentRow.Cells("PMoneda").Value.ToString() = "DOLARES", "02", "01")



                    If NotaCredito.ShowDialog = Windows.Forms.DialogResult.OK Then

                        dgvProgramacionPagos.CurrentRow.Cells("NotaCredito").Value = frmNotaCredito.VL_MONTO
                        dgvProgramacionPagos.CurrentRow.Cells("IdFactura").Value = frmNotaCredito.VL_IdFactura




                    End If

                End If




            Next
        End If



    End Sub

  
End Class