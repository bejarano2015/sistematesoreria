Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmBanco
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eBanco As clsBanco
    Private eFiltro As clsPlantFiltro
    Private eRegistroDocumento As clsRegistroDocumento
    Private eTempo As clsPlantTempo

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmBanco = Nothing
    Public Shared Function Instance() As frmBanco
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmBanco
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtDescripcion.Clear()
        txtAbreviado.Clear()
        txtRuc.Clear()
        txtDireccion.Clear()
        txtTelefono.Clear()
        txtFax.Clear()
        txtEmail.Clear()
        txtWeb.Clear()
    End Sub

    Private Sub DesabilitarControles()
        txtCodigo.Enabled = False
        txtDescripcion.Enabled = False
        txtAbreviado.Enabled = False
        txtRuc.Enabled = False
        txtDireccion.Enabled = False
        txtTelefono.Enabled = False
        txtFax.Enabled = False
        txtEmail.Enabled = False
        txtWeb.Enabled = False
        cboEstado.Enabled = False
        cboTipoAcreedor.Enabled = False
        cboMoneda.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        txtCodigo.Enabled = True
        txtDescripcion.Enabled = True
        txtAbreviado.Enabled = True
        txtRuc.Enabled = True
        txtDireccion.Enabled = True
        txtTelefono.Enabled = True
        txtFax.Enabled = True
        txtEmail.Enabled = True
        txtWeb.Enabled = True
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If
        Panel2.Visible = True
        rdbRuc.Checked = True
        txtBusqueda.Clear()
        txtBusqueda.Focus()
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            cboEstado.SelectedIndex = 0
            cboEstado.Enabled = False
            eBanco = New clsBanco
            eBanco.fCodigo(gEmpresa)
            txtCodigo.Text = eBanco.sCodFuturo
            txtCodigo.Enabled = False
            'cboTipoAcreedor.SelectedValue = "O"
            'cboTipoAcreedor.Enabled = False
            cboMoneda.SelectedIndex = -1
            cboMoneda.Enabled = True
            txtDescripcion.Focus()
            'Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            HabilitarControles()
            cboEstado.Enabled = True
            cboTipoAcreedor.Enabled = True
            cboMoneda.Enabled = True
            txtCodigo.Enabled = False
            'Timer1.Enabled = True
        End If
    End Sub


    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        'Else
        'Exit Sub
        'End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""

        eTempo = New clsPlantTempo
        eBanco = New clsBanco
        If sTab = 1 Then
            If Len(Trim(txtCodigo.Text)) > 0 And cboTipoAcreedor.SelectedIndex <> -1 And Len(Trim(txtDescripcion.Text)) > 0 And Len(Trim(txtAbreviado.Text)) > 0 And Len(Trim(txtRuc.Text)) > 0 And cboMoneda.SelectedIndex <> -1 And Len(Trim(txtDireccion.Text)) > 0 And Len(Trim(txtTelefono.Text)) > 0 And Len(Trim(txtFax.Text)) > 0 And Len(Trim(txtEmail.Text)) > 0 And Len(Trim(txtWeb.Text)) > 0 Then
                If iOpcion = 1 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 2 Then
                    Mensaje = "Actualizar"
                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then
                        'eBanco.fBuscarCod(Trim(txtCodigo.Text), gEmpresa)
                        'If eBanco.iNroRegistros > 0 Then
                        'MessageBox.Show("El C�digo : " & Trim(txtCodigo.Text) & ", ya est� Registrado. Ingrese Otro C�digo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                        'Exit Sub
                        'ElseIf eBanco.iNroRegistros = 0 Then
                        'frmPrincipal.sFlagGrabar = "1"
                        'sCodigoRegistro = Trim(txtCodigo.Text)
                        ''If iOpcion = 1 Then
                        eBanco.fCodigo(gEmpresa)
                        sCodigoRegistro = eBanco.sCodFuturo
                        txtCodigo.Text = Trim(sCodigoRegistro)
                        ''Else
                        ''    sCodigoRegistro = Me.txtCodigo.Text
                        ''End If
                        iResultado = eBanco.fGrabar(sCodigoRegistro, Convert.ToString(Me.txtDescripcion.Text.Trim), Convert.ToString(Me.txtAbreviado.Text.Trim), Convert.ToString(Me.txtRuc.Text.Trim), Convert.ToString(Me.txtDireccion.Text.Trim), Convert.ToString(Me.txtTelefono.Text.Trim), Convert.ToString(Me.txtFax.Text.Trim), Convert.ToString(Me.txtEmail.Text.Trim), Convert.ToString(Me.txtWeb.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue), iOpcion, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(cboMoneda.SelectedValue))
                        If iResultado > 0 Then
                            mMostrarGrilla()
                            Timer2.Enabled = True
                        End If
                        sTab = 0
                        'frmPrincipal.sFlagGrabar = "0"
                        'TabControl1.TabPages(0).Focus()
                        'End If
                    ElseIf iOpcion = 2 Then
                        'eBanco.fBuscarCod(Trim(txtCodigo.Text), gEmpresa)
                        'If eBanco.iNroRegistros > 0 Then
                        'MessageBox.Show("El C�digo : " & Trim(txtCodigo.Text) & ", ya est� Registrado. Ingrese Otro C�digo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        'Exit Sub
                        'ElseIf eBanco.iNroRegistros = 0 Then
                        'frmPrincipal.sFlagGrabar = "1"
                        'sCodigoRegistro = Trim(txtCodigo.Text)
                        ''If iOpcion = 1 Then
                        'eBanco.fCodigo(gEmpresa)
                        'sCodigoRegistro = eBanco.sCodFuturo
                        'txtCodigo.Text = Trim(sCodigoRegistro)
                        ''Else
                        sCodigoRegistro = Trim(txtCodigo.Text)
                        ''End If
                        iResultado = eBanco.fGrabar(sCodigoRegistro, Convert.ToString(Me.txtDescripcion.Text.Trim), Convert.ToString(Me.txtAbreviado.Text.Trim), Convert.ToString(Me.txtRuc.Text.Trim), Convert.ToString(Me.txtDireccion.Text.Trim), Convert.ToString(Me.txtTelefono.Text.Trim), Convert.ToString(Me.txtFax.Text.Trim), Convert.ToString(Me.txtEmail.Text.Trim), Convert.ToString(Me.txtWeb.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue), iOpcion, gEmpresa, Trim(cboTipoAcreedor.SelectedValue), Trim(cboMoneda.SelectedValue))
                        If iResultado > 0 Then
                            mMostrarGrilla()
                            'Timer2.Enabled = True
                            'v_CancelarReg()
                        End If
                        sTab = 0
                        'TabControl1.TabPages(0).Focus()
                        'frmPrincipal.sFlagGrabar = "0"
                        'End If
                    End If
                End If
            Else
                'eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                If Len(Trim(txtCodigo.Text)) = 0 Then
                    MessageBox.Show("Ingrese el C�digo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodigo.Focus()
                    Exit Sub
                End If

                If cboTipoAcreedor.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Anexo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoAcreedor.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtDescripcion.Text)) = 0 Then
                    MessageBox.Show("Ingrese una Descripcion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDescripcion.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtAbreviado.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Nombre Abreviado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtAbreviado.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtRuc.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Ruc", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtRuc.Focus()
                    Exit Sub
                End If

                If cboMoneda.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Moneda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboMoneda.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtDireccion.Text)) = 0 Then
                    MessageBox.Show("Ingrese la Direccion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDireccion.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtTelefono.Text)) = 0 Then
                    MessageBox.Show("Ingrese el numero de Telefono", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtTelefono.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtFax.Text)) = 0 Then
                    MessageBox.Show("Ingrese el numero de Fax", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtFax.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtEmail.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Email", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtEmail.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtWeb.Text)) = 0 Then
                    MessageBox.Show("Ingrese la Direccion Web", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtWeb.Focus()
                    Exit Sub
                End If
                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iActivo As Integer
            If sTab = 0 Then
                Try
                    If Me.dgvLista.Rows.Count > 0 Then
                        If Fila("Estados") = "Activo" Then
                            iActivo = 5
                            Mensaje = "�Desea Anular?"
                        End If
                        If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("Codigo") & "   " & Chr(13) & "Descripci�n: " & Fila("Descripcion"), "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                            eBanco = New clsBanco
                            iResultado = eBanco.fEliminar(Fila("Codigo"), iActivo, gEmpresa)
                            If iResultado = 1 Then
                                mMostrarGrilla()
                            End If
                        Else
                            Exit Sub
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eRegistroDocumento = New clsRegistroDocumento
        mMostrarGrilla()
        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        dgvLista.Focus()
        cboTipoAcreedor.DataSource = eRegistroDocumento.fListarAcreedor2(gEmpresa)
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboTipoAcreedor.ValueMember = "TipoAnaliticoCodigo"
            cboTipoAcreedor.DisplayMember = "Descrip"
            cboTipoAcreedor.SelectedIndex = -1
        End If
        cboMoneda.DataSource = eRegistroDocumento.fListarTipMoneda2()
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = -1
        End If
        dgvLista.Width = 955
        dgvLista.Height = 402
    End Sub

    'Function SoloNumeros(ByVal Keyascii As Short) As Short
    '    If InStr("1234567890", Chr(Keyascii)) = 0 Then
    '        SoloNumeros = 0
    '    Else
    '        SoloNumeros = Keyascii
    '    End If
    '    Select Case Keyascii
    '        Case 8
    '            SoloNumeros = Keyascii
    '        Case 13
    '            SoloNumeros = Keyascii
    '    End Select
    'End Function


    Private Sub mMostrarGrilla()
        eBanco = New clsBanco
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eBanco.fListar(iEstado01, iEstado02, gEmpresa)
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTable
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eBanco.iNroRegistros
        If eBanco.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("Codigo").ToString
                Me.txtDescripcion.Text = Fila("Descripcion").ToString
                Me.txtAbreviado.Text = Fila("Abreviado").ToString
                Me.txtRuc.Text = Fila("Ruc").ToString
                Me.txtDireccion.Text = Fila("Direccion").ToString
                Me.txtTelefono.Text = Fila("Telefono").ToString
                Me.txtFax.Text = Fila("Fax").ToString
                Me.txtEmail.Text = Fila("Email").ToString
                Me.txtWeb.Text = Fila("Web").ToString
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
                cboMoneda.SelectedValue = Trim(Fila("CodMoneda").ToString())
                cboTipoAcreedor.SelectedValue = Trim(Fila("TipoAnaliticoCodigo").ToString())
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtDescripcion.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text) > 0 And rdbRuc.Checked = True Then
            Dim Ruc As String = ""
            txtBusqueda.MaxLength = 11
            'txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
            eBanco = New clsBanco
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            Ruc = Trim(txtBusqueda.Text)
            dtTable = eBanco.fBuscarBancos(0, gEmpresa, Ruc, "", 0)
            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eBanco.iNroRegistros
            If eBanco.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text) > 0 And rdbDes.Checked = True Then
            txtBusqueda.MaxLength = 25
            'txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno

            Dim sDescripcion As String = ""
            eBanco = New clsBanco
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            sDescripcion = Trim(txtBusqueda.Text)
            dtTable = eBanco.fBuscarBancos(1, gEmpresa, "", sDescripcion, 0)
            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eBanco.iNroRegistros
            If eBanco.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text) = 0 Then
            eBanco = New clsBanco
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eBanco.fBuscarBancos(2, gEmpresa, "", "", 0)
            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eBanco.iNroRegistros
            If eBanco.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If
    End Sub

    Private Sub rdbRuc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbRuc.CheckedChanged
        If rdbRuc.Checked = True Then
            txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
            txtBusqueda.Focus()
        End If
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        Panel2.Visible = False
    End Sub

    Private Sub rdbDes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDes.CheckedChanged
        If rdbDes.Checked = True Then
            txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
            txtBusqueda.Focus()
        End If
    End Sub

    Private Sub txtTelefono_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefono.KeyPress
        'en la siguiente l�nea de c�digo se comprueba si el caracter es d�gito...
        If (Not Char.IsDigit(e.KeyChar)) Then
            Dim caracter As Char = e.KeyChar
            If e.KeyChar.ToString <> "/" And e.KeyChar.ToString <> "-" And e.KeyChar.ToString <> "(" And e.KeyChar.ToString <> ")" And (caracter = ChrW(Keys.Space)) = False Then
                If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                    e.Handled = True 'esto invalida la tecla pulsada
                End If
            End If
            
        End If
    End Sub

    Private Sub txtFax_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFax.KeyPress
        'en la siguiente l�nea de c�digo se comprueba si el caracter es d�gito...
        If (Not Char.IsDigit(e.KeyChar)) Then
            Dim caracter As Char = e.KeyChar
            If e.KeyChar.ToString <> "/" And e.KeyChar.ToString <> "-" And e.KeyChar.ToString <> "(" And e.KeyChar.ToString <> ")" And (caracter = ChrW(Keys.Space)) = False Then
                If Not (e.KeyChar = Convert.ToChar(Keys.Back)) Then
                    e.Handled = True 'esto invalida la tecla pulsada
                End If
            End If

        End If
    End Sub
End Class
