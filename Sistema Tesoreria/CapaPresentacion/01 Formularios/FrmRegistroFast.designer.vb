<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRegistroFast
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmRegistroFast))
        Me.gbRegistroProveedor = New System.Windows.Forms.GroupBox
        Me.txtRUC = New System.Windows.Forms.TextBox
        Me.txtReferencia = New System.Windows.Forms.TextBox
        Me.txtDescripcion = New System.Windows.Forms.TextBox
        Me.txtCodigo = New System.Windows.Forms.TextBox
        Me.lblDescTipoAnex = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnSalir = New System.Windows.Forms.Button
        Me.btnGrabar = New System.Windows.Forms.Button
        Me.txtTipoAnexo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.gbRegistroProveedor.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbRegistroProveedor
        '
        Me.gbRegistroProveedor.BackColor = System.Drawing.Color.WhiteSmoke
        Me.gbRegistroProveedor.Controls.Add(Me.txtRUC)
        Me.gbRegistroProveedor.Controls.Add(Me.txtReferencia)
        Me.gbRegistroProveedor.Controls.Add(Me.txtDescripcion)
        Me.gbRegistroProveedor.Controls.Add(Me.txtCodigo)
        Me.gbRegistroProveedor.Controls.Add(Me.lblDescTipoAnex)
        Me.gbRegistroProveedor.Controls.Add(Me.BeLabel5)
        Me.gbRegistroProveedor.Controls.Add(Me.GroupBox1)
        Me.gbRegistroProveedor.Controls.Add(Me.txtTipoAnexo)
        Me.gbRegistroProveedor.Controls.Add(Me.BeLabel4)
        Me.gbRegistroProveedor.Controls.Add(Me.BeLabel3)
        Me.gbRegistroProveedor.Controls.Add(Me.BeLabel2)
        Me.gbRegistroProveedor.Controls.Add(Me.BeLabel1)
        Me.gbRegistroProveedor.Location = New System.Drawing.Point(7, 1)
        Me.gbRegistroProveedor.Name = "gbRegistroProveedor"
        Me.gbRegistroProveedor.Size = New System.Drawing.Size(430, 171)
        Me.gbRegistroProveedor.TabIndex = 37
        Me.gbRegistroProveedor.TabStop = False
        '
        'txtRUC
        '
        Me.txtRUC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRUC.Location = New System.Drawing.Point(87, 129)
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.Size = New System.Drawing.Size(75, 20)
        Me.txtRUC.TabIndex = 4
        '
        'txtReferencia
        '
        Me.txtReferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtReferencia.Location = New System.Drawing.Point(87, 101)
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(329, 20)
        Me.txtReferencia.TabIndex = 3
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.Location = New System.Drawing.Point(88, 74)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(329, 20)
        Me.txtDescripcion.TabIndex = 2
        '
        'txtCodigo
        '
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.Location = New System.Drawing.Point(88, 48)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(75, 20)
        Me.txtCodigo.TabIndex = 1
        '
        'lblDescTipoAnex
        '
        Me.lblDescTipoAnex.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblDescTipoAnex.AutoSize = True
        Me.lblDescTipoAnex.BackColor = System.Drawing.Color.Transparent
        Me.lblDescTipoAnex.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescTipoAnex.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblDescTipoAnex.Location = New System.Drawing.Point(131, 22)
        Me.lblDescTipoAnex.Name = "lblDescTipoAnex"
        Me.lblDescTipoAnex.Size = New System.Drawing.Size(0, 13)
        Me.lblDescTipoAnex.TabIndex = 113
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(13, 108)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(59, 13)
        Me.BeLabel5.TabIndex = 112
        Me.BeLabel5.Text = "Referencia"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnSalir)
        Me.GroupBox1.Controls.Add(Me.btnGrabar)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.GroupBox1.Location = New System.Drawing.Point(327, 127)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(90, 35)
        Me.GroupBox1.TabIndex = 111
        Me.GroupBox1.TabStop = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.Location = New System.Drawing.Point(49, 9)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(37, 23)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnGrabar
        '
        Me.btnGrabar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGrabar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGrabar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.Location = New System.Drawing.Point(7, 9)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(37, 23)
        Me.btnGrabar.TabIndex = 0
        Me.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnGrabar.UseVisualStyleBackColor = False
        '
        'txtTipoAnexo
        '
        Me.txtTipoAnexo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTipoAnexo.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoAnexo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTipoAnexo.Enabled = False
        Me.txtTipoAnexo.ForeColor = System.Drawing.Color.Black
        Me.txtTipoAnexo.KeyEnter = True
        Me.txtTipoAnexo.Location = New System.Drawing.Point(88, 18)
        Me.txtTipoAnexo.MaxLength = 2
        Me.txtTipoAnexo.Name = "txtTipoAnexo"
        Me.txtTipoAnexo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTipoAnexo.ShortcutsEnabled = False
        Me.txtTipoAnexo.Size = New System.Drawing.Size(36, 20)
        Me.txtTipoAnexo.TabIndex = 0
        Me.txtTipoAnexo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(13, 136)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(30, 13)
        Me.BeLabel4.TabIndex = 3
        Me.BeLabel4.Text = "RUC"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(13, 79)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel3.TabIndex = 2
        Me.BeLabel3.Text = "Descripcion"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(13, 51)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(73, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Codigo Anexo"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(13, 21)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(76, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Tipo de Anexo"
        '
        'Timer1
        '
        '
        'FrmRegistroFast
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(445, 181)
        Me.Controls.Add(Me.gbRegistroProveedor)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmRegistroFast"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Registro de Nuevo Anexo"
        Me.gbRegistroProveedor.ResumeLayout(False)
        Me.gbRegistroProveedor.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbRegistroProveedor As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents txtTipoAnexo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblDescTipoAnex As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents txtReferencia As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtRUC As System.Windows.Forms.TextBox
End Class
