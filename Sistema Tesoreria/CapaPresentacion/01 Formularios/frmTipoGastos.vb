Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmTipoGasto
    Dim WithEvents cmr As CurrencyManager
    Private eTipoGastos As clsTipoGastos
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Dim NumFila As Integer
    Dim Fila As DataRow
    Dim iOpcion As Integer

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmTipoGasto = Nothing
    Public Shared Function Instance() As frmTipoGasto
        If frmInstance Is Nothing Then
            frmInstance = New frmTipoGasto
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmTipoGastos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmTipoGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mMostrarGrilla()
        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1

        cboEstados.DataSource = eTempo.fColEstados
        cboEstados.ValueMember = "Col02"
        cboEstados.DisplayMember = "Col01"
        cboEstados.SelectedIndex = -1
        dgvLista.Focus()
        dgvLista.Width = 391
        dgvLista.Height = 285
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If
        Panel2.Visible = True
        cboEstados.SelectedIndex = -1
        txtBusqueda.Clear()
        txtBusqueda.Focus()
    End Sub

    Private Sub mMostrarGrilla()
        eTipoGastos = New clsTipoGastos
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = eTipoGastos.fListar(gEmpresa)
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTableLista
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastos.iNroRegistros
        If eTipoGastos.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        iOpcion = 1
        LimpiarControles()
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False
        txtDescripcion.Enabled = True
        txtDescripcion.Focus()
    End Sub

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtDescripcion.Clear()
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg

        iOpcion = 2
        VerPosicion()
        txtDescripcion.Enabled = True
        cboEstado.Enabled = True

    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg

        VerPosicion()
        txtDescripcion.Enabled = False
        cboEstado.Enabled = False

    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                txtCodigo.Text = Trim(Fila("IdTipoGasto").ToString)
                txtDescripcion.Text = Trim(Fila("DescTGasto").ToString)
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""
        eTempo = New clsPlantTempo
        eTipoGastos = New clsTipoGastos
        If Len(txtDescripcion.Text.Trim) > 0 Then
            If iOpcion = 1 Then
                Mensaje = "Grabar"
            ElseIf iOpcion = 2 Then
                Mensaje = "Actualizar"
            End If
            If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                frmPrincipal.sFlagGrabar = "1"
                If iOpcion = 1 Then
                    eTipoGastos.fCodigo(gEmpresa)
                    sCodigoRegistro = eTipoGastos.sCodFuturo
                    txtCodigo.Text = sCodigoRegistro
                Else
                    sCodigoRegistro = Trim(txtCodigo.Text)
                End If
                iResultado = eTipoGastos.fGrabar(iOpcion, sCodigoRegistro, Trim(txtDescripcion.Text), gEmpresa, cboEstado.SelectedValue)
                If iResultado > 0 Then
                    mMostrarGrilla()
                End If
            End If
        Else
            If Len(txtDescripcion.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese la Descripcion", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtDescripcion.Focus()
                Exit Sub
            End If
            'eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
            frmPrincipal.sFlagGrabar = "0"
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iOpcionDel As Integer
            Try
                If dgvLista.Rows.Count > 0 Then
                    If Fila("Estados") = "Activo" Then
                        iOpcionDel = 6
                        Mensaje = "�Desea Anular?"
                    End If
                    If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Trim(Fila("IdTipoGasto").ToString) & "   " & Chr(13) & "Descripci�n: " & Trim(Fila("DescTGasto").ToString), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        eTipoGastos = New clsTipoGastos
                        iResultado = eTipoGastos.fEliminar(iOpcionDel, Trim(Fila("IdTipoGasto").ToString), gEmpresa)
                        If iResultado = 1 Then
                            mMostrarGrilla()
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
            End Try
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        ' Else
        'Exit Sub
        'End If
    End Sub

    Private Sub BeLabel7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel7.Click
        Panel2.Visible = False
    End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text.Trim) > 0 Then
            eTipoGastos = New clsTipoGastos
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoGastos.fBuscaTipoGastos(4, Trim(txtBusqueda.Text), gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastos.iNroRegistros
            If eTipoGastos.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text.Trim) = 0 Then
            eTipoGastos = New clsTipoGastos
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoGastos.fBuscaTipoGastos(5, "", gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastos.iNroRegistros
            If eTipoGastos.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If
    End Sub

    Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
        If (cboEstados.SelectedIndex > -1) Then
            Try
                eTipoGastos = New clsTipoGastos
                eTempo = New clsPlantTempo
                dtTable = New DataTable
                Dim sValorEstado As String = ""
                sValorEstado = cboEstados.SelectedValue.ToString
                If sValorEstado <> "CapaDatos.clsPlantTempo" Then
                    dtTable = eTipoGastos.fBuscaTipoGastos(7, "", gEmpresa, cboEstados.SelectedValue)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastos.iNroRegistros
                    If eTipoGastos.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub
End Class

