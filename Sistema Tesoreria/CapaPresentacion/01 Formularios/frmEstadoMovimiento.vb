Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmEstadoMovimiento
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer '18/06/2007: Modificacion Realizada
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Private eEstadoMovimiento As clsEstadoMovimiento
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmEstadoMovimiento = Nothing
    Public Shared Function Instance() As frmEstadoMovimiento
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmEstadoMovimiento
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        Me.txtCodigo.Clear()
        Me.txtEstMov.Clear()
    End Sub

    Private Sub DesabilitarControles()
        Me.txtEstMov.Enabled = False
        cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        Me.txtEstMov.Enabled = True
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        Panel2.Visible = True
        cboEstados.SelectedIndex = -1
        txtBusqueda.Clear()
        txtBusqueda.Focus()

        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            cboEstado.SelectedIndex = 0
            cboEstado.Enabled = False
            'Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            HabilitarControles()
            cboEstado.Enabled = True
            'Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        ' If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        ' Else
        ' Exit Sub
        ' End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        'Dim iDuplicado As Int32
        Dim sCodigoActual As String = ""
        eTempo = New clsPlantTempo
        eEstadoMovimiento = New clsEstadoMovimiento
        Dim Mensaje As String = ""

        'If sTab = 1 Then
        If Len(Trim(txtEstMov.Text)) > 0 And cboEstado.SelectedIndex > -1 Then
            If iOpcion = 1 Then
                Mensaje = "Grabar"
            ElseIf iOpcion = 2 Then
                Mensaje = "Actualizar"
            End If
            If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                frmPrincipal.sFlagGrabar = "1"
                If iOpcion = 1 Then
                    eEstadoMovimiento.fCodigo()
                    sCodigoActual = eEstadoMovimiento.sCodFuturo
                Else
                    sCodigoActual = Trim(Me.txtCodigo.Text)
                End If
                iResultado = eEstadoMovimiento.fGrabar(sCodigoActual, Convert.ToString(Me.txtEstMov.Text.Trim), cboEstado.SelectedValue, iOpcion)
                If iResultado > 0 Then
                    mMostrarGrilla()
                    Timer2.Enabled = True
                End If
                sTab = 0
            End If

        Else
            'iDuplicado = eEstadoMovimiento.fBuscarDoble(Convert.ToString(Me.txtEstMov.Text.Trim), Convert.ToString(Me.txtCodigo.Text.Trim)) 'Modificado el 05/06
            'If iDuplicado > 0 Then 'Modificado el 05/06
            'Exit Sub 'Modificado el 05/06
            'End If 'Modificado el 05/06
            'eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
            frmPrincipal.sFlagGrabar = "0"
            If Len(txtEstMov.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese la Descripcion del Estado", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtEstMov.Focus()
                Exit Sub
            End If
            If cboEstado.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Estado", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboEstado.Focus()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()

        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iActivo As Integer
            If sTab = 0 Then
                Try
                    If Me.dgvLista.Rows.Count > 0 Then
                        iActivo = 3
                        If (MessageBox.Show("�Desea Anular?" & Chr(13) & "C�digo: " & Fila("IdEstadoMovimiento") & "   " & Chr(13) & "Descripci�n: " & Fila("DescripcionMovimiento"), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                            eEstadoMovimiento = New clsEstadoMovimiento
                            iResultado = eEstadoMovimiento.fEliminar(Fila("IdEstadoMovimiento"), cboEstado.SelectedValue, iActivo)
                            If iResultado = 1 Then
                                mMostrarGrilla()
                            End If
                        Else
                            Exit Sub
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmEstadoMovimiento_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()
        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1

        cboEstados.DataSource = eTempo.fColEstados
        cboEstados.ValueMember = "Col02"
        cboEstados.DisplayMember = "Col01"
        cboEstados.SelectedIndex = -1
        dgvLista.Focus()
        'frmPrincipal.fBuscar = False 'Desabilita los botones de buscar
        'frmPrincipal.fExportar = False 'Desabilita los botones de exportar
        'frmPrincipal.fImprimir = False 'Desabilita los botones de imprimir
    End Sub

    Private Sub mMostrarGrilla()
        eEstadoMovimiento = New clsEstadoMovimiento
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = eEstadoMovimiento.fListar(iEstado01, iEstado02)
        Me.dgvLista.AutoGenerateColumns = False
        Me.dgvLista.DataSource = dtTableLista
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        'VerPosicion()
        Me.stsTotales.Items(0).Text = "Total de Registros= " & eEstadoMovimiento.iNroRegistros
        If eEstadoMovimiento.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdEstadoMovimiento").ToString
                Me.txtEstMov.Text = Fila("DescripcionMovimiento")
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtEstMov.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text.Trim) > 0 Then
            eEstadoMovimiento = New clsEstadoMovimiento
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eEstadoMovimiento.fBuscaEstadoMov(7, Trim(txtBusqueda.Text), gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eEstadoMovimiento.iNroRegistros
            If eEstadoMovimiento.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text.Trim) = 0 Then
            eEstadoMovimiento = New clsEstadoMovimiento
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eEstadoMovimiento.fBuscaEstadoMov(8, "", gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eEstadoMovimiento.iNroRegistros
            If eEstadoMovimiento.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If
    End Sub

    Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
        If (cboEstados.SelectedIndex > -1) Then
            Try
                eEstadoMovimiento = New clsEstadoMovimiento
                eTempo = New clsPlantTempo
                dtTable = New DataTable
                Dim sValorEstado As String = ""
                sValorEstado = cboEstados.SelectedValue.ToString
                If sValorEstado <> "CapaDatos.clsPlantTempo" Then
                    dtTable = eEstadoMovimiento.fBuscaEstadoMov(9, "", gEmpresa, cboEstados.SelectedValue)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eEstadoMovimiento.iNroRegistros
                    If eEstadoMovimiento.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub BeLabel7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel7.Click
        Panel2.Visible = False
    End Sub

End Class
