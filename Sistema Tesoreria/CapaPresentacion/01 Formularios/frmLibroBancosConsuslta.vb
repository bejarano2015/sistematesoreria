Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmLibroBancosConsuslta

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmLibroBancosConsuslta = Nothing
    Public Shared Function Instance() As frmLibroBancosConsuslta
        If frmInstance Is Nothing Then
            frmInstance = New frmLibroBancosConsuslta
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmLibroBancosConsuslta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub



    Private Sub frmLibroBancosConsuslta_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eChequera As clsChequera
    Private eLibroBancos As clsLibroBancos
    Private eTempo As clsPlantTempo

    Dim iOpcion As Int16 = 0
    Dim sVisualizarDatos As String = ""
    Dim sCargarCargos As String = ""
    Private eITF As clsITF

    Public sCodigoDetPosicion As String = ""

    Private Sub frmLibroBancosConsuslta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        eTempo = New clsPlantTempo

        eChequera = New clsChequera
        cboBancos.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        If eChequera.iNroRegistros > 0 Then
            cboBancos.ValueMember = "IdBanco"
            cboBancos.DisplayMember = "NombreBanco"
            cboBancos.SelectedIndex = -1
        End If

        'eChequera = New clsChequera
        'cboTipoCargo.DataSource = eChequera.fListarTiposCuenta()
        'If eChequera.iNroRegistros > 0 Then
        'cboTipoCargo.ValueMember = "IdTipoCuenta"
        'cboTipoCargo.DisplayMember = "Abreviado"
        'cboTipoCargo.SelectedIndex = -1
        'End If


        'btnGrabar.Enabled = False
        'btnCancelar.Enabled = False
        'btnGrabar.Enabled = False
        'btnNuevo.Enabled = False
        'btnBuscar.Enabled = False
        'btnEliminar.Enabled = False


        'Me.ToolTip1.SetToolTip(Me.btnNuevo, "Nuevo")
        'Me.ToolTip1.SetToolTip(Me.btnGrabar, "Grabar")
        'Me.ToolTip1.SetToolTip(Me.btnCancelar, "Cancelar")
        'Me.ToolTip1.SetToolTip(Me.btnBuscar, "Buscar")
        'Me.ToolTip1.SetToolTip(Me.btnEliminar, "Eliminar")
        'Me.ToolTip1.SetToolTip(Me.txtImporte, "Enter para Grabar")
        cboAno.Text = Year(Now)
        Label1.Text = ""
        Label2.Text = ""

    End Sub

    Dim CuentaEncargada As String = ""
    Dim sTipoCuenta As String = ""
    Dim NumeroCuentaEncargada As String = ""

    Private Sub cboBancos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBancos.SelectedIndexChanged
        Dim IdBanco As String = ""
        If (cboBancos.SelectedIndex > -1) Then
            Try
                IdBanco = cboBancos.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuentas.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    cboCuentas.ValueMember = "IdCuenta"
                    cboCuentas.DisplayMember = "NumeroCuenta"
                    cboCuentas.SelectedIndex = -1

                    ''cboCargoAnex.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    ''cboCargoAnex.ValueMember = "IdCuenta"
                    ''cboCargoAnex.DisplayMember = "NumeroCuenta"
                    ''cboCargoAnex.SelectedIndex = -1

                    lblMoneda.Text = ""
                    lblCodMoneda.Text = ""
                    lblTipoCta.Text = ""


                    Dim dtCuentaEn As DataTable
                    dtCuentaEn = New DataTable
                    dtCuentaEn = eChequera.fListarCuentasEncargada(gEmpresa, IdBanco)
                    If dtCuentaEn.Rows.Count = 1 Then
                        CuentaEncargada = dtCuentaEn.Rows(0).Item("IdCuenta")
                        NumeroCuentaEncargada = dtCuentaEn.Rows(0).Item("NumeroCuenta")
                        lblCtaEn.Text = NumeroCuentaEncargada 'CuentaEncargada & "  - " & NumeroCuentaEncargada
                    ElseIf dtCuentaEn.Rows.Count = 0 Then
                        CuentaEncargada = ""
                        NumeroCuentaEncargada = ""
                        lblCtaEn.Text = ""
                    End If

                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            CuentaEncargada = ""
            NumeroCuentaEncargada = ""
            lblCtaEn.Text = ""
        End If
        Limpiar()
    End Sub

    Private Sub cboCuentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuentas.SelectedIndexChanged
        Dim dtTable2 As DataTable
        Dim IdCuenta As String = ""
        If (cboCuentas.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuentas.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtTable2 = New DataTable
                    dtTable2 = eChequera.fListarMoneda(IdCuenta, gEmpresa)
                    lblMoneda.Text = dtTable2.Rows(0).Item("MonDescripcion")
                    lblTipoCta.Text = dtTable2.Rows(0).Item("Descripcion")
                    lblCodMoneda.Text = dtTable2.Rows(0).Item("MonCodigo")
                    sTipoCuenta = dtTable2.Rows(0).Item("TipoCuenta")
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
        Limpiar()
    End Sub

    Private Sub cboAno_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAno.SelectedIndexChanged
        Limpiar()
    End Sub

    Dim sCodigoLibroAsignado As String = ""
    Dim sCuentaAsignado As String = ""
    Dim SaldoPrendaBD As Double = 0
    Dim SaldoGarantiaBD As Double = 0
    Dim SaldoDepPlazBD As Double = 0
    Dim SaldoRetencionBD As Double = 0

    Private Sub cboMes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMes.SelectedIndexChanged
        Dim Cobrados As Double = 0
        Dim NoCobrados As Double = 0

        Label1.Text = ""
        If cboBancos.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Banco", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboBancos.Focus()
            Exit Sub
        End If
        If cboCuentas.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCuentas.Focus()
            Exit Sub
        End If
        If cboAno.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un A�o", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboAno.Focus()
            Exit Sub
        End If

        sVisualizarDatos = "NO"

        If cboMes.SelectedIndex > -1 Then
            Dim IdLibro As String = ""
            Dim SaldoInicial As Double = 0
            eLibroBancos = New clsLibroBancos
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            Dim dtLibroDet As DataTable
            dtLibroDet = New DataTable
            dtDatos = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
            If dtDatos.Rows.Count > 0 Then
                IdLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab"))
                txtCod.Text = IdLibro
                SaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial"))

                'SaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial"))
                'SaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial"))

                'Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI"))
                'NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO"))

                txtSaldoInicial.Text = Format(Convert.ToDouble(SaldoInicial), "#,##0.00")
                dgvLibroDet.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLibroDet.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvLibroDet.Columns(4).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                If dgvLibroDet.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvLibroDet.RowCount - 1
                        dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
                    Next
                    'dtLibroDet.Clear()
                    'dgvLibroDet.DataSource = ""
                End If

                Dim saldo As Double = 0
                saldo = SaldoInicial
                'Dim Cobrados As Double = 0
                'Dim NoCobrados As Double = 0
                'dtLibroDet.Clear()

                dtLibroDet = eLibroBancos.TraerLibroDet(2, IdLibro, gEmpresa)
                If dtLibroDet.Rows.Count > 0 Then
                    Label1.Text = "Total de Registros : " & dtLibroDet.Rows.Count
                    'Dim SaldoPrenda As Double = 0
                    'Dim SaldoGarantia As Double = 0
                    'Dim SaldoDepPla As Double = 0
                    'Dim SaldoRetencion As Double = 0
                    For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                        dgvLibroDet.Rows.Add()

                        'dgvLibroDet.Rows(y).Cells("Column3").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdMovimiento").ToString) = True, "", dtTable.Rows(y).Item("IdMovimiento").ToString) 'Microsoft.VisualBasic.Left(dtDetalle.Rows(y).Item("FechaDetMov"), 10)


                        For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                            If z = 7 Then ' chek
                                If dtLibroDet.Rows(y).Item(7).ToString = "1" Then
                                    dgvLibroDet.Rows(y).Cells(7).Value = True
                                    dgvLibroDet.Rows(y).Cells(13).Value = "1"
                                    dgvLibroDet.Rows(y).Cells(7).ReadOnly = False
                                    dgvLibroDet.Rows(y).Cells(15).ReadOnly = False
                                    If Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        gestionaResaltados2(dgvLibroDet, y, Color.Aqua)
                                    End If
                                ElseIf dtLibroDet.Rows(y).Item(7).ToString = "0" Then
                                    dgvLibroDet.Rows(y).Cells(7).Value = False
                                    dgvLibroDet.Rows(y).Cells(13).Value = "0"
                                    dgvLibroDet.Rows(y).Cells(7).ReadOnly = True
                                    dgvLibroDet.Rows(y).Cells(15).ReadOnly = True
                                    If Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        gestionaResaltados2(dgvLibroDet, y, Color.Silver)
                                    End If
                                End If
                            ElseIf z = 13 Then
                                dgvLibroDet.Rows(y).Cells(12).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 14 Then
                                dgvLibroDet.Rows(y).Cells(14).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 15 Then
                                dgvLibroDet.Rows(y).Cells(16).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 16 Then
                                dgvLibroDet.Rows(y).Cells(17).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 17 Then
                                dgvLibroDet.Rows(y).Cells(18).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 18 Then
                                dgvLibroDet.Rows(y).Cells(19).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 19 Then
                                dgvLibroDet.Rows(y).Cells(20).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 20 Then
                                dgvLibroDet.Rows(y).Cells(21).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 21 Then
                                dgvLibroDet.Rows(y).Cells(22).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 22 Then
                                dgvLibroDet.Rows(y).Cells(23).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 23 Then
                                dgvLibroDet.Rows(y).Cells(24).Value = dtLibroDet.Rows(y).Item(z)
                            ElseIf z = 24 Then
                                dgvLibroDet.Rows(y).Cells(25).Value = dtLibroDet.Rows(y).Item(z)
                                Dim TipoCargox As String = Trim(dtLibroDet.Rows(y).Item(z).ToString)
                                If TipoCargox = "00001" Or TipoCargox = "00002" Or TipoCargox = "00003" Or TipoCargox = "00004" Or TipoCargox = "00005" Then
                                    gestionaResaltados3(dgvLibroDet, y, Color.MistyRose)
                                End If
                            ElseIf z = 25 Then
                                dgvLibroDet.Rows(y).Cells(26).Value = dtLibroDet.Rows(y).Item(z)
                                If dgvLibroDet.Rows(y).Cells(26).Value = "1" Then
                                    gestionaResaltados4(dgvLibroDet, y, Color.Yellow)
                                End If
                            ElseIf z = 26 Then
                                'dgvLibroDet.Rows(y).Cells(27).Value = dtLibroDet.Rows(y).Item(z)
                                'If dgvLibroDet.Rows(y).Cells(27).Value = "1" Then
                                '    gestionaResaltados4(dgvLibroDet, y, Color.Yellow)
                                'End If

                                If dtLibroDet.Rows(y).Item(z) = "1" Then
                                    dgvLibroDet.Rows(y).Cells(27).Value = True
                                    dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("ConciliadoS").Value = 1
                                Else
                                    dgvLibroDet.Rows(y).Cells(27).Value = False
                                    dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("ConciliadoS").Value = 0
                                End If
                            ElseIf z = 0 Then 'fecha
                                dgvLibroDet.Rows(y).Cells(0).Value = Microsoft.VisualBasic.Left(dtLibroDet.Rows(y).Item(0), 10)
                            ElseIf z = 11 Then 'importe
                                dgvLibroDet.Rows(y).Cells(z).Value = dtLibroDet.Rows(y).Item(z)
                                If dtLibroDet.Rows(y).Item(10).ToString = "D" Then
                                    saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then
                                    saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    If Trim(dtLibroDet.Rows(y).Item("Marca").ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    End If
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    If Trim(dtLibroDet.Rows(y).Item("Marca").ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    End If
                                End If
                                dgvLibroDet.Rows(y).Cells(6).Value = Format(saldo, "#,##0.00")
                            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 8 Or z = 9 Or z = 10 Then '1 2 3 4 5 
                                dgvLibroDet.Rows(y).Cells(z).Value = dtLibroDet.Rows(y).Item(z)
                            End If

                        Next
                    Next


                    'aqui tengo q sumar los cheques cobrados y no cobrados para la conciliacion mensual
                    txtSaldo.Text = Format(saldo, "#,##0.00")
                    txtCobradosSi.Text = Format(Cobrados, "#,##0.00")
                    txtCobradosNo.Text = Format(NoCobrados, "#,##0.00")
                    Dim iresxxxx As Integer = 0
                    'iresxxxx = eLibroBancos.fActSaldoLibro(gEmpresa, IdLibro, Format(SaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today()) 'graba el nuevo saldo del libro


                    'txtSaldoInicial.Text = Format(Convert.ToDouble(SaldoInicial), "#,##0.00")
                    'btnNuevo.Enabled = True
                    'btnBuscar.Enabled = True
                    'btnGrabar.Enabled = False
                    'btnCancelar.Enabled = False
                    'LimpiarControlesDetalle()
                    'DeshabilitarControlesDetalle()
                    iOpcion = 0
                    'btnConciliacion.Enabled = True
                    dgvLibroDet.Focus()
                    'Exit Sub
                Else
                    txtSaldo.Text = Format(saldo, "#,##0.00")
                    txtCobradosSi.Text = Format(Cobrados, "#,##0.00")
                    txtCobradosNo.Text = Format(NoCobrados, "#,##0.00")

                    Dim iresxxxx As Integer = 0
                    'iresxxxx = eLibroBancos.fActSaldoLibro(gEmpresa, IdLibro, Format(SaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today()) 'graba el nuevo saldo del libro


                    'txtSaldoInicial.Text = Format(Convert.ToDouble(SaldoInicial), "#,##0.00")
                    'btnNuevo.Enabled = True
                    'btnBuscar.Enabled = True
                    'btnGrabar.Enabled = False
                    'btnCancelar.Enabled = False
                    'LimpiarControlesDetalle()
                    'DeshabilitarControlesDetalle()
                    iOpcion = 0
                    'btnConciliacion.Enabled = True
                    dgvLibroDet.Focus()
                    'Exit Sub
                End If
                'btnNuevo_Click(sender, e)
                dgvLibroDet.Focus()
            Else
                If dgvLibroDet.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvLibroDet.RowCount - 1
                        dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
                    Next
                End If
                'LimpiarControlesDetalle()
                'DeshabilitarControlesDetalle()
                txtSaldo.Clear()
                txtSaldoInicial.Clear()
                txtCobradosSi.Clear()
                txtCobradosNo.Clear()
                txtCodDet.Clear()
                txtMarca.Clear()
                txtMarca2.Clear()
                txtCod.Clear()
                'btnCancelar.Enabled = False
                'btnGrabar.Enabled = False
                'btnNuevo.Enabled = False
                'btnBuscar.Enabled = False
                'btnEliminar.Enabled = False
                'btnConciliacion.Enabled = False
                MessageBox.Show("No Existe Libro Bancos del Mes de " & cboMes.Text & " / " & cboAno.Text)

                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                'If (MessageBox.Show("No Existe Libro Bancos del Mes de " & cboMes.Text & " / " & cboAno.Text & " �Desea crearlo?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                '    Dim Mes As String = ""
                '    Dim Anio As String = ""
                '    Dim MesAnt As String = ""
                '    Dim AnioAnt As String = ""
                '    Mes = TraerNumeroMes(Trim(cboMes.Text))
                '    MesAnt = Format(Mes - 1, "00") 'resto el mes actual, para usar el mes anterior
                '    Anio = Trim(cboAno.Text)
                '    If Trim(Mes) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y a�o el ACTUAL -1
                '        MesAnt = "12"
                '        AnioAnt = Trim(Anio) - 1
                '    Else
                '        AnioAnt = Anio
                '    End If

                '    Dim dtLibroAnterior As DataTable
                '    dtLibroAnterior = New DataTable
                '    dtLibroAnterior = eLibroBancos.TraerIdLibro(AnioAnt, MesAnt, Trim(cboCuentas.SelectedValue), gEmpresa)
                '    Dim iResultado As Int16 = 0
                '    Dim iResultadoDet2 As Int16 = 0
                '    Dim iResultadoActSaldoLibro As Int16 = 0
                '    Dim SaldoAnterior As Double = 0
                '    Dim sCodigoLibro As String = ""
                '    Dim FechaLibro As String = "01/" & Mes & "/" & Anio
                '    If dtLibroAnterior.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                '        eLibroBancos = New clsLibroBancos
                '        eLibroBancos.fCodigo(gEmpresa)
                '        sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                '        SaldoAnterior = Trim(dtLibroAnterior.Rows(0).Item("Saldo"))

                '        Dim SaldoPrenda As Double = 0
                '        Dim SaldoGarantia As Double = 0
                '        Dim SaldoDepPlaz As Double = 0
                '        Dim SaldoRetencion As Double = 0
                '        SaldoPrenda = Trim(dtLibroAnterior.Rows(0).Item("SaldoPrenda"))
                '        SaldoGarantia = Trim(dtLibroAnterior.Rows(0).Item("SaldoGarantia"))
                '        SaldoDepPlaz = Trim(dtLibroAnterior.Rows(0).Item("SaldoDepPlaz"))
                '        SaldoRetencion = Trim(dtLibroAnterior.Rows(0).Item("SaldoRetencion"))

                '        If sTipoCuenta = "00001" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00002" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoAnterior, SaldoGarantia, SaldoDepPlaz, SaldoRetencion) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00003" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoAnterior, SaldoDepPlaz, SaldoRetencion) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00004" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoAnterior, SaldoRetencion) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00005" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoAnterior) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        End If


                '        'iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, SaldoAnterior, SaldoAnterior, FechaLibro, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '    ElseIf dtLibroAnterior.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                '        Dim x As String = ""
                '        x = InputBox("Ingrese Saldo Inicial del Libro Bancos", "Libro Bancos", "0.00")
                '        If x.Length = 0 Then
                '            MessageBox.Show("Se ha Cancelado la Creacion del Libro Bancos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '            cboMes.SelectedIndex = -1
                '            Exit Sub
                '        End If
                '        While IsNumeric(Trim(x)) = False
                '            MessageBox.Show("Ingrese solo d�gitos como Saldo Inicial", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '            x = InputBox("Ingrese Saldo Inicial del Libro Bancos", "Libro Bancos", "0.00")
                '            If x.Length = 0 Then
                '                MessageBox.Show("Se ha Cancelado la Creacion del Libro Bancos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '                cboMes.SelectedIndex = -1
                '                Exit Sub
                '            End If
                '        End While
                '        eLibroBancos = New clsLibroBancos
                '        eLibroBancos.fCodigo(gEmpresa)
                '        sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                '        SaldoAnterior = 0

                '        If sTipoCuenta = "00001" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00002" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, x, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00003" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, x, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00004" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, 0, x, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        ElseIf sTipoCuenta = "00005" Then
                '            iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuentas.SelectedValue), Mes, Anio, x, x, FechaLibro, 0, 0, 0, x) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                '        End If
                '    End If
                '    If iResultado = 1 Then
                '        cboMes_SelectedIndexChanged(sender, e)
                '        MessageBox.Show("Se ha Creado el Libro con Exito!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    End If
                'Else
                '    cboMes.SelectedIndex = -1
                '    cboMes.Focus()
                '    Exit Sub
                'End If
                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            End If

            'traemos los saldos de los anexos en el libro asignado
            Dim dtLibroAsi As DataTable
            dtLibroAsi = New DataTable
            dtLibroAsi = eLibroBancos.TraerIdLibro(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(CuentaEncargada), gEmpresa)
            If dtLibroAsi.Rows.Count = 1 Then
                sCodigoLibroAsignado = Trim(dtLibroAsi.Rows(0).Item("IdLibroCab"))
                SaldoPrendaBD = Trim(dtLibroAsi.Rows(0).Item("SaldoPrenda"))
                SaldoGarantiaBD = Trim(dtLibroAsi.Rows(0).Item("SaldoGarantia"))
                SaldoDepPlazBD = Trim(dtLibroAsi.Rows(0).Item("SaldoDepPlaz"))
                SaldoRetencionBD = Trim(dtLibroAsi.Rows(0).Item("SaldoRetencion"))
            End If

        End If 'If cboMes.SelectedIndex > -1

        ' btnNuevo_Click(sender, e)
    End Sub



    Private Sub gestionaResaltados2(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(7).Style.BackColor = c
        'visor.Rows(fila).Cells(15).Style.BackColor = c
    End Sub

    Private Sub gestionaResaltados3(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        'visor.Rows(fila).Cells(15).Style.BackColor = c
    End Sub

    Private Sub gestionaResaltados4(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
    End Sub

    Private Sub Limpiar()
        If dgvLibroDet.Rows.Count > 0 Then
            For x As Integer = 0 To dgvLibroDet.RowCount - 1
                dgvLibroDet.Rows.Remove(dgvLibroDet.CurrentRow)
            Next
        End If
        'LimpiarControlesDetalle()
        'DeshabilitarControlesDetalle()
        txtSaldo.Clear()
        txtSaldoInicial.Clear()
        txtCobradosSi.Clear()
        txtCobradosNo.Clear()
        txtCodDet.Clear()
        txtMarca.Clear()
        txtMarca2.Clear()
        txtCod.Clear()

        'btnCancelar.Enabled = False
        'btnGrabar.Enabled = False
        ' btnNuevo.Enabled = False
        ' btnEliminar.Enabled = False
        'btnBuscar.Enabled = False

        ' btnConciliacion.Enabled = False
        cboMes.SelectedIndex = -1
    End Sub

    Private Sub TextBox1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles TextBox1.KeyDown
        Dim np As Integer = 0 'dgvLibroDet.CurrentRow.Index

        Select Case e.KeyCode
            Case Keys.Enter
                If dgvLibroDet.Rows.Count > 0 Then

                    'Dim row As Integer = 1
                    'row = dgvLibroDet.CurrentRow.Index + 1
                    Dim x As Integer
                    'x = dgvLibroDet.FirstDisplayedScrollingRowIndex + 1
                    'For x As Integer = PosicionGrilla To dgvLibroDet.Rows.Count
                    'If x = dgvLibroDet.Rows.Count Then
                    '    x = 0
                    '    'dgvLibroDet.FirstDisplayedScrollingRowIndex = 0
                    'Else
                    '    x = dgvLibroDet.FirstDisplayedScrollingRowIndex + 1
                    'End If

                    For x = dgvLibroDet.FirstDisplayedScrollingRowIndex + 1 To dgvLibroDet.Rows.Count

                        If x = dgvLibroDet.Rows.Count Then
                            x = 0
                            dgvLibroDet.FirstDisplayedScrollingRowIndex = 0
                            TextBox1.Focus()
                            Exit For
                        End If

                        If (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column3").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column4").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column6").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(dgvLibroDet.Rows(x).Cells("Column10").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Then 'Or (UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column4").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Then


                            Dim row As DataGridViewRow
                            For Each row In dgvLibroDet.Rows
                                row.Selected = False
                                dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                            Next

                            'row.Selected = True
                            dgvLibroDet.Rows(x).Selected = True
                            dgvLibroDet.FirstDisplayedScrollingRowIndex = x

                            x = x + 1
                            'dgvLibroDet.Rows(x).Cells("Column3").Value)
                            'TextBox1.Focus()
                            'PosicionGrilla = row.Index + 1
                            'row
                            If x = dgvLibroDet.Rows.Count Then
                                x = 0
                            End If
                            TextBox1.Focus()
                            Exit For
                        End If



                    Next
                    'If x = dgvLibroDet.Rows.Count Then
                    '    x = 0
                    'End If
                    'For x = 0 To 3

                    'Next



                End If
        End Select
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        Dim row As DataGridViewRow

        If Len(Trim(TextBox1.Text)) > 0 Then

            For Each row In dgvLibroDet.Rows
                'If UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*") Then
                row.Selected = False
                dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                'Exit For
                'End If
            Next

            For Each row In dgvLibroDet.Rows
                If (UCase(Trim(row.Cells("Column3").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column4").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column6").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Or (UCase(Trim(row.Cells("Column10").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*")) Then
                    row.Selected = True
                    dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                    'PosicionGrilla = row.Index + 1
                    TextBox1.Focus()
                    Exit For
                End If
            Next

            TextBox1.Focus()

        ElseIf Len(Trim(TextBox1.Text)) = 0 Then
            For Each row In dgvLibroDet.Rows
                'If UCase(Trim(row.Cells("Column5").Value)) Like ("*" & UCase(Trim(TextBox1.Text)) & "*") Then
                row.Selected = False
                dgvLibroDet.FirstDisplayedScrollingRowIndex = row.Index
                'Exit For
                'End If
            Next

            TextBox1.Focus()
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If Len(Trim(txtCod.Text)) > 0 Then
            Dim IdLibroCab As String = ""
            Dim BancoDes As String = ""
            Dim MonedaDes As String = ""
            Dim NroCuentaDes As String = ""

            Dim Fecha As DateTime
            Dim dtResumenLibro As DataTable
            dtResumenLibro = New DataTable
            dtResumenLibro = eLibroBancos.fResumenLibro(cboCuentas.SelectedValue, gEmpresa, TraerNumeroMes(cboMes.Text), Trim(cboAno.Text))

            If dtResumenLibro.Rows.Count > 0 Then
                Fecha = dtResumenLibro.Rows(0).Item("Fecha")
            End If
            'IdLibroCab = dtResumenLibro.Rows(0).Item("IdLibroCab")

            Dim dtCronogramas As DataTable
            dtCronogramas = New DataTable
            dtCronogramas = eLibroBancos.fListarCheqNoCobrados(cboCuentas.SelectedValue, gEmpresa, TraerNumeroMes(cboMes.Text), Trim(cboAno.Text), Fecha)
            If dtResumenLibro.Rows.Count > 0 Then
                IdLibroCab = dtResumenLibro.Rows(0).Item("IdLibroCab")
                'txtSaldoBanco.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
                'txtCheNoCobra.Text = Format(dtResumenLibro.Rows(0).Item("CheCobradosNO"), "#,##0.00")
                'txtCheCobra.Text = Format(dtResumenLibro.Rows(0).Item("CheCobradosSI"), "#,##0.00")
                'txtTotal.Text = Format(dtResumenLibro.Rows(0).Item("CheCobradosNO") - dtResumenLibro.Rows(0).Item("CheCobradosSI"), "#,##0.00")
                BancoDes = Trim(dtResumenLibro.Rows(0).Item("NombreBanco"))
                MonedaDes = Trim(dtResumenLibro.Rows(0).Item("MonDescripcion"))
                NroCuentaDes = Trim(dtResumenLibro.Rows(0).Item("NumeroCuenta"))
                'BeLabel2.Text = "(-) CHEQUES GIRADOS Y NO COBRADOS " & TraerNombreMes(Mes) & " / " & Anio
                'BeLabel3.Text = "(-) CHEQUES COBRADOS EN " & TraerNombreMes(Mes)
            End If





            Dim x As frmReporteLibroBancos2 = frmReporteLibroBancos2.Instance
            x.IdLIbroCab = Trim(IdLibroCab)
            x.Empresa = Trim(gDesEmpresa)
            x.Banco = Trim(BancoDes)
            x.Moneda = Trim(MonedaDes)
            x.NroCuenta = Trim(NroCuentaDes)

            x.Mes = Trim(cboMes.Text)
            x.Anio = Trim(cboAno.Text)
            x.IdCuenta = Trim(cboCuentas.SelectedValue)
            x.FechaFinal = Fecha
            x.SaldoInicialRep = txtSaldoInicial.Text
            x.SaldoFinalRep = txtSaldo.Text
            x.SaldoSegunBanco = 0
            x.Show()
        ElseIf Len(Trim(txtCod.Text)) = 0 Then

            MessageBox.Show("Seleccione un Mes")

        End If
        'eArqueo = New clsArqueo
        'Dim dtIdArqueo As DataTable
        'dtIdArqueo = New DataTable
        'Dim IdArqueo As String

        'dtIdArqueo = eArqueo.ExisteArqueoHoy(Trim(txtCodigo.Text), sIdCaja, sIdArea, gEmpresa)
        'If dtIdArqueo.Rows.Count = 1 Then
        


    End Sub
End Class