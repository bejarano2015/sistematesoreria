Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Public Class frmLibroBancosIngresos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmLibroBancosIngresos = Nothing
    Public Shared Function Instance() As frmLibroBancosIngresos
        If frmInstance Is Nothing Then
            frmInstance = New frmLibroBancosIngresos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmLibroBancosIngresos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eGastosGenerales As clsGastosGenerales
    Private eBusquedaGastos As clsBusquedaGastos


    Private Sub btnVerPrestamos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerPrestamos.Click

        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        Dim RutaValida As String = RutaAppReportes.Substring(0, RutaAppReportes.Length - 19) & "Reportes\"
        Try
            Dim dtTable2 As DataTable
            Dim pIdRetencion As String = ""
            Dim pSerie As String = ""
            dtTable2 = New DataTable
            eGastosGenerales = New clsGastosGenerales
            eBusquedaGastos = New clsBusquedaGastos

            If r1.Checked = True Then

                If chkTodasEmpr.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(0, "", gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If
                If chkTodosBanc.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(1, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If
                If chkTodosCuen.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(2, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If

                If cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex = -1 And cboCuenta.SelectedIndex = -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(1, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                ElseIf cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(2, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                ElseIf cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(3, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(cboCuenta.SelectedValue), Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If


                If dtTable2.Rows.Count > 0 Then
                    If Not Directory.Exists(RutaAppReportes) Then
                        Muestra_Reporte(RutaValida & "rptLibrosIngresosXFecha.rpt", dtTable2, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value, "Titulo;" & "ABONOS POR CUENTAS")
                        'MessageBox.Show("No Existe este Directorio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        'Exit Sub
                    Else
                        Muestra_Reporte(RutaAppReportes & "rptLibrosIngresosXFecha.rpt", dtTable2, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value, "Titulo;" & "ABONOS POR CUENTAS")
                    End If


                End If

            End If

            If r2.Checked = True Then

                If chkTodasEmpr.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(4, "", gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If
                If chkTodosBanc.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(5, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If
                If chkTodosCuen.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(6, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If

                If cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex = -1 And cboCuenta.SelectedIndex = -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(5, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                ElseIf cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(6, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                ElseIf cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(7, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(cboCuenta.SelectedValue), Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If

                If dtTable2.Rows.Count > 0 Then
                    If Not Directory.Exists(RutaAppReportes) Then
                        Muestra_Reporte(RutaValida & "rptLibrosIngresosXFecha.rpt", dtTable2, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value, "Titulo;" & "CHEQUES POR COBRAR")
                        'MessageBox.Show("No Existe este Directorio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        'Exit Sub
                    Else
                        Muestra_Reporte(RutaAppReportes & "rptLibrosIngresosXFecha.rpt", dtTable2, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value, "Titulo;" & "CHEQUES POR COBRAR")
                    End If
                End If

            End If

            If r3.Checked = True Then

                If chkTodasEmpr.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(8, "", gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If
                If chkTodosBanc.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(9, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If
                If chkTodosCuen.Checked = True Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(10, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If

                If cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex = -1 And cboCuenta.SelectedIndex = -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(9, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", "", gEmprRuc, gDesEmpresa, Trim(gUsuario))
                ElseIf cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(10, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, "", Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                ElseIf cboEmpresas.SelectedIndex > -1 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    dtTable2 = eBusquedaGastos.fConsultaIngresosaBancos(11, "", Trim(cboEmpresas.SelectedValue), dtpFechaIni.Value, dtpFechaFin.Value, Trim(cboCuenta.SelectedValue), Trim(cboBanco.SelectedValue), gEmprRuc, gDesEmpresa, Trim(gUsuario))
                End If

                If dtTable2.Rows.Count > 0 Then
                    If Not Directory.Exists(RutaAppReportes) Then
                        Muestra_Reporte(RutaValida & "rptLibrosIngresosXFecha.rpt", dtTable2, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value, "Titulo;" & "MOVIMIENTOS POR IDENTIFICAR")
                        'MessageBox.Show("No Existe este Directorio", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        'Exit Sub
                    Else
                        Muestra_Reporte(RutaAppReportes & "rptLibrosIngresosXFecha.rpt", dtTable2, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value, "Titulo;" & "MOVIMIENTOS POR IDENTIFICAR")
                    End If


                End If


            End If

            'If RD2.Checked = True Then
            '    dtTable2 = eBusquedaGastos.fConsultaGastosEnviados(1, "", gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, cboCuenta.SelectedValue, cboBanco.SelectedValue, gEmprRuc, gDesEmpresa, Trim(gUsuario))
            '    Muestra_Reporte(RutaAppReportes & "rptGastosEnviados.rpt", dtTable2, "TblLibroDiario", "", "Banco;" & Trim(cboBanco.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Cuenta;" & Trim(cboCuenta.Text), "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value & " - Usuario: " & UCase(Trim(glbNameUsuario)))
            'End If

            If dtTable2.Rows.Count = 0 Then

                Dim mensaje As String = ""
                mensaje = "No Se Encontraron Registros."
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        Me.Cursor = Cursors.Default

    End Sub




    Private Sub cboEmpresas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpresas.SelectedIndexChanged
        Dim IdEmpresa As String
        If (cboEmpresas.SelectedIndex > -1) Then
            Try
                IdEmpresa = cboEmpresas.SelectedValue.ToString
                If IdEmpresa <> "System.Data.DataRowView" Then
                    'If rdb1.Checked = True Then
                    'cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa2(gEmpresa, IdBanco)
                    'ElseIf rdb2.Checked = True Then
                    cboBanco.DataSource = eGastosGenerales.fListarBancos(IdEmpresa)
                    'End If
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboBanco.ValueMember = "IdBanco"
                        cboBanco.DisplayMember = "NombreBanco"
                        cboBanco.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboBanco.DataSource = Nothing
        End If
    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Dim IdBanco As String
        If (cboBanco.SelectedIndex > -1) Then
            Try
                IdBanco = cboBanco.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    'If rdb1.Checked = True Then
                    'cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa2(gEmpresa, IdBanco)
                    'ElseIf rdb2.Checked = True Then
                    cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    'End If
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCuenta.ValueMember = "IdCuenta"
                        cboCuenta.DisplayMember = "NumeroCuenta"
                        cboCuenta.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboCuenta.DataSource = Nothing
        End If
    End Sub

  
    Private Sub frmLibroBancosIngresos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eGastosGenerales = New clsGastosGenerales
        cboEmpresas.DataSource = eGastosGenerales.fListarEmpresas(54, gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboEmpresas.ValueMember = "EmprCodigo"
            cboEmpresas.DisplayMember = "EmprDescripcion"
            cboEmpresas.SelectedIndex = -1
        End If
        dtpFechaFin.Text = Now.Date()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        cboEmpresas.Focus()
    End Sub

    Private Sub chkTodasEmpr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodasEmpr.CheckedChanged
        If chkTodasEmpr.Checked = True Then
            cboEmpresas.SelectedIndex = -1
            cboBanco.SelectedIndex = -1
            cboCuenta.SelectedIndex = -1
            cboEmpresas.Enabled = False
            cboBanco.Enabled = False
            cboCuenta.Enabled = False

            chkTodosBanc.Checked = False
            chkTodosCuen.Checked = False
            chkTodosBanc.Enabled = False
            chkTodosCuen.Enabled = False
        ElseIf chkTodasEmpr.Checked = False Then

            cboEmpresas.SelectedIndex = -1
            cboBanco.SelectedIndex = -1
            cboCuenta.SelectedIndex = -1
            cboEmpresas.Enabled = True
            cboBanco.Enabled = True
            cboCuenta.Enabled = True

            chkTodosBanc.Checked = False
            chkTodosCuen.Checked = False
            chkTodosBanc.Enabled = True
            chkTodosCuen.Enabled = True
        End If
    End Sub


    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub chkTodosBanc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodosBanc.CheckedChanged
        If chkTodosBanc.Checked = True Then

            If cboEmpresas.SelectedIndex = -1 Then
                chkTodosBanc.Checked = False
                MessageBox.Show("Seleccione una Empresa", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboEmpresas.Focus()
                Exit Sub
            End If

            'cboEmpresas.SelectedIndex = -1
            cboBanco.SelectedIndex = -1
            cboCuenta.SelectedIndex = -1
            'cboEmpresas.Enabled = False
            cboBanco.Enabled = False
            cboCuenta.Enabled = False

            'chkTodosBanc.Checked = False
            chkTodosCuen.Checked = False
            'chkTodosBanc.Enabled = False
            chkTodosCuen.Enabled = False
        ElseIf chkTodosBanc.Checked = False Then

            'cboEmpresas.SelectedIndex = -1
            cboBanco.SelectedIndex = -1
            cboCuenta.SelectedIndex = -1
            'cboEmpresas.Enabled = True
            cboBanco.Enabled = True
            cboCuenta.Enabled = True

            'chkTodosBanc.Checked = False
            chkTodosCuen.Checked = False
            'chkTodosBanc.Enabled = True
            chkTodosCuen.Enabled = True
        End If
    End Sub

    Private Sub chkTodosCuen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodosCuen.CheckedChanged
        If chkTodosCuen.Checked = True Then


            If cboEmpresas.SelectedIndex = -1 Then
                chkTodosCuen.Checked = False
                MessageBox.Show("Seleccione una Empresa", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboEmpresas.Focus()
                Exit Sub
            End If
            If cboBanco.SelectedIndex = -1 Then
                chkTodosCuen.Checked = False
                MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboBanco.Focus()
                Exit Sub
            End If

            'cboEmpresas.SelectedIndex = -1
            'cboBanco.SelectedIndex = -1
            cboCuenta.SelectedIndex = -1
            'cboEmpresas.Enabled = False
            'cboBanco.Enabled = False
            cboCuenta.Enabled = False

            'chkTodosBanc.Checked = False
            'chkTodosCuen.Checked = False
            'chkTodosBanc.Enabled = False
            'chkTodosCuen.Enabled = False
        ElseIf chkTodosCuen.Checked = False Then

            'cboEmpresas.SelectedIndex = -1
            'cboBanco.SelectedIndex = -1
            cboCuenta.SelectedIndex = -1
            'cboEmpresas.Enabled = True
            'cboBanco.Enabled = True
            cboCuenta.Enabled = True

            'chkTodosBanc.Checked = False
            'chkTodosCuen.Checked = False
            'chkTodosBanc.Enabled = True
            'chkTodosCuen.Enabled = True
        End If
    End Sub
End Class