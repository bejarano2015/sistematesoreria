Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmItf
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eITF As clsITF
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmItf = Nothing
    Public Shared Function Instance() As frmItf
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmItf
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmItf_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmItf_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eITF = New clsITF
        mMostrarGrilla()
        dgvLista.Width = 523
        dgvLista.Height = 226
    End Sub

    Private Sub mMostrarGrilla()
        eITF = New clsITF
        dtTable = New DataTable
        dtTable = eITF.fListar()
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTable
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eITF.iNroRegistros
        If eITF.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
            '
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtITF.Clear()
        cboA�os.SelectedIndex = -1
        dtpFechaIni.Value = Now.Date().ToShortDateString
        dtpFechaFin.Value = Now.Date().ToShortDateString
    End Sub

    Private Sub HabilitarControles()
        txtCodigo.Enabled = True
        txtITF.Enabled = True
        cboA�os.Enabled = True
        dtpFechaIni.Enabled = True
        dtpFechaFin.Enabled = True
    End Sub

    Private Sub DesabilitarControles()
        txtCodigo.Enabled = False
        txtITF.Enabled = False
        cboA�os.Enabled = False
        dtpFechaIni.Enabled = False
        dtpFechaFin.Enabled = False
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            eITF = New clsITF
            eITF.fCodigo()
            txtCodigo.Text = eITF.sCodFuturo
            txtCodigo.Enabled = False
            cboA�os.Focus()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            DesabilitarControles()
            txtITF.Enabled = True
            txtITF.Focus()
            dtpFechaIni.Enabled = True
            dtpFechaFin.Enabled = True
        End If
    End Sub


    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
        End If
    End Sub


    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            'Timer2.Enabled = True
        End If
    End Sub


    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdItf").ToString
                Me.cboA�os.Text = Fila("A�o").ToString
                Me.txtITF.Text = Fila("Porcentaje").ToString
                dtpFechaIni.Value = Fila("FechaIni").ToString
                dtpFechaFin.Value = Fila("FechaFin").ToString
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""
        eITF = New clsITF
        If sTab = 1 Then
            If Len(Trim(txtCodigo.Text)) > 0 And cboA�os.SelectedIndex > -1 And Len(Trim(txtITF.Text)) > 0 Then
                If iOpcion = 1 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 2 Then
                    Mensaje = "Actualizar"
                End If
                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then

                        'Dim dtDoble As DataTable
                        'dtDoble = New DataTable
                        'dtDoble = eITF.fBuscarDoble(Trim(cboA�os.Text))
                        'If dtDoble.Rows.Count > 0 Then
                        '    MessageBox.Show("Ya Existe un ITF asignado para el a�o " & cboA�os.Text, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        '    sTab = 0
                        '    Exit Sub
                        'End If

                        eITF.fCodigo()
                        sCodigoRegistro = eITF.sCodFuturo
                        txtCodigo.Text = Trim(sCodigoRegistro)
                        iResultado = eITF.fGrabar(iOpcion, Trim(sCodigoRegistro), Trim(cboA�os.Text), Trim(txtITF.Text), dtpFechaIni.Value, dtpFechaFin.Value)
                        If iResultado > 0 Then
                            mMostrarGrilla()
                        End If
                        sTab = 0
                    ElseIf iOpcion = 2 Then
                        sCodigoRegistro = Trim(txtCodigo.Text)
                        iResultado = eITF.fGrabar(iOpcion, Trim(sCodigoRegistro), Trim(cboA�os.Text), Trim(txtITF.Text), dtpFechaIni.Value, dtpFechaFin.Value)
                        If iResultado > 0 Then
                            mMostrarGrilla()
                        End If
                        sTab = 0
                    End If
                End If
            Else
                If Len(Trim(txtCodigo.Text)) = 0 Then
                    MessageBox.Show("Ingrese el C�digo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtCodigo.Focus()
                    Exit Sub
                End If
                If cboA�os.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboA�os.Focus()
                    Exit Sub
                End If
                If Len(Trim(txtITF.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Porcentaje de ITF", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtITF.Focus()
                    Exit Sub
                End If
                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub

   
End Class
