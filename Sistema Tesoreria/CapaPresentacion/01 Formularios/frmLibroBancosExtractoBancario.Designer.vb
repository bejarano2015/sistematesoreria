<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroBancosExtractoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle31 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle32 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle33 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle34 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle35 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvLibroDet = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdLibroDet = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdLibroCab = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaCobro = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TipoOperacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldoInicialExtracto = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldoFinalExtracto = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvLibroDet
        '
        Me.dgvLibroDet.AllowUserToAddRows = False
        Me.dgvLibroDet.BackgroundColor = System.Drawing.Color.White
        Me.dgvLibroDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLibroDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column3, Me.Column5, Me.Column4, Me.Column6, Me.Column10, Me.Column8, Me.IdLibroDet, Me.IdLibroCab, Me.FechaCobro, Me.TipoOperacion, Me.Importe})
        Me.dgvLibroDet.EnableHeadersVisualStyles = False
        Me.dgvLibroDet.Location = New System.Drawing.Point(12, 93)
        Me.dgvLibroDet.Name = "dgvLibroDet"
        Me.dgvLibroDet.RowHeadersVisible = False
        Me.dgvLibroDet.Size = New System.Drawing.Size(931, 349)
        Me.dgvLibroDet.TabIndex = 18
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "Fecha"
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.Format = "M"
        DataGridViewCellStyle29.NullValue = Nothing
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle29
        Me.Column1.FillWeight = 95.0!
        Me.Column1.HeaderText = "Día"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column1.Width = 30
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "NroDocumento"
        DataGridViewCellStyle30.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle30
        Me.Column3.FillWeight = 95.0!
        Me.Column3.HeaderText = "Cheq. / Not."
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "Concepto"
        DataGridViewCellStyle31.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle31
        Me.Column5.FillWeight = 95.0!
        Me.Column5.HeaderText = "Concepto"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column5.Width = 290
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Descripcion"
        DataGridViewCellStyle32.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle32
        Me.Column4.FillWeight = 95.0!
        Me.Column4.HeaderText = "Descripción"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column4.Width = 250
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "Abono"
        DataGridViewCellStyle33.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle33.Format = "N2"
        DataGridViewCellStyle33.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle33
        Me.Column6.FillWeight = 95.0!
        Me.Column6.HeaderText = "Abono"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column6.Width = 80
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "Cargo"
        DataGridViewCellStyle34.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle34.Format = "N2"
        DataGridViewCellStyle34.NullValue = Nothing
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle34
        Me.Column10.FillWeight = 95.0!
        Me.Column10.HeaderText = "Cargo"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column10.Width = 80
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Saldo"
        DataGridViewCellStyle35.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle35.Format = "N2"
        DataGridViewCellStyle35.NullValue = Nothing
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle35
        Me.Column8.FillWeight = 95.0!
        Me.Column8.HeaderText = "Saldo"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column8.Width = 75
        '
        'IdLibroDet
        '
        Me.IdLibroDet.DataPropertyName = "IdLibroDet"
        Me.IdLibroDet.HeaderText = "IdLibroDet"
        Me.IdLibroDet.Name = "IdLibroDet"
        Me.IdLibroDet.Visible = False
        '
        'IdLibroCab
        '
        Me.IdLibroCab.DataPropertyName = "IdLibroCab"
        Me.IdLibroCab.HeaderText = "IdLibroCab"
        Me.IdLibroCab.Name = "IdLibroCab"
        Me.IdLibroCab.Visible = False
        '
        'FechaCobro
        '
        Me.FechaCobro.DataPropertyName = "FechaCobro"
        Me.FechaCobro.HeaderText = "FechaCobro"
        Me.FechaCobro.Name = "FechaCobro"
        Me.FechaCobro.Visible = False
        '
        'TipoOperacion
        '
        Me.TipoOperacion.DataPropertyName = "TipoOperacion"
        Me.TipoOperacion.HeaderText = "TipoOperacion"
        Me.TipoOperacion.Name = "TipoOperacion"
        Me.TipoOperacion.Visible = False
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.Visible = False
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel19.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel19.Location = New System.Drawing.Point(709, 71)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(96, 16)
        Me.BeLabel19.TabIndex = 19
        Me.BeLabel19.Text = "Saldo Inicial"
        '
        'txtSaldoInicialExtracto
        '
        Me.txtSaldoInicialExtracto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoInicialExtracto.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldoInicialExtracto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoInicialExtracto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoInicialExtracto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoInicialExtracto.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldoInicialExtracto.KeyEnter = True
        Me.txtSaldoInicialExtracto.Location = New System.Drawing.Point(811, 67)
        Me.txtSaldoInicialExtracto.Name = "txtSaldoInicialExtracto"
        Me.txtSaldoInicialExtracto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoInicialExtracto.ReadOnly = True
        Me.txtSaldoInicialExtracto.ShortcutsEnabled = False
        Me.txtSaldoInicialExtracto.Size = New System.Drawing.Size(132, 20)
        Me.txtSaldoInicialExtracto.TabIndex = 20
        Me.txtSaldoInicialExtracto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoInicialExtracto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.BeLabel11.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel11.Location = New System.Drawing.Point(719, 452)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(87, 16)
        Me.BeLabel11.TabIndex = 24
        Me.BeLabel11.Text = "Saldo Final"
        '
        'txtSaldoFinalExtracto
        '
        Me.txtSaldoFinalExtracto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoFinalExtracto.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldoFinalExtracto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoFinalExtracto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoFinalExtracto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtSaldoFinalExtracto.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldoFinalExtracto.KeyEnter = True
        Me.txtSaldoFinalExtracto.Location = New System.Drawing.Point(818, 448)
        Me.txtSaldoFinalExtracto.Name = "txtSaldoFinalExtracto"
        Me.txtSaldoFinalExtracto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoFinalExtracto.ReadOnly = True
        Me.txtSaldoFinalExtracto.ShortcutsEnabled = False
        Me.txtSaldoFinalExtracto.Size = New System.Drawing.Size(125, 20)
        Me.txtSaldoFinalExtracto.TabIndex = 25
        Me.txtSaldoFinalExtracto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoFinalExtracto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel1.Location = New System.Drawing.Point(7, 30)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(268, 25)
        Me.BeLabel1.TabIndex = 26
        Me.BeLabel1.Text = "EXTRACTO BANCARIO"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(638, 448)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 27
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 452)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Label1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(420, 452)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Label2"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel2.Location = New System.Drawing.Point(303, 9)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel2.TabIndex = 31
        Me.BeLabel2.Text = "Banco :"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel3.Location = New System.Drawing.Point(303, 32)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel3.TabIndex = 32
        Me.BeLabel3.Text = "Cuenta :"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel4.Location = New System.Drawing.Point(303, 54)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(60, 13)
        Me.BeLabel4.TabIndex = 33
        Me.BeLabel4.Text = "Moneda :"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel5.Location = New System.Drawing.Point(648, 9)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel5.TabIndex = 34
        Me.BeLabel5.Text = "Año :"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel6.Location = New System.Drawing.Point(648, 32)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel6.TabIndex = 35
        Me.BeLabel6.Text = "Mes :"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel7.Location = New System.Drawing.Point(371, 9)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel7.TabIndex = 36
        Me.BeLabel7.Text = "Banco"
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel8.Location = New System.Drawing.Point(371, 32)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel8.TabIndex = 37
        Me.BeLabel8.Text = "Banco"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel9.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel9.Location = New System.Drawing.Point(371, 54)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel9.TabIndex = 38
        Me.BeLabel9.Text = "Banco"
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel10.Location = New System.Drawing.Point(692, 9)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel10.TabIndex = 39
        Me.BeLabel10.Text = "Banco"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel12.Location = New System.Drawing.Point(692, 32)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel12.TabIndex = 40
        Me.BeLabel12.Text = "Banco"
        '
        'frmLibroBancosExtractoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(951, 479)
        Me.Controls.Add(Me.BeLabel12)
        Me.Controls.Add(Me.BeLabel10)
        Me.Controls.Add(Me.BeLabel9)
        Me.Controls.Add(Me.BeLabel8)
        Me.Controls.Add(Me.BeLabel7)
        Me.Controls.Add(Me.BeLabel6)
        Me.Controls.Add(Me.BeLabel5)
        Me.Controls.Add(Me.BeLabel4)
        Me.Controls.Add(Me.BeLabel3)
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.BeLabel11)
        Me.Controls.Add(Me.txtSaldoFinalExtracto)
        Me.Controls.Add(Me.BeLabel19)
        Me.Controls.Add(Me.txtSaldoInicialExtracto)
        Me.Controls.Add(Me.dgvLibroDet)
        Me.MaximizeBox = False
        Me.Name = "frmLibroBancosExtractoBancario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Extracto Bancario"
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvLibroDet As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldoInicialExtracto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldoFinalExtracto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdLibroDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdLibroCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaCobro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoOperacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
End Class
