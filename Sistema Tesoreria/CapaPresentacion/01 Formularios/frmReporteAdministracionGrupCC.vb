Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Windows.Forms

Public Class frmReporteAdministracionGrupCC

    Public strCodigoReporte As String
    Public strDesReporte As String
    Dim dtTable As DataTable
    Private eTipoGastosG1 As clsTipoGastosG1

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAdministracionGrupCC = Nothing
    Public Shared Function Instance() As frmReporteAdministracionGrupCC
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteAdministracionGrupCC
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteAdministracionGrupCC_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteAdministracionGrupCC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListaPermisoMenu()
        Label1.Text = strDesReporte
    End Sub

    Sub ListaPermisoMenu()
        Try
            'Dim ds As DataSet
            dtTable = New DataTable
            eTipoGastosG1 = New clsTipoGastosG1
            dtTable = eTipoGastosG1.fListarCC(gEmpresa)
            'ds = ObjCli.ListaPermisoMenu(gEmpresa, glbSisCodigo, strUsuario)
            'TAB = ds.Tables(0)
            Me.DgvLista.AutoGenerateColumns = False
            Me.DgvLista.DataSource = dtTable 'TAB()
            Me.DgvLista.ReadOnly = False
            Me.DgvLista.Columns(0).ReadOnly = True
            Me.DgvLista.Columns(1).ReadOnly = True
            Me.DgvLista.Columns(2).ReadOnly = False
            ActualizaValoresMenu()
            'cmr = Me.BindingContext(Me.DgvLista.DataSource)
            'VerPosicion()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Sub ActualizaValoresMenu()
        Dim n As Integer
        Dim Cadena As String
        Me.DgvLista.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                n = 1
                For Each fila As DataGridViewRow In Me.DgvLista.Rows
                    Cadena = strCodigoReporte '"2" 'Trim(Mid(strHabilita, n, 1))
                    If Cadena <> "" Then
                        'If fila.Cells("Grupo1").Value = Nothing Then
                        'End If
                        If Cadena = "1" Then
                            If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoInverCapital").Value) = False Then
                                If Cadena = Trim(fila.Cells("GrupoInverCapital").Value) Then
                                    fila.Cells("Column3").Value = True
                                Else
                                    fila.Cells("Column3").Value = False
                                End If
                            Else
                                fila.Cells("Column3").Value = False
                            End If
                        ElseIf Cadena = "2" Then
                            If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoAdmFijos").Value) = False Then
                                If Cadena = Trim(fila.Cells("GrupoAdmFijos").Value) Then
                                    fila.Cells("Column3").Value = True
                                Else
                                    fila.Cells("Column3").Value = False
                                End If
                            Else
                                fila.Cells("Column3").Value = False
                            End If
                        ElseIf Cadena = "3" Then
                            If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoAdmOca").Value) = False Then
                                If Cadena = Trim(fila.Cells("GrupoAdmOca").Value) Then
                                    fila.Cells("Column3").Value = True
                                Else
                                    fila.Cells("Column3").Value = False
                                End If
                            Else
                                fila.Cells("Column3").Value = False
                            End If
                        ElseIf Cadena = "4" Then
                            If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoGastosObra").Value) = False Then
                                If Cadena = Trim(fila.Cells("GrupoGastosObra").Value) Then
                                    fila.Cells("Column3").Value = True
                                Else
                                    fila.Cells("Column3").Value = False
                                End If
                            Else
                                fila.Cells("Column3").Value = False
                            End If
                        End If
                    End If
                    n = n + 1
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.DgvLista.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                For Each fila As DataGridViewRow In Me.DgvLista.Rows
                    If Me.CheckBox1.Checked = True Then
                        fila.Cells("Column3").Value = True
                    Else
                        fila.Cells("Column3").Value = False
                    End If
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        'If MsgBox("Esta seguro de aplicar los cambios?", MsgBoxStyle.YesNo, glbNameSistema) = MsgBoxResult.No Then
        '    Exit Sub
        'End If

        Me.DgvLista.ClearSelection()
        eTipoGastosG1 = New clsTipoGastosG1

        For z As Integer = 0 To DgvLista.Rows.Count - 1
            If Microsoft.VisualBasic.IsDBNull(DgvLista.Rows(z).Cells(2).Value) = False Then
                If Convert.ToBoolean(DgvLista.Rows(z).Cells(2).Value) = True Then
                    If Trim(strCodigoReporte) = 1 Then
                        eTipoGastosG1.fActualizarGasto(18, strCodigoReporte, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    ElseIf Trim(strCodigoReporte) = 2 Then
                        eTipoGastosG1.fActualizarGasto(19, strCodigoReporte, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    ElseIf Trim(strCodigoReporte) = 3 Then
                        eTipoGastosG1.fActualizarGasto(20, strCodigoReporte, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    ElseIf Trim(strCodigoReporte) = 4 Then
                        eTipoGastosG1.fActualizarGasto(21, strCodigoReporte, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    End If
                Else
                    If Trim(strCodigoReporte) = 1 Then
                        eTipoGastosG1.fActualizarGasto(18, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    ElseIf Trim(strCodigoReporte) = 2 Then
                        eTipoGastosG1.fActualizarGasto(19, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    ElseIf Trim(strCodigoReporte) = 3 Then
                        eTipoGastosG1.fActualizarGasto(20, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    ElseIf Trim(strCodigoReporte) = 4 Then
                        eTipoGastosG1.fActualizarGasto(21, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    End If
                End If
            Else
                If Trim(strCodigoReporte) = 1 Then
                    eTipoGastosG1.fActualizarGasto(18, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                ElseIf Trim(strCodigoReporte) = 2 Then
                    eTipoGastosG1.fActualizarGasto(19, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                ElseIf Trim(strCodigoReporte) = 3 Then
                    eTipoGastosG1.fActualizarGasto(20, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                ElseIf Trim(strCodigoReporte) = 4 Then
                    eTipoGastosG1.fActualizarGasto(21, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                End If
            End If
        Next


        'strHabilita = Habilita
        'ListaPermisoMenu()

        'If x <> 1 Then
        'MsgBox("Hubo un error en la actualizacion!", MsgBoxStyle.Information, glbNameSistema)
        'Exit Sub
        'End If
        'frm.mMostrarGrilla()
        'Close()
        Me.Close()

    End Sub
End Class