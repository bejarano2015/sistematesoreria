<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaRegistrosxUser
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConsultaRegistrosxUser))
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.txtSemana = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.btnBusca = New ctrLibreria.Controles.BeButton
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.RD1 = New System.Windows.Forms.RadioButton
        Me.RD2 = New System.Windows.Forms.RadioButton
        Me.chkPrestamos = New System.Windows.Forms.CheckBox
        Me.cboCentroCosto = New ctrLibreria.Controles.BeComboBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkCCtodos = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.AxMonthView1 = New AxMSComCtl2.AxMonthView
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(6, 25)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel4.TabIndex = 57
        Me.BeLabel4.Text = "Semana"
        '
        'txtSemana
        '
        Me.txtSemana.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSemana.BackColor = System.Drawing.Color.Ivory
        Me.txtSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSemana.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSemana.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSemana.ForeColor = System.Drawing.Color.Black
        Me.txtSemana.KeyEnter = True
        Me.txtSemana.Location = New System.Drawing.Point(58, 19)
        Me.txtSemana.MaxLength = 2
        Me.txtSemana.Name = "txtSemana"
        Me.txtSemana.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSemana.ShortcutsEnabled = False
        Me.txtSemana.Size = New System.Drawing.Size(84, 20)
        Me.txtSemana.TabIndex = 58
        Me.txtSemana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtSemana.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(148, 55)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(16, 13)
        Me.BeLabel7.TabIndex = 55
        Me.BeLabel7.Text = "Al"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(6, 55)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(21, 13)
        Me.BeLabel6.TabIndex = 53
        Me.BeLabel6.Text = "De"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(170, 48)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaFin.TabIndex = 56
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(58, 48)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaIni.TabIndex = 54
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'btnBusca
        '
        Me.btnBusca.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnBusca.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBusca.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBusca.Location = New System.Drawing.Point(141, 214)
        Me.btnBusca.Name = "btnBusca"
        Me.btnBusca.Size = New System.Drawing.Size(222, 35)
        Me.btnBusca.TabIndex = 59
        Me.btnBusca.Text = "Ver"
        Me.btnBusca.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnBusca.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'RD1
        '
        Me.RD1.AutoSize = True
        Me.RD1.Checked = True
        Me.RD1.Location = New System.Drawing.Point(12, 92)
        Me.RD1.Name = "RD1"
        Me.RD1.Size = New System.Drawing.Size(115, 17)
        Me.RD1.TabIndex = 60
        Me.RD1.TabStop = True
        Me.RD1.Text = "Todas las Empresa"
        Me.RD1.UseVisualStyleBackColor = True
        '
        'RD2
        '
        Me.RD2.AutoSize = True
        Me.RD2.Location = New System.Drawing.Point(141, 92)
        Me.RD2.Name = "RD2"
        Me.RD2.Size = New System.Drawing.Size(99, 17)
        Me.RD2.TabIndex = 61
        Me.RD2.Text = "Empresa Actual"
        Me.RD2.UseVisualStyleBackColor = True
        '
        'chkPrestamos
        '
        Me.chkPrestamos.AutoSize = True
        Me.chkPrestamos.Location = New System.Drawing.Point(141, 191)
        Me.chkPrestamos.Name = "chkPrestamos"
        Me.chkPrestamos.Size = New System.Drawing.Size(106, 17)
        Me.chkPrestamos.TabIndex = 62
        Me.chkPrestamos.Text = "Incluir Préstamos"
        Me.chkPrestamos.UseVisualStyleBackColor = True
        '
        'cboCentroCosto
        '
        Me.cboCentroCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCentroCosto.BackColor = System.Drawing.Color.Ivory
        Me.cboCentroCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroCosto.ForeColor = System.Drawing.Color.Black
        Me.cboCentroCosto.FormattingEnabled = True
        Me.cboCentroCosto.KeyEnter = True
        Me.cboCentroCosto.Location = New System.Drawing.Point(6, 42)
        Me.cboCentroCosto.Name = "cboCentroCosto"
        Me.cboCentroCosto.Size = New System.Drawing.Size(210, 21)
        Me.cboCentroCosto.TabIndex = 63
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BeLabel4)
        Me.GroupBox1.Controls.Add(Me.txtSemana)
        Me.GroupBox1.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox1.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox1.Controls.Add(Me.BeLabel7)
        Me.GroupBox1.Controls.Add(Me.BeLabel6)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(351, 79)
        Me.GroupBox1.TabIndex = 64
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Semana"
        '
        'chkCCtodos
        '
        Me.chkCCtodos.AutoSize = True
        Me.chkCCtodos.Location = New System.Drawing.Point(6, 19)
        Me.chkCCtodos.Name = "chkCCtodos"
        Me.chkCCtodos.Size = New System.Drawing.Size(56, 17)
        Me.chkCCtodos.TabIndex = 65
        Me.chkCCtodos.Text = "Todos"
        Me.chkCCtodos.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboCentroCosto)
        Me.GroupBox2.Controls.Add(Me.chkCCtodos)
        Me.GroupBox2.Location = New System.Drawing.Point(141, 115)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(222, 70)
        Me.GroupBox2.TabIndex = 66
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Por Centro de Costo"
        '
        'AxMonthView1
        '
        Me.AxMonthView1.Location = New System.Drawing.Point(599, 27)
        Me.AxMonthView1.Name = "AxMonthView1"
        Me.AxMonthView1.OcxState = CType(resources.GetObject("AxMonthView1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxMonthView1.Size = New System.Drawing.Size(181, 158)
        Me.AxMonthView1.TabIndex = 67
        '
        'frmConsultaRegistrosxUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(394, 266)
        Me.Controls.Add(Me.AxMonthView1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkPrestamos)
        Me.Controls.Add(Me.RD2)
        Me.Controls.Add(Me.RD1)
        Me.Controls.Add(Me.btnBusca)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmConsultaRegistrosxUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Consulta de Movimientos Por Usuario"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSemana As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnBusca As ctrLibreria.Controles.BeButton
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents RD1 As System.Windows.Forms.RadioButton
    Friend WithEvents RD2 As System.Windows.Forms.RadioButton
    Friend WithEvents chkPrestamos As System.Windows.Forms.CheckBox
    Friend WithEvents cboCentroCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkCCtodos As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents AxMonthView1 As AxMSComCtl2.AxMonthView
End Class
