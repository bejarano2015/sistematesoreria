Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Windows.Forms

Public Class frmAgruparCC

    'Friend frm As New frmAgruparCC
    'Private strCodigoPadron As String
    'Public strHabilita As String 'Opciones cadena 200
    Public strCodigoGrupo As String
    Public strDesGrupo As String
    'Public strCodigoGrupoGeneral As String
    Dim dtTable As DataTable
    'Private eUsuario As Usuarios
    Private eTipoGastosG1 As clsTipoGastosG1

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmAgruparCC = Nothing
    Public Shared Function Instance() As frmAgruparCC
        If frmInstance Is Nothing Then
            frmInstance = New frmAgruparCC
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmAgruparCC_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmAgruparCC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListaPermisoMenu()
        Label1.Text = strCodigoGrupo & " - " & strDesGrupo
    End Sub

    Sub ListaPermisoMenu()
        Try
            'Dim ds As DataSet....
            dtTable = New DataTable
            eTipoGastosG1 = New clsTipoGastosG1
            dtTable = eTipoGastosG1.fListarCC(gEmpresa)
            Me.DgvLista.AutoGenerateColumns = False
            Me.DgvLista.DataSource = dtTable
            Me.DgvLista.ReadOnly = False
            Me.DgvLista.Columns(0).ReadOnly = True
            Me.DgvLista.Columns(0).ReadOnly = True
            Me.DgvLista.Columns(1).ReadOnly = True
            Me.DgvLista.Columns(2).ReadOnly = False
            ActualizaValoresMenu()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Sub ActualizaValoresMenu()
        Dim n As Integer
        Dim Cadena As String
        Me.DgvLista.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                n = 1
                For Each fila As DataGridViewRow In Me.DgvLista.Rows
                    Cadena = strCodigoGrupo '"2" 'Trim(Mid(strHabilita, n, 1))
                    If Cadena <> "" Then
                        If Microsoft.VisualBasic.IsDBNull(fila.Cells("Grupo1").Value) = False Then
                            If Cadena = Trim(fila.Cells("Grupo1").Value) Then
                                fila.Cells("Column3").Value = True
                            Else
                                fila.Cells("Column3").Value = False
                            End If
                        End If
                    End If
                    n = n + 1
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.DgvLista.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                For Each fila As DataGridViewRow In Me.DgvLista.Rows
                    If Me.CheckBox1.Checked = True Then
                        fila.Cells("Column3").Value = True
                    Else
                        fila.Cells("Column3").Value = False
                    End If
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        If MsgBox("Esta seguro de aplicar los cambios?", MsgBoxStyle.YesNo, glbNameSistema) = MsgBoxResult.No Then
            Exit Sub
        End If


        Me.DgvLista.ClearSelection()
        eTipoGastosG1 = New clsTipoGastosG1

        For z As Integer = 0 To DgvLista.Rows.Count - 1
            If Microsoft.VisualBasic.IsDBNull(DgvLista.Rows(z).Cells(2).Value) = False Then
               
                If Convert.ToBoolean(DgvLista.Rows(z).Cells(2).Value) = True Then
                    'If Trim(strCodigoGrupoGeneral) = 1 Then
                    eTipoGastosG1.fActualizarGasto(17, strCodigoGrupo, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                    'ElseIf Trim(strCodigoGrupoGeneral) = 2 Then
                    '    eTipoGastosG1.fActualizarGasto(12, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, strCodigoSubGrupo, "")
                    'ElseIf Trim(strCodigoGrupoGeneral) = 3 Then
                    '    'MessageBox.Show(DgvLista.Rows(z).Cells(2).Value)
                    '    eTipoGastosG1.fActualizarGasto(13, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, strCodigoSubGrupo, "")
                    'ElseIf Trim(strCodigoGrupoGeneral) = 4 Then
                    '    eTipoGastosG1.fActualizarGasto(14, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, strCodigoSubGrupo, "")
                    'End If
                Else
                    'MessageBox.Show(DgvLista.Rows(z).Cells(3).Value)
                    If Trim(DgvLista.Rows(z).Cells(3).Value) = strCodigoGrupo Then
                        'MessageBox.Show(DgvLista.Rows(z).Cells(3).Value)
                        'eTipoGastosG1.fActualizarGasto(9, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, "", "")
                        'If Trim(strCodigoGrupoGeneral) = 1 Then
                        eTipoGastosG1.fActualizarGasto(17, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                        'ElseIf Trim(strCodigoGrupoGeneral) = 2 Then
                        '    eTipoGastosG1.fActualizarGasto(12, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, "", "")
                        'ElseIf Trim(strCodigoGrupoGeneral) = 3 Then
                        '    eTipoGastosG1.fActualizarGasto(13, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, "", "")
                        'ElseIf Trim(strCodigoGrupoGeneral) = 4 Then
                        '    eTipoGastosG1.fActualizarGasto(14, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, "", "")
                        'End If
                    End If
                End If
            Else
                'If Trim(strCodigoGrupoGeneral) = 1 Then
                eTipoGastosG1.fActualizarGasto(17, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))
                'ElseIf Trim(strCodigoGrupoGeneral) = 2 Then
                '    eTipoGastosG1.fActualizarGasto(12, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, "", "")
                'ElseIf Trim(strCodigoGrupoGeneral) = 3 Then
                '    eTipoGastosG1.fActualizarGasto(13, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, "", "")
                'ElseIf Trim(strCodigoGrupoGeneral) = 4 Then
                '    eTipoGastosG1.fActualizarGasto(14, Trim(DgvLista.Rows(z).Cells(0).Value), gEmpresa, "", "")
                'End If
            End If
        Next
        'strHabilita = Habilita
        'ListaPermisoMenu()
        'If x <> 1 Then
        'MsgBox("Hubo un error en la actualizacion!", MsgBoxStyle.Information, glbNameSistema)
        'MessageBox.Show("perra")
        'Exit Sub
        'End If
        'frm.mMostrarGrilla()
        'Close()
        Me.Close()
    End Sub
End Class