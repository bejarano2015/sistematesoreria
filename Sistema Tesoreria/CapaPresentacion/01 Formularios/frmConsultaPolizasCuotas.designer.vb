<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaPolizasCuotas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rbCuotas = New System.Windows.Forms.RadioButton
        Me.rbPolizas = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPoliza = New ctrLibreria.Controles.BeTextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDias = New ctrLibreria.Controles.BeTextBox
        Me.chkPorVencer = New System.Windows.Forms.CheckBox
        Me.chkVencidas = New System.Windows.Forms.CheckBox
        Me.btnConsultar = New ctrLibreria.Controles.BeButton
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.dgvLista2 = New System.Windows.Forms.DataGridView
        Me.dgvLista3 = New System.Windows.Forms.DataGridView
        Me.lblTotalRegistro = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvLista2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLista3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbCuotas)
        Me.GroupBox1.Controls.Add(Me.rbPolizas)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtPoliza)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(252, 78)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'rbCuotas
        '
        Me.rbCuotas.AutoSize = True
        Me.rbCuotas.Location = New System.Drawing.Point(103, 19)
        Me.rbCuotas.Name = "rbCuotas"
        Me.rbCuotas.Size = New System.Drawing.Size(58, 17)
        Me.rbCuotas.TabIndex = 1
        Me.rbCuotas.TabStop = True
        Me.rbCuotas.Text = "Cuotas"
        Me.rbCuotas.UseVisualStyleBackColor = True
        '
        'rbPolizas
        '
        Me.rbPolizas.AutoSize = True
        Me.rbPolizas.Location = New System.Drawing.Point(12, 19)
        Me.rbPolizas.Name = "rbPolizas"
        Me.rbPolizas.Size = New System.Drawing.Size(58, 17)
        Me.rbPolizas.TabIndex = 0
        Me.rbPolizas.TabStop = True
        Me.rbPolizas.Text = "Polizas"
        Me.rbPolizas.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Nro de Poliza"
        '
        'txtPoliza
        '
        Me.txtPoliza.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPoliza.BackColor = System.Drawing.Color.Ivory
        Me.txtPoliza.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPoliza.ForeColor = System.Drawing.Color.Black
        Me.txtPoliza.KeyEnter = True
        Me.txtPoliza.Location = New System.Drawing.Point(85, 45)
        Me.txtPoliza.Name = "txtPoliza"
        Me.txtPoliza.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPoliza.ShortcutsEnabled = False
        Me.txtPoliza.Size = New System.Drawing.Size(121, 20)
        Me.txtPoliza.TabIndex = 2
        Me.txtPoliza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPoliza.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtDias)
        Me.GroupBox2.Controls.Add(Me.chkPorVencer)
        Me.GroupBox2.Controls.Add(Me.chkVencidas)
        Me.GroupBox2.Location = New System.Drawing.Point(292, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 78)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(121, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(28, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Dias"
        '
        'txtDias
        '
        Me.txtDias.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDias.BackColor = System.Drawing.Color.Ivory
        Me.txtDias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDias.ForeColor = System.Drawing.Color.Black
        Me.txtDias.KeyEnter = True
        Me.txtDias.Location = New System.Drawing.Point(124, 46)
        Me.txtDias.Name = "txtDias"
        Me.txtDias.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDias.ShortcutsEnabled = False
        Me.txtDias.Size = New System.Drawing.Size(61, 20)
        Me.txtDias.TabIndex = 2
        Me.txtDias.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDias.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Númerico
        '
        'chkPorVencer
        '
        Me.chkPorVencer.AutoSize = True
        Me.chkPorVencer.Location = New System.Drawing.Point(16, 49)
        Me.chkPorVencer.Name = "chkPorVencer"
        Me.chkPorVencer.Size = New System.Drawing.Size(79, 17)
        Me.chkPorVencer.TabIndex = 1
        Me.chkPorVencer.Text = "Por Vencer"
        Me.chkPorVencer.UseVisualStyleBackColor = True
        '
        'chkVencidas
        '
        Me.chkVencidas.AutoSize = True
        Me.chkVencidas.Location = New System.Drawing.Point(17, 23)
        Me.chkVencidas.Name = "chkVencidas"
        Me.chkVencidas.Size = New System.Drawing.Size(70, 17)
        Me.chkVencidas.TabIndex = 0
        Me.chkVencidas.Text = "Vencidas"
        Me.chkVencidas.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnConsultar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnConsultar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Grid_properties_
        Me.btnConsultar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnConsultar.Location = New System.Drawing.Point(498, 21)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(76, 31)
        Me.btnConsultar.TabIndex = 2
        Me.btnConsultar.Text = "Consultar"
        Me.btnConsultar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnConsultar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'dgvLista2
        '
        Me.dgvLista2.AllowUserToAddRows = False
        Me.dgvLista2.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista2.EnableHeadersVisualStyles = False
        Me.dgvLista2.Location = New System.Drawing.Point(12, 101)
        Me.dgvLista2.Name = "dgvLista2"
        Me.dgvLista2.ReadOnly = True
        Me.dgvLista2.RowHeadersVisible = False
        Me.dgvLista2.Size = New System.Drawing.Size(995, 158)
        Me.dgvLista2.TabIndex = 13
        '
        'dgvLista3
        '
        Me.dgvLista3.AllowUserToAddRows = False
        Me.dgvLista3.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista3.EnableHeadersVisualStyles = False
        Me.dgvLista3.Location = New System.Drawing.Point(12, 299)
        Me.dgvLista3.Name = "dgvLista3"
        Me.dgvLista3.ReadOnly = True
        Me.dgvLista3.RowHeadersVisible = False
        Me.dgvLista3.Size = New System.Drawing.Size(995, 158)
        Me.dgvLista3.TabIndex = 14
        '
        'lblTotalRegistro
        '
        Me.lblTotalRegistro.AutoSize = True
        Me.lblTotalRegistro.Location = New System.Drawing.Point(9, 262)
        Me.lblTotalRegistro.Name = "lblTotalRegistro"
        Me.lblTotalRegistro.Size = New System.Drawing.Size(28, 13)
        Me.lblTotalRegistro.TabIndex = 15
        Me.lblTotalRegistro.Text = "Dias"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 460)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Dias"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(413, 283)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Endosos"
        '
        'frmConsultaPolizasCuotas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1018, 476)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblTotalRegistro)
        Me.Controls.Add(Me.dgvLista3)
        Me.Controls.Add(Me.dgvLista2)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmConsultaPolizasCuotas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Consulta de Polizas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvLista2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLista3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPoliza As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPorVencer As System.Windows.Forms.CheckBox
    Friend WithEvents chkVencidas As System.Windows.Forms.CheckBox
    Friend WithEvents btnConsultar As ctrLibreria.Controles.BeButton
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents rbCuotas As System.Windows.Forms.RadioButton
    Friend WithEvents rbPolizas As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDias As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dgvLista2 As System.Windows.Forms.DataGridView
    Friend WithEvents dgvLista3 As System.Windows.Forms.DataGridView
    Friend WithEvents lblTotalRegistro As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
