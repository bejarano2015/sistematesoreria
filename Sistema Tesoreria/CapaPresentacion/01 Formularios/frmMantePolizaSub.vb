Imports CapaNegocios

Public Class frmMantePolizaSub

    Private cSegurosPolizas As clsSegurosPolizas
    Dim BolEditar As Boolean
    Public NroPoli As String = ""
    Public VigenciaPoli As String = ""
    Public Moneda As String = ""
    Public Ramo As String = ""
    Public Cliente As String = ""
    Public Compa�ia As String = ""

     
    Public VigenciaIni As DateTime
    Public VigenciaFin As DateTime

    Public RamoCod As String = ""
    Public RUC As String = ""
    Public Direccion As String = ""
    Public CodMoneda As String = ""
    Public CompCod As String = ""
    Friend DS_Datos As New DataSet

    Private Sub frmMantePolizaSub_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Limpiar()

        lblNroPoliza.Text = NroPoli
        lblVigencia.Text = VigenciaPoli
        lblMoneda.Text = Moneda
        lblRamo.Text = Ramo
        lblCliente.Text = Cliente
        lblCompa.Text = Compa�ia


        cSegurosPolizas = New clsSegurosPolizas
        DS_Datos = cSegurosPolizas.fCargaDatos
        cboFormaPago.DataSource = DS_Datos.Tables(2)
        cboFormaPago.ValueMember = "Codigo"
        cboFormaPago.DisplayMember = "Descripcion"
        cboFormaPago.SelectedIndex = -1

        cboRamo.DataSource = DS_Datos.Tables(0)
        cboRamo.ValueMember = "Codigo"
        cboRamo.DisplayMember = "Descripcion"
        cboRamo.SelectedIndex = -1

        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If Timer1.Enabled = True Then
            txtPoliza.Focus()
        End If
        Timer1.Enabled = False
    End Sub

    Function Limpiar() As Boolean
        'lblNroPoliza.Text = ""
        'lblVigencia.Text = ""
        'lblMoneda.Text = ""
        'lblRamo.Text = ""
        'lblCliente.Text = ""
        'lblCompa.Text = ""

        mtbVigenciaIni.Text = ""
        mtbVigenciaFin.Text = ""
        txtPrima.Text = ""
        txtDerechoEmision.Text = ""
        txtInteres.Text = ""
        txtIGV.Text = ""
        txtTotal.Text = ""
        txtSumaAseguradora.Text = ""
        cboFormaPago.SelectedIndex = -1
        cboRamo.SelectedIndex = -1
        txtLetras.Text = ""
        mtbFechaLetraIni.Text = ""
    End Function


    Function ValidarIngreso() As Boolean
        If txtPoliza.Text = "" Then
            MessageBox.Show("Ingrese Numero de Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtPoliza.Focus()
            Return False
        

        ElseIf (mtbVigenciaIni.Text.Length <> 10 Or mtbVigenciaFin.Text.Length <> 10) Then
            MessageBox.Show("Ingrese correctamente Vigencia de Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            mtbVigenciaIni.Focus()
            Return False
        ElseIf Convert.ToDateTime(mtbVigenciaIni.Text) < VigenciaIni Then
            MessageBox.Show("La Fecha de Vigencia Inicial debe ser Mayor o Igual a la de la Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            mtbVigenciaIni.Focus()
            Return False
        ElseIf Convert.ToDateTime(mtbVigenciaFin.Text) > VigenciaFin Then
            MessageBox.Show("La Fecha de Vigencia Final debe ser Menor o Igual a la de la Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            mtbVigenciaFin.Focus()
            Return False
        ElseIf cboRamo.SelectedIndex = -1 Then
            MessageBox.Show("Ingrese Ramo para la Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboRamo.Focus()
            Return False

            'ElseIf cboAseguradora.SelectedIndex = -1 Then
            '    MessageBox.Show("Ingrese Cia. Aseguradora", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    cboAseguradora.Focus()
            '    Return False
            'ElseIf cboMoneda.SelectedIndex = -1 Then
            '    MessageBox.Show("Ingrese Moneda", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    cboMoneda.Focus()
            '    Return False
        ElseIf cboFormaPago.SelectedIndex = -1 Then
            MessageBox.Show("Ingrese Forma de Pago", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboFormaPago.Focus()
            Return False
        ElseIf cboFormaPago.SelectedIndex = 1 Then
            If txtLetras.Text = "" Then
                MessageBox.Show("Ingrese Numero de Letras", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtLetras.Focus()
                Return False
            ElseIf mtbFechaLetraIni.Text.Length <> 10 Then
                MessageBox.Show("Ingrese Fecha Inicial de Letra", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                mtbFechaLetraIni.Focus()
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub btngrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngrabar.Click
        Dim bResultado As Boolean
        cSegurosPolizas = New clsSegurosPolizas
        If ValidarIngreso() = False Then Exit Sub

        '********* txtSumaAseguradora = Interes de Financiacion 

       

        If cSegurosPolizas.fVerificaPagosPoliza(gEmpresa, NroPoli, txtPoliza.Text.Trim) > 0 Then
            MessageBox.Show("No se puede Actualizar la Informacion porque el Endoso" & txtPoliza.Text.Trim & " tiene Pagos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        If MessageBox.Show("�Desea Grabar?", "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bResultado = cSegurosPolizas.fGrabarEndosos(gEmpresa, txtPoliza.Text.Trim, Trim(NroPoli), Trim(cboRamo.SelectedValue), Convert.ToDateTime(mtbVigenciaIni.Text), _
                        Convert.ToDateTime(mtbVigenciaFin.Text), Trim(RUC), (Direccion), Trim(CodMoneda), _
                        Convert.ToDecimal(txtPrima.Text), Convert.ToDecimal(txtDerechoEmision.Text), Convert.ToDecimal(txtInteres.Text), _
                        Convert.ToDecimal(txtIGV.Text), Convert.ToDecimal(txtTotal.Text), Trim(CompCod), Convert.ToDecimal(txtSumaAseguradora.Text), _
                        cboFormaPago.SelectedValue, Convert.ToInt32(txtLetras.Text), Convert.ToDateTime(IIf(mtbFechaLetraIni.Text.Trim.Length < 10, Today, mtbFechaLetraIni.Text)), BolEditar, gUsuario)

            If bResultado = True Then
                If cboFormaPago.SelectedValue = "02" Then   'CUOTAS
                    cSegurosPolizas.fGenerarCronogramaPagoPoliza2(gEmpresa, txtPoliza.Text.Trim, Trim(NroPoli), Convert.ToDecimal(txtTotal.Text), Convert.ToDecimal(txtSumaAseguradora.Text), Convert.ToInt32(txtLetras.Text), Convert.ToDateTime(IIf(mtbFechaLetraIni.Text.Trim.Length < 10, Today, mtbFechaLetraIni.Text)), gUsuario)
                End If
                MessageBox.Show("Endoso " & IIf(BolEditar = False, "Grabado", "Actualizado") & " con Exito", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'DgvLista.DataSource = cSegurosPolizas.fListarPolizas(gEmpresa)
                'sTab = 0
                'frmPrincipal.sFlagGrabar = "1"
                txtPoliza_TextChanged(sender, e)
            Else
                MessageBox.Show("No se Pudo Grabar", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'txtPoliza_TextChanged(sender, e)
            End If

        End If
    End Sub

    Private Sub txtPoliza_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPoliza.TextChanged
        If Len(Trim(txtPoliza.Text)) > 0 Then
            Dim Dtable As DataTable
            Dtable = New DataTable
            cSegurosPolizas = New clsSegurosPolizas
            Dtable = cSegurosPolizas.fVerificaDuplicadoPolizaEndoso(gEmpresa, Trim(lblNroPoliza.Text), Trim(Trim(txtPoliza.Text)))
            If Dtable.Rows.Count > 0 Then
                'MessageBox.Show("No se puede Actualizar la Informacion porque la Poliza " & txtPoliza.Text.Trim & " tiene Pagos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                'Exit Sub

                mtbVigenciaIni.Text = Dtable.Rows(0).Item("Vigencia_FechaInicio")
                mtbVigenciaFin.Text = Dtable.Rows(0).Item("Vigencia_FechaFin")
                cboRamo.SelectedValue = Dtable.Rows(0).Item("CodigoRamo")
                'txtCliente.Text = cSegurosPolizas.DescripcionEmpresa(gEmpresa)
                'txtRUC.Text = cSegurosPolizas.RucEmpresa(gEmpresa)
                'txtDireccion.Text = cSegurosPolizas.DireccionEmpresa(gEmpresa)
                'cboMoneda.SelectedValue = Dtable.Rows(0).Item("MonCodigo")
                txtPrima.Text = Format(Dtable.Rows(0).Item("Prima"), "#,##0.00")
                txtDerechoEmision.Text = Format(Dtable.Rows(0).Item("DerechoEmision"), "#,##0.00")
                txtInteres.Text = Format(Dtable.Rows(0).Item("Intereses"), "#,##0.00")
                txtIGV.Text = Format(Dtable.Rows(0).Item("IGV"), "#,##0.00")
                txtTotal.Text = Format(Dtable.Rows(0).Item("Total"), "#,##0.00")
                'cboAseguradora.SelectedValue = Dtable.Rows(0).Item("CodigoAseguradora")
                txtSumaAseguradora.Text = Format(Dtable.Rows(0).Item("SumaAsegurada"), "#,##0.00")
                cboFormaPago.SelectedValue = Dtable.Rows(0).Item("CodCondCancelacion")
                txtLetras.Text = Dtable.Rows(0).Item("NumLetra")
                mtbFechaLetraIni.Text = IIf(IsDBNull(Dtable.Rows(0).Item("FechaIniLetra")), "", Dtable.Rows(0).Item("FechaIniLetra"))
                'sTab = 1
                BolEditar = True
            ElseIf Dtable.Rows.Count = 0 Then
                Limpiar()
                BolEditar = False
            End If
        ElseIf Len(Trim(txtPoliza.Text)) = 0 Then
            Limpiar()
            BolEditar = False 
        End If
    End Sub

End Class

