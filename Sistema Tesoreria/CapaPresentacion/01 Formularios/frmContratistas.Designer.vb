<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContratistas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.CCosto = New ctrLibreria.Controles.BeComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPresupuesto = New ctrLibreria.Controles.BeTextBox
        Me.lblFGaran = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.lblPresu = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtObservacion = New ctrLibreria.Controles.BeTextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtRuc = New ctrLibreria.Controles.BeTextBox
        Me.lblContrato = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.lblOrden = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.lblPorRete = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblPorAbo = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.lblPorSaldo = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.lblTotFac = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.lblPagoFG = New System.Windows.Forms.Label
        Me.lblSaldo = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.lblRetenido = New System.Windows.Forms.Label
        Me.lblAbonado = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.lblDoc = New System.Windows.Forms.Label
        Me.lblFechaFin = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.lblMoneda = New System.Windows.Forms.Label
        Me.lblFechaIni = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtDescripcionEsp = New ctrLibreria.Controles.BeTextBox
        Me.cboPartidas = New ctrLibreria.Controles.BeComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboContratista = New ctrLibreria.Controles.BeComboBox
        Me.cboObra = New ctrLibreria.Controles.BeComboBox
        Me.lblTipoC = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.dgvHistorial = New System.Windows.Forms.DataGridView
        Me.TD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TotNA = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdValorizacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaPago = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TotPagar = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Pagado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdTipoPago = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Tipo1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.CCosto)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtPresupuesto)
        Me.GroupBox1.Controls.Add(Me.lblFGaran)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.lblPresu)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtRuc)
        Me.GroupBox1.Controls.Add(Me.lblContrato)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.lblOrden)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.lblPorRete)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.lblDoc)
        Me.GroupBox1.Controls.Add(Me.lblFechaFin)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.lblMoneda)
        Me.GroupBox1.Controls.Add(Me.lblFechaIni)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtDescripcionEsp)
        Me.GroupBox1.Controls.Add(Me.cboPartidas)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cboContratista)
        Me.GroupBox1.Controls.Add(Me.cboObra)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(9, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1209, 244)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Contrato"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label15.Location = New System.Drawing.Point(11, 44)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(53, 13)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "C Costo"
        '
        'CCosto
        '
        Me.CCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.CCosto.BackColor = System.Drawing.Color.Ivory
        Me.CCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CCosto.ForeColor = System.Drawing.Color.Black
        Me.CCosto.FormattingEnabled = True
        Me.CCosto.KeyEnter = True
        Me.CCosto.Location = New System.Drawing.Point(74, 41)
        Me.CCosto.Name = "CCosto"
        Me.CCosto.Size = New System.Drawing.Size(317, 21)
        Me.CCosto.TabIndex = 35
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(775, 95)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Presupuesto"
        '
        'txtPresupuesto
        '
        Me.txtPresupuesto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPresupuesto.BackColor = System.Drawing.Color.Ivory
        Me.txtPresupuesto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPresupuesto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPresupuesto.Enabled = False
        Me.txtPresupuesto.ForeColor = System.Drawing.Color.Black
        Me.txtPresupuesto.KeyEnter = True
        Me.txtPresupuesto.Location = New System.Drawing.Point(858, 92)
        Me.txtPresupuesto.Name = "txtPresupuesto"
        Me.txtPresupuesto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPresupuesto.ShortcutsEnabled = False
        Me.txtPresupuesto.Size = New System.Drawing.Size(338, 21)
        Me.txtPresupuesto.TabIndex = 33
        Me.txtPresupuesto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'lblFGaran
        '
        Me.lblFGaran.BackColor = System.Drawing.Color.White
        Me.lblFGaran.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFGaran.ForeColor = System.Drawing.Color.DarkRed
        Me.lblFGaran.Location = New System.Drawing.Point(363, 142)
        Me.lblFGaran.Name = "lblFGaran"
        Me.lblFGaran.Size = New System.Drawing.Size(117, 18)
        Me.lblFGaran.TabIndex = 32
        Me.lblFGaran.Text = "lblFGaran"
        Me.lblFGaran.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label21.Location = New System.Drawing.Point(287, 145)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(70, 13)
        Me.Label21.TabIndex = 30
        Me.Label21.Text = "F. Garantía"
        '
        'lblPresu
        '
        Me.lblPresu.BackColor = System.Drawing.Color.White
        Me.lblPresu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPresu.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPresu.Location = New System.Drawing.Point(103, 140)
        Me.lblPresu.Name = "lblPresu"
        Me.lblPresu.Size = New System.Drawing.Size(117, 18)
        Me.lblPresu.TabIndex = 31
        Me.lblPresu.Text = "lblPresu"
        Me.lblPresu.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label22.Location = New System.Drawing.Point(11, 145)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(91, 13)
        Me.Label22.TabIndex = 29
        Me.Label22.Text = "Valor Contrato"
        '
        'txtObservacion
        '
        Me.txtObservacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtObservacion.BackColor = System.Drawing.Color.Ivory
        Me.txtObservacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Enabled = False
        Me.txtObservacion.ForeColor = System.Drawing.Color.Black
        Me.txtObservacion.KeyEnter = True
        Me.txtObservacion.Location = New System.Drawing.Point(858, 117)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtObservacion.ShortcutsEnabled = False
        Me.txtObservacion.Size = New System.Drawing.Size(341, 43)
        Me.txtObservacion.TabIndex = 8
        Me.txtObservacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label12.Location = New System.Drawing.Point(932, 18)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(77, 13)
        Me.Label12.TabIndex = 28
        Me.Label12.Text = "Especialidad"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(778, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "Ruc"
        '
        'txtRuc
        '
        Me.txtRuc.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRuc.BackColor = System.Drawing.Color.Ivory
        Me.txtRuc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRuc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRuc.Enabled = False
        Me.txtRuc.ForeColor = System.Drawing.Color.Black
        Me.txtRuc.KeyEnter = True
        Me.txtRuc.Location = New System.Drawing.Point(813, 14)
        Me.txtRuc.MaxLength = 11
        Me.txtRuc.Name = "txtRuc"
        Me.txtRuc.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRuc.ShortcutsEnabled = False
        Me.txtRuc.Size = New System.Drawing.Size(110, 21)
        Me.txtRuc.TabIndex = 26
        Me.txtRuc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'lblContrato
        '
        Me.lblContrato.BackColor = System.Drawing.Color.White
        Me.lblContrato.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblContrato.ForeColor = System.Drawing.Color.DarkRed
        Me.lblContrato.Location = New System.Drawing.Point(103, 117)
        Me.lblContrato.Name = "lblContrato"
        Me.lblContrato.Size = New System.Drawing.Size(117, 18)
        Me.lblContrato.TabIndex = 25
        Me.lblContrato.Text = "Label13"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(11, 121)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(75, 13)
        Me.Label13.TabIndex = 24
        Me.Label13.Text = "Nº Contrato"
        '
        'lblOrden
        '
        Me.lblOrden.BackColor = System.Drawing.Color.White
        Me.lblOrden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblOrden.ForeColor = System.Drawing.Color.DarkRed
        Me.lblOrden.Location = New System.Drawing.Point(642, 118)
        Me.lblOrden.Name = "lblOrden"
        Me.lblOrden.Size = New System.Drawing.Size(121, 18)
        Me.lblOrden.TabIndex = 23
        Me.lblOrden.Text = "Label13"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label9.Location = New System.Drawing.Point(560, 121)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(60, 13)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Nº Orden"
        '
        'lblPorRete
        '
        Me.lblPorRete.BackColor = System.Drawing.Color.White
        Me.lblPorRete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPorRete.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPorRete.Location = New System.Drawing.Point(642, 142)
        Me.lblPorRete.Name = "lblPorRete"
        Me.lblPorRete.Size = New System.Drawing.Size(121, 18)
        Me.lblPorRete.TabIndex = 21
        Me.lblPorRete.Text = "Label15"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(560, 147)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(46, 13)
        Me.Label16.TabIndex = 20
        Me.Label16.Text = "% Ret."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPorAbo)
        Me.GroupBox2.Controls.Add(Me.Label36)
        Me.GroupBox2.Controls.Add(Me.lblPorSaldo)
        Me.GroupBox2.Controls.Add(Me.Label34)
        Me.GroupBox2.Controls.Add(Me.lblTotFac)
        Me.GroupBox2.Controls.Add(Me.Label32)
        Me.GroupBox2.Controls.Add(Me.lblPagoFG)
        Me.GroupBox2.Controls.Add(Me.lblSaldo)
        Me.GroupBox2.Controls.Add(Me.Label29)
        Me.GroupBox2.Controls.Add(Me.Label30)
        Me.GroupBox2.Controls.Add(Me.lblRetenido)
        Me.GroupBox2.Controls.Add(Me.lblAbonado)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Location = New System.Drawing.Point(14, 172)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1182, 66)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Resumen de Operaciones"
        '
        'lblPorAbo
        '
        Me.lblPorAbo.AutoSize = True
        Me.lblPorAbo.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPorAbo.Location = New System.Drawing.Point(847, 22)
        Me.lblPorAbo.Name = "lblPorAbo"
        Me.lblPorAbo.Size = New System.Drawing.Size(61, 13)
        Me.lblPorAbo.TabIndex = 39
        Me.lblPorAbo.Text = "lblPorAbo"
        Me.lblPorAbo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label36.Location = New System.Drawing.Point(766, 21)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(65, 13)
        Me.Label36.TabIndex = 38
        Me.Label36.Text = "% Pagado"
        '
        'lblPorSaldo
        '
        Me.lblPorSaldo.AutoSize = True
        Me.lblPorSaldo.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPorSaldo.Location = New System.Drawing.Point(847, 45)
        Me.lblPorSaldo.Name = "lblPorSaldo"
        Me.lblPorSaldo.Size = New System.Drawing.Size(71, 13)
        Me.lblPorSaldo.TabIndex = 37
        Me.lblPorSaldo.Text = "lblPorSaldo"
        Me.lblPorSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label34.Location = New System.Drawing.Point(766, 45)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(55, 13)
        Me.Label34.TabIndex = 36
        Me.Label34.Text = "% Saldo"
        '
        'lblTotFac
        '
        Me.lblTotFac.AutoSize = True
        Me.lblTotFac.ForeColor = System.Drawing.Color.DarkRed
        Me.lblTotFac.Location = New System.Drawing.Point(676, 45)
        Me.lblTotFac.Name = "lblTotFac"
        Me.lblTotFac.Size = New System.Drawing.Size(57, 13)
        Me.lblTotFac.TabIndex = 35
        Me.lblTotFac.Text = "lblTotFac"
        Me.lblTotFac.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label32.Location = New System.Drawing.Point(593, 45)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(62, 13)
        Me.Label32.TabIndex = 34
        Me.Label32.Text = "Total Fac."
        '
        'lblPagoFG
        '
        Me.lblPagoFG.AutoSize = True
        Me.lblPagoFG.ForeColor = System.Drawing.Color.DarkRed
        Me.lblPagoFG.Location = New System.Drawing.Point(492, 45)
        Me.lblPagoFG.Name = "lblPagoFG"
        Me.lblPagoFG.Size = New System.Drawing.Size(63, 13)
        Me.lblPagoFG.TabIndex = 33
        Me.lblPagoFG.Text = "lblPagoFG"
        Me.lblPagoFG.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSaldo
        '
        Me.lblSaldo.AutoSize = True
        Me.lblSaldo.ForeColor = System.Drawing.Color.DarkRed
        Me.lblSaldo.Location = New System.Drawing.Point(492, 21)
        Me.lblSaldo.Name = "lblSaldo"
        Me.lblSaldo.Size = New System.Drawing.Size(52, 13)
        Me.lblSaldo.TabIndex = 32
        Me.lblSaldo.Text = "lblSaldo"
        Me.lblSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label29.Location = New System.Drawing.Point(405, 45)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(87, 13)
        Me.Label29.TabIndex = 31
        Me.Label29.Text = "Pagos x F Gar"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label30.Location = New System.Drawing.Point(405, 22)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(87, 13)
        Me.Label30.TabIndex = 30
        Me.Label30.Text = "Saldo x Pagar"
        '
        'lblRetenido
        '
        Me.lblRetenido.AutoSize = True
        Me.lblRetenido.ForeColor = System.Drawing.Color.DarkRed
        Me.lblRetenido.Location = New System.Drawing.Point(299, 45)
        Me.lblRetenido.Name = "lblRetenido"
        Me.lblRetenido.Size = New System.Drawing.Size(70, 13)
        Me.lblRetenido.TabIndex = 29
        Me.lblRetenido.Text = "lblRetenido"
        Me.lblRetenido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblAbonado
        '
        Me.lblAbonado.AutoSize = True
        Me.lblAbonado.ForeColor = System.Drawing.Color.DarkRed
        Me.lblAbonado.Location = New System.Drawing.Point(299, 21)
        Me.lblAbonado.Name = "lblAbonado"
        Me.lblAbonado.Size = New System.Drawing.Size(70, 13)
        Me.lblAbonado.TabIndex = 28
        Me.lblAbonado.Text = "lblAbonado"
        Me.lblAbonado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label25.Location = New System.Drawing.Point(197, 45)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(89, 13)
        Me.Label25.TabIndex = 27
        Me.Label25.Text = "Total Retenido"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label26.Location = New System.Drawing.Point(197, 21)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(81, 13)
        Me.Label26.TabIndex = 26
        Me.Label26.Text = "Total Pagado"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(778, 117)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(38, 13)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Obs.:"
        '
        'lblDoc
        '
        Me.lblDoc.BackColor = System.Drawing.Color.White
        Me.lblDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDoc.ForeColor = System.Drawing.Color.DarkRed
        Me.lblDoc.Location = New System.Drawing.Point(642, 94)
        Me.lblDoc.Name = "lblDoc"
        Me.lblDoc.Size = New System.Drawing.Size(121, 18)
        Me.lblDoc.TabIndex = 17
        Me.lblDoc.Text = "Label12"
        '
        'lblFechaFin
        '
        Me.lblFechaFin.BackColor = System.Drawing.Color.White
        Me.lblFechaFin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFechaFin.ForeColor = System.Drawing.Color.DarkRed
        Me.lblFechaFin.Location = New System.Drawing.Point(363, 95)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(117, 18)
        Me.lblFechaFin.TabIndex = 16
        Me.lblFechaFin.Text = "Label13"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label10.Location = New System.Drawing.Point(560, 95)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Doc. Rend."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label11.Location = New System.Drawing.Point(287, 93)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(37, 13)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "F. Fin"
        '
        'lblMoneda
        '
        Me.lblMoneda.BackColor = System.Drawing.Color.White
        Me.lblMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblMoneda.ForeColor = System.Drawing.Color.DarkRed
        Me.lblMoneda.Location = New System.Drawing.Point(363, 119)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(117, 18)
        Me.lblMoneda.TabIndex = 13
        Me.lblMoneda.Text = "Label8"
        '
        'lblFechaIni
        '
        Me.lblFechaIni.BackColor = System.Drawing.Color.White
        Me.lblFechaIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFechaIni.ForeColor = System.Drawing.Color.DarkRed
        Me.lblFechaIni.Location = New System.Drawing.Point(103, 94)
        Me.lblFechaIni.Name = "lblFechaIni"
        Me.lblFechaIni.Size = New System.Drawing.Size(117, 18)
        Me.lblFechaIni.TabIndex = 12
        Me.lblFechaIni.Text = "Label9"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(287, 121)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Moneda"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(11, 93)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "F. Inicio"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(11, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Contrato"
        '
        'txtDescripcionEsp
        '
        Me.txtDescripcionEsp.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDescripcionEsp.BackColor = System.Drawing.Color.Ivory
        Me.txtDescripcionEsp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcionEsp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcionEsp.Enabled = False
        Me.txtDescripcionEsp.ForeColor = System.Drawing.Color.Black
        Me.txtDescripcionEsp.KeyEnter = True
        Me.txtDescripcionEsp.Location = New System.Drawing.Point(1015, 14)
        Me.txtDescripcionEsp.Name = "txtDescripcionEsp"
        Me.txtDescripcionEsp.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDescripcionEsp.ShortcutsEnabled = False
        Me.txtDescripcionEsp.Size = New System.Drawing.Size(184, 21)
        Me.txtDescripcionEsp.TabIndex = 7
        Me.txtDescripcionEsp.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboPartidas
        '
        Me.cboPartidas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPartidas.BackColor = System.Drawing.Color.Ivory
        Me.cboPartidas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartidas.ForeColor = System.Drawing.Color.Black
        Me.cboPartidas.FormattingEnabled = True
        Me.cboPartidas.KeyEnter = True
        Me.cboPartidas.Location = New System.Drawing.Point(74, 66)
        Me.cboPartidas.Name = "cboPartidas"
        Me.cboPartidas.Size = New System.Drawing.Size(689, 21)
        Me.cboPartidas.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(397, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Contratista"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(11, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Obra"
        '
        'cboContratista
        '
        Me.cboContratista.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboContratista.BackColor = System.Drawing.Color.Ivory
        Me.cboContratista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContratista.ForeColor = System.Drawing.Color.Black
        Me.cboContratista.FormattingEnabled = True
        Me.cboContratista.KeyEnter = True
        Me.cboContratista.Location = New System.Drawing.Point(473, 15)
        Me.cboContratista.Name = "cboContratista"
        Me.cboContratista.Size = New System.Drawing.Size(290, 21)
        Me.cboContratista.TabIndex = 1
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(74, 14)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(317, 21)
        Me.cboObra.TabIndex = 0
        '
        'lblTipoC
        '
        Me.lblTipoC.AutoSize = True
        Me.lblTipoC.ForeColor = System.Drawing.Color.DarkRed
        Me.lblTipoC.Location = New System.Drawing.Point(1170, 4)
        Me.lblTipoC.Name = "lblTipoC"
        Me.lblTipoC.Size = New System.Drawing.Size(45, 13)
        Me.lblTipoC.TabIndex = 23
        Me.lblTipoC.Text = "Label17"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label18.Location = New System.Drawing.Point(1137, 4)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(27, 13)
        Me.Label18.TabIndex = 22
        Me.Label18.Text = "T.C."
        '
        'dgvHistorial
        '
        Me.dgvHistorial.AllowUserToAddRows = False
        Me.dgvHistorial.BackgroundColor = System.Drawing.Color.White
        Me.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistorial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TD, Me.Column1, Me.Column4, Me.Column5, Me.Column6, Me.NC, Me.TotNA, Me.Column7, Me.Column10, Me.IdValorizacion, Me.FechaPago, Me.Column8, Me.Column9, Me.Column11, Me.Column2, Me.TotPagar, Me.Pagado, Me.Column3, Me.IdTipoPago, Me.Tipo1, Me.Column12})
        Me.dgvHistorial.EnableHeadersVisualStyles = False
        Me.dgvHistorial.Location = New System.Drawing.Point(12, 263)
        Me.dgvHistorial.Name = "dgvHistorial"
        Me.dgvHistorial.RowHeadersVisible = False
        Me.dgvHistorial.Size = New System.Drawing.Size(1206, 169)
        Me.dgvHistorial.TabIndex = 24
        '
        'TD
        '
        Me.TD.DataPropertyName = "AbrDoc"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.TD.DefaultCellStyle = DataGridViewCellStyle1
        Me.TD.HeaderText = "TD"
        Me.TD.Name = "TD"
        Me.TD.Width = 30
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "FechaBase"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column1.HeaderText = "F. Emi."
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 70
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Nro"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column4.HeaderText = "Nº"
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 60
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "MonSimbolo"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column5.HeaderText = "Mon."
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 45
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column6.HeaderText = "Importe"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 70
        '
        'NC
        '
        Me.NC.DataPropertyName = "NA"
        Me.NC.HeaderText = "NC"
        Me.NC.Name = "NC"
        Me.NC.Width = 50
        '
        'TotNA
        '
        Me.TotNA.DataPropertyName = "SUMNA"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.TotNA.DefaultCellStyle = DataGridViewCellStyle6
        Me.TotNA.HeaderText = "Tot NC"
        Me.TotNA.Name = "TotNA"
        Me.TotNA.Width = 70
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "MtoDP"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.Column7.DefaultCellStyle = DataGridViewCellStyle7
        Me.Column7.HeaderText = "% Detr."
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 70
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "Detra"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle8
        Me.Column10.HeaderText = "Imp. Detr."
        Me.Column10.Name = "Column10"
        Me.Column10.Width = 80
        '
        'IdValorizacion
        '
        Me.IdValorizacion.DataPropertyName = "IdValorizacion"
        Me.IdValorizacion.HeaderText = "IdValorizacion"
        Me.IdValorizacion.Name = "IdValorizacion"
        Me.IdValorizacion.Visible = False
        '
        'FechaPago
        '
        Me.FechaPago.DataPropertyName = "FechaInicio"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.Format = "d"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.FechaPago.DefaultCellStyle = DataGridViewCellStyle9
        Me.FechaPago.HeaderText = "Fecha"
        Me.FechaPago.Name = "FechaPago"
        Me.FechaPago.Width = 70
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Concepto"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle10
        Me.Column8.HeaderText = "Concepto"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 120
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "TotalVal"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.Column9.DefaultCellStyle = DataGridViewCellStyle11
        Me.Column9.HeaderText = "Valorización"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 80
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "RetPor"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.Column11.DefaultCellStyle = DataGridViewCellStyle12
        Me.Column11.HeaderText = "Retención"
        Me.Column11.Name = "Column11"
        Me.Column11.Width = 75
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "OtrosD"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle13.Format = "N2"
        DataGridViewCellStyle13.NullValue = Nothing
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle13
        Me.Column2.HeaderText = "O. Des."
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 70
        '
        'TotPagar
        '
        Me.TotPagar.DataPropertyName = "TotPagar"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.Red
        DataGridViewCellStyle14.Format = "N4"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.TotPagar.DefaultCellStyle = DataGridViewCellStyle14
        Me.TotPagar.HeaderText = "A Pagar"
        Me.TotPagar.Name = "TotPagar"
        Me.TotPagar.Width = 80
        '
        'Pagado
        '
        Me.Pagado.DataPropertyName = "TotPagado"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle15.Format = "N4"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.Pagado.DefaultCellStyle = DataGridViewCellStyle15
        Me.Pagado.HeaderText = "Pagado"
        Me.Pagado.Name = "Pagado"
        Me.Pagado.Width = 80
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "PorcPaga"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle16.Format = "N1"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle16
        Me.Column3.HeaderText = "% Pag"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 65
        '
        'IdTipoPago
        '
        Me.IdTipoPago.DataPropertyName = "IdTipoPago"
        Me.IdTipoPago.HeaderText = "IdTipoPago"
        Me.IdTipoPago.Name = "IdTipoPago"
        Me.IdTipoPago.Visible = False
        '
        'Tipo1
        '
        Me.Tipo1.DataPropertyName = "Tipo1"
        Me.Tipo1.HeaderText = "Tipo1"
        Me.Tipo1.Name = "Tipo1"
        Me.Tipo1.Visible = False
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "OtrosDesc"
        Me.Column12.HeaderText = "OtrosDesc"
        Me.Column12.Name = "Column12"
        Me.Column12.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(995, 436)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(223, 13)
        Me.Label4.TabIndex = 28
        Me.Label4.Text = "F4 = Programar Detalle de Pagos"
        '
        'frmContratistas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1227, 455)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dgvHistorial)
        Me.Controls.Add(Me.lblTipoC)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "frmContratistas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Programación de Pagos (Valorizaciones - Adelantos - FG)"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDescripcionEsp As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboPartidas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboContratista As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents lblPorRete As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPorAbo As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents lblPorSaldo As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents lblTotFac As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents lblPagoFG As System.Windows.Forms.Label
    Friend WithEvents lblSaldo As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents lblRetenido As System.Windows.Forms.Label
    Friend WithEvents lblAbonado As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblDoc As System.Windows.Forms.Label
    Friend WithEvents lblFechaFin As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblMoneda As System.Windows.Forms.Label
    Friend WithEvents lblFechaIni As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents lblTipoC As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dgvHistorial As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblContrato As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblOrden As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtRuc As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblFGaran As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents lblPresu As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents TD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotNA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdValorizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotPagar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pagado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdTipoPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPresupuesto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents CCosto As ctrLibreria.Controles.BeComboBox
End Class
