<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmREP_Poliza_Detalle
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmREP_Poliza_Detalle))
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCia = New ctrLibreria.Controles.BeComboBox
        Me.chkTodos = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.mtbFechaIni = New ctrLibreria.Controles.BeMaskedTextBox
        Me.mtbFechaFin = New ctrLibreria.Controles.BeMaskedTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.btnSalir = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rdb1 = New System.Windows.Forms.RadioButton
        Me.rdb2 = New System.Windows.Forms.RadioButton
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Aseguradora"
        '
        'cboCia
        '
        Me.cboCia.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCia.BackColor = System.Drawing.Color.Ivory
        Me.cboCia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCia.ForeColor = System.Drawing.Color.Black
        Me.cboCia.FormattingEnabled = True
        Me.cboCia.KeyEnter = True
        Me.cboCia.Location = New System.Drawing.Point(84, 14)
        Me.cboCia.Name = "cboCia"
        Me.cboCia.Size = New System.Drawing.Size(139, 21)
        Me.cboCia.TabIndex = 0
        '
        'chkTodos
        '
        Me.chkTodos.AutoSize = True
        Me.chkTodos.Location = New System.Drawing.Point(238, 16)
        Me.chkTodos.Name = "chkTodos"
        Me.chkTodos.Size = New System.Drawing.Size(56, 17)
        Me.chkTodos.TabIndex = 1
        Me.chkTodos.Text = "Todos"
        Me.chkTodos.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Fecha Inicio"
        '
        'mtbFechaIni
        '
        Me.mtbFechaIni.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaIni.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaIni.BeepOnError = True
        Me.mtbFechaIni.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaIni.HidePromptOnLeave = True
        Me.mtbFechaIni.KeyEnter = True
        Me.mtbFechaIni.Location = New System.Drawing.Point(84, 41)
        Me.mtbFechaIni.Mask = "00/00/0000"
        Me.mtbFechaIni.Name = "mtbFechaIni"
        Me.mtbFechaIni.Size = New System.Drawing.Size(65, 20)
        Me.mtbFechaIni.TabIndex = 2
        Me.mtbFechaIni.ValidatingType = GetType(Date)
        '
        'mtbFechaFin
        '
        Me.mtbFechaFin.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaFin.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaFin.BeepOnError = True
        Me.mtbFechaFin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaFin.HidePromptOnLeave = True
        Me.mtbFechaFin.KeyEnter = True
        Me.mtbFechaFin.Location = New System.Drawing.Point(229, 43)
        Me.mtbFechaFin.Mask = "00/00/0000"
        Me.mtbFechaFin.Name = "mtbFechaFin"
        Me.mtbFechaFin.Size = New System.Drawing.Size(65, 20)
        Me.mtbFechaFin.TabIndex = 3
        Me.mtbFechaFin.ValidatingType = GetType(Date)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(161, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Fecha Final"
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnImprimir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(156, 143)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(73, 25)
        Me.btnImprimir.TabIndex = 4
        Me.btnImprimir.Text = "  &Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalir.Location = New System.Drawing.Point(235, 143)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(73, 25)
        Me.btnSalir.TabIndex = 5
        Me.btnSalir.Text = "  &Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdb2)
        Me.GroupBox1.Controls.Add(Me.mtbFechaFin)
        Me.GroupBox1.Controls.Add(Me.rdb1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboCia)
        Me.GroupBox1.Controls.Add(Me.chkTodos)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.mtbFechaIni)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(305, 114)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        '
        'rdb1
        '
        Me.rdb1.AutoSize = True
        Me.rdb1.Location = New System.Drawing.Point(14, 82)
        Me.rdb1.Name = "rdb1"
        Me.rdb1.Size = New System.Drawing.Size(79, 17)
        Me.rdb1.TabIndex = 7
        Me.rdb1.TabStop = True
        Me.rdb1.Text = "Detallado 1"
        Me.rdb1.UseVisualStyleBackColor = True
        '
        'rdb2
        '
        Me.rdb2.AutoSize = True
        Me.rdb2.Location = New System.Drawing.Point(204, 82)
        Me.rdb2.Name = "rdb2"
        Me.rdb2.Size = New System.Drawing.Size(79, 17)
        Me.rdb2.TabIndex = 8
        Me.rdb2.TabStop = True
        Me.rdb2.Text = "Detallado 2"
        Me.rdb2.UseVisualStyleBackColor = True
        '
        'frmREP_Poliza_Detalle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(312, 175)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmREP_Poliza_Detalle"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte  de Polizas y Detalle"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCia As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkTodos As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents mtbFechaIni As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents mtbFechaFin As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdb2 As System.Windows.Forms.RadioButton
    Friend WithEvents rdb1 As System.Windows.Forms.RadioButton
End Class
