Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReportePagosProv

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    Dim rptPagosProv As New rptPagosProv
    Dim rptPagosProvxCC As New rptPagosProvxCC
    Dim rptPagosFacturasxSemana As New rptPagosFacturasxSemana
    Dim rptPagosProvxBancos As New rptPagosProvxCuenta
    Private eGastosGenerales As clsGastosGenerales

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReportePagosProv = Nothing
    Public Shared Function Instance() As frmReportePagosProv
        If frmInstance Is Nothing Then
            frmInstance = New frmReportePagosProv
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReportePagosProv_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReportePagosProv_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboA�o.Text = Year(Now)
        cboA�o.DropDownStyle = ComboBoxStyle.DropDownList
        cboSemana.DropDownStyle = ComboBoxStyle.DropDownList
    End Sub

    Private Sub cboA�o_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboA�o.SelectedIndexChanged
        eGastosGenerales = New clsGastosGenerales
        'Dim dtTable As DataTable
        'dtTable = New DataTable
        cboSemana.DataSource = eGastosGenerales.fListarSemanasdePago(gEmpresa, Trim(cboA�o.Text)) 'eGastosGenerales.fListarCronogramas5(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboSemana.ValueMember = "NroSemana"
            cboSemana.DisplayMember = "NroSemana"
            cboSemana.SelectedIndex = 0
        End If

        'Try
        '    eSesionCajas = New clsSesionCajas
        '    cboArea.DataSource = eSesionCajas.fListarArea(gEmpresa)
        '    If eSesionCajas.iNroRegistros > 0 Then
        '        cboArea.ValueMember = "AreaCodigo"
        '        cboArea.DisplayMember = "AreaNombre"
        '        cboArea.SelectedIndex = -1
        '    End If

        'Catch ex As Exception
        'End Try

        'If dtTable.Rows.Count > 0 Then
        '    Dim SemanaMax As String = ""
        '    SemanaMax = Trim(dtTable.Rows(0).Item("NroSemana"))
        '    Try
        '        eGastosGenerales = New clsGastosGenerales
        '        cboSemana.DataSource = eGastosGenerales.fListarArea(gEmpresa, Trim(cboA�o.Text))
        '        If eGastosGenerales.iNroRegistros > 0 Then
        '            cboSemana.ValueMember = "AreaCodigo"
        '            cboSemana.DisplayMember = "AreaNombre"
        '            cboSemana.SelectedValue = SemanaMax
        '        End If
        '    Catch ex As Exception
        '    End Try

        'End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If cboA�o.Text = "" Then
            MessageBox.Show("Seleccione un A�o", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If cboSemana.Text = "" Then
            MessageBox.Show("Seleccione un N�mero de Semana", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        If rdb1.Checked = True Then
            rptPagosProv.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptPagosProv.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NroSemana")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboSemana.SelectedValue)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '2
            crParameterFieldDefinitions = rptPagosProv.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptPagosProv.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptPagosProv.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptPagosProv.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            CrystalReportViewer1.ReportSource = rptPagosProv
        End If

        If rdb2.Checked = True Then
            rptPagosProvxCC.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptPagosProvxCC.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NroSemana")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboSemana.SelectedValue)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '2
            crParameterFieldDefinitions = rptPagosProvxCC.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptPagosProvxCC.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptPagosProvxCC.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptPagosProvxCC.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            CrystalReportViewer1.ReportSource = rptPagosProvxCC
        End If

        If rdb3.Checked = True Then
            rptPagosFacturasxSemana.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptPagosFacturasxSemana.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NroSemana")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboSemana.SelectedValue)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '2
            crParameterFieldDefinitions = rptPagosFacturasxSemana.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptPagosFacturasxSemana.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptPagosFacturasxSemana.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptPagosFacturasxSemana.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            CrystalReportViewer1.ReportSource = rptPagosFacturasxSemana
        End If


        If rdb4.Checked = True Then
            rptPagosProvxBancos.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptPagosProvxBancos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NroSemana")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboSemana.SelectedValue)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '2
            crParameterFieldDefinitions = rptPagosProvxBancos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptPagosProvxBancos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(cboA�o.Text)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptPagosProvxBancos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptPagosProvxBancos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            CrystalReportViewer1.ReportSource = rptPagosProvxBancos
        End If

    End Sub

    Private Sub rdb1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdb1.CheckedChanged

    End Sub
End Class