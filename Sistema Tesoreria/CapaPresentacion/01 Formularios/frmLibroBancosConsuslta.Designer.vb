<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroBancosConsuslta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.txtCobradosSi = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.dgvLibroDet = New System.Windows.Forms.DataGridView
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.txtCobradosNo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.txtCodDet = New ctrLibreria.Controles.BeTextBox
        Me.txtMarca2 = New ctrLibreria.Controles.BeTextBox
        Me.txtTipoCambio = New ctrLibreria.Controles.BeTextBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldo = New ctrLibreria.Controles.BeTextBox
        Me.txtSaldoInicial = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel
        Me.lblCtaEn = New ctrLibreria.Controles.BeLabel
        Me.lblTipoCta = New ctrLibreria.Controles.BeLabel
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.cboAno = New ctrLibreria.Controles.BeComboBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel
        Me.cboCuentas = New ctrLibreria.Controles.BeComboBox
        Me.lblCodMoneda = New ctrLibreria.Controles.BeLabel
        Me.lblMoneda = New ctrLibreria.Controles.BeLabel
        Me.cboBancos = New ctrLibreria.Controles.BeComboBox
        Me.cboMes = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.txtCod = New ctrLibreria.Controles.BeTextBox
        Me.txtMarca = New ctrLibreria.Controles.BeTextBox
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdCab = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Marca1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NroDocumento2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ITF = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Identificarx = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Conciliado = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.ConciliadoS = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtCobradosSi
        '
        Me.txtCobradosSi.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCobradosSi.BackColor = System.Drawing.Color.Silver
        Me.txtCobradosSi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCobradosSi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCobradosSi.ForeColor = System.Drawing.Color.Black
        Me.txtCobradosSi.KeyEnter = True
        Me.txtCobradosSi.Location = New System.Drawing.Point(127, 410)
        Me.txtCobradosSi.Name = "txtCobradosSi"
        Me.txtCobradosSi.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCobradosSi.ReadOnly = True
        Me.txtCobradosSi.ShortcutsEnabled = False
        Me.txtCobradosSi.Size = New System.Drawing.Size(115, 20)
        Me.txtCobradosSi.TabIndex = 2
        Me.txtCobradosSi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCobradosSi.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.BeLabel11.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel11.Location = New System.Drawing.Point(662, 388)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(87, 16)
        Me.BeLabel11.TabIndex = 22
        Me.BeLabel11.Text = "Saldo Final"
        '
        'dgvLibroDet
        '
        Me.dgvLibroDet.AllowUserToAddRows = False
        Me.dgvLibroDet.BackgroundColor = System.Drawing.Color.White
        Me.dgvLibroDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLibroDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column3, Me.Column5, Me.Column4, Me.Column6, Me.Column10, Me.Column8, Me.Column9, Me.Column2, Me.Column7, Me.Column11, Me.Column12, Me.IdCab, Me.Marca1, Me.NroDocumento2, Me.ITF, Me.Column13, Me.Column14, Me.Column15, Me.Column16, Me.Column17, Me.Column18, Me.Column19, Me.Column20, Me.Column21, Me.Column22, Me.Identificarx, Me.Conciliado, Me.ConciliadoS})
        Me.dgvLibroDet.EnableHeadersVisualStyles = False
        Me.dgvLibroDet.Location = New System.Drawing.Point(8, 29)
        Me.dgvLibroDet.Name = "dgvLibroDet"
        Me.dgvLibroDet.RowHeadersVisible = False
        Me.dgvLibroDet.Size = New System.Drawing.Size(879, 349)
        Me.dgvLibroDet.TabIndex = 17
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(5, 412)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(116, 13)
        Me.BeLabel13.TabIndex = 0
        Me.BeLabel13.Text = "Cheques Cobrados"
        '
        'txtCobradosNo
        '
        Me.txtCobradosNo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCobradosNo.BackColor = System.Drawing.Color.Aqua
        Me.txtCobradosNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCobradosNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCobradosNo.ForeColor = System.Drawing.Color.Black
        Me.txtCobradosNo.KeyEnter = True
        Me.txtCobradosNo.Location = New System.Drawing.Point(389, 410)
        Me.txtCobradosNo.Name = "txtCobradosNo"
        Me.txtCobradosNo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCobradosNo.ReadOnly = True
        Me.txtCobradosNo.ShortcutsEnabled = False
        Me.txtCobradosNo.Size = New System.Drawing.Size(115, 20)
        Me.txtCobradosNo.TabIndex = 5
        Me.txtCobradosNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCobradosNo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(248, 412)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(135, 13)
        Me.BeLabel14.TabIndex = 3
        Me.BeLabel14.Text = "Cheques No Cobrados"
        '
        'txtCodDet
        '
        Me.txtCodDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodDet.BackColor = System.Drawing.Color.Ivory
        Me.txtCodDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodDet.ForeColor = System.Drawing.Color.Black
        Me.txtCodDet.KeyEnter = True
        Me.txtCodDet.Location = New System.Drawing.Point(1282, 343)
        Me.txtCodDet.Name = "txtCodDet"
        Me.txtCodDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodDet.ShortcutsEnabled = False
        Me.txtCodDet.Size = New System.Drawing.Size(121, 20)
        Me.txtCodDet.TabIndex = 22
        Me.txtCodDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCodDet.Visible = False
        '
        'txtMarca2
        '
        Me.txtMarca2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMarca2.BackColor = System.Drawing.Color.Ivory
        Me.txtMarca2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca2.ForeColor = System.Drawing.Color.Black
        Me.txtMarca2.KeyEnter = True
        Me.txtMarca2.Location = New System.Drawing.Point(1282, 397)
        Me.txtMarca2.Name = "txtMarca2"
        Me.txtMarca2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMarca2.ShortcutsEnabled = False
        Me.txtMarca2.Size = New System.Drawing.Size(46, 20)
        Me.txtMarca2.TabIndex = 26
        Me.txtMarca2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtMarca2.Visible = False
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.txtTipoCambio.KeyEnter = True
        Me.txtTipoCambio.Location = New System.Drawing.Point(1282, 246)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTipoCambio.ShortcutsEnabled = False
        Me.txtTipoCambio.Size = New System.Drawing.Size(86, 20)
        Me.txtTipoCambio.TabIndex = 21
        Me.txtTipoCambio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtTipoCambio.Visible = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(1279, 230)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel1.TabIndex = 20
        Me.BeLabel1.Text = "T. Cambio"
        Me.BeLabel1.Visible = False
        '
        'txtSaldo
        '
        Me.txtSaldo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldo.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtSaldo.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldo.KeyEnter = True
        Me.txtSaldo.Location = New System.Drawing.Point(762, 384)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldo.ReadOnly = True
        Me.txtSaldo.ShortcutsEnabled = False
        Me.txtSaldo.Size = New System.Drawing.Size(125, 20)
        Me.txtSaldo.TabIndex = 23
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSaldoInicial
        '
        Me.txtSaldoInicial.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoInicial.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldoInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoInicial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoInicial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoInicial.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldoInicial.KeyEnter = True
        Me.txtSaldoInicial.Location = New System.Drawing.Point(755, 6)
        Me.txtSaldoInicial.Name = "txtSaldoInicial"
        Me.txtSaldoInicial.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoInicial.ReadOnly = True
        Me.txtSaldoInicial.ShortcutsEnabled = False
        Me.txtSaldoInicial.Size = New System.Drawing.Size(132, 20)
        Me.txtSaldoInicial.TabIndex = 16
        Me.txtSaldoInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoInicial.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel19.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel19.Location = New System.Drawing.Point(653, 10)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(96, 16)
        Me.BeLabel19.TabIndex = 15
        Me.BeLabel19.Text = "Saldo Inicial"
        '
        'lblCtaEn
        '
        Me.lblCtaEn.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCtaEn.AutoSize = True
        Me.lblCtaEn.BackColor = System.Drawing.Color.Transparent
        Me.lblCtaEn.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCtaEn.ForeColor = System.Drawing.Color.Black
        Me.lblCtaEn.Location = New System.Drawing.Point(1279, 180)
        Me.lblCtaEn.Name = "lblCtaEn"
        Me.lblCtaEn.Size = New System.Drawing.Size(64, 13)
        Me.lblCtaEn.TabIndex = 25
        Me.lblCtaEn.Text = "[lblCtaEn]"
        Me.lblCtaEn.Visible = False
        '
        'lblTipoCta
        '
        Me.lblTipoCta.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblTipoCta.AutoSize = True
        Me.lblTipoCta.BackColor = System.Drawing.Color.Transparent
        Me.lblTipoCta.Font = New System.Drawing.Font("Arial Narrow", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoCta.ForeColor = System.Drawing.Color.Navy
        Me.lblTipoCta.Location = New System.Drawing.Point(733, 8)
        Me.lblTipoCta.Name = "lblTipoCta"
        Me.lblTipoCta.Size = New System.Drawing.Size(37, 13)
        Me.lblTipoCta.TabIndex = 15
        Me.lblTipoCta.Text = "[TipoCta]"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(5, 33)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel15.TabIndex = 5
        Me.BeLabel15.Text = "Año"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(120, 34)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel3.TabIndex = 7
        Me.BeLabel3.Text = "Mes"
        '
        'cboAno
        '
        Me.cboAno.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAno.BackColor = System.Drawing.Color.Ivory
        Me.cboAno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAno.ForeColor = System.Drawing.Color.Black
        Me.cboAno.FormattingEnabled = True
        Me.cboAno.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040"})
        Me.cboAno.KeyEnter = True
        Me.cboAno.Location = New System.Drawing.Point(44, 29)
        Me.cboAno.Name = "cboAno"
        Me.cboAno.Size = New System.Drawing.Size(70, 21)
        Me.cboAno.TabIndex = 6
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.TextBox1)
        Me.Panel2.Controls.Add(Me.BeLabel23)
        Me.Panel2.Controls.Add(Me.BeLabel13)
        Me.Panel2.Controls.Add(Me.btnImprimir)
        Me.Panel2.Controls.Add(Me.txtCobradosSi)
        Me.Panel2.Controls.Add(Me.Panel8)
        Me.Panel2.Controls.Add(Me.txtCobradosNo)
        Me.Panel2.Controls.Add(Me.BeLabel14)
        Me.Panel2.Controls.Add(Me.BeLabel28)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.BeLabel19)
        Me.Panel2.Controls.Add(Me.txtSaldoInicial)
        Me.Panel2.Controls.Add(Me.BeLabel11)
        Me.Panel2.Controls.Add(Me.dgvLibroDet)
        Me.Panel2.Controls.Add(Me.txtSaldo)
        Me.Panel2.Location = New System.Drawing.Point(3, 68)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(897, 442)
        Me.Panel2.TabIndex = 19
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(324, 3)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(134, 23)
        Me.btnImprimir.TabIndex = 30
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.MistyRose
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Location = New System.Drawing.Point(591, 381)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(33, 14)
        Me.Panel8.TabIndex = 7
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel28.ForeColor = System.Drawing.Color.Black
        Me.BeLabel28.Location = New System.Drawing.Point(539, 381)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(49, 13)
        Me.BeLabel28.TabIndex = 6
        Me.BeLabel28.Text = "Anexos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(397, 381)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Label2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 381)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Label1"
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(112, 4)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(152, 20)
        Me.TextBox1.TabIndex = 30
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(12, 8)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(94, 13)
        Me.BeLabel23.TabIndex = 31
        Me.BeLabel23.Text = "Buscar en Lista"
        '
        'cboCuentas
        '
        Me.cboCuentas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuentas.BackColor = System.Drawing.Color.Ivory
        Me.cboCuentas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentas.ForeColor = System.Drawing.Color.Black
        Me.cboCuentas.FormattingEnabled = True
        Me.cboCuentas.KeyEnter = True
        Me.cboCuentas.Location = New System.Drawing.Point(324, 4)
        Me.cboCuentas.Name = "cboCuentas"
        Me.cboCuentas.Size = New System.Drawing.Size(336, 21)
        Me.cboCuentas.TabIndex = 3
        '
        'lblCodMoneda
        '
        Me.lblCodMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCodMoneda.AutoSize = True
        Me.lblCodMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblCodMoneda.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodMoneda.ForeColor = System.Drawing.Color.Black
        Me.lblCodMoneda.Location = New System.Drawing.Point(1279, 298)
        Me.lblCodMoneda.Name = "lblCodMoneda"
        Me.lblCodMoneda.Size = New System.Drawing.Size(84, 13)
        Me.lblCodMoneda.TabIndex = 24
        Me.lblCodMoneda.Text = "[CodMoneda]"
        Me.lblCodMoneda.Visible = False
        '
        'lblMoneda
        '
        Me.lblMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblMoneda.Font = New System.Drawing.Font("Arial Narrow", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoneda.ForeColor = System.Drawing.Color.Navy
        Me.lblMoneda.Location = New System.Drawing.Point(666, 8)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(37, 13)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.Text = "[Moneda]"
        '
        'cboBancos
        '
        Me.cboBancos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBancos.BackColor = System.Drawing.Color.Ivory
        Me.cboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBancos.ForeColor = System.Drawing.Color.Black
        Me.cboBancos.FormattingEnabled = True
        Me.cboBancos.KeyEnter = True
        Me.cboBancos.Location = New System.Drawing.Point(44, 4)
        Me.cboBancos.Name = "cboBancos"
        Me.cboBancos.Size = New System.Drawing.Size(220, 21)
        Me.cboBancos.TabIndex = 1
        '
        'cboMes
        '
        Me.cboMes.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMes.BackColor = System.Drawing.Color.Ivory
        Me.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMes.ForeColor = System.Drawing.Color.Black
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cboMes.KeyEnter = True
        Me.cboMes.Location = New System.Drawing.Point(150, 29)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(114, 21)
        Me.cboMes.TabIndex = 8
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(5, 8)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel4.TabIndex = 0
        Me.BeLabel4.Text = "Banco"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Arial Narrow", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(270, 34)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel12.TabIndex = 13
        Me.BeLabel12.Text = "Código Libro"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblTipoCta)
        Me.Panel1.Controls.Add(Me.BeLabel15)
        Me.Panel1.Controls.Add(Me.BeLabel3)
        Me.Panel1.Controls.Add(Me.cboAno)
        Me.Panel1.Controls.Add(Me.cboCuentas)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Controls.Add(Me.cboBancos)
        Me.Panel1.Controls.Add(Me.cboMes)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Controls.Add(Me.BeLabel12)
        Me.Panel1.Controls.Add(Me.BeLabel5)
        Me.Panel1.Controls.Add(Me.txtCod)
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(897, 59)
        Me.Panel1.TabIndex = 18
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(270, 8)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel5.TabIndex = 2
        Me.BeLabel5.Text = "Cuenta"
        '
        'txtCod
        '
        Me.txtCod.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCod.BackColor = System.Drawing.Color.Ivory
        Me.txtCod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCod.Enabled = False
        Me.txtCod.Font = New System.Drawing.Font("Arial Narrow", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCod.ForeColor = System.Drawing.Color.Black
        Me.txtCod.KeyEnter = True
        Me.txtCod.Location = New System.Drawing.Point(324, 31)
        Me.txtCod.Name = "txtCod"
        Me.txtCod.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCod.ShortcutsEnabled = False
        Me.txtCod.Size = New System.Drawing.Size(84, 17)
        Me.txtCod.TabIndex = 14
        Me.txtCod.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtMarca
        '
        Me.txtMarca.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMarca.BackColor = System.Drawing.Color.Ivory
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.ForeColor = System.Drawing.Color.Black
        Me.txtMarca.KeyEnter = True
        Me.txtMarca.Location = New System.Drawing.Point(1282, 371)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMarca.ShortcutsEnabled = False
        Me.txtMarca.Size = New System.Drawing.Size(46, 20)
        Me.txtMarca.TabIndex = 23
        Me.txtMarca.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtMarca.Visible = False
        '
        'Column1
        '
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.Format = "M"
        DataGridViewCellStyle19.NullValue = Nothing
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle19
        Me.Column1.FillWeight = 95.0!
        Me.Column1.HeaderText = "Día"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column1.Width = 30
        '
        'Column3
        '
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle20
        Me.Column3.FillWeight = 95.0!
        Me.Column3.HeaderText = "Cheq. / Not."
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column5
        '
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle21
        Me.Column5.FillWeight = 95.0!
        Me.Column5.HeaderText = "Concepto"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column5.Width = 290
        '
        'Column4
        '
        DataGridViewCellStyle22.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle22
        Me.Column4.FillWeight = 95.0!
        Me.Column4.HeaderText = "Descripción"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column4.Width = 200
        '
        'Column6
        '
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle23.Format = "N2"
        DataGridViewCellStyle23.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle23
        Me.Column6.FillWeight = 95.0!
        Me.Column6.HeaderText = "Abono"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column6.Width = 80
        '
        'Column10
        '
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle24.Format = "N2"
        DataGridViewCellStyle24.NullValue = Nothing
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle24
        Me.Column10.FillWeight = 95.0!
        Me.Column10.HeaderText = "Cargo"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column10.Width = 80
        '
        'Column8
        '
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle25.Format = "N2"
        DataGridViewCellStyle25.NullValue = Nothing
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle25
        Me.Column8.FillWeight = 95.0!
        Me.Column8.HeaderText = "Saldo"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column8.Width = 75
        '
        'Column9
        '
        DataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle26.NullValue = False
        Me.Column9.DefaultCellStyle = DataGridViewCellStyle26
        Me.Column9.FillWeight = 95.0!
        Me.Column9.HeaderText = "ITF"
        Me.Column9.Name = "Column9"
        Me.Column9.Visible = False
        Me.Column9.Width = 40
        '
        'Column2
        '
        Me.Column2.FillWeight = 95.0!
        Me.Column2.HeaderText = "IdDet"
        Me.Column2.Name = "Column2"
        Me.Column2.Visible = False
        '
        'Column7
        '
        Me.Column7.FillWeight = 95.0!
        Me.Column7.HeaderText = "TipMov"
        Me.Column7.Name = "Column7"
        Me.Column7.Visible = False
        '
        'Column11
        '
        Me.Column11.FillWeight = 95.0!
        Me.Column11.HeaderText = "TipOpe"
        Me.Column11.Name = "Column11"
        Me.Column11.Visible = False
        '
        'Column12
        '
        Me.Column12.FillWeight = 95.0!
        Me.Column12.HeaderText = "Importe"
        Me.Column12.Name = "Column12"
        Me.Column12.Visible = False
        '
        'IdCab
        '
        Me.IdCab.DataPropertyName = "IdLibroCab"
        Me.IdCab.FillWeight = 95.0!
        Me.IdCab.HeaderText = "IdCab"
        Me.IdCab.Name = "IdCab"
        Me.IdCab.Visible = False
        '
        'Marca1
        '
        Me.Marca1.DataPropertyName = "Marca"
        Me.Marca1.FillWeight = 95.0!
        Me.Marca1.HeaderText = "Marca1"
        Me.Marca1.Name = "Marca1"
        Me.Marca1.Visible = False
        '
        'NroDocumento2
        '
        Me.NroDocumento2.DataPropertyName = "NroDocumento2"
        Me.NroDocumento2.FillWeight = 95.0!
        Me.NroDocumento2.HeaderText = "NroDocumento2"
        Me.NroDocumento2.Name = "NroDocumento2"
        Me.NroDocumento2.Visible = False
        '
        'ITF
        '
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.NullValue = False
        Me.ITF.DefaultCellStyle = DataGridViewCellStyle27
        Me.ITF.FillWeight = 95.0!
        Me.ITF.HeaderText = "Varios ITF"
        Me.ITF.Name = "ITF"
        Me.ITF.Visible = False
        Me.ITF.Width = 70
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "NroMovimientoAnx"
        Me.Column13.FillWeight = 95.0!
        Me.Column13.HeaderText = "NroMovimientoAnx"
        Me.Column13.Name = "Column13"
        Me.Column13.Visible = False
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "FechaAperturaAnx"
        Me.Column14.FillWeight = 95.0!
        Me.Column14.HeaderText = "FechaAperturaAnx"
        Me.Column14.Name = "Column14"
        Me.Column14.Visible = False
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "FechaVencimientoAnx"
        Me.Column15.FillWeight = 95.0!
        Me.Column15.HeaderText = "FechaVencimientoAnx"
        Me.Column15.Name = "Column15"
        Me.Column15.Visible = False
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "MontoAnx"
        Me.Column16.FillWeight = 95.0!
        Me.Column16.HeaderText = "MontoAnx"
        Me.Column16.Name = "Column16"
        Me.Column16.Visible = False
        '
        'Column17
        '
        Me.Column17.DataPropertyName = "CodMonedaAnx"
        Me.Column17.FillWeight = 95.0!
        Me.Column17.HeaderText = "CodMonedaAnx"
        Me.Column17.Name = "Column17"
        Me.Column17.Visible = False
        '
        'Column18
        '
        Me.Column18.DataPropertyName = "CodCtaCargoAnx"
        Me.Column18.FillWeight = 95.0!
        Me.Column18.HeaderText = "CodCtaCargoAnx"
        Me.Column18.Name = "Column18"
        Me.Column18.Visible = False
        '
        'Column19
        '
        Me.Column19.DataPropertyName = "ClienteAnx"
        Me.Column19.FillWeight = 95.0!
        Me.Column19.HeaderText = "ClienteAnx"
        Me.Column19.Name = "Column19"
        Me.Column19.Visible = False
        '
        'Column20
        '
        Me.Column20.DataPropertyName = "CtaClienteAnx"
        Me.Column20.FillWeight = 95.0!
        Me.Column20.HeaderText = "CtaClienteAnx"
        Me.Column20.Name = "Column20"
        Me.Column20.Visible = False
        '
        'Column21
        '
        Me.Column21.DataPropertyName = "ObraAnx"
        Me.Column21.FillWeight = 95.0!
        Me.Column21.HeaderText = "ObraAnx"
        Me.Column21.Name = "Column21"
        Me.Column21.Visible = False
        '
        'Column22
        '
        Me.Column22.DataPropertyName = "TipoCtaAnx"
        Me.Column22.FillWeight = 95.0!
        Me.Column22.HeaderText = "TipoCtaAnx"
        Me.Column22.Name = "Column22"
        Me.Column22.Visible = False
        '
        'Identificarx
        '
        Me.Identificarx.HeaderText = "Identificarx"
        Me.Identificarx.Name = "Identificarx"
        Me.Identificarx.Visible = False
        '
        'Conciliado
        '
        Me.Conciliado.HeaderText = "Conciliado"
        Me.Conciliado.Name = "Conciliado"
        Me.Conciliado.Visible = False
        Me.Conciliado.Width = 65
        '
        'ConciliadoS
        '
        Me.ConciliadoS.HeaderText = "ConciliadoS"
        Me.ConciliadoS.Name = "ConciliadoS"
        Me.ConciliadoS.Visible = False
        '
        'frmLibroBancosConsuslta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(904, 513)
        Me.Controls.Add(Me.txtCodDet)
        Me.Controls.Add(Me.txtMarca2)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.lblCtaEn)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lblCodMoneda)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txtMarca)
        Me.MaximizeBox = False
        Me.Name = "frmLibroBancosConsuslta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Ingresos y Egresos del Mes Por Cuenta"
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCobradosSi As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvLibroDet As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCobradosNo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtMarca2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTipoCambio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSaldoInicial As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblCtaEn As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblTipoCta As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboAno As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCuentas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents lblCodMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboBancos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboMes As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCod As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtMarca As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Marca1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocumento2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ITF As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Identificarx As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Conciliado As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ConciliadoS As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
