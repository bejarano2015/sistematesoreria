<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPagoProveedores
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle94 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle95 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle96 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle97 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle98 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle99 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle100 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle101 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle102 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle103 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle104 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle105 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle106 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle107 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle108 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle109 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle110 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle111 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle112 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle113 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle114 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle115 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle116 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle117 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle118 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle119 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle120 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle121 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle122 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle123 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle124 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtIdLinea = New ctrLibreria.Controles.BeTextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txtDiasCred = New ctrLibreria.Controles.BeTextBox()
        Me.txtLineaCreDolares = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel24 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel()
        Me.txtLineaCreSoles = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel()
        Me.txtDireccion = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.cboAnexo = New ctrLibreria.Controles.BeComboBox()
        Me.cboTipoAnexo = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.txtSaldoSoles = New ctrLibreria.Controles.BeTextBox()
        Me.txtSaldoDolares = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtEstadoSoles = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel26 = New ctrLibreria.Controles.BeLabel()
        Me.txtEstadoDolares = New ctrLibreria.Controles.BeTextBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.txtNroOrden = New ctrLibreria.Controles.BeTextBox()
        Me.chkVerTodo = New System.Windows.Forms.CheckBox()
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel()
        Me.dgvOrdenes = New System.Windows.Forms.DataGridView()
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCosto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Moneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdOrden = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrvCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrvRUC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDocumentos = New System.Windows.Forms.DataGridView()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCC = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AbrDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtTotDolares = New ctrLibreria.Controles.BeTextBox()
        Me.txtTotSoles = New ctrLibreria.Controles.BeTextBox()
        Me.txtNroDoc = New ctrLibreria.Controles.BeTextBox()
        Me.btnProgramar = New ctrLibreria.Controles.BeButton()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroSemana = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.txtTipoCambio = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.BeLabel43 = New ctrLibreria.Controles.BeLabel()
        Me.txtCanDocLetra = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel42 = New ctrLibreria.Controles.BeLabel()
        Me.txtReferencia = New ctrLibreria.Controles.BeTextBox()
        Me.btnGrabarLetra = New ctrLibreria.Controles.BeButton()
        Me.BeLabel39 = New ctrLibreria.Controles.BeLabel()
        Me.txtTotalLetra = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel37 = New ctrLibreria.Controles.BeLabel()
        Me.txtLugarGiro = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel36 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel35 = New ctrLibreria.Controles.BeLabel()
        Me.dtpFechaVen = New ctrLibreria.Controles.BeDateTimePicker()
        Me.dtpFechaGiro = New ctrLibreria.Controles.BeDateTimePicker()
        Me.BeLabel34 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroLetra = New ctrLibreria.Controles.BeTextBox()
        Me.btnLetra = New ctrLibreria.Controles.BeButton()
        Me.BeLabel41 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel40 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.txtPagarDol = New ctrLibreria.Controles.BeTextBox()
        Me.txtImporteSoles = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel()
        Me.txtImporteDolares = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel()
        Me.txtSalSoles = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel()
        Me.txtSalDolares = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel()
        Me.txtPagarSol = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCC2 = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column33 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cboAnexos = New ctrLibreria.Controles.BeComboBox()
        Me.btnActualizar = New ctrLibreria.Controles.BeButton()
        Me.cboTipAnexos = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel()
        Me.cboResumenes = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnExportarExcel = New System.Windows.Forms.Button()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.BeLabel32 = New ctrLibreria.Controles.BeLabel()
        Me.txtCheSoles = New ctrLibreria.Controles.BeTextBox()
        Me.txtCheDolares = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel33 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel27 = New ctrLibreria.Controles.BeLabel()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.BeLabel38 = New ctrLibreria.Controles.BeLabel()
        Me.cboCriterio = New ctrLibreria.Controles.BeComboBox()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.lblIngrese = New ctrLibreria.Controles.BeLabel()
        Me.txtCadenaBusqueda = New ctrLibreria.Controles.BeTextBox()
        Me.dgvDocumentosaEntregar = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Entregar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IdRegistro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.BeLabel30 = New ctrLibreria.Controles.BeLabel()
        Me.txtSol = New ctrLibreria.Controles.BeTextBox()
        Me.txtDol = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel31 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel()
        Me.dgvEstadoDocs = New System.Windows.Forms.DataGridView()
        Me.Ruc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AnaliticoDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImpDoc = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn27 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn28 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.rdbRazon = New System.Windows.Forms.RadioButton()
        Me.chkDocSinCance = New System.Windows.Forms.CheckBox()
        Me.chkDocCance = New System.Windows.Forms.CheckBox()
        Me.rdbNroDocumento = New System.Windows.Forms.RadioButton()
        Me.rdbRuc = New System.Windows.Forms.RadioButton()
        Me.txtBuscarEstadoDoc = New ctrLibreria.Controles.BeTextBox()
        Me.cboTipoPago = New ctrLibreria.Controles.BeComboBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvOrdenes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        CType(Me.dgvDocumentosaEntregar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        CType(Me.dgvEstadoDocs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox10.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.txtIdLinea)
        Me.Panel1.Controls.Add(Me.TabControl1)
        Me.Panel1.Controls.Add(Me.cboTipoPago)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1065, 490)
        Me.Panel1.TabIndex = 0
        '
        'txtIdLinea
        '
        Me.txtIdLinea.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIdLinea.BackColor = System.Drawing.Color.Lavender
        Me.txtIdLinea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdLinea.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdLinea.ForeColor = System.Drawing.Color.Black
        Me.txtIdLinea.KeyEnter = True
        Me.txtIdLinea.Location = New System.Drawing.Point(478, 532)
        Me.txtIdLinea.Name = "txtIdLinea"
        Me.txtIdLinea.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIdLinea.ShortcutsEnabled = False
        Me.txtIdLinea.Size = New System.Drawing.Size(198, 20)
        Me.txtIdLinea.TabIndex = 12
        Me.txtIdLinea.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtIdLinea.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtIdLinea.Visible = False
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(-5, 2)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1074, 483)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1066, 457)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Programación"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.GroupBox7)
        Me.GroupBox1.Controls.Add(Me.txtDireccion)
        Me.GroupBox1.Controls.Add(Me.BeLabel8)
        Me.GroupBox1.Controls.Add(Me.BeLabel1)
        Me.GroupBox1.Controls.Add(Me.cboAnexo)
        Me.GroupBox1.Controls.Add(Me.cboTipoAnexo)
        Me.GroupBox1.Controls.Add(Me.BeLabel3)
        Me.GroupBox1.Controls.Add(Me.BeLabel2)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(569, 135)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Anexo"
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.Ivory
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(93, 79)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(120, 21)
        Me.txtCodigo.TabIndex = 5
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtDiasCred)
        Me.GroupBox7.Controls.Add(Me.txtLineaCreDolares)
        Me.GroupBox7.Controls.Add(Me.BeLabel24)
        Me.GroupBox7.Controls.Add(Me.BeLabel23)
        Me.GroupBox7.Controls.Add(Me.txtLineaCreSoles)
        Me.GroupBox7.Controls.Add(Me.BeLabel22)
        Me.GroupBox7.Location = New System.Drawing.Point(346, 12)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(217, 114)
        Me.GroupBox7.TabIndex = 16
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Linea de Crédito"
        '
        'txtDiasCred
        '
        Me.txtDiasCred.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDiasCred.BackColor = System.Drawing.Color.Lavender
        Me.txtDiasCred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDiasCred.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiasCred.ForeColor = System.Drawing.Color.Black
        Me.txtDiasCred.KeyEnter = True
        Me.txtDiasCred.Location = New System.Drawing.Point(112, 70)
        Me.txtDiasCred.Name = "txtDiasCred"
        Me.txtDiasCred.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDiasCred.ShortcutsEnabled = False
        Me.txtDiasCred.Size = New System.Drawing.Size(94, 21)
        Me.txtDiasCred.TabIndex = 11
        Me.txtDiasCred.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtDiasCred.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtLineaCreDolares
        '
        Me.txtLineaCreDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtLineaCreDolares.BackColor = System.Drawing.Color.Lavender
        Me.txtLineaCreDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLineaCreDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLineaCreDolares.ForeColor = System.Drawing.Color.Black
        Me.txtLineaCreDolares.KeyEnter = True
        Me.txtLineaCreDolares.Location = New System.Drawing.Point(112, 43)
        Me.txtLineaCreDolares.Name = "txtLineaCreDolares"
        Me.txtLineaCreDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtLineaCreDolares.ShortcutsEnabled = False
        Me.txtLineaCreDolares.Size = New System.Drawing.Size(94, 21)
        Me.txtLineaCreDolares.TabIndex = 9
        Me.txtLineaCreDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLineaCreDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel24
        '
        Me.BeLabel24.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel24.AutoSize = True
        Me.BeLabel24.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel24.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel24.ForeColor = System.Drawing.Color.Black
        Me.BeLabel24.Location = New System.Drawing.Point(5, 75)
        Me.BeLabel24.Name = "BeLabel24"
        Me.BeLabel24.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel24.TabIndex = 10
        Me.BeLabel24.Text = "Dias"
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(5, 21)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel23.TabIndex = 6
        Me.BeLabel23.Text = "Crédito S/."
        '
        'txtLineaCreSoles
        '
        Me.txtLineaCreSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtLineaCreSoles.BackColor = System.Drawing.Color.Lavender
        Me.txtLineaCreSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLineaCreSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLineaCreSoles.ForeColor = System.Drawing.Color.Black
        Me.txtLineaCreSoles.KeyEnter = True
        Me.txtLineaCreSoles.Location = New System.Drawing.Point(113, 16)
        Me.txtLineaCreSoles.Name = "txtLineaCreSoles"
        Me.txtLineaCreSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtLineaCreSoles.ShortcutsEnabled = False
        Me.txtLineaCreSoles.Size = New System.Drawing.Size(94, 21)
        Me.txtLineaCreSoles.TabIndex = 7
        Me.txtLineaCreSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLineaCreSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(5, 48)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(66, 13)
        Me.BeLabel22.TabIndex = 8
        Me.BeLabel22.Text = "Crédito $"
        '
        'txtDireccion
        '
        Me.txtDireccion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDireccion.BackColor = System.Drawing.Color.Ivory
        Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.Enabled = False
        Me.txtDireccion.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDireccion.ForeColor = System.Drawing.Color.Black
        Me.txtDireccion.KeyEnter = True
        Me.txtDireccion.Location = New System.Drawing.Point(93, 104)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDireccion.ShortcutsEnabled = False
        Me.txtDireccion.Size = New System.Drawing.Size(247, 21)
        Me.txtDireccion.TabIndex = 7
        Me.txtDireccion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(6, 58)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel8.TabIndex = 2
        Me.BeLabel8.Text = "Anexo"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(7, 29)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(80, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Tipo Anexo"
        '
        'cboAnexo
        '
        Me.cboAnexo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Ninguno
        Me.cboAnexo.BackColor = System.Drawing.Color.Ivory
        Me.cboAnexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnexo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAnexo.ForeColor = System.Drawing.Color.Black
        Me.cboAnexo.FormattingEnabled = True
        Me.cboAnexo.KeyEnter = True
        Me.cboAnexo.Location = New System.Drawing.Point(93, 52)
        Me.cboAnexo.Name = "cboAnexo"
        Me.cboAnexo.Size = New System.Drawing.Size(249, 21)
        Me.cboAnexo.TabIndex = 3
        '
        'cboTipoAnexo
        '
        Me.cboTipoAnexo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoAnexo.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoAnexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoAnexo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoAnexo.ForeColor = System.Drawing.Color.Black
        Me.cboTipoAnexo.FormattingEnabled = True
        Me.cboTipoAnexo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cboTipoAnexo.KeyEnter = True
        Me.cboTipoAnexo.Location = New System.Drawing.Point(93, 25)
        Me.cboTipoAnexo.Name = "cboTipoAnexo"
        Me.cboTipoAnexo.Size = New System.Drawing.Size(249, 21)
        Me.cboTipoAnexo.TabIndex = 1
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(6, 83)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(32, 13)
        Me.BeLabel3.TabIndex = 4
        Me.BeLabel3.Text = "RUC"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(6, 108)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(68, 13)
        Me.BeLabel2.TabIndex = 6
        Me.BeLabel2.Text = "Dirección"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GroupBox3.Controls.Add(Me.GroupBox6)
        Me.GroupBox3.Controls.Add(Me.GroupBox5)
        Me.GroupBox3.Controls.Add(Me.Panel4)
        Me.GroupBox3.Controls.Add(Me.dgvDocumentos)
        Me.GroupBox3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(6, 147)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1049, 306)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Documentos Pendientes de Pago"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.BeLabel7)
        Me.GroupBox6.Controls.Add(Me.txtSaldoSoles)
        Me.GroupBox6.Controls.Add(Me.txtSaldoDolares)
        Me.GroupBox6.Controls.Add(Me.BeLabel14)
        Me.GroupBox6.Location = New System.Drawing.Point(337, 223)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(183, 68)
        Me.GroupBox6.TabIndex = 15
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Total a Pagar"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(103, 20)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel7.TabIndex = 2
        Me.BeLabel7.Text = "en Soles"
        '
        'txtSaldoSoles
        '
        Me.txtSaldoSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtSaldoSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoSoles.Enabled = False
        Me.txtSaldoSoles.ForeColor = System.Drawing.Color.Black
        Me.txtSaldoSoles.KeyEnter = True
        Me.txtSaldoSoles.Location = New System.Drawing.Point(9, 15)
        Me.txtSaldoSoles.Name = "txtSaldoSoles"
        Me.txtSaldoSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoSoles.ShortcutsEnabled = False
        Me.txtSaldoSoles.Size = New System.Drawing.Size(88, 21)
        Me.txtSaldoSoles.TabIndex = 3
        Me.txtSaldoSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSaldoDolares
        '
        Me.txtSaldoDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoDolares.BackColor = System.Drawing.Color.Ivory
        Me.txtSaldoDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoDolares.Enabled = False
        Me.txtSaldoDolares.ForeColor = System.Drawing.Color.Black
        Me.txtSaldoDolares.KeyEnter = True
        Me.txtSaldoDolares.Location = New System.Drawing.Point(9, 40)
        Me.txtSaldoDolares.Name = "txtSaldoDolares"
        Me.txtSaldoDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoDolares.ShortcutsEnabled = False
        Me.txtSaldoDolares.Size = New System.Drawing.Size(88, 21)
        Me.txtSaldoDolares.TabIndex = 5
        Me.txtSaldoDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(103, 42)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel14.TabIndex = 4
        Me.BeLabel14.Text = "en Dolares"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtEstadoSoles)
        Me.GroupBox5.Controls.Add(Me.BeLabel25)
        Me.GroupBox5.Controls.Add(Me.BeLabel26)
        Me.GroupBox5.Controls.Add(Me.txtEstadoDolares)
        Me.GroupBox5.Location = New System.Drawing.Point(4, 223)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(235, 68)
        Me.GroupBox5.TabIndex = 14
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Estado Crédito"
        '
        'txtEstadoSoles
        '
        Me.txtEstadoSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtEstadoSoles.BackColor = System.Drawing.Color.White
        Me.txtEstadoSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEstadoSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoSoles.ForeColor = System.Drawing.Color.Black
        Me.txtEstadoSoles.KeyEnter = True
        Me.txtEstadoSoles.Location = New System.Drawing.Point(135, 18)
        Me.txtEstadoSoles.Name = "txtEstadoSoles"
        Me.txtEstadoSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtEstadoSoles.ReadOnly = True
        Me.txtEstadoSoles.ShortcutsEnabled = False
        Me.txtEstadoSoles.Size = New System.Drawing.Size(94, 21)
        Me.txtEstadoSoles.TabIndex = 11
        Me.txtEstadoSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEstadoSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel25.ForeColor = System.Drawing.Color.Black
        Me.BeLabel25.Location = New System.Drawing.Point(8, 45)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(114, 13)
        Me.BeLabel25.TabIndex = 12
        Me.BeLabel25.Text = "Estado Crédito $"
        '
        'BeLabel26
        '
        Me.BeLabel26.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel26.AutoSize = True
        Me.BeLabel26.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel26.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel26.ForeColor = System.Drawing.Color.Black
        Me.BeLabel26.Location = New System.Drawing.Point(8, 23)
        Me.BeLabel26.Name = "BeLabel26"
        Me.BeLabel26.Size = New System.Drawing.Size(126, 13)
        Me.BeLabel26.TabIndex = 10
        Me.BeLabel26.Text = "Estado Crédito S/."
        '
        'txtEstadoDolares
        '
        Me.txtEstadoDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtEstadoDolares.BackColor = System.Drawing.Color.White
        Me.txtEstadoDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEstadoDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoDolares.ForeColor = System.Drawing.Color.Black
        Me.txtEstadoDolares.KeyEnter = True
        Me.txtEstadoDolares.Location = New System.Drawing.Point(135, 43)
        Me.txtEstadoDolares.Name = "txtEstadoDolares"
        Me.txtEstadoDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtEstadoDolares.ReadOnly = True
        Me.txtEstadoDolares.ShortcutsEnabled = False
        Me.txtEstadoDolares.Size = New System.Drawing.Size(94, 21)
        Me.txtEstadoDolares.TabIndex = 13
        Me.txtEstadoDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEstadoDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.SlateGray
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.txtNroOrden)
        Me.Panel4.Controls.Add(Me.chkVerTodo)
        Me.Panel4.Controls.Add(Me.BeLabel28)
        Me.Panel4.Controls.Add(Me.BeLabel21)
        Me.Panel4.Controls.Add(Me.dgvOrdenes)
        Me.Panel4.Location = New System.Drawing.Point(337, 44)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(433, 166)
        Me.Panel4.TabIndex = 1
        Me.Panel4.Visible = False
        '
        'txtNroOrden
        '
        Me.txtNroOrden.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroOrden.BackColor = System.Drawing.Color.LightYellow
        Me.txtNroOrden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroOrden.ForeColor = System.Drawing.Color.Black
        Me.txtNroOrden.KeyEnter = True
        Me.txtNroOrden.Location = New System.Drawing.Point(95, 8)
        Me.txtNroOrden.MaxLength = 9
        Me.txtNroOrden.Name = "txtNroOrden"
        Me.txtNroOrden.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroOrden.ShortcutsEnabled = False
        Me.txtNroOrden.Size = New System.Drawing.Size(205, 21)
        Me.txtNroOrden.TabIndex = 14
        Me.txtNroOrden.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'chkVerTodo
        '
        Me.chkVerTodo.AutoSize = True
        Me.chkVerTodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVerTodo.Location = New System.Drawing.Point(306, 10)
        Me.chkVerTodo.Name = "chkVerTodo"
        Me.chkVerTodo.Size = New System.Drawing.Size(84, 17)
        Me.chkVerTodo.TabIndex = 13
        Me.chkVerTodo.Text = "Ver Todos"
        Me.chkVerTodo.UseVisualStyleBackColor = True
        Me.chkVerTodo.Visible = False
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel28.ForeColor = System.Drawing.Color.Black
        Me.BeLabel28.Location = New System.Drawing.Point(3, 10)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(86, 13)
        Me.BeLabel28.TabIndex = 12
        Me.BeLabel28.Text = "Nº de Orden"
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel21.ForeColor = System.Drawing.Color.Red
        Me.BeLabel21.Location = New System.Drawing.Point(417, 0)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(15, 13)
        Me.BeLabel21.TabIndex = 0
        Me.BeLabel21.Text = "X"
        '
        'dgvOrdenes
        '
        Me.dgvOrdenes.AllowUserToAddRows = False
        Me.dgvOrdenes.BackgroundColor = System.Drawing.Color.White
        Me.dgvOrdenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOrdenes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Numero, Me.CCosto, Me.Moneda, Me.Importe, Me.IdOrden, Me.PrvCodigo, Me.PrvRUC, Me.Column29})
        Me.dgvOrdenes.Location = New System.Drawing.Point(2, 33)
        Me.dgvOrdenes.Name = "dgvOrdenes"
        Me.dgvOrdenes.RowHeadersVisible = False
        Me.dgvOrdenes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOrdenes.Size = New System.Drawing.Size(427, 129)
        Me.dgvOrdenes.TabIndex = 3
        '
        'Numero
        '
        Me.Numero.DataPropertyName = "Numero"
        Me.Numero.HeaderText = "Número"
        Me.Numero.Name = "Numero"
        Me.Numero.Width = 75
        '
        'CCosto
        '
        Me.CCosto.DataPropertyName = "CCosto"
        Me.CCosto.HeaderText = "Proveedor"
        Me.CCosto.Name = "CCosto"
        Me.CCosto.Width = 130
        '
        'Moneda
        '
        Me.Moneda.DataPropertyName = "Moneda"
        Me.Moneda.HeaderText = "Moneda"
        Me.Moneda.Name = "Moneda"
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Total"
        DataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle94.Format = "N2"
        DataGridViewCellStyle94.NullValue = Nothing
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle94
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        '
        'IdOrden
        '
        Me.IdOrden.DataPropertyName = "IdOrden"
        Me.IdOrden.HeaderText = "IdOrden"
        Me.IdOrden.Name = "IdOrden"
        Me.IdOrden.Visible = False
        '
        'PrvCodigo
        '
        Me.PrvCodigo.DataPropertyName = "PrvCodigo"
        Me.PrvCodigo.HeaderText = "PrvCodigo"
        Me.PrvCodigo.Name = "PrvCodigo"
        Me.PrvCodigo.Visible = False
        '
        'PrvRUC
        '
        Me.PrvRUC.DataPropertyName = "PrvRUC"
        Me.PrvRUC.HeaderText = "PrvRUC"
        Me.PrvRUC.Name = "PrvRUC"
        Me.PrvRUC.Visible = False
        '
        'Column29
        '
        Me.Column29.DataPropertyName = "CCosCodigo"
        Me.Column29.HeaderText = "CCosCodigo"
        Me.Column29.Name = "Column29"
        Me.Column29.Visible = False
        '
        'dgvDocumentos
        '
        Me.dgvDocumentos.AllowUserToAddRows = False
        Me.dgvDocumentos.BackgroundColor = System.Drawing.Color.White
        Me.dgvDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDocumentos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.NroDocumento, Me.Column3, Me.Column4, Me.Column10, Me.ImporteTotal, Me.Column12, Me.Column11, Me.Column1, Me.Column20, Me.Column5, Me.Column6, Me.IdCC, Me.Column7, Me.Column8, Me.AbrDoc, Me.Column13, Me.Column15, Me.Column17, Me.Column22, Me.Column23, Me.Column28})
        Me.dgvDocumentos.EnableHeadersVisualStyles = False
        Me.dgvDocumentos.Location = New System.Drawing.Point(3, 19)
        Me.dgvDocumentos.Name = "dgvDocumentos"
        Me.dgvDocumentos.RowHeadersVisible = False
        Me.dgvDocumentos.Size = New System.Drawing.Size(1039, 198)
        Me.dgvDocumentos.TabIndex = 0
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "AbrDoc"
        Me.Column2.HeaderText = "T/D"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 30
        '
        'NroDocumento
        '
        Me.NroDocumento.DataPropertyName = "NroDocumento"
        Me.NroDocumento.HeaderText = "Nº Doc."
        Me.NroDocumento.Name = "NroDocumento"
        Me.NroDocumento.ReadOnly = True
        Me.NroDocumento.Width = 107
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "FechaDocumento"
        Me.Column3.HeaderText = "F. Doc."
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 80
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "FechaVencimiento"
        Me.Column4.HeaderText = "F. Ven."
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "MonSimbolo"
        DataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle95
        Me.Column10.HeaderText = "Mon"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Column10.Width = 50
        '
        'ImporteTotal
        '
        Me.ImporteTotal.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle96.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle96.Format = "N2"
        DataGridViewCellStyle96.NullValue = Nothing
        Me.ImporteTotal.DefaultCellStyle = DataGridViewCellStyle96
        Me.ImporteTotal.HeaderText = "Importe"
        Me.ImporteTotal.Name = "ImporteTotal"
        Me.ImporteTotal.ReadOnly = True
        Me.ImporteTotal.Width = 85
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "Saldo"
        DataGridViewCellStyle97.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle97.Format = "N2"
        DataGridViewCellStyle97.NullValue = "0.00"
        Me.Column12.DefaultCellStyle = DataGridViewCellStyle97
        Me.Column12.HeaderText = "Saldo"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Width = 60
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "sPagar"
        DataGridViewCellStyle98.BackColor = System.Drawing.Color.Salmon
        DataGridViewCellStyle98.Format = "N2"
        DataGridViewCellStyle98.NullValue = Nothing
        Me.Column11.DefaultCellStyle = DataGridViewCellStyle98
        Me.Column11.HeaderText = "A Pagar"
        Me.Column11.Name = "Column11"
        Me.Column11.Width = 85
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "sDP"
        DataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle99
        Me.Column1.HeaderText = "D/P"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 40
        '
        'Column20
        '
        Me.Column20.DataPropertyName = "MtoDP"
        Me.Column20.HeaderText = "%"
        Me.Column20.Name = "Column20"
        Me.Column20.Width = 40
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "sPorc"
        DataGridViewCellStyle100.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle100.Format = "N2"
        DataGridViewCellStyle100.NullValue = Nothing
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle100
        Me.Column5.HeaderText = "Mon. D/P"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 84
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "NroOC"
        Me.Column6.HeaderText = "O/C"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 90
        '
        'IdCC
        '
        Me.IdCC.HeaderText = "C. Costo"
        Me.IdCC.Name = "IdCC"
        Me.IdCC.Width = 163
        '
        'Column7
        '
        Me.Column7.HeaderText = "X"
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 20
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "IdRegistro"
        Me.Column8.HeaderText = "IdRegistro"
        Me.Column8.Name = "Column8"
        Me.Column8.Visible = False
        '
        'AbrDoc
        '
        Me.AbrDoc.DataPropertyName = "AbrDoc"
        Me.AbrDoc.HeaderText = "AbrDoc"
        Me.AbrDoc.Name = "AbrDoc"
        Me.AbrDoc.Visible = False
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "MonDescripcion"
        Me.Column13.HeaderText = "MonDes"
        Me.Column13.Name = "Column13"
        Me.Column13.Visible = False
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "IdMoneda"
        Me.Column15.HeaderText = "IdMoneda"
        Me.Column15.Name = "Column15"
        Me.Column15.Visible = False
        '
        'Column17
        '
        Me.Column17.DataPropertyName = "SaldoOculto"
        Me.Column17.HeaderText = "SaldoOculto"
        Me.Column17.Name = "Column17"
        Me.Column17.Visible = False
        '
        'Column22
        '
        Me.Column22.DataPropertyName = "IdOC"
        Me.Column22.HeaderText = "IdOC"
        Me.Column22.Name = "Column22"
        Me.Column22.Visible = False
        '
        'Column23
        '
        Me.Column23.HeaderText = "IdBD"
        Me.Column23.Name = "Column23"
        Me.Column23.Visible = False
        '
        'Column28
        '
        Me.Column28.DataPropertyName = "CCosCodigo"
        Me.Column28.HeaderText = "CCosCodigo"
        Me.Column28.Name = "Column28"
        Me.Column28.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GroupBox2.Controls.Add(Me.txtTotDolares)
        Me.GroupBox2.Controls.Add(Me.txtTotSoles)
        Me.GroupBox2.Controls.Add(Me.txtNroDoc)
        Me.GroupBox2.Controls.Add(Me.btnProgramar)
        Me.GroupBox2.Controls.Add(Me.BeLabel10)
        Me.GroupBox2.Controls.Add(Me.txtNroSemana)
        Me.GroupBox2.Controls.Add(Me.BeLabel9)
        Me.GroupBox2.Controls.Add(Me.txtTipoCambio)
        Me.GroupBox2.Controls.Add(Me.BeLabel6)
        Me.GroupBox2.Controls.Add(Me.BeLabel4)
        Me.GroupBox2.Controls.Add(Me.BeLabel5)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(581, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(474, 135)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Resumen Documentos a Pagar (X)"
        '
        'txtTotDolares
        '
        Me.txtTotDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotDolares.BackColor = System.Drawing.Color.Ivory
        Me.txtTotDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotDolares.Enabled = False
        Me.txtTotDolares.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotDolares.ForeColor = System.Drawing.Color.Black
        Me.txtTotDolares.KeyEnter = True
        Me.txtTotDolares.Location = New System.Drawing.Point(79, 80)
        Me.txtTotDolares.Name = "txtTotDolares"
        Me.txtTotDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotDolares.ShortcutsEnabled = False
        Me.txtTotDolares.Size = New System.Drawing.Size(151, 21)
        Me.txtTotDolares.TabIndex = 5
        Me.txtTotDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtTotSoles
        '
        Me.txtTotSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtTotSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotSoles.Enabled = False
        Me.txtTotSoles.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotSoles.ForeColor = System.Drawing.Color.Black
        Me.txtTotSoles.KeyEnter = True
        Me.txtTotSoles.Location = New System.Drawing.Point(79, 54)
        Me.txtTotSoles.Name = "txtTotSoles"
        Me.txtTotSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotSoles.ShortcutsEnabled = False
        Me.txtTotSoles.Size = New System.Drawing.Size(151, 21)
        Me.txtTotSoles.TabIndex = 3
        Me.txtTotSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtNroDoc
        '
        Me.txtNroDoc.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroDoc.BackColor = System.Drawing.Color.Ivory
        Me.txtNroDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroDoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroDoc.Enabled = False
        Me.txtNroDoc.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroDoc.ForeColor = System.Drawing.Color.Black
        Me.txtNroDoc.KeyEnter = True
        Me.txtNroDoc.Location = New System.Drawing.Point(79, 29)
        Me.txtNroDoc.Name = "txtNroDoc"
        Me.txtNroDoc.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroDoc.ShortcutsEnabled = False
        Me.txtNroDoc.Size = New System.Drawing.Size(151, 21)
        Me.txtNroDoc.TabIndex = 1
        Me.txtNroDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroDoc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'btnProgramar
        '
        Me.btnProgramar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnProgramar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnProgramar.Location = New System.Drawing.Point(347, 106)
        Me.btnProgramar.Name = "btnProgramar"
        Me.btnProgramar.Size = New System.Drawing.Size(126, 23)
        Me.btnProgramar.TabIndex = 10
        Me.btnProgramar.Text = "&Programar Pagos"
        Me.btnProgramar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnProgramar.UseVisualStyleBackColor = True
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(8, 113)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(133, 13)
        Me.BeLabel10.TabIndex = 6
        Me.BeLabel10.Text = "Nº Semana a Pagar"
        '
        'txtNroSemana
        '
        Me.txtNroSemana.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroSemana.BackColor = System.Drawing.Color.Ivory
        Me.txtNroSemana.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroSemana.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroSemana.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroSemana.ForeColor = System.Drawing.Color.Black
        Me.txtNroSemana.KeyEnter = True
        Me.txtNroSemana.Location = New System.Drawing.Point(147, 108)
        Me.txtNroSemana.MaxLength = 2
        Me.txtNroSemana.Name = "txtNroSemana"
        Me.txtNroSemana.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroSemana.ShortcutsEnabled = False
        Me.txtNroSemana.Size = New System.Drawing.Size(46, 21)
        Me.txtNroSemana.TabIndex = 7
        Me.txtNroSemana.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNroSemana.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(8, 85)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel9.TabIndex = 4
        Me.BeLabel9.Text = "Total US$"
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoCambio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTipoCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoCambio.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.txtTipoCambio.KeyEnter = True
        Me.txtTipoCambio.Location = New System.Drawing.Point(276, 108)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTipoCambio.ShortcutsEnabled = False
        Me.txtTipoCambio.Size = New System.Drawing.Size(65, 21)
        Me.txtTipoCambio.TabIndex = 9
        Me.txtTipoCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTipoCambio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(8, 60)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(64, 13)
        Me.BeLabel6.TabIndex = 2
        Me.BeLabel6.Text = "Total S/."
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(199, 113)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(71, 13)
        Me.BeLabel4.TabIndex = 8
        Me.BeLabel4.Text = "T. Cambio"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(8, 34)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(72, 13)
        Me.BeLabel5.TabIndex = 0
        Me.BeLabel5.Text = "Cant. Doc."
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1066, 457)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Búsqueda"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GroupBox4.Controls.Add(Me.Panel2)
        Me.GroupBox4.Controls.Add(Me.btnLetra)
        Me.GroupBox4.Controls.Add(Me.BeLabel41)
        Me.GroupBox4.Controls.Add(Me.BeLabel40)
        Me.GroupBox4.Controls.Add(Me.GroupBox14)
        Me.GroupBox4.Controls.Add(Me.BeLabel15)
        Me.GroupBox4.Controls.Add(Me.cboMoneda)
        Me.GroupBox4.Controls.Add(Me.dgvDetalle)
        Me.GroupBox4.Controls.Add(Me.cboAnexos)
        Me.GroupBox4.Controls.Add(Me.btnActualizar)
        Me.GroupBox4.Controls.Add(Me.cboTipAnexos)
        Me.GroupBox4.Controls.Add(Me.BeLabel12)
        Me.GroupBox4.Controls.Add(Me.BeLabel13)
        Me.GroupBox4.Controls.Add(Me.cboResumenes)
        Me.GroupBox4.Controls.Add(Me.BeLabel11)
        Me.GroupBox4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(9, 6)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1050, 445)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Programaciones de Pago"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.GroupBox13)
        Me.Panel2.Location = New System.Drawing.Point(12, 271)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(625, 166)
        Me.Panel2.TabIndex = 11
        Me.Panel2.Visible = False
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.BeLabel43)
        Me.GroupBox13.Controls.Add(Me.txtCanDocLetra)
        Me.GroupBox13.Controls.Add(Me.BeLabel42)
        Me.GroupBox13.Controls.Add(Me.txtReferencia)
        Me.GroupBox13.Controls.Add(Me.btnGrabarLetra)
        Me.GroupBox13.Controls.Add(Me.BeLabel39)
        Me.GroupBox13.Controls.Add(Me.txtTotalLetra)
        Me.GroupBox13.Controls.Add(Me.BeLabel37)
        Me.GroupBox13.Controls.Add(Me.txtLugarGiro)
        Me.GroupBox13.Controls.Add(Me.BeLabel36)
        Me.GroupBox13.Controls.Add(Me.BeLabel35)
        Me.GroupBox13.Controls.Add(Me.dtpFechaVen)
        Me.GroupBox13.Controls.Add(Me.dtpFechaGiro)
        Me.GroupBox13.Controls.Add(Me.BeLabel34)
        Me.GroupBox13.Controls.Add(Me.txtNroLetra)
        Me.GroupBox13.Location = New System.Drawing.Point(7, 1)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(609, 155)
        Me.GroupBox13.TabIndex = 0
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Cancelar Programación con Letra"
        '
        'BeLabel43
        '
        Me.BeLabel43.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel43.AutoSize = True
        Me.BeLabel43.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel43.ForeColor = System.Drawing.Color.Black
        Me.BeLabel43.Location = New System.Drawing.Point(285, 89)
        Me.BeLabel43.Name = "BeLabel43"
        Me.BeLabel43.Size = New System.Drawing.Size(74, 13)
        Me.BeLabel43.TabIndex = 10
        Me.BeLabel43.Text = "Cant. Docs."
        '
        'txtCanDocLetra
        '
        Me.txtCanDocLetra.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCanDocLetra.BackColor = System.Drawing.Color.Ivory
        Me.txtCanDocLetra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCanDocLetra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCanDocLetra.ForeColor = System.Drawing.Color.Black
        Me.txtCanDocLetra.KeyEnter = True
        Me.txtCanDocLetra.Location = New System.Drawing.Point(365, 81)
        Me.txtCanDocLetra.Name = "txtCanDocLetra"
        Me.txtCanDocLetra.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCanDocLetra.ShortcutsEnabled = False
        Me.txtCanDocLetra.Size = New System.Drawing.Size(77, 21)
        Me.txtCanDocLetra.TabIndex = 11
        Me.txtCanDocLetra.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtCanDocLetra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel42
        '
        Me.BeLabel42.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel42.AutoSize = True
        Me.BeLabel42.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel42.ForeColor = System.Drawing.Color.Black
        Me.BeLabel42.Location = New System.Drawing.Point(207, 42)
        Me.BeLabel42.Name = "BeLabel42"
        Me.BeLabel42.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel42.TabIndex = 8
        Me.BeLabel42.Text = "Ref. Girador"
        '
        'txtReferencia
        '
        Me.txtReferencia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtReferencia.BackColor = System.Drawing.Color.Ivory
        Me.txtReferencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtReferencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferencia.ForeColor = System.Drawing.Color.Black
        Me.txtReferencia.KeyEnter = True
        Me.txtReferencia.Location = New System.Drawing.Point(288, 23)
        Me.txtReferencia.Multiline = True
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtReferencia.ShortcutsEnabled = False
        Me.txtReferencia.Size = New System.Drawing.Size(309, 50)
        Me.txtReferencia.TabIndex = 9
        Me.txtReferencia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'btnGrabarLetra
        '
        Me.btnGrabarLetra.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabarLetra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabarLetra.Location = New System.Drawing.Point(486, 108)
        Me.btnGrabarLetra.Name = "btnGrabarLetra"
        Me.btnGrabarLetra.Size = New System.Drawing.Size(111, 36)
        Me.btnGrabarLetra.TabIndex = 14
        Me.btnGrabarLetra.Text = "Grabar Letra"
        Me.btnGrabarLetra.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabarLetra.UseVisualStyleBackColor = True
        '
        'BeLabel39
        '
        Me.BeLabel39.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel39.AutoSize = True
        Me.BeLabel39.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel39.ForeColor = System.Drawing.Color.Black
        Me.BeLabel39.Location = New System.Drawing.Point(448, 89)
        Me.BeLabel39.Name = "BeLabel39"
        Me.BeLabel39.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel39.TabIndex = 12
        Me.BeLabel39.Text = "Total"
        '
        'txtTotalLetra
        '
        Me.txtTotalLetra.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotalLetra.BackColor = System.Drawing.Color.Ivory
        Me.txtTotalLetra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotalLetra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalLetra.ForeColor = System.Drawing.Color.Black
        Me.txtTotalLetra.KeyEnter = True
        Me.txtTotalLetra.Location = New System.Drawing.Point(486, 81)
        Me.txtTotalLetra.Name = "txtTotalLetra"
        Me.txtTotalLetra.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotalLetra.ShortcutsEnabled = False
        Me.txtTotalLetra.Size = New System.Drawing.Size(111, 21)
        Me.txtTotalLetra.TabIndex = 13
        Me.txtTotalLetra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalLetra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel37
        '
        Me.BeLabel37.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel37.AutoSize = True
        Me.BeLabel37.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel37.ForeColor = System.Drawing.Color.Black
        Me.BeLabel37.Location = New System.Drawing.Point(9, 112)
        Me.BeLabel37.Name = "BeLabel37"
        Me.BeLabel37.Size = New System.Drawing.Size(85, 13)
        Me.BeLabel37.TabIndex = 6
        Me.BeLabel37.Text = "Lugar de Giro"
        '
        'txtLugarGiro
        '
        Me.txtLugarGiro.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtLugarGiro.BackColor = System.Drawing.Color.Ivory
        Me.txtLugarGiro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLugarGiro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLugarGiro.ForeColor = System.Drawing.Color.Black
        Me.txtLugarGiro.KeyEnter = True
        Me.txtLugarGiro.Location = New System.Drawing.Point(99, 108)
        Me.txtLugarGiro.Name = "txtLugarGiro"
        Me.txtLugarGiro.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtLugarGiro.ShortcutsEnabled = False
        Me.txtLugarGiro.Size = New System.Drawing.Size(183, 21)
        Me.txtLugarGiro.TabIndex = 7
        Me.txtLugarGiro.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel36
        '
        Me.BeLabel36.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel36.AutoSize = True
        Me.BeLabel36.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel36.ForeColor = System.Drawing.Color.Black
        Me.BeLabel36.Location = New System.Drawing.Point(9, 81)
        Me.BeLabel36.Name = "BeLabel36"
        Me.BeLabel36.Size = New System.Drawing.Size(76, 13)
        Me.BeLabel36.TabIndex = 4
        Me.BeLabel36.Text = "Fecha Venc."
        '
        'BeLabel35
        '
        Me.BeLabel35.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel35.AutoSize = True
        Me.BeLabel35.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel35.ForeColor = System.Drawing.Color.Black
        Me.BeLabel35.Location = New System.Drawing.Point(8, 54)
        Me.BeLabel35.Name = "BeLabel35"
        Me.BeLabel35.Size = New System.Drawing.Size(68, 13)
        Me.BeLabel35.TabIndex = 2
        Me.BeLabel35.Text = "Fecha Giro"
        '
        'dtpFechaVen
        '
        Me.dtpFechaVen.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaVen.CustomFormat = ""
        Me.dtpFechaVen.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVen.KeyEnter = True
        Me.dtpFechaVen.Location = New System.Drawing.Point(99, 77)
        Me.dtpFechaVen.Name = "dtpFechaVen"
        Me.dtpFechaVen.Size = New System.Drawing.Size(92, 21)
        Me.dtpFechaVen.TabIndex = 5
        Me.dtpFechaVen.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaVen.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaGiro
        '
        Me.dtpFechaGiro.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaGiro.CustomFormat = ""
        Me.dtpFechaGiro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaGiro.KeyEnter = True
        Me.dtpFechaGiro.Location = New System.Drawing.Point(99, 50)
        Me.dtpFechaGiro.Name = "dtpFechaGiro"
        Me.dtpFechaGiro.Size = New System.Drawing.Size(92, 21)
        Me.dtpFechaGiro.TabIndex = 3
        Me.dtpFechaGiro.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaGiro.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeLabel34
        '
        Me.BeLabel34.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel34.AutoSize = True
        Me.BeLabel34.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel34.ForeColor = System.Drawing.Color.Black
        Me.BeLabel34.Location = New System.Drawing.Point(9, 31)
        Me.BeLabel34.Name = "BeLabel34"
        Me.BeLabel34.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel34.TabIndex = 0
        Me.BeLabel34.Text = "Número"
        '
        'txtNroLetra
        '
        Me.txtNroLetra.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroLetra.BackColor = System.Drawing.Color.Ivory
        Me.txtNroLetra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroLetra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroLetra.ForeColor = System.Drawing.Color.Black
        Me.txtNroLetra.KeyEnter = True
        Me.txtNroLetra.Location = New System.Drawing.Point(99, 23)
        Me.txtNroLetra.Name = "txtNroLetra"
        Me.txtNroLetra.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroLetra.ShortcutsEnabled = False
        Me.txtNroLetra.Size = New System.Drawing.Size(92, 21)
        Me.txtNroLetra.TabIndex = 1
        Me.txtNroLetra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'btnLetra
        '
        Me.btnLetra.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnLetra.Enabled = False
        Me.btnLetra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnLetra.Location = New System.Drawing.Point(933, 32)
        Me.btnLetra.Name = "btnLetra"
        Me.btnLetra.Size = New System.Drawing.Size(111, 23)
        Me.btnLetra.TabIndex = 9
        Me.btnLetra.Text = "Pagar con Letra"
        Me.btnLetra.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnLetra.UseVisualStyleBackColor = True
        '
        'BeLabel41
        '
        Me.BeLabel41.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel41.AutoSize = True
        Me.BeLabel41.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel41.ForeColor = System.Drawing.Color.Black
        Me.BeLabel41.Location = New System.Drawing.Point(781, 305)
        Me.BeLabel41.Name = "BeLabel41"
        Me.BeLabel41.Size = New System.Drawing.Size(148, 13)
        Me.BeLabel41.TabIndex = 12
        Me.BeLabel41.Text = "Documentos Cancelados"
        '
        'BeLabel40
        '
        Me.BeLabel40.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel40.AutoSize = True
        Me.BeLabel40.BackColor = System.Drawing.Color.Green
        Me.BeLabel40.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeLabel40.ForeColor = System.Drawing.Color.Black
        Me.BeLabel40.Location = New System.Drawing.Point(931, 303)
        Me.BeLabel40.Name = "BeLabel40"
        Me.BeLabel40.Size = New System.Drawing.Size(113, 15)
        Me.BeLabel40.TabIndex = 13
        Me.BeLabel40.Text = "                          "
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.txtPagarDol)
        Me.GroupBox14.Controls.Add(Me.txtImporteSoles)
        Me.GroupBox14.Controls.Add(Me.BeLabel20)
        Me.GroupBox14.Controls.Add(Me.txtImporteDolares)
        Me.GroupBox14.Controls.Add(Me.BeLabel19)
        Me.GroupBox14.Controls.Add(Me.txtSalSoles)
        Me.GroupBox14.Controls.Add(Me.BeLabel18)
        Me.GroupBox14.Controls.Add(Me.txtSalDolares)
        Me.GroupBox14.Controls.Add(Me.BeLabel17)
        Me.GroupBox14.Controls.Add(Me.txtPagarSol)
        Me.GroupBox14.Controls.Add(Me.BeLabel16)
        Me.GroupBox14.Location = New System.Drawing.Point(684, 326)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(359, 88)
        Me.GroupBox14.TabIndex = 14
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Resumen"
        '
        'txtPagarDol
        '
        Me.txtPagarDol.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPagarDol.BackColor = System.Drawing.Color.Ivory
        Me.txtPagarDol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPagarDol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPagarDol.Enabled = False
        Me.txtPagarDol.ForeColor = System.Drawing.Color.Black
        Me.txtPagarDol.KeyEnter = True
        Me.txtPagarDol.Location = New System.Drawing.Point(249, 61)
        Me.txtPagarDol.Name = "txtPagarDol"
        Me.txtPagarDol.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPagarDol.ShortcutsEnabled = False
        Me.txtPagarDol.Size = New System.Drawing.Size(100, 21)
        Me.txtPagarDol.TabIndex = 10
        Me.txtPagarDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPagarDol.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtImporteSoles
        '
        Me.txtImporteSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporteSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtImporteSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteSoles.Enabled = False
        Me.txtImporteSoles.ForeColor = System.Drawing.Color.Black
        Me.txtImporteSoles.KeyEnter = True
        Me.txtImporteSoles.Location = New System.Drawing.Point(37, 34)
        Me.txtImporteSoles.Name = "txtImporteSoles"
        Me.txtImporteSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporteSoles.ShortcutsEnabled = False
        Me.txtImporteSoles.Size = New System.Drawing.Size(100, 21)
        Me.txtImporteSoles.TabIndex = 4
        Me.txtImporteSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.ForeColor = System.Drawing.Color.Black
        Me.BeLabel20.Location = New System.Drawing.Point(274, 16)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel20.TabIndex = 2
        Me.BeLabel20.Text = "A Pagar"
        '
        'txtImporteDolares
        '
        Me.txtImporteDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporteDolares.BackColor = System.Drawing.Color.Ivory
        Me.txtImporteDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteDolares.Enabled = False
        Me.txtImporteDolares.ForeColor = System.Drawing.Color.Black
        Me.txtImporteDolares.KeyEnter = True
        Me.txtImporteDolares.Location = New System.Drawing.Point(37, 61)
        Me.txtImporteDolares.Name = "txtImporteDolares"
        Me.txtImporteDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporteDolares.ShortcutsEnabled = False
        Me.txtImporteDolares.Size = New System.Drawing.Size(100, 21)
        Me.txtImporteDolares.TabIndex = 6
        Me.txtImporteDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.ForeColor = System.Drawing.Color.Black
        Me.BeLabel19.Location = New System.Drawing.Point(172, 16)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(39, 13)
        Me.BeLabel19.TabIndex = 1
        Me.BeLabel19.Text = "Saldo"
        '
        'txtSalSoles
        '
        Me.txtSalSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSalSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtSalSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSalSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSalSoles.Enabled = False
        Me.txtSalSoles.ForeColor = System.Drawing.Color.Black
        Me.txtSalSoles.KeyEnter = True
        Me.txtSalSoles.Location = New System.Drawing.Point(143, 34)
        Me.txtSalSoles.Name = "txtSalSoles"
        Me.txtSalSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSalSoles.ShortcutsEnabled = False
        Me.txtSalSoles.Size = New System.Drawing.Size(100, 21)
        Me.txtSalSoles.TabIndex = 7
        Me.txtSalSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSalSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(66, 16)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(53, 13)
        Me.BeLabel18.TabIndex = 0
        Me.BeLabel18.Text = "Importe"
        '
        'txtSalDolares
        '
        Me.txtSalDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSalDolares.BackColor = System.Drawing.Color.Ivory
        Me.txtSalDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSalDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSalDolares.Enabled = False
        Me.txtSalDolares.ForeColor = System.Drawing.Color.Black
        Me.txtSalDolares.KeyEnter = True
        Me.txtSalDolares.Location = New System.Drawing.Point(143, 61)
        Me.txtSalDolares.Name = "txtSalDolares"
        Me.txtSalDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSalDolares.ShortcutsEnabled = False
        Me.txtSalDolares.Size = New System.Drawing.Size(100, 21)
        Me.txtSalDolares.TabIndex = 8
        Me.txtSalDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSalDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(11, 64)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(14, 13)
        Me.BeLabel17.TabIndex = 5
        Me.BeLabel17.Text = "$"
        '
        'txtPagarSol
        '
        Me.txtPagarSol.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPagarSol.BackColor = System.Drawing.Color.Ivory
        Me.txtPagarSol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPagarSol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPagarSol.Enabled = False
        Me.txtPagarSol.ForeColor = System.Drawing.Color.Black
        Me.txtPagarSol.KeyEnter = True
        Me.txtPagarSol.Location = New System.Drawing.Point(249, 34)
        Me.txtPagarSol.Name = "txtPagarSol"
        Me.txtPagarSol.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPagarSol.ShortcutsEnabled = False
        Me.txtPagarSol.Size = New System.Drawing.Size(100, 21)
        Me.txtPagarSol.TabIndex = 9
        Me.txtPagarSol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPagarSol.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(9, 38)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(24, 13)
        Me.BeLabel16.TabIndex = 3
        Me.BeLabel16.Text = "S/."
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(613, 16)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel15.TabIndex = 3
        Me.BeLabel15.Text = "Moneda"
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(616, 32)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(125, 21)
        Me.cboMoneda.TabIndex = 7
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.IdCC2, Me.DataGridViewCheckBoxColumn2, Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn25, Me.Column9, Me.Column14, Me.Column16, Me.Column18, Me.Column33})
        Me.dgvDetalle.EnableHeadersVisualStyles = False
        Me.dgvDetalle.Location = New System.Drawing.Point(12, 59)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(1032, 206)
        Me.dgvDetalle.TabIndex = 10
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "AbrDoc"
        Me.DataGridViewTextBoxColumn12.HeaderText = "T/D"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 30
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "NroDocumento"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Nº Doc."
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 107
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "FechaDocumento"
        DataGridViewCellStyle101.Format = "d"
        DataGridViewCellStyle101.NullValue = Nothing
        Me.DataGridViewTextBoxColumn14.DefaultCellStyle = DataGridViewCellStyle101
        Me.DataGridViewTextBoxColumn14.HeaderText = "F. Doc."
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 80
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "FechaVencimiento"
        DataGridViewCellStyle102.Format = "d"
        DataGridViewCellStyle102.NullValue = Nothing
        Me.DataGridViewTextBoxColumn15.DefaultCellStyle = DataGridViewCellStyle102
        Me.DataGridViewTextBoxColumn15.HeaderText = "F. Ven."
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 80
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.DataPropertyName = "SimboloMo"
        Me.DataGridViewTextBoxColumn16.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Width = 50
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Importe"
        DataGridViewCellStyle103.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle103.Format = "N2"
        DataGridViewCellStyle103.NullValue = Nothing
        Me.DataGridViewTextBoxColumn17.DefaultCellStyle = DataGridViewCellStyle103
        Me.DataGridViewTextBoxColumn17.HeaderText = "Importe"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Width = 80
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.DataPropertyName = "Saldo"
        DataGridViewCellStyle104.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle104.Format = "N2"
        DataGridViewCellStyle104.NullValue = Nothing
        Me.DataGridViewTextBoxColumn19.DefaultCellStyle = DataGridViewCellStyle104
        Me.DataGridViewTextBoxColumn19.HeaderText = "Saldo"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.ReadOnly = True
        Me.DataGridViewTextBoxColumn19.Width = 65
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.DataPropertyName = "MontoPagar"
        DataGridViewCellStyle105.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle105.Format = "N2"
        DataGridViewCellStyle105.NullValue = Nothing
        Me.DataGridViewTextBoxColumn18.DefaultCellStyle = DataGridViewCellStyle105
        Me.DataGridViewTextBoxColumn18.HeaderText = "A Pagar"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.ReadOnly = True
        Me.DataGridViewTextBoxColumn18.Width = 80
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.DataPropertyName = "DP"
        DataGridViewCellStyle106.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn20.DefaultCellStyle = DataGridViewCellStyle106
        Me.DataGridViewTextBoxColumn20.HeaderText = "D/P"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.ReadOnly = True
        Me.DataGridViewTextBoxColumn20.Width = 40
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.DataPropertyName = "MtoDP"
        DataGridViewCellStyle107.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle107.Format = "N2"
        DataGridViewCellStyle107.NullValue = Nothing
        Me.DataGridViewTextBoxColumn21.DefaultCellStyle = DataGridViewCellStyle107
        Me.DataGridViewTextBoxColumn21.HeaderText = "Mon. D/P"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.ReadOnly = True
        Me.DataGridViewTextBoxColumn21.Width = 90
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "NroOrden"
        Me.DataGridViewTextBoxColumn22.HeaderText = "O/C"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Width = 90
        '
        'IdCC2
        '
        Me.IdCC2.HeaderText = "CCosto"
        Me.IdCC2.Name = "IdCC2"
        Me.IdCC2.Width = 135
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.HeaderText = "X"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        Me.DataGridViewCheckBoxColumn2.Width = 25
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.DataPropertyName = "IdRegistro"
        Me.DataGridViewTextBoxColumn23.HeaderText = "IdRegistro"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        Me.DataGridViewTextBoxColumn23.Visible = False
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.DataPropertyName = "AbrDoc"
        Me.DataGridViewTextBoxColumn24.HeaderText = "AbrDoc"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.DataPropertyName = "MonDescripcion"
        Me.DataGridViewTextBoxColumn25.HeaderText = "DetalleCronograma"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        Me.DataGridViewTextBoxColumn25.ReadOnly = True
        Me.DataGridViewTextBoxColumn25.Visible = False
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "MontoPagar"
        Me.Column9.HeaderText = "IdCronograma"
        Me.Column9.Name = "Column9"
        Me.Column9.Visible = False
        '
        'Column14
        '
        Me.Column14.HeaderText = "Column14"
        Me.Column14.Name = "Column14"
        Me.Column14.Visible = False
        '
        'Column16
        '
        Me.Column16.HeaderText = "Column16"
        Me.Column16.Name = "Column16"
        Me.Column16.Visible = False
        '
        'Column18
        '
        Me.Column18.HeaderText = "Column18"
        Me.Column18.Name = "Column18"
        Me.Column18.Visible = False
        '
        'Column33
        '
        Me.Column33.HeaderText = "Letra"
        Me.Column33.Name = "Column33"
        Me.Column33.Width = 60
        '
        'cboAnexos
        '
        Me.cboAnexos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAnexos.BackColor = System.Drawing.Color.Ivory
        Me.cboAnexos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnexos.ForeColor = System.Drawing.Color.Black
        Me.cboAnexos.FormattingEnabled = True
        Me.cboAnexos.KeyEnter = True
        Me.cboAnexos.Location = New System.Drawing.Point(309, 32)
        Me.cboAnexos.Name = "cboAnexos"
        Me.cboAnexos.Size = New System.Drawing.Size(301, 21)
        Me.cboAnexos.TabIndex = 6
        '
        'btnActualizar
        '
        Me.btnActualizar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnActualizar.Enabled = False
        Me.btnActualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnActualizar.Location = New System.Drawing.Point(747, 32)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(183, 23)
        Me.btnActualizar.TabIndex = 8
        Me.btnActualizar.Text = "&Actualizar Programación"
        Me.btnActualizar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'cboTipAnexos
        '
        Me.cboTipAnexos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipAnexos.BackColor = System.Drawing.Color.Ivory
        Me.cboTipAnexos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipAnexos.ForeColor = System.Drawing.Color.Black
        Me.cboTipAnexos.FormattingEnabled = True
        Me.cboTipAnexos.KeyEnter = True
        Me.cboTipAnexos.Location = New System.Drawing.Point(171, 32)
        Me.cboTipAnexos.Name = "cboTipAnexos"
        Me.cboTipAnexos.Size = New System.Drawing.Size(132, 21)
        Me.cboTipAnexos.TabIndex = 5
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(306, 16)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel12.TabIndex = 2
        Me.BeLabel12.Text = "Anexo"
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(8, 16)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(157, 13)
        Me.BeLabel13.TabIndex = 0
        Me.BeLabel13.Text = "Semanas Programadas"
        '
        'cboResumenes
        '
        Me.cboResumenes.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboResumenes.BackColor = System.Drawing.Color.Ivory
        Me.cboResumenes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboResumenes.ForeColor = System.Drawing.Color.Black
        Me.cboResumenes.FormattingEnabled = True
        Me.cboResumenes.KeyEnter = True
        Me.cboResumenes.Location = New System.Drawing.Point(11, 32)
        Me.cboResumenes.Name = "cboResumenes"
        Me.cboResumenes.Size = New System.Drawing.Size(154, 21)
        Me.cboResumenes.TabIndex = 4
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(168, 16)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(80, 13)
        Me.BeLabel11.TabIndex = 1
        Me.BeLabel11.Text = "Tipo Anexo"
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage3.Controls.Add(Me.btnExportarExcel)
        Me.TabPage3.Controls.Add(Me.GroupBox12)
        Me.TabPage3.Controls.Add(Me.BeLabel27)
        Me.TabPage3.Controls.Add(Me.btnGrabar)
        Me.TabPage3.Controls.Add(Me.CheckBox1)
        Me.TabPage3.Controls.Add(Me.GroupBox8)
        Me.TabPage3.Controls.Add(Me.dgvDocumentosaEntregar)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1066, 457)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Control de Entrega"
        '
        'btnExportarExcel
        '
        Me.btnExportarExcel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnExportarExcel.Location = New System.Drawing.Point(641, 420)
        Me.btnExportarExcel.Name = "btnExportarExcel"
        Me.btnExportarExcel.Size = New System.Drawing.Size(114, 23)
        Me.btnExportarExcel.TabIndex = 42
        Me.btnExportarExcel.Text = "Exportar a Excel"
        Me.btnExportarExcel.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.BeLabel32)
        Me.GroupBox12.Controls.Add(Me.txtCheSoles)
        Me.GroupBox12.Controls.Add(Me.txtCheDolares)
        Me.GroupBox12.Controls.Add(Me.BeLabel33)
        Me.GroupBox12.Location = New System.Drawing.Point(779, 405)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(277, 44)
        Me.GroupBox12.TabIndex = 17
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Total a Pagar"
        '
        'BeLabel32
        '
        Me.BeLabel32.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel32.AutoSize = True
        Me.BeLabel32.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel32.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel32.ForeColor = System.Drawing.Color.Black
        Me.BeLabel32.Location = New System.Drawing.Point(6, 21)
        Me.BeLabel32.Name = "BeLabel32"
        Me.BeLabel32.Size = New System.Drawing.Size(27, 13)
        Me.BeLabel32.TabIndex = 2
        Me.BeLabel32.Text = "S/."
        '
        'txtCheSoles
        '
        Me.txtCheSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCheSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtCheSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCheSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCheSoles.Enabled = False
        Me.txtCheSoles.ForeColor = System.Drawing.Color.Black
        Me.txtCheSoles.KeyEnter = True
        Me.txtCheSoles.Location = New System.Drawing.Point(39, 18)
        Me.txtCheSoles.Name = "txtCheSoles"
        Me.txtCheSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCheSoles.ShortcutsEnabled = False
        Me.txtCheSoles.Size = New System.Drawing.Size(88, 20)
        Me.txtCheSoles.TabIndex = 3
        Me.txtCheSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCheSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCheDolares
        '
        Me.txtCheDolares.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCheDolares.BackColor = System.Drawing.Color.Ivory
        Me.txtCheDolares.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCheDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCheDolares.Enabled = False
        Me.txtCheDolares.ForeColor = System.Drawing.Color.Black
        Me.txtCheDolares.KeyEnter = True
        Me.txtCheDolares.Location = New System.Drawing.Point(176, 18)
        Me.txtCheDolares.Name = "txtCheDolares"
        Me.txtCheDolares.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCheDolares.ShortcutsEnabled = False
        Me.txtCheDolares.Size = New System.Drawing.Size(88, 20)
        Me.txtCheDolares.TabIndex = 5
        Me.txtCheDolares.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCheDolares.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel33
        '
        Me.BeLabel33.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel33.AutoSize = True
        Me.BeLabel33.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel33.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel33.ForeColor = System.Drawing.Color.Black
        Me.BeLabel33.Location = New System.Drawing.Point(138, 21)
        Me.BeLabel33.Name = "BeLabel33"
        Me.BeLabel33.Size = New System.Drawing.Size(32, 13)
        Me.BeLabel33.TabIndex = 4
        Me.BeLabel33.Text = "US$"
        '
        'BeLabel27
        '
        Me.BeLabel27.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel27.AutoSize = True
        Me.BeLabel27.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel27.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel27.ForeColor = System.Drawing.Color.Black
        Me.BeLabel27.Location = New System.Drawing.Point(8, 405)
        Me.BeLabel27.Name = "BeLabel27"
        Me.BeLabel27.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel27.TabIndex = 8
        Me.BeLabel27.Text = "Anexo"
        '
        'btnGrabar
        '
        Me.btnGrabar.Location = New System.Drawing.Point(854, 91)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(140, 23)
        Me.btnGrabar.TabIndex = 12
        Me.btnGrabar.Text = "Confirmar Entrega"
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(1000, 95)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(56, 17)
        Me.CheckBox1.TabIndex = 11
        Me.CheckBox1.Text = "Todos"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.BeLabel38)
        Me.GroupBox8.Controls.Add(Me.cboCriterio)
        Me.GroupBox8.Controls.Add(Me.GroupBox9)
        Me.GroupBox8.Controls.Add(Me.lblIngrese)
        Me.GroupBox8.Controls.Add(Me.txtCadenaBusqueda)
        Me.GroupBox8.Location = New System.Drawing.Point(11, 21)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(659, 91)
        Me.GroupBox8.TabIndex = 10
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Búsqueda"
        '
        'BeLabel38
        '
        Me.BeLabel38.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel38.AutoSize = True
        Me.BeLabel38.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel38.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel38.ForeColor = System.Drawing.Color.Black
        Me.BeLabel38.Location = New System.Drawing.Point(18, 24)
        Me.BeLabel38.Name = "BeLabel38"
        Me.BeLabel38.Size = New System.Drawing.Size(130, 13)
        Me.BeLabel38.TabIndex = 11
        Me.BeLabel38.Text = "Seleccione Criterio"
        '
        'cboCriterio
        '
        Me.cboCriterio.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCriterio.BackColor = System.Drawing.Color.Ivory
        Me.cboCriterio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCriterio.ForeColor = System.Drawing.Color.Black
        Me.cboCriterio.FormattingEnabled = True
        Me.cboCriterio.Items.AddRange(New Object() {"CHEQUE", "NUMERO DOCUMENTO", "RUC", "RAZON SOCIAL"})
        Me.cboCriterio.KeyEnter = True
        Me.cboCriterio.Location = New System.Drawing.Point(21, 44)
        Me.cboCriterio.Name = "cboCriterio"
        Me.cboCriterio.Size = New System.Drawing.Size(188, 21)
        Me.cboCriterio.TabIndex = 10
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.btnImprimir)
        Me.GroupBox9.Location = New System.Drawing.Point(472, 24)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(167, 52)
        Me.GroupBox9.TabIndex = 9
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Imprimir Cheques Por Entregar"
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(37, 18)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(96, 23)
        Me.btnImprimir.TabIndex = 8
        Me.btnImprimir.Text = "Ver Vista Previa"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'lblIngrese
        '
        Me.lblIngrese.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblIngrese.AutoSize = True
        Me.lblIngrese.BackColor = System.Drawing.Color.Transparent
        Me.lblIngrese.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIngrese.ForeColor = System.Drawing.Color.Black
        Me.lblIngrese.Location = New System.Drawing.Point(244, 24)
        Me.lblIngrese.Name = "lblIngrese"
        Me.lblIngrese.Size = New System.Drawing.Size(48, 13)
        Me.lblIngrese.TabIndex = 7
        Me.lblIngrese.Text = "Anexo"
        '
        'txtCadenaBusqueda
        '
        Me.txtCadenaBusqueda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCadenaBusqueda.BackColor = System.Drawing.Color.Ivory
        Me.txtCadenaBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCadenaBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCadenaBusqueda.ForeColor = System.Drawing.Color.Black
        Me.txtCadenaBusqueda.KeyEnter = True
        Me.txtCadenaBusqueda.Location = New System.Drawing.Point(247, 42)
        Me.txtCadenaBusqueda.Name = "txtCadenaBusqueda"
        Me.txtCadenaBusqueda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCadenaBusqueda.ShortcutsEnabled = False
        Me.txtCadenaBusqueda.Size = New System.Drawing.Size(207, 20)
        Me.txtCadenaBusqueda.TabIndex = 4
        Me.txtCadenaBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'dgvDocumentosaEntregar
        '
        Me.dgvDocumentosaEntregar.AllowUserToAddRows = False
        Me.dgvDocumentosaEntregar.BackgroundColor = System.Drawing.Color.White
        Me.dgvDocumentosaEntregar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDocumentosaEntregar.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.Column19, Me.DataGridViewTextBoxColumn5, Me.Column24, Me.Column27, Me.Column21, Me.Column25, Me.Column26, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.Entregar, Me.IdRegistro, Me.Column32})
        Me.dgvDocumentosaEntregar.EnableHeadersVisualStyles = False
        Me.dgvDocumentosaEntregar.Location = New System.Drawing.Point(11, 118)
        Me.dgvDocumentosaEntregar.Name = "dgvDocumentosaEntregar"
        Me.dgvDocumentosaEntregar.RowHeadersVisible = False
        Me.dgvDocumentosaEntregar.Size = New System.Drawing.Size(1048, 284)
        Me.dgvDocumentosaEntregar.TabIndex = 9
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Ruc"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Ruc"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 75
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "AnaliticoDescripcion"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Proveedor"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'Column19
        '
        Me.Column19.DataPropertyName = "AbrDoc"
        DataGridViewCellStyle108.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Column19.DefaultCellStyle = DataGridViewCellStyle108
        Me.Column19.HeaderText = "TD"
        Me.Column19.Name = "Column19"
        Me.Column19.ReadOnly = True
        Me.Column19.Width = 40
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "NroDoc"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Nro Doc"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 95
        '
        'Column24
        '
        Me.Column24.DataPropertyName = "MonSimbolo"
        DataGridViewCellStyle109.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Column24.DefaultCellStyle = DataGridViewCellStyle109
        Me.Column24.HeaderText = "Mon."
        Me.Column24.Name = "Column24"
        Me.Column24.ReadOnly = True
        Me.Column24.Width = 40
        '
        'Column27
        '
        Me.Column27.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle110.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle110.Format = "N2"
        DataGridViewCellStyle110.NullValue = Nothing
        Me.Column27.DefaultCellStyle = DataGridViewCellStyle110
        Me.Column27.HeaderText = "Importe"
        Me.Column27.Name = "Column27"
        Me.Column27.ReadOnly = True
        Me.Column27.Width = 90
        '
        'Column21
        '
        Me.Column21.DataPropertyName = "Abreviado"
        DataGridViewCellStyle111.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle111.BackColor = System.Drawing.Color.Lavender
        Me.Column21.DefaultCellStyle = DataGridViewCellStyle111
        Me.Column21.HeaderText = "Banco"
        Me.Column21.Name = "Column21"
        Me.Column21.ReadOnly = True
        Me.Column21.Width = 80
        '
        'Column25
        '
        Me.Column25.DataPropertyName = "NumeroCuenta"
        DataGridViewCellStyle112.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle112.BackColor = System.Drawing.Color.Lavender
        Me.Column25.DefaultCellStyle = DataGridViewCellStyle112
        Me.Column25.HeaderText = "Cuenta"
        Me.Column25.Name = "Column25"
        Me.Column25.ReadOnly = True
        Me.Column25.Width = 130
        '
        'Column26
        '
        Me.Column26.DataPropertyName = "NroChequera"
        DataGridViewCellStyle113.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle113.BackColor = System.Drawing.Color.Lavender
        Me.Column26.DefaultCellStyle = DataGridViewCellStyle113
        Me.Column26.HeaderText = "Chequera"
        Me.Column26.Name = "Column26"
        Me.Column26.ReadOnly = True
        Me.Column26.Width = 60
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Situacion"
        DataGridViewCellStyle114.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle114.BackColor = System.Drawing.Color.Lavender
        DataGridViewCellStyle114.Format = "d"
        DataGridViewCellStyle114.NullValue = Nothing
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle114
        Me.DataGridViewTextBoxColumn3.HeaderText = "Tipo Pago"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 85
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "NroFormaPago"
        DataGridViewCellStyle115.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle115.BackColor = System.Drawing.Color.Lavender
        DataGridViewCellStyle115.Format = "d"
        DataGridViewCellStyle115.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle115
        Me.DataGridViewTextBoxColumn4.HeaderText = "Número"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 70
        '
        'Entregar
        '
        Me.Entregar.HeaderText = "Entregado"
        Me.Entregar.Name = "Entregar"
        Me.Entregar.Width = 60
        '
        'IdRegistro
        '
        Me.IdRegistro.DataPropertyName = "IdRegistro"
        Me.IdRegistro.HeaderText = "IdRegistroDoc"
        Me.IdRegistro.Name = "IdRegistro"
        Me.IdRegistro.ReadOnly = True
        Me.IdRegistro.Visible = False
        '
        'Column32
        '
        Me.Column32.DataPropertyName = "IdMoneda"
        Me.Column32.HeaderText = "IdMoneda"
        Me.Column32.Name = "Column32"
        Me.Column32.ReadOnly = True
        Me.Column32.Visible = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage4.Controls.Add(Me.GroupBox11)
        Me.TabPage4.Controls.Add(Me.BeLabel29)
        Me.TabPage4.Controls.Add(Me.dgvEstadoDocs)
        Me.TabPage4.Controls.Add(Me.GroupBox10)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1066, 457)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Estado de Documentos"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.BeLabel30)
        Me.GroupBox11.Controls.Add(Me.txtSol)
        Me.GroupBox11.Controls.Add(Me.txtDol)
        Me.GroupBox11.Controls.Add(Me.BeLabel31)
        Me.GroupBox11.Location = New System.Drawing.Point(776, 402)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(277, 44)
        Me.GroupBox11.TabIndex = 16
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Total Cancelados"
        '
        'BeLabel30
        '
        Me.BeLabel30.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel30.AutoSize = True
        Me.BeLabel30.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel30.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel30.ForeColor = System.Drawing.Color.Black
        Me.BeLabel30.Location = New System.Drawing.Point(6, 21)
        Me.BeLabel30.Name = "BeLabel30"
        Me.BeLabel30.Size = New System.Drawing.Size(27, 13)
        Me.BeLabel30.TabIndex = 2
        Me.BeLabel30.Text = "S/."
        '
        'txtSol
        '
        Me.txtSol.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSol.BackColor = System.Drawing.Color.Ivory
        Me.txtSol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSol.Enabled = False
        Me.txtSol.ForeColor = System.Drawing.Color.Black
        Me.txtSol.KeyEnter = True
        Me.txtSol.Location = New System.Drawing.Point(39, 18)
        Me.txtSol.Name = "txtSol"
        Me.txtSol.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSol.ShortcutsEnabled = False
        Me.txtSol.Size = New System.Drawing.Size(88, 20)
        Me.txtSol.TabIndex = 3
        Me.txtSol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSol.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtDol
        '
        Me.txtDol.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDol.BackColor = System.Drawing.Color.Ivory
        Me.txtDol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDol.Enabled = False
        Me.txtDol.ForeColor = System.Drawing.Color.Black
        Me.txtDol.KeyEnter = True
        Me.txtDol.Location = New System.Drawing.Point(176, 18)
        Me.txtDol.Name = "txtDol"
        Me.txtDol.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDol.ShortcutsEnabled = False
        Me.txtDol.Size = New System.Drawing.Size(88, 20)
        Me.txtDol.TabIndex = 5
        Me.txtDol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDol.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel31
        '
        Me.BeLabel31.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel31.AutoSize = True
        Me.BeLabel31.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel31.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel31.ForeColor = System.Drawing.Color.Black
        Me.BeLabel31.Location = New System.Drawing.Point(138, 21)
        Me.BeLabel31.Name = "BeLabel31"
        Me.BeLabel31.Size = New System.Drawing.Size(32, 13)
        Me.BeLabel31.TabIndex = 4
        Me.BeLabel31.Text = "US$"
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel29.ForeColor = System.Drawing.Color.Black
        Me.BeLabel29.Location = New System.Drawing.Point(6, 402)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel29.TabIndex = 13
        Me.BeLabel29.Text = "Anexo"
        '
        'dgvEstadoDocs
        '
        Me.dgvEstadoDocs.AllowUserToAddRows = False
        Me.dgvEstadoDocs.BackgroundColor = System.Drawing.Color.White
        Me.dgvEstadoDocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEstadoDocs.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Ruc, Me.AnaliticoDescripcion, Me.Column30, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.ImpDoc, Me.DataGridViewTextBoxColumn26, Me.DataGridViewTextBoxColumn27, Me.DataGridViewTextBoxColumn28, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.Column31})
        Me.dgvEstadoDocs.EnableHeadersVisualStyles = False
        Me.dgvEstadoDocs.Location = New System.Drawing.Point(9, 73)
        Me.dgvEstadoDocs.Name = "dgvEstadoDocs"
        Me.dgvEstadoDocs.RowHeadersVisible = False
        Me.dgvEstadoDocs.Size = New System.Drawing.Size(1048, 323)
        Me.dgvEstadoDocs.TabIndex = 12
        '
        'Ruc
        '
        Me.Ruc.DataPropertyName = "Ruc"
        Me.Ruc.HeaderText = "Ruc"
        Me.Ruc.Name = "Ruc"
        Me.Ruc.ReadOnly = True
        Me.Ruc.Width = 75
        '
        'AnaliticoDescripcion
        '
        Me.AnaliticoDescripcion.DataPropertyName = "AnaliticoDescripcion"
        Me.AnaliticoDescripcion.HeaderText = "Proveedor"
        Me.AnaliticoDescripcion.Name = "AnaliticoDescripcion"
        Me.AnaliticoDescripcion.ReadOnly = True
        Me.AnaliticoDescripcion.Width = 200
        '
        'Column30
        '
        Me.Column30.DataPropertyName = "FechaBase"
        DataGridViewCellStyle116.Format = "d"
        DataGridViewCellStyle116.NullValue = Nothing
        Me.Column30.DefaultCellStyle = DataGridViewCellStyle116
        Me.Column30.HeaderText = "Fecha"
        Me.Column30.Name = "Column30"
        Me.Column30.ReadOnly = True
        Me.Column30.Width = 70
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "AbrDoc"
        DataGridViewCellStyle117.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle117
        Me.DataGridViewTextBoxColumn8.HeaderText = "TD"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 40
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "NroDoc"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Nro Doc"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 95
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "MonSimbolo"
        DataGridViewCellStyle118.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.DataGridViewTextBoxColumn10.DefaultCellStyle = DataGridViewCellStyle118
        Me.DataGridViewTextBoxColumn10.HeaderText = "Mon."
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 40
        '
        'ImpDoc
        '
        Me.ImpDoc.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle119.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle119.Format = "N2"
        DataGridViewCellStyle119.NullValue = Nothing
        Me.ImpDoc.DefaultCellStyle = DataGridViewCellStyle119
        Me.ImpDoc.HeaderText = "Importe"
        Me.ImpDoc.Name = "ImpDoc"
        Me.ImpDoc.ReadOnly = True
        Me.ImpDoc.Width = 85
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.DataPropertyName = "Abreviado"
        DataGridViewCellStyle120.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle120.BackColor = System.Drawing.Color.Lavender
        Me.DataGridViewTextBoxColumn26.DefaultCellStyle = DataGridViewCellStyle120
        Me.DataGridViewTextBoxColumn26.HeaderText = "Banco"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.ReadOnly = True
        Me.DataGridViewTextBoxColumn26.Width = 80
        '
        'DataGridViewTextBoxColumn27
        '
        Me.DataGridViewTextBoxColumn27.DataPropertyName = "NumeroCuenta"
        DataGridViewCellStyle121.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle121.BackColor = System.Drawing.Color.Lavender
        Me.DataGridViewTextBoxColumn27.DefaultCellStyle = DataGridViewCellStyle121
        Me.DataGridViewTextBoxColumn27.HeaderText = "Cuenta"
        Me.DataGridViewTextBoxColumn27.Name = "DataGridViewTextBoxColumn27"
        Me.DataGridViewTextBoxColumn27.ReadOnly = True
        Me.DataGridViewTextBoxColumn27.Width = 120
        '
        'DataGridViewTextBoxColumn28
        '
        Me.DataGridViewTextBoxColumn28.DataPropertyName = "NroChequera"
        DataGridViewCellStyle122.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle122.BackColor = System.Drawing.Color.Lavender
        Me.DataGridViewTextBoxColumn28.DefaultCellStyle = DataGridViewCellStyle122
        Me.DataGridViewTextBoxColumn28.HeaderText = "Chequera"
        Me.DataGridViewTextBoxColumn28.Name = "DataGridViewTextBoxColumn28"
        Me.DataGridViewTextBoxColumn28.ReadOnly = True
        Me.DataGridViewTextBoxColumn28.Width = 60
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.DataPropertyName = "Situacion"
        DataGridViewCellStyle123.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle123.BackColor = System.Drawing.Color.Lavender
        DataGridViewCellStyle123.Format = "d"
        DataGridViewCellStyle123.NullValue = Nothing
        Me.DataGridViewTextBoxColumn29.DefaultCellStyle = DataGridViewCellStyle123
        Me.DataGridViewTextBoxColumn29.HeaderText = "Tipo Pago"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.ReadOnly = True
        Me.DataGridViewTextBoxColumn29.Width = 85
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.DataPropertyName = "NroFormaPago"
        DataGridViewCellStyle124.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle124.BackColor = System.Drawing.Color.Lavender
        DataGridViewCellStyle124.Format = "d"
        DataGridViewCellStyle124.NullValue = Nothing
        Me.DataGridViewTextBoxColumn30.DefaultCellStyle = DataGridViewCellStyle124
        Me.DataGridViewTextBoxColumn30.HeaderText = "Número"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.ReadOnly = True
        Me.DataGridViewTextBoxColumn30.Width = 70
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.DataPropertyName = "IdRegistro"
        Me.DataGridViewTextBoxColumn31.HeaderText = "IdRegistroDoc"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.ReadOnly = True
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'Column31
        '
        Me.Column31.DataPropertyName = "IdMoneda"
        Me.Column31.HeaderText = "IdMoneda"
        Me.Column31.Name = "Column31"
        Me.Column31.ReadOnly = True
        Me.Column31.Visible = False
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.rdbRazon)
        Me.GroupBox10.Controls.Add(Me.chkDocSinCance)
        Me.GroupBox10.Controls.Add(Me.chkDocCance)
        Me.GroupBox10.Controls.Add(Me.rdbNroDocumento)
        Me.GroupBox10.Controls.Add(Me.rdbRuc)
        Me.GroupBox10.Controls.Add(Me.txtBuscarEstadoDoc)
        Me.GroupBox10.Location = New System.Drawing.Point(217, 3)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(699, 65)
        Me.GroupBox10.TabIndex = 11
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Búsqueda"
        '
        'rdbRazon
        '
        Me.rdbRazon.AutoSize = True
        Me.rdbRazon.Location = New System.Drawing.Point(70, 27)
        Me.rdbRazon.Name = "rdbRazon"
        Me.rdbRazon.Size = New System.Drawing.Size(88, 17)
        Me.rdbRazon.TabIndex = 9
        Me.rdbRazon.TabStop = True
        Me.rdbRazon.Text = "Razón Social"
        Me.rdbRazon.UseVisualStyleBackColor = True
        '
        'chkDocSinCance
        '
        Me.chkDocSinCance.AutoSize = True
        Me.chkDocSinCance.Location = New System.Drawing.Point(295, 39)
        Me.chkDocSinCance.Name = "chkDocSinCance"
        Me.chkDocSinCance.Size = New System.Drawing.Size(150, 17)
        Me.chkDocSinCance.TabIndex = 8
        Me.chkDocSinCance.Text = "Documentos Por Cancelar"
        Me.chkDocSinCance.UseVisualStyleBackColor = True
        '
        'chkDocCance
        '
        Me.chkDocCance.AutoSize = True
        Me.chkDocCance.Location = New System.Drawing.Point(295, 16)
        Me.chkDocCance.Name = "chkDocCance"
        Me.chkDocCance.Size = New System.Drawing.Size(145, 17)
        Me.chkDocCance.TabIndex = 7
        Me.chkDocCance.Text = "Documentos Cancelados"
        Me.chkDocCance.UseVisualStyleBackColor = True
        '
        'rdbNroDocumento
        '
        Me.rdbNroDocumento.AutoSize = True
        Me.rdbNroDocumento.Location = New System.Drawing.Point(169, 27)
        Me.rdbNroDocumento.Name = "rdbNroDocumento"
        Me.rdbNroDocumento.Size = New System.Drawing.Size(120, 17)
        Me.rdbNroDocumento.TabIndex = 6
        Me.rdbNroDocumento.TabStop = True
        Me.rdbNroDocumento.Text = "Número Documento"
        Me.rdbNroDocumento.UseVisualStyleBackColor = True
        '
        'rdbRuc
        '
        Me.rdbRuc.AutoSize = True
        Me.rdbRuc.Location = New System.Drawing.Point(15, 27)
        Me.rdbRuc.Name = "rdbRuc"
        Me.rdbRuc.Size = New System.Drawing.Size(45, 17)
        Me.rdbRuc.TabIndex = 5
        Me.rdbRuc.TabStop = True
        Me.rdbRuc.Text = "Ruc"
        Me.rdbRuc.UseVisualStyleBackColor = True
        '
        'txtBuscarEstadoDoc
        '
        Me.txtBuscarEstadoDoc.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBuscarEstadoDoc.BackColor = System.Drawing.Color.Ivory
        Me.txtBuscarEstadoDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBuscarEstadoDoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscarEstadoDoc.ForeColor = System.Drawing.Color.Black
        Me.txtBuscarEstadoDoc.KeyEnter = True
        Me.txtBuscarEstadoDoc.Location = New System.Drawing.Point(450, 24)
        Me.txtBuscarEstadoDoc.Name = "txtBuscarEstadoDoc"
        Me.txtBuscarEstadoDoc.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBuscarEstadoDoc.ShortcutsEnabled = False
        Me.txtBuscarEstadoDoc.Size = New System.Drawing.Size(236, 20)
        Me.txtBuscarEstadoDoc.TabIndex = 4
        Me.txtBuscarEstadoDoc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboTipoPago
        '
        Me.cboTipoPago.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoPago.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoPago.ForeColor = System.Drawing.Color.Black
        Me.cboTipoPago.FormattingEnabled = True
        Me.cboTipoPago.KeyEnter = True
        Me.cboTipoPago.Location = New System.Drawing.Point(682, 531)
        Me.cboTipoPago.Name = "cboTipoPago"
        Me.cboTipoPago.Size = New System.Drawing.Size(124, 21)
        Me.cboTipoPago.TabIndex = 0
        Me.cboTipoPago.Visible = False
        '
        'Timer1
        '
        '
        'frmPagoProveedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1065, 490)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "frmPagoProveedores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Pago de Proveedores"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvOrdenes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        CType(Me.dgvDocumentosaEntregar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        CType(Me.dgvEstadoDocs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTipoCambio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnProgramar As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvDocumentos As System.Windows.Forms.DataGridView
    Friend WithEvents cboTipoAnexo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboAnexo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroSemana As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTotDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTotSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNroDoc As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDireccion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSaldoDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSaldoSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents cboTipAnexos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboResumenes As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboAnexos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnActualizar As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtPagarDol As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPagarSol As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSalDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSalSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtImporteDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtImporteSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvOrdenes As System.Windows.Forms.DataGridView
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtLineaCreDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtLineaCreSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtDiasCred As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel24 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtEstadoDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtEstadoSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel26 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtIdLinea As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents dgvDocumentosaEntregar As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCadenaBusqueda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboTipoPago As ctrLibreria.Controles.BeComboBox
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents lblIngrese As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel27 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Moneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdOrden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrvCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrvRUC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocumento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCC As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AbrDoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents chkVerTodo As System.Windows.Forms.CheckBox
    Friend WithEvents txtNroOrden As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents dgvEstadoDocs As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbNroDocumento As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRuc As System.Windows.Forms.RadioButton
    Friend WithEvents txtBuscarEstadoDoc As ctrLibreria.Controles.BeTextBox
    Friend WithEvents chkDocSinCance As System.Windows.Forms.CheckBox
    Friend WithEvents chkDocCance As System.Windows.Forms.CheckBox
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel30 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSol As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDol As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel31 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Ruc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AnaliticoDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImpDoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn28 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column27 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Entregar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IdRegistro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel32 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCheSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCheDolares As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel33 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel34 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroLetra As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel39 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotalLetra As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel37 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtLugarGiro As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel36 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel35 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaVen As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaGiro As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnGrabarLetra As ctrLibreria.Controles.BeButton
    Friend WithEvents btnLetra As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel41 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel40 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel43 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCanDocLetra As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel42 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtReferencia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCC2 As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column33 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnExportarExcel As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents cboCriterio As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel38 As ctrLibreria.Controles.BeLabel
    Friend WithEvents rdbRazon As System.Windows.Forms.RadioButton
End Class
