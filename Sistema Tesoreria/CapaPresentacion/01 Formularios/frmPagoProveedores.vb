Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Imports System.Data

Public Class frmPagoProveedores

    Private ePagoProveedores As clsPagoProveedores
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Private eRegistroDocumento As clsRegistroDocumento
    Private eSesionCajas As clsSesionCajas
    Private eGastosGenerales As clsGastosGenerales

    Private eArqueo As clsArqueo
    Private eTempo As clsPlantTempo
    Dim dTipoCambio As Double
    Dim CountDocumentos As Integer
    Dim CountCantDocSoles As Integer
    Dim CountCantDocDolares As Integer
    Dim dblSoles As Double
    Dim dblDolares As Double
    Dim strGlosaFacturasSoles As String = ""
    Dim strGlosaFacturasDolares As String = ""
    Dim sRuc As String = ""

    Dim CountDocumentosLetra As Integer = 0
    Private da As SqlDataAdapter

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmPagoProveedores = Nothing
    Public Shared Function Instance() As frmPagoProveedores
        If frmInstance Is Nothing Then
            frmInstance = New frmPagoProveedores
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmPagoProveedores_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Items(0).Enabled = False
    'frmPrincipal.Barra.Enabled = False
  End Sub

    Private Sub frmPagoProveedores_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmPagoProveedores_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        For x As Integer = 0 To dgvDocumentos.Columns.Count - 1
            'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
            dgvDocumentos.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        ePagoProveedores = New clsPagoProveedores
        Try
            cboTipoAnexo.DataSource = ePagoProveedores.fListarTipoProveedores(gEmpresa)
            cboTipoAnexo.ValueMember = "TipoAcreedorId"
            cboTipoAnexo.DisplayMember = "TipoAnaliticoDescripcion"
            cboTipoAnexo.SelectedIndex = -1
            cboAnexo.SelectedIndex = -1
        Catch ex As Exception
        End Try

        ePagoProveedores = New clsPagoProveedores
        Try
            cboMoneda.DataSource = ePagoProveedores.fListarMonedas()
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = -1
            'cboAnexo.SelectedIndex = -1
        Catch ex As Exception
        End Try

        Dim dtTable As DataTable
        dtTable = New DataTable
        dtTable = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, Today())
        If dtTable.Rows.Count > 0 Then
            dTipoCambio = Val(dtTable.Rows(0).Item("TcaVenta"))
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
        Else
            dTipoCambio = 0.0
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
        End If
        Try
            ePagoProveedores = New clsPagoProveedores
            cboTipoPago.DataSource = ePagoProveedores.fListarTipoMovPagos()
            If ePagoProveedores.iNroRegistros > 0 Then
                cboTipoPago.ValueMember = "IdRendicion"
                cboTipoPago.DisplayMember = "DescripcionRendicion"
                cboTipoPago.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
        Timer1.Enabled = True

        'cboTipoAnexo.Focus()

    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Timer1.Enabled = True Then
            cboTipoAnexo.Focus()
        End If
        Timer1.Enabled = False
    End Sub

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

    Private Sub dgvDocumentos_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellDoubleClick
        If dgvDocumentos.Rows.Count > 0 Then
            Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex
            If columna = 11 Then
                Panel4.Visible = True
                txtNroOrden.Clear()
                txtNroOrden_TextChanged(sender, e)
                txtNroOrden.Focus()
                txtNroOrden.Enabled = True
            End If
        End If
    End Sub

    Private Sub dgvDocumentos_CellEndEdit1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellEndEdit
        Dim StrCad As String = String.Empty
        Dim StrImporteDoc As String = String.Empty
        If e.ColumnIndex = 7 Then
            'If e.ColumnIndex = 7 Then
            StrCad = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value)
            StrImporteDoc = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(18).Value)
            'StrImporteDoc = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(5).Value)

            'StrCad = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value)
            'StrImporteDoc = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(6).Value)

            If StrCad = Nothing Or StrImporteDoc = Nothing Then
                Calcular()
                Exit Sub
            End If
            Try
                If Convert.ToDouble(StrCad) > Convert.ToDouble(StrImporteDoc) Then
                    'If Convert.ToDouble(StrCad) > Convert.ToDouble(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(6).Value) Then

                    MessageBox.Show("El Monto a Pagar Supera al Importe del Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = ""
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(6).Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(18).Value
                    dgvDocumentos.CurrentCell = dgvDocumentos(7, dgvDocumentos.CurrentRow.Index)
                    If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(13).Value = True Then
                        dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(13).Value = False
                    End If
                Else
                    Dim resta As Double = 0
                    resta = Convert.ToDouble(StrImporteDoc) - Convert.ToDouble(StrCad)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(6).Value = resta 'Format(CDec(resta), "0.00")
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = StrCad 'Format(CDec(StrCad), "0.00")
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(13).Value = True
                    dgvDocumentos.CurrentCell = dgvDocumentos(7, dgvDocumentos.CurrentRow.Index)

                    If resta = StrImporteDoc Then
                        If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(13).Value = True Then
                            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(13).Value = False
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try

        ElseIf e.ColumnIndex = 9 Then
            Dim Importe As Double = 0
            Dim Porcentaje As Double = 0
            Dim PorcentajeD As Double = 0
            Dim PorcentajeP As Double = 0

            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = "0.00"
            End If

            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value = ""
            End If

            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "d" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(5).Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value)
                PorcentajeD = (Importe * Porcentaje) / 100

                'If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(16).Value) = "SOLES" Then
                '    PorcentajeD = Format(Math.Round(PorcentajeD, 0), "#,##0.00")
                'End If

                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(10).Value = PorcentajeD
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = Importe - PorcentajeD
            End If

            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "P" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "p" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(5).Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value)
                PorcentajeP = (Importe * Porcentaje) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(10).Value = PorcentajeP
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = Importe + PorcentajeP
            End If
        ElseIf e.ColumnIndex = 8 Then

            Dim CadenaCon As String
            If vb.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = True Then
                CadenaCon = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = Format(0, "0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(10).Value = Format(0, "0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(5).Value
            Else
                CadenaCon = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value
                'dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = Format(0, "0.00")
                'dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(10).Value = Format(0, "0.00")
            End If
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value = vb.UCase(CadenaCon)

            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = "0.00"
                Dim ImporteAux As Double = 0
                Dim PorcentajeAux As Double = 0
                Dim PorcentajeDAux As Double = 0
                Dim PorcentajePAux As Double = 0
                ImporteAux = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(5).Value)
                PorcentajeAux = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value)
                PorcentajeDAux = (ImporteAux * PorcentajeAux) / 100
                'If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(16).Value) = "SOLES" Then
                '    PorcentajeDAux = Format(Math.Round(PorcentajeDAux, 0), "#,##0.00")
                'End If
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = Format(PorcentajeAux, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(10).Value = PorcentajeDAux
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = ImporteAux - PorcentajeDAux
            End If

            Dim Importe As Double = 0
            Dim Porcentaje As Double = 0
            Dim PorcentajeD As Double = 0
            Dim PorcentajeP As Double = 0

            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = "0.00"
            End If

            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value = ""
            End If

            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "d" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(5).Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value)
                PorcentajeD = (Importe * Porcentaje) / 100
                'If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(16).Value) = "SOLES" Then
                '    PorcentajeD = Format(Math.Round(PorcentajeD, 0), "#,##0.00")
                'End If
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(10).Value = PorcentajeD
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = Importe - PorcentajeD
            End If

            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "P" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "p" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(5).Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value)
                PorcentajeP = (Importe * Porcentaje) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(9).Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(10).Value = PorcentajeP
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(7).Value = Importe + PorcentajeP
            End If

        ElseIf e.ColumnIndex = 11 Then
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(11).Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(19).Value = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IdCC").Value = ""
            End If
        End If
        Calcular()
    End Sub

    Private Sub dgvDocumentos_MouseUp1(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDocumentos.MouseUp
        If dgvDocumentos.Rows.Count > 0 Then
            Dim iFila As Integer
            Calcular()
            If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 13 Then
                iFila = dgvDocumentos.CurrentRow.Index
                dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                dgvDocumentos.CurrentCell = dgvDocumentos(13, iFila)

                If dgvDocumentos.Rows.Count > 0 Then
                    Dim iFila2 As Integer
                    iFila2 = dgvDocumentos.CurrentRow.Index
                    If Convert.ToBoolean(dgvDocumentos.Rows(iFila2).Cells(13).Value) = True Then
                        If dgvDocumentos.Rows(iFila2).Cells(12).Value = "" Then
                            MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            dgvDocumentos.Rows(iFila2).Cells(13).Value = False
                            dgvDocumentos.CurrentCell = dgvDocumentos(0, iFila2)
                            dgvDocumentos.CurrentCell = dgvDocumentos(12, iFila2)
                            Exit Sub
                        End If
                    End If
                End If

            End If
        End If
    End Sub

    'Dim dblDetraccion As Double
    Sub Calcular()
        strGlosaFacturasSoles = ""
        strGlosaFacturasDolares = ""
        CountDocumentos = 0
        CountCantDocSoles = 0
        CountCantDocDolares = 0
        dblSoles = 0
        dblDolares = 0
        Dim CantidadDetraccionesSoles As Integer = 0
        'dblDetraccion = 0
        If dgvDocumentos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells(13).Value) = True Then
                    If Trim(dgvDocumentos.Rows(x).Cells(16).Value) = "01" Then

                        If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(8).Value) = "d" Then
                            CantidadDetraccionesSoles = CantidadDetraccionesSoles + 1
                        End If

                        'If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(x).Cells(8).Value) = False And Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(x).Cells(9).Value) = False And Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(x).Cells(10).Value) = False Then
                        '  If Trim(dgvDocumentos.Rows(x).Cells(8).Value) = "D" Then
                        '    dblDetraccion = dblDetraccion + dgvDocumentos.Rows(x).Cells(10).Value
                        '    dblDetraccion = Format(Math.Round(dblDetraccion, 0), "#,##0.00")
                        '  End If
                        'End If

                        Dim StrCad As String = String.Empty
                        ''StrCad = Trim(dgvDocumentos.Rows(x).Cells(6).Value)
                        'StrCad = Trim(dgvDocumentos.Rows(x).Cells(7).Value)
                        'If StrCad = Nothing Then
                        '    dblSoles = dblSoles + 0
                        'Else
                        '    dblSoles = dblSoles + dgvDocumentos.Rows(x).Cells(7).Value
                        '    'StrCad = Trim(dgvDocumentos.Rows(x).Cells(7).Value)
                        'End If

                        If Trim(dgvDocumentos.Rows(x).Cells(0).Value) <> "NA" Then
                            'MessageBox.Show(dgvDocumentos.Rows(x).Cells(0).Value)
                            If StrCad = Nothing Then
                                dblSoles = dblSoles + 0
                            Else
                                dblSoles = dblSoles + dgvDocumentos.Rows(x).Cells(7).Value
                            End If
                        ElseIf Trim(dgvDocumentos.Rows(x).Cells(0).Value) = "NA" Then
                            If StrCad = Nothing Then
                                dblSoles = dblSoles - 0
                            Else
                                dblSoles = dblSoles - dgvDocumentos.Rows(x).Cells(7).Value
                            End If
                        End If

                        CountCantDocSoles = CountCantDocSoles + 1
                        strGlosaFacturasSoles = strGlosaFacturasSoles & " " & dgvDocumentos.Rows(x).Cells("AbrDoc").Value & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & "  "
                    ElseIf Trim(dgvDocumentos.Rows(x).Cells(16).Value) = "02" Then
                        Dim StrCad As String = String.Empty
                        'StrCad = Trim(dgvDocumentos.Rows(x).Cells(6).Value)
                        'StrCad = Trim(dgvDocumentos.Rows(x).Cells(7).Value)
                        'If StrCad = Nothing Then
                        '    dblDolares = dblDolares + 0
                        'Else
                        '    'dblDolares = dblDolares + dgvDocumentos.Rows(x).Cells(6).Value
                        '    dblDolares = dblDolares + dgvDocumentos.Rows(x).Cells(7).Value
                        'End If
                        If Trim(dgvDocumentos.Rows(x).Cells(0).Value) <> "NA" Then
                            If StrCad = Nothing Then
                                dblDolares = dblDolares + 0
                            Else
                                dblDolares = dblDolares + dgvDocumentos.Rows(x).Cells(7).Value
                            End If
                        ElseIf Trim(dgvDocumentos.Rows(x).Cells(0).Value) = "NA" Then
                            If StrCad = Nothing Then
                                dblDolares = dblDolares - 0
                            Else
                                dblDolares = dblDolares - dgvDocumentos.Rows(x).Cells(7).Value
                            End If
                        End If

                        CountCantDocDolares = CountCantDocDolares + 1
                        strGlosaFacturasDolares = strGlosaFacturasDolares & " " & dgvDocumentos.Rows(x).Cells("AbrDoc").Value & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & "  "
                    End If
                    CountDocumentos = CountDocumentos + 1
                Else
                    dgvDocumentos.Rows(x).Cells(6).Value = 0 'dgvDocumentos.Rows(x).Cells(17).Value
                    'MessageBox.Show(dgvDocumentos.Rows(x).Cells(17).Value)
                    'dgvDocumentos.Rows(x).Cells(7).Value = "" 'dgvDocumentos.Rows(x).Cells(16).Value
                    'MessageBox.Show(dgvDocumentos.Rows(x).Cells(16).Value)
                    'dgvDocumentos.Rows(x).Cells(7).Value = ""
                End If
            Next
            strGlosaFacturasSoles = "Pago a Proveedor, Doc: " & strGlosaFacturasSoles
            strGlosaFacturasDolares = "Pago a Proveedor, Doc: " & strGlosaFacturasDolares
            'dgvDocumentos.CurrentCell = dgvDocumentos(6, 0)
            'dgvDocumentos.CurrentCell = dgvDocumentos(6, 0)
        End If
        txtNroDoc.Text = CountDocumentos
        'txtTotSoles.Text = Format(dblSoles, "0.00")
        'txtTotDolares.Text = Format(dblDolares, "0.00")
        'txtTotSoles.Text = Format(Math.Round(dblSoles, 0), "#,##0.00")
        If CantidadDetraccionesSoles = 0 Then
            txtTotSoles.Text = Format(dblSoles, "#,##0.00")
        ElseIf CantidadDetraccionesSoles > 0 Then
            txtTotSoles.Text = Format(Math.Round(dblSoles, 0), "#,##0.00")
        End If

        txtTotDolares.Text = Format(dblDolares, "#,##0.00")

        'dgvDetalle.CurrentCell = dgvDetalle(7, dgvDetalle.CurrentRow.Index)
    End Sub

    Private Sub cargarCentrosDeCosto()
        IdCC.Items.Clear()
        ePagoProveedores = New clsPagoProveedores
        'eTempo = New clsPlantTempo
        Dim dtTable As DataTable
        dtTable = New DataTable
        dtTable = ePagoProveedores.fCargarCCosto(gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            'IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosDescripcion") & " - " & dtTable.Rows(i).Item("CCosCodigo")))
            IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosCodigo")) & " - " & dtTable.Rows(i).Item("CCosDescripcion"))
        Next
    End Sub

    Private Sub cargarCentrosDeCosto2()
        IdCC2.Items.Clear()
        ePagoProveedores = New clsPagoProveedores
        'eTempo = New clsPlantTempo
        Dim dtTable As DataTable
        dtTable = New DataTable
        dtTable = ePagoProveedores.fCargarCCosto(gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            'IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosDescripcion") & " - " & dtTable.Rows(i).Item("CCosCodigo")))
            IdCC2.Items.Add(Trim(dtTable.Rows(i).Item("CCosCodigo")) & " - " & dtTable.Rows(i).Item("CCosDescripcion"))
        Next
    End Sub

    Private Sub cboAnexo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAnexo.SelectedIndexChanged
        Dim IdAnexo As String
        Dim dSaldoSoles As Double = 0
        Dim dSaldoDolares As Double = 0
        If cboAnexo.SelectedIndex > -1 Then
            Try
                IdAnexo = cboAnexo.SelectedValue.ToString
                If IdAnexo <> "System.Data.DataRowView" Then
                    Dim dtTable As DataTable
                    dtTable = New DataTable
                    dtTable = ePagoProveedores.fTraerDatosProveedor(Trim(IdAnexo), Trim(cboTipoAnexo.SelectedValue), gEmpresa)
                    If dtTable.Rows.Count > 0 Then
                        txtCodigo.Text = dtTable.Rows(0).Item("AnaliticoCodigo")
                        txtDireccion.Text = dtTable.Rows(0).Item("Direccion")
                        sRuc = dtTable.Rows(0).Item("RUC")
                        Dim dtTableLineCre As DataTable
                        dtTableLineCre = New DataTable
                        dtTableLineCre = ePagoProveedores.fTraerLineaDeCreditoProv(gEmpresa, Trim(dtTable.Rows(0).Item("AnaliticoCodigo")))
                        If dtTableLineCre.Rows.Count > 0 Then
                            txtLineaCreSoles.Text = Format(dtTableLineCre.Rows(0).Item("SaldoSolesLinea"), "#,##0.00")
                            txtLineaCreDolares.Text = Format(dtTableLineCre.Rows(0).Item("SaldoDolaresLinea"), "#,##0.00")
                            txtDiasCred.Text = Trim(dtTableLineCre.Rows(0).Item("DiasCredito"))
                            txtIdLinea.Text = Trim(dtTableLineCre.Rows(0).Item("IdLinea"))
                            txtLineaCreSoles.Enabled = True
                            txtLineaCreDolares.Enabled = True
                            txtDiasCred.Enabled = True
                            txtIdLinea.Enabled = True
                        Else
                            txtLineaCreSoles.Text = "0.00"
                            txtLineaCreDolares.Text = "0.00"
                            txtDiasCred.Clear()
                            txtIdLinea.Clear()
                            txtLineaCreSoles.Enabled = False
                            txtLineaCreDolares.Enabled = False
                            txtDiasCred.Enabled = False
                            txtIdLinea.Enabled = False
                        End If
                    End If
                    Dim dtDocumentos As DataTable
                    dtDocumentos = New DataTable
                    dtDocumentos = ePagoProveedores.fTraerDocumentos(Trim(IdAnexo), Trim(cboTipoAnexo.SelectedValue), gEmpresa)
                    dgvDocumentos.AutoGenerateColumns = False
                    dgvDocumentos.DataSource = dtDocumentos
                    dgvDocumentos.Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns(7).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns(9).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    Dim y As Integer
                    ' Dim z As Integer
                    If dtDocumentos.Rows.Count > 0 Then
                        For y = 0 To dtDocumentos.Rows.Count - 1
                            'For z = 0 To dtDocumentos.Columns.Count - 1
                            'If z = 11 Then
                            If Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCosCodigo")) = False Then
                                For a As Integer = 0 To IdCC.Items.Count - 1
                                    Dim StrCadCodigo As String
                                    Dim cad As String = IdCC.Items.Item(a)
                                    StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                                    If Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCosCodigo")) = False Then
                                        If Trim(dtDocumentos.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                                            Dim xxx As String = Trim(dtDocumentos.Rows(y).Item("CCosCodigo"))
                                            dgvDocumentos.Rows(y).Cells("IdCC").Value = IdCC.Items.Item(a)
                                            'Dim zzz As String = dgvDocumentos.Rows(y).Cells(0).Value
                                        End If
                                    End If
                                Next
                            End If
                            'End If
                            'Next
                        Next
                    End If
                    If dtDocumentos.Rows.Count > 0 Then
                        cargarCentrosDeCosto()
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Trim(dgvDocumentos.Rows(x).Cells(16).Value) = "01" Then
                                Dim StrCad As String = String.Empty
                                StrCad = Trim(dgvDocumentos.Rows(x).Cells(5).Value)
                                If StrCad = Nothing Then
                                    dSaldoSoles = dSaldoSoles + 0
                                Else
                                    dSaldoSoles = dSaldoSoles + dgvDocumentos.Rows(x).Cells(5).Value
                                End If
                            ElseIf Trim(dgvDocumentos.Rows(x).Cells(16).Value) = "02" Then
                                Dim StrCad As String = String.Empty
                                StrCad = Trim(dgvDocumentos.Rows(x).Cells(5).Value)
                                If StrCad = Nothing Then
                                    dSaldoDolares = dSaldoDolares + 0
                                Else
                                    dSaldoDolares = dSaldoDolares + dgvDocumentos.Rows(x).Cells(5).Value
                                End If
                            End If
                        Next
                        txtSaldoSoles.Text = Format(dSaldoSoles, "#,##0.00")
                        txtSaldoDolares.Text = Format(dSaldoDolares, "#,##0.00")
                        Dim DifSoles As Decimal = 0
                        Dim DifDolares As Decimal = 0
                        DifSoles = Convert.ToDouble(txtLineaCreSoles.Text) - dSaldoSoles
                        DifDolares = Convert.ToDouble(txtLineaCreDolares.Text) - dSaldoDolares
                        txtEstadoSoles.Text = Format(DifSoles, "#,##0.00")
                        txtEstadoDolares.Text = Format(DifDolares, "#,##0.00")
                        If DifSoles >= (Convert.ToDouble(txtLineaCreSoles.Text) / 2) Then
                            txtEstadoSoles.BackColor = Color.Green
                        ElseIf DifSoles < (Convert.ToDouble(txtLineaCreSoles.Text) / 2) And DifSoles > 0 Then
                            txtEstadoSoles.BackColor = Color.Yellow
                        ElseIf DifSoles <= 0 Then
                            txtEstadoSoles.BackColor = Color.Red
                        End If
                        If DifDolares >= (Convert.ToDouble(txtLineaCreDolares.Text) / 2) Then
                            txtEstadoDolares.BackColor = Color.Green
                        ElseIf DifDolares < (Convert.ToDouble(txtLineaCreDolares.Text) / 2) And DifDolares > 0 Then
                            txtEstadoDolares.BackColor = Color.Yellow
                        ElseIf DifDolares <= 0 Then
                            txtEstadoDolares.BackColor = Color.Red
                        End If
                        If Trim(txtLineaCreSoles.Text) = "0.00" Then
                            txtEstadoSoles.BackColor = Color.White
                        End If
                        If Trim(txtLineaCreDolares.Text) = "0.00" Then
                            txtEstadoDolares.BackColor = Color.White
                        End If
                        If txtLineaCreSoles.Enabled = False Then
                            txtEstadoSoles.Clear()
                        End If
                        If txtLineaCreDolares.Enabled = False Then
                            txtEstadoDolares.Clear()
                        End If
                    Else
                        txtSaldoSoles.Text = "0.00"
                        txtSaldoDolares.Text = "0.00"
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            txtCodigo.Clear()
            txtDireccion.Clear()
            txtSaldoSoles.Text = "0.00"
            txtSaldoDolares.Text = "0.00"
            txtEstadoSoles.Text = "0.00"
            txtEstadoDolares.Text = "0.00"
            txtEstadoSoles.BackColor = Color.Ivory
            txtEstadoDolares.BackColor = Color.Ivory
            If dgvDocumentos.Rows.Count > 0 Then
                m_EditingRow = -1
                For x As Integer = 0 To dgvDocumentos.RowCount - 1
                    dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                Next
            End If
        End If
        txtNroDoc.Text = "0.00"
        txtTotSoles.Text = "0.00"
        txtTotDolares.Text = "0.00"
        cboAnexo.Focus()
        'dgvDocumentos.Focus()
    End Sub

    Private Sub btnProgramar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProgramar.Click
        eRegistroDocumento = New clsRegistroDocumento
        eSesionCajas = New clsSesionCajas
        Dim CodRegDoc, DetrPer, OrdCom, IdDetCro, CCostoCod, IdOrden As String
        Dim IdResumenSoles As String = ""
        Dim IdResumenDolares As String = ""
        Dim Monto As Double
        Dim iResultado1, iResultado2, iResultado3, iEditOrdenCompra As Integer
        Dim CountDocActu As Integer = 0
        Dim CountDocActuSoles As Integer = 0
        Dim CountDocActuDolares As Integer = 0
        Dim iResSoles As Integer = 0
        Dim iResDolares As Integer = 0
        Dim NroDoc, FechaDocumento, FechaVencimiento, DP, MontoDP, OrdCompr, XMarca, IdRegistro, AbrDoc As String
        Dim SimboloMo, Importe, MontoPagar, Saldo, MonedaDes As String
        Dim MntoSolesAnterior, MntoDolaresAnterior As Double
        Dim CantiDocResSoles As Integer = 0
        Dim CantiDocResDolares As Integer = 0
        Dim iResultado4 As Integer
        Dim GlosaAnteriorSoles As String = ""
        Dim GlosaAnteriorDolares As String = ""
        Dim GlosaFac As String = ""
        Dim newGlosaFacSoles As String = ""
        Dim newGlosaFacDolares As String = ""
        If Len(Trim(txtNroSemana.Text)) > 0 And cboTipoAnexo.SelectedIndex > -1 And cboAnexo.SelectedIndex > -1 And CountDocumentos > 0 Then
            'Existe Pago en Soles en esta semana de este proveedor?
            Dim dtTableSoles As DataTable
            dtTableSoles = New DataTable
            If CountCantDocSoles > 0 Then
                dtTableSoles = ePagoProveedores.fTraerResExisteSoles(Trim(cboTipoAnexo.SelectedValue), gEmpresa, Trim(cboAnexo.SelectedValue), Trim(txtNroSemana.Text), gPeriodo, "01")
                iResSoles = dtTableSoles.Rows.Count
            End If
            'Existe Pago en Dolares en esta semana de este proveedor?
            Dim dtTableDolares As DataTable
            dtTableDolares = New DataTable
            If CountCantDocDolares > 0 Then
                dtTableDolares = ePagoProveedores.fTraerResExisteDolares(Trim(cboTipoAnexo.SelectedValue), gEmpresa, Trim(cboAnexo.SelectedValue), Trim(txtNroSemana.Text), gPeriodo, "02")
                iResDolares = dtTableDolares.Rows.Count
            End If
            Dim Mensaje As String = ""

            If iResSoles > 0 Or iResDolares > 0 Then
                If (MessageBox.Show("Ya esta programada la semana " & Trim(txtNroSemana.Text) & " para este Anexo. Se actualizar� la Programaci�n.  �Esta seguro de Continuar con el Proceso?", "Pago de Proveedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    If iResSoles > 0 Then
                        IdResumenSoles = Trim(dtTableSoles.Rows(0).Item("IdResumen"))
                        MntoSolesAnterior = Trim(dtTableSoles.Rows(0).Item("MontoTotal"))
                        CantiDocResSoles = Trim(dtTableSoles.Rows(0).Item("CantidadDoc"))
                        GlosaAnteriorSoles = Trim(dtTableSoles.Rows(0).Item("GlosaNumPago"))
                    End If
                    If iResDolares > 0 Then
                        IdResumenDolares = Trim(dtTableDolares.Rows(0).Item("IdResumen"))
                        MntoDolaresAnterior = Trim(dtTableDolares.Rows(0).Item("MontoTotal"))
                        CantiDocResDolares = Trim(dtTableDolares.Rows(0).Item("CantidadDoc"))
                        GlosaAnteriorDolares = Trim(dtTableDolares.Rows(0).Item("GlosaNumPago"))
                    End If

                    If iResSoles = 0 And CountCantDocSoles > 0 Then 'sino existe resumen soles lo creo
                        Dim IdResumen As String = ""
                        ePagoProveedores.GenerarCodResumen(gEmpresa)
                        IdResumen = ePagoProveedores.sCodFuturo
                        iResultado1 = ePagoProveedores.GrabarResumen(IdResumen, gEmpresa, Trim(cboTipoAnexo.SelectedValue), "01", Trim(cboAnexo.SelectedValue), Convert.ToDouble(txtTotSoles.Text), Val(txtTipoCambio.Text), Trim(txtNroSemana.Text), CountCantDocSoles, strGlosaFacturasSoles, 0, Trim(cboAnexo.Text), sRuc, gPeriodo)
                        If iResultado1 = 1 Then
                            Dim dtTableResumen As DataTable
                            dtTableResumen = New DataTable
                            dtTableSoles = ePagoProveedores.fTraerResExisteSoles(Trim(cboTipoAnexo.SelectedValue), gEmpresa, Trim(cboAnexo.SelectedValue), Trim(txtNroSemana.Text), gPeriodo, "01")
                            If dtTableResumen.Rows.Count > 0 Then
                                IdResumenSoles = Trim(dtTableResumen.Rows(0).Item("IdResumen"))
                                MntoSolesAnterior = Trim(dtTableResumen.Rows(0).Item("MontoTotal"))
                                CantiDocResSoles = Trim(dtTableResumen.Rows(0).Item("CantidadDoc"))
                                GlosaAnteriorSoles = Trim(dtTableResumen.Rows(0).Item("GlosaNumPago"))
                            End If
                        End If
                    End If

                    If iResDolares = 0 And CountCantDocDolares > 0 Then 'sino existe resumen dolares lo creo
                        Dim IdResumen As String = ""
                        ePagoProveedores.GenerarCodResumen(gEmpresa)
                        IdResumen = ePagoProveedores.sCodFuturo
                        iResultado1 = ePagoProveedores.GrabarResumen(IdResumen, gEmpresa, Trim(cboTipoAnexo.SelectedValue), "02", Trim(cboAnexo.SelectedValue), Convert.ToDouble(txtTotDolares.Text), Val(txtTipoCambio.Text), Trim(txtNroSemana.Text), CountCantDocDolares, strGlosaFacturasDolares, 0, Trim(cboAnexo.Text), sRuc, gPeriodo)
                        If iResultado1 = 1 Then
                            Dim dtTableResumen As DataTable
                            dtTableResumen = New DataTable
                            dtTableResumen = ePagoProveedores.fTraerResExisteDolares(Trim(cboTipoAnexo.SelectedValue), gEmpresa, Trim(cboAnexo.SelectedValue), Trim(txtNroSemana.Text), gPeriodo, "02")
                            If dtTableResumen.Rows.Count > 0 Then
                                IdResumenDolares = Trim(dtTableResumen.Rows(0).Item("IdResumen"))
                                MntoDolaresAnterior = Trim(dtTableResumen.Rows(0).Item("MontoTotal"))
                                CantiDocResDolares = Trim(dtTableResumen.Rows(0).Item("CantidadDoc"))
                                GlosaAnteriorDolares = Trim(dtTableResumen.Rows(0).Item("GlosaNumPago"))
                            End If
                        End If
                    End If

                    If dgvDocumentos.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells(13).Value) = True Then
                                CodRegDoc = Trim(dgvDocumentos.Rows(x).Cells(14).Value)
                                DetrPer = Trim(dgvDocumentos.Rows(x).Cells(8).Value)
                                Monto = Val(Trim(dgvDocumentos.Rows(x).Cells(9).Value)) 'PORCENTAJE
                                CCostoCod = eSesionCajas.fCentroCosto(dgvDocumentos.Rows(x).Cells(12).Value) 'CCOSTO
                                OrdCom = Trim(dgvDocumentos.Rows(x).Cells(11).Value)
                                IdOrden = Trim(dgvDocumentos.Rows(x).Cells(19).Value) 'IDORDEN
                                If Len(Trim(IdOrden)) = 0 Then
                                    OrdCom = ""
                                End If
                                If Trim(dgvDocumentos.Rows(x).Cells(20).Value) <> "" Then
                                    iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, gEmpresa)
                                    If Trim(IdOrden) <> Trim(dgvDocumentos.Rows(x).Cells(20).Value) And Len(Trim(dgvDocumentos.Rows(x).Cells(20).Value)) > 0 Then
                                        iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(0, Trim(dgvDocumentos.Rows(x).Cells(20).Value), gEmpresa)
                                    End If
                                End If
                                Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells(6).Value))
                                iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, gEmpresa)

                                If Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "01" Then
                                    If Saldo = 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumenSoles, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                    ElseIf Saldo > 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumenSoles, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                    End If
                                ElseIf Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "02" Then
                                    If Saldo = 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumenDolares, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                    ElseIf Saldo > 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumenDolares, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                    End If
                                End If

                                AbrDoc = Trim(dgvDocumentos.Rows(x).Cells(0).Value)
                                NroDoc = Trim(dgvDocumentos.Rows(x).Cells(1).Value)
                                FechaDocumento = Trim(dgvDocumentos.Rows(x).Cells(2).Value)
                                FechaVencimiento = Trim(dgvDocumentos.Rows(x).Cells(3).Value)
                                SimboloMo = Trim(dgvDocumentos.Rows(x).Cells(4).Value)
                                Importe = Val(Trim(dgvDocumentos.Rows(x).Cells(5).Value))
                                MontoPagar = Val(Trim(dgvDocumentos.Rows(x).Cells(7).Value))
                                Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells(6).Value))
                                DP = Trim(dgvDocumentos.Rows(x).Cells(8).Value)
                                MontoDP = Val(Trim(dgvDocumentos.Rows(x).Cells(10).Value))
                                OrdCompr = Trim(dgvDocumentos.Rows(x).Cells(11).Value)
                                XMarca = 1
                                IdRegistro = Trim(dgvDocumentos.Rows(x).Cells(14).Value)
                                AbrDoc = Trim(dgvDocumentos.Rows(x).Cells(15).Value)
                                MonedaDes = Trim(dgvDocumentos.Rows(x).Cells(16).Value)

                                'If Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "01" Then
                                '  'actualizar saldo prove moneda soles ++++++++++
                                '  Dim IdSaldoProv As String = ""
                                '  Dim MontoSaldoSolesProv As Double = 0
                                '  Dim MontoSaldoSolesNew As Double = 0
                                '  Dim dtTable4 As DataTable
                                '  dtTable4 = New DataTable
                                '  dtTable4 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAnexo.SelectedValue), Trim(cboAnexo.SelectedValue), "01")
                                '  If dtTable4.Rows.Count > 0 Then
                                '    IdSaldoProv = dtTable4.Rows(0).Item("IdSaldo")
                                '    MontoSaldoSolesProv = dtTable4.Rows(0).Item("MontoSaldo")
                                '    MontoSaldoSolesNew = MontoSaldoSolesProv + MontoPagar
                                '    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAnexo.SelectedValue), Trim(cboAnexo.SelectedValue), gEmpresa, MontoSaldoSolesNew, "01")
                                '  End If
                                'ElseIf Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "02" Then
                                '  'actualizar saldo prove moneda dolares ++++++++++
                                '  Dim IdSaldoProv As String = ""
                                '  Dim MontoSaldoDolaresProv As Double = 0
                                '  Dim MontoSaldoDolaresNew As Double = 0
                                '  Dim dtTable5 As DataTable
                                '  dtTable5 = New DataTable
                                '  dtTable5 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipoAnexo.SelectedValue), Trim(cboAnexo.SelectedValue), "02")
                                '  If dtTable5.Rows.Count > 0 Then
                                '    IdSaldoProv = dtTable5.Rows(0).Item("IdSaldo")
                                '    MontoSaldoDolaresProv = dtTable5.Rows(0).Item("MontoSaldo")
                                '    MontoSaldoDolaresNew = MontoSaldoDolaresProv + MontoPagar
                                '    iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipoAnexo.SelectedValue), Trim(cboAnexo.SelectedValue), gEmpresa, MontoSaldoDolaresNew, "02")
                                '  End If
                                'End If

                                If Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "01" Then
                                    'Dim dtTableDocDet As DataTable
                                    'dtTableDocDet = New DataTable
                                    'dtTableDocDet = ePagoProveedores.BuscarDocenProg(8, IdResumenSoles, NroDoc, gEmpresa) 'busca si el doc esta en la programacion
                                    'If dtTableDocDet.Rows.Count = 1 Then
                                    '  Dim MntoPagarAnterior As Double = 0
                                    '  Dim Saldo2 As Double = 0
                                    '  Dim MontoPagar2 As Double = 0
                                    '  Dim IdDetalleCronograma As String = ""
                                    '  IdDetalleCronograma = Trim(dtTableDocDet.Rows(0).Item("IdDetalleCronograma"))
                                    '  MntoPagarAnterior = Trim(dtTableDocDet.Rows(0).Item("MontoPagar"))
                                    '  MontoPagar2 = MontoPagar + MntoPagarAnterior
                                    '  Saldo2 = Importe - MontoPagar2
                                    '  iResultado3 = ePagoProveedores.ActDetalleCro(9, IdDetalleCronograma, IdResumenSoles, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar2, Saldo2, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0)
                                    'Else
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumenSoles, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, gEmpresa)
                                    newGlosaFacSoles = newGlosaFacSoles & " " & AbrDoc & " " & NroDoc & "  "
                                    CountDocActuSoles = CountDocActuSoles + 1
                                    'End If
                                ElseIf Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "02" Then
                                    'Dim dtTableDocDet As DataTable
                                    'dtTableDocDet = New DataTable
                                    'dtTableDocDet = ePagoProveedores.BuscarDocenProg(8, IdResumenDolares, NroDoc, gEmpresa)
                                    'If ePagoProveedores.iNroRegistros = 1 Then
                                    '  Dim MntoPagarAnterior As Double = 0
                                    '  Dim Saldo2 As Double = 0
                                    '  Dim MontoPagar2 As Double = 0
                                    '  Dim IdDetalleCronograma As String = ""
                                    '  IdDetalleCronograma = Trim(dtTableDocDet.Rows(0).Item("IdDetalleCronograma"))
                                    '  MntoPagarAnterior = Trim(dtTableDocDet.Rows(0).Item("MontoPagar"))
                                    '  MontoPagar2 = MontoPagar + MntoPagarAnterior
                                    '  Saldo2 = Importe - MontoPagar2
                                    '  iResultado3 = ePagoProveedores.ActDetalleCro(9, IdDetalleCronograma, IdResumenDolares, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar2, Saldo2, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0)
                                    'Else
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumenDolares, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, gEmpresa)
                                    newGlosaFacDolares = newGlosaFacDolares & " " & AbrDoc & " " & NroDoc & "  "
                                    CountDocActuDolares = CountDocActuSoles + 1
                                    'End If
                                End If

                            End If
                        Next
                        If iResSoles > 0 Then
                            iResultado4 = ePagoProveedores.ActResumen(5, gEmpresa, IdResumenSoles, dblSoles + MntoSolesAnterior, 0, CountDocActuSoles + CantiDocResSoles, GlosaAnteriorSoles & " " & newGlosaFacSoles)
                        End If
                        If iResDolares > 0 Then
                            iResultado4 = ePagoProveedores.ActResumen(5, gEmpresa, IdResumenDolares, dblDolares + MntoDolaresAnterior, 0, CountDocActuDolares + CantiDocResDolares, GlosaAnteriorDolares & " " & newGlosaFacDolares)
                        End If
                        MessageBox.Show("Actualizaci�n de Pagos de la semana " & Trim(txtNroSemana.Text) & " con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        cboAnexo_SelectedIndexChanged(sender, e)
                        txtNroSemana.Clear()
                        CountDocumentos = 0
                    End If
                End If
            ElseIf iResSoles = 0 Or iResDolares = 0 Then
                If dgvDocumentos.Rows.Count > 0 Then
                    If (MessageBox.Show("�Esta seguro de Grabar esta Programaci�n de Pagos?", "Pago de Proveedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                        'SOLES
                        If CountCantDocSoles > 0 Then
                            Dim IdResSoles As String = ""
                            ePagoProveedores.GenerarCodResumen(gEmpresa)
                            IdResSoles = ePagoProveedores.sCodFuturo
                            iResultado1 = ePagoProveedores.GrabarResumen(IdResSoles, gEmpresa, Trim(cboTipoAnexo.SelectedValue), "01", Trim(cboAnexo.SelectedValue), Convert.ToDouble(txtTotSoles.Text), Val(txtTipoCambio.Text), Trim(txtNroSemana.Text), CountCantDocSoles, strGlosaFacturasSoles, 0, Trim(cboAnexo.Text), sRuc, gPeriodo)
                            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells(13).Value) = True Then
                                    CodRegDoc = Trim(dgvDocumentos.Rows(x).Cells(14).Value)
                                    DetrPer = Trim(dgvDocumentos.Rows(x).Cells(8).Value)
                                    Monto = Val(Trim(dgvDocumentos.Rows(x).Cells(9).Value)) 'PORCENTAJE
                                    CCostoCod = eSesionCajas.fCentroCosto(dgvDocumentos.Rows(x).Cells(12).Value) 'CCOSTO
                                    OrdCom = Trim(dgvDocumentos.Rows(x).Cells(11).Value)
                                    IdOrden = Trim(dgvDocumentos.Rows(x).Cells(19).Value) 'IDORDEN
                                    If Len(Trim(IdOrden)) = 0 Then
                                        OrdCom = ""
                                    End If
                                    If Trim(dgvDocumentos.Rows(x).Cells(20).Value) <> "" Then
                                        iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, gEmpresa)
                                        If Trim(IdOrden) <> Trim(dgvDocumentos.Rows(x).Cells(20).Value) And Len(Trim(dgvDocumentos.Rows(x).Cells(20).Value)) > 0 Then
                                            iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(0, Trim(dgvDocumentos.Rows(x).Cells(20).Value), gEmpresa)
                                        End If
                                    End If
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells(6).Value))
                                    iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, gEmpresa)
                                    If Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "01" Then
                                        If Saldo = 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResSoles, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResSoles, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                        End If
                                    End If
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    AbrDoc = Trim(dgvDocumentos.Rows(x).Cells(0).Value)
                                    NroDoc = Trim(dgvDocumentos.Rows(x).Cells(1).Value)
                                    FechaDocumento = Trim(dgvDocumentos.Rows(x).Cells(2).Value)
                                    FechaVencimiento = Trim(dgvDocumentos.Rows(x).Cells(3).Value)
                                    SimboloMo = Trim(dgvDocumentos.Rows(x).Cells(4).Value)
                                    Importe = Val(Trim(dgvDocumentos.Rows(x).Cells(5).Value))
                                    MontoPagar = Val(Trim(dgvDocumentos.Rows(x).Cells(7).Value))
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells(6).Value))
                                    DP = Trim(dgvDocumentos.Rows(x).Cells(8).Value)
                                    MontoDP = Val(Trim(dgvDocumentos.Rows(x).Cells(10).Value))
                                    OrdCompr = Trim(dgvDocumentos.Rows(x).Cells(11).Value)
                                    XMarca = 1
                                    IdRegistro = Trim(dgvDocumentos.Rows(x).Cells(14).Value)
                                    AbrDoc = Trim(dgvDocumentos.Rows(x).Cells(15).Value)
                                    MonedaDes = Trim(dgvDocumentos.Rows(x).Cells(16).Value)
                                    If Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "01" Then
                                        iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResSoles, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, gEmpresa)
                                    End If
                                End If
                            Next
                        End If
                        'DOLARES
                        If CountCantDocDolares > 0 Then
                            Dim IdResDolares As String = ""
                            ePagoProveedores.GenerarCodResumen(gEmpresa)
                            IdResDolares = ePagoProveedores.sCodFuturo
                            iResultado1 = ePagoProveedores.GrabarResumen(IdResDolares, gEmpresa, Trim(cboTipoAnexo.SelectedValue), "02", Trim(cboAnexo.SelectedValue), Convert.ToDouble(txtTotDolares.Text), Val(txtTipoCambio.Text), Trim(txtNroSemana.Text), CountCantDocDolares, strGlosaFacturasDolares, 0, Trim(cboAnexo.Text), sRuc, gPeriodo)
                            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells(13).Value) = True Then
                                    CodRegDoc = Trim(dgvDocumentos.Rows(x).Cells(14).Value)
                                    DetrPer = Trim(dgvDocumentos.Rows(x).Cells(8).Value)
                                    'MessageBox.Show(dgvDocumentos.Rows(x).Cells(12).Value)
                                    Monto = Val(Trim(dgvDocumentos.Rows(x).Cells(9).Value)) 'PORCENTAJE
                                    CCostoCod = eSesionCajas.fCentroCosto(dgvDocumentos.Rows(x).Cells(12).Value) 'CCOSTO
                                    'eSesionCajas.fCentroCosto(dgvDetalleCajas.Rows(x).Cells(i).EditedFormattedValue)
                                    OrdCom = Trim(dgvDocumentos.Rows(x).Cells(11).Value)
                                    IdOrden = Trim(dgvDocumentos.Rows(x).Cells(19).Value) 'IDORDEN
                                    If Len(Trim(IdOrden)) = 0 Then
                                        OrdCom = ""
                                    End If
                                    If Trim(dgvDocumentos.Rows(x).Cells(20).Value) <> "" Then
                                        iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, gEmpresa)
                                        If Trim(IdOrden) <> Trim(dgvDocumentos.Rows(x).Cells(20).Value) And Len(Trim(dgvDocumentos.Rows(x).Cells(20).Value)) > 0 Then
                                            iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(0, Trim(dgvDocumentos.Rows(x).Cells(20).Value), gEmpresa)
                                        End If
                                    End If
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells(6).Value))
                                    iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, gEmpresa)
                                    If Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "02" Then
                                        If Saldo = 0 Then
                                            'AQUI SI EL DOCUMENTO LLEGA A CANCELARSE POR COMPLETO
                                            'ESTE YA NO SE MOSTRARA MAS PARA LAS PROGRAMACIONES
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResDolares, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                        ElseIf Saldo > 0 Then
                                            'AQUI SI EL DOCUMENTO AUN CONSERVA SALDO
                                            'AUN SE MOSTRARA MAS PARA LAS PROGRAMACIONES
                                            ' SE ACTUALIZA EL SALDO DEL DOC
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, gEmpresa, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResDolares, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                        End If
                                    End If
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa) 'GENERA ID DETALLE DE PROGRAMACION POR EMPRESA
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    AbrDoc = Trim(dgvDocumentos.Rows(x).Cells(0).Value)
                                    NroDoc = Trim(dgvDocumentos.Rows(x).Cells(1).Value)
                                    FechaDocumento = Trim(dgvDocumentos.Rows(x).Cells(2).Value)
                                    FechaVencimiento = Trim(dgvDocumentos.Rows(x).Cells(3).Value)
                                    SimboloMo = Trim(dgvDocumentos.Rows(x).Cells(4).Value)
                                    Importe = Val(Trim(dgvDocumentos.Rows(x).Cells(5).Value))
                                    MontoPagar = Val(Trim(dgvDocumentos.Rows(x).Cells(7).Value))
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells(6).Value))
                                    DP = Trim(dgvDocumentos.Rows(x).Cells(8).Value)
                                    MontoDP = Val(Trim(dgvDocumentos.Rows(x).Cells(10).Value))
                                    OrdCompr = Trim(dgvDocumentos.Rows(x).Cells(11).Value)
                                    XMarca = 1
                                    IdRegistro = Trim(dgvDocumentos.Rows(x).Cells(14).Value)
                                    AbrDoc = Trim(dgvDocumentos.Rows(x).Cells(15).Value)
                                    MonedaDes = Trim(dgvDocumentos.Rows(x).Cells(16).Value)
                                    If Trim(dgvDocumentos.Rows(x).Cells(17).Value) = "02" Then
                                        'AQUI DEBO GRABAR A LA TABLA DE MIKI PARA QUE EL DOC SE
                                        'AQUI DEBO APROBAR EL DOC Y MIKI LO CONTABILICE Y UNA VEZ
                                        'CONTABILIZADO SE PUEDE USAR PARA PAGAR
                                        iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResDolares, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, gEmpresa)
                                    End If
                                End If
                            Next
                        End If
                        If iResultado1 = 1 And iResultado2 = 1 And iResultado3 = 1 Then
                            MessageBox.Show("Programacion de Pagos con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            cboAnexo_SelectedIndexChanged(sender, e)
                            txtNroSemana.Clear()
                            CountDocumentos = 0
                        End If
                    End If
                Else
                    MessageBox.Show("No Existen Documentos para la Programacion de Pagos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Else
            If cboTipoAnexo.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Tipo de Anexo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipoAnexo.Focus()
                Exit Sub
            End If
            If cboAnexo.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Anexo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboAnexo.Focus()
                Exit Sub
            End If
            If Len(Trim(txtNroSemana.Text)) = 0 Then
                MessageBox.Show("Ingrese el Numero de Semana", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtNroSemana.Focus()
                Exit Sub
            End If
            If dgvDocumentos.Rows.Count = 0 Then
                MessageBox.Show("No Existen Documentos para la Programacion de Pagos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            If CountDocumentos = 0 Then
                MessageBox.Show("No se ha Seleccionado Documentos Para la Programacion de Pagos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvDocumentos.Focus()
                Exit Sub
            End If
        End If
    End Sub

    Private Sub cboTipoAnexo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoAnexo.SelectedIndexChanged
        'Dim IdTipoAnexo As String = ""
        If cboTipoAnexo.SelectedIndex > -1 Then
            '    Try
            '        IdTipoAnexo = Trim(cboTipoAnexo.SelectedValue.ToString)
            '        If IdTipoAnexo <> "System.Data.DataRowView" Then
            '            cboAnexo.DataSource = ePagoProveedores.fListarProveedoresPorTipo(gEmpresa, Trim(IdTipoAnexo))
            '            cboAnexo.ValueMember = "CodProveedor"
            '            cboAnexo.DisplayMember = "NombreProveedor"
            '            cboAnexo.SelectedIndex = -1
            '            If ePagoProveedores.iNroRegistros = 0 Then
            '                txtDireccion.Text = ""
            '                txtCodigo.Text = ""
            '                txtSaldoSoles.Text = ""
            '                txtSaldoDolares.Text = ""
            '                txtNroDoc.Text = ""
            '                txtTotSoles.Text = ""
            '                txtTotDolares.Text = ""
            '                For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
            '                    dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
            '                Next
            '            End If
            '        End If
            '    Catch ex As System.IndexOutOfRangeException
            '        Exit Sub
            '    End Try
        Else
            cboAnexo.DataSource = Nothing
        End If
        'cboTipoAnexo.Focus()
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        If TabControl1.TabPages(1).Controls.Owner.Focus = True Then
            ePagoProveedores = New clsPagoProveedores
            Try
                cboResumenes.DataSource = ePagoProveedores.fListarSemanas(gEmpresa, gPeriodo)
                cboResumenes.ValueMember = "NroSemana"
                cboResumenes.DisplayMember = "Etiqueta"
                cboResumenes.SelectedIndex = -1
                cboResumenes.Focus()
            Catch ex As Exception
                Exit Sub
            End Try
        ElseIf TabControl1.TabPages(2).Controls.Owner.Focus = True Then
            'RadioButton1.Checked = True
            cboTipoPago.SelectedIndex = -1
            txtCadenaBusqueda.Text = ""
            CheckBox1.Checked = False
            RadioButton1_Click(sender, e)
            txtCadenaBusqueda_TextChanged(sender, e)
            txtCadenaBusqueda.Focus()
            'lblIngrese.Text = ""
        ElseIf TabControl1.TabPages(3).Controls.Owner.Focus = True Then
            rdbRuc.Checked = True
            chkDocCance.Checked = True
            chkDocSinCance.Checked = False
            txtBuscarEstadoDoc.Text = ""
            txtBuscarEstadoDoc_TextChanged(sender, e)
            txtBuscarEstadoDoc.Focus()
        Else
            cboTipoAnexo.SelectedIndex = -1
            cboTipoAnexo.Focus()
        End If
    End Sub


    Private Sub cboTipAnexos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipAnexos.SelectedIndexChanged
        Dim ImporteDocSoles As Double = 0
        Dim ImporteDocDolares As Double = 0
        Dim SaldoDocSoles As Double = 0
        Dim SaldoDocDolares As Double = 0
        Dim PagarDocSoles As Double = 0
        Dim PagarDocDolares As Double = 0

        Dim dtDetalleCronograma As DataTable
        Dim x As Integer
        Dim y As Integer
        Dim z As Integer
        Dim NroSemana As String = ""
        Dim Anio As String = ""
        Dim IdTipoAnexo As String = ""
        If cboResumenes.SelectedIndex > -1 And cboTipAnexos.SelectedIndex > -1 Then
            Try
                NroSemana = Trim(cboResumenes.SelectedValue.ToString)
                'Anio = Mid(Trim(cboResumenes.Text), 12, 16)
                Anio = vb.Right(Trim(cboResumenes.Text), 4)
                IdTipoAnexo = Trim(cboTipAnexos.SelectedValue.ToString)
                If NroSemana <> "System.Data.DataRowView" And IdTipoAnexo <> "System.Data.DataRowView" Then

                    cboAnexos.DataSource = ePagoProveedores.fLisAnexosxSemanayTipAnex(Trim(IdTipoAnexo), Trim(NroSemana), Trim(Anio), gEmpresa)
                    cboAnexos.ValueMember = "IdAnexo"
                    cboAnexos.DisplayMember = "NombreAnexo"
                    cboAnexos.SelectedIndex = -1

                    If dgvDetalle.Rows.Count > 0 Then
                        m_EditingRow2 = -1
                        For x = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                    End If

                    'For x = 0 To dgvDetalle.RowCount - 1
                    '    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    'Next

                    dtDetalleCronograma = New DataTable
                    dtDetalleCronograma = ePagoProveedores.fLisDetCronogramas3(Trim(IdTipoAnexo), Trim(NroSemana), Trim(Anio), gEmpresa)
                    If dtDetalleCronograma.Rows.Count > 0 Then
                        cargarCentrosDeCosto2()
                        For y = 0 To dtDetalleCronograma.Rows.Count - 1
                            dgvDetalle.Rows.Add()
                            For z = 0 To dtDetalleCronograma.Columns.Count - 1
                                Dim StrCadCodigo As String = String.Empty

                                If z = 5 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        ImporteDocSoles = ImporteDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        ImporteDocDolares = ImporteDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 6 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        SaldoDocSoles = SaldoDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        SaldoDocDolares = SaldoDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 7 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        PagarDocSoles = PagarDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        PagarDocDolares = PagarDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 11 Then
                                    For a As Integer = 0 To IdCC2.Items.Count - 1
                                        Dim cad As String = IdCC2.Items.Item(a)
                                        StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                                        If Trim(dtDetalleCronograma.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                                            dgvDetalle.Rows(y).Cells(z).Value = IdCC2.Items.Item(a)
                                        End If
                                    Next
                                ElseIf z = 12 Then
                                    If dtDetalleCronograma.Rows(y).Item(11).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = True
                                    ElseIf dtDetalleCronograma.Rows(y).Item(1).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = False
                                    End If
                                    If dtDetalleCronograma.Rows(y).Item(15).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(20).ReadOnly = True
                                        gestionaResaltados(dgvDetalle, y, Color.Orange)
                                    ElseIf dtDetalleCronograma.Rows(y).Item(15).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = False
                                    End If
                                ElseIf z = 13 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(12)
                                ElseIf z = 14 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(13)
                                ElseIf z = 15 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(17)
                                ElseIf z = 16 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(18)
                                Else ' 0 1 2 3 4 8 9 10 12 13 14 15
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                End If

                                'If z = 11 Then
                                '    If dtDetalleCronograma.Rows(y).Item(11).ToString = "1" Then
                                '        dgvDetalle.Rows(y).Cells(z).Value = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(11).ToString = "0" Then
                                '        dgvDetalle.Rows(y).Cells(z).Value = False
                                '    End If
                                '    If dtDetalleCronograma.Rows(y).Item(16).ToString = "1" Then
                                '        dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '        gestionaResaltados(dgvDetalle, y, Color.Orange)
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(16).ToString = "0" Then
                                '        dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 2 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtDetalleCronograma.Rows(y).Item(z), 10)
                                'ElseIf z = 3 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtDetalleCronograma.Rows(y).Item(z), 10)
                                'ElseIf z = 5 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        ImporteDocSoles = ImporteDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        ImporteDocDolares = ImporteDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'ElseIf z = 6 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        SaldoDocSoles = SaldoDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        SaldoDocDolares = SaldoDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'ElseIf z = 7 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        PagarDocSoles = PagarDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        PagarDocDolares = PagarDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(16).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(16).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 8 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(16).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(16).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 9 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(16).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(16).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 10 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(16).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(16).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'Else
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'End If

                            Next
                        Next

                        txtImporteSoles.Text = Format(ImporteDocSoles, "#,##0.00")
                        txtImporteDolares.Text = Format(ImporteDocDolares, "#,##0.00")
                        txtSalSoles.Text = Format(SaldoDocSoles, "#,##0.00")
                        txtSalDolares.Text = Format(SaldoDocDolares, "#,##0.00")
                        txtPagarSol.Text = Format(PagarDocSoles, "#,##0.00")
                        txtPagarDol.Text = Format(PagarDocDolares, "#,##0.00")

                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            If dgvDetalle.Rows.Count > 0 Then
                m_EditingRow2 = -1
                For x = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
            End If
            DesHabilitarLetra()
        End If

    End Sub

    Private Sub cboAnexos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAnexos.SelectedIndexChanged

        Dim ImporteDocSoles As Double = 0
        Dim ImporteDocDolares As Double = 0
        Dim SaldoDocSoles As Double = 0
        Dim SaldoDocDolares As Double = 0
        Dim PagarDocSoles As Double = 0
        Dim PagarDocDolares As Double = 0

        Dim dtDetalleCronograma As DataTable
        Dim x As Integer
        Dim y As Integer
        Dim z As Integer
        Dim NroSemana As String = ""
        Dim Anio As String = ""
        Dim IdTipoAnexo As String = ""
        Dim IdAnexo As String = ""
        If cboResumenes.SelectedIndex > -1 And cboTipAnexos.SelectedIndex > -1 And cboAnexos.SelectedIndex > -1 Then
            Try
                NroSemana = Trim(cboResumenes.SelectedValue.ToString)
                'Anio = Mid(Trim(cboResumenes.Text), 12, 16)
                Anio = vb.Right(Trim(cboResumenes.Text), 4)
                IdTipoAnexo = Trim(cboTipAnexos.SelectedValue.ToString)
                IdAnexo = Trim(cboAnexos.SelectedValue.ToString)
                If NroSemana <> "System.Data.DataRowView" And IdTipoAnexo <> "System.Data.DataRowView" And IdAnexo <> "System.Data.DataRowView" Then
                    cboMoneda.SelectedIndex = -1
                    'For x = 0 To dgvDetalle.RowCount - 1
                    '    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    'Next
                    If dgvDetalle.Rows.Count > 0 Then
                        m_EditingRow2 = -1
                        For x = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                    End If

                    dtDetalleCronograma = New DataTable
                    dtDetalleCronograma = ePagoProveedores.fLisDetCronogramas4(Trim(IdTipoAnexo), Trim(IdAnexo), Trim(NroSemana), Trim(Anio), gEmpresa)
                    If dtDetalleCronograma.Rows.Count > 0 Then
                        'btnActualizar.Enabled = True
                        cargarCentrosDeCosto2()
                        For y = 0 To dtDetalleCronograma.Rows.Count - 1
                            dgvDetalle.Rows.Add()
                            For z = 0 To dtDetalleCronograma.Columns.Count - 1
                                Dim StrCadCodigo As String = String.Empty

                                If z = 5 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        ImporteDocSoles = ImporteDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        ImporteDocDolares = ImporteDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 6 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        SaldoDocSoles = SaldoDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        SaldoDocDolares = SaldoDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 7 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        PagarDocSoles = PagarDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        PagarDocDolares = PagarDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 11 Then
                                    For a As Integer = 0 To IdCC2.Items.Count - 1
                                        Dim cad As String = IdCC2.Items.Item(a)
                                        StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                                        If Trim(dtDetalleCronograma.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                                            dgvDetalle.Rows(y).Cells(z).Value = IdCC2.Items.Item(a)
                                        End If
                                    Next
                                ElseIf z = 12 Then
                                    If dtDetalleCronograma.Rows(y).Item(11).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = True
                                    ElseIf dtDetalleCronograma.Rows(y).Item(1).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = False
                                    End If
                                    If dtDetalleCronograma.Rows(y).Item(15).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(20).ReadOnly = True
                                        gestionaResaltados(dgvDetalle, y, Color.Orange)
                                    ElseIf dtDetalleCronograma.Rows(y).Item(15).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = False
                                    End If
                                ElseIf z = 13 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(12)
                                ElseIf z = 14 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(13)
                                ElseIf z = 15 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(17)
                                ElseIf z = 16 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(18)
                                Else ' 0 1 2 3 4 8 9 10 12 13 14 15
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                End If

                                'If z = 11 Then
                                '    ''Dim aa As String
                                '    ''aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    If dtDetalleCronograma.Rows(y).Item(z).ToString = "1" Then
                                '        dgvDetalle.Rows(y).Cells(z).Value = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(z).ToString = "0" Then
                                '        dgvDetalle.Rows(y).Cells(z).Value = False
                                '    End If
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '        gestionaResaltados(dgvDetalle, y, Color.Orange)
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 2 Then
                                '    'Dim aa As String
                                '    'aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    dgvDetalle.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtDetalleCronograma.Rows(y).Item(z), 10)
                                'ElseIf z = 3 Then
                                '    'Dim aa As String
                                '    'aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    dgvDetalle.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtDetalleCronograma.Rows(y).Item(z), 10)

                                'ElseIf z = 5 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        ImporteDocSoles = ImporteDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        ImporteDocDolares = ImporteDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'ElseIf z = 6 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        SaldoDocSoles = SaldoDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        SaldoDocDolares = SaldoDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'ElseIf z = 7 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        PagarDocSoles = PagarDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        PagarDocDolares = PagarDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 8 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 9 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 10 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'Else
                                '    'Dim aa As String
                                '    'aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'End If

                            Next
                        Next

                        txtImporteSoles.Text = Format(ImporteDocSoles, "#,##0.00")
                        txtImporteDolares.Text = Format(ImporteDocDolares, "#,##0.00")
                        txtSalSoles.Text = Format(SaldoDocSoles, "#,##0.00")
                        txtSalDolares.Text = Format(SaldoDocDolares, "#,##0.00")
                        txtPagarSol.Text = Format(PagarDocSoles, "#,##0.00")
                        txtPagarDol.Text = Format(PagarDocDolares, "#,##0.00")

                        'Else
                        'btnActualizar.Enabled = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            btnActualizar.Enabled = False
            btnLetra.Enabled = False
            If dgvDetalle.Rows.Count > 0 Then
                m_EditingRow2 = -1
                For x = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
            End If
            DesHabilitarLetra()
        End If

    End Sub


    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(1).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(2).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(3).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(4).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(5).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(6).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(7).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(8).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(9).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(10).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(11).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(12).Style.BackColor = Color.ForestGreen
        visor.Rows(fila).Cells(20).Style.BackColor = Color.ForestGreen
    End Sub


    Private Sub cboResumenes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboResumenes.SelectedIndexChanged

        Dim ImporteDocSoles As Double = 0
        Dim ImporteDocDolares As Double = 0
        Dim SaldoDocSoles As Double = 0
        Dim SaldoDocDolares As Double = 0
        Dim PagarDocSoles As Double = 0
        Dim PagarDocDolares As Double = 0

        Dim dtDetalleCronograma As DataTable
        Dim x As Integer
        Dim y As Integer
        Dim z As Integer
        Dim NroSemana As String = ""
        Dim Anio As String = ""
        If cboResumenes.SelectedIndex > -1 Then
            Try
                NroSemana = Trim(cboResumenes.SelectedValue.ToString)
                'Anio = Mid(Trim(cboResumenes.Text), 12, 16)
                Anio = vb.Right(Trim(cboResumenes.Text), 4)
                'Dim aaa As String = ""
                'aaa = vb.Right(Trim(cboResumenes.Text), 4)
                If NroSemana <> "System.Data.DataRowView" Then
                    cboTipAnexos.DataSource = ePagoProveedores.fLisTipoAnexosxSemana(Trim(NroSemana), Trim(Anio), gEmpresa)

                    cboTipAnexos.ValueMember = "IdTipoAnexo"
                    cboTipAnexos.DisplayMember = "TipoAnaliticoDescripcion"
                    cboTipAnexos.SelectedIndex = -1

                    If dgvDetalle.Rows.Count > 0 Then
                        m_EditingRow2 = -1
                        For x = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                    End If

                    'For x = 0 To dgvDetalle.RowCount - 1
                    '    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                    'Next

                    dtDetalleCronograma = New DataTable
                    dtDetalleCronograma = ePagoProveedores.fLisDetCronogramas2(Trim(NroSemana), Trim(Anio), gEmpresa)
                    If dtDetalleCronograma.Rows.Count > 0 Then
                        cargarCentrosDeCosto2()
                        For y = 0 To dtDetalleCronograma.Rows.Count - 1
                            dgvDetalle.Rows.Add()
                            For z = 0 To dtDetalleCronograma.Columns.Count - 1
                                Dim StrCadCodigo As String = String.Empty

                                If z = 5 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        ImporteDocSoles = ImporteDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        ImporteDocDolares = ImporteDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 6 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        SaldoDocSoles = SaldoDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        SaldoDocDolares = SaldoDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 7 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        PagarDocSoles = PagarDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        PagarDocDolares = PagarDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 11 Then
                                    For a As Integer = 0 To IdCC2.Items.Count - 1
                                        Dim cad As String = IdCC2.Items.Item(a)
                                        StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                                        If Trim(dtDetalleCronograma.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                                            dgvDetalle.Rows(y).Cells(z).Value = IdCC2.Items.Item(a)
                                        End If
                                    Next
                                ElseIf z = 12 Then
                                    If dtDetalleCronograma.Rows(y).Item(11).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = True
                                    ElseIf dtDetalleCronograma.Rows(y).Item(1).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = False
                                    End If
                                    If dtDetalleCronograma.Rows(y).Item(15).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(20).ReadOnly = True
                                        gestionaResaltados(dgvDetalle, y, Color.Orange)
                                    ElseIf dtDetalleCronograma.Rows(y).Item(15).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = False
                                    End If
                                ElseIf z = 13 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(12)
                                ElseIf z = 14 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(13)
                                ElseIf z = 15 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(17)
                                ElseIf z = 16 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(18)
                                Else ' 0 1 2 3 4 8 9 10 12 13 14 15
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                End If
                            Next
                        Next

                        txtImporteSoles.Text = Format(ImporteDocSoles, "#,##0.00")
                        txtImporteDolares.Text = Format(ImporteDocDolares, "#,##0.00")
                        txtSalSoles.Text = Format(SaldoDocSoles, "#,##0.00")
                        txtSalDolares.Text = Format(SaldoDocDolares, "#,##0.00")
                        txtPagarSol.Text = Format(PagarDocSoles, "#,##0.00")
                        txtPagarDol.Text = Format(PagarDocDolares, "#,##0.00")

                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            If dgvDetalle.Rows.Count > 0 Then
                m_EditingRow2 = -1
                For x = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
            End If
            DesHabilitarLetra()
            
        End If
    End Sub

    Private Sub btnActualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActualizar.Click
        eMovimientoCajaBanco = New clsMovimientoCajaBanco
        eRegistroDocumento = New clsRegistroDocumento
        eGastosGenerales = New clsGastosGenerales
        eSesionCajas = New clsSesionCajas
        Dim CodRegDoc, IdDetCro As String
        Dim strGlosaFacturas2 As String = ""
        Dim iEditOrdenCompra As Integer = 0
        Dim IdResumenCro As String = ""
        Dim IdOrdenCompra As String = ""
        Dim CCostoCod As String = ""
        Dim iResultado2, iResultado3, iResultado4, iResultado5 As Integer
        Dim CountDocActu As Integer = 0
        Dim iRes As Integer = 0
        Dim DP, MontoDP, OrdCompr, IdRegistro, AbrDoc As String
        Dim MontoSoles As Double = 0
        Dim MontoDolares As Double = 0
        Dim MntoPagar As Double = 0

        Dim Saldo As Double = 0
        Dim SaldoaAgregar As Double = 0
        Dim SaldoDocumento As Double = 0
        Dim ActSaldo As Double = 0
        Dim MontoTotal As Double = 0

        If cboTipAnexos.SelectedIndex > -1 And cboAnexos.SelectedIndex > -1 And cboResumenes.SelectedIndex > -1 And cboMoneda.SelectedIndex > -1 And dgvDetalle.Rows.Count > 0 Then
            If (MessageBox.Show("�Esta seguro de Actualizar esta Programaci�n de Pagos?", "Pago de Proveedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                    If Convert.ToBoolean(dgvDetalle.Rows(x).Cells(12).Value) = True Then

                        ''If Trim(dgvDetalle.Rows(x).Cells(15).Value) = "SOLES" Then
                        'If Trim(cboMoneda.SelectedValue) = "01" Then
                        '    Dim StrCad As String = String.Empty
                        '    StrCad = Trim(dgvDetalle.Rows(x).Cells(7).Value)
                        '    If StrCad = Nothing Then
                        '        MontoSoles = MontoSoles + 0
                        '    Else
                        '        MontoSoles = MontoSoles + dgvDetalle.Rows(x).Cells(7).Value
                        '    End If
                        '    MontoTotal = MontoSoles
                        '    'ElseIf Trim(dgvDetalle.Rows(x).Cells(15).Value) = "DOLARES" Then
                        'ElseIf Trim(cboMoneda.SelectedValue) = "02" Then
                        '    Dim StrCad As String = String.Empty
                        '    StrCad = Trim(dgvDetalle.Rows(x).Cells(7).Value)
                        '    If StrCad = Nothing Then
                        '        MontoDolares = MontoDolares + 0
                        '    Else
                        '        MontoDolares = MontoDolares + dgvDetalle.Rows(x).Cells(7).Value
                        '    End If
                        '    MontoTotal = MontoDolares
                        'End If

                        CountDocActu = CountDocActu + 1

                        strGlosaFacturas2 = strGlosaFacturas2 & " " & dgvDetalle.Rows(x).Cells(0).Value & " " & dgvDetalle.Rows(x).Cells(1).Value & "  "

                        MntoPagar = Val(Trim(dgvDetalle.Rows(x).Cells(7).Value))
                        Saldo = Val(Trim(dgvDetalle.Rows(x).Cells(6).Value))
                        DP = Trim(dgvDetalle.Rows(x).Cells(8).Value)
                        MontoDP = Val(Trim(dgvDetalle.Rows(x).Cells(9).Value))
                        OrdCompr = Trim(dgvDetalle.Rows(x).Cells(10).Value)

                        IdRegistro = Trim(dgvDetalle.Rows(x).Cells(13).Value)
                        AbrDoc = Trim(dgvDetalle.Rows(x).Cells(14).Value)
                        IdDetCro = Trim(dgvDetalle.Rows(x).Cells(15).Value)
                        IdResumenCro = Trim(dgvDetalle.Rows(x).Cells(16).Value)
                        CCostoCod = eSesionCajas.fCentroCosto(dgvDetalle.Rows(x).Cells(11).Value) 'CCOSTO

                        'Dim MontoActSaldo As Double = 0
                        'Dim MntoPagar2 As Double = 0
                        'MntoPagar2 = 0 ' Trim(dgvDetalle.Rows(x).Cells(18).Value)
                        'If MntoPagar > MntoPagar Then
                        '    MontoActSaldo = MntoPagar - MntoPagar2
                        'ElseIf MntoPagar = MntoPagar2 Then
                        '    MontoActSaldo = 0
                        'ElseIf MntoPagar < MntoPagar2 Then
                        '    MontoActSaldo = MntoPagar - MntoPagar2
                        'End If

                        'XMarca = 1
                        'iResultado1 = ePagoProveedores.ActualizarDetalleCro(2, IdDetCro, IdResumenCro, "", "", Today(), Today(), MntoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, AbrDoc, gEmpresa, 0)
                        'If Saldo = 0 Then
                        'iResultado5 = ePagoProveedores.ActualizarDocumento(6, gEmpresa, IdRegistro, 1, 1, Saldo)
                        'ElseIf Saldo > 0 Then
                        iResultado5 = ePagoProveedores.ActualizarDocumento(6, gEmpresa, IdRegistro, 0, 0, Saldo, Trim(CCostoCod))
                        'End If

                        'If Trim(cboMoneda.SelectedValue) = "01" Then
                        '    'actualizar saldo prove moneda soles ++++++++++
                        '    Dim IdSaldoProv As String = ""
                        '    Dim MontoSaldoSolesProv As Double = 0
                        '    Dim MontoSaldoSolesNew As Double = 0
                        '    Dim dtTable4 As DataTable
                        '    dtTable4 = New DataTable
                        '    dtTable4 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipAnexos.SelectedValue), Trim(cboAnexos.SelectedValue), "01")
                        '    If dtTable4.Rows.Count > 0 Then
                        '        IdSaldoProv = dtTable4.Rows(0).Item("IdSaldo")
                        '        MontoSaldoSolesProv = dtTable4.Rows(0).Item("MontoSaldo")
                        '        MontoSaldoSolesNew = MontoSaldoSolesProv + MontoActSaldo
                        '        iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipAnexos.SelectedValue), Trim(cboAnexos.SelectedValue), gEmpresa, MontoSaldoSolesNew, "01")
                        '    End If
                        'ElseIf Trim(cboMoneda.SelectedValue) = "02" Then
                        '    'actualizar saldo prove moneda dolares ++++++++++
                        '    Dim IdSaldoProv As String = ""
                        '    Dim MontoSaldoDolaresProv As Double = 0
                        '    Dim MontoSaldoDolaresNew As Double = 0
                        '    Dim dtTable5 As DataTable
                        '    dtTable5 = New DataTable
                        '    dtTable5 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(cboTipAnexos.SelectedValue), Trim(cboAnexos.SelectedValue), "02")
                        '    If dtTable5.Rows.Count > 0 Then
                        '        IdSaldoProv = dtTable5.Rows(0).Item("IdSaldo")
                        '        MontoSaldoDolaresProv = dtTable5.Rows(0).Item("MontoSaldo")
                        '        MontoSaldoDolaresNew = MontoSaldoDolaresProv + MontoActSaldo
                        '        iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(cboTipAnexos.SelectedValue), Trim(cboAnexos.SelectedValue), gEmpresa, MontoSaldoDolaresNew, "02")
                        '    End If
                        'End If

                    Else

                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        If Trim(cboMoneda.SelectedValue) = "01" Then
                            Dim StrCad As String = String.Empty
                            StrCad = Trim(dgvDetalle.Rows(x).Cells(7).Value)
                            If StrCad = Nothing Then
                                MontoSoles = MontoSoles + 0
                            Else
                                MontoSoles = MontoSoles + dgvDetalle.Rows(x).Cells(7).Value
                            End If
                            MontoTotal = MontoSoles
                        ElseIf Trim(cboMoneda.SelectedValue) = "02" Then
                            Dim StrCad As String = String.Empty
                            StrCad = Trim(dgvDetalle.Rows(x).Cells(7).Value)
                            If StrCad = Nothing Then
                                MontoDolares = MontoDolares + 0
                            Else
                                MontoDolares = MontoDolares + dgvDetalle.Rows(x).Cells(7).Value
                            End If
                            MontoTotal = MontoDolares
                        End If
                        CountDocActu = CountDocActu + 1
                        strGlosaFacturas2 = strGlosaFacturas2 & " " & dgvDetalle.Rows(x).Cells(0).Value & " " & dgvDetalle.Rows(x).Cells(1).Value & "  "
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        'Dim APagarDoc As String
                        CodRegDoc = Trim(dgvDetalle.Rows(x).Cells(13).Value)
                        SaldoaAgregar = Val(Trim(dgvDetalle.Rows(x).Cells(7).Value))
                        'APagarDoc = Val(Trim(dgvDetalle.Rows(x).Cells(7).Value))
                        Dim dtTable As DataTable
                        dtTable = New DataTable
                        dtTable = ePagoProveedores.fTraerSaldoDocumento(7, gEmpresa, CodRegDoc)
                        If dtTable.Rows.Count > 0 Then
                            SaldoDocumento = dtTable.Rows(0).Item("Saldo")
                        End If
                        ActSaldo = SaldoDocumento + SaldoaAgregar
                        iResultado2 = ePagoProveedores.ActualizarDoc(3, gEmpresa, CodRegDoc, ActSaldo)
                        'eMovimientoCajaBanco.ActSaldoDoc(23, CodRegDoc, gEmpresa, ActSaldo)
                        IdDetCro = Trim(dgvDetalle.Rows(x).Cells(15).Value)
                        IdResumenCro = Trim(dgvDetalle.Rows(x).Cells(16).Value)
                        iResultado3 = ePagoProveedores.EliminarDetalleCro(4, IdDetCro, IdResumenCro, gEmpresa)
                        IdOrdenCompra = Trim(dgvDetalle.Rows(x).Cells(19).Value)
                        iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(0, Trim(IdOrdenCompra), gEmpresa)
                        'iResultado3 = ePagoProveedores.EliminarDetalleCro(4, IdDetCro, IdResumenCro, gEmpresa)
                    End If
                Next


                strGlosaFacturas2 = "Pago a Proveedor, Doc: " & strGlosaFacturas2

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim MontoSaldoaPagar As String = 0
                If Len(Trim(IdResumenCro)) > 0 Then
                    Dim dtMontoResumen As DataTable
                    Dim SaldoResumen As String
                    dtMontoResumen = New DataTable
                    dtMontoResumen = eGastosGenerales.fMontoResumen(Trim(IdResumenCro), gEmpresa)
                    If dtMontoResumen.Rows.Count > 0 Then
                        SaldoResumen = dtMontoResumen.Rows(0).Item("MontoTotal")
                        MontoSaldoaPagar = SaldoResumen - MontoTotal
                    End If
                End If
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                iResultado4 = ePagoProveedores.ActResumen(5, gEmpresa, IdResumenCro, MontoSaldoaPagar, MontoDolares, CountDocActu, strGlosaFacturas2)


                'traer todos los docs del resumen
                'en un loop  actualizar la tabla saldo con los montos a pagar de los docs traidos..
                'Dim iResultadoEdiEstadoResumen2 As Integer = 0
                'iResultadoEdiEstadoResumen2 = eMovimientoCajaBanco.fEditarResumen(7, Trim(dtpFechaGasto.Value), IdResumenBD, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, "", Trim(cboTipoPago.SelectedValue), MontoSaldoaPagar, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, "", "", "", 0, 0, "", sMonCodigo, gEmpresa)


                If CountDocActu = 0 Then
                    Dim iBorrarCronograma As Integer
                    iBorrarCronograma = ePagoProveedores.ActResumen(10, gEmpresa, IdResumenCro, MontoSaldoaPagar, MontoDolares, CountDocActu, strGlosaFacturas2)
                End If
                MessageBox.Show("Actualizacion de Programacion con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboMoneda_SelectedIndexChanged(sender, e)

            End If
        Else
            If cboTipAnexos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Tipo de Anexo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipAnexos.Focus()
                Exit Sub
            End If
            If cboAnexos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Anexo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboAnexos.Focus()
                Exit Sub
            End If
            If cboResumenes.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Programacion de Pagos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboResumenes.Focus()
                Exit Sub
            End If
            If dgvDetalle.Rows.Count = 0 Then
                MessageBox.Show("No Existe Registros de Programacion de Pagos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

        End If
    End Sub

    Private m_EditingRow As Integer = -1

    Private Sub dgvDocumentos_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDocumentos.EditingControlShowing
        m_EditingRow = dgvDocumentos.CurrentRow.Index

        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Dim CadenaOrden As String = ""

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex

        If columna = 7 Or columna = 9 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

        If columna = 8 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   

            If caracter = "d" Or caracter = "D" Or caracter = "p" Or caracter = "P" Or caracter = ChrW(Keys.Back) Then
                If (caracter = ChrW(Keys.Back)) Or (caracter = "D") And (txt.Text.Contains("D") = False) Or (caracter = "d") And (txt.Text.Contains("d") = False) Or (caracter = "P") And (txt.Text.Contains("P") = False) Or (caracter = "p") And (txt.Text.Contains("p") = False) Then
                    e.Handled = False
                    Exit Sub
                Else
                    e.Handled = True
                    Exit Sub
                End If
            Else
                e.Handled = True
                Exit Sub
            End If
            'If caracter = "p" Or caracter = "P" Then
            '    If (caracter = ChrW(Keys.Back)) Or (caracter = "P") And (txt.Text.Contains("P") = False) Or (caracter = "p") And (txt.Text.Contains("p") = False) Then
            '        e.Handled = False
            '        Exit Sub
            '    Else
            '        e.Handled = True
            '        Exit Sub
            '    End If
            'Else
            '    e.Handled = True
            '    Exit Sub
            'End If
        End If

        If columna = 11 Then
            'CadenaOrden = ""
            Dim caracter As Char = e.KeyChar
            'If caracter = ChrW(Keys.Back) And Len(Trim(CadenaOrden)) > 0 Then
            'CadenaOrden = Mid(CadenaOrden, 1, Len(CadenaOrden) - Len(CadenaOrden))
            'End If
            'If caracter <> ChrW(Keys.Back) Then
            'CadenaOrden = CadenaOrden & caracter
            'End If
            'If Len(Trim(CadenaOrden)) > 0 Then
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back) Or (caracter = "-")) Then
                e.Handled = False
                eRegistroDocumento = New clsRegistroDocumento
                If Len(caracter) > 0 And caracter <> ChrW(Keys.Back) Then
                    'Panel4.Visible = True
                    'txtNroOrden.Text = Trim(caracter)
                    'Dim dtOrdenes As DataTable
                    'dtOrdenes = New DataTable
                    'dtOrdenes = eRegistroDocumento.fListarOrdenes2(gEmpresa, Trim(caracter))
                    'dgvOrdenes.DataSource = dtOrdenes
                    'txtNroOrden.Focus()
                ElseIf Len(caracter) = 0 And caracter <> ChrW(Keys.Back) Then
                    'Panel4.Visible = True
                    'txtNroOrden.Clear()
                    'Dim dtOrdenes As DataTable
                    'dtOrdenes = New DataTable
                    'dtOrdenes = eRegistroDocumento.fListarOrdenes(gEmpresa)
                    'dgvOrdenes.DataSource = dtOrdenes
                    'txtNroOrden.Focus()
                End If
            Else
                e.Handled = True
            End If

            'End If
        End If
        'CadenaOrden = ""
    End Sub

    Private Sub dgvDocumentos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDocumentos.KeyDown
        If e.KeyCode = Keys.Return Then
            Dim cur_cell As DataGridViewCell = dgvDocumentos.CurrentCell
            Dim col As Integer = cur_cell.ColumnIndex
            col = (col + 1) Mod dgvDocumentos.Columns.Count
            If col >= 13 Then
                cur_cell = dgvDocumentos.CurrentRow.Cells(col - 1)
                dgvDocumentos.CurrentCell = cur_cell
                e.Handled = True
            Else
                cur_cell = dgvDocumentos.CurrentRow.Cells(col)
                dgvDocumentos.CurrentCell = cur_cell
                e.Handled = True
            End If
        ElseIf e.KeyCode = Keys.Space Then
            If dgvDocumentos.Rows.Count > 0 Then
                Dim iFila As Integer
                If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 13 Then
                    iFila = dgvDocumentos.CurrentRow.Index
                    dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                    dgvDocumentos.CurrentCell = dgvDocumentos(13, iFila)
                End If
                Calcular()
            End If
        End If
    End Sub

    'Private Sub dgvDocumentos_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDocumentos.SelectionChanged
    '    If m_EditingRow >= 0 Then
    '        Dim new_row As Integer = m_EditingRow
    '        m_EditingRow = -1
    '        'dgvDocumentos.CurrentCell = dgvDocumentos.Rows(new_row).Cells(dgvDocumentos.CurrentCell.ColumnIndex)
    '        dgvDocumentos.CurrentCell = dgvDocumentos.Rows(new_row).Cells(dgvDocumentos.CurrentCell.ColumnIndex)
    '    End If
    'End Sub

    Private Sub dgvDocumentos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellContentClick
        Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex
        If dgvDocumentos.Rows.Count > 0 Then
            Try
                dgvDocumentos.CurrentCell = dgvDocumentos(0, dgvDocumentos.CurrentRow.Index)
                'Calcular()
                dgvDocumentos.CurrentCell = dgvDocumentos(columna, dgvDocumentos.CurrentRow.Index)
            Catch ex As Exception
            End Try
            'Calcular()
        End If
        Calcular()
    End Sub

    'Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    'End Sub

    'Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
    '    Dim StrCad As String = String.Empty
    '    Dim StrImporteDoc As String = String.Empty
    '    If e.ColumnIndex = 7 Then
    '        StrCad = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(7).Value)
    '        StrImporteDoc = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(19).Value)
    '        'If StrCad = Nothing Or StrImporteDoc = Nothing Then
    '        'Calcular()
    '        'Exit Sub
    '        'End If
    '        Try
    '            If Convert.ToDouble(StrCad) > Convert.ToDouble(StrImporteDoc) Then
    '                MessageBox.Show("El Monto a Pagar Supera al Importe del Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(7).Value = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Column16").Value '""
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(6).Value = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Column19").Value
    '                dgvDetalle.CurrentCell = dgvDetalle(7, dgvDetalle.CurrentRow.Index)
    '                'If dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(11).Value = True Then
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(11).Value = True
    '                'End If
    '            Else
    '            Dim resta As Double = 0
    '            resta = Convert.ToDouble(StrImporteDoc) - Convert.ToDouble(StrCad)
    '            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(6).Value = Format(CDec(resta), "0.00")
    '            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(7).Value = Format(CDec(StrCad), "0.00")
    '            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(11).Value = True
    '            dgvDetalle.CurrentCell = dgvDetalle(7, dgvDetalle.CurrentRow.Index)
    '            If resta = StrImporteDoc Then
    '                If dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(11).Value = True Then
    '                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(11).Value = False
    '                End If
    '            End If
    '            End If
    '        Catch ex As Exception
    '    End Try
    '    End If
    '    Calcular2()
    'End Sub

    Private m_EditingRow2 As Integer = -1

    'Private Sub dgvDetalle_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalle.EditingControlShowing
    '    m_EditingRow2 = dgvDetalle.CurrentRow.Index

    '    Try
    '        'referencia a la celda   
    '        Dim validar As TextBox = CType(e.Control, TextBox)
    '        'agregar el controlador de eventos para el KeyPress   
    '        AddHandler validar.KeyPress, AddressOf validar_Keypress2
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub validar_Keypress2(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If columna = 7 Or columna = 9 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    'Private Sub dgvDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
    '    If e.KeyCode = Keys.Return Then
    '        Dim cur_cell As DataGridViewCell = dgvDetalle.CurrentCell
    '        Dim col As Integer = cur_cell.ColumnIndex
    '        col = (col + 1) Mod dgvDetalle.Columns.Count
    '        If col >= 13 Then
    '            cur_cell = dgvDetalle.CurrentRow.Cells(col - 1)
    '            dgvDetalle.CurrentCell = cur_cell
    '            e.Handled = True
    '        Else
    '            cur_cell = dgvDetalle.CurrentRow.Cells(col)
    '            dgvDetalle.CurrentCell = cur_cell
    '            e.Handled = True
    '        End If
    '    ElseIf e.KeyCode = Keys.Space Then


    '        If dgvDetalle.Rows.Count > 0 Then
    '            Dim iFila As Integer

    '            If dgvDetalle.Rows.Count > 0 And dgvDetalle.CurrentCell.ColumnIndex = 11 Then
    '                iFila = dgvDetalle.CurrentRow.Index
    '                dgvDetalle.CurrentCell = dgvDetalle(0, 0)
    '                dgvDetalle.CurrentCell = dgvDetalle(11, iFila)
    '            End If
    '            'Calcular()
    '        End If
    '    End If
    'End Sub

    'Private Sub dgvDetalle_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDetalle.MouseUp
    '    If dgvDetalle.Rows.Count > 0 Then
    '        Dim iFila As Integer
    '        Calcular2()
    '        If dgvDetalle.Rows.Count > 0 And dgvDetalle.CurrentCell.ColumnIndex = 11 Then
    '            iFila = dgvDetalle.CurrentRow.Index
    '            dgvDetalle.CurrentCell = dgvDetalle(0, 0)
    '            dgvDetalle.CurrentCell = dgvDetalle(11, iFila)
    '        End If
    '    End If
    'End Sub

    'Sub Calcular2()
    '    If dgvDetalle.Rows.Count > 0 Then
    '        For x As Integer = 0 To dgvDetalle.Rows.Count - 1
    '            If Convert.ToBoolean(dgvDetalle.Rows(x).Cells(12).Value) = True Then
    '                'If Trim(dgvDocumentos.Rows(x).Cells(14).Value) = "SOLES" Then
    '                '    Dim StrCad As String = String.Empty
    '                '    'StrCad = Trim(dgvDocumentos.Rows(x).Cells(6).Value)
    '                '    StrCad = Trim(dgvDocumentos.Rows(x).Cells(7).Value)
    '                '    If StrCad = Nothing Then
    '                '        dblSoles = dblSoles + 0
    '                '    Else
    '                '        dblSoles = dblSoles + dgvDocumentos.Rows(x).Cells(7).Value
    '                '        'StrCad = Trim(dgvDocumentos.Rows(x).Cells(7).Value)
    '                '    End If
    '                '    CountCantDocSoles = CountCantDocSoles + 1
    '                '    strGlosaFacturasSoles = strGlosaFacturasSoles & " " & dgvDocumentos.Rows(x).Cells("AbrDoc").Value & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & "  "
    '                'ElseIf Trim(dgvDocumentos.Rows(x).Cells(14).Value) = "DOLARES" Then
    '                '    Dim StrCad As String = String.Empty
    '                '    'StrCad = Trim(dgvDocumentos.Rows(x).Cells(6).Value)
    '                '    StrCad = Trim(dgvDocumentos.Rows(x).Cells(7).Value)
    '                '    If StrCad = Nothing Then
    '                '        dblDolares = dblDolares + 0
    '                '    Else
    '                '        'dblDolares = dblDolares + dgvDocumentos.Rows(x).Cells(6).Value
    '                '        dblDolares = dblDolares + dgvDocumentos.Rows(x).Cells(7).Value
    '                '    End If
    '                '    CountCantDocDolares = CountCantDocDolares + 1
    '                '    strGlosaFacturasDolares = strGlosaFacturasDolares & " " & dgvDocumentos.Rows(x).Cells("AbrDoc").Value & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & "  "
    '                'End If
    '                'CountDocumentos = CountDocumentos + 1
    '            Else
    '                'dgvDetalle.Rows(x).Cells(6).Value = dgvDetalle.Rows(x).Cells(5).Value
    '                'dgvDetalle.Rows(x).Cells(7).Value = ""

    '                dgvDetalle.Rows(x).Cells(6).Value = Format(dgvDetalle.Rows(x).Cells("Column19").Value) '- dgvDetalle.Rows(x).Cells(17).Value, "0.00")
    '                dgvDetalle.Rows(x).Cells(7).Value = dgvDetalle.Rows(x).Cells(18).Value

    '                'dgvDocumentos.Rows(x).Cells(16).Value
    '                'MessageBox.Show(dgvDocumentos.Rows(x).Cells(16).Value)
    '                'dgvDocumentos.Rows(x).Cells(7).Value = ""
    '            End If
    '        Next
    '        'strGlosaFacturasSoles = "Pago a Proveedor, Doc: " & strGlosaFacturasSoles
    '        'strGlosaFacturasDolares = "Pago a Proveedor, Doc: " & strGlosaFacturasDolares
    '        'dgvDocumentos.CurrentCell = dgvDocumentos(6, 0)
    '        'dgvDocumentos.CurrentCell = dgvDocumentos(6, 0)
    '    End If
    '    'txtNroDoc.Text = CountDocumentos
    '    'txtTotSoles.Text = Format(dblSoles, "0.00")
    '    'txtTotDolares.Text = Format(dblDolares, "0.00")

    '    'dgvDetalle.CurrentCell = dgvDetalle(7, dgvDetalle.CurrentRow.Index)
    'End Sub

    'Private Sub dgvDetalle_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDetalle.MouseUp
    '    If dgvDetalle.Rows.Count > 0 Then
    '        Dim iFila As Integer
    '        'Calcular()
    '        If dgvDetalle.Rows.Count > 0 And dgvDetalle.CurrentCell.ColumnIndex = 11 Then
    '            iFila = dgvDocumentos.CurrentRow.Index
    '            dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
    '            dgvDocumentos.CurrentCell = dgvDocumentos(11, iFila)
    '        End If
    '    End If

    'End Sub

    'Private Sub dgvDetalle_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalle.SelectionChanged
    '    If m_EditingRow2 >= 0 Then
    '        Dim new_row As Integer = m_EditingRow2
    '        m_EditingRow2 = -1
    '        dgvDetalle.CurrentCell = dgvDetalle.Rows(new_row).Cells(dgvDetalle.CurrentCell.ColumnIndex)
    '        'dgvDetalle.CurrentCell = dgvDetalle.Rows(1).Cells(1)
    '    End If
    'End Sub

    Private Sub dgvDocumentos_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDocumentos.SelectionChanged
        If m_EditingRow >= 0 Then
            Dim new_row As Integer = m_EditingRow
            m_EditingRow = -1
            'dgvDocumentos.CurrentCell = dgvDocumentos.Rows(new_row).Cells(dgvDocumentos.CurrentCell.ColumnIndex)
            dgvDocumentos.CurrentCell = dgvDocumentos.Rows(new_row).Cells(dgvDocumentos.CurrentCell.ColumnIndex)
        End If
    End Sub


    Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectedIndexChanged

        Dim ImporteDocSoles As Double = 0
        Dim ImporteDocDolares As Double = 0
        Dim SaldoDocSoles As Double = 0
        Dim SaldoDocDolares As Double = 0
        Dim PagarDocSoles As Double = 0
        Dim PagarDocDolares As Double = 0

        Dim dtDetalleCronograma As DataTable
        Dim x As Integer
        Dim y As Integer
        Dim z As Integer
        Dim NroSemana As String = ""
        Dim Anio As String = ""
        Dim IdTipoAnexo As String = ""
        Dim IdAnexo As String = ""
        Dim IdMoneda As String = ""
        If cboResumenes.SelectedIndex > -1 And cboTipAnexos.SelectedIndex > -1 And cboAnexos.SelectedIndex > -1 And cboMoneda.SelectedIndex > -1 Then
            Try
                NroSemana = Trim(cboResumenes.SelectedValue.ToString)
                'Anio = Mid(Trim(cboResumenes.Text), 12, 16)
                Anio = vb.Right(Trim(cboResumenes.Text), 4)
                IdTipoAnexo = Trim(cboTipAnexos.SelectedValue.ToString)
                IdAnexo = Trim(cboAnexos.SelectedValue.ToString)
                IdMoneda = Trim(cboMoneda.SelectedValue.ToString)
                If NroSemana <> "System.Data.DataRowView" And IdTipoAnexo <> "System.Data.DataRowView" And IdAnexo <> "System.Data.DataRowView" And IdMoneda <> "System.Data.DataRowView" Then
                    If dgvDetalle.Rows.Count > 0 Then
                        m_EditingRow2 = -1
                        For x = 0 To dgvDetalle.RowCount - 1
                            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                        Next
                    End If
                    dtDetalleCronograma = New DataTable
                    dtDetalleCronograma = ePagoProveedores.fLisDetCronogramas5(Trim(IdMoneda), Trim(IdTipoAnexo), Trim(IdAnexo), Trim(NroSemana), Trim(Anio), gEmpresa)
                    If dtDetalleCronograma.Rows.Count > 0 Then
                        cargarCentrosDeCosto2()
                        btnActualizar.Enabled = True
                        btnLetra.Enabled = True
                        For y = 0 To dtDetalleCronograma.Rows.Count - 1
                            dgvDetalle.Rows.Add()
                            For z = 0 To dtDetalleCronograma.Columns.Count - 1
                                Dim StrCadCodigo As String = String.Empty

                                If z = 5 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        ImporteDocSoles = ImporteDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        ImporteDocDolares = ImporteDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 6 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        SaldoDocSoles = SaldoDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        SaldoDocDolares = SaldoDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 7 Then
                                    If Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "01" Then
                                        PagarDocSoles = PagarDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(14).ToString) = "02" Then
                                        PagarDocDolares = PagarDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                    End If
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                ElseIf z = 11 Then
                                    For a As Integer = 0 To IdCC2.Items.Count - 1
                                        Dim cad As String = IdCC2.Items.Item(a)
                                        StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                                        If Trim(dtDetalleCronograma.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                                            dgvDetalle.Rows(y).Cells(z).Value = IdCC2.Items.Item(a)
                                        End If
                                    Next
                                ElseIf z = 12 Then
                                    If dtDetalleCronograma.Rows(y).Item(11).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = True
                                    ElseIf dtDetalleCronograma.Rows(y).Item(1).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).Value = False
                                    End If
                                    If dtDetalleCronograma.Rows(y).Item(15).ToString = "1" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = True
                                        dgvDetalle.Rows(y).Cells(20).ReadOnly = True
                                        gestionaResaltados(dgvDetalle, y, Color.Orange)
                                    ElseIf dtDetalleCronograma.Rows(y).Item(15).ToString = "0" Then
                                        dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                        dgvDetalle.Rows(y).Cells(11).ReadOnly = False
                                    End If
                                ElseIf z = 13 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(12)
                                ElseIf z = 14 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(13)
                                ElseIf z = 15 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(17)
                                ElseIf z = 16 Then
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(18)
                                Else ' 0 1 2 3 4 8 9 10 12 13 14 15
                                    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                End If

                                'If z = 11 Then
                                '    ''Dim aa As String
                                '    ''aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    If dtDetalleCronograma.Rows(y).Item(z).ToString = "1" Then
                                '        dgvDetalle.Rows(y).Cells(z).Value = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(z).ToString = "0" Then
                                '        dgvDetalle.Rows(y).Cells(z).Value = False
                                '    End If
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '        gestionaResaltados(dgvDetalle, y, Color.Orange)
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 2 Then
                                '    'Dim aa As String
                                '    'aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    dgvDetalle.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtDetalleCronograma.Rows(y).Item(z), 10)
                                'ElseIf z = 3 Then
                                '    'Dim aa As String
                                '    'aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    dgvDetalle.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtDetalleCronograma.Rows(y).Item(z), 10)
                                'ElseIf z = 5 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        ImporteDocSoles = ImporteDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        ImporteDocDolares = ImporteDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'ElseIf z = 6 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        SaldoDocSoles = SaldoDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        SaldoDocDolares = SaldoDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'ElseIf z = 7 Then
                                '    If Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "SOLES" Then
                                '        PagarDocSoles = PagarDocSoles + dtDetalleCronograma.Rows(y).Item(z)
                                '    ElseIf Trim(dtDetalleCronograma.Rows(y).Item(15).ToString) = "DOLARES" Then
                                '        PagarDocDolares = PagarDocDolares + dtDetalleCronograma.Rows(y).Item(z)
                                '    End If
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 8 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 9 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'ElseIf z = 10 Then
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                '    If dtDetalleCronograma.Rows(y).Item(18).ToString = "1" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = True
                                '    ElseIf dtDetalleCronograma.Rows(y).Item(18).ToString = "0" Then
                                '        'dgvDetalle.Rows(y).Cells(z).ReadOnly = False
                                '    End If
                                'Else
                                '    'Dim aa As String
                                '    'aa = Trim(dtDetalleCronograma.Rows(y).Item(z))
                                '    dgvDetalle.Rows(y).Cells(z).Value = dtDetalleCronograma.Rows(y).Item(z)
                                'End If

                            Next
                        Next


                        txtImporteSoles.Text = Format(ImporteDocSoles, "#,##0.00")
                        txtImporteDolares.Text = Format(ImporteDocDolares, "#,##0.00")
                        txtSalSoles.Text = Format(SaldoDocSoles, "#,##0.00")
                        txtSalDolares.Text = Format(SaldoDocDolares, "#,##0.00")
                        txtPagarSol.Text = Format(PagarDocSoles, "#,##0.00")
                        txtPagarDol.Text = Format(PagarDocDolares, "#,##0.00")



                    Else
                        btnActualizar.Enabled = False
                        btnLetra.Enabled = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            btnActualizar.Enabled = False
            btnLetra.Enabled = False
            If dgvDetalle.Rows.Count > 0 Then
                m_EditingRow2 = -1
                For x = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
            End If
            DesHabilitarLetra()
        End If
    End Sub



    Private Sub BeLabel21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel21.Click
        Panel4.Visible = False
        CadenaOrden = ""
        'If dgvDocumentos.Rows.Count > 0 Then
        '    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(11).Value = Trim(txtNroOrden.Text)
        '    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(19).Value = ""
        'End If

    End Sub

    Private Sub dgvOrdenes_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOrdenes.CellContentClick

    End Sub

    Private Sub dgvOrdenes_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.DoubleClick
        Dim sCodigoActualLinea As String = ""
        Dim iResultadoGrabLinea As Int32 = 0
        If dgvOrdenes.Rows.Count > 0 Then
            Dim CodOrden As String = ""
            Dim NroOrden As String = ""
            Dim CodProvLogistica As String = ""
            Dim xCC As String = ""
            Try

                Dim sCodProvRUC As String = ""
                sCodProvRUC = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(6).Value
                If Len(Trim(sCodProvRUC)) = 11 And Len(Trim(txtCodigo.Text)) = 11 Then
                    If Trim(sCodProvRUC) <> Trim(txtCodigo.Text) Then
                        MessageBox.Show("Este N�mero de Orden de Compra no Pertenece al Proveedor " & Trim(cboAnexo.Text), "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtNroOrden.Focus()
                        Exit Sub
                    End If
                End If


                CodOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(4).Value)
                NroOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(0).Value)
                CodProvLogistica = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(5).Value)
                xCC = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(7).Value)

                If dgvDocumentos.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                        If Trim(dgvDocumentos.Rows(x).Cells(19).Value) = CodOrden Then
                            MessageBox.Show("El N�mero de Orden " & NroOrden & " ya esta asignado a la Factura : " & Trim(dgvDocumentos.Rows(x).Cells(1).Value), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            txtNroOrden.Focus()
                            Exit Sub
                        End If
                    Next
                End If

                If dgvDocumentos.Rows.Count > 0 Then
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(11).Value = Trim(NroOrden)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(19).Value = Trim(CodOrden)

                    If Len(Trim(xCC)) > 0 Then
                        For a As Integer = 0 To IdCC.Items.Count - 1
                            Dim StrCadCodigo As String
                            Dim cad As String = IdCC.Items.Item(a)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(xCC) = StrCadCodigo Then
                                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IdCC").Value = IdCC.Items.Item(a)
                            End If
                        Next
                    End If

                    If Len(Trim(NroOrden)) > 0 And Len(Trim(cboAnexo.SelectedValue)) > 0 And Len(Trim(CodProvLogistica)) > 0 Then
                        eRegistroDocumento = New clsRegistroDocumento
                        Dim dtBuscarLinea As DataTable
                        dtBuscarLinea = New DataTable
                        dtBuscarLinea = eRegistroDocumento.VerificarLinea(Trim(cboAnexo.SelectedValue), gEmpresa)
                        If dtBuscarLinea.Rows.Count = 0 Then
                            eRegistroDocumento.fCodigoLinea(gEmpresa)
                            sCodigoActualLinea = eRegistroDocumento.sCodFuturoLinea
                            iResultadoGrabLinea = eRegistroDocumento.fGrabarLinea(39, sCodigoActualLinea, Trim(CodProvLogistica), gEmpresa, 0, 0, 0, Trim(cboAnexo.SelectedValue))
                            If iResultadoGrabLinea = 1 Then
                                txtLineaCreSoles.Enabled = True
                                txtLineaCreDolares.Enabled = True
                                txtDiasCred.Enabled = True
                                txtIdLinea.Enabled = True
                                txtIdLinea.Text = sCodigoActualLinea
                            End If
                        End If
                    End If

                End If
                Panel4.Visible = False
                CadenaOrden = ""
            Catch ex As Exception
                Exit Sub
            End Try
        End If

    End Sub

    'Private Sub txtNroOrden_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    Select Case Asc(e.KeyChar)
    '        Case 13
    '            dgvOrdenes.Focus()
    '    End Select
    'End Sub

    'Private Sub txtNroOrden_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    Select Case Asc(e.KeyChar)
    '        Case 13
    '            dgvOrdenes.Focus()
    '    End Select
    'End Sub

    'Private Sub txtNroOrden_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Len(Trim(txtNroOrden.Text)) > 0 Then
    '        'Panel4.Visible = True
    '        'txtNroOrden.Text = Trim(CadenaOrden)
    '        eRegistroDocumento = New clsRegistroDocumento
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        'CodAcreedor = Trim(cboTipoAcreedor.SelectedValue)

    '        'sDescripcion = Trim(txtFiltrar.Text)
    '        dtOrdenes = eRegistroDocumento.fListarOrdenes2(gEmpresa, Trim(txtNroOrden.Text))
    '        'dgvOrdenes.AutoGenerateColumns = False
    '        dgvOrdenes.DataSource = dtOrdenes
    '        'txtNroOrden.Focus()
    '    ElseIf Len(Trim(txtNroOrden.Text)) = 0 Then
    '        'Panel4.Visible = True
    '        'txtNroOrden.Text = Trim(CadenaOrden)
    '        eRegistroDocumento = New clsRegistroDocumento
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenes(gEmpresa)
    '        dgvOrdenes.DataSource = dtOrdenes
    '        'txtNroOrden.Focus()
    '    End If
    'End Sub

    'Private Sub txtNroOrden_KeyPress1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroOrden.KeyPress
    '    Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
    '    KeyAscii = CShort(SoloNumeros(KeyAscii))
    '    If KeyAscii = 0 Then
    '        e.Handled = True
    '    End If
    'End Sub

    'Private Sub txtNroOrden_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = False Then
    '        eRegistroDocumento = New clsRegistroDocumento
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenes2(gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = False Then
    '        eRegistroDocumento = New clsRegistroDocumento
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenes(gEmpresa, Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    ElseIf Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = True Then
    '        Panel4.Visible = True
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(43, gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = True Then
    '        Panel4.Visible = True
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(42, gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    End If
    'End Sub

    Private m_EditingRow3 As Integer = -1

    Private Sub dgvOrdenes_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvOrdenes.EditingControlShowing
        m_EditingRow3 = dgvOrdenes.CurrentRow.Index
    End Sub




    Private Sub dgvOrdenes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvOrdenes.KeyDown
        If dgvOrdenes.Rows.Count > 0 Then
            If e.KeyCode = Keys.Return Then
                Dim cur_cell As DataGridViewCell = dgvOrdenes.CurrentCell

                Dim col As Integer = cur_cell.ColumnIndex
                col = (col + 1) Mod dgvOrdenes.Columns.Count
                cur_cell = dgvOrdenes.CurrentRow.Cells(col)

                If cur_cell.Visible = False Then
                    Exit Sub
                End If

                dgvOrdenes.CurrentCell = cur_cell
                e.Handled = True
            End If
            'Else
            'txtFiltrar.Focus()
        End If
    End Sub

    Private Sub dgvOrdenes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvOrdenes.KeyPress
        Dim CodOrden As String = ""
        Dim NroOrden As String = ""
        Select Case Asc(e.KeyChar)
            Case 13
                dgvOrdenes_DoubleClick(sender, e)

                'Try
                '    CodOrden = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(4).Value
                '    NroOrden = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(0).Value
                '    txtCodOrden.Text = Trim(CodOrden)
                '    txtNroOrden.Text = Trim(NroOrden)
                '    Panel4.Visible = False
                '    cboDP.Focus()
                'Catch ex As Exception
                '    Exit Sub
                'End Try
        End Select

        'If dgvOrdenes.Rows.Count > 0 Then
        '    Dim CodOrden As String = ""
        '    Dim NroOrden As String = ""
        '    Try

        '        Dim sCodProvRUC As String = ""
        '        sCodProvRUC = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(6).Value
        '        If Len(Trim(sCodProvRUC)) = 11 And Len(Trim(txtCodigo.Text)) = 11 Then
        '            If Trim(sCodProvRUC) <> Trim(txtCodigo.Text) Then
        '                MessageBox.Show("Este N�mero de Orden de Compra no Pertenece al Proveedor " & Trim(cboAnexo.Text), "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                txtNroOrden.Focus()
        '                Exit Sub
        '            End If
        '        End If

        '        CodOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(4).Value)
        '        NroOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(0).Value)

        '        If dgvDocumentos.Rows.Count > 0 Then
        '            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
        '                If Trim(dgvDocumentos.Rows(x).Cells(19).Value) = CodOrden Then
        '                    MessageBox.Show("El N�mero de Orden " & NroOrden & " ya esta asignado a la Factura : " & Trim(dgvDocumentos.Rows(x).Cells(1).Value), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '                    txtNroOrden.Focus()
        '                    Exit Sub
        '                End If
        '            Next
        '        End If

        '        If dgvDocumentos.Rows.Count > 0 Then
        '            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(11).Value = Trim(NroOrden)
        '            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells(19).Value = Trim(CodOrden)
        '        End If
        '        Panel4.Visible = False
        '        CadenaOrden = ""
        '    Catch ex As Exception
        '        Exit Sub
        '    End Try
        'End If


    End Sub

    Private Sub dgvOrdenes_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.SelectionChanged
        If m_EditingRow3 >= 0 Then
            Dim new_row As Integer = m_EditingRow3
            m_EditingRow3 = -1
            dgvOrdenes.CurrentCell = dgvOrdenes.Rows(new_row).Cells(dgvOrdenes.CurrentCell.ColumnIndex)
        End If
    End Sub

    Private Sub txtLineaCreSoles_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLineaCreSoles.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtIdLinea.Text)) > 0 Then
                    ePagoProveedores = New clsPagoProveedores
                    Dim ActualizaLineaCre As Integer = 0
                    Dim Dias As Integer = 0
                    Dim CreditoSoles As Double = 0
                    Dim CreditoDolares As Double = 0
                    If Len(Trim(txtDiasCred.Text)) = 0 Then
                        Dias = 0
                    Else
                        Dias = Trim(txtDiasCred.Text)
                    End If

                    If Len(Trim(txtLineaCreSoles.Text)) = 0 Then
                        CreditoSoles = 0
                    Else
                        CreditoSoles = Trim(txtLineaCreSoles.Text)
                    End If

                    If Len(Trim(txtLineaCreDolares.Text)) = 0 Then
                        CreditoDolares = 0
                    Else
                        CreditoDolares = Trim(txtLineaCreDolares.Text)
                    End If
                    ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito(Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), CreditoSoles, CreditoDolares)
                    'cboAnexo_SelectedIndexChanged(sender, e)
                End If
        End Select
    End Sub

    Private Sub txtLineaCreDolares_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLineaCreDolares.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtIdLinea.Text)) > 0 Then
                    ePagoProveedores = New clsPagoProveedores
                    Dim ActualizaLineaCre As Integer = 0

                    Dim Dias As Integer = 0
                    Dim CreditoSoles As Double = 0
                    Dim CreditoDolares As Double = 0
                    If Len(Trim(txtDiasCred.Text)) = 0 Then
                        Dias = 0
                    Else
                        Dias = Trim(txtDiasCred.Text)
                    End If

                    If Len(Trim(txtLineaCreSoles.Text)) = 0 Then
                        CreditoSoles = 0
                    Else
                        CreditoSoles = Trim(txtLineaCreSoles.Text)
                    End If

                    If Len(Trim(txtLineaCreDolares.Text)) = 0 Then
                        CreditoDolares = 0
                    Else
                        CreditoDolares = Trim(txtLineaCreDolares.Text)
                    End If

                    ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito(Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), CreditoSoles, CreditoDolares)
                    'cboAnexo_SelectedIndexChanged(sender, e)
                End If
        End Select
    End Sub

    Private Sub txtDiasCred_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiasCred.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtIdLinea.Text)) > 0 Then
                    ePagoProveedores = New clsPagoProveedores
                    Dim ActualizaLineaCre As Integer = 0

                    Dim Dias As Integer = 0
                    Dim CreditoSoles As Double = 0
                    Dim CreditoDolares As Double = 0
                    If Len(Trim(txtDiasCred.Text)) = 0 Then
                        Dias = 0
                    Else
                        Dias = Trim(txtDiasCred.Text)
                    End If

                    If Len(Trim(txtLineaCreSoles.Text)) = 0 Then
                        CreditoSoles = 0
                    Else
                        CreditoSoles = Trim(txtLineaCreSoles.Text)
                    End If

                    If Len(Trim(txtLineaCreDolares.Text)) = 0 Then
                        CreditoDolares = 0
                    Else
                        CreditoDolares = Trim(txtLineaCreDolares.Text)
                    End If

                    ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito(Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), CreditoSoles, CreditoDolares)
                    'cboAnexo_SelectedIndexChanged(sender, e)
                End If
        End Select
    End Sub

    Private Sub RadioButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblIngrese.Text = "Ingrese N�mero de Cheque"
        'cboTipoPago.Focus()
        txtCadenaBusqueda.Focus()
    End Sub

    Private Sub RadioButton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblIngrese.Text = "Ingrese N�mero de Documento"
        txtCadenaBusqueda.Focus()
    End Sub

    'Private Sub txtCadenaBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCadenaBusqueda.TextChanged
    '    ePagoProveedores = New clsPagoProveedores

    '    If Len(Trim(txtCadenaBusqueda.Text)) > 0 And RadioButton1.Checked = True Then
    '        Dim dtTable As DataTable
    '        dtTable = New DataTable
    '        dtTable = ePagoProveedores.fListarCronogramas7(gEmpresa, Trim(txtFiltrar.Text))
    '        dgvDocumentosaEntregar.DataSource = dtTable
    '    ElseIf Len(Trim(txtCadenaBusqueda.Text)) > 0 And RadioButton2.Checked = True Then
    '        Dim dtTable As DataTable
    '        dtTable = New DataTable
    '        dtTable = ePagoProveedores.fListarCronogramas6(gEmpresa, Trim(txtFiltrar.Text))
    '        dgvDocumentosaEntregar.DataSource = dtTable
    '    End If

    '    If Len(Trim(txtCadenaBusqueda.Text)) = 0 Then
    '        Dim dtTable As DataTable
    '        dtTable = New DataTable
    '        dtTable = ePagoProveedores.fListarCronogramas8(gEmpresa)
    '        dgvDocumentosaEntregar.DataSource = dtTable
    '    End If

    'End Sub

    Private Sub txtCadenaBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCadenaBusqueda.TextChanged
        CheckBox1.Checked = False
        ePagoProveedores = New clsPagoProveedores
        Dim dblSolesDocChes As Double = 0
        Dim dblDolaresDocChes As Double = 0
        Dim dtTable As DataTable
        dtTable = New DataTable

        'If cboCriterio.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione un criterio de b�squeda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    'txtCadenaBusqueda.Text = ""
        '    cboCriterio.Focus()
        '    '.Clear()
        '    Exit Sub
        'End If

        If Len(Trim(txtCadenaBusqueda.Text)) > 0 And Trim(cboCriterio.Text) = "CHEQUE" Then
            'If cboTipoPago.SelectedIndex = -1 Then
            '    MessageBox.Show("Seleccione un Tipo de Pago", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    cboTipoPago.Focus()
            '    Exit Sub
            'End If
            'Dim dtTable As DataTable
            'dtTable = New DataTable
            'dtTable = ePagoProveedores.fListarDocumentosaEntregar(20, gEmpresa, "00002", Trim(txtCadenaBusqueda.Text))
            'dtTable = ePagoProveedores.fListarDocumentosaEntregar(20, gEmpresa, ""cboTipoPago.SelectedValue, Trim(txtCadenaBusqueda.Text))
            Dim cadenaBusqueda As String = ""


            cadenaBusqueda = "SELECT " & _
                             "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                             "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                             "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                             "TM.NroFormaPago,TR.IdRegistro,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                             "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                             "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                             "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                             "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                             "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                             "WHERE TR.EmprCodigo='" & gEmpresa & "' AND TM.NroFormaPago LIKE '%" & Trim(txtCadenaBusqueda.Text) & "%' " & _
                             "AND TR.SituacionId='00004' AND TR.Entregado<>1 AND TM.IdFormaPago='00002' " & _
                             "ORDER BY Situacion,TM.NroFormaPago"
            da = New SqlDataAdapter(cadenaBusqueda, conexion)
            da.Fill(dtTable)
        ElseIf Len(Trim(txtCadenaBusqueda.Text)) > 0 And Trim(cboCriterio.Text) = "NUMERO DOCUMENTO" Then

            Dim cadenaBusqueda As String = ""
            cadenaBusqueda = "SELECT " & _
                             "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                             "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                             "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                             "TM.NroFormaPago,TR.IdRegistro,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                             "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                             "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                             "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                             "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                             "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                             "WHERE TR.EmprCodigo='" & gEmpresa & "' AND TR.NroDocumento LIKE '%" & Trim(txtCadenaBusqueda.Text) & "%' " & _
                             "AND TR.SituacionId='00004' AND TR.Entregado<>1 AND TM.IdFormaPago='00002' " & _
                             "ORDER BY Situacion,TM.NroFormaPago"
            da = New SqlDataAdapter(cadenaBusqueda, conexion)
            da.Fill(dtTable)
        ElseIf Len(Trim(txtCadenaBusqueda.Text)) > 0 And Trim(cboCriterio.Text) = "RUC" Then
            Dim cadenaBusqueda As String = ""


            cadenaBusqueda = "SELECT " & _
                             "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                             "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                             "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                             "TM.NroFormaPago,TR.IdRegistro,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                             "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                             "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                             "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                             "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                             "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                             "WHERE TR.EmprCodigo='" & gEmpresa & "' AND CA.Ruc LIKE '%" & Trim(txtCadenaBusqueda.Text) & "%' " & _
                             "AND TR.SituacionId='00004' AND TR.Entregado<>1 AND TM.IdFormaPago='00002' " & _
                             "ORDER BY Situacion,TM.NroFormaPago"
            da = New SqlDataAdapter(cadenaBusqueda, conexion)
            da.Fill(dtTable)
        ElseIf Len(Trim(txtCadenaBusqueda.Text)) > 0 And Trim(cboCriterio.Text) = "RAZON SOCIAL" Then
            Dim cadenaBusqueda As String = ""


            cadenaBusqueda = "SELECT " & _
                             "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                             "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                             "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                             "TM.NroFormaPago,TR.IdRegistro,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                             "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                             "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                             "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                             "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                             "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                             "WHERE TR.EmprCodigo='" & gEmpresa & "' AND CA.AnaliticoDescripcion LIKE '%" & Trim(txtCadenaBusqueda.Text) & "%' " & _
                             "AND TR.SituacionId='00004' AND TR.Entregado<>1 AND TM.IdFormaPago='00002' " & _
                             "ORDER BY Situacion,TM.NroFormaPago"
            da = New SqlDataAdapter(cadenaBusqueda, conexion)
            da.Fill(dtTable)

        End If

        If Len(Trim(txtCadenaBusqueda.Text)) = 0 Then


            Dim cadenaBusqueda As String = ""
            cadenaBusqueda = "SELECT " & _
                             "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                             "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                             "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                             "TM.NroFormaPago,TR.IdRegistro,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                             "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                             "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda  " & _
                             "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                             "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                             "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                             "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                             "WHERE TR.EmprCodigo='" & gEmpresa & "' AND TR.SituacionId='00004' AND TR.Entregado<>1 AND TM.IdFormaPago='00002' " & _
                             "ORDER BY Situacion,TM.NroFormaPago"
            da = New SqlDataAdapter(cadenaBusqueda, conexion)
            da.Fill(dtTable)
        End If


        If dtTable.Rows.Count = 0 Then
            BeLabel27.Text = "Total de Registros : 0"
            txtCheSoles.Text = Format(Math.Round(0, 0), "#,##0.00")
            txtCheDolares.Text = Format(Math.Round(0, 0), "#,##0.00")

            If dgvDocumentosaEntregar.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDocumentosaEntregar.RowCount - 1
                    dgvDocumentosaEntregar.Rows.Remove(dgvDocumentosaEntregar.CurrentRow)
                Next
            End If

        ElseIf dtTable.Rows.Count > 0 Then
            dgvDocumentosaEntregar.DataSource = dtTable
            For x As Integer = 0 To dgvDocumentosaEntregar.Rows.Count - 1
                If Trim(dgvDocumentosaEntregar.Rows(x).Cells("Column32").Value) = "01" Then
                    Dim StrCad As String = String.Empty
                    StrCad = Trim(dgvDocumentosaEntregar.Rows(x).Cells("Column27").Value)
                    If StrCad = Nothing Then
                        dblSolesDocChes = dblSolesDocChes + 0
                    Else
                        dblSolesDocChes = dblSolesDocChes + dgvDocumentosaEntregar.Rows(x).Cells("Column27").Value
                    End If
                ElseIf Trim(dgvDocumentosaEntregar.Rows(x).Cells("Column32").Value) = "02" Then
                    Dim StrCad As String = String.Empty
                    StrCad = Trim(dgvDocumentosaEntregar.Rows(x).Cells("Column27").Value)
                    If StrCad = Nothing Then
                        dblDolaresDocChes = dblDolaresDocChes + 0
                    Else
                        dblDolaresDocChes = dblDolaresDocChes + dgvDocumentosaEntregar.Rows(x).Cells("Column27").Value
                    End If
                End If
                txtCheSoles.Text = Format(Math.Round(dblSolesDocChes, 2), "#,##0.00")
                txtCheDolares.Text = Format(Math.Round(dblDolaresDocChes, 2), "#,##0.00")
            Next

            BeLabel27.Text = "Total de Registros : " & dtTable.Rows.Count
        End If

    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.dgvDocumentosaEntregar.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                For Each fila As DataGridViewRow In Me.dgvDocumentosaEntregar.Rows
                    If Me.CheckBox1.Checked = True Then
                        fila.Cells("Entregar").Value = True
                    Else
                        fila.Cells("Entregar").Value = False
                    End If
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        If MsgBox("�Esta seguro de confirmar la entrega del pago?", MsgBoxStyle.YesNo, glbNameSistema) = MsgBoxResult.No Then
            Exit Sub
        End If

        Dim Act As Integer = 0
        Me.dgvDocumentosaEntregar.ClearSelection()
        ePagoProveedores = New clsPagoProveedores
        For z As Integer = 0 To dgvDocumentosaEntregar.Rows.Count - 1
            If Convert.ToBoolean(dgvDocumentosaEntregar.Rows(z).Cells("Entregar").Value) = True Then
                Act = ePagoProveedores.fActualizarEntregaDoc(gEmpresa, Trim(dgvDocumentosaEntregar.Rows(z).Cells("IdRegistro").Value))
            End If
        Next

        If Act = 1 Then
            MessageBox.Show("Proceso grabado con �xito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtCadenaBusqueda.Clear()
            txtCadenaBusqueda_TextChanged(sender, e)
        End If

    End Sub

    Private Sub chkVerTodo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVerTodo.CheckedChanged
        txtNroOrden_TextChanged(sender, e)
        txtNroOrden.Focus()
    End Sub

    Private Sub txtNroOrden_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroOrden.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                dgvOrdenes.Focus()
        End Select
    End Sub

    'Private Sub txtNroOrden_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = False Then
    '        eRegistroDocumento = New clsRegistroDocumento
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenes2(gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = False Then
    '        eRegistroDocumento = New clsRegistroDocumento
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenes(gEmpresa, Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    ElseIf Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = True Then
    '        Panel4.Visible = True
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(43, gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = True Then
    '        Panel4.Visible = True
    '        Dim dtOrdenes As DataTable
    '        dtOrdenes = New DataTable
    '        dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(42, gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
    '        dgvOrdenes.DataSource = dtOrdenes
    '    End If
    'End Sub

 
    Private Sub txtNroOrden_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroOrden.TextChanged
        If Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = False Then
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenes2(gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = False Then
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenes(gEmpresa, Trim(txtCodigo.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = True Then
            Panel4.Visible = True
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(43, gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = True Then
            Panel4.Visible = True
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(42, gEmpresa, Trim(txtNroOrden.Text), Trim(txtCodigo.Text))
            dgvOrdenes.DataSource = dtOrdenes
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        'Dim x As frmChequesPorEntregar = frmChequesPorEntregar.Instance
        'x.Owner = Me
        'x.ShowInTaskbar = False
        'x.ShowDialog()

        Dim x As frmChequesPorEntregar = frmChequesPorEntregar.Instance
        x.MdiParent = frmPrincipal
        x.Show()

    End Sub

    Private Sub txtBuscarEstadoDoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscarEstadoDoc.TextChanged
        ePagoProveedores = New clsPagoProveedores
        Dim cadenaBusqueda As String = ""
        Dim dtTable As DataTable
        dtTable = New DataTable
        Dim dblSolesDocs As Double = 0
        Dim dblDolaresDocs As Double = 0

        If chkDocCance.Checked = True Then
            If Len(Trim(txtBuscarEstadoDoc.Text)) > 0 And rdbRuc.Checked = True Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                                 "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                                 "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                                 "TM.NroFormaPago,TR.IdRegistro,TR.FechaBase,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                                 "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' AND CA.Ruc LIKE '%" & Trim(txtBuscarEstadoDoc.Text) & "%' " & _
                                 "AND TR.SituacionId='00004' AND TR.Entregado=1 AND TM.IdFormaPago='00002' " & _
                                 "ORDER BY Situacion,TM.NroFormaPago"

                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)
            ElseIf Len(Trim(txtBuscarEstadoDoc.Text)) > 0 And rdbNroDocumento.Checked = True Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                                 "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                                 "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                                 "TM.NroFormaPago,TR.IdRegistro,TR.FechaBase,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                                 "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' AND TR.NroDocumento LIKE '%" & Trim(txtBuscarEstadoDoc.Text) & "%' " & _
                                 "AND TR.SituacionId='00004' AND TR.Entregado=1 AND TM.IdFormaPago='00002' " & _
                                 "ORDER BY Situacion,TM.NroFormaPago"
                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)
            ElseIf Len(Trim(txtBuscarEstadoDoc.Text)) > 0 And rdbRazon.Checked = True Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                                 "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                                 "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                                 "TM.NroFormaPago,TR.IdRegistro,TR.FechaBase,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                                 "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' AND CA.AnaliticoDescripcion LIKE '%" & Trim(txtBuscarEstadoDoc.Text) & "%' " & _
                                 "AND TR.SituacionId='00004' AND TR.Entregado=1 AND TM.IdFormaPago='00002' " & _
                                 "ORDER BY Situacion,TM.NroFormaPago"
                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)

            End If

            If Len(Trim(txtBuscarEstadoDoc.Text)) = 0 Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal,B.Abreviado,C.NumeroCuenta, " & _
                                 "NroChequera=(SELECT cast(NroChequera as int) FROM Tesoreria.Chequera WHERE IdChequera=TM.IdChequera AND EmprCodigo=TM.EmprCodigo), " & _
                                 "Situacion=CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END, " & _
                                 "TM.NroFormaPago,TR.IdRegistro,TR.FechaBase,TR.IdMoneda FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoMovimiento TTM ON TM.IdFormaPago=TTM.IdRendicion " & _
                                 "INNER JOIN Tesoreria.Banco B ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.CuentaBanco C ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' " & _
                                 "AND TR.SituacionId='00004' AND TR.Entregado=1 AND TM.IdFormaPago='00002' " & _
                                 "ORDER BY Situacion,TM.NroFormaPago"

                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)
            End If
        End If

        If chkDocSinCance.Checked = True Then
            If Len(Trim(txtBuscarEstadoDoc.Text)) > 0 And rdbRuc.Checked = True Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal, " & _
                                 "Abreviado=(SELECT B.Abreviado FROM Tesoreria.Banco B INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NumeroCuenta=(SELECT C.NumeroCuenta FROM Tesoreria.CuentaBanco C INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NroChequera=(SELECT cast(CH.NroChequera as int) FROM Tesoreria.Chequera CH INNER JOIN Tesoreria.MovimientoCajaBanco TM ON CH.IdChequera=TM.IdChequera AND CH.EmprCodigo=TM.EmprCodigo " & _
                                 "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "Situacion=(SELECT CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END " & _
                                    "FROM Tesoreria.MovimientoCajaBanco TM  WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "NroFormaPago=(SELECT top 1 TM.NroFormaPago FROM Tesoreria.MovimientoCajaBanco TM WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "TR.IdRegistro,TR.FechaBase,TR.IdMoneda " & _
                                 "FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' AND CA.Ruc LIKE '%" & Trim(txtBuscarEstadoDoc.Text) & "%' " & _
                                 "AND TR.Entregado=0 " & _
                                 "ORDER BY Situacion"

                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)
            ElseIf Len(Trim(txtBuscarEstadoDoc.Text)) > 0 And rdbNroDocumento.Checked = True Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal, " & _
                                 "Abreviado=(SELECT B.Abreviado FROM Tesoreria.Banco B INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NumeroCuenta=(SELECT C.NumeroCuenta FROM Tesoreria.CuentaBanco C INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NroChequera=(SELECT cast(CH.NroChequera as int) FROM Tesoreria.Chequera CH INNER JOIN Tesoreria.MovimientoCajaBanco TM ON CH.IdChequera=TM.IdChequera AND CH.EmprCodigo=TM.EmprCodigo " & _
                                 "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "Situacion=(SELECT CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END " & _
                                 "FROM Tesoreria.MovimientoCajaBanco TM  WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "NroFormaPago=(SELECT top 1 TM.NroFormaPago FROM Tesoreria.MovimientoCajaBanco TM WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "TR.IdRegistro,TR.FechaBase,TR.IdMoneda " & _
                                 "FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' AND TR.NroDocumento LIKE '%" & Trim(txtBuscarEstadoDoc.Text) & "%' " & _
                                 "AND TR.Entregado=0 " & _
                                 "ORDER BY Situacion"
                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)
            ElseIf Len(Trim(txtBuscarEstadoDoc.Text)) > 0 And rdbRazon.Checked = True Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal, " & _
                                 "Abreviado=(SELECT B.Abreviado FROM Tesoreria.Banco B INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NumeroCuenta=(SELECT C.NumeroCuenta FROM Tesoreria.CuentaBanco C INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NroChequera=(SELECT cast(CH.NroChequera as int) FROM Tesoreria.Chequera CH INNER JOIN Tesoreria.MovimientoCajaBanco TM ON CH.IdChequera=TM.IdChequera AND CH.EmprCodigo=TM.EmprCodigo " & _
                                 "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "Situacion=(SELECT CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END " & _
                                 "FROM Tesoreria.MovimientoCajaBanco TM  WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "NroFormaPago=(SELECT top 1 TM.NroFormaPago FROM Tesoreria.MovimientoCajaBanco TM WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "TR.IdRegistro,TR.FechaBase,TR.IdMoneda " & _
                                 "FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' AND CA.AnaliticoDescripcion LIKE '%" & Trim(txtBuscarEstadoDoc.Text) & "%' " & _
                                 "AND TR.Entregado=0 " & _
                                 "ORDER BY Situacion"
                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)
            End If
            If Len(Trim(txtBuscarEstadoDoc.Text)) = 0 Then
                cadenaBusqueda = "SELECT " & _
                                 "CA.Ruc,CA.AnaliticoDescripcion,TTD.AbrDoc,NroDoc=TR.Serie+'-'+TR.NroDocumento,CM.MonSimbolo,TR.ImporteTotal, " & _
                                 "Abreviado=(SELECT B.Abreviado FROM Tesoreria.Banco B INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.IdBanco=B.IdBanco AND TM.EmprCodigo=B.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NumeroCuenta=(SELECT C.NumeroCuenta FROM Tesoreria.CuentaBanco C INNER JOIN Tesoreria.MovimientoCajaBanco TM ON TM.NumeroCuenta=C.IdCuenta AND TM.EmprCodigo=C.EmprCodigo " & _
                                    "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "NroChequera=(SELECT cast(CH.NroChequera as int) FROM Tesoreria.Chequera CH INNER JOIN Tesoreria.MovimientoCajaBanco TM ON CH.IdChequera=TM.IdChequera AND CH.EmprCodigo=TM.EmprCodigo " & _
                                 "WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "Situacion=(SELECT CASE TM.IdFormaPago WHEN '00002' THEN 'CHEQ' WHEN '00005' THEN 'TRANS' WHEN '00007' THEN 'CRTA' WHEN '00008' THEN 'FINAN' ELSE 'OTROS' END " & _
                                    "FROM Tesoreria.MovimientoCajaBanco TM  WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo ), " & _
                                 "NroFormaPago=(SELECT top 1 TM.NroFormaPago FROM Tesoreria.MovimientoCajaBanco TM WHERE TM.IdMovimiento=TR.IdMovimiento AND TM.EmprCodigo=TR.EmprCodigo), " & _
                                 "TR.IdRegistro,TR.FechaBase,TR.IdMoneda " & _
                                 "FROM Tesoreria.RegistroDocumento TR " & _
                                 "INNER JOIN Contabilidad.ct_analitico CA ON CA.AnaliticoCodigo=TR.CodProveedor AND CA.TipoAnaliticoCodigo=TR.TipoAcreedorId AND CA.EmprCodigo=TR.EmprCodigo " & _
                                 "INNER JOIN Tesoreria.TipoDocumento TTD ON TTD.IdDocumento=TR.IdDocumento " & _
                                 "INNER JOIN Comun.Moneda CM ON CM.MonCodigo=TR.IdMoneda " & _
                                 "WHERE TR.EmprCodigo='" & gEmpresa & "' " & _
                                 "AND TR.Entregado=0 " & _
                                 "ORDER BY Situacion"

                da = New SqlDataAdapter(cadenaBusqueda, conexion)
                da.Fill(dtTable)
            End If

        End If


        If dtTable.Rows.Count = 0 Then
            BeLabel29.Text = "Total de Registros : 0"
            txtSol.Text = Format(0, "#,##0.00")
            txtDol.Text = Format(0, "#,##0.00")

            If dgvEstadoDocs.Rows.Count > 0 Then
                For x As Integer = 0 To dgvEstadoDocs.RowCount - 1
                    dgvEstadoDocs.Rows.Remove(dgvEstadoDocs.CurrentRow)
                Next
            End If

        ElseIf dtTable.Rows.Count > 0 Then
            dgvEstadoDocs.DataSource = dtTable
            For x As Integer = 0 To dgvEstadoDocs.Rows.Count - 1
                If Trim(dgvEstadoDocs.Rows(x).Cells("Column31").Value) = "01" Then
                    Dim StrCad As String = String.Empty
                    StrCad = Trim(dgvEstadoDocs.Rows(x).Cells("ImpDoc").Value)
                    If StrCad = Nothing Then
                        dblSolesDocs = dblSolesDocs + 0
                    Else
                        dblSolesDocs = dblSolesDocs + dgvEstadoDocs.Rows(x).Cells("ImpDoc").Value
                    End If
                ElseIf Trim(dgvEstadoDocs.Rows(x).Cells("Column31").Value) = "02" Then
                    Dim StrCad As String = String.Empty
                    StrCad = Trim(dgvEstadoDocs.Rows(x).Cells("ImpDoc").Value)
                    If StrCad = Nothing Then
                        dblDolaresDocs = dblDolaresDocs + 0
                    Else
                        dblDolaresDocs = dblDolaresDocs + dgvEstadoDocs.Rows(x).Cells("ImpDoc").Value
                    End If
                End If
                txtSol.Text = Format(Math.Round(dblSolesDocs, 2), "#,##0.00")
                txtDol.Text = Format(Math.Round(dblDolaresDocs, 2), "#,##0.00")
            Next
            BeLabel29.Text = "Total de Registros : " & dtTable.Rows.Count
        End If

    End Sub

    Private Sub chkDocCance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDocCance.Click
        chkDocCance.Checked = True
        chkDocSinCance.Checked = False
        txtBuscarEstadoDoc.Focus()
        txtBuscarEstadoDoc_TextChanged(sender, e)
        GroupBox11.Text = "Total Cancelados"
    End Sub

    Private Sub chkDocSinCance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDocSinCance.Click
        chkDocCance.Checked = False
        chkDocSinCance.Checked = True
        txtBuscarEstadoDoc.Focus()
        txtBuscarEstadoDoc_TextChanged(sender, e)
        GroupBox11.Text = "Total a Pagar"
    End Sub

    Private Sub rdbRuc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRuc.Click
        txtBuscarEstadoDoc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        txtBuscarEstadoDoc.MaxLength = 11
        txtBuscarEstadoDoc.Focus()
    End Sub

    Private Sub rdbNroDocumento_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbNroDocumento.Click
        txtBuscarEstadoDoc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        txtBuscarEstadoDoc.MaxLength = 20
        txtBuscarEstadoDoc.Focus()
    End Sub


    Private Sub chkDocCance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDocCance.CheckedChanged

    End Sub

    Private Sub chkDocSinCance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDocSinCance.CheckedChanged

    End Sub

    Private Sub btnLetra_ChangeUICues(ByVal sender As Object, ByVal e As System.Windows.Forms.UICuesEventArgs) Handles btnLetra.ChangeUICues

    End Sub

    Private Sub BeButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLetra.Click
        If Panel2.Visible = False Then
            CalcularLetra()
            If dgvDetalle.Rows.Count = 0 Then
                MessageBox.Show("No Existe Documentos para Asignar a una Letra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            ElseIf dgvDetalle.Rows.Count > 0 Then
                If CountDocumentosLetra = 0 Then
                    MessageBox.Show("Seleccione documentos para pagar con Letra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
            HabilitarLetra()
        ElseIf Panel2.Visible = True Then
            Panel2.Visible = False
        End If
    End Sub

    Sub HabilitarLetra()
        txtNroLetra.Clear()
        txtLugarGiro.Clear()
        'txtReferencia.Clear()
        'txtCanDocLetra.Clear()
        'txtTotalLetra.Clear()
        dtpFechaGiro.Value = Now.Date().ToShortDateString()
        dtpFechaVen.Value = Now.Date().ToShortDateString()
        txtNroLetra.Focus()
        Panel2.Visible = True
    End Sub

    Sub DesHabilitarLetra()
        txtNroLetra.Clear()
        txtLugarGiro.Clear()
        txtReferencia.Clear()
        txtCanDocLetra.Clear()
        txtTotalLetra.Clear()
        dtpFechaGiro.Value = Now.Date().ToShortDateString()
        dtpFechaVen.Value = Now.Date().ToShortDateString()
        Panel2.Visible = False
    End Sub
    
    Sub CalcularLetra()
        Dim strGlosaFacturasLetra As String = ""
        Dim dblTotalLetra As Double = 0
        CountDocumentosLetra = 0
        If dgvDetalle.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                If Convert.ToBoolean(dgvDetalle.Rows(x).Cells(20).Value) = True Then
                    Dim StrCad As String = String.Empty
                    StrCad = Trim(dgvDetalle.Rows(x).Cells(7).Value)
                    If StrCad = Nothing Then
                        dblTotalLetra = dblTotalLetra + 0
                    Else
                        dblTotalLetra = dblTotalLetra + dgvDetalle.Rows(x).Cells(7).Value
                    End If
                    CountDocumentosLetra = CountDocumentosLetra + 1
                    strGlosaFacturasLetra = strGlosaFacturasLetra & " " & dgvDetalle.Rows(x).Cells(0).Value & " " & dgvDetalle.Rows(x).Cells(1).Value & "  "
                End If
            Next
            'strGlosaFacturasLetra = strGlosaFacturasLetra
        End If
        txtCanDocLetra.Text = Trim(CountDocumentosLetra)
        txtReferencia.Text = Trim(strGlosaFacturasLetra)
        'txtTotSoles.Text = Format(dblSoles, "0.00")
        'txtTotDolares.Text = Format(dblDolares, "0.00")
        'txtTotalLetra.Text = Format(Math.Round(dblTotalLetra, 2), "#,##0.00")

        If Trim(cboMoneda.SelectedValue) = "01" Then
            'txtTotSoles.Text = Format(dblTotalLetra, "#,##0.00")
            txtTotalLetra.Text = Format(Math.Round(dblTotalLetra, 0), "#,##0.00")
        ElseIf Trim(cboMoneda.SelectedValue) = "02" Then
            'txtTotSoles.Text = Format(Math.Round(dblSoles, 0), "#,##0.00")
            txtTotalLetra.Text = Format(dblTotalLetra, "#,##0.00")
        End If



        'txtTotDolares.Text = Format(dblDolares, "#,##0.00")

        'dgvDetalle.CurrentCell = dgvDetalle(7, dgvDetalle.CurrentRow.Index)
    End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If dgvDetalle.Rows.Count > 0 Then
            Try
                dgvDetalle.CurrentCell = dgvDetalle(0, dgvDetalle.CurrentRow.Index)
                'Calcular()
                dgvDetalle.CurrentCell = dgvDetalle(columna, dgvDetalle.CurrentRow.Index)
            Catch ex As Exception
            End Try
            'Calcular()
        End If
        CalcularLetra()
    End Sub


    Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        CalcularLetra()
    End Sub

    Private Sub dgvDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        'If e.KeyCode = Keys.Space Then
        'dgvDetalle_MouseUp(sender, System.Windows.Forms.MouseEventArgs)
        'If dgvDetalle.Rows.Count > 0 Then
        '    Dim iFila As Integer
        '    CalcularLetra()
        '    If dgvDetalle.Rows.Count > 0 And dgvDetalle.CurrentCell.ColumnIndex = 20 Then
        '        iFila = dgvDetalle.CurrentRow.Index
        '        dgvDetalle.CurrentCell = dgvDetalle(0, iFila)
        '        dgvDetalle.CurrentCell = dgvDetalle(20, iFila)
        '    End If
        '    'CalcularLetra()
        'End If
        'End If
        CalcularLetra()
    End Sub

    Private Sub dgvDetalle_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetalle.KeyPress
        CalcularLetra()
    End Sub

    Private Sub dgvDetalle_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDetalle.MouseUp
        If dgvDetalle.Rows.Count > 0 Then
            Dim iFila As Integer
            CalcularLetra()
            If dgvDetalle.Rows.Count > 0 And dgvDetalle.CurrentCell.ColumnIndex = 20 Then
                iFila = dgvDetalle.CurrentRow.Index
                dgvDetalle.CurrentCell = dgvDetalle(0, 0)
                dgvDetalle.CurrentCell = dgvDetalle(20, iFila)
            End If
        End If
    End Sub

    Private Sub btnGrabarLetra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarLetra.Click
        ePagoProveedores = New clsPagoProveedores
        If dgvDetalle.Rows.Count > 0 Then
            CalcularLetra()
            Dim dtTableNumeroLetra As DataTable
            dtTableNumeroLetra = New DataTable
            Dim iResLetra As Integer = 0
            If CountDocumentosLetra > 0 Then
                dtTableNumeroLetra = ePagoProveedores.fTraerResExisteLetraProv(Trim(cboTipAnexos.SelectedValue), gEmpresa, Trim(cboAnexos.SelectedValue), Trim(txtNroLetra.Text))
                iResLetra = dtTableNumeroLetra.Rows.Count
            End If

            Dim IdLetra As String = ""
            If iResLetra = 0 Then
                If (MessageBox.Show("�Esta seguro de Grabar la Letra?", "Pago de Proveedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    ePagoProveedores.GenerarCodLetra(gEmpresa)
                    IdLetra = ePagoProveedores.sCodFuturo
                    Dim iResultado As Integer
                    iResultado = ePagoProveedores.GrabarLetra(25, IdLetra, Trim(txtNroLetra.Text), gEmpresa, dtpFechaGiro.Value, dtpFechaVen.Value, Trim(txtLugarGiro.Text), Trim(txtReferencia.Text), Trim(txtCanDocLetra.Text), txtTotalLetra.Text, txtTotalLetra.Text, cboTipAnexos.SelectedValue, cboAnexos.SelectedValue, cboResumenes.SelectedValue, gPeriodo, cboMoneda.SelectedValue, gUsuario)
                    If iResultado = 1 Then
                        If dgvDetalle.Rows.Count > 0 Then
                            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                                If Convert.ToBoolean(dgvDetalle.Rows(x).Cells(20).Value) = True Then
                                    'eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                    Dim IdRegistro As String = ""
                                    Dim IdCronograma As String = ""
                                    Dim Saldo As Double = 0
                                    IdRegistro = Trim(dgvDetalle.Rows(x).Cells(13).Value)
                                    IdCronograma = Trim(dgvDetalle.Rows(x).Cells(14).Value)
                                    'Saldo = Trim(dgvDetalle.Rows(x).Cells(16).Value)
                                    ePagoProveedores.ActSaldoDoc(26, IdRegistro, gEmpresa, 0, Trim(IdLetra), IdCronograma)
                                End If
                            Next
                        End If
                        MessageBox.Show("Se Grabo la Letra con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        cboMoneda_SelectedIndexChanged(sender, e)
                        DesHabilitarLetra()
                    End If
                End If
            ElseIf iResLetra = 1 Then

                If (MessageBox.Show("Ya Existe la Letra N� " & Trim(txtNroLetra.Text) & " para este Proveedor. �Esta seguro de incluir los documentos seleccionados y Actualizar la Letra a Pagar?", "Pago de Proveedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    Dim SaldoNew As Double = 0
                    Dim TotalNew As Double = 0
                    Dim CantidadNew As Integer = 0
                    Dim RefereNewLetra As String = ""
                    IdLetra = Trim(dtTableNumeroLetra.Rows(0).Item("IdLetra"))
                    TotalNew = dtTableNumeroLetra.Rows(0).Item("Total") + txtTotalLetra.Text
                    SaldoNew = dtTableNumeroLetra.Rows(0).Item("Saldo") + txtTotalLetra.Text
                    RefereNewLetra = Trim(dtTableNumeroLetra.Rows(0).Item("ReferenciaGiro")) & " " & Trim(txtReferencia.Text)
                    CantidadNew = dtTableNumeroLetra.Rows(0).Item("CantidadDocs") + txtCanDocLetra.Text

                    Dim iResultado As Integer
                    iResultado = ePagoProveedores.GrabarLetra(27, IdLetra, Trim(txtNroLetra.Text), gEmpresa, dtpFechaGiro.Value, dtpFechaVen.Value, Trim(txtLugarGiro.Text), RefereNewLetra, CantidadNew, TotalNew, SaldoNew, cboTipAnexos.SelectedValue, cboAnexos.SelectedValue, cboResumenes.SelectedValue, gPeriodo, cboMoneda.SelectedValue, gUsuario)
                    If iResultado = 1 Then
                        If dgvDetalle.Rows.Count > 0 Then
                            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                                If Convert.ToBoolean(dgvDetalle.Rows(x).Cells(20).Value) = True Then
                                    'eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                    Dim IdRegistro As String = ""
                                    Dim IdCronograma As String = ""
                                    Dim Saldo As Double = 0
                                    IdRegistro = Trim(dgvDetalle.Rows(x).Cells(13).Value)
                                    IdCronograma = Trim(dgvDetalle.Rows(x).Cells(14).Value)
                                    'Saldo = Trim(dgvDetalle.Rows(x).Cells(16).Value)
                                    ePagoProveedores.ActSaldoDoc(26, IdRegistro, gEmpresa, 0, Trim(IdLetra), IdCronograma)
                                End If
                            Next
                        End If
                        MessageBox.Show("Se Actualizo la Letra con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        cboMoneda_SelectedIndexChanged(sender, e)
                        DesHabilitarLetra()
                    End If
                End If

              
            End If


        End If
    End Sub

    Private eFunciones As Funciones
    Private Sub btnExportarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportarExcel.Click
        'Me.dgDatos.DataSource = dt
        Dim fichero As String = ""


        'SaveFileDialog1.Filter = "Excel(*.xls)|*.xls|"

        SaveFileDialog1.InitialDirectory = "C:\"
        SaveFileDialog1.Title = "Guardar Archivo"
        SaveFileDialog1.Filter = "Excel (*.xls) |*.xls;*.rtf|(*.txt) |*.txt|(*.*) |*.*"
        'SaveFileDialog1.Filter = "Excel (*.xlsx) |*.xlsx;*.rtf|(*.txt) |*.txt|(*.*) |*.*"

        'SaveFileDialog1.InitialDirectory = "C:\Transito\fotos\" xlsx

        If SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            fichero = SaveFileDialog1.FileName
            Try
                'Dim iExp As New Funciones
                'Dim iExp As New Funciones
                eFunciones = New Funciones

                Dim dtBusquedaX As DataTable
                dtBusquedaX = New DataTable
                dtBusquedaX = Funciones.ToTable(dgvDocumentosaEntregar, "Jesus")
                'eFunciones.DataTableToExcel(CType(Me.dgvGastos.DataSource, DataTable))
                'eFunciones.DataTableToExcel(dtBusqueda, fichero)
                eFunciones.DataTableToExcel(dtBusquedaX, fichero)

                'Dim miDataTableModificado As DataTable
                'miDataTableModificado = New DataTable
                'miDataTableModificado = CType(dgvGastos.DataSource, DataTable)
                'eFunciones.DataTableToExcel(miDataTableModificado, fichero)


                dtBusquedaX.Dispose()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            'Else
        End If
    End Sub

    Private Sub cboCriterio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCriterio.SelectedIndexChanged
        If cboCriterio.SelectedIndex = 0 Then
            lblIngrese.Text = "Ingrese el n�mero : "
            txtCadenaBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
            txtCadenaBusqueda.MaxLength = 20
        ElseIf cboCriterio.SelectedIndex = 1 Then
            lblIngrese.Text = "Ingrese el n�mero : "
            txtCadenaBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
            txtCadenaBusqueda.MaxLength = 10
        ElseIf cboCriterio.SelectedIndex = 2 Then
            lblIngrese.Text = "Ingrese el RUC : "
            txtCadenaBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
            txtCadenaBusqueda.MaxLength = 11
        ElseIf cboCriterio.SelectedIndex = 3 Then
            lblIngrese.Text = "Ingrese la Raz�n : "
            txtCadenaBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
            txtCadenaBusqueda.MaxLength = 100
        End If
    End Sub

    Private Sub rdbRazon_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRazon.Click
        txtBuscarEstadoDoc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        txtBuscarEstadoDoc.MaxLength = 100
        txtBuscarEstadoDoc.Focus()
    End Sub

    Private Sub cboTipoAnexo_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoAnexo.SelectionChangeCommitted
        Dim IdTipoAnexo As String = ""
        If cboTipoAnexo.SelectedIndex > -1 Then
            Try
                IdTipoAnexo = Trim(cboTipoAnexo.SelectedValue.ToString)
                If IdTipoAnexo <> "System.Data.DataRowView" Then
                    ePagoProveedores = New clsPagoProveedores
                    cboAnexo.DataSource = ePagoProveedores.fListarProveedoresPorTipo(gEmpresa, Trim(IdTipoAnexo))
                    cboAnexo.ValueMember = "CodProveedor"
                    cboAnexo.DisplayMember = "NombreProveedor"
                    cboAnexo.SelectedIndex = -1
                    If ePagoProveedores.iNroRegistros = 0 Then
                        txtDireccion.Text = ""
                        txtCodigo.Text = ""
                        txtSaldoSoles.Text = ""
                        txtSaldoDolares.Text = ""
                        txtNroDoc.Text = ""
                        txtTotSoles.Text = ""
                        txtTotDolares.Text = ""
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                        Next
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboAnexo.DataSource = Nothing
        End If
        cboTipoAnexo.Focus()
    End Sub

    Private Sub txtLineaCreSoles_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLineaCreSoles.TextChanged

    End Sub
End Class
