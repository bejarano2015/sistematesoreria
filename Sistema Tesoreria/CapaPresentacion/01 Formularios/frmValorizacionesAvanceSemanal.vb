Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports System.Data

Public Class frmValorizacionesAvanceSemanal

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmValorizacionesAvanceSemanal = Nothing
    Public Shared Function Instance() As frmValorizacionesAvanceSemanal
        If frmInstance Is Nothing Then
            frmInstance = New frmValorizacionesAvanceSemanal
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmValorizacionesAvanceSemanal_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eValorizaciones As clsValorizaciones
    Public sxIdValorizacion As String = ""
    Public sxCC As String = ""
    Public F1 As DateTime
    Public F2 As DateTime

    Public EtiquetaLunes As String = ""
    Public EtiquetaMartes As String = ""
    Public EtiquetaMiercoles As String = ""
    Public EtiquetaJueves As String = ""
    Public EtiquetaViernes As String = ""
    Public EtiquetaSabado As String = ""
    Public EtiquetaDomingo As String = ""

    Private Sub frmValorizacionesAvanceSemanal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Text = "Avance Semanal del " & F1 & " al " & F2

        If Len(Trim(sxIdValorizacion)) > 0 Then
            eValorizaciones = New clsValorizaciones
            Dim dtValorizaciones As DataTable
            dtValorizaciones = New DataTable
            'dtValorizaciones = eValorizaciones.fBuscarValorizacion(71, gEmpresa, gPeriodo, "", Trim(sxIdValorizacion), Today())

            dtValorizaciones = eValorizaciones.fBuscarValorizacion(71, gEmpresa, Trim(sxIdValorizacion), 0, F1, F2, gPeriodo, Trim(sxCC))

            Lunes.HeaderText = EtiquetaLunes
            Martes.HeaderText = EtiquetaMartes
            Miercoles.HeaderText = EtiquetaMiercoles
            Jueves.HeaderText = EtiquetaJueves
            Viernes.HeaderText = EtiquetaViernes
            Sabado.HeaderText = EtiquetaSabado
            Domingo.HeaderText = EtiquetaDomingo

            'Lunes.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'Martes.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'Miercoles.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'Jueves.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'Viernes.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'Sabado.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'Domingo.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

            If dtValorizaciones.Rows.Count > 0 Then
                'dgvHistorial.DataSource = dtValorizaciones
                For y As Integer = 0 To dtValorizaciones.Rows.Count - 1
                    dgvHistorial.Rows.Add()
                    dgvHistorial.Rows(y).Cells("Descripcion").Value = dtValorizaciones.Rows(y).Item("Descripcion").ToString
                    dgvHistorial.Rows(y).Cells("UnidMed").Value = dtValorizaciones.Rows(y).Item("UnidMed").ToString
                    dgvHistorial.Rows(y).Cells("MetradoImpor").Value = dtValorizaciones.Rows(y).Item("MetradoImpor").ToString  '"0"
                    dgvHistorial.Rows(y).Cells("Lunes").Value = dtValorizaciones.Rows(y).Item("Lunes").ToString
                    dgvHistorial.Rows(y).Cells("Martes").Value = dtValorizaciones.Rows(y).Item("Martes").ToString
                    dgvHistorial.Rows(y).Cells("Miercoles").Value = dtValorizaciones.Rows(y).Item("Miercoles").ToString
                    dgvHistorial.Rows(y).Cells("Jueves").Value = dtValorizaciones.Rows(y).Item("Jueves").ToString
                    dgvHistorial.Rows(y).Cells("Viernes").Value = dtValorizaciones.Rows(y).Item("Viernes").ToString
                    dgvHistorial.Rows(y).Cells("Sabado").Value = dtValorizaciones.Rows(y).Item("Sabado").ToString
                    dgvHistorial.Rows(y).Cells("Domingo").Value = dtValorizaciones.Rows(y).Item("Domingo").ToString
                    dgvHistorial.Rows(y).Cells("Total").Value = dtValorizaciones.Rows(y).Item("Total").ToString
                Next
            ElseIf dtValorizaciones.Rows.Count = 0 Then
                'Me.Hide()
                'CapaPresentacionTesoreria.frmValorizacionesAvanceSemanal.Hide()
            End If
            dgvHistorial.Focus()
            'dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
        End If
    End Sub

    Private Sub dgvHistorial_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistorial.CellContentClick

    End Sub

    Private Sub dgvHistorial_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvHistorial.KeyDown
        If dgvHistorial.Rows.Count > 0 Then
            Select Case e.KeyCode
                Case Keys.Escape
                    Me.Hide()
            End Select
        End If
    End Sub
End Class