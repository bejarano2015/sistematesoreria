Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows
Imports System.Data.SqlClient

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data
'Imports System.Data.SqlClient

Public Class frmConsultaRetenciones

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaRetenciones = Nothing
    Public Shared Function Instance() As frmConsultaRetenciones
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaRetenciones
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmConsultaRetenciones_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private da As SqlDataAdapter
    Private eBusquedaRetenciones As clsBusquedaRetenciones
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Private eTempo As clsPlantTempo
    Dim ir As String = ""

    Dim AplicarPintado As Integer = 0
    Public hace As Integer = 0
    Private Sub frmConsultaRetenciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ir = "NO"
        dtpFechaFin.Text = Now.Date()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        ir = "SI"
        dtpFechaIni_TextChanged(sender, e)
        Timer2.Enabled = True

        For x As Integer = 0 To dgvGastos.Columns.Count - 1
            'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
            dgvGastos.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
        Next
        AplicarPintado = 1
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        If Timer2.Enabled = True Then
            cboCriterio.Focus()
        End If
        Timer2.Enabled = False
    End Sub

    Private Sub dtpFechaIni_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFechaIni.TextChanged
        If Len(Trim(txtTexto.Text)) > 0 Then
            'txtTexto_TextChanged(sender, e)
        Else
            If dtpFechaFin.Value < dtpFechaIni.Value Then
                MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dtpFechaIni.Focus()
                Exit Sub
            End If
            If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCriterio.Focus()
                Exit Sub
            End If
            Dim dt As DataTable
            dt = New DataTable
            If ir = "SI" Then
                Dim dtBusqueda As DataTable
                eBusquedaRetenciones = New clsBusquedaRetenciones
                dtBusqueda = New DataTable
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
                dgvGastos.DataSource = dtBusqueda
                Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
                Calcular()
            End If
        End If
    End Sub

    Sub Calcular()
        Dim Mon As String = ""
        Dim Importe As Double = 0
        Dim ImporteSoles As Double = 0
        Dim ImporteDolares As Double = 0
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                Importe = Convert.ToDouble(Trim(dgvGastos.Rows(x).Cells("Importe").Value))
                'If Trim(dgvGastos.Rows(x).Cells("MonCod").Value) = "01" Then
                If Importe = Nothing Then
                    ImporteSoles = ImporteSoles + 0
                Else
                    ImporteSoles = ImporteSoles + Importe
                End If
                'ElseIf Trim(dgvGastos.Rows(x).Cells("MonCod").Value) = "02" Then
                'If Importe = Nothing Then
                'ImporteDolares = ImporteDolares + 0
                'Else
                'ImporteDolares = ImporteDolares + Importe
                'End If
                'End If

                
                If Trim(dgvGastos.Rows(x).Cells("xEstado").Value) = "ACTIVO" Then
                    gestionaResaltados(dgvGastos, x, Color.Aqua)
                    If Trim(dgvGastos.Rows(x).Cells("Prestamo").Value) = "1" Then
                        gestionaResaltados2(dgvGastos, x, Color.Silver)
                    End If
                ElseIf Trim(dgvGastos.Rows(x).Cells("xEstado").Value) = "ANULADO" Then
                    gestionaResaltados(dgvGastos, x, Color.LightCoral)
                    If Trim(dgvGastos.Rows(x).Cells("Prestamo").Value) = "1" Then
                        gestionaResaltados2(dgvGastos, x, Color.Silver)
                    End If
                    'ElseIf Trim(dgvGastos.Rows(x).Cells("Prestamo").Value) = "1" Then
                    '    gestionaResaltados(dgvGastos, x, Color.Silver)
                End If
            Next
            txtSoles.Text = Format(ImporteSoles, "#,##0.00")
            'txtDolares.Text = Format(ImporteDolares, "#,##0.00")
        ElseIf dgvGastos.Rows.Count = 0 Then
            txtSoles.Text = Format(0, "#,##0.00")
            'txtDolares.Text = Format(0, "#,##0.00")
        End If
    End Sub

    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(8).Style.BackColor = c
        visor.Rows(fila).Cells(9).Style.BackColor = c
        visor.Rows(fila).Cells(10).Style.BackColor = c
        visor.Rows(fila).Cells(11).Style.BackColor = c
        visor.Rows(fila).Cells(12).Style.BackColor = c
        visor.Rows(fila).Cells(13).Style.BackColor = c
        visor.Rows(fila).Cells(14).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        visor.Rows(fila).Cells(16).Style.BackColor = c
        visor.Rows(fila).Cells(17).Style.BackColor = c
        visor.Rows(fila).Cells(18).Style.BackColor = c
        'visor.Rows(fila).Cells(19).Style.BackColor = c
    End Sub

    Private Sub gestionaResaltados2(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        'visor.Rows(fila).Cells("Anular").Style.BackColor = c
        'visor.Rows(fila).Cells("Moneda").Style.BackColor = c
        'visor.Rows(fila).Cells("Column7").Style.BackColor = c
        'visor.Rows(fila).Cells("Column8").Style.BackColor = c
        'visor.Rows(fila).Cells("CCosto").Style.BackColor = c
        'visor.Rows(fila).Cells("Column4").Style.BackColor = c
        'visor.Rows(fila).Cells("Column3").Style.BackColor = c
        'visor.Rows(fila).Cells("MonCod").Style.BackColor = c
        'visor.Rows(fila).Cells("IdRetencion").Style.BackColor = c
        'visor.Rows(fila).Cells("Prestamo").Style.BackColor = c
        'visor.Rows(fila).Cells("EmpPrestataria").Style.BackColor = c

        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(8).Style.BackColor = c
        visor.Rows(fila).Cells(9).Style.BackColor = c
        visor.Rows(fila).Cells(10).Style.BackColor = c
        visor.Rows(fila).Cells(11).Style.BackColor = c
        visor.Rows(fila).Cells(12).Style.BackColor = c
        visor.Rows(fila).Cells(13).Style.BackColor = c
        visor.Rows(fila).Cells(14).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        visor.Rows(fila).Cells(16).Style.BackColor = c
        visor.Rows(fila).Cells(17).Style.BackColor = c
        visor.Rows(fila).Cells(18).Style.BackColor = c

    End Sub

    Private Sub txtTexto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTexto.TextChanged
        'If Len(Trim(txtTexto.Text)) > 0 Then
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        If cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCriterio.Focus()
            Exit Sub
        End If
        Dim dtBusqueda As DataTable
        eBusquedaRetenciones = New clsBusquedaRetenciones
        dtBusqueda = New DataTable
        If Len(txtTexto.Text) > 0 Then
            If cboCriterio.Text = "RUC" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(1, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "RAZON SOCIAL" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "NUMERO" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(3, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "CONCEPTO" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(4, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "RESPONSABLE" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(5, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "CHEQUE" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(6, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "CARTA" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(7, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "TRANSFERENCIA" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(8, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            End If
        ElseIf Len(txtTexto.Text) = 0 Then
            dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
        End If
        dgvGastos.DataSource = dtBusqueda
        Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
        Calcular()
        'End If
    End Sub

    Private Sub dtpFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        'txtTexto_TextChanged(sender, e)
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        'txtTexto_TextChanged(sender, e)

        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        'If cboCriterio.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    cboCriterio.Focus()
        '    Exit Sub
        'End If
        Dim dtBusqueda As DataTable
        eBusquedaRetenciones = New clsBusquedaRetenciones
        dtBusqueda = New DataTable
        If Len(txtTexto.Text) > 0 Then
            If cboCriterio.Text = "RUC" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(1, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "RAZON SOCIAL" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "NUMERO" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(3, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "CONCEPTO" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(4, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "RESPONSABLE" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(5, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "CHEQUE" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(6, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "CARTA" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(7, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            ElseIf cboCriterio.Text = "TRANSFERENCIA" Then
                dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(8, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            End If
        ElseIf Len(txtTexto.Text) = 0 Then
            dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
        End If
        dgvGastos.DataSource = dtBusqueda
        Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
        Calcular()

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        Try
            'Next
            Dim dtTable2 As DataTable
            Dim pIdRetencion As String = ""
            Dim pSerie As String = ""
            dtTable2 = New DataTable
            eBusquedaRetenciones = New clsBusquedaRetenciones

            If dgvGastos.Rows.Count > 0 Then
                pIdRetencion = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("IdRetencion").Value
                pSerie = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("xSerie").Value
                dtTable2 = eBusquedaRetenciones.fConsultaRetenciones(9, Trim(pIdRetencion), gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, Trim(pSerie))
            End If

            If dtTable2.Rows.Count > 0 Then
                Dim MontoLetras As String = ""
                'Dim MontoRet As Double = 0
                'MontoRet = IIf(Microsoft.VisualBasic.IsDBNull(dtTable2.Rows(0).Item("PorcentajeRetCab")), 0, dtTable2.Rows(0).Item("PorcentajeRetCab"))
                'Dim dtTableLetras As DataTable
                'dtTableLetras = New DataTable
                'eBusquedaRetenciones = New clsBusquedaRetenciones
                'dtTableLetras = eBusquedaRetenciones.fConsultaMontoLetras(MontoRet)
                'If dtTableLetras.Rows.Count > 0 Then
                '    MontoLetras = IIf(Microsoft.VisualBasic.IsDBNull(dtTableLetras.Rows(0).Item(0)), "", dtTableLetras.Rows(0).Item(0))
                'End If
                'Muestra_Reporte(STRRUTAREPORTES & "rptConsolidado.rpt", dtTable2, "TblLibroDiario", "", "StrEmpresa;" & cCapaFunctGen.DescripcionEmpresa(sEmpresa), "StrRUC;" & cCapaFunctGen.RUCempresa(sEmpresa), "StrPeriodo;" & "MES DE " & cboMes.Text & " DE " & sPeriodo, "StrDesMoneda;" & cboMoneda.Text)

                'Dim RutaApp As String = ""
                'RutaApp = My.Application.Info.DirectoryPath
                'If Not RutaApp.EndsWith("\") Then
                '    RutaApp &= "\Reportes\"
                'End If


                Muestra_Reporte(RutaAppReportes & "rptRetencion.rpt", dtTable2, "TblLibroDiario", "", "MontoLetras;" & MontoLetras)
                'Imprimir_Reporte(RutaAppReportes & "rptRetencion.rpt", dtTable2, "TblLibroDiario", "", "MontoLetras;" & MontoLetras)

            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Public Sub Imprimir_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            'Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                'f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            'f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            'f.CRVisor.ReportSource = myReporte
            'f.CRVisor.DisplayGroupTree = False
            'f.Show()

            myReporte.PrintToPrinter(1, False, 0, 1)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub btnAnular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnular.Click
        Dim ContaAnulados As Integer = 0
        If dgvGastos.Rows.Count > 0 Then

            For z As Integer = 0 To dgvGastos.Rows.Count - 1
                If Convert.ToBoolean(dgvGastos.Rows(z).Cells("Anular").Value) = True Then
                    ContaAnulados = ContaAnulados + 1
                End If
            Next

            Dim mensaje As String = ""
            If ContaAnulados = 1 Then
                mensaje = "la Retencion Seleccionada?"
            ElseIf ContaAnulados > 1 Then
                mensaje = "las Retenciones Seleccionadas?"
            ElseIf ContaAnulados = 0 Then
                MessageBox.Show("Seleccione Registros para Anular", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            If (MessageBox.Show("�Est� seguro de Anular " & mensaje, "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                For z As Integer = 0 To dgvGastos.Rows.Count - 1
                    If Convert.ToBoolean(dgvGastos.Rows(z).Cells("Anular").Value) = True Then
                        'ContaAnulados = ContaAnulados + 1
                        eMovimientoCajaBanco = New clsMovimientoCajaBanco
                        Dim pIdRetencion As String = ""
                        Dim pSerie As String = ""
                        Dim iGrabarRetencion As Integer = 0
                        pIdRetencion = Trim(dgvGastos.Rows(z).Cells("IdRetencion").Value)
                        pSerie = Trim(dgvGastos.Rows(z).Cells("xSerie").Value)
                        iGrabarRetencion = eMovimientoCajaBanco.fGrabarRetencion(2, Trim(pIdRetencion), gEmpresa, Trim(pSerie), "", Today(), "", "", "", "", 0, PorcentajeRetencion, 0 * (PorcentajeRetencion / 100), 0, 0 - (0 * (PorcentajeRetencion / 100)), 0, gUsuario, "")
                    End If
                Next
                If ContaAnulados = 1 Then
                    MessageBox.Show("Se Anul� " & ContaAnulados & " un Comprobante de Retenci�n", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    dtpFechaFin_ValueChanged(sender, e)
                ElseIf ContaAnulados > 1 Then
                    MessageBox.Show("Se Anularon " & ContaAnulados & " Comprobantes de Retenci�n", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    dtpFechaFin_ValueChanged(sender, e)
                ElseIf ContaAnulados = 0 Then
                    MessageBox.Show("Seleccione Registros para Anular", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
    End Sub



    Private Sub dgvGastos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvGastos.Click
        If AplicarPintado = 1 Then
            Calcular()
            AplicarPintado = AplicarPintado + 1
        End If
    End Sub

    Private Sub dgvGastos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGastos.CellContentClick

    End Sub

    Private Sub dgvGastos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvGastos.KeyDown
        Select Case e.KeyCode




            Case Keys.F5
                '''''''''''
                If dgvGastos.Rows.Count > 0 Then

                    'If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value) = "" Then
                    '    MessageBox.Show("Este Registro no esta grabado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    '    Exit Sub
                    'End If
                    'If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "" Then
                    '    MessageBox.Show("Este Registro no es un Comprobante", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    '    Exit Sub
                    'End If
                    'If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value) = "" Then
                    '    MessageBox.Show("Este Comprobante no tiene Numero", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    '    Exit Sub
                    'End If

                    'eCompraRequerimientos = New clsCompraRequerimientos
                    'Dim dtDatosEnReq As DataTable
                    'dtDatosEnReq = New DataTable
                    'dtDatosEnReq = eCompraRequerimientos.fListar2(10, gEmpresa, "", "", "", Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value), Trim(txtCodigo.Text))
                    'If dtDatosEnReq.Rows.Count > 0 Then
                    '    MessageBox.Show("Este Comprobante ya esta enlazado con una compra de Requerimientos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    '    Exit Sub
                    'End If
                    If dgvGastos.Rows.Count > 0 Then
                        'IdResumen
                        If Len(Trim(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column2").Value)) > 0 Then
                            If Trim(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column2").Value) = "ANULADO" Then
                                Exit Sub
                            End If
                        End If

                        'IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdMovimiento").ToString) = True, "", dtTable.Rows(y).Item("IdMovimiento").ToString)

                        If Len(Trim(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("IdResumen").Value)) > 0 Then

                            MessageBox.Show("Esta Retenci�n tiene Detalle de Comprobantes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)

                        ElseIf Len(Trim(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("IdResumen").Value)) = 0 Then
                            hace = 0
                            Dim x As frmConsultaRetencionesDetallar = frmConsultaRetencionesDetallar.Instance
                            x.Owner = Me
                            'x.NroForm = 1
                            'x.IdCaja = Trim(txtCodigo.Text) 'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value
                            x.NumeroRet = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column11").Value
                            x.FechaMov = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Fecha").Value
                            x.MonedaMov = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("MonCod").Value
                            x.RucMov = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column1").Value
                            x.IdMov = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("IdMovimiento").Value
                            x.IdRet = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("IdRetencion").Value
                            x.xxSerie = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("xSerie").Value
                            x.xIdCheque = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("IdCheque").Value
                            x.MontoPagado = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Importe").Value
                            x.MontoRet = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Monto").Value
                            'x.TipoDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
                            'x.FechaFac = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
                            'x.RucDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column7").Value
                            'x.NroDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value
                            'x.RazonDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value
                            'x.MonedaDoc = Trim(sMonedaCaja)
                            x.ShowInTaskbar = False
                            x.ShowDialog()

                            If hace = 1 Then
                                dtpFechaFin_ValueChanged(sender, e)
                            End If

                        End If

                    End If


                End If

            Case Keys.F6

                'If dgvDetalleCajas.Rows.Count > 0 Then
                '    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value) = "" Then
                '        MessageBox.Show("Este Registro no esta grabado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '        Exit Sub
                '    End If
                '    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "" Then
                '        MessageBox.Show("Este Registro no es un Comprobante", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '        Exit Sub
                '    End If
                '    If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value) = "" Then
                '        MessageBox.Show("Este Comprobante no tiene Numero", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '        Exit Sub
                '    End If


                '    eCompraRequerimientos = New clsCompraRequerimientos
                '    Dim dtDatosEnReq As DataTable
                '    dtDatosEnReq = New DataTable
                '    dtDatosEnReq = eCompraRequerimientos.fListar2(12, gEmpresa, "", "", "", Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value), Trim(txtCodigo.Text))
                '    If dtDatosEnReq.Rows.Count = 0 Then
                '        MessageBox.Show("Este Comprobante no tiene Detalle de Requerimiento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '        Exit Sub
                '    End If

                '    Dim x As frmCajaChicaRequerimientos3 = frmCajaChicaRequerimientos3.Instance
                '    x.Owner = Me

                '    x.IdCaja = Trim(txtCodigo.Text) 'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value
                '    x.IdDetCaja = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value

                '    x.TipoDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
                '    x.FechaFac = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
                '    x.RucDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column7").Value
                '    x.NroDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value
                '    x.RazonDoc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value
                '    x.MonedaDoc = Trim(sMonedaCaja)
                '    x.ShowInTaskbar = False
                '    x.ShowDialog()

                'End If



        End Select

    End Sub
End Class