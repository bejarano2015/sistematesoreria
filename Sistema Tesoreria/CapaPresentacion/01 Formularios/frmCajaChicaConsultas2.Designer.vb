<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCajaChicaConsultas2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboCaja = New ctrLibreria.Controles.BeComboBox
        Me.cboArea = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.txtAnio = New ctrLibreria.Controles.BeTextBox
        Me.lstCajas = New System.Windows.Forms.ListBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.txtTotSaldo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.txtAporteGeneral = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaInicio = New ctrLibreria.Controles.BeDateTimePicker
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.txtTotGasto = New ctrLibreria.Controles.BeTextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dtpFechaPago = New ctrLibreria.Controles.BeDateTimePicker
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.txtTotPago = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.txtNumMov = New ctrLibreria.Controles.BeTextBox
        Me.txtFormaPago = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.txtEmpresa = New ctrLibreria.Controles.BeTextBox
        Me.txtBanco = New ctrLibreria.Controles.BeTextBox
        Me.dgvDetalleCajas = New System.Windows.Forms.DataGridView
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TipoDocumento = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdGasto = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.Glosa = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OtrosD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CentroCosto = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdDetalleMovimiento = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Opcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Anular = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Desglozado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Empresas = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.btnEliminar = New ctrLibreria.Controles.BeButton
        Me.btnGrabar = New ctrLibreria.Controles.BeButton
        Me.btnCerrar = New ctrLibreria.Controles.BeButton
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvDetalleCajas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboCaja
        '
        Me.cboCaja.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCaja.BackColor = System.Drawing.Color.Ivory
        Me.cboCaja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCaja.ForeColor = System.Drawing.Color.Black
        Me.cboCaja.FormattingEnabled = True
        Me.cboCaja.KeyEnter = True
        Me.cboCaja.Location = New System.Drawing.Point(118, 39)
        Me.cboCaja.Name = "cboCaja"
        Me.cboCaja.Size = New System.Drawing.Size(321, 21)
        Me.cboCaja.TabIndex = 8
        '
        'cboArea
        '
        Me.cboArea.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboArea.BackColor = System.Drawing.Color.Ivory
        Me.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArea.ForeColor = System.Drawing.Color.Black
        Me.cboArea.FormattingEnabled = True
        Me.cboArea.KeyEnter = True
        Me.cboArea.Location = New System.Drawing.Point(118, 12)
        Me.cboArea.Name = "cboArea"
        Me.cboArea.Size = New System.Drawing.Size(321, 21)
        Me.cboArea.TabIndex = 7
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(13, 15)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel4.TabIndex = 5
        Me.BeLabel4.Text = "Area/Obra"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(13, 43)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(87, 13)
        Me.BeLabel2.TabIndex = 6
        Me.BeLabel2.Text = "Descripción Caja"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(115, 73)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(43, 13)
        Me.BeLabel3.TabIndex = 9
        Me.BeLabel3.Text = "Periodo"
        '
        'txtAnio
        '
        Me.txtAnio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAnio.BackColor = System.Drawing.Color.Ivory
        Me.txtAnio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAnio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAnio.ForeColor = System.Drawing.Color.Black
        Me.txtAnio.KeyEnter = True
        Me.txtAnio.Location = New System.Drawing.Point(164, 71)
        Me.txtAnio.Name = "txtAnio"
        Me.txtAnio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAnio.ShortcutsEnabled = False
        Me.txtAnio.Size = New System.Drawing.Size(100, 20)
        Me.txtAnio.TabIndex = 10
        Me.txtAnio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'lstCajas
        '
        Me.lstCajas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstCajas.FormattingEnabled = True
        Me.lstCajas.Location = New System.Drawing.Point(118, 98)
        Me.lstCajas.Name = "lstCajas"
        Me.lstCajas.Size = New System.Drawing.Size(146, 158)
        Me.lstCajas.TabIndex = 11
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(115, 261)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(43, 13)
        Me.BeLabel1.TabIndex = 12
        Me.BeLabel1.Text = "Periodo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.BeLabel15)
        Me.GroupBox2.Controls.Add(Me.txtTotSaldo)
        Me.GroupBox2.Controls.Add(Me.BeLabel14)
        Me.GroupBox2.Controls.Add(Me.txtAporteGeneral)
        Me.GroupBox2.Controls.Add(Me.BeLabel13)
        Me.GroupBox2.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox2.Controls.Add(Me.dtpFechaInicio)
        Me.GroupBox2.Controls.Add(Me.BeLabel6)
        Me.GroupBox2.Controls.Add(Me.BeLabel5)
        Me.GroupBox2.Controls.Add(Me.txtTotGasto)
        Me.GroupBox2.Location = New System.Drawing.Point(287, 71)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(202, 187)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos Caja"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(6, 135)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel15.TabIndex = 21
        Me.BeLabel15.Text = "Saldo a Rendir"
        '
        'txtTotSaldo
        '
        Me.txtTotSaldo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotSaldo.BackColor = System.Drawing.Color.Ivory
        Me.txtTotSaldo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotSaldo.ForeColor = System.Drawing.Color.Black
        Me.txtTotSaldo.KeyEnter = True
        Me.txtTotSaldo.Location = New System.Drawing.Point(100, 128)
        Me.txtTotSaldo.Name = "txtTotSaldo"
        Me.txtTotSaldo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotSaldo.ShortcutsEnabled = False
        Me.txtTotSaldo.Size = New System.Drawing.Size(87, 20)
        Me.txtTotSaldo.TabIndex = 20
        Me.txtTotSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotSaldo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(6, 80)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel14.TabIndex = 19
        Me.BeLabel14.Text = "Total Base Caja"
        '
        'txtAporteGeneral
        '
        Me.txtAporteGeneral.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAporteGeneral.BackColor = System.Drawing.Color.Ivory
        Me.txtAporteGeneral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAporteGeneral.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAporteGeneral.ForeColor = System.Drawing.Color.Black
        Me.txtAporteGeneral.KeyEnter = True
        Me.txtAporteGeneral.Location = New System.Drawing.Point(100, 73)
        Me.txtAporteGeneral.Name = "txtAporteGeneral"
        Me.txtAporteGeneral.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAporteGeneral.ShortcutsEnabled = False
        Me.txtAporteGeneral.Size = New System.Drawing.Size(87, 20)
        Me.txtAporteGeneral.TabIndex = 18
        Me.txtAporteGeneral.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAporteGeneral.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(6, 52)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(88, 13)
        Me.BeLabel13.TabIndex = 17
        Me.BeLabel13.Text = "Fecha Rendición"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(100, 45)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(87, 20)
        Me.dtpFechaFin.TabIndex = 15
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaInicio.CustomFormat = ""
        Me.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicio.KeyEnter = True
        Me.dtpFechaInicio.Location = New System.Drawing.Point(100, 19)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(87, 20)
        Me.dtpFechaInicio.TabIndex = 16
        Me.dtpFechaInicio.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaInicio.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(6, 106)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(86, 13)
        Me.BeLabel6.TabIndex = 14
        Me.BeLabel6.Text = "Total Gasto Caja"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(6, 26)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(80, 13)
        Me.BeLabel5.TabIndex = 13
        Me.BeLabel5.Text = "Fecha Apertura"
        '
        'txtTotGasto
        '
        Me.txtTotGasto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotGasto.BackColor = System.Drawing.Color.Ivory
        Me.txtTotGasto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotGasto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotGasto.ForeColor = System.Drawing.Color.Black
        Me.txtTotGasto.KeyEnter = True
        Me.txtTotGasto.Location = New System.Drawing.Point(100, 99)
        Me.txtTotGasto.Name = "txtTotGasto"
        Me.txtTotGasto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotGasto.ShortcutsEnabled = False
        Me.txtTotGasto.Size = New System.Drawing.Size(87, 20)
        Me.txtTotGasto.TabIndex = 12
        Me.txtTotGasto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotGasto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpFechaPago)
        Me.GroupBox1.Controls.Add(Me.BeLabel11)
        Me.GroupBox1.Controls.Add(Me.BeLabel12)
        Me.GroupBox1.Controls.Add(Me.txtTotPago)
        Me.GroupBox1.Controls.Add(Me.BeLabel9)
        Me.GroupBox1.Controls.Add(Me.BeLabel10)
        Me.GroupBox1.Controls.Add(Me.txtNumMov)
        Me.GroupBox1.Controls.Add(Me.txtFormaPago)
        Me.GroupBox1.Controls.Add(Me.BeLabel7)
        Me.GroupBox1.Controls.Add(Me.BeLabel8)
        Me.GroupBox1.Controls.Add(Me.txtEmpresa)
        Me.GroupBox1.Controls.Add(Me.txtBanco)
        Me.GroupBox1.Location = New System.Drawing.Point(495, 71)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(202, 187)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Gastos"
        '
        'dtpFechaPago
        '
        Me.dtpFechaPago.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaPago.CustomFormat = ""
        Me.dtpFechaPago.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaPago.KeyEnter = True
        Me.dtpFechaPago.Location = New System.Drawing.Point(92, 124)
        Me.dtpFechaPago.Name = "dtpFechaPago"
        Me.dtpFechaPago.Size = New System.Drawing.Size(100, 20)
        Me.dtpFechaPago.TabIndex = 23
        Me.dtpFechaPago.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaPago.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(6, 154)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(59, 13)
        Me.BeLabel11.TabIndex = 22
        Me.BeLabel11.Text = "Total Pago"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(6, 128)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(65, 13)
        Me.BeLabel12.TabIndex = 21
        Me.BeLabel12.Text = "Fecha Pago"
        '
        'txtTotPago
        '
        Me.txtTotPago.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotPago.BackColor = System.Drawing.Color.Ivory
        Me.txtTotPago.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotPago.ForeColor = System.Drawing.Color.Black
        Me.txtTotPago.KeyEnter = True
        Me.txtTotPago.Location = New System.Drawing.Point(92, 152)
        Me.txtTotPago.Name = "txtTotPago"
        Me.txtTotPago.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotPago.ShortcutsEnabled = False
        Me.txtTotPago.Size = New System.Drawing.Size(100, 20)
        Me.txtTotPago.TabIndex = 20
        Me.txtTotPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotPago.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(6, 99)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(59, 13)
        Me.BeLabel9.TabIndex = 18
        Me.BeLabel9.Text = "Num. Mov."
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(6, 73)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(79, 13)
        Me.BeLabel10.TabIndex = 17
        Me.BeLabel10.Text = "Forma de Pago"
        '
        'txtNumMov
        '
        Me.txtNumMov.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumMov.BackColor = System.Drawing.Color.Ivory
        Me.txtNumMov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumMov.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumMov.ForeColor = System.Drawing.Color.Black
        Me.txtNumMov.KeyEnter = True
        Me.txtNumMov.Location = New System.Drawing.Point(92, 97)
        Me.txtNumMov.Name = "txtNumMov"
        Me.txtNumMov.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumMov.ShortcutsEnabled = False
        Me.txtNumMov.Size = New System.Drawing.Size(100, 20)
        Me.txtNumMov.TabIndex = 16
        Me.txtNumMov.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'txtFormaPago
        '
        Me.txtFormaPago.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFormaPago.BackColor = System.Drawing.Color.Ivory
        Me.txtFormaPago.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFormaPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFormaPago.ForeColor = System.Drawing.Color.Black
        Me.txtFormaPago.KeyEnter = True
        Me.txtFormaPago.Location = New System.Drawing.Point(92, 71)
        Me.txtFormaPago.Name = "txtFormaPago"
        Me.txtFormaPago.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFormaPago.ShortcutsEnabled = False
        Me.txtFormaPago.Size = New System.Drawing.Size(100, 20)
        Me.txtFormaPago.TabIndex = 15
        Me.txtFormaPago.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(6, 47)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel7.TabIndex = 14
        Me.BeLabel7.Text = "Empresa"
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(6, 21)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel8.TabIndex = 13
        Me.BeLabel8.Text = "Banco"
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtEmpresa.BackColor = System.Drawing.Color.Ivory
        Me.txtEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmpresa.ForeColor = System.Drawing.Color.Black
        Me.txtEmpresa.KeyEnter = True
        Me.txtEmpresa.Location = New System.Drawing.Point(92, 45)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtEmpresa.ShortcutsEnabled = False
        Me.txtEmpresa.Size = New System.Drawing.Size(100, 20)
        Me.txtEmpresa.TabIndex = 12
        Me.txtEmpresa.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'txtBanco
        '
        Me.txtBanco.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBanco.BackColor = System.Drawing.Color.Ivory
        Me.txtBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBanco.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBanco.ForeColor = System.Drawing.Color.Black
        Me.txtBanco.KeyEnter = True
        Me.txtBanco.Location = New System.Drawing.Point(92, 19)
        Me.txtBanco.Name = "txtBanco"
        Me.txtBanco.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBanco.ShortcutsEnabled = False
        Me.txtBanco.Size = New System.Drawing.Size(100, 20)
        Me.txtBanco.TabIndex = 11
        Me.txtBanco.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'dgvDetalleCajas
        '
        Me.dgvDetalleCajas.AllowUserToAddRows = False
        Me.dgvDetalleCajas.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalleCajas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleCajas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Fecha, Me.Column7, Me.Column10, Me.TipoDocumento, Me.Column3, Me.IdGasto, Me.Glosa, Me.OtrosD, Me.CentroCosto, Me.Total, Me.IdDetalleMovimiento, Me.Opcion, Me.Anular, Me.Desglozado, Me.Empresas, Me.Column9})
        Me.dgvDetalleCajas.Location = New System.Drawing.Point(14, 277)
        Me.dgvDetalleCajas.Name = "dgvDetalleCajas"
        Me.dgvDetalleCajas.RowHeadersVisible = False
        Me.dgvDetalleCajas.Size = New System.Drawing.Size(545, 227)
        Me.dgvDetalleCajas.TabIndex = 26
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "FechaDetMov"
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Width = 70
        '
        'Column7
        '
        Me.Column7.HeaderText = "Ruc"
        Me.Column7.MaxInputLength = 11
        Me.Column7.Name = "Column7"
        Me.Column7.Width = 75
        '
        'Column10
        '
        Me.Column10.HeaderText = "Razón Social"
        Me.Column10.Name = "Column10"
        Me.Column10.Width = 95
        '
        'TipoDocumento
        '
        Me.TipoDocumento.DataPropertyName = "IdDocumento"
        Me.TipoDocumento.HeaderText = "Tipo Documento"
        Me.TipoDocumento.Name = "TipoDocumento"
        Me.TipoDocumento.Width = 92
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "DocumentoReferencia"
        Me.Column3.HeaderText = "Número"
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 70
        '
        'IdGasto
        '
        Me.IdGasto.DataPropertyName = "TipoGasto"
        Me.IdGasto.HeaderText = "Tipo Gasto"
        Me.IdGasto.Name = "IdGasto"
        Me.IdGasto.Width = 215
        '
        'Glosa
        '
        Me.Glosa.DataPropertyName = "Descripcion"
        Me.Glosa.HeaderText = "Concepto"
        Me.Glosa.Name = "Glosa"
        '
        'OtrosD
        '
        Me.OtrosD.HeaderText = "Otros Datos"
        Me.OtrosD.Name = "OtrosD"
        '
        'CentroCosto
        '
        Me.CentroCosto.DataPropertyName = "CCosCodigo"
        Me.CentroCosto.HeaderText = "Centro de Costo"
        Me.CentroCosto.Name = "CentroCosto"
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.Width = 50
        '
        'IdDetalleMovimiento
        '
        Me.IdDetalleMovimiento.DataPropertyName = "IdDetalleMovimiento"
        Me.IdDetalleMovimiento.HeaderText = "IdDetalleMovimiento"
        Me.IdDetalleMovimiento.Name = "IdDetalleMovimiento"
        Me.IdDetalleMovimiento.Visible = False
        Me.IdDetalleMovimiento.Width = 35
        '
        'Opcion
        '
        Me.Opcion.HeaderText = "Opción"
        Me.Opcion.Name = "Opcion"
        Me.Opcion.Visible = False
        Me.Opcion.Width = 35
        '
        'Anular
        '
        Me.Anular.HeaderText = "Rendir"
        Me.Anular.Name = "Anular"
        Me.Anular.Width = 40
        '
        'Desglozado
        '
        Me.Desglozado.HeaderText = "Vale"
        Me.Desglozado.Name = "Desglozado"
        Me.Desglozado.Width = 50
        '
        'Empresas
        '
        Me.Empresas.HeaderText = "Empresa a Prestar"
        Me.Empresas.Name = "Empresas"
        Me.Empresas.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Empresas.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Empresas.Width = 120
        '
        'Column9
        '
        Me.Column9.HeaderText = "Prestamo"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 53
        '
        'btnEliminar
        '
        Me.btnEliminar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(567, 344)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(130, 26)
        Me.btnEliminar.TabIndex = 27
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(567, 373)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(130, 26)
        Me.btnGrabar.TabIndex = 28
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnCerrar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCerrar.Location = New System.Drawing.Point(567, 402)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(130, 26)
        Me.btnCerrar.TabIndex = 29
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'frmCajaChicaConsultas2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(704, 516)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.dgvDetalleCajas)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.lstCajas)
        Me.Controls.Add(Me.BeLabel3)
        Me.Controls.Add(Me.txtAnio)
        Me.Controls.Add(Me.cboCaja)
        Me.Controls.Add(Me.cboArea)
        Me.Controls.Add(Me.BeLabel4)
        Me.Controls.Add(Me.BeLabel2)
        Me.MaximizeBox = False
        Me.Name = "frmCajaChicaConsultas2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Mantenimiento de Caja Chica"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvDetalleCajas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboCaja As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboArea As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtAnio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents lstCajas As System.Windows.Forms.ListBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotGasto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtEmpresa As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtBanco As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotPago As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumMov As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtFormaPago As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dgvDetalleCajas As System.Windows.Forms.DataGridView
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoDocumento As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdGasto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Glosa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OtrosD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CentroCosto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdDetalleMovimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Opcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Anular As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Desglozado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Empresas As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaInicio As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotSaldo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtAporteGeneral As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnEliminar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnGrabar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnCerrar As ctrLibreria.Controles.BeButton
    Friend WithEvents dtpFechaPago As ctrLibreria.Controles.BeDateTimePicker
End Class
