Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic

Public Class frmCCostosxEmpresa

    Public NroForm As Integer = 0
    Private eSesionCajas As clsSesionCajas

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCCostosxEmpresa = Nothing
    Public Shared Function Instance() As frmCCostosxEmpresa
        If frmInstance Is Nothing Then
            frmInstance = New frmCCostosxEmpresa
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmCCostosxEmpresa_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Dim Codigo As String = ""
        If dgvDetalle.Rows.Count > 0 Then
            Codigo = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Cod").Value)
            'dSaldo = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Des").Value)
        End If

        If NroForm = 1 Then
            Dim formRetorno As frmConsultaAsegurados_x_Obra2 = CType(Me.Owner, frmConsultaAsegurados_x_Obra2)
            formRetorno.CodCC = Codigo
            Me.Close()
        End If

    End Sub

    Private Sub frmCCostosxEmpresa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            eSesionCajas = New clsSesionCajas
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            dtDatos = eSesionCajas.fCargarCCostoForms(35, gEmpresa, "")
            If eSesionCajas.iNroRegistros > 0 Then
                dgvDetalle.DataSource = dtDatos
                dgvDetalle.Focus()
            ElseIf eSesionCajas.iNroRegistros = 0 Then
                btnCancelar.Focus()
            End If
        Catch ex As Exception
        End Try

        'cboCriterio_SelectedIndexChanged(sender, e)
        cboCriterio.SelectedIndex = 1
        'cboCriterio.Text = ""
        Timer1.Enabled = True

    End Sub

    Private Sub txtBusqueda_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtBusqueda.KeyDown
        Select Case e.KeyCode
            'Case Keys.Enter
            'e.Handled = True
            Case Keys.Down
                dgvDetalle.Focus()
        End Select
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Timer1.Enabled = True Then
            txtBusqueda.Focus()
        End If
        Timer1.Enabled = False
    End Sub

    'Private Sub txtBusqueda_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBusqueda.KeyPress
    '    Select Case Asc(e.KeyChar)
    '        Case 13
    '            btnAceptar_Click(sender, e)
    '    End Select
    'End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de B�squeda.")
            'txtBusqueda.Clear()
            Exit Sub
        End If
        Try
            eSesionCajas = New clsSesionCajas
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            If Len(Trim(txtBusqueda.Text)) > 0 Then
                If Trim(cboCriterio.Text) = "C�digo" Then
                    dtDatos = eSesionCajas.fCargarCCostoForms(36, gEmpresa, Trim(txtBusqueda.Text))
                ElseIf Trim(cboCriterio.Text) = "Descripci�n" Then
                    dtDatos = eSesionCajas.fCargarCCostoForms(37, gEmpresa, Trim(txtBusqueda.Text))
                End If
            ElseIf Len(Trim(txtBusqueda.Text)) = 0 Then
                dtDatos = eSesionCajas.fCargarCCostoForms(35, gEmpresa, "")
            End If
            'If eSesionCajas.iNroRegistros > 0 Then
            dgvDetalle.DataSource = dtDatos
            'dgvDetalle.Focus 
            'End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub dgvDetalle_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalle.DoubleClick
        btnAceptar_Click(sender, e)
    End Sub

    Private Sub dgvDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.Handled = True
        End Select
    End Sub

    Private Sub dgvDetalle_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetalle.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                btnAceptar_Click(sender, e)
        End Select
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        If NroForm = 1 Then
            Dim formRetorno As frmConsultaAsegurados_x_Obra2 = CType(Me.Owner, frmConsultaAsegurados_x_Obra2)
            formRetorno.CodCC = ""
            Me.Close()
        End If
    End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub

    Private Sub cboCriterio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCriterio.SelectedIndexChanged
        If cboCriterio.SelectedIndex > -1 Then
            txtBusqueda_TextChanged(sender, e)
            txtBusqueda.Focus()
        End If
    End Sub
End Class