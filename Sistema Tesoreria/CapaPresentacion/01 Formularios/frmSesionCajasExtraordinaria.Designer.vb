<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSesionCajasExtraordinaria
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.txtNumSesion = New ctrLibreria.Controles.BeTextBox()
        Me.cboCaja = New ctrLibreria.Controles.BeComboBox()
        Me.cmdOK = New ctrLibreria.Controles.BeButton()
        Me.cboArea = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.SuspendLayout()
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(17, 72)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(83, 13)
        Me.BeLabel3.TabIndex = 2
        Me.BeLabel3.Text = "Número Caja"
        '
        'txtNumSesion
        '
        Me.txtNumSesion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumSesion.BackColor = System.Drawing.Color.Ivory
        Me.txtNumSesion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumSesion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumSesion.Enabled = False
        Me.txtNumSesion.ForeColor = System.Drawing.Color.Black
        Me.txtNumSesion.KeyEnter = True
        Me.txtNumSesion.Location = New System.Drawing.Point(121, 69)
        Me.txtNumSesion.Name = "txtNumSesion"
        Me.txtNumSesion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumSesion.ShortcutsEnabled = False
        Me.txtNumSesion.Size = New System.Drawing.Size(321, 21)
        Me.txtNumSesion.TabIndex = 5
        Me.txtNumSesion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'cboCaja
        '
        Me.cboCaja.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCaja.BackColor = System.Drawing.Color.Ivory
        Me.cboCaja.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCaja.ForeColor = System.Drawing.Color.Black
        Me.cboCaja.FormattingEnabled = True
        Me.cboCaja.KeyEnter = True
        Me.cboCaja.Location = New System.Drawing.Point(121, 42)
        Me.cboCaja.Name = "cboCaja"
        Me.cboCaja.Size = New System.Drawing.Size(321, 21)
        Me.cboCaja.TabIndex = 4
        '
        'cmdOK
        '
        Me.cmdOK.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.Location = New System.Drawing.Point(345, 96)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(97, 23)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "ACEPTAR"
        Me.cmdOK.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.cmdOK.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'cboArea
        '
        Me.cboArea.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboArea.BackColor = System.Drawing.Color.Ivory
        Me.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArea.ForeColor = System.Drawing.Color.Black
        Me.cboArea.FormattingEnabled = True
        Me.cboArea.KeyEnter = True
        Me.cboArea.Location = New System.Drawing.Point(121, 15)
        Me.cboArea.Name = "cboArea"
        Me.cboArea.Size = New System.Drawing.Size(321, 21)
        Me.cboArea.TabIndex = 3
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(16, 18)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel4.TabIndex = 0
        Me.BeLabel4.Text = "Area/Obra"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(16, 46)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(104, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Descripción Caja"
        '
        'frmSesionCajasExtraordinaria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(459, 127)
        Me.Controls.Add(Me.BeLabel3)
        Me.Controls.Add(Me.txtNumSesion)
        Me.Controls.Add(Me.cboCaja)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cboArea)
        Me.Controls.Add(Me.BeLabel4)
        Me.Controls.Add(Me.BeLabel2)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSesionCajasExtraordinaria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Sesión Caja Extraordinaria"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboArea As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cmdOK As ctrLibreria.Controles.BeButton
    Friend WithEvents cboCaja As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtNumSesion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
End Class
