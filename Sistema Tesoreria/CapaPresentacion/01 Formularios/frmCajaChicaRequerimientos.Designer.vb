<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCajaChicaRequerimientos2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvReq = New System.Windows.Forms.DataGridView()
        Me.X = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantGrupalPorComprar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EvlDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReqCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DrqCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmprCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCosCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdNivel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdEquivalencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProNombreComercial2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DrqAgregado2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvReqAgregados = New System.Windows.Forms.DataGridView()
        Me.cboCCostos = New System.Windows.Forms.ComboBox()
        Me.cboEmpresas = New System.Windows.Forms.ComboBox()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.txtImporteTotal = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroReq = New ctrLibreria.Controles.BeTextBox()
        Me.btnConfirmar = New ctrLibreria.Controles.BeButton()
        Me.btnQuitar = New ctrLibreria.Controles.BeButton()
        Me.btnAgregar = New ctrLibreria.Controles.BeButton()
        Me.X2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ReqCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Descripcion2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UndMed = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.CantGrupalPorComprar2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Precio2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantGrupalPorComprarOculto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EvlDescripcion2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DrqCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmprCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCosCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LinCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SubCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProCodigo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdDescripcion2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdNivel2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdEquivalencia2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantGrupalPorComprarOculto2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdDescripcionCombo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProNombreComercial2x = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DrqAgregado2x = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvReq, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvReqAgregados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvReq
        '
        Me.dgvReq.AllowUserToAddRows = False
        Me.dgvReq.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReq.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvReq.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReq.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.X, Me.Codigo, Me.Descripcion, Me.CantGrupalPorComprar, Me.EvlDescripcion, Me.ReqCodigo, Me.DrqCodigo, Me.EmprCodigo, Me.CCosCodigo, Me.LinCodigo, Me.SubCodigo, Me.TipCodigo, Me.ProCodigo, Me.UmdDescripcion, Me.UmdNivel, Me.UmdEquivalencia, Me.ProNombreComercial2, Me.DrqAgregado2})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvReq.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvReq.Location = New System.Drawing.Point(12, 87)
        Me.dgvReq.Name = "dgvReq"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReq.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvReq.RowHeadersVisible = False
        Me.dgvReq.Size = New System.Drawing.Size(455, 142)
        Me.dgvReq.TabIndex = 7
        '
        'X
        '
        Me.X.HeaderText = "X"
        Me.X.Name = "X"
        Me.X.Width = 20
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "Codigo"
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.Width = 70
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 165
        '
        'CantGrupalPorComprar
        '
        Me.CantGrupalPorComprar.DataPropertyName = "DrqCantGrupalPorComprar"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.CantGrupalPorComprar.DefaultCellStyle = DataGridViewCellStyle2
        Me.CantGrupalPorComprar.HeaderText = "Cant x Atender"
        Me.CantGrupalPorComprar.Name = "CantGrupalPorComprar"
        Me.CantGrupalPorComprar.ReadOnly = True
        '
        'EvlDescripcion
        '
        Me.EvlDescripcion.DataPropertyName = "EvlDescripcion"
        Me.EvlDescripcion.HeaderText = "Equivalencia"
        Me.EvlDescripcion.Name = "EvlDescripcion"
        Me.EvlDescripcion.ReadOnly = True
        Me.EvlDescripcion.Width = 90
        '
        'ReqCodigo
        '
        Me.ReqCodigo.DataPropertyName = "ReqCodigo"
        Me.ReqCodigo.HeaderText = "ReqCodigo"
        Me.ReqCodigo.Name = "ReqCodigo"
        Me.ReqCodigo.Visible = False
        '
        'DrqCodigo
        '
        Me.DrqCodigo.DataPropertyName = "DrqCodigo"
        Me.DrqCodigo.HeaderText = "ReqDetCodigo"
        Me.DrqCodigo.Name = "DrqCodigo"
        Me.DrqCodigo.Visible = False
        '
        'EmprCodigo
        '
        Me.EmprCodigo.DataPropertyName = "EmprCodigo"
        Me.EmprCodigo.HeaderText = "ReqEmpr"
        Me.EmprCodigo.Name = "EmprCodigo"
        Me.EmprCodigo.Visible = False
        '
        'CCosCodigo
        '
        Me.CCosCodigo.DataPropertyName = "CCosCodigo"
        Me.CCosCodigo.HeaderText = "CCosto"
        Me.CCosCodigo.Name = "CCosCodigo"
        Me.CCosCodigo.Visible = False
        '
        'LinCodigo
        '
        Me.LinCodigo.DataPropertyName = "LinCodigo"
        Me.LinCodigo.HeaderText = "LinCodigo"
        Me.LinCodigo.Name = "LinCodigo"
        Me.LinCodigo.Visible = False
        '
        'SubCodigo
        '
        Me.SubCodigo.DataPropertyName = "SubCodigo"
        Me.SubCodigo.HeaderText = "SubCodigo"
        Me.SubCodigo.Name = "SubCodigo"
        Me.SubCodigo.Visible = False
        '
        'TipCodigo
        '
        Me.TipCodigo.DataPropertyName = "TipCodigo"
        Me.TipCodigo.HeaderText = "TipCodigo"
        Me.TipCodigo.Name = "TipCodigo"
        Me.TipCodigo.Visible = False
        '
        'ProCodigo
        '
        Me.ProCodigo.DataPropertyName = "ProCodigo"
        Me.ProCodigo.HeaderText = "ProCodigo"
        Me.ProCodigo.Name = "ProCodigo"
        Me.ProCodigo.Visible = False
        '
        'UmdDescripcion
        '
        Me.UmdDescripcion.DataPropertyName = "UmdDescripcion"
        Me.UmdDescripcion.HeaderText = "UmdDescripcion"
        Me.UmdDescripcion.Name = "UmdDescripcion"
        Me.UmdDescripcion.Visible = False
        '
        'UmdNivel
        '
        Me.UmdNivel.DataPropertyName = "UmdNivel"
        Me.UmdNivel.HeaderText = "UmdNivel"
        Me.UmdNivel.Name = "UmdNivel"
        Me.UmdNivel.Visible = False
        '
        'UmdEquivalencia
        '
        Me.UmdEquivalencia.DataPropertyName = "UmdEquivalencia"
        Me.UmdEquivalencia.HeaderText = "UmdEquivalencia"
        Me.UmdEquivalencia.Name = "UmdEquivalencia"
        Me.UmdEquivalencia.Visible = False
        '
        'ProNombreComercial2
        '
        Me.ProNombreComercial2.DataPropertyName = "ProNombreComercial2"
        Me.ProNombreComercial2.HeaderText = "ProNombreComercial2"
        Me.ProNombreComercial2.Name = "ProNombreComercial2"
        Me.ProNombreComercial2.Visible = False
        '
        'DrqAgregado2
        '
        Me.DrqAgregado2.DataPropertyName = "DrqAgregado2"
        Me.DrqAgregado2.HeaderText = "DrqAgregado2"
        Me.DrqAgregado2.Name = "DrqAgregado2"
        Me.DrqAgregado2.Visible = False
        '
        'dgvReqAgregados
        '
        Me.dgvReqAgregados.AllowUserToAddRows = False
        Me.dgvReqAgregados.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReqAgregados.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvReqAgregados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReqAgregados.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.X2, Me.ReqCodigo2, Me.Codigo2, Me.Descripcion2, Me.UndMed, Me.CantGrupalPorComprar2, Me.Precio2, Me.CU, Me.CantGrupalPorComprarOculto, Me.EvlDescripcion2, Me.DrqCodigo2, Me.EmprCodigo2, Me.CCosCodigo2, Me.LinCodigo2, Me.SubCodigo2, Me.TipCodigo2, Me.ProCodigo2, Me.UmdDescripcion2, Me.UmdNivel2, Me.UmdEquivalencia2, Me.CantGrupalPorComprarOculto2, Me.UmdDescripcionCombo, Me.ProNombreComercial2x, Me.DrqAgregado2x, Me.Importe})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvReqAgregados.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvReqAgregados.Location = New System.Drawing.Point(513, 37)
        Me.dgvReqAgregados.Name = "dgvReqAgregados"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReqAgregados.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvReqAgregados.RowHeadersVisible = False
        Me.dgvReqAgregados.Size = New System.Drawing.Size(614, 192)
        Me.dgvReqAgregados.TabIndex = 9
        '
        'cboCCostos
        '
        Me.cboCCostos.FormattingEnabled = True
        Me.cboCCostos.Location = New System.Drawing.Point(68, 37)
        Me.cboCCostos.Name = "cboCCostos"
        Me.cboCCostos.Size = New System.Drawing.Size(223, 21)
        Me.cboCCostos.TabIndex = 3
        '
        'cboEmpresas
        '
        Me.cboEmpresas.FormattingEnabled = True
        Me.cboEmpresas.Location = New System.Drawing.Point(68, 10)
        Me.cboEmpresas.Name = "cboEmpresas"
        Me.cboEmpresas.Size = New System.Drawing.Size(399, 21)
        Me.cboEmpresas.TabIndex = 1
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(717, 235)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(89, 13)
        Me.BeLabel5.TabIndex = 14
        Me.BeLabel5.Text = "Importe Total:"
        '
        'txtImporteTotal
        '
        Me.txtImporteTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporteTotal.BackColor = System.Drawing.Color.Ivory
        Me.txtImporteTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporteTotal.ForeColor = System.Drawing.Color.Black
        Me.txtImporteTotal.KeyEnter = True
        Me.txtImporteTotal.Location = New System.Drawing.Point(818, 233)
        Me.txtImporteTotal.MaxLength = 15
        Me.txtImporteTotal.Name = "txtImporteTotal"
        Me.txtImporteTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporteTotal.ReadOnly = True
        Me.txtImporteTotal.ShortcutsEnabled = False
        Me.txtImporteTotal.Size = New System.Drawing.Size(131, 20)
        Me.txtImporteTotal.TabIndex = 13
        Me.txtImporteTotal.Text = "0.0"
        Me.txtImporteTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporteTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfanúmerico
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(534, 13)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(61, 13)
        Me.BeLabel4.TabIndex = 8
        Me.BeLabel4.Text = "Glosa Fac"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(351, 67)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(31, 13)
        Me.BeLabel3.TabIndex = 6
        Me.BeLabel3.Text = "Req"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(304, 45)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel2.TabIndex = 4
        Me.BeLabel2.Text = "Req"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(9, 18)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Empresa"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(9, 45)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(53, 13)
        Me.BeLabel12.TabIndex = 2
        Me.BeLabel12.Text = "C Costo"
        '
        'txtNroReq
        '
        Me.txtNroReq.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroReq.BackColor = System.Drawing.Color.Ivory
        Me.txtNroReq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroReq.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroReq.ForeColor = System.Drawing.Color.Black
        Me.txtNroReq.KeyEnter = True
        Me.txtNroReq.Location = New System.Drawing.Point(336, 38)
        Me.txtNroReq.MaxLength = 15
        Me.txtNroReq.Name = "txtNroReq"
        Me.txtNroReq.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroReq.ShortcutsEnabled = False
        Me.txtNroReq.Size = New System.Drawing.Size(131, 20)
        Me.txtNroReq.TabIndex = 5
        Me.txtNroReq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroReq.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfanúmerico
        '
        'btnConfirmar
        '
        Me.btnConfirmar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnConfirmar.Location = New System.Drawing.Point(998, 235)
        Me.btnConfirmar.Name = "btnConfirmar"
        Me.btnConfirmar.Size = New System.Drawing.Size(116, 23)
        Me.btnConfirmar.TabIndex = 12
        Me.btnConfirmar.Text = "Confirmar Compra"
        Me.btnConfirmar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnConfirmar.UseVisualStyleBackColor = True
        '
        'btnQuitar
        '
        Me.btnQuitar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnQuitar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnQuitar.Location = New System.Drawing.Point(468, 150)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(44, 23)
        Me.btnQuitar.TabIndex = 11
        Me.btnQuitar.Text = "<<"
        Me.btnQuitar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnAgregar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAgregar.Location = New System.Drawing.Point(468, 121)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(44, 23)
        Me.btnAgregar.TabIndex = 10
        Me.btnAgregar.Text = ">>"
        Me.btnAgregar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'X2
        '
        Me.X2.HeaderText = "X"
        Me.X2.Name = "X2"
        Me.X2.Width = 20
        '
        'ReqCodigo2
        '
        Me.ReqCodigo2.HeaderText = "ReqCodigo"
        Me.ReqCodigo2.Name = "ReqCodigo2"
        '
        'Codigo2
        '
        Me.Codigo2.HeaderText = "Codigo"
        Me.Codigo2.Name = "Codigo2"
        Me.Codigo2.Width = 80
        '
        'Descripcion2
        '
        Me.Descripcion2.HeaderText = "Descripcion"
        Me.Descripcion2.Name = "Descripcion2"
        Me.Descripcion2.ReadOnly = True
        Me.Descripcion2.Width = 180
        '
        'UndMed
        '
        Me.UndMed.HeaderText = "UndMed"
        Me.UndMed.Name = "UndMed"
        Me.UndMed.Width = 70
        '
        'CantGrupalPorComprar2
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.CantGrupalPorComprar2.DefaultCellStyle = DataGridViewCellStyle6
        Me.CantGrupalPorComprar2.HeaderText = "Cant Comprada"
        Me.CantGrupalPorComprar2.Name = "CantGrupalPorComprar2"
        Me.CantGrupalPorComprar2.Width = 50
        '
        'Precio2
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.Precio2.DefaultCellStyle = DataGridViewCellStyle7
        Me.Precio2.HeaderText = "Precio2"
        Me.Precio2.Name = "Precio2"
        Me.Precio2.Visible = False
        Me.Precio2.Width = 60
        '
        'CU
        '
        Me.CU.HeaderText = "C.U."
        Me.CU.Name = "CU"
        Me.CU.ReadOnly = True
        Me.CU.Width = 50
        '
        'CantGrupalPorComprarOculto
        '
        Me.CantGrupalPorComprarOculto.HeaderText = "CantGrupalPorComprarOculto"
        Me.CantGrupalPorComprarOculto.Name = "CantGrupalPorComprarOculto"
        Me.CantGrupalPorComprarOculto.Visible = False
        '
        'EvlDescripcion2
        '
        Me.EvlDescripcion2.HeaderText = "EvlDescripcion"
        Me.EvlDescripcion2.Name = "EvlDescripcion2"
        Me.EvlDescripcion2.Visible = False
        '
        'DrqCodigo2
        '
        Me.DrqCodigo2.HeaderText = "DrqCodigo2"
        Me.DrqCodigo2.Name = "DrqCodigo2"
        Me.DrqCodigo2.Visible = False
        '
        'EmprCodigo2
        '
        Me.EmprCodigo2.HeaderText = "EmprCodigo2"
        Me.EmprCodigo2.Name = "EmprCodigo2"
        Me.EmprCodigo2.Visible = False
        '
        'CCosCodigo2
        '
        Me.CCosCodigo2.HeaderText = "CCosCodigo"
        Me.CCosCodigo2.Name = "CCosCodigo2"
        Me.CCosCodigo2.Visible = False
        '
        'LinCodigo2
        '
        Me.LinCodigo2.HeaderText = "LinCodigo"
        Me.LinCodigo2.Name = "LinCodigo2"
        Me.LinCodigo2.Visible = False
        '
        'SubCodigo2
        '
        Me.SubCodigo2.HeaderText = "SubCodigo"
        Me.SubCodigo2.Name = "SubCodigo2"
        Me.SubCodigo2.Visible = False
        '
        'TipCodigo2
        '
        Me.TipCodigo2.HeaderText = "TipCodigo"
        Me.TipCodigo2.Name = "TipCodigo2"
        Me.TipCodigo2.Visible = False
        '
        'ProCodigo2
        '
        Me.ProCodigo2.HeaderText = "ProCodigo"
        Me.ProCodigo2.Name = "ProCodigo2"
        Me.ProCodigo2.Visible = False
        '
        'UmdDescripcion2
        '
        Me.UmdDescripcion2.HeaderText = "UmdDescripcion"
        Me.UmdDescripcion2.Name = "UmdDescripcion2"
        Me.UmdDescripcion2.Visible = False
        Me.UmdDescripcion2.Width = 50
        '
        'UmdNivel2
        '
        Me.UmdNivel2.HeaderText = "UmdNivel2"
        Me.UmdNivel2.Name = "UmdNivel2"
        Me.UmdNivel2.Visible = False
        '
        'UmdEquivalencia2
        '
        Me.UmdEquivalencia2.HeaderText = "UmdEquivalencia2"
        Me.UmdEquivalencia2.Name = "UmdEquivalencia2"
        Me.UmdEquivalencia2.Visible = False
        '
        'CantGrupalPorComprarOculto2
        '
        Me.CantGrupalPorComprarOculto2.HeaderText = "CantGrupalPorComprarOculto2"
        Me.CantGrupalPorComprarOculto2.Name = "CantGrupalPorComprarOculto2"
        Me.CantGrupalPorComprarOculto2.Visible = False
        '
        'UmdDescripcionCombo
        '
        Me.UmdDescripcionCombo.HeaderText = "UmdDescripcionCombo"
        Me.UmdDescripcionCombo.Name = "UmdDescripcionCombo"
        Me.UmdDescripcionCombo.Visible = False
        '
        'ProNombreComercial2x
        '
        Me.ProNombreComercial2x.HeaderText = "ProNombreComercial2x"
        Me.ProNombreComercial2x.Name = "ProNombreComercial2x"
        Me.ProNombreComercial2x.Visible = False
        '
        'DrqAgregado2x
        '
        Me.DrqAgregado2x.HeaderText = "DrqAgregado2x"
        Me.DrqAgregado2x.Name = "DrqAgregado2x"
        Me.DrqAgregado2x.Visible = False
        '
        'Importe
        '
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        '
        'frmCajaChicaRequerimientos2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1130, 266)
        Me.Controls.Add(Me.BeLabel5)
        Me.Controls.Add(Me.txtImporteTotal)
        Me.Controls.Add(Me.BeLabel4)
        Me.Controls.Add(Me.BeLabel3)
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.cboEmpresas)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.cboCCostos)
        Me.Controls.Add(Me.BeLabel12)
        Me.Controls.Add(Me.txtNroReq)
        Me.Controls.Add(Me.btnConfirmar)
        Me.Controls.Add(Me.btnQuitar)
        Me.Controls.Add(Me.btnAgregar)
        Me.Controls.Add(Me.dgvReqAgregados)
        Me.Controls.Add(Me.dgvReq)
        Me.MaximizeBox = False
        Me.Name = "frmCajaChicaRequerimientos2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compra de Requerimientos"
        CType(Me.dgvReq, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvReqAgregados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvReq As System.Windows.Forms.DataGridView
    Friend WithEvents dgvReqAgregados As System.Windows.Forms.DataGridView
    Friend WithEvents btnAgregar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnQuitar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnConfirmar As ctrLibreria.Controles.BeButton
    Friend WithEvents txtNroReq As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCCostos As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmpresas As System.Windows.Forms.ComboBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents X As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantGrupalPorComprar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EvlDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReqCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DrqCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmprCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdNivel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdEquivalencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProNombreComercial2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DrqAgregado2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtImporteTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents X2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ReqCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Codigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UndMed As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents CantGrupalPorComprar2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Precio2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantGrupalPorComprarOculto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EvlDescripcion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DrqCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmprCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SubCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProCodigo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdDescripcion2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdNivel2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdEquivalencia2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantGrupalPorComprarOculto2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdDescripcionCombo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProNombreComercial2x As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DrqAgregado2x As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
