Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared


Public Class frmReporteDocsxDia2

    Public nroSesion As String
    Public iOpcion As String
    Public F1 As DateTime
    Public F2 As DateTime
    Public CodProv As String
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptF As New rptDocumentosPorDia3
    Dim rptF4 As New rptDocumentosPorDia4
    Public Usuario As String = ""

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmReporteDocsxDia2 = Nothing

    Public Shared Function Instance() As frmReporteDocsxDia2
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteDocsxDia2
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmReporteDocsxDia2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region


    Private Sub frmReporteDocsxDia2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Reporte de Documentos Recepcionados entre " & F1 & " y " & F2

        If iOpcion = "Todos" Then
            rptF.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaRecepcion1")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Format(F1, "yyyy-MM-dd")
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaRecepcion2")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Format(F2, "yyyy-MM-dd")
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NombreEmpr")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Usuario")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gUsuario)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            CrystalReportViewer1.ReportSource = rptF
        ElseIf iOpcion = "Uno" Then
            rptF4.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            crParameterFieldDefinitions = rptF4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaRecepcion1")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Format(F1, "yyyy-MM-dd")
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@FechaRecepcion2")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Format(F2, "yyyy-MM-dd")
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NombreEmpr")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@RucProv")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(CodProv)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterFieldDefinitions = rptF4.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Usuario")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gUsuario)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            CrystalReportViewer1.ReportSource = rptF4
        End If
        

    End Sub

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

End Class