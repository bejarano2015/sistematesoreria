<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmChequera
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.stsTotales = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.cboEstado = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.txtNumIni = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.txtNumFin = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.txtNumChequera = New ctrLibreria.Controles.BeTextBox()
        Me.dtpFechaChequera = New ctrLibreria.Controles.BeDateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chkUso = New System.Windows.Forms.CheckBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboBanco = New ctrLibreria.Controles.BeComboBox()
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox()
        Me.lblMoneda = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.dgvLista = New ctrLibreria.Controles.BeDataGridView()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblDesMonCuenta = New System.Windows.Forms.Label()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.cboEstados = New ctrLibreria.Controles.BeComboBox()
        Me.cboCuentas = New ctrLibreria.Controles.BeComboBox()
        Me.cboBancos = New ctrLibreria.Controles.BeComboBox()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.stsTotales.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(942, 475)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Size = New System.Drawing.Size(934, 449)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Size = New System.Drawing.Size(934, 449)
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsTotales.Location = New System.Drawing.Point(3, 420)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(926, 24)
        Me.stsTotales.TabIndex = 18
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(122, 19)
        Me.ToolStripStatusLabel1.Text = "Total de Registros= 0"
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(6, 50)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel5.TabIndex = 4
        Me.BeLabel5.Text = "Estado"
        '
        'cboEstado
        '
        Me.cboEstado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstado.BackColor = System.Drawing.Color.Ivory
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.ForeColor = System.Drawing.Color.Black
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.KeyEnter = True
        Me.cboEstado.Location = New System.Drawing.Point(57, 43)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(133, 21)
        Me.cboEstado.TabIndex = 5
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(4, 22)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "N�mero"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(268, 22)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(95, 13)
        Me.BeLabel3.TabIndex = 2
        Me.BeLabel3.Text = "Fecha Creaci�n"
        '
        'txtNumIni
        '
        Me.txtNumIni.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNumIni.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumIni.BackColor = System.Drawing.Color.Ivory
        Me.txtNumIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumIni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumIni.ForeColor = System.Drawing.Color.Black
        Me.txtNumIni.KeyEnter = True
        Me.txtNumIni.Location = New System.Drawing.Point(62, 20)
        Me.txtNumIni.MaxLength = 40
        Me.txtNumIni.Name = "txtNumIni"
        Me.txtNumIni.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumIni.ShortcutsEnabled = False
        Me.txtNumIni.Size = New System.Drawing.Size(138, 21)
        Me.txtNumIni.TabIndex = 1
        Me.txtNumIni.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(4, 23)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(59, 13)
        Me.BeLabel6.TabIndex = 0
        Me.BeLabel6.Text = "N� Inicial"
        '
        'txtNumFin
        '
        Me.txtNumFin.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNumFin.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumFin.BackColor = System.Drawing.Color.Ivory
        Me.txtNumFin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumFin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumFin.ForeColor = System.Drawing.Color.Black
        Me.txtNumFin.KeyEnter = True
        Me.txtNumFin.Location = New System.Drawing.Point(271, 20)
        Me.txtNumFin.MaxLength = 40
        Me.txtNumFin.Name = "txtNumFin"
        Me.txtNumFin.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumFin.ShortcutsEnabled = False
        Me.txtNumFin.Size = New System.Drawing.Size(138, 21)
        Me.txtNumFin.TabIndex = 3
        Me.txtNumFin.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(214, 23)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel7.TabIndex = 2
        Me.BeLabel7.Text = "N� Final"
        '
        'txtNumChequera
        '
        Me.txtNumChequera.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNumChequera.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumChequera.BackColor = System.Drawing.Color.Ivory
        Me.txtNumChequera.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumChequera.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumChequera.ForeColor = System.Drawing.Color.Black
        Me.txtNumChequera.KeyEnter = True
        Me.txtNumChequera.Location = New System.Drawing.Point(62, 19)
        Me.txtNumChequera.MaxLength = 20
        Me.txtNumChequera.Name = "txtNumChequera"
        Me.txtNumChequera.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumChequera.ShortcutsEnabled = False
        Me.txtNumChequera.Size = New System.Drawing.Size(198, 21)
        Me.txtNumChequera.TabIndex = 1
        Me.txtNumChequera.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        '
        'dtpFechaChequera
        '
        Me.dtpFechaChequera.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaChequera.CustomFormat = ""
        Me.dtpFechaChequera.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaChequera.KeyEnter = True
        Me.dtpFechaChequera.Location = New System.Drawing.Point(369, 19)
        Me.dtpFechaChequera.Name = "dtpFechaChequera"
        Me.dtpFechaChequera.Size = New System.Drawing.Size(138, 21)
        Me.dtpFechaChequera.TabIndex = 3
        Me.dtpFechaChequera.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaChequera.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.GroupBox5)
        Me.Panel1.Controls.Add(Me.GroupBox3)
        Me.Panel1.Controls.Add(Me.GroupBox2)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.txtCodigo)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(926, 445)
        Me.Panel1.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.chkUso)
        Me.GroupBox5.Controls.Add(Me.cboEstado)
        Me.GroupBox5.Controls.Add(Me.BeLabel5)
        Me.GroupBox5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(464, 41)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(305, 75)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Condici�n"
        '
        'chkUso
        '
        Me.chkUso.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUso.ForeColor = System.Drawing.Color.Black
        Me.chkUso.Location = New System.Drawing.Point(6, 19)
        Me.chkUso.Name = "chkUso"
        Me.chkUso.Size = New System.Drawing.Size(293, 17)
        Me.chkUso.TabIndex = 0
        Me.chkUso.Text = "En Uso (Habilitado) / Lleno (Deshabilitado)"
        Me.chkUso.TextAlign = System.Drawing.ContentAlignment.TopLeft
        Me.chkUso.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtNumIni)
        Me.GroupBox3.Controls.Add(Me.BeLabel6)
        Me.GroupBox3.Controls.Add(Me.BeLabel7)
        Me.GroupBox3.Controls.Add(Me.txtNumFin)
        Me.GroupBox3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(15, 182)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(532, 50)
        Me.GroupBox3.TabIndex = 7
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Numeros de Cheque"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtNumChequera)
        Me.GroupBox2.Controls.Add(Me.BeLabel1)
        Me.GroupBox2.Controls.Add(Me.BeLabel3)
        Me.GroupBox2.Controls.Add(Me.dtpFechaChequera)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(15, 131)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(532, 45)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Datos Chequera"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboBanco)
        Me.GroupBox1.Controls.Add(Me.cboCuenta)
        Me.GroupBox1.Controls.Add(Me.lblMoneda)
        Me.GroupBox1.Controls.Add(Me.BeLabel8)
        Me.GroupBox1.Controls.Add(Me.BeLabel10)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(15, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(443, 75)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cuenta Corriente"
        '
        'cboBanco
        '
        Me.cboBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBanco.ForeColor = System.Drawing.Color.Black
        Me.cboBanco.FormattingEnabled = True
        Me.cboBanco.KeyEnter = True
        Me.cboBanco.Location = New System.Drawing.Point(62, 19)
        Me.cboBanco.Name = "cboBanco"
        Me.cboBanco.Size = New System.Drawing.Size(210, 21)
        Me.cboBanco.TabIndex = 1
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(62, 43)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(210, 21)
        Me.cboCuenta.TabIndex = 3
        '
        'lblMoneda
        '
        Me.lblMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin02
        Me.lblMoneda.BackColor = System.Drawing.Color.DimGray
        Me.lblMoneda.ForeColor = System.Drawing.Color.Black
        Me.lblMoneda.Location = New System.Drawing.Point(278, 42)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(145, 21)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(6, 21)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel8.TabIndex = 0
        Me.BeLabel8.Text = "Banco"
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(6, 46)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel10.TabIndex = 2
        Me.BeLabel10.Text = "Cuenta"
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(77, 13)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(133, 20)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(19, 13)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel4.TabIndex = 0
        Me.BeLabel4.Text = "C�digo"
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Column9, Me.Column10, Me.Column6, Me.Column8, Me.Column5, Me.Column2, Me.Column7, Me.Column1, Me.Column4, Me.Column3})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLista.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-1, -1)
        Me.dgvLista.MultiSelect = False
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLista.Size = New System.Drawing.Size(7402, 798)
        Me.dgvLista.TabIndex = 192
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "IdChequera"
        Me.Codigo.HeaderText = "C�digo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 130
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "NombreBanco"
        Me.Column9.HeaderText = "Banco"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 180
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "NumeroCuenta"
        Me.Column10.HeaderText = "Cuenta"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 120
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "IdBanco"
        Me.Column6.HeaderText = "IdBanco"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Visible = False
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "IdCuenta"
        Me.Column8.HeaderText = "IdCuenta"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Visible = False
        Me.Column8.Width = 130
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "NroChequera"
        Me.Column5.HeaderText = "N� Chequera"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 130
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "FechaChequera"
        Me.Column2.HeaderText = "Fecha"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 70
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "NroInicial"
        Me.Column7.HeaderText = "N� Inicial"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 80
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "NroFinal"
        Me.Column1.HeaderText = "N� Final"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 80
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Condicion"
        Me.Column4.HeaderText = "Condici�n"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 70
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Estados"
        Me.Column3.HeaderText = "Estado"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 50
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.GroupBox4)
        Me.Panel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(304, 139)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(420, 124)
        Me.Panel2.TabIndex = 193
        Me.Panel2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(396, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "X"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblDesMonCuenta)
        Me.GroupBox4.Controls.Add(Me.BeLabel12)
        Me.GroupBox4.Controls.Add(Me.BeLabel9)
        Me.GroupBox4.Controls.Add(Me.BeLabel2)
        Me.GroupBox4.Controls.Add(Me.cboEstados)
        Me.GroupBox4.Controls.Add(Me.cboCuentas)
        Me.GroupBox4.Controls.Add(Me.cboBancos)
        Me.GroupBox4.Location = New System.Drawing.Point(10, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(380, 108)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'lblDesMonCuenta
        '
        Me.lblDesMonCuenta.AutoSize = True
        Me.lblDesMonCuenta.Location = New System.Drawing.Point(295, 55)
        Me.lblDesMonCuenta.Name = "lblDesMonCuenta"
        Me.lblDesMonCuenta.Size = New System.Drawing.Size(0, 13)
        Me.lblDesMonCuenta.TabIndex = 16
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(19, 78)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel12.TabIndex = 10
        Me.BeLabel12.Text = "Estado"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(19, 50)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel9.TabIndex = 8
        Me.BeLabel9.Text = "Cuenta"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(19, 23)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel2.TabIndex = 7
        Me.BeLabel2.Text = "Banco"
        '
        'cboEstados
        '
        Me.cboEstados.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstados.BackColor = System.Drawing.Color.Ivory
        Me.cboEstados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstados.ForeColor = System.Drawing.Color.Black
        Me.cboEstados.FormattingEnabled = True
        Me.cboEstados.KeyEnter = True
        Me.cboEstados.Location = New System.Drawing.Point(83, 75)
        Me.cboEstados.Name = "cboEstados"
        Me.cboEstados.Size = New System.Drawing.Size(206, 21)
        Me.cboEstados.TabIndex = 3
        '
        'cboCuentas
        '
        Me.cboCuentas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuentas.BackColor = System.Drawing.Color.Ivory
        Me.cboCuentas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentas.ForeColor = System.Drawing.Color.Black
        Me.cboCuentas.FormattingEnabled = True
        Me.cboCuentas.KeyEnter = True
        Me.cboCuentas.Location = New System.Drawing.Point(83, 47)
        Me.cboCuentas.Name = "cboCuentas"
        Me.cboCuentas.Size = New System.Drawing.Size(206, 21)
        Me.cboCuentas.TabIndex = 1
        '
        'cboBancos
        '
        Me.cboBancos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBancos.BackColor = System.Drawing.Color.Ivory
        Me.cboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBancos.ForeColor = System.Drawing.Color.Black
        Me.cboBancos.FormattingEnabled = True
        Me.cboBancos.KeyEnter = True
        Me.cboBancos.Location = New System.Drawing.Point(83, 20)
        Me.cboBancos.Name = "cboBancos"
        Me.cboBancos.Size = New System.Drawing.Size(206, 21)
        Me.cboBancos.TabIndex = 0
        '
        'frmChequera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(942, 475)
        Me.MaximizeBox = False
        Me.Name = "frmChequera"
        Me.Text = "Cuenta de Chequera"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaChequera As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents txtNumChequera As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumFin As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumIni As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents chkUso As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents lblMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstados As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboCuentas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboBancos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents lblDesMonCuenta As System.Windows.Forms.Label
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
