Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad

Public Class frmSesionCajasExtraordinaria

    Dim sMaxValor As Integer = 0
    Private eSesionCajasExtraordinaria As clsSesionCajasExtraordinario
    Private eArqueo As clsArqueo
    Dim dtTable2 As DataTable
    Dim dtTable3 As DataTable

    'Dim dtCabeceraDePrimerRegistro As DataTable

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmSesionCajasExtraordinaria = Nothing
    Public Shared Function Instance() As frmSesionCajasExtraordinaria
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmSesionCajasExtraordinaria
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmSesionCajas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            eSesionCajasExtraordinaria = New clsSesionCajasExtraordinario
            LimpiarControles()
            cboArea.DataSource = eSesionCajasExtraordinaria.fListarArea(gEmpresa)
            cboArea.ValueMember = "AreaCodigo"
            cboArea.DisplayMember = "AreaNombre"
            cboArea.SelectedIndex = -1
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

    Private Sub LimpiarControles()
        cboArea.SelectedIndex = -1
        cboCaja.SelectedIndex = -1
        txtNumSesion.Clear()
    End Sub

    Private Sub DesabilitarControles()
        cboCaja.Enabled = False
        cboArea.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        cboCaja.Enabled = True
        cboArea.Enabled = True
    End Sub

    Private Sub cboCaja_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCaja.SelectionChangeCommitted
        If cboCaja.SelectedIndex <> -1 Then

            dtTable2 = New DataTable
            dtTable2 = eSesionCajasExtraordinaria.fTraerResponsable(Convert.ToString(cboCaja.SelectedValue), gEmpresa)
            If dtTable2.Rows.Count > 0 Then

                sDesEmpleadoId = dtTable2.Rows(0).Item("Responsable")
                sDesEmpleado = UCase(dtTable2.Rows(0).Item("Nombres"))
                sMontoMinimo = dtTable2.Rows(0).Item("MontoMinimo")
                sMontoBase = dtTable2.Rows(0).Item("MontoMaximo")
                'MontoGeneralInicio = sMontoBase
                sDescripcionMoneda = dtTable2.Rows(0).Item("MonDescripcion")
                sMonedaCaja = dtTable2.Rows(0).Item("MonCodigo")
                sMonSimbolo = dtTable2.Rows(0).Item("MonSimbolo")
                sUsuarioDeCaja = Trim(dtTable2.Rows(0).Item("UsuCodigo"))

                sTPlaCodigo = Trim(dtTable2.Rows(0).Item("TPlaCodigo"))
                sEmprResposable = Trim(dtTable2.Rows(0).Item("EmprResposable"))

                If UCase(Trim(sUsuarioDeCaja)) = Trim(gUsuario) Or Trim(glbUsuCategoria) = "A" Then
                    'Si el Usuario encargado de la Caja es Igual al Usuario que Ingreso al Sistema es igual
                    'o el Usuario es Administrador
                    'Puede ver la Caja
                    'Dim zzz As String = ""
                    'zzz = UCase(Trim(sUsuarioDeCaja))

                    ' ''Dim iCerrarCaja As Integer
                    ' ''eSesionCajasExtraordinaria = New clsSesionCajasExtraordinario
                    ' ''dtTable3 = New DataTable
                    ' ''dtTable3 = eSesionCajasExtraordinaria.fGenerarNumeroSesion(gEmpresa, Convert.ToString(cboCaja.SelectedValue), Convert.ToString(cboArea.SelectedValue), gPeriodo)
                    ' ''If dtTable3.Rows.Count > 0 Then
                    ' ''    iCerrarCaja = Val(dtTable3.Rows(0).Item("CerrarCaja"))
                    ' ''    If iCerrarCaja = 0 Then
                    ' ''        sMaxValor = Val(dtTable3.Rows(0).Item("NumSesion"))
                    ' ''    ElseIf iCerrarCaja = 1 Then
                    ' ''        sMaxValor = eSesionCajasExtraordinaria.sCadena
                    ' ''    End If
                    ' ''Else
                    ' ''    sMaxValor = eSesionCajasExtraordinaria.sCadena
                    ' ''    txtNumSesion.Enabled = True
                    ' ''End If
                    ' '' ''aqui se muestra el numero actual de caja � el numero siguiente
                    ' ''txtNumSesion.Text = sMaxValor
                    txtNumSesion.Text = 1
                Else
                    MessageBox.Show("No Tiene Permisos para Ingresar a la Caja Seleccionada", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboCaja.SelectedIndex = -1
                End If

            End If

        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
            Dim iContadorCabeceraDePrimerRegistro As Int16
            If cboArea.SelectedIndex > -1 And cboCaja.SelectedIndex > -1 And Len(txtNumSesion.Text) <> 0 Then
                Try
                    sIdArea = Convert.ToString(cboArea.SelectedValue)
                    sDesArea = Convert.ToString(cboArea.Text)
                    sIdCaja = Convert.ToString(cboCaja.SelectedValue)
                    sDesCaja = Convert.ToString(cboCaja.Text)
                    sNumeroSesion = Convert.ToInt32(Val(txtNumSesion.Text))
                    'dtCabeceraDePrimerRegistro = New DataTable
                    dtCabecera = New DataTable
                    dtDetalle = New DataTable
                    dtExisteAporteIni = New DataTable
                    Dim dtContadorSesionesDeCaja As New DataTable
                    Dim dtCabecera4 As New DataTable
                    Dim dtSaldoUltimoArqueo As New DataTable
                    Dim dtDatosdeUltimaSesionDeCaja As New DataTable
                    eSesionCajasExtraordinaria = New clsSesionCajasExtraordinario

                    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    dtContadorSesionesDeCaja = eSesionCajasExtraordinaria.fTraerCantidadSesionesDeCaja(sIdCaja, sIdArea, gEmpresa, gPeriodo) 'cuantos sesiones hay?  HAY EN UNA CAJA??
                    sCantidadSesiones = dtContadorSesionesDeCaja.Rows.Count

                    If sCantidadSesiones = 0 Then
                        sSaldoAnterior = sMontoBase
                        ListarAporteInicialMasGastos = 1
                        ListarSaldoAnteriorMasGastos = 0
                        ListarSaldoAnteriorMasReembolso = 0
                    ElseIf sCantidadSesiones = 1 Then
                        'y esa caja ya esta cerrada entonces el saldo anterior para la siguiente caja sera el saldo del ultimo arqueo de la primera caja
                        'sino el saldo de apertura sera el saldo final del arqueo ultimo
                        dtCabecera = eSesionCajasExtraordinaria.fListarCabeceraDePrimerRegistro(sNumeroSesion, sIdCaja, sIdArea, gEmpresa, gPeriodo)
                        iContadorCabeceraDePrimerRegistro = dtCabecera.Rows.Count
                        If iContadorCabeceraDePrimerRegistro = 1 Then
                            IdSesionCabecera = Convert.ToString(dtCabecera.Rows(0).Item("IdSesion"))
                            'MontoGeneralInicio = Convert.ToDouble(dtCabecera.Rows(0).Item("MontoInicioSoles"))
                            'MontoGeneralTotalGasto = Convert.ToDouble(dtCabecera.Rows(0).Item("MontoTotalGas"))
                            'MontoGeneralFinal = Convert.ToDouble(dtCabecera.Rows(0).Item("MontoFinSoles"))
                            dtDetalle = eSesionCajasExtraordinaria.fListarDet(IdSesionCabecera, gEmpresa, iEstado01)
                            dtExisteAporteIni = eSesionCajasExtraordinaria.fExisteAporteInicial(IdSesionCabecera, gEmpresa, iEstado01)
                            If Convert.ToString(dtCabecera.Rows(0).Item("CerrarCaja")) = 0 Then
                                eArqueo = New clsArqueo
                                If dtExisteAporteIni.Rows.Count = 1 Or dtExisteAporteIni.Rows.Count = 0 Then
                                    ListarAporteInicialMasGastos = 0
                                    ListarSaldoAnteriorMasGastos = 1
                                    ListarSaldoAnteriorMasReembolso = 0
                                    dtSaldoUltimoArqueo = eArqueo.fTraerSaldoAnteriorDeArqueo(IdSesionCabecera, gEmpresa)
                                    If dtSaldoUltimoArqueo.Rows.Count = 1 Then
                                        sSaldoAnterior = dtSaldoUltimoArqueo.Rows(0).Item("SaldoDia")
                                    ElseIf dtSaldoUltimoArqueo.Rows.Count = 0 Then
                                        'MessageBox.Show("No Existe Arqueo en este n�mero de Caja", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        sSaldoAnterior = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoFinSoles")), "0.00")
                                    End If
                                End If
                            End If
                        Else
                            dtDatosdeUltimaSesionDeCaja = eSesionCajasExtraordinaria.fTraerUltimoSesionDeCaja(sIdCaja, sIdArea, gEmpresa, gPeriodo)
                            If dtDatosdeUltimaSesionDeCaja.Rows.Count = 1 Then
                                If Convert.ToDouble(dtDatosdeUltimaSesionDeCaja.Rows(0).Item("MontoFinSoles")) = 0 And Convert.ToDouble(dtDatosdeUltimaSesionDeCaja.Rows(0).Item("CerrarCaja")) = 1 Then
                                    ListarAporteInicialMasGastos = 1
                                    ListarSaldoAnteriorMasGastos = 0
                                    ListarSaldoAnteriorMasReembolso = 0
                                    sSaldoAnterior = 0
                                Else 'If Convert.ToDouble(dtDatosdeUltimaSesionDeCaja.Rows(0).Item("MontoFinSoles")) > 0 Then
                                    ListarAporteInicialMasGastos = 0
                                    ListarSaldoAnteriorMasGastos = 0
                                    ListarSaldoAnteriorMasReembolso = 1
                                    sSaldoAnterior = dtDatosdeUltimaSesionDeCaja.Rows(0).Item("MontoFinSoles")
                                    sSaldoReembolso = Format(sMontoBase - sSaldoAnterior, "0.00")
                                End If
                            End If
                        End If
                    ElseIf sCantidadSesiones >= 2 Then
                        dtCabecera = eSesionCajasExtraordinaria.fListarCabeceraDePrimerRegistro(sNumeroSesion, sIdCaja, sIdArea, gEmpresa, gPeriodo)
                        iContadorCabecera = dtCabecera.Rows.Count
                        If iContadorCabecera = 1 Then
                            IdSesionCabecera = Convert.ToString(dtCabecera.Rows(0).Item("IdSesion"))
                            'MontoGeneralInicio = Convert.ToDouble(dtCabecera.Rows(0).Item("MontoInicioSoles"))
                            'MontoGeneralTotalGasto = Convert.ToDouble(dtCabecera.Rows(0).Item("MontoTotalGas"))
                            'MontoGeneralFinal = Convert.ToDouble(dtCabecera.Rows(0).Item("MontoFinSoles"))
                            dtDetalle = eSesionCajasExtraordinaria.fListarDet(IdSesionCabecera, gEmpresa, iEstado01)
                            dtExisteAporteIni = eSesionCajasExtraordinaria.fExisteAporteInicial(IdSesionCabecera, gEmpresa, iEstado01)
                            If Convert.ToString(dtCabecera.Rows(0).Item("CerrarCaja")) = 0 Then
                                eArqueo = New clsArqueo
                                If dtExisteAporteIni.Rows.Count = 1 Then
                                    ListarAporteInicialMasGastos = 0
                                    ListarSaldoAnteriorMasGastos = 1
                                    ListarSaldoAnteriorMasReembolso = 0
                                    dtSaldoUltimoArqueo = eArqueo.fTraerSaldoAnteriorDeArqueo(IdSesionCabecera, gEmpresa)
                                    If dtSaldoUltimoArqueo.Rows.Count = 1 Then
                                        sSaldoAnterior = dtSaldoUltimoArqueo.Rows(0).Item("SaldoDia")
                                    ElseIf dtSaldoUltimoArqueo.Rows.Count = 0 Then
                                        'MessageBox.Show("No Existe Arqueo en este n�mero de Caja", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        sSaldoAnterior = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoFinSoles")), "0.00")
                                    End If
                                    'sSaldoAnterior = dtSaldoUltimoArqueo.Rows(0).Item("SaldoDia")
                                Else
                                    ListarAporteInicialMasGastos = 0
                                    ListarSaldoAnteriorMasGastos = 1
                                    ListarSaldoAnteriorMasReembolso = 0
                                    dtSaldoUltimoArqueo = eArqueo.fTraerSaldoAnteriorDeArqueo(IdSesionCabecera, gEmpresa)
                                    If dtSaldoUltimoArqueo.Rows.Count = 1 Then
                                        sSaldoAnterior = dtSaldoUltimoArqueo.Rows(0).Item("SaldoDia")
                                    ElseIf dtSaldoUltimoArqueo.Rows.Count = 0 Then
                                        'MessageBox.Show("No Existe Arqueo en este n�mero de Caja", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        sSaldoAnterior = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoFinSoles")), "0.00")
                                    End If
                                    sExisteReembolso = 1
                                End If
                            End If
                        Else

                            dtDatosdeUltimaSesionDeCaja = eSesionCajasExtraordinaria.fTraerUltimoSesionDeCaja(sIdCaja, sIdArea, gEmpresa, gPeriodo)
                            If dtDatosdeUltimaSesionDeCaja.Rows.Count = 1 Then
                                If Convert.ToDouble(dtDatosdeUltimaSesionDeCaja.Rows(0).Item("MontoFinSoles")) = 0 And Convert.ToDouble(dtDatosdeUltimaSesionDeCaja.Rows(0).Item("CerrarCaja")) = 1 Then
                                    ListarAporteInicialMasGastos = 1
                                    ListarSaldoAnteriorMasGastos = 0
                                    ListarSaldoAnteriorMasReembolso = 0
                                    sSaldoAnterior = 0
                                ElseIf Convert.ToDouble(dtDatosdeUltimaSesionDeCaja.Rows(0).Item("CerrarCaja")) = 1 Then
                                    ListarAporteInicialMasGastos = 0
                                    ListarSaldoAnteriorMasGastos = 0
                                    ListarSaldoAnteriorMasReembolso = 1
                                    sSaldoAnterior = dtDatosdeUltimaSesionDeCaja.Rows(0).Item("MontoFinSoles")
                                    sSaldoReembolso = Format(sMontoBase - sSaldoAnterior, "0.00")
                                End If
                            End If
                        End If
                    End If
                '+++++++++++++++++++++++++++++++++++++++++++++++++++++++

                    Me.Close()
                    Dim x As frmCajaChicaExtraordinaria = frmCajaChicaExtraordinaria.Instance
                    'x.Owner = Me
                    x.MdiParent = frmPrincipal

                    'Dim x As New frmCajaChica ' 
                    'x.MdiParent = frmPrincipal
                    x.Show()
                Catch ex As Exception
                    Exit Sub
                End Try
            Else
                If cboArea.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Area", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboArea.Focus()
                    Exit Sub
                End If
                If cboCaja.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Caja", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'MsgBox("Seleccione una Caja")
                    cboCaja.Focus()
                    Exit Sub
                End If
                If Len(txtNumSesion.Text) = 0 Then
                    MessageBox.Show("Ingrese un N�mero de Caja", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'MsgBox("Ingrese un Numero de Caja")
                    txtNumSesion.Focus()
                    Exit Sub
                End If
            End If
    End Sub

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        Dim IdArea As String
        txtNumSesion.Clear()
        Try
            IdArea = cboArea.SelectedValue.ToString
            If IdArea <> "System.Data.DataRowView" Then
                cboCaja.DataSource = eSesionCajasExtraordinaria.fListarCajasPorArea(cboArea.SelectedValue, gEmpresa, gPeriodo)
                cboCaja.ValueMember = "IdCaja"
                cboCaja.DisplayMember = "DescripcionCaja"
                cboCaja.SelectedIndex = -1
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub txtNumSesion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumSesion.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                cmdOK_Click(sender, e)
        End Select
    End Sub

End Class

