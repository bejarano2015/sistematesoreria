Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmTipoMovimiento
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Private eTipoMovimiento As clsTipoMovimiento
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmTipoMovimiento = Nothing
    Public Shared Function Instance() As frmTipoMovimiento
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmTipoMovimiento
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtTipoMov.Clear()
        'cboEstado.Clear()
        cboEstado.SelectedIndex = 0
    End Sub

    Private Sub DesabilitarControles()
        txtTipoMov.Enabled = False
        cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        txtTipoMov.Enabled = True
        cboEstado.Enabled = False
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If
        Panel2.Visible = True
        cboEstados.SelectedIndex = -1
        txtBusqueda.Clear()
        txtBusqueda.Focus()
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            DesabilitarControles()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            HabilitarControles()
            cboEstado.Enabled = True
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        ' Else
        ' Exit Sub
        'End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        'Dim iDuplicado As Int32
        Dim sCodigoRegistro As String = ""
        eTempo = New clsPlantTempo
        eTipoMovimiento = New clsTipoMovimiento

        If sTab = 1 Then
            If Me.txtTipoMov.Text.Trim.Length = 0 Or Me.cboEstado.Text.Trim.Length = 0 Then
                eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                frmPrincipal.sFlagGrabar = "0"
            Else
                If (MessageBox.Show("�Esta seguro de Grabar ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then
                        eTipoMovimiento.fCodigo()
                        sCodigoRegistro = eTipoMovimiento.sCodFuturo
                        txtCodigo.Text = sCodigoRegistro
                    Else
                        sCodigoRegistro = Me.txtCodigo.Text
                    End If
                    iResultado = eTipoMovimiento.fGrabar(sCodigoRegistro, Convert.ToString(txtTipoMov.Text.Trim), cboEstado.SelectedValue, iOpcion) '18/06/2007: Modificacion   
                    If iResultado > 0 Then
                        mMostrarGrilla()
                        Timer2.Enabled = True
                    End If
                    sTab = 0
                End If
            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()

        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iActivo As Integer
            If sTab = 0 Then
                Try
                    If Me.dgvLista.Rows.Count > 0 Then
                        If Fila("Estados") = "Activo" Then
                            iActivo = 5
                            Mensaje = "�Desea Anular?"
                            'Else
                            '    iActivo = 3
                            '    Mensaje = "� Desea Eliminar ?"
                        End If
                        If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("IdRendicion") & "   " & Chr(13) & "Descripci�n: " & Fila("DescripcionRendicion"), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                            eTipoMovimiento = New clsTipoMovimiento
                            iResultado = eTipoMovimiento.fEliminar(Fila("IdRendicion"), iActivo)
                            If iResultado = 1 Then
                                mMostrarGrilla()
                            End If
                        Else
                            Exit Sub
                        End If
                    End If
                Catch ex As Exception
                End Try
            End If
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()
        Me.cboEstado.DataSource = eTempo.fColEstado
        Me.cboEstado.ValueMember = "Col02"
        Me.cboEstado.DisplayMember = "Col01"

        Me.cboEstados.DataSource = eTempo.fColEstados
        Me.cboEstados.ValueMember = "Col02"
        Me.cboEstados.DisplayMember = "Col01"
        dgvLista.Focus()
        dgvLista.Width = 430
        dgvLista.Height = 358
    End Sub

    Private Sub mMostrarGrilla()
        eTipoMovimiento = New clsTipoMovimiento
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = eTipoMovimiento.fListar(iEstado01, iEstado02)
        Me.dgvLista.AutoGenerateColumns = False
        Me.dgvLista.DataSource = dtTableLista
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        VerPosicion()
        Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoMovimiento.iNroRegistros
        If eTipoMovimiento.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
        
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdRendicion").ToString
                Me.txtTipoMov.Text = Fila("DescripcionRendicion").ToString
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtTipoMov.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text.Trim) > 0 Then
            eTipoMovimiento = New clsTipoMovimiento
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoMovimiento.fBuscaTipoMovimiento(9, Trim(txtBusqueda.Text), gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoMovimiento.iNroRegistros
            If eTipoMovimiento.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text.Trim) = 0 Then
            eTipoMovimiento = New clsTipoMovimiento
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoMovimiento.fBuscaTipoMovimiento(10, "", gEmpresa, 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoMovimiento.iNroRegistros
            If eTipoMovimiento.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If
    End Sub

    Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
        If (cboEstados.SelectedIndex > -1) Then
            Try
                eTipoMovimiento = New clsTipoMovimiento
                eTempo = New clsPlantTempo
                dtTable = New DataTable
                Dim sValorEstado As String = ""
                sValorEstado = cboEstados.SelectedValue.ToString
                If sValorEstado <> "CapaDatos.clsPlantTempo" Then
                    dtTable = eTipoMovimiento.fBuscaTipoMovimiento(11, "", gEmpresa, cboEstados.SelectedValue)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eTipoMovimiento.iNroRegistros
                    If eTipoMovimiento.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub BeLabel7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel7.Click
        Panel2.Visible = False
    End Sub
End Class
