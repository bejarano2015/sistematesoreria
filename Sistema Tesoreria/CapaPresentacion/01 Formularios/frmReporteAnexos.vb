Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReporteAnexos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAnexos = Nothing
    Public Shared Function Instance() As frmReporteAnexos
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteAnexos
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmReporteAnexos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub
    Private Sub frmReporteAnexos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eChequera As clsChequera

    Private Sub frmReporteAnexos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eChequera = New clsChequera
        cboBancos.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        If eChequera.iNroRegistros > 0 Then
            cboBancos.ValueMember = "IdBanco"
            cboBancos.DisplayMember = "NombreBanco"
            cboBancos.SelectedIndex = -1
        End If

        eChequera = New clsChequera
        cboBancos2.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        If eChequera.iNroRegistros > 0 Then
            cboBancos2.ValueMember = "IdBanco"
            cboBancos2.DisplayMember = "NombreBanco"
            cboBancos2.SelectedIndex = -1
        End If

        eChequera = New clsChequera
        cboTipoCargo.DataSource = eChequera.fListarTiposCuenta()
        If eChequera.iNroRegistros > 0 Then
            cboTipoCargo.ValueMember = "IdTipoCuenta"
            cboTipoCargo.DisplayMember = "Abreviado"
            cboTipoCargo.SelectedIndex = -1
        End If

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        If cboBancos.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboBancos.Focus()
            Exit Sub
        End If
        If cboTipoCargo.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Anexo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboTipoCargo.Focus()
            Exit Sub
        End If

        Dim Banco1 As String = ""
        Dim Banco2 As String = ""
        Dim NombreBanco1 As String = ""
        Dim NombreBanco2 As String = ""
        Banco1 = cboBancos.Text
        Banco2 = cboBancos2.Text
        If cboBancos.SelectedIndex > -1 And cboBancos2.SelectedIndex > -1 Then
            NombreBanco1 = Banco1.Substring(0, Banco1.IndexOf("-")).Trim
            NombreBanco2 = Banco2.Substring(0, Banco2.IndexOf("-")).Trim
            If Trim(NombreBanco1) = Trim(NombreBanco2) Then
            Else
                MessageBox.Show("Seleccione Bancos Iguales y Monedas Distintas", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        End If

        If cboTipoCargo.SelectedValue = "00004" Or cboTipoCargo.SelectedValue = "00003" Then 'deposito plazo
            Dim x As frmReporteAnexos2 = frmReporteAnexos2.Instance
            x.MdiParent = frmPrincipal
            x.RepAnexosIdBanco = Trim(cboBancos.SelectedValue)
            x.RepAnexosIdBanco2 = Trim(cboBancos2.SelectedValue)
            x.RepAnexosTipoCuenta = Trim(cboTipoCargo.SelectedValue)
            x.Show()
        ElseIf cboTipoCargo.SelectedValue = "00002" Then  'prenda
            Dim x As frmReporteAnexos3 = frmReporteAnexos3.Instance
            x.MdiParent = frmPrincipal
            x.RepAnexosIdBancoPrenda = Trim(cboBancos.SelectedValue)
            x.RepAnexosIdBanco2Prenda = Trim(cboBancos2.SelectedValue)
            x.RepAnexosTipoCuentaPrenda = Trim(cboTipoCargo.SelectedValue)
            x.Show()
        ElseIf cboTipoCargo.SelectedValue = "00005" Then  'prenda
            MessageBox.Show("No Existe un Formato de Reporte para este Anexo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If


       
    End Sub
End Class