<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaAsegurados_x_Obra2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConsultaAsegurados_x_Obra2))
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Asegurado = New System.Windows.Forms.Label
        Me.lblTotalRegistro = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.PictureBox4 = New System.Windows.Forms.PictureBox
        Me.txtDesCC = New ctrLibreria.Controles.BeTextBox
        Me.txtCodCC = New ctrLibreria.Controles.BeTextBox
        Me.txtCliente = New ctrLibreria.Controles.BeTextBox
        Me.dgvAsegurados = New ctrLibreria.Controles.BeDataGridView
        Me.Aseguradora = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CodigoPolizaS = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Ramo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DescripcionAsegurado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CodigoCCosto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DescripcionCCosto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Vigencia_FechaInicio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Vigencia_FechaFin = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SumaAsegurada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DesCondCancelacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgvAsegurados2 = New System.Windows.Forms.DataGridView
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CodigoSubPoliza = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Endoso = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAsegurados, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAsegurados2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Asegurado
        '
        Me.Asegurado.AutoSize = True
        Me.Asegurado.Location = New System.Drawing.Point(321, 47)
        Me.Asegurado.Name = "Asegurado"
        Me.Asegurado.Size = New System.Drawing.Size(58, 13)
        Me.Asegurado.TabIndex = 3
        Me.Asegurado.Text = "Asegurado"
        '
        'lblTotalRegistro
        '
        Me.lblTotalRegistro.AutoSize = True
        Me.lblTotalRegistro.Location = New System.Drawing.Point(9, 433)
        Me.lblTotalRegistro.Name = "lblTotalRegistro"
        Me.lblTotalRegistro.Size = New System.Drawing.Size(39, 13)
        Me.lblTotalRegistro.TabIndex = 6
        Me.lblTotalRegistro.Text = "Label2"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(296, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Centro de Costo"
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.White
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(469, 16)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(20, 16)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 153
        Me.PictureBox4.TabStop = False
        '
        'txtDesCC
        '
        Me.txtDesCC.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDesCC.BackColor = System.Drawing.Color.Ivory
        Me.txtDesCC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDesCC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDesCC.Enabled = False
        Me.txtDesCC.ForeColor = System.Drawing.Color.Black
        Me.txtDesCC.KeyEnter = True
        Me.txtDesCC.Location = New System.Drawing.Point(520, 14)
        Me.txtDesCC.Name = "txtDesCC"
        Me.txtDesCC.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDesCC.ShortcutsEnabled = False
        Me.txtDesCC.Size = New System.Drawing.Size(325, 20)
        Me.txtDesCC.TabIndex = 2
        Me.txtDesCC.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCodCC
        '
        Me.txtCodCC.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodCC.BackColor = System.Drawing.Color.Ivory
        Me.txtCodCC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodCC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodCC.ForeColor = System.Drawing.Color.Black
        Me.txtCodCC.KeyEnter = True
        Me.txtCodCC.Location = New System.Drawing.Point(400, 14)
        Me.txtCodCC.MaxLength = 10
        Me.txtCodCC.Name = "txtCodCC"
        Me.txtCodCC.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodCC.ShortcutsEnabled = False
        Me.txtCodCC.Size = New System.Drawing.Size(91, 20)
        Me.txtCodCC.TabIndex = 1
        Me.txtCodCC.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCliente
        '
        Me.txtCliente.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCliente.BackColor = System.Drawing.Color.Ivory
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCliente.ForeColor = System.Drawing.Color.Black
        Me.txtCliente.KeyEnter = True
        Me.txtCliente.Location = New System.Drawing.Point(400, 41)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCliente.ShortcutsEnabled = False
        Me.txtCliente.Size = New System.Drawing.Size(445, 20)
        Me.txtCliente.TabIndex = 4
        Me.txtCliente.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'dgvAsegurados
        '
        Me.dgvAsegurados.AllowUserToAddRows = False
        Me.dgvAsegurados.AllowUserToDeleteRows = False
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvAsegurados.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvAsegurados.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAsegurados.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvAsegurados.BackgroundColor = System.Drawing.Color.Gray
        Me.dgvAsegurados.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvAsegurados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAsegurados.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Aseguradora, Me.CodigoPolizaS, Me.Ramo, Me.Total, Me.DescripcionAsegurado, Me.CodigoCCosto, Me.DescripcionCCosto, Me.Vigencia_FechaInicio, Me.Vigencia_FechaFin, Me.SumaAsegurada, Me.DesCondCancelacion})
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAsegurados.DefaultCellStyle = DataGridViewCellStyle18
        Me.dgvAsegurados.EnableHeadersVisualStyles = False
        Me.dgvAsegurados.Location = New System.Drawing.Point(12, 258)
        Me.dgvAsegurados.MultiSelect = False
        Me.dgvAsegurados.Name = "dgvAsegurados"
        Me.dgvAsegurados.ReadOnly = True
        Me.dgvAsegurados.RowHeadersVisible = False
        Me.dgvAsegurados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAsegurados.Size = New System.Drawing.Size(1238, 160)
        Me.dgvAsegurados.TabIndex = 5
        '
        'Aseguradora
        '
        Me.Aseguradora.DataPropertyName = "Aseguradora"
        Me.Aseguradora.HeaderText = "Aseguradora"
        Me.Aseguradora.Name = "Aseguradora"
        Me.Aseguradora.ReadOnly = True
        Me.Aseguradora.Width = 160
        '
        'CodigoPolizaS
        '
        Me.CodigoPolizaS.DataPropertyName = "CodigoPolizaS"
        Me.CodigoPolizaS.HeaderText = "Poliza"
        Me.CodigoPolizaS.Name = "CodigoPolizaS"
        Me.CodigoPolizaS.ReadOnly = True
        '
        'Ramo
        '
        Me.Ramo.DataPropertyName = "Ramo"
        Me.Ramo.HeaderText = "Ramo"
        Me.Ramo.Name = "Ramo"
        Me.Ramo.ReadOnly = True
        Me.Ramo.Width = 105
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.Total.DefaultCellStyle = DataGridViewCellStyle14
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'DescripcionAsegurado
        '
        Me.DescripcionAsegurado.DataPropertyName = "DescripcionAsegurado"
        Me.DescripcionAsegurado.HeaderText = "Asegurado"
        Me.DescripcionAsegurado.Name = "DescripcionAsegurado"
        Me.DescripcionAsegurado.ReadOnly = True
        Me.DescripcionAsegurado.Width = 180
        '
        'CodigoCCosto
        '
        Me.CodigoCCosto.DataPropertyName = "CodigoCCosto"
        Me.CodigoCCosto.HeaderText = "Cod CCosto"
        Me.CodigoCCosto.Name = "CodigoCCosto"
        Me.CodigoCCosto.ReadOnly = True
        Me.CodigoCCosto.Width = 90
        '
        'DescripcionCCosto
        '
        Me.DescripcionCCosto.DataPropertyName = "DescripcionCCosto"
        Me.DescripcionCCosto.HeaderText = "Des CCosto"
        Me.DescripcionCCosto.Name = "DescripcionCCosto"
        Me.DescripcionCCosto.ReadOnly = True
        '
        'Vigencia_FechaInicio
        '
        Me.Vigencia_FechaInicio.DataPropertyName = "Vigencia_FechaInicio"
        DataGridViewCellStyle15.Format = "d"
        DataGridViewCellStyle15.NullValue = Nothing
        Me.Vigencia_FechaInicio.DefaultCellStyle = DataGridViewCellStyle15
        Me.Vigencia_FechaInicio.HeaderText = "Fecha Inicio"
        Me.Vigencia_FechaInicio.Name = "Vigencia_FechaInicio"
        Me.Vigencia_FechaInicio.ReadOnly = True
        Me.Vigencia_FechaInicio.Width = 90
        '
        'Vigencia_FechaFin
        '
        Me.Vigencia_FechaFin.DataPropertyName = "Vigencia_FechaFin"
        DataGridViewCellStyle16.Format = "d"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.Vigencia_FechaFin.DefaultCellStyle = DataGridViewCellStyle16
        Me.Vigencia_FechaFin.HeaderText = "Fecha Fin"
        Me.Vigencia_FechaFin.Name = "Vigencia_FechaFin"
        Me.Vigencia_FechaFin.ReadOnly = True
        Me.Vigencia_FechaFin.Width = 90
        '
        'SumaAsegurada
        '
        Me.SumaAsegurada.DataPropertyName = "SumaAsegurada"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle17.Format = "N2"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.SumaAsegurada.DefaultCellStyle = DataGridViewCellStyle17
        Me.SumaAsegurada.HeaderText = "Suma"
        Me.SumaAsegurada.Name = "SumaAsegurada"
        Me.SumaAsegurada.ReadOnly = True
        '
        'DesCondCancelacion
        '
        Me.DesCondCancelacion.DataPropertyName = "DesCondCancelacion"
        Me.DesCondCancelacion.HeaderText = "Cancelacion"
        Me.DesCondCancelacion.Name = "DesCondCancelacion"
        Me.DesCondCancelacion.ReadOnly = True
        '
        'dgvAsegurados2
        '
        Me.dgvAsegurados2.AllowUserToAddRows = False
        Me.dgvAsegurados2.BackgroundColor = System.Drawing.Color.White
        Me.dgvAsegurados2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAsegurados2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.CodigoSubPoliza, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Endoso, Me.Column12, Me.Column13})
        Me.dgvAsegurados2.EnableHeadersVisualStyles = False
        Me.dgvAsegurados2.Location = New System.Drawing.Point(12, 73)
        Me.dgvAsegurados2.Name = "dgvAsegurados2"
        Me.dgvAsegurados2.ReadOnly = True
        Me.dgvAsegurados2.RowHeadersVisible = False
        Me.dgvAsegurados2.Size = New System.Drawing.Size(1238, 346)
        Me.dgvAsegurados2.TabIndex = 154
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1062, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 155
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(1088, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 156
        Me.Label1.Text = "F1 = Buscar"
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.Location = New System.Drawing.Point(1136, 428)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(114, 23)
        Me.Button1.TabIndex = 157
        Me.Button1.Text = "Exportar a Excel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "Aseguradora"
        Me.Column1.HeaderText = "Aseguradora"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 120
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "CodigoPolizaS"
        Me.Column2.HeaderText = "Poliza"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'CodigoSubPoliza
        '
        Me.CodigoSubPoliza.DataPropertyName = "CodigoSubPolizaS"
        Me.CodigoSubPoliza.HeaderText = "EndosoPoliza"
        Me.CodigoSubPoliza.Name = "CodigoSubPoliza"
        Me.CodigoSubPoliza.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Ramo"
        Me.Column3.HeaderText = "Ramo"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 90
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Total"
        Me.Column4.HeaderText = "Total"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "DescripcionAsegurado"
        Me.Column5.HeaderText = "Asegurado"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "CodigoCCosto"
        Me.Column6.HeaderText = "Cod CCosto"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Visible = False
        Me.Column6.Width = 90
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "DescripcionCCosto"
        Me.Column7.HeaderText = "Des CCosto"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 90
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Vigencia_FechaInicio"
        Me.Column8.HeaderText = "F. Inicio"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 70
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "Vigencia_FechaFin"
        Me.Column9.HeaderText = "F. Fin"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 70
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "SumaAsegurada"
        Me.Column10.HeaderText = "Suma"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 70
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "DesCondCancelacion"
        Me.Column11.HeaderText = "Cancelacion"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 80
        '
        'Endoso
        '
        Me.Endoso.DataPropertyName = "cuotas"
        Me.Endoso.HeaderText = "Cuotas"
        Me.Endoso.Name = "Endoso"
        Me.Endoso.ReadOnly = True
        Me.Endoso.Width = 80
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "ImporteCuota"
        Me.Column12.HeaderText = "Importe"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Width = 80
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "descripcionendoso"
        Me.Column13.HeaderText = "Endoso"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 80
        '
        'frmConsultaAsegurados_x_Obra2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1262, 455)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.dgvAsegurados2)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtDesCC)
        Me.Controls.Add(Me.txtCodCC)
        Me.Controls.Add(Me.lblTotalRegistro)
        Me.Controls.Add(Me.Asegurado)
        Me.Controls.Add(Me.txtCliente)
        Me.Controls.Add(Me.dgvAsegurados)
        Me.MaximizeBox = False
        Me.Name = "frmConsultaAsegurados_x_Obra2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Bienes y Asegurados por Obra"
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAsegurados, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAsegurados2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvAsegurados As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents txtCliente As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Asegurado As System.Windows.Forms.Label
    Friend WithEvents lblTotalRegistro As System.Windows.Forms.Label
    Friend WithEvents txtCodCC As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDesCC As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Aseguradora As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodigoPolizaS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ramo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionAsegurado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodigoCCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionCCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vigencia_FechaInicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vigencia_FechaFin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SumaAsegurada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DesCondCancelacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents dgvAsegurados2 As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodigoSubPoliza As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Endoso As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
