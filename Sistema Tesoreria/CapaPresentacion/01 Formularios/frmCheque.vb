Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmCheque
    Dim WithEvents cmr As CurrencyManager

    Public NuevoNo As Integer = 1

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eCheque As clsCheque
    Private eChequera As clsChequera

    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCheque = Nothing
    Public Shared Function Instance() As frmCheque
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmCheque
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtMontoEmitido.Clear()
        txtResponsable.Clear()
        'Me.cboEstado.SelectedIndex = 0
        Me.cboAnulado.SelectedIndex = 0
    End Sub

    Private Sub DesabilitarControles()
        cboMoneda.Enabled = False
        txtNumeroCheque.ReadOnly = True
        cboChequera.Enabled = False
        dtpFechaEmision.Enabled = False
        txtMontoEmitido.ReadOnly = True
        txtResponsable.ReadOnly = True
        cboAnulado.Enabled = False
        'cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        cboMoneda.Enabled = True
        txtMontoEmitido.ReadOnly = False
        txtResponsable.ReadOnly = False
        cboAnulado.Enabled = True
        'cboEstado.Enabled = True
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        Panel2.Visible = True
        rdbFecha.Checked = False
        rdbMes.Checked = False
        cboMes.SelectedIndex = -1
        cboAnio.SelectedIndex = -1
        cboEstCheque.SelectedIndex = -1
        cboChequeras.SelectedIndex = -1
        cboCuenta.SelectedIndex = -1
        cboBanco.SelectedIndex = -1
        '
        'If cboChequeras.Items.Count > 0 Then
        '    cboChequeras.Items.Clear()
        'End If

        ''cboChequeras.Items.Clear()

        'If cboCuenta.Items.Count > 0 Then
        '    cboCuenta.Items.Clear()
        'End If

        'cboCuenta.Items.Clear()
        '
        cboBanco.Focus()
        'End If
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            'cboEstado.Enabled = False
            cboAnulado.Enabled = False
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            HabilitarControles()
            cboMoneda.Enabled = False
            txtNumeroCheque.Enabled = False
            cboChequera.Enabled = False

            'Timer1.Enabled = True
        End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        ' If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        ' Else
        ' Exit Sub
        ' End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        'Dim iDuplicado As Int32
        Dim sCodigoRegistro As String = ""
        eTempo = New clsPlantTempo
        eCheque = New clsCheque
        If sTab = 1 Then
            If Me.txtNumeroCheque.Text.Trim.Length = 0 Then
                eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                frmPrincipal.sFlagGrabar = "0"
            Else
                'iDuplicado = eCheque.fBuscarDoble(Convert.ToString(Me.dtpFechaEmision.Text.Trim), Convert.ToString(Me.txtCodigo.Text.Trim)) 'Modificado el 05/06
                'If iDuplicado > 0 Then 'Modificado el 05/06
                'Exit Sub 'Modificado el 05/06
                'End If 'Modificado el 05/06
                If (MessageBox.Show("�Esta seguro de Grabar ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then
                        eCheque.fCodigo()
                        sCodigoRegistro = eCheque.sCodFuturo
                        Me.txtCodigo.Text = sCodigoRegistro
                    Else
                        sCodigoRegistro = Trim(Me.txtCodigo.Text)
                    End If
                    Dim dMonto As Decimal = 0
                    If Len(Trim(txtMontoEmitido.Text)) > 0 Then
                        dMonto = Trim(txtMontoEmitido.Text)
                    Else
                        dMonto = 0
                    End If

                    iResultado = eCheque.fGrabar(gEmpresa, sCodigoRegistro, Convert.ToString(Me.cboChequera.SelectedValue), Convert.ToString(Me.cboMoneda.SelectedValue), gUsuario, Convert.ToString(Me.cboChequera.Text), Convert.ToString(Me.dtpFechaEmision.Text.Trim), dMonto, Convert.ToString(Me.txtResponsable.Text.Trim), Convert.ToString(Me.cboAnulado.SelectedValue), iOpcion)

                    If iResultado > 0 Then
                        mMostrarGrilla()
                        Timer2.Enabled = True
                    End If

                    sTab = 0

                End If
            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'VerPosicion()
        ''MessageBox.Show(Fila("Estados"))
        'If Fila("Estados") = "Pendiente" Then
        '    Dim iResultado As Int32
        '    Dim Mensaje As String = ""
        '    Dim iActivo As Integer
        '    If sTab = 0 Then
        '        Try
        '            If Me.dgvLista.Rows.Count > 0 Then
        '                If Fila("Estados") = "Pendiente" Then
        '                    iActivo = 5 '18/06/2007: Modificacion Realizada
        '                    Mensaje = "�Desea Anular?"
        '                    'Else
        '                    '    iActivo = 3 '18/06/2007: Modificacion Realizada
        '                    '    Mensaje = "Desea ELIMINAR: "
        '                End If
        '                If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("IdCheque") & "   " & Chr(13) & "Numero de Cheque: " & Fila("NumeroCheque"), "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        '                    'Using scope As TransactionScope = New TransactionScope '(MSDTC)
        '                    eCheque = New clsCheque
        '                    iResultado = eCheque.fEliminar(gEmpresa, Fila("IdCheque"), iActivo) '18/06/2007: Modificacion Realizada
        '                    'scope.Complete()
        '                    'End Using
        '                    If iResultado = 1 Then
        '                        mMostrarGrilla()
        '                    End If
        '                Else
        '                    Exit Sub
        '                End If
        '            End If
        '        Catch ex As Exception
        '        End Try
        '    End If
        'Else
        '    MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End If
        MessageBox.Show("Opcion Inhabilitada, los Cheques se Anulan o Eliminan desde Egresos de Caja", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eChequera = New clsChequera

        mMostrarGrilla()
        dtpFechaEmision.Value = Now.Date()
        dtpF1.Value = Now.Date()
        dtpF2.Value = Now.Date()
        frmPrincipal.fBuscar = False 'Desabilita los botones de buscar
        frmPrincipal.fExportar = False 'Desabilita los botones de exportar
        frmPrincipal.fImprimir = False 'Desabilita los botones de imprimir

        Me.cboAnulado.DataSource = eTempo.fColEstado2
        Me.cboAnulado.ValueMember = "Col02"
        Me.cboAnulado.DisplayMember = "Col01"

        Me.cboEstCheque.DataSource = eTempo.fColEstado4
        Me.cboEstCheque.ValueMember = "Col02"
        Me.cboEstCheque.DisplayMember = "Col01"
        Me.cboEstCheque.SelectedIndex = -1

        Me.cboMoneda.DataSource = eCheque.fListarMoneda
        Me.cboMoneda.ValueMember = "MonCodigo"
        Me.cboMoneda.DisplayMember = "MonDescripcion"

        Me.cboChequera.DataSource = eCheque.fListarChequera
        Me.cboChequera.ValueMember = "IdChequera"
        Me.cboChequera.DisplayMember = "NroChequera"

        cboBanco.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        cboBanco.ValueMember = "IdBanco"
        cboBanco.DisplayMember = "NombreBanco"
        cboBanco.SelectedIndex = -1

        dgvLista.Focus()
        dgvLista.Width = 1045
        dgvLista.Height = 393
        'frmPrincipal.Barra.Items("Nuevo").Enabled = False
        'frmPrincipal.Modifica.Enabled = False
        'frmPrincipal.Barra.Items.Item(1).Enabled = False
        'frmPrincipal.Barra.Items.Item(2).Enabled = False
        'frmPrincipal.Barra.Items.Item(3).Enabled = False
        'frmPrincipal.Barra.Items.Item(4).Enabled = False
        'frmPrincipal.Barra.Items.Item(5).Enabled = False

    End Sub

    Private Sub mMostrarGrilla()
        eCheque = New clsCheque
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eCheque.fListar(gEmpresa, iEstado01, iEstado02)
        Me.dgvLista.AutoGenerateColumns = False
        Me.dgvLista.DataSource = dtTable
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        'VerPosicion()
        Me.stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
        If eCheque.iNroRegistros > 0 Then
            frmPrincipal.Modifica.Enabled = True
            frmPrincipal.Elimina.Enabled = True
            frmPrincipal.Visualiza.Enabled = True
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If

        

        'Me.cboEstado.DataSource = eTempo.fColEstado
        'Me.cboEstado.ValueMember = "Col02"
        'Me.cboEstado.DisplayMember = "Col01"

    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdCheque").ToString
                Me.cboChequera.Text = Fila("NroChequera").ToString
                Me.cboMoneda.Text = Fila("MonDescripcion").ToString
                Me.txtNumeroCheque.Text = Fila("NumeroCheque").ToString
                'Me.cboChequera.Text = Fila("NroChequera").ToString
                Me.dtpFechaEmision.Text = Fila("FechaEmision").ToString
                Me.txtMontoEmitido.Text = Fila("MontoEmitido").ToString
                Me.txtResponsable.Text = Fila("Responsable").ToString
                'Me.cboAnulado.Text = Fila("Anulado").ToString
                'Me.cboEstado.Text = Fila("Estado").ToString
                'If Fila("Estado").ToString = 0 Then
                '    Me.cboEstado.SelectedIndex = 0
                'Else
                '    Me.cboEstado.SelectedIndex = 1
                'End If
                If Fila("Anulado").ToString = 0 Then
                    Me.cboAnulado.SelectedIndex = 0
                ElseIf Fila("Anulado").ToString = 1 Then
                    Me.cboAnulado.SelectedIndex = 1
                ElseIf Fila("Anulado").ToString = 2 Then
                    Me.cboAnulado.SelectedIndex = 2
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    'Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
    '    Try
    '        VerPosicion()
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.dtpFechaEmision.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Dim IdBanco As String = ""
        If (cboBanco.SelectedIndex > -1) Then
            Try
                IdBanco = Trim(cboBanco.SelectedValue.ToString)
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuenta.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    cboCuenta.ValueMember = "IdCuenta"
                    cboCuenta.DisplayMember = "NumeroCuenta"
                    cboCuenta.SelectedIndex = -1
                    cboChequeras.SelectedIndex = -1

                    'cboChequeras.Items.Clear()


                    '----------------------------->
                    eCheque = New clsCheque
                    dtTable = New DataTable
                    dtTable = eCheque.fListarConsultaCheques(0, Trim(gEmpresa), Trim(IdBanco), "", "", 0, "", dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                    If eCheque.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                    '----------------------------->
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub


    Private Sub cboCuenta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuenta.SelectedIndexChanged
        Dim IdCuenta As String = ""
        If (cboCuenta.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuenta.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    cboChequeras.DataSource = eChequera.fListarChequerasxBanyCuen(gEmpresa, Trim(cboBanco.SelectedValue), IdCuenta)
                    cboChequeras.ValueMember = "IdChequera"
                    cboChequeras.DisplayMember = "NroChequera"
                    cboChequeras.SelectedIndex = -1

                    '----------------------------->
                    If cboBanco.SelectedIndex > -1 Then
                        eCheque = New clsCheque
                        dtTable = New DataTable
                        dtTable = eCheque.fListarConsultaCheques(1, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(IdCuenta), "", 0, "", dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                        dgvLista.AutoGenerateColumns = False
                        dgvLista.DataSource = dtTable
                        cmr = BindingContext(dgvLista.DataSource)
                        stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                        If eCheque.iNroRegistros > 0 Then
                            frmPrincipal.Modifica.Enabled = True
                            frmPrincipal.Elimina.Enabled = True
                            frmPrincipal.Visualiza.Enabled = True
                            frmPrincipal.fDatos = True
                        Else
                            frmPrincipal.Modifica.Enabled = False
                            frmPrincipal.Elimina.Enabled = False
                            frmPrincipal.Visualiza.Enabled = False
                            frmPrincipal.fDatos = False
                        End If
                    End If
                    '----------------------------->

                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If

    End Sub


    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        Panel2.Visible = False
    End Sub

    Private Sub cboChequeras_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChequeras.SelectedIndexChanged
        Dim IdChequera As String = ""
        If (cboChequeras.SelectedIndex > -1) Then
            Try
                IdChequera = cboChequeras.SelectedValue.ToString
                If IdChequera <> "System.Data.DataRowView" Then
                    'cboChequeras.DataSource = eChequera.fListarChequerasxBanyCuen(gEmpresa, Trim(cboBanco.SelectedValue), IdCuenta)
                    'cboChequeras.ValueMember = "IdChequera"
                    'cboChequeras.DisplayMember = "NroChequera"
                    'cboChequeras.SelectedIndex = -1

                    '----------------------------->
                    If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                        eCheque = New clsCheque
                        dtTable = New DataTable
                        dtTable = eCheque.fListarConsultaCheques(2, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), IdChequera, 0, "", dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                        dgvLista.AutoGenerateColumns = False
                        dgvLista.DataSource = dtTable
                        cmr = BindingContext(dgvLista.DataSource)
                        stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                        If eCheque.iNroRegistros > 0 Then
                            frmPrincipal.Modifica.Enabled = True
                            frmPrincipal.Elimina.Enabled = True
                            frmPrincipal.Visualiza.Enabled = True
                            frmPrincipal.fDatos = True
                        Else
                            frmPrincipal.Modifica.Enabled = False
                            frmPrincipal.Elimina.Enabled = False
                            frmPrincipal.Visualiza.Enabled = False
                            frmPrincipal.fDatos = False
                        End If
                    End If
                    '----------------------------->

                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    'Private Sub rdbFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbFecha.CheckedChanged
    '    cboBanco.SelectedIndex = -1
    '    cboCuenta.SelectedIndex = -1
    '    cboChequeras.SelectedIndex = -1
    '    ' rdbMes.Checked = False
    'End Sub

    'Private Sub rdbMes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbMes.CheckedChanged
    '    cboBanco.SelectedIndex = -1
    '    cboCuenta.SelectedIndex = -1
    '    cboChequeras.SelectedIndex = -1
    '    ' rdbFecha.Checked = False
    'End Sub

    Private Sub cboMes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMes.SelectedIndexChanged

        If rdbMes.Checked = True And cboMes.SelectedIndex > -1 Then

            '----------------------------->
            eCheque = New clsCheque
            dtTable = New DataTable
            If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                'traer chekes solo del banco con ese mes
                dtTable = eCheque.fListarConsultaCheques(3, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", TraerNumeroMes(Me.cboMes.Text), "", dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                'traer checkes del banco con la cuenta y el mes
                dtTable = eCheque.fListarConsultaCheques(4, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", TraerNumeroMes(Me.cboMes.Text), "", dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                'traer checkes del banco con la cuenta con su id de chekera y el mes
                dtTable = eCheque.fListarConsultaCheques(5, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), TraerNumeroMes(Me.cboMes.Text), "", dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            Else
                Exit Sub
            End If

            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
            If eCheque.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
            '----------------------------->
        ElseIf rdbMes.Checked = True And cboMes.SelectedIndex > -1 And cboAnio.SelectedIndex > -1 Then
            'filtrar ckekes con a�o y mes
            '----------------------------->
            eCheque = New clsCheque
            dtTable = New DataTable
            If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                'traer chekes solo del banco con ese mes y a�o
                dtTable = eCheque.fListarConsultaCheques(6, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                'traer checkes del banco con la cuenta y el mes y a�o
                dtTable = eCheque.fListarConsultaCheques(7, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                'traer checkes del banco con la cuenta con su id de chekera y el mes y a�o
                dtTable = eCheque.fListarConsultaCheques(8, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            Else
                Exit Sub
            End If

            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
            If eCheque.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
            '----------------------------->
        End If

    End Sub

    Private Sub cboAnio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAnio.SelectedIndexChanged

        If rdbMes.Checked = True And cboMes.SelectedIndex > -1 And cboAnio.SelectedIndex > -1 Then
            'filtrar ckekes con a�o y mes
            '----------------------------->
            eCheque = New clsCheque
            dtTable = New DataTable
            If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                'traer chekes solo del banco con ese mes y a�o
                dtTable = eCheque.fListarConsultaCheques(6, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                'traer checkes del banco con la cuenta y el mes y a�o
                dtTable = eCheque.fListarConsultaCheques(7, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                'traer checkes del banco con la cuenta con su id de chekera y el mes y a�o
                dtTable = eCheque.fListarConsultaCheques(8, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            Else
                Exit Sub
            End If

            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
            If eCheque.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
            '----------------------------->
        ElseIf rdbMes.Checked = True And cboMes.SelectedIndex = -1 And cboAnio.SelectedIndex > -1 Then
            'filtrar chekes por a�o
            '----------------------------->
            eCheque = New clsCheque
            dtTable = New DataTable
            If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                'traer chekes solo del banco con ese mes y a�o
                dtTable = eCheque.fListarConsultaCheques(9, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", 0, Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                'traer checkes del banco con la cuenta y el mes y a�o
                dtTable = eCheque.fListarConsultaCheques(10, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", 0, Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                'traer checkes del banco con la cuenta con su id de chekera y el mes y a�o
                dtTable = eCheque.fListarConsultaCheques(11, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), 0, Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
            Else
                Exit Sub
            End If

            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
            If eCheque.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
            '----------------------------->
        End If

    End Sub

    Private Sub dtpF2_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpF2.ValueChanged

        If rdbFecha.Checked = True Then
            If dtpF1.Value > dtpF2.Value Then
                MessageBox.Show("La Fecha de Inicio supera a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                '----------------------------->
                eCheque = New clsCheque
                dtTable = New DataTable
                If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                    'traer chekes solo del banco con rango de fechas
                    dtTable = eCheque.fListarConsultaCheques(12, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                    'traer checkes del banco con la cuenta con rango de fechas
                    dtTable = eCheque.fListarConsultaCheques(13, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                    'traer checkes del banco con la cuenta con su id de chekera con rango de fechas
                    dtTable = eCheque.fListarConsultaCheques(14, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                Else
                    Exit Sub
                End If

                dgvLista.AutoGenerateColumns = False
                dgvLista.DataSource = dtTable
                cmr = BindingContext(dgvLista.DataSource)
                stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                If eCheque.iNroRegistros > 0 Then
                    frmPrincipal.Modifica.Enabled = True
                    frmPrincipal.Elimina.Enabled = True
                    frmPrincipal.Visualiza.Enabled = True
                    frmPrincipal.fDatos = True
                Else
                    frmPrincipal.Modifica.Enabled = False
                    frmPrincipal.Elimina.Enabled = False
                    frmPrincipal.Visualiza.Enabled = False
                    frmPrincipal.fDatos = False
                End If
                '----------------------------->

            End If
        End If

    End Sub

    Private Sub cboEstCheque_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstCheque.SelectedIndexChanged

        If cboEstCheque.SelectedIndex > -1 Then

            If rdbMes.Checked = True And cboMes.SelectedIndex > -1 And cboAnio.SelectedIndex > -1 Then
                'filtrar ckekes con a�o y mes y diferentes estados
                '----------------------------->
                eCheque = New clsCheque
                dtTable = New DataTable
                If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                    'traer chekes solo del banco con ese mes y a�o
                    dtTable = eCheque.fListarConsultaCheques(15, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                    'traer checkes del banco con la cuenta y el mes y a�o
                    dtTable = eCheque.fListarConsultaCheques(16, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                    'traer checkes del banco con la cuenta con su id de chekera y el mes y a�o
                    dtTable = eCheque.fListarConsultaCheques(17, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                Else
                    Exit Sub
                End If

                dgvLista.AutoGenerateColumns = False
                dgvLista.DataSource = dtTable
                cmr = BindingContext(dgvLista.DataSource)
                stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                If eCheque.iNroRegistros > 0 Then
                    frmPrincipal.Modifica.Enabled = True
                    frmPrincipal.Elimina.Enabled = True
                    frmPrincipal.Visualiza.Enabled = True
                    frmPrincipal.fDatos = True
                Else
                    frmPrincipal.Modifica.Enabled = False
                    frmPrincipal.Elimina.Enabled = False
                    frmPrincipal.Visualiza.Enabled = False
                    frmPrincipal.fDatos = False
                End If
                '----------------------------->
            ElseIf rdbMes.Checked = True And cboMes.SelectedIndex = -1 And cboAnio.SelectedIndex > -1 Then
                'filtrar chekes por a�o
                '----------------------------->
                eCheque = New clsCheque
                dtTable = New DataTable
                If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                    'traer chekes solo del banco con ese mes y a�o
                    dtTable = eCheque.fListarConsultaCheques(18, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", 0, Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                    'traer checkes del banco con la cuenta y el mes y a�o
                    dtTable = eCheque.fListarConsultaCheques(19, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", 0, Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                    'traer checkes del banco con la cuenta con su id de chekera y el mes y a�o
                    dtTable = eCheque.fListarConsultaCheques(20, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), 0, Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                Else
                    Exit Sub
                End If

                dgvLista.AutoGenerateColumns = False
                dgvLista.DataSource = dtTable
                cmr = BindingContext(dgvLista.DataSource)
                stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                If eCheque.iNroRegistros > 0 Then
                    frmPrincipal.Modifica.Enabled = True
                    frmPrincipal.Elimina.Enabled = True
                    frmPrincipal.Visualiza.Enabled = True
                    frmPrincipal.fDatos = True
                Else
                    frmPrincipal.Modifica.Enabled = False
                    frmPrincipal.Elimina.Enabled = False
                    frmPrincipal.Visualiza.Enabled = False
                    frmPrincipal.fDatos = False
                End If
                '----------------------------->
            End If

            If rdbFecha.Checked = True Then
                If dtpF1.Value > dtpF2.Value Then
                    MessageBox.Show("La Fecha de Inicio supera a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    '----------------------------->
                    eCheque = New clsCheque
                    dtTable = New DataTable
                    If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex = -1 And cboChequeras.SelectedIndex = -1 Then
                        'traer chekes solo del banco con rango de fechas
                        dtTable = eCheque.fListarConsultaCheques(21, Trim(gEmpresa), Trim(cboBanco.SelectedValue), "", "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                    ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex = -1 Then
                        'traer checkes del banco con la cuenta con rango de fechas
                        dtTable = eCheque.fListarConsultaCheques(22, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), "", TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                    ElseIf cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 Then
                        'traer checkes del banco con la cuenta con su id de chekera con rango de fechas
                        dtTable = eCheque.fListarConsultaCheques(23, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), TraerNumeroMes(Me.cboMes.Text), Trim(cboAnio.Text), dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)
                    Else
                        Exit Sub
                    End If

                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                    If eCheque.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                    '----------------------------->

                End If
            End If

            If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboChequeras.SelectedIndex > -1 And cboEstCheque.SelectedIndex > -1 Then
                'traer checkes del banco con la cuenta con su id de chekera y el mes y a�o
                eCheque = New clsCheque
                dtTable = New DataTable


                dtTable = eCheque.fListarConsultaCheques(24, Trim(gEmpresa), Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboChequeras.SelectedValue), 0, "", dtpF1.Value, dtpF2.Value, cboEstCheque.SelectedValue)

                
                dgvLista.AutoGenerateColumns = False
                dgvLista.DataSource = dtTable
                cmr = BindingContext(dgvLista.DataSource)
                stsTotales.Items(0).Text = "Total de Registros= " & eCheque.iNroRegistros
                If eCheque.iNroRegistros > 0 Then
                    frmPrincipal.Modifica.Enabled = True
                    frmPrincipal.Elimina.Enabled = True
                    frmPrincipal.Visualiza.Enabled = True
                    frmPrincipal.fDatos = True
                Else
                    frmPrincipal.Modifica.Enabled = False
                    frmPrincipal.Elimina.Enabled = False
                    frmPrincipal.Visualiza.Enabled = False
                    frmPrincipal.fDatos = False
                End If
            End If

        End If

    End Sub


    Private Sub dtpF1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpF1.ValueChanged
        dtpF2_ValueChanged(sender, e)
    End Sub

    Private Sub rdbFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbFecha.CheckedChanged

    End Sub

    Private Sub rdbMes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbMes.CheckedChanged

    End Sub
End Class
