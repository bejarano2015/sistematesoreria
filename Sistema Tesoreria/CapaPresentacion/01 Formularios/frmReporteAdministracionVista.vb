Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmReporteAdministracionVista

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Private eGastosGenerales As clsGastosGenerales

    Public MesLetras As String = ""
    Public A�o As String = ""
    Public Categoria As String = ""
    Public Res_O_Det As String = ""
    Public dTipoCambio As Double = 0
    Public Moneda As String = ""
    Public MonedaDes As String = ""

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAdministracionVista = Nothing
    Public Shared Function Instance() As frmReporteAdministracionVista
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteAdministracionVista
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteAdministracionVista_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteAdministracionVista_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rptInversionEnCapital As New rptGastosInversionEnCapital
        Dim rptGastosAdmFijos As New rptGastosAdmFijos
        Dim rptGastosAdmOcacionales As New rptGastosAdmOcacionales

        Dim rptInversionEnCapitalDet As New rptGastosInversionEnCapitalDetallado
        Dim rptGastosAdmFijosDet As New rptGastosAdmFijosDetallado
        Dim rptGastosAdmOcacionalesDet As New rptGastosAdmOcacionalesDetallado

        Dim rptGastosPorObra As New rptGastosPorObraCruzadas

        If Trim(Categoria) = "1" And Res_O_Det = "R" Then

            rptInversionEnCapital.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4

            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(A�o)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptInversionEnCapital.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = dTipoCambio
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '----------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptInversionEnCapital

        ElseIf Trim(Categoria) = "1" And Res_O_Det = "D" Then

            rptInversionEnCapitalDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(A�o)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptInversionEnCapitalDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = dTipoCambio
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptInversionEnCapitalDet

        ElseIf Trim(Categoria) = "2" And Res_O_Det = "R" Then

            rptGastosAdmFijos.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            'crParameterFieldDefiniti()
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(A�o)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmFijos.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = dTipoCambio
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmFijos

        ElseIf Trim(Categoria) = "2" And Res_O_Det = "D" Then

            rptGastosAdmFijosDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(A�o)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmFijosDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = dTipoCambio
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmFijosDet

        ElseIf Trim(Categoria) = "3" And Res_O_Det = "R" Then

            rptGastosAdmOcacionales.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(A�o)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmOcacionales.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = dTipoCambio
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmOcacionales

        ElseIf Trim(Categoria) = "3" And Res_O_Det = "D" Then


            rptGastosAdmOcacionalesDet.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Mes")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(TraerNumeroMes(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@MesCadena")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = UCase(Trim(MesLetras))
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '4
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Anio")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(A�o)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '5
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gDesEmpresa
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '6
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = gEmprRuc
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '7
            crParameterFieldDefinitions = rptGastosAdmOcacionalesDet.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@TC")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = dTipoCambio
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '-------------------------------------------------------------------------------------------
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptGastosAdmOcacionalesDet

        ElseIf Trim(Categoria) = "4" And Res_O_Det = "R" Then
            Try
                Dim dtTable2 As DataTable
                Dim pIdRetencion As String = ""
                Dim pSerie As String = ""
                dtTable2 = New DataTable
                eGastosGenerales = New clsGastosGenerales
                If Trim(Moneda) = "01" Then
                    dtTable2 = eGastosGenerales.fListarGastosDeObra(0, gEmpresa, Trim(TraerNumeroMes(MesLetras)), UCase(Trim(MesLetras)), Trim(A�o), gDesEmpresa, gEmpresa, 0)
                End If
                If Trim(Moneda) = "02" Then
                    dtTable2 = eGastosGenerales.fListarGastosDeObra(1, gEmpresa, Trim(TraerNumeroMes(MesLetras)), UCase(Trim(MesLetras)), Trim(A�o), gDesEmpresa, gEmpresa, 0)
                End If
                If dtTable2.Rows.Count > 0 Then
                    Dim NumeroMes As String = ""
                    NumeroMes = Trim(TraerNumeroMes(A�o))
                    Muestra_Reporte(RutaAppReportes & "rptGastosPorObraCruzadas3.rpt", dtTable2, "TblLibroDiario", "", "@EmprCodigo;" & gEmpresa, "@Mes;" & NumeroMes, "@MesCadena;" & UCase(Trim(MesLetras)), "@Anio;" & Trim(A�o), "@Empresa;" & gDesEmpresa, "@RUC;" & gEmprRuc, "@TC;" & dTipoCambio, "@MonedaDes;" & Trim(MonedaDes))
                    'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                ElseIf dtTable2.Rows.Count = 0 Then
                    Dim mensaje As String = ""
                    MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Catch ex As Exception
                MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try



        ElseIf Trim(Categoria) = "4" And Res_O_Det = "D" Then
            Try
                Dim dtTable2 As DataTable
                Dim pIdRetencion As String = ""
                Dim pSerie As String = ""
                dtTable2 = New DataTable
                eGastosGenerales = New clsGastosGenerales
                'If cboMoneda.SelectedValue = "01" Then
                dtTable2 = eGastosGenerales.fListarGastosDeObra(2, gEmpresa, Trim(TraerNumeroMes(MesLetras)), UCase(Trim(MesLetras)), Trim(A�o), gDesEmpresa, gEmpresa, 0)
                If dtTable2.Rows.Count > 0 Then
                    Dim NumeroMes As String = ""
                    NumeroMes = Trim(TraerNumeroMes(MesLetras))
                    Muestra_Reporte(RutaAppReportes & "rptGastosPorObraDetallado.rpt", dtTable2, "TblLibroDiario", "", "@EmprCodigo;" & gEmpresa, "@Mes;" & NumeroMes, "@MesCadena;" & UCase(Trim(MesLetras)), "@Anio;" & Trim(A�o), "@Empresa;" & gDesEmpresa, "@RUC;" & gEmprRuc, "@TC;" & dTipoCambio, "@MonedaDes;" & Trim(MonedaDes))
                    'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                ElseIf dtTable2.Rows.Count = 0 Then
                    Dim mensaje As String = ""
                    MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Catch ex As Exception
                MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End Try
            Me.Cursor = Cursors.Default
        End If

    End Sub



    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            'Dim f As New frmReporteConsolidado2
            'Dim myReporte As New ReportDocument

            ''Cargo el reporte segun ruta
            'myReporte.Load(STRnombreReporte)

            ''Leo los parametros
            'If Parametros.Length > 0 Then
            '    f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            'End If

            'f.CRVisor.SelectionFormula = STRfiltro
            'myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument
            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)
            'Leo los parametros
            If Parametros.Length > 0 Then
                CrystalReportViewer1.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            CrystalReportViewer1.SelectionFormula = STRfiltro
            myReporte.SetDataSource(myDatos)
            CrystalReportViewer1.ReportSource = myReporte
            'f.CRVisor.DisplayGroupTree = False
            'f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields
        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class