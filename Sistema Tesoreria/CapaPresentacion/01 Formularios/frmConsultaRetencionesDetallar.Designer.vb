<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaRetencionesDetallar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConsultaRetencionesDetallar))
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.cboOtrasEmpresas = New ctrLibreria.Controles.BeComboBox
        Me.chkDocTodas = New System.Windows.Forms.CheckBox
        Me.BeLabel42 = New ctrLibreria.Controles.BeLabel
        Me.txtRazonProv = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel38 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel39 = New ctrLibreria.Controles.BeLabel
        Me.txtRucProv = New ctrLibreria.Controles.BeTextBox
        Me.txtDireccionProv = New ctrLibreria.Controles.BeTextBox
        Me.GroupBox11 = New System.Windows.Forms.GroupBox
        Me.BeLabel47 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel46 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel45 = New ctrLibreria.Controles.BeLabel
        Me.txtEstadoCre = New ctrLibreria.Controles.BeTextBox
        Me.txtDiasCred = New ctrLibreria.Controles.BeTextBox
        Me.txtLineaCre = New ctrLibreria.Controles.BeTextBox
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.BeLabel37 = New ctrLibreria.Controles.BeLabel
        Me.txtTotPagarProv = New ctrLibreria.Controles.BeTextBox
        Me.txtSemanaPago = New ctrLibreria.Controles.BeTextBox
        Me.txtNroDoc = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel40 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel41 = New ctrLibreria.Controles.BeLabel
        Me.dgvDocumentos = New System.Windows.Forms.DataGridView
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Doc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NroDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaV = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Mo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ImporteTotal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Saldo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.aPagar = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DP = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Porcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IPor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdCC = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.X = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.IdRegistro = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AbrDoc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MonDes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TipoMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SaldoOculto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CodigoOC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdBD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CosCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.PPago = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.txtNroOrden = New ctrLibreria.Controles.BeTextBox
        Me.chkVerTodo = New System.Windows.Forms.CheckBox
        Me.BeLabel43 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel44 = New ctrLibreria.Controles.BeLabel
        Me.dgvOrdenes = New System.Windows.Forms.DataGridView
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CCosto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Moneda = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdOrden = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PrvCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PrvRUC = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeLabel36 = New ctrLibreria.Controles.BeLabel
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.Panel9 = New System.Windows.Forms.Panel
        Me.BeLabel48 = New ctrLibreria.Controles.BeLabel
        Me.txtTipoCambio = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.lblTotDocs = New ctrLibreria.Controles.BeLabel
        Me.txtIdLinea = New ctrLibreria.Controles.BeTextBox
        Me.txtIdProv = New ctrLibreria.Controles.BeTextBox
        Me.AxMonthView1 = New AxMSComCtl2.AxMonthView
        Me.btnGrabar = New System.Windows.Forms.Button
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.dgvOrdenes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.cboOtrasEmpresas)
        Me.GroupBox9.Controls.Add(Me.chkDocTodas)
        Me.GroupBox9.Controls.Add(Me.BeLabel42)
        Me.GroupBox9.Controls.Add(Me.txtRazonProv)
        Me.GroupBox9.Controls.Add(Me.BeLabel38)
        Me.GroupBox9.Controls.Add(Me.BeLabel39)
        Me.GroupBox9.Controls.Add(Me.txtRucProv)
        Me.GroupBox9.Controls.Add(Me.txtDireccionProv)
        Me.GroupBox9.Location = New System.Drawing.Point(12, 38)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(590, 108)
        Me.GroupBox9.TabIndex = 1
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Proveedor"
        '
        'cboOtrasEmpresas
        '
        Me.cboOtrasEmpresas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboOtrasEmpresas.BackColor = System.Drawing.Color.Ivory
        Me.cboOtrasEmpresas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtrasEmpresas.ForeColor = System.Drawing.Color.Black
        Me.cboOtrasEmpresas.FormattingEnabled = True
        Me.cboOtrasEmpresas.KeyEnter = True
        Me.cboOtrasEmpresas.Location = New System.Drawing.Point(367, 20)
        Me.cboOtrasEmpresas.Name = "cboOtrasEmpresas"
        Me.cboOtrasEmpresas.Size = New System.Drawing.Size(217, 21)
        Me.cboOtrasEmpresas.TabIndex = 26
        '
        'chkDocTodas
        '
        Me.chkDocTodas.AutoSize = True
        Me.chkDocTodas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDocTodas.Location = New System.Drawing.Point(247, 22)
        Me.chkDocTodas.Name = "chkDocTodas"
        Me.chkDocTodas.Size = New System.Drawing.Size(114, 17)
        Me.chkDocTodas.TabIndex = 2
        Me.chkDocTodas.Text = "Otras Empresas"
        Me.chkDocTodas.UseVisualStyleBackColor = True
        '
        'BeLabel42
        '
        Me.BeLabel42.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel42.AutoSize = True
        Me.BeLabel42.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel42.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel42.ForeColor = System.Drawing.Color.Black
        Me.BeLabel42.Location = New System.Drawing.Point(6, 74)
        Me.BeLabel42.Name = "BeLabel42"
        Me.BeLabel42.Size = New System.Drawing.Size(68, 13)
        Me.BeLabel42.TabIndex = 5
        Me.BeLabel42.Text = "Dirección"
        '
        'txtRazonProv
        '
        Me.txtRazonProv.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRazonProv.BackColor = System.Drawing.Color.Ivory
        Me.txtRazonProv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRazonProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonProv.Enabled = False
        Me.txtRazonProv.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRazonProv.ForeColor = System.Drawing.Color.Black
        Me.txtRazonProv.KeyEnter = True
        Me.txtRazonProv.Location = New System.Drawing.Point(101, 44)
        Me.txtRazonProv.Name = "txtRazonProv"
        Me.txtRazonProv.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRazonProv.ShortcutsEnabled = False
        Me.txtRazonProv.Size = New System.Drawing.Size(483, 21)
        Me.txtRazonProv.TabIndex = 4
        Me.txtRazonProv.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel38
        '
        Me.BeLabel38.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel38.AutoSize = True
        Me.BeLabel38.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel38.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel38.ForeColor = System.Drawing.Color.Black
        Me.BeLabel38.Location = New System.Drawing.Point(6, 24)
        Me.BeLabel38.Name = "BeLabel38"
        Me.BeLabel38.Size = New System.Drawing.Size(32, 13)
        Me.BeLabel38.TabIndex = 0
        Me.BeLabel38.Text = "RUC"
        '
        'BeLabel39
        '
        Me.BeLabel39.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel39.AutoSize = True
        Me.BeLabel39.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel39.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel39.ForeColor = System.Drawing.Color.Black
        Me.BeLabel39.Location = New System.Drawing.Point(6, 51)
        Me.BeLabel39.Name = "BeLabel39"
        Me.BeLabel39.Size = New System.Drawing.Size(89, 13)
        Me.BeLabel39.TabIndex = 3
        Me.BeLabel39.Text = "Razón Social"
        '
        'txtRucProv
        '
        Me.txtRucProv.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRucProv.BackColor = System.Drawing.Color.Ivory
        Me.txtRucProv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRucProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRucProv.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRucProv.ForeColor = System.Drawing.Color.Black
        Me.txtRucProv.KeyEnter = True
        Me.txtRucProv.Location = New System.Drawing.Point(101, 20)
        Me.txtRucProv.MaxLength = 11
        Me.txtRucProv.Name = "txtRucProv"
        Me.txtRucProv.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRucProv.ShortcutsEnabled = False
        Me.txtRucProv.Size = New System.Drawing.Size(120, 21)
        Me.txtRucProv.TabIndex = 1
        Me.txtRucProv.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'txtDireccionProv
        '
        Me.txtDireccionProv.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDireccionProv.BackColor = System.Drawing.Color.Ivory
        Me.txtDireccionProv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDireccionProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccionProv.Enabled = False
        Me.txtDireccionProv.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDireccionProv.ForeColor = System.Drawing.Color.Black
        Me.txtDireccionProv.KeyEnter = True
        Me.txtDireccionProv.Location = New System.Drawing.Point(101, 68)
        Me.txtDireccionProv.Name = "txtDireccionProv"
        Me.txtDireccionProv.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDireccionProv.ShortcutsEnabled = False
        Me.txtDireccionProv.Size = New System.Drawing.Size(383, 21)
        Me.txtDireccionProv.TabIndex = 6
        Me.txtDireccionProv.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.BeLabel47)
        Me.GroupBox11.Controls.Add(Me.BeLabel46)
        Me.GroupBox11.Controls.Add(Me.BeLabel45)
        Me.GroupBox11.Controls.Add(Me.txtEstadoCre)
        Me.GroupBox11.Controls.Add(Me.txtDiasCred)
        Me.GroupBox11.Controls.Add(Me.txtLineaCre)
        Me.GroupBox11.Location = New System.Drawing.Point(608, 38)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(160, 108)
        Me.GroupBox11.TabIndex = 3
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Linea de Crédito"
        '
        'BeLabel47
        '
        Me.BeLabel47.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel47.AutoSize = True
        Me.BeLabel47.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel47.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel47.ForeColor = System.Drawing.Color.Black
        Me.BeLabel47.Location = New System.Drawing.Point(6, 29)
        Me.BeLabel47.Name = "BeLabel47"
        Me.BeLabel47.Size = New System.Drawing.Size(54, 13)
        Me.BeLabel47.TabIndex = 0
        Me.BeLabel47.Text = "Crédito"
        '
        'BeLabel46
        '
        Me.BeLabel46.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel46.AutoSize = True
        Me.BeLabel46.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel46.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel46.ForeColor = System.Drawing.Color.Black
        Me.BeLabel46.Location = New System.Drawing.Point(6, 53)
        Me.BeLabel46.Name = "BeLabel46"
        Me.BeLabel46.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel46.TabIndex = 2
        Me.BeLabel46.Text = "Dias"
        '
        'BeLabel45
        '
        Me.BeLabel45.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel45.AutoSize = True
        Me.BeLabel45.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel45.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel45.ForeColor = System.Drawing.Color.Black
        Me.BeLabel45.Location = New System.Drawing.Point(6, 74)
        Me.BeLabel45.Name = "BeLabel45"
        Me.BeLabel45.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel45.TabIndex = 4
        Me.BeLabel45.Text = "Estado"
        '
        'txtEstadoCre
        '
        Me.txtEstadoCre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtEstadoCre.BackColor = System.Drawing.Color.White
        Me.txtEstadoCre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEstadoCre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEstadoCre.ForeColor = System.Drawing.Color.Black
        Me.txtEstadoCre.KeyEnter = True
        Me.txtEstadoCre.Location = New System.Drawing.Point(66, 69)
        Me.txtEstadoCre.Name = "txtEstadoCre"
        Me.txtEstadoCre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtEstadoCre.ReadOnly = True
        Me.txtEstadoCre.ShortcutsEnabled = False
        Me.txtEstadoCre.Size = New System.Drawing.Size(87, 20)
        Me.txtEstadoCre.TabIndex = 5
        Me.txtEstadoCre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEstadoCre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtDiasCred
        '
        Me.txtDiasCred.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDiasCred.BackColor = System.Drawing.Color.Lavender
        Me.txtDiasCred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDiasCred.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiasCred.ForeColor = System.Drawing.Color.Black
        Me.txtDiasCred.KeyEnter = True
        Me.txtDiasCred.Location = New System.Drawing.Point(66, 46)
        Me.txtDiasCred.Name = "txtDiasCred"
        Me.txtDiasCred.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDiasCred.ShortcutsEnabled = False
        Me.txtDiasCred.Size = New System.Drawing.Size(87, 20)
        Me.txtDiasCred.TabIndex = 3
        Me.txtDiasCred.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtDiasCred.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'txtLineaCre
        '
        Me.txtLineaCre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtLineaCre.BackColor = System.Drawing.Color.Lavender
        Me.txtLineaCre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLineaCre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLineaCre.ForeColor = System.Drawing.Color.Black
        Me.txtLineaCre.KeyEnter = True
        Me.txtLineaCre.Location = New System.Drawing.Point(66, 23)
        Me.txtLineaCre.Name = "txtLineaCre"
        Me.txtLineaCre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtLineaCre.ShortcutsEnabled = False
        Me.txtLineaCre.Size = New System.Drawing.Size(87, 20)
        Me.txtLineaCre.TabIndex = 1
        Me.txtLineaCre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLineaCre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.BeLabel37)
        Me.GroupBox10.Controls.Add(Me.txtTotPagarProv)
        Me.GroupBox10.Controls.Add(Me.txtSemanaPago)
        Me.GroupBox10.Controls.Add(Me.txtNroDoc)
        Me.GroupBox10.Controls.Add(Me.BeLabel40)
        Me.GroupBox10.Controls.Add(Me.BeLabel41)
        Me.GroupBox10.Location = New System.Drawing.Point(774, 38)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(191, 108)
        Me.GroupBox10.TabIndex = 4
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Resumen Pago"
        '
        'BeLabel37
        '
        Me.BeLabel37.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel37.AutoSize = True
        Me.BeLabel37.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel37.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel37.ForeColor = System.Drawing.Color.Black
        Me.BeLabel37.Location = New System.Drawing.Point(6, 29)
        Me.BeLabel37.Name = "BeLabel37"
        Me.BeLabel37.Size = New System.Drawing.Size(59, 13)
        Me.BeLabel37.TabIndex = 30
        Me.BeLabel37.Text = "Semana"
        '
        'txtTotPagarProv
        '
        Me.txtTotPagarProv.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotPagarProv.BackColor = System.Drawing.Color.Ivory
        Me.txtTotPagarProv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotPagarProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotPagarProv.Enabled = False
        Me.txtTotPagarProv.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotPagarProv.ForeColor = System.Drawing.Color.Black
        Me.txtTotPagarProv.KeyEnter = True
        Me.txtTotPagarProv.Location = New System.Drawing.Point(87, 69)
        Me.txtTotPagarProv.Name = "txtTotPagarProv"
        Me.txtTotPagarProv.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotPagarProv.ShortcutsEnabled = False
        Me.txtTotPagarProv.Size = New System.Drawing.Size(94, 21)
        Me.txtTotPagarProv.TabIndex = 5
        Me.txtTotPagarProv.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotPagarProv.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSemanaPago
        '
        Me.txtSemanaPago.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSemanaPago.BackColor = System.Drawing.Color.Ivory
        Me.txtSemanaPago.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSemanaPago.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSemanaPago.Enabled = False
        Me.txtSemanaPago.ForeColor = System.Drawing.Color.Black
        Me.txtSemanaPago.KeyEnter = True
        Me.txtSemanaPago.Location = New System.Drawing.Point(87, 22)
        Me.txtSemanaPago.Name = "txtSemanaPago"
        Me.txtSemanaPago.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSemanaPago.ShortcutsEnabled = False
        Me.txtSemanaPago.Size = New System.Drawing.Size(94, 20)
        Me.txtSemanaPago.TabIndex = 31
        Me.txtSemanaPago.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSemanaPago.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtNroDoc
        '
        Me.txtNroDoc.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroDoc.BackColor = System.Drawing.Color.Ivory
        Me.txtNroDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroDoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroDoc.Enabled = False
        Me.txtNroDoc.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroDoc.ForeColor = System.Drawing.Color.Black
        Me.txtNroDoc.KeyEnter = True
        Me.txtNroDoc.Location = New System.Drawing.Point(87, 45)
        Me.txtNroDoc.Name = "txtNroDoc"
        Me.txtNroDoc.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroDoc.ShortcutsEnabled = False
        Me.txtNroDoc.Size = New System.Drawing.Size(94, 21)
        Me.txtNroDoc.TabIndex = 3
        Me.txtNroDoc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroDoc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel40
        '
        Me.BeLabel40.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel40.AutoSize = True
        Me.BeLabel40.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel40.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel40.ForeColor = System.Drawing.Color.Black
        Me.BeLabel40.Location = New System.Drawing.Point(6, 78)
        Me.BeLabel40.Name = "BeLabel40"
        Me.BeLabel40.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel40.TabIndex = 4
        Me.BeLabel40.Text = "Total Pagar"
        '
        'BeLabel41
        '
        Me.BeLabel41.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel41.AutoSize = True
        Me.BeLabel41.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel41.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel41.ForeColor = System.Drawing.Color.Black
        Me.BeLabel41.Location = New System.Drawing.Point(6, 54)
        Me.BeLabel41.Name = "BeLabel41"
        Me.BeLabel41.Size = New System.Drawing.Size(72, 13)
        Me.BeLabel41.TabIndex = 2
        Me.BeLabel41.Text = "Cant. Doc."
        '
        'dgvDocumentos
        '
        Me.dgvDocumentos.AllowUserToAddRows = False
        Me.dgvDocumentos.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDocumentos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDocumentos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column4, Me.Doc, Me.NroDocumento, Me.Fecha, Me.FechaV, Me.Mo, Me.ImporteTotal, Me.Saldo, Me.aPagar, Me.DP, Me.Porcentaje, Me.IPor, Me.OC, Me.IdCC, Me.X, Me.IdRegistro, Me.AbrDoc, Me.MonDes, Me.TipoMoneda, Me.SaldoOculto, Me.CodigoOC, Me.IdBD, Me.CosCodigo, Me.Column1, Me.Column2, Me.Column5, Me.Column6, Me.PPago})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDocumentos.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvDocumentos.EnableHeadersVisualStyles = False
        Me.dgvDocumentos.Location = New System.Drawing.Point(21, 155)
        Me.dgvDocumentos.Name = "dgvDocumentos"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDocumentos.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvDocumentos.RowHeadersVisible = False
        Me.dgvDocumentos.Size = New System.Drawing.Size(939, 189)
        Me.dgvDocumentos.TabIndex = 5
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "EmprAbreviado"
        Me.Column4.HeaderText = "Empresa"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 80
        '
        'Doc
        '
        Me.Doc.DataPropertyName = "AbrDoc"
        Me.Doc.HeaderText = "TD"
        Me.Doc.Name = "Doc"
        Me.Doc.ReadOnly = True
        Me.Doc.Width = 30
        '
        'NroDocumento
        '
        Me.NroDocumento.DataPropertyName = "NroDocumento"
        Me.NroDocumento.HeaderText = "Nº Doc."
        Me.NroDocumento.Name = "NroDocumento"
        Me.NroDocumento.ReadOnly = True
        Me.NroDocumento.Width = 80
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "FechaDocumento"
        Me.Fecha.HeaderText = "F. Doc."
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 70
        '
        'FechaV
        '
        Me.FechaV.DataPropertyName = "FechaVencimiento"
        Me.FechaV.HeaderText = "F. Ven."
        Me.FechaV.Name = "FechaV"
        Me.FechaV.ReadOnly = True
        Me.FechaV.Width = 70
        '
        'Mo
        '
        Me.Mo.DataPropertyName = "MonSimbolo"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Mo.DefaultCellStyle = DataGridViewCellStyle2
        Me.Mo.HeaderText = "M"
        Me.Mo.Name = "Mo"
        Me.Mo.ReadOnly = True
        Me.Mo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Mo.Width = 25
        '
        'ImporteTotal
        '
        Me.ImporteTotal.DataPropertyName = "ImporteTotal"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.ImporteTotal.DefaultCellStyle = DataGridViewCellStyle3
        Me.ImporteTotal.HeaderText = "Importe"
        Me.ImporteTotal.Name = "ImporteTotal"
        Me.ImporteTotal.ReadOnly = True
        Me.ImporteTotal.Width = 70
        '
        'Saldo
        '
        Me.Saldo.DataPropertyName = "Saldo"
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = "0.00"
        Me.Saldo.DefaultCellStyle = DataGridViewCellStyle4
        Me.Saldo.HeaderText = "Saldo"
        Me.Saldo.Name = "Saldo"
        Me.Saldo.ReadOnly = True
        Me.Saldo.Width = 70
        '
        'aPagar
        '
        Me.aPagar.DataPropertyName = "sPagar"
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Salmon
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.aPagar.DefaultCellStyle = DataGridViewCellStyle5
        Me.aPagar.HeaderText = "A Pagar"
        Me.aPagar.Name = "aPagar"
        Me.aPagar.Width = 70
        '
        'DP
        '
        Me.DP.DataPropertyName = "sDP"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DP.DefaultCellStyle = DataGridViewCellStyle6
        Me.DP.HeaderText = "D/P"
        Me.DP.Name = "DP"
        Me.DP.Width = 30
        '
        'Porcentaje
        '
        Me.Porcentaje.DataPropertyName = "MtoDP"
        Me.Porcentaje.HeaderText = "%"
        Me.Porcentaje.Name = "Porcentaje"
        Me.Porcentaje.Width = 35
        '
        'IPor
        '
        Me.IPor.DataPropertyName = "sPorc"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.IPor.DefaultCellStyle = DataGridViewCellStyle7
        Me.IPor.HeaderText = "Im D-P"
        Me.IPor.Name = "IPor"
        Me.IPor.Width = 65
        '
        'OC
        '
        Me.OC.DataPropertyName = "NroOC"
        Me.OC.HeaderText = "O/C"
        Me.OC.Name = "OC"
        Me.OC.Width = 75
        '
        'IdCC
        '
        Me.IdCC.HeaderText = "C. Costo"
        Me.IdCC.Name = "IdCC"
        Me.IdCC.Width = 112
        '
        'X
        '
        Me.X.HeaderText = "X"
        Me.X.Name = "X"
        Me.X.Width = 20
        '
        'IdRegistro
        '
        Me.IdRegistro.DataPropertyName = "IdRegistro"
        Me.IdRegistro.HeaderText = "IdRegistro"
        Me.IdRegistro.Name = "IdRegistro"
        Me.IdRegistro.Visible = False
        '
        'AbrDoc
        '
        Me.AbrDoc.DataPropertyName = "AbrDoc"
        Me.AbrDoc.HeaderText = "AbrDoc"
        Me.AbrDoc.Name = "AbrDoc"
        Me.AbrDoc.Visible = False
        '
        'MonDes
        '
        Me.MonDes.DataPropertyName = "MonDescripcion"
        Me.MonDes.HeaderText = "MonDes"
        Me.MonDes.Name = "MonDes"
        Me.MonDes.Visible = False
        '
        'TipoMoneda
        '
        Me.TipoMoneda.DataPropertyName = "IdMoneda"
        Me.TipoMoneda.HeaderText = "IdMoneda"
        Me.TipoMoneda.Name = "TipoMoneda"
        Me.TipoMoneda.Visible = False
        '
        'SaldoOculto
        '
        Me.SaldoOculto.DataPropertyName = "SaldoOculto"
        Me.SaldoOculto.HeaderText = "SaldoOculto"
        Me.SaldoOculto.Name = "SaldoOculto"
        Me.SaldoOculto.Visible = False
        '
        'CodigoOC
        '
        Me.CodigoOC.DataPropertyName = "IdOC"
        Me.CodigoOC.HeaderText = "IdOC"
        Me.CodigoOC.Name = "CodigoOC"
        Me.CodigoOC.Visible = False
        '
        'IdBD
        '
        Me.IdBD.HeaderText = "IdBD"
        Me.IdBD.Name = "IdBD"
        Me.IdBD.Visible = False
        '
        'CosCodigo
        '
        Me.CosCodigo.DataPropertyName = "CCosCodigo"
        Me.CosCodigo.HeaderText = "CCosCodigo"
        Me.CosCodigo.Name = "CosCodigo"
        Me.CosCodigo.Visible = False
        Me.CosCodigo.Width = 96
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "Archivado"
        Me.Column1.HeaderText = "Archivado"
        Me.Column1.Name = "Column1"
        Me.Column1.Visible = False
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "EmprCodigo"
        Me.Column2.HeaderText = "Empresa"
        Me.Column2.Name = "Column2"
        Me.Column2.Visible = False
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "CCCadena"
        Me.Column5.HeaderText = "CCCadena"
        Me.Column5.Name = "Column5"
        Me.Column5.Visible = False
        '
        'Column6
        '
        Me.Column6.HeaderText = "PPago"
        Me.Column6.Name = "Column6"
        Me.Column6.Visible = False
        Me.Column6.Width = 35
        '
        'PPago
        '
        Me.PPago.DataPropertyName = "PPago"
        Me.PPago.HeaderText = "PPago"
        Me.PPago.Name = "PPago"
        Me.PPago.Visible = False
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.SlateGray
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.txtNroOrden)
        Me.Panel7.Controls.Add(Me.chkVerTodo)
        Me.Panel7.Controls.Add(Me.BeLabel43)
        Me.Panel7.Controls.Add(Me.BeLabel44)
        Me.Panel7.Controls.Add(Me.dgvOrdenes)
        Me.Panel7.Location = New System.Drawing.Point(272, 179)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(433, 156)
        Me.Panel7.TabIndex = 6
        Me.Panel7.Visible = False
        '
        'txtNroOrden
        '
        Me.txtNroOrden.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroOrden.BackColor = System.Drawing.Color.LightYellow
        Me.txtNroOrden.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroOrden.ForeColor = System.Drawing.Color.Black
        Me.txtNroOrden.KeyEnter = True
        Me.txtNroOrden.Location = New System.Drawing.Point(95, 12)
        Me.txtNroOrden.MaxLength = 9
        Me.txtNroOrden.Name = "txtNroOrden"
        Me.txtNroOrden.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroOrden.ShortcutsEnabled = False
        Me.txtNroOrden.Size = New System.Drawing.Size(205, 20)
        Me.txtNroOrden.TabIndex = 2
        Me.txtNroOrden.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'chkVerTodo
        '
        Me.chkVerTodo.AutoSize = True
        Me.chkVerTodo.Checked = True
        Me.chkVerTodo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkVerTodo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVerTodo.Location = New System.Drawing.Point(306, 14)
        Me.chkVerTodo.Name = "chkVerTodo"
        Me.chkVerTodo.Size = New System.Drawing.Size(84, 17)
        Me.chkVerTodo.TabIndex = 3
        Me.chkVerTodo.Text = "Ver Todos"
        Me.chkVerTodo.UseVisualStyleBackColor = True
        Me.chkVerTodo.Visible = False
        '
        'BeLabel43
        '
        Me.BeLabel43.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel43.AutoSize = True
        Me.BeLabel43.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel43.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel43.ForeColor = System.Drawing.Color.Black
        Me.BeLabel43.Location = New System.Drawing.Point(3, 14)
        Me.BeLabel43.Name = "BeLabel43"
        Me.BeLabel43.Size = New System.Drawing.Size(86, 13)
        Me.BeLabel43.TabIndex = 0
        Me.BeLabel43.Text = "Nº de Orden"
        '
        'BeLabel44
        '
        Me.BeLabel44.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel44.AutoSize = True
        Me.BeLabel44.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel44.ForeColor = System.Drawing.Color.Red
        Me.BeLabel44.Location = New System.Drawing.Point(413, 2)
        Me.BeLabel44.Name = "BeLabel44"
        Me.BeLabel44.Size = New System.Drawing.Size(16, 15)
        Me.BeLabel44.TabIndex = 4
        Me.BeLabel44.Text = "X"
        '
        'dgvOrdenes
        '
        Me.dgvOrdenes.AllowUserToAddRows = False
        Me.dgvOrdenes.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOrdenes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvOrdenes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOrdenes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Numero, Me.CCosto, Me.Moneda, Me.Importe, Me.IdOrden, Me.PrvCodigo, Me.PrvRUC, Me.DataGridViewTextBoxColumn22})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvOrdenes.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgvOrdenes.Location = New System.Drawing.Point(2, 37)
        Me.dgvOrdenes.Name = "dgvOrdenes"
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvOrdenes.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvOrdenes.RowHeadersVisible = False
        Me.dgvOrdenes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOrdenes.Size = New System.Drawing.Size(427, 114)
        Me.dgvOrdenes.TabIndex = 1
        '
        'Numero
        '
        Me.Numero.DataPropertyName = "Numero"
        Me.Numero.HeaderText = "Número"
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Width = 75
        '
        'CCosto
        '
        Me.CCosto.DataPropertyName = "CCosto"
        Me.CCosto.HeaderText = "Proveedor"
        Me.CCosto.Name = "CCosto"
        Me.CCosto.ReadOnly = True
        Me.CCosto.Width = 130
        '
        'Moneda
        '
        Me.Moneda.DataPropertyName = "Moneda"
        Me.Moneda.HeaderText = "Moneda"
        Me.Moneda.Name = "Moneda"
        Me.Moneda.ReadOnly = True
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Total"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle11
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        '
        'IdOrden
        '
        Me.IdOrden.DataPropertyName = "IdOrden"
        Me.IdOrden.HeaderText = "IdOrden"
        Me.IdOrden.Name = "IdOrden"
        Me.IdOrden.ReadOnly = True
        Me.IdOrden.Visible = False
        '
        'PrvCodigo
        '
        Me.PrvCodigo.DataPropertyName = "PrvCodigo"
        Me.PrvCodigo.HeaderText = "PrvCodigo"
        Me.PrvCodigo.Name = "PrvCodigo"
        Me.PrvCodigo.ReadOnly = True
        Me.PrvCodigo.Visible = False
        '
        'PrvRUC
        '
        Me.PrvRUC.DataPropertyName = "PrvRUC"
        Me.PrvRUC.HeaderText = "PrvRUC"
        Me.PrvRUC.Name = "PrvRUC"
        Me.PrvRUC.ReadOnly = True
        Me.PrvRUC.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.DataPropertyName = "CCosCodigo"
        Me.DataGridViewTextBoxColumn22.HeaderText = "CCosCodigo"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.ReadOnly = True
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'BeLabel36
        '
        Me.BeLabel36.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel36.AutoSize = True
        Me.BeLabel36.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel36.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel36.ForeColor = System.Drawing.Color.Black
        Me.BeLabel36.Location = New System.Drawing.Point(804, 350)
        Me.BeLabel36.Name = "BeLabel36"
        Me.BeLabel36.Size = New System.Drawing.Size(119, 13)
        Me.BeLabel36.TabIndex = 20
        Me.BeLabel36.Text = "Doc. Sin Archivar"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Aqua
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Location = New System.Drawing.Point(927, 350)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(33, 14)
        Me.Panel8.TabIndex = 21
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.Violet
        Me.Panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel9.Location = New System.Drawing.Point(767, 350)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(33, 14)
        Me.Panel9.TabIndex = 19
        '
        'BeLabel48
        '
        Me.BeLabel48.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel48.AutoSize = True
        Me.BeLabel48.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel48.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel48.ForeColor = System.Drawing.Color.Black
        Me.BeLabel48.Location = New System.Drawing.Point(657, 350)
        Me.BeLabel48.Name = "BeLabel48"
        Me.BeLabel48.Size = New System.Drawing.Size(105, 13)
        Me.BeLabel48.TabIndex = 18
        Me.BeLabel48.Text = "Doc. Archivado"
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoCambio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTipoCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.txtTipoCambio.KeyEnter = True
        Me.txtTipoCambio.Location = New System.Drawing.Point(861, 7)
        Me.txtTipoCambio.MaxLength = 20
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTipoCambio.ShortcutsEnabled = False
        Me.txtTipoCambio.Size = New System.Drawing.Size(94, 20)
        Me.txtTipoCambio.TabIndex = 26
        Me.txtTipoCambio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(771, 9)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(66, 13)
        Me.BeLabel14.TabIndex = 25
        Me.BeLabel14.Text = "Tipo Cambio"
        '
        'lblTotDocs
        '
        Me.lblTotDocs.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblTotDocs.AutoSize = True
        Me.lblTotDocs.BackColor = System.Drawing.Color.Transparent
        Me.lblTotDocs.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotDocs.ForeColor = System.Drawing.Color.Black
        Me.lblTotDocs.Location = New System.Drawing.Point(18, 350)
        Me.lblTotDocs.Name = "lblTotDocs"
        Me.lblTotDocs.Size = New System.Drawing.Size(11, 13)
        Me.lblTotDocs.TabIndex = 28
        Me.lblTotDocs.Text = "."
        '
        'txtIdLinea
        '
        Me.txtIdLinea.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIdLinea.BackColor = System.Drawing.Color.Lavender
        Me.txtIdLinea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdLinea.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdLinea.ForeColor = System.Drawing.Color.Black
        Me.txtIdLinea.KeyEnter = True
        Me.txtIdLinea.Location = New System.Drawing.Point(1060, 266)
        Me.txtIdLinea.Name = "txtIdLinea"
        Me.txtIdLinea.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIdLinea.ShortcutsEnabled = False
        Me.txtIdLinea.Size = New System.Drawing.Size(10, 20)
        Me.txtIdLinea.TabIndex = 23
        Me.txtIdLinea.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIdLinea.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtIdLinea.Visible = False
        '
        'txtIdProv
        '
        Me.txtIdProv.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIdProv.BackColor = System.Drawing.Color.Ivory
        Me.txtIdProv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdProv.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIdProv.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIdProv.ForeColor = System.Drawing.Color.Black
        Me.txtIdProv.KeyEnter = True
        Me.txtIdProv.Location = New System.Drawing.Point(1044, 265)
        Me.txtIdProv.Name = "txtIdProv"
        Me.txtIdProv.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIdProv.ShortcutsEnabled = False
        Me.txtIdProv.Size = New System.Drawing.Size(10, 21)
        Me.txtIdProv.TabIndex = 22
        Me.txtIdProv.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtIdProv.Visible = False
        '
        'AxMonthView1
        '
        Me.AxMonthView1.Location = New System.Drawing.Point(1020, 81)
        Me.AxMonthView1.Name = "AxMonthView1"
        Me.AxMonthView1.OcxState = CType(resources.GetObject("AxMonthView1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxMonthView1.Size = New System.Drawing.Size(197, 158)
        Me.AxMonthView1.TabIndex = 29
        Me.AxMonthView1.Visible = False
        '
        'btnGrabar
        '
        Me.btnGrabar.Location = New System.Drawing.Point(851, 373)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(109, 40)
        Me.btnGrabar.TabIndex = 30
        Me.btnGrabar.Text = "Grabar "
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(18, 9)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(97, 16)
        Me.BeLabel1.TabIndex = 31
        Me.BeLabel1.Text = "Tipo Cambio"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(365, 11)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(97, 16)
        Me.BeLabel2.TabIndex = 32
        Me.BeLabel2.Text = "Tipo Cambio"
        '
        'frmConsultaRetencionesDetallar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(972, 422)
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.AxMonthView1)
        Me.Controls.Add(Me.lblTotDocs)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.BeLabel14)
        Me.Controls.Add(Me.txtIdLinea)
        Me.Controls.Add(Me.txtIdProv)
        Me.Controls.Add(Me.BeLabel36)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.Panel9)
        Me.Controls.Add(Me.BeLabel48)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.dgvDocumentos)
        Me.Controls.Add(Me.GroupBox11)
        Me.Controls.Add(Me.GroupBox10)
        Me.Controls.Add(Me.GroupBox9)
        Me.MaximizeBox = False
        Me.Name = "frmConsultaRetencionesDetallar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.dgvOrdenes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxMonthView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents cboOtrasEmpresas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkDocTodas As System.Windows.Forms.CheckBox
    Friend WithEvents BeLabel42 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtRazonProv As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel38 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel39 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtRucProv As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDireccionProv As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel47 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel46 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel45 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtEstadoCre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDiasCred As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtLineaCre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTotPagarProv As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNroDoc As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel40 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel41 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvDocumentos As System.Windows.Forms.DataGridView
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents txtNroOrden As ctrLibreria.Controles.BeTextBox
    Friend WithEvents chkVerTodo As System.Windows.Forms.CheckBox
    Friend WithEvents BeLabel43 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel44 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvOrdenes As System.Windows.Forms.DataGridView
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Moneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdOrden As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrvCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrvRUC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel36 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel48 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTipoCambio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblTotDocs As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtIdLinea As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtIdProv As ctrLibreria.Controles.BeTextBox
    Friend WithEvents AxMonthView1 As AxMSComCtl2.AxMonthView
    Friend WithEvents BeLabel37 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSemanaPago As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Doc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocumento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Saldo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents aPagar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Porcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IPor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCC As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents X As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IdRegistro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AbrDoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MonDes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoOculto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodigoOC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdBD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CosCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents PPago As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
