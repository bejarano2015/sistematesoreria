Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data
Imports System.Data.SqlClient

Public Class frmConsultaGastos2

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaGastos2 = Nothing
    Public Shared Function Instance() As frmConsultaGastos2
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaGastos2
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

    Private Sub frmConsultaGastos2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private eBusquedaGastos As clsBusquedaGastos
    Private eTempo As clsPlantTempo
    Dim GrabarDet As Int16 = 0
    Dim traer As String = ""
    Private da As SqlDataAdapter

    Private Sub txtTexto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTexto.TextChanged
        If Len(Trim(txtTexto.Text)) > 0 Then
            If dtpFechaFin.Value < dtpFechaIni.Value Then
                MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                dtpFechaIni.Focus()
                Exit Sub
            End If
            If cboCriterio.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCriterio.Focus()
                Exit Sub
            End If
            Dim dtBusqueda As DataTable
            eBusquedaGastos = New clsBusquedaGastos
            dtBusqueda = New DataTable
            Dim EstEnvio As Integer = 0
            If rdbEnviados.Checked = True Then
                EstEnvio = 1
            End If
            If rdbPorEnviar.Checked = True Then
                EstEnvio = 0
            End If

            If Len(txtTexto.Text) > 0 Then
                If cboCriterio.Text = "CONCEPTO" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(1, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(29, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(21, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                ElseIf cboCriterio.Text = "RESPONSABLE" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(30, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(22, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                ElseIf cboCriterio.Text = "CHEQUE" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(3, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(31, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(23, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                ElseIf cboCriterio.Text = "MACRO" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(4, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(32, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(24, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                ElseIf cboCriterio.Text = "CARTA" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(5, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(33, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(25, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                ElseIf cboCriterio.Text = "TRANSFERENCIA" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(6, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(34, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(26, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                ElseIf cboCriterio.Text = "OTROS" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(7, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(35, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(27, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                ElseIf cboCriterio.Text = "CUENTA CORRIENTE" Then
                    If rdbPorEnviar.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(46, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviados.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos2(47, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, EstEnvio, "", 0)
                    End If
                    If rdbEnviadosRendidos.Checked = True Then
                        dtBusqueda = eBusquedaGastos.fConsultaGastos3(48, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                    End If
                End If

                If rdbPorEnviar.Checked = True And traer = "SI" Then
                    dgvGastos.DataSource = dtBusqueda
                    Pintar()
                    Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
                    'If dgvGastos.Rows.Count > 0 Then
                    '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
                    '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                    '        dgvGastos.Rows(x).Cells("Column15").Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
                    '        dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    '        dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                    '    Next
                    'End If
                    Exit Sub
                End If

                If rdbEnviados.Checked = True And traer = "SI" Then
                    dgvGastos.DataSource = dtBusqueda
                    Pintar2()
                    Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
                    'If dgvGastos.Rows.Count > 0 Then
                    '    For x As Integer = 0 To dgvGastos.Rows.Count - 1

                    '        dgvGastos.Rows(x).Cells("Column14").Value = True
                    '        dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                    '        If dgvGastos.Rows(x).Cells("Column21").Value = "01" Then
                    '            dgvGastos.Rows(x).Cells("Column17").Value = False
                    '            dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                    '        ElseIf dgvGastos.Rows(x).Cells("Column21").Value = "02" Then
                    '            dgvGastos.Rows(x).Cells("Column17").Value = True
                    '            dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
                    '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
                    '        End If
                    '        dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
                    '    Next
                    'End If
                    Exit Sub
                End If

                If rdbEnviadosRendidos.Checked = True And traer = "SI" Then
                    dgvGastos.DataSource = dtBusqueda
                    Pintar3()
                    'Pintar2()
                    Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
                    'If dgvGastos.Rows.Count > 0 Then
                    '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
                    '        gestionaResaltados3(dgvGastos, x, Color.LimeGreen)
                    '        dgvGastos.Rows(x).Cells("Column14").Value = True
                    '        dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                    '        If dtBusqueda.Rows(x).Item("IdRendicion") = "01" Then
                    '            dgvGastos.Rows(x).Cells("Column17").Value = False
                    '            dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                    '        End If
                    '        If dtBusqueda.Rows(x).Item("IdRendicion") = "02" Then
                    '            dgvGastos.Rows(x).Cells("Column17").Value = True
                    '            dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
                    '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
                    '        End If
                    '        dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
                    '    Next
                    'End If
                    Exit Sub
                End If
            End If

        ElseIf Len(Trim(txtTexto.Text)) = 0 Then
            dtpFechaFin_ValueChanged(sender, e)
        End If

    End Sub

    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(1).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(2).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(3).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(4).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(5).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(6).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(7).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(8).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(9).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(10).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(11).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(12).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(13).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(14).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(15).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(16).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(17).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(18).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(19).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(20).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(21).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(22).Style.BackColor = Color.Silver
    End Sub

    Private Sub gestionaResaltados2(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells("Column6").Style.BackColor = Color.Aqua
    End Sub

    Private Sub gestionaResaltados3(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(8).Style.BackColor = c
        visor.Rows(fila).Cells(9).Style.BackColor = c
        visor.Rows(fila).Cells(10).Style.BackColor = c
        visor.Rows(fila).Cells(11).Style.BackColor = c
        visor.Rows(fila).Cells(12).Style.BackColor = c
        visor.Rows(fila).Cells(13).Style.BackColor = c
        visor.Rows(fila).Cells(14).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        visor.Rows(fila).Cells(16).Style.BackColor = c
        visor.Rows(fila).Cells(17).Style.BackColor = c
        visor.Rows(fila).Cells(18).Style.BackColor = c
        visor.Rows(fila).Cells(19).Style.BackColor = c
        visor.Rows(fila).Cells(20).Style.BackColor = c
        visor.Rows(fila).Cells(21).Style.BackColor = c
        visor.Rows(fila).Cells(22).Style.BackColor = c
    End Sub

    Private Sub frmConsultaGastos2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        traer = "NO"
        'rdbEnviados.Checked = False
        'rdbPorEnviar.Checked = False
        dtpFechaFin.Text = Now.Date()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        rdbPorEnviar.Checked = True
        traer = "SI"
        
        If rdbPorEnviar.Checked = True And traer = "SI" Then
            Dim dtBusqueda As DataTable
            eBusquedaGastos = New clsBusquedaGastos
            dtBusqueda = New DataTable
            dtBusqueda = eBusquedaGastos.fConsultaGastos2(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
            dgvGastos.DataSource = dtBusqueda
            'Pintar()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            'dtpFechaFin_ValueChanged(sender, e)
        End If
    End Sub

    Sub Pintar()
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                    gestionaResaltados(dgvGastos, x, Color.Orange)
                End If
                If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                    gestionaResaltados2(dgvGastos, x, Color.Orange)
                End If
                If Len(Trim(dgvGastos.Rows(x).Cells("Observacion").Value)) > 0 Then
                    gestionaResaltados3(dgvGastos, x, Color.LightCoral)
                End If
                dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                'dgvGastos.Rows(x).Cells("Column15").Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
                'dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"

                'dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                If dgvGastos.Rows(x).Cells("Column21").Value = "01" Then
                    dgvGastos.Rows(x).Cells("Column17").Value = False
                    dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                ElseIf dgvGastos.Rows(x).Cells("Column21").Value = "02" Then
                    dgvGastos.Rows(x).Cells("Column17").Value = True
                    dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
                    dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
                End If
            Next
        End If
    End Sub

    Sub Pintar2()
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                    gestionaResaltados(dgvGastos, x, Color.Orange)
                End If
                If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                    gestionaResaltados2(dgvGastos, x, Color.Orange)
                End If

                dgvGastos.Rows(x).Cells("Column14").Value = True
                dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
                dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                If dgvGastos.Rows(x).Cells("Column21").Value = "01" Then
                    dgvGastos.Rows(x).Cells("Column17").Value = False
                    dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                ElseIf dgvGastos.Rows(x).Cells("Column21").Value = "02" Then
                    dgvGastos.Rows(x).Cells("Column17").Value = True
                    dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
                    dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
                End If
                dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
                dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
                dgvGastos.Rows(x).Cells("Column15").ReadOnly = True

            Next

            'If dgvGastos.Rows.Count > 0 Then
            '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
            '        dgvGastos.Rows(x).Cells("Column14").Value = True
            '        dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
            '        If dgvGastos.Rows(x).Cells("Column21").Value = "01" Then
            '            dgvGastos.Rows(x).Cells("Column17").Value = False
            '            dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
            '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
            '        ElseIf dgvGastos.Rows(x).Cells("Column21").Value = "02" Then
            '            dgvGastos.Rows(x).Cells("Column17").Value = True
            '            dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
            '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
            '        End If
            '        dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
            '    Next
            'End If

        End If
    End Sub

    Sub Pintar3()
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                gestionaResaltados3(dgvGastos, x, Color.LightCoral)
                If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                    gestionaResaltados(dgvGastos, x, Color.Orange)
                End If
                If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                    gestionaResaltados2(dgvGastos, x, Color.Orange)
                End If

                'If dgvGastos.Rows.Count > 0 Then
                'For x As Integer = 0 To dgvGastos.Rows.Count - 1
                gestionaResaltados3(dgvGastos, x, Color.LimeGreen)
                dgvGastos.Rows(x).Cells("Column14").Value = True
                dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
                dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                If dgvGastos.Rows(x).Cells("Column21").Value = "01" Then
                    dgvGastos.Rows(x).Cells("Column17").Value = False
                    dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                End If
                If dgvGastos.Rows(x).Cells("Column21").Value = "02" Then
                    dgvGastos.Rows(x).Cells("Column17").Value = True
                    dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
                    dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
                End If
                dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
                dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
                dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
                'Next
                'End If

            Next
        End If
    End Sub

    Private Sub dgvGastos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGastos.CellContentClick
        If dgvGastos.Rows.Count > 0 Then
            Dim columna As Integer = dgvGastos.CurrentCell.ColumnIndex

            If columna = 0 Then

                If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = False Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value = True
                ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = True Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value = False
                End If

                If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = False Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Value() = "A Rendir"
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Style.ForeColor = Color.Blue
                    'A Rendir
                    eBusquedaGastos = New clsBusquedaGastos
                    Dim IdMovimiento As String = ""
                    Dim resp As Integer = 0
                    IdMovimiento = Trim(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value)

                    resp = eBusquedaGastos.fGrabarRendidoOPorRendir(53, IdMovimiento, gEmpresa, "01")

                ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = True Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Value() = "Rendido"
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Style.ForeColor = Color.Red
                    'Rendido
                    eBusquedaGastos = New clsBusquedaGastos
                    Dim IdMovimiento As String = ""
                    Dim resp As Integer = 0
                    IdMovimiento = Trim(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value)

                    resp = eBusquedaGastos.fGrabarRendidoOPorRendir(53, IdMovimiento, gEmpresa, "02")

                End If
            ElseIf columna = 1 Then
                If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value) = False Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value = True
                ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value) = True Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value = False
                End If
            End If

        End If

        'If dgvGastos.Rows.Count > 0 Then
        '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
        '        If Convert.ToBoolean(dgvGastos.Rows(x).Cells("Column17").Value) = False Then
        '            dgvGastos.Rows(x).Cells("Column18").Value() = "A Rendir"
        '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
        '        ElseIf Convert.ToBoolean(dgvGastos.Rows(x).Cells("Column17").Value) = True Then
        '            dgvGastos.Rows(x).Cells("Column18").Value() = "Rendido"
        '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
        '        End If
        '        dgvGastos.Rows(x).Cells("Observacion").Value = IIf(Microsoft.VisualBasic.IsDBNull(dgvGastos.Rows(x).Cells("Observacion").Value), "", dgvGastos.Rows(x).Cells("Observacion").Value)
        '        If Len(Trim(dgvGastos.Rows(x).Cells("Observacion").Value)) > 0 Then
        '            gestionaResaltados3(dgvGastos, x, Color.LightCoral)
        '        End If
        '        If Trim(dgvGastos.Rows(x).Cells("Column7").Value) = "1" Or Trim(dgvGastos.Rows(x).Cells("Column7").Value) = "2" Then
        '            gestionaResaltados(dgvGastos, x, Color.Orange)
        '        End If
        '    Next
        'End If

    End Sub

    'Private Sub dgvGastos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvGastos.Click
    '    If dgvGastos.Rows.Count > 0 Then
    '        Dim columna As Integer = dgvGastos.CurrentCell.ColumnIndex
    '        If columna = 1 Then

    '            If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = False Then
    '                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value = True
    '            ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = True Then
    '                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value = False
    '            End If

    '            If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = False Then
    '                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Value() = "A Rendir"
    '                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Style.ForeColor = Color.Blue
    '            ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column17").Value) = True Then
    '                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Value() = "Rendido"
    '                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Style.ForeColor = Color.Red
    '            End If
    '        End If
    '    End If
    'End Sub

    Private Sub dgvGastos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGastos.CellEndEdit
        Dim StrCad As String = String.Empty
        If e.ColumnIndex = 21 Then
            StrCad = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells(e.ColumnIndex).Value
            If StrCad = Nothing Then Exit Sub
            Try
                Dim FechaIng As DateTime
                FechaIng = Convert.ToDateTime(StrCad)
            Catch ex As Exception
                MessageBox.Show("Ingrese una Fecha Valida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvGastos.CurrentCell = dgvGastos(18, dgvGastos.CurrentRow.Index)
                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells(e.ColumnIndex).Value = ""
            End Try
        End If
    End Sub



    Private Sub dgvGastos_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvGastos.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvGastos.CurrentCell.ColumnIndex
        If columna = 21 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
                e.KeyChar = Chr(0)
            End If
        End If
    End Sub

    Private Sub dgvGastos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvGastos.KeyDown

        Select Case e.KeyCode
            Case Keys.F2
                If dgvGastos.Rows.Count > 0 Then
                    Dim sIdMovimiento As String = ""
                    Dim sFormaDet As String = ""
                    Dim xEmpresaAPrestar As String = ""
                    Dim dblMonto As Double = 0
                    sIdMovimiento = IIf(Microsoft.VisualBasic.IsDBNull(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value), "", dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value)
                    If Trim(sIdMovimiento) <> "" Then
                        Dim frm As New frmDetalleMovimiento2
                        frm.Owner = Me
                        frm.sIdMovimiento = Trim(sIdMovimiento)
                        'frm.Monto = Trim(dblMonto)
                        frm.ShowInTaskbar = False
                        frm.ShowDialog()
                    Else
                        MessageBox.Show("No Existe Detalle", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    MessageBox.Show("Seleccione un Registro de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Case Keys.Space
                If dgvGastos.Rows.Count > 0 Then
                    'dgvGastos_CellContentClick(sender, e)
                    'txtTexto.Focus()
                    'Else
                    'MessageBox.Show("Seleccione un Registro de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
        End Select
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'If dtpFechaFin.Value < dtpFechaIni.Value Then
        '    MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    dtpFechaIni.Focus()
        '    Exit Sub
        'End If

        'If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    cboCriterio.Focus()
        '    Exit Sub
        'End If

        'Dim x As frmReporteConsultaGas = frmReporteConsultaGas.Instance
        'x.MdiParent = frmPrincipal

        'If Len(txtTexto.Text) > 0 Then
        '    If cboCriterio.Text = "CONCEPTO" Then
        '        x.prmOpcionx = 1
        '        x.prmTextox = Trim(txtTexto.Text)
        '        x.prmFIni = dtpFechaIni.Value
        '        x.prmFFin = dtpFechaFin.Value
        '        x.prmCampoOrd = ""
        '        x.prmAscDesc = ""
        '    ElseIf cboCriterio.Text = "RESPONSABLE" Then
        '        x.prmOpcionx = 2
        '        x.prmTextox = Trim(txtTexto.Text)
        '        x.prmFIni = dtpFechaIni.Value
        '        x.prmFFin = dtpFechaFin.Value
        '        x.prmCampoOrd = ""
        '        x.prmAscDesc = ""
        '    ElseIf cboCriterio.Text = "CHEQUE" Then
        '        x.prmOpcionx = 3
        '        x.prmTextox = Trim(txtTexto.Text)
        '        x.prmFIni = dtpFechaIni.Value
        '        x.prmFFin = dtpFechaFin.Value
        '        x.prmCampoOrd = ""
        '        x.prmAscDesc = ""
        '    ElseIf cboCriterio.Text = "MACRO" Then
        '        x.prmOpcionx = 4
        '        x.prmTextox = Trim(txtTexto.Text)
        '        x.prmFIni = dtpFechaIni.Value
        '        x.prmFFin = dtpFechaFin.Value
        '        x.prmCampoOrd = ""
        '        x.prmAscDesc = ""
        '    ElseIf cboCriterio.Text = "CARTA" Then
        '        x.prmOpcionx = 5
        '        x.prmTextox = Trim(txtTexto.Text)
        '        x.prmFIni = dtpFechaIni.Value
        '        x.prmFFin = dtpFechaFin.Value
        '        x.prmCampoOrd = ""
        '        x.prmAscDesc = ""
        '    ElseIf cboCriterio.Text = "TRANSFERENCIA" Then
        '        x.prmOpcionx = 6
        '        x.prmTextox = Trim(txtTexto.Text)
        '        x.prmFIni = dtpFechaIni.Value
        '        x.prmFFin = dtpFechaFin.Value
        '        x.prmCampoOrd = ""
        '        x.prmAscDesc = ""
        '    ElseIf cboCriterio.Text = "OTROS" Then
        '        x.prmOpcionx = 7
        '        x.prmTextox = Trim(txtTexto.Text)
        '        x.prmFIni = dtpFechaIni.Value
        '        x.prmFFin = dtpFechaFin.Value
        '        x.prmCampoOrd = ""
        '        x.prmAscDesc = ""
        '    End If
        'End If

        'If cboCriterio.SelectedIndex = -1 Then
        '    x.prmOpcionx = 0
        '    x.prmTextox = Trim(txtTexto.Text)
        '    x.prmFIni = dtpFechaIni.Value
        '    x.prmFFin = dtpFechaFin.Value
        '    x.prmCampoOrd = ""
        '    x.prmAscDesc = ""
        'End If

        'x.Show()
    End Sub


    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.dgvGastos.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                For Each fila As DataGridViewRow In Me.dgvGastos.Rows
                    If Me.CheckBox1.Checked = True Then
                        fila.Cells("Column14").Value = True
                    Else
                        fila.Cells("Column14").Value = False
                    End If
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    'Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.dgvGastos.ClearSelection()
    '    Try
    '        Using scope As TransactionScope = New TransactionScope
    '            For Each fila As DataGridViewRow In Me.dgvGastos.Rows
    '                If Me.CheckBox1.Checked = True Then
    '                    fila.Cells("Column16").Value = True
    '                Else
    '                    fila.Cells("Column16").Value = False
    '                End If
    '            Next
    '            scope.Complete()
    '        End Using
    '    Catch ex As Exception
    '        MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
    '    End Try
    'End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCriterio.Focus()
            Exit Sub
        End If
        Dim dtBusqueda As DataTable
        eBusquedaGastos = New clsBusquedaGastos
        dtBusqueda = New DataTable
        If rdbPorEnviar.Checked = True And traer = "SI" Then
            dtBusqueda = eBusquedaGastos.fConsultaGastos2(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        End If

        If rdbEnviados.Checked = True And traer = "SI" Then
            dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "", 0)
        End If

        If rdbEnviadosRendidos.Checked = True And traer = "SI" Then
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(11, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        End If

        If rdbPorEnviar.Checked = True And traer = "SI" Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            'If dgvGastos.Rows.Count > 0 Then
            '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
            '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
            '        dgvGastos.Rows(x).Cells("Column15").Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
            '        dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
            '        dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
            '    Next
            'End If
            Exit Sub
        End If

        If rdbEnviados.Checked = True And traer = "SI" Then
            dgvGastos.DataSource = dtBusqueda
            Pintar2()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            'If dgvGastos.Rows.Count > 0 Then
            '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
            '        dgvGastos.Rows(x).Cells("Column14").Value = True
            '        dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
            '        If dgvGastos.Rows(x).Cells("Column21").Value = "01" Then
            '            dgvGastos.Rows(x).Cells("Column17").Value = False
            '            dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
            '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
            '        ElseIf dgvGastos.Rows(x).Cells("Column21").Value = "02" Then
            '            dgvGastos.Rows(x).Cells("Column17").Value = True
            '            dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
            '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
            '        End If
            '        dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
            '    Next
            'End If
            Exit Sub
        End If

        If rdbEnviadosRendidos.Checked = True And traer = "SI" Then
            dgvGastos.DataSource = dtBusqueda
            Pintar3()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            'If dgvGastos.Rows.Count > 0 Then
            '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
            '        gestionaResaltados3(dgvGastos, x, Color.LimeGreen)
            '        dgvGastos.Rows(x).Cells("Column14").Value = True
            '        dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
            '        If dtBusqueda.Rows(x).Item("IdRendicion") = "01" Then
            '            dgvGastos.Rows(x).Cells("Column17").Value = False
            '            dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
            '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
            '        End If
            '        If dtBusqueda.Rows(x).Item("IdRendicion") = "02" Then
            '            dgvGastos.Rows(x).Cells("Column17").Value = True
            '            dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
            '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
            '        End If
            '        dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
            '        dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
            '    Next
            'End If
            Exit Sub
        End If

    End Sub

    Private Sub dtpFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub rdbEnviados_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbEnviados.CheckedChanged
        btnEnviar.Enabled = False
        CheckBox1.Enabled = False
        CheckBox1.Checked = True
        btnImprimir.Enabled = True
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub rdbPorEnviar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPorEnviar.CheckedChanged
        btnEnviar.Enabled = True
        CheckBox1.Enabled = True
        CheckBox1.Checked = False
        btnImprimir.Enabled = False
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Sub ValidarDatos()
        'Dim count = 0
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                If Convert.ToBoolean(dgvGastos.Rows(x).Cells("Column14").Value) = True And Trim(dgvGastos.Rows(x).Cells("Column15").Value) = "" Then
                    MessageBox.Show("Ingrese la Fecha del Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvGastos.CurrentCell = dgvGastos(18, x)
                    GrabarDet = 0
                    Exit Sub
                End If
            Next
            GrabarDet = 1
        End If
    End Sub


    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        ValidarDatos()
        If GrabarDet = 1 Then
            Dim ContaEnviarSinRendir As Integer = 0
            Dim ContaEnviarRendidos As Integer = 0
            Dim rEnviaDoc As Integer = 0
            Dim rEnviaDoc2 As Integer = 0

            If dgvGastos.Rows.Count > 0 Then
                For z As Integer = 0 To dgvGastos.Rows.Count - 1
                    If Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column14").Value) = True And Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column17").Value) = False Then
                        'a rendir y enviar
                        eBusquedaGastos = New clsBusquedaGastos
                        Dim IdMovimiento As String = ""
                        Dim ObservacionRechazo As String = ""
                        Dim FechaEntrega As DateTime
                        IdMovimiento = Trim(dgvGastos.Rows(z).Cells("Column9").Value)
                        FechaEntrega = Trim(dgvGastos.Rows(z).Cells("Column15").Value)
                        ObservacionRechazo = Trim(dgvGastos.Rows(z).Cells("Column16").Value)
                        rEnviaDoc = eBusquedaGastos.fEnviarDoc(IdMovimiento, gEmpresa, 1, FechaEntrega, ObservacionRechazo)
                        If rEnviaDoc = 1 Then
                            ContaEnviarSinRendir = ContaEnviarSinRendir + 1
                        End If
                    ElseIf Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column14").Value) = True And Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column17").Value) = True Then
                        'rendido y enviar
                        eBusquedaGastos = New clsBusquedaGastos
                        Dim IdMovimiento As String = ""
                        Dim ObservacionRechazo As String = ""
                        Dim FechaEntrega As DateTime
                        IdMovimiento = Trim(dgvGastos.Rows(z).Cells("Column9").Value)
                        FechaEntrega = Trim(dgvGastos.Rows(z).Cells("Column15").Value)
                        ObservacionRechazo = Trim(dgvGastos.Rows(z).Cells("Column16").Value)
                        rEnviaDoc2 = eBusquedaGastos.fEnviarDocRendido(IdMovimiento, gEmpresa, 1, FechaEntrega, ObservacionRechazo)
                        If rEnviaDoc2 = 1 Then
                            ContaEnviarRendidos = ContaEnviarRendidos + 1
                        End If
                    End If
                Next
            End If
            If ContaEnviarSinRendir = 0 And ContaEnviarRendidos = 0 Then
                MessageBox.Show("Seleccione Documentos para Enviar!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            If ContaEnviarSinRendir > 0 Or ContaEnviarRendidos > 0 Then
                MessageBox.Show("Se Enviaron: " & ContaEnviarSinRendir & " Documentos A Rendir y " & ContaEnviarRendidos & " Documentos Rendidos.", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                'dtpFechaFin_ValueChanged(sender, e)
                rdbEnviados.Focus()
                rdbEnviados.Checked = True
                'rdbEnviados_CheckedChanged(sender, e)
                Exit Sub
            End If
        End If
    End Sub


    Private Sub cboCriterio_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCriterio.SelectionChangeCommitted
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If

        'If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    cboCriterio.Focus()
        '    Exit Sub
        'End If

        If cboCriterio.SelectedIndex <> -1 Then
            If Trim(cboCriterio.Text) = "TRANSFERENCIA" Then

                Dim dtBusqueda As DataTable
                eBusquedaGastos = New clsBusquedaGastos
                dtBusqueda = New DataTable

                If rdbEnviados.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(52, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "00005", 0)
                End If

                If rdbPorEnviar.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(12, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "00005", 0)
                End If

                If rdbPorEnviar.Checked = True And traer = "SI" Then
                    dgvGastos.DataSource = dtBusqueda
                    Pintar()
                    Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
                    'If dgvGastos.Rows.Count > 0 Then
                    '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
                    '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                    '        dgvGastos.Rows(x).Cells("Column15").Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
                    '        dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    '        dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                    '    Next
                    'End If
                    Exit Sub
                End If

                If rdbEnviados.Checked = True And traer = "SI" Then
                    dgvGastos.DataSource = dtBusqueda
                    Pintar2()
                    Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
                    'If dgvGastos.Rows.Count > 0 Then
                    '    For x As Integer = 0 To dgvGastos.Rows.Count - 1
                    '        dgvGastos.Rows(x).Cells("Column14").Value = True
                    '        dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                    '        If dgvGastos.Rows(x).Cells("Column21").Value = "01" Then
                    '            dgvGastos.Rows(x).Cells("Column17").Value = False
                    '            dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                    '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                    '        ElseIf dgvGastos.Rows(x).Cells("Column21").Value = "02" Then
                    '            dgvGastos.Rows(x).Cells("Column17").Value = True
                    '            dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
                    '            dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
                    '        End If
                    '        dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
                    '        dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
                    '    Next
                    'End If
                    Exit Sub
                End If

                'If rdbEnviadosRendidos.Checked = True Then
                '    dgvGastos.DataSource = dtBusqueda
                '    Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
                '    If dgvGastos.Rows.Count > 0 Then
                '        For x As Integer = 0 To dgvGastos.Rows.Count - 1
                '            gestionaResaltados3(dgvGastos, x, Color.LimeGreen)
                '            dgvGastos.Rows(x).Cells("Column14").Value = True
                '            dgvGastos.Rows(x).Cells("Column14").ReadOnly = True
                '            dgvGastos.Rows(x).Cells("Column16").ReadOnly = False
                '            If dtBusqueda.Rows(x).Item("IdRendicion") = "01" Then
                '                dgvGastos.Rows(x).Cells("Column17").Value = False
                '                dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                '                dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
                '            End If
                '            If dtBusqueda.Rows(x).Item("IdRendicion") = "02" Then
                '                dgvGastos.Rows(x).Cells("Column17").Value = True
                '                dgvGastos.Rows(x).Cells("Column18").Value = "Rendido"
                '                dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Red
                '            End If
                '            dgvGastos.Rows(x).Cells("Column17").ReadOnly = True
                '            dgvGastos.Rows(x).Cells("Column18").ReadOnly = True
                '            dgvGastos.Rows(x).Cells("Column15").ReadOnly = True
                '        Next
                '    End If
                '    Exit Sub
                'End If

                'For x As Integer = 0 To dgvGastos.RowCount - 1
                '    dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
                'Next

                'If rdbPorEnviar.Checked = True Then
                '    If dtBusqueda.Rows.Count > 0 Then
                '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
                '            dgvGastos.Rows.Add()
                '            For z As Integer = 0 To dtBusqueda.Columns.Count - 1
                '                If z = 9 Then
                '                    If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
                '                        gestionaResaltados(dgvGastos, y, Color.Orange)
                '                    End If
                '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                '                ElseIf z = 10 Then
                '                    If dtBusqueda.Rows(y).Item(z) > "1" Then
                '                        gestionaResaltados2(dgvGastos, y, Color.Orange)
                '                    End If
                '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                '                ElseIf z = 19 Then
                '                    If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
                '                        dgvGastos.Rows(y).Cells(17).Value = ""
                '                    Else
                '                        dgvGastos.Rows(y).Cells(17).Value = Trim(dtBusqueda.Rows(y).Item(z))
                '                        If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
                '                            gestionaResaltados3(dgvGastos, y, Color.Orange)
                '                        End If
                '                    End If
                '                    dgvGastos.Rows(y).Cells(17).ReadOnly = True
                '                ElseIf z = 0 Then
                '                    dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
                '                ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
                '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                '                End If
                '                'dgvGastos.Rows(y).Cells(z).ReadOnly = False
                '            Next
                '            dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
                '        Next
                '    End If
                'End If

                'If rdbEnviados.Checked = True Then
                '    If dtBusqueda.Rows.Count > 0 Then
                '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
                '            dgvGastos.Rows.Add()
                '            For z As Integer = 0 To dtBusqueda.Columns.Count - 1
                '                If z = 9 Then
                '                    If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
                '                        gestionaResaltados(dgvGastos, y, Color.Orange)
                '                    End If
                '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                '                ElseIf z = 10 Then
                '                    If dtBusqueda.Rows(y).Item(z) > "1" Then
                '                        gestionaResaltados2(dgvGastos, y, Color.Orange)
                '                    End If
                '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                '                ElseIf z = 0 Then
                '                    dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
                '                ElseIf z = 15 Then
                '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
                '                    dgvGastos.Rows(y).Cells(15).Value = True
                '                    'End If
                '                    dgvGastos.Rows(y).Cells(z).ReadOnly = True
                '                ElseIf z = 16 Then
                '                    dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
                '                    dgvGastos.Rows(y).Cells(16).ReadOnly = True
                '                    'Else
                '                ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
                '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
                '                End If
                '            Next
                '        Next
                '    End If
                'End If

                'If rdbPorEnviar.Checked = True Then
                '    If dtBusqueda.Rows.Count > 0 Then
                '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
                '            dgvGastos.Rows.Add()
                '            dgvGastos.Rows(y).Cells("Column7").Value = dtBusqueda.Rows(y).Item("Prestamo")
                '            dgvGastos.Rows(y).Cells("Column8").Value = dtBusqueda.Rows(y).Item("CountDet")
                '            dgvGastos.Rows(y).Cells("Fecha").Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item("FechaMovimiento"), 10)
                '            dgvGastos.Rows(y).Cells(1).Value = dtBusqueda.Rows(y).Item(1)
                '            dgvGastos.Rows(y).Cells(2).Value = dtBusqueda.Rows(y).Item(2)
                '            dgvGastos.Rows(y).Cells(3).Value = dtBusqueda.Rows(y).Item(3)
                '            dgvGastos.Rows(y).Cells(4).Value = dtBusqueda.Rows(y).Item(4)
                '            dgvGastos.Rows(y).Cells(5).Value = dtBusqueda.Rows(y).Item(5)
                '            dgvGastos.Rows(y).Cells(6).Value = dtBusqueda.Rows(y).Item(6)
                '            dgvGastos.Rows(y).Cells(7).Value = dtBusqueda.Rows(y).Item(7)
                '            dgvGastos.Rows(y).Cells(8).Value = dtBusqueda.Rows(y).Item(8)
                '            dgvGastos.Rows(y).Cells(11).Value = dtBusqueda.Rows(y).Item(11)
                '            dgvGastos.Rows(y).Cells(12).Value = dtBusqueda.Rows(y).Item(12)
                '            dgvGastos.Rows(y).Cells(13).Value = dtBusqueda.Rows(y).Item(13)
                '            dgvGastos.Rows(y).Cells(14).Value = dtBusqueda.Rows(y).Item(14)
                '            If dtBusqueda.Rows(y).Item("Prestamo") = "1" Or dtBusqueda.Rows(y).Item("Prestamo") = "2" Then
                '                gestionaResaltados(dgvGastos, y, Color.Orange)
                '            End If
                '            If dtBusqueda.Rows(y).Item("CountDet") > "1" Then
                '                gestionaResaltados2(dgvGastos, y, Color.Orange)
                '            End If
                '            If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item("ObservacionRechazo")) = True Then
                '                dgvGastos.Rows(y).Cells("Column16").Value = ""
                '            Else
                '                dgvGastos.Rows(y).Cells("Column16").Value = Trim(dtBusqueda.Rows(y).Item("ObservacionRechazo"))
                '                If Len(Trim(dtBusqueda.Rows(y).Item("ObservacionRechazo"))) > 0 Then
                '                    gestionaResaltados3(dgvGastos, y, Color.LightCoral)
                '                End If
                '            End If
                '            dgvGastos.Rows(y).Cells("Column16").ReadOnly = False
                '            dgvGastos.Rows(y).Cells("Column15").Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
                '            dgvGastos.Rows(y).Cells("Column18").Value = "A Rendir"
                '            dgvGastos.Rows(y).Cells("Column18").Style.ForeColor = Color.Blue
                '        Next
                '    End If
                'End If

                'If rdbEnviados.Checked = True Then
                '    If dtBusqueda.Rows.Count > 0 Then
                '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
                '            dgvGastos.Rows.Add()
                '            gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
                '            dgvGastos.Rows(y).Cells("Column7").Value = dtBusqueda.Rows(y).Item("Prestamo")
                '            dgvGastos.Rows(y).Cells("Column8").Value = dtBusqueda.Rows(y).Item("CountDet")
                '            dgvGastos.Rows(y).Cells("Fecha").Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item("FechaMovimiento"), 10)
                '            dgvGastos.Rows(y).Cells(1).Value = dtBusqueda.Rows(y).Item(1)
                '            dgvGastos.Rows(y).Cells(2).Value = dtBusqueda.Rows(y).Item(2)
                '            dgvGastos.Rows(y).Cells(3).Value = dtBusqueda.Rows(y).Item(3)
                '            dgvGastos.Rows(y).Cells(4).Value = dtBusqueda.Rows(y).Item(4)
                '            dgvGastos.Rows(y).Cells(5).Value = dtBusqueda.Rows(y).Item(5)
                '            dgvGastos.Rows(y).Cells(6).Value = dtBusqueda.Rows(y).Item(6)
                '            dgvGastos.Rows(y).Cells(7).Value = dtBusqueda.Rows(y).Item(7)
                '            dgvGastos.Rows(y).Cells(8).Value = dtBusqueda.Rows(y).Item(8)
                '            dgvGastos.Rows(y).Cells(11).Value = dtBusqueda.Rows(y).Item(11)
                '            dgvGastos.Rows(y).Cells(12).Value = dtBusqueda.Rows(y).Item(12)
                '            dgvGastos.Rows(y).Cells(13).Value = dtBusqueda.Rows(y).Item(13)
                '            dgvGastos.Rows(y).Cells(14).Value = dtBusqueda.Rows(y).Item(14)
                '            If dtBusqueda.Rows(y).Item("Prestamo") = "1" Or dtBusqueda.Rows(y).Item("Prestamo") = "2" Then
                '                gestionaResaltados(dgvGastos, y, Color.Orange)
                '            End If
                '            If dtBusqueda.Rows(y).Item("CountDet") > "1" Then
                '                gestionaResaltados2(dgvGastos, y, Color.Orange)
                '            End If

                '            dgvGastos.Rows(y).Cells("Column14").Value = True
                '            dgvGastos.Rows(y).Cells("Column14").ReadOnly = True

                '            If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item("ObservacionRechazo")) = True Then
                '                dgvGastos.Rows(y).Cells("Column16").Value = ""
                '            Else
                '                dgvGastos.Rows(y).Cells("Column16").Value = Trim(dtBusqueda.Rows(y).Item("ObservacionRechazo"))
                '            End If
                '            dgvGastos.Rows(y).Cells("Column16").ReadOnly = False

                '            If dtBusqueda.Rows(y).Item("IdRendicion") = "01" Then
                '                dgvGastos.Rows(y).Cells(15).Value = False
                '                dgvGastos.Rows(y).Cells(16).Value = "A Rendir"
                '                dgvGastos.Rows(y).Cells(16).Style.ForeColor = Color.Blue
                '            End If
                '            If dtBusqueda.Rows(y).Item("IdRendicion") = "02" Then
                '                dgvGastos.Rows(y).Cells(15).Value = True
                '                dgvGastos.Rows(y).Cells(16).Value = "Rendido"
                '                dgvGastos.Rows(y).Cells(16).Style.ForeColor = Color.Red
                '            End If
                '            dgvGastos.Rows(y).Cells(15).ReadOnly = True
                '            dgvGastos.Rows(y).Cells(16).ReadOnly = True

                '            dgvGastos.Rows(y).Cells("Column15").Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item("FechaEnvio"), 10) 'Microsoft.VisualBasic.Left(Now.Date(), 10)
                '            dgvGastos.Rows(y).Cells("Column15").ReadOnly = True
                '        Next
                '    End If
                'End If

                'Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString

            End If
        End If
    End Sub

    Private Sub btnImprimir_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If

        Dim x As frmReporteDocEnv = frmReporteDocEnv
        x.MdiParent = frmPrincipal
        x.xprmOpcionx = 1
        x.xprmTextox = Trim(txtTexto.Text)
        x.xprmFIni = dtpFechaIni.Value
        x.xprmFFin = dtpFechaFin.Value
        x.xprmCampoOrd = ""
        x.xprmAscDesc = ""
        x.Show()
    End Sub

    Private Sub rdbEnviadosRendidos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbEnviadosRendidos.CheckedChanged
        btnEnviar.Enabled = False
        CheckBox1.Enabled = False
        CheckBox1.Checked = False
        btnImprimir.Enabled = False
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub cboCriterio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCriterio.SelectedIndexChanged

    End Sub

 
End Class