﻿
Imports BusinessLogicLayer.Procesos
Imports System.Data.SqlClient
Imports System.Transactions
Imports CapaEntidad

Public Class frmNotaCredito

    Public Shared VL_MONTO As Decimal
    Public Shared VL_IdFactura As String


    Private Sub ListarNotaCredito()

        Dim VL_SrvNotaCredito As SrvNotaCredito = New SrvNotaCredito()
        Dim VL_BeanResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect

        Try
            VL_BeanResultado = VL_SrvNotaCredito.Fnc_Listar_NotaCredito(frmDerivarPagosOrdenCompra.VL_EMPRESA, frmDerivarPagosOrdenCompra.VL_RUC, frmDerivarPagosOrdenCompra.VL_MONEDA)


            If VL_BeanResultado.blnExiste = True Then

                'dgvLista.DataSource = VL_BeanResultado.dtResultado
                dgvNotaCredito.DataSource = VL_BeanResultado.dtResultado

            Else
                  MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try
    End Sub

    Private Sub InsertarNotaCredito(ByVal VL_IdCorrelativo As Decimal)
        Dim VL_SrvNotaCredito As SrvNotaCredito = New SrvNotaCredito()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()



        VL_BeanResultado = VL_SrvNotaCredito.Fnc_Insertar_NotaCredito(VL_IdCorrelativo, frmDerivarPagosOrdenCompra.VL_DOCPENDIENTE)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
    End Sub


    Private Sub frmNotaCredito_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ListarNotaCredito()

    End Sub

    Private Sub btnAplicar_Click(sender As Object, e As EventArgs) Handles btnAplicar.Click

        Dim yesno As Integer

        yesno = MessageBox.Show("¿Esta seguro que desea aplicar esta Nota de Crédito?", "INFORMACION", MessageBoxButtons.YesNo, MessageBoxIcon.Question)


        If yesno = vbYes Then

            For i As Integer = 0 To dgvNotaCredito.RowCount - 1
                If dgvNotaCredito.Rows(i).Cells("Seleccionar").Value = True Then

                    If dgvNotaCredito.Rows(i).Cells("ImporteTotal").Value <= frmDerivarPagosOrdenCompra.VL_NETOPAGAR Then
                        VL_MONTO += Convert.ToDecimal(dgvNotaCredito.Rows(i).Cells("ImporteTotal").Value)
                        VL_IdFactura = dgvNotaCredito.Rows(i).Cells("IdCorrelativo").Value

                        Dim IdCorrelativo As Decimal = Convert.ToDecimal(dgvNotaCredito.Rows(i).Cells("IdCorrelativo").Value)

                        Me.DialogResult = Windows.Forms.DialogResult.OK
                        Me.Close()
                        InsertarNotaCredito(IdCorrelativo)
                    Else
                        VL_MONTO = "0.00"
                        MessageBox.Show("Neto a Pagar es menor a la Nota de Crédito", "INFORMACIÓN...!")
                        dgvNotaCredito.Rows(i).Cells("Seleccionar").Value = False

                    End If
   

                End If


            Next


        End If


        If yesno = vbNo Then
            For i As Integer = 0 To dgvNotaCredito.RowCount - 1
                dgvNotaCredito.Rows(i).Cells("Seleccionar").Value = False
            Next
        End If
    End Sub

End Class