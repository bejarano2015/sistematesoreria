Imports CapaDatos
Imports IWshRuntimeLibrary
Public Class FrmRegistroFast
    Private cCapaDatos As clsCapaDatos
    Dim NomPc As New WshNetwork
    Dim CajasTextbox As Object

    Private Sub FrmRegistroFast_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cCapaDatos = New clsCapaDatos

        For Each CajasTextbox In gbRegistroProveedor.Controls
            If TypeOf CajasTextbox Is TextBox Then
                If CajasTextbox.name <> "txtTipoAnexo" Then
                    If TypeOf CajasTextbox Is TextBox Then CajasTextbox.text = String.Empty
                End If
            End If
        Next

        txtTipoAnexo.Text = "P"
        txtTipoAnexo.Enabled = False
        txtCodigo.Text = ""
        Timer1.Enabled = True
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        'FrmRegCompras.txtCodigoProveedor.Text = txtCodigo.Text
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        txtCodigo.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Devuelve_Descripcion()
        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("SELECT tipoanaliticodescripcion " & _
                            "FROM Contabilidad.ct_tipoanalitico " & _
                            "WHERE tipoanaliticocodigo='" & "P" & "'", CommandType.Text)
                lblDescTipoAnex.Text = .fCmdExecuteScalar()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
                lblDescTipoAnex.Text = String.Empty
            End Try
            .sDesconectarSQL()
        End With
    End Sub

    Private Sub txtTipoAnexo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTipoAnexo.TextChanged
        Call Devuelve_Descripcion()
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        If Validar_Datos() = False Then Exit Sub
        If MessageBox.Show("Desea grabar?", "Contabilidad", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            With cCapaDatos
                Try
                    .sConectarSQL()
                    .sComenzarTransaccion()
                    .sComandoSQL("INSERT INTO Contabilidad.ct_analitico " & _
                                    "(EmprCodigo,tipoanaliticocodigo,analiticocodigo,analiticodescripcion,RUC,f_Poliza)" & _
                                    "VALUES " & _
                                    "('" & gEmpresa & "','" & Trim(txtTipoAnexo.Text) & "','" & Trim(txtCodigo.Text) & "','" & Trim(txtDescripcion.Text) & "','" & Trim(txtRUC.Text) & "','S')", CommandType.Text)
                    .fCmdExecuteNonQuery()
                    .sConfirmarTransaccion()
                    MessageBox.Show("Se guardaron los datos con exito", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    .sCancelarTransaccion()
                End Try
                .sDesconectarSQL()
            End With
            'FrmRegCompras.txtCodigoProveedor.Text = String.Empty
            'FrmRegCompras.txtCodigoProveedor.Text = txtCodigo.Text
            Me.Close()
        End If

    End Sub

    Private Function Validar_Datos() As Boolean
        If txtCodigo.Text = String.Empty Then
            MessageBox.Show("No ingreso Codigo para el Anexo", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtCodigo.Focus()
            Return False
        ElseIf txtDescripcion.Text = String.Empty Then
            MessageBox.Show("No ingreso la Descripcion para el Anexo", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtDescripcion.Focus()
            Return False
        ElseIf txtReferencia.Text = String.Empty Then
            MessageBox.Show("No ingreso la referencia para el Anexo", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtReferencia.Focus()
            Return False
        ElseIf txtTipoAnexo.Text = "P" Then
            If txtRuc.Text = String.Empty Then
                MessageBox.Show("No ingreso la referencia para el Anexo", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
                txtRuc.Focus()
                Return False
            End If
        End If
        Return True
    End Function


    Private Sub txtRuc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRUC.GotFocus
        txtRUC.Text = txtCodigo.Text
    End Sub

    Private Sub txtCodigo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodigo.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
            Case Keys.F5
                btnGrabar_Click(sender, e)
        End Select
    End Sub

    Private Sub txtDescripcion_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtDescripcion.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
            Case Keys.F5
                btnGrabar_Click(sender, e)
        End Select
    End Sub

    Private Sub txtReferencia_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtReferencia.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
            Case Keys.F5
                btnGrabar_Click(sender, e)
        End Select
    End Sub

    Private Sub txtRUC_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRUC.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
            Case Keys.F5
                btnGrabar_Click(sender, e)
        End Select
    End Sub

    Private Sub txtCodigo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodigo.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtDescripcion.Focus()
        End Select
    End Sub

    Private Sub txtDescripcion_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescripcion.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtReferencia.Focus()
        End Select
    End Sub

    Private Sub txtReferencia_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtReferencia.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtRUC.Focus()
        End Select
    End Sub

    Private Sub txtRUC_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRUC.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                btnGrabar.Focus()
        End Select
    End Sub

End Class