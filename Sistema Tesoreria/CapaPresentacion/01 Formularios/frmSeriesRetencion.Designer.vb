<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeriesRetencion
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtNroIni = New ctrLibreria.Controles.BeTextBox
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtSerie = New ctrLibreria.Controles.BeTextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.BeTextBox1 = New ctrLibreria.Controles.BeTextBox
        Me.dgvLista = New System.Windows.Forms.DataGridView
        Me.IdSerie = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cSerie = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NumeroIni = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.nf = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(538, 262)
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Size = New System.Drawing.Size(530, 236)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.BeTextBox1)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.txtNroIni)
        Me.TabPage2.Controls.Add(Me.txtCodigo)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.txtSerie)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Size = New System.Drawing.Size(530, 236)
        Me.TabPage2.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(35, 138)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 40
        Me.Label2.Text = "N� Inicial"
        '
        'txtNroIni
        '
        Me.txtNroIni.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroIni.BackColor = System.Drawing.Color.Ivory
        Me.txtNroIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroIni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroIni.ForeColor = System.Drawing.Color.Black
        Me.txtNroIni.KeyEnter = True
        Me.txtNroIni.Location = New System.Drawing.Point(129, 131)
        Me.txtNroIni.Name = "txtNroIni"
        Me.txtNroIni.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroIni.ShortcutsEnabled = False
        Me.txtNroIni.Size = New System.Drawing.Size(185, 20)
        Me.txtNroIni.TabIndex = 39
        Me.txtNroIni.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.Ivory
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(129, 74)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(185, 20)
        Me.txtCodigo.TabIndex = 36
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(35, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "C�digo"
        '
        'txtSerie
        '
        Me.txtSerie.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSerie.BackColor = System.Drawing.Color.Ivory
        Me.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.ForeColor = System.Drawing.Color.Black
        Me.txtSerie.KeyEnter = True
        Me.txtSerie.Location = New System.Drawing.Point(129, 103)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSerie.ShortcutsEnabled = False
        Me.txtSerie.Size = New System.Drawing.Size(185, 20)
        Me.txtSerie.TabIndex = 38
        Me.txtSerie.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(35, 105)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 37
        Me.Label4.Text = "Serie"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(35, 164)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 13)
        Me.Label3.TabIndex = 42
        Me.Label3.Text = "N� Final"
        '
        'BeTextBox1
        '
        Me.BeTextBox1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.BeTextBox1.BackColor = System.Drawing.Color.Ivory
        Me.BeTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.BeTextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.BeTextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeTextBox1.ForeColor = System.Drawing.Color.Black
        Me.BeTextBox1.KeyEnter = True
        Me.BeTextBox1.Location = New System.Drawing.Point(129, 157)
        Me.BeTextBox1.Name = "BeTextBox1"
        Me.BeTextBox1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.BeTextBox1.ShortcutsEnabled = False
        Me.BeTextBox1.Size = New System.Drawing.Size(185, 20)
        Me.BeTextBox1.TabIndex = 41
        Me.BeTextBox1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdSerie, Me.cSerie, Me.NumeroIni, Me.nf})
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-5, -1)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.Size = New System.Drawing.Size(529, 231)
        Me.dgvLista.TabIndex = 7
        '
        'IdSerie
        '
        Me.IdSerie.DataPropertyName = "IdSerie"
        Me.IdSerie.HeaderText = "C�digo"
        Me.IdSerie.Name = "IdSerie"
        Me.IdSerie.ReadOnly = True
        Me.IdSerie.Width = 140
        '
        'cSerie
        '
        Me.cSerie.DataPropertyName = "Serie"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.cSerie.DefaultCellStyle = DataGridViewCellStyle1
        Me.cSerie.HeaderText = "Serie"
        Me.cSerie.Name = "cSerie"
        Me.cSerie.ReadOnly = True
        '
        'NumeroIni
        '
        Me.NumeroIni.DataPropertyName = "NumeroIni"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.NumeroIni.DefaultCellStyle = DataGridViewCellStyle2
        Me.NumeroIni.HeaderText = "N� Inicial"
        Me.NumeroIni.Name = "NumeroIni"
        Me.NumeroIni.ReadOnly = True
        '
        'nf
        '
        Me.nf.HeaderText = "N� Final"
        Me.nf.Name = "nf"
        '
        'frmSeriesRetencion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(538, 262)
        Me.Name = "frmSeriesRetencion"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents BeTextBox1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNroIni As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSerie As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents IdSerie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cSerie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumeroIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nf As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
