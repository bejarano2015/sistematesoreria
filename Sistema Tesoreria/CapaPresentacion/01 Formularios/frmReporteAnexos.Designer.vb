<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteAnexos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.cboBancos2 = New ctrLibreria.Controles.BeComboBox
    Me.btnImprimir = New System.Windows.Forms.Button
    Me.cboTipoCargo = New ctrLibreria.Controles.BeComboBox
    Me.BeLabel27 = New ctrLibreria.Controles.BeLabel
    Me.cboBancos = New ctrLibreria.Controles.BeComboBox
    Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.cboBancos2)
    Me.Panel1.Controls.Add(Me.btnImprimir)
    Me.Panel1.Controls.Add(Me.cboTipoCargo)
    Me.Panel1.Controls.Add(Me.BeLabel27)
    Me.Panel1.Controls.Add(Me.cboBancos)
    Me.Panel1.Controls.Add(Me.BeLabel4)
    Me.Panel1.Location = New System.Drawing.Point(5, 5)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(408, 102)
    Me.Panel1.TabIndex = 0
    '
    'cboBancos2
    '
    Me.cboBancos2.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
    Me.cboBancos2.BackColor = System.Drawing.Color.Ivory
    Me.cboBancos2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboBancos2.ForeColor = System.Drawing.Color.Black
    Me.cboBancos2.FormattingEnabled = True
    Me.cboBancos2.KeyEnter = True
    Me.cboBancos2.Location = New System.Drawing.Point(89, 40)
    Me.cboBancos2.Name = "cboBancos2"
    Me.cboBancos2.Size = New System.Drawing.Size(209, 21)
    Me.cboBancos2.TabIndex = 27
    '
    'btnImprimir
    '
    Me.btnImprimir.Location = New System.Drawing.Point(317, 67)
    Me.btnImprimir.Name = "btnImprimir"
    Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
    Me.btnImprimir.TabIndex = 25
    Me.btnImprimir.Text = "Imprimir"
    Me.btnImprimir.UseVisualStyleBackColor = True
    '
    'cboTipoCargo
    '
    Me.cboTipoCargo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
    Me.cboTipoCargo.BackColor = System.Drawing.Color.Ivory
    Me.cboTipoCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboTipoCargo.ForeColor = System.Drawing.Color.Black
    Me.cboTipoCargo.FormattingEnabled = True
    Me.cboTipoCargo.KeyEnter = True
    Me.cboTipoCargo.Location = New System.Drawing.Point(89, 67)
    Me.cboTipoCargo.Name = "cboTipoCargo"
    Me.cboTipoCargo.Size = New System.Drawing.Size(209, 21)
    Me.cboTipoCargo.TabIndex = 24
    '
    'BeLabel27
    '
    Me.BeLabel27.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
    Me.BeLabel27.AutoSize = True
    Me.BeLabel27.BackColor = System.Drawing.Color.Transparent
    Me.BeLabel27.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.BeLabel27.ForeColor = System.Drawing.Color.Black
    Me.BeLabel27.Location = New System.Drawing.Point(15, 70)
    Me.BeLabel27.Name = "BeLabel27"
    Me.BeLabel27.Size = New System.Drawing.Size(71, 13)
    Me.BeLabel27.TabIndex = 23
    Me.BeLabel27.Text = "Tipo Anexo"
    '
    'cboBancos
    '
    Me.cboBancos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
    Me.cboBancos.BackColor = System.Drawing.Color.Ivory
    Me.cboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboBancos.ForeColor = System.Drawing.Color.Black
    Me.cboBancos.FormattingEnabled = True
    Me.cboBancos.KeyEnter = True
    Me.cboBancos.Location = New System.Drawing.Point(89, 13)
    Me.cboBancos.Name = "cboBancos"
    Me.cboBancos.Size = New System.Drawing.Size(209, 21)
    Me.cboBancos.TabIndex = 3
    '
    'BeLabel4
    '
    Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
    Me.BeLabel4.AutoSize = True
    Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
    Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.BeLabel4.ForeColor = System.Drawing.Color.Black
    Me.BeLabel4.Location = New System.Drawing.Point(15, 31)
    Me.BeLabel4.Name = "BeLabel4"
    Me.BeLabel4.Size = New System.Drawing.Size(42, 13)
    Me.BeLabel4.TabIndex = 2
    Me.BeLabel4.Text = "Banco"
    '
    'frmReporteAnexos
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.Color.LightSteelBlue
    Me.ClientSize = New System.Drawing.Size(417, 111)
    Me.Controls.Add(Me.Panel1)
    Me.MaximizeBox = False
    Me.Name = "frmReporteAnexos"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
    Me.Text = "Reporte de Anexos"
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cboBancos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboTipoCargo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel27 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents cboBancos2 As ctrLibreria.Controles.BeComboBox
End Class
