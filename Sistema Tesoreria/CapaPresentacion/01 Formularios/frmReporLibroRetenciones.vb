Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CapaNegocios
Imports CapaDatos

Public Class frmReporLibroRetenciones

    'Public nroArqueo As String
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    'Dim rptGastos As New rptGasGene
    'Dim rptGastos As New rptGasGeneDetallado
    Dim rptRetencionLibro As New rptRetencionesLibro
    Private eRetenciones As clsRetencion

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmReporLibroRetenciones = Nothing
    Public Shared Function Instance() As frmReporLibroRetenciones
        If frmInstance Is Nothing Then
            frmInstance = New frmReporLibroRetenciones
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

#End Region

    Private Sub frmReporGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eRetenciones = New clsRetencion

        cboAnio.Text = Convert.ToString(DateTime.Now.Year)
        cboMes.SelectedIndex = DateTime.Now.Month - 1

        InitCombos()
    End Sub

    Private Sub frmReporGastos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub
    Private Sub frmReporGastos_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

    Private Sub btnGenerar_Click(sender As Object, e As EventArgs) Handles btnGenerar.Click
        rptRetencionLibro.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '1
        crParameterFieldDefinitions = rptRetencionLibro.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@IDEMPRESA")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptRetencionLibro.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@ANIO")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = cboAnio.Text.ToString()
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '3
        crParameterFieldDefinitions = rptRetencionLibro.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@MES")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Convert.ToString(cboMes.SelectedIndex + 1)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptRetencionLibro.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@IDDOCUMENTOORIGEN")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Convert.ToDecimal(cboDocumentoOrigen.SelectedValue)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        ' Ruc
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmprRuc
        crParameterValues.Add(crParameterDiscreteValue)
        rptRetencionLibro.DataDefinition.ParameterFields("strRuc").ApplyCurrentValues(crParameterValues)
        crParameterValues.Clear()

        ' Empresa
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gDesEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        rptRetencionLibro.DataDefinition.ParameterFields("strEmpresa").ApplyCurrentValues(crParameterValues)
        crParameterValues.Clear()

        CrystalReportViewer1.ReportSource = rptRetencionLibro
        'rptRetencion.PrintToPrinter(1, False, 0, 1)
    End Sub

    Private Sub InitCombos()
        Try
            Dim odtDocumento As New DataTable
            odtDocumento = eRetenciones.fCargarDocumentos(11)
            cboDocumentoOrigen.DataSource = odtDocumento
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

End Class