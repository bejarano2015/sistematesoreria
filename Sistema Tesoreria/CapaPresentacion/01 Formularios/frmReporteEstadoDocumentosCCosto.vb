Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmReporteEstadoDocumentosCCosto

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteEstadoDocumentosCCosto = Nothing
    Public Shared Function Instance() As frmReporteEstadoDocumentosCCosto
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteEstadoDocumentosCCosto
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteEstadoDocumentosCCosto_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eGastosGenerales As clsGastosGenerales

    Private Sub frmReporteEstadoDocumentosCCosto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eGastosGenerales = New clsGastosGenerales
        cboCentroCosto.DataSource = eGastosGenerales.fListarCentrosCostos(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboCentroCosto.ValueMember = "CCosCodigo"
            cboCentroCosto.DisplayMember = "CCosDescripcion"
            cboCentroCosto.SelectedIndex = -1
        End If
        'dtpFechaGasto.Value = Now.Date()
        Timer1.Enabled = True
        chk3.Text = "Deudas (Total - Vencidos) X " & gDesEmpresa
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If rdbCancelar.Checked = True Or rdbCancelados.Checked = True Or rdbCC.Checked = True Then
            If cboCentroCosto.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
                cboCentroCosto.Focus()
                Exit Sub
            End If
        End If

        Try
            Dim dtTable2 As DataTable
            Dim pIdRetencion As String = ""
            Dim pSerie As String = ""
            dtTable2 = New DataTable
            eGastosGenerales = New clsGastosGenerales
            If rdbCancelar.Checked = True Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCosto(0, gEmpresa, Trim(cboCentroCosto.SelectedValue))
            End If
            If rdbCancelados.Checked = True Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCosto(1, gEmpresa, Trim(cboCentroCosto.SelectedValue))
            End If
            If rdbCC.Checked = True Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCosto(2, gEmpresa, Trim(cboCentroCosto.SelectedValue))
            End If
            If chk3.Checked = True Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCosto(5, gEmpresa, Trim(cboCentroCosto.SelectedValue))
            End If
            If chk4.Checked = True Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCosto(6, gEmpresa, Trim(cboCentroCosto.SelectedValue))
            End If
            'If dtTable2.Rows.Count > 0 Then
            'End If

            If dtTable2.Rows.Count > 0 Then
                'Dim MontoLetras As String = ""
                'Dim MontoRet As Double = 0
                'MontoRet = IIf(Microsoft.VisualBasic.IsDBNull(dtTable2.Rows(0).Item("PorcentajeRetCab")), 0, dtTable2.Rows(0).Item("PorcentajeRetCab"))
                'Dim dtTableLetras As DataTable
                'dtTableLetras = New DataTable
                'eBusquedaRetenciones = New clsBusquedaRetenciones
                'dtTableLetras = eBusquedaRetenciones.fConsultaMontoLetras(MontoRet)
                'If dtTableLetras.Rows.Count > 0 Then
                '    MontoLetras = IIf(Microsoft.VisualBasic.IsDBNull(dtTableLetras.Rows(0).Item(0)), "", dtTableLetras.Rows(0).Item(0))
                'End If
                'Muestra_Reporte(STRRUTAREPORTES & "rptConsolidado.rpt", dtTable2, "TblLibroDiario", "", "StrEmpresa;" & cCapaFunctGen.DescripcionEmpresa(sEmpresa), "StrRUC;" & cCapaFunctGen.RUCempresa(sEmpresa), "StrPeriodo;" & "MES DE " & cboMes.Text & " DE " & sPeriodo, "StrDesMoneda;" & cboMoneda.Text)

                'Dim RutaApp As String = ""
                'RutaApp = My.Application.Info.DirectoryPath
                'If Not RutaApp.EndsWith("\") Then
                '    RutaApp &= "\Reportes\"
                'End If

                If rdbCancelar.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptDocPagar.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & Trim(cboCentroCosto.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & Trim(cboCentroCosto.SelectedValue))
                End If
                If rdbCancelados.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptDocPagados.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & Trim(cboCentroCosto.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & Trim(cboCentroCosto.SelectedValue))
                End If
                If rdbCC.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & Trim(cboCentroCosto.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & Trim(cboCentroCosto.SelectedValue))
                End If
                If chk3.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                    'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                End If
                If chk4.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente3.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                    'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                End If
                'Imprimir_Reporte(RutaAppReportes & "rptRetencion.rpt", dtTable2, "TblLibroDiario", "", "MontoLetras;" & MontoLetras)
            ElseIf dtTable2.Rows.Count = 0 Then
                Dim mensaje As String = ""
                If rdbCancelar.Checked = True Then
                    mensaje = "No se Encontro ningun Comprobante a Cancelar."
                End If
                If rdbCancelados.Checked = True Then
                    mensaje = "No se Encontro ningun Comprobante Cancelado."
                End If
                If rdbCC.Checked = True Then
                    mensaje = "No se Encontro Cuenta Corriente de Proveedores en este Centro de Costo."
                End If
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        cboCentroCosto.Focus()
        Timer1.Enabled = False
    End Sub


    Private Sub chk3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk3.CheckedChanged
        If chk3.Checked = True Then
            cboCentroCosto.SelectedIndex = -1
            rdbCancelar.Checked = False
            rdbCancelados.Checked = False
            rdbCC.Checked = False
        End If
    End Sub

    Private Sub rdbCancelar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbCancelar.CheckedChanged
        chk3.Checked = False
        chk4.Checked = False
    End Sub

    Private Sub rdbCancelados_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbCancelados.CheckedChanged
        chk3.Checked = False
        chk4.Checked = False
    End Sub

    Private Sub rdbCC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbCC.CheckedChanged
        chk3.Checked = False
        chk4.Checked = False
    End Sub

    Private Sub chk4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chk4.CheckedChanged
        If chk4.Checked = True Then
            cboCentroCosto.SelectedIndex = -1
            rdbCancelar.Checked = False
            rdbCancelados.Checked = False
            rdbCC.Checked = False
        End If
    End Sub

    Private Sub cboCentroCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCentroCosto.SelectedIndexChanged

    End Sub
End Class