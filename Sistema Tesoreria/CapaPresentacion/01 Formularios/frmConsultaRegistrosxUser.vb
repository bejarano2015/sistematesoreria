Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data


Public Class frmConsultaRegistrosxUser

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaRegistrosxUser = Nothing
    Public Shared Function Instance() As frmConsultaRegistrosxUser
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaRegistrosxUser
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmConsultaRegistrosxUser_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private ePagoProveedores As clsPagoProveedores
    Private eGastosGenerales As clsGastosGenerales

    Private Sub txtSemana_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSemana.KeyPress
        Try
            Select Case Asc(e.KeyChar)
                Case 13
                    Dim vFinal As Date
                    AxMonthView1.MultiSelect = False
                    AxMonthView1.Week = CInt(txtSemana.Text) + 1
                    AxMonthView1.DayOfWeek = MSComCtl2.DayConstants.mvwMonday
                    AxMonthView1.MultiSelect = True
                    AxMonthView1.MaxSelCount = 7
                    vFinal = DateAdd(DateInterval.Day, 6, AxMonthView1.SelStart)
                    AxMonthView1.SelEnd = vFinal
                    dtpFechaIni.Value = AxMonthView1.SelStart
                    dtpFechaFin.Value = AxMonthView1.SelEnd
                    'dtpEnvio.Value = AxMonthView1.SelEnd
            End Select
        Catch ex As Exception
        End Try

    End Sub

    Private Sub txtSemana_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSemana.TextChanged

    End Sub

    Private Sub frmConsultaRegistrosxUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Try
        '    AxMonthView1.Value = Now.Date
        '    txtSemana.Text = AxMonthView1.Week()
        'Catch ex As Exception
        'End Try

        'Dim dTipoCambio As Double
        'Dim dtTable As DataTable
        'dtTable = New DataTable
        'ePagoProveedores = New clsPagoProveedores

        'dtTable = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, Today())
        'If dtTable.Rows.Count > 0 Then
        '    dTipoCambio = Val(dtTable.Rows(0).Item("TcaVenta"))
        '    'lblTipoCambio.Text = Format(dTipoCambio, "0.00")
        '    txtTipoCambio.Text = Format(dTipoCambio, "0.00")
        'Else
        '    dTipoCambio = 0.0
        '    'lblTipoCambio.Text = Format(dTipoCambio, "0.00")
        '    txtTipoCambio.Text = Format(dTipoCambio, "0.00")
        'End If
        eGastosGenerales = New clsGastosGenerales
        cboCentroCosto.DataSource = eGastosGenerales.fListarCentrosCostos(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboCentroCosto.ValueMember = "CCosCodigo"
            cboCentroCosto.DisplayMember = "CCosDescripcion"
            cboCentroCosto.SelectedIndex = -1
        End If

        dtpFechaFin.Value = Now.Date()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Value = "01/" & Mes & "/" & Now.Year()

        Try
            'AxMonthView1.Year = Now.Year()
            AxMonthView1.Value = dtpFechaFin.Value ' Now.Date
            txtSemana.Text = AxMonthView1.Week()
        Catch ex As Exception
        End Try
        'txtSemana.Focus()
        Timer1.Enabled = True

        ' Private Sub dtpFechaGasto_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFechaGasto.LostFocus
        Try
            'AxMonthView1.Value = dtpFechaGasto.Value
            'txtSemanaPago.Text = AxMonthView1.Week()
        Catch ex As Exception
        End Try


    End Sub


    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Timer1.Enabled = False
        If Timer1.Enabled = True Then
            txtSemana.Focus()
            'BeButton4_Click(sender, e)
        End If
        Timer1.Enabled = False
    End Sub

    Private Sub btnBusca_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBusca.Click
        'If Trim(txtTipoCambio.Text) = "" Or Trim(txtTipoCambio.Text) = "0.00" Then
        '    MessageBox.Show("Ingrese el Tipo de Cambio", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    txtTipoCambio.Focus()
        '    Exit Sub
        'End If

        Try

            Dim dtTable2 As DataTable
            dtTable2 = New DataTable
            ePagoProveedores = New clsPagoProveedores

            If RD1.Checked = True And chkPrestamos.Checked = False Then
                '31
                dtTable2 = ePagoProveedores.fListMovxUsuario(37, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
                '32
                dtTable2 = ePagoProveedores.fListMovxUsuario(38, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            End If

            If RD1.Checked = True And chkPrestamos.Checked = True Then
                '33
                dtTable2 = ePagoProveedores.fListMovxUsuario(39, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
                '34
                dtTable2 = ePagoProveedores.fListMovxUsuario(40, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            End If

            If RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
                dtTable2 = ePagoProveedores.fListMovxUsuario(38, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
                dtTable2 = ePagoProveedores.fListMovxUsuario(41, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, Trim(cboCentroCosto.SelectedValue))
            End If

            If RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
                dtTable2 = ePagoProveedores.fListMovxUsuario(40, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
                dtTable2 = ePagoProveedores.fListMovxUsuario(42, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, Trim(cboCentroCosto.SelectedValue))
            End If

            '''''''''''''''con detalle
            'If RD1.Checked = True And chkPrestamos.Checked = False Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(31, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            'ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(32, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            'End If

            'If RD1.Checked = True And chkPrestamos.Checked = True Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(33, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            'ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(34, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            'End If


            'If RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(32, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            'ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(35, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, Trim(cboCentroCosto.SelectedValue))
            'End If

            'If RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(34, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, "")
            'ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
            '    dtTable2 = ePagoProveedores.fListMovxUsuario(36, gEmpresa, gUsuario, dtpFechaIni.Value, dtpFechaFin.Value, Trim(cboCentroCosto.SelectedValue))
            'End If
            '''''''''''''''fin con detalle
          
            If dtTable2.Rows.Count > 0 Then
                'Dim rptGastos As New rptMovimientosxUsuario6
                'rptGastos.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
                'Dim rptGastos2 As New rptDetalleMov
                'rptGastos2.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)


                If RD1.Checked = True And chkPrestamos.Checked = False Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario6.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario6.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                End If
                If RD1.Checked = True And chkPrestamos.Checked = True Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario6.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario6.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                End If

                If RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario7.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario7.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                End If

                If RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario7.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
                    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario7.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                End If

                '''''''''''''''con detalle
                'If RD1.Checked = True And chkPrestamos.Checked = False Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario2.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario4.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario2.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario4.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'End If

                'If RD1.Checked = True And chkPrestamos.Checked = True Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario2.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario4.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex = -1 Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario2.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario4.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'End If

                'If RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario3.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario5.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'ElseIf RD2.Checked = True And chkPrestamos.Checked = False And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario3.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario5.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'End If

                'If RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = True And cboCentroCosto.SelectedIndex = -1 Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario3.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario5.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'ElseIf RD2.Checked = True And chkPrestamos.Checked = True And chkCCtodos.Checked = False And cboCentroCosto.SelectedIndex > -1 Then
                '    'Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario3.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                '    Muestra_Reporte(RutaAppReportes & "rptMovimientosxUsuario5.rpt", dtTable2, "TblLibroDiario", "", "Semana;" & Trim(txtSemana.Text), "TC;" & 0, "F1;" & dtpFechaIni.Value, "F2;" & dtpFechaFin.Value)
                'End If
                '''''''''''''''fin con detalle
            ElseIf dtTable2.Rows.Count = 0 Then
                Dim mensaje As String = ""
                mensaje = "No se Encontr� ningun Movimiento"
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transacci�n." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If

            f.CRVisor.SelectionFormula = STRfiltro
            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields
        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

   
   
    Private Sub RD1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD1.CheckedChanged
        If RD1.Checked = True Then
            cboCentroCosto.SelectedIndex = -1
            cboCentroCosto.Enabled = False

            chkCCtodos.Checked = False
            chkCCtodos.Enabled = False
        End If
    End Sub

    Private Sub RD2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD2.CheckedChanged
        If RD2.Checked = True Then
            cboCentroCosto.SelectedIndex = -1
            cboCentroCosto.Enabled = True

            chkCCtodos.Checked = False
            chkCCtodos.Enabled = True
        End If
    End Sub

    Private Sub cboCentroCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCentroCosto.SelectedIndexChanged
        If cboCentroCosto.SelectedIndex > -1 Then
            chkCCtodos.Checked = False
        End If
    End Sub

    Private Sub chkCCtodos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCCtodos.CheckedChanged
        If chkCCtodos.Checked = True Then
            cboCentroCosto.SelectedIndex = -1
        End If
    End Sub
End Class