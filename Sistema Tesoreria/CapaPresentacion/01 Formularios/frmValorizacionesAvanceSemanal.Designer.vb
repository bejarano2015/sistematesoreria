<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValorizacionesAvanceSemanal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvHistorial = New System.Windows.Forms.DataGridView
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UnidMed = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MetradoImpor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Lunes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Martes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Miercoles = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Jueves = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Viernes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sabado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Domingo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvHistorial
        '
        Me.dgvHistorial.AllowUserToAddRows = False
        Me.dgvHistorial.BackgroundColor = System.Drawing.Color.White
        Me.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistorial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.UnidMed, Me.MetradoImpor, Me.Lunes, Me.Martes, Me.Miercoles, Me.Jueves, Me.Viernes, Me.Sabado, Me.Domingo, Me.Total})
        Me.dgvHistorial.EnableHeadersVisualStyles = False
        Me.dgvHistorial.Location = New System.Drawing.Point(2, 3)
        Me.dgvHistorial.Name = "dgvHistorial"
        Me.dgvHistorial.RowHeadersVisible = False
        Me.dgvHistorial.Size = New System.Drawing.Size(1126, 178)
        Me.dgvHistorial.TabIndex = 26
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.Width = 200
        '
        'UnidMed
        '
        Me.UnidMed.DataPropertyName = "UnidMed"
        Me.UnidMed.HeaderText = "U.M."
        Me.UnidMed.Name = "UnidMed"
        Me.UnidMed.Width = 50
        '
        'MetradoImpor
        '
        Me.MetradoImpor.DataPropertyName = "MetradoImpor"
        Me.MetradoImpor.HeaderText = "Metrado"
        Me.MetradoImpor.Name = "MetradoImpor"
        Me.MetradoImpor.Width = 50
        '
        'Lunes
        '
        Me.Lunes.DataPropertyName = "Lunes"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle1.Format = "N2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Lunes.DefaultCellStyle = DataGridViewCellStyle1
        Me.Lunes.HeaderText = "Lunes"
        Me.Lunes.Name = "Lunes"
        '
        'Martes
        '
        Me.Martes.DataPropertyName = "Martes"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Martes.DefaultCellStyle = DataGridViewCellStyle2
        Me.Martes.HeaderText = "Martes"
        Me.Martes.Name = "Martes"
        '
        'Miercoles
        '
        Me.Miercoles.DataPropertyName = "Miercoles"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Miercoles.DefaultCellStyle = DataGridViewCellStyle3
        Me.Miercoles.HeaderText = "Miercoles"
        Me.Miercoles.Name = "Miercoles"
        '
        'Jueves
        '
        Me.Jueves.DataPropertyName = "Jueves"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Jueves.DefaultCellStyle = DataGridViewCellStyle4
        Me.Jueves.HeaderText = "Jueves"
        Me.Jueves.Name = "Jueves"
        '
        'Viernes
        '
        Me.Viernes.DataPropertyName = "Viernes"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Viernes.DefaultCellStyle = DataGridViewCellStyle5
        Me.Viernes.HeaderText = "Viernes"
        Me.Viernes.Name = "Viernes"
        '
        'Sabado
        '
        Me.Sabado.DataPropertyName = "Sabado"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.Sabado.DefaultCellStyle = DataGridViewCellStyle6
        Me.Sabado.HeaderText = "Sábado"
        Me.Sabado.Name = "Sabado"
        '
        'Domingo
        '
        Me.Domingo.DataPropertyName = "Domingo"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.Domingo.DefaultCellStyle = DataGridViewCellStyle7
        Me.Domingo.HeaderText = "Domingo"
        Me.Domingo.Name = "Domingo"
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Blue
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = Nothing
        Me.Total.DefaultCellStyle = DataGridViewCellStyle8
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        '
        'frmValorizacionesAvanceSemanal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1129, 183)
        Me.Controls.Add(Me.dgvHistorial)
        Me.MaximizeBox = False
        Me.Name = "frmValorizacionesAvanceSemanal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Avance Semanal"
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvHistorial As System.Windows.Forms.DataGridView
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UnidMed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MetradoImpor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lunes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Martes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Miercoles As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Jueves As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Viernes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sabado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Domingo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
