Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
'Imports CrystalDecisions.Enterprise.Framework
'Imports CrystalDecisions.Enterprise.InfoStore



Public Class frmReporteVoucherPago

    'Public rCodEmpresa As String
    Public rNroVoucher As String = ""



    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    Dim rptF As New rptVoucher
    Public Reporte1o2 As String = ""
    Dim rptF2 As New rptVoucher2
    Public rNroMovimiento As String = ""


#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteVoucherPago = Nothing
    Public Shared Function Instance() As frmReporteVoucherPago
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteVoucherPago
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteVoucherPago_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteVoucherPago_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Reporte1o2 = "1" Then
            rptF.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            '1
            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@CodEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '2
            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NroVaucher")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(rNroVoucher)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gDesEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


            CrystalReportViewer1.ReportSource = rptF




            ' CrystalReportViewer1.PrintReport()

            'rptF.Refresh()



            'rptF.PrintToPrinter(1, False, 0, 1)
        End If
       
        If Reporte1o2 = "2" Then
            rptF2.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            '1
            crParameterFieldDefinitions = rptF2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@CodEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '2
            crParameterFieldDefinitions = rptF2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NroMovimiento")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(rNroMovimiento)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptF2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues

            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gDesEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


            CrystalReportViewer1.ReportSource = rptF2



            ' CrystalReportViewer1.PrintReport()

            'rptF2.Refresh()

            'rptF2.PrintToPrinter(1, False, 0, 1)
        End If


        'rptF.Refresh()

        'rptF.PrintToPrinter(1, False, 0, 1)


        'Me.Hide()

    End Sub

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

End Class