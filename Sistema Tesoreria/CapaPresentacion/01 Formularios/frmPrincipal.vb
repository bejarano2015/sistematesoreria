Imports System.Windows.Forms
Imports CapaPreTesoreria
Imports System.Configuration

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmPrincipal

    Public sFlagGrabar As String = "0"
    Public sFlagEdicion As String = "0"
    Public sFlagVisualizar As String = "0"
    Public fDatos As Boolean = False
    Public fBuscar As Boolean = True
    Public fImprimir As Boolean = True
    Public fExportar As Boolean = True

    Private eUsuarios As Usuarios
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    'Dim p As Process
    '
    Private ePagoProveedores As clsPagoProveedores

    Private Property SesionDeCajaChicaToolStripMenuItemOld As Object

    Private Sub tsmiSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiSalir.Click
        'Close()
        If (MessageBox.Show("¿Esta seguro de Salir del Sistema?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            ' frmLogin.swc = False
            ' Me.Close()
            Application.Exit()
            ' KillProcess("CapaPresentacionTesoreria.exe")
        Else
            Exit Sub
        End If
    End Sub

    Private Sub frmPrincipal_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs)
        'KillProcess("CapaPresentacionTesoreria.exe")
    End Sub

    Private Sub frmPrincipal_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
        'KillProcess("CapaPresentacionTesoreria.exe")
        If CloseReason.UserClosing Then
            Application.Exit()
        End If
    End Sub

    'Private Sub frmPrincipal_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    '    'If (MessageBox.Show("¿Esta seguro de Salir del Sistema?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
    '    Close()
    '    'Else
    '    'Exit Sub
    '    'End If
    'End Sub

    'Private Sub frmPrincipal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    'Try
    '    'Dim sNomForm As String = Me.ActiveMdiChild.Name
    '    '////////////////////////////////////////////////////////////
    '    If Me.ActiveMdiChild Is Nothing Then
    '        Exit Sub
    '    End If
    '    Dim sNomForm As String = Me.ActiveMdiChild.Name
    '    '////////////////////////////////////////////////////////////

    '    If sNomForm = "frmSesionCajas" Or sNomForm = "frmConsultaRetenciones" Or sNomForm = "frmReporteValorizacionesSaldos" Or sNomForm = "frmReporteConsolidado" Or sNomForm = "frmContratistas" Or sNomForm = "frmGastosGenerales2" Or sNomForm = "frmPartidaAvanceDiario" Or sNomForm = "frmValorizacionesPagosVarios" Then
    '        Exit Sub
    '    End If

    '    If sNomForm = "frmPagoProveedores" Or sNomForm = "FrmUsuarioMenu" Or sNomForm = "frmReporteLibroBancos" Or sNomForm = "frmConciliacionBanc" Or sNomForm = "frmReporteVoucherPago" Or sNomForm = "frmReporteGasto" Or sNomForm = "frmReporteCajaChica" Or sNomForm = "frmReporteArqueo" Or sNomForm = "frmLibroBancos" Or sNomForm = "frmCajaChica" Or sNomForm = "frmEstadoCuentaProvDoc" Then
    '        Exit Sub
    '    End If

    '    If e.KeyCode = Keys.F2 Then 'Nuevo

    '        If Me.ActiveMdiChild Is Nothing Then
    '            Exit Sub
    '        End If

    '        If Me.Nuevo.Enabled = True Then

    '            If Not Me.ActiveMdiChild Is Nothing Then

    '                If sFlagEdicion = "0" Then 'Nueva Modificacion

    '                    sFlagEdicion = "1" 'Nueva Modificacion

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vNuevoRegistro()
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
    '                End If 'Nueva Modificacion
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.F3 Then 'Modifica
    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            If Me.Modifica.Enabled = True Then
    '                If sFlagEdicion = "0" Then 'Nueva Modificacion
    '                    sFlagEdicion = "1" 'Nueva Modificacion
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vModificaRegistro()
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
    '                End If 'Nueva Modificacion
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.F5 Then 'Graba
    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            If Graba.Enabled = True Then

    '                If sFlagVisualizar = "0" Then  'Ultima Modificacion
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vGrabaRegistro()
    '                End If  'Ultima Modificacion

    '                If sFlagGrabar = "1" Then
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = True
    '                    '------------------------------------->

    '                    If Me.ActiveMdiChild.Name = "frmCheque" Then
    '                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
    '                    End If

    '                    '------------------------------------->
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True

    '                    If Me.fBuscar = True Then
    '                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = True
    '                    Else
    '                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
    '                    End If

    '                    If Me.fExportar = True Then
    '                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = True
    '                    Else
    '                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
    '                    End If

    '                    If Me.fImprimir = True Then
    '                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = True
    '                    Else
    '                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
    '                    End If

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 0
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
    '                    sFlagGrabar = "0"
    '                    sFlagEdicion = "0" 'Nueva Modificacion
    '                End If
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.F9 Then 'cancela
    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            If Cancela.Enabled = True Then
    '                sFlagEdicion = "0" 'Nueva Modificacion
    '                sFlagVisualizar = "0" 'Ultima Modificacion
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = True
    '                '------------------------------------->
    '                If Me.ActiveMdiChild.Name = "frmCheque" Then
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
    '                End If
    '                '------------------------------------->
    '                If Me.fDatos = True Then
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = True
    '                Else
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
    '                End If

    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = False
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True

    '                If Me.fBuscar = True Then
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = True
    '                Else
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
    '                End If

    '                If Me.fExportar = True Then
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = True
    '                Else
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
    '                End If

    '                If Me.fImprimir = True Then
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = True
    '                Else
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
    '                End If

    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vCancelar()
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 0

    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.Delete Then 'Elimina

    '        'If sNomForm = "frmGastosGenerales2" Then
    '        '    'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmSesionCajas).Salir()
    '        '    Exit Sub
    '        'End If

    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            If Me.Elimina.Enabled = True Then
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vEliminaRegistro()
    '            End If
    '        End If

    '    End If

    '    If e.KeyCode = Keys.F4 Then 'Buscar
    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            'If sNomForm = "frmContratistas" Then
    '            '    Exit Sub
    '            '    'Exit Sub
    '            'End If
    '            If Me.Buscar.Enabled = True Then
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vBuscaRegistro()
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.F6 Then 'Exportar
    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            If Me.Exporta.Enabled = True Then
    '                'MessageBox.Show("Exportar")
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vExportaRegistro()
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.F7 Then 'Imprimir
    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            If Me.Imprime.Enabled = True Then
    '                'MessageBox.Show("Imprimir")
    '                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vImprimeRegistro()
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.F8 Then 'Visualizar
    '        If Not Me.ActiveMdiChild Is Nothing Then
    '            If Me.Visualiza.Enabled = True Then
    '                If sFlagEdicion = "0" Then 'Nueva Modificacion
    '                    sFlagEdicion = "1" 'Nueva Modificacion
    '                    sFlagVisualizar = "1" 'Ultima Modificacion
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
    '                    'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).vModificaRegistro()
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vVisualizaRegistro()
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))

    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vVisualizaRegistro()
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
    '                End If 'Nueva Modificacion
    '            End If
    '        End If
    '    End If

    '    If e.KeyCode = Keys.Escape Then 'Salir


    '        If Me.ActiveMdiChild Is Nothing Then
    '            Exit Sub
    '        End If

    '        'Dim sNomForm As String = Me.ActiveMdiChild.Name

    '        If sNomForm = "frmSesionCajas" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmSesionCajas).Salir()
    '            Exit Sub
    '        End If
    '        'If sNomForm = "frmPagoProveedores" Then
    '        '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPagoProveedores).Salir()
    '        '    Exit Sub
    '        'End If
    '        If sNomForm = "frmCajaChica" Then
    '            'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmCajaChica).Salir()
    '            Exit Sub
    '        End If
    '        'If sNomForm = "frmGastosGenerales2" Then
    '        '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmGastosGenerales2).Salir()
    '        '    Exit Sub
    '        'End If
    '        If sNomForm = "frmEstadoCuentaProvDoc" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmEstadoCuentaProvDoc).Salir()
    '            Exit Sub
    '        End If
    '        If sNomForm = "frmLibroBancos" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmLibroBancos).Salir()
    '            Exit Sub
    '        End If

    '        If sNomForm = "frmReporteArqueo" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteArqueo).Salir()
    '            Exit Sub
    '        End If

    '        If sNomForm = "frmReporteCajaChica" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteCajaChica).Salir()
    '            Exit Sub
    '        End If

    '        If sNomForm = "frmReporteGasto" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteGasto).Salir()
    '            Exit Sub
    '        End If

    '        If sNomForm = "frmReporteVoucherPago" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteVoucherPago).Salir()
    '            Exit Sub
    '        End If

    '        If sNomForm = "frmConciliacionBanc" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmConciliacionBanc).Salir()
    '            Exit Sub
    '        End If

    '        If sNomForm = "frmReporteLibroBancos" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteLibroBancos).Salir()
    '            Exit Sub
    '        End If

    '        If sNomForm = "FrmUsuarioMenu" Then
    '            CType(Me.ActiveMdiChild, CapaPreTesoreria.FrmUsuarioMenu).Salir()
    '            Exit Sub
    '        End If

    '        'FrmUsuarioMenu.vb
    '        'If sNomForm = "frmUsuario" Then
    '        '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmUsuario).Salir()
    '        '    Exit Sub
    '        'End If

    '        If (MessageBox.Show("¿Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
    '            Try
    '                sFlagVisualizar = "0"
    '                sFlagEdicion = "0"
    '                If Not Me.ActiveMdiChild Is Nothing Then
    '                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vSalir()
    '                    If Me.ActiveMdiChild Is Nothing Then
    '                        Barra.Enabled = False
    '                    End If
    '                Else
    '                    Barra.Enabled = False
    '                End If
    '            Catch ex As Exception
    '            End Try
    '        Else
    '            Exit Sub
    '        End If



    '        '+++++++++++++++++++ siempre estaba con este codigo
    '        'Try
    '        '    sFlagVisualizar = "0" 'Ultima Modificacion
    '        '    sFlagEdicion = "0" 'Nueva Modificacion
    '        '    If Not Me.ActiveMdiChild Is Nothing Then
    '        '        CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).vSalir()
    '        '        If Me.ActiveMdiChild Is Nothing Then
    '        '            Barra.Enabled = False
    '        '        End If
    '        '    Else
    '        '        Barra.Enabled = False
    '        '    End If
    '        'Catch ex As Exception

    '        'End Try
    '        '+++++++++++++++++++++++++

    '        'sFlagVisualizar = "0" 'Ultima Modificacion
    '        'sFlagEdicion = "0" 'Nueva Modificacion
    '        'If Not Me.ActiveMdiChild Is Nothing Then
    '        '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).vSalir()
    '        '    If Me.ActiveMdiChild Is Nothing Then
    '        '        Barra.Enabled = False
    '        '    End If
    '        'Else
    '        '
    '        '    Barra.Enabled = False
    '        'End If
    '    End If
    '    'Catch ex As Exception
    '    'End Try

    'End Sub

    'Private Sub frmPrincipal_Load(ByVal sender As Object, ByVal e As System.EventArgs)

    'RutaAppReportes = My.Application.Info.DirectoryPath
    'If Not RutaAppReportes.EndsWith("\") Then
    '    RutaAppReportes &= "\Reportes\"
    'End If

    ''para ver los reportes activar esto cuando se prueba con la fuente

    ''desactivar esto cuando se haga el pakete de instalacion
    ''RutaAppReportes = "D:\Sistema Tesoreria26\CapaPresentacion\Reportes\"

    ''RutaAppReportes = "D:\SISTESORERIA\Sistema Tesoreria25\CapaPresentacion\Reportes\"

    ''RutaAppReportes = "E:\SISTESORERIA\Sistema Tesoreria25\CapaPresentacion\Reportes\"

    ''RutaAppReportes = "D:\CARPETA\2010 - Camila - Dejarte De Amar\Sistema Tesoreria24\CapaPresentacion\Reportes\"

    'Dim dtTable As DataTable
    'dtTable = New DataTable
    'eMovimientoCajaBanco = New clsMovimientoCajaBanco
    'dtTable = eMovimientoCajaBanco.fListarParametrosRetencion(5)
    'If dtTable.Rows.Count > 0 Then
    '    'dgvListado.Rows(y).Cells("Column3").Value = dtTable.Rows(y).Item("IdMovimiento").ToString
    '    '--------------------->
    '    PorcentajeRetencion = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("Porcentaje").ToString) = True, "0.00", dtTable.Rows(0).Item("Porcentaje").ToString)
    '    MontoRetencion = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("ImporteParaRet").ToString) = True, "0.00", dtTable.Rows(0).Item("ImporteParaRet").ToString) 'dtTable.Rows(y).Item("FechaMovimiento").ToString
    'ElseIf dtTable.Rows.Count = 0 Then
    '    PorcentajeRetencion = "0.00"
    '    MontoRetencion = "0.00"
    'End If

    'Me.MenuStrip1.MdiWindowListItem = Me.tsmiVentana
    'Barra.Enabled = False
    'BarraSecundaria.Enabled = False
    'Dim frm As New frmLogin
    'frm.Owner = Me
    'frm.ShowInTaskbar = False
    'frm.ShowDialog()
    'If glbUsuCategoria = "U" Or glbUsuCategoria = "A" Then
    '    InhabilitaAccesoMenu(False) 'al agregar un submenu ...en este adicionar
    '    ValidandoAccesoMenu() 'al agregar un submenu ...en este adicionar
    '    'Else
    '    'InhabilitaAccesoMenu(True)
    'End If

    'toEmpresa.Text = " |Empresa:" & gEmpresa & " - " & gDesEmpresa
    'toUsuario.Text = " |Usuario:" & gUsuario 'gUsuarioCod & " - " & gUsuario
    'toPeriodo.Text = " |Periodo:" & gPeriodo
    'toPC.Text = " |PC:" & gPC
    'toFecha.Text = " |Fecha:" & Now.Date().ToShortDateString()
    'Me.Text = "    ::  Sistema de Tesorería - " & Trim(gDesEmpresa) & "  ::    "

    'End Sub

    Sub ValidandoAccesoMenu()
        'Dim ObjCli As New PlaniNegocios.Usuarios(glbNameBD)
        'Dim ObjCli As New CapaNegocios.Usuarios(glbNameBD)
        eUsuarios = New Usuarios
        Dim dtPerUsu As DataTable
        'Dim dr As DataRow
        'Dim Tab As New DataTable
        Dim Estado As Boolean
        Dim Habilita As String
        Dim i As Integer
        Try
            dtPerUsu = New DataTable
            dtPerUsu = eUsuarios.BuscaUsuario(gEmpresa, glbSisCodigo, gUsuario)
            If dtPerUsu.Rows.Count > 0 Then
                If Not IsDBNull(dtPerUsu.Rows(0).Item("Habilita")) Then
                    Habilita = Trim(dtPerUsu.Rows(0).Item("Habilita"))
                    For i = 1 To Len(Habilita)
                        Estado = IIf(Mid(Habilita, i, 1) = "1", True, False)
                        'Mantenimientos
                        If i = 1 Then
                            Me.tsmiCatalogos.Enabled = Estado 'CABECERA
                        ElseIf i = 2 Then
                            Me.tsmiCaja.Enabled = Estado
                        ElseIf i = 3 Then
                            Me.CajaToolStripMenuItem.Enabled = Estado
                        ElseIf i = 4 Then
                            Me.TipoDocumentoToolStripMenuItem.Enabled = Estado
                        ElseIf i = 5 Then
                            Me.TipoGastosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 6 Then
                            Me.EstadoArqueoToolStripMenuItem.Enabled = Estado
                        ElseIf i = 7 Then
                            Me.TipoGastosGrupo1ToolStripMenuItem.Enabled = Estado
                        ElseIf i = 8 Then
                            Me.CondigurarReportesDePagosAProvToolStripMenuItem.Enabled = Estado
                        ElseIf i = 9 Then
                            Me.tsmiCondicionPago.Enabled = Estado
                        ElseIf i = 10 Then
                            Me.tsmiBancos.Enabled = Estado
                        ElseIf i = 11 Then
                            Me.tsmiCuenta.Enabled = Estado
                        ElseIf i = 12 Then
                            Me.tsmiChequera.Enabled = Estado
                        ElseIf i = 13 Then
                            Me.tsmiCheque.Enabled = Estado
                        ElseIf i = 14 Then
                            Me.tsmiRegistroDocumento.Enabled = Estado
                        ElseIf i = 15 Then
                            Me.ITFToolStripMenuItem.Enabled = Estado
                        ElseIf i = 16 Then
                            Me.ContratistasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 17 Then
                            Me.EspecialidadToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 18 Then
                            Me.TipoContratistasToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 19 Then
                            Me.ContratistasToolStripMenuItem2.Enabled = Estado
                        ElseIf i = 20 Then
                            Me.ContratosToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 21 Then
                            Me.Cigarra1ToolStripMenuItem.Enabled = Estado
                        ElseIf i = 22 Then
                            'PROCESOS
                            Me.tsmiMoviminetos.Enabled = Estado
                        ElseIf i = 23 Then
                            Me.SesionDeCajaChicaToolStripMenuItem.Enabled = Estado
                        ElseIf i = 24 Then
                            Me.PagoAProveedoresToolStripMenuItem.Enabled = Estado
                        ElseIf i = 25 Then
                            Me.LibroBancosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 26 Then
                            Me.LibroBancosToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 27 Then
                            Me.MantenimientoLibrosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 28 Then
                            Me.ReportesToolStripMenuItem.Enabled = Estado
                        ElseIf i = 29 Then
                            Me.SaldoBancarioToolStripMenuItem.Enabled = Estado
                        ElseIf i = 30 Then
                            Me.AnexosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 31 Then
                            Me.DepPlazoDeLaEmpresaToolStripMenuItem.Enabled = Estado
                        ElseIf i = 32 Then
                            Me.ControlDeDocumentacionPorCargosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 33 Then
                            Me.CuentasPorPagarToolStripMenuItem.Enabled = Estado
                        ElseIf i = 34 Then
                            Me.ProveedoresToolStripMenuItem.Enabled = Estado
                        ElseIf i = 35 Then
                            Me.CuentaCorrienteProveedorProgramaciónToolStripMenuItem.Enabled = Estado
                        ElseIf i = 36 Then
                            Me.EstadoDeProveedoresToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 37 Then
                            Me.ContratistasToolStripMenuItem3.Enabled = Estado
                        ElseIf i = 38 Then
                            Me.HistorialDeContratosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 39 Then
                            Me.ToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 40 Then
                            Me.ToolStripMenuItem3.Enabled = Estado
                            'REPORTES
                        ElseIf i = 41 Then
                            Me.tsmiReportes.Enabled = Estado
                        ElseIf i = 42 Then
                            Me.GastosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 43 Then
                            Me.PrestamosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 44 Then
                            Me.GastosPorMovimientosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 45 Then
                            Me.GastosPorCentroDeCostoToolStripMenuItem.Enabled = Estado
                        ElseIf i = 46 Then
                            Me.PagoProveedoresToolStripMenuItem.Enabled = Estado
                        ElseIf i = 47 Then
                            Me.ReportesVariosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 48 Then
                            Me.PagosPorCategoriaToolStripMenuItem.Enabled = Estado
                        ElseIf i = 49 Then
                            Me.EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem.Enabled = Estado
                        ElseIf i = 50 Then
                            Me.AdToolStripMenuItem.Enabled = Estado
                        ElseIf i = 51 Then
                            Me.ListaDeRetencionesToolStripMenuItem.Enabled = Estado
                        ElseIf i = 52 Then
                            Me.ContabilidadToolStripMenuItem.Enabled = Estado
                        ElseIf i = 53 Then
                            Me.GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 54 Then
                            Me.PolizasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 55 Then
                            Me.PolizasPorPagarToolStripMenuItem.Enabled = Estado
                        ElseIf i = 56 Then
                            Me.DetalleDePolizasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 57 Then
                            Me.CuotasCanceladasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 58 Then
                            Me.CuotasVencidasPorVencerToolStripMenuItem.Enabled = Estado
                        ElseIf i = 59 Then
                            'CONSULTAS
                            Me.tsmiProcesos.Enabled = Estado
                        ElseIf i = 60 Then
                            Me.OperacionesRealizadasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 61 Then
                            Me.ControlAdministraciónToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 62 Then
                            Me.ControlContabilidadToolStripMenuItem.Enabled = Estado
                        ElseIf i = 63 Then
                            Me.RetencionesToolStripMenuItem.Enabled = Estado
                        ElseIf i = 64 Then
                            Me.MovimientosPorUsuarioToolStripMenuItem.Enabled = Estado
                        ElseIf i = 65 Then
                            Me.EstadoDeComprobantesToolStripMenuItem.Enabled = Estado
                        ElseIf i = 66 Then
                            Me.Cigarra2ToolStripMenuItem.Enabled = Estado
                        ElseIf i = 67 Then
                            'UTILITARIOS
                            Me.tsmiUtilitarios.Enabled = Estado
                        ElseIf i = 68 Then
                            Me.ControlDeContratosYValorizacionesToolStripMenuItem.Enabled = Estado
                        ElseIf i = 69 Then
                            Me.ParteDiarioDeContToolStripMenuItem.Enabled = Estado
                        ElseIf i = 70 Then
                            Me.ValorizacionesSemanalesToolStripMenuItem.Enabled = Estado
                        ElseIf i = 71 Then
                            Me.OperacionesVariasAdelantosFGAdendasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 72 Then
                            'CONFIGURACION
                            Me.tsmiHerramientas.Enabled = Estado
                        ElseIf i = 73 Then
                            Me.UsuariosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 74 Then
                            Me.CajasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 75 Then
                            Me.RetencionesToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 76 Then
                            Me.ParametrosToolStripMenuItem.Enabled = Estado
                        ElseIf i = 77 Then
                            Me.SeriesRetencionesToolStripMenuItem1.Enabled = Estado
                        ElseIf i = 78 Then
                            Me.ValorizacionesToolStripMenuItem.Enabled = Estado
                            'REPORTES
                        ElseIf i = 79 Then
                            Me.GastosEnviadosToolStripMenuItem.Enabled = Estado
                            'CONSULTAS
                        ElseIf i = 80 Then
                            Me.VencimientoDePolizasToolStripMenuItem.Enabled = Estado
                        ElseIf i = 81 Then
                            Me.AseguradosPorObra2ToolStripMenuItem.Enabled = Estado

                        ElseIf i = 82 Then
                            Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem.Enabled = Estado
                        ElseIf i = 83 Then
                            Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1.Enabled = Estado
                            'Procesos
                        ElseIf i = 82 Then
                            Me.DepPlazoDeLaEmpresaToolStripMenuItem.Enabled = Estado
                            'CONSULTAS
                        ElseIf i = 83 Then
                            Me.MovimientosPorCuentaToolStripMenuItem.Enabled = Estado

                        End If
                    Next
                End If
            End If
            'Tab.Dispose()
            'ds.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Tesorería")
        End Try
    End Sub

    Sub InhabilitaAccesoMenu(ByVal Estado As Boolean)
        'Mantenimientos 
        Me.tsmiCatalogos.Enabled = Estado
        Me.tsmiCaja.Enabled = Estado
        Me.CajaToolStripMenuItem.Enabled = Estado
        Me.TipoDocumentoToolStripMenuItem.Enabled = Estado
        Me.TipoGastosToolStripMenuItem.Enabled = Estado
        Me.EstadoArqueoToolStripMenuItem.Enabled = Estado
        Me.TipoGastosGrupo1ToolStripMenuItem.Enabled = Estado
        Me.CondigurarReportesDePagosAProvToolStripMenuItem.Enabled = Estado

        Me.tsmiCondicionPago.Enabled = Estado
        Me.tsmiBancos.Enabled = Estado
        Me.tsmiCuenta.Enabled = Estado
        Me.tsmiChequera.Enabled = Estado
        Me.tsmiCheque.Enabled = Estado
        Me.tsmiRegistroDocumento.Enabled = Estado
        Me.ITFToolStripMenuItem.Enabled = Estado

        Me.ContratistasToolStripMenuItem.Enabled = Estado
        Me.EspecialidadToolStripMenuItem1.Enabled = Estado
        Me.TipoContratistasToolStripMenuItem1.Enabled = Estado
        Me.ContratistasToolStripMenuItem2.Enabled = Estado
        Me.ContratosToolStripMenuItem1.Enabled = Estado

        Me.Cigarra1ToolStripMenuItem.Enabled = Estado

        'Procesos
        Me.tsmiMoviminetos.Enabled = Estado
        Me.SesionDeCajaChicaToolStripMenuItem.Enabled = Estado

        Me.PagoAProveedoresToolStripMenuItem.Enabled = Estado

        Me.LibroBancosToolStripMenuItem.Enabled = Estado
        Me.LibroBancosToolStripMenuItem1.Enabled = Estado
        Me.MantenimientoLibrosToolStripMenuItem.Enabled = Estado
        Me.ReportesToolStripMenuItem.Enabled = Estado
        Me.SaldoBancarioToolStripMenuItem.Enabled = Estado
        Me.AnexosToolStripMenuItem.Enabled = Estado
        Me.DepPlazoDeLaEmpresaToolStripMenuItem.Enabled = Estado

        Me.ControlDeDocumentacionPorCargosToolStripMenuItem.Enabled = Estado

        Me.CuentasPorPagarToolStripMenuItem.Enabled = Estado
        Me.ProveedoresToolStripMenuItem.Enabled = Estado
        Me.CuentaCorrienteProveedorProgramaciónToolStripMenuItem.Enabled = Estado
        Me.EstadoDeProveedoresToolStripMenuItem1.Enabled = Estado

        Me.ContratistasToolStripMenuItem3.Enabled = Estado

        Me.HistorialDeContratosToolStripMenuItem.Enabled = Estado
        Me.ToolStripMenuItem1.Enabled = Estado
        Me.ToolStripMenuItem3.Enabled = Estado

        'REPORTES
        Me.tsmiReportes.Enabled = Estado
        Me.GastosToolStripMenuItem.Enabled = Estado
        Me.PrestamosToolStripMenuItem.Enabled = Estado
        Me.GastosPorMovimientosToolStripMenuItem.Enabled = Estado
        Me.GastosPorCentroDeCostoToolStripMenuItem.Enabled = Estado

        Me.PagoProveedoresToolStripMenuItem.Enabled = Estado
        Me.ReportesVariosToolStripMenuItem.Enabled = Estado
        Me.PagosPorCategoriaToolStripMenuItem.Enabled = Estado
        Me.EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem.Enabled = Estado

        Me.AdToolStripMenuItem.Enabled = Estado
        Me.ListaDeRetencionesToolStripMenuItem.Enabled = Estado
        Me.ContabilidadToolStripMenuItem.Enabled = Estado
        Me.GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem.Enabled = Estado

        Me.PolizasToolStripMenuItem.Enabled = Estado
        Me.PolizasPorPagarToolStripMenuItem.Enabled = Estado
        Me.DetalleDePolizasToolStripMenuItem.Enabled = Estado
        Me.CuotasCanceladasToolStripMenuItem.Enabled = Estado
        Me.CuotasVencidasPorVencerToolStripMenuItem.Enabled = Estado
        Me.GastosEnviadosToolStripMenuItem.Enabled = Estado

        'CONSULTAS
        Me.tsmiProcesos.Enabled = Estado
        Me.OperacionesRealizadasToolStripMenuItem.Enabled = Estado
        Me.ControlAdministraciónToolStripMenuItem1.Enabled = Estado
        Me.ControlContabilidadToolStripMenuItem.Enabled = Estado
        Me.RetencionesToolStripMenuItem.Enabled = Estado
        Me.MovimientosPorUsuarioToolStripMenuItem.Enabled = Estado
        Me.EstadoDeComprobantesToolStripMenuItem.Enabled = Estado
        Me.Cigarra2ToolStripMenuItem.Enabled = Estado
        Me.VencimientoDePolizasToolStripMenuItem.Enabled = Estado
        Me.AseguradosPorObra2ToolStripMenuItem.Enabled = Estado

        'UTILITARIOS
        Me.tsmiUtilitarios.Enabled = Estado
        Me.ControlDeContratosYValorizacionesToolStripMenuItem.Enabled = Estado
        Me.ParteDiarioDeContToolStripMenuItem.Enabled = Estado
        Me.ValorizacionesSemanalesToolStripMenuItem.Enabled = Estado
        Me.OperacionesVariasAdelantosFGAdendasToolStripMenuItem.Enabled = Estado

        'CONFIGURACION
        Me.tsmiHerramientas.Enabled = Estado 'CABECERA
        Me.UsuariosToolStripMenuItem.Enabled = Estado
        Me.CajasToolStripMenuItem.Enabled = Estado
        Me.RetencionesToolStripMenuItem1.Enabled = Estado
        Me.ParametrosToolStripMenuItem.Enabled = Estado
        Me.SeriesRetencionesToolStripMenuItem1.Enabled = Estado
        Me.ValorizacionesToolStripMenuItem.Enabled = Estado

        'VENTANA
        'Me.tsmiVentana.Enabled = Estado 'CABECERA

        'AYUDA
        'Me.tsmiAyuda.Enabled = Estado 'CABECERA
    End Sub

    'Private Sub BotonesClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub

    Private Sub BotonesClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

        If Me.ActiveMdiChild.Name = "frmPagoProveedores" Or Me.ActiveMdiChild.Name = "frmReporteDocsxDia" Or Me.ActiveMdiChild.Name = "frmValorizaciones" Then
            'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Nuevo = False
            Exit Sub
        End If

        If Me.ActiveMdiChild.Name = "frmReporteValorizacionesSaldos" Or Me.ActiveMdiChild.Name = "frmReporteConsolidado" Or Me.ActiveMdiChild.Name = "frmPartidaAvanceDiario" Or Me.ActiveMdiChild.Name = "frmValorizacionesPagosVarios" Or Me.ActiveMdiChild.Name = "frmContratistas" Then
            'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Nuevo = False
            Exit Sub
        End If


        Dim btnSeleccion As ToolStripButton
        btnSeleccion = CType(sender, ToolStripButton)
        Select Case btnSeleccion.Name
            Case "Nuevo"
                If Not Me.ActiveMdiChild Is Nothing Then
                    If sFlagEdicion = "0" Then 'Nueva Modificacion
                        sFlagEdicion = "1" 'Nueva Modificacion
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vNuevoRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                    End If 'Nueva Modificacion
                End If
            Case "Modifica"
                If Not Me.ActiveMdiChild Is Nothing Then
                    If sFlagEdicion = "0" Then 'Nueva Modificacion
                        sFlagEdicion = "1" 'Nueva Modificacion
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vModificaRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
                        'CUANDO ENTRA A TabControl1.SelectedIndex = 1 SE METE A ESE ARCHIVO tabpersonalizado -_- xq?
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                    End If 'Nueva Modificacion
                End If
            Case "Visualiza"
                If Not Me.ActiveMdiChild Is Nothing Then
                    If sFlagEdicion = "0" Then 'Nueva Modificacion
                        sFlagEdicion = "1" 'Nueva Modificacion
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vVisualizaRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                    End If 'Nueva Modificacion
                End If
            Case "Graba"
                If Not Me.ActiveMdiChild Is Nothing Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vGrabaRegistro()
                    If sFlagGrabar = "1" Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = True

                        '------------------------------------->
                        If Me.ActiveMdiChild.Name = "frmCheque" Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        End If
                        '------------------------------------->

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True

                        If Me.fBuscar = True Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = True
                        Else
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        End If

                        '------------------------------------->
                        If Me.ActiveMdiChild.Name = "frmCheque" Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = True
                        End If
                        '------------------------------------->

                        If Me.fExportar = True Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = True
                        Else
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        End If

                        If Me.fImprimir = True Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = True
                        Else
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
                        End If

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 0
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        sFlagGrabar = "0"
                        sFlagEdicion = "0" 'Nueva Modificacion
                    End If

                End If
            Case "Cancela"
                If Not Me.ActiveMdiChild Is Nothing Then
                    sFlagEdicion = "0" 'Nueva Modificacion
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = True
                    '------------------------------------->
                    If Me.ActiveMdiChild.Name = "frmCheque" Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                    End If
                    '------------------------------------->
                    If Me.fDatos = True Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = True
                    Else
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                    End If

                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True

                    'If Me.fBuscar = True Then
                    '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Buscar = True
                    'Else
                    '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Buscar = False
                    'End If

                    'If Me.fExportar = True Then
                    '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Exportar = True
                    'Else
                    '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Exportar = False
                    'End If

                    'If Me.fImprimir = True Then
                    '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Imprimir = True
                    'Else
                    '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).Imprimir = False
                    'End If

                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vCancelar()
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 0
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                End If
            Case "Elimina"
                If Not Me.ActiveMdiChild Is Nothing Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vEliminaRegistro()
                End If

            Case "Buscar"
                If Not Me.ActiveMdiChild Is Nothing Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vBuscaRegistro()
                End If
            Case "Exporta"
                If Not Me.ActiveMdiChild Is Nothing Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vExportaRegistro()
                End If
            Case "Imprime"
                If Not Me.ActiveMdiChild Is Nothing Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vImprimeRegistro()
                End If
            Case "Salir"
                sFlagEdicion = "0" 'Nueva Modificacion
                If Not Me.ActiveMdiChild Is Nothing Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vSalir()
                    If Me.ActiveMdiChild Is Nothing Then
                        Barra.Enabled = False
                    End If
                Else
                    Barra.Enabled = False
                End If
        End Select
    End Sub


    Private Sub tsmiBancos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiBancos.Click
        Dim x As frmBanco = frmBanco.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub



    Private Sub tsmiCuenta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiCuenta.Click
        Dim x As frmCuentaBanco = frmCuentaBanco.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    'Private Sub tsmiCronogramaPago_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiCronogramaPago.Click
    '    Dim x As frmCronogramaPago = frmCronogramaPago.Instance
    '    x.CControl = Barra
    '    Barra.Enabled = True

    '    x.Nuevo = True
    '    x.Modificar = True
    '    x.Visualizar = True
    '    x.Eliminar = True
    '    x.Grabar = False
    '    x.Cancelar = False
    '    x.Salir = True
    '    x.Buscar = True
    '    x.Exportar = True
    '    x.Imprimir = True
    '    x.MdiParent = Me
    '    x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    x.Show()
    'End Sub

    Private Sub tsmiEstadoSesion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmEstadoSesion = frmEstadoSesion.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub ToolStripMenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub tsmiEstadoMovimiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub tsmiCondicionPago_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiCondicionPago.Click
        'Dim x As frmCondicionPago = frmCondicionPago.Instance
        'x.CControl = Barra
        'Barra.Enabled = True

        'x.Nuevo = True
        'x.Modificar = True
        'x.Eliminar = True
        'x.Grabar = False
        'x.Cancelar = False

        'x.Buscar = False
        'x.Exportar = False
        'x.Imprimir = False
        'x.Visualizar = True
        'x.Salir = True

        'x.MdiParent = Me
        'x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        'x.Show()

        Dim x As frmTipoMovimiento = frmTipoMovimiento.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    Private Sub tsmiPeriodo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmPeriodo = frmPeriodo.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))

        x.Show()
    End Sub

    Private Sub tsmiTipoMovimiento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub tsmiMovimientoCajaBanco_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiMovimientoCajaBanco.Click
    '    Dim x As frmMovimientoCajaBanco = frmMovimientoCajaBanco.Instance
    '    x.CControl = Barra
    '    Barra.Enabled = True

    '    x.Nuevo = True
    '    x.Modificar = True
    '    x.Visualizar = True
    '    x.Eliminar = True
    '    x.Grabar = False
    '    x.Cancelar = False
    '    x.Salir = True
    '    x.Buscar = True
    '    x.Exportar = True
    '    x.Imprimir = True
    '    x.MdiParent = Me
    '    x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    x.Show()
    'End Sub

    Private Sub tsmiCheque_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiCheque.Click
        Dim x As frmCheque = frmCheque.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    'Private Sub tsmiDetalleCronograma_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiDetalleCronograma.Click
    '    Dim x As frmDetalleCronograma = frmDetalleCronograma.Instance
    '    x.CControl = Barra
    '    Barra.Enabled = True

    '    x.Nuevo = True
    '    x.Modificar = True
    '    x.Visualizar = True
    '    x.Eliminar = True
    '    x.Grabar = False
    '    x.Cancelar = False
    '    x.Salir = True
    '    x.Buscar = True
    '    x.Exportar = True
    '    x.Imprimir = True
    '    x.MdiParent = Me
    '    x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    x.Show()

    'End Sub

    Private Sub tsmiArqueo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim x As frmArqueo = frmArqueo.Instance
        'x.CControl = Barra
        'Barra.Enabled = True

        'x.Nuevo = True
        'x.Modificar = True
        'x.Visualizar = True
        'x.Eliminar = True
        'x.Grabar = False
        'x.Cancelar = False
        'x.Salir = True
        'x.Buscar = True
        'x.Exportar = True
        'x.Imprimir = True
        'x.MdiParent = Me
        'x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        'x.Show()
    End Sub

    Private Sub EstadoArquepToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub tsmiChequera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiChequera.Click
        Dim x As frmChequera = frmChequera.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    'Private Sub tsmiSaldoDocumento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiSaldoDocumento.Click
    '    Dim x As frmSaldoDocumento = frmSaldoDocumento.Instance
    '    x.CControl = Barra
    '    Barra.Enabled = True

    '    x.Nuevo = True
    '    x.Modificar = True
    '    x.Visualizar = True
    '    x.Eliminar = True
    '    x.Grabar = False
    '    x.Cancelar = False
    '    x.Salir = True
    '    x.Buscar = True
    '    x.Exportar = True
    '    x.Imprimir = True
    '    x.MdiParent = Me
    '    x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    x.Show()
    'End Sub

    Private Sub tsmiRegistroDocumento_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiRegistroDocumento.Click
        Dim x As frmRegistroDocumento = frmRegistroDocumento.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    'Private Sub SesionDeCajaChicaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SesionDeCajaChicaToolStripMenuItemOld.Click
    '    Dim x As frmSesionCajas = frmSesionCajas.Instance
    '    x.MdiParent = Me
    '    x.Show()
    'End Sub

    'Private Sub SesionDeCajaChicaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SesionDeCajaChicaToolStripMenuItem.Click
    '    Dim x As frmSesionCajas = frmSesionCajas.Instance
    '    x.MdiParent = Me
    '    x.Show()
    'End Sub

    ' Private Sub SesionDeCajaChicaExtraOrdinariaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SesionDeCajaChicaExtraOrdinariaToolStripMenuItem.Click
    'Dim x As frmSesionCajasExtraordinaria = frmSesionCajasExtraordinaria.Instance
    '   x.MdiParent = Me
    '   x.Show()
    ' End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        '    'F: Transaccion

        '    Dim iResultado As Int32
        '    'Dim iDuplicado As Int32 'Modificado el 05/06
        '    Dim sCodigoActual As String = ""
        '    eTempo = New clsPlantTempo
        '    eSesionCaja = New clsCajaChica 'Movido el 05/06

        '    If sTab = 1 Then

        '        If Me.txtNumCaja.Text.Trim.Length = 0 Or Me.txtMontoFinSoles.Text.Trim.Length = 0 Or Me.dtpFechaInicio.Text.Trim.Length = 0 Or Me.txtMontoIniSoles.Text.Trim.Length = 0 Or Me.dtpFechaFin.Text.Trim.Length = 0 Or Me.txtGastos.Text.Trim.Length = 0 Or Me.txtMontoIniSoles.Text.Trim.Length = 0 Then
        '            eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
        '            frmPrincipal.sFlagGrabar = "0"
        '        Else

        '            'iDuplicado = eSesionCaja.fBuscarDoble(Convert.ToString(Me.dtpFechaInicio.Text.Trim), Convert.ToString(Me.txtCodigo.Text.Trim)) 'Modificado el 05/06
        '            'If iDuplicado > 0 Then 'Modificado el 05/06
        '            '    Exit Sub 'Modificado el 05/06
        '            'End If 'Modificado el 05/06

        '            If (MessageBox.Show("¿Esta seguro de GRABAR ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        '                'Dim sResultado As String = ""
        '                'Dim MontoActual As Decimal
        '                'sResultado = eSesionCaja.fSeleccionarTipoCambio(dtpFechaInicio.Value)
        '                'If sResultado <> "" Then

        '                frmPrincipal.sFlagGrabar = "1"
        '                If iOpcion = 1 Then '18/06/2007: Modificacion Realizada
        '                    eSesionCaja.fCodigo()
        '                    sCodigoActual = eSesionCaja.sCodFuturo
        '                Else
        '                    sCodigoActual = Me.txtCodigo.Text
        '                End If
        '                'MontoActual = Convert.ToDouble(eSesionCaja.fSeleccionarTcaVenta())
        '                'txtMontoActual.Text = MontoActual

        '                'Dim fecha As Date
        '                'fecha = Date.Now.Date
        '                'fecha = Now(dtpFechaInicio.Text)

        '                iResultado = eSesionCaja.fGrabar(sCodigoActual, Convert.ToString(Me.lblArea.Text.Trim), Convert.ToString(Me.lblEmpleado.Text.Trim), Convert.ToString(Me.lblNumCaja.Text.Trim), Convert.ToString(Me.dtpFechaInicio.Text.Trim), Convert.ToString(Me.dtpFechaInicio.Text.Trim), Convert.ToString(Me.txtMontoIniSoles.Text.Trim), Convert.ToString(Me.txtGastos.Text.Trim), Convert.ToString(Me.txtMontoFinSoles.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue), "01", iOpcion, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells(0).Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Fecha").Value, 0, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Tipo de Gasto").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Concepto de Gasto").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Tipo de Movimiento").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Centro de Costo").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Total").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Empresa").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Prestamo").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Reembolso de Caja").Value, dgvDetalleCaja.Rows(dgvDetalleCaja.CurrentRow.Index).Cells("Movimiento").Value)

        '                If iResultado > 0 Then
        '                    mMostrarGrilla()
        '                    Timer2.Enabled = True
        '                End If
        '                sTab = 0
        '                'Else
        '                '    MsgBox("Tipo de Cambio no se encuentra disponible")
        '                'End If
        '            End If

        '        End If
        '    End If

    End Sub

    Private Sub tsmiArqueos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim x As frmArqueos = frmArqueos.Instance
        'x.CControl = Barra
        'Barra.Enabled = True

        'x.Nuevo = True
        'x.Modificar = True
        'x.Visualizar = True
        'x.Eliminar = True
        'x.Grabar = False
        'x.Cancelar = False
        'x.Salir = True
        'x.Buscar = True
        'x.Exportar = True
        'x.Imprimir = True
        'x.MdiParent = Me
        'x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        'x.Show()
    End Sub


    Private Sub CajaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajaToolStripMenuItem.Click
        Dim x As frmCaja = frmCaja.Instance
        x.CControl = Barra
        Barra.Enabled = True
        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    'Private Sub CajaExtraordinariaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CajaExtraordinariaToolStripMenuItem.Click
    '    Dim x As frmCajaExtraordinaria = frmCajaExtraordinaria.Instance
    '    x.CControl = Barra
    '    Barra.Enabled = True
    '    x.Nuevo = True
    '    x.Modificar = True
    '    x.Visualizar = True
    '    x.Eliminar = True
    '    x.Grabar = False
    '    x.Cancelar = False
    '    x.Salir = True
    '    x.Buscar = True
    '    x.Exportar = True
    '    x.Imprimir = True
    '    x.MdiParent = Me
    '    x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    x.Show()
    'End Sub

    Private Sub EstadoArqueoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoArqueoToolStripMenuItem.Click
        Dim x As frmEstadoArqueo = frmEstadoArqueo.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub TipoDocumentoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoDocumentoToolStripMenuItem.Click

        Dim x As frmTipoDocumento = frmTipoDocumento.Instance
        x.CControl = Barra
        Barra.Enabled = True
        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    'Private Sub PagoAProveedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PagoAProveedoresToolStripMenuItem.Click
    '    Dim x As frmGastosGenerales = frmGastosGenerales.Instance
    '    x.CControl = Barra
    '    Barra.Enabled = True
    '    x.Nuevo = True
    '    x.Modificar = True
    '    x.Visualizar = True
    '    x.Eliminar = True
    '    x.Grabar = False
    '    x.Cancelar = False
    '    x.Salir = True
    '    x.Buscar = True
    '    x.Exportar = True
    '    x.Imprimir = True
    '    x.MdiParent = Me
    '    x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    x.Show()
    'End Sub


    'Private Sub mnuEgresoCajaBanco_Click(sender As Object, e As EventArgs) Handles mnuEgresoCajaBanco.Click
    '    'Dim x As frmGastosGenerales2 = frmGastosGenerales2.Instance
    '    'x.CControl = Barra
    '    'Barra.Enabled = True
    '    'x.Nuevo = True
    '    'x.Modificar = True
    '    'x.Visualizar = True
    '    'x.Eliminar = True
    '    'x.Grabar = False
    '    'x.Cancelar = False
    '    'x.Salir = True
    '    'x.Buscar = True
    '    'x.Exportar = True
    '    'x.Imprimir = True
    '    'x.MdiParent = Me
    '    'x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    'x.Show()

    '    Dim x As frmGastosGenerales2 = frmGastosGenerales2.Instance
    '    x.MdiParent = Me
    '    x.Show()
    'End Sub

    Private Sub TipoGastosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoGastosToolStripMenuItem.Click
        Dim x As frmTipoGasto = frmTipoGasto.Instance
        x.CControl = Barra
        Barra.Enabled = True
        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub PagoAProveedores2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmPagoProveedores = frmPagoProveedores.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub EstadoDeProveedoresToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmEstadoCuentaProvDoc = frmEstadoCuentaProvDoc.Instance
        x.MdiParent = Me
        x.Show()
    End Sub


    'Private Sub TipoDeMovimientosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoDeMovimientosToolStripMenuItem.Click
    '    Dim x As frmTipoMovimiento = frmTipoMovimiento.Instance
    '    x.CControl = Barra
    '    Barra.Enabled = True

    '    x.Nuevo = True
    '    x.Modificar = True
    '    x.Eliminar = True
    '    x.Grabar = False
    '    x.Cancelar = False

    '    x.Buscar = False
    '    x.Exportar = False
    '    x.Imprimir = False
    '    x.Visualizar = True
    '    x.Salir = True

    '    x.MdiParent = Me
    '    x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
    '    x.Show()
    'End Sub

    'Private Sub LibroYAsientosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LibroYAsientosToolStripMenuItem.Click
    '    Dim x As frmLibroBancos = frmLibroBancos.Instance
    '    x.MdiParent = Me
    '    x.Show()
    'End Sub


    Private Sub UsuariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsuariosToolStripMenuItem.Click

        Dim x As frmUsuario = frmUsuario.Instance

        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub


    Private Sub GastosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GastosToolStripMenuItem.Click
        Dim x As frmReporGastos = frmReporGastos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub PrestamosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrestamosToolStripMenuItem.Click
        Dim x As frmReporPrestamos = frmReporPrestamos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub OperacionesRealizadasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperacionesRealizadasToolStripMenuItem.Click
        Dim x As frmConsultaGastos = frmConsultaGastos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub LibroBancosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LibroBancosToolStripMenuItem1.Click
        Dim x As frmLibroBancos = frmLibroBancos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub MantenimientoLibrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MantenimientoLibrosToolStripMenuItem.Click
        Dim x As frmCabeceraLibros = frmCabeceraLibros.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    'Private Sub HistorialToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HistorialToolStripMenuItem.Click
    '    Dim x As frmLibroBancosReporte = frmLibroBancosReporte.Instance
    '    x.MdiParent = Me
    '    x.Show()
    'End Sub

    Private Sub ReporteGeneralToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ReportesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportesToolStripMenuItem.Click

    End Sub

    Private Sub SaldoBancarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaldoBancarioToolStripMenuItem.Click
        'Dim x As frmOffice = frmOffice.Instance
        'x.MdiParent = Me
        'x.Show()

        Dim x As frmOffice = frmOffice.Instance
        'x.MdiParent = Me
        x.Show()

    End Sub

    Private Sub AnexosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnexosToolStripMenuItem.Click
        Dim x As frmReporteAnexos = frmReporteAnexos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub


    Private Sub DepPlazoDeLaEmpresaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DepPlazoDeLaEmpresaToolStripMenuItem.Click
        Dim x As frmReporteAnexosDepxCuenta = frmReporteAnexosDepxCuenta.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub DocumentosEnviadosAContabilidadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmConsultaGastos2 = frmConsultaGastos2.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ControlContabilidadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmConsultaGastos3 = frmConsultaGastos3.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ControlAdministraciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ControlContabilidadToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmConsultaGastos3 = frmConsultaGastos3.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ControlAdministraciónToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ControlAdministraciónToolStripMenuItem1.Click
        Dim x As frmConsultaGastos2 = frmConsultaGastos2.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ControlContabilidadToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ControlContabilidadToolStripMenuItem.Click
        Dim x As frmConsultaGastos3 = frmConsultaGastos3.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub GastosPorMovimientosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GastosPorMovimientosToolStripMenuItem.Click
        Dim x As frmReporGastosxPago = frmReporGastosxPago.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub GastosPorCentroDeCostoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GastosPorCentroDeCostoToolStripMenuItem.Click
        Dim x As frmReporteGastosxCC = frmReporteGastosxCC.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ITFToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ITFToolStripMenuItem.Click
        Dim x As frmItf = frmItf.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    Private Sub CuentasPorPagarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmCuentasxPagar = frmCuentasxPagar.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub MIKIToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim x As FrmCompConConversion = FrmCompConConversion.Instance
        'x.MdiParent = Me
        FrmCompConConversion.Show()
    End Sub

    Private Sub TipoGastosGrupo1ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoGastosGrupo1ToolStripMenuItem.Click
        Dim x As frmTipoGastosGrup1 = frmTipoGastosGrup1.Instance
        x.CControl = Barra
        Barra.Enabled = True
        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub CajasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CajasToolStripMenuItem.Click
        Dim x As frmCajasChicasAdm = frmCajasChicasAdm.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub CondigurarReportesDePagosAProvToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CondigurarReportesDePagosAProvToolStripMenuItem.Click
        Dim x As frmGrupoxCCosto = frmGrupoxCCosto.Instance
        x.CControl = Barra
        Barra.Enabled = True
        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub ReportesVariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReportesVariosToolStripMenuItem.Click
        Dim x As frmReportePagosProv = frmReportePagosProv.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub PagosPorCategoriaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PagosPorCategoriaToolStripMenuItem.Click
        Dim x As frmReportePagoProvxCheq = frmReportePagoProvxCheq.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub AdToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdToolStripMenuItem.Click
        'Dim x As frmReporteAdministracion = frmReporteAdministracion.Instance
        'x.MdiParent = Me
        'x.Show()

        'Dim x As frmReporteAdministracion2 = frmReporteAdministracion2.Instance
        ''x.MdiParent = Me
        'x.Show()

        Dim x As frmReporteAdministracion2 = frmReporteAdministracion2.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ContratistasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmContratistasMantenimiento = frmContratistasMantenimiento.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    Private Sub PartidasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmPartidas = frmPartidas.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    Private Sub ContratistasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmContratistasMantenimiento = frmContratistasMantenimiento.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub PartidasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmPartidas = frmPartidas.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub EspecialidadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmEspecialidad = frmEspecialidad.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub TipoContratistasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmTipoContratista = frmTipoContratista.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub ValorizacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub PagoDeContratistasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmContratistas = frmContratistas.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ControlDeDocumentacionPorCargosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ControlDeDocumentacionPorCargosToolStripMenuItem.Click
        Dim x As frmControlDocCargos = frmControlDocCargos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    'Private Sub ValorizacionesPorPagarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ValorizacionesPorPagarToolStripMenuItem.Click
    '    Dim x As frmValorizacionesEstado = frmValorizacionesEstado.Instance
    '    x.MdiParent = Me
    '    x.Show()
    'End Sub

    Private Sub RetencionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RetencionesToolStripMenuItem.Click
        Dim x As frmConsultaRetenciones = frmConsultaRetenciones.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub SeriesRetencionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim x As frmSerieRetenciones = frmSerieRetenciones.Instance
        'x.MdiParent = Me
        'x.Show()
    End Sub

    Private Sub SeriesRetencionesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeriesRetencionesToolStripMenuItem1.Click

        Dim x As frmSerieRetenciones = frmSerieRetenciones.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub ParametrosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ParametrosToolStripMenuItem.Click
        Dim x As frmParametrosRetencion = frmParametrosRetencion.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem.Click
        Dim x As frmReporteEstadoDocumentosCCosto = frmReporteEstadoDocumentosCCosto.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ListaDeRetencionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListaDeRetencionesToolStripMenuItem.Click
        Dim x As frmReporteRetencionesxFecha = frmReporteRetencionesxFecha.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ReportesValorizacionesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub MovimientosPorUsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MovimientosPorUsuarioToolStripMenuItem.Click
        Dim x As frmConsultaRegistrosxUser = frmConsultaRegistrosxUser.Instance
        x.MdiParent = Me
        x.Show()

    End Sub

    Private Sub Administración2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmReporteAdministracion2 = frmReporteAdministracion2.Instance
        'x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem.Click
        Dim x As frmReporGastosxPagoyCC = frmReporGastosxPagoyCC.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub EspecialidadToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EspecialidadToolStripMenuItem1.Click
        Dim x As frmEspecialidad = frmEspecialidad.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()

    End Sub

    Private Sub TipoContratistasToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoContratistasToolStripMenuItem1.Click
        Dim x As frmTipoContratista = frmTipoContratista.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub ContratistasToolStripMenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratistasToolStripMenuItem2.Click
        Dim x As frmContratistasMantenimiento = frmContratistasMantenimiento.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub ContratosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CuentaCorrienteProveedorProgramaciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CuentaCorrienteProveedorProgramaciónToolStripMenuItem.Click
        Dim x As frmPagoProveedores = frmPagoProveedores.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub EstadoDeProveedoresToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoDeProveedoresToolStripMenuItem1.Click
        Dim x As frmEstadoCuentaProvDoc = frmEstadoCuentaProvDoc.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub PagosVariosYAdendasDeContratosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmValorizacionesPagosVarios = frmValorizacionesPagosVarios.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub AvanceDiarioDeContratosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmPartidaAvanceDiario = frmPartidaAvanceDiario.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ValorizacionesToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim x As frmValorizaciones = frmValorizaciones.Instance
        'x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub HistorialDeContratosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HistorialDeContratosToolStripMenuItem.Click
        Dim x As frmContratistas = frmContratistas.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        Dim x As frmReporteConsolidado = frmReporteConsolidado.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ToolStripMenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem3.Click
        Dim x As frmReporteValorizacionesSaldos = frmReporteValorizacionesSaldos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub Cigarra1ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cigarra1ToolStripMenuItem.Click
        Dim x As frmMantePoliza = frmMantePoliza.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub Cigarra2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cigarra2ToolStripMenuItem.Click

    End Sub

    Private Sub ValorizacionesToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ValorizacionesToolStripMenuItem.Click
        Dim x As frmContratistasControlValorizaciones = frmContratistasControlValorizaciones.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub EstadoDeComprobantesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EstadoDeComprobantesToolStripMenuItem.Click
        Dim x As frmRegistroDocumentoEstado = frmRegistroDocumentoEstado.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub PolizasPorPagarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PolizasPorPagarToolStripMenuItem.Click
        Dim x As FrmREP_PolizasXPagar = FrmREP_PolizasXPagar.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub DetalleDePolizasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DetalleDePolizasToolStripMenuItem.Click
        Dim x As frmREP_Poliza_Detalle = frmREP_Poliza_Detalle.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub CuotasCanceladasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CuotasCanceladasToolStripMenuItem.Click
        Dim x As FrmPolizasCuotas_Canceladas = FrmPolizasCuotas_Canceladas.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub CuotasVencidasPorVencerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CuotasVencidasPorVencerToolStripMenuItem.Click
        Dim x As frmCuotasVencidas_xVencer = frmCuotasVencidas_xVencer.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ContratosToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratosToolStripMenuItem1.Click
        Dim x As frmPartidas = frmPartidas.Instance
        x.CControl = Barra
        Barra.Enabled = True

        x.Nuevo = True
        x.Modificar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False

        x.Buscar = False
        x.Exportar = False
        x.Imprimir = False
        x.Visualizar = True
        x.Salir = True

        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub OperacionesVariasAdelantosFGAdendasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperacionesVariasAdelantosFGAdendasToolStripMenuItem.Click
        Dim x As frmValorizacionesPagosVarios = frmValorizacionesPagosVarios.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ParteDiarioDeContToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ParteDiarioDeContToolStripMenuItem.Click
        Dim x As frmPartidaAvanceDiario = frmPartidaAvanceDiario.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ValorizacionesSemanalesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ValorizacionesSemanalesToolStripMenuItem.Click
        Dim x As frmValorizaciones = frmValorizaciones.Instance
        'x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub XxxToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'frmLibroBancosReporte()
        Dim x As frmReporteGastosEnviados = frmReporteGastosEnviados.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub FormaDePagoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub GastosEnviadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GastosEnviadosToolStripMenuItem.Click
        Dim x As frmReporteGastosEnviados = frmReporteGastosEnviados.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub VencimientoDePolizasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VencimientoDePolizasToolStripMenuItem.Click
        Dim x As frmConsultaPolizasCuotas = frmConsultaPolizasCuotas.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    'Private Sub AseguradosPorObraToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim x As frmConsultaAsegurados_x_Obra = frmConsultaAsegurados_x_Obra.Instance
    '    x.MdiParent = Me
    '    x.Show()
    'End Sub

    Private Sub AseguradosPorObra2ToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AseguradosPorObra2ToolStripMenuItem.Click
        Dim x As frmConsultaAsegurados_x_Obra2 = frmConsultaAsegurados_x_Obra2.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ConsultaDeComprobantesDeCajaChicaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeComprobantesDeCajaChicaToolStripMenuItem.Click
        Dim x As frmCajaChicaConsultas = frmCajaChicaConsultas.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1.Click
        Dim x As frmCajaChicaConsultas2 = frmCajaChicaConsultas2.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub frmPrincipal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        'Private Sub frmPrincipal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        'Try
        'Dim sNomForm As String = Me.ActiveMdiChild.Name
        '////////////////////////////////////////////////////////////
        If Me.ActiveMdiChild Is Nothing Then
            Exit Sub
        End If
        Dim sNomForm As String = Me.ActiveMdiChild.Name
        '////////////////////////////////////////////////////////////
        If sNomForm = "frmSesionCajas" Or sNomForm = "frmConsultaRetenciones" Or sNomForm = "frmReporteValorizacionesSaldos" Or sNomForm = "frmReporteConsolidado" Or sNomForm = "frmContratistas" Or sNomForm = "frmGastosGenerales2" Or sNomForm = "frmPartidaAvanceDiario" Or sNomForm = "frmValorizacionesPagosVarios" Then
            Exit Sub
        End If
        If sNomForm = "frmPagoProveedores" Or sNomForm = "FrmUsuarioMenu" Or sNomForm = "frmReporteLibroBancos" Or sNomForm = "frmConciliacionBanc" Or sNomForm = "frmReporteVoucherPago" Or sNomForm = "frmReporteGasto" Or sNomForm = "frmReporteCajaChica" Or sNomForm = "frmReporteArqueo" Or sNomForm = "frmLibroBancos" Or sNomForm = "frmCajaChica" Or sNomForm = "frmEstadoCuentaProvDoc" Then
            Exit Sub
        End If
        If e.KeyCode = Keys.F2 Then 'Nuevo
            If Me.ActiveMdiChild Is Nothing Then
                Exit Sub
            End If
            If Me.Nuevo.Enabled = True Then
                If Not Me.ActiveMdiChild Is Nothing Then
                    If sFlagEdicion = "0" Then 'Nueva Modificacion
                        sFlagEdicion = "1" 'Nueva Modificacion
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vNuevoRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
                    End If 'Nueva Modificacion
                End If
            End If
        End If

        If e.KeyCode = Keys.F3 Then 'Modifica
            If Not Me.ActiveMdiChild Is Nothing Then
                If Me.Modifica.Enabled = True Then
                    If sFlagEdicion = "0" Then 'Nueva Modificacion
                        sFlagEdicion = "1" 'Nueva Modificacion
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vModificaRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
                    End If 'Nueva Modificacion
                End If
            End If
        End If

        If e.KeyCode = Keys.F5 Then 'Graba
            If Not Me.ActiveMdiChild Is Nothing Then
                If Graba.Enabled = True Then

                    If sFlagVisualizar = "0" Then  'Ultima Modificacion
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vGrabaRegistro()
                    End If  'Ultima Modificacion

                    If sFlagGrabar = "1" Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = True
                        '------------------------------------->

                        If Me.ActiveMdiChild.Name = "frmCheque" Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        End If

                        '------------------------------------->
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True

                        If Me.fBuscar = True Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = True
                        Else
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        End If

                        If Me.fExportar = True Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = True
                        Else
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        End If

                        If Me.fImprimir = True Then
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = True
                        Else
                            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
                        End If

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 0
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        sFlagGrabar = "0"
                        sFlagEdicion = "0" 'Nueva Modificacion
                    End If
                End If
            End If
        End If

        If e.KeyCode = Keys.F9 Then 'cancela
            If Not Me.ActiveMdiChild Is Nothing Then
                If Cancela.Enabled = True Then
                    sFlagEdicion = "0" 'Nueva Modificacion
                    sFlagVisualizar = "0" 'Ultima Modificacion
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = True
                    '------------------------------------->
                    If Me.ActiveMdiChild.Name = "frmCheque" Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                    End If
                    '------------------------------------->
                    If Me.fDatos = True Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = True
                    Else
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                    End If

                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = False
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True

                    If Me.fBuscar = True Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = True
                    Else
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                    End If

                    If Me.fExportar = True Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = True
                    Else
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                    End If

                    If Me.fImprimir = True Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = True
                    Else
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False
                    End If

                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vCancelar()
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 0

                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                End If
            End If
        End If

        If e.KeyCode = Keys.Delete Then 'Elimina

            'If sNomForm = "frmGastosGenerales2" Then
            '    'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmSesionCajas).Salir()
            '    Exit Sub
            'End If

            If Not Me.ActiveMdiChild Is Nothing Then
                If Me.Elimina.Enabled = True Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vEliminaRegistro()
                End If
            End If

        End If

        If e.KeyCode = Keys.F4 Then 'Buscar
            If Not Me.ActiveMdiChild Is Nothing Then
                'If sNomForm = "frmContratistas" Then
                '    Exit Sub
                '    'Exit Sub
                'End If
                If Me.Buscar.Enabled = True Then
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vBuscaRegistro()
                End If
            End If
        End If

        If e.KeyCode = Keys.F6 Then 'Exportar
            If Not Me.ActiveMdiChild Is Nothing Then
                If Me.Exporta.Enabled = True Then
                    'MessageBox.Show("Exportar")
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vExportaRegistro()
                End If
            End If
        End If

        If e.KeyCode = Keys.F7 Then 'Imprimir
            If Not Me.ActiveMdiChild Is Nothing Then
                If Me.Imprime.Enabled = True Then
                    'MessageBox.Show("Imprimir")
                    CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vImprimeRegistro()
                End If
            End If
        End If

        If e.KeyCode = Keys.F8 Then 'Visualizar
            If Not Me.ActiveMdiChild Is Nothing Then
                If Me.Visualiza.Enabled = True Then
                    If sFlagEdicion = "0" Then 'Nueva Modificacion
                        sFlagEdicion = "1" 'Nueva Modificacion
                        sFlagVisualizar = "1" 'Ultima Modificacion
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).CControl = Barra
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Nuevo = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Modificar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Visualizar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Eliminar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Grabar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Salir = True
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Cancelar = True

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Buscar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Exportar = False
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Imprimir = False

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).Activo(Barra)
                        'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).vModificaRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vVisualizaRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.DisablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(0))
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.EnablePage(CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.TabPages(1))

                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vVisualizaRegistro()
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).TabControl1.SelectedIndex = 1
                    End If 'Nueva Modificacion
                End If
            End If
        End If

        If e.KeyCode = Keys.Escape Then 'Salir


            If Me.ActiveMdiChild Is Nothing Then
                Exit Sub
            End If

            'Dim sNomForm As String = Me.ActiveMdiChild.Name

            If sNomForm = "frmSesionCajas" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmSesionCajas).Salir()
                Exit Sub
            End If
            'If sNomForm = "frmPagoProveedores" Then
            '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPagoProveedores).Salir()
            '    Exit Sub
            'End If
            If sNomForm = "frmCajaChica" Then
                'CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmCajaChica).Salir()
                Exit Sub
            End If
            'If sNomForm = "frmGastosGenerales2" Then
            '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmGastosGenerales2).Salir()
            '    Exit Sub
            'End If
            If sNomForm = "frmEstadoCuentaProvDoc" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmEstadoCuentaProvDoc).Salir()
                Exit Sub
            End If
            If sNomForm = "frmLibroBancos" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmLibroBancos).Salir()
                Exit Sub
            End If

            If sNomForm = "frmReporteArqueo" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteArqueo).Salir()
                Exit Sub
            End If

            If sNomForm = "frmReporteCajaChica" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteCajaChica).Salir()
                Exit Sub
            End If

            If sNomForm = "frmReporteGasto" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteGasto).Salir()
                Exit Sub
            End If

            If sNomForm = "frmReporteVoucherPago" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteVoucherPago).Salir()
                Exit Sub
            End If

            If sNomForm = "frmConciliacionBanc" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmConciliacionBanc).Salir()
                Exit Sub
            End If

            If sNomForm = "frmReporteLibroBancos" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.frmReporteLibroBancos).Salir()
                Exit Sub
            End If

            If sNomForm = "FrmUsuarioMenu" Then
                CType(Me.ActiveMdiChild, CapaPreTesoreria.FrmUsuarioMenu).Salir()
                Exit Sub
            End If

            'FrmUsuarioMenu.vb
            'If sNomForm = "frmUsuario" Then
            '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmUsuario).Salir()
            '    Exit Sub
            'End If

            If (MessageBox.Show("¿Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                Try
                    sFlagVisualizar = "0"
                    sFlagEdicion = "0"
                    If Not Me.ActiveMdiChild Is Nothing Then
                        CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vSalir()
                        If Me.ActiveMdiChild Is Nothing Then
                            Barra.Enabled = False
                        End If
                    Else
                        Barra.Enabled = False
                    End If
                Catch ex As Exception
                End Try
            Else
                Exit Sub
            End If



            '+++++++++++++++++++ siempre estaba con este codigo
            'Try
            '    sFlagVisualizar = "0" 'Ultima Modificacion
            '    sFlagEdicion = "0" 'Nueva Modificacion
            '    If Not Me.ActiveMdiChild Is Nothing Then
            '        CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).vSalir()
            '        If Me.ActiveMdiChild Is Nothing Then
            '            Barra.Enabled = False
            '        End If
            '    Else
            '        Barra.Enabled = False
            '    End If
            'Catch ex As Exception

            'End Try
            '+++++++++++++++++++++++++

            'sFlagVisualizar = "0" 'Ultima Modificacion
            'sFlagEdicion = "0" 'Nueva Modificacion
            'If Not Me.ActiveMdiChild Is Nothing Then
            '    CType(Me.ActiveMdiChild, CapaPresentacionTesoreria.frmPadre00).vSalir()
            '    If Me.ActiveMdiChild Is Nothing Then
            '        Barra.Enabled = False
            '    End If
            'Else
            '
            '    Barra.Enabled = False
            'End If
        End If
        'Catch ex As Exception
        'End Try


    End Sub


    'Private Sub frmPrincipal_Load_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    'RutaAppReportes = My.Application.Info.DirectoryPath
    'If Not RutaAppReportes.EndsWith("\") Then
    '    RutaAppReportes &= "\Reportes\"
    'End If

    ''para ver los reportes activar esto cuando se prueba con la fuente

    ''desactivar esto cuando se haga el pakete de instalacion
    ''RutaAppReportes = "D:\Sistema Tesoreria26\CapaPresentacion\Reportes\"

    ''RutaAppReportes = "D:\SISTESORERIA\Sistema Tesoreria25\CapaPresentacion\Reportes\"

    ''RutaAppReportes = "E:\SISTESORERIA\Sistema Tesoreria25\CapaPresentacion\Reportes\"

    ''RutaAppReportes = "D:\CARPETA\2010 - Camila - Dejarte De Amar\Sistema Tesoreria24\CapaPresentacion\Reportes\"

    'Dim dtTable As DataTable
    'dtTable = New DataTable
    'eMovimientoCajaBanco = New clsMovimientoCajaBanco
    'dtTable = eMovimientoCajaBanco.fListarParametrosRetencion(5)
    'If dtTable.Rows.Count > 0 Then
    '    'dgvListado.Rows(y).Cells("Column3").Value = dtTable.Rows(y).Item("IdMovimiento").ToString
    '    '--------------------->
    '    PorcentajeRetencion = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("Porcentaje").ToString) = True, "0.00", dtTable.Rows(0).Item("Porcentaje").ToString)
    '    MontoRetencion = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("ImporteParaRet").ToString) = True, "0.00", dtTable.Rows(0).Item("ImporteParaRet").ToString) 'dtTable.Rows(y).Item("FechaMovimiento").ToString
    'ElseIf dtTable.Rows.Count = 0 Then
    '    PorcentajeRetencion = "0.00"
    '    MontoRetencion = "0.00"
    'End If

    'Me.MenuStrip1.MdiWindowListItem = Me.tsmiVentana
    'Barra.Enabled = False
    'BarraSecundaria.Enabled = False
    'Dim frm As New frmLogin
    'frm.Owner = Me
    'frm.ShowInTaskbar = False
    'frm.ShowDialog()
    'If glbUsuCategoria = "U" Or glbUsuCategoria = "A" Then
    '    InhabilitaAccesoMenu(False) 'al agregar un submenu ...en este adicionar
    '    ValidandoAccesoMenu() 'al agregar un submenu ...en este adicionar
    '    'Else
    '    'InhabilitaAccesoMenu(True)
    'End If

    'toEmpresa.Text = " |Empresa:" & gEmpresa & " - " & gDesEmpresa
    'toUsuario.Text = " |Usuario:" & gUsuario 'gUsuarioCod & " - " & gUsuario
    'toPeriodo.Text = " |Periodo:" & gPeriodo
    'toPC.Text = " |PC:" & gPC
    'toFecha.Text = " |Fecha:" & Now.Date().ToShortDateString()
    'Me.Text = "    ::  Sistema de Tesorería - " & Trim(gDesEmpresa) & "  ::    "

    'End Sub


    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        EstadoServicioruc = VerRuc("20502574109").blnExiste

        RutaAppReportes = My.Application.Info.DirectoryPath
        If Not RutaAppReportes.EndsWith("\") Then
            RutaAppReportes &= "\Reportes\"
        End If

        Dim dtTable As DataTable
        dtTable = New DataTable
        eMovimientoCajaBanco = New clsMovimientoCajaBanco
        dtTable = eMovimientoCajaBanco.fListarParametrosRetencion(5)
        If dtTable.Rows.Count > 0 Then
            'dgvListado.Rows(y).Cells("Column3").Value = dtTable.Rows(y).Item("IdMovimiento").ToString
            '--------------------->
            PorcentajeRetencion = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("Porcentaje").ToString) = True, "0.00", dtTable.Rows(0).Item("Porcentaje").ToString)
            MontoRetencion = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("ImporteParaRet").ToString) = True, "0.00", dtTable.Rows(0).Item("ImporteParaRet").ToString) 'dtTable.Rows(y).Item("FechaMovimiento").ToString
        ElseIf dtTable.Rows.Count = 0 Then
            PorcentajeRetencion = "0.00"
            MontoRetencion = "0.00"
        End If

        Me.MenuStrip1.MdiWindowListItem = Me.tsmiVentana
        Barra.Enabled = False
        BarraSecundaria.Enabled = False
        Dim frm As New frmLogin
        frm.Owner = Me
        frm.ShowInTaskbar = False
        frm.ShowDialog()
        If glbUsuCategoria = "U" Or glbUsuCategoria = "A" Then
            InhabilitaAccesoMenu(False) 'al agregar un submenu ...en este adicionar
            ValidandoAccesoMenu() 'al agregar un submenu ...en este adicionar
            'Else
            'InhabilitaAccesoMenu(True)
        End If

        toEmpresa.Text = " |Empresa:" & gEmpresa & " - " & gDesEmpresa
        toUsuario.Text = " |Usuario:" & gUsuario 'gUsuarioCod & " - " & gUsuario
        toPeriodo.Text = " |Periodo:" & gPeriodo
        toPC.Text = " |PC:" & gPC
        toFecha.Text = " |Fecha:" & Now.Date().ToShortDateString()
        Me.Text = "    ::  Sistema de Tesorería - " & Trim(gDesEmpresa) & "  ::    "
        toVersion.Text = versionSistema
    End Sub


    Private Sub Nuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Nuevo.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Modifica_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Modifica.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Elimina_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Elimina.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Graba_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Graba.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Cancela_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancela.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Buscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Buscar.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Exporta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Exporta.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Imprime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Imprime.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Visualiza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Visualiza.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub Salir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Salir.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub btnModificar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
        BotonesClick(sender, e)
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        BotonesClick(sender, e)
    End Sub


    Private Sub VariosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles VariosToolStripMenuItem.Click
        Dim x As frmLibroBancosIngresos = frmLibroBancosIngresos.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub MovimientosPorCuentaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MovimientosPorCuentaToolStripMenuItem.Click
        Dim x As frmLibroBancosConsuslta = frmLibroBancosConsuslta.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    'Private Sub mnuIngresoBancario_Click(sender As Object, e As EventArgs) Handles mnuIngresoBancario.Click
    '    Dim x As New frmIngresoBancarioListado()
    '    x = frmIngresoBancarioListado.GetContainerControl()
    '    x.MdiParent = Me
    '    x.Show()
    '    x.Focus()
    'End Sub
    Private Sub ProgramaciónCuentasARendirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProgramaciónCuentasARendirToolStripMenuItem.Click
        Dim x As frmEntregaRendir = frmEntregaRendir.Instance

        x.MdiParent = Me
        x.Show()




    End Sub


    Private Sub mnuCajaExtraordinaria_Click(sender As Object, e As EventArgs) Handles mnuCajaExtraordinaria.Click
        Dim x As frmCajaExtraordinaria = frmCajaExtraordinaria.Instance
        x.CControl = Barra
        Barra.Enabled = True
        x.Nuevo = True
        x.Modificar = True
        x.Visualizar = True
        x.Eliminar = True
        x.Grabar = False
        x.Cancelar = False
        x.Salir = True
        x.Buscar = True
        x.Exportar = True
        x.Imprimir = True
        x.MdiParent = Me
        x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
        x.Show()
    End Sub

    Private Sub CajaChicaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles mnuSesionCajaChica.Click
        Dim x As frmSesionCajas = frmSesionCajas.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub mnuSesionCajaExtraordinaria_Click(sender As Object, e As EventArgs) Handles mnuSesionCajaExtraordinaria.Click
        Dim x As frmSesionCajasExtraordinaria = frmSesionCajasExtraordinaria.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub mnuEgresoCajaBancos_Click(sender As Object, e As EventArgs) Handles mnuEgresoCajaBancos.Click
        Dim x As frmGastosGenerales2 = frmGastosGenerales2.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub mnuIngresoBancario_Click(sender As Object, e As EventArgs) Handles mnuIngresoBancario.Click
        Dim x As New frmIngresoBancarioListado()
        x = frmIngresoBancarioListado.GetContainerControl
    End Sub

    Private Sub RetencionesToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles RetencionesToolStripMenuItem2.Click
        Dim x As New frmRegistroRetencion()
        x = frmRegistroRetencion.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub


    Private Sub DetraccionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DetraccionesToolStripMenuItem.Click
        Dim x As New frmRegistroDetraccion()
        x = frmRegistroDetraccion.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub

    Private Sub GestionDeDocumentosPendientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestionDeDocumentosPendientesToolStripMenuItem.Click
        Dim x As frmDerivarPagosOrdenCompra = frmDerivarPagosOrdenCompra.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ProgramaciónDePagosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProgramaciónDePagosToolStripMenuItem.Click
        Dim x As New frmProgramacionPago()
        x = frmProgramacionPago.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub

    Private Sub GestionDocumentariaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestionDocumentariaToolStripMenuItem.Click
        Dim x As New WFGestionDocumento()
        x = WFGestionDocumento.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub

#Region "MENU PRINCIPALES"
    Private Sub DocumentoDeVentaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles DocumentoDeVentaToolStripMenuItem1.Click
        Dim documentos As New FrmDocumentosSunat()
        documentos = FrmDocumentosSunat.GetContainerControl()
        documentos.MdiParent = Me
        documentos.Show()
        documentos.Focus()
    End Sub
    Private Sub msImpuesto_Click(sender As Object, e As EventArgs) Handles msImpuesto.Click
        Dim impuesto As New frmImpuestos()
        impuesto = frmImpuestos.GetContainerControl()
        impuesto.MdiParent = Me
        impuesto.Show()
        impuesto.Focus()
    End Sub


    Private Sub ItemsDeVentaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ItemsDeVentaToolStripMenuItem.Click
        Dim item As New frmItemVentas()
        item = frmItemVentas.GetContainerControl()
        item.MdiParent = Me
        item.Show()
        item.Focus()
    End Sub

    Private Sub VentasToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles VentasToolStripMenuItem1.Click
        Dim ventas As New frmVentas()
        ventas = frmVentas.GetContainerControl()
        ventas.MdiParent = Me
        ventas.Show()
        ventas.Focus()
    End Sub

    Private Sub MantenimientoNumeradorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MantenimientoNumeradorToolStripMenuItem.Click
        Dim numerador As New FrmManteNumerador()
        numerador = FrmManteNumerador.GetContainerControl()
        numerador.MdiParent = Me
        numerador.Show()
        numerador.Focus()
    End Sub

    Private Sub CobranzasToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CobranzasToolStripMenuItem1.Click
        Dim cobranza As New frmCobranza()
        cobranza = frmCobranza.GetContainerControl()
        cobranza.MdiParent = Me
        cobranza.Show()
        cobranza.Focus()
    End Sub

    Private Sub PagosPlanillasToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PagosPlanillasToolStripMenuItem.Click
        Dim x As New FrmConceptosPlanillas()
        x = FrmConceptosPlanillas.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub
#End Region



    Private Sub RegistroDeRendicionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RegistroDeRendicionesToolStripMenuItem.Click
        Dim x As New frmRegistroRendicion()
        x = frmRegistroRendicion.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub

    Private Sub DocumentosPendieToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DocumentosPendieToolStripMenuItem.Click
        Dim x As frmReporGestionDocumentoPagosTodo = frmReporGestionDocumentoPagosTodo.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub MigrarDataToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MigrarDataToolStripMenuItem.Click
        Dim x As New FrmMigrarDataBoletas()
        x = FrmMigrarDataBoletas.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub

    Private Sub ClientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClientesToolStripMenuItem.Click
        Dim x As New FrmClientes()
        x = FrmClientes.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub

    Private Sub GestiónDeDocumentosPendientesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestiónDeDocumentosPendientesToolStripMenuItem.Click
        Dim x As frmReporGestionDocumentoPagosTodo = frmReporGestionDocumentoPagosTodo.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub ProgramaciónDePagosToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ProgramaciónDePagosToolStripMenuItem1.Click

        Dim x As frmReporProgramacionPago = frmReporProgramacionPago.Instance
        x.MdiParent = Me
        x.Show()
    End Sub

    Private Sub VistoBuenoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VistoBuenoToolStripMenuItem.Click

        Dim x As frmVistoBuenoUsuario = frmVistoBuenoUsuario.Instance
        x.MdiParent = Me
        x.Show()

    End Sub

    Private Sub PercepciónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PercepciónToolStripMenuItem.Click
        Dim x As New frmRegistroPercepcion()
        x = frmRegistroPercepcion.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub

    Private Sub GestionValorizacionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GestionValorizacionesToolStripMenuItem.Click
        Dim x As New frmGestionarValorizacion()
        x = frmGestionarValorizacion.GetContainerControl()
        x.MdiParent = Me
        x.Show()
        x.Focus()
    End Sub
End Class