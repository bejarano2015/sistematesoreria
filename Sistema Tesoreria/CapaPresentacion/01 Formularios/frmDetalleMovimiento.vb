Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Windows.Forms

Public Class frmDetalleMovimiento

    Dim dtTable As DataTable

    Public sIdMovimiento As String = ""
    Public sFormaDetalle As String = ""
    Public sEmpresaAPrestar As String = ""

    Public Monto As Double = 0
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Private eGastosGenerales As clsGastosGenerales
    Dim Importe As Double = 0

    Private Sub frmDetalleMovimiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblMonto.Text = Format(Monto, "#,##0.00") 
        lblMonto2.Text = Format(Monto, "0.00")
        txtTotal.Text = Format(Monto, "#,##0.00")
        If sIdMovimiento <> "" Then
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            dtTable = New DataTable
            dtTable = eMovimientoCajaBanco.fListarDetalle(sIdMovimiento, gEmpresa)
            If dtTable.Rows.Count > 0 Then
                For y As Integer = 0 To dtTable.Rows.Count - 1
                    dgvDetalle.Rows.Add()
                    For z As Integer = 0 To dtTable.Columns.Count - 1
                        If z = 4 Then
                            If dtTable.Rows(y).Item(4).ToString = 1 Then
                                dgvDetalle.Rows(y).Cells(z).Value = True
                            ElseIf dtTable.Rows(y).Item(4).ToString = 0 Then
                                dgvDetalle.Rows(y).Cells(z).Value = False
                            End If
                        ElseIf z = 7 Then
                            dgvDetalle.Rows(y).Cells(8).Value = dtTable.Rows(y).Item(z)
                        Else
                            dgvDetalle.Rows(y).Cells(z).Value = dtTable.Rows(y).Item(z)
                        End If
                        dgvDetalle.Rows(y).Cells(7).Value = "1"
                    Next
                Next
            End If
        End If
    End Sub

    Private Sub dgvDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        If dgvDetalle.Rows.Count > 0 Then
            Select Case e.KeyCode
                Case Keys.Delete
                    If dgvDetalle.Rows.Count > 0 Then
                        Dim CodigoIdDetalle As String = ""
                        If MessageBox.Show("�Desea Eliminar el Item?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            CodigoIdDetalle = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(8).Value
                            If CodigoIdDetalle <> "" Then
                                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                eMovimientoCajaBanco.fEliminarItemDetalle(Trim(CodigoIdDetalle), gEmpresa)
                                dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                            Else
                                dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                            End If
                        End If
                    End If
                Case Keys.Escape
                    Dim ImporteGrabar As Double = 0
                    Dim ImporteGrabar2 As Double = 0
                    If dgvDetalle.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                            If dgvDetalle.Rows(x).Cells(5).Value = Nothing Then
                                MessageBox.Show("Ingrese el Importe", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgvDetalle.CurrentCell = dgvDetalle(5, x)
                                Exit Sub
                            End If
                            ImporteGrabar = ImporteGrabar + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells(5).Value)))
                        Next
                        'Importe = 0
                        'If dgvDetalle.Rows.Count > 0 Then
                        '    For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                        '        Importe = Importe + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells(5).Value)))
                        '    Next
                        '    txtTotal.Text = Format(Importe, "#,##0.00")
                        'End If
                        ImporteGrabar2 = Format(ImporteGrabar, "#,##0.00")
                    End If
                    If ImporteGrabar2 = Convert.ToDouble(Val(lblMonto2.Text)) Then
                        For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                            Dim IdDetMovimiento As String = ""
                            Dim CCosto As String = ""
                            Dim Gasto As String = ""
                            Dim Descripcion As String = ""
                            Dim EmprCodPre As String = ""
                            Dim Estado As Integer
                            Dim MontoGrabar As Double = 0
                            Dim CCosto2 As String = ""
                            Dim Opcion As String = ""
                            Dim IdDetalle As String = ""
                            Dim iResultadoGrabaGastos As Integer = 0
                            Opcion = IIf(dgvDetalle.Rows(x).Cells(7).Value = Nothing, "", dgvDetalle.Rows(x).Cells(7).Value)
                            'dgvListado.Rows(dgvListado.CurrentRow.Index).Cells(1).Value()
                            IdDetalle = IIf(dgvDetalle.Rows(x).Cells(8).Value = Nothing, "", dgvDetalle.Rows(x).Cells(8).Value)
                            CCosto = IIf(dgvDetalle.Rows(x).Cells(0).Value = Nothing, "", dgvDetalle.Rows(x).Cells(0).Value)
                            Gasto = IIf(dgvDetalle.Rows(x).Cells(1).Value = Nothing, "", dgvDetalle.Rows(x).Cells(1).Value)
                            Descripcion = IIf(dgvDetalle.Rows(x).Cells(2).Value = Nothing, "", dgvDetalle.Rows(x).Cells(2).Value)
                            EmprCodPre = IIf(dgvDetalle.Rows(x).Cells(3).Value = Nothing, "", dgvDetalle.Rows(x).Cells(3).Value)
                            If Convert.ToBoolean(dgvDetalle.Rows(x).Cells(4).Value) = False Then
                                Estado = 0
                            Else
                                Estado = 1
                            End If
                            MontoGrabar = IIf(dgvDetalle.Rows(x).Cells(5).Value = Nothing, 0, dgvDetalle.Rows(x).Cells(5).Value)
                            CCosto2 = IIf(dgvDetalle.Rows(x).Cells(6).Value = Nothing, "", dgvDetalle.Rows(x).Cells(6).Value)
                            If Opcion = "1" Then
                                'actualizar
                                iResultadoGrabaGastos = eMovimientoCajaBanco.fGrabarDetalle(30, Today(), sIdMovimiento, "", Trim(IdDetalle), CCosto, "", "", "", "", MontoGrabar, "", Descripcion, Gasto, "", 0, "", "", "", "", 0, 0, "", "", gEmpresa, "", 0, "", "", 0, "", "", "", "", EmprCodPre, Estado, CCosto2)
                            ElseIf Opcion = "0" Then
                                'grabar
                                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                eMovimientoCajaBanco.fCodigoDetalleMov(gEmpresa)
                                IdDetMovimiento = eMovimientoCajaBanco.sCodFuturoDetMov
                                iResultadoGrabaGastos = eMovimientoCajaBanco.fGrabarDetalle(27, Today(), sIdMovimiento, "", Trim(IdDetMovimiento), CCosto, "", "", "", "", MontoGrabar, "", Descripcion, Gasto, "", 0, "", "", "", "", 0, 0, "", "", gEmpresa, "", 0, "", "", 0, "", "", "", "", EmprCodPre, Estado, CCosto2)
                            End If
                        Next

                        Dim iResActCantDetenCab As Integer = 0
                        iResActCantDetenCab = eMovimientoCajaBanco.fActCantDetCabecera(43, Trim(sIdMovimiento), gEmpresa, dgvDetalle.Rows.Count)

                        MessageBox.Show("Se Actualizo el Detalle Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Me.Hide()
                    Else
                        MessageBox.Show("El Total del Detalle no es Igual al Total Abonado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
            End Select
            Calcular()
        End If
    End Sub

    Private Sub dgvDetalle_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetalle.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                'Importe = 0
                Calcular()
                If Importe = Convert.ToDouble(Val(lblMonto2.Text)) Then
                    MessageBox.Show("El Detalle esta completo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                Else
                    If dgvDetalle.Rows.Count = 0 Then
                        dgvDetalle.Rows.Add()
                        'dgvDetalleCajas.Rows(0).Cells("Opcion").Value = "0"
                        'dgvDetalleCajas.Rows(0).Cells("Fecha").Value = Now.Date().ToShortDateString
                        dgvDetalle.CurrentCell = dgvDetalle(0, 0)
                    Else
                        If dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Selected = True Then
                            Dim A As Integer
                            dgvDetalle.Rows.Add()
                            A = dgvDetalle.Rows.Count
                            If dgvDetalle.Rows(A - 2).Cells(7).Value = "0" Or dgvDetalle.Rows(A - 2).Cells(7).Value = "1" Then
                                A = dgvDetalle.Rows.Count
                                dgvDetalle.Rows(A - 1).Cells(7).Value = "0"
                                dgvDetalle.Rows(A - 1).Cells(0).Value = dgvDetalle.Rows(A - 2).Cells(0).Value
                                dgvDetalle.Rows(A - 1).Cells(1).Value = dgvDetalle.Rows(A - 2).Cells(1).Value
                                dgvDetalle.Rows(A - 1).Cells(5).Value = "0.00"
                            Else
                                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index + 1).Cells(7).Value = "0"
                            End If
                            dgvDetalle.CurrentCell = dgvDetalle(0, A - 1)
                        End If
                    End If

                End If
        End Select
    End Sub

    Sub Calcular()
        Importe = 0
        If dgvDetalle.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                Importe = Importe + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells(5).Value)))
            Next
            txtTotal.Text = Format(Importe, "#,##0.00")
        End If
    End Sub

    Private Sub dgvDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalle.Click
        Calcular()
    End Sub

    Private Sub dgvDetalle_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellDoubleClick
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If dgvDetalle.Rows.Count > 0 Then
            If columna = 0 Then
                eGastosGenerales = New clsGastosGenerales
                cboCCosEmpr.DataSource = eGastosGenerales.fListarCentrosCostos(gEmpresa)
                If eGastosGenerales.iNroRegistros > 0 Then
                    cboCCosEmpr.ValueMember = "CCosCodigo"
                    cboCCosEmpr.DisplayMember = "CCosDescripcion"
                    cboCCosEmpr.SelectedIndex = -1
                    Panel3.Visible = True
                End If
            ElseIf columna = 1 Then
                eGastosGenerales = New clsGastosGenerales
                TipoGastoEmpr.DataSource = eGastosGenerales.fListarTiposdeGasto(gEmpresa)
                If eGastosGenerales.iNroRegistros > 0 Then
                    TipoGastoEmpr.ValueMember = "IdTipoGasto"
                    TipoGastoEmpr.DisplayMember = "DescTGasto"
                    TipoGastoEmpr.SelectedIndex = -1
                    Panel4.Visible = True
                End If
            ElseIf columna = 3 Then
                If sFormaDetalle = "1" Then
                    eGastosGenerales = New clsGastosGenerales
                    cboEmpresa.DataSource = eGastosGenerales.fListarEmpresasUno(Trim(sEmpresaAPrestar))
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboEmpresa.ValueMember = "EmprCodigo"
                        cboEmpresa.DisplayMember = "EmprDescripcion"
                        cboEmpresa.SelectedIndex = -1
                    End If
                ElseIf sFormaDetalle = "2" Then
                    eGastosGenerales = New clsGastosGenerales
                    cboEmpresa.DataSource = eGastosGenerales.fListarEmpresas(24, gEmpresa)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboEmpresa.ValueMember = "EmprCodigo"
                        cboEmpresa.DisplayMember = "EmprDescripcion"
                        cboEmpresa.SelectedIndex = -1
                    End If
                End If
                Panel1.Visible = True
            ElseIf columna = 6 Then
                Dim xCodEmpresa As String = ""
                If dgvDetalle.Rows.Count > 0 Then
                    xCodEmpresa = IIf(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(3).Value = Nothing, "", dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(3).Value)
                    If Trim(xCodEmpresa) <> "" Then
                        eGastosGenerales = New clsGastosGenerales
                        cboCCosto.DataSource = eGastosGenerales.fListarCCxEmpresa(Trim(xCodEmpresa))
                        If eGastosGenerales.iNroRegistros > 0 Then
                            cboCCosto.ValueMember = "CCosCodigo"
                            cboCCosto.DisplayMember = "CCosDescripcion"
                            cboCCosto.SelectedIndex = -1
                        End If
                        Panel2.Visible = True
                    Else
                        MessageBox.Show("Ingrese una Empresa", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalle.CurrentCell = dgvDetalle(3, dgvDetalle.CurrentRow.Index)
                        Panel2.Visible = False
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpresa.SelectedIndexChanged
        If dgvDetalle.Rows.Count > 0 Then
            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(3).Value = cboEmpresa.SelectedValue
            Panel1.Visible = False
        End If
    End Sub

    Private Sub cboCCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCCosto.SelectedIndexChanged
        If dgvDetalle.Rows.Count > 0 Then
            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(6).Value = cboCCosto.SelectedValue
            Panel2.Visible = False
        End If
    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click
        Panel1.Visible = False
    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click
        Panel2.Visible = False
    End Sub

    Private Sub cboCCosEmpr_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCCosEmpr.SelectedIndexChanged
        If dgvDetalle.Rows.Count > 0 Then
            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(0).Value = cboCCosEmpr.SelectedValue
            Panel3.Visible = False
        End If
    End Sub

    Private Sub Label5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label5.Click
        Panel3.Visible = False
    End Sub

    Private Sub TipoGastoEmpr_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TipoGastoEmpr.SelectedIndexChanged
        If dgvDetalle.Rows.Count > 0 Then
            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(1).Value = TipoGastoEmpr.SelectedValue
            Panel4.Visible = False
        End If
    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click
        Panel4.Visible = False
    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        If dgvDetalle.Rows.Count > 0 Then
            Dim StrCad As String = String.Empty
            If e.ColumnIndex = 5 Then
                If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Value) <> "" Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Value.ToString), "#,##0.00")
                    If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Value) = True Then
                        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Value.ToString), "#,##0.00")
                    End If
                Else
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(5).Value = "0.00"
                End If
            End If
            Calcular()
        End If
    End Sub


    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub
End Class