<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCaja
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.cboUsuarios = New ctrLibreria.Controles.BeComboBox
        Me.txtEmpr = New ctrLibreria.Controles.BeTextBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.dgvEmpleados = New System.Windows.Forms.DataGridView
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtPla = New ctrLibreria.Controles.BeTextBox
        Me.txtCod = New ctrLibreria.Controles.BeTextBox
        Me.txtNombre = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.cboArea = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox
        Me.cboEstado = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.txtMontoMaximo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.txtMontoMinimo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.txtDescripcion = New ctrLibreria.Controles.BeTextBox
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.stsTotales = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.cboAreas = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.txtDes = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.dgvLista = New ctrLibreria.Controles.BeDataGridView
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Area = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvEmpleados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stsTotales.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(918, 452)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Size = New System.Drawing.Size(910, 426)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Size = New System.Drawing.Size(910, 426)
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.BeLabel13)
        Me.Panel1.Controls.Add(Me.BeLabel12)
        Me.Panel1.Controls.Add(Me.cboUsuarios)
        Me.Panel1.Controls.Add(Me.txtEmpr)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.txtPla)
        Me.Panel1.Controls.Add(Me.txtCod)
        Me.Panel1.Controls.Add(Me.txtNombre)
        Me.Panel1.Controls.Add(Me.BeLabel7)
        Me.Panel1.Controls.Add(Me.cboArea)
        Me.Panel1.Controls.Add(Me.BeLabel6)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Controls.Add(Me.cboMoneda)
        Me.Panel1.Controls.Add(Me.cboEstado)
        Me.Panel1.Controls.Add(Me.BeLabel9)
        Me.Panel1.Controls.Add(Me.txtMontoMaximo)
        Me.Panel1.Controls.Add(Me.BeLabel5)
        Me.Panel1.Controls.Add(Me.txtMontoMinimo)
        Me.Panel1.Controls.Add(Me.BeLabel3)
        Me.Panel1.Controls.Add(Me.txtDescripcion)
        Me.Panel1.Controls.Add(Me.txtCodigo)
        Me.Panel1.Controls.Add(Me.BeLabel2)
        Me.Panel1.Controls.Add(Me.BeLabel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(902, 424)
        Me.Panel1.TabIndex = 0
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(508, 96)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(301, 13)
        Me.BeLabel13.TabIndex = 21
        Me.BeLabel13.Text = "(Doble Click o Enter para visualizar los Empleados)"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(508, 72)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(50, 13)
        Me.BeLabel12.TabIndex = 20
        Me.BeLabel12.Text = "Usuario"
        '
        'cboUsuarios
        '
        Me.cboUsuarios.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboUsuarios.BackColor = System.Drawing.Color.Ivory
        Me.cboUsuarios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUsuarios.ForeColor = System.Drawing.Color.Black
        Me.cboUsuarios.FormattingEnabled = True
        Me.cboUsuarios.KeyEnter = True
        Me.cboUsuarios.Location = New System.Drawing.Point(564, 65)
        Me.cboUsuarios.Name = "cboUsuarios"
        Me.cboUsuarios.Size = New System.Drawing.Size(155, 21)
        Me.cboUsuarios.TabIndex = 19
        '
        'txtEmpr
        '
        Me.txtEmpr.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtEmpr.BackColor = System.Drawing.Color.Ivory
        Me.txtEmpr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmpr.ForeColor = System.Drawing.Color.Black
        Me.txtEmpr.KeyEnter = True
        Me.txtEmpr.Location = New System.Drawing.Point(752, 92)
        Me.txtEmpr.Name = "txtEmpr"
        Me.txtEmpr.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtEmpr.ShortcutsEnabled = False
        Me.txtEmpr.Size = New System.Drawing.Size(45, 20)
        Me.txtEmpr.TabIndex = 14
        Me.txtEmpr.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtEmpr.Visible = False
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label2)
        Me.Panel4.Controls.Add(Me.dgvEmpleados)
        Me.Panel4.Location = New System.Drawing.Point(120, 118)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(634, 183)
        Me.Panel4.TabIndex = 15
        Me.Panel4.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(618, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(15, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "X"
        '
        'dgvEmpleados
        '
        Me.dgvEmpleados.AllowUserToAddRows = False
        Me.dgvEmpleados.BackgroundColor = System.Drawing.Color.White
        Me.dgvEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmpleados.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column2, Me.Column3, Me.Column8, Me.Column12, Me.Column13})
        Me.dgvEmpleados.Location = New System.Drawing.Point(3, 16)
        Me.dgvEmpleados.Name = "dgvEmpleados"
        Me.dgvEmpleados.RowHeadersVisible = False
        Me.dgvEmpleados.Size = New System.Drawing.Size(626, 162)
        Me.dgvEmpleados.TabIndex = 1
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "TPlaCodigo"
        Me.Column2.HeaderText = "TPlaCodigo"
        Me.Column2.Name = "Column2"
        Me.Column2.Visible = False
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "PadCodigo"
        Me.Column3.HeaderText = "PadCodigo"
        Me.Column3.Name = "Column3"
        Me.Column3.Visible = False
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Nombres"
        Me.Column8.HeaderText = "Empleado"
        Me.Column8.Name = "Column8"
        Me.Column8.Width = 300
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "EmprCodigo"
        Me.Column12.HeaderText = "EmprCodigo"
        Me.Column12.Name = "Column12"
        Me.Column12.Visible = False
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "EmprDescripcion"
        Me.Column13.HeaderText = "Empresa"
        Me.Column13.Name = "Column13"
        Me.Column13.Width = 300
        '
        'txtPla
        '
        Me.txtPla.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPla.BackColor = System.Drawing.Color.Ivory
        Me.txtPla.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPla.ForeColor = System.Drawing.Color.Black
        Me.txtPla.KeyEnter = True
        Me.txtPla.Location = New System.Drawing.Point(655, 92)
        Me.txtPla.Name = "txtPla"
        Me.txtPla.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPla.ShortcutsEnabled = False
        Me.txtPla.Size = New System.Drawing.Size(90, 20)
        Me.txtPla.TabIndex = 13
        Me.txtPla.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtPla.Visible = False
        '
        'txtCod
        '
        Me.txtCod.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCod.BackColor = System.Drawing.Color.Ivory
        Me.txtCod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCod.ForeColor = System.Drawing.Color.Black
        Me.txtCod.KeyEnter = True
        Me.txtCod.Location = New System.Drawing.Point(508, 92)
        Me.txtCod.Name = "txtCod"
        Me.txtCod.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCod.ShortcutsEnabled = False
        Me.txtCod.Size = New System.Drawing.Size(141, 20)
        Me.txtCod.TabIndex = 12
        Me.txtCod.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCod.Visible = False
        '
        'txtNombre
        '
        Me.txtNombre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNombre.BackColor = System.Drawing.Color.Ivory
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.ForeColor = System.Drawing.Color.Black
        Me.txtNombre.KeyEnter = True
        Me.txtNombre.Location = New System.Drawing.Point(120, 92)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNombre.ShortcutsEnabled = False
        Me.txtNombre.Size = New System.Drawing.Size(382, 20)
        Me.txtNombre.TabIndex = 11
        Me.txtNombre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(20, 72)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel7.TabIndex = 2
        Me.BeLabel7.Text = "Area"
        '
        'cboArea
        '
        Me.cboArea.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboArea.BackColor = System.Drawing.Color.Ivory
        Me.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArea.ForeColor = System.Drawing.Color.Black
        Me.cboArea.FormattingEnabled = True
        Me.cboArea.KeyEnter = True
        Me.cboArea.Location = New System.Drawing.Point(120, 64)
        Me.cboArea.Name = "cboArea"
        Me.cboArea.Size = New System.Drawing.Size(382, 21)
        Me.cboArea.TabIndex = 10
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(20, 126)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(100, 13)
        Me.BeLabel6.TabIndex = 4
        Me.BeLabel6.Text = "Moneda de Caja"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(20, 99)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel4.TabIndex = 3
        Me.BeLabel4.Text = "Empleado"
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(120, 118)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(159, 21)
        Me.cboMoneda.TabIndex = 9
        '
        'cboEstado
        '
        Me.cboEstado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstado.BackColor = System.Drawing.Color.Ivory
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.ForeColor = System.Drawing.Color.Black
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.KeyEnter = True
        Me.cboEstado.Location = New System.Drawing.Point(120, 197)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(160, 21)
        Me.cboEstado.TabIndex = 18
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(20, 205)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel9.TabIndex = 7
        Me.BeLabel9.Text = "Estado"
        '
        'txtMontoMaximo
        '
        Me.txtMontoMaximo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMontoMaximo.BackColor = System.Drawing.Color.Ivory
        Me.txtMontoMaximo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoMaximo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoMaximo.ForeColor = System.Drawing.Color.Black
        Me.txtMontoMaximo.KeyEnter = True
        Me.txtMontoMaximo.Location = New System.Drawing.Point(120, 171)
        Me.txtMontoMaximo.MaxLength = 20
        Me.txtMontoMaximo.Name = "txtMontoMaximo"
        Me.txtMontoMaximo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMontoMaximo.ShortcutsEnabled = False
        Me.txtMontoMaximo.Size = New System.Drawing.Size(160, 20)
        Me.txtMontoMaximo.TabIndex = 17
        Me.txtMontoMaximo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(20, 178)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(73, 13)
        Me.BeLabel5.TabIndex = 6
        Me.BeLabel5.Text = "Monto Base"
        '
        'txtMontoMinimo
        '
        Me.txtMontoMinimo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMontoMinimo.BackColor = System.Drawing.Color.Ivory
        Me.txtMontoMinimo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoMinimo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoMinimo.ForeColor = System.Drawing.Color.Black
        Me.txtMontoMinimo.KeyEnter = True
        Me.txtMontoMinimo.Location = New System.Drawing.Point(120, 145)
        Me.txtMontoMinimo.MaxLength = 20
        Me.txtMontoMinimo.Name = "txtMontoMinimo"
        Me.txtMontoMinimo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMontoMinimo.ShortcutsEnabled = False
        Me.txtMontoMinimo.Size = New System.Drawing.Size(161, 20)
        Me.txtMontoMinimo.TabIndex = 16
        Me.txtMontoMinimo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(18, 152)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(85, 13)
        Me.BeLabel3.TabIndex = 5
        Me.BeLabel3.Text = "Monto M�nimo"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.AcceptsReturn = True
        Me.txtDescripcion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDescripcion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDescripcion.BackColor = System.Drawing.Color.Ivory
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.ForeColor = System.Drawing.Color.Black
        Me.txtDescripcion.KeyEnter = True
        Me.txtDescripcion.Location = New System.Drawing.Point(120, 33)
        Me.txtDescripcion.MaxLength = 80
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDescripcion.ShortcutsEnabled = False
        Me.txtDescripcion.Size = New System.Drawing.Size(599, 20)
        Me.txtDescripcion.TabIndex = 9
        Me.txtDescripcion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtDescripcion.WordWrap = False
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(120, 7)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(159, 20)
        Me.txtCodigo.TabIndex = 8
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(18, 40)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(73, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Descripci�n"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(18, 14)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "C�digo"
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsTotales.Location = New System.Drawing.Point(3, 397)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(902, 24)
        Me.stsTotales.TabIndex = 2
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(122, 19)
        Me.ToolStripStatusLabel1.Text = "Total de Registros= 0"
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.cboAreas)
        Me.Panel2.Controls.Add(Me.BeLabel11)
        Me.Panel2.Controls.Add(Me.BeLabel10)
        Me.Panel2.Controls.Add(Me.txtDes)
        Me.Panel2.Controls.Add(Me.BeLabel8)
        Me.Panel2.Location = New System.Drawing.Point(262, 154)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(427, 76)
        Me.Panel2.TabIndex = 1
        Me.Panel2.Visible = False
        '
        'cboAreas
        '
        Me.cboAreas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAreas.BackColor = System.Drawing.Color.Ivory
        Me.cboAreas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAreas.ForeColor = System.Drawing.Color.Black
        Me.cboAreas.FormattingEnabled = True
        Me.cboAreas.KeyEnter = True
        Me.cboAreas.Location = New System.Drawing.Point(82, 44)
        Me.cboAreas.Name = "cboAreas"
        Me.cboAreas.Size = New System.Drawing.Size(270, 21)
        Me.cboAreas.TabIndex = 4
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(8, 47)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel11.TabIndex = 3
        Me.BeLabel11.Text = "Area"
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(8, 20)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(73, 13)
        Me.BeLabel10.TabIndex = 1
        Me.BeLabel10.Text = "Descripci�n"
        '
        'txtDes
        '
        Me.txtDes.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDes.BackColor = System.Drawing.Color.Ivory
        Me.txtDes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDes.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDes.ForeColor = System.Drawing.Color.Black
        Me.txtDes.KeyEnter = True
        Me.txtDes.Location = New System.Drawing.Point(82, 18)
        Me.txtDes.Name = "txtDes"
        Me.txtDes.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDes.ShortcutsEnabled = False
        Me.txtDes.Size = New System.Drawing.Size(331, 20)
        Me.txtDes.TabIndex = 2
        Me.txtDes.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Red
        Me.BeLabel8.Location = New System.Drawing.Point(409, -1)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(18, 16)
        Me.BeLabel8.TabIndex = 0
        Me.BeLabel8.Text = "X"
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Descripcion, Me.Area, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Estado, Me.Column9, Me.Column10, Me.Column11, Me.Column1, Me.Column14, Me.Column15})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLista.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-1, -1)
        Me.dgvLista.MultiSelect = False
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLista.Size = New System.Drawing.Size(1762, 382)
        Me.dgvLista.TabIndex = 3
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "IdCaja"
        Me.Codigo.HeaderText = "C�digo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 130
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "DescripcionCaja"
        Me.Descripcion.HeaderText = "Descripci�n"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 150
        '
        'Area
        '
        Me.Area.DataPropertyName = "AreaNombre"
        Me.Area.HeaderText = "Area"
        Me.Area.Name = "Area"
        Me.Area.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Nombres"
        Me.Column4.HeaderText = "Responsable"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "MonDescripcion"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column5.HeaderText = "Moneda"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "MontoMinimo"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column6.HeaderText = "Monto Minimo"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "MontoMaximo"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Column7.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column7.HeaderText = "Monto M�ximo"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 110
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estados"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "PadCodigo"
        Me.Column9.HeaderText = "PadCodigo"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Visible = False
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "IdMoneda"
        Me.Column10.HeaderText = "IdMoneda"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Visible = False
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "CodArea"
        Me.Column11.HeaderText = "CodArea"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Visible = False
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "TPlaCodigo"
        Me.Column1.HeaderText = "TPlaCodigo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "EmprResposable"
        Me.Column14.HeaderText = "EmprResposable"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        Me.Column14.Visible = False
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "UsuCodigo"
        Me.Column15.HeaderText = "Usuario"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        Me.Column15.Visible = False
        '
        'frmCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(918, 452)
        Me.MaximizeBox = False
        Me.Name = "frmCaja"
        Me.Text = "Cajas"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvEmpleados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtMontoMaximo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtMontoMinimo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtDescripcion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents cboEstado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboArea As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtDes As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboAreas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents dgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents txtPla As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCod As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNombre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgvEmpleados As System.Windows.Forms.DataGridView
    Friend WithEvents txtEmpr As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboUsuarios As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Area As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel

End Class
