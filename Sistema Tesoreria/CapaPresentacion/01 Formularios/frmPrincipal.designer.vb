<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.tsmiCatalogos = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiCaja = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCajaExtraordinaria = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoDocumentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoArqueoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.TipoGastosGrupo1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CondigurarReportesDePagosAProvToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiCondicionPago = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiBancos = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiCuenta = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiChequera = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiCheque = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiRegistroDocumento = New System.Windows.Forms.ToolStripMenuItem()
        Me.ITFToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContratistasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EspecialidadToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoContratistasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContratistasToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContratosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Cigarra1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProgramaciónCuentasARendirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VistoBuenoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.msImpuesto = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiMoviminetos = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSesionCajaChica = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSesionCajaExtraordinaria = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagoAProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEgresoCajaBancos = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuIngresoBancario = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroBancosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LibroBancosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MantenimientoLibrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaldoBancarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnexosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DepPlazoDeLaEmpresaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeDocumentacionPorCargosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentasPorPagarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuentaCorrienteProveedorProgramaciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeProveedoresToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContratistasToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistorialDeContratosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DerivarPagosDeOrdenDeCompraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionDeDocumentosPendientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProgramaciónDePagosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroDeRendicionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SesionDeCajaChicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobranzasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentoDeVentaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ItemsDeVentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MantenimientoNumeradorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MigrarDataToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CobranzasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagosPlanillasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PercepciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RetencionesToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetraccionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestionDocumentariaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiReportes = New System.Windows.Forms.ToolStripMenuItem()
        Me.GastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrestamosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GastosPorMovimientosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GastosPorCentroDeCostoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagoProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesVariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PagosPorCategoriaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentosPendieToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeRetencionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContabilidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PolizasPorPagarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetalleDePolizasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuotasCanceladasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CuotasVencidasPorVencerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GastosEnviadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GestiónDeDocumentosPendientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProgramaciónDePagosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiProcesos = New System.Windows.Forms.ToolStripMenuItem()
        Me.OperacionesRealizadasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlAdministraciónToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlContabilidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RetencionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosPorUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadoDeComprobantesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Cigarra2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VencimientoDePolizasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AseguradosPorObra2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MovimientosPorCuentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiUtilitarios = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlDeContratosYValorizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ParteDiarioDeContToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValorizacionesSemanalesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OperacionesVariasAdelantosFGAdendasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiHerramientas = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RetencionesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ParametrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeriesRetencionesToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValorizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiVentana = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiAyuda = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmiSalir = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Barra = New System.Windows.Forms.ToolStrip()
        Me.Nuevo = New System.Windows.Forms.ToolStripButton()
        Me.Modifica = New System.Windows.Forms.ToolStripButton()
        Me.Elimina = New System.Windows.Forms.ToolStripButton()
        Me.Graba = New System.Windows.Forms.ToolStripButton()
        Me.Cancela = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator20 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator()
        Me.Buscar = New System.Windows.Forms.ToolStripButton()
        Me.Exporta = New System.Windows.Forms.ToolStripButton()
        Me.Imprime = New System.Windows.Forms.ToolStripButton()
        Me.Visualiza = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Salir = New System.Windows.Forms.ToolStripButton()
        Me.BarraSecundaria = New System.Windows.Forms.ToolStrip()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator12 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnModificar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator11 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripSeparator10 = New System.Windows.Forms.ToolStripSeparator()
        Me.stsEstado = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toEmpresa = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toPeriodo = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toPC = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toFecha = New System.Windows.Forms.ToolStripStatusLabel()
        Me.toVersion = New System.Windows.Forms.ToolStripStatusLabel()
        Me.GestionValorizacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.Barra.SuspendLayout()
        Me.BarraSecundaria.SuspendLayout()
        Me.stsEstado.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AutoSize = False
        Me.MenuStrip1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiCatalogos, Me.tsmiMoviminetos, Me.tsmiReportes, Me.tsmiProcesos, Me.tsmiUtilitarios, Me.tsmiHerramientas, Me.tsmiVentana, Me.tsmiAyuda, Me.tsmiSalir})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuStrip1.Size = New System.Drawing.Size(933, 37)
        Me.MenuStrip1.TabIndex = 27
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'tsmiCatalogos
        '
        Me.tsmiCatalogos.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmiCaja, Me.tsmiCondicionPago, Me.tsmiBancos, Me.tsmiCuenta, Me.tsmiChequera, Me.tsmiCheque, Me.tsmiRegistroDocumento, Me.ITFToolStripMenuItem, Me.ContratistasToolStripMenuItem, Me.ContratosToolStripMenuItem1, Me.Cigarra1ToolStripMenuItem, Me.ProgramaciónCuentasARendirToolStripMenuItem, Me.VistoBuenoToolStripMenuItem, Me.ToolStripMenuItem4, Me.msImpuesto})
        Me.tsmiCatalogos.Image = CType(resources.GetObject("tsmiCatalogos.Image"), System.Drawing.Image)
        Me.tsmiCatalogos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.tsmiCatalogos.Name = "tsmiCatalogos"
        Me.tsmiCatalogos.Size = New System.Drawing.Size(127, 33)
        Me.tsmiCatalogos.Text = "&Mantenimientos"
        Me.tsmiCatalogos.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tsmiCaja
        '
        Me.tsmiCaja.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CajaToolStripMenuItem, Me.mnuCajaExtraordinaria, Me.TipoDocumentoToolStripMenuItem, Me.TipoGastosToolStripMenuItem, Me.EstadoArqueoToolStripMenuItem, Me.ToolStripSeparator8, Me.TipoGastosGrupo1ToolStripMenuItem, Me.CondigurarReportesDePagosAProvToolStripMenuItem})
        Me.tsmiCaja.Name = "tsmiCaja"
        Me.tsmiCaja.Size = New System.Drawing.Size(279, 22)
        Me.tsmiCaja.Text = "Anexos"
        '
        'CajaToolStripMenuItem
        '
        Me.CajaToolStripMenuItem.Name = "CajaToolStripMenuItem"
        Me.CajaToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.CajaToolStripMenuItem.Text = "Cajas"
        '
        'mnuCajaExtraordinaria
        '
        Me.mnuCajaExtraordinaria.Name = "mnuCajaExtraordinaria"
        Me.mnuCajaExtraordinaria.Size = New System.Drawing.Size(419, 22)
        Me.mnuCajaExtraordinaria.Text = "Caja Extraordinaria"
        '
        'TipoDocumentoToolStripMenuItem
        '
        Me.TipoDocumentoToolStripMenuItem.Name = "TipoDocumentoToolStripMenuItem"
        Me.TipoDocumentoToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.TipoDocumentoToolStripMenuItem.Text = "Tipo Comprobante"
        '
        'TipoGastosToolStripMenuItem
        '
        Me.TipoGastosToolStripMenuItem.Name = "TipoGastosToolStripMenuItem"
        Me.TipoGastosToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.TipoGastosToolStripMenuItem.Text = "Tipo Gastos"
        '
        'EstadoArqueoToolStripMenuItem
        '
        Me.EstadoArqueoToolStripMenuItem.Name = "EstadoArqueoToolStripMenuItem"
        Me.EstadoArqueoToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.EstadoArqueoToolStripMenuItem.Text = "Estado Arqueo"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(416, 6)
        '
        'TipoGastosGrupo1ToolStripMenuItem
        '
        Me.TipoGastosGrupo1ToolStripMenuItem.Name = "TipoGastosGrupo1ToolStripMenuItem"
        Me.TipoGastosGrupo1ToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.TipoGastosGrupo1ToolStripMenuItem.Text = "Configurar Reportes de Gastos"
        '
        'CondigurarReportesDePagosAProvToolStripMenuItem
        '
        Me.CondigurarReportesDePagosAProvToolStripMenuItem.Name = "CondigurarReportesDePagosAProvToolStripMenuItem"
        Me.CondigurarReportesDePagosAProvToolStripMenuItem.Size = New System.Drawing.Size(419, 22)
        Me.CondigurarReportesDePagosAProvToolStripMenuItem.Text = "Configurar Colegios/Obras - Reporte de Pago de Proveedores"
        '
        'tsmiCondicionPago
        '
        Me.tsmiCondicionPago.Name = "tsmiCondicionPago"
        Me.tsmiCondicionPago.Size = New System.Drawing.Size(279, 22)
        Me.tsmiCondicionPago.Text = "Tipo Movimiento"
        '
        'tsmiBancos
        '
        Me.tsmiBancos.Name = "tsmiBancos"
        Me.tsmiBancos.Size = New System.Drawing.Size(279, 22)
        Me.tsmiBancos.Text = "Bancos"
        '
        'tsmiCuenta
        '
        Me.tsmiCuenta.Name = "tsmiCuenta"
        Me.tsmiCuenta.Size = New System.Drawing.Size(279, 22)
        Me.tsmiCuenta.Text = "Cuenta Bancos"
        '
        'tsmiChequera
        '
        Me.tsmiChequera.Name = "tsmiChequera"
        Me.tsmiChequera.Size = New System.Drawing.Size(279, 22)
        Me.tsmiChequera.Text = "Chequera"
        '
        'tsmiCheque
        '
        Me.tsmiCheque.Name = "tsmiCheque"
        Me.tsmiCheque.Size = New System.Drawing.Size(279, 22)
        Me.tsmiCheque.Text = "Cheque"
        '
        'tsmiRegistroDocumento
        '
        Me.tsmiRegistroDocumento.Name = "tsmiRegistroDocumento"
        Me.tsmiRegistroDocumento.Size = New System.Drawing.Size(279, 22)
        Me.tsmiRegistroDocumento.Text = "Registro de Comprobantes"
        '
        'ITFToolStripMenuItem
        '
        Me.ITFToolStripMenuItem.Name = "ITFToolStripMenuItem"
        Me.ITFToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.ITFToolStripMenuItem.Text = "ITF"
        '
        'ContratistasToolStripMenuItem
        '
        Me.ContratistasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EspecialidadToolStripMenuItem1, Me.TipoContratistasToolStripMenuItem1, Me.ContratistasToolStripMenuItem2})
        Me.ContratistasToolStripMenuItem.Name = "ContratistasToolStripMenuItem"
        Me.ContratistasToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.ContratistasToolStripMenuItem.Text = "Contratistas"
        '
        'EspecialidadToolStripMenuItem1
        '
        Me.EspecialidadToolStripMenuItem1.Name = "EspecialidadToolStripMenuItem1"
        Me.EspecialidadToolStripMenuItem1.Size = New System.Drawing.Size(172, 22)
        Me.EspecialidadToolStripMenuItem1.Text = "Especialidad"
        '
        'TipoContratistasToolStripMenuItem1
        '
        Me.TipoContratistasToolStripMenuItem1.Name = "TipoContratistasToolStripMenuItem1"
        Me.TipoContratistasToolStripMenuItem1.Size = New System.Drawing.Size(172, 22)
        Me.TipoContratistasToolStripMenuItem1.Text = "Tipo"
        '
        'ContratistasToolStripMenuItem2
        '
        Me.ContratistasToolStripMenuItem2.Name = "ContratistasToolStripMenuItem2"
        Me.ContratistasToolStripMenuItem2.Size = New System.Drawing.Size(172, 22)
        Me.ContratistasToolStripMenuItem2.Text = "Datos Personales"
        '
        'ContratosToolStripMenuItem1
        '
        Me.ContratosToolStripMenuItem1.Name = "ContratosToolStripMenuItem1"
        Me.ContratosToolStripMenuItem1.Size = New System.Drawing.Size(279, 22)
        Me.ContratosToolStripMenuItem1.Text = "Contratos"
        '
        'Cigarra1ToolStripMenuItem
        '
        Me.Cigarra1ToolStripMenuItem.Name = "Cigarra1ToolStripMenuItem"
        Me.Cigarra1ToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.Cigarra1ToolStripMenuItem.Text = "Polizas"
        '
        'ProgramaciónCuentasARendirToolStripMenuItem
        '
        Me.ProgramaciónCuentasARendirToolStripMenuItem.Name = "ProgramaciónCuentasARendirToolStripMenuItem"
        Me.ProgramaciónCuentasARendirToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.ProgramaciónCuentasARendirToolStripMenuItem.Text = "Entregas a Rendir"
        '
        'VistoBuenoToolStripMenuItem
        '
        Me.VistoBuenoToolStripMenuItem.Name = "VistoBuenoToolStripMenuItem"
        Me.VistoBuenoToolStripMenuItem.Size = New System.Drawing.Size(279, 22)
        Me.VistoBuenoToolStripMenuItem.Text = "Visto bueno"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(279, 22)
        Me.ToolStripMenuItem4.Text = "-----------------------------------------"
        '
        'msImpuesto
        '
        Me.msImpuesto.Name = "msImpuesto"
        Me.msImpuesto.Size = New System.Drawing.Size(279, 22)
        Me.msImpuesto.Text = "Impuestos"
        '
        'tsmiMoviminetos
        '
        Me.tsmiMoviminetos.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CajaToolStripMenuItem1, Me.PagoAProveedoresToolStripMenuItem, Me.LibroBancosToolStripMenuItem, Me.ControlDeDocumentacionPorCargosToolStripMenuItem, Me.CuentasPorPagarToolStripMenuItem, Me.DerivarPagosDeOrdenDeCompraToolStripMenuItem, Me.SesionDeCajaChicaToolStripMenuItem, Me.VentasToolStripMenuItem, Me.CobranzasToolStripMenuItem, Me.CobranzasToolStripMenuItem1, Me.PagosPlanillasToolStripMenuItem, Me.PercepciónToolStripMenuItem, Me.RetencionesToolStripMenuItem2, Me.DetraccionesToolStripMenuItem, Me.GestionDocumentariaToolStripMenuItem, Me.GestionValorizacionesToolStripMenuItem})
        Me.tsmiMoviminetos.Image = CType(resources.GetObject("tsmiMoviminetos.Image"), System.Drawing.Image)
        Me.tsmiMoviminetos.Name = "tsmiMoviminetos"
        Me.tsmiMoviminetos.Size = New System.Drawing.Size(86, 33)
        Me.tsmiMoviminetos.Text = "&Procesos"
        '
        'CajaToolStripMenuItem1
        '
        Me.CajaToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSesionCajaChica, Me.mnuSesionCajaExtraordinaria})
        Me.CajaToolStripMenuItem1.Name = "CajaToolStripMenuItem1"
        Me.CajaToolStripMenuItem1.Size = New System.Drawing.Size(289, 22)
        Me.CajaToolStripMenuItem1.Text = "Caja"
        '
        'mnuSesionCajaChica
        '
        Me.mnuSesionCajaChica.Name = "mnuSesionCajaChica"
        Me.mnuSesionCajaChica.Size = New System.Drawing.Size(183, 22)
        Me.mnuSesionCajaChica.Text = "Caja Chica"
        '
        'mnuSesionCajaExtraordinaria
        '
        Me.mnuSesionCajaExtraordinaria.Name = "mnuSesionCajaExtraordinaria"
        Me.mnuSesionCajaExtraordinaria.Size = New System.Drawing.Size(183, 22)
        Me.mnuSesionCajaExtraordinaria.Text = "Caja Extraordinaria"
        '
        'PagoAProveedoresToolStripMenuItem
        '
        Me.PagoAProveedoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEgresoCajaBancos, Me.mnuIngresoBancario})
        Me.PagoAProveedoresToolStripMenuItem.Name = "PagoAProveedoresToolStripMenuItem"
        Me.PagoAProveedoresToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.PagoAProveedoresToolStripMenuItem.Text = "Caja y Bancos"
        '
        'mnuEgresoCajaBancos
        '
        Me.mnuEgresoCajaBancos.Name = "mnuEgresoCajaBancos"
        Me.mnuEgresoCajaBancos.Size = New System.Drawing.Size(210, 22)
        Me.mnuEgresoCajaBancos.Text = "Egreso de Caja y Bancos"
        '
        'mnuIngresoBancario
        '
        Me.mnuIngresoBancario.Name = "mnuIngresoBancario"
        Me.mnuIngresoBancario.Size = New System.Drawing.Size(210, 22)
        Me.mnuIngresoBancario.Text = "Ingreso Bancario"
        '
        'LibroBancosToolStripMenuItem
        '
        Me.LibroBancosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LibroBancosToolStripMenuItem1, Me.MantenimientoLibrosToolStripMenuItem, Me.ReportesToolStripMenuItem})
        Me.LibroBancosToolStripMenuItem.Name = "LibroBancosToolStripMenuItem"
        Me.LibroBancosToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.LibroBancosToolStripMenuItem.Text = "Conciliacion Bancaria"
        '
        'LibroBancosToolStripMenuItem1
        '
        Me.LibroBancosToolStripMenuItem1.Name = "LibroBancosToolStripMenuItem1"
        Me.LibroBancosToolStripMenuItem1.Size = New System.Drawing.Size(184, 22)
        Me.LibroBancosToolStripMenuItem1.Text = "Libro Bancos"
        '
        'MantenimientoLibrosToolStripMenuItem
        '
        Me.MantenimientoLibrosToolStripMenuItem.Name = "MantenimientoLibrosToolStripMenuItem"
        Me.MantenimientoLibrosToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.MantenimientoLibrosToolStripMenuItem.Text = "Saldo Bancos x Mes"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaldoBancarioToolStripMenuItem, Me.VariosToolStripMenuItem, Me.AnexosToolStripMenuItem, Me.DepPlazoDeLaEmpresaToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'SaldoBancarioToolStripMenuItem
        '
        Me.SaldoBancarioToolStripMenuItem.Name = "SaldoBancarioToolStripMenuItem"
        Me.SaldoBancarioToolStripMenuItem.Size = New System.Drawing.Size(392, 22)
        Me.SaldoBancarioToolStripMenuItem.Text = "Saldo Bancario"
        '
        'VariosToolStripMenuItem
        '
        Me.VariosToolStripMenuItem.Name = "VariosToolStripMenuItem"
        Me.VariosToolStripMenuItem.Size = New System.Drawing.Size(392, 22)
        Me.VariosToolStripMenuItem.Text = "Abonos a Cuentas - Cheques x Cobrar - Mov x Identificar"
        '
        'AnexosToolStripMenuItem
        '
        Me.AnexosToolStripMenuItem.Name = "AnexosToolStripMenuItem"
        Me.AnexosToolStripMenuItem.Size = New System.Drawing.Size(392, 22)
        Me.AnexosToolStripMenuItem.Text = "Anexos"
        '
        'DepPlazoDeLaEmpresaToolStripMenuItem
        '
        Me.DepPlazoDeLaEmpresaToolStripMenuItem.Name = "DepPlazoDeLaEmpresaToolStripMenuItem"
        Me.DepPlazoDeLaEmpresaToolStripMenuItem.Size = New System.Drawing.Size(392, 22)
        Me.DepPlazoDeLaEmpresaToolStripMenuItem.Text = "Dep Plazo por Cuenta"
        '
        'ControlDeDocumentacionPorCargosToolStripMenuItem
        '
        Me.ControlDeDocumentacionPorCargosToolStripMenuItem.Name = "ControlDeDocumentacionPorCargosToolStripMenuItem"
        Me.ControlDeDocumentacionPorCargosToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.ControlDeDocumentacionPorCargosToolStripMenuItem.Text = "Recepción de Documentos por Cargos"
        '
        'CuentasPorPagarToolStripMenuItem
        '
        Me.CuentasPorPagarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProveedoresToolStripMenuItem, Me.ContratistasToolStripMenuItem3})
        Me.CuentasPorPagarToolStripMenuItem.Name = "CuentasPorPagarToolStripMenuItem"
        Me.CuentasPorPagarToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.CuentasPorPagarToolStripMenuItem.Text = "Cuentas por Pagar"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CuentaCorrienteProveedorProgramaciónToolStripMenuItem, Me.EstadoDeProveedoresToolStripMenuItem1})
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores"
        '
        'CuentaCorrienteProveedorProgramaciónToolStripMenuItem
        '
        Me.CuentaCorrienteProveedorProgramaciónToolStripMenuItem.Name = "CuentaCorrienteProveedorProgramaciónToolStripMenuItem"
        Me.CuentaCorrienteProveedorProgramaciónToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.CuentaCorrienteProveedorProgramaciónToolStripMenuItem.Text = "Programación"
        '
        'EstadoDeProveedoresToolStripMenuItem1
        '
        Me.EstadoDeProveedoresToolStripMenuItem1.Name = "EstadoDeProveedoresToolStripMenuItem1"
        Me.EstadoDeProveedoresToolStripMenuItem1.Size = New System.Drawing.Size(172, 22)
        Me.EstadoDeProveedoresToolStripMenuItem1.Text = "Estado de Cuenta"
        '
        'ContratistasToolStripMenuItem3
        '
        Me.ContratistasToolStripMenuItem3.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HistorialDeContratosToolStripMenuItem, Me.ToolStripMenuItem1, Me.ToolStripMenuItem3})
        Me.ContratistasToolStripMenuItem3.Name = "ContratistasToolStripMenuItem3"
        Me.ContratistasToolStripMenuItem3.Size = New System.Drawing.Size(146, 22)
        Me.ContratistasToolStripMenuItem3.Text = "Contratistas"
        '
        'HistorialDeContratosToolStripMenuItem
        '
        Me.HistorialDeContratosToolStripMenuItem.Name = "HistorialDeContratosToolStripMenuItem"
        Me.HistorialDeContratosToolStripMenuItem.Size = New System.Drawing.Size(394, 22)
        Me.HistorialDeContratosToolStripMenuItem.Text = "Programación de Pagos (Valorizaciones - Adelantos - FG)"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(394, 22)
        Me.ToolStripMenuItem1.Text = "Reporte Consolidado"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(394, 22)
        Me.ToolStripMenuItem3.Text = "Reportes Valorizaciones"
        '
        'DerivarPagosDeOrdenDeCompraToolStripMenuItem
        '
        Me.DerivarPagosDeOrdenDeCompraToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GestionDeDocumentosPendientesToolStripMenuItem, Me.ProgramaciónDePagosToolStripMenuItem, Me.RegistroDeRendicionesToolStripMenuItem})
        Me.DerivarPagosDeOrdenDeCompraToolStripMenuItem.Name = "DerivarPagosDeOrdenDeCompraToolStripMenuItem"
        Me.DerivarPagosDeOrdenDeCompraToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.DerivarPagosDeOrdenDeCompraToolStripMenuItem.Text = "Documentos Pendientes"
        '
        'GestionDeDocumentosPendientesToolStripMenuItem
        '
        Me.GestionDeDocumentosPendientesToolStripMenuItem.Name = "GestionDeDocumentosPendientesToolStripMenuItem"
        Me.GestionDeDocumentosPendientesToolStripMenuItem.Size = New System.Drawing.Size(274, 22)
        Me.GestionDeDocumentosPendientesToolStripMenuItem.Text = "Gestión de Documentos Pendientes"
        '
        'ProgramaciónDePagosToolStripMenuItem
        '
        Me.ProgramaciónDePagosToolStripMenuItem.Name = "ProgramaciónDePagosToolStripMenuItem"
        Me.ProgramaciónDePagosToolStripMenuItem.Size = New System.Drawing.Size(274, 22)
        Me.ProgramaciónDePagosToolStripMenuItem.Text = "Programación de Pagos"
        '
        'RegistroDeRendicionesToolStripMenuItem
        '
        Me.RegistroDeRendicionesToolStripMenuItem.Name = "RegistroDeRendicionesToolStripMenuItem"
        Me.RegistroDeRendicionesToolStripMenuItem.Size = New System.Drawing.Size(274, 22)
        Me.RegistroDeRendicionesToolStripMenuItem.Text = "Registro de Rendiciones"
        '
        'SesionDeCajaChicaToolStripMenuItem
        '
        Me.SesionDeCajaChicaToolStripMenuItem.Name = "SesionDeCajaChicaToolStripMenuItem"
        Me.SesionDeCajaChicaToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.SesionDeCajaChicaToolStripMenuItem.Text = "Caja Chica"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.VentasToolStripMenuItem.Text = "-------------------------------------------"
        '
        'CobranzasToolStripMenuItem
        '
        Me.CobranzasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DocumentoDeVentaToolStripMenuItem1, Me.ItemsDeVentaToolStripMenuItem, Me.VentasToolStripMenuItem1, Me.MantenimientoNumeradorToolStripMenuItem, Me.MigrarDataToolStripMenuItem})
        Me.CobranzasToolStripMenuItem.Name = "CobranzasToolStripMenuItem"
        Me.CobranzasToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.CobranzasToolStripMenuItem.Text = "Ventas"
        '
        'DocumentoDeVentaToolStripMenuItem1
        '
        Me.DocumentoDeVentaToolStripMenuItem1.Name = "DocumentoDeVentaToolStripMenuItem1"
        Me.DocumentoDeVentaToolStripMenuItem1.Size = New System.Drawing.Size(226, 22)
        Me.DocumentoDeVentaToolStripMenuItem1.Text = "Documento de Venta"
        '
        'ItemsDeVentaToolStripMenuItem
        '
        Me.ItemsDeVentaToolStripMenuItem.Name = "ItemsDeVentaToolStripMenuItem"
        Me.ItemsDeVentaToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ItemsDeVentaToolStripMenuItem.Text = "Items de Venta"
        '
        'VentasToolStripMenuItem1
        '
        Me.VentasToolStripMenuItem1.Name = "VentasToolStripMenuItem1"
        Me.VentasToolStripMenuItem1.Size = New System.Drawing.Size(226, 22)
        Me.VentasToolStripMenuItem1.Text = "Ventas"
        '
        'MantenimientoNumeradorToolStripMenuItem
        '
        Me.MantenimientoNumeradorToolStripMenuItem.Name = "MantenimientoNumeradorToolStripMenuItem"
        Me.MantenimientoNumeradorToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.MantenimientoNumeradorToolStripMenuItem.Text = "Mantenimiento Numerador"
        '
        'MigrarDataToolStripMenuItem
        '
        Me.MigrarDataToolStripMenuItem.Name = "MigrarDataToolStripMenuItem"
        Me.MigrarDataToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.MigrarDataToolStripMenuItem.Text = "Migrar Data"
        '
        'CobranzasToolStripMenuItem1
        '
        Me.CobranzasToolStripMenuItem1.Name = "CobranzasToolStripMenuItem1"
        Me.CobranzasToolStripMenuItem1.Size = New System.Drawing.Size(289, 22)
        Me.CobranzasToolStripMenuItem1.Text = "Cobranzas"
        '
        'PagosPlanillasToolStripMenuItem
        '
        Me.PagosPlanillasToolStripMenuItem.Name = "PagosPlanillasToolStripMenuItem"
        Me.PagosPlanillasToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.PagosPlanillasToolStripMenuItem.Text = "Pagos Planillas"
        '
        'PercepciónToolStripMenuItem
        '
        Me.PercepciónToolStripMenuItem.Name = "PercepciónToolStripMenuItem"
        Me.PercepciónToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.PercepciónToolStripMenuItem.Text = "Percepción"
        '
        'RetencionesToolStripMenuItem2
        '
        Me.RetencionesToolStripMenuItem2.Name = "RetencionesToolStripMenuItem2"
        Me.RetencionesToolStripMenuItem2.Size = New System.Drawing.Size(289, 22)
        Me.RetencionesToolStripMenuItem2.Text = "Retenciones"
        '
        'DetraccionesToolStripMenuItem
        '
        Me.DetraccionesToolStripMenuItem.Name = "DetraccionesToolStripMenuItem"
        Me.DetraccionesToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.DetraccionesToolStripMenuItem.Text = "Detracciones"
        '
        'GestionDocumentariaToolStripMenuItem
        '
        Me.GestionDocumentariaToolStripMenuItem.Name = "GestionDocumentariaToolStripMenuItem"
        Me.GestionDocumentariaToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.GestionDocumentariaToolStripMenuItem.Text = "Gestion Documentaria"
        '
        'tsmiReportes
        '
        Me.tsmiReportes.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GastosToolStripMenuItem, Me.PrestamosToolStripMenuItem, Me.GastosPorMovimientosToolStripMenuItem, Me.GastosPorCentroDeCostoToolStripMenuItem, Me.PagoProveedoresToolStripMenuItem, Me.AdToolStripMenuItem, Me.DocumentosPendieToolStripMenuItem, Me.ListaDeRetencionesToolStripMenuItem, Me.ContabilidadToolStripMenuItem, Me.PolizasToolStripMenuItem, Me.GastosEnviadosToolStripMenuItem, Me.GestiónDeDocumentosPendientesToolStripMenuItem, Me.ProgramaciónDePagosToolStripMenuItem1})
        Me.tsmiReportes.Image = CType(resources.GetObject("tsmiReportes.Image"), System.Drawing.Image)
        Me.tsmiReportes.Name = "tsmiReportes"
        Me.tsmiReportes.Size = New System.Drawing.Size(87, 33)
        Me.tsmiReportes.Text = "&Reportes"
        '
        'GastosToolStripMenuItem
        '
        Me.GastosToolStripMenuItem.Name = "GastosToolStripMenuItem"
        Me.GastosToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.GastosToolStripMenuItem.Text = "Gastos"
        '
        'PrestamosToolStripMenuItem
        '
        Me.PrestamosToolStripMenuItem.Name = "PrestamosToolStripMenuItem"
        Me.PrestamosToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.PrestamosToolStripMenuItem.Text = "Préstamos"
        '
        'GastosPorMovimientosToolStripMenuItem
        '
        Me.GastosPorMovimientosToolStripMenuItem.Name = "GastosPorMovimientosToolStripMenuItem"
        Me.GastosPorMovimientosToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.GastosPorMovimientosToolStripMenuItem.Text = "Gastos Por Tipo de Pago"
        '
        'GastosPorCentroDeCostoToolStripMenuItem
        '
        Me.GastosPorCentroDeCostoToolStripMenuItem.Name = "GastosPorCentroDeCostoToolStripMenuItem"
        Me.GastosPorCentroDeCostoToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.GastosPorCentroDeCostoToolStripMenuItem.Text = "Gastos Por Centro de Costo"
        '
        'PagoProveedoresToolStripMenuItem
        '
        Me.PagoProveedoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReportesVariosToolStripMenuItem, Me.PagosPorCategoriaToolStripMenuItem, Me.EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem})
        Me.PagoProveedoresToolStripMenuItem.Name = "PagoProveedoresToolStripMenuItem"
        Me.PagoProveedoresToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.PagoProveedoresToolStripMenuItem.Text = "Pago Proveedores"
        '
        'ReportesVariosToolStripMenuItem
        '
        Me.ReportesVariosToolStripMenuItem.Name = "ReportesVariosToolStripMenuItem"
        Me.ReportesVariosToolStripMenuItem.Size = New System.Drawing.Size(339, 22)
        Me.ReportesVariosToolStripMenuItem.Text = "Reportes Varios"
        '
        'PagosPorCategoriaToolStripMenuItem
        '
        Me.PagosPorCategoriaToolStripMenuItem.Name = "PagosPorCategoriaToolStripMenuItem"
        Me.PagosPorCategoriaToolStripMenuItem.Size = New System.Drawing.Size(339, 22)
        Me.PagosPorCategoriaToolStripMenuItem.Text = "Reporte Pago Proveedores de Colegios y Obras"
        '
        'EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem
        '
        Me.EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem.Name = "EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem"
        Me.EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem.Size = New System.Drawing.Size(339, 22)
        Me.EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem.Text = "Estado de Comprobantes por Centro de Costo"
        '
        'AdToolStripMenuItem
        '
        Me.AdToolStripMenuItem.Name = "AdToolStripMenuItem"
        Me.AdToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.AdToolStripMenuItem.Text = "Administración"
        '
        'DocumentosPendieToolStripMenuItem
        '
        Me.DocumentosPendieToolStripMenuItem.Name = "DocumentosPendieToolStripMenuItem"
        Me.DocumentosPendieToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.DocumentosPendieToolStripMenuItem.Text = "Gestión de Documentos Pendientes"
        '
        'ListaDeRetencionesToolStripMenuItem
        '
        Me.ListaDeRetencionesToolStripMenuItem.Name = "ListaDeRetencionesToolStripMenuItem"
        Me.ListaDeRetencionesToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.ListaDeRetencionesToolStripMenuItem.Text = "Lista de Retenciones"
        '
        'ContabilidadToolStripMenuItem
        '
        Me.ContabilidadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem})
        Me.ContabilidadToolStripMenuItem.Name = "ContabilidadToolStripMenuItem"
        Me.ContabilidadToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.ContabilidadToolStripMenuItem.Text = "Contabilidad"
        '
        'GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem
        '
        Me.GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem.Name = "GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem"
        Me.GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem.Size = New System.Drawing.Size(324, 22)
        Me.GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem.Text = "Gastos Por Centro de Costo y Tipo de Gastos"
        '
        'PolizasToolStripMenuItem
        '
        Me.PolizasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PolizasPorPagarToolStripMenuItem, Me.DetalleDePolizasToolStripMenuItem, Me.CuotasCanceladasToolStripMenuItem, Me.CuotasVencidasPorVencerToolStripMenuItem})
        Me.PolizasToolStripMenuItem.Name = "PolizasToolStripMenuItem"
        Me.PolizasToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.PolizasToolStripMenuItem.Text = "Polizas"
        '
        'PolizasPorPagarToolStripMenuItem
        '
        Me.PolizasPorPagarToolStripMenuItem.Name = "PolizasPorPagarToolStripMenuItem"
        Me.PolizasPorPagarToolStripMenuItem.Size = New System.Drawing.Size(239, 22)
        Me.PolizasPorPagarToolStripMenuItem.Text = "Polizas por Pagar"
        '
        'DetalleDePolizasToolStripMenuItem
        '
        Me.DetalleDePolizasToolStripMenuItem.Name = "DetalleDePolizasToolStripMenuItem"
        Me.DetalleDePolizasToolStripMenuItem.Size = New System.Drawing.Size(239, 22)
        Me.DetalleDePolizasToolStripMenuItem.Text = "Detalle de Polizas"
        '
        'CuotasCanceladasToolStripMenuItem
        '
        Me.CuotasCanceladasToolStripMenuItem.Name = "CuotasCanceladasToolStripMenuItem"
        Me.CuotasCanceladasToolStripMenuItem.Size = New System.Drawing.Size(239, 22)
        Me.CuotasCanceladasToolStripMenuItem.Text = "Cuotas Canceladas"
        '
        'CuotasVencidasPorVencerToolStripMenuItem
        '
        Me.CuotasVencidasPorVencerToolStripMenuItem.Name = "CuotasVencidasPorVencerToolStripMenuItem"
        Me.CuotasVencidasPorVencerToolStripMenuItem.Size = New System.Drawing.Size(239, 22)
        Me.CuotasVencidasPorVencerToolStripMenuItem.Text = "Cuotas Vencidas / Por Vencer"
        '
        'GastosEnviadosToolStripMenuItem
        '
        Me.GastosEnviadosToolStripMenuItem.Name = "GastosEnviadosToolStripMenuItem"
        Me.GastosEnviadosToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.GastosEnviadosToolStripMenuItem.Text = "Documentos Enviados a Contabilidad"
        '
        'GestiónDeDocumentosPendientesToolStripMenuItem
        '
        Me.GestiónDeDocumentosPendientesToolStripMenuItem.Name = "GestiónDeDocumentosPendientesToolStripMenuItem"
        Me.GestiónDeDocumentosPendientesToolStripMenuItem.Size = New System.Drawing.Size(281, 22)
        Me.GestiónDeDocumentosPendientesToolStripMenuItem.Text = "Gestión de Documentos Pendientes"
        '
        'ProgramaciónDePagosToolStripMenuItem1
        '
        Me.ProgramaciónDePagosToolStripMenuItem1.Name = "ProgramaciónDePagosToolStripMenuItem1"
        Me.ProgramaciónDePagosToolStripMenuItem1.Size = New System.Drawing.Size(281, 22)
        Me.ProgramaciónDePagosToolStripMenuItem1.Text = "Programación de Pagos"
        '
        'tsmiProcesos
        '
        Me.tsmiProcesos.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OperacionesRealizadasToolStripMenuItem, Me.ControlAdministraciónToolStripMenuItem1, Me.ControlContabilidadToolStripMenuItem, Me.RetencionesToolStripMenuItem, Me.MovimientosPorUsuarioToolStripMenuItem, Me.EstadoDeComprobantesToolStripMenuItem, Me.Cigarra2ToolStripMenuItem, Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem, Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1, Me.MovimientosPorCuentaToolStripMenuItem})
        Me.tsmiProcesos.Image = CType(resources.GetObject("tsmiProcesos.Image"), System.Drawing.Image)
        Me.tsmiProcesos.Name = "tsmiProcesos"
        Me.tsmiProcesos.Size = New System.Drawing.Size(90, 33)
        Me.tsmiProcesos.Text = "&Consultas"
        '
        'OperacionesRealizadasToolStripMenuItem
        '
        Me.OperacionesRealizadasToolStripMenuItem.Name = "OperacionesRealizadasToolStripMenuItem"
        Me.OperacionesRealizadasToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.OperacionesRealizadasToolStripMenuItem.Text = "Operaciones Registradas"
        '
        'ControlAdministraciónToolStripMenuItem1
        '
        Me.ControlAdministraciónToolStripMenuItem1.Name = "ControlAdministraciónToolStripMenuItem1"
        Me.ControlAdministraciónToolStripMenuItem1.Size = New System.Drawing.Size(304, 22)
        Me.ControlAdministraciónToolStripMenuItem1.Text = "Rendición de Documentos"
        '
        'ControlContabilidadToolStripMenuItem
        '
        Me.ControlContabilidadToolStripMenuItem.Name = "ControlContabilidadToolStripMenuItem"
        Me.ControlContabilidadToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.ControlContabilidadToolStripMenuItem.Text = "Cargo Contabilidad"
        '
        'RetencionesToolStripMenuItem
        '
        Me.RetencionesToolStripMenuItem.Name = "RetencionesToolStripMenuItem"
        Me.RetencionesToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.RetencionesToolStripMenuItem.Text = "Retenciones"
        '
        'MovimientosPorUsuarioToolStripMenuItem
        '
        Me.MovimientosPorUsuarioToolStripMenuItem.Name = "MovimientosPorUsuarioToolStripMenuItem"
        Me.MovimientosPorUsuarioToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.MovimientosPorUsuarioToolStripMenuItem.Text = "Movimientos Por Usuario"
        '
        'EstadoDeComprobantesToolStripMenuItem
        '
        Me.EstadoDeComprobantesToolStripMenuItem.Name = "EstadoDeComprobantesToolStripMenuItem"
        Me.EstadoDeComprobantesToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.EstadoDeComprobantesToolStripMenuItem.Text = "Estado de Comprobantes"
        '
        'Cigarra2ToolStripMenuItem
        '
        Me.Cigarra2ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VencimientoDePolizasToolStripMenuItem, Me.AseguradosPorObra2ToolStripMenuItem})
        Me.Cigarra2ToolStripMenuItem.Name = "Cigarra2ToolStripMenuItem"
        Me.Cigarra2ToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.Cigarra2ToolStripMenuItem.Text = "Polizas"
        '
        'VencimientoDePolizasToolStripMenuItem
        '
        Me.VencimientoDePolizasToolStripMenuItem.Name = "VencimientoDePolizasToolStripMenuItem"
        Me.VencimientoDePolizasToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.VencimientoDePolizasToolStripMenuItem.Text = "Vencimiento de Polizas"
        '
        'AseguradosPorObra2ToolStripMenuItem
        '
        Me.AseguradosPorObra2ToolStripMenuItem.Name = "AseguradosPorObra2ToolStripMenuItem"
        Me.AseguradosPorObra2ToolStripMenuItem.Size = New System.Drawing.Size(243, 22)
        Me.AseguradosPorObra2ToolStripMenuItem.Text = "Bienes y Asegurados por Obra"
        '
        'ConsultaDeComprobantesDeCajaChicaToolStripMenuItem
        '
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem.Name = "ConsultaDeComprobantesDeCajaChicaToolStripMenuItem"
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem.Text = "Consulta de Comprobantes de Caja Chica"
        '
        'ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1
        '
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1.Name = "ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1"
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1.Size = New System.Drawing.Size(304, 22)
        Me.ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1.Text = "Administración de Caja Chica"
        '
        'MovimientosPorCuentaToolStripMenuItem
        '
        Me.MovimientosPorCuentaToolStripMenuItem.Name = "MovimientosPorCuentaToolStripMenuItem"
        Me.MovimientosPorCuentaToolStripMenuItem.Size = New System.Drawing.Size(304, 22)
        Me.MovimientosPorCuentaToolStripMenuItem.Text = "Ingresos y Egresos del Mes Por Cuenta"
        '
        'tsmiUtilitarios
        '
        Me.tsmiUtilitarios.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ControlDeContratosYValorizacionesToolStripMenuItem})
        Me.tsmiUtilitarios.Image = CType(resources.GetObject("tsmiUtilitarios.Image"), System.Drawing.Image)
        Me.tsmiUtilitarios.Name = "tsmiUtilitarios"
        Me.tsmiUtilitarios.Size = New System.Drawing.Size(90, 33)
        Me.tsmiUtilitarios.Text = "&Utilitarios"
        '
        'ControlDeContratosYValorizacionesToolStripMenuItem
        '
        Me.ControlDeContratosYValorizacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ParteDiarioDeContToolStripMenuItem, Me.ValorizacionesSemanalesToolStripMenuItem, Me.OperacionesVariasAdelantosFGAdendasToolStripMenuItem})
        Me.ControlDeContratosYValorizacionesToolStripMenuItem.Name = "ControlDeContratosYValorizacionesToolStripMenuItem"
        Me.ControlDeContratosYValorizacionesToolStripMenuItem.Size = New System.Drawing.Size(285, 22)
        Me.ControlDeContratosYValorizacionesToolStripMenuItem.Text = "Control de Contratos y Valorizaciones"
        '
        'ParteDiarioDeContToolStripMenuItem
        '
        Me.ParteDiarioDeContToolStripMenuItem.Name = "ParteDiarioDeContToolStripMenuItem"
        Me.ParteDiarioDeContToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.ParteDiarioDeContToolStripMenuItem.Text = "Parte Diario de Contratos x Partida"
        '
        'ValorizacionesSemanalesToolStripMenuItem
        '
        Me.ValorizacionesSemanalesToolStripMenuItem.Name = "ValorizacionesSemanalesToolStripMenuItem"
        Me.ValorizacionesSemanalesToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.ValorizacionesSemanalesToolStripMenuItem.Text = "Valorizaciones x Contrato"
        '
        'OperacionesVariasAdelantosFGAdendasToolStripMenuItem
        '
        Me.OperacionesVariasAdelantosFGAdendasToolStripMenuItem.Name = "OperacionesVariasAdelantosFGAdendasToolStripMenuItem"
        Me.OperacionesVariasAdelantosFGAdendasToolStripMenuItem.Size = New System.Drawing.Size(337, 22)
        Me.OperacionesVariasAdelantosFGAdendasToolStripMenuItem.Text = "Operaciones Varias (Adelantos - FG - Adendas)"
        '
        'tsmiHerramientas
        '
        Me.tsmiHerramientas.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UsuariosToolStripMenuItem, Me.CajasToolStripMenuItem, Me.RetencionesToolStripMenuItem1, Me.ValorizacionesToolStripMenuItem})
        Me.tsmiHerramientas.Image = CType(resources.GetObject("tsmiHerramientas.Image"), System.Drawing.Image)
        Me.tsmiHerramientas.Name = "tsmiHerramientas"
        Me.tsmiHerramientas.Size = New System.Drawing.Size(112, 33)
        Me.tsmiHerramientas.Text = "&Configuración"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'CajasToolStripMenuItem
        '
        Me.CajasToolStripMenuItem.Name = "CajasToolStripMenuItem"
        Me.CajasToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.CajasToolStripMenuItem.Text = "Cajas Chicas"
        '
        'RetencionesToolStripMenuItem1
        '
        Me.RetencionesToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ParametrosToolStripMenuItem, Me.SeriesRetencionesToolStripMenuItem1})
        Me.RetencionesToolStripMenuItem1.Name = "RetencionesToolStripMenuItem1"
        Me.RetencionesToolStripMenuItem1.Size = New System.Drawing.Size(155, 22)
        Me.RetencionesToolStripMenuItem1.Text = "Retenciones"
        '
        'ParametrosToolStripMenuItem
        '
        Me.ParametrosToolStripMenuItem.Name = "ParametrosToolStripMenuItem"
        Me.ParametrosToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.ParametrosToolStripMenuItem.Text = "Parámetros"
        '
        'SeriesRetencionesToolStripMenuItem1
        '
        Me.SeriesRetencionesToolStripMenuItem1.Name = "SeriesRetencionesToolStripMenuItem1"
        Me.SeriesRetencionesToolStripMenuItem1.Size = New System.Drawing.Size(182, 22)
        Me.SeriesRetencionesToolStripMenuItem1.Text = "Series Retenciones"
        '
        'ValorizacionesToolStripMenuItem
        '
        Me.ValorizacionesToolStripMenuItem.Name = "ValorizacionesToolStripMenuItem"
        Me.ValorizacionesToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.ValorizacionesToolStripMenuItem.Text = "Valorizaciones"
        '
        'tsmiVentana
        '
        Me.tsmiVentana.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem})
        Me.tsmiVentana.Image = CType(resources.GetObject("tsmiVentana.Image"), System.Drawing.Image)
        Me.tsmiVentana.Name = "tsmiVentana"
        Me.tsmiVentana.Size = New System.Drawing.Size(82, 33)
        Me.tsmiVentana.Text = "&Ventana"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'tsmiAyuda
        '
        Me.tsmiAyuda.Image = CType(resources.GetObject("tsmiAyuda.Image"), System.Drawing.Image)
        Me.tsmiAyuda.Name = "tsmiAyuda"
        Me.tsmiAyuda.Size = New System.Drawing.Size(71, 33)
        Me.tsmiAyuda.Text = "&Ayuda"
        '
        'tsmiSalir
        '
        Me.tsmiSalir.Image = CType(resources.GetObject("tsmiSalir.Image"), System.Drawing.Image)
        Me.tsmiSalir.Name = "tsmiSalir"
        Me.tsmiSalir.Size = New System.Drawing.Size(60, 33)
        Me.tsmiSalir.Text = "&Salir"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "Alpha Dista Icon 73.ico")
        Me.ImageList1.Images.SetKeyName(1, "1 Fish.ico")
        Me.ImageList1.Images.SetKeyName(2, "1.ico")
        Me.ImageList1.Images.SetKeyName(3, "2 Fish.ico")
        Me.ImageList1.Images.SetKeyName(4, "2.ico")
        Me.ImageList1.Images.SetKeyName(5, "3.ico")
        Me.ImageList1.Images.SetKeyName(6, "4.ico")
        Me.ImageList1.Images.SetKeyName(7, "5.ico")
        Me.ImageList1.Images.SetKeyName(8, "6.ico")
        Me.ImageList1.Images.SetKeyName(9, "7.ico")
        Me.ImageList1.Images.SetKeyName(10, "7Zip.ico")
        Me.ImageList1.Images.SetKeyName(11, "7-Zip.ico")
        Me.ImageList1.Images.SetKeyName(12, "8.ico")
        Me.ImageList1.Images.SetKeyName(13, "9.ico")
        Me.ImageList1.Images.SetKeyName(14, "10.ico")
        Me.ImageList1.Images.SetKeyName(15, "11.ico")
        Me.ImageList1.Images.SetKeyName(16, "12.ico")
        Me.ImageList1.Images.SetKeyName(17, "13.ico")
        Me.ImageList1.Images.SetKeyName(18, "14.ico")
        Me.ImageList1.Images.SetKeyName(19, "15.ico")
        Me.ImageList1.Images.SetKeyName(20, "16.ico")
        Me.ImageList1.Images.SetKeyName(21, "17.ico")
        Me.ImageList1.Images.SetKeyName(22, "Acces.ico")
        Me.ImageList1.Images.SetKeyName(23, "Accessibilité.ico")
        Me.ImageList1.Images.SetKeyName(24, "Accessibility.ico")
        Me.ImageList1.Images.SetKeyName(25, "ActiveSync.ico")
        Me.ImageList1.Images.SetKeyName(26, "AdAware.ico")
        Me.ImageList1.Images.SetKeyName(27, "Add Folder.ico")
        Me.ImageList1.Images.SetKeyName(28, "Add.ico")
        Me.ImageList1.Images.SetKeyName(29, "addfolder.ico")
        Me.ImageList1.Images.SetKeyName(30, "Address Book.ico")
        Me.ImageList1.Images.SetKeyName(31, "Address-Book.ico")
        Me.ImageList1.Images.SetKeyName(32, "Adium.ico")
        Me.ImageList1.Images.SetKeyName(33, "Adobe Reader File.ico")
        Me.ImageList1.Images.SetKeyName(34, "Adobe.ico")
        Me.ImageList1.Images.SetKeyName(35, "Aim.ico")
        Me.ImageList1.Images.SetKeyName(36, "Ajouter.ico")
        Me.ImageList1.Images.SetKeyName(37, "Alarm.ico")
        Me.ImageList1.Images.SetKeyName(38, "Alcohol120.ico")
        Me.ImageList1.Images.SetKeyName(39, "Alert.ico")
        Me.ImageList1.Images.SetKeyName(40, "Alpha Dista (Grey) Icon 01.ico")
        Me.ImageList1.Images.SetKeyName(41, "Alpha Dista (Grey) Icon 02.ico")
        Me.ImageList1.Images.SetKeyName(42, "Alpha Dista (Grey) Icon 03.ico")
        Me.ImageList1.Images.SetKeyName(43, "Alpha Dista (Grey) Icon 04.ico")
        Me.ImageList1.Images.SetKeyName(44, "Alpha Dista (Grey) Icon 05.ico")
        Me.ImageList1.Images.SetKeyName(45, "Alpha Dista (Grey) Icon 06.ico")
        Me.ImageList1.Images.SetKeyName(46, "Alpha Dista (Grey) Icon 07.ico")
        Me.ImageList1.Images.SetKeyName(47, "Alpha Dista (Grey) Icon 08.ico")
        Me.ImageList1.Images.SetKeyName(48, "Alpha Dista (Grey) Icon 09.ico")
        Me.ImageList1.Images.SetKeyName(49, "Alpha Dista (Grey) Icon 10.ico")
        Me.ImageList1.Images.SetKeyName(50, "Alpha Dista (Grey) Icon 11.ico")
        Me.ImageList1.Images.SetKeyName(51, "Alpha Dista (Grey) Icon 12.ico")
        Me.ImageList1.Images.SetKeyName(52, "Alpha Dista (Grey) Icon 13.ico")
        Me.ImageList1.Images.SetKeyName(53, "Alpha Dista (Grey) Icon 14.ico")
        Me.ImageList1.Images.SetKeyName(54, "Alpha Dista (Grey) Icon 15.ico")
        Me.ImageList1.Images.SetKeyName(55, "Alpha Dista (Grey) Icon 16.ico")
        Me.ImageList1.Images.SetKeyName(56, "Alpha Dista (Grey) Icon 17.ico")
        Me.ImageList1.Images.SetKeyName(57, "Alpha Dista (Grey) Icon 18.ico")
        Me.ImageList1.Images.SetKeyName(58, "Alpha Dista (Grey) Icon 19.ico")
        Me.ImageList1.Images.SetKeyName(59, "Alpha Dista (Grey) Icon 20.ico")
        Me.ImageList1.Images.SetKeyName(60, "Alpha Dista (Grey) Icon 21.ico")
        Me.ImageList1.Images.SetKeyName(61, "Alpha Dista (Grey) Icon 22.ico")
        Me.ImageList1.Images.SetKeyName(62, "Alpha Dista (Grey) Icon 23.ico")
        Me.ImageList1.Images.SetKeyName(63, "Alpha Dista (Grey) Icon 24.ico")
        Me.ImageList1.Images.SetKeyName(64, "Alpha Dista (Grey) Icon 25.ico")
        Me.ImageList1.Images.SetKeyName(65, "Alpha Dista (Grey) Icon 26.ico")
        Me.ImageList1.Images.SetKeyName(66, "Alpha Dista (Grey) Icon 27.ico")
        Me.ImageList1.Images.SetKeyName(67, "Alpha Dista (Grey) Icon 28.ico")
        Me.ImageList1.Images.SetKeyName(68, "Alpha Dista (Grey) Icon 29.ico")
        Me.ImageList1.Images.SetKeyName(69, "Alpha Dista (Grey) Icon 30.ico")
        Me.ImageList1.Images.SetKeyName(70, "Alpha Dista (Grey) Icon 31.ico")
        Me.ImageList1.Images.SetKeyName(71, "Alpha Dista (Grey) Icon 32.ico")
        Me.ImageList1.Images.SetKeyName(72, "Alpha Dista (Grey) Icon 33.ico")
        Me.ImageList1.Images.SetKeyName(73, "Alpha Dista (Grey) Icon 34.ico")
        Me.ImageList1.Images.SetKeyName(74, "Alpha Dista (Grey) Icon 35.ico")
        Me.ImageList1.Images.SetKeyName(75, "Alpha Dista (Grey) Icon 36.ico")
        Me.ImageList1.Images.SetKeyName(76, "Alpha Dista (Grey) Icon 37.ico")
        Me.ImageList1.Images.SetKeyName(77, "Alpha Dista (Grey) Icon 38.ico")
        Me.ImageList1.Images.SetKeyName(78, "Alpha Dista (Grey) Icon 39.ico")
        Me.ImageList1.Images.SetKeyName(79, "Alpha Dista (Grey) Icon 40.ico")
        Me.ImageList1.Images.SetKeyName(80, "Alpha Dista (Grey) Icon 41.ico")
        Me.ImageList1.Images.SetKeyName(81, "Alpha Dista (Grey) Icon 42.ico")
        Me.ImageList1.Images.SetKeyName(82, "Alpha Dista (Grey) Icon 43.ico")
        Me.ImageList1.Images.SetKeyName(83, "Alpha Dista (Grey) Icon 44.ico")
        Me.ImageList1.Images.SetKeyName(84, "Alpha Dista (Grey) Icon 45.ico")
        Me.ImageList1.Images.SetKeyName(85, "Alpha Dista (Grey) Icon 46.ico")
        Me.ImageList1.Images.SetKeyName(86, "Alpha Dista (Grey) Icon 47.ico")
        Me.ImageList1.Images.SetKeyName(87, "Alpha Dista (Grey) Icon 48.ico")
        Me.ImageList1.Images.SetKeyName(88, "Alpha Dista (Grey) Icon 49.ico")
        Me.ImageList1.Images.SetKeyName(89, "Alpha Dista (Grey) Icon 50.ico")
        Me.ImageList1.Images.SetKeyName(90, "Alpha Dista Icon 01.ico")
        Me.ImageList1.Images.SetKeyName(91, "Alpha Dista Icon 02.ico")
        Me.ImageList1.Images.SetKeyName(92, "Alpha Dista Icon 03.ico")
        Me.ImageList1.Images.SetKeyName(93, "Alpha Dista Icon 04.ico")
        Me.ImageList1.Images.SetKeyName(94, "Alpha Dista Icon 05.ico")
        Me.ImageList1.Images.SetKeyName(95, "Alpha Dista Icon 06.ico")
        Me.ImageList1.Images.SetKeyName(96, "Alpha Dista Icon 07.ico")
        Me.ImageList1.Images.SetKeyName(97, "Alpha Dista Icon 08.ico")
        Me.ImageList1.Images.SetKeyName(98, "Alpha Dista Icon 09.ico")
        Me.ImageList1.Images.SetKeyName(99, "Alpha Dista Icon 10.ico")
        Me.ImageList1.Images.SetKeyName(100, "Alpha Dista Icon 13.ico")
        Me.ImageList1.Images.SetKeyName(101, "Alpha Dista Icon 14.ico")
        Me.ImageList1.Images.SetKeyName(102, "Alpha Dista Icon 15.ico")
        Me.ImageList1.Images.SetKeyName(103, "Alpha Dista Icon 16.ico")
        Me.ImageList1.Images.SetKeyName(104, "Alpha Dista Icon 17.ico")
        Me.ImageList1.Images.SetKeyName(105, "Alpha Dista Icon 18.ico")
        Me.ImageList1.Images.SetKeyName(106, "Alpha Dista Icon 19.ico")
        Me.ImageList1.Images.SetKeyName(107, "Alpha Dista Icon 20.ico")
        Me.ImageList1.Images.SetKeyName(108, "Alpha Dista Icon 21.ico")
        Me.ImageList1.Images.SetKeyName(109, "Alpha Dista Icon 22.ico")
        Me.ImageList1.Images.SetKeyName(110, "Alpha Dista Icon 23.ico")
        Me.ImageList1.Images.SetKeyName(111, "Alpha Dista Icon 23.png")
        Me.ImageList1.Images.SetKeyName(112, "Alpha Dista Icon 24.ico")
        Me.ImageList1.Images.SetKeyName(113, "Alpha Dista Icon 25.ico")
        Me.ImageList1.Images.SetKeyName(114, "Alpha Dista Icon 26.ico")
        Me.ImageList1.Images.SetKeyName(115, "Alpha Dista Icon 27.ico")
        Me.ImageList1.Images.SetKeyName(116, "Alpha Dista Icon 28.ico")
        Me.ImageList1.Images.SetKeyName(117, "Alpha Dista Icon 28.png")
        Me.ImageList1.Images.SetKeyName(118, "Alpha Dista Icon 29.ico")
        Me.ImageList1.Images.SetKeyName(119, "Alpha Dista Icon 30.ico")
        Me.ImageList1.Images.SetKeyName(120, "Alpha Dista Icon 31.ico")
        Me.ImageList1.Images.SetKeyName(121, "Alpha Dista Icon 32.ico")
        Me.ImageList1.Images.SetKeyName(122, "Alpha Dista Icon 33.ico")
        Me.ImageList1.Images.SetKeyName(123, "Alpha Dista Icon 34.ico")
        Me.ImageList1.Images.SetKeyName(124, "Alpha Dista Icon 35.ico")
        Me.ImageList1.Images.SetKeyName(125, "Alpha Dista Icon 36.ico")
        Me.ImageList1.Images.SetKeyName(126, "Alpha Dista Icon 37.ico")
        Me.ImageList1.Images.SetKeyName(127, "Alpha Dista Icon 38.ico")
        Me.ImageList1.Images.SetKeyName(128, "Alpha Dista Icon 39.ico")
        Me.ImageList1.Images.SetKeyName(129, "Alpha Dista Icon 40.ico")
        Me.ImageList1.Images.SetKeyName(130, "Alpha Dista Icon 40.png")
        Me.ImageList1.Images.SetKeyName(131, "Alpha Dista Icon 41.ico")
        Me.ImageList1.Images.SetKeyName(132, "Alpha Dista Icon 42.ico")
        Me.ImageList1.Images.SetKeyName(133, "Alpha Dista Icon 43.ico")
        Me.ImageList1.Images.SetKeyName(134, "Alpha Dista Icon 44.ico")
        Me.ImageList1.Images.SetKeyName(135, "Alpha Dista Icon 45.ico")
        Me.ImageList1.Images.SetKeyName(136, "Alpha Dista Icon 46.ico")
        Me.ImageList1.Images.SetKeyName(137, "Alpha Dista Icon 47.ico")
        Me.ImageList1.Images.SetKeyName(138, "Alpha Dista Icon 48.ico")
        Me.ImageList1.Images.SetKeyName(139, "Alpha Dista Icon 49.ico")
        Me.ImageList1.Images.SetKeyName(140, "Alpha Dista Icon 50.ico")
        Me.ImageList1.Images.SetKeyName(141, "Alpha Dista Icon 51.ico")
        Me.ImageList1.Images.SetKeyName(142, "Alpha Dista Icon 52.ico")
        Me.ImageList1.Images.SetKeyName(143, "Alpha Dista Icon 53.ico")
        Me.ImageList1.Images.SetKeyName(144, "Alpha Dista Icon 54.ico")
        Me.ImageList1.Images.SetKeyName(145, "Alpha Dista Icon 55.ico")
        Me.ImageList1.Images.SetKeyName(146, "Alpha Dista Icon 56.ico")
        Me.ImageList1.Images.SetKeyName(147, "Alpha Dista Icon 57.ico")
        Me.ImageList1.Images.SetKeyName(148, "Alpha Dista Icon 58.ico")
        Me.ImageList1.Images.SetKeyName(149, "Alpha Dista Icon 59.ico")
        Me.ImageList1.Images.SetKeyName(150, "Alpha Dista Icon 60.ico")
        Me.ImageList1.Images.SetKeyName(151, "Alpha Dista Icon 61.ico")
        Me.ImageList1.Images.SetKeyName(152, "Alpha Dista Icon 62.ico")
        Me.ImageList1.Images.SetKeyName(153, "Alpha Dista Icon 63.ico")
        Me.ImageList1.Images.SetKeyName(154, "Alpha Dista Icon 64.ico")
        Me.ImageList1.Images.SetKeyName(155, "Alpha Dista Icon 65.ico")
        Me.ImageList1.Images.SetKeyName(156, "Alpha Dista Icon 66.ico")
        Me.ImageList1.Images.SetKeyName(157, "Alpha Dista Icon 67.ico")
        Me.ImageList1.Images.SetKeyName(158, "Alpha Dista Icon 68.ico")
        Me.ImageList1.Images.SetKeyName(159, "Alpha Dista Icon 69.ico")
        Me.ImageList1.Images.SetKeyName(160, "Alpha Dista Icon 70.ico")
        Me.ImageList1.Images.SetKeyName(161, "Alpha Dista Icon 71.ico")
        Me.ImageList1.Images.SetKeyName(162, "Alpha Dista Icon 72.ico")
        '
        'Barra
        '
        Me.Barra.AutoSize = False
        Me.Barra.BackColor = System.Drawing.Color.LightSlateGray
        Me.Barra.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Nuevo, Me.Modifica, Me.Elimina, Me.Graba, Me.Cancela, Me.ToolStripSeparator1, Me.ToolStripSeparator20, Me.ToolStripSeparator9, Me.Buscar, Me.Exporta, Me.Imprime, Me.Visualiza, Me.ToolStripSeparator2, Me.Salir})
        Me.Barra.Location = New System.Drawing.Point(0, 37)
        Me.Barra.Name = "Barra"
        Me.Barra.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.Barra.Size = New System.Drawing.Size(933, 37)
        Me.Barra.TabIndex = 30
        Me.Barra.Text = "ToolStrip"
        '
        'Nuevo
        '
        Me.Nuevo.Image = CType(resources.GetObject("Nuevo.Image"), System.Drawing.Image)
        Me.Nuevo.ImageTransparentColor = System.Drawing.Color.Black
        Me.Nuevo.Name = "Nuevo"
        Me.Nuevo.Size = New System.Drawing.Size(83, 34)
        Me.Nuevo.Text = "Nuevo : F2"
        Me.Nuevo.ToolTipText = "Nuevo : F2"
        '
        'Modifica
        '
        Me.Modifica.Image = CType(resources.GetObject("Modifica.Image"), System.Drawing.Image)
        Me.Modifica.ImageTransparentColor = System.Drawing.Color.Black
        Me.Modifica.Name = "Modifica"
        Me.Modifica.Size = New System.Drawing.Size(99, 34)
        Me.Modifica.Text = "Modificar : F3"
        Me.Modifica.ToolTipText = "Modificar : F3"
        '
        'Elimina
        '
        Me.Elimina.Image = CType(resources.GetObject("Elimina.Image"), System.Drawing.Image)
        Me.Elimina.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Elimina.Name = "Elimina"
        Me.Elimina.Size = New System.Drawing.Size(103, 34)
        Me.Elimina.Text = "Eliminar : Supr"
        Me.Elimina.ToolTipText = "Eliminar : Supr"
        '
        'Graba
        '
        Me.Graba.Image = CType(resources.GetObject("Graba.Image"), System.Drawing.Image)
        Me.Graba.ImageTransparentColor = System.Drawing.Color.Black
        Me.Graba.Name = "Graba"
        Me.Graba.Size = New System.Drawing.Size(90, 34)
        Me.Graba.Text = "Guardar : F5"
        '
        'Cancela
        '
        Me.Cancela.Image = CType(resources.GetObject("Cancela.Image"), System.Drawing.Image)
        Me.Cancela.ImageTransparentColor = System.Drawing.Color.Black
        Me.Cancela.Name = "Cancela"
        Me.Cancela.Size = New System.Drawing.Size(94, 34)
        Me.Cancela.Text = "Cancelar : F9"
        Me.Cancela.ToolTipText = "Cancelar : F9"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 37)
        '
        'ToolStripSeparator20
        '
        Me.ToolStripSeparator20.Name = "ToolStripSeparator20"
        Me.ToolStripSeparator20.Size = New System.Drawing.Size(6, 37)
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 37)
        '
        'Buscar
        '
        Me.Buscar.DoubleClickEnabled = True
        Me.Buscar.Image = CType(resources.GetObject("Buscar.Image"), System.Drawing.Image)
        Me.Buscar.ImageTransparentColor = System.Drawing.Color.Black
        Me.Buscar.Name = "Buscar"
        Me.Buscar.Size = New System.Drawing.Size(86, 34)
        Me.Buscar.Text = "Buscar : F4 "
        Me.Buscar.ToolTipText = "Buscar : F4 "
        '
        'Exporta
        '
        Me.Exporta.Image = CType(resources.GetObject("Exporta.Image"), System.Drawing.Image)
        Me.Exporta.ImageTransparentColor = System.Drawing.Color.Black
        Me.Exporta.Name = "Exporta"
        Me.Exporta.Size = New System.Drawing.Size(91, 34)
        Me.Exporta.Text = "Exportar : F6"
        Me.Exporta.ToolTipText = "Exportar : F6"
        '
        'Imprime
        '
        Me.Imprime.Image = CType(resources.GetObject("Imprime.Image"), System.Drawing.Image)
        Me.Imprime.ImageTransparentColor = System.Drawing.Color.Black
        Me.Imprime.Name = "Imprime"
        Me.Imprime.Size = New System.Drawing.Size(94, 34)
        Me.Imprime.Text = "Imprimir : F7"
        '
        'Visualiza
        '
        Me.Visualiza.Image = CType(resources.GetObject("Visualiza.Image"), System.Drawing.Image)
        Me.Visualiza.ImageTransparentColor = System.Drawing.Color.Black
        Me.Visualiza.Name = "Visualiza"
        Me.Visualiza.Size = New System.Drawing.Size(97, 34)
        Me.Visualiza.Text = "Visualizar : F8"
        Me.Visualiza.ToolTipText = "Visualizar : F8"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 37)
        '
        'Salir
        '
        Me.Salir.Image = CType(resources.GetObject("Salir.Image"), System.Drawing.Image)
        Me.Salir.ImageTransparentColor = System.Drawing.Color.Black
        Me.Salir.Name = "Salir"
        Me.Salir.Size = New System.Drawing.Size(75, 20)
        Me.Salir.Text = "Salir : Esc"
        Me.Salir.ToolTipText = "Salir : Esc"
        '
        'BarraSecundaria
        '
        Me.BarraSecundaria.AutoSize = False
        Me.BarraSecundaria.BackColor = System.Drawing.Color.LightSlateGray
        Me.BarraSecundaria.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarraSecundaria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarraSecundaria.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripSeparator5, Me.ToolStripSeparator4, Me.btnNuevo, Me.ToolStripSeparator7, Me.ToolStripSeparator12, Me.btnModificar, Me.ToolStripSeparator6, Me.ToolStripSeparator11, Me.btnEliminar, Me.ToolStripSeparator3, Me.ToolStripSeparator10})
        Me.BarraSecundaria.Location = New System.Drawing.Point(0, 74)
        Me.BarraSecundaria.Name = "BarraSecundaria"
        Me.BarraSecundaria.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.BarraSecundaria.Size = New System.Drawing.Size(41, 357)
        Me.BarraSecundaria.TabIndex = 31
        Me.BarraSecundaria.Text = "ToolStrip"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(39, 6)
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(39, 6)
        '
        'btnNuevo
        '
        Me.btnNuevo.AutoSize = False
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Black
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(39, 80)
        Me.btnNuevo.Text = "Nuevo : F2"
        Me.btnNuevo.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical270
        Me.btnNuevo.ToolTipText = "Insertar : F2"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(39, 6)
        '
        'ToolStripSeparator12
        '
        Me.ToolStripSeparator12.Name = "ToolStripSeparator12"
        Me.ToolStripSeparator12.Size = New System.Drawing.Size(39, 6)
        '
        'btnModificar
        '
        Me.btnModificar.AutoSize = False
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageTransparentColor = System.Drawing.Color.Black
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(39, 80)
        Me.btnModificar.Text = "Modificar : F3"
        Me.btnModificar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical270
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(39, 6)
        '
        'ToolStripSeparator11
        '
        Me.ToolStripSeparator11.Name = "ToolStripSeparator11"
        Me.ToolStripSeparator11.Size = New System.Drawing.Size(39, 6)
        '
        'btnEliminar
        '
        Me.btnEliminar.AutoSize = False
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(39, 100)
        Me.btnEliminar.Text = " Eliminar : Supr"
        Me.btnEliminar.TextDirection = System.Windows.Forms.ToolStripTextDirection.Vertical270
        Me.btnEliminar.ToolTipText = "Eliminar : Supr"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(39, 6)
        '
        'ToolStripSeparator10
        '
        Me.ToolStripSeparator10.Name = "ToolStripSeparator10"
        Me.ToolStripSeparator10.Size = New System.Drawing.Size(39, 6)
        '
        'stsEstado
        '
        Me.stsEstado.BackColor = System.Drawing.Color.LightSteelBlue
        Me.stsEstado.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel, Me.toEmpresa, Me.toUsuario, Me.toPeriodo, Me.toPC, Me.toFecha, Me.toVersion})
        Me.stsEstado.Location = New System.Drawing.Point(0, 431)
        Me.stsEstado.Name = "stsEstado"
        Me.stsEstado.Size = New System.Drawing.Size(933, 22)
        Me.stsEstado.TabIndex = 26
        Me.stsEstado.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Image = CType(resources.GetObject("ToolStripStatusLabel.Image"), System.Drawing.Image)
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(58, 17)
        Me.ToolStripStatusLabel.Text = "Estado"
        '
        'toEmpresa
        '
        Me.toEmpresa.Name = "toEmpresa"
        Me.toEmpresa.Size = New System.Drawing.Size(37, 17)
        Me.toEmpresa.Text = "          "
        Me.toEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'toUsuario
        '
        Me.toUsuario.Name = "toUsuario"
        Me.toUsuario.Size = New System.Drawing.Size(37, 17)
        Me.toUsuario.Text = "          "
        '
        'toPeriodo
        '
        Me.toPeriodo.Name = "toPeriodo"
        Me.toPeriodo.Size = New System.Drawing.Size(37, 17)
        Me.toPeriodo.Text = "          "
        '
        'toPC
        '
        Me.toPC.Name = "toPC"
        Me.toPC.Size = New System.Drawing.Size(37, 17)
        Me.toPC.Text = "          "
        '
        'toFecha
        '
        Me.toFecha.Name = "toFecha"
        Me.toFecha.Size = New System.Drawing.Size(37, 17)
        Me.toFecha.Text = "          "
        '
        'toVersion
        '
        Me.toVersion.Name = "toVersion"
        Me.toVersion.Size = New System.Drawing.Size(0, 17)
        Me.toVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GestionValorizacionesToolStripMenuItem
        '
        Me.GestionValorizacionesToolStripMenuItem.Name = "GestionValorizacionesToolStripMenuItem"
        Me.GestionValorizacionesToolStripMenuItem.Size = New System.Drawing.Size(289, 22)
        Me.GestionValorizacionesToolStripMenuItem.Text = "Gestion Valorizaciones"
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(933, 453)
        Me.Controls.Add(Me.BarraSecundaria)
        Me.Controls.Add(Me.Barra)
        Me.Controls.Add(Me.stsEstado)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPrincipal"
        Me.Text = "    ::  Sistema de Tesorería  ::"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Barra.ResumeLayout(False)
        Me.Barra.PerformLayout()
        Me.BarraSecundaria.ResumeLayout(False)
        Me.BarraSecundaria.PerformLayout()
        Me.stsEstado.ResumeLayout(False)
        Me.stsEstado.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tsmiMoviminetos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCatalogos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents tsmiReportes As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents tsmiUtilitarios As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiHerramientas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiVentana As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiAyuda As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiSalir As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Public WithEvents Barra As System.Windows.Forms.ToolStrip
    Friend WithEvents Nuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents Modifica As System.Windows.Forms.ToolStripButton
    Friend WithEvents Elimina As System.Windows.Forms.ToolStripButton
    Friend WithEvents Graba As System.Windows.Forms.ToolStripButton
    Friend WithEvents Cancela As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Buscar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Exporta As System.Windows.Forms.ToolStripButton
    Friend WithEvents Imprime As System.Windows.Forms.ToolStripButton
    Friend WithEvents Visualiza As System.Windows.Forms.ToolStripButton
    Friend WithEvents Salir As System.Windows.Forms.ToolStripButton
    Public WithEvents BarraSecundaria As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator12 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator11 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator10 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents stsEstado As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripSeparator20 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmiBancos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCaja As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCuenta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCondicionPago As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiCheque As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiChequera As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiRegistroDocumento As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SesionDeCajaChicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoDocumentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoArqueoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagoAProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroBancosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmiProcesos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toEmpresa As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents toUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents toPeriodo As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents toPC As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents toFecha As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrestamosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OperacionesRealizadasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LibroBancosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MantenimientoLibrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaldoBancarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnexosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DepPlazoDeLaEmpresaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlAdministraciónToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlContabilidadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GastosPorMovimientosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GastosPorCentroDeCostoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ITFToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoGastosGrupo1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagoProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CondigurarReportesDePagosAProvToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesVariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagosPorCategoriaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlDeDocumentacionPorCargosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RetencionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RetencionesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ParametrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeriesRetencionesToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoDeComprobantesPorCentroDeCostoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListaDeRetencionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosPorUsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContabilidadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GastosPorCentroDeCostoYTipoDeGastosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratistasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EspecialidadToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoContratistasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratistasToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentasPorPagarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuentaCorrienteProveedorProgramaciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoDeProveedoresToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratistasToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistorialDeContratosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Cigarra1ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Cigarra2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValorizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EstadoDeComprobantesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PolizasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PolizasPorPagarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetalleDePolizasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuotasCanceladasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CuotasVencidasPorVencerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContratosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ControlDeContratosYValorizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ParteDiarioDeContToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ValorizacionesSemanalesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OperacionesVariasAdelantosFGAdendasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents GastosEnviadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VencimientoDePolizasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AseguradosPorObra2ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeComprobantesDeCajaChicaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConsultaDeComprobantesDeCajaChicaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MovimientosPorCuentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProgramaciónCuentasARendirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DerivarPagosDeOrdenDeCompraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCajaExtraordinaria As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSesionCajaChica As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSesionCajaExtraordinaria As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEgresoCajaBancos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuIngresoBancario As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msImpuesto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobranzasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentoDeVentaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ItemsDeVentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CobranzasToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MantenimientoNumeradorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RetencionesToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PagosPlanillasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DetraccionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GestionDeDocumentosPendientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProgramaciónDePagosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GestionDocumentariaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegistroDeRendicionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DocumentosPendieToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MigrarDataToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toVersion As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GestiónDeDocumentosPendientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProgramaciónDePagosToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VistoBuenoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PercepciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GestionValorizacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
