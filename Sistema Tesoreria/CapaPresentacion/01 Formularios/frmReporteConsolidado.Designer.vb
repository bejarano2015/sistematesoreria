<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteConsolidado
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.cboUbicacion = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.cboEspecialidad = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.cboTitulo = New ctrLibreria.Controles.BeComboBox
        Me.chkTodosEsp = New System.Windows.Forms.CheckBox
        Me.chkTodosTit = New System.Windows.Forms.CheckBox
        Me.btnConsultar = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(6, 16)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(105, 13)
        Me.BeLabel13.TabIndex = 13
        Me.BeLabel13.Text = "Ubicación de trabajo"
        '
        'cboUbicacion
        '
        Me.cboUbicacion.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboUbicacion.BackColor = System.Drawing.Color.Ivory
        Me.cboUbicacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUbicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUbicacion.ForeColor = System.Drawing.Color.Black
        Me.cboUbicacion.FormattingEnabled = True
        Me.cboUbicacion.KeyEnter = True
        Me.cboUbicacion.Location = New System.Drawing.Point(122, 8)
        Me.cboUbicacion.Name = "cboUbicacion"
        Me.cboUbicacion.Size = New System.Drawing.Size(239, 21)
        Me.cboUbicacion.TabIndex = 14
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(6, 42)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel1.TabIndex = 15
        Me.BeLabel1.Text = "Especialidad"
        '
        'cboEspecialidad
        '
        Me.cboEspecialidad.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEspecialidad.BackColor = System.Drawing.Color.Ivory
        Me.cboEspecialidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEspecialidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEspecialidad.ForeColor = System.Drawing.Color.Black
        Me.cboEspecialidad.FormattingEnabled = True
        Me.cboEspecialidad.KeyEnter = True
        Me.cboEspecialidad.Location = New System.Drawing.Point(122, 34)
        Me.cboEspecialidad.Name = "cboEspecialidad"
        Me.cboEspecialidad.Size = New System.Drawing.Size(239, 21)
        Me.cboEspecialidad.TabIndex = 16
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(6, 67)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel2.TabIndex = 17
        Me.BeLabel2.Text = "Título"
        '
        'cboTitulo
        '
        Me.cboTitulo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTitulo.BackColor = System.Drawing.Color.Ivory
        Me.cboTitulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTitulo.ForeColor = System.Drawing.Color.Black
        Me.cboTitulo.FormattingEnabled = True
        Me.cboTitulo.KeyEnter = True
        Me.cboTitulo.Location = New System.Drawing.Point(122, 59)
        Me.cboTitulo.Name = "cboTitulo"
        Me.cboTitulo.Size = New System.Drawing.Size(239, 21)
        Me.cboTitulo.TabIndex = 18
        '
        'chkTodosEsp
        '
        Me.chkTodosEsp.AutoSize = True
        Me.chkTodosEsp.Location = New System.Drawing.Point(367, 40)
        Me.chkTodosEsp.Name = "chkTodosEsp"
        Me.chkTodosEsp.Size = New System.Drawing.Size(56, 17)
        Me.chkTodosEsp.TabIndex = 19
        Me.chkTodosEsp.Text = "Todos"
        Me.chkTodosEsp.UseVisualStyleBackColor = True
        '
        'chkTodosTit
        '
        Me.chkTodosTit.AutoSize = True
        Me.chkTodosTit.Location = New System.Drawing.Point(367, 63)
        Me.chkTodosTit.Name = "chkTodosTit"
        Me.chkTodosTit.Size = New System.Drawing.Size(56, 17)
        Me.chkTodosTit.TabIndex = 20
        Me.chkTodosTit.Text = "Todos"
        Me.chkTodosTit.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.Location = New System.Drawing.Point(429, 34)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(118, 23)
        Me.btnConsultar.TabIndex = 21
        Me.btnConsultar.Text = "Consultar"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'frmReporteConsolidado
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(554, 95)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.chkTodosTit)
        Me.Controls.Add(Me.chkTodosEsp)
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.cboTitulo)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.cboEspecialidad)
        Me.Controls.Add(Me.BeLabel13)
        Me.Controls.Add(Me.cboUbicacion)
        Me.MaximizeBox = False
        Me.Name = "frmReporteConsolidado"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte Consolidado"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboUbicacion As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEspecialidad As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboTitulo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkTodosEsp As System.Windows.Forms.CheckBox
    Friend WithEvents chkTodosTit As System.Windows.Forms.CheckBox
    Friend WithEvents btnConsultar As System.Windows.Forms.Button
End Class
