<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheque
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.stsTotales = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.dgvLista = New ctrLibreria.Controles.BeDataGridView
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.label22 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.txtMontoEmitido = New ctrLibreria.Controles.BeTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtNumeroCheque = New ctrLibreria.Controles.BeTextBox
        Me.cboAnulado = New ctrLibreria.Controles.BeComboBox
        Me.cboChequera = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.txtResponsable = New ctrLibreria.Controles.BeTextBox
        Me.dtpFechaEmision = New ctrLibreria.Controles.BeDateTimePicker
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cboAnio = New System.Windows.Forms.ComboBox
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.cboMes = New System.Windows.Forms.ComboBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.rdbMes = New System.Windows.Forms.RadioButton
        Me.rdbFecha = New System.Windows.Forms.RadioButton
        Me.dtpF2 = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpF1 = New ctrLibreria.Controles.BeDateTimePicker
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.cboEstCheque = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.cboChequeras = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox
        Me.cboBanco = New ctrLibreria.Controles.BeComboBox
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NumeroCheque = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaEmision = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.stsTotales.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(1053, 448)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Size = New System.Drawing.Size(1045, 422)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Size = New System.Drawing.Size(940, 422)
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.stsTotales.Location = New System.Drawing.Point(3, 395)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(1037, 22)
        Me.stsTotales.TabIndex = 12
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel1.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(115, 17)
        Me.ToolStripStatusLabel1.Text = "Total de Registros= 0"
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.Column7, Me.Column8, Me.NumeroCheque, Me.Column5, Me.FechaEmision, Me.Column1, Me.Column2, Me.Column3, Me.Column6, Me.Estado})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLista.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-1, -1)
        Me.dgvLista.MultiSelect = False
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLista.Size = New System.Drawing.Size(1045, 393)
        Me.dgvLista.TabIndex = 11
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(17, 14)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "C�digo"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(17, 67)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(87, 13)
        Me.BeLabel2.TabIndex = 4
        Me.BeLabel2.Text = "N� de Cheque"
        '
        'label22
        '
        Me.label22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.label22.AutoSize = True
        Me.label22.BackColor = System.Drawing.Color.Transparent
        Me.label22.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label22.ForeColor = System.Drawing.Color.Black
        Me.label22.Location = New System.Drawing.Point(15, 147)
        Me.label22.Name = "label22"
        Me.label22.Size = New System.Drawing.Size(87, 13)
        Me.label22.TabIndex = 10
        Me.label22.Text = "Monto Emitido"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(15, 120)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(88, 13)
        Me.BeLabel4.TabIndex = 8
        Me.BeLabel4.Text = "Fecha Emisi�n"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(17, 173)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(79, 13)
        Me.BeLabel6.TabIndex = 12
        Me.BeLabel6.Text = "Responsable"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(17, 41)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel7.TabIndex = 2
        Me.BeLabel7.Text = "Moneda"
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(122, 33)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(154, 21)
        Me.cboMoneda.TabIndex = 3
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(17, 200)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel3.TabIndex = 16
        Me.BeLabel3.Text = "Estado"
        '
        'txtMontoEmitido
        '
        Me.txtMontoEmitido.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMontoEmitido.BackColor = System.Drawing.Color.Ivory
        Me.txtMontoEmitido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMontoEmitido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoEmitido.ForeColor = System.Drawing.Color.Black
        Me.txtMontoEmitido.KeyEnter = True
        Me.txtMontoEmitido.Location = New System.Drawing.Point(121, 140)
        Me.txtMontoEmitido.MaxLength = 20
        Me.txtMontoEmitido.Name = "txtMontoEmitido"
        Me.txtMontoEmitido.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMontoEmitido.ShortcutsEnabled = False
        Me.txtMontoEmitido.Size = New System.Drawing.Size(155, 20)
        Me.txtMontoEmitido.TabIndex = 11
        Me.txtMontoEmitido.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtNumeroCheque)
        Me.Panel1.Controls.Add(Me.cboAnulado)
        Me.Panel1.Controls.Add(Me.cboChequera)
        Me.Panel1.Controls.Add(Me.BeLabel5)
        Me.Panel1.Controls.Add(Me.txtCodigo)
        Me.Panel1.Controls.Add(Me.txtResponsable)
        Me.Panel1.Controls.Add(Me.dtpFechaEmision)
        Me.Panel1.Controls.Add(Me.txtMontoEmitido)
        Me.Panel1.Controls.Add(Me.BeLabel3)
        Me.Panel1.Controls.Add(Me.cboMoneda)
        Me.Panel1.Controls.Add(Me.BeLabel7)
        Me.Panel1.Controls.Add(Me.BeLabel6)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Controls.Add(Me.label22)
        Me.Panel1.Controls.Add(Me.BeLabel2)
        Me.Panel1.Controls.Add(Me.BeLabel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(932, 424)
        Me.Panel1.TabIndex = 0
        '
        'txtNumeroCheque
        '
        Me.txtNumeroCheque.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumeroCheque.BackColor = System.Drawing.Color.Ivory
        Me.txtNumeroCheque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumeroCheque.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroCheque.ForeColor = System.Drawing.Color.Black
        Me.txtNumeroCheque.KeyEnter = True
        Me.txtNumeroCheque.Location = New System.Drawing.Point(121, 60)
        Me.txtNumeroCheque.MaxLength = 20
        Me.txtNumeroCheque.Name = "txtNumeroCheque"
        Me.txtNumeroCheque.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumeroCheque.ShortcutsEnabled = False
        Me.txtNumeroCheque.Size = New System.Drawing.Size(155, 20)
        Me.txtNumeroCheque.TabIndex = 5
        Me.txtNumeroCheque.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        '
        'cboAnulado
        '
        Me.cboAnulado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAnulado.BackColor = System.Drawing.Color.Ivory
        Me.cboAnulado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnulado.ForeColor = System.Drawing.Color.Black
        Me.cboAnulado.FormattingEnabled = True
        Me.cboAnulado.KeyEnter = True
        Me.cboAnulado.Location = New System.Drawing.Point(122, 192)
        Me.cboAnulado.Name = "cboAnulado"
        Me.cboAnulado.Size = New System.Drawing.Size(154, 21)
        Me.cboAnulado.TabIndex = 15
        '
        'cboChequera
        '
        Me.cboChequera.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboChequera.BackColor = System.Drawing.Color.Ivory
        Me.cboChequera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChequera.ForeColor = System.Drawing.Color.Black
        Me.cboChequera.FormattingEnabled = True
        Me.cboChequera.KeyEnter = True
        Me.cboChequera.Location = New System.Drawing.Point(122, 86)
        Me.cboChequera.Name = "cboChequera"
        Me.cboChequera.Size = New System.Drawing.Size(154, 21)
        Me.cboChequera.TabIndex = 7
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(16, 94)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(99, 13)
        Me.BeLabel5.TabIndex = 6
        Me.BeLabel5.Text = "N� de Chequera"
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(122, 7)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(154, 20)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.TabStop = False
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtResponsable
        '
        Me.txtResponsable.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtResponsable.BackColor = System.Drawing.Color.Ivory
        Me.txtResponsable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtResponsable.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtResponsable.ForeColor = System.Drawing.Color.Black
        Me.txtResponsable.KeyEnter = True
        Me.txtResponsable.Location = New System.Drawing.Point(121, 166)
        Me.txtResponsable.MaxLength = 150
        Me.txtResponsable.Name = "txtResponsable"
        Me.txtResponsable.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtResponsable.ShortcutsEnabled = False
        Me.txtResponsable.Size = New System.Drawing.Size(155, 20)
        Me.txtResponsable.TabIndex = 13
        Me.txtResponsable.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'dtpFechaEmision
        '
        Me.dtpFechaEmision.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaEmision.CustomFormat = ""
        Me.dtpFechaEmision.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaEmision.KeyEnter = True
        Me.dtpFechaEmision.Location = New System.Drawing.Point(121, 113)
        Me.dtpFechaEmision.Name = "dtpFechaEmision"
        Me.dtpFechaEmision.Size = New System.Drawing.Size(155, 20)
        Me.dtpFechaEmision.TabIndex = 9
        Me.dtpFechaEmision.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaEmision.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Controls.Add(Me.GroupBox1)
        Me.Panel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(125, 115)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(847, 173)
        Me.Panel2.TabIndex = 13
        Me.Panel2.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(828, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(18, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "X"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboAnio)
        Me.GroupBox2.Controls.Add(Me.BeLabel13)
        Me.GroupBox2.Controls.Add(Me.cboMes)
        Me.GroupBox2.Controls.Add(Me.BeLabel9)
        Me.GroupBox2.Controls.Add(Me.rdbMes)
        Me.GroupBox2.Controls.Add(Me.rdbFecha)
        Me.GroupBox2.Controls.Add(Me.dtpF2)
        Me.GroupBox2.Controls.Add(Me.dtpF1)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 74)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(393, 88)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Por Fechas"
        '
        'cboAnio
        '
        Me.cboAnio.FormattingEnabled = True
        Me.cboAnio.Items.AddRange(New Object() {"1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"})
        Me.cboAnio.Location = New System.Drawing.Point(240, 48)
        Me.cboAnio.Name = "cboAnio"
        Me.cboAnio.Size = New System.Drawing.Size(133, 21)
        Me.cboAnio.TabIndex = 14
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(208, 51)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel13.TabIndex = 4
        Me.BeLabel13.Text = "A�o"
        '
        'cboMes
        '
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cboMes.Location = New System.Drawing.Point(66, 47)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(133, 21)
        Me.cboMes.TabIndex = 15
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(215, 26)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(14, 13)
        Me.BeLabel9.TabIndex = 6
        Me.BeLabel9.Text = "y"
        '
        'rdbMes
        '
        Me.rdbMes.AutoSize = True
        Me.rdbMes.Location = New System.Drawing.Point(7, 48)
        Me.rdbMes.Name = "rdbMes"
        Me.rdbMes.Size = New System.Drawing.Size(47, 17)
        Me.rdbMes.TabIndex = 1
        Me.rdbMes.TabStop = True
        Me.rdbMes.Text = "Mes"
        Me.rdbMes.UseVisualStyleBackColor = True
        '
        'rdbFecha
        '
        Me.rdbFecha.AutoSize = True
        Me.rdbFecha.Location = New System.Drawing.Point(7, 25)
        Me.rdbFecha.Name = "rdbFecha"
        Me.rdbFecha.Size = New System.Drawing.Size(58, 17)
        Me.rdbFecha.TabIndex = 4
        Me.rdbFecha.TabStop = True
        Me.rdbFecha.Text = "Fecha"
        Me.rdbFecha.UseVisualStyleBackColor = True
        '
        'dtpF2
        '
        Me.dtpF2.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpF2.CustomFormat = ""
        Me.dtpF2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF2.KeyEnter = True
        Me.dtpF2.Location = New System.Drawing.Point(240, 23)
        Me.dtpF2.Name = "dtpF2"
        Me.dtpF2.Size = New System.Drawing.Size(133, 21)
        Me.dtpF2.TabIndex = 3
        Me.dtpF2.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpF2.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpF1
        '
        Me.dtpF1.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpF1.CustomFormat = ""
        Me.dtpF1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF1.KeyEnter = True
        Me.dtpF1.Location = New System.Drawing.Point(66, 22)
        Me.dtpF1.Name = "dtpF1"
        Me.dtpF1.Size = New System.Drawing.Size(133, 21)
        Me.dtpF1.TabIndex = 2
        Me.dtpF1.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpF1.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BeLabel10)
        Me.GroupBox1.Controls.Add(Me.cboEstCheque)
        Me.GroupBox1.Controls.Add(Me.BeLabel8)
        Me.GroupBox1.Controls.Add(Me.cboChequeras)
        Me.GroupBox1.Controls.Add(Me.BeLabel12)
        Me.GroupBox1.Controls.Add(Me.BeLabel11)
        Me.GroupBox1.Controls.Add(Me.cboCuenta)
        Me.GroupBox1.Controls.Add(Me.cboBanco)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(825, 67)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "B�squeda de Chequeras"
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(700, 18)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel10.TabIndex = 6
        Me.BeLabel10.Text = "Estado"
        '
        'cboEstCheque
        '
        Me.cboEstCheque.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstCheque.BackColor = System.Drawing.Color.Ivory
        Me.cboEstCheque.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstCheque.ForeColor = System.Drawing.Color.Black
        Me.cboEstCheque.FormattingEnabled = True
        Me.cboEstCheque.KeyEnter = True
        Me.cboEstCheque.Location = New System.Drawing.Point(703, 34)
        Me.cboEstCheque.Name = "cboEstCheque"
        Me.cboEstCheque.Size = New System.Drawing.Size(116, 21)
        Me.cboEstCheque.TabIndex = 0
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(483, 18)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel8.TabIndex = 5
        Me.BeLabel8.Text = "Chequeras"
        '
        'cboChequeras
        '
        Me.cboChequeras.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboChequeras.BackColor = System.Drawing.Color.Ivory
        Me.cboChequeras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChequeras.ForeColor = System.Drawing.Color.Black
        Me.cboChequeras.FormattingEnabled = True
        Me.cboChequeras.KeyEnter = True
        Me.cboChequeras.Location = New System.Drawing.Point(485, 34)
        Me.cboChequeras.Name = "cboChequeras"
        Me.cboChequeras.Size = New System.Drawing.Size(212, 21)
        Me.cboChequeras.TabIndex = 0
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(261, 17)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel12.TabIndex = 4
        Me.BeLabel12.Text = "Cuenta"
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(7, 16)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel11.TabIndex = 3
        Me.BeLabel11.Text = "Banco"
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(264, 34)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(215, 21)
        Me.cboCuenta.TabIndex = 2
        '
        'cboBanco
        '
        Me.cboBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBanco.ForeColor = System.Drawing.Color.Black
        Me.cboBanco.FormattingEnabled = True
        Me.cboBanco.KeyEnter = True
        Me.cboBanco.Location = New System.Drawing.Point(10, 34)
        Me.cboBanco.Name = "cboBanco"
        Me.cboBanco.Size = New System.Drawing.Size(248, 21)
        Me.cboBanco.TabIndex = 1
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "IdCheque"
        Me.Codigo.HeaderText = "C�digo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Width = 130
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "NombreBanco"
        Me.Column7.HeaderText = "Banco"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 90
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "NumeroCuenta"
        Me.Column8.HeaderText = "Cuenta"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'NumeroCheque
        '
        Me.NumeroCheque.DataPropertyName = "NumeroCheque"
        Me.NumeroCheque.HeaderText = "N�mero"
        Me.NumeroCheque.Name = "NumeroCheque"
        Me.NumeroCheque.ReadOnly = True
        Me.NumeroCheque.Width = 50
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "NroChequera"
        Me.Column5.HeaderText = "N� Chequera"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 130
        '
        'FechaEmision
        '
        Me.FechaEmision.DataPropertyName = "FechaEmision"
        Me.FechaEmision.HeaderText = "Fecha"
        Me.FechaEmision.Name = "FechaEmision"
        Me.FechaEmision.ReadOnly = True
        Me.FechaEmision.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.FechaEmision.Width = 70
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "MontoEmitido"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column1.HeaderText = "Monto Emitido"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "Responsable"
        Me.Column2.HeaderText = "Responsable"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 110
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "MonDescripcion"
        Me.Column3.HeaderText = "Moneda"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 70
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "Estados"
        Me.Column6.HeaderText = "Estado"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 70
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado2"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'frmCheque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(1053, 448)
        Me.MaximizeBox = False
        Me.Name = "frmCheque"
        Me.Text = "Cheques"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents dgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtMontoEmitido As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents label22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaEmision As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents txtResponsable As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboChequera As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboAnulado As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtNumeroCheque As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents cboChequeras As ctrLibreria.Controles.BeComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboEstCheque As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents rdbMes As System.Windows.Forms.RadioButton
    Friend WithEvents rdbFecha As System.Windows.Forms.RadioButton
    Friend WithEvents dtpF2 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpF1 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboAnio As System.Windows.Forms.ComboBox
    Friend WithEvents cboMes As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumeroCheque As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaEmision As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
