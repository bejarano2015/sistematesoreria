<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCabeceraLibros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.txtRestar = New ctrLibreria.Controles.BeTextBox
        Me.lblTipoCta = New System.Windows.Forms.Label
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldoInicial = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.txtNOCobrados = New ctrLibreria.Controles.BeTextBox
        Me.txtCobrados = New ctrLibreria.Controles.BeTextBox
        Me.txtSaldo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.txtCod = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.cboAno = New ctrLibreria.Controles.BeComboBox
        Me.cboCuentas = New ctrLibreria.Controles.BeComboBox
        Me.cboBancos = New ctrLibreria.Controles.BeComboBox
        Me.cboMes = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.lblMoneda = New ctrLibreria.Controles.BeLabel
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnEliminar = New ctrLibreria.Controles.BeButton
        Me.btnNuevo = New ctrLibreria.Controles.BeButton
        Me.btnGrabar = New ctrLibreria.Controles.BeButton
        Me.btnCancelar = New ctrLibreria.Controles.BeButton
        Me.dgvLibroDet = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.BeLabel8)
        Me.Panel1.Controls.Add(Me.txtRestar)
        Me.Panel1.Controls.Add(Me.lblTipoCta)
        Me.Panel1.Controls.Add(Me.BeLabel2)
        Me.Panel1.Controls.Add(Me.txtSaldoInicial)
        Me.Panel1.Controls.Add(Me.BeLabel7)
        Me.Panel1.Controls.Add(Me.BeLabel6)
        Me.Panel1.Controls.Add(Me.BeLabel1)
        Me.Panel1.Controls.Add(Me.txtNOCobrados)
        Me.Panel1.Controls.Add(Me.txtCobrados)
        Me.Panel1.Controls.Add(Me.txtSaldo)
        Me.Panel1.Controls.Add(Me.BeLabel12)
        Me.Panel1.Controls.Add(Me.txtCod)
        Me.Panel1.Controls.Add(Me.BeLabel15)
        Me.Panel1.Controls.Add(Me.BeLabel3)
        Me.Panel1.Controls.Add(Me.cboAno)
        Me.Panel1.Controls.Add(Me.cboCuentas)
        Me.Panel1.Controls.Add(Me.cboBancos)
        Me.Panel1.Controls.Add(Me.cboMes)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Controls.Add(Me.BeLabel5)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(838, 211)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Tipo Cuenta"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 67)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Moneda"
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(340, 175)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(25, 13)
        Me.BeLabel8.TabIndex = 20
        Me.BeLabel8.Text = "(-)"
        '
        'txtRestar
        '
        Me.txtRestar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRestar.BackColor = System.Drawing.Color.Ivory
        Me.txtRestar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRestar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRestar.ForeColor = System.Drawing.Color.Black
        Me.txtRestar.KeyEnter = True
        Me.txtRestar.Location = New System.Drawing.Point(368, 169)
        Me.txtRestar.Name = "txtRestar"
        Me.txtRestar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRestar.ShortcutsEnabled = False
        Me.txtRestar.Size = New System.Drawing.Size(128, 20)
        Me.txtRestar.TabIndex = 21
        Me.txtRestar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRestar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'lblTipoCta
        '
        Me.lblTipoCta.AutoSize = True
        Me.lblTipoCta.Location = New System.Drawing.Point(97, 91)
        Me.lblTipoCta.Name = "lblTipoCta"
        Me.lblTipoCta.Size = New System.Drawing.Size(66, 13)
        Me.lblTipoCta.TabIndex = 7
        Me.lblTipoCta.Text = "[ lblTipoCta ]"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(608, 124)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(87, 13)
        Me.BeLabel2.TabIndex = 16
        Me.BeLabel2.Text = "Saldo Final :"
        '
        'txtSaldoInicial
        '
        Me.txtSaldoInicial.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoInicial.BackColor = System.Drawing.Color.Ivory
        Me.txtSaldoInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoInicial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoInicial.ForeColor = System.Drawing.Color.Black
        Me.txtSaldoInicial.KeyEnter = True
        Me.txtSaldoInicial.Location = New System.Drawing.Point(697, 91)
        Me.txtSaldoInicial.Name = "txtSaldoInicial"
        Me.txtSaldoInicial.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoInicial.ShortcutsEnabled = False
        Me.txtSaldoInicial.Size = New System.Drawing.Size(128, 20)
        Me.txtSaldoInicial.TabIndex = 15
        Me.txtSaldoInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoInicial.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(523, 150)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(172, 13)
        Me.BeLabel7.TabIndex = 18
        Me.BeLabel7.Text = "Total Cheques Cobrados :"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(502, 176)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(193, 13)
        Me.BeLabel6.TabIndex = 22
        Me.BeLabel6.Text = "Total Cheques No Cobrados :"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(599, 98)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(96, 13)
        Me.BeLabel1.TabIndex = 14
        Me.BeLabel1.Text = "Saldo Inicial :"
        '
        'txtNOCobrados
        '
        Me.txtNOCobrados.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNOCobrados.BackColor = System.Drawing.Color.Ivory
        Me.txtNOCobrados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNOCobrados.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNOCobrados.ForeColor = System.Drawing.Color.Black
        Me.txtNOCobrados.KeyEnter = True
        Me.txtNOCobrados.Location = New System.Drawing.Point(697, 169)
        Me.txtNOCobrados.Name = "txtNOCobrados"
        Me.txtNOCobrados.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNOCobrados.ShortcutsEnabled = False
        Me.txtNOCobrados.Size = New System.Drawing.Size(128, 20)
        Me.txtNOCobrados.TabIndex = 23
        Me.txtNOCobrados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNOCobrados.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCobrados
        '
        Me.txtCobrados.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCobrados.BackColor = System.Drawing.Color.Ivory
        Me.txtCobrados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCobrados.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCobrados.ForeColor = System.Drawing.Color.Black
        Me.txtCobrados.KeyEnter = True
        Me.txtCobrados.Location = New System.Drawing.Point(697, 143)
        Me.txtCobrados.Name = "txtCobrados"
        Me.txtCobrados.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCobrados.ShortcutsEnabled = False
        Me.txtCobrados.Size = New System.Drawing.Size(128, 20)
        Me.txtCobrados.TabIndex = 19
        Me.txtCobrados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCobrados.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSaldo
        '
        Me.txtSaldo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldo.BackColor = System.Drawing.Color.Ivory
        Me.txtSaldo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldo.ForeColor = System.Drawing.Color.Black
        Me.txtSaldo.KeyEnter = True
        Me.txtSaldo.Location = New System.Drawing.Point(697, 117)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldo.ShortcutsEnabled = False
        Me.txtSaldo.Size = New System.Drawing.Size(128, 20)
        Me.txtSaldo.TabIndex = 17
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(597, 19)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(96, 13)
        Me.BeLabel12.TabIndex = 8
        Me.BeLabel12.Text = "Código Libro :"
        '
        'txtCod
        '
        Me.txtCod.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCod.BackColor = System.Drawing.Color.Ivory
        Me.txtCod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCod.Enabled = False
        Me.txtCod.ForeColor = System.Drawing.Color.Black
        Me.txtCod.KeyEnter = True
        Me.txtCod.Location = New System.Drawing.Point(697, 12)
        Me.txtCod.Name = "txtCod"
        Me.txtCod.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCod.ShortcutsEnabled = False
        Me.txtCod.Size = New System.Drawing.Size(128, 20)
        Me.txtCod.TabIndex = 9
        Me.txtCod.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCod.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(655, 45)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel15.TabIndex = 10
        Me.BeLabel15.Text = "Año :"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(655, 72)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel3.TabIndex = 12
        Me.BeLabel3.Text = "Mes :"
        '
        'cboAno
        '
        Me.cboAno.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAno.BackColor = System.Drawing.Color.Ivory
        Me.cboAno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAno.ForeColor = System.Drawing.Color.Black
        Me.cboAno.FormattingEnabled = True
        Me.cboAno.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040"})
        Me.cboAno.KeyEnter = True
        Me.cboAno.Location = New System.Drawing.Point(697, 37)
        Me.cboAno.Name = "cboAno"
        Me.cboAno.Size = New System.Drawing.Size(128, 21)
        Me.cboAno.TabIndex = 11
        '
        'cboCuentas
        '
        Me.cboCuentas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuentas.BackColor = System.Drawing.Color.Ivory
        Me.cboCuentas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentas.ForeColor = System.Drawing.Color.Black
        Me.cboCuentas.FormattingEnabled = True
        Me.cboCuentas.KeyEnter = True
        Me.cboCuentas.Location = New System.Drawing.Point(100, 38)
        Me.cboCuentas.Name = "cboCuentas"
        Me.cboCuentas.Size = New System.Drawing.Size(459, 21)
        Me.cboCuentas.TabIndex = 3
        '
        'cboBancos
        '
        Me.cboBancos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBancos.BackColor = System.Drawing.Color.Ivory
        Me.cboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBancos.ForeColor = System.Drawing.Color.Black
        Me.cboBancos.FormattingEnabled = True
        Me.cboBancos.KeyEnter = True
        Me.cboBancos.Location = New System.Drawing.Point(100, 11)
        Me.cboBancos.Name = "cboBancos"
        Me.cboBancos.Size = New System.Drawing.Size(459, 21)
        Me.cboBancos.TabIndex = 1
        '
        'cboMes
        '
        Me.cboMes.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMes.BackColor = System.Drawing.Color.Ivory
        Me.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMes.ForeColor = System.Drawing.Color.Black
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cboMes.KeyEnter = True
        Me.cboMes.Location = New System.Drawing.Point(697, 64)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(128, 21)
        Me.cboMes.TabIndex = 13
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(14, 14)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(43, 13)
        Me.BeLabel4.TabIndex = 0
        Me.BeLabel4.Text = "Banco"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(14, 41)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel5.TabIndex = 2
        Me.BeLabel5.Text = "Cuenta"
        '
        'lblMoneda
        '
        Me.lblMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblMoneda.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoneda.ForeColor = System.Drawing.Color.Black
        Me.lblMoneda.Location = New System.Drawing.Point(97, 67)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(61, 13)
        Me.lblMoneda.TabIndex = 5
        Me.lblMoneda.Text = "[Moneda]"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnEliminar)
        Me.GroupBox3.Controls.Add(Me.btnNuevo)
        Me.GroupBox3.Controls.Add(Me.btnGrabar)
        Me.GroupBox3.Controls.Add(Me.btnCancelar)
        Me.GroupBox3.Location = New System.Drawing.Point(678, 229)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(172, 50)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(131, 14)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(35, 30)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnNuevo.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__File_new_
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(8, 13)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(35, 30)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(49, 14)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(35, 30)
        Me.btnGrabar.TabIndex = 1
        Me.btnGrabar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(90, 14)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(35, 30)
        Me.btnCancelar.TabIndex = 2
        Me.btnCancelar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'dgvLibroDet
        '
        Me.dgvLibroDet.AllowUserToAddRows = False
        Me.dgvLibroDet.BackgroundColor = System.Drawing.Color.White
        Me.dgvLibroDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLibroDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column3, Me.Column5, Me.Column4, Me.Column6, Me.Column7, Me.Column10, Me.Column8, Me.Column2})
        Me.dgvLibroDet.EnableHeadersVisualStyles = False
        Me.dgvLibroDet.Location = New System.Drawing.Point(12, 229)
        Me.dgvLibroDet.Name = "dgvLibroDet"
        Me.dgvLibroDet.RowHeadersVisible = False
        Me.dgvLibroDet.Size = New System.Drawing.Size(660, 184)
        Me.dgvLibroDet.TabIndex = 1
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "IdLibroCab"
        Me.Column1.HeaderText = "Codigo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column1.Width = 70
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "CodEmpresa"
        Me.Column3.HeaderText = "Empresa"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column3.Visible = False
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "IdCuenta"
        Me.Column5.HeaderText = "Cuenta"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column5.Visible = False
        Me.Column5.Width = 150
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Mes"
        Me.Column4.HeaderText = "Mes"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column4.Width = 50
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "Año"
        Me.Column6.HeaderText = "Año"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column6.Width = 80
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "SaldoInicial"
        Me.Column7.HeaderText = "Saldo Inicial"
        Me.Column7.Name = "Column7"
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "Saldo"
        Me.Column10.HeaderText = "Saldo Final"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column10.Width = 80
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "CheCobradosSI"
        DataGridViewCellStyle1.Format = "N2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column8.HeaderText = "Ch. Cobrados"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column8.Width = 120
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "CheCobradosNO"
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column2.HeaderText = "Ch. No Cobrados"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 120
        '
        'frmCabeceraLibros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(858, 421)
        Me.Controls.Add(Me.dgvLibroDet)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "frmCabeceraLibros"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Libros"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboAno As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboCuentas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboBancos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboMes As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnEliminar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnNuevo As ctrLibreria.Controles.BeButton
    Friend WithEvents btnGrabar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnCancelar As ctrLibreria.Controles.BeButton
    Friend WithEvents dgvLibroDet As System.Windows.Forms.DataGridView
    Friend WithEvents txtNOCobrados As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCobrados As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSaldo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCod As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldoInicial As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblTipoCta As System.Windows.Forms.Label
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtRestar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
