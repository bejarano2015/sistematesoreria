<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroBancosBuscar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvLibroDet = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdCab = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Marca1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NroDocumento2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtTexto = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboCriterio = New ctrLibreria.Controles.BeComboBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.txtITF1 = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel
        Me.txtImporteItf = New System.Windows.Forms.TextBox
        Me.txtGlosaItf = New System.Windows.Forms.TextBox
        Me.btnCancelarItf = New System.Windows.Forms.Button
        Me.dtpFechaItf = New ctrLibreria.Controles.BeDateTimePicker
        Me.btnGrabarItf = New System.Windows.Forms.Button
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.btnGenerarITF = New System.Windows.Forms.Button
        Me.btnConfirmar = New System.Windows.Forms.Button
        Me.txtMarca = New ctrLibreria.Controles.BeTextBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.dtpFechaCobro = New ctrLibreria.Controles.BeDateTimePicker
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvLibroDet
        '
        Me.dgvLibroDet.AllowUserToAddRows = False
        Me.dgvLibroDet.BackgroundColor = System.Drawing.Color.White
        Me.dgvLibroDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLibroDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column3, Me.Column5, Me.Column4, Me.Column6, Me.Column10, Me.Column8, Me.Column9, Me.Column2, Me.Column7, Me.Column11, Me.Column12, Me.IdCab, Me.Marca1, Me.NroDocumento2})
        Me.dgvLibroDet.EnableHeadersVisualStyles = False
        Me.dgvLibroDet.Location = New System.Drawing.Point(5, 75)
        Me.dgvLibroDet.Name = "dgvLibroDet"
        Me.dgvLibroDet.RowHeadersVisible = False
        Me.dgvLibroDet.Size = New System.Drawing.Size(727, 319)
        Me.dgvLibroDet.TabIndex = 2
        '
        'Column1
        '
        Me.Column1.HeaderText = "Fecha"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column1.Width = 70
        '
        'Column3
        '
        Me.Column3.HeaderText = "Cheq. / Not."
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column5
        '
        Me.Column5.HeaderText = "Concepto"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column5.Width = 150
        '
        'Column4
        '
        Me.Column4.HeaderText = "Descripción"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column4.Width = 180
        '
        'Column6
        '
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle10
        Me.Column6.HeaderText = "Abono"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column6.Width = 80
        '
        'Column10
        '
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle11
        Me.Column10.HeaderText = "Cargo"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column10.Width = 80
        '
        'Column8
        '
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle12
        Me.Column8.HeaderText = "Saldo"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column8.Visible = False
        Me.Column8.Width = 80
        '
        'Column9
        '
        Me.Column9.HeaderText = "ITF"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 40
        '
        'Column2
        '
        Me.Column2.HeaderText = "IdDet"
        Me.Column2.Name = "Column2"
        Me.Column2.Visible = False
        '
        'Column7
        '
        Me.Column7.HeaderText = "TipMov"
        Me.Column7.Name = "Column7"
        Me.Column7.Visible = False
        '
        'Column11
        '
        Me.Column11.HeaderText = "TipOpe"
        Me.Column11.Name = "Column11"
        Me.Column11.Visible = False
        '
        'Column12
        '
        Me.Column12.HeaderText = "Importe"
        Me.Column12.Name = "Column12"
        Me.Column12.Visible = False
        '
        'IdCab
        '
        Me.IdCab.DataPropertyName = "IdLibroCab"
        Me.IdCab.HeaderText = "IdCab"
        Me.IdCab.Name = "IdCab"
        Me.IdCab.Visible = False
        '
        'Marca1
        '
        Me.Marca1.DataPropertyName = "Marca"
        Me.Marca1.HeaderText = "Marca1"
        Me.Marca1.Name = "Marca1"
        Me.Marca1.Visible = False
        '
        'NroDocumento2
        '
        Me.NroDocumento2.DataPropertyName = "NroDocumento2"
        Me.NroDocumento2.HeaderText = "NroDocumento2"
        Me.NroDocumento2.Name = "NroDocumento2"
        Me.NroDocumento2.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtTexto)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboCriterio)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(5, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(427, 66)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Buscar Cheque"
        '
        'txtTexto
        '
        Me.txtTexto.BackColor = System.Drawing.Color.Ivory
        Me.txtTexto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTexto.Location = New System.Drawing.Point(136, 32)
        Me.txtTexto.Name = "txtTexto"
        Me.txtTexto.Size = New System.Drawing.Size(285, 21)
        Me.txtTexto.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(130, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Texto"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Criterio"
        '
        'cboCriterio
        '
        Me.cboCriterio.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCriterio.BackColor = System.Drawing.Color.Ivory
        Me.cboCriterio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCriterio.ForeColor = System.Drawing.Color.Black
        Me.cboCriterio.FormattingEnabled = True
        Me.cboCriterio.Items.AddRange(New Object() {"NUMERO", "CONCEPTO", "DESCRIPCION"})
        Me.cboCriterio.KeyEnter = True
        Me.cboCriterio.Location = New System.Drawing.Point(6, 32)
        Me.cboCriterio.Name = "cboCriterio"
        Me.cboCriterio.Size = New System.Drawing.Size(121, 21)
        Me.cboCriterio.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Panel7)
        Me.GroupBox2.Controls.Add(Me.Panel6)
        Me.GroupBox2.Controls.Add(Me.BeLabel13)
        Me.GroupBox2.Controls.Add(Me.BeLabel14)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(586, 9)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(146, 60)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cheques"
        '
        'Panel7
        '
        Me.Panel7.BackColor = System.Drawing.Color.Aqua
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Location = New System.Drawing.Point(105, 38)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(33, 14)
        Me.Panel7.TabIndex = 3
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.Silver
        Me.Panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel6.Location = New System.Drawing.Point(105, 15)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(33, 14)
        Me.Panel6.TabIndex = 1
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(6, 18)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel13.TabIndex = 0
        Me.BeLabel13.Text = "Cobrados"
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(6, 41)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(81, 13)
        Me.BeLabel14.TabIndex = 2
        Me.BeLabel14.Text = "No Cobrados"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.GroupBox3)
        Me.Panel3.Location = New System.Drawing.Point(123, 184)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(489, 110)
        Me.Panel3.TabIndex = 3
        Me.Panel3.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtITF1)
        Me.GroupBox3.Controls.Add(Me.BeLabel16)
        Me.GroupBox3.Controls.Add(Me.txtImporteItf)
        Me.GroupBox3.Controls.Add(Me.txtGlosaItf)
        Me.GroupBox3.Controls.Add(Me.btnCancelarItf)
        Me.GroupBox3.Controls.Add(Me.dtpFechaItf)
        Me.GroupBox3.Controls.Add(Me.btnGrabarItf)
        Me.GroupBox3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(3, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(481, 102)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Registro de ITF"
        '
        'txtITF1
        '
        Me.txtITF1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtITF1.BackColor = System.Drawing.Color.Ivory
        Me.txtITF1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtITF1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtITF1.Enabled = False
        Me.txtITF1.ForeColor = System.Drawing.Color.Black
        Me.txtITF1.KeyEnter = True
        Me.txtITF1.Location = New System.Drawing.Point(159, 19)
        Me.txtITF1.Name = "txtITF1"
        Me.txtITF1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtITF1.ShortcutsEnabled = False
        Me.txtITF1.Size = New System.Drawing.Size(53, 21)
        Me.txtITF1.TabIndex = 2
        Me.txtITF1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtITF1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(112, 23)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(41, 13)
        Me.BeLabel16.TabIndex = 1
        Me.BeLabel16.Text = "ITF %"
        '
        'txtImporteItf
        '
        Me.txtImporteItf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteItf.Location = New System.Drawing.Point(395, 45)
        Me.txtImporteItf.Name = "txtImporteItf"
        Me.txtImporteItf.Size = New System.Drawing.Size(75, 21)
        Me.txtImporteItf.TabIndex = 4
        Me.txtImporteItf.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGlosaItf
        '
        Me.txtGlosaItf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGlosaItf.Location = New System.Drawing.Point(14, 45)
        Me.txtGlosaItf.Name = "txtGlosaItf"
        Me.txtGlosaItf.Size = New System.Drawing.Size(375, 21)
        Me.txtGlosaItf.TabIndex = 3
        '
        'btnCancelarItf
        '
        Me.btnCancelarItf.Location = New System.Drawing.Point(395, 73)
        Me.btnCancelarItf.Name = "btnCancelarItf"
        Me.btnCancelarItf.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelarItf.TabIndex = 6
        Me.btnCancelarItf.Text = "Cancelar"
        Me.btnCancelarItf.UseVisualStyleBackColor = True
        '
        'dtpFechaItf
        '
        Me.dtpFechaItf.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaItf.CustomFormat = ""
        Me.dtpFechaItf.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaItf.KeyEnter = True
        Me.dtpFechaItf.Location = New System.Drawing.Point(14, 19)
        Me.dtpFechaItf.Name = "dtpFechaItf"
        Me.dtpFechaItf.Size = New System.Drawing.Size(92, 21)
        Me.dtpFechaItf.TabIndex = 0
        Me.dtpFechaItf.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaItf.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'btnGrabarItf
        '
        Me.btnGrabarItf.Location = New System.Drawing.Point(314, 73)
        Me.btnGrabarItf.Name = "btnGrabarItf"
        Me.btnGrabarItf.Size = New System.Drawing.Size(75, 23)
        Me.btnGrabarItf.TabIndex = 5
        Me.btnGrabarItf.Text = "Grabar"
        Me.btnGrabarItf.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.GroupBox4)
        Me.Panel5.Controls.Add(Me.Button1)
        Me.Panel5.Controls.Add(Me.btnGenerarITF)
        Me.Panel5.Location = New System.Drawing.Point(123, 184)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(489, 110)
        Me.Panel5.TabIndex = 4
        Me.Panel5.Visible = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(351, 45)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(127, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "CANCELAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnGenerarITF
        '
        Me.btnGenerarITF.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarITF.Location = New System.Drawing.Point(8, 45)
        Me.btnGenerarITF.Name = "btnGenerarITF"
        Me.btnGenerarITF.Size = New System.Drawing.Size(127, 23)
        Me.btnGenerarITF.TabIndex = 0
        Me.btnGenerarITF.Text = "GENERAR ITF"
        Me.btnGenerarITF.UseVisualStyleBackColor = True
        '
        'btnConfirmar
        '
        Me.btnConfirmar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConfirmar.Location = New System.Drawing.Point(9, 44)
        Me.btnConfirmar.Name = "btnConfirmar"
        Me.btnConfirmar.Size = New System.Drawing.Size(138, 23)
        Me.btnConfirmar.TabIndex = 1
        Me.btnConfirmar.Text = "CONFIRMAR COBRO"
        Me.btnConfirmar.UseVisualStyleBackColor = True
        '
        'txtMarca
        '
        Me.txtMarca.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMarca.BackColor = System.Drawing.Color.Ivory
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.ForeColor = System.Drawing.Color.Black
        Me.txtMarca.KeyEnter = True
        Me.txtMarca.Location = New System.Drawing.Point(852, 75)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMarca.ShortcutsEnabled = False
        Me.txtMarca.Size = New System.Drawing.Size(46, 20)
        Me.txtMarca.TabIndex = 27
        Me.txtMarca.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Timer1
        '
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dtpFechaCobro)
        Me.GroupBox4.Controls.Add(Me.btnConfirmar)
        Me.GroupBox4.Location = New System.Drawing.Point(162, 16)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(157, 78)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        '
        'dtpFechaCobro
        '
        Me.dtpFechaCobro.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaCobro.CustomFormat = ""
        Me.dtpFechaCobro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaCobro.KeyEnter = True
        Me.dtpFechaCobro.Location = New System.Drawing.Point(34, 18)
        Me.dtpFechaCobro.Name = "dtpFechaCobro"
        Me.dtpFechaCobro.Size = New System.Drawing.Size(88, 20)
        Me.dtpFechaCobro.TabIndex = 4
        Me.dtpFechaCobro.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaCobro.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'frmLibroBancosBuscar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(737, 398)
        Me.Controls.Add(Me.txtMarca)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvLibroDet)
        Me.MaximizeBox = False
        Me.Name = "frmLibroBancosBuscar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Búsqueda de Cheques"
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvLibroDet As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCriterio As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Marca1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocumento2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtITF1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtImporteItf As System.Windows.Forms.TextBox
    Friend WithEvents txtGlosaItf As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelarItf As System.Windows.Forms.Button
    Friend WithEvents dtpFechaItf As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnGrabarItf As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnGenerarITF As System.Windows.Forms.Button
    Friend WithEvents btnConfirmar As System.Windows.Forms.Button
    Friend WithEvents txtMarca As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTexto As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpFechaCobro As ctrLibreria.Controles.BeDateTimePicker
End Class
