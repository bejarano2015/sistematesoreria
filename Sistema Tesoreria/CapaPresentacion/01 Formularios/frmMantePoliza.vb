Imports CapaNegocios
Public Class frmMantePoliza

    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private cSegurosPolizas As clsSegurosPolizas
    Friend DS_Datos As New DataSet
    Dim BolEditar As Boolean
    Dim DT_ListPolizas As DataTable
    Dim DvFiltro As DataView

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmMantePoliza = Nothing

    Public Shared Function Instance() As frmMantePoliza
        If frmInstance Is Nothing Then
            frmInstance = New frmMantePoliza
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmMantePoliza_CancelarReg() Handles Me.CancelarReg
        Timer1.Enabled = True
    End Sub
    Private Sub frmMantePoliza_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region
    Private Sub v_SalidaReg() Handles Me.SalidaReg
        Me.Close()
    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If TabControl1.SelectedIndex = 0 Then
            txtFiltro.Focus()
        ElseIf TabControl1.SelectedIndex = 1 Then
            If txtPoliza.Enabled = True Then
                txtPoliza.Focus()
            Else
                mtbVigenciaIni.Focus()
            End If
        End If
        Timer1.Enabled = False
    End Sub
    Private Sub frmMantePoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cSegurosPolizas = New clsSegurosPolizas
        DT_ListPolizas = New DataTable

        DS_Datos = cSegurosPolizas.fCargaDatos

        cboRamo.DataSource = DS_Datos.Tables(0)
        cboRamo.ValueMember = "Codigo"
        cboRamo.DisplayMember = "Descripcion"
        cboRamo.SelectedIndex = -1

        cboMoneda.DataSource = DS_Datos.Tables(1)
        cboMoneda.ValueMember = "Codigo"
        cboMoneda.DisplayMember = "Descripcion"
        cboMoneda.SelectedIndex = -1

        cboFormaPago.DataSource = DS_Datos.Tables(2)
        cboFormaPago.ValueMember = "Codigo"
        cboFormaPago.DisplayMember = "Descripcion"
        cboFormaPago.SelectedIndex = -1

        'cboAseguradora.DataSource = DS_Datos.Tables(3)
        cboAseguradora.DataSource = cSegurosPolizas.ListarAseguradoras(gEmpresa)
        cboAseguradora.ValueMember = "Codigo"
        cboAseguradora.DisplayMember = "Descripcion"
        cboAseguradora.SelectedIndex = -1

        DT_ListPolizas = cSegurosPolizas.fListarPolizas(gEmpresa)
        DgvLista.DataSource = DT_ListPolizas
        DvFiltro = DT_ListPolizas.DefaultView

        For xCol As Integer = 0 To DgvLista.Columns.Count - 1
            DgvLista.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
            'DgvLista.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next
        frmPrincipal.fDatos = True
        cboBusqueda.SelectedIndex = 0
        lblTotalRegistro.Text = DgvLista.Rows.Count & " Items"
        Timer1.Enabled = True
    End Sub
    Private Sub frmMantePoliza_NuevoReg() Handles Me.NuevoReg
        txtPoliza.Enabled = True
        txtPoliza.Text = ""
        mtbVigenciaIni.Text = ""
        mtbVigenciaFin.Text = ""
        cboRamo.SelectedIndex = -1
        txtCliente.Text = cSegurosPolizas.DescripcionEmpresa(gEmpresa)
        txtRUC.Text = cSegurosPolizas.RucEmpresa(gEmpresa)
        txtDireccion.Text = cSegurosPolizas.DireccionEmpresa(gEmpresa)
        cboMoneda.SelectedIndex = -1
        txtPrima.Text = 0
        txtDerechoEmision.Text = 0
        txtInteres.Text = 0
        txtIGV.Text = 0
        txtSumaAseguradora.Text = 0
        txtTotal.Text = 0
        cboAseguradora.SelectedIndex = -1
        cboFormaPago.SelectedIndex = -1
        txtLetras.Text = 0
        mtbFechaLetraIni.Text = ""
        'sTab = 1
        BolEditar = False

        'bolEdit = False
        'CountFacturas = 0
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
        End If

        Timer1.Enabled = True
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'VerPosicion()
        If Me.DgvLista.Rows.Count > 0 Then
            Try
                cSegurosPolizas = New clsSegurosPolizas
                Dim iResult As Integer
                Dim sNroPoliza As String = Trim(DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(1).Value)
                If (MessageBox.Show("�Desea Eliminar la Poliza, detalles, cuotas por pagar y Endosos?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                    iResult = cSegurosPolizas.fEliminaPolizayDetallePoliza(41, gEmpresa, sNroPoliza, "", "", 0, "")
                End If
                If iResult > 0 Then
                    MessageBox.Show("Poliza " & IIf(iResult > 0, "Eliminada", "Actualizado") & " con Exito", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    DgvLista.DataSource = cSegurosPolizas.fListarPolizas(gEmpresa)
                    sTab = 0
                    frmPrincipal.sFlagGrabar = "1"
                End If
                'Dim Dtable As New DataTable
                'Dtable = cSegurosPolizas.fMostrarPoliza(gEmpresa, DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(1).Value).Tables(0)


                'txtPoliza.Text = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(1).Value
                'txtPoliza.Enabled = False
                'mtbVigenciaIni.Text = Dtable.Rows(0).Item("Vigencia_FechaInicio")
                'mtbVigenciaFin.Text = Dtable.Rows(0).Item("Vigencia_FechaFin")
                'cboRamo.SelectedValue = Dtable.Rows(0).Item("CodigoRamo")
                'txtCliente.Text = cSegurosPolizas.DescripcionEmpresa(gEmpresa)
                'txtRUC.Text = cSegurosPolizas.RucEmpresa(gEmpresa)
                'txtDireccion.Text = cSegurosPolizas.DireccionEmpresa(gEmpresa)
                'cboMoneda.SelectedValue = Dtable.Rows(0).Item("MonCodigo")
                'txtPrima.Text = Format(Dtable.Rows(0).Item("Prima"), "#,##0.00")
                'txtDerechoEmision.Text = Format(Dtable.Rows(0).Item("DerechoEmision"), "#,##0.00")
                'txtInteres.Text = Format(Dtable.Rows(0).Item("Intereses"), "#,##0.00")
                'txtIGV.Text = Format(Dtable.Rows(0).Item("IGV"), "#,##0.00")
                'txtTotal.Text = Format(Dtable.Rows(0).Item("Total"), "#,##0.00")
                'cboAseguradora.SelectedValue = Dtable.Rows(0).Item("CodigoAseguradora")
                'txtSumaAseguradora.Text = Format(Dtable.Rows(0).Item("SumaAsegurada"), "#,##0.00")
                'cboFormaPago.SelectedValue = Dtable.Rows(0).Item("CodCondCancelacion")
                'txtLetras.Text = Dtable.Rows(0).Item("NumLetra")
                'mtbFechaLetraIni.Text = IIf(IsDBNull(Dtable.Rows(0).Item("FechaIniLetra")), "", Dtable.Rows(0).Item("FechaIniLetra"))
                ''sTab = 1
                'BolEditar = True
                'Timer1.Enabled = True

                ''bolEdit = True

                'If sTab = 0 Then
                '    sTab = 1
                '    iOpcion = 2
                'End If
            Catch ex As Exception

            End Try
        End If
        


        'If Fila("SituacionId") = "00001" Then
        '    Dim iResultado As Int32
        '    Dim iResultadoDesSaldoDoc As Int32
        '    Dim iActivo As Integer
        '    Dim sCodigoActual2 As String = ""
        '    If sTab = 0 Then
        '        Try
        '            If Me.dgvDetalle.Rows.Count > 0 Then
        '                iActivo = 3
        '                If (MessageBox.Show("�Desea Eliminar el Registro?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        '                    'VerPosicion()
        '                    eRegistroDocumento = New clsRegistroDocumento
        '                    'encuestadoras compradas, apoyando el fraude, le daran el favoritismo a keiko hasta el dia de las elecciones, y como alan le confirmara la victoria diran gano en las encuestas y gano en la 2da vuelta! no a la FARSA, SI a las barricadas, SI a la marcha de los 4 SUYOS!!!
        '                    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        '                    'ELIMINAR(UPDATE) REGISTROS DE LAS TABLAS COMUN
        '                    If eRegistroDocumento.Delete_Registro(Fila("IdRegistro"), gEmpresa, gPeriodo) = False Then
        '                        MessageBox.Show("Ocurrio un Error en el Proceso para Eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '                        Exit Sub
        '                    End If
        '                    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        '                    iResultado = eRegistroDocumento.fEliminar(Fila("IdRegistro"), iActivo, gEmpresa) '18/
        '                    If Trim(Fila("IdDocumento")) = "05" Then
        '                        Dim NCGlosaFactura As String = ""
        '                        Dim iResultadoActFacdeNC As Integer = 0
        '                        NCGlosaFactura = Trim("NA" & Fila("Serie") & "-" & Fila("NroDocumento"))
        '                        iResultadoActFacdeNC = eRegistroDocumento.fActualizarCamposFactura(49, Fila("NCCodFT"), Fila("NCCodFTEmpr"), Trim(NCGlosaFactura), Fila("ImporteTotal"))
        '                    End If

        '                    ''en caso de adelanto de valorizacion q hareeee�
        '                    iResultadoDesSaldoDoc = eRegistroDocumento.fEliminar2(Fila("IdRegistro"), 4, gEmpresa) '18/

        '                    'eValorizaciones = New clsValorizaciones
        '                    'iResultadoMod = eValorizaciones.fActualizarSaldos(52, 0, 0, 0, Today(), Trim(sCodigoActual), gEmpresa, Trim(IdEmprValor), Trim(IdValorizacion), "")


        '                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '                    Dim iResultado1 As Integer = 0
        '                    ePagoProveedores = New clsPagoProveedores
        '                    'If Trim(cboMoneda.SelectedValue) = "01" Then

        '                    If Fila("IdMoneda") = "01" Then
        '                        Dim dtTable As DataTable
        '                        dtTable = New DataTable
        '                        'cboTipoAcreedor.SelectedValue = Fila("TipoAcreedorId").ToString
        '                        'txtCod.Text = Fila("CodProveedor").ToString
        '                        dtTable = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), "01")
        '                        If dtTable.Rows.Count = 0 Then
        '                            'crear
        '                            Dim IdSaldoProv As String = ""
        '                            ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
        '                            IdSaldoProv = ePagoProveedores.sCodFuturo
        '                            iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, Val(Fila("ImporteTotal").ToString), "01")
        '                        ElseIf dtTable.Rows.Count = 1 Then
        '                            'actualizar saldo prove moneda soles ++++++++++
        '                            Dim IdSaldoProv As String = ""
        '                            Dim MontoSaldoSolesProv As Double = 0
        '                            Dim MontoSaldoSolesNew As Double = 0
        '                            IdSaldoProv = dtTable.Rows(0).Item("IdSaldo")
        '                            MontoSaldoSolesProv = dtTable.Rows(0).Item("MontoSaldo")
        '                            MontoSaldoSolesNew = MontoSaldoSolesProv - Val(Trim(Fila("ImporteTotal").ToString)) 'Val(txtImporteTotal.Text)
        '                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, MontoSaldoSolesNew, "01")
        '                        End If
        '                    ElseIf Fila("IdMoneda") = "02" Then
        '                        Dim dtTable2 As DataTable
        '                        dtTable2 = New DataTable
        '                        dtTable2 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), "02")
        '                        If dtTable2.Rows.Count = 0 Then
        '                            'crear
        '                            Dim IdSaldoProv As String = ""
        '                            ePagoProveedores.GenerarCodIdSaldoProv(gEmpresa)
        '                            IdSaldoProv = ePagoProveedores.sCodFuturo
        '                            iResultado1 = ePagoProveedores.GrabarSaldoProv(18, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, Val(Fila("ImporteTotal").ToString), "02")
        '                        ElseIf dtTable2.Rows.Count = 1 Then
        '                            'actualizar saldo prove moneda dolares ++++++++++
        '                            Dim IdSaldoProv As String = ""
        '                            Dim MontoSaldoDolaresProv As Double = 0
        '                            Dim MontoSaldoDolaresNew As Double = 0
        '                            IdSaldoProv = dtTable2.Rows(0).Item("IdSaldo")
        '                            MontoSaldoDolaresProv = dtTable2.Rows(0).Item("MontoSaldo")
        '                            MontoSaldoDolaresNew = MontoSaldoDolaresProv - Trim(Fila("ImporteTotal").ToString) 'Val(txtImporteTotal.Text)
        '                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, Trim(Fila("TipoAcreedorId").ToString), Trim(Fila("CodProveedor").ToString), gEmpresa, MontoSaldoDolaresNew, "02")
        '                        End If
        '                    End If

        '                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '                    If iResultado > 0 Then
        '                        mMostrarGrilla()
        '                    End If
        '                Else
        '                    Exit Sub
        '                End If
        '            End If
        '        Catch ex As Exception
        '        End Try
        '    End If
        'ElseIf Fila("SituacionId") <> "00001" Then
        '    Dim Mensaje As String = ""
        '    If Fila("SituacionId") = "00002" Then
        '        Mensaje = "No se puede eliminar el documento, porque se encuentra Aprovado."
        '    ElseIf Fila("SituacionId") = "00003" Then
        '        Mensaje = "No se puede eliminar el documento, porque se encuentra Provisionado."
        '    ElseIf Fila("SituacionId") = "00004" Then
        '        Mensaje = "No se puede eliminar el documento, porque se encuentra Cancelado."
        '    End If
        '    MessageBox.Show(Mensaje, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        'End If

        'If Fila("Estados") = "Activo" Then
        'Else
        'MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End If
    End Sub

    Function ValidarIngreso() As Boolean
        If txtPoliza.Text = "" Then
            MessageBox.Show("Ingrese Numero de Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtPoliza.Focus()
            Return False
        ElseIf (mtbVigenciaIni.Text.Length <> 10 Or mtbVigenciaFin.Text.Length <> 10) Then
            MessageBox.Show("Ingrese correctamente Vigencia de Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            mtbVigenciaIni.Focus()
            Return False
        ElseIf cboRamo.SelectedIndex = -1 Then
            MessageBox.Show("Ingrese Ramo para la Poliza", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboRamo.Focus()
            Return False

        ElseIf cboAseguradora.SelectedIndex = -1 Then
            MessageBox.Show("Ingrese Cia. Aseguradora", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboAseguradora.Focus()
            Return False
        ElseIf cboMoneda.SelectedIndex = -1 Then
            MessageBox.Show("Ingrese Moneda", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboMoneda.Focus()
            Return False
        ElseIf cboFormaPago.SelectedIndex = -1 Then
            MessageBox.Show("Ingrese Forma de Pago", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboFormaPago.Focus()
            Return False
        ElseIf cboFormaPago.SelectedIndex = 1 Then
            If txtLetras.Text = "" Then
                MessageBox.Show("Ingrese Numero de Letras", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtLetras.Focus()
                Return False
            ElseIf mtbFechaLetraIni.Text.Length <> 10 Then
                MessageBox.Show("Ingrese Fecha Inicial de Letra", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                mtbFechaLetraIni.Focus()
                Return False
            End If
        End If
        Return True
    End Function
    Private Sub frmMantePoliza_GrabaReg() Handles Me.GrabaReg
        Dim bResultado As Boolean
        cSegurosPolizas = New clsSegurosPolizas
        If ValidarIngreso() = False Then Exit Sub

        '********* txtSumaAseguradora = Interes de Financiacion 
        If cSegurosPolizas.fVerificaPagosPoliza(gEmpresa, txtPoliza.Text.Trim, txtPoliza.Text.Trim) > 0 Then
            MessageBox.Show("No se puede Actualizar la Informacion porque la Poliza " & txtPoliza.Text.Trim & " tiene Pagos", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        If MessageBox.Show("�Desea Grabar?", "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            bResultado = cSegurosPolizas.fGrabarPolizas(gEmpresa, txtPoliza.Text.Trim, cboRamo.SelectedValue, Convert.ToDateTime(mtbVigenciaIni.Text), _
                        Convert.ToDateTime(mtbVigenciaFin.Text), txtRUC.Text, txtDireccion.Text.Trim, cboMoneda.SelectedValue, _
                        Convert.ToDecimal(txtPrima.Text), Convert.ToDecimal(txtDerechoEmision.Text), Convert.ToDecimal(txtInteres.Text), _
                        Convert.ToDecimal(txtIGV.Text), Convert.ToDecimal(txtTotal.Text), cboAseguradora.SelectedValue, Convert.ToDecimal(txtSumaAseguradora.Text), _
                        cboFormaPago.SelectedValue, Convert.ToInt32(txtLetras.Text), Convert.ToDateTime(IIf(mtbFechaLetraIni.Text.Trim.Length < 10, Today, mtbFechaLetraIni.Text)), BolEditar, gUsuario)
            If bResultado = True Then
                If cboFormaPago.SelectedValue = "02" Then   'CUOTAS
                    cSegurosPolizas.fGenerarCronogramaPagoPoliza(gEmpresa, txtPoliza.Text.Trim, Convert.ToDecimal(txtTotal.Text), Convert.ToDecimal(txtSumaAseguradora.Text), Convert.ToInt32(txtLetras.Text), Convert.ToDateTime(IIf(mtbFechaLetraIni.Text.Trim.Length < 10, Today, mtbFechaLetraIni.Text)), gUsuario)
                End If
                MessageBox.Show("Poliza " & IIf(BolEditar = False, "Grabada", "Actualizado") & " con Exito", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                DgvLista.DataSource = cSegurosPolizas.fListarPolizas(gEmpresa)
                sTab = 0
                frmPrincipal.sFlagGrabar = "1"
            Else
                MessageBox.Show("No se Grabo la Poliza", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
    Private Sub frmMantePoliza_ModificaReg() Handles Me.ModificaReg
        Try
            cSegurosPolizas = New clsSegurosPolizas
            Dim sNroPoliza As String = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(0).Value
            Dim Dtable As New DataTable
            Dtable = cSegurosPolizas.fMostrarPoliza(gEmpresa, DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(1).Value).Tables(0)

            txtPoliza.Text = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(1).Value
            txtPoliza.Enabled = False
            mtbVigenciaIni.Text = Dtable.Rows(0).Item("Vigencia_FechaInicio")
            mtbVigenciaFin.Text = Dtable.Rows(0).Item("Vigencia_FechaFin")
            cboRamo.SelectedValue = Dtable.Rows(0).Item("CodigoRamo")
            txtCliente.Text = cSegurosPolizas.DescripcionEmpresa(gEmpresa)
            txtRUC.Text = cSegurosPolizas.RucEmpresa(gEmpresa)
            txtDireccion.Text = cSegurosPolizas.DireccionEmpresa(gEmpresa)
            cboMoneda.SelectedValue = Dtable.Rows(0).Item("MonCodigo")
            txtPrima.Text = Format(Dtable.Rows(0).Item("Prima"), "#,##0.00")
            txtDerechoEmision.Text = Format(Dtable.Rows(0).Item("DerechoEmision"), "#,##0.00")
            txtInteres.Text = Format(Dtable.Rows(0).Item("Intereses"), "#,##0.00")
            txtIGV.Text = Format(Dtable.Rows(0).Item("IGV"), "#,##0.00")
            txtTotal.Text = Format(Dtable.Rows(0).Item("Total"), "#,##0.00")
            cboAseguradora.SelectedValue = Dtable.Rows(0).Item("CodigoAseguradora")
            txtSumaAseguradora.Text = Format(Dtable.Rows(0).Item("SumaAsegurada"), "#,##0.00")
            cboFormaPago.SelectedValue = Dtable.Rows(0).Item("CodCondCancelacion")
            txtLetras.Text = Dtable.Rows(0).Item("NumLetra")
            mtbFechaLetraIni.Text = IIf(IsDBNull(Dtable.Rows(0).Item("FechaIniLetra")), "", Dtable.Rows(0).Item("FechaIniLetra"))
            'sTab = 1
            BolEditar = True
            Timer1.Enabled = True

            'bolEdit = True

            If sTab = 0 Then
                sTab = 1
                iOpcion = 2
            End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub DgvLista_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DgvLista.DoubleClick
        Dim frmDetaPoliza As New FrmDetallePoliza
        frmDetaPoliza.FrmMantPoliza = Me

        If DgvLista.Rows.Count > 0 Then
            frmDetaPoliza.CodPoliza = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells("Poliza").Value()
            frmDetaPoliza.Ramo = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells("Ramo").Value
            frmDetaPoliza.Aseguradora = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells("Aseguradora").Value
        Else
            frmDetaPoliza.CodPoliza = ""
            frmDetaPoliza.Ramo = ""
            frmDetaPoliza.Aseguradora = ""
        End If

        frmDetaPoliza.ShowDialog()
    End Sub

    Private Sub txtFiltro_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFiltro.KeyDown
        Select Case e.KeyCode
            Case Keys.Up
                Me.BindingContext(DT_ListPolizas).Position = Me.BindingContext(DT_ListPolizas).Position - 1
            Case Keys.Down
                Me.BindingContext(DT_ListPolizas).Position = Me.BindingContext(DT_ListPolizas).Position + 1
        End Select
    End Sub

    Private Sub txtFiltro_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFiltro.TextChanged
        Try
            DvFiltro.RowFilter = "[" & cboBusqueda.Text & "]" & " LIKE '" & txtFiltro.Text.Trim & "%'"
            lblTotalRegistro.Text = DgvLista.Rows.Count
        Catch ex As Exception
            MessageBox.Show("No se puede Buscar lo Solicitado", "Sistema Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFiltro.Text = String.Empty
        End Try
    End Sub

    Private Sub mtbFechaLetraIni_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles mtbFechaLetraIni.GotFocus
        'If BolEditar = False Then mtbFechaLetraIni.Text = mtbVigenciaIni.Text
    End Sub

    Private Sub DgvLista_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DgvLista.KeyDown
        Select Case e.KeyCode
            Case Keys.F4





                Try
                    cSegurosPolizas = New clsSegurosPolizas
                    Dim sNroPoliza As String = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(0).Value
                    Dim Dtable As New DataTable
                    Dtable = cSegurosPolizas.fMostrarPoliza(gEmpresa, DgvLista.Rows(DgvLista.CurrentRow.Index).Cells(1).Value).Tables(0)
                    If Dtable.Rows.Count > 0 Then
                        If Dtable.Rows(0).Item("CodCondCancelacion") = "02" Then
                            Dim frmCronoPoliza As frmCronogramaPoliza = frmCronogramaPoliza.Instance
                            frmCronoPoliza.FrmMantPoliza = Me
                            If DgvLista.Rows.Count > 0 Then
                                frmCronoPoliza.sCodPolizaOriginal = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells("Poliza").Value()
                            Else
                                frmCronoPoliza.sCodPolizaOriginal = ""
                            End If

                            frmCronoPoliza.ShowDialog()
                        Else
                            MessageBox.Show("Esta Poliza se pagara al Contado", "Sistema Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                Catch ex As Exception

                End Try




                

            Case Keys.Enter
                e.Handled = True
        End Select
    End Sub

    Private Sub DgvLista_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DgvLista.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                Dim frmDetaPoliza As New FrmDetallePoliza
                frmDetaPoliza.FrmMantPoliza = Me
                If DgvLista.Rows.Count > 0 Then
                    frmDetaPoliza.CodPoliza = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells("Poliza").Value()
                    frmDetaPoliza.Ramo = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells("Ramo").Value
                    frmDetaPoliza.Aseguradora = DgvLista.Rows(DgvLista.CurrentRow.Index).Cells("Aseguradora").Value
                Else
                    frmDetaPoliza.CodPoliza = ""
                    frmDetaPoliza.Ramo = ""
                    frmDetaPoliza.Aseguradora = ""
                End If

                

                frmDetaPoliza.ShowDialog()
        End Select
    End Sub

    Private Sub btnNuevoProv_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoProv.Click
        FrmRegistroFast.ShowDialog()
        Me.txtSumaAseguradora.Focus()
    End Sub

    Private Sub cboAseguradora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAseguradora.Click
        cboAseguradora.DataSource = cSegurosPolizas.ListarAseguradoras(gEmpresa)
        cboAseguradora.ValueMember = "Codigo"
        cboAseguradora.DisplayMember = "Descripcion"
        cboAseguradora.SelectedIndex = -1
    End Sub

    Private Sub txtSumaAseguradora_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSumaAseguradora.LostFocus
        If txtSumaAseguradora.Text = "" Then txtSumaAseguradora.Text = 0
    End Sub

    Private Sub mtbVigenciaIni_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles mtbVigenciaIni.LostFocus
        If BolEditar = False Then mtbFechaLetraIni.Text = mtbVigenciaIni.Text
    End Sub

    Private Sub txtTotal_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotal.LostFocus
        Dim Imp1, Imp2, Imp3, Imp4, Imp5 As Double

        Imp1 = txtPrima.Text
        Imp2 = txtDerechoEmision.Text
        Imp3 = txtInteres.Text
        Imp4 = txtIGV.Text
        Imp5 = txtTotal.Text

        txtPrima.Text = Format(Imp1, "#,##0.00")
        txtDerechoEmision.Text = Format(Imp2, "#,##0.00")
        txtInteres.Text = Format(Imp3, "#,##0.00")
        txtIGV.Text = Format(Imp4, "#,##0.00")
        txtTotal.Text = Format(Imp5, "#,##0.00")

    End Sub

    
    Private Sub DgvLista_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvLista.CellContentClick

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Dim x As New frmMantePolizaSub
        x.MdiParent = frmPrincipal
        'x.Show()

        x.NroPoli = Trim(txtPoliza.Text)
        x.VigenciaPoli = "Del " & mtbVigenciaIni.Text & " al " & mtbVigenciaFin.Text

        x.VigenciaIni = Convert.ToDateTime(mtbVigenciaIni.Text)
        x.VigenciaFin = Convert.ToDateTime(mtbVigenciaFin.Text)

        x.Moneda = Trim(cboMoneda.Text)
        x.Ramo = Trim(cboRamo.Text)
        x.Cliente = Trim(txtCliente.Text)
        x.Compa�ia = Trim(cboAseguradora.Text)

        x.RamoCod = Trim(cboRamo.SelectedValue)
        x.RUC = Trim(txtRUC.Text)
        x.Direccion = Trim(txtDireccion.Text)
        x.CodMoneda = Trim(cboMoneda.SelectedValue)
        x.CompCod = Trim(cboAseguradora.SelectedValue)

        x.ShowInTaskbar = False
        'x.ShowDialog()
        x.Show()
    End Sub
End Class
