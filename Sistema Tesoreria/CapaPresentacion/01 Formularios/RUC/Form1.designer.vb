﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtCapcha = New System.Windows.Forms.TextBox()
        Me.txtNumDni = New System.Windows.Forms.TextBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.label2 = New System.Windows.Forms.Label()
        Me.txtRazon = New System.Windows.Forms.TextBox()
        Me.txtRuc = New System.Windows.Forms.TextBox()
        Me.txtDireccion = New System.Windows.Forms.TextBox()
        Me.label5 = New System.Windows.Forms.Label()
        Me.label4 = New System.Windows.Forms.Label()
        Me.label1 = New System.Windows.Forms.Label()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnConsultar = New System.Windows.Forms.Button()
        Me.pictureCapcha = New System.Windows.Forms.PictureBox()
        Me.txtEstado = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        CType(Me.pictureCapcha, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCapcha
        '
        Me.txtCapcha.Location = New System.Drawing.Point(123, 35)
        Me.txtCapcha.MaxLength = 4
        Me.txtCapcha.Name = "txtCapcha"
        Me.txtCapcha.Size = New System.Drawing.Size(153, 20)
        Me.txtCapcha.TabIndex = 17
        '
        'txtNumDni
        '
        Me.txtNumDni.Location = New System.Drawing.Point(123, 9)
        Me.txtNumDni.MaxLength = 11
        Me.txtNumDni.Name = "txtNumDni"
        Me.txtNumDni.Size = New System.Drawing.Size(153, 20)
        Me.txtNumDni.TabIndex = 18
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(12, 38)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(83, 13)
        Me.label3.TabIndex = 15
        Me.label3.Text = "Codigo Captcha"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(12, 12)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(30, 13)
        Me.label2.TabIndex = 16
        Me.label2.Text = "RUC"
        '
        'txtRazon
        '
        Me.txtRazon.Location = New System.Drawing.Point(15, 121)
        Me.txtRazon.Name = "txtRazon"
        Me.txtRazon.Size = New System.Drawing.Size(401, 20)
        Me.txtRazon.TabIndex = 12
        '
        'txtRuc
        '
        Me.txtRuc.Location = New System.Drawing.Point(15, 164)
        Me.txtRuc.Name = "txtRuc"
        Me.txtRuc.Size = New System.Drawing.Size(401, 20)
        Me.txtRuc.TabIndex = 13
        '
        'txtDireccion
        '
        Me.txtDireccion.Location = New System.Drawing.Point(15, 203)
        Me.txtDireccion.Multiline = True
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(401, 40)
        Me.txtDireccion.TabIndex = 14
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(12, 105)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(70, 13)
        Me.label5.TabIndex = 9
        Me.label5.Text = "Razon Social"
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(12, 148)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(30, 13)
        Me.label4.TabIndex = 10
        Me.label4.Text = "RUC"
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Location = New System.Drawing.Point(12, 187)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(52, 13)
        Me.label1.TabIndex = 11
        Me.label1.Text = "Direccion"
        '
        'btnActualizar
        '
        Me.btnActualizar.Location = New System.Drawing.Point(282, 61)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(134, 33)
        Me.btnActualizar.TabIndex = 7
        Me.btnActualizar.Text = "Actualizar Captcha"
        Me.btnActualizar.UseVisualStyleBackColor = True
        '
        'btnConsultar
        '
        Me.btnConsultar.Location = New System.Drawing.Point(15, 61)
        Me.btnConsultar.Name = "btnConsultar"
        Me.btnConsultar.Size = New System.Drawing.Size(261, 33)
        Me.btnConsultar.TabIndex = 8
        Me.btnConsultar.Text = "Realizar Consulta"
        Me.btnConsultar.UseVisualStyleBackColor = True
        '
        'pictureCapcha
        '
        Me.pictureCapcha.BackColor = System.Drawing.Color.White
        Me.pictureCapcha.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pictureCapcha.Location = New System.Drawing.Point(282, 9)
        Me.pictureCapcha.Name = "pictureCapcha"
        Me.pictureCapcha.Size = New System.Drawing.Size(134, 46)
        Me.pictureCapcha.TabIndex = 6
        Me.pictureCapcha.TabStop = False
        '
        'txtEstado
        '
        Me.txtEstado.Location = New System.Drawing.Point(15, 267)
        Me.txtEstado.Name = "txtEstado"
        Me.txtEstado.Size = New System.Drawing.Size(401, 20)
        Me.txtEstado.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 251)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(40, 13)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Estado"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 290)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(54, 13)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Telefonos"
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(15, 306)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(401, 20)
        Me.txtTelefono.TabIndex = 22
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(136, 355)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(54, 13)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Telefonos"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(430, 394)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtEstado)
        Me.Controls.Add(Me.txtCapcha)
        Me.Controls.Add(Me.txtNumDni)
        Me.Controls.Add(Me.label3)
        Me.Controls.Add(Me.label2)
        Me.Controls.Add(Me.txtRazon)
        Me.Controls.Add(Me.txtRuc)
        Me.Controls.Add(Me.txtDireccion)
        Me.Controls.Add(Me.label5)
        Me.Controls.Add(Me.label4)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.btnActualizar)
        Me.Controls.Add(Me.btnConsultar)
        Me.Controls.Add(Me.pictureCapcha)
        Me.Name = "Form1"
        Me.Text = "Consulta Ruc - Sunat"
        CType(Me.pictureCapcha, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents txtCapcha As System.Windows.Forms.TextBox
    Private WithEvents txtNumDni As System.Windows.Forms.TextBox
    Private WithEvents label3 As System.Windows.Forms.Label
    Private WithEvents label2 As System.Windows.Forms.Label
    Private WithEvents txtRazon As System.Windows.Forms.TextBox
    Private WithEvents txtRuc As System.Windows.Forms.TextBox
    Private WithEvents txtDireccion As System.Windows.Forms.TextBox
    Private WithEvents label5 As System.Windows.Forms.Label
    Private WithEvents label4 As System.Windows.Forms.Label
    Private WithEvents label1 As System.Windows.Forms.Label
    Private WithEvents btnActualizar As System.Windows.Forms.Button
    Private WithEvents btnConsultar As System.Windows.Forms.Button
    Private WithEvents pictureCapcha As System.Windows.Forms.PictureBox
    Private WithEvents txtEstado As System.Windows.Forms.TextBox
    Private WithEvents Label6 As System.Windows.Forms.Label
    Private WithEvents Label7 As System.Windows.Forms.Label
    Private WithEvents txtTelefono As System.Windows.Forms.TextBox
    Private WithEvents Label8 As System.Windows.Forms.Label

End Class
