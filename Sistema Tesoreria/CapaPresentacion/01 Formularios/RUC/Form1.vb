﻿Imports CapaPreTesoreria


Public Class Form1
    Dim myInfo As ConsultaRuc

    Public Sub New()
        InitializeComponent()
        Try
            CargarImagen()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Sub CaptionResul()
        Try
            Select Case myInfo.GetResul
                Case ConsultaRuc.Resul.Ok
                    Me.txtDireccion.Text = myInfo.ApeMaterno
                    Me.txtRazon.Text = myInfo.Nombres
                    Me.txtRuc.Text = txtNumDni.Text
                    Me.txtEstado.Text = myInfo.Estado
                    Me.txtTelefono.Text = myInfo.Telefono
                    Me.txtNumDni.Text = ""
                    Me.txtCapcha.Text = ""
                    Exit Select
                Case ConsultaRuc.Resul.NoResul
                    Me.Label8.Text = "No existe RUC"
                    Me.txtDireccion.Text = ""
                    Me.txtRazon.Text = ""
                    Me.txtRuc.Text = ""
                    Me.txtEstado.Text = ""
                    Me.txtTelefono.Text = ""
                    Exit Select
                Case ConsultaRuc.Resul.ErrorCapcha
                    CargarImagen()
                    Me.Label8.Text = "Ingrese imagen correctamente"
                    Exit Select
                Case ConsultaRuc.Resul.[Error]
                    Me.Label8.Text = "Error Desconocido"
                    Exit Select
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub CargarImagen()
        Try
            If myInfo Is Nothing Then
                myInfo = New ConsultaRuc
            End If
            Me.pictureCapcha.Image = myInfo.GetCapcha
        Catch ex As Exception
            Throw ex
        End Try

    End Sub
    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        Try
            CargarImagen()
            Me.txtCapcha.SelectAll()
            Me.txtCapcha.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        Label8.Text = ""
        Try
            If Me.txtNumDni.Text.Length <> 11 Then
                Me.Label8.Text = "Ingrese ruc Valido"
                Me.txtNumDni.SelectAll()
                Me.txtNumDni.Focus()
                Return
            End If
            myInfo.GetInfo(Me.txtNumDni.Text, Me.txtCapcha.Text)
            CaptionResul()
            CargarImagen()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label8.Text = ""
    End Sub
End Class

