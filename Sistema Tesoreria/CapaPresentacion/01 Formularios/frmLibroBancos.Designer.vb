<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLibroBancos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnEliminar = New ctrLibreria.Controles.BeButton
        Me.btnBuscar = New ctrLibreria.Controles.BeButton
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel
        Me.btnNuevo = New ctrLibreria.Controles.BeButton
        Me.btnGrabar = New ctrLibreria.Controles.BeButton
        Me.btnConciliacion = New ctrLibreria.Controles.BeButton
        Me.btnItfCant = New ctrLibreria.Controles.BeButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblTipoCta = New ctrLibreria.Controles.BeLabel
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.cboAno = New ctrLibreria.Controles.BeComboBox
        Me.cboCuentas = New ctrLibreria.Controles.BeComboBox
        Me.lblMoneda = New ctrLibreria.Controles.BeLabel
        Me.cboBancos = New ctrLibreria.Controles.BeComboBox
        Me.cboMes = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.txtCod = New ctrLibreria.Controles.BeTextBox
        Me.lblCtaEn = New ctrLibreria.Controles.BeLabel
        Me.lblCodMoneda = New ctrLibreria.Controles.BeLabel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.BeLabel31 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldoDisponible = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel30 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldoRetenido = New ctrLibreria.Controles.BeTextBox
        Me.chkConciliacionMes = New System.Windows.Forms.CheckBox
        Me.BeButton1 = New ctrLibreria.Controles.BeButton
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.dtpFIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.cboTipoCargo = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel27 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel26 = New ctrLibreria.Controles.BeLabel
        Me.txtObra = New ctrLibreria.Controles.BeTextBox
        Me.txtCtaCliente = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel24 = New ctrLibreria.Controles.BeLabel
        Me.txtCliente = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel
        Me.dtpFfin = New ctrLibreria.Controles.BeDateTimePicker
        Me.txtNroMov = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel
        Me.dtpFechaPago = New ctrLibreria.Controles.BeDateTimePicker
        Me.chkFecha = New System.Windows.Forms.CheckBox
        Me.rdbCargo = New System.Windows.Forms.RadioButton
        Me.rdbAbono = New System.Windows.Forms.RadioButton
        Me.cboTipoMov = New ctrLibreria.Controles.BeComboBox
        Me.txtNroDoc = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.txtImporte = New ctrLibreria.Controles.BeTextBox
        Me.txtDescripcion = New System.Windows.Forms.TextBox
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.txtConcepto = New System.Windows.Forms.TextBox
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.dtpFecha = New ctrLibreria.Controles.BeDateTimePicker
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.dtpFechaCobro = New ctrLibreria.Controles.BeDateTimePicker
        Me.btnConfirmar = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.btnGenerarITF = New System.Windows.Forms.Button
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldoInicial = New ctrLibreria.Controles.BeTextBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel
        Me.txtITF2 = New ctrLibreria.Controles.BeTextBox
        Me.txtImporITF = New System.Windows.Forms.TextBox
        Me.txtSumaChe = New System.Windows.Forms.TextBox
        Me.txtGlosaITFCant = New System.Windows.Forms.TextBox
        Me.btnCancelarITFCant = New System.Windows.Forms.Button
        Me.dtpITFCant = New ctrLibreria.Controles.BeDateTimePicker
        Me.btnGrabarITFCant = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.txtCobradosSi = New ctrLibreria.Controles.BeTextBox
        Me.txtCobradosNo = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtITF1 = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel
        Me.txtImporteItf = New System.Windows.Forms.TextBox
        Me.txtGlosaItf = New System.Windows.Forms.TextBox
        Me.btnCancelarItf = New System.Windows.Forms.Button
        Me.dtpFechaItf = New ctrLibreria.Controles.BeDateTimePicker
        Me.btnGrabarItf = New System.Windows.Forms.Button
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.dgvLibroDet = New System.Windows.Forms.DataGridView
        Me.txtSaldo = New ctrLibreria.Controles.BeTextBox
        Me.txtTipoCambio = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtCodDet = New ctrLibreria.Controles.BeTextBox
        Me.txtMarca2 = New ctrLibreria.Controles.BeTextBox
        Me.txtMarca = New ctrLibreria.Controles.BeTextBox
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdCab = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Marca1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NroDocumento2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ITF = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column22 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Identificarx = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Conciliado = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.ConciliadoS = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaPago = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnEliminar)
        Me.GroupBox3.Controls.Add(Me.btnBuscar)
        Me.GroupBox3.Controls.Add(Me.TextBox1)
        Me.GroupBox3.Controls.Add(Me.BeLabel23)
        Me.GroupBox3.Controls.Add(Me.btnNuevo)
        Me.GroupBox3.Controls.Add(Me.btnGrabar)
        Me.GroupBox3.Controls.Add(Me.btnConciliacion)
        Me.GroupBox3.Controls.Add(Me.btnItfCant)
        Me.GroupBox3.Location = New System.Drawing.Point(1055, 120)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(164, 151)
        Me.GroupBox3.TabIndex = 25
        Me.GroupBox3.TabStop = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(121, 57)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(35, 30)
        Me.btnEliminar.TabIndex = 28
        Me.btnEliminar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        Me.btnBuscar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnBuscar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuscar.Location = New System.Drawing.Point(80, 57)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(35, 30)
        Me.btnBuscar.TabIndex = 4
        Me.btnBuscar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(4, 29)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(152, 20)
        Me.TextBox1.TabIndex = 30
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(32, 13)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(94, 13)
        Me.BeLabel23.TabIndex = 31
        Me.BeLabel23.Text = "Buscar en Lista"
        '
        'btnNuevo
        '
        Me.btnNuevo.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnNuevo.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__File_new_
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(4, 57)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(35, 30)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(41, 57)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(35, 30)
        Me.btnGrabar.TabIndex = 2
        Me.btnGrabar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnConciliacion
        '
        Me.btnConciliacion.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnConciliacion.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConciliacion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnConciliacion.Location = New System.Drawing.Point(6, 122)
        Me.btnConciliacion.Name = "btnConciliacion"
        Me.btnConciliacion.Size = New System.Drawing.Size(150, 23)
        Me.btnConciliacion.TabIndex = 0
        Me.btnConciliacion.Text = "CONCILIACION"
        Me.btnConciliacion.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnConciliacion.UseVisualStyleBackColor = True
        '
        'btnItfCant
        '
        Me.btnItfCant.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnItfCant.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnItfCant.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnItfCant.Location = New System.Drawing.Point(6, 93)
        Me.btnItfCant.Name = "btnItfCant"
        Me.btnItfCant.Size = New System.Drawing.Size(150, 23)
        Me.btnItfCant.TabIndex = 24
        Me.btnItfCant.Text = "ITF Varios"
        Me.btnItfCant.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnItfCant.UseVisualStyleBackColor = True
        Me.btnItfCant.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblTipoCta)
        Me.Panel1.Controls.Add(Me.BeLabel15)
        Me.Panel1.Controls.Add(Me.BeLabel3)
        Me.Panel1.Controls.Add(Me.cboAno)
        Me.Panel1.Controls.Add(Me.cboCuentas)
        Me.Panel1.Controls.Add(Me.lblMoneda)
        Me.Panel1.Controls.Add(Me.cboBancos)
        Me.Panel1.Controls.Add(Me.cboMes)
        Me.Panel1.Controls.Add(Me.BeLabel4)
        Me.Panel1.Controls.Add(Me.BeLabel12)
        Me.Panel1.Controls.Add(Me.BeLabel5)
        Me.Panel1.Controls.Add(Me.txtCod)
        Me.Panel1.Location = New System.Drawing.Point(4, 7)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1228, 30)
        Me.Panel1.TabIndex = 0
        '
        'lblTipoCta
        '
        Me.lblTipoCta.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblTipoCta.AutoSize = True
        Me.lblTipoCta.BackColor = System.Drawing.Color.Transparent
        Me.lblTipoCta.Font = New System.Drawing.Font("Arial Narrow", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoCta.ForeColor = System.Drawing.Color.Navy
        Me.lblTipoCta.Location = New System.Drawing.Point(733, 8)
        Me.lblTipoCta.Name = "lblTipoCta"
        Me.lblTipoCta.Size = New System.Drawing.Size(37, 13)
        Me.lblTipoCta.TabIndex = 15
        Me.lblTipoCta.Text = "[TipoCta]"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(821, 8)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel15.TabIndex = 5
        Me.BeLabel15.Text = "Año"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(932, 8)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(29, 13)
        Me.BeLabel3.TabIndex = 7
        Me.BeLabel3.Text = "Mes"
        '
        'cboAno
        '
        Me.cboAno.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAno.BackColor = System.Drawing.Color.Ivory
        Me.cboAno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAno.ForeColor = System.Drawing.Color.Black
        Me.cboAno.FormattingEnabled = True
        Me.cboAno.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040"})
        Me.cboAno.KeyEnter = True
        Me.cboAno.Location = New System.Drawing.Point(856, 4)
        Me.cboAno.Name = "cboAno"
        Me.cboAno.Size = New System.Drawing.Size(70, 21)
        Me.cboAno.TabIndex = 6
        '
        'cboCuentas
        '
        Me.cboCuentas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuentas.BackColor = System.Drawing.Color.Ivory
        Me.cboCuentas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentas.ForeColor = System.Drawing.Color.Black
        Me.cboCuentas.FormattingEnabled = True
        Me.cboCuentas.KeyEnter = True
        Me.cboCuentas.Location = New System.Drawing.Point(316, 4)
        Me.cboCuentas.Name = "cboCuentas"
        Me.cboCuentas.Size = New System.Drawing.Size(344, 21)
        Me.cboCuentas.TabIndex = 3
        '
        'lblMoneda
        '
        Me.lblMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblMoneda.Font = New System.Drawing.Font("Arial Narrow", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoneda.ForeColor = System.Drawing.Color.Navy
        Me.lblMoneda.Location = New System.Drawing.Point(666, 8)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(37, 13)
        Me.lblMoneda.TabIndex = 4
        Me.lblMoneda.Text = "[Moneda]"
        '
        'cboBancos
        '
        Me.cboBancos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBancos.BackColor = System.Drawing.Color.Ivory
        Me.cboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBancos.ForeColor = System.Drawing.Color.Black
        Me.cboBancos.FormattingEnabled = True
        Me.cboBancos.KeyEnter = True
        Me.cboBancos.Location = New System.Drawing.Point(44, 4)
        Me.cboBancos.Name = "cboBancos"
        Me.cboBancos.Size = New System.Drawing.Size(220, 21)
        Me.cboBancos.TabIndex = 1
        '
        'cboMes
        '
        Me.cboMes.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMes.BackColor = System.Drawing.Color.Ivory
        Me.cboMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMes.ForeColor = System.Drawing.Color.Black
        Me.cboMes.FormattingEnabled = True
        Me.cboMes.Items.AddRange(New Object() {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"})
        Me.cboMes.KeyEnter = True
        Me.cboMes.Location = New System.Drawing.Point(965, 4)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(114, 21)
        Me.cboMes.TabIndex = 8
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(5, 8)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel4.TabIndex = 0
        Me.BeLabel4.Text = "Banco"
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Arial Narrow", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(1087, 9)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel12.TabIndex = 13
        Me.BeLabel12.Text = "Código Libro"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(270, 8)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(48, 13)
        Me.BeLabel5.TabIndex = 2
        Me.BeLabel5.Text = "Cuenta"
        '
        'txtCod
        '
        Me.txtCod.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCod.BackColor = System.Drawing.Color.Ivory
        Me.txtCod.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCod.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCod.Enabled = False
        Me.txtCod.Font = New System.Drawing.Font("Arial Narrow", 6.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCod.ForeColor = System.Drawing.Color.Black
        Me.txtCod.KeyEnter = True
        Me.txtCod.Location = New System.Drawing.Point(1135, 6)
        Me.txtCod.Name = "txtCod"
        Me.txtCod.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCod.ShortcutsEnabled = False
        Me.txtCod.Size = New System.Drawing.Size(84, 17)
        Me.txtCod.TabIndex = 14
        Me.txtCod.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'lblCtaEn
        '
        Me.lblCtaEn.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCtaEn.AutoSize = True
        Me.lblCtaEn.BackColor = System.Drawing.Color.Transparent
        Me.lblCtaEn.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCtaEn.ForeColor = System.Drawing.Color.Black
        Me.lblCtaEn.Location = New System.Drawing.Point(1282, 184)
        Me.lblCtaEn.Name = "lblCtaEn"
        Me.lblCtaEn.Size = New System.Drawing.Size(64, 13)
        Me.lblCtaEn.TabIndex = 17
        Me.lblCtaEn.Text = "[lblCtaEn]"
        Me.lblCtaEn.Visible = False
        '
        'lblCodMoneda
        '
        Me.lblCodMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCodMoneda.AutoSize = True
        Me.lblCodMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblCodMoneda.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodMoneda.ForeColor = System.Drawing.Color.Black
        Me.lblCodMoneda.Location = New System.Drawing.Point(1282, 302)
        Me.lblCodMoneda.Name = "lblCodMoneda"
        Me.lblCodMoneda.Size = New System.Drawing.Size(84, 13)
        Me.lblCodMoneda.TabIndex = 16
        Me.lblCodMoneda.Text = "[CodMoneda]"
        Me.lblCodMoneda.Visible = False
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.BeLabel31)
        Me.Panel2.Controls.Add(Me.txtSaldoDisponible)
        Me.Panel2.Controls.Add(Me.BeLabel30)
        Me.Panel2.Controls.Add(Me.txtSaldoRetenido)
        Me.Panel2.Controls.Add(Me.chkConciliacionMes)
        Me.Panel2.Controls.Add(Me.BeButton1)
        Me.Panel2.Controls.Add(Me.Panel8)
        Me.Panel2.Controls.Add(Me.BeLabel28)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.GroupBox7)
        Me.Panel2.Controls.Add(Me.GroupBox5)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Controls.Add(Me.BeLabel19)
        Me.Panel2.Controls.Add(Me.txtSaldoInicial)
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.BeLabel11)
        Me.Panel2.Controls.Add(Me.dgvLibroDet)
        Me.Panel2.Controls.Add(Me.txtSaldo)
        Me.Panel2.Controls.Add(Me.GroupBox3)
        Me.Panel2.Location = New System.Drawing.Point(4, 43)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1228, 539)
        Me.Panel2.TabIndex = 1
        '
        'BeLabel31
        '
        Me.BeLabel31.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel31.AutoSize = True
        Me.BeLabel31.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel31.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.BeLabel31.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel31.Location = New System.Drawing.Point(799, 517)
        Me.BeLabel31.Name = "BeLabel31"
        Me.BeLabel31.Size = New System.Drawing.Size(127, 16)
        Me.BeLabel31.TabIndex = 36
        Me.BeLabel31.Text = "Saldo Disponible"
        '
        'txtSaldoDisponible
        '
        Me.txtSaldoDisponible.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoDisponible.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldoDisponible.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoDisponible.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoDisponible.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtSaldoDisponible.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldoDisponible.KeyEnter = True
        Me.txtSaldoDisponible.Location = New System.Drawing.Point(929, 515)
        Me.txtSaldoDisponible.Name = "txtSaldoDisponible"
        Me.txtSaldoDisponible.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoDisponible.ShortcutsEnabled = False
        Me.txtSaldoDisponible.Size = New System.Drawing.Size(125, 20)
        Me.txtSaldoDisponible.TabIndex = 37
        Me.txtSaldoDisponible.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoDisponible.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel30
        '
        Me.BeLabel30.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel30.AutoSize = True
        Me.BeLabel30.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel30.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.BeLabel30.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel30.Location = New System.Drawing.Point(810, 495)
        Me.BeLabel30.Name = "BeLabel30"
        Me.BeLabel30.Size = New System.Drawing.Size(116, 16)
        Me.BeLabel30.TabIndex = 34
        Me.BeLabel30.Text = "Saldo Retenido"
        '
        'txtSaldoRetenido
        '
        Me.txtSaldoRetenido.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoRetenido.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldoRetenido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoRetenido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoRetenido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtSaldoRetenido.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldoRetenido.KeyEnter = True
        Me.txtSaldoRetenido.Location = New System.Drawing.Point(929, 493)
        Me.txtSaldoRetenido.Name = "txtSaldoRetenido"
        Me.txtSaldoRetenido.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoRetenido.ShortcutsEnabled = False
        Me.txtSaldoRetenido.Size = New System.Drawing.Size(125, 20)
        Me.txtSaldoRetenido.TabIndex = 35
        Me.txtSaldoRetenido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoRetenido.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'chkConciliacionMes
        '
        Me.chkConciliacionMes.AutoSize = True
        Me.chkConciliacionMes.Location = New System.Drawing.Point(1064, 467)
        Me.chkConciliacionMes.Name = "chkConciliacionMes"
        Me.chkConciliacionMes.Size = New System.Drawing.Size(154, 17)
        Me.chkConciliacionMes.TabIndex = 33
        Me.chkConciliacionMes.Text = "Cerrar Conciliacion del Mes"
        Me.chkConciliacionMes.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkConciliacionMes.UseVisualStyleBackColor = True
        '
        'BeButton1
        '
        Me.BeButton1.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton1.Location = New System.Drawing.Point(1064, 422)
        Me.BeButton1.Name = "BeButton1"
        Me.BeButton1.Size = New System.Drawing.Size(150, 23)
        Me.BeButton1.TabIndex = 32
        Me.BeButton1.Text = "Ver Extracto Bancario"
        Me.BeButton1.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton1.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.MistyRose
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Location = New System.Drawing.Point(646, 472)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(33, 14)
        Me.Panel8.TabIndex = 7
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel28.ForeColor = System.Drawing.Color.Black
        Me.BeLabel28.Location = New System.Drawing.Point(594, 472)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(49, 13)
        Me.BeLabel28.TabIndex = 6
        Me.BeLabel28.Text = "Anexos"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(471, 472)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Label2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 472)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Label1"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.dtpFIni)
        Me.GroupBox7.Controls.Add(Me.cboTipoCargo)
        Me.GroupBox7.Controls.Add(Me.BeLabel27)
        Me.GroupBox7.Controls.Add(Me.BeLabel26)
        Me.GroupBox7.Controls.Add(Me.txtObra)
        Me.GroupBox7.Controls.Add(Me.txtCtaCliente)
        Me.GroupBox7.Controls.Add(Me.BeLabel24)
        Me.GroupBox7.Controls.Add(Me.txtCliente)
        Me.GroupBox7.Controls.Add(Me.BeLabel22)
        Me.GroupBox7.Controls.Add(Me.dtpFfin)
        Me.GroupBox7.Controls.Add(Me.txtNroMov)
        Me.GroupBox7.Controls.Add(Me.BeLabel20)
        Me.GroupBox7.Controls.Add(Me.BeLabel25)
        Me.GroupBox7.Controls.Add(Me.BeLabel21)
        Me.GroupBox7.Location = New System.Drawing.Point(669, 5)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(587, 89)
        Me.GroupBox7.TabIndex = 27
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Otras Operaciones"
        '
        'dtpFIni
        '
        Me.dtpFIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFIni.CustomFormat = ""
        Me.dtpFIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFIni.KeyEnter = True
        Me.dtpFIni.Location = New System.Drawing.Point(187, 16)
        Me.dtpFIni.Name = "dtpFIni"
        Me.dtpFIni.Size = New System.Drawing.Size(83, 20)
        Me.dtpFIni.TabIndex = 9
        Me.dtpFIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'cboTipoCargo
        '
        Me.cboTipoCargo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoCargo.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoCargo.ForeColor = System.Drawing.Color.Black
        Me.cboTipoCargo.FormattingEnabled = True
        Me.cboTipoCargo.KeyEnter = True
        Me.cboTipoCargo.Location = New System.Drawing.Point(444, 14)
        Me.cboTipoCargo.Name = "cboTipoCargo"
        Me.cboTipoCargo.Size = New System.Drawing.Size(106, 21)
        Me.cboTipoCargo.TabIndex = 22
        '
        'BeLabel27
        '
        Me.BeLabel27.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel27.AutoSize = True
        Me.BeLabel27.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel27.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel27.ForeColor = System.Drawing.Color.Black
        Me.BeLabel27.Location = New System.Drawing.Point(404, 18)
        Me.BeLabel27.Name = "BeLabel27"
        Me.BeLabel27.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel27.TabIndex = 21
        Me.BeLabel27.Text = "Cargo"
        '
        'BeLabel26
        '
        Me.BeLabel26.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel26.AutoSize = True
        Me.BeLabel26.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel26.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel26.ForeColor = System.Drawing.Color.Black
        Me.BeLabel26.Location = New System.Drawing.Point(6, 69)
        Me.BeLabel26.Name = "BeLabel26"
        Me.BeLabel26.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel26.TabIndex = 20
        Me.BeLabel26.Text = "Obra"
        '
        'txtObra
        '
        Me.txtObra.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtObra.BackColor = System.Drawing.Color.Ivory
        Me.txtObra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtObra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObra.ForeColor = System.Drawing.Color.Black
        Me.txtObra.KeyEnter = True
        Me.txtObra.Location = New System.Drawing.Point(46, 65)
        Me.txtObra.Name = "txtObra"
        Me.txtObra.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtObra.ShortcutsEnabled = False
        Me.txtObra.Size = New System.Drawing.Size(504, 20)
        Me.txtObra.TabIndex = 19
        Me.txtObra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCtaCliente
        '
        Me.txtCtaCliente.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCtaCliente.BackColor = System.Drawing.Color.Ivory
        Me.txtCtaCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCtaCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCtaCliente.ForeColor = System.Drawing.Color.Black
        Me.txtCtaCliente.KeyEnter = True
        Me.txtCtaCliente.Location = New System.Drawing.Point(324, 42)
        Me.txtCtaCliente.Name = "txtCtaCliente"
        Me.txtCtaCliente.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCtaCliente.ShortcutsEnabled = False
        Me.txtCtaCliente.Size = New System.Drawing.Size(226, 20)
        Me.txtCtaCliente.TabIndex = 17
        Me.txtCtaCliente.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel24
        '
        Me.BeLabel24.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel24.AutoSize = True
        Me.BeLabel24.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel24.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel24.ForeColor = System.Drawing.Color.Black
        Me.BeLabel24.Location = New System.Drawing.Point(5, 46)
        Me.BeLabel24.Name = "BeLabel24"
        Me.BeLabel24.Size = New System.Drawing.Size(26, 13)
        Me.BeLabel24.TabIndex = 16
        Me.BeLabel24.Text = "Cli."
        '
        'txtCliente
        '
        Me.txtCliente.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCliente.BackColor = System.Drawing.Color.Ivory
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCliente.ForeColor = System.Drawing.Color.Black
        Me.txtCliente.KeyEnter = True
        Me.txtCliente.Location = New System.Drawing.Point(48, 42)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCliente.ShortcutsEnabled = False
        Me.txtCliente.Size = New System.Drawing.Size(215, 20)
        Me.txtCliente.TabIndex = 15
        Me.txtCliente.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(276, 20)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(36, 13)
        Me.BeLabel22.TabIndex = 12
        Me.BeLabel22.Text = "Vcto."
        '
        'dtpFfin
        '
        Me.dtpFfin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFfin.CustomFormat = ""
        Me.dtpFfin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFfin.KeyEnter = True
        Me.dtpFfin.Location = New System.Drawing.Point(318, 15)
        Me.dtpFfin.Name = "dtpFfin"
        Me.dtpFfin.Size = New System.Drawing.Size(83, 20)
        Me.dtpFfin.TabIndex = 10
        Me.dtpFfin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFfin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'txtNroMov
        '
        Me.txtNroMov.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroMov.BackColor = System.Drawing.Color.Ivory
        Me.txtNroMov.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroMov.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroMov.ForeColor = System.Drawing.Color.Black
        Me.txtNroMov.KeyEnter = True
        Me.txtNroMov.Location = New System.Drawing.Point(48, 15)
        Me.txtNroMov.Name = "txtNroMov"
        Me.txtNroMov.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroMov.ShortcutsEnabled = False
        Me.txtNroMov.Size = New System.Drawing.Size(88, 20)
        Me.txtNroMov.TabIndex = 8
        Me.txtNroMov.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel20.ForeColor = System.Drawing.Color.Black
        Me.BeLabel20.Location = New System.Drawing.Point(5, 19)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(31, 13)
        Me.BeLabel20.TabIndex = 7
        Me.BeLabel20.Text = "Nro."
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel25.ForeColor = System.Drawing.Color.Black
        Me.BeLabel25.Location = New System.Drawing.Point(276, 46)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(54, 13)
        Me.BeLabel25.TabIndex = 18
        Me.BeLabel25.Text = "Cta. Cli."
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel21.ForeColor = System.Drawing.Color.Black
        Me.BeLabel21.Location = New System.Drawing.Point(134, 19)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel21.TabIndex = 11
        Me.BeLabel21.Text = "Apertura"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.BeLabel29)
        Me.GroupBox5.Controls.Add(Me.dtpFechaPago)
        Me.GroupBox5.Controls.Add(Me.chkFecha)
        Me.GroupBox5.Controls.Add(Me.rdbCargo)
        Me.GroupBox5.Controls.Add(Me.rdbAbono)
        Me.GroupBox5.Controls.Add(Me.cboTipoMov)
        Me.GroupBox5.Controls.Add(Me.txtNroDoc)
        Me.GroupBox5.Controls.Add(Me.BeLabel9)
        Me.GroupBox5.Controls.Add(Me.txtImporte)
        Me.GroupBox5.Controls.Add(Me.txtDescripcion)
        Me.GroupBox5.Controls.Add(Me.BeLabel8)
        Me.GroupBox5.Controls.Add(Me.txtConcepto)
        Me.GroupBox5.Controls.Add(Me.BeLabel10)
        Me.GroupBox5.Controls.Add(Me.BeLabel2)
        Me.GroupBox5.Controls.Add(Me.BeLabel6)
        Me.GroupBox5.Controls.Add(Me.GroupBox6)
        Me.GroupBox5.Controls.Add(Me.dtpFecha)
        Me.GroupBox5.Controls.Add(Me.BeLabel7)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 3)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(655, 115)
        Me.GroupBox5.TabIndex = 26
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Datos Principales"
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel29.ForeColor = System.Drawing.Color.Black
        Me.BeLabel29.Location = New System.Drawing.Point(328, 95)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(72, 13)
        Me.BeLabel29.TabIndex = 15
        Me.BeLabel29.Text = "Fecha Pago"
        '
        'dtpFechaPago
        '
        Me.dtpFechaPago.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaPago.CustomFormat = ""
        Me.dtpFechaPago.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaPago.KeyEnter = True
        Me.dtpFechaPago.Location = New System.Drawing.Point(402, 91)
        Me.dtpFechaPago.Name = "dtpFechaPago"
        Me.dtpFechaPago.Size = New System.Drawing.Size(88, 20)
        Me.dtpFechaPago.TabIndex = 14
        Me.dtpFechaPago.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaPago.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(561, 43)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(91, 17)
        Me.chkFecha.TabIndex = 13
        Me.chkFecha.Text = "Por Identificar"
        Me.chkFecha.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'rdbCargo
        '
        Me.rdbCargo.AutoSize = True
        Me.rdbCargo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbCargo.Location = New System.Drawing.Point(232, 16)
        Me.rdbCargo.Name = "rdbCargo"
        Me.rdbCargo.Size = New System.Drawing.Size(60, 17)
        Me.rdbCargo.TabIndex = 1
        Me.rdbCargo.TabStop = True
        Me.rdbCargo.Text = "Cargo"
        Me.rdbCargo.UseVisualStyleBackColor = True
        '
        'rdbAbono
        '
        Me.rdbAbono.AutoSize = True
        Me.rdbAbono.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbAbono.Location = New System.Drawing.Point(172, 16)
        Me.rdbAbono.Name = "rdbAbono"
        Me.rdbAbono.Size = New System.Drawing.Size(61, 17)
        Me.rdbAbono.TabIndex = 0
        Me.rdbAbono.TabStop = True
        Me.rdbAbono.Text = "Abono"
        Me.rdbAbono.UseVisualStyleBackColor = True
        '
        'cboTipoMov
        '
        Me.cboTipoMov.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoMov.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoMov.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoMov.ForeColor = System.Drawing.Color.Black
        Me.cboTipoMov.FormattingEnabled = True
        Me.cboTipoMov.KeyEnter = True
        Me.cboTipoMov.Location = New System.Drawing.Point(366, 13)
        Me.cboTipoMov.Name = "cboTipoMov"
        Me.cboTipoMov.Size = New System.Drawing.Size(124, 21)
        Me.cboTipoMov.TabIndex = 4
        '
        'txtNroDoc
        '
        Me.txtNroDoc.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroDoc.BackColor = System.Drawing.Color.Ivory
        Me.txtNroDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroDoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroDoc.ForeColor = System.Drawing.Color.Black
        Me.txtNroDoc.KeyEnter = True
        Me.txtNroDoc.Location = New System.Drawing.Point(545, 14)
        Me.txtNroDoc.Name = "txtNroDoc"
        Me.txtNroDoc.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroDoc.ShortcutsEnabled = False
        Me.txtNroDoc.Size = New System.Drawing.Size(104, 20)
        Me.txtNroDoc.TabIndex = 6
        Me.txtNroDoc.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(4, 41)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(61, 13)
        Me.BeLabel9.TabIndex = 7
        Me.BeLabel9.Text = "Concepto"
        '
        'txtImporte
        '
        Me.txtImporte.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImporte.BackColor = System.Drawing.Color.Ivory
        Me.txtImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImporte.ForeColor = System.Drawing.Color.Black
        Me.txtImporte.KeyEnter = True
        Me.txtImporte.Location = New System.Drawing.Point(544, 66)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImporte.ShortcutsEnabled = False
        Me.txtImporte.Size = New System.Drawing.Size(104, 20)
        Me.txtImporte.TabIndex = 12
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImporte.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.Ivory
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.Location = New System.Drawing.Point(75, 60)
        Me.txtDescripcion.MaxLength = 400
        Me.txtDescripcion.Multiline = True
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescripcion.Size = New System.Drawing.Size(415, 27)
        Me.txtDescripcion.TabIndex = 10
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(5, 66)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(73, 13)
        Me.BeLabel8.TabIndex = 9
        Me.BeLabel8.Text = "Descripción"
        '
        'txtConcepto
        '
        Me.txtConcepto.BackColor = System.Drawing.Color.Ivory
        Me.txtConcepto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtConcepto.Location = New System.Drawing.Point(75, 38)
        Me.txtConcepto.MaxLength = 400
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtConcepto.Size = New System.Drawing.Size(415, 20)
        Me.txtConcepto.TabIndex = 8
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(493, 70)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(53, 13)
        Me.BeLabel10.TabIndex = 11
        Me.BeLabel10.Text = "Importe"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(5, 19)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(69, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Fecha Emi."
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(302, 18)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel6.TabIndex = 3
        Me.BeLabel6.Text = "Tipo Mov."
        '
        'GroupBox6
        '
        Me.GroupBox6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox6.Location = New System.Drawing.Point(169, 8)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(128, 27)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        '
        'dtpFecha
        '
        Me.dtpFecha.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFecha.CustomFormat = ""
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.KeyEnter = True
        Me.dtpFecha.Location = New System.Drawing.Point(75, 15)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(88, 20)
        Me.dtpFecha.TabIndex = 2
        Me.dtpFecha.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFecha.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(495, 17)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(31, 13)
        Me.BeLabel7.TabIndex = 5
        Me.BeLabel7.Text = "Nro."
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.GroupBox8)
        Me.Panel5.Controls.Add(Me.Button1)
        Me.Panel5.Controls.Add(Me.btnGenerarITF)
        Me.Panel5.Location = New System.Drawing.Point(316, 242)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(489, 110)
        Me.Panel5.TabIndex = 19
        Me.Panel5.Visible = False
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.dtpFechaCobro)
        Me.GroupBox8.Controls.Add(Me.btnConfirmar)
        Me.GroupBox8.Location = New System.Drawing.Point(174, 13)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(152, 81)
        Me.GroupBox8.TabIndex = 3
        Me.GroupBox8.TabStop = False
        '
        'dtpFechaCobro
        '
        Me.dtpFechaCobro.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaCobro.CustomFormat = ""
        Me.dtpFechaCobro.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaCobro.KeyEnter = True
        Me.dtpFechaCobro.Location = New System.Drawing.Point(33, 19)
        Me.dtpFechaCobro.Name = "dtpFechaCobro"
        Me.dtpFechaCobro.Size = New System.Drawing.Size(88, 20)
        Me.dtpFechaCobro.TabIndex = 3
        Me.dtpFechaCobro.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaCobro.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'btnConfirmar
        '
        Me.btnConfirmar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConfirmar.Location = New System.Drawing.Point(9, 47)
        Me.btnConfirmar.Name = "btnConfirmar"
        Me.btnConfirmar.Size = New System.Drawing.Size(137, 23)
        Me.btnConfirmar.TabIndex = 1
        Me.btnConfirmar.Text = "CONFIRMAR COBRO"
        Me.btnConfirmar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(350, 43)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(127, 23)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "CANCELAR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnGenerarITF
        '
        Me.btnGenerarITF.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerarITF.Location = New System.Drawing.Point(12, 43)
        Me.btnGenerarITF.Name = "btnGenerarITF"
        Me.btnGenerarITF.Size = New System.Drawing.Size(127, 23)
        Me.btnGenerarITF.TabIndex = 0
        Me.btnGenerarITF.Text = "GENERAR ITF"
        Me.btnGenerarITF.UseVisualStyleBackColor = True
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel19.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel19.Location = New System.Drawing.Point(822, 102)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(96, 16)
        Me.BeLabel19.TabIndex = 15
        Me.BeLabel19.Text = "Saldo Inicial"
        '
        'txtSaldoInicial
        '
        Me.txtSaldoInicial.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoInicial.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldoInicial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoInicial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoInicial.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSaldoInicial.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldoInicial.KeyEnter = True
        Me.txtSaldoInicial.Location = New System.Drawing.Point(922, 100)
        Me.txtSaldoInicial.Name = "txtSaldoInicial"
        Me.txtSaldoInicial.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoInicial.ReadOnly = True
        Me.txtSaldoInicial.ShortcutsEnabled = False
        Me.txtSaldoInicial.Size = New System.Drawing.Size(132, 20)
        Me.txtSaldoInicial.TabIndex = 16
        Me.txtSaldoInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoInicial.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.GroupBox4)
        Me.Panel4.Location = New System.Drawing.Point(316, 242)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(489, 110)
        Me.Panel4.TabIndex = 20
        Me.Panel4.Visible = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.BeLabel18)
        Me.GroupBox4.Controls.Add(Me.BeLabel17)
        Me.GroupBox4.Controls.Add(Me.txtITF2)
        Me.GroupBox4.Controls.Add(Me.txtImporITF)
        Me.GroupBox4.Controls.Add(Me.txtSumaChe)
        Me.GroupBox4.Controls.Add(Me.txtGlosaITFCant)
        Me.GroupBox4.Controls.Add(Me.btnCancelarITFCant)
        Me.GroupBox4.Controls.Add(Me.dtpITFCant)
        Me.GroupBox4.Controls.Add(Me.btnGrabarITFCant)
        Me.GroupBox4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(3, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(481, 102)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Registro de ITF"
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(211, 23)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(89, 13)
        Me.BeLabel18.TabIndex = 3
        Me.BeLabel18.Text = "Total Cheques"
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(105, 23)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(41, 13)
        Me.BeLabel17.TabIndex = 1
        Me.BeLabel17.Text = "ITF %"
        '
        'txtITF2
        '
        Me.txtITF2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtITF2.BackColor = System.Drawing.Color.Ivory
        Me.txtITF2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtITF2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtITF2.Enabled = False
        Me.txtITF2.ForeColor = System.Drawing.Color.Black
        Me.txtITF2.KeyEnter = True
        Me.txtITF2.Location = New System.Drawing.Point(152, 19)
        Me.txtITF2.Name = "txtITF2"
        Me.txtITF2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtITF2.ShortcutsEnabled = False
        Me.txtITF2.Size = New System.Drawing.Size(53, 21)
        Me.txtITF2.TabIndex = 2
        Me.txtITF2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtITF2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtImporITF
        '
        Me.txtImporITF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporITF.Location = New System.Drawing.Point(395, 19)
        Me.txtImporITF.Name = "txtImporITF"
        Me.txtImporITF.Size = New System.Drawing.Size(75, 21)
        Me.txtImporITF.TabIndex = 5
        Me.txtImporITF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSumaChe
        '
        Me.txtSumaChe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSumaChe.Enabled = False
        Me.txtSumaChe.Location = New System.Drawing.Point(302, 19)
        Me.txtSumaChe.Name = "txtSumaChe"
        Me.txtSumaChe.Size = New System.Drawing.Size(87, 21)
        Me.txtSumaChe.TabIndex = 4
        Me.txtSumaChe.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGlosaITFCant
        '
        Me.txtGlosaITFCant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGlosaITFCant.Location = New System.Drawing.Point(6, 45)
        Me.txtGlosaITFCant.Multiline = True
        Me.txtGlosaITFCant.Name = "txtGlosaITFCant"
        Me.txtGlosaITFCant.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtGlosaITFCant.Size = New System.Drawing.Size(383, 49)
        Me.txtGlosaITFCant.TabIndex = 6
        '
        'btnCancelarITFCant
        '
        Me.btnCancelarITFCant.Location = New System.Drawing.Point(395, 71)
        Me.btnCancelarITFCant.Name = "btnCancelarITFCant"
        Me.btnCancelarITFCant.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelarITFCant.TabIndex = 8
        Me.btnCancelarITFCant.Text = "Cancelar"
        Me.btnCancelarITFCant.UseVisualStyleBackColor = True
        '
        'dtpITFCant
        '
        Me.dtpITFCant.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpITFCant.CustomFormat = ""
        Me.dtpITFCant.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpITFCant.KeyEnter = True
        Me.dtpITFCant.Location = New System.Drawing.Point(6, 19)
        Me.dtpITFCant.Name = "dtpITFCant"
        Me.dtpITFCant.Size = New System.Drawing.Size(96, 21)
        Me.dtpITFCant.TabIndex = 0
        Me.dtpITFCant.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpITFCant.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'btnGrabarITFCant
        '
        Me.btnGrabarITFCant.Location = New System.Drawing.Point(395, 45)
        Me.btnGrabarITFCant.Name = "btnGrabarITFCant"
        Me.btnGrabarITFCant.Size = New System.Drawing.Size(75, 23)
        Me.btnGrabarITFCant.TabIndex = 7
        Me.btnGrabarITFCant.Text = "Grabar"
        Me.btnGrabarITFCant.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.BeLabel13)
        Me.GroupBox2.Controls.Add(Me.txtCobradosSi)
        Me.GroupBox2.Controls.Add(Me.txtCobradosNo)
        Me.GroupBox2.Controls.Add(Me.BeLabel14)
        Me.GroupBox2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(1055, 277)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(164, 119)
        Me.GroupBox2.TabIndex = 21
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Total Cheques"
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(6, 25)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel13.TabIndex = 0
        Me.BeLabel13.Text = "Cobrados"
        '
        'txtCobradosSi
        '
        Me.txtCobradosSi.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCobradosSi.BackColor = System.Drawing.Color.Silver
        Me.txtCobradosSi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCobradosSi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCobradosSi.ForeColor = System.Drawing.Color.Black
        Me.txtCobradosSi.KeyEnter = True
        Me.txtCobradosSi.Location = New System.Drawing.Point(41, 41)
        Me.txtCobradosSi.Name = "txtCobradosSi"
        Me.txtCobradosSi.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCobradosSi.ReadOnly = True
        Me.txtCobradosSi.ShortcutsEnabled = False
        Me.txtCobradosSi.Size = New System.Drawing.Size(115, 21)
        Me.txtCobradosSi.TabIndex = 2
        Me.txtCobradosSi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCobradosSi.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCobradosNo
        '
        Me.txtCobradosNo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCobradosNo.BackColor = System.Drawing.Color.Aqua
        Me.txtCobradosNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCobradosNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCobradosNo.ForeColor = System.Drawing.Color.Black
        Me.txtCobradosNo.KeyEnter = True
        Me.txtCobradosNo.Location = New System.Drawing.Point(41, 92)
        Me.txtCobradosNo.Name = "txtCobradosNo"
        Me.txtCobradosNo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCobradosNo.ReadOnly = True
        Me.txtCobradosNo.ShortcutsEnabled = False
        Me.txtCobradosNo.Size = New System.Drawing.Size(115, 21)
        Me.txtCobradosNo.TabIndex = 5
        Me.txtCobradosNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCobradosNo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(6, 76)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(81, 13)
        Me.BeLabel14.TabIndex = 3
        Me.BeLabel14.Text = "No Cobrados"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.GroupBox1)
        Me.Panel3.Location = New System.Drawing.Point(316, 242)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(489, 110)
        Me.Panel3.TabIndex = 18
        Me.Panel3.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtITF1)
        Me.GroupBox1.Controls.Add(Me.BeLabel16)
        Me.GroupBox1.Controls.Add(Me.txtImporteItf)
        Me.GroupBox1.Controls.Add(Me.txtGlosaItf)
        Me.GroupBox1.Controls.Add(Me.btnCancelarItf)
        Me.GroupBox1.Controls.Add(Me.dtpFechaItf)
        Me.GroupBox1.Controls.Add(Me.btnGrabarItf)
        Me.GroupBox1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(3, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(481, 102)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Registro de ITF"
        '
        'txtITF1
        '
        Me.txtITF1.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtITF1.BackColor = System.Drawing.Color.Ivory
        Me.txtITF1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtITF1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtITF1.Enabled = False
        Me.txtITF1.ForeColor = System.Drawing.Color.Black
        Me.txtITF1.KeyEnter = True
        Me.txtITF1.Location = New System.Drawing.Point(159, 19)
        Me.txtITF1.Name = "txtITF1"
        Me.txtITF1.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtITF1.ShortcutsEnabled = False
        Me.txtITF1.Size = New System.Drawing.Size(53, 21)
        Me.txtITF1.TabIndex = 2
        Me.txtITF1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtITF1.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(112, 23)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(41, 13)
        Me.BeLabel16.TabIndex = 1
        Me.BeLabel16.Text = "ITF %"
        '
        'txtImporteItf
        '
        Me.txtImporteItf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporteItf.Location = New System.Drawing.Point(395, 45)
        Me.txtImporteItf.Name = "txtImporteItf"
        Me.txtImporteItf.Size = New System.Drawing.Size(75, 21)
        Me.txtImporteItf.TabIndex = 5
        Me.txtImporteItf.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtGlosaItf
        '
        Me.txtGlosaItf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGlosaItf.Location = New System.Drawing.Point(14, 45)
        Me.txtGlosaItf.Name = "txtGlosaItf"
        Me.txtGlosaItf.Size = New System.Drawing.Size(375, 21)
        Me.txtGlosaItf.TabIndex = 3
        '
        'btnCancelarItf
        '
        Me.btnCancelarItf.Location = New System.Drawing.Point(395, 71)
        Me.btnCancelarItf.Name = "btnCancelarItf"
        Me.btnCancelarItf.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelarItf.TabIndex = 6
        Me.btnCancelarItf.Text = "Cancelar"
        Me.btnCancelarItf.UseVisualStyleBackColor = True
        '
        'dtpFechaItf
        '
        Me.dtpFechaItf.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaItf.CustomFormat = ""
        Me.dtpFechaItf.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaItf.KeyEnter = True
        Me.dtpFechaItf.Location = New System.Drawing.Point(14, 19)
        Me.dtpFechaItf.Name = "dtpFechaItf"
        Me.dtpFechaItf.Size = New System.Drawing.Size(92, 21)
        Me.dtpFechaItf.TabIndex = 0
        Me.dtpFechaItf.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaItf.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'btnGrabarItf
        '
        Me.btnGrabarItf.Location = New System.Drawing.Point(314, 71)
        Me.btnGrabarItf.Name = "btnGrabarItf"
        Me.btnGrabarItf.Size = New System.Drawing.Size(75, 23)
        Me.btnGrabarItf.TabIndex = 4
        Me.btnGrabarItf.Text = "Grabar"
        Me.btnGrabarItf.UseVisualStyleBackColor = True
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold)
        Me.BeLabel11.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel11.Location = New System.Drawing.Point(769, 474)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(156, 16)
        Me.BeLabel11.TabIndex = 22
        Me.BeLabel11.Text = "Saldo Final Contable"
        '
        'dgvLibroDet
        '
        Me.dgvLibroDet.AllowUserToAddRows = False
        Me.dgvLibroDet.BackgroundColor = System.Drawing.Color.White
        Me.dgvLibroDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLibroDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column3, Me.Column5, Me.Column4, Me.Column6, Me.Column10, Me.Column8, Me.Column9, Me.Column2, Me.Column7, Me.Column11, Me.Column12, Me.IdCab, Me.Marca1, Me.NroDocumento2, Me.ITF, Me.Column13, Me.Column14, Me.Column15, Me.Column16, Me.Column17, Me.Column18, Me.Column19, Me.Column20, Me.Column21, Me.Column22, Me.Identificarx, Me.Conciliado, Me.ConciliadoS, Me.FechaPago})
        Me.dgvLibroDet.EnableHeadersVisualStyles = False
        Me.dgvLibroDet.Location = New System.Drawing.Point(17, 121)
        Me.dgvLibroDet.Name = "dgvLibroDet"
        Me.dgvLibroDet.RowHeadersVisible = False
        Me.dgvLibroDet.Size = New System.Drawing.Size(1037, 349)
        Me.dgvLibroDet.TabIndex = 17
        '
        'txtSaldo
        '
        Me.txtSaldo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldo.BackColor = System.Drawing.Color.Lavender
        Me.txtSaldo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtSaldo.ForeColor = System.Drawing.Color.Navy
        Me.txtSaldo.KeyEnter = True
        Me.txtSaldo.Location = New System.Drawing.Point(929, 471)
        Me.txtSaldo.Name = "txtSaldo"
        Me.txtSaldo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldo.ReadOnly = True
        Me.txtSaldo.ShortcutsEnabled = False
        Me.txtSaldo.Size = New System.Drawing.Size(125, 20)
        Me.txtSaldo.TabIndex = 23
        Me.txtSaldo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtTipoCambio
        '
        Me.txtTipoCambio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtTipoCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.txtTipoCambio.KeyEnter = True
        Me.txtTipoCambio.Location = New System.Drawing.Point(1285, 250)
        Me.txtTipoCambio.Name = "txtTipoCambio"
        Me.txtTipoCambio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTipoCambio.ShortcutsEnabled = False
        Me.txtTipoCambio.Size = New System.Drawing.Size(86, 20)
        Me.txtTipoCambio.TabIndex = 14
        Me.txtTipoCambio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtTipoCambio.Visible = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(1282, 234)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel1.TabIndex = 13
        Me.BeLabel1.Text = "T. Cambio"
        Me.BeLabel1.Visible = False
        '
        'txtCodDet
        '
        Me.txtCodDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodDet.BackColor = System.Drawing.Color.Ivory
        Me.txtCodDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodDet.ForeColor = System.Drawing.Color.Black
        Me.txtCodDet.KeyEnter = True
        Me.txtCodDet.Location = New System.Drawing.Point(1285, 347)
        Me.txtCodDet.Name = "txtCodDet"
        Me.txtCodDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodDet.ShortcutsEnabled = False
        Me.txtCodDet.Size = New System.Drawing.Size(121, 20)
        Me.txtCodDet.TabIndex = 15
        Me.txtCodDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCodDet.Visible = False
        '
        'txtMarca2
        '
        Me.txtMarca2.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMarca2.BackColor = System.Drawing.Color.Ivory
        Me.txtMarca2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca2.ForeColor = System.Drawing.Color.Black
        Me.txtMarca2.KeyEnter = True
        Me.txtMarca2.Location = New System.Drawing.Point(1285, 401)
        Me.txtMarca2.Name = "txtMarca2"
        Me.txtMarca2.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMarca2.ShortcutsEnabled = False
        Me.txtMarca2.Size = New System.Drawing.Size(46, 20)
        Me.txtMarca2.TabIndex = 17
        Me.txtMarca2.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtMarca2.Visible = False
        '
        'txtMarca
        '
        Me.txtMarca.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMarca.BackColor = System.Drawing.Color.Ivory
        Me.txtMarca.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMarca.ForeColor = System.Drawing.Color.Black
        Me.txtMarca.KeyEnter = True
        Me.txtMarca.Location = New System.Drawing.Point(1285, 375)
        Me.txtMarca.Name = "txtMarca"
        Me.txtMarca.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMarca.ShortcutsEnabled = False
        Me.txtMarca.Size = New System.Drawing.Size(46, 20)
        Me.txtMarca.TabIndex = 16
        Me.txtMarca.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtMarca.Visible = False
        '
        'Column1
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.Format = "M"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column1.FillWeight = 95.0!
        Me.Column1.HeaderText = "Día"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column1.Width = 30
        '
        'Column3
        '
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column3.FillWeight = 95.0!
        Me.Column3.HeaderText = "Cheq. / Not."
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Column5
        '
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column5.FillWeight = 95.0!
        Me.Column5.HeaderText = "Concepto"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column5.Width = 290
        '
        'Column4
        '
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Arial", 8.25!)
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column4.FillWeight = 95.0!
        Me.Column4.HeaderText = "Descripción"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column4.Width = 250
        '
        'Column6
        '
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column6.FillWeight = 95.0!
        Me.Column6.HeaderText = "Abono"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column6.Width = 80
        '
        'Column10
        '
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle6
        Me.Column10.FillWeight = 95.0!
        Me.Column10.HeaderText = "Cargo"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column10.Width = 80
        '
        'Column8
        '
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Arial", 8.25!)
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle7
        Me.Column8.FillWeight = 95.0!
        Me.Column8.HeaderText = "Saldo"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Column8.Width = 75
        '
        'Column9
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.NullValue = False
        Me.Column9.DefaultCellStyle = DataGridViewCellStyle8
        Me.Column9.FillWeight = 95.0!
        Me.Column9.HeaderText = "ITF"
        Me.Column9.Name = "Column9"
        Me.Column9.Width = 40
        '
        'Column2
        '
        Me.Column2.FillWeight = 95.0!
        Me.Column2.HeaderText = "IdDet"
        Me.Column2.Name = "Column2"
        Me.Column2.Visible = False
        '
        'Column7
        '
        Me.Column7.FillWeight = 95.0!
        Me.Column7.HeaderText = "TipMov"
        Me.Column7.Name = "Column7"
        Me.Column7.Visible = False
        '
        'Column11
        '
        Me.Column11.FillWeight = 95.0!
        Me.Column11.HeaderText = "TipOpe"
        Me.Column11.Name = "Column11"
        Me.Column11.Visible = False
        '
        'Column12
        '
        Me.Column12.FillWeight = 95.0!
        Me.Column12.HeaderText = "Importe"
        Me.Column12.Name = "Column12"
        Me.Column12.Visible = False
        '
        'IdCab
        '
        Me.IdCab.DataPropertyName = "IdLibroCab"
        Me.IdCab.FillWeight = 95.0!
        Me.IdCab.HeaderText = "IdCab"
        Me.IdCab.Name = "IdCab"
        Me.IdCab.Visible = False
        '
        'Marca1
        '
        Me.Marca1.DataPropertyName = "Marca"
        Me.Marca1.FillWeight = 95.0!
        Me.Marca1.HeaderText = "Marca1"
        Me.Marca1.Name = "Marca1"
        Me.Marca1.Visible = False
        '
        'NroDocumento2
        '
        Me.NroDocumento2.DataPropertyName = "NroDocumento2"
        Me.NroDocumento2.FillWeight = 95.0!
        Me.NroDocumento2.HeaderText = "NroDocumento2"
        Me.NroDocumento2.Name = "NroDocumento2"
        Me.NroDocumento2.Visible = False
        '
        'ITF
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.NullValue = False
        Me.ITF.DefaultCellStyle = DataGridViewCellStyle9
        Me.ITF.FillWeight = 95.0!
        Me.ITF.HeaderText = "Varios ITF"
        Me.ITF.Name = "ITF"
        Me.ITF.Visible = False
        Me.ITF.Width = 70
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "NroMovimientoAnx"
        Me.Column13.FillWeight = 95.0!
        Me.Column13.HeaderText = "NroMovimientoAnx"
        Me.Column13.Name = "Column13"
        Me.Column13.Visible = False
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "FechaAperturaAnx"
        Me.Column14.FillWeight = 95.0!
        Me.Column14.HeaderText = "FechaAperturaAnx"
        Me.Column14.Name = "Column14"
        Me.Column14.Visible = False
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "FechaVencimientoAnx"
        Me.Column15.FillWeight = 95.0!
        Me.Column15.HeaderText = "FechaVencimientoAnx"
        Me.Column15.Name = "Column15"
        Me.Column15.Visible = False
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "MontoAnx"
        Me.Column16.FillWeight = 95.0!
        Me.Column16.HeaderText = "MontoAnx"
        Me.Column16.Name = "Column16"
        Me.Column16.Visible = False
        '
        'Column17
        '
        Me.Column17.DataPropertyName = "CodMonedaAnx"
        Me.Column17.FillWeight = 95.0!
        Me.Column17.HeaderText = "CodMonedaAnx"
        Me.Column17.Name = "Column17"
        Me.Column17.Visible = False
        '
        'Column18
        '
        Me.Column18.DataPropertyName = "CodCtaCargoAnx"
        Me.Column18.FillWeight = 95.0!
        Me.Column18.HeaderText = "CodCtaCargoAnx"
        Me.Column18.Name = "Column18"
        Me.Column18.Visible = False
        '
        'Column19
        '
        Me.Column19.DataPropertyName = "ClienteAnx"
        Me.Column19.FillWeight = 95.0!
        Me.Column19.HeaderText = "ClienteAnx"
        Me.Column19.Name = "Column19"
        Me.Column19.Visible = False
        '
        'Column20
        '
        Me.Column20.DataPropertyName = "CtaClienteAnx"
        Me.Column20.FillWeight = 95.0!
        Me.Column20.HeaderText = "CtaClienteAnx"
        Me.Column20.Name = "Column20"
        Me.Column20.Visible = False
        '
        'Column21
        '
        Me.Column21.DataPropertyName = "ObraAnx"
        Me.Column21.FillWeight = 95.0!
        Me.Column21.HeaderText = "ObraAnx"
        Me.Column21.Name = "Column21"
        Me.Column21.Visible = False
        '
        'Column22
        '
        Me.Column22.DataPropertyName = "TipoCtaAnx"
        Me.Column22.FillWeight = 95.0!
        Me.Column22.HeaderText = "TipoCtaAnx"
        Me.Column22.Name = "Column22"
        Me.Column22.Visible = False
        '
        'Identificarx
        '
        Me.Identificarx.HeaderText = "Identificarx"
        Me.Identificarx.Name = "Identificarx"
        Me.Identificarx.Visible = False
        '
        'Conciliado
        '
        Me.Conciliado.HeaderText = "Conciliado"
        Me.Conciliado.Name = "Conciliado"
        Me.Conciliado.Width = 65
        '
        'ConciliadoS
        '
        Me.ConciliadoS.HeaderText = "ConciliadoS"
        Me.ConciliadoS.Name = "ConciliadoS"
        Me.ConciliadoS.Visible = False
        '
        'FechaPago
        '
        Me.FechaPago.HeaderText = "FechaPago"
        Me.FechaPago.Name = "FechaPago"
        Me.FechaPago.Visible = False
        '
        'frmLibroBancos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1236, 584)
        Me.Controls.Add(Me.lblCtaEn)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.lblCodMoneda)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txtCodDet)
        Me.Controls.Add(Me.txtMarca2)
        Me.Controls.Add(Me.txtMarca)
        Me.Controls.Add(Me.txtTipoCambio)
        Me.Controls.Add(Me.BeLabel1)
        Me.ForeColor = System.Drawing.Color.Black
        Me.MaximizeBox = False
        Me.Name = "frmLibroBancos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Libro Bancos"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvLibroDet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpFecha As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents cboBancos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboCuentas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroDoc As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtImporte As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTipoCambio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnNuevo As ctrLibreria.Controles.BeButton
    Friend WithEvents btnGrabar As ctrLibreria.Controles.BeButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboAno As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboMes As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboTipoMov As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtMarca As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnConciliacion As ctrLibreria.Controles.BeButton
    Friend WithEvents txtCobradosSi As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCobradosNo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtMarca2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbCargo As System.Windows.Forms.RadioButton
    Friend WithEvents rdbAbono As System.Windows.Forms.RadioButton
    Friend WithEvents dgvLibroDet As System.Windows.Forms.DataGridView
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtGlosaItf As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelarItf As System.Windows.Forms.Button
    Friend WithEvents dtpFechaItf As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnGrabarItf As System.Windows.Forms.Button
    Friend WithEvents txtImporteItf As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCod As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnItfCant As ctrLibreria.Controles.BeButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtGlosaITFCant As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelarITFCant As System.Windows.Forms.Button
    Friend WithEvents dtpITFCant As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents btnGrabarITFCant As System.Windows.Forms.Button
    Friend WithEvents txtImporITF As System.Windows.Forms.TextBox
    Friend WithEvents txtSumaChe As System.Windows.Forms.TextBox
    Friend WithEvents txtITF1 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtITF2 As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscar As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldoInicial As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents btnGenerarITF As System.Windows.Forms.Button
    Friend WithEvents btnConfirmar As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel24 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCliente As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFfin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents txtNroMov As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel26 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtObra As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCtaCliente As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboTipoCargo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel27 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblTipoCta As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblCodMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblCtaEn As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnEliminar As ctrLibreria.Controles.BeButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents chkFecha As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpFechaCobro As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeButton1 As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaPago As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents chkConciliacionMes As System.Windows.Forms.CheckBox
    Friend WithEvents BeLabel31 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldoDisponible As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel30 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldoRetenido As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Marca1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroDocumento2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ITF As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Identificarx As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Conciliado As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ConciliadoS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaPago As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
