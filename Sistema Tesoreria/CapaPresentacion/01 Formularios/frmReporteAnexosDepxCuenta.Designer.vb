<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteAnexosDepxCuenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
    Me.cboCuenta = New ctrLibreria.Controles.BeComboBox
    Me.btnImprimir = New System.Windows.Forms.Button
    Me.cboBancos = New ctrLibreria.Controles.BeComboBox
    Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.BeLabel1)
    Me.Panel1.Controls.Add(Me.cboCuenta)
    Me.Panel1.Controls.Add(Me.btnImprimir)
    Me.Panel1.Controls.Add(Me.cboBancos)
    Me.Panel1.Controls.Add(Me.BeLabel4)
    Me.Panel1.Location = New System.Drawing.Point(2, 2)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(456, 102)
    Me.Panel1.TabIndex = 1
    '
    'BeLabel1
    '
    Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
    Me.BeLabel1.AutoSize = True
    Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
    Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.BeLabel1.ForeColor = System.Drawing.Color.Black
    Me.BeLabel1.Location = New System.Drawing.Point(15, 43)
    Me.BeLabel1.Name = "BeLabel1"
    Me.BeLabel1.Size = New System.Drawing.Size(48, 13)
    Me.BeLabel1.TabIndex = 28
    Me.BeLabel1.Text = "Cuenta"
    '
    'cboCuenta
    '
    Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
    Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
    Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboCuenta.ForeColor = System.Drawing.Color.Black
    Me.cboCuenta.FormattingEnabled = True
    Me.cboCuenta.KeyEnter = True
    Me.cboCuenta.Location = New System.Drawing.Point(89, 40)
    Me.cboCuenta.Name = "cboCuenta"
    Me.cboCuenta.Size = New System.Drawing.Size(358, 21)
    Me.cboCuenta.TabIndex = 27
    '
    'btnImprimir
    '
    Me.btnImprimir.Location = New System.Drawing.Point(372, 69)
    Me.btnImprimir.Name = "btnImprimir"
    Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
    Me.btnImprimir.TabIndex = 25
    Me.btnImprimir.Text = "Imprimir"
    Me.btnImprimir.UseVisualStyleBackColor = True
    '
    'cboBancos
    '
    Me.cboBancos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
    Me.cboBancos.BackColor = System.Drawing.Color.Ivory
    Me.cboBancos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboBancos.ForeColor = System.Drawing.Color.Black
    Me.cboBancos.FormattingEnabled = True
    Me.cboBancos.KeyEnter = True
    Me.cboBancos.Location = New System.Drawing.Point(89, 13)
    Me.cboBancos.Name = "cboBancos"
    Me.cboBancos.Size = New System.Drawing.Size(207, 21)
    Me.cboBancos.TabIndex = 3
    '
    'BeLabel4
    '
    Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
    Me.BeLabel4.AutoSize = True
    Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
    Me.BeLabel4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.BeLabel4.ForeColor = System.Drawing.Color.Black
    Me.BeLabel4.Location = New System.Drawing.Point(15, 21)
    Me.BeLabel4.Name = "BeLabel4"
    Me.BeLabel4.Size = New System.Drawing.Size(42, 13)
    Me.BeLabel4.TabIndex = 2
    Me.BeLabel4.Text = "Banco"
    '
    'frmReporteAnexosDepxCuenta
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.BackColor = System.Drawing.Color.LightSteelBlue
    Me.ClientSize = New System.Drawing.Size(462, 107)
    Me.Controls.Add(Me.Panel1)
    Me.MaximizeBox = False
    Me.Name = "frmReporteAnexosDepxCuenta"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
    Me.Text = "Reporte de Depositos a Plazo por Cuenta"
    Me.Panel1.ResumeLayout(False)
    Me.Panel1.PerformLayout()
    Me.ResumeLayout(False)

  End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents cboBancos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
End Class
