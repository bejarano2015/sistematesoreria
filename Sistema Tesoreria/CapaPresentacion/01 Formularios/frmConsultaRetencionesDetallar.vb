Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmConsultaRetencionesDetallar

    Public FechaMov As DateTime
    Public MonedaMov As String = ""
    Public RucMov As String = ""
    Public IdMov As String = ""
    Public IdRet As String = ""
    Public xxSerie As String = ""
    Public IdCheque As String = ""
    Public xIdCheque As String = ""
    Public NumeroRet As String = ""

    Public MontoRet As Double = 0
    Public MontoPagado As Double = 0

    Dim strGlosaFacturasCalc As String = ""
    Dim sMonCodigo As String = ""


    Dim AplicarPintado As Integer = 1
    Private eRegistroDocumento As clsRegistroDocumento
    Dim strGlosaFacturasSoles As String = ""
    Dim strGlosaFacturasDolares As String = ""
    Dim CountDocumentos As Integer
    Dim CountCantDocSoles As Integer
    Dim CountCantDocDolares As Integer
    Dim dblSoles As Double
    Dim dblDolares As Double
    Dim Mov As String = ""
    Dim TipoMonedas As String = ""
    Dim EmpresaRetencion As String = ""
    Dim strRuc As String = ""
    Dim dIgv As Double
    Dim strIdTipoAnexo As String = ""
    Dim strIdAnexo As String = ""
    Dim MontoRestar As Double = 0
    Dim CadenaOrden As String = ""
    Private m_EditingRow3 As Integer = -1
    Private ePagoProveedores As clsPagoProveedores
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Private eGastosGenerales As clsGastosGenerales

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaRetencionesDetallar = Nothing
    Public Shared Function Instance() As frmConsultaRetencionesDetallar
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaRetencionesDetallar
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmConsultaRetencionesDetallar_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub dgvDocumentos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellContentClick
        If dgvDocumentos.Rows.Count > 0 Then
            Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex
            Try
                dgvDocumentos.CurrentCell = dgvDocumentos(0, dgvDocumentos.CurrentRow.Index)
                'Calcular()
                dgvDocumentos.CurrentCell = dgvDocumentos(columna, dgvDocumentos.CurrentRow.Index)
                If dgvDocumentos.Rows.Count > 0 Then
                    Dim iFila As Integer
                    Calcular()
                    If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 26 Then
                        iFila = dgvDocumentos.CurrentRow.Index
                        dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                        dgvDocumentos.CurrentCell = dgvDocumentos(26, iFila)
                    End If
                    If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 14 Then
                        iFila = dgvDocumentos.CurrentRow.Index
                        dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                        dgvDocumentos.CurrentCell = dgvDocumentos(14, iFila)
                        If dgvDocumentos.Rows.Count > 0 Then
                            Dim iFila2 As Integer
                            iFila2 = dgvDocumentos.CurrentRow.Index
                            If Convert.ToBoolean(dgvDocumentos.Rows(iFila2).Cells("X").Value) = True Then
                                If dgvDocumentos.Rows(iFila2).Cells(13).Value = "" Then
                                    MessageBox.Show("Seleccione un Centro de Costo para el Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    dgvDocumentos.Rows(iFila2).Cells("X").Value = False
                                    dgvDocumentos.CurrentCell = dgvDocumentos(0, iFila2)
                                    dgvDocumentos.CurrentCell = dgvDocumentos(13, iFila2)
                                    Exit Sub
                                End If
                                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
                            ElseIf Convert.ToBoolean(dgvDocumentos.Rows(iFila2).Cells("X").Value) = False Then
                                'dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value + dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
                            End If
                        End If
                    End If
                End If

            Catch ex As Exception
            End Try
            'Calcular()
        End If
        Calcular()
    End Sub

    Private Sub dgvDocumentos_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellDoubleClick
        If dgvDocumentos.Rows.Count > 0 Then
            Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex
            If columna = 12 Then
                Panel7.Visible = True
                txtNroOrden.Clear()
                txtNroOrden_TextChanged(sender, e)
                txtNroOrden.Focus()
                txtNroOrden.Enabled = True
            ElseIf columna = 25 Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
            End If
        End If
    End Sub

    Private Sub dgvDocumentos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellEndEdit
        Dim StrCad As String = String.Empty
        Dim StrImporteDoc As String = String.Empty
        If e.ColumnIndex = 8 Or e.ColumnIndex = 14 Then
            StrCad = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value)
            StrImporteDoc = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value)
            If StrCad = Nothing Or StrImporteDoc = Nothing Or StrCad = "0.00" Then
                Calcular()
                Exit Sub
            End If
            Try
                If Convert.ToDouble(StrCad) > Convert.ToDouble(StrImporteDoc) Then
                    MessageBox.Show("El Monto a Pagar Supera al Importe del Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = False
                    dgvDocumentos.CurrentCell = dgvDocumentos(8, dgvDocumentos.CurrentRow.Index)
                    If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = True Then
                        dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = False
                    End If
                Else
                    Dim resta As Double = 0
                    resta = Convert.ToDouble(StrImporteDoc) - Convert.ToDouble(StrCad)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = resta 'Format(CDec(resta), "0.00")
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = StrCad 'Format(CDec(StrCad), "0.00")
                    dgvDocumentos.CurrentCell = dgvDocumentos(8, dgvDocumentos.CurrentRow.Index)

                    If (resta > 0) And (resta < StrImporteDoc) Then
                        'If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = False Then
                        dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = True
                        'End If
                    ElseIf (resta = 0) Or (resta < 0) Then
                        dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = False
                    End If

                    If resta = StrImporteDoc Then
                        If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = True Then
                            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = False
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try
        ElseIf e.ColumnIndex = 10 Then
            Dim Importe As Double = 0
            Dim Porcentaje As Double = 0
            Dim PorcentajeD As Double = 0
            Dim PorcentajeP As Double = 0
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = "0.00"
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = ""
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "d" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "R" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "r" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeD = (Importe * Porcentaje) / 100
                PorcentajeD = Format(Math.Round(PorcentajeD, 0), "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeD
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe - PorcentajeD
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "P" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "p" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeP = (Importe * Porcentaje) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeP
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe + PorcentajeP
            End If
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value) = False Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value, "#,##0.00")
            End If
        ElseIf e.ColumnIndex = 9 Then
            Dim CadenaCon As String
            If vb.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                CadenaCon = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(0, "0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = Format(0, "0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value
            Else
                CadenaCon = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value
            End If
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = vb.UCase(CadenaCon)
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = "0.00"
                Dim ImporteAux As Double = 0
                Dim PorcentajeAux As Double = 0
                Dim PorcentajeDAux As Double = 0
                Dim PorcentajePAux As Double = 0
                ImporteAux = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                PorcentajeAux = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeDAux = (ImporteAux * PorcentajeAux) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(PorcentajeAux, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeDAux
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = ImporteAux - PorcentajeDAux
            End If
            Dim Importe As Double = 0
            Dim Porcentaje As Double = 0
            Dim PorcentajeD As Double = 0
            Dim PorcentajeP As Double = 0
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = "0.00"
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = ""
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "d" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "R" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "r" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeD = (Importe * Porcentaje) / 100
                PorcentajeD = Format(Math.Round(PorcentajeD, 0), "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeD
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe - PorcentajeD
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "P" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "p" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeP = (Importe * Porcentaje) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeP
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe + PorcentajeP
            End If
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
        ElseIf e.ColumnIndex = 12 Then
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("OC").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("CodigoOC").Value = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IdCC").Value = ""
            End If
        ElseIf e.ColumnIndex = 11 Then
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value
        End If
        Calcular()
    End Sub

    Private Sub gestionaResaltadosDocumentos(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(8).Style.BackColor = c
        visor.Rows(fila).Cells(9).Style.BackColor = c
        visor.Rows(fila).Cells(10).Style.BackColor = c
        visor.Rows(fila).Cells(11).Style.BackColor = c
        visor.Rows(fila).Cells(12).Style.BackColor = c
        visor.Rows(fila).Cells(13).Style.BackColor = c
        visor.Rows(fila).Cells(14).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        visor.Rows(fila).Cells(16).Style.BackColor = c
        visor.Rows(fila).Cells(17).Style.BackColor = c
        visor.Rows(fila).Cells(18).Style.BackColor = c
        visor.Rows(fila).Cells(19).Style.BackColor = c
        visor.Rows(fila).Cells(20).Style.BackColor = c
        visor.Rows(fila).Cells(21).Style.BackColor = c
        visor.Rows(fila).Cells(22).Style.BackColor = c
        visor.Rows(fila).Cells(23).Style.BackColor = c
        visor.Rows(fila).Cells(24).Style.BackColor = c
        visor.Rows(fila).Cells(25).Style.BackColor = c
        visor.Rows(fila).Cells(26).Style.BackColor = c
        visor.Rows(fila).Cells(27).Style.BackColor = c
    End Sub

    Sub Pintar()
        If dgvDocumentos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                If dgvDocumentos.Rows(x).Cells("Column1").Value = "0" Then
                    gestionaResaltadosDocumentos(dgvDocumentos, x, Color.Aqua)
                End If 'Violet
                If dgvDocumentos.Rows(x).Cells("Column1").Value = "1" Then
                    gestionaResaltadosDocumentos(dgvDocumentos, x, Color.Violet)
                End If '
                If Trim(dgvDocumentos.Rows(x).Cells("PPago").Value) = "1" Then
                    dgvDocumentos.Rows(x).Cells("Column6").Value = True
                ElseIf Trim(dgvDocumentos.Rows(x).Cells("PPago").Value) = "0" Then
                    dgvDocumentos.Rows(x).Cells("Column6").Value = False
                End If
            Next
        End If
    End Sub

    Private Sub dgvDocumentos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDocumentos.Click
        If AplicarPintado = 1 Then
            Pintar()
            AplicarPintado = AplicarPintado + 1
        End If
    End Sub

    Private Sub cargarCentrosDeCostoxEmpresa()
        For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
            'dgvDocumentos.Rows(y).Cells("IdCC").Value()
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(y).Cells("Column2")) = False Then
                Dim CodeEmpresa As String = ""
                CodeEmpresa = Trim(dgvDocumentos.Rows(y).Cells("Column2").Value)
                Dim my_strsql As String
                Dim d_objds As New DataSet()
                Dim dgrow As New DataGridViewComboBoxCell
                my_strsql = "SELECT CCosCodigo as 'Codigo',CCosCodigo+' - '+CCosDescripcion as 'Cadena' FROM Comun.CCosto WHERE EmprCodigo='" & CodeEmpresa & "' ORDER BY CCosDescripcion"
                Dim objda As New SqlDataAdapter(my_strsql, conexionParaGastosGene)
                objda.Fill(d_objds, "Codigo")
                objda.Fill(d_objds, "Cadena")
                'Finalmente gracias al datagridViewComboBoxCell almacenamos esta
                'consulta como si fuese en un comboBox Normal
                dgrow.DataSource = d_objds.Tables(0).DefaultView
                dgrow.DisplayMember = "Cadena"
                dgrow.ValueMember = "Codigo"
                Try
                    'Aqui viene lo bueno: Tanto como la celda de la columna Distrito y el 
                    'dgrow  son practicamente comboBoxes... entonces podemos pasarle los
                    'datos del uno al otro.
                    'Me.dgvDocumentos.Item("IdCC", y) = dgrow
                    Me.dgvDocumentos.Item("IdCC", y) = dgrow
                    'Me.dgvDocumentos.Item("IdCC", Me.dgvDocumentos.CurrentCell.RowIndex) = dgrow
                    'MessageBox.Show(dgvDocumentos.Rows(y).Cells("Column5").Value)

                    'For i As Integer = 0 To dtTable.Rows.Count - 1
                    '    'IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosDescripcion") & " - " & dtTable.Rows(i).Item("CCosCodigo")))
                    '    IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosCodigo")) & " - " & dtTable.Rows(i).Item("CCosDescripcion"))
                    'Next

                Catch ex As Exception
                    MessageBox.Show("Aqui un error: " & ex.ToString)
                End Try
                'For a As Integer = 0 To IdCC.Items.Count - 1
                '    Dim StrCadCodigo As String
                '    Dim cad As String = IdCC.Items.Item(a)
                '    StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                '    If Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCosCodigo")) = False Then
                '        If Trim(dtDocumentos.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                '            dgvDocumentos.Rows(y).Cells("IdCC").Value = IdCC.Items.Item(a)
                '        End If
                '    End If
                'Next
            End If
        Next
    End Sub

    Private Sub dgvDocumentos_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvDocumentos.ColumnHeaderMouseClick
        cargarCentrosDeCostoxEmpresa()
        Pintar()
        For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
            If Trim(dgvDocumentos.Rows(y).Cells("Column5").Value) <> "" Then
                dgvDocumentos.Rows(y).Cells("IdCC").Value = dgvDocumentos.Rows(y).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
            End If
        Next
    End Sub

    Private Sub dgvDocumentos_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDocumentos.EditingControlShowing
        'm_EditingRow = dgvDocumentos.CurrentRow.Index
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception

        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex

        If columna = 8 Or columna = 10 Or columna = 11 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

        If columna = 9 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   

            If caracter = "r" Or caracter = "R" Or caracter = "d" Or caracter = "D" Or caracter = "p" Or caracter = "P" Or caracter = ChrW(Keys.Back) Then
                If (caracter = ChrW(Keys.Back)) Or (caracter = "r") And (txt.Text.Contains("r") = False) Or (caracter = "R") And (txt.Text.Contains("R") = False) Or (caracter = "D") And (txt.Text.Contains("D") = False) Or (caracter = "d") And (txt.Text.Contains("d") = False) Or (caracter = "P") And (txt.Text.Contains("P") = False) Or (caracter = "p") And (txt.Text.Contains("p") = False) Then
                    e.Handled = False
                    Exit Sub
                Else
                    e.Handled = True
                    Exit Sub
                End If
            Else
                e.Handled = True
                Exit Sub
            End If
        End If

        If columna = 12 Then
            'CadenaOrden = ""
            Dim caracter As Char = e.KeyChar
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back) Or (caracter = "-")) Then
                e.Handled = False
                eRegistroDocumento = New clsRegistroDocumento
                If Len(caracter) > 0 And caracter <> ChrW(Keys.Back) Then

                ElseIf Len(caracter) = 0 And caracter <> ChrW(Keys.Back) Then

                End If
            Else
                e.Handled = True
            End If

        End If

    End Sub


    Private Sub dgvDocumentos_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDocumentos.MouseUp
        If dgvDocumentos.Rows.Count > 0 Then
            Dim iFila As Integer
            Calcular()
            If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 14 Then
                iFila = dgvDocumentos.CurrentRow.Index
                dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                dgvDocumentos.CurrentCell = dgvDocumentos(14, iFila)

                If dgvDocumentos.Rows.Count > 0 Then
                    Dim iFila2 As Integer
                    iFila2 = dgvDocumentos.CurrentRow.Index
                    If Convert.ToBoolean(dgvDocumentos.Rows(iFila2).Cells("X").Value) = True Then
                        If dgvDocumentos.Rows(iFila2).Cells(13).Value = "" Then
                            MessageBox.Show("Seleccione un Centro de Costo para el Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            dgvDocumentos.Rows(iFila2).Cells("X").Value = False
                            dgvDocumentos.CurrentCell = dgvDocumentos(0, iFila2)
                            dgvDocumentos.CurrentCell = dgvDocumentos(13, iFila2)
                            Exit Sub
                        End If
                    End If
                End If

            End If
        End If
    End Sub

    Sub Calcular()
        strGlosaFacturasSoles = ""
        strGlosaFacturasDolares = ""
        CountDocumentos = 0
        CountCantDocSoles = 0
        CountCantDocDolares = 0
        dblSoles = 0
        dblDolares = 0
        Dim CantidadDetraccionesSoles As Integer = 0

        If dgvDocumentos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                    Mov = "PP"
                    TipoMonedas = Trim(dgvDocumentos.Rows(x).Cells("TipoMoneda").Value)
                    EmpresaRetencion = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                    strRuc = Trim(txtRucProv.Text)
                    strIdTipoAnexo = "P" 'Trim(dgvCronogramas.Rows(dgvCronogramas.CurrentRow.Index).Cells("Column22").Value)
                    strIdAnexo = Trim(txtIdProv.Text)

                    'If Trim(strRuc) <> "" Then
                    '    txtRuc.Text = Trim(strRuc)
                    'End If

                    If Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value) = "01" Then
                        If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "d" Then
                            CantidadDetraccionesSoles = CantidadDetraccionesSoles + 1
                        End If
                        Dim StrCad As String = String.Empty
                        StrCad = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)

                        If Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) <> "NA" Then
                            If StrCad = Nothing Then
                                dblSoles = dblSoles + 0
                            Else
                                dblSoles = dblSoles + dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        ElseIf Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) = "NA" Then
                            If StrCad = Nothing Then
                                dblSoles = dblSoles - 0
                            Else
                                dblSoles = dblSoles - dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        End If
                        CountCantDocSoles = CountCantDocSoles + 1
                        strGlosaFacturasSoles = strGlosaFacturasSoles & " " & Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value) & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & " "
                    ElseIf Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value) = "02" Then
                        Dim StrCad As String = String.Empty
                        StrCad = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)
                        If Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) <> "NA" Then
                            If StrCad = Nothing Then
                                dblDolares = dblDolares + 0
                            Else
                                dblDolares = dblDolares + dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        ElseIf Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) = "NA" Then
                            If StrCad = Nothing Then
                                dblDolares = dblDolares - 0
                            Else
                                dblDolares = dblDolares - dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        End If
                        CountCantDocDolares = CountCantDocDolares + 1
                        strGlosaFacturasDolares = strGlosaFacturasDolares & " " & Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value) & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & " "
                    End If

                    CountDocumentos = CountDocumentos + 1
                Else
                    'dgvDocumentos.Rows(x).Cells("Saldo").Value = 0
                End If
            Next
            strGlosaFacturasSoles = "PAGO DE" & strGlosaFacturasSoles
            strGlosaFacturasDolares = "PAGO DE" & strGlosaFacturasDolares
            If CountCantDocSoles = 0 Then
                strGlosaFacturasSoles = ""
            End If
            If CountCantDocDolares = 0 Then
                strGlosaFacturasDolares = ""
            End If

            If CountDocumentos > 0 And (Trim(EmpresaRetencion) <> Trim(gEmpresa)) Then
                'si la empresa de retencion es diferente a la empresa del sistema
                'tomara como prestamo
                EmpresaRetencion = Trim(EmpresaRetencion)
                'cboEmpresas.SelectedValue = Trim(EmpresaRetencion)
                'rdbUnaEmpresa.Checked = True
                'cboEstado.SelectedValue = "02"
            ElseIf CountDocumentos > 0 And (Trim(EmpresaRetencion) = Trim(gEmpresa)) Then
                EmpresaRetencion = gEmpresa
                'cboEmpresas.SelectedIndex = -1
                'rdbUnaEmpresa.Checked = False
                'cboEstado.SelectedValue = "02"
            ElseIf CountDocumentos = 0 Then
                EmpresaRetencion = gEmpresa
                'cboEmpresas.SelectedIndex = -1
                'rdbUnaEmpresa.Checked = False
                'cboEstado.SelectedValue = "01"
            End If

        End If

        'txtBeneficiario.Text = Trim(txtRazonProv.Text)

        'If CountDocumentos = 0 Then
        '    txtBeneficiario.Clear()
        'End If

        txtNroDoc.Text = CountDocumentos

        If Trim(sMonCodigo) = "01" Then
            strGlosaFacturasCalc = Trim(strGlosaFacturasSoles)
            If CantidadDetraccionesSoles = 0 Then
                txtTotPagarProv.Text = Format(dblSoles, "#,##0.00")
            ElseIf CantidadDetraccionesSoles > 0 Then
                txtTotPagarProv.Text = Format(Math.Round(dblSoles, 0), "#,##0.00")
            End If
        ElseIf Trim(sMonCodigo) = "02" Then
            strGlosaFacturasCalc = Trim(strGlosaFacturasDolares)
            txtTotPagarProv.Text = Format(dblDolares, "#,##0.00")
        End If

        'If TipoMonedas = "02" And sMonCodigo = "01" Then
        '    txtMonto.Text = Format(Convert.ToDouble(txtTotPagarProv.Text) * Val(txtTipoCambio.Text), "#,##0.00")
        'End If

        'If TipoMonedas = "01" And sMonCodigo = "02" Then
        '    txtMonto.Text = Format(Convert.ToDouble(txtTotPagarProv.Text) / Val(txtTipoCambio.Text), "#,##0.00")
        'End If

        If TipoMonedas = "02" And sMonCodigo = "02" Then
            txtTotPagarProv.Text = Format(Convert.ToDouble(txtTotPagarProv.Text), "#,##0.00")
        End If

        If TipoMonedas = "01" And sMonCodigo = "01" Then
            If CantidadDetraccionesSoles = 0 Then
                txtTotPagarProv.Text = Format(Convert.ToDouble(txtTotPagarProv.Text), "#,##0.00")
            ElseIf CantidadDetraccionesSoles > 0 Then
                txtTotPagarProv.Text = Format(Math.Round(Convert.ToDouble(txtTotPagarProv.Text), 0), "#,##0.00")
            End If
        End If

        If Len(Trim(txtTotPagarProv.Text)) > 0 Then
            MontoRestar = Convert.ToDouble(txtTotPagarProv.Text)
        ElseIf Len(Trim(txtTotPagarProv.Text)) = 0 Then
            MontoRestar = 0
        End If


    End Sub

    Private Sub chkVerTodo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVerTodo.CheckedChanged
        txtNroOrden_TextChanged(sender, e)
        txtNroOrden.Focus()
    End Sub

    Private Sub BeLabel44_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel44.Click
        Panel7.Visible = False
        CadenaOrden = ""
    End Sub

    Private Sub txtNroOrden_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroOrden.TextChanged
        Dim CodEmpresa As String = ""
        If dgvDocumentos.Rows.Count > 0 Then
            CodEmpresa = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column2").Value)
        End If
        If Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = False Then
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenes2(CodEmpresa, Trim(txtNroOrden.Text), Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = False Then
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenes(CodEmpresa, Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = True Then
            Panel7.Visible = True
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(43, CodEmpresa, Trim(txtNroOrden.Text), Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = True Then
            Panel7.Visible = True
            Dim dtOrdenes As DataTable
            eRegistroDocumento = New clsRegistroDocumento
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(42, CodEmpresa, Trim(txtNroOrden.Text), Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        End If
    End Sub


    Private Sub dgvOrdenes_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.DoubleClick
        Dim sCodigoActualLinea As String = ""
        Dim iResultadoGrabLinea As Int32 = 0
        If dgvOrdenes.Rows.Count > 0 Then
            Dim CodOrden As String = ""
            Dim NroOrden As String = ""
            Dim CodProvLogistica As String = ""
            Dim xCC As String = ""
            Try
                'Dim sCodProvRUC As String = ""
                'sCodProvRUC = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(6).Value
                'If Len(Trim(sCodProvRUC)) = 11 And Len(Trim(txtCodigo.Text)) = 11 Then
                '    If Trim(sCodProvRUC) <> Trim(txtCodigo.Text) Then
                '        MessageBox.Show("Este N�mero de Orden de Compra no Pertenece al Proveedor " & Trim(txtRazonProv.Text), "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '        txtNroOrden.Focus()
                '        Exit Sub
                '    End If
                'End If

                CodOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(4).Value)
                NroOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(0).Value)
                CodProvLogistica = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(5).Value)
                xCC = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(7).Value)

                If dgvDocumentos.Rows.Count > 0 Then
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("OC").Value = Trim(NroOrden)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("CodigoOC").Value = Trim(CodOrden)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("CosCodigo").Value = Trim(xCC)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IdCC").Value = Trim(xCC)
                    If Len(Trim(NroOrden)) > 0 And Len(Trim(txtIdProv.Text)) > 0 And Len(Trim(CodProvLogistica)) > 0 Then
                        eRegistroDocumento = New clsRegistroDocumento
                        Dim dtBuscarLinea As DataTable
                        dtBuscarLinea = New DataTable
                        dtBuscarLinea = eRegistroDocumento.VerificarLinea(Trim(txtIdProv.Text), gEmpresa)
                        If dtBuscarLinea.Rows.Count = 0 Then
                            eRegistroDocumento.fCodigoLinea(gEmpresa)
                            sCodigoActualLinea = eRegistroDocumento.sCodFuturoLinea
                            iResultadoGrabLinea = eRegistroDocumento.fGrabarLinea(39, sCodigoActualLinea, Trim(CodProvLogistica), gEmpresa, 0, 0, 0, Trim(txtIdProv.Text))
                            If iResultadoGrabLinea = 1 Then
                                'txtLineaCreSoles.Enabled = True
                                'txtLineaCreDolares.Enabled = True
                                'txtDiasCred.Enabled = True
                                'txtIdLinea.Enabled = True
                                'txtIdLinea.Text = sCodigoActualLinea
                            End If
                        End If
                    End If

                End If
                'Panel4.Visible = False
                CadenaOrden = ""
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub dgvOrdenes_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvOrdenes.EditingControlShowing
        m_EditingRow3 = dgvOrdenes.CurrentRow.Index
    End Sub

    Private Sub dgvOrdenes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvOrdenes.KeyDown
        If dgvOrdenes.Rows.Count > 0 Then
            If e.KeyCode = Keys.Return Then
                Dim cur_cell As DataGridViewCell = dgvOrdenes.CurrentCell
                Dim col As Integer = cur_cell.ColumnIndex
                col = (col + 1) Mod dgvOrdenes.Columns.Count
                cur_cell = dgvOrdenes.CurrentRow.Cells(col)

                If cur_cell.Visible = False Then
                    Exit Sub
                End If
                dgvOrdenes.CurrentCell = cur_cell
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvOrdenes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvOrdenes.KeyPress
        Dim CodOrden As String = ""
        Dim NroOrden As String = ""
        Select Case Asc(e.KeyChar)
            Case 13
                dgvOrdenes_DoubleClick(sender, e)
        End Select
    End Sub

    Private Sub dgvOrdenes_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.SelectionChanged
        If m_EditingRow3 >= 0 Then
            Dim new_row As Integer = m_EditingRow3
            m_EditingRow3 = -1
            dgvOrdenes.CurrentCell = dgvOrdenes.Rows(new_row).Cells(dgvOrdenes.CurrentCell.ColumnIndex)
        End If
    End Sub

    Private Sub txtRucProv_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRucProv.KeyPress
        'Select Case Asc(e.KeyChar)
        '    Case 13
        '        chkDocTodas_CheckedChanged(sender, e)
        'End Select
    End Sub

    Private Sub txtRucProv_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRucProv.TextChanged
        'If Len(Trim(txtRucProv.Text)) = 11 Then
        '    chkDocTodas_CheckedChanged(sender, e)
        'End If
        If Len(Trim(txtRucProv.Text)) = 11 Then
            chkDocTodas_CheckedChanged(sender, e)
        End If
    End Sub

    Private Sub cboOtrasEmpresas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOtrasEmpresas.SelectionChangeCommitted
        If Len(Trim(txtRucProv.Text)) = 11 Then
            chkDocTodas_CheckedChanged(sender, e)
        End If
    End Sub

    Private Sub chkDocTodas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDocTodas.CheckedChanged
        Dim dSaldoProv As Double = 0
        'txtConcepto.Clear()
        txtTotPagarProv.Clear()
        'chkDocTodas.Checked
        If Len(Trim(txtRucProv.Text)) = 11 Then
            AplicarPintado = 1
            Dim dtTable As DataTable
            Dim IdProveedor As String = ""
            Dim RucProveedor As String = ""
            Dim RazonProveedor As String = ""
            Dim DireccionProveedor As String = ""
            dtTable = New DataTable
            ePagoProveedores = New clsPagoProveedores
            dtTable = ePagoProveedores.fTraerDatosProveedor2(Trim(txtRucProv.Text), "P", gEmpresa)
            If dtTable.Rows.Count > 0 Then
                IdProveedor = dtTable.Rows(0).Item("AnaliticoCodigo")
                DireccionProveedor = dtTable.Rows(0).Item("Direccion")
                RucProveedor = dtTable.Rows(0).Item("RUC")
                RazonProveedor = dtTable.Rows(0).Item("AnaliticoDescripcion")
                txtIdProv.Text = Trim(IdProveedor)
                txtRucProv.Text = Trim(RucProveedor)
                txtDireccionProv.Text = Trim(DireccionProveedor)
                txtRazonProv.Text = Trim(RazonProveedor)
                Dim dtTableLineCre As DataTable
                dtTableLineCre = New DataTable
                dtTableLineCre = ePagoProveedores.fTraerLineaDeCreditoProv(gEmpresa, Trim(IdProveedor))
                If dtTableLineCre.Rows.Count > 0 Then
                    If sMonCodigo = "01" Then
                        GroupBox11.Text = "Linea de Cr�dito Soles"
                        txtLineaCre.Text = Format(dtTableLineCre.Rows(0).Item("SaldoSolesLinea"), "#,##0.00")
                    ElseIf sMonCodigo = "02" Then
                        GroupBox11.Text = "Linea de Cr�dito Dolares"
                        txtLineaCre.Text = Format(dtTableLineCre.Rows(0).Item("SaldoDolaresLinea"), "#,##0.00")
                    End If
                    txtDiasCred.Text = Trim(dtTableLineCre.Rows(0).Item("DiasCredito"))
                    txtIdLinea.Text = Trim(dtTableLineCre.Rows(0).Item("IdLinea"))
                    txtLineaCre.Enabled = True
                    txtDiasCred.Enabled = True
                    txtIdLinea.Enabled = True
                Else
                    GroupBox11.Text = "Linea de Cr�dito"
                    txtLineaCre.Text = "0.00"
                    txtDiasCred.Clear()
                    txtIdLinea.Clear()
                    txtLineaCre.Enabled = False
                    txtDiasCred.Enabled = False
                    txtIdLinea.Enabled = False
                End If
                Dim dtDocumentos As DataTable
                dtDocumentos = New DataTable
                ePagoProveedores = New clsPagoProveedores

                If chkDocTodas.Checked = True Then
                    '''''''''''''''nuevo
                    If cboOtrasEmpresas.SelectedIndex = -1 Then

                        ePagoProveedores = New clsPagoProveedores
                        cboOtrasEmpresas.DataSource = ePagoProveedores.fTraerDocumentosPorMoneda(33, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                        If ePagoProveedores.iNroRegistros > 0 Then
                            cboOtrasEmpresas.ValueMember = "EmprCodigo"
                            cboOtrasEmpresas.DisplayMember = "EmprAbreviado"
                            cboOtrasEmpresas.SelectedIndex = 0
                        End If

                    End If


                    If cboOtrasEmpresas.SelectedIndex > -1 Then
                        ePagoProveedores = New clsPagoProveedores
                        dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(29, Trim(txtRucProv.Text), "P", Trim(cboOtrasEmpresas.SelectedValue), Trim(sMonCodigo))
                    End If
                    ''''''''''''''fin nuevo
                    'dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(31, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                ElseIf chkDocTodas.Checked = False Then
                    cboOtrasEmpresas.DataSource = Nothing
                    dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(29, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                End If
                dgvDocumentos.AutoGenerateColumns = False
                'dgvDocumentos.DataSource = dtDocumentos
                For x As Integer = 0 To dgvDocumentos.RowCount - 1
                    dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                Next
                If dtDocumentos.Rows.Count > 0 Then
                    For y As Integer = 0 To dtDocumentos.Rows.Count - 1
                        dgvDocumentos.Rows.Add()
                        dgvDocumentos.Rows(y).Cells("Column4").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("EmprAbreviado").ToString) = True, "", dtDocumentos.Rows(y).Item("EmprAbreviado").ToString)
                        dgvDocumentos.Rows(y).Cells("Doc").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("AbrDoc").ToString) = True, "", dtDocumentos.Rows(y).Item("AbrDoc"))
                        dgvDocumentos.Rows(y).Cells("NroDocumento").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("NroDocumento").ToString) = True, "", dtDocumentos.Rows(y).Item("NroDocumento").ToString)
                        dgvDocumentos.Rows(y).Cells("Fecha").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("FechaDocumento").ToString) = True, "", Microsoft.VisualBasic.Left(dtDocumentos.Rows(y).Item("FechaDocumento").ToString, 10))
                        dgvDocumentos.Rows(y).Cells("FechaV").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("FechaVencimiento").ToString) = True, "", Microsoft.VisualBasic.Left(dtDocumentos.Rows(y).Item("FechaVencimiento").ToString, 10))
                        dgvDocumentos.Rows(y).Cells("Mo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("MonSimbolo").ToString) = True, "", dtDocumentos.Rows(y).Item("MonSimbolo").ToString)
                        dgvDocumentos.Rows(y).Cells("ImporteTotal").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("ImporteTotal").ToString) = True, "", dtDocumentos.Rows(y).Item("ImporteTotal").ToString)
                        dgvDocumentos.Rows(y).Cells("Saldo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("Saldo").ToString) = True, "", dtDocumentos.Rows(y).Item("Saldo").ToString)
                        dgvDocumentos.Rows(y).Cells("aPagar").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("sPagar").ToString) = True, "", dtDocumentos.Rows(y).Item("sPagar").ToString)
                        dgvDocumentos.Rows(y).Cells("DP").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("sDP").ToString) = True, "", dtDocumentos.Rows(y).Item("sDP").ToString)
                        dgvDocumentos.Rows(y).Cells("Porcentaje").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("MtoDP").ToString) = True, "", dtDocumentos.Rows(y).Item("MtoDP").ToString)
                        dgvDocumentos.Rows(y).Cells("IPor").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("sPorc").ToString) = True, "", dtDocumentos.Rows(y).Item("sPorc").ToString)
                        dgvDocumentos.Rows(y).Cells("OC").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("NroOC").ToString) = True, "", dtDocumentos.Rows(y).Item("NroOC").ToString)


                        dgvDocumentos.Rows(y).Cells("IdRegistro").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("IdRegistro").ToString) = True, "", dtDocumentos.Rows(y).Item("IdRegistro").ToString)
                        dgvDocumentos.Rows(y).Cells("AbrDoc").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("AbrDoc").ToString) = True, "", dtDocumentos.Rows(y).Item("AbrDoc").ToString)
                        dgvDocumentos.Rows(y).Cells("MonDes").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("MonDescripcion").ToString) = True, "", dtDocumentos.Rows(y).Item("MonDescripcion").ToString)
                        dgvDocumentos.Rows(y).Cells("TipoMoneda").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("IdMoneda").ToString) = True, "", dtDocumentos.Rows(y).Item("IdMoneda").ToString)
                        dgvDocumentos.Rows(y).Cells("SaldoOculto").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("SaldoOculto").ToString) = True, "", dtDocumentos.Rows(y).Item("SaldoOculto").ToString)
                        dgvDocumentos.Rows(y).Cells("CodigoOC").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("IdOC").ToString) = True, "", dtDocumentos.Rows(y).Item("IdOC").ToString)
                        dgvDocumentos.Rows(y).Cells("CosCodigo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCosCodigo").ToString) = True, "", dtDocumentos.Rows(y).Item("CCosCodigo").ToString)
                        dgvDocumentos.Rows(y).Cells("Column1").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("Archivado").ToString) = True, "", dtDocumentos.Rows(y).Item("Archivado").ToString)
                        dgvDocumentos.Rows(y).Cells("Column2").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("EmprCodigo").ToString) = True, "", dtDocumentos.Rows(y).Item("EmprCodigo").ToString)
                        dgvDocumentos.Rows(y).Cells("Column5").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCCadena").ToString) = True, "", dtDocumentos.Rows(y).Item("CCCadena").ToString)

                        dgvDocumentos.Rows(y).Cells("PPago").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("PPago").ToString) = True, "", dtDocumentos.Rows(y).Item("PPago").ToString)
                    Next
                End If
                dgvDocumentos.Columns("ImporteTotal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDocumentos.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDocumentos.Columns("aPagar").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDocumentos.Columns("Porcentaje").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                'Dim y As Integer
                If dtDocumentos.Rows.Count > 0 Then
                    lblTotDocs.Text = "Cant. Docs. : " & dtDocumentos.Rows.Count
                    'cargarCentrosDeCosto()
                    cargarCentrosDeCostoxEmpresa()
                    'Exit Sub
                    'Panel1.Visible = True
                    For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
                        If Trim(dgvDocumentos.Rows(y).Cells("Column5").Value) <> "" Then
                            'MessageBox.Show(dgvDocumentos.Rows(y).Cells("IdCC").Value)
                            'MessageBox.Show(dgvDocumentos.Rows(y).Cells("Column5").Value)
                            dgvDocumentos.Rows(y).Cells("IdCC").Value = dgvDocumentos.Rows(y).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
                            'MessageBox.Show(dgvDocumentos.Rows(y).Cells("IdCC").Value)
                        End If
                    Next

                    'Exit Sub

                    Pintar()

                    For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                        If Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) = Trim(gEmpresa) Then
                            Dim StrCad As String = String.Empty
                            StrCad = Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value)
                            If StrCad = Nothing Then
                                dSaldoProv = dSaldoProv + 0
                            Else
                                dSaldoProv = dSaldoProv + dgvDocumentos.Rows(x).Cells("ImporteTotal").Value
                            End If
                        End If
                    Next

                    Dim DifSoles As Decimal = 0
                    DifSoles = Convert.ToDouble(txtLineaCre.Text) - dSaldoProv
                    txtEstadoCre.Text = Format(DifSoles, "#,##0.00")
                    If DifSoles >= (Convert.ToDouble(txtLineaCre.Text) / 2) Then
                        txtEstadoCre.BackColor = Color.Green
                    ElseIf DifSoles < (Convert.ToDouble(txtLineaCre.Text) / 2) And DifSoles > 0 Then
                        txtEstadoCre.BackColor = Color.Yellow
                    ElseIf DifSoles <= 0 Then
                        txtEstadoCre.BackColor = Color.Red
                    End If
                    If Trim(txtLineaCre.Text) = "0.00" Then
                        txtEstadoCre.BackColor = Color.White
                    End If
                    If txtLineaCre.Enabled = False Then
                        txtEstadoCre.Clear()
                    End If
                    dgvDocumentos.Focus()
                ElseIf dtDocumentos.Rows.Count = 0 Then
                    'MessageBox.Show("No existen documentos en moneda " & txtCodMoneda.Text.Trim & " para pagar a este proveedor.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    lblTotDocs.Text = "Cant. Docs. : 0"
                    If dgvDocumentos.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDocumentos.RowCount - 1
                            dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                        Next
                    End If
                End If
            Else
                Dim dtDocumentos As DataTable
                dtDocumentos = New DataTable
                ePagoProveedores = New clsPagoProveedores
                If chkDocTodas.Checked = True Then
                    dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(31, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                ElseIf chkDocTodas.Checked = False Then
                    dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(29, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                End If
                dgvDocumentos.AutoGenerateColumns = False
                'dgvDocumentos.DataSource = dtDocumentos
                For x As Integer = 0 To dgvDocumentos.RowCount - 1
                    dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                Next
                If dtDocumentos.Rows.Count > 0 Then
                    For y As Integer = 0 To dtDocumentos.Rows.Count - 1
                        dgvDocumentos.Rows.Add()
                        dgvDocumentos.Rows(y).Cells("Column4").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("EmprAbreviado").ToString) = True, "", dtDocumentos.Rows(y).Item("EmprAbreviado").ToString)
                        dgvDocumentos.Rows(y).Cells("Doc").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("AbrDoc").ToString) = True, "", dtDocumentos.Rows(y).Item("AbrDoc"))
                        dgvDocumentos.Rows(y).Cells("NroDocumento").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("NroDocumento").ToString) = True, "", dtDocumentos.Rows(y).Item("NroDocumento").ToString)
                        dgvDocumentos.Rows(y).Cells("Fecha").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("FechaDocumento").ToString) = True, "", Microsoft.VisualBasic.Left(dtDocumentos.Rows(y).Item("FechaDocumento").ToString, 10))
                        dgvDocumentos.Rows(y).Cells("FechaV").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("FechaVencimiento").ToString) = True, "", Microsoft.VisualBasic.Left(dtDocumentos.Rows(y).Item("FechaVencimiento").ToString, 10))
                        dgvDocumentos.Rows(y).Cells("Mo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("MonSimbolo").ToString) = True, "", dtDocumentos.Rows(y).Item("MonSimbolo").ToString)
                        dgvDocumentos.Rows(y).Cells("ImporteTotal").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("ImporteTotal").ToString) = True, "", dtDocumentos.Rows(y).Item("ImporteTotal").ToString)
                        dgvDocumentos.Rows(y).Cells("Saldo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("Saldo").ToString) = True, "", dtDocumentos.Rows(y).Item("Saldo").ToString)
                        dgvDocumentos.Rows(y).Cells("aPagar").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("sPagar").ToString) = True, "", dtDocumentos.Rows(y).Item("sPagar").ToString)
                        dgvDocumentos.Rows(y).Cells("DP").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("sDP").ToString) = True, "", dtDocumentos.Rows(y).Item("sDP").ToString)
                        dgvDocumentos.Rows(y).Cells("Porcentaje").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("MtoDP").ToString) = True, "", dtDocumentos.Rows(y).Item("MtoDP").ToString)
                        dgvDocumentos.Rows(y).Cells("IPor").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("sPorc").ToString) = True, "", dtDocumentos.Rows(y).Item("sPorc").ToString)
                        dgvDocumentos.Rows(y).Cells("OC").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("NroOC").ToString) = True, "", dtDocumentos.Rows(y).Item("NroOC").ToString)


                        dgvDocumentos.Rows(y).Cells("IdRegistro").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("IdRegistro").ToString) = True, "", dtDocumentos.Rows(y).Item("IdRegistro").ToString)
                        dgvDocumentos.Rows(y).Cells("AbrDoc").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("AbrDoc").ToString) = True, "", dtDocumentos.Rows(y).Item("AbrDoc").ToString)
                        dgvDocumentos.Rows(y).Cells("MonDes").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("MonDescripcion").ToString) = True, "", dtDocumentos.Rows(y).Item("MonDescripcion").ToString)
                        dgvDocumentos.Rows(y).Cells("TipoMoneda").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("IdMoneda").ToString) = True, "", dtDocumentos.Rows(y).Item("IdMoneda").ToString)
                        dgvDocumentos.Rows(y).Cells("SaldoOculto").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("SaldoOculto").ToString) = True, "", dtDocumentos.Rows(y).Item("SaldoOculto").ToString)
                        dgvDocumentos.Rows(y).Cells("CodigoOC").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("IdOC").ToString) = True, "", dtDocumentos.Rows(y).Item("IdOC").ToString)
                        dgvDocumentos.Rows(y).Cells("CosCodigo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCosCodigo").ToString) = True, "", dtDocumentos.Rows(y).Item("CCosCodigo").ToString)
                        dgvDocumentos.Rows(y).Cells("Column1").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("Archivado").ToString) = True, "", dtDocumentos.Rows(y).Item("Archivado").ToString)
                        dgvDocumentos.Rows(y).Cells("Column2").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("EmprCodigo").ToString) = True, "", dtDocumentos.Rows(y).Item("EmprCodigo").ToString)
                        dgvDocumentos.Rows(y).Cells("Column5").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCCadena").ToString) = True, "", dtDocumentos.Rows(y).Item("CCCadena").ToString)

                        dgvDocumentos.Rows(y).Cells("PPago").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("PPago").ToString) = True, "", dtDocumentos.Rows(y).Item("PPago").ToString)
                    Next
                End If
                dgvDocumentos.Columns("ImporteTotal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDocumentos.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDocumentos.Columns("aPagar").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDocumentos.Columns("Porcentaje").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                If dtDocumentos.Rows.Count > 0 Then
                    lblTotDocs.Text = "Cant. Docs. : " & dtDocumentos.Rows.Count
                    cargarCentrosDeCostoxEmpresa()
                    'Panel1.Visible = True
                    For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
                        If Trim(dgvDocumentos.Rows(y).Cells("Column5").Value) <> "" Then
                            dgvDocumentos.Rows(y).Cells("IdCC").Value = dgvDocumentos.Rows(y).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
                        End If
                    Next
                    Pintar()
                    For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                        If Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) = Trim(gEmpresa) Then
                            Dim StrCad As String = String.Empty
                            StrCad = Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value)
                            If StrCad = Nothing Then
                                dSaldoProv = dSaldoProv + 0
                            Else
                                dSaldoProv = dSaldoProv + dgvDocumentos.Rows(x).Cells("ImporteTotal").Value
                            End If
                        End If
                    Next
                    Dim DifSoles As Decimal = 0
                    DifSoles = Convert.ToDouble(Val(txtLineaCre.Text)) - dSaldoProv
                    txtEstadoCre.Text = Format(DifSoles, "#,##0.00")
                    If DifSoles >= (Convert.ToDouble(Val(txtLineaCre.Text)) / 2) Then
                        txtEstadoCre.BackColor = Color.Green
                    ElseIf DifSoles < (Convert.ToDouble(Val(txtLineaCre.Text)) / 2) And DifSoles > 0 Then
                        txtEstadoCre.BackColor = Color.Yellow
                    ElseIf DifSoles <= 0 Then
                        txtEstadoCre.BackColor = Color.Red
                    End If
                    If Trim(txtLineaCre.Text) = "0.00" Then
                        txtEstadoCre.BackColor = Color.White
                    End If
                    If txtLineaCre.Enabled = False Then
                        txtEstadoCre.Clear()
                    End If
                    dgvDocumentos.Focus()
                ElseIf dtDocumentos.Rows.Count = 0 Then
                    'MessageBox.Show("No existen documentos en moneda " & txtCodMoneda.Text.Trim & " para pagar a este proveedor", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    lblTotDocs.Text = "Cant. Docs. : 0"
                    If dgvDocumentos.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDocumentos.RowCount - 1
                            dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                        Next
                    End If
                    'txtRuc.Clear()
                    txtRucProv.Clear()
                    txtIdProv.Clear()
                    txtRazonProv.Clear()
                    txtDireccionProv.Clear()
                    txtNroDoc.Clear()
                    txtTotPagarProv.Clear()
                    txtIdLinea.Clear()
                    txtLineaCre.Clear()
                    txtDiasCred.Clear()
                    txtEstadoCre.Clear()
                    'Panel1.Visible = True
                    txtRucProv.Focus()
                End If
            End If
        Else
            'OpcionMostrar = "Boton"
            If chkDocTodas.Checked = True Then
                If Len(txtRucProv.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese un N�mero de Ruc", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtRucProv.Focus()
                    Exit Sub
                End If
                If Len(txtRucProv.Text.Trim) > 0 And Len(txtRucProv.Text.Trim) < 11 Then
                    MessageBox.Show("Ingrese un Ruc V�lido", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtRucProv.Focus()
                    Exit Sub
                End If
            End If

        End If
        Panel7.Visible = False
    End Sub

    Private Sub frmConsultaRetencionesDetallar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim dTipoCambio As Double
        Dim dtTableTipoCambio As DataTable
        dtTableTipoCambio = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, FechaMov)
        If dtTableTipoCambio.Rows.Count > 0 Then
            dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            txtTipoCambio.ForeColor = Color.Blue
        Else
            dTipoCambio = 0.0
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            txtTipoCambio.ForeColor = Color.Red
        End If

        BeLabel1.Text = "Retencion N� : " & Trim(NumeroRet)
        BeLabel2.Text = "Importe Pagado : " & Format(MontoPagado + MontoRet, "#,##0.00")
        sMonCodigo = MonedaMov

        Try
            AxMonthView1.Value = FechaMov
            txtSemanaPago.Text = AxMonthView1.Week()
        Catch ex As Exception
        End Try

        txtRucProv.Text = Trim(RucMov)
        Panel7.Visible = False
        'chkDocTodas_CheckedChanged(sender, e)

    End Sub

    Private Sub txtLineaCre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLineaCre.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtIdLinea.Text)) > 0 Then
                    ePagoProveedores = New clsPagoProveedores
                    Dim ActualizaLineaCre As Integer = 0
                    Dim Dias As Integer = 0
                    Dim Credito As Double = 0
                    If Len(Trim(txtDiasCred.Text)) = 0 Then
                        Dias = 0
                    Else
                        Dias = Trim(txtDiasCred.Text)
                    End If
                    If Len(Trim(txtLineaCre.Text)) = 0 Then
                        Credito = 0
                    Else
                        Credito = Trim(txtLineaCre.Text)
                    End If
                    If sMonCodigo = "01" Then
                        ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito2(29, Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), Credito, 0)
                    ElseIf sMonCodigo = "02" Then
                        ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito2(28, Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), Credito, 0)
                    End If
                End If
        End Select
    End Sub

 

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        If CountDocumentos = 0 Then
            MessageBox.Show("Seleccione Comprobantes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If (MessageBox.Show("�Esta seguro de Grabar los Cambios?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then

            Dim iResultadoEdiEstadoResumen As Integer = 0
            Dim MontoSaldoaPagar As Double = 0
            Dim IdResumen As String = ""
            If Trim(strIdTipoAnexo) = "P" Then
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim CodRegDoc, DetrPer, OrdCom, IdDetCro, CCostoCod, IdOrden, EmpresaCode As String
                Dim iResProgramacion As Integer = 0
                Dim iResultadoGrabaPro As Integer = 0
                Dim Monto As Double
                Dim iResultado2, iResultado3, iResultado4, iEditOrdenCompra As Integer
                Dim NroDoc, FechaDocumento, FechaVencimiento, DP, MontoDP, OrdCompr, XMarca, IdRegistro, AbrDoc As String
                Dim SimboloMo, Importe, MontoPagar, Saldo, MonedaDes, CodEmprz As String
                Dim MntoProgramacioAnt As Double
                Dim CantiDocProgramacioAnt As Integer = 0
                Dim GlosaProgramacioAnt As String = ""
                Dim newGlosaFac As String = ""
                Dim dtTableProgramacion As DataTable
                dtTableProgramacion = New DataTable

                If CountCantDocSoles > 0 Or CountCantDocDolares > 0 Then
                    'si se escogio 1 o mas docs, entonces se buscara una programacion en esa semana con el idProv,NrSemana,Anio y CodEmpr actual.
                    ePagoProveedores = New clsPagoProveedores
                    dtTableProgramacion = ePagoProveedores.fTraerResExisteSoles("P", gEmpresa, Trim(txtIdProv.Text), Trim(txtSemanaPago.Text), gPeriodo, Trim(sMonCodigo))
                    iResProgramacion = dtTableProgramacion.Rows.Count
                    'cboEstado.SelectedValue = "02"
                ElseIf CountCantDocSoles = 0 Or CountCantDocDolares = 0 Then
                    'cboEstado.SelectedValue = "01"
                End If

                If iResProgramacion = 0.5 Then
                    'nunca entrara aqui.
                    IdResumen = Trim(dtTableProgramacion.Rows(0).Item("IdResumen"))
                    MntoProgramacioAnt = Trim(dtTableProgramacion.Rows(0).Item("MontoTotal"))
                    CantiDocProgramacioAnt = Trim(dtTableProgramacion.Rows(0).Item("CantidadDoc"))
                    GlosaProgramacioAnt = Trim(dtTableProgramacion.Rows(0).Item("GlosaNumPago"))
                    If dgvDocumentos.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then

                                CodRegDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                DetrPer = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                Monto = Val(Trim(dgvDocumentos.Rows(x).Cells("Porcentaje").Value)) 'PORCENTAJE
                                CCostoCod = Trim(dgvDocumentos.Rows(x).Cells("IdCC").Value)
                                OrdCom = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                IdOrden = Trim(dgvDocumentos.Rows(x).Cells("CodigoOC").Value) 'IDORDEN
                                EmpresaCode = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) 'IDORDEN
                                If Len(Trim(IdOrden)) = 0 Then
                                    OrdCom = ""
                                Else
                                    iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, EmpresaCode)
                                End If
                                Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                If Trim(EmpresaCode) = Trim(gEmpresa) Then
                                    If Saldo = 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                    ElseIf Saldo > 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                    End If
                                ElseIf Trim(EmpresaCode) <> Trim(gEmpresa) Then
                                    If Saldo = 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 1, 1, Saldo, CCostoCod, IdOrden, "00003", 1, gEmpresa)
                                    ElseIf Saldo > 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 0, 0, Saldo, CCostoCod, IdOrden, "00001", 1, gEmpresa)
                                    End If
                                End If
                                AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("Doc").Value)
                                NroDoc = Trim(dgvDocumentos.Rows(x).Cells("NroDocumento").Value)
                                FechaDocumento = Trim(dgvDocumentos.Rows(x).Cells("Fecha").Value)
                                FechaVencimiento = Trim(dgvDocumentos.Rows(x).Cells("FechaV").Value)
                                SimboloMo = Trim(dgvDocumentos.Rows(x).Cells("Mo").Value)
                                Importe = Val(Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value))
                                Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                MontoPagar = Val(Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value))
                                DP = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                MontoDP = Val(Trim(dgvDocumentos.Rows(x).Cells("IPor").Value))
                                OrdCompr = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                XMarca = 1
                                IdRegistro = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value)
                                MonedaDes = Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value)
                                CodEmprz = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                                If Trim(dgvDocumentos.Rows(x).Cells("TipoMoneda").Value) = "01" Then
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumen, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, CodEmprz)
                                    newGlosaFac = newGlosaFac & " " & AbrDoc & " " & NroDoc & "  "
                                ElseIf Trim(dgvDocumentos.Rows(x).Cells("TipoMoneda").Value) = "02" Then
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumen, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, CodEmprz)
                                    newGlosaFac = newGlosaFac & " " & AbrDoc & " " & NroDoc & "  "
                                End If
                            End If
                        Next
                        If Trim(IdResumen) <> "" Then
                            iResultado4 = ePagoProveedores.ActResumen(5, gEmpresa, IdResumen, Convert.ToDouble(txtTotPagarProv.Text) + MntoProgramacioAnt, 0, CountDocumentos + CantiDocProgramacioAnt, GlosaProgramacioAnt & " " & newGlosaFac)
                        End If
                    End If
                ElseIf iResProgramacion = 0 Or iResProgramacion > 0 Then
                    'si NO hay programacion alguna con el Proveedor, o si ya existe entonces...
                    If dgvDocumentos.Rows.Count > 0 Then ' si hay datos en el gri de documentos...
                        If CountDocumentos > 0 Then
                            'genero el nuevo IdDeCabeceradePagoProv
                            ePagoProveedores.GenerarCodResumen(gEmpresa)
                            IdResumen = ePagoProveedores.sCodFuturo
                            'GRABO CABECERA DE PROGRAMACION DE DOCUMENTOS
                            iResultadoGrabaPro = ePagoProveedores.GrabarResumen(IdResumen, gEmpresa, "P", Trim(sMonCodigo), Trim(txtIdProv.Text), Convert.ToDouble(txtTotPagarProv.Text), Val(txtTipoCambio.Text), Trim(txtSemanaPago.Text), CountDocumentos, strGlosaFacturasSoles, 0, Trim(txtRazonProv.Text), Trim(txtRucProv.Text), gPeriodo)
                            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                                    CodRegDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                    DetrPer = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                    Monto = Val(Trim(dgvDocumentos.Rows(x).Cells("Porcentaje").Value)) 'PORCENTAJE
                                    CCostoCod = Trim(dgvDocumentos.Rows(x).Cells("IdCC").Value)
                                    OrdCom = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                    IdOrden = Trim(dgvDocumentos.Rows(x).Cells("CodigoOC").Value) 'IDORDEN
                                    EmpresaCode = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) 'IDORDEN
                                    If Len(Trim(IdOrden)) = 0 Then
                                        OrdCom = ""
                                    Else
                                        'NroOrden Desactivada
                                        iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, EmpresaCode)
                                    End If
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                    'EDITO COMPROBANTES A ESTADO DE PROVISIONADOS

                                    If Trim(EmpresaCode) = Trim(gEmpresa) Then
                                        'si la emprdoc del documento es igual a la empresa accedida
                                        'actualiza el doc datos... y provisiona pero esta mal q provisione aki,mmm
                                        If Saldo <= 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                        End If

                                    ElseIf Trim(EmpresaCode) <> Trim(gEmpresa) Then
                                        'si la emprdoc del documento NO es igual a la empresa accedida
                                        'actualiza el doc datos... y provisiona pero esta mal q provisione aki,mmm, PERO AQUI ACTIVA COMO PAGOS PRESTADOS
                                        If Saldo <= 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 1, 1, Saldo, CCostoCod, IdOrden, "00003", 1, gEmpresa)
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 0, 0, Saldo, CCostoCod, IdOrden, "00001", 1, gEmpresa)
                                        End If
                                    End If
                                    'GRABO DETALLE DE PROGRAMACION CON LOS DOCS SELECCIONADOS

                                    AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("Doc").Value)
                                    NroDoc = Trim(dgvDocumentos.Rows(x).Cells("NroDocumento").Value)
                                    FechaDocumento = Trim(dgvDocumentos.Rows(x).Cells("Fecha").Value)
                                    FechaVencimiento = Trim(dgvDocumentos.Rows(x).Cells("FechaV").Value)
                                    SimboloMo = Trim(dgvDocumentos.Rows(x).Cells("Mo").Value)
                                    Importe = Val(Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value))
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                    MontoPagar = Val(Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value))
                                    DP = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                    MontoDP = Val(Trim(dgvDocumentos.Rows(x).Cells("IPor").Value))
                                    OrdCompr = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                    XMarca = 1
                                    IdRegistro = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                    'AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value)
                                    MonedaDes = Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value)
                                    CodEmprz = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)

                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumen, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, CodEmprz)
                                End If
                            Next
                        End If
                    End If
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If IdResumen <> "" Then
                    If TipoMonedas = "01" Then
                        'actualizar saldo prove moneda soles ++++++++++
                        Dim iResultado1 As Integer = 0
                        Dim IdSaldoProv As String = ""
                        Dim MontoSaldoSolesProv As Double = 0
                        Dim MontoSaldoSolesNew As Double = 0
                        Dim dtTable4 As DataTable
                        dtTable4 = New DataTable
                        dtTable4 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(strIdTipoAnexo), strIdAnexo, "01")
                        If dtTable4.Rows.Count > 0 Then
                            IdSaldoProv = dtTable4.Rows(0).Item("IdSaldo")
                            MontoSaldoSolesProv = dtTable4.Rows(0).Item("MontoSaldo")
                            MontoSaldoSolesNew = MontoSaldoSolesProv - MontoRestar
                            'MontoSaldoaPagar = MontoSaldoSolesNew
                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, strIdTipoAnexo, strIdAnexo, gEmpresa, MontoSaldoSolesNew, "01")
                        End If
                    ElseIf TipoMonedas = "02" Then
                        'actualizar saldo prove moneda dolares ++++++++++
                        Dim iResultado1 As Integer = 0
                        Dim IdSaldoProv As String = ""
                        Dim MontoSaldoDolaresProv As Double = 0
                        Dim MontoSaldoDolaresNew As Double = 0
                        Dim dtTable5 As DataTable
                        dtTable5 = New DataTable
                        dtTable5 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, strIdTipoAnexo, strIdAnexo, "02")
                        If dtTable5.Rows.Count > 0 Then
                            IdSaldoProv = dtTable5.Rows(0).Item("IdSaldo")
                            MontoSaldoDolaresProv = dtTable5.Rows(0).Item("MontoSaldo")
                            MontoSaldoDolaresNew = MontoSaldoDolaresProv - MontoRestar
                            'MontoSaldoaPagar = MontoSaldoDolaresNew
                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, strIdTipoAnexo, strIdAnexo, gEmpresa, MontoSaldoDolaresNew, "02")
                        End If
                    End If

                    If dgvDocumentos.Rows.Count > 0 Then
                        'AQUI CAMBIAMOS COMO CANCELADO A LAS FACTURAS SELECCIONADAS
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                                Dim IdRegistroDoc As String = ""
                                Dim EmprCod As String = ""

                                Dim SaldoDoc As Double = 0
                                'For x As Integer = 0 To dtTableDetRes.Rows.Count - 1
                                IdRegistroDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                Saldo = Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value)
                                EmprCod = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)

                                Dim PPago As Integer = 0
                                Dim Cancelado As Integer = 0
                                Dim IdSituacion As String = ""
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("Column6").Value) = True Then
                                    PPago = 1
                                    Cancelado = 0
                                    IdSituacion = "00003"
                                ElseIf Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("Column6").Value) = False Then
                                    PPago = 0
                                    Cancelado = 1
                                    IdSituacion = "00004"

                                End If
                                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                eMovimientoCajaBanco.ActSaldoDoc(42, IdRegistroDoc, EmprCod, Saldo, PPago, Cancelado, Trim(IdSituacion))
                                'Next
                            End If
                        Next
                        If Len(Trim(IdResumen)) > 0 Then
                            Dim dtMontoResumen As DataTable
                            Dim SaldoResumen As String
                            dtMontoResumen = New DataTable
                            eGastosGenerales = New clsGastosGenerales
                            dtMontoResumen = eGastosGenerales.fMontoResumen(Trim(IdResumen), gEmpresa)
                            If dtMontoResumen.Rows.Count > 0 Then
                                SaldoResumen = dtMontoResumen.Rows(0).Item("MontoTotal")
                                MontoSaldoaPagar = SaldoResumen - MontoRestar
                            End If
                        End If
                    End If

                    'traer todos los docs del resumen
                    'en un loop  actualizar la tabla saldo con los montos a pagar de los docs traidos..
                    iResultadoEdiEstadoResumen = eMovimientoCajaBanco.fEditarResumen(7, Today(), IdResumen, "", "", "", strIdTipoAnexo, strIdAnexo, "", "", MontoSaldoaPagar, "", "", 0, "", Convert.ToDouble(txtTipoCambio.Text), "", "", Trim(gUsuario), "", 0, 0, "", sMonCodigo, gEmpresa)

                    Dim iResActCab As Integer = 0
                    iResActCab = eMovimientoCajaBanco.ResActCab(47, Trim(IdMov), gEmpresa, RucMov, IdResumen, strGlosaFacturasCalc)

                End If 'If IdResumen <> "" Then
            End If 'If Trim(strIdTipoAnexo) = "P" Then

            If dgvDocumentos.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                    If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                        Dim IdRegistro As String = ""
                        Dim CCosCodigoDoc As String = ""
                        Dim MontoDoc As String = ""
                        Dim Importe As Double = 0
                        Dim MontoPagar As Double = 0
                        Dim MontoActualizarDoc As Double = 0
                        Dim Saldo As Double = 0
                        Dim CodEmpresaX As String = ""
                        IdRegistro = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                        Saldo = Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value)
                        CCosCodigoDoc = Trim(dgvDocumentos.Rows(x).Cells("CosCodigo").Value)
                        MontoDoc = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)
                        CodEmpresaX = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                        Dim MontoRete As Double = 0
                        Dim ImporteTotal As Double = 0
                        If Len(Trim(IdRet)) > 0 Then
                            ImporteTotal = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)
                            If Trim(sMonCodigo) = "01" Then
                                MontoRete = ImporteTotal * (PorcentajeRetencion / 100)
                            ElseIf Trim(sMonCodigo) = "02" Then
                                MontoRete = (ImporteTotal * Convert.ToDouble(txtTipoCambio.Text)) * (PorcentajeRetencion / 100)
                            End If
                        End If
                        eMovimientoCajaBanco = New clsMovimientoCajaBanco
                        If Trim(CodEmpresaX) = Trim(gEmpresa) Then
                            'eMovimientoCajaBanco = New clsMovimientoCajaBanco
                            eMovimientoCajaBanco.fActDetalleCronoIdChek(16, Today(), IdMov, "", Trim(IdRegistro), "", "", strIdAnexo, "", "", MontoRestar, "", "", 0, "", Convert.ToDouble(txtTipoCambio.Text), IdCheque, gEmpresa, Trim(gUsuario), "", 0, 0, IdResumen, sMonCodigo, CodEmpresaX, PorcentajeRetencion, MontoRete, EmpresaRetencion, IdRet, xxSerie)
                        ElseIf Trim(CodEmpresaX) <> Trim(gEmpresa) Then
                            eMovimientoCajaBanco.fActDetalleCronoIdChek(44, Today(), IdMov, "", Trim(IdRegistro), "", "", strIdAnexo, "", "", MontoRestar, "", "", 0, "", Convert.ToDouble(txtTipoCambio.Text), IdCheque, gEmpresa, Trim(gUsuario), "", 0, 0, IdResumen, sMonCodigo, CodEmpresaX, PorcentajeRetencion, MontoRete, EmpresaRetencion, IdRet, xxSerie)
                        End If
                    End If
                Next




                If CountDocumentos > 0 Then
                    Dim CCosCodigoDoc As String = ""
                    Dim MontoDoc As String = ""
                    Dim dtDocs As DataTable
                    dtDocs = New DataTable
                    Dim dt1o2Empr As DataTable
                    dt1o2Empr = New DataTable
                    dt1o2Empr = eMovimientoCajaBanco.fListarDocsMovimiento2(IdResumen, gEmpresa)
                    If dt1o2Empr.Rows.Count = 1 Then
                        dtDocs = eMovimientoCajaBanco.fListarDocsMovimiento(38, IdMov, gEmpresa)
                    ElseIf dt1o2Empr.Rows.Count > 1 Then
                        dtDocs = eMovimientoCajaBanco.fListarDocsMovimiento3(IdResumen, gEmpresa)
                    End If
                    If dtDocs.Rows.Count > 0 Then
                        For x As Integer = 0 To dtDocs.Rows.Count - 1
                            Dim iResultadoGrabaDetGastos As Integer = 0
                            If dt1o2Empr.Rows.Count = 1 Then
                                CCosCodigoDoc = dtDocs.Rows(x).Item("CCosCodigo")
                                MontoDoc = dtDocs.Rows(x).Item("Suma")
                                Dim IdDetMovimientoProveedor As String = ""
                                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                eMovimientoCajaBanco.fCodigoDetalleMov(gEmpresa)
                                IdDetMovimientoProveedor = eMovimientoCajaBanco.sCodFuturoDetMov
                                iResultadoGrabaDetGastos = eMovimientoCajaBanco.fGrabarDetalle(27, FechaMov, IdMov, "", Trim(IdDetMovimientoProveedor), CCosCodigoDoc, Trim(strIdTipoAnexo), strIdAnexo, "", 1, Convert.ToDouble(MontoDoc), "", "", 1, "", Convert.ToDouble(txtTipoCambio.Text), IdCheque, "", "", "", 0, 0, "", sMonCodigo, gEmpresa, "", 0, IdResumen, "", 0, "", "", "", "", "", 0, "")
                            ElseIf dt1o2Empr.Rows.Count > 1 Then
                                Dim EmprDocx As String = ""
                                EmprDocx = dtDocs.Rows(x).Item("EmprDoc")
                                If Trim(gEmpresa) = Trim(EmprDocx) Then
                                    CCosCodigoDoc = dtDocs.Rows(x).Item("CCosCodigo")
                                    'EmpresaPrestada = ""
                                    'CCEmpresaPrestada = ""
                                    'iEstado = 0
                                ElseIf Trim(gEmpresa) <> Trim(EmprDocx) Then
                                    'CCosCodigoDoc = Trim(cboCentroCosto.SelectedValue)
                                    'EmpresaPrestada = dtDocs.Rows(x).Item("EmprDoc")
                                    ' CCEmpresaPrestada = dtDocs.Rows(x).Item("CCosCodigo")
                                    'iEstado = 1
                                End If
                                MontoDoc = dtDocs.Rows(x).Item("Suma")
                                Dim IdDetMovimientoProveedor As String = ""
                                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                eMovimientoCajaBanco.fCodigoDetalleMov(gEmpresa)
                                IdDetMovimientoProveedor = eMovimientoCajaBanco.sCodFuturoDetMov
                                iResultadoGrabaDetGastos = eMovimientoCajaBanco.fGrabarDetalle(27, FechaMov, IdMov, "", Trim(IdDetMovimientoProveedor), CCosCodigoDoc, Trim(strIdTipoAnexo), strIdAnexo, "", 1, Convert.ToDouble(MontoDoc), "", "", 1, "", Convert.ToDouble(txtTipoCambio.Text), IdCheque, "", "", "", 0, 0, "", sMonCodigo, gEmpresa, "", 0, IdResumen, "", 0, "", "", "", "", "", 0, "")
                            End If
                        Next
                        Dim iResActCantDetenCab As Integer = 0
                        iResActCantDetenCab = eMovimientoCajaBanco.fActCantDetCabecera(43, Trim(IdMov), gEmpresa, dtDocs.Rows.Count)
                    End If
                End If
            End If

            MessageBox.Show("Se ha Actualizado el comprobante de Retenci�n", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            Dim _formInterface As frmConsultaRetenciones = CType(Me.Owner, frmConsultaRetenciones)
            _formInterface.hace = 1

            Me.Close()

            'Me.Hide()

        End If


        


    End Sub
End Class