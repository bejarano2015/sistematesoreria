Imports CapaNegocios
Public Class frmConsultaAsegurados_x_Obra
    Private cSegurosPolizas As clsSegurosPolizas
    Dim DTobras As DataTable

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaAsegurados_x_Obra = Nothing
    Public Shared Function Instance() As frmConsultaAsegurados_x_Obra
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaAsegurados_x_Obra
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmConsultaAsegurados_x_Obra_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click

        Me.Close()
    End Sub

    Private Sub frmConsultaAsegurados_x_Obra_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cSegurosPolizas = New clsSegurosPolizas
        DTobras = New DataTable
        DTobras = cSegurosPolizas.fCargar_Obras(gEmpresa)
        cboObra.DataSource = DTobras
        cboObra.DisplayMember = "Descripcion"
        cboObra.ValueMember = "Codigo"
        cboObra.SelectedIndex = -1
        cboObra.Focus()


    End Sub


    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        If cboObra.SelectedIndex <> -1 Then
            dgvAsegurados.DataSource = cSegurosPolizas.fCargar_AseguradosXObra(gEmpresa, cboObra.SelectedValue)
            For xCol As Integer = 0 To dgvAsegurados.Columns.Count - 1
                dgvAsegurados.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
                dgvAsegurados.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            Next
            dgvAsegurados.Columns(0).Visible = False
            dgvAsegurados.Columns(1).Visible = False
        End If
    End Sub

    Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObra.SelectedIndexChanged

    End Sub
End Class