Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports CapaEntidad
Imports CapaEntidad.Proceso
Imports BusinessLogicLayer.Procesos


Public Class frmGastosGenerales2

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Private eBusquedaRetenciones As clsBusquedaRetenciones
    Dim rptF As New rptVoucher
    Dim rptF2 As New rptVoucher2

    Dim WithEvents cmr As CurrencyManager
    Dim AplicarPintado As Integer = 1
    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sMonCodigo As String = ""
    Dim sMonCodigoDest As String = ""
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim IdResumen As String = ""
    Dim TipoMonedas As String = ""
    Dim EmpresaRetencion As String = ""
    Dim MontoRestar As Double = 0
    Dim IdMovimiento As String = ""
    Dim NroVoucher As String = ""
    Private eLibroBancos As clsLibroBancos
    Private eSesionCajas As clsSesionCajas
    Private eGastosGenerales As clsGastosGenerales
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Private eRegistroDocumento As clsRegistroDocumento
    Private ePagoProveedores As clsPagoProveedores
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Private eSeries As clsSeries
    Private eValorizaciones As clsValorizaciones
    Private eITF As clsITF
    Dim FechaParaBuscar As Date = Now.Date

    Dim IdChequeBD As String = ""
    Dim IdChequeraBD As String = ""
    Dim IdTransaccionBD As String = ""
    Dim IdCartaBD As String = ""
    Dim IdBancoDestinoBD As String = ""
    Dim IdCuentaDestinoBD As String = ""
    Dim GlosaDestinoBD As String = ""
    Dim CodigoLibroDestinoBD As String = ""
    Dim CodigoDetDestinoBD As String = ""

    Dim IdResumenBD As String = ""
    Dim strRuc As String = ""
    Dim dIgv As Double
    Dim strIdTipoAnexo As String = ""
    Dim strIdAnexo As String = ""
    Dim Opcion As String = ""
    Dim OpcionMostrar As String = ""
    Dim MostrarDetalles As String = ""
    Dim Anul As String = ""

    Dim Mov As String = ""
    Dim MovBD As String = ""
    Dim NroSesion As String = ""
    Dim IdSesion As String = ""
    Dim IdLetraBD As String = ""

    Dim IdRetencionBD As String = ""
    Dim SerieBD As String = ""
    Dim ConRetencionBD As String = ""
    Dim TotRetenidoBD As Double = 0
    Dim RetEmprCodigoBD As String = ""

    Dim IdArea As String = ""
    Dim IdCaja As String = ""
    Dim sIdCodigoCabLibro As String = ""
    Dim sIdCodigoDetLibro As String = ""

    Dim IdLetra As String = ""
    Dim MontoLetra As Double = 0
    Dim FechaGiroG As DateTime
    Dim FechaVenG As DateTime


    Private xReporte As CapaPreTesoreria.rptVoucher2

    Dim strGlosaVal As String = ""
    Dim strGlosaContrato As String = ""
    Dim strGlosaContratista As String = ""
    Dim CountVal As Integer
    Dim dblMontoVal As Double

    Dim CountDocumentosLetra As Integer = 0

    Private m_EditingRow3 As Integer = -1

    Dim CadenaOrden As String = ""

    Dim strGlosaFacturasSoles As String = ""
    Dim strGlosaFacturasDolares As String = ""
    Dim CountDocumentos As Integer
    Dim CountCantDocSoles As Integer
    Dim CountCantDocDolares As Integer
    Dim dblSoles As Double
    Dim dblDolares As Double

    Dim strGlosaFacturas As String
    Dim dblMontoTotalFacturas As Double
    Dim strNombreAnexo As String
    Dim StrMoneda As String

    Private cSegurosPolizas As clsSegurosPolizas
    Dim DT_ListaCuotas As DataTable
    Dim DV_ListaCuotas As New DataView
    Dim sPoliza As String
    Dim sEndoso As String
    Dim sNroCuota As String
    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    ' VARIABLE GLOBAL REGISTRO PARA TABLA PAGOS
    Dim ID_DocPendiente As String = "0"

    Dim GlobalIdBanco As String = ""

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmGastosGenerales2 = Nothing
    Public Shared Function Instance() As frmGastosGenerales2
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmGastosGenerales2
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmGastosGenerales2_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub

    'Private Sub frmGastosGenerales2_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    '    'cboEstado.DataSource = eTempo.fColEstadoRendido
    '    'cboEstado.ValueMember = "Col02"
    '    'cboEstado.DisplayMember = "Col01"
    '    'cboEstado.SelectedIndex = -1
    'End Sub

    Private Sub frmGastosGenerales2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(1).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(2).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(3).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(4).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(5).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(6).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(7).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(8).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(9).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(10).Style.BackColor = Color.Silver
    End Sub

    Private Sub gestionaResaltados2(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(10).Style.BackColor = Color.Aqua
    End Sub

    Sub Pintar2()
        If dgvListado.Rows.Count > 0 Then
            For x As Integer = 0 To dgvListado.Rows.Count - 1
                If Trim(dgvListado.Rows(x).Cells("Column37").Value) = "1" Or Trim(dgvListado.Rows(x).Cells("Column37").Value) = "2" Then
                    gestionaResaltados(dgvListado, x, Color.Orange)
                End If
                If Trim(dgvListado.Rows(x).Cells("Column40").Value) > "1" Then
                    gestionaResaltados2(dgvListado, x, Color.Orange)
                End If
            Next
        End If
    End Sub

    Private Sub mMostrarGrilla()
        Try


            eGastosGenerales = New clsGastosGenerales
            eTempo = New clsPlantTempo
            dtTable = New DataTable

            Dim dTipoCambio As Double
            Dim dtTableTipoCambio As DataTable
            dtTableTipoCambio = New DataTable
            ePagoProveedores = New clsPagoProveedores
            dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, dtpFechaGasto.Value)
            If dtTableTipoCambio.Rows.Count > 0 Then
                dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
                lblTipoCambio.Text = Format(dTipoCambio, "0.00")
                txtTipoCambio.Text = Format(dTipoCambio, "0.00")
                lblTipoCambio.ForeColor = Color.Blue
                txtTipoCambio.ForeColor = Color.Blue
            Else
                dTipoCambio = 0.0
                lblTipoCambio.Text = Format(dTipoCambio, "0.00")
                txtTipoCambio.Text = Format(dTipoCambio, "0.00")
                lblTipoCambio.ForeColor = Color.Red
                txtTipoCambio.ForeColor = Color.Red
            End If

            If chkFecha.Checked = False Then

            ElseIf chkFecha.Checked = True Then
                dtTable = eGastosGenerales.fListarxFecha(gEmpresa, dtpFechaGasto.Value)
            End If

            If dgvListado.Rows.Count > 0 Then
                For x As Integer = 0 To dgvListado.RowCount - 1
                    MostrarDetalles = "NO"
                    dgvListado.Rows.Remove(dgvListado.CurrentRow)
                Next
            End If

            If dtTable.Rows.Count > 0 Then
                MostrarDetalles = "NO"
                For y As Integer = 0 To dtTable.Rows.Count - 1
                    dgvListado.Rows.Add()
                    'dgvListado.Rows(y).Cells("Column3").Value = dtTable.Rows(y).Item("IdMovimiento").ToString
                    dgvListado.Rows(y).Cells("Column3").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdMovimiento").ToString) = True, "", dtTable.Rows(y).Item("IdMovimiento").ToString) 'Microsoft.VisualBasic.Left(dtDetalle.Rows(y).Item("FechaDetMov"), 10)
                    dgvListado.Rows(y).Cells("Column8").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("FechaMovimiento").ToString) = True, "", dtTable.Rows(y).Item("FechaMovimiento")) 'dtTable.Rows(y).Item("FechaMovimiento").ToString
                    dgvListado.Rows(y).Cells("Column9").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("DescTGasto").ToString) = True, "", dtTable.Rows(y).Item("DescTGasto").ToString) 'dtTable.Rows(y).Item("DescTGasto").ToString
                    dgvListado.Rows(y).Cells("Column11").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("CCosDescripcion").ToString) = True, "", dtTable.Rows(y).Item("CCosDescripcion").ToString) 'dtTable.Rows(y).Item("CCosDescripcion").ToString
                    dgvListado.Rows(y).Cells("Column12").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Estados").ToString) = True, "", dtTable.Rows(y).Item("Estados").ToString) 'dtTable.Rows(y).Item("Estados").ToString
                    dgvListado.Rows(y).Cells("Column13").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("NombreBanco").ToString) = True, "", dtTable.Rows(y).Item("NombreBanco").ToString) 'dtTable.Rows(y).Item("NombreBanco").ToString
                    dgvListado.Rows(y).Cells("Column14").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("NumeroCuenta").ToString) = True, "", dtTable.Rows(y).Item("NumeroCuenta").ToString) 'dtTable.Rows(y).Item("NumeroCuenta").ToString
                    dgvListado.Rows(y).Cells("Column15").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("DescripcionRendicion").ToString) = True, "", dtTable.Rows(y).Item("DescripcionRendicion").ToString) 'dtTable.Rows(y).Item("DescripcionRendicion").ToString
                    dgvListado.Rows(y).Cells("Column16").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("MonDescripcion").ToString) = True, "", dtTable.Rows(y).Item("MonDescripcion").ToString) 'dtTable.Rows(y).Item("MonDescripcion").ToString
                    dgvListado.Rows(y).Cells("Column17").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Monto").ToString) = True, "", dtTable.Rows(y).Item("Monto").ToString) 'dtTable.Rows(y).Item("Monto").ToString
                    dgvListado.Rows(y).Cells("IdBanco").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdBanco").ToString) = True, "", dtTable.Rows(y).Item("IdBanco").ToString) 'dtTable.Rows(y).Item("IdBanco").ToString
                    dgvListado.Rows(y).Cells("NumeroCuenta").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdCuenta").ToString) = True, "", dtTable.Rows(y).Item("IdCuenta").ToString) 'dtTable.Rows(y).Item("IdCuenta").ToString
                    dgvListado.Rows(y).Cells("CCosCodigo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("CCosCodigo").ToString) = True, "", dtTable.Rows(y).Item("CCosCodigo").ToString) 'dtTable.Rows(y).Item("CCosCodigo").ToString
                    dgvListado.Rows(y).Cells("IdTipoAnexo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdTipoAnexo").ToString) = True, "", dtTable.Rows(y).Item("IdTipoAnexo").ToString) 'dtTable.Rows(y).Item("IdTipoAnexo").ToString
                    dgvListado.Rows(y).Cells("IdProveedor").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdProveedor").ToString) = True, "", dtTable.Rows(y).Item("IdProveedor").ToString) 'dtTable.Rows(y).Item("IdProveedor").ToString
                    dgvListado.Rows(y).Cells("IdTipoMovimiento").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdTipoMovimiento").ToString) = True, "", dtTable.Rows(y).Item("IdTipoMovimiento").ToString) 'dtTable.Rows(y).Item("IdTipoMovimiento").ToString
                    dgvListado.Rows(y).Cells("IdFormaPago").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdFormaPago").ToString) = True, "", dtTable.Rows(y).Item("IdFormaPago").ToString) 'dtTable.Rows(y).Item("IdFormaPago").ToString
                    dgvListado.Rows(y).Cells("NroFormaPago").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("NroFormaPago").ToString) = True, "", dtTable.Rows(y).Item("NroFormaPago").ToString) 'dtTable.Rows(y).Item("NroFormaPago").ToString
                    dgvListado.Rows(y).Cells("Anulado").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Anulado").ToString) = True, "", dtTable.Rows(y).Item("Anulado").ToString) 'dtTable.Rows(y).Item("Anulado").ToString
                    dgvListado.Rows(y).Cells("Beneficiario").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Beneficiario").ToString) = True, "", dtTable.Rows(y).Item("Beneficiario").ToString) 'dtTable.Rows(y).Item("Beneficiario").ToString
                    dgvListado.Rows(y).Cells("Glosa").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Glosa").ToString) = True, "", dtTable.Rows(y).Item("Glosa").ToString) 'dtTable.Rows(y).Item("Glosa").ToString
                    dgvListado.Rows(y).Cells("IdTipoGasto").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdTipoGasto").ToString) = True, "", dtTable.Rows(y).Item("IdTipoGasto").ToString) 'dtTable.Rows(y).Item("IdTipoGasto").ToString
                    dgvListado.Rows(y).Cells("NroVaucher").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("NroVaucher").ToString) = True, "", dtTable.Rows(y).Item("NroVaucher").ToString) 'dtTable.Rows(y).Item("NroVaucher").ToString
                    dgvListado.Rows(y).Cells("TipoCambio").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("TipoCambio").ToString) = True, "", dtTable.Rows(y).Item("TipoCambio").ToString) 'dtTable.Rows(y).Item("TipoCambio").ToString
                    dgvListado.Rows(y).Cells("IdRendicion").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdRendicion").ToString) = True, "", dtTable.Rows(y).Item("IdRendicion").ToString) 'dtTable.Rows(y).Item("IdRendicion").ToString
                    dgvListado.Rows(y).Cells("Column18").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdCheque").ToString) = True, "", dtTable.Rows(y).Item("IdCheque").ToString) 'dtTable.Rows(y).Item("IdCheque").ToString
                    dgvListado.Rows(y).Cells("Column19").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdResumen").ToString) = True, "", dtTable.Rows(y).Item("IdResumen").ToString) 'dtTable.Rows(y).Item("IdResumen").ToString
                    dgvListado.Rows(y).Cells("Column24").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Ruc").ToString) = True, "", dtTable.Rows(y).Item("Ruc").ToString) 'dtTable.Rows(y).Item("Ruc").ToString
                    dgvListado.Rows(y).Cells("Column31").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdSesion").ToString) = True, "", dtTable.Rows(y).Item("IdSesion").ToString) 'dtTable.Rows(y).Item("IdSesion").ToString
                    dgvListado.Rows(y).Cells("Column32").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("NumeroSesion").ToString) = True, "", dtTable.Rows(y).Item("NumeroSesion").ToString) 'dtTable.Rows(y).Item("NumeroSesion").ToString
                    dgvListado.Rows(y).Cells("Column33").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("AreaCodigo").ToString) = True, "", dtTable.Rows(y).Item("AreaCodigo").ToString) 'dtTable.Rows(y).Item("AreaCodigo").ToString
                    dgvListado.Rows(y).Cells("Column34").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdCaja").ToString) = True, "", dtTable.Rows(y).Item("IdCaja").ToString) 'dtTable.Rows(y).Item("IdCaja").ToString
                    dgvListado.Rows(y).Cells("Column35").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdCodigoCabLibro").ToString) = True, "", dtTable.Rows(y).Item("IdCodigoCabLibro").ToString) 'dtTable.Rows(y).Item("IdCodigoCabLibro").ToString
                    dgvListado.Rows(y).Cells("Column36").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdCodigoDetLibro").ToString) = True, "", dtTable.Rows(y).Item("IdCodigoDetLibro").ToString) 'dtTable.Rows(y).Item("IdCodigoDetLibro").ToString
                    dgvListado.Rows(y).Cells("Column37").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Prestamo").ToString) = True, "", dtTable.Rows(y).Item("Prestamo").ToString) 'dtTable.Rows(y).Item("Prestamo").ToString
                    dgvListado.Rows(y).Cells("Column38").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("EmprPrestamo").ToString) = True, "", dtTable.Rows(y).Item("EmprPrestamo").ToString) 'dtTable.Rows(y).Item("EmprPrestamo").ToString
                    dgvListado.Rows(y).Cells("Column39").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("CCPrestamo").ToString) = True, "", dtTable.Rows(y).Item("CCPrestamo").ToString) 'dtTable.Rows(y).Item("CCPrestamo").ToString
                    dgvListado.Rows(y).Cells("Column40").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("CountDet").ToString) = True, "", dtTable.Rows(y).Item("CountDet").ToString) 'dtTable.Rows(y).Item("CountDet").ToString
                    dgvListado.Rows(y).Cells("IdChequera").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdChequera").ToString) = True, "", dtTable.Rows(y).Item("IdChequera").ToString) 'dtTable.Rows(y).Item("IdChequera").ToString
                    dgvListado.Rows(y).Cells("IdTransaccion").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdTransaccion").ToString) = True, "", dtTable.Rows(y).Item("IdTransaccion").ToString) 'dtTable.Rows(y).Item("IdTransaccion").ToString
                    dgvListado.Rows(y).Cells("Column41").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdBancoDestino").ToString) = True, "", dtTable.Rows(y).Item("IdBancoDestino").ToString) 'dtTable.Rows(y).Item("IdBancoDestino").ToString
                    dgvListado.Rows(y).Cells("IdCuentaDestino").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdCuentaDestino").ToString) = True, "", dtTable.Rows(y).Item("IdCuentaDestino").ToString) 'dtTable.Rows(y).Item("IdCuentaDestino").ToString
                    dgvListado.Rows(y).Cells("GlosaDestino").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("GlosaDestino").ToString) = True, "", dtTable.Rows(y).Item("GlosaDestino").ToString) 'dtTable.Rows(y).Item("GlosaDestino").ToString
                    dgvListado.Rows(y).Cells("CodigoLibroDestino").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("CodigoLibroDestino").ToString) = True, "", dtTable.Rows(y).Item("CodigoLibroDestino").ToString) 'dtTable.Rows(y).Item("CodigoLibroDestino").ToString
                    dgvListado.Rows(y).Cells("CodigoDetDestino").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("CodigoDetDestino").ToString) = True, "", dtTable.Rows(y).Item("CodigoDetDestino").ToString) 'dtTable.Rows(y).Item("CodigoDetDestino").ToString
                    dgvListado.Rows(y).Cells("IdCarta").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdCarta").ToString) = True, "", dtTable.Rows(y).Item("IdCarta").ToString) 'dtTable.Rows(y).Item("IdCarta").ToString
                    dgvListado.Rows(y).Cells("Column55").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdLetra").ToString) = True, "", dtTable.Rows(y).Item("IdLetra").ToString) 'dtTable.Rows(y).Item("IdLetra").ToString
                    dgvListado.Rows(y).Cells("Column50").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("IdRetencion").ToString) = True, "", dtTable.Rows(y).Item("IdRetencion").ToString) 'dtTable.Rows(y).Item("IdRetencion").ToString
                    dgvListado.Rows(y).Cells("xSerie").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("Serie").ToString) = True, "", dtTable.Rows(y).Item("Serie").ToString) 'dtTable.Rows(y).Item("Serie").ToString
                    dgvListado.Rows(y).Cells("ConRetencion").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("ConRetencion").ToString) = True, "", dtTable.Rows(y).Item("ConRetencion").ToString) 'dtTable.Rows(y).Item("ConRetencion").ToString

                    If IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("ConRetencion").ToString) = True, "", dtTable.Rows(y).Item("ConRetencion").ToString) = "1" Then
                        dgvListado.Rows(y).Cells("Retx").Value = True
                    ElseIf IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("ConRetencion").ToString) = True, "", dtTable.Rows(y).Item("ConRetencion").ToString) = "0" Then
                        dgvListado.Rows(y).Cells("Retx").Value = False
                    End If

                    dgvListado.Rows(y).Cells("TotRetenido").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("TotRetenido").ToString) = True, "", dtTable.Rows(y).Item("TotRetenido").ToString) 'dtTable.Rows(y).Item("TotRetenido").ToString
                    dgvListado.Rows(y).Cells("RetEmprCodigo").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("RetEmprCodigo").ToString) = True, "", dtTable.Rows(y).Item("RetEmprCodigo").ToString) 'dtTable.Rows(y).Item("RetEmprCodigo").ToString
                    dgvListado.Rows(y).Cells("NumeroRet").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("NumeroRet").ToString) = True, "", dtTable.Rows(y).Item("NumeroRet").ToString) 'dtTable.Rows(y).Item("RetEmprCodigo").ToString
                    dgvListado.Rows(y).Cells("EmprRetDes").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("EmprRetDes").ToString) = True, "", dtTable.Rows(y).Item("EmprRetDes").ToString) 'dtTable.Rows(y).Item("RetEmprCodigo").ToString
                    dgvListado.Rows(y).Cells("CheqEntregado").Value = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(y).Item("CheqEntregado").ToString) = True, "", dtTable.Rows(y).Item("CheqEntregado").ToString) 'dtTable.Rows(y).Item("RetEmprCodigo").ToString

                Next
                Label3.Text = "Total de Registros : " & dtTable.Rows.Count
            Else
                Label3.Text = "Total de Registros : 0"
                dtpFechaGasto.Value = FechaParaBuscar
            End If

            Pintar2()

            'Dim dTipoCambio As Double
            'Dim dtTableTipoCambio As DataTable
            'dtTableTipoCambio = New DataTable
            'ePagoProveedores = New clsPagoProveedores
            'dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, dtpFechaGasto.Value)
            'If dtTableTipoCambio.Rows.Count > 0 Then
            '    dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
            '    lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    lblTipoCambio.ForeColor = Color.Blue
            '    txtTipoCambio.ForeColor = Color.Blue
            'Else
            '    dTipoCambio = 0.0
            '    lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    lblTipoCambio.ForeColor = Color.Red
            '    txtTipoCambio.ForeColor = Color.Red
            'End If

            Label4.Text = ""

        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub frmGastosGenerales2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblSerieText.Text = ""
        Panel11.Visible = False
        btnOpciones.Visible = False
        eTempo = New clsPlantTempo
        cboEstado.DataSource = eTempo.fColEstadoRendido
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        dtpFechaGasto.Value = Now.Date()
        mMostrarGrilla()
        eGastosGenerales = New clsGastosGenerales
        eRegistroDocumento = New clsRegistroDocumento
        Try
            eSesionCajas = New clsSesionCajas
            cboArea.DataSource = eSesionCajas.fListarArea(gEmpresa)
            If eSesionCajas.iNroRegistros > 0 Then
                cboArea.ValueMember = "AreaCodigo"
                cboArea.DisplayMember = "AreaNombre"
                cboArea.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
        cboBanco.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboBanco.ValueMember = "IdBanco"
            cboBanco.DisplayMember = "NombreBanco"
            cboBanco.SelectedIndex = -1
        End If
        cboCentroCosto.DataSource = eGastosGenerales.fListarCentrosCostos(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboCentroCosto.ValueMember = "CCosCodigo"
            cboCentroCosto.DisplayMember = "CCosDescripcion"
            cboCentroCosto.SelectedIndex = -1
        End If
        cboTipoGasto.DataSource = eGastosGenerales.fListarTiposdeGasto(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboTipoGasto.ValueMember = "IdTipoGasto"
            cboTipoGasto.DisplayMember = "DescTGasto"
            cboTipoGasto.SelectedIndex = -1
        End If
        Try
            eLibroBancos = New clsLibroBancos
            cboTipoPago.DataSource = eLibroBancos.fListarTipoMovPagos()
            If eLibroBancos.iNroRegistros > 0 Then
                cboTipoPago.ValueMember = "IdRendicion"
                cboTipoPago.DisplayMember = "DescripcionRendicion"
                cboTipoPago.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Panel3.Visible = False
        rdbEmpresas.Checked = False
        rdbUnaEmpresa.Checked = False
        cboEmpresas.Enabled = False
        cboCentrodeCostos.Enabled = False
        eGastosGenerales = New clsGastosGenerales
        cboEmpresas.DataSource = eGastosGenerales.fListarEmpresas(24, gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboEmpresas.ValueMember = "EmprCodigo"
            cboEmpresas.DisplayMember = "EmprDescripcion"
            cboEmpresas.SelectedIndex = -1
        End If
        cboEmpresas.SelectedIndex = -1
        cboCentrodeCostos.SelectedIndex = -1

        

        ''''''''''''''''''''''''''''''''''''''''''''''''''''

        BeLabel21.Text = Now.ToLongDateString
        dtpFechaGasto.Value = Now.Date()
        BeButton1.Enabled = False

        'Try
        '    eGastosGenerales = New clsGastosGenerales
        '    cbpBancoPrestado.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
        '    If eGastosGenerales.iNroRegistros > 0 Then
        '        cbpBancoPrestado.ValueMember = "IdBanco"
        '        cbpBancoPrestado.DisplayMember = "NombreBanco"
        '        cbpBancoPrestado.SelectedIndex = -1
        '    End If
        'Catch ex As Exception
        'End Try

        Label4.Text = ""
        Timer1.Enabled = True

        'For x As Integer = 0 To dgvListado.Columns.Count - 1
        '    'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
        '    dgvListado.Columns(x).Visible = True
        'Next
        '-------------------------------------------------------------

        lblEmpresa.Text = gEmpresa
        ListarPP(lblEmpresa.Text)



    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Dim IdBanco As String = ""
        GlobalIdBanco = ""

        If (cboBanco.SelectedIndex > -1) Then
            Try
                IdBanco = cboBanco.SelectedValue.ToString
                GlobalIdBanco = IdBanco
                If IdBanco <> "System.Data.DataRowView" Then
                    If rdb1.Checked = True Then
                        cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa2(gEmpresa, IdBanco)
                    ElseIf rdb2.Checked = True Then
                        cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    End If
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCuenta.ValueMember = "IdCuenta"
                        cboCuenta.DisplayMember = "NumeroCuenta"
                        cboCuenta.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboCuenta.DataSource = Nothing
        End If
        cboTipoPago.SelectedIndex = -1
        txtMontoAutomatico.Clear()
        txtIdCheque.Clear()
        Panel2.Visible = False
        Panel4.Visible = False
        cboBanco.Focus()
    End Sub

    Private Sub cboCuenta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuenta.SelectedIndexChanged
        Dim IdCuenta As String
        Dim dtCodMoneda As DataTable
        Dim sDescripcionMoneda As String
        If (cboCuenta.SelectedIndex > -1) Then
            Try

                IdCuenta = cboCuenta.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtCodMoneda = New DataTable
                    dtCodMoneda = eGastosGenerales.fTraerCodMonedaCuentaCombo(IdCuenta, gEmpresa, GlobalIdBanco)
                    If dtCodMoneda.Rows.Count > 0 Then
                        sDescripcionMoneda = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonDescripcion")))
                        sMonCodigo = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonCodigo")))
                        txtCodMoneda.Text = Trim(sDescripcionMoneda)
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            txtCodMoneda.Clear()
        End If
        cboTipoPago.SelectedIndex = -1
        txtMontoAutomatico.Clear()
        txtIdCheque.Clear()
        Panel2.Visible = False
        Panel4.Visible = False
        cboCuenta.Focus()
    End Sub

    Private Sub cboCentroCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCentroCosto.SelectedIndexChanged
        Dim IdCentroCosto As String
        If (cboCentroCosto.SelectedIndex > -1) Then
            Try
                IdCentroCosto = Trim(cboCentroCosto.SelectedValue.ToString)
                If IdCentroCosto <> "System.Data.DataRowView" Then
                    txtCodCosto.Text = Trim(IdCentroCosto)
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            txtCodCosto.Clear()
        End If
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Panel1.Visible = False
    End Sub

    Private Sub dgvCronogramas_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        Calcular()
    End Sub

    Private Sub cboTipoPago_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoPago.Click
        Opcion = "Combo"
    End Sub

    Private Sub cboChequeras_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChequeras.SelectedIndexChanged
        Dim IdChequera As String = ""
        If cboChequeras.SelectedIndex > -1 Then
            Try
                IdChequera = cboChequeras.SelectedValue.ToString
                If IdChequera <> "System.Data.DataRowView" Then
                    cboCheques.DataSource = eGastosGenerales.fListarChequesxChequeraEmpr(IdChequera, gEmpresa)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCheques.ValueMember = "IdCheque"
                        cboCheques.DisplayMember = "NumeroCheque"
                        cboCheques.SelectedIndex = 0
                        If cboCheques.Text = "System.Data.DataRowView" Then
                            txtMontoAutomatico.Text = ""
                        Else
                            txtMontoAutomatico.Text = cboCheques.Text
                        End If
                    ElseIf eGastosGenerales.iNroRegistros = 0 Then
                        cboChequeras.Focus()
                        Panel2.Visible = False
                    End If
                End If
            Catch ex As Exception
            End Try
        Else
            cboCheques.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboTipoPago_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoPago.SelectedIndexChanged
        Dim IdFormaPago As String
        Dim IdBanco As String
        Dim IdCuenta As String
        If cboTipoPago.SelectedIndex > -1 Then
            Try
                IdFormaPago = Trim(cboTipoPago.SelectedValue.ToString)
                If IdFormaPago = "00002" Then
                    txtMontoAutomatico.ReadOnly = True
                ElseIf IdFormaPago <> "00002" Then
                    txtMontoAutomatico.ReadOnly = False
                End If
                If IdFormaPago = "00002" And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    'ES CHEQUE
                    IdBanco = Trim(cboBanco.SelectedValue)
                    IdCuenta = Trim(cboCuenta.SelectedValue)
                    Dim dtChequeSig As DataTable
                    dtChequeSig = New DataTable
                    dtChequeSig = eGastosGenerales.fListarChequesBancoyCuentas(IdBanco, IdCuenta, gEmpresa)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        txtMontoAutomatico.Text = Trim(dtChequeSig.Rows(0).Item("NumeroCheque"))
                        txtIdCheque.Text = Trim(dtChequeSig.Rows(0).Item("IdCheque"))
                        txtChequera.Text = Trim(dtChequeSig.Rows(0).Item("IdChequera"))
                        txtMonto.Focus()
                    ElseIf eGastosGenerales.iNroRegistros = 0 Then
                        If Opcion = "Combo" Then
                            MessageBox.Show("No Existen Cheques con el n�mero de cuenta: " & Trim(cboCuenta.Text) & " y el Banco: " & Trim(cboBanco.Text) & ". Seleccione otra Cuenta.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboTipoPago.SelectedIndex = -1
                            cboCuenta.Focus()
                        End If
                        txtMontoAutomatico.Text = ""
                        cboChequeras.SelectedIndex = -1
                        Panel2.Visible = False
                    End If
                ElseIf IdFormaPago = "00005" And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    eGastosGenerales.fCodigoTransaccion(gEmpresa, gPeriodo)
                    txtMontoAutomatico.Text = Trim(eGastosGenerales.sCodFuturoTrans)
                Else
                    txtMontoAutomatico.Text = ""
                    cboChequeras.SelectedIndex = -1
                    Panel2.Visible = False
                    Panel4.Visible = False
                    If (OpcionMostrar = "Boton" And IdFormaPago = "00002") Or (OpcionMostrar = "Boton" And IdFormaPago = "00005") Then
                        If cboBanco.SelectedIndex = -1 Then
                            MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboTipoPago.SelectedIndex = -1
                            cboBanco.Focus()
                            Exit Sub
                        End If
                        If cboCuenta.SelectedIndex = -1 Then
                            MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboTipoPago.SelectedIndex = -1
                            cboCuenta.Focus()
                            Exit Sub
                        End If
                    ElseIf (OpcionMostrar = "Boton" And IdFormaPago <> "00002") Or (OpcionMostrar = "Boton" And IdFormaPago <> "00005") Then
                        txtMontoAutomatico.Focus()
                        txtIdCheque.Clear()
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub cboCheques_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCheques.SelectedIndexChanged
        If cboCheques.SelectedIndex > -1 Then
            If Trim(cboCheques.SelectedValue.ToString) <> "System.Data.DataRowView" Then
                txtMontoAutomatico.Text = Trim(cboCheques.Text)
                txtIdCheque.Text = Trim(cboCheques.SelectedValue.ToString)
                txtChequera.Text = Trim(cboChequeras.SelectedValue.ToString)
            End If
        Else
            txtMontoAutomatico.Text = ""
        End If
    End Sub

    Private Sub BeButton9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton9.Click
        Panel2.Visible = False
        txtMonto.Focus()
    End Sub

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click

        If rdbUnaEmpresa.Checked = True And (cboEmpresas.SelectedIndex > -1 Or cboCentrodeCostos.SelectedIndex = -1) Then
            If cboCentrodeCostos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo de la Empresa de Prestamo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCentrodeCostos.Focus()
                Panel3.Visible = True
                Exit Sub
            End If
        End If

        If rdbUnaEmpresa.Checked = True And (cboEmpresas.SelectedIndex = -1 Or cboCentrodeCostos.SelectedIndex = -1) Then
            If cboEmpresas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Empresa de Prestamo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboEmpresas.Focus()
                Panel3.Visible = True
                Exit Sub
            End If
            If cboCentrodeCostos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo de la Empresa de Prestamo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCentrodeCostos.Focus()
                Panel3.Visible = True
                Exit Sub
            End If
        End If

        Dim IdRetencionNum As String = ""
        Dim iResultadoGrabaGastos As Integer = 0
        Dim iResultadoEditCheke As Integer = 0
        Dim iResultadoEdiEstadoResumen As Integer = 0
        Dim Anulado As Integer = 0
        Dim IdTipoMovimiento As String = ""
        Dim IdCheque As String = ""
        Dim IdChequera As String = ""
        Dim IdRendicion As String = ""

        If rdbEgreso.Checked = True Then
            IdTipoMovimiento = "H"
        End If
        If rdbIngreso.Checked = True Then
            IdTipoMovimiento = "D"
        End If

        If (rdb1.Checked = True Or rdb2.Checked = True) And Len(Trim(txtCodigo.Text)) > 0 And Len(Trim(txtNroVoucher.Text)) > 0 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboCentroCosto.SelectedIndex > -1 And IdTipoMovimiento <> "" And cboTipoPago.SelectedIndex > -1 And Len(Trim(txtMonto.Text)) > 0 And Len(Trim(txtBeneficiario.Text)) > 0 And Len(Trim(txtConcepto.Text)) > 0 And cboTipoGasto.SelectedIndex > -1 And Len(Trim(txtTipoCambio.Text)) > 0 And cboEstado.SelectedIndex > -1 Then 'txtConcepto

            '**************LIBRO BANCOS CUENTA ORIGEN**************************************************************
            '******************************************************************************************************
            Dim sCodigoLibro As String = ""
            Dim sCodigoDet As String = ""
            eLibroBancos = New clsLibroBancos
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            Dim IdLibro As String = ""
            Dim A�o As String = dtpFechaGasto.Value.Year
            Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
            dtDatos = eLibroBancos.TraerIdLibro(A�o, Mes, Trim(cboCuenta.SelectedValue), gEmpresa)
            Dim MesCerrado As Integer = 0
            If dtDatos.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO
                MesCerrado = Trim(dtDatos.Rows(0).Item("ConciliadoConExtracto"))
                If MesCerrado = 1 Then
                    MessageBox.Show("Registro Bloqueado puesto que la fecha de emision pertenece a un mes Conciliado y Cerrado en el Libro Bancos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
                Dim iResultadoDet As Int16 = 0
                Dim iResultadoActSaldoLibro As Int16 = 0
                sCodigoLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                Dim dSaldoInicial As Double = 0
                Dim dSaldoFinal As Double = 0
                Dim dCheCobradosNO As Double = 0
                Dim dCheCobradosSI As Double = 0
                dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                dSaldoFinal = Trim(dtDatos.Rows(0).Item("Saldo")) 'Saldo Final
                dCheCobradosNO = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                dCheCobradosSI = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                'Agregar un nuevo detalle de libro Actual
                eLibroBancos = New clsLibroBancos
                eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro.Trim)
                sCodigoDet = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro creado
                Dim MarcaCheqNoCobrado As Integer = 0
                Dim Cobrados As Double = 0
                Dim NoCobrados As Double = 0

                Cobrados = dCheCobradosSI
                NoCobrados = dCheCobradosNO
                If Trim(cboTipoPago.SelectedValue) = "00002" Then
                    MarcaCheqNoCobrado = 1 'por cobrar 
                    NoCobrados = dCheCobradosNO + Convert.ToDouble(txtMonto.Text)
                ElseIf Trim(cboTipoPago.SelectedValue) <> "00002" Then
                    MarcaCheqNoCobrado = 0 'cobrado
                End If
                ''''''''''''''''''''''''' 'graba detalle Tabla (Tesoreria.LibroBancosDet)'''''''''''''''''
                iResultadoDet = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text.Trim), Trim(txtConcepto.Text.Trim), IdTipoMovimiento, Convert.ToDouble(txtMonto.Text), 0, MarcaCheqNoCobrado, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                ''''''''''''''''''genera itf''''''''''''''''''''''''''''''
                ''''''''''''''''''fin genera itf''''''''''''''''''''''''''''''
                Dim saldo As Double = 0
                saldo = dSaldoFinal - Convert.ToDouble(txtMonto.Text)
                'actualiza el SALDO FINAL DEL libro
                If iResultadoDet = 1 Then
                    Dim iResultadoActDetalleLibro As Int16 = 0
                    'grabamos en este ultimo detalle el saldo
                    ''''''''''''''''''''''''' 'Update para Tabla (Tesoreria.LibroBancosDet)'''''''''''''''''
                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                    Dim dtDtFech As DataTable
                    dtDtFech = New DataTable
                    Dim Fecha As DateTime
                    Dim FechaActCabecera As DateTime
                    dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sCodigoLibro)
                    If dtDtFech.Rows.Count > 0 Then
                        Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                    End If
                    If dtpFechaGasto.Value > Fecha Then
                        FechaActCabecera = dtpFechaGasto.Value
                    Else
                        FechaActCabecera = Fecha
                    End If
                    '''''''update para la tabla Tesoreria.LibroBancosCab''''''''''''''''''''''
                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                    '----------------------------------------
                    'saber si existe libros proximos al mes actual de la cuenta
                    Dim dtDatosLibro As DataTable
                    dtDatosLibro = New DataTable
                    dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigoLibro, gEmpresa)
                    Dim sIdCuenta As String = ""
                    If dtDatosLibro.Rows.Count > 0 Then
                        sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                    End If
                    Dim dtLibrosSuperiores As DataTable
                    dtLibrosSuperiores = New DataTable
                    dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                    If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                        For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                            Dim iResultadoActLibroSup As Int16 = 0
                            Dim CodigoLibroBD As String = ""
                            Dim SaldoInicialBD As Double ' = 0
                            Dim SaldoFinalBD As Double '= 0
                            CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            SaldoInicialBD = Format(saldo, "0.00")


                            SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtMonto.Text)
                            'graba saldos de uno de los libro proximos
                            '''''''''''''''''''''''''''''''''''''''''
                            Dim SaldoPrendaS As Double
                            Dim SaldoGarantiaS As Double
                            Dim SaldoDepPlazS As Double
                            Dim SaldoRetencionS As Double
                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                            '''''''''''''''''''''''''''''''''''''''''
                            ''''''''''''''Update para la tabla Tesoreria.LibroBancosCab''''''''''
                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                            saldo = SaldoFinalBD
                        Next
                    End If
                    '----------------------------------------
                End If
            ElseIf dtDatos.Rows.Count = 0 Then ' NO EXISTE LIBRO DEL MES DE PAGO
                Dim MesAnt As String = ""
                Dim Anio As String = ""
                MesAnt = Format(Mes - 1, "00") 'resto el mes actual, para usar el mes anterior
                Anio = Trim(dtpFechaGasto.Value.Year)
                If Trim(Mes) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y a�o el ACTUAL -1
                    MesAnt = "12"
                    Anio = Trim(A�o) - 1
                End If
                Dim dtLibroAnterior As DataTable
                dtLibroAnterior = New DataTable
                dtLibroAnterior = eLibroBancos.TraerIdLibro(Anio, MesAnt, Trim(cboCuenta.SelectedValue), gEmpresa)
                Dim iResultado As Int16 = 0
                Dim iResultadoDet2 As Int16 = 0
                Dim SaldoAnterior As Double = 0
                If dtLibroAnterior.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                    eLibroBancos = New clsLibroBancos
                    eLibroBancos.fCodigo(gEmpresa)
                    sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                    'graba la cabecera de libro
                    'saldo inicial = saldo final del libro anterior
                    SaldoAnterior = Trim(dtLibroAnterior.Rows(0).Item("Saldo"))
                    Dim SaldoPrenda As Double = 0
                    Dim SaldoGarantia As Double = 0
                    Dim SaldoDepPlaz As Double = 0
                    Dim SaldoRetencion As Double = 0
                    Dim SaldoIniExtracto As Double = 0
                    SaldoPrenda = Trim(dtLibroAnterior.Rows(0).Item("SaldoPrenda"))
                    SaldoGarantia = Trim(dtLibroAnterior.Rows(0).Item("SaldoGarantia"))
                    SaldoDepPlaz = Trim(dtLibroAnterior.Rows(0).Item("SaldoDepPlaz"))
                    SaldoRetencion = Trim(dtLibroAnterior.Rows(0).Item("SaldoRetencion"))
                    SaldoIniExtracto = Trim(dtLibroAnterior.Rows(0).Item("SaldoFinExtracto"))
                    'SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion

                    '''''''''''''''Graba en la tabla Tesoreria.LibroBancosCab'''''''''''''''''
                    iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuenta.SelectedValue), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtMonto.Text), dtpFechaGasto.Value, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                    'If iResultado = 2627 Then
                    '    'MessageBox.Show("No se Grabo el Registro Intente grabar otra vez", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    '    MessageBox.Show("No se Grabo el Registro Intente grabar otra vez", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    '    Exit Sub
                    'End If
                ElseIf dtLibroAnterior.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                    eLibroBancos = New clsLibroBancos
                    eLibroBancos.fCodigo(gEmpresa)
                    sCodigoLibro = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                    'graba la cabecera de libro
                    'saldo anterior =0
                    SaldoAnterior = 0
                    '''''''''''''''Graba en la tabla Tesoreria.LibroBancosCab'''''''''''''''''
                    iResultado = eLibroBancos.fGrabar(4, sCodigoLibro, gEmpresa, Trim(cboCuenta.SelectedValue), Mes, A�o, SaldoAnterior, Convert.ToDouble(txtMonto.Text), dtpFechaGasto.Value, 0, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                End If

                If iResultado = 2627 Then
                    MessageBox.Show("No se Grabo el Registro Intente grabar otra vez", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If

                If iResultado > 0 And iResultado <> 2627 Then
                    eLibroBancos = New clsLibroBancos
                    eLibroBancos.fCodigoDet(gEmpresa, sCodigoLibro)
                    sCodigoDet = eLibroBancos.sCodFuturoDet
                    Dim MarcaCheqNoCobrado As Integer = 0
                    Dim Cobrados As Double = 0
                    Dim NoCobrados As Double = 0
                    'If Trim(IdCheque) <> "" Then
                    '    MarcaCheqNoCobrado = 1
                    'ElseIf Trim(IdCheque) = "" Then
                    '    MarcaCheqNoCobrado = 0
                    'End If
                    Cobrados = 0
                    NoCobrados = 0
                    If Trim(cboTipoPago.SelectedValue) = "00002" Then
                        NoCobrados = Convert.ToDouble(txtMonto.Text)
                        MarcaCheqNoCobrado = 1
                    ElseIf Trim(cboTipoPago.SelectedValue) <> "00002" Then
                        MarcaCheqNoCobrado = 0
                    End If
                    'graba detalle del 1er registro
                    iResultadoDet2 = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDet, sCodigoLibro, dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), IdTipoMovimiento, Convert.ToDouble(txtMonto.Text), 0, MarcaCheqNoCobrado, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                    ''''''''''''''''''genera itf''''''''''''''''''''''''''''''
                    ''''''''''''''''''fin genera itf''''''''''''''''''''''''''''''
                    Dim saldo As Double = 0
                    saldo = SaldoAnterior - Convert.ToDouble(txtMonto.Text)
                    'actualiza el nuevo libro
                    If iResultadoDet2 = 1 Then
                        Dim iResultadoActDetalleLibro As Int16 = 0
                        iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, sCodigoDet, sCodigoLibro, saldo)
                        Dim iResultadoActSaldoLibro As Int16 = 0
                        iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sCodigoLibro, Format(SaldoAnterior, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), dtpFechaGasto.Value) 'graba el nuevo saldo del libro
                    End If
                End If
            End If
            '************** FIN LIBRO BANCOS CUENTA ORIGEN ********************************************************

            '******************************************************************************************************
            Dim sCodigoLibroDestino As String = ""
            Dim sCodigoDetDestino As String = ""


            If (Trim(cboTipoPago.SelectedValue.ToString) = "00005" Or Trim(cboTipoPago.SelectedValue.ToString) = "00007") And cboEmpresasLibros.SelectedIndex > -1 And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 And Len(txtMontoAutomatico.Text.Trim) > 0 Then
                '**************LIBRO BANCOS CUENTA DESTINO*************************************************************
                '******************************************************************************************************
                eLibroBancos = New clsLibroBancos
                Dim dtLibroDestino As DataTable
                dtLibroDestino = New DataTable
                'Dim IdLibroDestino As String = ""
                Dim A�oDestino As String = dtpFechaGasto.Value.Year
                Dim MesDestino As String = Format(dtpFechaGasto.Value.Month, "00")
                Dim MontoDest As Double = 0

                If sMonCodigo = sMonCodigoDest Then
                    MontoDest = Convert.ToDouble(txtMonto.Text)
                ElseIf sMonCodigo <> sMonCodigoDest Then
                    If sMonCodigo = "02" And sMonCodigoDest = "01" Then
                        MontoDest = Format(Convert.ToDouble(txtMonto.Text) * Val(txtTipoCambio.Text), "#,##0.00")
                    End If
                    If sMonCodigo = "01" And sMonCodigoDest = "02" Then
                        MontoDest = Format(Convert.ToDouble(txtMonto.Text) / Val(txtTipoCambio.Text), "#,##0.00")
                    End If
                    'MontoDest = Convert.ToDouble(txtMonto.Text)
                End If

                dtLibroDestino = eLibroBancos.TraerIdLibro(A�oDestino, MesDestino, Trim(cboCuentaPrestada.SelectedValue), Trim(cboEmpresasLibros.SelectedValue))
                If dtLibroDestino.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO EN LA CUENTA DESTINO
                    sCodigoLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                    Dim iResultadoDetDestino As Int16 = 0
                    Dim iResultadoActSaldoLibroDestino As Int16 = 0
                    sCodigoLibroDestino = Trim(dtLibroDestino.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                    Dim dSaldoInicialDes As Double = 0
                    Dim dSaldoFinalDes As Double = 0
                    Dim dCheCobradosNODes As Double = 0
                    Dim dCheCobradosSIDes As Double = 0
                    dSaldoInicialDes = Trim(dtLibroDestino.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                    dSaldoFinalDes = Trim(dtLibroDestino.Rows(0).Item("Saldo")) 'Saldo Final
                    dCheCobradosNODes = Trim(dtLibroDestino.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                    dCheCobradosSIDes = Trim(dtLibroDestino.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                    'Agregar el detalle de libro Actual, Agregando un nuevo registro
                    eLibroBancos = New clsLibroBancos
                    eLibroBancos.fCodigoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino.Trim)
                    sCodigoDetDestino = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro creado
                    Dim Cobrados As Double = 0
                    Dim NoCobrados As Double = 0
                    Cobrados = dCheCobradosSIDes
                    NoCobrados = dCheCobradosNODes
     

                    iResultadoDetDestino = eLibroBancos.fGrabarDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, dtpFechaGasto.Value, "00012", "", Trim(txtReferencia.Text), "", "D", MontoDest, 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                    Dim saldo As Double = 0
                    saldo = dSaldoFinalDes + Convert.ToDouble(MontoDest)
                    'actualiza el nuevo libro
                    If iResultadoDetDestino = 1 Then
                        Dim iResultadoActDetalleLibro As Int16 = 0
                        iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, saldo)
                        Dim dtDtFech As DataTable
                        dtDtFech = New DataTable
                        Dim Fecha As DateTime
                        Dim FechaActCabecera As DateTime
                        dtDtFech = eLibroBancos.TraerMaxFechadeLibro(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino)
                        If dtDtFech.Rows.Count > 0 Then
                            Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                        End If
                        If dtpFechaGasto.Value > Fecha Then
                            FechaActCabecera = dtpFechaGasto.Value
                        Else
                            FechaActCabecera = Fecha
                        End If
                        iResultadoActSaldoLibroDestino = eLibroBancos.fActSaldoLibro(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino, Format(dSaldoInicialDes, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                        'saber si existe libros proximos al mes actual de la cuenta
                        Dim dtDatosLibro As DataTable
                        dtDatosLibro = New DataTable
                        dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigoLibroDestino, Trim(cboEmpresasLibros.SelectedValue))
                        Dim sIdCuenta As String = ""
                        If dtDatosLibro.Rows.Count > 0 Then
                            sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                        End If
                        Dim dtLibrosSuperiores As DataTable
                        dtLibrosSuperiores = New DataTable
                        dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, Trim(cboEmpresasLibros.SelectedValue), FechaActCabecera) 'trae los proximos libros
                        If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                            For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                Dim iResultadoActLibroSup As Int16 = 0
                                Dim CodigoLibroBD As String = ""
                                Dim SaldoInicialBD As Double ' = 0
                                Dim SaldoFinalBD As Double '= 0
                                CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                SaldoInicialBD = Format(saldo, "0.00")
                                SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(MontoDest)
                                'graba saldos de uno de los libro proximos
                                '''''''''''''''''''''''''''''''''''''''''
                                Dim SaldoPrendaS As Double
                                Dim SaldoGarantiaS As Double
                                Dim SaldoDepPlazS As Double
                                Dim SaldoRetencionS As Double
                                SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                '''''''''''''''''''''''''''''''''''''''''
                                iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(Trim(cboEmpresasLibros.SelectedValue), CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                saldo = SaldoFinalBD
                            Next
                        End If
                    End If
                ElseIf dtLibroDestino.Rows.Count = 0 Then ' NO EXISTE LIBRO DEL MES DE PAGO
                    Dim MesAnt As String = ""
                    Dim Anio As String = ""
                    MesAnt = Format(MesDestino - 1, "00") 'resto el mes actual, para usar el mes anterior
                    Anio = Trim(dtpFechaGasto.Value.Year)
                    If Trim(MesDestino) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y mi a�o el actual -1
                        MesAnt = "12"
                        Anio = Trim(A�o) - 1
                    End If
                    Dim dtLibroAnterior2 As DataTable
                    dtLibroAnterior2 = New DataTable
                    dtLibroAnterior2 = eLibroBancos.TraerIdLibro(Anio, MesAnt, Trim(cboCuentaPrestada.SelectedValue), Trim(cboEmpresasLibros.SelectedValue))
                    Dim iResultado As Int16 = 0
                    Dim iResultadoDet2 As Int16 = 0
                    Dim SaldoAnteriorDes As Double = 0
                    If dtLibroAnterior2.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigo(Trim(cboEmpresasLibros.SelectedValue))
                        sCodigoLibroDestino = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                        'graba la cabecera de libro
                        'saldo inicial = saldo final del libro anterior
                        SaldoAnteriorDes = Trim(dtLibroAnterior2.Rows(0).Item("Saldo"))
                        Dim SaldoPrenda As Double = 0
                        Dim SaldoGarantia As Double = 0
                        Dim SaldoDepPlaz As Double = 0
                        Dim SaldoRetencion As Double = 0
                        Dim SaldoIniExtracto As Double = 0
                        SaldoPrenda = Trim(dtLibroAnterior2.Rows(0).Item("SaldoPrenda"))
                        SaldoGarantia = Trim(dtLibroAnterior2.Rows(0).Item("SaldoGarantia"))
                        SaldoDepPlaz = Trim(dtLibroAnterior2.Rows(0).Item("SaldoDepPlaz"))
                        SaldoRetencion = Trim(dtLibroAnterior2.Rows(0).Item("SaldoRetencion"))
                        SaldoIniExtracto = Trim(dtLibroAnterior2.Rows(0).Item("SaldoFinExtracto"))
                        'SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion
                        iResultado = eLibroBancos.fGrabar(4, sCodigoLibroDestino, Trim(cboEmpresasLibros.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), MesDestino, A�oDestino, SaldoAnteriorDes, Convert.ToDouble(MontoDest), dtpFechaGasto.Value, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                    ElseIf dtLibroAnterior2.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigo(Trim(cboEmpresasLibros.SelectedValue))
                        sCodigoLibroDestino = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                        'graba la cabecera de libro
                        'saldo anterior =0
                        SaldoAnteriorDes = 0
                        iResultado = eLibroBancos.fGrabar(4, sCodigoLibroDestino, Trim(cboEmpresasLibros.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), MesDestino, A�oDestino, SaldoAnteriorDes, Convert.ToDouble(MontoDest), dtpFechaGasto.Value, 0, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                    End If

                    If iResultado > 0 And iResultado <> 2627 Then
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino)
                        sCodigoDetDestino = eLibroBancos.sCodFuturoDet
                        Dim Cobrados As Double = 0
                        Dim NoCobrados As Double = 0
                        'graba detalle del 1er registro
                        iResultadoDet2 = eLibroBancos.fGrabarDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, dtpFechaGasto.Value, "00012", "", Trim(txtReferencia.Text), "", "D", Convert.ToDouble(MontoDest), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                        Dim saldo As Double = 0
                        saldo = SaldoAnteriorDes + Convert.ToDouble(MontoDest)
                        'actualiza el nuevo libro
                        If iResultadoDet2 = 1 Then
                            Dim iResultadoActDetalleLibro As Int16 = 0
                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, saldo)
                            Dim iResultadoActSaldoLibro As Int16 = 0
                            iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino, Format(SaldoAnteriorDes, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), dtpFechaGasto.Value) 'graba el nuevo saldo del libro
                        End If
                    End If
                End If
                '************** FIN LIBRO BANCOS CUENTA DESTINO *******************************************************
                '******************************************************************************************************
            End If

            If Mov = "L" Then
                If Convert.ToDouble(txtMonto.Text) > MontoLetra Then
                    MessageBox.Show("El Monto Supera la Deuda a Pagar de la Letra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtMonto.Focus()
                    Exit Sub
                End If
            End If

            IdRendicion = Trim(cboEstado.SelectedValue)

            If Trim(cboTipoPago.SelectedValue.ToString) = "00002" Then 'si es pagado con cheke
                If Len(txtIdCheque.Text.Trim) > 0 Then 'si tiene asignado un numero de cheke?
                    IdCheque = Trim(txtIdCheque.Text.Trim) 'aqui el id de cheque
                    IdChequera = Trim(txtChequera.Text.Trim)
                    'editar estado cheke
                    If CheckBox1.Checked = True Then 'el cheque sera anulado? SI
                        'anular
                        Anulado = 1
                        iResultadoEditCheke = eMovimientoCajaBanco.fEditarCheque(8, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), 0, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, gUsuario, sMonCodigo, gEmpresa)
                    Else 'el cheque sera anulado? NO
                        'emitido
                        Anulado = 0
                        iResultadoEditCheke = eMovimientoCajaBanco.fEditarCheque(5, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, gUsuario, sMonCodigo, gEmpresa)
                    End If
                    Dim dtChequesxChequera As DataTable
                    dtChequesxChequera = New DataTable
                    dtChequesxChequera = eGastosGenerales.fListarCantidadChequesxChequeraEmpr(IdChequera, gEmpresa)
                    If dtChequesxChequera.Rows.Count > 0 Then
                        Dim CantidadChe As Integer ' = 0
                        CantidadChe = Trim(dtChequesxChequera.Rows(0).Item("Contador"))
                        If CantidadChe = 0 Then
                            'CAMBIAR ESTADO CHEQUERA
                            iResultadoEditCheke = eMovimientoCajaBanco.fEditarChequera(35, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), IdChequera, Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, Trim(gUsuario), "", 0, 0, "", sMonCodigo, gEmpresa)
                        ElseIf CantidadChe > 0 Then
                            iResultadoEditCheke = eMovimientoCajaBanco.fEditarChequera(36, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), IdChequera, Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, Trim(gUsuario), "", 0, 0, "", sMonCodigo, gEmpresa)
                        End If
                    End If
                Else
                    IdCheque = ""
                End If
            Else
                IdCheque = ""
            End If 'fin If Trim(cboTipoPago.SelectedValue.ToString) = "00002" Then


            Dim MontoSaldoaPagar As Double = 0
            Dim IdResumen As String = ""
            If Trim(strIdTipoAnexo) = "P" Then
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim CodRegDoc, DetrPer, OrdCom, IdDetCro, CCostoCod, IdOrden, EmpresaCode As String
                Dim iResProgramacion As Integer = 0
                Dim iResultadoGrabaPro As Integer = 0
                Dim Monto As Double
                Dim iResultado2, iResultado3, iResultado4, iEditOrdenCompra As Integer
                Dim NroDoc, FechaDocumento, FechaVencimiento, DP, MontoDP, OrdCompr, XMarca, IdRegistro, AbrDoc As String
                Dim SimboloMo, Importe, MontoPagar, Saldo, MonedaDes, CodEmprz As String
                Dim MntoProgramacioAnt As Double
                Dim CantiDocProgramacioAnt As Integer = 0
                Dim GlosaProgramacioAnt As String = ""
                Dim newGlosaFac As String = ""
                Dim dtTableProgramacion As DataTable
                dtTableProgramacion = New DataTable
                If CountCantDocSoles > 0 Or CountCantDocDolares > 0 Then
                    'si se escogio 1 o mas docs, entonces se buscara una programacion en esa semana con el idProv,NrSemana,Anio y CodEmpr actual.
                    ePagoProveedores = New clsPagoProveedores
                    dtTableProgramacion = ePagoProveedores.fTraerResExisteSoles("P", gEmpresa, Trim(txtIdProv.Text), Trim(txtSemanaPago.Text), gPeriodo, Trim(sMonCodigo))
                    iResProgramacion = dtTableProgramacion.Rows.Count
                    cboEstado.SelectedValue = "02"
                ElseIf CountCantDocSoles = 0 Or CountCantDocDolares = 0 Then
                    cboEstado.SelectedValue = "01"
                End If
                If iResProgramacion = 0.5 Then
                    'nunca entrara aqui.
                    IdResumen = Trim(dtTableProgramacion.Rows(0).Item("IdResumen"))
                    MntoProgramacioAnt = Trim(dtTableProgramacion.Rows(0).Item("MontoTotal"))
                    CantiDocProgramacioAnt = Trim(dtTableProgramacion.Rows(0).Item("CantidadDoc"))
                    GlosaProgramacioAnt = Trim(dtTableProgramacion.Rows(0).Item("GlosaNumPago"))
                    If dgvDocumentos.Rows.Count > 0 Then
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                                CodRegDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                DetrPer = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                Monto = Val(Trim(dgvDocumentos.Rows(x).Cells("Porcentaje").Value)) 'PORCENTAJE
                                CCostoCod = Trim(dgvDocumentos.Rows(x).Cells("IdCC").Value)
                                OrdCom = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                IdOrden = Trim(dgvDocumentos.Rows(x).Cells("CodigoOC").Value) 'IDORDEN
                                EmpresaCode = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) 'IDORDEN
                                If Len(Trim(IdOrden)) = 0 Then
                                    OrdCom = ""
                                Else
                                    iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, EmpresaCode)
                                End If
                                Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                If Trim(EmpresaCode) = Trim(gEmpresa) Then
                                    If Saldo = 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                    ElseIf Saldo > 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                    End If
                                ElseIf Trim(EmpresaCode) <> Trim(gEmpresa) Then
                                    If Saldo = 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 1, 1, Saldo, CCostoCod, IdOrden, "00003", 1, gEmpresa)
                                    ElseIf Saldo > 0 Then
                                        iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 0, 0, Saldo, CCostoCod, IdOrden, "00001", 1, gEmpresa)
                                    End If
                                End If
                                AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("Doc").Value)
                                NroDoc = Trim(dgvDocumentos.Rows(x).Cells("NroDocumento").Value)
                                FechaDocumento = Trim(dgvDocumentos.Rows(x).Cells("Fecha").Value)
                                FechaVencimiento = Trim(dgvDocumentos.Rows(x).Cells("FechaV").Value)
                                SimboloMo = Trim(dgvDocumentos.Rows(x).Cells("Mo").Value)
                                Importe = Val(Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value))
                                Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                MontoPagar = Val(Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value))
                                DP = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                MontoDP = Val(Trim(dgvDocumentos.Rows(x).Cells("IPor").Value))
                                OrdCompr = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                XMarca = 1
                                IdRegistro = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value)
                                MonedaDes = Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value)
                                CodEmprz = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                                If Trim(dgvDocumentos.Rows(x).Cells("TipoMoneda").Value) = "01" Then
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumen, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, CodEmprz)
                                    newGlosaFac = newGlosaFac & " " & AbrDoc & " " & NroDoc & "  "
                                ElseIf Trim(dgvDocumentos.Rows(x).Cells("TipoMoneda").Value) = "02" Then
                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumen, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, CodEmprz)
                                    newGlosaFac = newGlosaFac & " " & AbrDoc & " " & NroDoc & "  "
                                End If
                            End If
                        Next
                        If Trim(IdResumen) <> "" Then
                            iResultado4 = ePagoProveedores.ActResumen(5, gEmpresa, IdResumen, Convert.ToDouble(txtTotPagarProv.Text) + MntoProgramacioAnt, 0, CountDocumentos + CantiDocProgramacioAnt, GlosaProgramacioAnt & " " & newGlosaFac)
                        End If
                    End If 'fin If dgvDocumentos.Rows.Count > 0 Then
                ElseIf iResProgramacion = 0 Or iResProgramacion > 0 Then
                    'si NO hay programacion alguna con el Proveedor, o si ya existe entonces...
                    If dgvDocumentos.Rows.Count > 0 Then ' si hay datos en el gri de documentos...
                        If CountDocumentos > 0 Then
                            'genero el nuevo IdDeCabeceradePagoProv
                            ePagoProveedores.GenerarCodResumen(gEmpresa)
                            IdResumen = ePagoProveedores.sCodFuturo
                            'GRABO CABECERA DE PROGRAMACION DE DOCUMENTOS
                            iResultadoGrabaPro = ePagoProveedores.GrabarResumen(IdResumen, gEmpresa, "P", Trim(sMonCodigo), Trim(txtIdProv.Text), Convert.ToDouble(txtTotPagarProv.Text), Val(txtTipoCambio.Text), Trim(txtSemanaPago.Text), CountDocumentos, strGlosaFacturasSoles, 0, Trim(txtRazonProv.Text), Trim(txtRucProv.Text), gPeriodo)
                            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                                    CodRegDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                    DetrPer = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                    Monto = Val(Trim(dgvDocumentos.Rows(x).Cells("Porcentaje").Value)) 'PORCENTAJE
                                    CCostoCod = Trim(dgvDocumentos.Rows(x).Cells("IdCC").Value)
                                    OrdCom = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                    IdOrden = Trim(dgvDocumentos.Rows(x).Cells("CodigoOC").Value) 'IDORDEN
                                    EmpresaCode = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) 'IDORDEN
                                    If Len(Trim(IdOrden)) = 0 Then
                                        OrdCom = ""
                                    Else
                                        'NroOrden Desactivada
                                        iEditOrdenCompra = eRegistroDocumento.fEditarEstadoOrden(1, IdOrden, EmpresaCode)
                                    End If
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                    'EDITO COMPROBANTES A ESTADO DE PROVISIONADOS

                                    If Trim(EmpresaCode) = Trim(gEmpresa) Then
                                        'si la emprdoc del documento es igual a la empresa accedida
                                        'actualiza el doc datos... y provisiona pero esta mal q provisione aki,mmm
                                        If Saldo <= 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                        End If

                                    ElseIf Trim(EmpresaCode) <> Trim(gEmpresa) Then
                                        'si la emprdoc del documento NO es igual a la empresa accedida
                                        'actualiza el doc datos... y provisiona pero esta mal q provisione aki,mmm, PERO AQUI ACTIVA COMO PAGOS PRESTADOS
                                        If Saldo <= 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 1, 1, Saldo, CCostoCod, IdOrden, "00003", 1, gEmpresa)
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmpresaCode, CodRegDoc, DetrPer, Monto, OrdCom, 1, "", 0, 0, Saldo, CCostoCod, IdOrden, "00001", 1, gEmpresa)
                                        End If
                                    End If
                                    'GRABO DETALLE DE PROGRAMACION CON LOS DOCS SELECCIONADOS

                                    AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("Doc").Value)
                                    NroDoc = Trim(dgvDocumentos.Rows(x).Cells("NroDocumento").Value)
                                    FechaDocumento = Trim(dgvDocumentos.Rows(x).Cells("Fecha").Value)
                                    FechaVencimiento = Trim(dgvDocumentos.Rows(x).Cells("FechaV").Value)
                                    SimboloMo = Trim(dgvDocumentos.Rows(x).Cells("Mo").Value)
                                    Importe = Val(Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value))
                                    Saldo = Val(Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value))
                                    MontoPagar = Val(Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value))
                                    DP = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                    MontoDP = Val(Trim(dgvDocumentos.Rows(x).Cells("IPor").Value))
                                    OrdCompr = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                    XMarca = 1
                                    IdRegistro = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                    'AbrDoc = Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value)
                                    MonedaDes = Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value)
                                    CodEmprz = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)

                                    ePagoProveedores.GenerarCodDetCro(gEmpresa)
                                    IdDetCro = ePagoProveedores.sCodFuturoDetCro
                                    iResultado3 = ePagoProveedores.GrabarDetalleCro(0, IdDetCro, IdResumen, AbrDoc, NroDoc, FechaDocumento, FechaVencimiento, Importe, MontoPagar, Saldo, DP, MontoDP, OrdCompr, XMarca, IdRegistro, MonedaDes, SimboloMo, gEmpresa, 0, CodEmprz)
                                End If
                            Next
                        End If ' fin If CountDocumentos > 0 Then
                    End If ' fin If dgvDocumentos.Rows.Count > 0 Then 
                End If 'fin If iResProgramacion = 0.5 Then
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If IdResumen <> "" Then
                    If TipoMonedas = "01" Then
                        'actualizar saldo prove moneda soles ++++++++++
                        Dim iResultado1 As Integer = 0
                        Dim IdSaldoProv As String = ""
                        Dim MontoSaldoSolesProv As Double = 0
                        Dim MontoSaldoSolesNew As Double = 0
                        Dim dtTable4 As DataTable
                        dtTable4 = New DataTable
                        dtTable4 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, Trim(strIdTipoAnexo), strIdAnexo, "01")
                        If dtTable4.Rows.Count > 0 Then
                            IdSaldoProv = dtTable4.Rows(0).Item("IdSaldo")
                            MontoSaldoSolesProv = dtTable4.Rows(0).Item("MontoSaldo")
                            MontoSaldoSolesNew = MontoSaldoSolesProv - MontoRestar
                            'MontoSaldoaPagar = MontoSaldoSolesNew
                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, strIdTipoAnexo, strIdAnexo, gEmpresa, MontoSaldoSolesNew, "01")
                        End If
                    ElseIf TipoMonedas = "02" Then
                        'actualizar saldo prove moneda dolares ++++++++++
                        Dim iResultado1 As Integer = 0
                        Dim IdSaldoProv As String = ""
                        Dim MontoSaldoDolaresProv As Double = 0
                        Dim MontoSaldoDolaresNew As Double = 0
                        Dim dtTable5 As DataTable
                        dtTable5 = New DataTable
                        dtTable5 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, strIdTipoAnexo, strIdAnexo, "02")
                        If dtTable5.Rows.Count > 0 Then
                            IdSaldoProv = dtTable5.Rows(0).Item("IdSaldo")
                            MontoSaldoDolaresProv = dtTable5.Rows(0).Item("MontoSaldo")
                            MontoSaldoDolaresNew = MontoSaldoDolaresProv - MontoRestar
                            'MontoSaldoaPagar = MontoSaldoDolaresNew
                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, strIdTipoAnexo, strIdAnexo, gEmpresa, MontoSaldoDolaresNew, "02")
                        End If
                    End If

                    If dgvDocumentos.Rows.Count > 0 Then
                        'AQUI CAMBIAMOS COMO CANCELADO A LAS FACTURAS SELECCIONADAS
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                                Dim IdRegistroDoc As String = ""
                                Dim EmprCod As String = ""
                                Dim SaldoDoc As Double = 0
                                'For x As Integer = 0 To dtTableDetRes.Rows.Count - 1
                                IdRegistroDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                Saldo = Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value)
                                EmprCod = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                                Dim PPago As Integer = 0
                                Dim Cancelado As Integer = 0
                                Dim IdSituacion As String = ""
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("Column6").Value) = True Then
                                    PPago = 1
                                    Cancelado = 0
                                    IdSituacion = "00003"
                                ElseIf Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("Column6").Value) = False Then
                                    PPago = 0
                                    Cancelado = 1
                                    IdSituacion = "00004"
                                End If
                                eMovimientoCajaBanco.ActSaldoDoc(42, IdRegistroDoc, EmprCod, Saldo, PPago, Cancelado, Trim(IdSituacion))
                                'Next
                            End If
                        Next
                        If Len(Trim(IdResumen)) > 0 Then
                            Dim dtMontoResumen As DataTable
                            Dim SaldoResumen As String
                            dtMontoResumen = New DataTable
                            dtMontoResumen = eGastosGenerales.fMontoResumen(Trim(IdResumen), gEmpresa)
                            If dtMontoResumen.Rows.Count > 0 Then
                                SaldoResumen = dtMontoResumen.Rows(0).Item("MontoTotal")
                                MontoSaldoaPagar = SaldoResumen - MontoRestar
                            End If
                        End If
                    End If ' fin If dgvDocumentos.Rows.Count > 0 Then
                    'traer todos los docs del resumen
                    'en un loop  actualizar la tabla saldo con los montos a pagar de los docs traidos..
                    iResultadoEdiEstadoResumen = eMovimientoCajaBanco.fEditarResumen(7, Trim(dtpFechaGasto.Value), IdResumen, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), MontoSaldoaPagar, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdRendicion, Trim(gUsuario), "", 0, 0, "", sMonCodigo, gEmpresa)
                End If 'If IdResumen <> "" Then
            End If 'If Trim(strIdTipoAnexo) = "P" Then

            ''**************LIBRO BANCOS CUENTA ORIGEN**************************************************************
            ''******************************************************************************************************
            'Dim dTipoCambio As Double
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            eMovimientoCajaBanco.fCodigo(gEmpresa)
            IdMovimiento = eMovimientoCajaBanco.sCodFuturo
            txtCodigo.Text = IdMovimiento
            eMovimientoCajaBanco.fCodigoVouCher(gEmpresa, gPeriodo)
            NroVoucher = eMovimientoCajaBanco.sCodFuturoVoucher
            txtNroVoucher.Text = NroVoucher
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim Prestamo As String = ""
            Dim EmprPrestamo As String = ""
            Dim CCPrestamo As String = ""
            If rdbEmpresas.Checked = True Then
                Prestamo = "2"
                EmprPrestamo = ""
                CCPrestamo = ""
            End If
            If rdbUnaEmpresa.Checked = True Then
                Prestamo = "1"
                EmprPrestamo = Trim(cboEmpresas.SelectedValue)
                CCPrestamo = Trim(cboCentrodeCostos.SelectedValue)
            End If

            'PARA TRANSACCIONES
            Dim IdTransaccion As String = ""
            Dim IdBancoDestino As String = ""
            Dim IdCuentaDestino As String = ""
            Dim GlosaDestino As String = ""
            If Trim(cboTipoPago.SelectedValue) = "00005" And Len(Trim(txtMontoAutomatico.Text)) > 0 Then ' And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 Then
                Dim iResultadoGrabaTrans As Integer = 0
                eGastosGenerales.fIdTransaccion(gEmpresa)
                IdTransaccion = Trim(eGastosGenerales.sCodTransId)
                iResultadoGrabaTrans = eMovimientoCajaBanco.fGrabarTransaccion(37, dtpFechaGasto.Value, IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtReferencia.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdChequera, IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, sCodigoLibro, sCodigoDet, Prestamo, EmprPrestamo, CCPrestamo, Convert.ToInt64(txtMontoAutomatico.Text), IdTransaccion, Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), gPeriodo)
                'MessageBox.Show(dtpFechaGasto.Value)
            End If
            'PARA CARTAS
            Dim IdCarta As String = ""
            'Dim IdBancoDestino As String = ""
            'Dim IdCuentaDestino As String = ""
            'Dim GlosaDestino As String = ""
            If Trim(cboTipoPago.SelectedValue.ToString) = "00007" And Len(txtMontoAutomatico.Text.Trim) > 0 Then 'And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 Then
                Dim iResultadoGrabaTrans As Integer = 0
                eGastosGenerales.fIdCarta(gEmpresa)
                IdCarta = Trim(eGastosGenerales.sCodTransId)
                iResultadoGrabaTrans = eMovimientoCajaBanco.fGrabarTransaccion(39, dtpFechaGasto.Value, IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtReferencia.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdChequera, IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, sCodigoLibro, sCodigoDet, Prestamo, EmprPrestamo, CCPrestamo, Convert.ToInt64(txtMontoAutomatico.Text), IdCarta, Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), gPeriodo)
            End If
            ''''''''''''''''''''''''
            'Dim sCodigoLibroDestino As String = ""
            'Dim sCodigoDetDestino As String = ""


            '++++++++++++++++++++++RETENCIONES
            Dim IdRetencion As String = ""
            Dim SerieParaRet As String = ""
            Dim RetencioSiNo As Integer = 0
            Dim NombreProv As String = ""
            'Dim DirProv As String = ""
            If CountDocumentos > 0 Then
                NombreProv = Trim(txtRazonProv.Text)
                'DirProv = Trim(txtDireccionProv.Text)
            ElseIf CountDocumentos = 0 Then
                NombreProv = Trim(txtBeneficiario.Text)
                'DirProv = ""
            End If
            '------------------------------------------------------------------

            Dim sCCostoRetencion As String = ""
            Dim DesEmpresaPrestada As String = ""

            If rdbUnaEmpresa.Checked = True And cboEmpresas.SelectedIndex > -1 And cboCentrodeCostos.SelectedIndex > -1 Then
                EmpresaRetencion = Trim(cboEmpresas.SelectedValue)
                sCCostoRetencion = cboCentrodeCostos.SelectedValue
                DesEmpresaPrestada = " [ En la Empresa: " & cboEmpresas.Text & " ] "
            Else
                EmpresaRetencion = gEmpresa
                sCCostoRetencion = cboCentroCosto.SelectedValue
                DesEmpresaPrestada = ""
            End If
            '------------------------------------------------------------------
            Dim dtTableSerieRete As DataTable
            dtTableSerieRete = New DataTable
            eSeries = New clsSeries
            'buscar Serie de la EmprRetenida con CC
            dtTableSerieRete = eSeries.fListar(5, EmpresaRetencion, Trim(sCCostoRetencion))
            If dtTableSerieRete.Rows.Count > 0 Then
                gSerie = Trim(dtTableSerieRete.Rows(0).Item("Serie"))
                gNumeroInicioSerie = Trim(dtTableSerieRete.Rows(0).Item("NumeroIni"))
            Else
                'si el cc no esta en ninguna serie buscara la Serie activada en la EmprRetenida
                Dim dtTableSerieReteAct As DataTable
                dtTableSerieReteAct = New DataTable
                eSeries = New clsSeries
                dtTableSerieReteAct = eSeries.fListar(9, EmpresaRetencion, Trim(sCCostoRetencion))
                If dtTableSerieReteAct.Rows.Count > 0 Then
                    gSerie = Trim(dtTableSerieReteAct.Rows(0).Item("Serie"))
                    gNumeroInicioSerie = Trim(dtTableSerieReteAct.Rows(0).Item("NumeroIni"))
                ElseIf dtTableSerieReteAct.Rows.Count = 0 Then
                    'sino hay ninguna serie activa en la empresaRetenida no hay serie 
                    gSerie = ""
                    gNumeroInicioSerie = ""
                End If
            End If
            If chkRetencion.Checked = True And Trim(gSerie) <> "" And Trim(gNumeroInicioSerie) <> "" Then
                RetencioSiNo = 1
                If RetencioSiNo = 1 Then

                    Dim iGrabarRetencion As Integer = 0
                    eMovimientoCajaBanco = New clsMovimientoCajaBanco
                    eMovimientoCajaBanco.fCodigoRetencion(1, EmpresaRetencion, gSerie, gNumeroInicioSerie)
                    IdRetencion = eMovimientoCajaBanco.sCodFuturoRetencion
                    IdRetencionNum = Microsoft.VisualBasic.Right(Trim(IdRetencion), 7)
                    Dim MontoParaRetencion As Double = 0
                    'Dim RetenidoTxt As Double = 0
                    If Trim(sMonCodigo) = "01" Then
                        MontoParaRetencion = Convert.ToDouble(txtMonto.Text) + Convert.ToDouble(txtRetenido.Text)
                        'RetenidoTxt = Convert.ToDouble(txtRetenido.Text)
                    ElseIf Trim(sMonCodigo) = "02" Then
                        MontoParaRetencion = (Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)) + Convert.ToDouble(txtRetenido.Text)
                        'RetenidoTxt = Convert.ToDouble(txtRetenido.Text) * Convert.ToDouble(txtTipoCambio.Text)
                    End If
                    Dim MontoLetras As String = ""
                    Dim dtTableLetras As DataTable
                    dtTableLetras = New DataTable
                    eBusquedaRetenciones = New clsBusquedaRetenciones
                    dtTableLetras = eBusquedaRetenciones.fConsultaMontoLetras(Convert.ToDouble(txtRetenido.Text))
                    If dtTableLetras.Rows.Count > 0 Then
                        'MontoLetras = Trim(Convert.ToString(dtTableLetras.Rows(0).Item(0)))
                        MontoLetras = IIf(Microsoft.VisualBasic.IsDBNull(dtTableLetras.Rows(0).Item(0)), "", dtTableLetras.Rows(0).Item(0))
                    End If
                    Dim DireccionProv As String = ""
                    Dim dtTableDireccionProv As DataTable
                    dtTableDireccionProv = New DataTable
                    eBusquedaRetenciones = New clsBusquedaRetenciones
                    dtTableDireccionProv = eBusquedaRetenciones.fConsultaRetenciones(11, Trim(txtRuc.Text), gEmpresa, Today(), Today(), 0, "")
                    If dtTableDireccionProv.Rows.Count > 0 Then
                        DireccionProv = IIf(Microsoft.VisualBasic.IsDBNull(dtTableDireccionProv.Rows(0).Item(0)), "", dtTableDireccionProv.Rows(0).Item(0))
                    End If
                    iGrabarRetencion = eMovimientoCajaBanco.fGrabarRetencion(0, IdRetencion, EmpresaRetencion, gSerie, IdRetencionNum, dtpFechaGasto.Value, NombreProv, DireccionProv, Trim(txtRuc.Text), MontoLetras, MontoParaRetencion, PorcentajeRetencion, Convert.ToDouble(txtRetenido.Text), txtTipoCambio.Text, MontoParaRetencion - Convert.ToDouble(txtRetenido.Text), 0, gUsuario, Trim(sCCostoRetencion))
                End If
                SerieParaRet = gSerie
            ElseIf chkRetencion.Checked = False Then
                SerieParaRet = ""
            End If
            '++++++++++++++++++++++ FIN RETENCIONES

            If Trim(txtRetenido.Text) = "" Then
                txtRetenido.Text = "0.00"
            End If

            Try
                iResultadoGrabaGastos = eMovimientoCajaBanco.fGrabar(1, dtpFechaGasto.Value, IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdChequera, IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, sCodigoLibro, sCodigoDet, Prestamo, EmprPrestamo, CCPrestamo, 0, IdTransaccion, Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), Trim(txtReferencia.Text), sCodigoLibroDestino, sCodigoDetDestino, gPeriodo, IdCarta, gUsuario, IdLetra, Trim(Mov), Trim(IdRetencion), gSerie, Convert.ToString(IIf(Me.chkRetencion.Checked = True, 1, 0)), Convert.ToDouble(txtRetenido.Text), EmpresaRetencion, ID_DocPendiente)

                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                If Len(Trim(IdMovimiento)) > 0 Then
                    Dim iResultado As Integer = 0
                    iResultado = eMovimientoCajaBanco.fActEntregCheq(48, Trim(IdMovimiento), gEmpresa, gUsuario, Convert.ToString(IIf(Me.chkEntregado.Checked = True, 1, 0)))
                End If

            Catch ex As Exception
            End Try

            If iResultadoGrabaGastos > 0 Then
                cSegurosPolizas = New clsSegurosPolizas
                If Len(Trim(sEndoso)) = 0 Then
                    sEndoso = sPoliza
                End If
                cSegurosPolizas.fUpdateEstadoEgresos(gEmpresa, sPoliza, sNroCuota, "C", gUsuario, IdMovimiento, Trim(sEndoso))
            End If



            If Mov = "V" Then
                If dgvDetalleVal.Rows.Count > 0 Then
                    Dim iResultadoActVal As Integer = 0
                    For x As Integer = 0 To dgvDetalleVal.Rows.Count - 1
                        If Convert.ToBoolean(dgvDetalleVal.Rows(x).Cells("XX").Value) = True Then
                            Dim IdDoc As String = ""
                            Dim EmprDoc As String = ""
                            Dim SaldoVV As Double = 0
                            IdDoc = Trim(dgvDetalleVal.Rows(x).Cells("IdRegistroDoc").Value)
                            EmprDoc = Trim(dgvDetalleVal.Rows(x).Cells("EmprCodIdRegistroDoc").Value)
                            If Trim(IdDoc) = "" Then
                                IdDoc = ".........."
                            End If
                            If Trim(EmprDoc) = "" Then
                                EmprDoc = ".........."
                            End If
                            SaldoVV = Convert.ToDouble(dgvDetalleVal.Rows(x).Cells("SaldoV").Value)
                            Dim dtDocProgxFecha As DataTable
                            eValorizaciones = New clsValorizaciones
                            dtDocProgxFecha = New DataTable
                            dtDocProgxFecha = eValorizaciones.fListarObras(58, EmprDoc, gPeriodo, "", Trim(IdDoc), dtpFechaGasto.Value, "")
                            Dim SumaImportePago As Double = 0
                            Dim DetraDoc As Double = 0
                            Dim RetPorDoc As Double = 0
                            Dim OtroDesDoc As Double = 0
                            Dim dtDocProgX As DataTable
                            eValorizaciones = New clsValorizaciones
                            dtDocProgX = New DataTable
                            dtDocProgX = eValorizaciones.fListarObras(61, EmprDoc, gPeriodo, "", Trim(IdDoc), dtpFechaGasto.Value, "")

                            If dtDocProgX.Rows.Count > 0 Then
                                For z As Integer = 0 To dtDocProgX.Rows.Count - 1
                                    DetraDoc = DetraDoc + Convert.ToDouble(dtDocProgX.Rows(z).Item("Detra"))
                                    RetPorDoc = RetPorDoc + Convert.ToDouble(dtDocProgX.Rows(z).Item("RetPor"))
                                    OtroDesDoc = OtroDesDoc + Convert.ToDouble(dtDocProgX.Rows(z).Item("OtrosD"))
                                Next
                            End If

                            If dtDocProgxFecha.Rows.Count > 0 Then
                                For z As Integer = 0 To dtDocProgxFecha.Rows.Count - 1
                                    Dim ImportePago As Double = 0
                                    Dim StrIdValorizacion As String = ""
                                    Dim StrIdPago As String = ""
                                    ImportePago = dtDocProgxFecha.Rows(z).Item("ImportePago")
                                    StrIdValorizacion = Trim(dtDocProgxFecha.Rows(z).Item("IdValorizacion"))
                                    StrIdPago = Trim(dtDocProgxFecha.Rows(z).Item("IdPago"))
                                    eValorizaciones = New clsValorizaciones
                                    Dim DtTotPagadoVal As DataTable
                                    DtTotPagadoVal = New DataTable
                                    DtTotPagadoVal = eValorizaciones.fListarTotPagadodeVal(28, Trim(StrIdValorizacion), gEmpresa, gPeriodo)
                                    If DtTotPagadoVal.Rows.Count > 0 Then
                                        Dim PagadoNuevo As Double = 0
                                        Dim SaldoNuevo As Double = 0
                                        PagadoNuevo = Format((DtTotPagadoVal.Rows(0).Item("TotPagado") + ImportePago), "#,##0.0000")
                                        SaldoNuevo = Format((DtTotPagadoVal.Rows(0).Item("Saldo") - ImportePago), "#,##0.0000")
                                        SumaImportePago = SumaImportePago + ImportePago
                                        Dim xIdDoc As String = ""
                                        Dim IdEmprDoc As String = ""
                                        xIdDoc = Trim(DtTotPagadoVal.Rows(0).Item("IdRegistro"))
                                        IdEmprDoc = Trim(DtTotPagadoVal.Rows(0).Item("EmprCodigoDoc"))
                                        If Trim(xIdDoc) = "" Then
                                            xIdDoc = ".........."
                                        End If
                                        If Trim(IdEmprDoc) = "" Then
                                            IdEmprDoc = ".........."
                                        End If
                                        iResultadoActVal = eValorizaciones.fActualizarSaldos(29, PagadoNuevo, SaldoNuevo, 1, dtpFechaGasto.Value, IdMovimiento, gEmpresa, gUsuario, StrIdValorizacion, StrIdPago, xIdDoc, IdEmprDoc, 0, gPeriodo)
                                    End If
                                Next
                            End If

                            Dim SaldoNew As Double = 0
                            Dim SumaNew As Double = 0
                            Dim iResultadoActDoc As Integer = 0
                            SaldoNew = Format((SaldoVV - SumaImportePago), "#,##0.00")
                            SumaNew = Format((DetraDoc + RetPorDoc + OtroDesDoc), "#,##0.00")
                            If SaldoNew = SumaNew Then
                                iResultadoActDoc = eValorizaciones.fActualizarSaldos(59, SaldoNew, 0, 1, dtpFechaGasto.Value, Trim(IdDoc), Trim(EmprDoc), gUsuario, "00004", "", "", "", 0, gPeriodo)
                            Else
                                iResultadoActDoc = eValorizaciones.fActualizarSaldos(59, SaldoNew, 0, 0, dtpFechaGasto.Value, Trim(IdDoc), Trim(EmprDoc), gUsuario, "00003", "", "", "", 0, gPeriodo)
                            End If
                        End If
                    Next
                    If iResultadoActVal > 0 Then
                        txtBuscar_TextChanged(sender, e)
                    End If
                End If
            End If
            '#####Fin If Mov = "V" Then

            Dim IdDetMovimiento As String = ""
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            eMovimientoCajaBanco.fCodigoDetalleMov(gEmpresa)
            IdDetMovimiento = eMovimientoCajaBanco.sCodFuturoDetMov
            Dim iEstado As Integer
            Dim EmpresaPrestada As String
            Dim CCEmpresaPrestada As String
            If rdbUnaEmpresa.Checked = True And rdbEmpresas.Checked = False Then
                iEstado = 1
            End If
            If rdbEmpresas.Checked = True And rdbUnaEmpresa.Checked = False Then
                iEstado = 0
            End If
            If cboEmpresas.SelectedIndex > -1 Then
                EmpresaPrestada = cboEmpresas.SelectedValue
            Else
                EmpresaPrestada = ""
            End If
            If cboCentrodeCostos.SelectedIndex > -1 Then
                CCEmpresaPrestada = cboCentrodeCostos.SelectedValue
            Else
                CCEmpresaPrestada = ""
            End If

            If CountDocumentos = 0 Then
                Dim iResultadoGrabaDetGastos As Integer = 0
                iResultadoGrabaDetGastos = eMovimientoCajaBanco.fGrabarDetalle(27, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(IdDetMovimiento), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, sCodigoLibro, sCodigoDet, EmpresaPrestada, iEstado, CCEmpresaPrestada)
                Dim iResActCantDetenCab As Integer = 0
                iResActCantDetenCab = eMovimientoCajaBanco.fActCantDetCabecera(43, Trim(IdMovimiento), gEmpresa, 1)
            End If

            '''''''''''''''''''''''''''''''''''''''''

            'DesEmpresaPrestada

            If dgvDocumentos.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                    If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                        Dim IdRegistro As String = ""
                        Dim CCosCodigoDoc As String = ""
                        Dim MontoDoc As String = ""
                        Dim Importe As Double = 0
                        Dim MontoPagar As Double = 0
                        Dim MontoActualizarDoc As Double = 0
                        Dim Saldo As Double = 0
                        Dim CodEmpresaX As String = ""
                        IdRegistro = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                        Saldo = Trim(dgvDocumentos.Rows(x).Cells("Saldo").Value)
                        CCosCodigoDoc = Trim(dgvDocumentos.Rows(x).Cells("CosCodigo").Value)
                        MontoDoc = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)
                        CodEmpresaX = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                        Dim MontoRete As Double = 0
                        Dim ImporteTotal As Double = 0
                        If Len(Trim(IdRetencion)) > 0 Then
                            ImporteTotal = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)
                            If Trim(sMonCodigo) = "01" Then
                                MontoRete = ImporteTotal * (PorcentajeRetencion / 100)
                            ElseIf Trim(sMonCodigo) = "02" Then
                                MontoRete = (ImporteTotal * Convert.ToDouble(txtTipoCambio.Text)) * (PorcentajeRetencion / 100)
                            End If
                        End If
                        If Trim(CodEmpresaX) = Trim(gEmpresa) Then
                            eMovimientoCajaBanco.fActDetalleCronoIdChek(16, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(IdRegistro), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), MontoRestar, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, gEmpresa, Trim(gUsuario), "", 0, 0, IdResumen, sMonCodigo, CodEmpresaX, PorcentajeRetencion, MontoRete, EmpresaRetencion, IdRetencion, SerieParaRet)
                        ElseIf Trim(CodEmpresaX) <> Trim(gEmpresa) Then
                            eMovimientoCajaBanco.fActDetalleCronoIdChek(44, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(IdRegistro), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), MontoRestar, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, gEmpresa, Trim(gUsuario), "", 0, 0, IdResumen, sMonCodigo, CodEmpresaX, PorcentajeRetencion, MontoRete, EmpresaRetencion, IdRetencion, SerieParaRet)
                        End If
                    End If
                Next

                If CountDocumentos > 0 Then
                    Dim CCosCodigoDoc As String = ""
                    Dim MontoDoc As String = ""
                    Dim dtDocs As DataTable
                    dtDocs = New DataTable
                    Dim dt1o2Empr As DataTable
                    dt1o2Empr = New DataTable
                    dt1o2Empr = eMovimientoCajaBanco.fListarDocsMovimiento2(IdResumen, gEmpresa)
                    If dt1o2Empr.Rows.Count = 1 Then
                        dtDocs = eMovimientoCajaBanco.fListarDocsMovimiento(38, IdMovimiento, gEmpresa)
                    ElseIf dt1o2Empr.Rows.Count > 1 Then
                        dtDocs = eMovimientoCajaBanco.fListarDocsMovimiento3(IdResumen, gEmpresa)
                    End If
                    If dtDocs.Rows.Count > 0 Then
                        For x As Integer = 0 To dtDocs.Rows.Count - 1
                            Dim iResultadoGrabaDetGastos As Integer = 0
                            If dt1o2Empr.Rows.Count = 1 Then
                                CCosCodigoDoc = dtDocs.Rows(x).Item("CCosCodigo")
                                MontoDoc = dtDocs.Rows(x).Item("Suma")
                                Dim IdDetMovimientoProveedor As String = ""
                                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                eMovimientoCajaBanco.fCodigoDetalleMov(gEmpresa)
                                IdDetMovimientoProveedor = eMovimientoCajaBanco.sCodFuturoDetMov
                                iResultadoGrabaDetGastos = eMovimientoCajaBanco.fGrabarDetalle(27, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(IdDetMovimientoProveedor), CCosCodigoDoc, Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(MontoDoc), Trim(txtBeneficiario.Text), "", Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, sCodigoLibro, sCodigoDet, EmpresaPrestada, iEstado, CCEmpresaPrestada)
                            ElseIf dt1o2Empr.Rows.Count > 1 Then
                                Dim EmprDocx As String = ""
                                EmprDocx = dtDocs.Rows(x).Item("EmprDoc")
                                If Trim(gEmpresa) = Trim(EmprDocx) Then
                                    CCosCodigoDoc = dtDocs.Rows(x).Item("CCosCodigo")
                                    EmpresaPrestada = ""
                                    CCEmpresaPrestada = ""
                                    iEstado = 0
                                ElseIf Trim(gEmpresa) <> Trim(EmprDocx) Then
                                    CCosCodigoDoc = Trim(cboCentroCosto.SelectedValue)
                                    EmpresaPrestada = dtDocs.Rows(x).Item("EmprDoc")
                                    CCEmpresaPrestada = dtDocs.Rows(x).Item("CCosCodigo")
                                    iEstado = 1
                                End If
                                MontoDoc = dtDocs.Rows(x).Item("Suma")
                                Dim IdDetMovimientoProveedor As String = ""
                                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                                eMovimientoCajaBanco.fCodigoDetalleMov(gEmpresa)
                                IdDetMovimientoProveedor = eMovimientoCajaBanco.sCodFuturoDetMov
                                iResultadoGrabaDetGastos = eMovimientoCajaBanco.fGrabarDetalle(27, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(IdDetMovimientoProveedor), CCosCodigoDoc, Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(MontoDoc), Trim(txtBeneficiario.Text), "", Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, sCodigoLibro, sCodigoDet, EmpresaPrestada, iEstado, CCEmpresaPrestada)
                            End If
                        Next
                        Dim iResActCantDetenCab As Integer = 0
                        iResActCantDetenCab = eMovimientoCajaBanco.fActCantDetCabecera(43, Trim(IdMovimiento), gEmpresa, dtDocs.Rows.Count)
                    End If
                End If
            End If

            If iResultadoGrabaGastos > 0 Then
                If Mov = "RC" Then
                    Dim iResActReembolsoSesion As Int16 = 0
                    eSesionCajas = New clsSesionCajas
                    iResActReembolsoSesion = eSesionCajas.fActualizarCabecera2(7, IdSesion, Trim(gEmpresa), NroSesion, IdArea, IdCaja, Trim(dtpFechaGasto.Value), Trim(dtpFechaGasto.Value), 0, 0, 0, 1, gUsuario)
                End If
                If Mov = "L" Then
                    Dim iResActSaldoLetra As Integer
                    Dim SaldoLetra As Double = 0
                    Dim EstadoLetra As Integer = 0
                    eSesionCajas = New clsSesionCajas
                    SaldoLetra = MontoLetra - (Convert.ToDouble(txtMonto.Text))
                    If SaldoLetra <= 0 Then
                        EstadoLetra = 1
                    End If
                    iResActSaldoLetra = eSesionCajas.fActualizarLetras(8, EstadoLetra, IdLetra, Trim(gEmpresa), SaldoLetra, gUsuario, FechaGiroG, FechaVenG)
                End If

            End If

            If iResultadoGrabaGastos > 0 And RetencioSiNo = 0 Then
                MessageBox.Show("Proceso de Grabaci�n Concluido!", "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            ElseIf iResultadoGrabaGastos > 0 And RetencioSiNo = 1 Then
                MessageBox.Show("Proceso de Grabaci�n Concluido! con Nro de Retenci�n: " & gSerie & " - " & IdRetencionNum & DesEmpresaPrestada, "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            If iResultadoGrabaGastos > 0 Then
                mMostrarGrilla()
                If dgvListado.Rows.Count > 0 Then
                    LimpiarControles()
                    txtCodigo.Clear()
                    txtRuc.Clear()
                    txtNroVoucher.Clear()
                    CheckBox1.Checked = False
                End If
            End If

            Mov = ""
            BeButton4_Click(sender, e)
            CountDocumentos = 0
            strIdTipoAnexo = ""
            IdResumen = ""
            TipoMonedas = ""

        Else
            If rdb1.Checked = False And rdb2.Checked = False Then
                MessageBox.Show("Seleccione el tipo de Movimiento: Cheque o Transferencia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                BeButton4.Focus()
                Exit Sub
            End If

            If Len(Trim(txtCodigo.Text)) = 0 Then
                MessageBox.Show("Genere los C�digos de Registro con el Bot�n Nuevo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                BeButton4.Focus()
                Exit Sub
            End If
            If Len(Trim(txtNroVoucher.Text)) = 0 Then
                MessageBox.Show("Genere los C�digos de Registro con el Bot�n Nuevo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                BeButton4.Focus()
                Exit Sub
            End If
            If cboBanco.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboBanco.Focus()
                Exit Sub
            End If
            If cboCuenta.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un n�mero de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCuenta.Focus()
                Exit Sub
            End If
            If cboCentroCosto.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCentroCosto.Focus()
                Exit Sub
            End If
            If IdTipoMovimiento = "" Then
                MessageBox.Show("Seleccione un Tipo de Operacion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                rdbEgreso.Focus()
                Exit Sub
            End If
            If cboTipoPago.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Forma de Pago", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipoPago.Focus()
                Exit Sub
            End If
            If Len(Trim(txtMonto.Text)) = 0 Then
                MessageBox.Show("Ingrese el Monto a Pagar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtMonto.Focus()
                Exit Sub
            End If
            If Len(Trim(txtBeneficiario.Text)) = 0 Then
                MessageBox.Show("Ingrese el nombre del Beneficiario", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtBeneficiario.Focus()
                Exit Sub
            End If
            If Len(Trim(txtConcepto.Text)) = 0 Then
                MessageBox.Show("Ingrese la Glosa", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtConcepto.Focus()
                Exit Sub
            End If
            If cboTipoGasto.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboTipoGasto.Focus()
                Exit Sub
            End If
            If Len(Trim(txtTipoCambio.Text)) = 0 Then
                MessageBox.Show("Ingrese el Tipo de Cambio", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtTipoCambio.Focus()
                Exit Sub
            End If
            If cboEstado.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Estado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboEstado.Focus()
                Exit Sub
            End If
        End If
        If dgvPD.RowCount <> Nothing Then

            ActualziarEstadoPPD(dgvPD.CurrentRow.Cells("ID_PROPAGO_DET").Value)
            ID_DocPendiente = "0"
            ListarPP(lblEmpresa.Text)
        End If


    End Sub

    ''Termino del Boton Agregar
    Private Sub BeButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton4.Click
        chkDocTodas.Checked = False
        CountDocumentos = 0
        strIdTipoAnexo = ""
        IdResumen = ""
        TipoMonedas = ""
        Panel1.Visible = False
        txtLineaCre.Clear()
        txtDiasCred.Clear()
        txtEstadoCre.Clear()
        txtIdLinea.Clear()
        txtEstadoCre.BackColor = Color.White
        txtRuc.Clear()
        txtRucProv.Clear()
        txtIdProv.Clear()
        txtRazonProv.Clear()
        txtDireccionProv.Clear()
        txtNroDoc.Clear()
        txtTotPagarProv.Clear()

        If dgvDocumentos.Rows.Count > 0 Then
            For zz As Integer = 0 To dgvDocumentos.RowCount - 1
                dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
            Next
        End If

        OpcionMostrar = "Boton"
        If dgvListadoReem.Rows.Count > 0 Then
            For zz As Integer = 0 To dgvListadoReem.RowCount - 1
                dgvListadoReem.Rows.Remove(dgvListadoReem.CurrentRow)
            Next
        End If

        'Dim dTipoCambio As Double
        eMovimientoCajaBanco = New clsMovimientoCajaBanco
        eMovimientoCajaBanco.fCodigo(gEmpresa)
        IdMovimiento = eMovimientoCajaBanco.sCodFuturo
        txtCodigo.Text = IdMovimiento
        eMovimientoCajaBanco.fCodigoVouCher(gEmpresa, gPeriodo)
        NroVoucher = eMovimientoCajaBanco.sCodFuturoVoucher
        txtNroVoucher.Text = NroVoucher
        ePagoProveedores = New clsPagoProveedores
        eTempo = New clsPlantTempo
        Dim dTipoCambio As Double
        Dim dtTableTipoCambio As DataTable
        dtTableTipoCambio = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, dtpFechaGasto.Value)
        If dtTableTipoCambio.Rows.Count > 0 Then
            dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
            lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            lblTipoCambio.ForeColor = Color.Blue
            txtTipoCambio.ForeColor = Color.Blue
        Else
            dTipoCambio = 0.0
            lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            lblTipoCambio.ForeColor = Color.Red
            txtTipoCambio.ForeColor = Color.Red
        End If
        BeButton1.Enabled = True
        BeButton8.Enabled = True
        BeButton3.Enabled = False
        LimpiarControles()
        cboBanco.Enabled = True
        cboCuenta.Enabled = True
        txtRuc.Clear()
        cboArea.SelectedIndex = -1
        cboCaja.SelectedIndex = -1
        cboTipoPago.Enabled = True
        BeButton1.Enabled = True
        CheckBox1.Enabled = False
        txtMontoAutomatico.Enabled = True
        txtMonto.Enabled = True
        txtBeneficiario.Enabled = True
        txtConcepto.Enabled = True
        txtNroVoucher.Enabled = False
        BeButton6.Enabled = True
        txtRuc.Enabled = True
        BeButton2.Enabled = False
        BeButton8.Enabled = False
        rdb1.Checked = False
        rdb2.Checked = False
        dtpFechaGasto.Enabled = True
        rdbEmpresas.Checked = False
        rdbUnaEmpresa.Checked = False
        cboEmpresas.Enabled = False
        cboCentrodeCostos.Enabled = False
        cboEstado.DataSource = eTempo.fColEstadoRendido
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        chkFecha.Checked = True
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False
        cboCentroCosto.Enabled = True
        btnPrestamo.Enabled = True
        cboTipoGasto.Enabled = True
        txtTipoCambio.Enabled = True
        
        txtReferencia.Clear()
        cbpBancoPrestado.Enabled = True
        cboCuentaPrestada.Enabled = True
        txtReferencia.ReadOnly = False
        btnCancel.Enabled = True
        rdb1.Enabled = True
        rdb2.Enabled = True
        rdb1.Focus()
        dtpFechaGasto.Value = FechaParaBuscar
        dtpFechaGasto.Focus()
        chkRetencion.Checked = False
        chkRetencion.Enabled = True
        txtRetenido.Clear()
        txtMonto.Clear()
        Panel11.Visible = False
        btnOpciones.Visible = False
        'txtGirar.Clear()
        txtIdRetencion.Clear()
        txtRetenido.Enabled = True
        cboOtrasEmpresas.DataSource = Nothing

        rdbEmpresas.Checked = False
        rdbUnaEmpresa.Checked = False
        cboEmpresas.Enabled = True
        cboCentrodeCostos.Enabled = True
        eGastosGenerales = New clsGastosGenerales
        cboEmpresas.DataSource = eGastosGenerales.fListarEmpresas(24, gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboEmpresas.ValueMember = "EmprCodigo"
            cboEmpresas.DisplayMember = "EmprDescripcion"
            cboEmpresas.SelectedIndex = -1
        End If
        cboEmpresas.SelectedIndex = -1
        cboCentrodeCostos.SelectedIndex = -1

        eGastosGenerales = New clsGastosGenerales
        cboEmpresasLibros.DataSource = eGastosGenerales.fListarEmpresas(54, gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboEmpresasLibros.ValueMember = "EmprCodigo"
            cboEmpresasLibros.DisplayMember = "EmprDescripcion"
            cboEmpresasLibros.SelectedIndex = -1
        End If

        'cboEmpresasLibros.SelectedIndex = -1
        'cbpBancoPrestado.SelectedIndex = -1
        cbpBancoPrestado.DataSource = Nothing
        cboCuentaPrestada.DataSource = Nothing

        chkEntregado.Checked = False

        'cboCentrodeCostos.ValueMember= "vaca"
    End Sub

    Private Sub LimpiarControles()
        dtpFechaGasto.Value = Now.Date()
        cboBanco.SelectedIndex = -1
        cboCuenta.SelectedIndex = -1
        cboCentroCosto.SelectedIndex = -1
        rdbEgreso.Checked = True
        rdbIngreso.Checked = False
        cboTipoPago.SelectedIndex = -1
        txtMontoAutomatico.Clear()
        txtMonto.Clear()
        CheckBox1.Checked = False
        txtBeneficiario.Clear()
        txtConcepto.Clear()
        cboTipoGasto.SelectedIndex = -1
        cboEstado.SelectedIndex = -1
        'txtConcepto.Clear
    End Sub

    Private Sub BeButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton3.Click

        If Trim(sIdCodigoCabLibro) <> "" And Trim(sIdCodigoDetLibro) <> "" And gEmpresa <> "" Then
            Dim A�o As String = dtpFechaGasto.Value.Year
            Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            dtDatos = eLibroBancos.TraerIdLibro(A�o, Mes, Trim(cboCuenta.SelectedValue), gEmpresa)
            Dim MesCerrado As Integer = 0
            If dtDatos.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO
                MesCerrado = Trim(dtDatos.Rows(0).Item("ConciliadoConExtracto"))
                If MesCerrado = 1 Then
                    MessageBox.Show("Registro Bloqueado para Editar puesto que la fecha de emision pertenece a un mes Conciliado y Cerrado en el Libro Bancos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
        End If

        If rdbUnaEmpresa.Checked = True And (cboEmpresas.SelectedIndex > -1 Or cboCentrodeCostos.SelectedIndex = -1) Then
            If cboCentrodeCostos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo de la Empresa de Prestamo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCentrodeCostos.Focus()
                Panel3.Visible = True
                Exit Sub
            End If
        End If


        Dim iResultadoGrabaGastos As Integer = 0
        Dim iResultadoEditCheke As Integer = 0
        Dim iResultadoEdiEstadoResumen As Integer = 0
        Dim Anulado As Integer = 0
        Dim IdTipoMovimiento As String = ""
        Dim IdCheque As String = ""
        Dim IdRendicion As String = ""
        eMovimientoCajaBanco = New clsMovimientoCajaBanco
        If MovBD = "V" Then
            eValorizaciones = New clsValorizaciones
            Dim DtTotPagadoValDet As DataTable
            DtTotPagadoValDet = New DataTable
            DtTotPagadoValDet = eValorizaciones.fListarTotPagadodeVal(30, Trim(txtCodigo.Text), gEmpresa, gPeriodo)
            Dim iResultadoActSaldos As Integer = 0
            If DtTotPagadoValDet.Rows.Count > 0 Then
                For x As Integer = 0 To DtTotPagadoValDet.Rows.Count - 1
                    Dim StrCad As Double = 0
                    Dim StrIdValorizacion As String = ""
                    Dim StrIdPago As String = ""
                    StrCad = Convert.ToDouble(DtTotPagadoValDet.Rows(x).Item("ImportePago"))
                    StrIdValorizacion = DtTotPagadoValDet.Rows(x).Item("IdValorizacion")
                    StrIdPago = DtTotPagadoValDet.Rows(x).Item("IdPago")
                    eValorizaciones = New clsValorizaciones
                    Dim DtTotPagadoVal As DataTable
                    DtTotPagadoVal = New DataTable
                    DtTotPagadoVal = eValorizaciones.fListarTotPagadodeVal(28, Trim(StrIdValorizacion), gEmpresa, gPeriodo)
                    If DtTotPagadoVal.Rows.Count > 0 Then
                        Dim PagadoNuevo As Double = 0
                        Dim SaldoNuevo As Double = 0
                        Dim IdDoc As String = ""
                        Dim IdEmprDoc As String = ""
                        PagadoNuevo = DtTotPagadoVal.Rows(0).Item("TotPagado") - StrCad
                        SaldoNuevo = DtTotPagadoVal.Rows(0).Item("Saldo") + StrCad
                        IdDoc = Trim(DtTotPagadoVal.Rows(0).Item("IdRegistro"))
                        IdEmprDoc = Trim(DtTotPagadoVal.Rows(0).Item("EmprCodigoDoc"))
                        If Trim(IdDoc) = "" Then
                            IdDoc = ".........."
                        End If
                        If Trim(IdEmprDoc) = "" Then
                            IdEmprDoc = ".........."
                        End If
                        iResultadoActSaldos = eValorizaciones.fActualizarSaldos(29, PagadoNuevo, SaldoNuevo, 0, dtpFechaGasto.Value, "", gEmpresa, gUsuario, StrIdValorizacion, StrIdPago, Trim(IdDoc), Trim(IdEmprDoc), StrCad, gPeriodo)
                    End If
                Next
                If iResultadoActSaldos > 0 Then
                    txtBuscar_TextChanged(sender, e)
                End If
            End If
        End If
        If Len(Trim(txtCodigo.Text)) > 0 And Len(Trim(txtNroVoucher.Text)) > 0 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And cboCentroCosto.SelectedIndex > -1 And cboTipoPago.SelectedIndex > -1 And Len(Trim(txtMonto.Text)) > 0 And Len(Trim(txtBeneficiario.Text)) > 0 And Len(Trim(txtConcepto.Text)) > 0 And cboTipoGasto.SelectedIndex > -1 And Len(Trim(txtTipoCambio.Text)) > 0 And cboEstado.SelectedIndex > -1 Then 'txtConcepto
            IdRendicion = Trim(cboEstado.SelectedValue)
            If rdbEgreso.Checked = True Then
                IdTipoMovimiento = "H"
            End If
            If rdbIngreso.Checked = True Then
                IdTipoMovimiento = "D"
            End If
            IdCheque = IdChequeBD
            If Trim(IdSesion) <> "" And NroSesion <> 0 And Trim(IdArea) <> "" And Trim(IdCaja) <> "" Then
                Dim iResActReembolsoSesion As Int16 = 0
                eSesionCajas = New clsSesionCajas
                iResActReembolsoSesion = eSesionCajas.fActualizarCabecera2(7, IdSesion, Trim(gEmpresa), NroSesion, IdArea, IdCaja, Trim(dtpFechaGasto.Value), Trim(dtpFechaGasto.Value), 0, 0, 0, 0, gUsuario)
            End If
            'si esta asignado a una letra
            If Trim(IdLetraBD) <> "" Then
                Dim iResActReembolsoSesion As Int16 = 0
                eSesionCajas = New clsSesionCajas
                Dim iResActSaldoLetra As Integer
                Dim SaldoLetra As Double = 0
                Dim SaldoLetraBD As Double = 0
                Dim FechaGiroBD As DateTime
                Dim FechaVctoBD As DateTime
                Dim EstadoLetra As Integer = 0
                Dim dtSaldoLetra As DataTable
                eSesionCajas = New clsSesionCajas
                dtSaldoLetra = eSesionCajas.VerificarDatosLetra(9, IdLetraBD, Trim(gEmpresa))
                If dtSaldoLetra.Rows.Count > 0 Then
                    SaldoLetraBD = Convert.ToString(dtSaldoLetra.Rows(0).Item("Saldo"))
                    FechaGiroBD = Convert.ToString(dtSaldoLetra.Rows(0).Item("FechaGiro"))
                    FechaVctoBD = Convert.ToString(dtSaldoLetra.Rows(0).Item("FechaVcto"))
                    SaldoLetra = SaldoLetraBD + dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value '(Convert.ToDouble(txtMonto.Text))
                    If SaldoLetra > 0 Then
                        EstadoLetra = 0
                    ElseIf SaldoLetra < 0 Or SaldoLetra = 0 Then
                        EstadoLetra = 1
                    End If
                    iResActSaldoLetra = eSesionCajas.fActualizarLetras(8, EstadoLetra, IdLetraBD, Trim(gEmpresa), SaldoLetra, gUsuario, FechaGiroBD, FechaVctoBD)
                End If
            End If

            If CheckBox1.Checked = True Then 'SE ANULA SI O NO
                Anulado = 1 'ANULAR
                If Len(Trim(IdRetencionBD)) > 0 And Len(Trim(RetEmprCodigoBD)) Then
                    Dim iGrabarRetencion As Integer = 0
                    eMovimientoCajaBanco = New clsMovimientoCajaBanco
                    iGrabarRetencion = eMovimientoCajaBanco.fGrabarRetencion(2, Trim(IdRetencionBD), RetEmprCodigoBD, Trim(SerieBD), "", dtpFechaGasto.Value, "", "", Trim(txtRuc.Text), "", 0, PorcentajeRetencion, 0 * (PorcentajeRetencion / 100), txtTipoCambio.Text, 0 - (0 * (PorcentajeRetencion / 100)), 0, gUsuario, "")
                End If
                If IdResumenBD <> "" Then 'si registro es un pago de proveedor?�
                    Dim dtTableDetRes As DataTable
                    dtTableDetRes = New DataTable
                    dtTableDetRes = eMovimientoCajaBanco.fDetRes(20, Trim(txtCodigo.Text), IdResumenBD, gEmpresa) 'busca detalle del pago
                    If dtTableDetRes.Rows.Count > 0 Then
                        Dim IdRegistro As String = ""
                        Dim Importe As Double = 0
                        Dim MontoPagar As Double = 0
                        Dim MontoActualizarDoc As Double = 0
                        Dim SaldoDocumento As Double = 0
                        Dim MontoActualizarSaldoDoc As Double = 0
                        Dim MontoSumSaldoProv As Double = 0
                        Dim sDescripDoc As String = ""
                        Dim EmprDocBd As String = ""
                        For x As Integer = 0 To dtTableDetRes.Rows.Count - 1 ' a cada detalle de pago, cada doc
                            sDescripDoc = dtTableDetRes.Rows(x).Item("DescripDoc") 'se le asigna el monto a pagar al salo del documento
                            MontoPagar = dtTableDetRes.Rows(x).Item("MontoPagar")
                            IdRegistro = dtTableDetRes.Rows(x).Item("IdRegistro")
                            EmprDocBd = dtTableDetRes.Rows(x).Item("EmprDoc")
                            Dim dtTableSalDoc As DataTable
                            dtTableSalDoc = New DataTable
                            dtTableSalDoc = eMovimientoCajaBanco.SaldoDoc(22, IdRegistro, EmprDocBd)
                            If dtTableSalDoc.Rows.Count > 0 Then
                                SaldoDocumento = dtTableSalDoc.Rows(0).Item("SaldoDocumento")
                            End If
                            MontoActualizarSaldoDoc = SaldoDocumento + MontoPagar
                            eMovimientoCajaBanco.ActSaldoDoc(21, IdRegistro, EmprDocBd, MontoActualizarSaldoDoc, 1, 0, "00003")
                            MontoSumSaldoProv = MontoSumSaldoProv + MontoPagar
                            eMovimientoCajaBanco.AnularDetResumen(24, IdResumenBD, IdRegistro, EmprDocBd, gUsuario)
                        Next
                        Dim MontoSaldoaPagar As String = 0
                        If Len(Trim(IdResumenBD)) > 0 Then
                            Dim dtMontoResumen As DataTable
                            Dim SaldoResumen As String
                            dtMontoResumen = New DataTable
                            dtMontoResumen = eGastosGenerales.fMontoResumen(Trim(IdResumenBD), gEmpresa)
                            If dtMontoResumen.Rows.Count > 0 Then
                                SaldoResumen = dtMontoResumen.Rows(0).Item("MontoTotal")
                                MontoSaldoaPagar = SaldoResumen + MontoSumSaldoProv
                            End If
                        End If
                        'traer todos los docs del resumen
                        'en un loop  actualizar la tabla saldo con los montos a pagar de los docs traidos..
                        Dim iResultadoEdiEstadoResumen2 As Integer = 0
                        iResultadoEdiEstadoResumen2 = eMovimientoCajaBanco.fEditarResumen(7, Trim(dtpFechaGasto.Value), IdResumenBD, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, "", Trim(cboTipoPago.SelectedValue), MontoSaldoaPagar, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, "", "", "", 0, 0, "", sMonCodigo, gEmpresa)
                        Dim dtTable5 As DataTable
                        dtTable5 = New DataTable
                        Dim CodMoneda As String = ""
                        Dim IdSaldoProv As String = ""
                        Dim MontoSaldoDolaresProv As Double = 0
                        Dim MontoSaldoNew As Double = 0
                        Dim iResultado1 As Int32
                        If sDescripDoc = "01" Then
                            CodMoneda = "01"
                        ElseIf sDescripDoc = "02" Then
                            CodMoneda = "02"
                        End If
                        dtTable5 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, strIdTipoAnexo, strIdAnexo, CodMoneda)
                        If dtTable5.Rows.Count > 0 Then 'se actualiza el saldo del proveedor
                            IdSaldoProv = dtTable5.Rows(0).Item("IdSaldo")
                            MontoSaldoDolaresProv = dtTable5.Rows(0).Item("MontoSaldo")
                            MontoSaldoNew = MontoSaldoDolaresProv + MontoSumSaldoProv
                            iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, strIdTipoAnexo, strIdAnexo, gEmpresa, MontoSaldoNew, CodMoneda)
                        End If
                    End If
                End If

                If IdCheque <> "" Then
                    'ANULAR EL CHEKE
                    iResultadoEditCheke = eMovimientoCajaBanco.fEditarCheque(8, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, "", sMonCodigo, gEmpresa)
                    ''Dim IdCheque As String = ""
                    Dim IdChequera As String = ""
                    ''IdCheque = IdChequeBD
                    IdChequera = IdChequeraBD
                    Dim dtChequesxChequera As DataTable
                    dtChequesxChequera = New DataTable
                    dtChequesxChequera = eGastosGenerales.fListarCantidadChequesxChequeraEmpr(IdChequera, gEmpresa)
                    If dtChequesxChequera.Rows.Count > 0 Then
                        Dim CantidadChe As Integer ' = 0
                        CantidadChe = Trim(dtChequesxChequera.Rows(0).Item("Contador"))
                        If CantidadChe = 0 Then
                            'CAMBIAR ESTADO CHEQUERA
                            iResultadoEditCheke = eMovimientoCajaBanco.fEditarChequera(35, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), IdChequera, Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, "", Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, "", sMonCodigo, gEmpresa)
                        ElseIf CantidadChe > 0 Then
                            iResultadoEditCheke = eMovimientoCajaBanco.fEditarChequera(36, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), IdChequera, Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, "", sMonCodigo, gEmpresa)
                        End If
                    End If

                    'SI TIENE LIBRO ORIGEN
                    If Trim(sIdCodigoCabLibro) <> "" And Trim(sIdCodigoDetLibro) <> "" And gEmpresa <> "" Then
                        Dim Cobrados As Double = 0
                        Dim NoCobrados As Double = 0
                        eLibroBancos = New clsLibroBancos
                        Dim iResultadoActDetalle As Int16 = 0
                        iResultadoActDetalle = eLibroBancos.fActDet2(gEmpresa, Trim(sIdCodigoDetLibro), Trim(sIdCodigoCabLibro), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), IdTipoMovimiento, Convert.ToDouble(txtMonto.Text), 0, 0, 0)
                        ''''''''''''''''''QUITAR ITF''''''''''''''''''''''''''''''
                        'If (Trim(cboTipoPago.SelectedValue) = "00005" Or Trim(cboTipoPago.SelectedValue) = "00007") And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 And Len(txtMontoAutomatico.Text.Trim) > 0 Then
                        'no graba por que es de cuenta a cuenta
                        'Else
                        If Trim(cboTipoPago.SelectedValue) = "00002" Then
                            Dim dtExisteItf As DataTable
                            dtExisteItf = New DataTable
                            dtExisteItf = eLibroBancos.TraerSiExisteItf(gEmpresa, Trim(sIdCodigoCabLibro), Trim(sIdCodigoCabLibro), Trim(sIdCodigoDetLibro))
                            Dim iResultadoEliminarItf As Integer = 0
                            Dim IdDetLibroItf As String = ""
                            Dim IdCabLibroItf As String = ""
                            If dtExisteItf.Rows.Count = 1 Then
                                IdDetLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroDet")) 'Saldo Inicial
                                IdCabLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroCab")) 'Saldo Inicial
                                'iResultadoEliminarItf = eLibroBancos.fActDet(gEmpresa, IdDetLibroItf, IdCabLibroItf, dtpFechaGasto.Value, "00011", "", TipoMovCadena, "", "H", Convert.ToDouble(ItfMontoSumado), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "")
                                iResultadoEliminarItf = eLibroBancos.fActDelete2(gEmpresa, Trim(IdDetLibroItf), Trim(IdCabLibroItf), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), "", Convert.ToDouble(txtMonto.Text), 0, 0, 0)
                            End If
                        End If
                        'End If
                        ''''''''''''''''''fin QUITAR itf''''''''''''''''''''''''''''''
                        'falta actualizar saldo de libro
                        '--------------
                        Dim dtDatos As DataTable
                        dtDatos = New DataTable
                        dtDatos = eLibroBancos.TraerIdLibro2(sIdCodigoCabLibro, gEmpresa)
                        Dim dSaldoInicial As Double = 0
                        If dtDatos.Rows.Count > 0 Then
                            dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                            Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                            NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                        End If
                        '--------------
                        Dim iResultadoActSaldoLibro As Int16 = 0
                        Dim dtLibroDet As DataTable
                        dtLibroDet = New DataTable
                        Dim saldo As Double = 0
                        saldo = dSaldoInicial

                        dtLibroDet = eLibroBancos.TraerLibroDet(37, Trim(sIdCodigoCabLibro), gEmpresa)
                        If dtLibroDet.Rows.Count > 0 Then
                            For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                                For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                    If z = 11 Then 'lee el importe
                                        If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                            saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sIdCodigoCabLibro), saldo)
                                        ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                            saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                            Dim iResultadoActDetalleLibro As Int16 = 0
                                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sIdCodigoCabLibro), saldo)
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                            '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                            'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                            '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                            'End If
                                        End If
                                    End If
                                Next
                            Next
                            Dim dtDtFech As DataTable
                            dtDtFech = New DataTable
                            Dim Fecha As DateTime
                            Dim FechaActCabecera As DateTime
                            dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sIdCodigoCabLibro)
                            If dtDtFech.Rows.Count > 0 Then
                                Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                            End If
                            If dtpFechaGasto.Value > Fecha Then
                                FechaActCabecera = dtpFechaGasto.Value
                            Else
                                FechaActCabecera = Fecha
                            End If

                            'Cobrados = Cobrados + Convert.ToDouble(Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells(11).Value))
                            'ElseIf Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Column7").Value) = "00002" And Trim(dgvLibroDet.Rows(dgvLibroDet.CurrentRow.Index).Cells("Marca1").Value) = "1" Then
                            NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))

                            iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sIdCodigoCabLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                            '----------------------------------------
                            'saber si existe libros proximos al mes actual de la cuenta
                            Dim dtDatosLibro As DataTable
                            dtDatosLibro = New DataTable
                            dtDatosLibro = eLibroBancos.TraerIdLibro2(sIdCodigoCabLibro, gEmpresa)
                            Dim sIdCuenta As String = ""
                            If dtDatosLibro.Rows.Count > 0 Then
                                sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                            End If
                            Dim dtLibrosSuperiores As DataTable
                            dtLibrosSuperiores = New DataTable
                            Dim A�o As String = dtpFechaGasto.Value.Year
                            Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                            dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                            If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                    Dim iResultadoActLibroSup As Int16 = 0
                                    Dim CodigoLibroBD As String = ""
                                    Dim SaldoInicialBD As Double ' = 0
                                    Dim SaldoFinalBD As Double '= 0
                                    Dim ImporteOculto As Double = 0
                                    If dgvListado.Rows.Count > 0 Then
                                        ImporteOculto = Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                                    End If
                                    CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    SaldoInicialBD = Format(saldo, "0.00")
                                    SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto) '- Convert.ToDouble(txtMonto.Text)
                                    'graba saldos de uno de los libro proximos
                                    '''''''''''''''''''''''''''''''''''''''''
                                    Dim SaldoPrendaS As Double
                                    Dim SaldoGarantiaS As Double
                                    Dim SaldoDepPlazS As Double
                                    Dim SaldoRetencionS As Double
                                    SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                    SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                    SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                    SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                    '''''''''''''''''''''''''''''''''''''''''
                                    iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                    saldo = SaldoFinalBD
                                Next
                            End If
                            '----------------------------------------
                        End If
                    End If 'SI TIENE LIBRO ORIGEN
                End If 'If IdCheque <> "" Then
            Else
                'NO ANULA
                Anulado = 0
                If IdCheque <> "" Then 'NO SE ANULA 'SI ES CHEKE EDITA INFORMACION DEL CHEKE
                    iResultadoEditCheke = eMovimientoCajaBanco.fEditarCheque(5, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, "", sMonCodigo, gEmpresa)
                End If
                'SI TIENE LIBRO ORIGEN
                If Trim(sIdCodigoCabLibro) <> "" And Trim(sIdCodigoDetLibro) <> "" And gEmpresa <> "" Then
                    eLibroBancos = New clsLibroBancos
                    Dim iResultadoActDetalle As Int16 = 0
                    iResultadoActDetalle = eLibroBancos.fActDet3(gEmpresa, Trim(sIdCodigoDetLibro), Trim(sIdCodigoCabLibro), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), IdTipoMovimiento, Convert.ToDouble(txtMonto.Text), 0, 0, 0)
                    ''''''''''''''''''genera itf''''''''''''''''''''''''''''''
                    If (Trim(cboTipoPago.SelectedValue) = "00005" Or Trim(cboTipoPago.SelectedValue) = "00007") And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 And Len(txtMontoAutomatico.Text.Trim) > 0 Then
                        '    'no graba por que es de cuenta a cuenta
                    Else
                        If Trim(cboTipoPago.SelectedValue) <> "00002" Then
                            '        Dim dtExisteItf As DataTable
                            '        dtExisteItf = New DataTable
                            '        dtExisteItf = eLibroBancos.TraerSiExisteItf(gEmpresa, sIdCodigoCabLibro, sIdCodigoCabLibro, sIdCodigoDetLibro)
                            '        Dim TipoMovCadena As String = ""
                            '        Dim sCodigoDetLibroItf As String = ""
                            '        Dim ItfMontoSumado As Double = 0
                            '        Dim iResultadoDetGrabaItf As Integer = 0
                            '        Dim IdDetLibroItf As String = ""
                            '        Dim IdCabLibroItf As String = ""
                            '        Dim dtDoble As DataTable
                            '        dtDoble = New DataTable
                            '        eITF = New clsITF
                            '        dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpFechaGasto.Value)
                            '        If dtDoble.Rows.Count > 0 Then
                            '            PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
                            '        End If
                            '        'ItfMontoSumado = (Val(Convert.ToDouble(txtMonto.Text)) * PorcentajeITF) / 100
                            '        Dim MontoParaItf As Double = 0
                            '        MontoParaItf = ReturnMontoParaCalcItf(txtMonto.Text)
                            '        ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100

                            '        ItfMontoSumado = Format(ItfMontoSumado, "0.00")
                            '        If Trim(cboTipoPago.SelectedValue.ToString) = "00005" Then
                            '            TipoMovCadena = "ITF de TRANSF. # " & Trim(txtMontoAutomatico.Text)
                            '        ElseIf Trim(cboTipoPago.SelectedValue.ToString) = "00006" Then
                            '            TipoMovCadena = "ITF " & Trim(txtMontoAutomatico.Text)
                            '        ElseIf Trim(cboTipoPago.SelectedValue.ToString) = "00007" Then
                            '            TipoMovCadena = "ITF de CARTA # " & Trim(txtMontoAutomatico.Text)
                            '        End If
                            '        If dtExisteItf.Rows.Count = 0 Then
                            '            eLibroBancos = New clsLibroBancos
                            '            eLibroBancos.fCodigoDet(gEmpresa, sIdCodigoCabLibro)
                            '            sCodigoDetLibroItf = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro actual
                            '            iResultadoDetGrabaItf = eLibroBancos.fGrabarDet(gEmpresa, sCodigoDetLibroItf, sIdCodigoCabLibro, dtpFechaGasto.Value, "00011", "", TipoMovCadena, "", "H", Convert.ToDouble(ItfMontoSumado), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", Trim(sIdCodigoCabLibro), Trim(sIdCodigoDetLibro))
                            '        ElseIf dtExisteItf.Rows.Count = 1 Then
                            '            IdDetLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroDet")) 'Saldo Inicial
                            '            IdCabLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroCab")) 'Saldo Inicial
                            '            iResultadoDetGrabaItf = eLibroBancos.fActDet(gEmpresa, IdDetLibroItf, IdCabLibroItf, dtpFechaGasto.Value, "00011", "", TipoMovCadena, "", "H", Convert.ToDouble(ItfMontoSumado), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "")
                            '        End If
                        Else
                            Dim dtExisteItf As DataTable
                            dtExisteItf = New DataTable
                            dtExisteItf = eLibroBancos.TraerSiExisteItf(gEmpresa, sIdCodigoCabLibro, sIdCodigoCabLibro, sIdCodigoDetLibro)
                            Dim TipoMovCadena As String = ""
                            Dim sCodigoDetLibroItf As String = ""
                            Dim ItfMontoSumado As Double = 0
                            Dim iResultadoDetGrabaItf As Integer = 0
                            Dim IdDetLibroItf As String = ""
                            Dim IdCabLibroItf As String = ""
                            Dim FechaIdDetLibroItf As DateTime
                            Dim dtDoble As DataTable
                            dtDoble = New DataTable
                            eITF = New clsITF
                            dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), dtpFechaGasto.Value)
                            If dtDoble.Rows.Count > 0 Then
                                PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
                            End If
                            'ItfMontoSumado = (Val(Convert.ToDouble(txtMonto.Text)) * PorcentajeITF) / 100
                            Dim MontoParaItf As Double = 0
                            MontoParaItf = ReturnMontoParaCalcItf(txtMonto.Text)
                            ItfMontoSumado = (MontoParaItf * PorcentajeITF) / 100
                            ItfMontoSumado = Format(ItfMontoSumado, "0.00")
                            TipoMovCadena = "ITF de CHEQUE # " & Trim(txtMontoAutomatico.Text)
                            'End If
                            If dtExisteItf.Rows.Count = 1 Then
                                IdDetLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroDet")) 'Saldo Inicial
                                IdCabLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroCab")) 'Saldo Inicial
                                FechaIdDetLibroItf = Trim(dtExisteItf.Rows(0).Item("Fecha"))
                                iResultadoDetGrabaItf = eLibroBancos.fActDet(gEmpresa, IdDetLibroItf, IdCabLibroItf, FechaIdDetLibroItf, "00011", "", TipoMovCadena, "", "H", Convert.ToDouble(ItfMontoSumado), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "")
                            End If
                        End If
                    End If
                    ''''''''''''''''''fin genera itf''''''''''''''''''''''''''''''
                    'falta actualizar saldo de libro
                    '--------------
                    Dim Cobrados As Double = 0
                    Dim NoCobrados As Double = 0
                    Dim dtDatos As DataTable
                    dtDatos = New DataTable
                    dtDatos = eLibroBancos.TraerIdLibro2(sIdCodigoCabLibro, gEmpresa)
                    Dim dSaldoInicial As Double = 0
                    If dtDatos.Rows.Count > 0 Then
                        dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                        Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                        NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                    End If
                    '--------------
                    Dim iResultadoActSaldoLibro As Int16 = 0
                    Dim dtLibroDet As DataTable
                    dtLibroDet = New DataTable
                    Dim saldo As Double = 0
                    saldo = dSaldoInicial

                    dtLibroDet = eLibroBancos.TraerLibroDet(37, Trim(sIdCodigoCabLibro), gEmpresa)
                    If dtLibroDet.Rows.Count > 0 Then
                        For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                            For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                                If z = 11 Then 'lee el importe
                                    If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                        saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                        Dim iResultadoActDetalleLibro As Int16 = 0
                                        iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sIdCodigoCabLibro), saldo)
                                    ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                        saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                        Dim iResultadoActDetalleLibro As Int16 = 0
                                        iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sIdCodigoCabLibro), saldo)
                                        'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                        '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                        'End If
                                        'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                        '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                        'End If
                                    End If
                                End If
                            Next
                        Next
                        Dim dtDtFech As DataTable
                        dtDtFech = New DataTable
                        Dim Fecha As DateTime
                        Dim FechaActCabecera As DateTime
                        dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sIdCodigoCabLibro)
                        If dtDtFech.Rows.Count > 0 Then
                            Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                        End If
                        If dtpFechaGasto.Value > Fecha Then
                            FechaActCabecera = dtpFechaGasto.Value
                        Else
                            FechaActCabecera = Fecha
                        End If
                        NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                        NoCobrados = NoCobrados + Convert.ToDouble(txtMonto.Text)
                        iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sIdCodigoCabLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                        '----------------------------------------
                        'saber si existe libros proximos al mes actual de la cuenta
                        Dim dtDatosLibro As DataTable
                        dtDatosLibro = New DataTable
                        dtDatosLibro = eLibroBancos.TraerIdLibro2(sIdCodigoCabLibro, gEmpresa)
                        Dim sIdCuenta As String = ""
                        If dtDatosLibro.Rows.Count > 0 Then
                            sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                        End If
                        Dim dtLibrosSuperiores As DataTable
                        dtLibrosSuperiores = New DataTable
                        Dim A�o As String = dtpFechaGasto.Value.Year
                        Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                        dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                        If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                            For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                Dim iResultadoActLibroSup As Int16 = 0
                                Dim CodigoLibroBD As String = ""
                                Dim SaldoInicialBD As Double ' = 0
                                Dim SaldoFinalBD As Double '= 0
                                Dim ImporteOculto As Double = 0
                                If dgvListado.Rows.Count > 0 Then
                                    ImporteOculto = Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                                End If
                                CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                SaldoInicialBD = Format(saldo, "0.00")
                                Dim MontoNuevo As Double = Format(Convert.ToDouble(txtMonto.Text), "0.00")
                                If MontoNuevo = ImporteOculto Then
                                    SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                Else
                                    SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                                End If
                                'graba saldos de uno de los libro proximos
                                '''''''''''''''''''''''''''''''''''''''''
                                Dim SaldoPrendaS As Double
                                Dim SaldoGarantiaS As Double
                                Dim SaldoDepPlazS As Double
                                Dim SaldoRetencionS As Double
                                SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                '''''''''''''''''''''''''''''''''''''''''
                                iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                saldo = SaldoFinalBD
                            Next
                        End If
                        '----------------------------------------
                    End If
                End If 'SI TIENE LIBRO ORIGEN
                End If
                'SI TIENE LIBRO DE DESTINO (TRANSACCION)
            If Trim(CodigoLibroDestinoBD) <> "" And Trim(CodigoDetDestinoBD) <> "" Then 'And gEmpresa <> "" Then
                eLibroBancos = New clsLibroBancos
                Dim iResultadoActDetalle As Int16 = 0
                iResultadoActDetalle = eLibroBancos.fActDet3(cboEmpresasLibros.SelectedValue, Trim(CodigoDetDestinoBD), Trim(CodigoLibroDestinoBD), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtReferencia.Text), "", "D", Convert.ToDouble(txtMonto.Text), 0, 0, 0)
                'actualizar saldo de libro
                '--------------
                Dim dtDatos As DataTable
                dtDatos = New DataTable
                dtDatos = eLibroBancos.TraerIdLibro2(CodigoLibroDestinoBD, cboEmpresasLibros.SelectedValue)
                Dim dSaldoInicial As Double = 0
                Dim Cobrados As Double = 0
                Dim NoCobrados As Double = 0
                If dtDatos.Rows.Count > 0 Then
                    dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                    Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                    NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                End If
                '--------------
                Dim iResultadoActSaldoLibro As Int16 = 0
                Dim dtLibroDet As DataTable
                dtLibroDet = New DataTable
                Dim saldo As Double = 0
                saldo = dSaldoInicial

                dtLibroDet = eLibroBancos.TraerLibroDet(37, Trim(CodigoLibroDestinoBD), cboEmpresasLibros.SelectedValue)
                If dtLibroDet.Rows.Count > 0 Then
                    For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                        For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                            If z = 11 Then 'lee el importe
                                If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                    saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(cboEmpresasLibros.SelectedValue, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(CodigoLibroDestinoBD), saldo)
                                ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                    saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(cboEmpresasLibros.SelectedValue, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(CodigoLibroDestinoBD), saldo)
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                    '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    'End If
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    'End If
                                End If
                            End If
                        Next
                    Next
                    Dim dtDtFech As DataTable
                    dtDtFech = New DataTable
                    Dim Fecha As DateTime
                    Dim FechaActCabecera As DateTime
                    dtDtFech = eLibroBancos.TraerMaxFechadeLibro(cboEmpresasLibros.SelectedValue, CodigoLibroDestinoBD)
                    If dtDtFech.Rows.Count > 0 Then
                        Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                    End If
                    If dtpFechaGasto.Value > Fecha Then
                        FechaActCabecera = dtpFechaGasto.Value
                    Else
                        FechaActCabecera = Fecha
                    End If
                    NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                    NoCobrados = NoCobrados + Convert.ToDouble(txtMonto.Text)

                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(cboEmpresasLibros.SelectedValue, CodigoLibroDestinoBD, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                    '----------------------------------------
                    'saber si existe libros proximos al mes actual de la cuenta
                    Dim dtDatosLibro As DataTable
                    dtDatosLibro = New DataTable
                    dtDatosLibro = eLibroBancos.TraerIdLibro2(CodigoLibroDestinoBD, cboEmpresasLibros.SelectedValue)
                    Dim sIdCuenta As String = ""
                    If dtDatosLibro.Rows.Count > 0 Then
                        sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                    End If
                    Dim dtLibrosSuperiores As DataTable
                    dtLibrosSuperiores = New DataTable
                    Dim A�o As String = dtpFechaGasto.Value.Year
                    Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                    dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, cboEmpresasLibros.SelectedValue, FechaActCabecera) 'trae los proximos libros
                    If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                        For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                            Dim iResultadoActLibroSup As Int16 = 0
                            Dim CodigoLibroBD As String = ""
                            Dim SaldoInicialBD As Double ' = 0
                            Dim SaldoFinalBD As Double '= 0
                            'CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            'SaldoInicialBD = Format(saldo, "0.00")
                            Dim ImporteOculto As Double = 0
                            If dgvListado.Rows.Count > 0 Then
                                ImporteOculto = Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                            End If
                            CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            SaldoInicialBD = Format(saldo, "0.00")
                            Dim MontoNuevo As Double = Format(Convert.ToDouble(txtMonto.Text), "0.00")
                            If MontoNuevo = ImporteOculto Then
                                SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) '- ImporteOculto) - Convert.ToDouble(txtMonto.Text)
                            Else
                                SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - ImporteOculto) + Convert.ToDouble(txtMonto.Text)
                            End If
                            'SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtMonto.Text)
                            'graba saldos de uno de los libro proximos
                            '''''''''''''''''''''''''''''''''''''''''
                            Dim SaldoPrendaS As Double
                            Dim SaldoGarantiaS As Double
                            Dim SaldoDepPlazS As Double
                            Dim SaldoRetencionS As Double
                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                            '''''''''''''''''''''''''''''''''''''''''
                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(cboEmpresasLibros.SelectedValue, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                            saldo = SaldoFinalBD
                        Next
                    End If
                    '----------------------------------------
                End If
            Else
                Dim A�o As String = dtpFechaGasto.Value.Year
                Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                Dim sCodigoLibroDestino As String = ""
                Dim sCodigoDetDestino As String = ""
                Dim MontoDest As Double = 0
                If (Trim(cboTipoPago.SelectedValue.ToString) = "00007" Or Trim(cboTipoPago.SelectedValue.ToString) = "00005") And cboEmpresasLibros.SelectedIndex > -1 And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 And Len(txtMontoAutomatico.Text.Trim) > 0 Then

                    If sMonCodigo = sMonCodigoDest Then
                        MontoDest = Convert.ToDouble(txtMonto.Text)
                    ElseIf sMonCodigo <> sMonCodigoDest Then
                        If sMonCodigo = "02" And sMonCodigoDest = "01" Then
                            MontoDest = Format(Convert.ToDouble(txtMonto.Text) * Val(txtTipoCambio.Text), "#,##0.00")
                        End If
                        If sMonCodigo = "01" And sMonCodigoDest = "02" Then
                            MontoDest = Format(Convert.ToDouble(txtMonto.Text) / Val(txtTipoCambio.Text), "#,##0.00")
                        End If
                        'MontoDest = Convert.ToDouble(txtMonto.Text)
                    End If

                    '**************LIBRO BANCOS CUENTA DESTINO*************************************************************
                    '******************************************************************************************************
                    eLibroBancos = New clsLibroBancos
                    Dim dtLibroDestino As DataTable
                    dtLibroDestino = New DataTable
                    'Dim IdLibroDestino As String = ""
                    Dim A�oDestino As String = dtpFechaGasto.Value.Year
                    Dim MesDestino As String = Format(dtpFechaGasto.Value.Month, "00")
                    dtLibroDestino = eLibroBancos.TraerIdLibro(A�oDestino, MesDestino, Trim(cboCuentaPrestada.SelectedValue), Trim(cboEmpresasLibros.SelectedValue))
                    If dtLibroDestino.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO EN LA CUENTA DESTINO
                        'sCodigoLibro = Trim(dtDatos.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                        Dim iResultadoDetDestino As Int16 = 0
                        Dim iResultadoActSaldoLibroDestino As Int16 = 0
                        sCodigoLibroDestino = Trim(dtLibroDestino.Rows(0).Item("IdLibroCab")) 'toma codigo de libro
                        Dim dSaldoInicialDes As Double = 0
                        Dim dSaldoFinalDes As Double = 0
                        Dim dCheCobradosNODes As Double = 0
                        Dim dCheCobradosSIDes As Double = 0
                        dSaldoInicialDes = Trim(dtLibroDestino.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                        dSaldoFinalDes = Trim(dtLibroDestino.Rows(0).Item("Saldo")) 'Saldo Final
                        dCheCobradosNODes = Trim(dtLibroDestino.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                        dCheCobradosSIDes = Trim(dtLibroDestino.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                        'Agregar el detalle de libro Actual, Agregando un nuevo registro
                        eLibroBancos = New clsLibroBancos
                        eLibroBancos.fCodigoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino.Trim)
                        sCodigoDetDestino = eLibroBancos.sCodFuturoDet 'genera codigo_detalle del libro creado
                        CodigoLibroDestinoBD = sCodigoLibroDestino
                        CodigoDetDestinoBD = sCodigoDetDestino
                        Dim Cobrados As Double = 0
                        Dim NoCobrados As Double = 0
                        Cobrados = dCheCobradosSIDes
                        NoCobrados = dCheCobradosNODes
                        'graba detalle

                        'If sMonCodigo = sMonCodigoDest Then
                        '    MontoDest = Convert.ToDouble(txtMonto.Text)
                        'ElseIf sMonCodigo <> sMonCodigoDest Then
                        '    If sMonCodigo = "02" And sMonCodigoDest = "01" Then
                        '        MontoDest = Format(Convert.ToDouble(txtMonto.Text) * Val(txtTipoCambio.Text), "#,##0.00")
                        '    End If
                        '    If sMonCodigo = "01" And sMonCodigoDest = "02" Then
                        '        MontoDest = Format(Convert.ToDouble(txtMonto.Text) / Val(txtTipoCambio.Text), "#,##0.00")
                        '    End If
                        '    'MontoDest = Convert.ToDouble(txtMonto.Text)
                        'End If

                        iResultadoDetDestino = eLibroBancos.fGrabarDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, dtpFechaGasto.Value, "00012", "", Trim(txtReferencia.Text), "", "D", Convert.ToDouble(MontoDest), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                        Dim saldo As Double = 0
                        saldo = dSaldoFinalDes + Convert.ToDouble(MontoDest)
                        'actualiza el nuevo libro
                        If iResultadoDetDestino = 1 Then
                            Dim iResultadoActDetalleLibro As Int16 = 0
                            iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, saldo)
                            Dim dtDtFech As DataTable
                            dtDtFech = New DataTable
                            Dim Fecha As DateTime
                            Dim FechaActCabecera As DateTime
                            dtDtFech = eLibroBancos.TraerMaxFechadeLibro(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino)
                            If dtDtFech.Rows.Count > 0 Then
                                Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                            End If
                            If dtpFechaGasto.Value > Fecha Then
                                FechaActCabecera = dtpFechaGasto.Value
                            Else
                                FechaActCabecera = Fecha
                            End If
                            iResultadoActSaldoLibroDestino = eLibroBancos.fActSaldoLibro(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino, Format(dSaldoInicialDes, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                            'saber si existe libros proximos al mes actual de la cuenta
                            Dim dtDatosLibro As DataTable
                            dtDatosLibro = New DataTable
                            dtDatosLibro = eLibroBancos.TraerIdLibro2(sCodigoLibroDestino, Trim(cboEmpresasLibros.SelectedValue))
                            Dim sIdCuenta As String = ""
                            If dtDatosLibro.Rows.Count > 0 Then
                                sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                            End If
                            Dim dtLibrosSuperiores As DataTable
                            dtLibrosSuperiores = New DataTable
                            dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, Trim(cboEmpresasLibros.SelectedValue), FechaActCabecera) 'trae los proximos libros
                            If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                                For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                                    Dim iResultadoActLibroSup As Int16 = 0
                                    Dim CodigoLibroBD As String = ""
                                    Dim SaldoInicialBD As Double ' = 0
                                    Dim SaldoFinalBD As Double '= 0
                                    CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                                    SaldoInicialBD = Format(saldo, "0.00")
                                    SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(MontoDest)
                                    'graba saldos de uno de los libro proximos
                                    '''''''''''''''''''''''''''''''''''''''''
                                    Dim SaldoPrendaS As Double
                                    Dim SaldoGarantiaS As Double
                                    Dim SaldoDepPlazS As Double
                                    Dim SaldoRetencionS As Double
                                    SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                                    SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                                    SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                                    SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                                    ''''''''''''''''''''''''''''''''''''''''
                                    iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(Trim(cboEmpresasLibros.SelectedValue), CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                                    saldo = SaldoFinalBD
                                Next
                            End If
                        End If
                    ElseIf dtLibroDestino.Rows.Count = 0 Then ' NO EXISTE LIBRO DEL MES DE PAGO
                        Dim MesAnt As String = ""
                        Dim Anio As String = ""
                        MesAnt = Format(MesDestino - 1, "00") 'resto el mes actual, para usar el mes anterior
                        Anio = Trim(dtpFechaGasto.Value.Year)
                        If Trim(MesDestino) = "01" Then 'si el mes es ENERO mi mes anterior sera diciembre y mi a�o el actual -1
                            MesAnt = "12"
                            Anio = Trim(A�o) - 1
                        End If
                        Dim dtLibroAnterior2 As DataTable
                        dtLibroAnterior2 = New DataTable
                        dtLibroAnterior2 = eLibroBancos.TraerIdLibro(Anio, MesAnt, Trim(cboCuentaPrestada.SelectedValue), Trim(cboEmpresasLibros.SelectedValue))
                        Dim iResultado As Int16 = 0
                        Dim iResultadoDet2 As Int16 = 0
                        Dim SaldoAnteriorDes As Double = 0
                        If dtLibroAnterior2.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                            eLibroBancos = New clsLibroBancos
                            eLibroBancos.fCodigo(Trim(cboEmpresasLibros.SelectedValue))
                            sCodigoLibroDestino = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                            CodigoLibroDestinoBD = sCodigoLibroDestino
                            'CodigoDetDestinoBD = sCodigoDetDestino
                            'graba la cabecera de libro
                            'saldo inicial = saldo final del libro anterior
                            SaldoAnteriorDes = Trim(dtLibroAnterior2.Rows(0).Item("Saldo"))
                            Dim SaldoPrenda As Double = 0
                            Dim SaldoGarantia As Double = 0
                            Dim SaldoDepPlaz As Double = 0
                            Dim SaldoRetencion As Double = 0
                            Dim SaldoIniExtracto As Double = 0
                            SaldoPrenda = Trim(dtLibroAnterior2.Rows(0).Item("SaldoPrenda"))
                            SaldoGarantia = Trim(dtLibroAnterior2.Rows(0).Item("SaldoGarantia"))
                            SaldoDepPlaz = Trim(dtLibroAnterior2.Rows(0).Item("SaldoDepPlaz"))
                            SaldoRetencion = Trim(dtLibroAnterior2.Rows(0).Item("SaldoRetencion"))
                            SaldoIniExtracto = Trim(dtLibroAnterior2.Rows(0).Item("SaldoFinExtracto"))

                            'SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibroDestino, Trim(cboEmpresasLibros.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), MesDestino, A�oDestino, SaldoAnteriorDes, Convert.ToDouble(MontoDest), dtpFechaGasto.Value, SaldoPrenda, SaldoGarantia, SaldoDepPlaz, SaldoRetencion, SaldoIniExtracto) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        ElseIf dtLibroAnterior2.Rows.Count = 0 Then 'NO EXISTE LIBRO DEL MES ANTERIOR  'Graba Cabecera de Libro y 1er Detalle
                            eLibroBancos = New clsLibroBancos
                            eLibroBancos.fCodigo(Trim(cboEmpresasLibros.SelectedValue))
                            sCodigoLibroDestino = eLibroBancos.sCodFuturo 'genera codigo del nuevo libro
                            CodigoLibroDestinoBD = sCodigoLibroDestino
                            'CodigoDetDestinoBD = sCodigoDetDestino
                            'graba la cabecera de libro
                            'saldo anterior =0
                            SaldoAnteriorDes = 0
                            iResultado = eLibroBancos.fGrabar(4, sCodigoLibroDestino, Trim(cboEmpresasLibros.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), MesDestino, A�oDestino, SaldoAnteriorDes, Convert.ToDouble(MontoDest), dtpFechaGasto.Value, 0, 0, 0, 0, 0) 'saldo es el primer importe y 0 para cheques cobrados y no cobrados
                        End If
                        If iResultado > 0 And iResultado <> 2627 Then
                            eLibroBancos = New clsLibroBancos
                            eLibroBancos.fCodigoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino)
                            sCodigoDetDestino = eLibroBancos.sCodFuturoDet
                            'CodigoLibroDestinoBD = sCodigoLibroDestino
                            CodigoDetDestinoBD = sCodigoDetDestino
                            Dim Cobrados As Double = 0
                            Dim NoCobrados As Double = 0
                            'graba detalle del 1er registro
                            iResultadoDet2 = eLibroBancos.fGrabarDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, dtpFechaGasto.Value, "00012", "", Trim(txtReferencia.Text), "", "D", Convert.ToDouble(MontoDest), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "", "", "")
                            Dim saldo As Double = 0
                            saldo = SaldoAnteriorDes + Convert.ToDouble(MontoDest)
                            'actualiza el nuevo libro
                            If iResultadoDet2 = 1 Then
                                Dim iResultadoActDetalleLibro As Int16 = 0
                                iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(Trim(cboEmpresasLibros.SelectedValue), sCodigoDetDestino, sCodigoLibroDestino, saldo)
                                Dim iResultadoActSaldoLibro As Int16 = 0
                                iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(Trim(cboEmpresasLibros.SelectedValue), sCodigoLibroDestino, Format(SaldoAnteriorDes, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), dtpFechaGasto.Value) 'graba el nuevo saldo del libro
                            End If
                        End If
                    End If
                    If Trim(IdTransaccionBD) = "" Then
                        'PARA TRANSACCIONES
                        Dim IdTransaccion As String = ""
                        Dim IdBancoDestino As String = ""
                        Dim IdCuentaDestino As String = ""
                        Dim GlosaDestino As String = ""
                        If Trim(cboTipoPago.SelectedValue.ToString) = "00005" And Len(txtMontoAutomatico.Text.Trim) > 0 Then 'And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 Then
                            Dim iResultadoGrabaTrans As Integer = 0
                            eGastosGenerales.fIdTransaccion(gEmpresa)
                            IdTransaccion = Trim(eGastosGenerales.sCodTransId)
                            IdTransaccionBD = IdTransaccion
                            iResultadoGrabaTrans = eMovimientoCajaBanco.fGrabarTransaccion(37, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtReferencia.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, Trim(gUsuario), IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, "", "", "", "", "", Convert.ToInt64(txtMontoAutomatico.Text), IdTransaccion, Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), gPeriodo)
                        End If
                    End If
                    If Trim(IdCartaBD) = "" Then
                        'PARA CARTAS
                        Dim IdCarta As String = ""
                        If Trim(cboTipoPago.SelectedValue.ToString) = "00007" And Len(txtMontoAutomatico.Text.Trim) > 0 Then ' And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 Then
                            Dim iResultadoGrabaTrans As Integer = 0
                            eGastosGenerales.fIdCarta(gEmpresa)
                            IdCarta = Trim(eGastosGenerales.sCodTransId)
                            IdCartaBD = IdCarta
                            iResultadoGrabaTrans = eMovimientoCajaBanco.fGrabarTransaccion(39, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtReferencia.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, Trim(gUsuario), IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, "", "", "", "", "", Convert.ToInt64(txtMontoAutomatico.Text), IdCarta, Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), gPeriodo)
                        End If
                    End If
                    '************** FIN LIBRO BANCOS CUENTA DESTINO *******************************************************
                    '******************************************************************************************************
                End If
            End If 'SI TIENE LIBRO DE DESTINO (TRANSACCION)

                If Trim(IdTransaccionBD) <> "" Then
                    'PARA TRANSACCIONES
                    Dim IdTransaccion As String = ""
                    Dim IdBancoDestino As String = ""
                    Dim IdCuentaDestino As String = ""
                    Dim GlosaDestino As String = ""
                    If Trim(cboTipoPago.SelectedValue.ToString) = "00005" And Len(txtMontoAutomatico.Text.Trim) > 0 Then 'And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 Then
                        Dim iResultadoGrabaTrans As Integer = 0
                        iResultadoGrabaTrans = eMovimientoCajaBanco.fGrabarTransaccion(40, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtReferencia.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, Trim(gUsuario), IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, "", "", "", "", "", Convert.ToInt64(txtMontoAutomatico.Text), Trim(IdTransaccionBD), Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), gPeriodo)
                    End If
                End If
                If Trim(IdCartaBD) <> "" Then
                    'PARA CARTAS
                    Dim IdCarta As String = ""
                    If Trim(cboTipoPago.SelectedValue.ToString) = "00007" And Len(txtMontoAutomatico.Text.Trim) > 0 Then 'And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 Then
                        Dim iResultadoGrabaTrans As Integer = 0
                        iResultadoGrabaTrans = eMovimientoCajaBanco.fGrabarTransaccion(41, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtReferencia.Text), Trim(cboTipoGasto.SelectedValue), NroVoucher, Convert.ToDouble(txtTipoCambio.Text), IdCheque, Trim(gUsuario), IdRendicion, "", "", 0, 0, IdSesion, sMonCodigo, gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumen, Trim(txtRuc.Text), Val(NroSesion), IdArea, IdCaja, "", "", "", "", "", Convert.ToInt64(txtMontoAutomatico.Text), IdCartaBD, Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), gPeriodo)
                    End If
                End If
                Dim Prestamo As String = ""
                Dim EmprPrestamo As String = ""
                Dim CCPrestamo As String = ""
                Dim iEstado2 As Integer = 0
                If rdbEmpresas.Checked = True Then
                    Prestamo = "2"
                    EmprPrestamo = ""
                    CCPrestamo = ""
                    iEstado2 = 0
                End If
                If rdbUnaEmpresa.Checked = True Then
                    Prestamo = "1"
                    EmprPrestamo = Trim(cboEmpresas.SelectedValue)
                    CCPrestamo = Trim(cboCentrodeCostos.SelectedValue)
                    iEstado2 = 1
                End If
                'If cboEmpresasLibros.SelectedIndex > -1 Then
                '    EmprPrestamo = Trim(cboEmpresasLibros.SelectedValue)
                'End If
                If CheckBox1.Checked = True Then
                    IdRetencionBD = ""
                    SerieBD = ""
                    ConRetencionBD = "0"
                    TotRetenidoBD = 0
                End If

                Dim CodigoDetModificar As String = Trim(txtCodigo.Text)
                Dim MontoNew As String = Trim(txtMonto.Text)
                'Dim Monto As Double = 0
                'Monto = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value
            iResultadoGrabaGastos = eMovimientoCajaBanco.fGrabar(2, Trim(dtpFechaGasto.Value), Trim(txtCodigo.Text), "", "", Trim(cboCentroCosto.SelectedValue), "", "", IdTipoMovimiento, "", Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, "", IdRendicion, "", "", 0, 0, "", "", gEmpresa, Trim(txtMontoAutomatico.Text), Anulado, IdResumenBD, Trim(txtRuc.Text), 0, "", "", "", "", Prestamo, EmprPrestamo, CCPrestamo, iEstado2, Trim(IdTransaccionBD), Trim(cbpBancoPrestado.SelectedValue), Trim(cboCuentaPrestada.SelectedValue), Trim(txtReferencia.Text), CodigoLibroDestinoBD, CodigoDetDestinoBD, gPeriodo, IdCartaBD, gUsuario, IdLetra, Trim(Mov), IdRetencionBD, SerieBD, ConRetencionBD, TotRetenidoBD, RetEmprCodigoBD, ID_DocPendiente)

                If iResultadoGrabaGastos = 1 Then
                    cSegurosPolizas = New clsSegurosPolizas
                    If Len(Trim(sEndoso)) = 0 Then
                        sEndoso = sPoliza
                    End If
                    cSegurosPolizas.fUpdateEstadoEgresos(gEmpresa, sPoliza, sNroCuota, "C", gUsuario, Trim(txtCodigo.Text), Trim(sEndoso))
                End If

                Dim dtPrimerDet As DataTable
                dtPrimerDet = New DataTable
                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                dtPrimerDet = eMovimientoCajaBanco.fListarPrimerDetalle(Trim(txtCodigo.Text), gEmpresa)
                If dtPrimerDet.Rows.Count > 0 Then
                    Dim CodPrimerDet As String = ""
                    Dim iActualizarDetalle As Integer = 0
                    CodPrimerDet = dtPrimerDet.Rows(0).Item("IdDetalle")
                    iActualizarDetalle = eMovimientoCajaBanco.fGrabarDetalle(33, Today(), Trim(txtCodigo.Text), "", Trim(CodPrimerDet), Trim(cboCentroCosto.SelectedValue), "", "", "", "", Convert.ToDouble(txtMonto.Text), "", Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), "", 0, "", "", "", "", 0, 0, "", "", gEmpresa, "", 0, "", "", 0, "", "", "", "", EmprPrestamo, iEstado2, CCPrestamo)
                End If
                If Anulado = 1 Then
                    Dim iResEliminarDetalles As Integer = 0
                    iResEliminarDetalles = eMovimientoCajaBanco.fEliminarItemDetalles(Trim(txtCodigo.Text), gEmpresa)
                End If
                If iResultadoGrabaGastos > 0 Then
                    txtCodigo.Text = Trim(IdMovimiento)
                    mMostrarGrilla()
                    If dgvListado.Rows.Count > 0 Then
                        LimpiarControles()
                        txtCodigo.Clear()
                        txtRuc.Clear()
                        txtNroVoucher.Clear()
                        CheckBox1.Checked = False
                    End If
                    BeButton4_Click(sender, e)
                End If

                If iResultadoGrabaGastos > 0 Then
                    MessageBox.Show("Proceso de Actualizaci�n Concluido!", "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If



                'If Format(Convert.ToDouble(MontoNew), "#,##0.00") <> Format(Convert.ToDouble(Monto), "#,##0.00") Then

                Dim dtDocs As DataTable
                dtDocs = New DataTable
                eMovimientoCajaBanco = New clsMovimientoCajaBanco
                dtDocs = eMovimientoCajaBanco.fListarDocsMovimiento(49, CodigoDetModificar, gEmpresa)
                'If dtDocs.Rows.Count > 0 Then
                If Format(Convert.ToDouble(MontoNew), "#,##0.00") <> Format(Convert.ToDouble(dtDocs.Rows(0).Item("SumDetalle")), "#,##0.00") Then
                    Dim row As DataGridViewRow
                    For Each row In dgvListado.Rows
                        If (UCase(Trim(row.Cells("Column3").Value)) Like ("*" & UCase(Trim(CodigoDetModificar)) & "*")) Then
                            row.Selected = True
                            dgvListado.FirstDisplayedScrollingRowIndex = row.Index
                            'MostrarDetalles = "SI"
                            'dgvListado_SelectionChanged(sender, e)
                            If dgvListado.Rows.Count > 0 Then
                                Dim sIdMovimiento As String = ""
                                Dim sFormaDet As String = ""
                                Dim xEmpresaAPrestar As String = ""
                                Dim dblMonto As Double = 0
                                sIdMovimiento = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(row.Index).Cells("Column3").Value), "", dgvListado.Rows(row.Index).Cells("Column3").Value)
                                sFormaDet = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(row.Index).Cells("Column37").Value), "", dgvListado.Rows(row.Index).Cells("Column37").Value)
                                xEmpresaAPrestar = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(row.Index).Cells("Column38").Value), "", dgvListado.Rows(row.Index).Cells("Column38").Value)
                                dblMonto = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(row.Index).Cells("Column17").Value), 0, dgvListado.Rows(row.Index).Cells("Column17").Value)
                                If Trim(sIdMovimiento) <> "" Then
                                    Dim frm As New frmDetalleMovimiento
                                    frm.Owner = Me
                                    frm.sIdMovimiento = Trim(sIdMovimiento)
                                    frm.sFormaDetalle = Trim(sFormaDet)
                                    frm.sEmpresaAPrestar = Trim(xEmpresaAPrestar)
                                    frm.Monto = Trim(dblMonto)
                                    frm.ShowInTaskbar = False
                                    frm.ShowDialog()

                                End If

                            End If
                            Exit For
                        End If
                    Next
                End If
                'End If






        End If
    End Sub

    Private Sub BeButton6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton6.Click
        'If Len(Trim(txtRuc.Text)) = 11 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And CountDocumentos = 0 Then
        '    txtRucProv.Text = Trim(txtRuc.Text)
        '    txtBuscar.Text = Trim(txtRuc.Text)
        '    txtBuscar_TextChanged(sender, e)
        '    chkDocTodas_CheckedChanged(sender, e)
        '    Panel1.Visible = True
        '    'TabControl2.TabPages(0).Controls.Owner.Focus()
        '    TabControl2.TabPages(0).Focus()
        'ElseIf Len(Trim(txtRuc.Text)) = 11 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And CountDocumentos > 0 Then
        '    'txtRucProv.Text = Trim(txtRuc.Text)
        '    'txtBuscar.Text = Trim(txtRuc.Text)
        '    'txtBuscar_TextChanged(sender, e)
        '    'chkDocTodas_CheckedChanged(sender, e)
        '    Panel1.Visible = True
        '    'TabControl2.TabPages(0).Controls.Owner.Focus()
        '    'TabControl2.TabPages(0).Focus()
        If cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
            txtBuscarLetra_TextChanged(sender, e)
            Panel1.Visible = True
            TabControl2.TabPages(0).Select()
            txtBuscar_TextChanged(sender, e)
            txtRucProv.Focus()
            Panel7.Visible = False
        Else
            Panel1.Visible = False
            If cboBanco.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboBanco.Focus()
                Exit Sub
            End If
            If cboCuenta.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un N�mero de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCuenta.Focus()
                Exit Sub
            End If
        End If
        TabControl2.SelectedIndex = 0
    End Sub

    Private Sub cargarCentrosDeCosto()
        IdCC.Items.Clear()
        ePagoProveedores = New clsPagoProveedores
        Dim dtTable As DataTable
        dtTable = New DataTable
        dtTable = ePagoProveedores.fCargarCCosto(gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            'IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosDescripcion") & " - " & dtTable.Rows(i).Item("CCosCodigo")))
            IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosCodigo")) & " - " & dtTable.Rows(i).Item("CCosDescripcion"))
        Next
    End Sub

    Private Sub cargarCentrosDeCostoxEmpresa()
        For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
            'dgvDocumentos.Rows(y).Cells("IdCC").Value()
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(y).Cells("Column2")) = False Then
                Dim CodeEmpresa As String = ""
                CodeEmpresa = Trim(dgvDocumentos.Rows(y).Cells("Column2").Value)
                Dim my_strsql As String
                Dim d_objds As New DataSet()
                Dim dgrow As New DataGridViewComboBoxCell
                my_strsql = "SELECT CCosCodigo as 'Codigo',CCosCodigo+' - '+CCosDescripcion as 'Cadena' FROM Comun.CCosto WHERE EmprCodigo='" & CodeEmpresa & "' ORDER BY CCosDescripcion"
                Dim objda As New SqlDataAdapter(my_strsql, conexionParaGastosGene)
                objda.Fill(d_objds, "Codigo")
                objda.Fill(d_objds, "Cadena")
                'Finalmente gracias al datagridViewComboBoxCell almacenamos esta
                'consulta como si fuese en un comboBox Normal
                dgrow.DataSource = d_objds.Tables(0).DefaultView
                dgrow.DisplayMember = "Cadena"
                dgrow.ValueMember = "Codigo"
                Try
                    'Aqui viene lo bueno: Tanto como la celda de la columna Distrito y el 
                    'dgrow  son practicamente comboBoxes... entonces podemos pasarle los
                    'datos del uno al otro.
                    Me.dgvDocumentos.Item("IdCC", y) = dgrow
                    'Me.dgvDocumentos.Item("IdCC", Me.dgvDocumentos.CurrentCell.RowIndex) = dgrow
                    'MessageBox.Show(dgvDocumentos.Rows(y).Cells("Column5").Value)

                    'For i As Integer = 0 To dtTable.Rows.Count - 1
                    '    'IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosDescripcion") & " - " & dtTable.Rows(i).Item("CCosCodigo")))
                    '    IdCC.Items.Add(Trim(dtTable.Rows(i).Item("CCosCodigo")) & " - " & dtTable.Rows(i).Item("CCosDescripcion"))
                    'Next

                Catch ex As Exception
                    MessageBox.Show("Aqui un error: " & ex.ToString)
                End Try
                'For a As Integer = 0 To IdCC.Items.Count - 1
                '    Dim StrCadCodigo As String
                '    Dim cad As String = IdCC.Items.Item(a)
                '    StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                '    If Microsoft.VisualBasic.IsDBNull(dtDocumentos.Rows(y).Item("CCosCodigo")) = False Then
                '        If Trim(dtDocumentos.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                '            dgvDocumentos.Rows(y).Cells("IdCC").Value = IdCC.Items.Item(a)
                '        End If
                '    End If
                'Next
            End If
        Next
    End Sub

    Private Sub txtRuc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRuc.KeyPress
        'MessageBox.Show(Asc(e.KeyChar))

        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtRuc.Text)) = 11 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And CountDocumentos = 0 Then
                    objResultado = VerRuc(txtRuc.Text)
                    If objResultado.blnExiste = True Then
                        If (objResultado.dtResultado.Rows(0)("Estado").ToString() = "ACTIVO" And objResultado.dtResultado.Rows(0)("Condicion").ToString() = "HABIDO") Then
                            MessageBox.Show("EL RUC  ESTA ACTIVO")
                        Else
                            txtRuc.Text = ""
                            Dim OB_frmValidarRuc As frmValidarRuc = New frmValidarRuc(objResultado.dtResultado.Rows(0)("Ruc").ToString(), objResultado.dtResultado.Rows(0)("RazonSocial").ToString(), objResultado.dtResultado.Rows(0)("Estado").ToString(), objResultado.dtResultado.Rows(0)("Condicion").ToString(), objResultado.dtResultado.Rows(0)("Direccion").ToString())
                            OB_frmValidarRuc.ShowDialog()
                        End If
                    Else
                        MessageBox.Show("INGRESE CORRECTAMENTE EL RUC")
                    End If
                    txtRucProv.Text = Trim(txtRuc.Text)
                    txtBuscar.Text = Trim(txtRuc.Text)
                    txtBuscar_TextChanged(sender, e)
                    chkDocTodas_CheckedChanged(sender, e)
                    Panel1.Visible = True
                    'TabControl2.TabPages(0).Controls.Owner.Focus()
                    TabControl2.TabPages(0).Focus()
                ElseIf Len(Trim(txtRuc.Text)) = 11 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 And CountDocumentos > 0 Then
                    'txtRucProv.Text = Trim(txtRuc.Text)
                    'txtBuscar.Text = Trim(txtRuc.Text)
                    'txtBuscar_TextChanged(sender, e)
                    'chkDocTodas_CheckedChanged(sender, e)
                    Panel1.Visible = True
                    'TabControl2.TabPages(0).Controls.Owner.Focus()
                    'TabControl2.TabPages(0).Focus()
                Else
                    If cboBanco.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboBanco.Focus()
                        Exit Sub
                    End If
                    If cboCuenta.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un N�mero de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboCuenta.Focus()
                        Exit Sub
                    End If
                    If Len(txtRuc.Text.Trim) = 0 Then
                        Panel1.Visible = False
                        MessageBox.Show("Ingrese un N�mero de Ruc", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtRuc.Focus()
                        Exit Sub
                    End If

                    If Len(txtRuc.Text.Trim) > 0 And Len(txtRuc.Text.Trim) < 11 Then
                        Panel1.Visible = False
                        MessageBox.Show("Ingrese un Ruc V�lido", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtRuc.Focus()
                        Exit Sub
                    End If
                End If
                'Case 13
                'BeButton6.Focus()
                

        End Select
    End Sub

    Private Sub BeButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton2.Click
        Dim iResultadoDelete As Int32 = 0

        If Trim(sIdCodigoCabLibro) <> "" And Trim(sIdCodigoDetLibro) <> "" And gEmpresa <> "" Then
            Dim A�o As String = dtpFechaGasto.Value.Year
            Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            dtDatos = eLibroBancos.TraerIdLibro(A�o, Mes, Trim(cboCuenta.SelectedValue), gEmpresa)
            Dim MesCerrado As Integer = 0
            If dtDatos.Rows.Count > 0 Then 'SI EXISTE LIBRO DEL MES DE PAGO
                MesCerrado = Trim(dtDatos.Rows(0).Item("ConciliadoConExtracto"))
                If MesCerrado = 1 Then
                    MessageBox.Show("Registro Bloqueado para Eliminar puesto que la fecha de emision pertenece a un mes Conciliado y Cerrado en el Libro Bancos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
        End If

        If (MessageBox.Show("�Esta seguro de Quitar el Registro?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            If MovBD = "V" Then
                eValorizaciones = New clsValorizaciones
                Dim DtTotPagadoValDet As DataTable
                DtTotPagadoValDet = New DataTable
                DtTotPagadoValDet = eValorizaciones.fListarTotPagadodeVal(30, Trim(txtCodigo.Text), gEmpresa, gPeriodo)
                Dim iResultadoActSaldos As Integer = 0
                If DtTotPagadoValDet.Rows.Count > 0 Then
                    For x As Integer = 0 To DtTotPagadoValDet.Rows.Count - 1
                        Dim StrCad As Double = 0
                        Dim StrIdValorizacion As String = ""
                        Dim StrIdPago As String = ""
                        StrCad = Convert.ToDouble(DtTotPagadoValDet.Rows(x).Item("ImportePago"))
                        StrIdValorizacion = DtTotPagadoValDet.Rows(x).Item("IdValorizacion")
                        StrIdPago = DtTotPagadoValDet.Rows(x).Item("IdPago")
                        eValorizaciones = New clsValorizaciones
                        Dim DtTotPagadoVal As DataTable
                        DtTotPagadoVal = New DataTable
                        DtTotPagadoVal = eValorizaciones.fListarTotPagadodeVal(28, Trim(StrIdValorizacion), gEmpresa, gPeriodo)
                        If DtTotPagadoVal.Rows.Count > 0 Then
                            Dim PagadoNuevo As Double = 0
                            Dim SaldoNuevo As Double = 0
                            Dim IdDoc As String = ""
                            Dim IdEmprDoc As String = ""
                            PagadoNuevo = DtTotPagadoVal.Rows(0).Item("TotPagado") - StrCad
                            SaldoNuevo = DtTotPagadoVal.Rows(0).Item("Saldo") + StrCad
                            IdDoc = Trim(DtTotPagadoVal.Rows(0).Item("IdRegistro"))
                            IdEmprDoc = Trim(DtTotPagadoVal.Rows(0).Item("EmprCodigoDoc"))
                            If Trim(IdDoc) = "" Then
                                IdDoc = ".........."
                            End If
                            If Trim(IdEmprDoc) = "" Then
                                IdEmprDoc = ".........."
                            End If
                            iResultadoActSaldos = eValorizaciones.fActualizarSaldos(29, PagadoNuevo, SaldoNuevo, 0, dtpFechaGasto.Value, "", gEmpresa, gUsuario, StrIdValorizacion, StrIdPago, Trim(IdDoc), Trim(IdEmprDoc), StrCad, gPeriodo)
                        End If
                    Next
                    If iResultadoActSaldos > 0 Then
                        txtBuscar_TextChanged(sender, e)
                    End If
                End If
            End If

            'If Len(Trim(IdRetencionBD)) > 0 Then
            '    Dim iGrabarRetencion As Integer = 0
            '    eMovimientoCajaBanco = New clsMovimientoCajaBanco
            '----------------------------------------------------------------------------------------------------------->
            '----------------------------------------------------------------------------------------------------------->
            '----------------------------------------------------------------------------------------------------------->
            '    iGrabarRetencion = eMovimientoCajaBanco.fGrabarRetencion(2, Trim(IdRetencionBD), gEmpresa, Trim(SerieBD), "", dtpFechaGasto.Value, "", "", Trim(txtRuc.Text), "", 0, PorcentajeRetencion, 0 * (PorcentajeRetencion / 100), txtTipoCambio.Text, 0 - (0 * (PorcentajeRetencion / 100)), 0, gUsuario)
            'End If

            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            Dim IdCheque As String = ""
            Dim IdChequera As String = ""
            Dim iResultadoEditCheke As Int32 = 0
            If IdResumenBD <> "" Then 'si registro es un pago de proveedor?�
                Dim dtTableDetRes As DataTable
                dtTableDetRes = New DataTable
                dtTableDetRes = eMovimientoCajaBanco.fDetRes(20, Trim(txtCodigo.Text), IdResumenBD, gEmpresa) 'busca detalle del pago
                If dtTableDetRes.Rows.Count > 0 Then
                    Dim IdRegistro As String = ""
                    Dim Importe As Double = 0
                    Dim MontoPagar As Double = 0
                    Dim MontoActualizarDoc As Double = 0
                    Dim SaldoDocumento As Double = 0
                    Dim MontoActualizarSaldoDoc As Double = 0
                    Dim MontoSumSaldoProv As Double = 0
                    Dim sDescripDoc As String = ""
                    Dim EmprDocBd As String = ""
                    For x As Integer = 0 To dtTableDetRes.Rows.Count - 1 ' a cada detalle de pago, cada doc
                        sDescripDoc = dtTableDetRes.Rows(x).Item("DescripDoc") 'se le asigna el monto a pagar al salo del documento
                        MontoPagar = dtTableDetRes.Rows(x).Item("MontoPagar")
                        IdRegistro = dtTableDetRes.Rows(x).Item("IdRegistro")
                        EmprDocBd = dtTableDetRes.Rows(x).Item("EmprDoc")
                        Dim dtTableSalDoc As DataTable
                        dtTableSalDoc = New DataTable
                        dtTableSalDoc = eMovimientoCajaBanco.SaldoDoc(22, IdRegistro, EmprDocBd)
                        If dtTableSalDoc.Rows.Count > 0 Then
                            SaldoDocumento = dtTableSalDoc.Rows(0).Item("SaldoDocumento")
                        End If
                        MontoActualizarSaldoDoc = SaldoDocumento + MontoPagar
                        eMovimientoCajaBanco.ActSaldoDoc(21, IdRegistro, EmprDocBd, MontoActualizarSaldoDoc, 1, 0, "00003")
                        MontoSumSaldoProv = MontoSumSaldoProv + MontoPagar
                        eMovimientoCajaBanco.AnularDetResumen(24, IdResumenBD, IdRegistro, EmprDocBd, gUsuario)
                    Next
                    Dim MontoSaldoaPagar As String = 0
                    If Len(Trim(IdResumenBD)) > 0 Then
                        Dim dtMontoResumen As DataTable
                        Dim SaldoResumen As String
                        dtMontoResumen = New DataTable
                        dtMontoResumen = eGastosGenerales.fMontoResumen(Trim(IdResumenBD), gEmpresa)
                        If dtMontoResumen.Rows.Count > 0 Then
                            SaldoResumen = dtMontoResumen.Rows(0).Item("MontoTotal")
                            MontoSaldoaPagar = SaldoResumen + MontoSumSaldoProv
                        End If
                    End If
                    'traer todos los docs del resumen
                    'en un loop  actualizar la tabla saldo con los montos a pagar de los docs traidos..
                    Dim iResultadoEdiEstadoResumen2 As Integer = 0
                    iResultadoEdiEstadoResumen2 = eMovimientoCajaBanco.fEditarResumen(7, Trim(dtpFechaGasto.Value), IdResumenBD, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, "", Trim(cboTipoPago.SelectedValue), MontoSaldoaPagar, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, "", "", "", 0, 0, "", sMonCodigo, gEmpresa)
                    'eMovimientoCajaBanco.fActDetalleCronoIdChek(16, Trim(dtpFechaGasto.Value), IdResumen, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), Trim(strIdTipoAnexo), strIdAnexo, IdTipoMovimiento, Trim(cboTipoPago.SelectedValue), MontoRestar, Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, IdRendicion, "", "", 0, 0, "", sMonCodigo, gEmpresa)
                    Dim dtTable5 As DataTable
                    dtTable5 = New DataTable
                    Dim CodMoneda As String = ""
                    Dim IdSaldoProv As String = ""
                    Dim MontoSaldoDolaresProv As Double = 0
                    Dim MontoSaldoNew As Double = 0
                    Dim iResultado1 As Int32
                    If sDescripDoc = "01" Then
                        CodMoneda = "01"
                    ElseIf sDescripDoc = "02" Then
                        CodMoneda = "02"
                    End If
                    dtTable5 = ePagoProveedores.BuscarSaldoProvexMoneda(14, gEmpresa, strIdTipoAnexo, strIdAnexo, CodMoneda)
                    If dtTable5.Rows.Count > 0 Then 'se actualiza el saldo del proveedor
                        IdSaldoProv = dtTable5.Rows(0).Item("IdSaldo")
                        MontoSaldoDolaresProv = dtTable5.Rows(0).Item("MontoSaldo")
                        MontoSaldoNew = MontoSaldoDolaresProv + MontoSumSaldoProv
                        iResultado1 = ePagoProveedores.GrabarSaldoProv(19, IdSaldoProv, strIdTipoAnexo, strIdAnexo, gEmpresa, MontoSaldoNew, CodMoneda)
                    End If
                End If
            End If
            IdCheque = IdChequeBD
            IdChequera = IdChequeraBD
            If IdCheque <> "" Then 'si esta asignado a un cheke el monto a pagar   
                'se vuelve a activar el cheke 
                iResultadoEditCheke = eMovimientoCajaBanco.fEditarCheque(9, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue), Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, "", Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, Trim(gUsuario), "", 0, 0, "", sMonCodigo, gEmpresa)
                Dim dtChequesxChequera As DataTable
                dtChequesxChequera = New DataTable
                dtChequesxChequera = eGastosGenerales.fListarCantidadChequesxChequeraEmpr(IdChequera, gEmpresa)
                If dtChequesxChequera.Rows.Count > 0 Then
                    Dim CantidadChe As Integer ' = 0
                    CantidadChe = Trim(dtChequesxChequera.Rows(0).Item("Contador"))
                    If CantidadChe = 0 Then
                        'CAMBIAR ESTADO CHEQUERA
                        iResultadoEditCheke = eMovimientoCajaBanco.fEditarChequera(35, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), IdChequera, Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, "", Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, "", sMonCodigo, gEmpresa)
                    ElseIf CantidadChe > 0 Then
                        iResultadoEditCheke = eMovimientoCajaBanco.fEditarChequera(36, Trim(dtpFechaGasto.Value), IdMovimiento, Trim(cboBanco.SelectedValue), IdChequera, Trim(cboCentroCosto.SelectedValue), strIdTipoAnexo, strIdAnexo, "", Trim(cboTipoPago.SelectedValue), Convert.ToDouble(txtMonto.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), Trim(cboTipoGasto.SelectedValue), Trim(txtNroVoucher.Text), Convert.ToDouble(txtTipoCambio.Text), IdCheque, cboEstado.SelectedValue, "", "", 0, 0, "", sMonCodigo, gEmpresa)
                    End If
                End If
            End If
            ' si esta asignado a una caja
            If Trim(IdSesion) <> "" And NroSesion <> 0 And Trim(IdArea) <> "" And Trim(IdCaja) <> "" Then
                Dim iResActReembolsoSesion As Int16 = 0
                eSesionCajas = New clsSesionCajas
                iResActReembolsoSesion = eSesionCajas.fActualizarCabecera2(7, IdSesion, Trim(gEmpresa), NroSesion, IdArea, IdCaja, Trim(dtpFechaGasto.Value), Trim(dtpFechaGasto.Value), 0, 0, 0, 0, gUsuario)
            End If
            ' si esta asignado a una letra
            If Trim(IdLetraBD) <> "" Then
                Dim iResActReembolsoSesion As Int16 = 0
                eSesionCajas = New clsSesionCajas
                'iResActReembolsoSesion = eSesionCajas.fActualizarCabecera2(7, IdSesion, Trim(gEmpresa), NroSesion, IdArea, IdCaja, Trim(dtpFechaGasto.Value), Trim(dtpFechaGasto.Value), 0, 0, 0, 0)
                Dim iResActSaldoLetra As Integer
                Dim SaldoLetra As Double = 0
                Dim SaldoLetraBD As Double = 0
                Dim FechaGiroBD As DateTime
                Dim FechaVctoBD As DateTime
                Dim EstadoLetra As Integer = 0
                Dim dtSaldoLetra As DataTable
                eSesionCajas = New clsSesionCajas
                dtSaldoLetra = eSesionCajas.VerificarDatosLetra(9, IdLetraBD, Trim(gEmpresa))
                If dtSaldoLetra.Rows.Count > 0 Then
                    SaldoLetraBD = Convert.ToString(dtSaldoLetra.Rows(0).Item("Saldo"))
                    FechaGiroBD = Convert.ToString(dtSaldoLetra.Rows(0).Item("FechaGiro"))
                    FechaVctoBD = Convert.ToString(dtSaldoLetra.Rows(0).Item("FechaVcto"))
                    SaldoLetra = SaldoLetraBD + dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value '(Convert.ToDouble(txtMonto.Text))
                    If SaldoLetra > 0 Then
                        EstadoLetra = 0
                    End If
                    iResActSaldoLetra = eSesionCajas.fActualizarLetras(8, EstadoLetra, IdLetraBD, Trim(gEmpresa), SaldoLetra, gUsuario, FechaGiroBD, FechaVctoBD)
                End If
            End If
            'elimina detalles del Movimiento
            Dim iResEliminarDetalles As Integer = 0
            iResEliminarDetalles = eMovimientoCajaBanco.fEliminarItemDetalles(Trim(txtCodigo.Text), gEmpresa)
            'End If
            'si esta asignado a un detalle de libro banco
            If Trim(sIdCodigoCabLibro) <> "" And Trim(sIdCodigoDetLibro) <> "" And gEmpresa <> "" Then
                eLibroBancos = New clsLibroBancos
                Dim iResultadoActDetalle As Int16 = 0
                iResultadoActDetalle = eLibroBancos.fActDelete2(gEmpresa, Trim(sIdCodigoDetLibro), Trim(sIdCodigoCabLibro), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), "", Convert.ToDouble(txtMonto.Text), 0, 0, 0)
                ''''''''''''''''''QUITAR ITF''''''''''''''''''''''''''''''
                'If (Trim(cboTipoPago.SelectedValue) = "00005" Or Trim(cboTipoPago.SelectedValue) = "00007") And cbpBancoPrestado.SelectedIndex > -1 And cboCuentaPrestada.SelectedIndex > -1 And Len(txtReferencia.Text.Trim) > 0 And Len(txtMontoAutomatico.Text.Trim) > 0 Then
                '    'no graba por que es de cuenta a cuenta
                '    '
                '    '
                'Else
                '    'If Trim(cboTipoPago.SelectedValue) <> "00002" Then
                '    Dim dtExisteItf As DataTable
                '    dtExisteItf = New DataTable
                '    dtExisteItf = eLibroBancos.TraerSiExisteItf(gEmpresa, Trim(sIdCodigoCabLibro), Trim(sIdCodigoCabLibro), Trim(sIdCodigoDetLibro))
                '    Dim iResultadoEliminarItf As Integer = 0
                '    Dim IdDetLibroItf As String = ""
                '    Dim IdCabLibroItf As String = ""
                '    If dtExisteItf.Rows.Count = 1 Then
                '        IdDetLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroDet")) 'Saldo Inicial
                '        IdCabLibroItf = Trim(dtExisteItf.Rows(0).Item("IdLibroCab")) 'Saldo Inicial
                '        'iResultadoEliminarItf = eLibroBancos.fActDet(gEmpresa, IdDetLibroItf, IdCabLibroItf, dtpFechaGasto.Value, "00011", "", TipoMovCadena, "", "H", Convert.ToDouble(ItfMontoSumado), 0, 0, 0, "", Today(), Today(), 0, "", "", "", "", "", "")
                '        iResultadoEliminarItf = eLibroBancos.fActDelete2(gEmpresa, Trim(IdDetLibroItf), Trim(IdCabLibroItf), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), "", Convert.ToDouble(txtMonto.Text), 0, 0, 0)
                '    End If
                '    'End If
                'End If
                ''''''''''''''''''fin QUITAR itf''''''''''''''''''''''''''''''
                'falta actualizar saldo de libro
                '----------------------------------'--------------------------------------------
                '--------------'----------------------------------------------------------------
                Dim dtDatos As DataTable
                dtDatos = New DataTable
                Dim Cobrados As Double = 0
                Dim NoCobrados As Double = 0
                dtDatos = eLibroBancos.TraerIdLibro2(sIdCodigoCabLibro, gEmpresa)
                Dim dSaldoInicial As Double = 0
                If dtDatos.Rows.Count > 0 Then
                    dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                    Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                    NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                End If
                '--------------'----------------------------------------------------------------
                Dim iResultadoActSaldoLibro As Int16 = 0
                Dim dtLibroDet As DataTable
                dtLibroDet = New DataTable
                Dim saldo As Double = 0
                saldo = dSaldoInicial



                dtLibroDet = eLibroBancos.TraerLibroDet(37, Trim(sIdCodigoCabLibro), gEmpresa)
                If dtLibroDet.Rows.Count > 0 Then
                    For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                        For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                            If z = 11 Then 'lee el importe
                                If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                    saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sIdCodigoCabLibro), saldo)
                                ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                    saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(gEmpresa, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(sIdCodigoCabLibro), saldo)
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                    '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    'End If
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    'End If
                                End If
                            End If
                        Next
                    Next
                    Dim dtDtFech As DataTable
                    dtDtFech = New DataTable
                    Dim Fecha As DateTime
                    Dim FechaActCabecera As DateTime
                    dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, sIdCodigoCabLibro)
                    If dtDtFech.Rows.Count > 0 Then
                        Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                    End If
                    If dtpFechaGasto.Value > Fecha Then
                        FechaActCabecera = dtpFechaGasto.Value
                    Else
                        FechaActCabecera = Fecha
                    End If

                    If Trim(cboTipoPago.SelectedValue) = "00002" Then
                        '    NoCobrados = NoCobrados - Convert.ToDouble(txtMonto.Text)
                        NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                    End If

                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sIdCodigoCabLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                    '----------------------------------------
                    'saber si existe libros proximos al mes actual de la cuenta
                    Dim dtDatosLibro As DataTable
                    dtDatosLibro = New DataTable
                    dtDatosLibro = eLibroBancos.TraerIdLibro2(sIdCodigoCabLibro, gEmpresa)
                    Dim sIdCuenta As String = ""
                    If dtDatosLibro.Rows.Count > 0 Then
                        sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                    End If
                    Dim dtLibrosSuperiores As DataTable
                    dtLibrosSuperiores = New DataTable
                    Dim A�o As String = dtpFechaGasto.Value.Year
                    Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                    dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                    If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                        For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                            Dim iResultadoActLibroSup As Int16 = 0
                            Dim CodigoLibroBD As String = ""
                            Dim SaldoInicialBD As Double ' = 0
                            Dim SaldoFinalBD As Double '= 0
                            Dim ImporteOculto As Double = 0
                            If dgvListado.Rows.Count > 0 Then
                                ImporteOculto = Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                            End If
                            CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            SaldoInicialBD = Format(saldo, "0.00")
                            SaldoFinalBD = (Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + ImporteOculto) '- Convert.ToDouble(txtMonto.Text)
                            'graba saldos de uno de los libro proximos
                            '''''''''''''''''''''''''''''''''''''''''
                            Dim SaldoPrendaS As Double
                            Dim SaldoGarantiaS As Double
                            Dim SaldoDepPlazS As Double
                            Dim SaldoRetencionS As Double
                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                            '''''''''''''''''''''''''''''''''''''''''
                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                            saldo = SaldoFinalBD
                        Next
                    End If
                    '----------------------------------------
                Else
                    Dim FechaActCabecera As DateTime
                    FechaActCabecera = dtpFechaGasto.Value
                    'End If
                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(gEmpresa, sIdCodigoCabLibro, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today()) 'graba el nuevo saldo del libro
                    '----------------------------------------
                    'saber si existe libros proximos al mes actual de la cuenta
                    Dim dtDatosLibro As DataTable
                    dtDatosLibro = New DataTable
                    dtDatosLibro = eLibroBancos.TraerIdLibro2(sIdCodigoCabLibro, gEmpresa)
                    Dim sIdCuenta As String = ""
                    If dtDatosLibro.Rows.Count > 0 Then
                        sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                    End If
                    Dim dtLibrosSuperiores As DataTable
                    dtLibrosSuperiores = New DataTable
                    Dim A�o As String = dtpFechaGasto.Value.Year
                    Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                    dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, gEmpresa, FechaActCabecera) 'trae los proximos libros
                    If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                        For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                            Dim iResultadoActLibroSup As Int16 = 0
                            Dim CodigoLibroBD As String = ""
                            Dim SaldoInicialBD As Double ' = 0
                            Dim SaldoFinalBD As Double '= 0
                            CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            SaldoInicialBD = Format(saldo, "0.00")
                            SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) + Convert.ToDouble(txtMonto.Text)
                            'graba saldos de uno de los libro proximos
                            '''''''''''''''''''''''''''''''''''''''''
                            Dim SaldoPrendaS As Double
                            Dim SaldoGarantiaS As Double
                            Dim SaldoDepPlazS As Double
                            Dim SaldoRetencionS As Double
                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                            '''''''''''''''''''''''''''''''''''''''''
                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(gEmpresa, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                            saldo = SaldoFinalBD
                        Next
                    End If
                End If
            End If
            Dim iResultadoDeleteTrans As Integer = 0
            If Trim(IdTransaccionBD) <> "" Then
                iResultadoDeleteTrans = eLibroBancos.fActDeleteTransaccion(gEmpresa, Trim(IdTransaccionBD), Trim(CodigoLibroDestinoBD), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), "", Convert.ToDouble(txtMonto.Text), 0, 0, 0, gPeriodo)
            End If
            Dim iResultadoDeleteCarta As Integer = 0
            If Trim(IdCartaBD) <> "" Then
                iResultadoDeleteCarta = eLibroBancos.fActDeleteCarta(gEmpresa, Trim(IdCartaBD), Trim(CodigoLibroDestinoBD), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), "", Convert.ToDouble(txtMonto.Text), 0, 0, 0, gPeriodo)
            End If
            'SI TIENE LIBRO DE DESTINO (TRANSACCION)
            Dim xEmprPrestamo As String = ""
            xEmprPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value), "", Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value)) ' datos.Rows(i).Item(7)

            If Trim(CodigoLibroDestinoBD) <> "" And Trim(CodigoDetDestinoBD) <> "" And xEmprPrestamo <> "" Then

                eLibroBancos = New clsLibroBancos
                Dim iResultadoActDetalle As Int16 = 0
                iResultadoActDetalle = eLibroBancos.fActDelete2(xEmprPrestamo, Trim(CodigoDetDestinoBD), Trim(CodigoLibroDestinoBD), dtpFechaGasto.Value, Trim(cboTipoPago.SelectedValue), Trim(txtMontoAutomatico.Text), Trim(txtBeneficiario.Text), Trim(txtConcepto.Text), "", Convert.ToDouble(txtMonto.Text), 0, 0, 0)
                'falta actualizar saldo de libro
                '--------------
                Dim dtDatos As DataTable
                dtDatos = New DataTable
                Dim Cobrados As Double = 0
                Dim NoCobrados As Double = 0
                dtDatos = eLibroBancos.TraerIdLibro2(CodigoLibroDestinoBD, xEmprPrestamo)
                Dim dSaldoInicial As Double = 0
                If dtDatos.Rows.Count > 0 Then
                    dSaldoInicial = Trim(dtDatos.Rows(0).Item("SaldoInicial")) 'Saldo Inicial
                    Cobrados = Trim(dtDatos.Rows(0).Item("CheCobradosSI")) 'Saldo Inicial
                    NoCobrados = Trim(dtDatos.Rows(0).Item("CheCobradosNO")) 'Saldo Inicial
                End If
                '--------------
                Dim iResultadoActSaldoLibro As Int16 = 0
                Dim dtLibroDet As DataTable
                dtLibroDet = New DataTable
                Dim saldo As Double = 0
                saldo = dSaldoInicial

                dtLibroDet = eLibroBancos.TraerLibroDet(37, Trim(CodigoLibroDestinoBD), xEmprPrestamo)
                If dtLibroDet.Rows.Count > 0 Then
                    For y As Integer = 0 To dtLibroDet.Rows.Count - 1
                        For z As Integer = 0 To dtLibroDet.Columns.Count - 1
                            If z = 11 Then 'lee el importe
                                If dtLibroDet.Rows(y).Item(10).ToString = "D" Then 'pregunta si es DEBE (ENTRADA)
                                    saldo = saldo + dtLibroDet.Rows(y).Item(11).ToString
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(xEmprPrestamo, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(CodigoLibroDestinoBD), saldo)
                                ElseIf dtLibroDet.Rows(y).Item(10).ToString = "H" Then 'pregunta si es HABER (SALIDA)
                                    saldo = saldo - dtLibroDet.Rows(y).Item(11).ToString
                                    Dim iResultadoActDetalleLibro As Int16 = 0
                                    iResultadoActDetalleLibro = eLibroBancos.fActSaldoDet(xEmprPrestamo, Trim(dtLibroDet.Rows(y).Item(8).ToString), Trim(CodigoLibroDestinoBD), saldo)
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "1" Then
                                    '    NoCobrados = NoCobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    'End If
                                    'If Trim(dtLibroDet.Rows(y).Item(7).ToString) = "0" And Trim(dtLibroDet.Rows(y).Item(9).ToString) = "00002" Then
                                    '    Cobrados = Cobrados + dtLibroDet.Rows(y).Item(11).ToString
                                    'End If
                                End If
                            End If
                        Next
                    Next
                    Dim dtDtFech As DataTable
                    dtDtFech = New DataTable
                    Dim Fecha As DateTime
                    Dim FechaActCabecera As DateTime
                    dtDtFech = eLibroBancos.TraerMaxFechadeLibro(xEmprPrestamo, CodigoLibroDestinoBD)
                    If dtDtFech.Rows.Count > 0 Then
                        Fecha = Trim(dtDtFech.Rows(0).Item("Fecha"))
                    End If
                    If dtpFechaGasto.Value > Fecha Then
                        FechaActCabecera = dtpFechaGasto.Value
                    Else
                        FechaActCabecera = Fecha
                    End If
                    If Trim(cboTipoPago.SelectedValue) = "00002" Then
                        '    NoCobrados = NoCobrados - Convert.ToDouble(txtMonto.Text)
                        NoCobrados = NoCobrados - Convert.ToDouble(Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value))
                    End If

                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(xEmprPrestamo, CodigoLibroDestinoBD, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera) 'graba el nuevo saldo del libro
                    '----------------------------------------
                    'saber si existe libros proximos al mes actual de la cuenta
                    Dim dtDatosLibro As DataTable
                    dtDatosLibro = New DataTable
                    dtDatosLibro = eLibroBancos.TraerIdLibro2(CodigoLibroDestinoBD, xEmprPrestamo)
                    Dim sIdCuenta As String = ""
                    If dtDatosLibro.Rows.Count > 0 Then
                        sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                    End If
                    Dim dtLibrosSuperiores As DataTable
                    dtLibrosSuperiores = New DataTable
                    Dim A�o As String = dtpFechaGasto.Value.Year
                    Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                    dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, xEmprPrestamo, FechaActCabecera) 'trae los proximos libros
                    If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                        For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                            Dim iResultadoActLibroSup As Int16 = 0
                            Dim CodigoLibroBD As String = ""
                            Dim SaldoInicialBD As Double ' = 0
                            Dim SaldoFinalBD As Double '= 0
                            CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            SaldoInicialBD = Format(saldo, "0.00")
                            SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtMonto.Text)
                            'graba saldos de uno de los libro proximos
                            '''''''''''''''''''''''''''''''''''''''''
                            Dim SaldoPrendaS As Double
                            Dim SaldoGarantiaS As Double
                            Dim SaldoDepPlazS As Double
                            Dim SaldoRetencionS As Double
                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                            '''''''''''''''''''''''''''''''''''''''''
                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(xEmprPrestamo, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                            saldo = SaldoFinalBD
                        Next
                    End If
                    '----------------------------------------
                Else
                    'Dim dtDtFech As DataTable
                    'dtDtFech = New DataTable
                    Dim FechaActCabecera As DateTime
                    'dtDtFech = eLibroBancos.TraerMaxFechadeLibro(gEmpresa, CodigoLibroDestinoBD)
                    'If dtDtFech.Rows.Count > 0 Then
                    FechaActCabecera = dtpFechaGasto.Value
                    'End If
                    iResultadoActSaldoLibro = eLibroBancos.fActSaldoLibro(xEmprPrestamo, CodigoLibroDestinoBD, Format(dSaldoInicial, "0.00"), Format(saldo, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), Today()) 'graba el nuevo saldo del libro
                    '----------------------------------------
                    'saber si existe libros proximos al mes actual de la cuenta
                    Dim dtDatosLibro As DataTable
                    dtDatosLibro = New DataTable
                    dtDatosLibro = eLibroBancos.TraerIdLibro2(CodigoLibroDestinoBD, xEmprPrestamo)
                    Dim sIdCuenta As String = ""
                    If dtDatosLibro.Rows.Count > 0 Then
                        sIdCuenta = Trim(dtDatosLibro.Rows(0).Item("IdCuenta")) 'cuenta del libro
                    End If
                    Dim dtLibrosSuperiores As DataTable
                    dtLibrosSuperiores = New DataTable
                    Dim A�o As String = dtpFechaGasto.Value.Year
                    Dim Mes As String = Format(dtpFechaGasto.Value.Month, "00")
                    dtLibrosSuperiores = eLibroBancos.TraerLibrosSuperiores(Mes, A�o, sIdCuenta, xEmprPrestamo, FechaActCabecera) 'trae los proximos libros
                    If dtLibrosSuperiores.Rows.Count > 0 Then 'si existen libro proximos
                        For y As Integer = 0 To dtLibrosSuperiores.Rows.Count - 1
                            Dim iResultadoActLibroSup As Int16 = 0
                            Dim CodigoLibroBD As String = ""
                            Dim SaldoInicialBD As Double ' = 0
                            Dim SaldoFinalBD As Double '= 0
                            CodigoLibroBD = Trim(dtLibrosSuperiores.Rows(y).Item(0).ToString)
                            SaldoInicialBD = Format(saldo, "0.00")
                            SaldoFinalBD = Trim(dtLibrosSuperiores.Rows(y).Item(2).ToString) - Convert.ToDouble(txtMonto.Text)
                            'graba saldos de uno de los libro proximos
                            '''''''''''''''''''''''''''''''''''''''''
                            Dim SaldoPrendaS As Double
                            Dim SaldoGarantiaS As Double
                            Dim SaldoDepPlazS As Double
                            Dim SaldoRetencionS As Double
                            SaldoPrendaS = Trim(dtLibrosSuperiores.Rows(y).Item(5).ToString)
                            SaldoGarantiaS = Trim(dtLibrosSuperiores.Rows(y).Item(6).ToString)
                            SaldoDepPlazS = Trim(dtLibrosSuperiores.Rows(y).Item(7).ToString)
                            SaldoRetencionS = Trim(dtLibrosSuperiores.Rows(y).Item(8).ToString)
                            '''''''''''''''''''''''''''''''''''''''''
                            iResultadoActLibroSup = eLibroBancos.fActSaldoLibro2(xEmprPrestamo, CodigoLibroBD, Format(SaldoInicialBD, "0.00"), Format(SaldoFinalBD, "0.00"), Format(Cobrados, "0.00"), Format(NoCobrados, "0.00"), FechaActCabecera, SaldoPrendaS, SaldoGarantiaS, SaldoDepPlazS, SaldoRetencionS)
                            saldo = SaldoFinalBD
                        Next
                    End If
                    '----------------------------------------
                End If
            End If
            'SI TIENE LIBRO DE DESTINO (TRANSACCION)
            'si es cero borra el movimiento anulando los datos en el 
            iResultadoDelete = eMovimientoCajaBanco.Eliminar(25, Trim(txtCodigo.Text), gEmpresa) 'elimina el movimiento
            If iResultadoDelete = 1 Then
                cSegurosPolizas = New clsSegurosPolizas
                cSegurosPolizas.fUpdateEstadoEgresos_Delete(gEmpresa, "R", Trim(txtCodigo.Text))
            End If

            If iResultadoDelete = 1 Then
                mMostrarGrilla()
                LimpiarControles()
                txtCodigo.Clear()
                txtRuc.Clear()
                txtNroVoucher.Clear()
                CheckBox1.Checked = False
                BeButton4_Click(sender, e)
            End If
        End If 'sale de decir si o no para BORRAR
    End Sub

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        If cboArea.SelectedIndex > -1 Then
            Dim IdArea As String
            Try
                IdArea = IIf(Microsoft.VisualBasic.IsDBNull(cboArea.SelectedValue) = True, "", cboArea.SelectedValue.ToString)
                If IdArea <> "System.Data.DataRowView" Then
                    cboCaja.DataSource = eSesionCajas.fListarCajasPorArea(cboArea.SelectedValue, gEmpresa, gPeriodo)
                    cboCaja.ValueMember = "IdCaja"
                    cboCaja.DisplayMember = "DescripcionCaja"
                    cboCaja.SelectedIndex = -1
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
            If dgvListadoReem.Rows.Count > 0 Then
                For x As Integer = 0 To dgvListadoReem.RowCount - 1
                    dgvListadoReem.Rows.Remove(dgvListadoReem.CurrentRow)
                Next
            End If
        Else

        End If
    End Sub

    Private Sub cboCaja_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCaja.SelectionChangeCommitted
        Dim IdCaja As String
        Try
            IdCaja = cboCaja.SelectedValue.ToString
            If IdCaja <> "System.Data.DataRowView" Then
                Dim dtTable As DataTable
                dtTable = New DataTable
                dtTable = eSesionCajas.fListReembolsosdeCaja(Trim(cboArea.SelectedValue), Trim(IdCaja), gEmpresa, gPeriodo)
                dgvListadoReem.AutoGenerateColumns = False
                dgvListadoReem.DataSource = dtTable
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub BeLabel20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel20.Click
        Panel1.Visible = False
        cboTipoPago.Focus()
    End Sub

    Private Sub dgvListadoReem_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvListadoReem.DoubleClick
        Mov = "RC"
        Dim sMonCodigoCaja As String = ""
        Try
            txtMonto.Text = Format(dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(2).Value, "#,##0.00")
            txtConcepto.Text = "REEMBOLSO CAJA CHICA # " & Trim(dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells("Column26").Value) & " - " & Trim(dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(0).Value) & " - " & cboArea.Text & " / " & cboCaja.Text
            txtBeneficiario.Text = dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(6).Value
            NroSesion = dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(1).Value
            IdSesion = dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(3).Value
            IdArea = dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(4).Value
            IdCaja = dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(5).Value
            sMonCodigoCaja = Trim(dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(7).Value)
            Panel1.Visible = False
            cboEstado.SelectedValue = "02"
        Catch ex As Exception
            Exit Sub
        End Try
        If sMonCodigo = "02" And sMonCodigoCaja = "01" Then
            txtMonto.Text = Format(dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(2).Value / Val(txtTipoCambio.Text), "#,##0.00")
        End If
        If sMonCodigo = "01" And sMonCodigoCaja = "02" Then
            txtMonto.Text = Format(dgvListadoReem.Rows(dgvListadoReem.CurrentRow.Index).Cells(2).Value * Val(txtTipoCambio.Text), "#,##0.00")
        End If
    End Sub

    Private Sub BeButton8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton8.Click
        If Len(txtCodigo.Text) > 0 Then
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            Dim dtCantDetalle As DataTable
            dtCantDetalle = New DataTable
            dtCantDetalle = eMovimientoCajaBanco.fContarContarDetallesMovimiento(Trim(txtCodigo.Text), gEmpresa)
            If eMovimientoCajaBanco.iNroReg = 1 Then
                Dim ssCantiDet As String = ""
                ssCantiDet = dtCantDetalle.Rows(0).Item("CountDet")
                If Trim(ssCantiDet) = "1" Or Trim(ssCantiDet) = "0" Then
                    'abre reporte 1
                    If Len(txtNroVoucher.Text.Trim) > 0 Then
                        rptF.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
                        '1
                        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("@CodEmpresa")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(gEmpresa)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
                        '2
                        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("@NroVaucher")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(txtCodigo.Text)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
                        '3
                        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(gDesEmpresa)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

                        '4
                        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("Cuenta")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(cboCuenta.Text)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


                        'frmRptRequerimientoLogistica.crvReporte.DisplayGroupTree = False
                        'frmRptRequerimientoLogistica.WindowState = Windows.Forms.FormWindowState.Maximized
                        'frmRptRequerimientoLogistica.ShowDialog()
                        'frmRptRequerimientoLogistica.crvReporte.ReportSource = rptOCompra


                        rptF.PrintToPrinter(1, False, 0, 1)
                    End If

                ElseIf Trim(ssCantiDet) > "1" Then
                    'abre reporte 2
                    If Len(txtCodigo.Text.Trim) > 0 Then
                        rptF2.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
                        '1
                        crParameterFieldDefinitions = rptF2.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("@CodEmpresa")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(gEmpresa)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
                        '2
                        crParameterFieldDefinitions = rptF2.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("@NroMovimiento")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(txtCodigo.Text)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
                        '3
                        crParameterFieldDefinitions = rptF2.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(gDesEmpresa)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

                        '4
                        crParameterFieldDefinitions = rptF2.DataDefinition.ParameterFields()
                        crParameterFieldDefinition = crParameterFieldDefinitions("Cuenta")
                        crParameterValues = crParameterFieldDefinition.CurrentValues
                        crParameterDiscreteValue = New ParameterDiscreteValue()
                        crParameterDiscreteValue.Value = Trim(cboCuenta.Text)
                        crParameterValues.Add(crParameterDiscreteValue)
                        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
                        rptF2.PrintToPrinter(1, False, 0, 1)
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub BeButton5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton5.Click
        Me.Close()
    End Sub

    Private Sub rdb1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdb1.Click
        Try
            eGastosGenerales = New clsGastosGenerales
            cboBanco.DataSource = eGastosGenerales.fListarBancos2(gEmpresa)
            If eGastosGenerales.iNroRegistros > 0 Then
                cboBanco.ValueMember = "IdBanco"
                cboBanco.DisplayMember = "NombreBanco"
                cboBanco.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub rdb2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdb2.Click
        Try
            eGastosGenerales = New clsGastosGenerales
            cboBanco.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
            If eGastosGenerales.iNroRegistros > 0 Then
                cboBanco.ValueMember = "IdBanco"
                cboBanco.DisplayMember = "NombreBanco"
                cboBanco.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnPrestamo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrestamo.Click
        Panel3.Visible = True
        cboEmpresas.Enabled = False
        cboCentrodeCostos.Enabled = False
        If cboEmpresas.SelectedIndex > -1 Then
            cboEmpresas.Enabled = True
            cboCentrodeCostos.Enabled = True
        End If
        If OpcionMostrar = "Grid" Then
            If dgvListado.Rows.Count > 0 Then
                Dim xPrestamo, xEmprPrestamo, xCCPrestamo As String
                xPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column37").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column37").Value) ' datos.Rows(i).Item(7)
                xEmprPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value) ' datos.Rows(i).Item(7)
                xCCPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column39").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column39").Value) ' datos.Rows(i).Item(7)
                If xPrestamo = "1" Then
                    rdbEmpresas.Checked = False
                    rdbUnaEmpresa.Checked = True
                    cboEmpresas.SelectedValue = Trim(xEmprPrestamo)
                    cboCentrodeCostos.SelectedValue = Trim(xCCPrestamo)
                    cboEmpresas.Enabled = True
                    cboCentrodeCostos.Enabled = True
                End If
                If xPrestamo = "2" Or xPrestamo = "" Then
                    rdbEmpresas.Checked = True
                    rdbUnaEmpresa.Checked = False
                    cboEmpresas.SelectedIndex = -1
                    cboCentrodeCostos.SelectedIndex = -1
                    cboEmpresas.Enabled = False
                    cboCentrodeCostos.Enabled = False
                End If
                If Trim(xPrestamo) = "" Then
                    rdbEmpresas.Checked = True
                    rdbUnaEmpresa.Checked = False
                    cboEmpresas.SelectedIndex = -1
                    cboCentrodeCostos.SelectedIndex = -1
                    cboEmpresas.Enabled = False
                    cboCentrodeCostos.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Sub cboEmpresas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpresas.SelectedIndexChanged
        Dim IdEmpresa As String = ""
        If (cboEmpresas.SelectedIndex > -1) Then
            Try
                IdEmpresa = Trim(cboEmpresas.SelectedValue.ToString)
                If IdEmpresa <> "System.Data.DataRowView" Then
                    eGastosGenerales = New clsGastosGenerales
                    cboCentrodeCostos.DataSource = eGastosGenerales.fListarCCxEmpresa(IdEmpresa)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCentrodeCostos.ValueMember = "CCosCodigo"
                        cboCentrodeCostos.DisplayMember = "CCosDescripcion"
                        cboCentrodeCostos.SelectedIndex = -1
                    End If
                    Dim dtRuc As DataTable
                    dtRuc = New DataTable
                    dtRuc = eGastosGenerales.fListarRucxEmpresa(IdEmpresa)
                    If dtRuc.Rows.Count > 0 Then
                        Dim Ruc As String = ""
                        Ruc = dtRuc.Rows(0).Item("EmprRUC")
                        lblRuc.Text = Trim(Ruc)
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            lblRuc.Text = ""
            cboCentrodeCostos.DataSource = Nothing
        End If
    End Sub

    Private Sub rdbEmpresas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbEmpresas.CheckedChanged
        If rdbEmpresas.Checked = True Then
            'cboEmpresas.SelectedIndex = -1
            'cboCentrodeCostos.SelectedIndex = -1
            'lblRuc.Text = ""
            'lblCodCC.Text = ""
            'cboEmpresas.Enabled = False
            'cboCentrodeCostos.Enabled = False

            cboEmpresas.SelectedIndex = -1
            cboCentrodeCostos.SelectedIndex = -1
            lblRuc.Text = ""
            lblCodCC.Text = ""
            cboEmpresas.Enabled = True
            cboCentrodeCostos.Enabled = True

        End If

        'If rdbUnaEmpresa.Checked = True Then
        '    cboEmpresas.SelectedIndex = -1
        '    cboCentrodeCostos.SelectedIndex = -1
        '    lblRuc.Text = ""
        '    lblCodCC.Text = ""
        '    cboEmpresas.Enabled = True
        '    cboCentrodeCostos.Enabled = True
        'End If

    End Sub

    Private Sub rdbUnaEmpresa_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbUnaEmpresa.CheckedChanged
        If rdbUnaEmpresa.Checked = True Then
            cboEmpresas.SelectedIndex = -1
            cboCentrodeCostos.SelectedIndex = -1
            lblRuc.Text = ""
            lblCodCC.Text = ""
            cboEmpresas.Enabled = True
            cboCentrodeCostos.Enabled = True
        End If
    End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        If (MessageBox.Show("�Est� seguro de Cancelar el Pr�stamo?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            cboEmpresas.SelectedIndex = -1
            cboCentrodeCostos.SelectedIndex = -1
            lblRuc.Text = ""
            lblCodCC.Text = ""
            rdbUnaEmpresa.Checked = False
            rdbEmpresas.Checked = False
            Panel3.Visible = False
        End If
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If rdbUnaEmpresa.Checked = True Then
            If cboEmpresas.SelectedIndex = -1 And cboCentrodeCostos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Empresa y Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboEmpresas.Focus()
                Exit Sub
            End If

            If cboEmpresas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Empresa", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboEmpresas.Focus()
                Exit Sub
            End If
            If cboCentrodeCostos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCentrodeCostos.Focus()
                Exit Sub
            End If
        End If

        If rdbEmpresas.Checked = True Then
            If cboEmpresas.SelectedIndex = -1 And cboCentrodeCostos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Empresa y Centro de Costo de alguna de las empresas que va a Prestar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboEmpresas.Focus()
                Exit Sub
            End If

            If cboEmpresas.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Empresa", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboEmpresas.Focus()
                Exit Sub
            End If
            If cboCentrodeCostos.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCentrodeCostos.Focus()
                Exit Sub
            End If
        End If

        Panel3.Visible = False
    End Sub

    Private Sub dgvListado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvListado.Click
        MostrarDetalles = "SI"
    End Sub

    Private Sub dgvListado_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListado.KeyDown
        Select Case e.KeyCode
            Case Keys.F2
                If dgvListado.Rows.Count > 0 Then
                    Dim sIdMovimiento As String = ""
                    Dim sFormaDet As String = ""
                    Dim xEmpresaAPrestar As String = ""
                    Dim dblMonto As Double = 0
                    sIdMovimiento = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column3").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column3").Value)
                    sFormaDet = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column37").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column37").Value)
                    xEmpresaAPrestar = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value)
                    dblMonto = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value), 0, dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value)
                    If Trim(sIdMovimiento) <> "" Then
                        Dim frm As New frmDetalleMovimiento
                        frm.Owner = Me
                        frm.sIdMovimiento = Trim(sIdMovimiento)
                        frm.sFormaDetalle = Trim(sFormaDet)
                        frm.sEmpresaAPrestar = Trim(xEmpresaAPrestar)
                        frm.Monto = Trim(dblMonto)
                        frm.ShowInTaskbar = False
                        frm.ShowDialog()
                    Else
                        MessageBox.Show("No Existe Detalle", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    MessageBox.Show("Seleccione un Registro de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

            Case Keys.Up
                MostrarDetalles = "SI"
            Case Keys.Down
                MostrarDetalles = "SI"
        End Select
    End Sub

    Private Sub dgvListado_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvListado.SelectionChanged
        If MostrarDetalles = "NO" Then

        Else
            rdb2_Click(sender, e)
            rdb2.Checked = True
            Opcion = "Grid"
            OpcionMostrar = "Grid"
            BeButton3.Enabled = True
            BeButton8.Enabled = False
            BeButton1.Enabled = False
            Dim FechaPago As DateTime
            Dim Codigo, BancoId, CtaCorId, CentroCosto, TipoANexo, Anexo, Operacion, FormaPago, NroTransferencia As String
            Dim Monto, BeneficiarioBD, GlosaBD, TipoGasto, NroVoucherBD, TipoCambioBD, CtaRendir As String
            Dim RetNumeroBD, EmprDesRete As String
            Dim CheqEntrega As String = ""
            CheqEntrega = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CheqEntregado").Value
            If Len(Trim(CheqEntrega)) > 0 Then
                If Trim(CheqEntrega) = "1" Then
                    chkEntregado.Checked = True
                ElseIf Trim(CheqEntrega) = "0" Then
                    chkEntregado.Checked = False
                End If
            End If
            IdSesion = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column31").Value
            IdLetraBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column55").Value
            NroSesion = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column32").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column32").Value)
            IdArea = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column33").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column33").Value)
            IdCaja = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column34").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column34").Value)

            sIdCodigoCabLibro = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column35").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column35").Value)
            sIdCodigoDetLibro = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column36").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column36").Value)

            Codigo = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column3").Value
            FechaPago = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column8").Value
            BancoId = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdBanco").Value
            cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa(gEmpresa, BancoId)
            If eGastosGenerales.iNroRegistrosCtasxBanc > 0 Then
                cboCuenta.ValueMember = "IdCuenta"
                cboCuenta.DisplayMember = "NumeroCuenta"
                cboCuenta.SelectedIndex = -1
            End If
            CtaCorId = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("NumeroCuenta").Value
            CentroCosto = Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CCosCodigo").Value)
            TipoANexo = Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdTipoAnexo").Value)
            Anexo = Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdProveedor").Value)
            Operacion = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdTipoMovimiento").Value
            FormaPago = Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdFormaPago").Value)
            NroTransferencia = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("NroFormaPago").Value
            Anul = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Anulado").Value
            Monto = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column17").Value
            BeneficiarioBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Beneficiario").Value
            GlosaBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Glosa").Value
            TipoGasto = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdTipoGasto").Value
            NroVoucherBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("NroVaucher").Value
            TipoCambioBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("TipoCambio").Value
            CtaRendir = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdRendicion").Value
            IdChequeBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column18").Value
            txtIdCheque.Text = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column18").Value 'Trim(IdChequeBD)
            IdChequeraBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdChequera").Value
            'Dim tipocambioBD As Double = 0
            'tipocambioBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdRendicion").Value

            txtChequera.Text = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdChequera").Value 'Trim(IdChequeBD)

            IdResumenBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column19").Value
            strRuc = Trim(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column24").Value)
            txtCodigo.Text = Trim(Codigo)
            eValorizaciones = New clsValorizaciones
            Dim DtTipoMov As DataTable
            DtTipoMov = New DataTable
            DtTipoMov = eValorizaciones.fListarTotPagadodeVal(31, Trim(txtCodigo.Text), gEmpresa, gPeriodo)
            If DtTipoMov.Rows.Count > 0 Then
                MovBD = IIf(Microsoft.VisualBasic.IsDBNull(DtTipoMov.Rows(0).Item("TipoMov")), "", Trim(DtTipoMov.Rows(0).Item("TipoMov")))
            ElseIf DtTipoMov.Rows.Count = 0 Then
                MovBD = ""
            End If
            dtpFechaGasto.Value = FechaPago
            cboBanco.SelectedValue = Trim(BancoId)
            cboCuenta.SelectedValue = Trim(CtaCorId)
            cboCentroCosto.SelectedValue = Trim(CentroCosto)
            strIdTipoAnexo = Trim(TipoANexo)
            strIdAnexo = Trim(Anexo)
            If Trim(Operacion) = "H" Then
                rdbEgreso.Checked = True
                rdbIngreso.Checked = False
            ElseIf Trim(Operacion) = "D" Then
                rdbIngreso.Checked = True
                rdbEgreso.Checked = False
            End If
            cboTipoPago.SelectedValue = Trim(FormaPago)
            txtMontoAutomatico.Text = Trim(NroTransferencia)
            If Anul = "1" Then
                CheckBox1.Checked = True
                CheckBox1.Enabled = False
                BeButton3.Enabled = False
            ElseIf Anul = "0" Then
                CheckBox1.Checked = False
                CheckBox1.Enabled = True
                BeButton3.Enabled = True
            End If
            'txtMonto.Text = Format(Convert.ToDouble(Monto), "#,##0.00")


            IdRetencionBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column50").Value
            SerieBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("xSerie").Value

            ConRetencionBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("ConRetencion").Value
            TotRetenidoBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("TotRetenido").Value
            RetEmprCodigoBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("RetEmprCodigo").Value

            RetNumeroBD = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("NumeroRet").Value
            EmprDesRete = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("EmprRetDes").Value
            If Trim(ConRetencionBD) = "1" Then
                chkRetencion.Checked = True
                chkRetencion.Enabled = False
                txtMonto.Enabled = False
                txtRetenido.Text = Format(Convert.ToDouble(TotRetenidoBD), "#,##0.00")
                txtRetenido.Enabled = False

                If Len(Trim(RetEmprCodigoBD)) > 0 Then
                    If Trim(gEmpresa) = Trim(RetEmprCodigoBD) Then
                        lblSerieText.Text = "Serie : " & SerieBD & " - " & RetNumeroBD
                    ElseIf Trim(gEmpresa) <> Trim(RetEmprCodigoBD) Then
                        lblSerieText.Text = "Serie : " & SerieBD & " - " & RetNumeroBD & " [ " & EmprDesRete & " ]"
                    End If
                End If



                If Len(Trim(IdRetencionBD)) > 0 Then
                    btnOpciones.Visible = False
                    Panel11.Visible = False
                Else
                    btnOpciones.Visible = True
                    Panel11.Visible = True
                End If
                'If Len(Trim(IdRetencionBD)) > 0 Then
                '    btnOpciones.Visible = False
                '    Panel11.Visible = False
                'Else
                ''btnOpciones.Visible = True
                'End If

                'If Len(Trim(IdRetencionBD)) > 0 Then
                '    btnOpciones.Visible = True
                '    Panel11.Visible = True
                'Else
                '    btnOpciones.Visible = False
                'End If

                'txtGirar.Text = ""
            ElseIf Trim(ConRetencionBD) = "0" Then
                chkRetencion.Checked = False
                chkRetencion.Enabled = False
                txtMonto.Enabled = False
                txtRetenido.Text = "0.00"
                txtRetenido.Enabled = False
                btnOpciones.Visible = False
                Panel11.Visible = False
                'txtGirar.Text = ""
                lblSerieText.Text = ""
            End If



            'If Trim(ConRetencionBD) = "1" Then
            '    chkRetencion.Checked = True
            '    chkRetencion.Enabled = False
            'ElseIf Trim(ConRetencionBD) = "0" Then
            '    chkRetencion.Checked = False
            '    chkRetencion.Enabled = True
            'End If

            txtBeneficiario.Text = Trim(BeneficiarioBD)
            txtConcepto.Text = Trim(GlosaBD)
            cboTipoGasto.SelectedValue = Trim(TipoGasto)
            txtNroVoucher.Text = Trim(NroVoucherBD)
            txtTipoCambio.Text = Trim(TipoCambioBD)
            cboEstado.SelectedValue = Trim(CtaRendir)
            txtRuc.Text = Trim(strRuc)
            Panel2.Visible = False
            cboBanco.Enabled = False
            cboCuenta.Enabled = False
            cboTipoPago.Enabled = False
            txtBeneficiario.Enabled = True
            txtConcepto.Enabled = True
            txtMontoAutomatico.Enabled = False
            'txtMonto.Enabled = True
            BeButton6.Enabled = False
            txtRuc.Enabled = True
            BeButton8.Enabled = True
            BeButton2.Enabled = True
            txtNroVoucher.Enabled = False
            txtTipoCambio.Enabled = True
            dtpFechaGasto.Enabled = False
            Panel1.Visible = False
            If Trim(Anul) = "1" Then
                dtpFechaGasto.Enabled = False
                txtCodigo.Enabled = False
                cboBanco.Enabled = False
                cboCuenta.Enabled = False
                cboCentroCosto.Enabled = False
                txtRuc.Enabled = False
                rdbEgreso.Enabled = False
                rdbIngreso.Enabled = False
                cboTipoPago.Enabled = False
                txtMontoAutomatico.Enabled = False
                CheckBox1.Checked = True
                CheckBox1.Enabled = False
                txtMonto.Enabled = False
                txtBeneficiario.Enabled = False
                txtConcepto.Enabled = False
                cboTipoGasto.Enabled = False
                txtNroVoucher.Enabled = False
                txtTipoCambio.Enabled = False
                cboEstado.Enabled = False
                BeButton8.Enabled = True
                BeButton1.Enabled = False
                btnPrestamo.Enabled = False

                btnOpciones.Visible = False
                Panel11.Visible = False
                txtRetenido.Text = "0.00"
                chkRetencion.Checked = False
            ElseIf Trim(Anul) = "0" Then
                dtpFechaGasto.Enabled = False
                txtCodigo.Enabled = False
                cboBanco.Enabled = False
                cboCuenta.Enabled = False
                cboCentroCosto.Enabled = True
                txtRuc.Enabled = True
                rdbEgreso.Enabled = True
                rdbIngreso.Enabled = True
                cboTipoPago.Enabled = False
                txtMontoAutomatico.Enabled = False
                CheckBox1.Checked = False
                CheckBox1.Enabled = True
                txtMonto.Enabled = True
                txtBeneficiario.Enabled = True
                txtConcepto.Enabled = True
                cboTipoGasto.Enabled = True
                txtNroVoucher.Enabled = False
                txtTipoCambio.Enabled = True
                cboEstado.Enabled = False
                BeButton8.Enabled = True
                BeButton1.Enabled = False
                btnPrestamo.Enabled = True
            End If
            txtMonto.Text = Format(Convert.ToDouble(Monto), "#,##0.00")

            Dim xPrestamo, xEmprPrestamo, xCCPrestamo As String
            If dgvListado.Rows.Count > 0 Then
                xPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column37").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column37").Value) ' datos.Rows(i).Item(7)
                xEmprPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value) ' datos.Rows(i).Item(7)
                xCCPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column39").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column39").Value) ' datos.Rows(i).Item(7)
                If xPrestamo = "1" Then
                    rdbEmpresas.Checked = False
                    rdbUnaEmpresa.Checked = True
                    cboEmpresas.SelectedValue = Trim(xEmprPrestamo)
                    cboCentrodeCostos.SelectedValue = Trim(xCCPrestamo)
                    cboEmpresas.Enabled = True
                    cboCentrodeCostos.Enabled = True
                End If
                If xPrestamo = "2" Or xPrestamo = "" Then
                    rdbEmpresas.Checked = True
                    rdbUnaEmpresa.Checked = False
                    cboEmpresas.SelectedIndex = -1
                    cboCentrodeCostos.SelectedIndex = -1
                    cboEmpresas.Enabled = False
                    cboCentrodeCostos.Enabled = False
                End If
            End If


            IdTransaccionBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdTransaccion").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdTransaccion").Value)
            IdCartaBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCarta").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCarta").Value)
            If Trim(IdTransaccionBD) <> "" Or Trim(IdCartaBD) <> "" Then
                IdBancoDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column41").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column41").Value)
                IdCuentaDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCuentaDestino").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCuentaDestino").Value)
                GlosaDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("GlosaDestino").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("GlosaDestino").Value)
                CodigoLibroDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CodigoLibroDestino").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CodigoLibroDestino").Value)
                CodigoDetDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CodigoDetDestino").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CodigoDetDestino").Value)
                cboEmpresasLibros.SelectedValue = Trim(xEmprPrestamo)
                cbpBancoPrestado.SelectedValue = Trim(IdBancoDestinoBD)
                cboCuentaPrestada.SelectedValue = Trim(IdCuentaDestinoBD)
                txtReferencia.Text = Trim(GlosaDestinoBD)
                'Panel4.Visible = True
                Panel4.Visible = False
            Else
                Panel4.Visible = False
                cboEmpresasLibros.SelectedIndex = -1
                cbpBancoPrestado.SelectedIndex = -1
                cboCuentaPrestada.SelectedIndex = -1
                txtReferencia.Clear()
                IdBancoDestinoBD = ""
                IdCuentaDestinoBD = ""
                GlosaDestinoBD = ""
                CodigoLibroDestinoBD = ""
                CodigoDetDestinoBD = ""
            End If

            cbpBancoPrestado.Enabled = False
            cboCuentaPrestada.Enabled = False
            txtReferencia.ReadOnly = True
            btnCancel.Enabled = False
            Label4.Text = "Registro " & Convert.ToString(dgvListado.CurrentRow.Index + 1) & " de " & Convert.ToString(dgvListado.Rows.Count)
            If cboTipoPago.SelectedValue = "00002" Then
                txtMontoAutomatico.Enabled = False
                txtMontoAutomatico.ReadOnly = True
            ElseIf cboTipoPago.SelectedValue <> "00002" Then
                txtMontoAutomatico.Enabled = True
                txtMontoAutomatico.ReadOnly = True
            End If
            txtRuc.Enabled = False
            If Trim(ConRetencionBD) = "1" Then
                txtMonto.Enabled = False
            ElseIf Trim(ConRetencionBD) = "0" Then
                txtMonto.Enabled = True
            End If

           
            ''''''''''''preguntar si esta conciliado???
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            dtDatos = eLibroBancos.TraerConciliadoDetLibro(54, sIdCodigoCabLibro, sIdCodigoDetLibro, gEmpresa)
            If dtDatos.Rows.Count > 0 Then
                If Trim(dtDatos.Rows(0).Item("Conciliado")) = "1" Then
                    BeButton3.Enabled = False
                    BeButton2.Enabled = False
                    CheckBox1.Enabled = False
                ElseIf Trim(dtDatos.Rows(0).Item("Conciliado")) = "0" Then
                    BeButton3.Enabled = True
                    BeButton2.Enabled = True
                    CheckBox1.Enabled = True
                End If

            ElseIf dtDatos.Rows.Count = 0 Then
                BeButton3.Enabled = True
                BeButton2.Enabled = True
                CheckBox1.Enabled = True
            End If
            ''''''''''''fin de preguntar si esta conciliado???
            dgvListado.Focus()

        End If
    End Sub

    Private Sub cboCentrodeCostos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCentrodeCostos.SelectedIndexChanged
        Dim IdCentroCostoPre As String
        If (cboCentrodeCostos.SelectedIndex > -1) Then
            Try
                IdCentroCostoPre = Trim(cboCentrodeCostos.SelectedValue.ToString)
                If IdCentroCostoPre <> "System.Data.DataRowView" Then
                    lblCodCC.Text = Trim(IdCentroCostoPre)
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            lblCodCC.Text = ""
        End If
    End Sub

    Private Sub chkFecha_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFecha.CheckedChanged
        MostrarDetalles = "NO"
        If chkFecha.Checked = False Then
            dtpFechaGasto.Enabled = True
            dtpFechaGasto.Focus()
        End If

        If chkFecha.Checked = True Then
            dtpFechaGasto.Enabled = False
            mMostrarGrilla()
            Timer1.Enabled = True
        End If
        FechaParaBuscar = dtpFechaGasto.Value
    End Sub

    Private Sub dtpFechaGasto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpFechaGasto.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                MostrarDetalles = "NO"
                chkFecha.Checked = True
                FechaParaBuscar = dtpFechaGasto.Value
                mMostrarGrilla()
                Label4.Text = ""
                Timer1.Enabled = True
                cboBanco.Focus()
        End Select
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Timer1.Enabled = True Then
            BeButton4.Focus()
            BeButton4_Click(sender, e)
        End If
        Timer1.Enabled = False
    End Sub

    Private Sub txtMonto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMonto.KeyPress
        Dim caracter As Char = e.KeyChar
        ' referencia a la celda   
        Dim txt As TextBox = CType(sender, TextBox)
        ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
        ' es el separador decimal, y que no contiene ya el separador   
        If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
            e.Handled = False
        ElseIf caracter = ChrW(Keys.Enter) Then
            txtBeneficiario.Focus()
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtMonto_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMonto.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtMonto.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtMonto.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
        chkRetencion_CheckedChanged(sender, e)
    End Sub

    Private Sub txtBeneficiario_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBeneficiario.KeyPress
        Dim caracter As Char = e.KeyChar
        If caracter = ChrW(Keys.Enter) Then
            txtConcepto.Focus()
        End If
    End Sub

    Private Sub txtConcepto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtConcepto.KeyPress
        Dim caracter As Char = e.KeyChar
        If caracter = ChrW(Keys.Enter) Then
            cboTipoGasto.Focus()
        End If
    End Sub

    Private Sub txtMontoAutomatico_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMontoAutomatico.DoubleClick
        Dim IdFormaPago As String
        Dim IdBanco As String
        Dim IdCuenta As String
        If cboTipoPago.SelectedIndex > -1 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
            Try
                IdFormaPago = Trim(cboTipoPago.SelectedValue.ToString)
                If IdFormaPago = "00002" And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    'ES CHEQUE
                    IdBanco = Trim(cboBanco.SelectedValue)
                    IdCuenta = Trim(cboCuenta.SelectedValue)
                    cboChequeras.DataSource = eGastosGenerales.fListarChequerasxBancoCuenyEmpr(IdBanco, IdCuenta, gEmpresa)
                    If eGastosGenerales.iNroRegistrosChequeras > 0 Then
                        cboChequeras.ValueMember = "IdChequera"
                        cboChequeras.DisplayMember = "NroChequera"
                        cboChequeras.SelectedIndex = 0
                        cboChequeras_SelectedIndexChanged(sender, e)
                        BeLabel24.Text = Trim(cboBanco.Text)
                        BeLabel25.Text = Trim(cboCuenta.Text)
                        Panel2.Visible = True
                    ElseIf eGastosGenerales.iNroRegistrosChequeras = 0 Then
                        If Opcion = "Combo" Then
                            MessageBox.Show("No Existen Chequeras con el n�mero de cuenta: " & Trim(cboCuenta.Text) & " y el Banco: " & Trim(cboBanco.Text) & ". Seleccione otra Cuenta.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboTipoPago.SelectedIndex = -1
                            cboCuenta.Focus()
                        End If
                        txtMontoAutomatico.Text = ""
                        cboChequeras.SelectedIndex = -1
                        Panel2.Visible = False
                    End If

                    If cboCheques.Text = "System.Data.DataRowView" Then
                        txtMontoAutomatico.Text = ""
                    Else
                        txtMontoAutomatico.Text = cboCheques.Text
                    End If

                ElseIf IdFormaPago = "00005" And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then

                    If rdb1.Checked = True Then
                        MessageBox.Show("Para realizar Transferencias o Cartas de Cuenta a Cuenta Seleccione la Opcion Transferencia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    eGastosGenerales.fCodigoTransaccion(gEmpresa, gPeriodo)
                    txtMontoAutomatico.Text = Trim(eGastosGenerales.sCodFuturoTrans)
                    If OpcionMostrar = "Boton" Then
                        cboEmpresasLibros.Enabled = True
                        cbpBancoPrestado.Enabled = True
                        cboCuentaPrestada.Enabled = True

                        cboEmpresasLibros.SelectedIndex = -1
                        cbpBancoPrestado.SelectedIndex = -1
                        cboCuentaPrestada.SelectedIndex = -1
                        txtReferencia.Text = "TRANSACCION N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                        cboEmpresasLibros.Focus()
                    End If
                    Panel4.Visible = True

                ElseIf IdFormaPago = "00007" And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    If rdb1.Checked = True Then
                        MessageBox.Show("Para realizar Transferencias o Cartas de Cuenta a Cuenta, seleccione la opci�n Transferencia", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    If OpcionMostrar = "Boton" Then
                        cboEmpresasLibros.SelectedIndex = -1
                        cbpBancoPrestado.SelectedIndex = -1
                        cboCuentaPrestada.SelectedIndex = -1
                        txtReferencia.Text = "CARTA N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                        cboEmpresasLibros.Focus()
                    End If
                    Panel4.Visible = True
                Else
                    txtMontoAutomatico.Text = ""
                    cboChequeras.SelectedIndex = -1
                    Panel2.Visible = False
                    If OpcionMostrar = "Boton" And IdFormaPago = "00002" Then
                        If cboBanco.SelectedIndex = -1 Then
                            MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboBanco.Focus()
                            Exit Sub
                        End If

                        If cboCuenta.SelectedIndex = -1 Then
                            MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            cboCuenta.Focus()
                            Exit Sub
                        End If
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            If OpcionMostrar = "Boton" Then
                If cboBanco.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboBanco.Focus()
                    Exit Sub
                End If
                If cboCuenta.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboCuenta.Focus()
                    Exit Sub
                End If
                If cboTipoPago.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione El Tipo de Pago", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoPago.Focus()
                    Exit Sub
                End If
            End If
        End If
        If OpcionMostrar = "Grid" Then
            Dim NroTransferencia As String = ""
            Dim xIdTransaccion As String = ""
            Dim xIdCarta As String = ""
            Dim xIdBancoDestinoBD As String = ""
            Dim xIdCuentaDestinoBD As String = ""
            Dim xGlosaDestinoBD As String = ""
            Dim xEmprPrestamo As String = ""
            If dgvListado.Rows.Count > 0 Then
                NroTransferencia = dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("NroFormaPago").Value
                xIdTransaccion = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdTransaccion").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdTransaccion").Value)
                xIdCarta = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCarta").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCarta").Value)
                xIdBancoDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column41").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column41").Value)
                xIdCuentaDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCuentaDestino").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("IdCuentaDestino").Value)
                xGlosaDestinoBD = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("GlosaDestino").Value) = True, "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("GlosaDestino").Value)
                xEmprPrestamo = IIf(Microsoft.VisualBasic.IsDBNull(dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value), "", dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("Column38").Value) ' datos.Rows(i).Item(7)

                txtMontoAutomatico.Text = Trim(NroTransferencia)
                If Trim(xIdTransaccion) <> "" Or Trim(xIdCarta) <> "" Then
                    'cboEmpresasLibros.Enabled = False
                    'cbpBancoPrestado.Enabled = False
                    'cboCuentaPrestada.Enabled = False

                    cboEmpresasLibros.Enabled = True
                    cbpBancoPrestado.Enabled = True
                    cboCuentaPrestada.Enabled = True

                    txtReferencia.ReadOnly = False
                    btnCancel.Enabled = True

                    cboEmpresasLibros.SelectedValue = Trim(xEmprPrestamo)
                    cbpBancoPrestado.SelectedValue = Trim(xIdBancoDestinoBD)
                    cboCuentaPrestada.SelectedValue = Trim(xIdCuentaDestinoBD)

                    If Trim(xIdBancoDestinoBD) <> "" And Trim(xIdCuentaDestinoBD) <> "" And Trim(xEmprPrestamo) = "" Then
                        cboEmpresasLibros.SelectedValue = Trim(gEmpresa)
                        cbpBancoPrestado.SelectedValue = Trim(xIdBancoDestinoBD)
                        cboCuentaPrestada.SelectedValue = Trim(xIdCuentaDestinoBD)
                        cboEmpresasLibros.Enabled = False
                        cbpBancoPrestado.Enabled = False
                        cboCuentaPrestada.Enabled = False
                    ElseIf Trim(xIdBancoDestinoBD) <> "" And Trim(xIdCuentaDestinoBD) <> "" And Trim(xEmprPrestamo) <> "" Then
                        cboEmpresasLibros.SelectedValue = Trim(xEmprPrestamo)
                        cbpBancoPrestado.SelectedValue = Trim(xIdBancoDestinoBD)
                        cboCuentaPrestada.SelectedValue = Trim(xIdCuentaDestinoBD)
                        cboEmpresasLibros.Enabled = False
                        cbpBancoPrestado.Enabled = False
                        cboCuentaPrestada.Enabled = False
                    End If

                    'If Trim(xEmprPrestamo) <> "" Then
                    '    cboEmpresasLibros.Enabled = False
                    'End If
                    'If Trim(xIdBancoDestinoBD) <> "" Then
                    '    cbpBancoPrestado.Enabled = False
                    'End If
                    'If Trim(xIdCuentaDestinoBD) <> "" Then
                    '    cboCuentaPrestada.Enabled = False
                    'End If

                    txtReferencia.Text = Trim(xGlosaDestinoBD)
                    If cboTipoPago.SelectedValue = "00007" Then
                        If xGlosaDestinoBD = "" Then
                            txtReferencia.Text = "CARTA N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                        End If
                    ElseIf cboTipoPago.SelectedValue = "00005" Then
                        If xGlosaDestinoBD = "" Then
                            txtReferencia.Text = "TRANSACCION N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                        End If
                    End If

                    Panel4.Visible = True
                ElseIf Trim(xIdTransaccion) = "" Then
                    cboEmpresasLibros.SelectedIndex = -1
                    cbpBancoPrestado.Enabled = True
                    cbpBancoPrestado.SelectedIndex = -1
                    cboCuentaPrestada.Enabled = True
                    cboCuentaPrestada.SelectedIndex = -1
                    txtReferencia.ReadOnly = False
                    btnCancel.Enabled = True
                    txtReferencia.Text = Trim(xGlosaDestinoBD)
                    If cboTipoPago.SelectedValue = "00007" Then
                        If xGlosaDestinoBD = "" Then
                            'txtReferencia.Text = "CARTA N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                            txtReferencia.Text = "CARTA N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                        End If
                    ElseIf cboTipoPago.SelectedValue = "00005" Then
                        If xGlosaDestinoBD = "" Then
                            'txtReferencia.Text = "TRANSACCION N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                            txtReferencia.Text = "TRANSACCION N� " & txtMontoAutomatico.Text.Trim & " ( " & gDesEmpresa & " - " & cboBanco.Text & " - " & cboCuenta.Text & " )"
                        End If
                    End If
                    Panel4.Visible = True
                End If
            End If
        End If
    End Sub

    Private Sub txtMontoAutomatico_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMontoAutomatico.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtMontoAutomatico_DoubleClick(sender, e)
        End Select
    End Sub

    Private Sub txtMontoAutomatico_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMontoAutomatico.TextChanged
        If Len(txtMontoAutomatico.Text.Trim) = 0 Then
            txtIdCheque.Clear()
            txtChequera.Clear()
        End If
    End Sub

    Private Sub dtpFechaGasto_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFechaGasto.LostFocus
        Try
            AxMonthView1.Value = dtpFechaGasto.Value
            txtSemanaPago.Text = AxMonthView1.Week()

            'Dim dTipoCambio As Double
            'Dim dtTableTipoCambio As DataTable
            'dtTableTipoCambio = New DataTable
            'ePagoProveedores = New clsPagoProveedores
            'dtTableTipoCambio = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, dtpFechaGasto.Value)
            'If dtTableTipoCambio.Rows.Count > 0 Then
            '    dTipoCambio = Val(dtTableTipoCambio.Rows(0).Item("TcaVenta"))
            '    lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    lblTipoCambio.ForeColor = Color.Blue
            '    txtTipoCambio.ForeColor = Color.Blue
            'Else
            '    dTipoCambio = 0.0
            '    lblTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    txtTipoCambio.Text = Format(dTipoCambio, "0.00")
            '    lblTipoCambio.ForeColor = Color.Red
            '    txtTipoCambio.ForeColor = Color.Red
            'End If
            'mMostrarGrilla()

        Catch ex As Exception
        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            If cboTipoPago.SelectedValue <> "00002" Then
                MessageBox.Show("Solo pueden Anularse Cheques, de lo contrario Quite el Registro", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                CheckBox1.Checked = False
                CheckBox1.Enabled = False
                BeButton2.Focus()
                Exit Sub
            ElseIf cboTipoPago.SelectedValue = "00002" Then
                If MostrarDetalles = "SI" And OpcionMostrar = "Grid" And Anul = "0" Then
                    If (MessageBox.Show("�Esta seguro de Anular el Cheque?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        txtMonto.Text = 0
                        txtBeneficiario.Text = "ANULADO"
                        txtConcepto.Text = "ANULADO"
                        BeButton3_Click(sender, e)
                    Else
                        CheckBox1.Checked = False
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cbpBancoPrestado_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbpBancoPrestado.SelectedIndexChanged
        Dim IdBanco As String '= cbpBancoPrestado.SelectedValue.ToString

        If OpcionMostrar = "Grid" Or OpcionMostrar = "Boton" Then

            If cbpBancoPrestado.SelectedIndex > -1 Then
                IdBanco = cbpBancoPrestado.SelectedValue.ToString
                If IdBanco = "System.Data.DataRowView" Then
                    Exit Sub
                End If
            End If

            If cbpBancoPrestado.SelectedValue = Nothing Then
                Exit Sub
            End If

        End If


        If (cboBanco.SelectedIndex > -1) Or (cbpBancoPrestado.SelectedValue.ToString <> "System.Data.DataRowView") Then
            Try
                IdBanco = cbpBancoPrestado.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuentaPrestada.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa(Trim(cboEmpresasLibros.SelectedValue), IdBanco)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCuentaPrestada.ValueMember = "IdCuenta"
                        cboCuentaPrestada.DisplayMember = "NumeroCuenta"
                        cboCuentaPrestada.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
        End If

    End Sub

    Private Sub btnAceptarPrestamo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptarPrestamo.Click
        If cboEmpresasLibros.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione una Empresa.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboEmpresasLibros.Focus()
            Exit Sub
        End If

        If cbpBancoPrestado.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Banco.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cbpBancoPrestado.Focus()
            Exit Sub
        End If

        If cboCuentaPrestada.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione una Cuenta.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboCuentaPrestada.Focus()
            Exit Sub
        End If

        If (Trim(gEmpresa) = Trim(cboEmpresasLibros.SelectedValue)) And (Trim(cboBanco.SelectedValue) = Trim(cbpBancoPrestado.SelectedValue)) And (Trim(cboCuenta.SelectedValue) = Trim(cboCuentaPrestada.SelectedValue)) Then
            MessageBox.Show("No se Puede Transferir a la misma Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            If (Trim(gEmpresa) <> Trim(cboEmpresasLibros.SelectedValue)) Then
                rdbUnaEmpresa.Checked = True
                cboEmpresas.SelectedValue = Trim(cboEmpresasLibros.SelectedValue)
            End If
            Panel4.Visible = False
            txtMonto.Focus()
        End If

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        cboEmpresasLibros.SelectedIndex = -1
        cbpBancoPrestado.SelectedIndex = -1
        cboCuentaPrestada.SelectedIndex = -1
        txtReferencia.Clear()
        Panel4.Visible = False
        txtMonto.Focus()
    End Sub

    Sub Calcular()
        strGlosaFacturasSoles = ""
        strGlosaFacturasDolares = ""
        CountDocumentos = 0
        CountCantDocSoles = 0
        CountCantDocDolares = 0
        dblSoles = 0
        dblDolares = 0
        Dim CantidadDetraccionesSoles As Integer = 0

        If dgvDocumentos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                    Mov = "PP"
                    TipoMonedas = Trim(dgvDocumentos.Rows(x).Cells("TipoMoneda").Value)
                    EmpresaRetencion = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                    strRuc = Trim(txtRucProv.Text)
                    strIdTipoAnexo = "P" 'Trim(dgvCronogramas.Rows(dgvCronogramas.CurrentRow.Index).Cells("Column22").Value)
                    strIdAnexo = Trim(txtIdProv.Text)
                    If Trim(strRuc) <> "" Then
                        txtRuc.Text = Trim(strRuc)
                    End If
                    If Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value) = "01" Then
                        If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "d" Then
                            CantidadDetraccionesSoles = CantidadDetraccionesSoles + 1
                        End If
                        Dim StrCad As String = String.Empty
                        StrCad = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)

                        If Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) <> "NA" Then
                            If StrCad = Nothing Then
                                dblSoles = dblSoles + 0
                            Else
                                dblSoles = dblSoles + dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        ElseIf Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) = "NA" Then
                            If StrCad = Nothing Then
                                dblSoles = dblSoles - 0
                            Else
                                dblSoles = dblSoles - dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        End If
                        CountCantDocSoles = CountCantDocSoles + 1
                        strGlosaFacturasSoles = strGlosaFacturasSoles & " " & Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value) & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & " "
                    ElseIf Trim(dgvDocumentos.Rows(x).Cells("MonDes").Value) = "02" Then
                        Dim StrCad As String = String.Empty
                        StrCad = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)
                        If Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) <> "NA" Then
                            If StrCad = Nothing Then
                                dblDolares = dblDolares + 0
                            Else
                                dblDolares = dblDolares + dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        ElseIf Trim(dgvDocumentos.Rows(x).Cells("Doc").Value) = "NA" Then
                            If StrCad = Nothing Then
                                dblDolares = dblDolares - 0
                            Else
                                dblDolares = dblDolares - dgvDocumentos.Rows(x).Cells("aPagar").Value
                            End If
                        End If
                        CountCantDocDolares = CountCantDocDolares + 1
                        strGlosaFacturasDolares = strGlosaFacturasDolares & " " & Trim(dgvDocumentos.Rows(x).Cells("AbrDoc").Value) & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & " "
                    End If

                    CountDocumentos = CountDocumentos + 1
                Else
                    'dgvDocumentos.Rows(x).Cells("Saldo").Value = 0
                End If
            Next
            strGlosaFacturasSoles = "PAGO DE" & strGlosaFacturasSoles
            strGlosaFacturasDolares = "PAGO DE" & strGlosaFacturasDolares
            If CountCantDocSoles = 0 Then
                strGlosaFacturasSoles = ""
            End If
            If CountCantDocDolares = 0 Then
                strGlosaFacturasDolares = ""
            End If

            If CountDocumentos > 0 And (Trim(EmpresaRetencion) <> Trim(gEmpresa)) Then
                'si la empresa de retencion es diferente a la empresa del sistema
                'tomara como prestamo
                EmpresaRetencion = Trim(EmpresaRetencion)
                cboEmpresas.SelectedValue = Trim(EmpresaRetencion)
                rdbUnaEmpresa.Checked = True
                cboEstado.SelectedValue = "02"
            ElseIf CountDocumentos > 0 And (Trim(EmpresaRetencion) = Trim(gEmpresa)) Then
                EmpresaRetencion = gEmpresa
                cboEmpresas.SelectedIndex = -1
                rdbUnaEmpresa.Checked = False
                cboEstado.SelectedValue = "02"
            ElseIf CountDocumentos = 0 Then
                EmpresaRetencion = gEmpresa
                cboEmpresas.SelectedIndex = -1
                rdbUnaEmpresa.Checked = False
                cboEstado.SelectedValue = "01"
            End If

        End If

        txtBeneficiario.Text = Trim(txtRazonProv.Text)
        If CountDocumentos = 0 Then
            txtBeneficiario.Clear()
        End If
        txtNroDoc.Text = CountDocumentos

        If Trim(sMonCodigo) = "01" Then
            txtConcepto.Text = Trim(strGlosaFacturasSoles)
            If CantidadDetraccionesSoles = 0 Then
                txtTotPagarProv.Text = Format(dblSoles, "#,##0.00")
            ElseIf CantidadDetraccionesSoles > 0 Then
                txtTotPagarProv.Text = Format(Math.Round(dblSoles, 0), "#,##0.00")
            End If
        ElseIf Trim(sMonCodigo) = "02" Then
            txtConcepto.Text = Trim(strGlosaFacturasDolares)
            txtTotPagarProv.Text = Format(dblDolares, "#,##0.00")
        End If

        If TipoMonedas = "02" And sMonCodigo = "01" Then
            txtMonto.Text = Format(Convert.ToDouble(txtTotPagarProv.Text) * Val(txtTipoCambio.Text), "#,##0.00")
        End If
        If TipoMonedas = "01" And sMonCodigo = "02" Then
            txtMonto.Text = Format(Convert.ToDouble(txtTotPagarProv.Text) / Val(txtTipoCambio.Text), "#,##0.00")
        End If

        If TipoMonedas = "02" And sMonCodigo = "02" Then
            txtMonto.Text = Format(Convert.ToDouble(txtTotPagarProv.Text), "#,##0.00")
        End If
        If TipoMonedas = "01" And sMonCodigo = "01" Then
            If CantidadDetraccionesSoles = 0 Then
                txtMonto.Text = Format(Convert.ToDouble(txtTotPagarProv.Text), "#,##0.00")
            ElseIf CantidadDetraccionesSoles > 0 Then
                txtMonto.Text = Format(Math.Round(Convert.ToDouble(txtTotPagarProv.Text), 0), "#,##0.00")
            End If
        End If
        If Len(Trim(txtMonto.Text)) > 0 Then
            MontoRestar = Convert.ToDouble(txtMonto.Text)
        ElseIf Len(Trim(txtMonto.Text)) = 0 Then
            MontoRestar = 0
        End If
    End Sub

    Private Sub TabControl2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControl2.SelectedIndexChanged
        If TabControl2.TabPages(0).Controls.Owner.Focus = True Then
            txtRucProv.Focus()
        ElseIf TabControl2.TabPages(1).Controls.Owner.Focus = True Then
            cboArea.SelectedIndex = -1
            cboCaja.SelectedIndex = -1
            cboArea.Focus()
            If dgvListadoReem.Rows.Count > 0 Then
                For x As Integer = 0 To dgvListadoReem.RowCount - 1
                    dgvListadoReem.Rows.Remove(dgvListadoReem.CurrentRow)
                Next
            End If
        ElseIf TabControl2.TabPages(2).Controls.Owner.Focus = True Then
            rdbRazonLetra.Checked = True
            txtBuscarLetra.Clear()
            txtBuscarLetra_TextChanged(sender, e)
            txtBuscarLetra.Focus()
        ElseIf TabControl2.TabPages(3).Controls.Owner.Focus = True Then
            'rdbObra.Checked = True
            'txtBuscar.Clear()
            'txtBuscar_TextChanged(sender, e)
            txtBuscar.Focus()

        ElseIf TabControl2.TabPages(4).Controls.Owner.Focus = True Then

            cSegurosPolizas = New clsSegurosPolizas
            DT_ListaCuotas = New DataTable

            DT_ListaCuotas = cSegurosPolizas.fCuotasPolizasProgramadas(gEmpresa)
            DV_ListaCuotas = DT_ListaCuotas.DefaultView

            DgvCuotasPolizas.DataSource = DT_ListaCuotas
            For xCol As Integer = 0 To DgvCuotasPolizas.Columns.Count - 1
                DgvCuotasPolizas.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
                DgvCuotasPolizas.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                If DgvCuotasPolizas.Columns(xCol).Name = "Importe" Then
                    DgvCuotasPolizas.Columns(xCol).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    DgvCuotasPolizas.Columns(xCol).DefaultCellStyle.Format = "#,##0.00"
                ElseIf DgvCuotasPolizas.Columns(xCol).Name = "RUC" Then
                    DgvCuotasPolizas.Columns(xCol).Visible = False
                End If
            Next
            cboBusqueda.SelectedIndex = 0
            txtFiltro.Focus()
        End If
    End Sub
    Private Sub txtFiltro_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFiltro.KeyDown
        Select Case e.KeyCode
            Case Keys.Up
                Me.BindingContext(DT_ListaCuotas).Position = Me.BindingContext(DT_ListaCuotas).Position - 1
            Case Keys.Down
                Me.BindingContext(DT_ListaCuotas).Position = Me.BindingContext(DT_ListaCuotas).Position + 1
        End Select
    End Sub
    Private Sub txtFiltro_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFiltro.TextChanged
        Try
            DV_ListaCuotas.RowFilter = "[" & cboBusqueda.Text & "]" & " LIKE '" & txtFiltro.Text.Trim & "%'"
        Catch ex As Exception
            MessageBox.Show("No se puede Buscar lo Solicitado", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtFiltro.Text = String.Empty
        End Try
    End Sub
    Private Sub DgvCuotasPolizas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DgvCuotasPolizas.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.Handled = True
        End Select
    End Sub
    Private Sub DgvCuotasPolizas_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles DgvCuotasPolizas.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If DgvCuotasPolizas.Rows.Count > 0 Then
                    If MessageBox.Show("�Desea Agregar la Cuota Programada en seleccion a la Pantalla de Egresos de Caja?", "Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        txtMonto.Text = Format(DgvCuotasPolizas.Rows(DgvCuotasPolizas.CurrentRow.Index).Cells("Importe").Value, "#,##0.00")
                        txtBeneficiario.Text = DgvCuotasPolizas.Rows(DgvCuotasPolizas.CurrentRow.Index).Cells("Cia").Value
                        txtRuc.Text = DgvCuotasPolizas.Rows(DgvCuotasPolizas.CurrentRow.Index).Cells("RUC").Value
                        sPoliza = DgvCuotasPolizas.Rows(DgvCuotasPolizas.CurrentRow.Index).Cells("Poliza").Value
                        sEndoso = DgvCuotasPolizas.Rows(DgvCuotasPolizas.CurrentRow.Index).Cells("Endoso").Value
                        sNroCuota = DgvCuotasPolizas.Rows(DgvCuotasPolizas.CurrentRow.Index).Cells("NroCuota2").Value
                    End If
                    Panel1.Visible = False
                    'MsgBox(DgvCuotasPolizas.Rows(DgvCuotasPolizas.CurrentRow.Index).Cells("Poliza").Value)
                End If
        End Select
    End Sub


    Private Sub txtBuscarLetra_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscarLetra.TextChanged
        If Len(Trim(txtBuscarLetra.Text)) > 0 And rdbRazonLetra.Checked = True Then
            Dim sDescripcion As String = ""
            Dim dtTable As DataTable
            dtTable = New DataTable
            sDescripcion = Trim(txtBuscarLetra.Text)
            dtTable = eGastosGenerales.fListarLetrasxSemana(48, gEmpresa, "", sDescripcion)
            dgvLetras.AutoGenerateColumns = False
            dgvLetras.DataSource = dtTable
        ElseIf Len(Trim(txtBuscarLetra.Text)) > 0 And rdbRucLetra.Checked = True Then
            Dim Ruc As String = ""
            Dim sDescripcion As String = ""
            Dim dtTable As DataTable
            dtTable = New DataTable
            Ruc = Trim(txtBuscarLetra.Text)
            dtTable = eGastosGenerales.fListarLetrasxSemana(47, gEmpresa, Ruc, "")
            dgvLetras.AutoGenerateColumns = False
            dgvLetras.DataSource = dtTable
        End If
        If Len(Trim(txtBuscarLetra.Text)) = 0 Then
            Dim dtTable As DataTable
            dtTable = New DataTable
            dtTable = eGastosGenerales.fListarLetrasxSemana(46, gEmpresa, "", "")
            dgvLetras.AutoGenerateColumns = False
            dgvLetras.DataSource = dtTable
        End If
    End Sub

    Private Sub rdbRazonLetra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRazonLetra.Click
        rdbRucLetra.Checked = False
        rdbRazonLetra.Checked = True
        txtBuscarLetra.Clear()
        txtBuscarLetra.MaxLength = 150
        txtBuscarLetra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfabetico
        txtBuscarLetra_TextChanged(sender, e)
        txtBuscarLetra.Focus()
    End Sub

    Private Sub rdbRucLetra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRucLetra.Click
        rdbRucLetra.Checked = True
        rdbRazonLetra.Checked = False
        txtBuscarLetra.Clear()
        txtBuscarLetra.MaxLength = 11
        txtBuscarLetra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        txtBuscarLetra_TextChanged(sender, e)
        txtBuscarLetra.Focus()
    End Sub

    Private Sub DataGridView1_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs) Handles dgvLetras.DataError
        If e.Context = "768" Then
            MessageBox.Show("Ingrese una Fecha V�lida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        If (e.Context = DataGridViewDataErrorContexts.Commit) Then
            MessageBox.Show("Commit error")
        End If
        If (e.Context = DataGridViewDataErrorContexts.CurrentCellChange) Then
            MessageBox.Show("Cell change")
        End If
        If (e.Context = DataGridViewDataErrorContexts.Parsing) Then
            MessageBox.Show("parsing error")
        End If
        If (e.Context = DataGridViewDataErrorContexts.LeaveControl) Then
            MessageBox.Show("leave control error")
        End If
        If (TypeOf (e.Exception) Is ConstraintException) Then
            Dim view As DataGridView = CType(sender, DataGridView)
            view.Rows(e.RowIndex).ErrorText = "an error"
            view.Rows(e.RowIndex).Cells(e.ColumnIndex).ErrorText = "an error"
            e.ThrowException = False
        End If
    End Sub

    Private Sub dgvLetras_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLetras.CellEndEdit
        Dim StrCad As String = String.Empty
        If e.ColumnIndex = 3 Then
            If Microsoft.VisualBasic.IsDBNull(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(3).Value) = True Then
                MessageBox.Show("Ingrese una Fecha de Giro", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvLetras.CurrentCell = dgvLetras(3, dgvLetras.CurrentRow.Index)
                Exit Sub
            End If
            StrCad = dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(3).Value
            If StrCad = Nothing Then Exit Sub
            Try
                If Convert.ToDateTime(StrCad) > Convert.ToDateTime(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(4).Value) Then
                    MessageBox.Show("La Fecha de Giro debe ser Menor o igual a la Fecha de Vencimiento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvLetras.CurrentCell = dgvLetras(3, dgvLetras.CurrentRow.Index)
                End If
            Catch ex As Exception
                MessageBox.Show("Ingrese una Fecha V�lida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvLetras.CurrentCell = dgvLetras(3, dgvLetras.CurrentRow.Index)
            End Try
        ElseIf e.ColumnIndex = 4 Then
            If Microsoft.VisualBasic.IsDBNull(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(4).Value) = True Then
                MessageBox.Show("Ingrese una Fecha de Vencimiento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvLetras.CurrentCell = dgvLetras(4, dgvLetras.CurrentRow.Index)
                Exit Sub
            End If
            StrCad = dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(4).Value
            If StrCad = Nothing Then Exit Sub
            Try
                If Convert.ToDateTime(StrCad) < Convert.ToDateTime(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(3).Value) Then
                    MessageBox.Show("La Fecha de Vencimiento debe ser Mayor o igual a la Fecha de Giro", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvLetras.CurrentCell = dgvLetras(4, dgvLetras.CurrentRow.Index)
                End If
            Catch ex As Exception
                MessageBox.Show("Ingrese una Fecha V�lida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvLetras.CurrentCell = dgvLetras(4, dgvLetras.CurrentRow.Index)
            End Try
        End If
    End Sub

    Private Sub dgvLetras_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLetras.DoubleClick
        If dgvLetras.Rows.Count > 0 Then
            Mov = "L"
            If Microsoft.VisualBasic.IsDBNull(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(3).Value) = True Then
                MessageBox.Show("Ingrese una Fecha de Giro", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvLetras.CurrentCell = dgvLetras(3, dgvLetras.CurrentRow.Index)
                Exit Sub
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(4).Value) = True Then
                MessageBox.Show("Ingrese una Fecha de Vencimiento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvLetras.CurrentCell = dgvLetras(4, dgvLetras.CurrentRow.Index)
                Exit Sub
            End If
            Dim monedaL As String = ""
            Try
                txtMonto.Text = Format(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(7).Value, "#,##0.00")
                MontoLetra = Format(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(7).Value, "0.00")
                FechaGiroG = Convert.ToDateTime(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells("FechaGiroX").Value)
                FechaVenG = Convert.ToDateTime(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells("FechaVctoX").Value)
                txtConcepto.Text = "PAGO DE LETRA # " & Trim(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells("NumeroL").Value)
                txtBeneficiario.Text = Trim(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(1).Value)
                IdLetra = Trim(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(9).Value)
                txtRuc.Text = Trim(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(0).Value)
                monedaL = Trim(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(10).Value)
                Panel1.Visible = False
            Catch ex As Exception
                Exit Sub
            End Try
            If sMonCodigo = "02" And monedaL = "01" Then
                txtMonto.Text = Format(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(7).Value / Val(txtTipoCambio.Text), "#,##0.00")
            End If
            If sMonCodigo = "01" And monedaL = "02" Then
                txtMonto.Text = Format(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(7).Value * Val(txtTipoCambio.Text), "#,##0.00")
            End If
        End If
    End Sub

    Private Sub dgvLetras_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvLetras.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress2
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress2(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvLetras.CurrentCell.ColumnIndex
        If columna = 3 Or columna = 4 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
                e.KeyChar = Chr(0)
            End If
        End If
    End Sub

    Private Sub dgvDocumentos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellContentClick

        If dgvDocumentos.Rows.Count > 0 Then
            Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex
            Try
                dgvDocumentos.CurrentCell = dgvDocumentos(0, dgvDocumentos.CurrentRow.Index)
                'Calcular()
                dgvDocumentos.CurrentCell = dgvDocumentos(columna, dgvDocumentos.CurrentRow.Index)
                If dgvDocumentos.Rows.Count > 0 Then
                    Dim iFila As Integer
                    Calcular()
                    If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 26 Then
                        iFila = dgvDocumentos.CurrentRow.Index
                        dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                        dgvDocumentos.CurrentCell = dgvDocumentos(26, iFila)
                    End If
                    If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 14 Then
                        iFila = dgvDocumentos.CurrentRow.Index
                        dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                        dgvDocumentos.CurrentCell = dgvDocumentos(14, iFila)
                        If dgvDocumentos.Rows.Count > 0 Then
                            Dim iFila2 As Integer
                            iFila2 = dgvDocumentos.CurrentRow.Index
                            If Convert.ToBoolean(dgvDocumentos.Rows(iFila2).Cells("X").Value) = True Then
                                If dgvDocumentos.Rows(iFila2).Cells(13).Value = "" Then
                                    MessageBox.Show("Seleccione un Centro de Costo para el Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    dgvDocumentos.Rows(iFila2).Cells("X").Value = False
                                    dgvDocumentos.CurrentCell = dgvDocumentos(0, iFila2)
                                    dgvDocumentos.CurrentCell = dgvDocumentos(13, iFila2)
                                    Exit Sub
                                End If
                                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
                            ElseIf Convert.ToBoolean(dgvDocumentos.Rows(iFila2).Cells("X").Value) = False Then
                                'dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value + dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
                            End If
                        End If
                    End If
                End If

            Catch ex As Exception
            End Try
            'Calcular()
        End If
        Calcular()
    End Sub

    Private Sub txtRucProv_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRucProv.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                chkDocTodas_CheckedChanged(sender, e)
        End Select
    End Sub

    Private Sub txtRucProv_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRucProv.TextChanged
        If Len(Trim(txtRucProv.Text)) = 11 Then
            chkDocTodas_CheckedChanged(sender, e)
        End If
    End Sub

    Private Sub dgvDocumentos_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellDoubleClick
        If dgvDocumentos.Rows.Count > 0 Then
            Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex
            If columna = 12 Then
                Panel7.Visible = True
                txtNroOrden.Clear()
                txtNroOrden_TextChanged(sender, e)
                txtNroOrden.Focus()
                txtNroOrden.Enabled = True
            ElseIf columna = 25 Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
            End If
        End If
    End Sub

    Private Sub dgvDocumentos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDocumentos.CellEndEdit
        Dim StrCad As String = String.Empty
        Dim StrImporteDoc As String = String.Empty
        If e.ColumnIndex = 8 Or e.ColumnIndex = 14 Then
            StrCad = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value)
            StrImporteDoc = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value)
            If StrCad = Nothing Or StrImporteDoc = Nothing Or StrCad = "0.00" Then
                Calcular()
                Exit Sub
            End If
            Try
                If Convert.ToDouble(StrCad) > Convert.ToDouble(StrImporteDoc) Then
                    MessageBox.Show("El Monto a Pagar Supera al Importe del Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = False
                    dgvDocumentos.CurrentCell = dgvDocumentos(8, dgvDocumentos.CurrentRow.Index)
                    If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = True Then
                        dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = False
                    End If
                Else
                    Dim resta As Double = 0
                    resta = Convert.ToDouble(StrImporteDoc) - Convert.ToDouble(StrCad)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = resta 'Format(CDec(resta), "0.00")
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = StrCad 'Format(CDec(StrCad), "0.00")
                    dgvDocumentos.CurrentCell = dgvDocumentos(8, dgvDocumentos.CurrentRow.Index)

                    If (resta > 0) And (resta < StrImporteDoc) Then
                        'If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = False Then
                        dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = True
                        'End If
                    ElseIf (resta = 0) Or (resta < 0) Then
                        dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column6").Value = False
                    End If

                    If resta = StrImporteDoc Then
                        If dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = True Then
                            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("X").Value = False
                        End If
                    End If
                End If
            Catch ex As Exception
            End Try
        ElseIf e.ColumnIndex = 10 Then
            Dim Importe As Double = 0
            Dim Porcentaje As Double = 0
            Dim PorcentajeD As Double = 0
            Dim PorcentajeP As Double = 0
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = "0.00"
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = ""
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "d" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "R" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "r" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeD = (Importe * Porcentaje) / 100
                PorcentajeD = Format(Math.Round(PorcentajeD, 0), "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeD
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe - PorcentajeD
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "P" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "p" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeP = (Importe * Porcentaje) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeP
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe + PorcentajeP
            End If
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value) = False Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value, "#,##0.00")
            End If
        ElseIf e.ColumnIndex = 9 Then
            Dim CadenaCon As String
            If vb.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                CadenaCon = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(0, "0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = Format(0, "0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value
            Else
                CadenaCon = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value
            End If
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = vb.UCase(CadenaCon)
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = "0.00"
                Dim ImporteAux As Double = 0
                Dim PorcentajeAux As Double = 0
                Dim PorcentajeDAux As Double = 0
                Dim PorcentajePAux As Double = 0
                ImporteAux = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                PorcentajeAux = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeDAux = (ImporteAux * PorcentajeAux) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(PorcentajeAux, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeDAux
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = ImporteAux - PorcentajeDAux
            End If
            Dim Importe As Double = 0
            Dim Porcentaje As Double = 0
            Dim PorcentajeD As Double = 0
            Dim PorcentajeP As Double = 0
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = "0.00"
            End If
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value = ""
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "D" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "d" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "R" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "r" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeD = (Importe * Porcentaje) / 100
                PorcentajeD = Format(Math.Round(PorcentajeD, 0), "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeD
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe - PorcentajeD
            End If
            If Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "P" Or Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("DP").Value) = "p" Then
                Importe = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value)
                Porcentaje = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value)
                PorcentajeP = (Importe * Porcentaje) / 100
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Porcentaje").Value = Format(Porcentaje, "#,##0.00")
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value = PorcentajeP
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = Importe + PorcentajeP
            End If
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Saldo").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("SaldoOculto").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value
        ElseIf e.ColumnIndex = 12 Then
            If Microsoft.VisualBasic.IsDBNull(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("OC").Value) = True Then
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("CodigoOC").Value = ""
                dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IdCC").Value = ""
            End If
        ElseIf e.ColumnIndex = 11 Then
            dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("aPagar").Value = dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("ImporteTotal").Value - dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IPor").Value
        End If
        Calcular()
    End Sub

    Private Sub dgvDocumentos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDocumentos.Click
        If AplicarPintado = 1 Then
            Pintar()
            AplicarPintado = AplicarPintado + 1
        End If
    End Sub

    Private Sub gestionaResaltadosDocumentos(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(8).Style.BackColor = c
        visor.Rows(fila).Cells(9).Style.BackColor = c
        visor.Rows(fila).Cells(10).Style.BackColor = c
        visor.Rows(fila).Cells(11).Style.BackColor = c
        visor.Rows(fila).Cells(12).Style.BackColor = c
        visor.Rows(fila).Cells(13).Style.BackColor = c
        visor.Rows(fila).Cells(14).Style.BackColor = c
        visor.Rows(fila).Cells(15).Style.BackColor = c
        visor.Rows(fila).Cells(16).Style.BackColor = c
        visor.Rows(fila).Cells(17).Style.BackColor = c
        visor.Rows(fila).Cells(18).Style.BackColor = c
        visor.Rows(fila).Cells(19).Style.BackColor = c
        visor.Rows(fila).Cells(20).Style.BackColor = c
        visor.Rows(fila).Cells(21).Style.BackColor = c
        visor.Rows(fila).Cells(22).Style.BackColor = c
        visor.Rows(fila).Cells(23).Style.BackColor = c
        visor.Rows(fila).Cells(24).Style.BackColor = c
        visor.Rows(fila).Cells(25).Style.BackColor = c
        visor.Rows(fila).Cells(26).Style.BackColor = c
        visor.Rows(fila).Cells(27).Style.BackColor = c
    End Sub

    Sub Pintar()
        If dgvDocumentos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                If dgvDocumentos.Rows(x).Cells("Column1").Value = "0" Then
                    gestionaResaltadosDocumentos(dgvDocumentos, x, Color.Aqua)
                End If 'Violet
                If dgvDocumentos.Rows(x).Cells("Column1").Value = "1" Then
                    gestionaResaltadosDocumentos(dgvDocumentos, x, Color.Violet)
                End If '
                If Trim(dgvDocumentos.Rows(x).Cells("PPago").Value) = "1" Then
                    dgvDocumentos.Rows(x).Cells("Column6").Value = True
                ElseIf Trim(dgvDocumentos.Rows(x).Cells("PPago").Value) = "0" Then
                    dgvDocumentos.Rows(x).Cells("Column6").Value = False
                End If
            Next
        End If
    End Sub

    Private Sub dgvDocumentos_ColumnHeaderMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvDocumentos.ColumnHeaderMouseClick
        cargarCentrosDeCostoxEmpresa()
        Pintar()
        For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
            If Trim(dgvDocumentos.Rows(y).Cells("Column5").Value) <> "" Then
                dgvDocumentos.Rows(y).Cells("IdCC").Value = dgvDocumentos.Rows(y).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
            End If
        Next
    End Sub

    Private Sub dgvDocumentos_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDocumentos.EditingControlShowing
        'm_EditingRow = dgvDocumentos.CurrentRow.Index
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception

        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvDocumentos.CurrentCell.ColumnIndex

        If columna = 8 Or columna = 10 Or columna = 11 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

        If columna = 9 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   

            If caracter = "r" Or caracter = "R" Or caracter = "d" Or caracter = "D" Or caracter = "p" Or caracter = "P" Or caracter = ChrW(Keys.Back) Then
                If (caracter = ChrW(Keys.Back)) Or (caracter = "r") And (txt.Text.Contains("r") = False) Or (caracter = "R") And (txt.Text.Contains("R") = False) Or (caracter = "D") And (txt.Text.Contains("D") = False) Or (caracter = "d") And (txt.Text.Contains("d") = False) Or (caracter = "P") And (txt.Text.Contains("P") = False) Or (caracter = "p") And (txt.Text.Contains("p") = False) Then
                    e.Handled = False
                    Exit Sub
                Else
                    e.Handled = True
                    Exit Sub
                End If
            Else
                e.Handled = True
                Exit Sub
            End If
        End If

        If columna = 12 Then
            'CadenaOrden = ""
            Dim caracter As Char = e.KeyChar
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back) Or (caracter = "-")) Then
                e.Handled = False
                eRegistroDocumento = New clsRegistroDocumento
                If Len(caracter) > 0 And caracter <> ChrW(Keys.Back) Then
                 
                ElseIf Len(caracter) = 0 And caracter <> ChrW(Keys.Back) Then
                  
                End If
            Else
                e.Handled = True
            End If

        End If

    End Sub

    Private Sub dgvDocumentos_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDocumentos.MouseUp
        If dgvDocumentos.Rows.Count > 0 Then
            Dim iFila As Integer
            Calcular()
            If dgvDocumentos.Rows.Count > 0 And dgvDocumentos.CurrentCell.ColumnIndex = 14 Then
                iFila = dgvDocumentos.CurrentRow.Index
                dgvDocumentos.CurrentCell = dgvDocumentos(0, 0)
                dgvDocumentos.CurrentCell = dgvDocumentos(14, iFila)

                If dgvDocumentos.Rows.Count > 0 Then
                    Dim iFila2 As Integer
                    iFila2 = dgvDocumentos.CurrentRow.Index
                    If Convert.ToBoolean(dgvDocumentos.Rows(iFila2).Cells("X").Value) = True Then
                        If dgvDocumentos.Rows(iFila2).Cells(13).Value = "" Then
                            MessageBox.Show("Seleccione un Centro de Costo para el Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            dgvDocumentos.Rows(iFila2).Cells("X").Value = False
                            dgvDocumentos.CurrentCell = dgvDocumentos(0, iFila2)
                            dgvDocumentos.CurrentCell = dgvDocumentos(13, iFila2)
                            Exit Sub
                        End If
                    End If
                End If

            End If
        End If
    End Sub

    Private Sub txtNroOrden_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroOrden.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                dgvOrdenes.Focus()
        End Select
    End Sub

    Private Sub txtNroOrden_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroOrden.TextChanged
        Dim CodEmpresa As String = ""
        If dgvDocumentos.Rows.Count > 0 Then
            CodEmpresa = Trim(dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("Column2").Value)
        End If
        If Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = False Then
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenes2(CodEmpresa, Trim(txtNroOrden.Text), Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = False Then
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenes(CodEmpresa, Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) > 0 And chkVerTodo.Checked = True Then
            Panel7.Visible = True
            eRegistroDocumento = New clsRegistroDocumento
            Dim dtOrdenes As DataTable
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(43, CodEmpresa, Trim(txtNroOrden.Text), Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        ElseIf Len(Trim(txtNroOrden.Text)) = 0 And chkVerTodo.Checked = True Then
            Panel7.Visible = True
            Dim dtOrdenes As DataTable
            eRegistroDocumento = New clsRegistroDocumento
            dtOrdenes = New DataTable
            dtOrdenes = eRegistroDocumento.fListarOrdenesTodos(42, CodEmpresa, Trim(txtNroOrden.Text), Trim(txtRucProv.Text))
            dgvOrdenes.DataSource = dtOrdenes
        End If
    End Sub

    Private Sub BeLabel44_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel44.Click
        Panel7.Visible = False
        CadenaOrden = ""
    End Sub

    Private Sub dgvOrdenes_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.DoubleClick
        Dim sCodigoActualLinea As String = ""
        Dim iResultadoGrabLinea As Int32 = 0
        If dgvOrdenes.Rows.Count > 0 Then
            Dim CodOrden As String = ""
            Dim NroOrden As String = ""
            Dim CodProvLogistica As String = ""
            Dim xCC As String = ""
            Try
                Dim sCodProvRUC As String = ""
                sCodProvRUC = dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(6).Value
                If Len(Trim(sCodProvRUC)) = 11 And Len(Trim(txtCodigo.Text)) = 11 Then
                    If Trim(sCodProvRUC) <> Trim(txtCodigo.Text) Then
                        MessageBox.Show("Este N�mero de Orden de Compra no Pertenece al Proveedor " & Trim(txtRazonProv.Text), "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtNroOrden.Focus()
                        Exit Sub
                    End If
                End If

                CodOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(4).Value)
                NroOrden = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(0).Value)
                CodProvLogistica = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(5).Value)
                xCC = Trim(dgvOrdenes.Rows(dgvOrdenes.CurrentRow.Index).Cells(7).Value)

                If dgvDocumentos.Rows.Count > 0 Then
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("OC").Value = Trim(NroOrden)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("CodigoOC").Value = Trim(CodOrden)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("CosCodigo").Value = Trim(xCC)
                    dgvDocumentos.Rows(dgvDocumentos.CurrentRow.Index).Cells("IdCC").Value = Trim(xCC)
                    If Len(Trim(NroOrden)) > 0 And Len(Trim(txtIdProv.Text)) > 0 And Len(Trim(CodProvLogistica)) > 0 Then
                        eRegistroDocumento = New clsRegistroDocumento
                        Dim dtBuscarLinea As DataTable
                        dtBuscarLinea = New DataTable
                        dtBuscarLinea = eRegistroDocumento.VerificarLinea(Trim(txtIdProv.Text), gEmpresa)
                        If dtBuscarLinea.Rows.Count = 0 Then
                            eRegistroDocumento.fCodigoLinea(gEmpresa)
                            sCodigoActualLinea = eRegistroDocumento.sCodFuturoLinea
                            iResultadoGrabLinea = eRegistroDocumento.fGrabarLinea(39, sCodigoActualLinea, Trim(CodProvLogistica), gEmpresa, 0, 0, 0, Trim(txtIdProv.Text))
                            If iResultadoGrabLinea = 1 Then
                                'txtLineaCreSoles.Enabled = True
                                'txtLineaCreDolares.Enabled = True
                                'txtDiasCred.Enabled = True
                                'txtIdLinea.Enabled = True
                                'txtIdLinea.Text = sCodigoActualLinea
                            End If
                        End If
                    End If

                End If
                Panel4.Visible = False
                CadenaOrden = ""
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub dgvOrdenes_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvOrdenes.EditingControlShowing
        m_EditingRow3 = dgvOrdenes.CurrentRow.Index
    End Sub

    Private Sub dgvOrdenes_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvOrdenes.KeyDown
        If dgvOrdenes.Rows.Count > 0 Then
            If e.KeyCode = Keys.Return Then
                Dim cur_cell As DataGridViewCell = dgvOrdenes.CurrentCell
                Dim col As Integer = cur_cell.ColumnIndex
                col = (col + 1) Mod dgvOrdenes.Columns.Count
                cur_cell = dgvOrdenes.CurrentRow.Cells(col)

                If cur_cell.Visible = False Then
                    Exit Sub
                End If
                dgvOrdenes.CurrentCell = cur_cell
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvOrdenes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvOrdenes.KeyPress
        Dim CodOrden As String = ""
        Dim NroOrden As String = ""
        Select Case Asc(e.KeyChar)
            Case 13
                dgvOrdenes_DoubleClick(sender, e)
        End Select
    End Sub

    Private Sub dgvOrdenes_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvOrdenes.SelectionChanged
        If m_EditingRow3 >= 0 Then
            Dim new_row As Integer = m_EditingRow3
            m_EditingRow3 = -1
            dgvOrdenes.CurrentCell = dgvOrdenes.Rows(new_row).Cells(dgvOrdenes.CurrentCell.ColumnIndex)
        End If
    End Sub

    Private Sub chkVerTodo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVerTodo.CheckedChanged
        txtNroOrden_TextChanged(sender, e)
        txtNroOrden.Focus()
    End Sub

    Private Sub txtLineaCre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLineaCre.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtIdLinea.Text)) > 0 Then
                    ePagoProveedores = New clsPagoProveedores
                    Dim ActualizaLineaCre As Integer = 0
                    Dim Dias As Integer = 0
                    Dim Credito As Double = 0
                    If Len(Trim(txtDiasCred.Text)) = 0 Then
                        Dias = 0
                    Else
                        Dias = Trim(txtDiasCred.Text)
                    End If
                    If Len(Trim(txtLineaCre.Text)) = 0 Then
                        Credito = 0
                    Else
                        Credito = Trim(txtLineaCre.Text)
                    End If
                    If sMonCodigo = "01" Then
                        ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito2(29, Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), Credito, 0)
                    ElseIf sMonCodigo = "02" Then
                        ActualizaLineaCre = ePagoProveedores.ActualizaLineaCredito2(28, Trim(txtIdLinea.Text), gEmpresa, Trim(Dias), Credito, 0)
                    End If
                End If
        End Select
    End Sub

    Private Sub chkDocTodas_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDocTodas.CheckedChanged
        Dim dSaldoProv As Double = 0
        txtConcepto.Clear()
        txtMonto.Clear()
        'chkDocTodas.Checked


        objResultado = VerRuc(txtRucProv.Text)

        If (objResultado.dtResultado.Rows(0)("Estado").ToString() = "ACTIVO" And objResultado.dtResultado.Rows(0)("Condicion").ToString() = "HABIDO") Then

            If Len(Trim(txtRucProv.Text)) = 11 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                AplicarPintado = 1
                Dim dtTable As DataTable
                Dim IdProveedor As String = ""
                Dim RucProveedor As String = ""
                Dim RazonProveedor As String = ""
                Dim DireccionProveedor As String = ""
                dtTable = New DataTable
                ePagoProveedores = New clsPagoProveedores
                dtTable = ePagoProveedores.fTraerDatosProveedor2(Trim(txtRucProv.Text), "P", gEmpresa)
                If dtTable.Rows.Count > 0 Then
                    IdProveedor = dtTable.Rows(0).Item("AnaliticoCodigo")
                    DireccionProveedor = dtTable.Rows(0).Item("Direccion")
                    RucProveedor = dtTable.Rows(0).Item("RUC")
                    RazonProveedor = dtTable.Rows(0).Item("AnaliticoDescripcion")
                    txtIdProv.Text = Trim(IdProveedor)
                    txtRucProv.Text = Trim(RucProveedor)
                    txtDireccionProv.Text = Trim(DireccionProveedor)
                    txtRazonProv.Text = Trim(RazonProveedor)
                    Dim dtTableLineCre As DataTable
                    dtTableLineCre = New DataTable
                    dtTableLineCre = ePagoProveedores.fTraerLineaDeCreditoProv(gEmpresa, Trim(IdProveedor))
                    If dtTableLineCre.Rows.Count > 0 Then
                        If sMonCodigo = "01" Then
                            GroupBox11.Text = "Linea de Cr�dito Soles"
                            txtLineaCre.Text = Format(dtTableLineCre.Rows(0).Item("SaldoSolesLinea"), "#,##0.00")
                        ElseIf sMonCodigo = "02" Then
                            GroupBox11.Text = "Linea de Cr�dito Dolares"
                            txtLineaCre.Text = Format(dtTableLineCre.Rows(0).Item("SaldoDolaresLinea"), "#,##0.00")
                        End If
                        txtDiasCred.Text = Trim(dtTableLineCre.Rows(0).Item("DiasCredito"))
                        txtIdLinea.Text = Trim(dtTableLineCre.Rows(0).Item("IdLinea"))
                        txtLineaCre.Enabled = True
                        txtDiasCred.Enabled = True
                        txtIdLinea.Enabled = True
                    Else
                        GroupBox11.Text = "Linea de Cr�dito"
                        txtLineaCre.Text = "0.00"
                        txtDiasCred.Clear()
                        txtIdLinea.Clear()
                        txtLineaCre.Enabled = False
                        txtDiasCred.Enabled = False
                        txtIdLinea.Enabled = False
                    End If
                    Dim dtDocumentos As DataTable
                    dtDocumentos = New DataTable
                    ePagoProveedores = New clsPagoProveedores

                    If chkDocTodas.Checked = True Then
                        '''''''''''''''nuevo
                        If cboOtrasEmpresas.SelectedIndex = -1 Then
                            ePagoProveedores = New clsPagoProveedores
                            cboOtrasEmpresas.DataSource = ePagoProveedores.fTraerDocumentosPorMoneda(33, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                            If ePagoProveedores.iNroRegistros > 0 Then
                                cboOtrasEmpresas.ValueMember = "EmprCodigo"
                                cboOtrasEmpresas.DisplayMember = "EmprAbreviado"
                                cboOtrasEmpresas.SelectedIndex = 0
                            End If
                        End If
                        ePagoProveedores = New clsPagoProveedores
                        If cboOtrasEmpresas.SelectedIndex > -1 Then
                            dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(29, Trim(txtRucProv.Text), "P", Trim(cboOtrasEmpresas.SelectedValue), Trim(sMonCodigo))
                        End If
                        ''''''''''''''fin nuevo
                        'dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(31, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                    ElseIf chkDocTodas.Checked = False Then
                        cboOtrasEmpresas.DataSource = Nothing
                        dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(29, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                    End If

                    dgvDocumentos.AutoGenerateColumns = False
                    dgvDocumentos.DataSource = dtDocumentos
                    dgvDocumentos.Columns("ImporteTotal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns("aPagar").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns("Porcentaje").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    'Dim y As Integer
                    If dtDocumentos.Rows.Count > 0 Then
                        lblTotDocs.Text = "Cant. Docs. : " & dtDocumentos.Rows.Count
                        'cargarCentrosDeCosto()
                        cargarCentrosDeCostoxEmpresa()
                        Panel1.Visible = True
                        For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Trim(dgvDocumentos.Rows(y).Cells("Column5").Value) <> "" Then
                                dgvDocumentos.Rows(y).Cells("IdCC").Value = dgvDocumentos.Rows(y).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
                            End If
                        Next
                        Pintar()

                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) = Trim(gEmpresa) Then
                                Dim StrCad As String = String.Empty
                                StrCad = Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value)
                                If StrCad = Nothing Then
                                    dSaldoProv = dSaldoProv + 0
                                Else
                                    dSaldoProv = dSaldoProv + dgvDocumentos.Rows(x).Cells("ImporteTotal").Value
                                End If
                            End If
                        Next

                        Dim DifSoles As Decimal = 0
                        DifSoles = Convert.ToDouble(txtLineaCre.Text) - dSaldoProv
                        txtEstadoCre.Text = Format(DifSoles, "#,##0.00")
                        If DifSoles >= (Convert.ToDouble(txtLineaCre.Text) / 2) Then
                            txtEstadoCre.BackColor = Color.Green
                        ElseIf DifSoles < (Convert.ToDouble(txtLineaCre.Text) / 2) And DifSoles > 0 Then
                            txtEstadoCre.BackColor = Color.Yellow
                        ElseIf DifSoles <= 0 Then
                            txtEstadoCre.BackColor = Color.Red
                        End If
                        If Trim(txtLineaCre.Text) = "0.00" Then
                            txtEstadoCre.BackColor = Color.White
                        End If
                        If txtLineaCre.Enabled = False Then
                            txtEstadoCre.Clear()
                        End If
                        dgvDocumentos.Focus()
                    ElseIf dtDocumentos.Rows.Count = 0 Then
                        'MessageBox.Show("No existen documentos en moneda " & txtCodMoneda.Text.Trim & " para pagar a este proveedor.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        lblTotDocs.Text = "Cant. Docs. : 0"
                        If dgvDocumentos.Rows.Count > 0 Then
                            For x As Integer = 0 To dgvDocumentos.RowCount - 1
                                dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                            Next
                        End If
                    End If
                Else
                    Dim dtDocumentos As DataTable
                    dtDocumentos = New DataTable
                    ePagoProveedores = New clsPagoProveedores
                    If chkDocTodas.Checked = True Then
                        dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(31, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                    ElseIf chkDocTodas.Checked = False Then
                        dtDocumentos = ePagoProveedores.fTraerDocumentosPorMoneda(29, Trim(txtRucProv.Text), "P", gEmpresa, Trim(sMonCodigo))
                    End If
                    dgvDocumentos.AutoGenerateColumns = False
                    dgvDocumentos.DataSource = dtDocumentos
                    dgvDocumentos.Columns("ImporteTotal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns("Saldo").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns("aPagar").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    dgvDocumentos.Columns("Porcentaje").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    If dtDocumentos.Rows.Count > 0 Then
                        lblTotDocs.Text = "Cant. Docs. : " & dtDocumentos.Rows.Count
                        cargarCentrosDeCostoxEmpresa()
                        Panel1.Visible = True
                        For y As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Trim(dgvDocumentos.Rows(y).Cells("Column5").Value) <> "" Then
                                dgvDocumentos.Rows(y).Cells("IdCC").Value = dgvDocumentos.Rows(y).Cells("Column5").Value '"" 'Trim(dgvDocumentos.Rows(y).Cells("Column5").Value)
                            End If
                        Next
                        Pintar()
                        For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                            If Trim(dgvDocumentos.Rows(x).Cells("Column2").Value) = Trim(gEmpresa) Then
                                Dim StrCad As String = String.Empty
                                StrCad = Trim(dgvDocumentos.Rows(x).Cells("ImporteTotal").Value)
                                If StrCad = Nothing Then
                                    dSaldoProv = dSaldoProv + 0
                                Else
                                    dSaldoProv = dSaldoProv + dgvDocumentos.Rows(x).Cells("ImporteTotal").Value
                                End If
                            End If
                        Next
                        Dim DifSoles As Decimal = 0
                        DifSoles = Convert.ToDouble(Val(txtLineaCre.Text)) - dSaldoProv
                        txtEstadoCre.Text = Format(DifSoles, "#,##0.00")
                        If DifSoles >= (Convert.ToDouble(Val(txtLineaCre.Text)) / 2) Then
                            txtEstadoCre.BackColor = Color.Green
                        ElseIf DifSoles < (Convert.ToDouble(Val(txtLineaCre.Text)) / 2) And DifSoles > 0 Then
                            txtEstadoCre.BackColor = Color.Yellow
                        ElseIf DifSoles <= 0 Then
                            txtEstadoCre.BackColor = Color.Red
                        End If
                        If Trim(txtLineaCre.Text) = "0.00" Then
                            txtEstadoCre.BackColor = Color.White
                        End If
                        If txtLineaCre.Enabled = False Then
                            txtEstadoCre.Clear()
                        End If
                        dgvDocumentos.Focus()
                    ElseIf dtDocumentos.Rows.Count = 0 Then
                        'MessageBox.Show("No existen documentos en moneda " & txtCodMoneda.Text.Trim & " para pagar a este proveedor", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        lblTotDocs.Text = "Cant. Docs. : 0"
                        If dgvDocumentos.Rows.Count > 0 Then
                            For x As Integer = 0 To dgvDocumentos.RowCount - 1
                                dgvDocumentos.Rows.Remove(dgvDocumentos.CurrentRow)
                            Next
                        End If
                        txtRuc.Clear()
                        txtRucProv.Clear()
                        txtIdProv.Clear()
                        txtRazonProv.Clear()
                        txtDireccionProv.Clear()
                        txtNroDoc.Clear()
                        txtTotPagarProv.Clear()
                        txtIdLinea.Clear()
                        txtLineaCre.Clear()
                        txtDiasCred.Clear()
                        txtEstadoCre.Clear()
                        Panel1.Visible = True
                        txtRucProv.Focus()
                    End If
                End If
            Else
                'OpcionMostrar = "Boton"
                If chkDocTodas.Checked = True Then
                    If cboBanco.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboBanco.Focus()
                        Exit Sub
                    End If
                    If cboCuenta.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un N�mero de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboCuenta.Focus()
                        Exit Sub
                    End If
                    If Len(txtRucProv.Text.Trim) = 0 Then
                        MessageBox.Show("Ingrese un N�mero de Ruc", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtRucProv.Focus()
                        Exit Sub
                    End If
                    If Len(txtRucProv.Text.Trim) > 0 And Len(txtRucProv.Text.Trim) < 11 Then
                        MessageBox.Show("Ingrese un Ruc V�lido", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtRucProv.Focus()
                        Exit Sub
                    End If
                End If

            End If
        Else
            txtRucProv.Text = ""
            Dim OB_frmValidarRuc As frmValidarRuc = New frmValidarRuc(objResultado.dtResultado.Rows(0)("Ruc").ToString(), objResultado.dtResultado.Rows(0)("RazonSocial").ToString(), objResultado.dtResultado.Rows(0)("Estado").ToString(), objResultado.dtResultado.Rows(0)("Condicion").ToString(), objResultado.dtResultado.Rows(0)("Direccion").ToString())
            OB_frmValidarRuc.ShowDialog()
        End If
        Panel7.Visible = False
    End Sub

    Private Sub btnLetra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLetra.Click
        If Panel10.Visible = False Then
            CalcularLetra()
            If dgvDocumentos.Rows.Count = 0 Then
                MessageBox.Show("No Existe Documentos para Asignar a una Letra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            ElseIf dgvDocumentos.Rows.Count > 0 Then
                If CountDocumentosLetra = 0 Then
                    MessageBox.Show("Seleccione documentos para pagar con Letra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
            HabilitarLetra()
            txtNroLetra.Focus()
        ElseIf Panel10.Visible = True Then
            Panel10.Visible = False
        End If
    End Sub

    Sub HabilitarLetra()
        txtNroLetra.Clear()
        txtLugarGiro.Clear()
        dtpFechaGiro.Value = Now.Date().ToShortDateString()
        dtpFechaVen.Value = Now.Date().ToShortDateString()
        txtNroLetra.Focus()
        Panel10.Visible = True
    End Sub

    Sub DesHabilitarLetra()
        txtNroLetra.Clear()
        txtLugarGiro.Clear()
        txtReferenciaLetra.Clear()
        txtCanDocLetra.Clear()
        txtTotalLetra.Clear()
        dtpFechaGiro.Value = Now.Date().ToShortDateString()
        dtpFechaVen.Value = Now.Date().ToShortDateString()
        Panel10.Visible = False
    End Sub

    Sub CalcularLetra()
        Dim strGlosaFacturasLetra As String = ""
        Dim dblTotalLetra As Double = 0
        CountDocumentosLetra = 0
        Dim CantidadDetraccionesSoles As Integer = 0
        If dgvDocumentos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then
                    If Trim(sMonCodigo) = "01" Then
                        If Trim(dgvDocumentos.Rows(x).Cells("DP").Value) = "D" Or Trim(dgvDocumentos.Rows(x).Cells("DP").Value) = "d" Then
                            CantidadDetraccionesSoles = CantidadDetraccionesSoles + 1
                        End If
                    End If

                    Dim StrCad As String = String.Empty
                    StrCad = Trim(dgvDocumentos.Rows(x).Cells("aPagar").Value)
                    If StrCad = Nothing Then
                        dblTotalLetra = dblTotalLetra + 0
                    Else
                        dblTotalLetra = dblTotalLetra + dgvDocumentos.Rows(x).Cells("aPagar").Value
                    End If
                    CountDocumentosLetra = CountDocumentosLetra + 1
                    strGlosaFacturasLetra = strGlosaFacturasLetra & " " & dgvDocumentos.Rows(x).Cells("Doc").Value & " " & dgvDocumentos.Rows(x).Cells("NroDocumento").Value & "  "
                End If
            Next

        End If
        txtCanDocLetra.Text = Trim(CountDocumentosLetra)
        txtReferenciaLetra.Text = Trim(strGlosaFacturasLetra)
        If Trim(sMonCodigo) = "01" Then
            'txtConcepto.Text = Trim(strGlosaFacturasSoles)
            If CantidadDetraccionesSoles = 0 Then
                txtTotalLetra.Text = Format(dblTotalLetra, "#,##0.00")
            ElseIf CantidadDetraccionesSoles > 0 Then
                txtTotalLetra.Text = Format(Math.Round(dblTotalLetra, 0), "#,##0.00")
            End If
        ElseIf Trim(sMonCodigo) = "02" Then
            'txtConcepto.Text = Trim(strGlosaFacturasDolares)
            txtTotalLetra.Text = Format(dblTotalLetra, "#,##0.00")
        End If
    End Sub

    Private Sub btnGrabarLetra_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabarLetra.Click
        If dtpFechaVen.Value < dtpFechaGiro.Value Then
            MessageBox.Show("La Fecha de Giro es Mayor a la Fecha de Vencimiento, Ingrese una feha igual o menor.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaGiro.Focus()
            Exit Sub
        End If
        ePagoProveedores = New clsPagoProveedores
        Dim DetrPer, OrdCom, CCostoCod, IdOrden As String
        Dim Monto As Double
        If dgvDocumentos.Rows.Count > 0 Then
            'CalcularLetra()
            Dim dtTableNumeroLetra As DataTable
            dtTableNumeroLetra = New DataTable
            Dim iResLetra As Integer = 0
            If CountDocumentosLetra > 0 Then
                dtTableNumeroLetra = ePagoProveedores.fTraerResExisteLetraProv("P", gEmpresa, Trim(txtIdProv.Text), Trim(txtNroLetra.Text))
                iResLetra = dtTableNumeroLetra.Rows.Count
            End If
            Dim IdLetra As String = ""
            If iResLetra = 0 Then
                If (MessageBox.Show("�Esta seguro de Grabar la Letra?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    ePagoProveedores.GenerarCodLetra(gEmpresa)
                    IdLetra = ePagoProveedores.sCodFuturo
                    Dim iResultado As Integer
                    iResultado = ePagoProveedores.GrabarLetra(25, IdLetra, Trim(txtNroLetra.Text), gEmpresa, dtpFechaGiro.Value, dtpFechaVen.Value, Trim(txtLugarGiro.Text), Trim(txtReferenciaLetra.Text), Trim(txtCanDocLetra.Text), txtTotalLetra.Text, txtTotalLetra.Text, "P", Trim(txtIdProv.Text), Trim(txtSemanaPago.Text), gPeriodo, Trim(sMonCodigo), gUsuario)
                    If iResultado = 1 Then
                        If dgvDocumentos.Rows.Count > 0 Then
                            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then

                                    Dim IdRegistro As String = ""
                                    Dim EmprCod As String = ""
                                    Dim IdCronogramaDoc As String = ""
                                    Dim Saldo As Double = 0
                                    Dim iResultado2 As Integer = 0
                                    IdCronogramaDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                    EmprCod = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                                    DetrPer = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                    Monto = Val(Trim(dgvDocumentos.Rows(x).Cells("Porcentaje").Value))
                                    CCostoCod = Trim(dgvDocumentos.Rows(x).Cells("IdCC").Value)
                                    OrdCom = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                    IdOrden = Trim(dgvDocumentos.Rows(x).Cells("CodigoOC").Value)
                                    If Trim(EmprCod) = Trim(gEmpresa) Then
                                        If Saldo = 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                        End If
                                    ElseIf Trim(EmprCod) <> Trim(gEmpresa) Then
                                        If Saldo = 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, "", 1, 1, Saldo, CCostoCod, IdOrden, "00003", 1, gEmpresa)
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, "", 0, 0, Saldo, CCostoCod, IdOrden, "00001", 1, gEmpresa)
                                        End If
                                    End If
                                    If Trim(EmprCod) = Trim(gEmpresa) Then
                                        ePagoProveedores.ActSaldoDoc(26, IdCronogramaDoc, EmprCod, 0, Trim(IdLetra), "")
                                    ElseIf Trim(EmprCod) <> Trim(gEmpresa) Then
                                        ePagoProveedores.ActSaldoDoc(30, IdCronogramaDoc, EmprCod, 0, Trim(IdLetra), "")
                                    End If
                                End If
                            Next
                        End If
                        MessageBox.Show("Se Grabo la Letra con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        chkDocTodas_CheckedChanged(sender, e)
                        DesHabilitarLetra()
                    End If
                End If
            ElseIf iResLetra = 1 Then
                If (MessageBox.Show("Ya Existe la Letra N� " & Trim(txtNroLetra.Text) & " para este Proveedor. �Esta seguro de incluir los documentos seleccionados y Actualizar la Letra a Pagar?", "Pago de Proveedores", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    Dim SaldoNew As Double = 0
                    Dim TotalNew As Double = 0
                    Dim CantidadNew As Integer = 0
                    Dim RefereNewLetra As String = ""
                    IdLetra = Trim(dtTableNumeroLetra.Rows(0).Item("IdLetra"))
                    TotalNew = dtTableNumeroLetra.Rows(0).Item("Total") + txtTotalLetra.Text
                    SaldoNew = dtTableNumeroLetra.Rows(0).Item("Saldo") + txtTotalLetra.Text
                    RefereNewLetra = Trim(dtTableNumeroLetra.Rows(0).Item("ReferenciaGiro")) & " " & Trim(txtReferencia.Text)
                    CantidadNew = dtTableNumeroLetra.Rows(0).Item("CantidadDocs") + txtCanDocLetra.Text

                    Dim iResultado As Integer
                    iResultado = ePagoProveedores.GrabarLetra(27, IdLetra, Trim(txtNroLetra.Text), gEmpresa, dtpFechaGiro.Value, dtpFechaVen.Value, Trim(txtLugarGiro.Text), RefereNewLetra, CantidadNew, TotalNew, SaldoNew, "P", Trim(txtIdProv.Text), Trim(txtSemanaPago.Text), gPeriodo, Trim(sMonCodigo), gUsuario)
                    If iResultado = 1 Then
                        If dgvDocumentos.Rows.Count > 0 Then
                            For x As Integer = 0 To dgvDocumentos.Rows.Count - 1
                                If Convert.ToBoolean(dgvDocumentos.Rows(x).Cells("X").Value) = True Then

                                    Dim IdRegistro As String = ""
                                    Dim IdCronogramaDoc As String = ""
                                    Dim EmprCod As String = ""
                                    Dim Saldo As Double = 0
                                    Dim iResultado2 As Integer = 0
                                    IdCronogramaDoc = Trim(dgvDocumentos.Rows(x).Cells("IdRegistro").Value)
                                    EmprCod = Trim(dgvDocumentos.Rows(x).Cells("Column2").Value)
                                    DetrPer = Trim(dgvDocumentos.Rows(x).Cells("DP").Value)
                                    Monto = Val(Trim(dgvDocumentos.Rows(x).Cells("Porcentaje").Value))
                                    CCostoCod = Trim(dgvDocumentos.Rows(x).Cells("IdCC").Value)
                                    OrdCom = Trim(dgvDocumentos.Rows(x).Cells("OC").Value)
                                    IdOrden = Trim(dgvDocumentos.Rows(x).Cells("CodigoOC").Value)

                                    If Trim(EmprCod) = Trim(gEmpresa) Then
                                        If Saldo = 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 1, 1, Saldo, CCostoCod, IdOrden, "00003", 0, "")
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, IdResumen, 0, 0, Saldo, CCostoCod, IdOrden, "00001", 0, "")
                                        End If
                                    ElseIf Trim(EmprCod) <> Trim(gEmpresa) Then
                                        If Saldo = 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, "", 1, 1, Saldo, CCostoCod, IdOrden, "00003", 1, gEmpresa)
                                        ElseIf Saldo > 0 Then
                                            iResultado2 = ePagoProveedores.ActualizarDocumentos(0, EmprCod, IdCronogramaDoc, DetrPer, Monto, OrdCom, 1, "", 0, 0, Saldo, CCostoCod, IdOrden, "00001", 1, gEmpresa)
                                        End If
                                    End If
                                    If Trim(EmprCod) = Trim(gEmpresa) Then
                                        ePagoProveedores.ActSaldoDoc(26, IdCronogramaDoc, EmprCod, 0, Trim(IdLetra), "")
                                    ElseIf Trim(EmprCod) <> Trim(gEmpresa) Then
                                        ePagoProveedores.ActSaldoDoc(30, IdCronogramaDoc, EmprCod, 0, Trim(IdLetra), "")
                                    End If
                                End If
                            Next
                        End If
                        MessageBox.Show("Se Actualizo la Letra con Exito", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        chkDocTodas_CheckedChanged(sender, e)
                        DesHabilitarLetra()
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub dgvLetras_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvLetras.KeyDown
        Select Case e.KeyCode
            Case Keys.Delete
                If dgvLetras.Rows.Count > 0 Then
                    Dim IdLetraEliminar As String = ""
                    Dim iResDeleteLetra As Integer
                    IdLetraEliminar = Trim(dgvLetras.Rows(dgvLetras.CurrentRow.Index).Cells(9).Value)
                    eSesionCajas = New clsSesionCajas
                    If (MessageBox.Show("�Esta seguro de Eliminar esta Letra?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        iResDeleteLetra = eSesionCajas.fActualizarLetras(10, 0, IdLetraEliminar, Trim(gEmpresa), 0, gUsuario, Today(), Today())
                    End If
                    If iResDeleteLetra > 1 Then
                        txtBuscarLetra_TextChanged(sender, e)
                    End If
                End If
        End Select
    End Sub

    Private Sub txtBuscar_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBuscar.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If Len(Trim(txtBuscar.Text)) = 11 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                    'txtRucProv.Text = Trim(txtRuc.Text)
                    'txtBuscar.Text = Trim(txtRuc.Text)
                    txtBuscar_TextChanged(sender, e)
                    'chkDocTodas_CheckedChanged(sender, e)
                    'Panel1.Visible = True
                    'TabControl2.TabPages(0).Controls.Owner.Focus()
                    'TabControl2.TabPages(0).Focus()

                Else
                    If cboBanco.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboBanco.Focus()
                        Exit Sub
                    End If
                    If cboCuenta.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un N�mero de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cboCuenta.Focus()
                        Exit Sub
                    End If
                    If Len(txtBuscar.Text.Trim) = 0 Then
                        'Panel1.Visible = False
                        MessageBox.Show("Ingrese un N�mero de Ruc", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtBuscar.Focus()
                        Exit Sub
                    End If

                    If Len(txtBuscar.Text.Trim) > 0 And Len(txtBuscar.Text.Trim) < 11 Then
                        'Panel1.Visible = False
                        MessageBox.Show("Ingrese un Ruc V�lido", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        txtBuscar.Focus()
                        Exit Sub
                    End If
                End If
                'Case 13
                'BeButton6.Focus()
        End Select
    End Sub

    Private Sub txtBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscar.TextChanged

        Dim dtListadeVal2 As DataTable
        eValorizaciones = New clsValorizaciones
        dtListadeVal2 = New DataTable
        txtRazon.Clear()

        For x As Integer = 0 To dgvDetalleVal.RowCount - 1
            dgvDetalleVal.Rows.Remove(dgvDetalleVal.CurrentRow)
        Next

        dgvDetalleVal.AutoGenerateColumns = False

        If Len(Trim(txtBuscar.Text)) = 11 Then

            dtListadeVal2 = eValorizaciones.fListarObras(57, gEmpresa, gPeriodo, "", Trim(txtBuscar.Text), dtpFechaGasto.Value, "")
            If dtListadeVal2.Rows.Count > 0 Then
                dgvDetalleVal.DataSource = dtListadeVal2
                txtRazon.Text = dtListadeVal2.Rows(0).Item("Contratista")
            End If
        End If

    End Sub

    Sub CalcularValorizaciones2()
        'strGlosaVal = ""
        'strGlosaContrato = ""
        strGlosaContratista = ""
        CountVal = 0
        dblMontoVal = 0
        If dgvDetalleVal.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDetalleVal.Rows.Count - 1
                If Convert.ToBoolean(dgvDetalleVal.Rows(x).Cells("XX").Value) = True Then
                    Mov = "V"
                    Dim StrCad As String = String.Empty
                    StrCad = Trim(dgvDetalleVal.Rows(x).Cells("APagarF").Value)
                    If StrCad = Nothing Then
                        dblMontoVal = dblMontoVal + 0
                    Else
                        dblMontoVal = dblMontoVal + dgvDetalleVal.Rows(x).Cells("APagarF").Value
                    End If
                    CountVal = CountVal + 1
                    strGlosaContrato = Trim(dgvDetalleVal.Rows(x).Cells("ObraDescripcion").Value)
                    strGlosaContratista = Trim(dgvDetalleVal.Rows(x).Cells("Contratista").Value)
                End If
            Next
            If CountVal > 0 Then
                strGlosaVal = "PAGO DE VALORIZACIONES - " & strGlosaContrato
                txtBeneficiario.Text = Trim(strGlosaContratista)
                txtConcepto.Text = Trim(strGlosaVal)
                txtMonto.Text = Format(dblMontoVal, "#,##0.00")
            ElseIf CountVal = 0 Then
                Mov = ""
                txtBeneficiario.Clear()
                txtConcepto.Clear()
                txtBeneficiario.Clear()
                txtMonto.Text = "0.00"
            End If
        End If
    End Sub

    'Private Sub validar_KeypressValorizaciones(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
    '    Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
    '    If columna = 2 Or columna = 3 Or columna = 1 Then
    '        Dim caracter As Char = e.KeyChar
    '        ' referencia a la celda   
    '        Dim txt As TextBox = CType(sender, TextBox)
    '        ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
    '        ' es el separador decimal, y que no contiene ya el separador   
    '        If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
    '            e.Handled = False
    '        Else
    '            e.Handled = True
    '        End If
    '    End If
    'End Sub

    'Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
    '    Dim StrCad As String = String.Empty
    '    Dim StrImporteDoc As String = String.Empty
    '    If e.ColumnIndex = 13 Then
    '        StrCad = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValAPagar").Value)
    '        StrImporteDoc = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValSaldoOculto").Value)
    '        Try
    '            If Convert.ToDouble(StrCad) > Convert.ToDouble(StrImporteDoc) Then
    '                MessageBox.Show("El Monto a Pagar Supera al Importe de la Valorizaci�n", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValAPagar").Value = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValSaldoOculto").Value
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValPor").Value = "0.00"
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImpPor").Value = "0.00"
    '                dgvDetalle.CurrentCell = dgvDetalle(13, dgvDetalle.CurrentRow.Index)
    '            Else
    '                Dim resta As Double = 0
    '                resta = Convert.ToDouble(StrImporteDoc) - Convert.ToDouble(StrCad)
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValSaldo").Value = resta 'Format(CDec(resta), "0.00")
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValAPagar").Value = StrCad 'Format(CDec(StrCad), "0.00")
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValPor").Value = "0.00"
    '                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImpPor").Value = "0.00"
    '                dgvDetalle.CurrentCell = dgvDetalle(13, dgvDetalle.CurrentRow.Index)
    '            End If
    '        Catch ex As Exception
    '        End Try
    '    'xDDDDDDDDD
    '    ElseIf e.ColumnIndex = 1 Then
    '        Dim Importe As Double = 0
    '        Dim Porcentaje As Double = 0
    '        Dim PorcentajeVal As Double = 0
    '        Dim PorcentajeD As Double = 0
    '        Dim PorcentajeP As Double = 0
    '        If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValPor").Value) = True Then
    '            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValPor").Value = "0.00"
    '        End If
    '        Importe = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImporte").Value)
    '        Porcentaje = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValPor").Value
    '        PorcentajeVal = (Importe * Porcentaje) / 100
    '        PorcentajeVal = Format(PorcentajeVal, "#,##0.00")
    '        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValPor").Value = Format(Porcentaje, "#,##0.00")
    '        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImpPor").Value = PorcentajeVal
    '        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValAPagar").Value = Importe - PorcentajeVal
    '        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValSaldo").Value = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValSaldoOculto").Value - dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValAPagar").Value
    '    ElseIf e.ColumnIndex = 2 Then
    '        Dim Importe As Double = 0
    '        Dim ImporPorcentaje As Double = 0
    '        Dim PorcentajeVal As Double = 0
    '        Dim PorcentajeD As Double = 0
    '        Dim PorcentajeP As Double = 0
    '        If Microsoft.VisualBasic.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImpPor").Value) = True Then
    '            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImpPor").Value = "0.00"
    '        End If
    '        Importe = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImporte").Value)
    '        ImporPorcentaje = Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImpPor").Value)
    '        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValImpPor").Value = Format(Convert.ToDouble(ImporPorcentaje), "#,##0.00")
    '        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValAPagar").Value = Importe - PorcentajeVal
    '        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValSaldo").Value = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValSaldoOculto").Value - dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("ValAPagar").Value
    '    ElseIf e.ColumnIndex = 9 Then
    '    ElseIf e.ColumnIndex = 12 Then
    '    ElseIf e.ColumnIndex = 11 Then
    '    End If
    '    CalcularValorizaciones()
    'End Sub

    'Private Sub dgvDetalle_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDetalle.MouseUp
    '    If dgvDetalle.Rows.Count > 0 Then
    '        CalcularValorizaciones()
    '    End If
    'End Sub

    Private Sub txtMonto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMonto.TextChanged
        'txtMonto2.Text = Trim(txtMonto.Text)
        'chkRetencion.Checked = False
        'chkRetencion_CheckedChanged(sender, e)
        'Try
        'If Len(Trim(txtMonto.Text)) > 0 Then
        'Dim MontoParaRetencion As Double = 0
        'If Trim(sMonCodigo) = "01" Then
        '    If Convert.ToDouble(txtMonto.Text) > MontoRetencion Then
        '        chkRetencion.Checked = True
        '        MontoParaRetencion = Convert.ToDouble(txtMonto.Text)
        '        txtRetenido.Text = (MontoParaRetencion * (PorcentajeRetencion / 100))
        '        txtGirar.Text = MontoParaRetencion - (MontoParaRetencion * (PorcentajeRetencion / 100))
        '        txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
        '        txtGirar.Text = Format(Convert.ToDouble(txtGirar.Text), "#,##0.00")
        '    Else
        '        chkRetencion.Checked = False
        '        txtRetenido.Text = "0.00"
        '        txtGirar.Text = "0.00"
        '    End If
        'ElseIf Trim(sMonCodigo) = "02" Then
        '    If (Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)) > MontoRetencion Then
        '        chkRetencion.Checked = True
        '        MontoParaRetencion = Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)
        '        txtRetenido.Text = ((MontoParaRetencion * (PorcentajeRetencion / 100)) / Convert.ToDouble(txtTipoCambio.Text))
        '        txtGirar.Text = Convert.ToDouble(txtMonto.Text) - Convert.ToDouble(txtRetenido.Text)
        '        txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
        '        txtGirar.Text = Format(Convert.ToDouble(txtGirar.Text), "#,##0.00")
        '    Else
        '        chkRetencion.Checked = False
        '        txtRetenido.Text = "0.00"
        '        txtGirar.Text = "0.00"
        '    End If
        'End If
        'ElseIf Len(Trim(txtMonto.Text)) = 0 Then
        'chkRetencion.Checked = False
        'txtRetenido.Text = "0.00"
        'txtGirar.Text = "0.00"
        'End If
        'Catch ex As Exception
        'End Try

        '-----------------------------------------------------------------------
        'ABAJO
        'Try
        '    If Len(Trim(txtMonto.Text)) > 0 Then
        '        Dim MontoParaRetencion As Double = 0
        '        If Trim(sMonCodigo) = "01" Then
        '            If Convert.ToDouble(txtMonto.Text) > MontoRetencion Then
        '                chkRetencion.Checked = True
        '                MontoParaRetencion = Convert.ToDouble(txtMonto.Text)
        '                txtRetenido.Text = (MontoParaRetencion * (PorcentajeRetencion / 100))
        '                txtGirar.Text = MontoParaRetencion - (MontoParaRetencion * (PorcentajeRetencion / 100))
        '                txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
        '                txtGirar.Text = Format(Convert.ToDouble(txtGirar.Text), "#,##0.00")
        '            Else
        '                chkRetencion.Checked = False
        '                txtRetenido.Text = "0.00"
        '                txtGirar.Text = "0.00"
        '            End If
        '        ElseIf Trim(sMonCodigo) = "02" Then
        '            If (Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)) > MontoRetencion Then
        '                chkRetencion.Checked = True
        '                MontoParaRetencion = Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)
        '                txtRetenido.Text = ((MontoParaRetencion * (PorcentajeRetencion / 100)) / Convert.ToDouble(txtTipoCambio.Text))
        '                txtGirar.Text = Convert.ToDouble(txtMonto.Text) - Convert.ToDouble(txtRetenido.Text)
        '                txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
        '                txtGirar.Text = Format(Convert.ToDouble(txtGirar.Text), "#,##0.00")
        '            Else
        '                chkRetencion.Checked = False
        '                txtRetenido.Text = "0.00"
        '                txtGirar.Text = "0.00"
        '            End If
        '        End If
        '    ElseIf Len(Trim(txtMonto.Text)) = 0 Then
        '        chkRetencion.Checked = False
        '        txtRetenido.Text = "0.00"
        '        txtGirar.Text = "0.00"
        '    End If
        'Catch ex As Exception
        'End Try

    End Sub

    Private Sub chkRetencion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRetencion.CheckedChanged
        If MostrarDetalles = "SI" And OpcionMostrar = "Grid" Then
            Exit Sub
        End If

        If chkRetencion.Checked = False Then
            btnOpciones.Visible = False
            If Len(Trim(txtRetenido.Text)) = 0 Then
                txtRetenido.Text = "0.00"
            End If
            If Len(Trim(txtMonto.Text)) = 0 Then
                txtMonto.Text = "0.00"
            End If
            If Trim(sMonCodigo) = "01" Then
                txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text) + Convert.ToDouble(txtRetenido.Text), "#,##0.00")
            ElseIf Trim(sMonCodigo) = "02" Then
                If Trim(txtTipoCambio.Text) = "0.00" Then
                    txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text), "#,##0.00")
                Else
                    txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text) + (Convert.ToDouble(txtRetenido.Text) / Convert.ToDouble(txtTipoCambio.Text)), "#,##0.00")
                End If
            End If
            txtRetenido.Text = "0.00"
            lblSerieText.Text = ""
            ' txtBeneficiario.Text = ""
        ElseIf chkRetencion.Checked = True Then
            Try
                '------------------------------------------------------------------
                Dim DesEmpresaPrestada As String = ""
                Dim sCCostoRetencion As String = ""
                If rdbUnaEmpresa.Checked = True And cboEmpresas.SelectedIndex > -1 Then
                    If cboCentrodeCostos.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un Centro de Costo de la Empresa a Prestar y vuelva a activar la Retenci�n", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        chkRetencion.Checked = False
                        cboCentrodeCostos.SelectedIndex = -1
                        cboCentrodeCostos.Focus()
                        Panel3.Visible = True
                        Exit Sub
                    End If
                    EmpresaRetencion = Trim(cboEmpresas.SelectedValue)
                    DesEmpresaPrestada = " - " & cboEmpresas.Text
                    sCCostoRetencion = cboCentrodeCostos.SelectedValue
                Else
                    EmpresaRetencion = gEmpresa
                    DesEmpresaPrestada = ""
                    sCCostoRetencion = cboCentroCosto.SelectedValue
                    If cboCentroCosto.SelectedIndex = -1 Then
                        MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        chkRetencion.Checked = False
                        cboCentroCosto.SelectedIndex = -1
                        cboCentroCosto.Focus()
                        Exit Sub
                    End If
                End If
                If Trim(txtRuc.Text) <> "" And Len(Trim(txtRuc.Text)) = 11 Then
                    Dim DireccionProv As String = ""
                    Dim dtTableDireccionProv As DataTable
                    dtTableDireccionProv = New DataTable
                    eBusquedaRetenciones = New clsBusquedaRetenciones
                    dtTableDireccionProv = eBusquedaRetenciones.fConsultaRetenciones(11, Trim(txtRuc.Text), EmpresaRetencion, Today(), Today(), 0, "")
                    If dtTableDireccionProv.Rows.Count = 0 Then
                        'DireccionProv = IIf(Microsoft.VisualBasic.IsDBNull(dtTableDireccionProv.Rows(0).Item(0)), "", dtTableDireccionProv.Rows(0).Item(0))
                        'DireccionProv = IIf(Microsoft.VisualBasic.IsDBNull(dtTableDireccionProv.Rows(0).Item(1)), "", dtTableDireccionProv.Rows(0).Item(1))

                        If Trim(EmpresaRetencion) <> Trim(gEmpresa) Then
                            MessageBox.Show("Este Proveedor no Existe en la empresa " & DesEmpresaPrestada & ", Registrelo!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ElseIf Trim(EmpresaRetencion) = Trim(gEmpresa) Then
                            MessageBox.Show("Este Proveedor no Existe, Registrelo!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                        chkRetencion.Checked = False
                        txtBeneficiario.Clear()
                        Exit Sub
                    ElseIf dtTableDireccionProv.Rows.Count > 0 Then
                        DireccionProv = IIf(Microsoft.VisualBasic.IsDBNull(dtTableDireccionProv.Rows(0).Item(0)), "", dtTableDireccionProv.Rows(0).Item(0))
                        'txtBeneficiario.Text = IIf(Microsoft.VisualBasic.IsDBNull(dtTableDireccionProv.Rows(0).Item(1)), "", dtTableDireccionProv.Rows(0).Item(1))
                        If Len(Trim(DireccionProv)) = 0 Then
                            If Trim(EmpresaRetencion) <> Trim(gEmpresa) Then
                                MessageBox.Show("Este Proveedor no tiene Direcci�n en la empresa " & DesEmpresaPrestada & ", Actualice!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            ElseIf Trim(EmpresaRetencion) = Trim(gEmpresa) Then
                                MessageBox.Show("Este Proveedor no tiene Direcci�n, Actualice!", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                            chkRetencion.Checked = False
                            Exit Sub
                        End If
                        txtBeneficiario.Text = IIf(Microsoft.VisualBasic.IsDBNull(dtTableDireccionProv.Rows(0).Item(1)), "", dtTableDireccionProv.Rows(0).Item(1))
                    End If
                End If
        '------------------------------------------------------------------
                Dim dtTableSerieRete As DataTable
                dtTableSerieRete = New DataTable
                eSeries = New clsSeries
                'buscar Serie de la EmprRetenida con CC
                dtTableSerieRete = eSeries.fListar(5, EmpresaRetencion, Trim(sCCostoRetencion))
                If dtTableSerieRete.Rows.Count > 0 Then
                    gSerie = Trim(dtTableSerieRete.Rows(0).Item("Serie"))
                    gNumeroInicioSerie = Trim(dtTableSerieRete.Rows(0).Item("NumeroIni"))
                Else
                    'si el cc no esta en ninguna serie buscara la Serie activada en la EmprRetenida
                    Dim dtTableSerieReteAct As DataTable
                    dtTableSerieReteAct = New DataTable
                    eSeries = New clsSeries
                    dtTableSerieReteAct = eSeries.fListar(9, EmpresaRetencion, Trim(sCCostoRetencion))
                    If dtTableSerieReteAct.Rows.Count > 0 Then
                        gSerie = Trim(dtTableSerieReteAct.Rows(0).Item("Serie"))
                        gNumeroInicioSerie = Trim(dtTableSerieReteAct.Rows(0).Item("NumeroIni"))
                    ElseIf dtTableSerieReteAct.Rows.Count = 0 Then
                        'sino hay ninguna serie activa en la empresaRetenida no hay serie 
                        gSerie = ""
                        gNumeroInicioSerie = ""
                        If Trim(DesEmpresaPrestada) = "" Then
                            MessageBox.Show("No se ha detectado alguna Serie para crear el Comprobante de Retenci�n.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        ElseIf Trim(DesEmpresaPrestada) <> "" Then
                            MessageBox.Show("No se ha detectado alguna Serie para crear el Comprobante de Retenci�n en la Empresa " & Trim(DesEmpresaPrestada), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If
                        chkRetencion.Checked = False
                        Exit Sub
                    End If
                End If

                lblSerieText.Text = "Serie: " & gSerie & DesEmpresaPrestada
                If Len(Trim(txtMonto.Text)) > 0 Then
                    Dim MontoParaRetencion As Double = 0
                    If Trim(sMonCodigo) = "01" Then
                        If Convert.ToDouble(txtMonto.Text) > MontoRetencion Then
                            MontoParaRetencion = Convert.ToDouble(txtMonto.Text)
                            txtRetenido.Text = (MontoParaRetencion * (PorcentajeRetencion / 100))
                            txtMonto.Text = MontoParaRetencion - (MontoParaRetencion * (PorcentajeRetencion / 100))
                            txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
                            txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text), "#,##0.00")
                        Else
                            chkRetencion.Checked = False
                            txtRetenido.Text = "0.00"
                            txtMonto.Text = Trim(txtMonto.Text)
                        End If
                    ElseIf Trim(sMonCodigo) = "02" Then
                        If (Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)) > MontoRetencion Then
                            MontoParaRetencion = Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)
                            txtRetenido.Text = ((MontoParaRetencion * (PorcentajeRetencion / 100))) '/ Convert.ToDouble(txtTipoCambio.Text))
                            txtMonto.Text = Convert.ToDouble(txtMonto.Text) - (Convert.ToDouble(txtRetenido.Text) / Convert.ToDouble(txtTipoCambio.Text))
                            txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
                            txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text), "#,##0.00")
                        Else
                            If Trim(txtTipoCambio.Text) = "0.00" Then
                                MessageBox.Show("Ingrese el Tipo de Cambio del d�a (" & dtpFechaGasto.Value & ")", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                txtTipoCambio.Focus()
                            End If
                            chkRetencion.Checked = False
                            txtRetenido.Text = "0.00"
                            txtMonto.Text = Trim(txtMonto.Text)
                        End If
                    End If
                ElseIf Len(Trim(txtMonto.Text)) = 0 Then
                    chkRetencion.Checked = False
                    txtRetenido.Text = "0.00"
                End If
            Catch ex As Exception
        End Try
        End If

        'If chkRetencion.Checked = False Then
        '    If Len(Trim(txtRetenido.Text)) = 0 Then
        '        txtRetenido.Text = "0.00"
        '    End If
        '    txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text) + Convert.ToDouble(txtRetenido.Text), "#,##0.00")
        '    txtRetenido.Text = "0.00"
        'ElseIf chkRetencion.Checked = True Then
        '    Try
        '        If Len(Trim(txtMonto2.Text)) > 0 Then
        '            Dim MontoParaRetencion As Double = 0
        '            If Trim(sMonCodigo) = "01" Then
        '                If Convert.ToDouble(txtMonto2.Text) > MontoRetencion Then
        '                    MontoParaRetencion = Convert.ToDouble(txtMonto2.Text)
        '                    txtRetenido.Text = (MontoParaRetencion * (PorcentajeRetencion / 100))
        '                    txtMonto.Text = MontoParaRetencion - (MontoParaRetencion * (PorcentajeRetencion / 100))
        '                    txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
        '                    txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text), "#,##0.00")
        '                Else
        '                    chkRetencion.Checked = False
        '                    txtRetenido.Text = "0.00"
        '                    txtMonto.Text = Trim(txtMonto2.Text)
        '                End If
        '            ElseIf Trim(sMonCodigo) = "02" Then
        '                If (Convert.ToDouble(txtMonto2.Text) * Convert.ToDouble(txtTipoCambio.Text)) > MontoRetencion Then

        '                    MontoParaRetencion = Convert.ToDouble(txtMonto2.Text) * Convert.ToDouble(txtTipoCambio.Text)
        '                    txtRetenido.Text = ((MontoParaRetencion * (PorcentajeRetencion / 100)) / Convert.ToDouble(txtTipoCambio.Text))
        '                    txtMonto.Text = Convert.ToDouble(txtMonto2.Text) - Convert.ToDouble(txtRetenido.Text)
        '                    txtRetenido.Text = Format(Convert.ToDouble(txtRetenido.Text), "#,##0.00")
        '                    txtMonto.Text = Format(Convert.ToDouble(txtMonto.Text), "#,##0.00")
        '                Else
        '                    chkRetencion.Checked = False
        '                    txtRetenido.Text = "0.00"
        '                    txtMonto.Text = Trim(txtMonto2.Text)
        '                End If
        '            End If
        '        ElseIf Len(Trim(txtMonto2.Text)) = 0 Then
        '            chkRetencion.Checked = False
        '            txtRetenido.Text = "0.00"
        '        End If
        '    Catch ex As Exception
        '    End Try
        'End If

    End Sub

    Private Sub txtRetenido_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRetenido.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtRetenido.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtRetenido.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub btnOpciones_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpciones.Click
        lblRetMensaje.Text = ""
        If Panel11.Visible = False Then
            Panel11.Visible = True
            If OpcionMostrar = "Boton" Then
                chkGenerar.Checked = False
                chkGenerar.Enabled = False
            Else
                chkGenerar.Enabled = True
                chkGenerar.Checked = False
            End If
        ElseIf Panel11.Visible = True Then
            Panel11.Visible = False
        End If
    End Sub

    Private Sub chkGenerar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGenerar.CheckedChanged
        If chkGenerar.Checked = True Then
            If (MessageBox.Show("�Est� seguro de generar un nuevo comprobante de Retenci�n para este Movimiento?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                Dim IdRetencion As String = ""
                Dim RetencioSiNo As Integer = 1
                If RetencioSiNo = 1 Then
                    '------------------------------------------------------------------
                    Dim DesEmpresaPrestada As String = ""
                    Dim sCCostoRetencion As String = ""
                    If rdbUnaEmpresa.Checked = True And cboEmpresas.SelectedIndex > -1 And cboCentrodeCostos.SelectedIndex > -1 Then
                        EmpresaRetencion = Trim(cboEmpresas.SelectedValue)
                        DesEmpresaPrestada = " - " & cboEmpresas.Text
                        sCCostoRetencion = cboCentrodeCostos.SelectedValue
                    Else
                        EmpresaRetencion = gEmpresa
                        DesEmpresaPrestada = ""
                        sCCostoRetencion = cboCentroCosto.SelectedValue
                    End If
                    '------------------------------------------------------------------
                    Dim dtTableSerieRete As DataTable
                    dtTableSerieRete = New DataTable
                    eSeries = New clsSeries
                    'buscar Serie de la EmprRetenida con CC
                    dtTableSerieRete = eSeries.fListar(5, RetEmprCodigoBD, Trim(sCCostoRetencion))
                    If dtTableSerieRete.Rows.Count > 0 Then
                        gSerie = Trim(dtTableSerieRete.Rows(0).Item("Serie"))
                        gNumeroInicioSerie = Trim(dtTableSerieRete.Rows(0).Item("NumeroIni"))
                    Else
                        'si el cc no esta en ninguna serie buscara la Serie activada en la EmprRetenida

                        Dim dtTableSerieReteAct As DataTable
                        dtTableSerieReteAct = New DataTable
                        eSeries = New clsSeries
                        dtTableSerieReteAct = eSeries.fListar(9, RetEmprCodigoBD, Trim(sCCostoRetencion))
                        If dtTableSerieReteAct.Rows.Count > 0 Then
                            gSerie = Trim(dtTableSerieReteAct.Rows(0).Item("Serie"))
                            gNumeroInicioSerie = Trim(dtTableSerieReteAct.Rows(0).Item("NumeroIni"))
                        ElseIf dtTableSerieReteAct.Rows.Count = 0 Then
                            'sino hay ninguna serie activa en la empresaRetenida no hay serie 
                            gSerie = ""
                            gNumeroInicioSerie = ""
                            If Trim(DesEmpresaPrestada) = "" Then
                                MessageBox.Show("No se ha detectado alguna Serie para crear el Comprobante de Retenci�n.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            ElseIf Trim(DesEmpresaPrestada) <> "" Then
                                MessageBox.Show("No se ha detectado alguna Serie para crear el Comprobante de Retenci�n en la Empresa " & Trim(DesEmpresaPrestada), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                            chkGenerar.Checked = False
                            Exit Sub
                        End If
                    End If
                    lblRetMensaje.Text = "Serie: " & gSerie & DesEmpresaPrestada
                    If Trim(gSerie) <> "" And Trim(gNumeroInicioSerie) <> "" Then
                        Dim IdRetencionNum As String = ""
                        Dim iGrabarRetencion As Integer = 0
                        Dim iActualizaMov As Integer = 0
                        eMovimientoCajaBanco = New clsMovimientoCajaBanco
                        eMovimientoCajaBanco.fCodigoRetencion(1, RetEmprCodigoBD, gSerie, gNumeroInicioSerie)
                        IdRetencion = eMovimientoCajaBanco.sCodFuturoRetencion
                        IdRetencionNum = Microsoft.VisualBasic.Right(Trim(IdRetencion), 7) 'eMovimientoCajaBanco.sCodFuturoRetencion
                        Dim MontoParaRetencion As Double = 0
                        If Trim(sMonCodigo) = "01" Then
                            MontoParaRetencion = Convert.ToDouble(txtMonto.Text) + Convert.ToDouble(txtRetenido.Text)
                        ElseIf Trim(sMonCodigo) = "02" Then
                            MontoParaRetencion = (Convert.ToDouble(txtMonto.Text) * Convert.ToDouble(txtTipoCambio.Text)) + Convert.ToDouble(txtRetenido.Text)
                        End If
                        Dim MontoLetras As String = ""
                        Dim dtTableLetras As DataTable
                        dtTableLetras = New DataTable
                        eBusquedaRetenciones = New clsBusquedaRetenciones
                        dtTableLetras = eBusquedaRetenciones.fConsultaMontoLetras(Convert.ToDouble(txtRetenido.Text))
                        If dtTableLetras.Rows.Count > 0 Then
                            MontoLetras = IIf(Microsoft.VisualBasic.IsDBNull(dtTableLetras.Rows(0).Item(0)), "", dtTableLetras.Rows(0).Item(0))
                        End If
                        Dim DireccionProv As String = ""
                        Dim dtTableDireccionProv As DataTable
                        dtTableDireccionProv = New DataTable
                        eBusquedaRetenciones = New clsBusquedaRetenciones
                        dtTableDireccionProv = eBusquedaRetenciones.fConsultaRetenciones(11, Trim(txtRuc.Text), gEmpresa, Today(), Today(), 0, "")
                        If dtTableDireccionProv.Rows.Count > 0 Then
                            DireccionProv = IIf(Microsoft.VisualBasic.IsDBNull(dtTableDireccionProv.Rows(0).Item(0)), "", dtTableDireccionProv.Rows(0).Item(0))
                        End If
                        iGrabarRetencion = eMovimientoCajaBanco.fGrabarRetencion(0, IdRetencion, RetEmprCodigoBD, Trim(gSerie), IdRetencionNum, dtpFechaGasto.Value, Trim(txtBeneficiario.Text), Trim(DireccionProv), Trim(txtRuc.Text), Trim(MontoLetras), MontoParaRetencion, PorcentajeRetencion, MontoParaRetencion * (PorcentajeRetencion / 100), txtTipoCambio.Text, MontoParaRetencion - (MontoParaRetencion * (PorcentajeRetencion / 100)), 0, gUsuario, "")
                        iActualizaMov = eMovimientoCajaBanco.fGrabarRetencion(3, IdRetencion, RetEmprCodigoBD, Trim(gSerie), IdRetencionNum, dtpFechaGasto.Value, Trim(txtCodigo.Text), "", "", "", 0, PorcentajeRetencion, 0 * (PorcentajeRetencion / 100), 0, 0 - (0 * (PorcentajeRetencion / 100)), 0, gUsuario, "")
                        If iGrabarRetencion = 1 Then
                            If cboEmpresas.SelectedIndex > -1 Then
                                MessageBox.Show("Se ha generado el comprobante de retenci�n N� " & Trim(gSerie) & "-" & Trim(IdRetencionNum) & " en la Empresa " & Trim(cboEmpresas.Text), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            ElseIf cboEmpresas.SelectedIndex = -1 Then
                                MessageBox.Show("Se ha generado el comprobante de retenci�n N� " & Trim(gSerie) & "-" & Trim(IdRetencionNum) & " para este Movimiento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            End If
                            mMostrarGrilla()
                            If dgvListado.Rows.Count > 0 Then
                                LimpiarControles()
                                txtCodigo.Clear()
                                txtRuc.Clear()
                                txtNroVoucher.Clear()
                                CheckBox1.Checked = False
                            End If
                            BeButton4_Click(sender, e)
                        End If
                    Else
                        MessageBox.Show("No Se ha generado el comprobante de retenci�n ya que no cuenta con una Serie Activada", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        chkGenerar.Checked = False
                    End If
                End If
            Else
                chkGenerar.Checked = False
            End If
        End If
    End Sub

    'Private Sub txtBuscarNroRet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim dtBusqueda As DataTable
    '    eBusquedaRetenciones = New clsBusquedaRetenciones
    '    dtBusqueda = New DataTable
    '    If Len(txtBuscarNroRet.Text) > 0 Then
    '        dtBusqueda = eBusquedaRetenciones.fConsultaRetenciones(10, Trim(txtBuscarNroRet.Text), gEmpresa, Today(), Today(), 0, Trim(txtSerieRet.Text))
    '        If dtBusqueda.Rows.Count > 0 Then
    '            lblExiste.Text = "Existe"
    '            lblExiste.ForeColor = Color.Blue
    '            'txtIdRetencion.Text = Trim(Convert.ToString(dtBusqueda.Rows(0).Item("IdRetencion")))
    '            txtIdRetencion.Text = IIf(Microsoft.VisualBasic.IsDBNull(Convert.ToString(dtBusqueda.Rows(0).Item("IdRetencion"))) = True, "", Convert.ToString(dtBusqueda.Rows(0).Item("IdRetencion")))
    '        ElseIf dtBusqueda.Rows.Count = 0 Then
    '            lblExiste.Text = "No Existe"
    '            lblExiste.ForeColor = Color.Red
    '            txtIdRetencion.Clear()
    '        End If
    '    ElseIf Len(txtBuscarNroRet.Text) = 0 Then
    '        lblExiste.Text = ""
    '        txtIdRetencion.Clear()
    '    End If
    'End Sub

    Private Sub cboOtrasEmpresas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOtrasEmpresas.SelectionChangeCommitted
        If Len(Trim(txtRucProv.Text)) = 11 Then
            chkDocTodas_CheckedChanged(sender, e)
        End If
    End Sub

    Private Sub txtTipoCambio_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTipoCambio.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtTipoCambio.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtTipoCambio.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub dgvDetalleVal_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleVal.CellContentClick
        If dgvDetalleVal.Rows.Count > 0 Then
            Dim columna As Integer = dgvDetalleVal.CurrentCell.ColumnIndex
            Try
                dgvDetalleVal.CurrentCell = dgvDetalleVal(0, dgvDetalleVal.CurrentRow.Index)
                dgvDetalleVal.CurrentCell = dgvDetalleVal(columna, dgvDetalleVal.CurrentRow.Index)
                If dgvDetalleVal.Rows.Count > 0 Then
                    Dim iFila As Integer

                    CalcularValorizaciones2()
                    If dgvDetalleVal.Rows.Count > 0 And dgvDetalleVal.CurrentCell.ColumnIndex = 11 Then
                        iFila = dgvDetalleVal.CurrentRow.Index
                        dgvDetalleVal.CurrentCell = dgvDetalleVal(0, 0)
                        dgvDetalleVal.CurrentCell = dgvDetalleVal(11, iFila)
                    End If
                End If
            Catch ex As Exception
            End Try
        End If
        CalcularValorizaciones2()
    End Sub

    'Private Sub dgvDetalleVal_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleVal.CellEndEdit
    '    Dim StrCad As String = String.Empty
    '    Dim StrImporteDoc As String = String.Empty
    '    If e.ColumnIndex = 22 Then

    '        Dim CadenaCon As String '++++++++++
    '        If vb.IsDBNull(dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("DV").Value) = True Then
    '            CadenaCon = ""
    '            dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("DV").Value = Format(0, "0.00")
    '            dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("ImpDV").Value = Format(0, "0.00")
    '            dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("aPagarDV").Value = dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("ImporteVal").Value
    '            'Exit Sub
    '        Else
    '            CadenaCon = dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("DV").Value
    '            If Trim(CadenaCon) = "" Then
    '                CadenaCon = ""
    '                dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("DV").Value = Format(0, "0.00")
    '                dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("ImpDV").Value = Format(0, "0.00")
    '                dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("aPagarDV").Value = dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("ImporteVal").Value
    '                Exit Sub
    '            End If
    '        End If

    '        Dim Importe As Double = 0
    '        Dim Porcentaje As Double = 0
    '        Dim PorcentajeD As Double = 0

    '        Importe = Trim(dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("ImporteDoc").Value)
    '        Porcentaje = Trim(dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("DV").Value)
    '        PorcentajeD = (Importe * Porcentaje) / 100
    '        dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("DV").Value = Format(Porcentaje, "#,##0.00")
    '        dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("ImpDV").Value = PorcentajeD
    '        dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("aPagarDV").Value = dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("ImporteVal").Value - PorcentajeD

    '        'ElseIf e.ColumnIndex = 23 Then
    '    End If
    'End Sub

    Private Sub dgvDetalleVal_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalleVal.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress
            AddHandler validar.KeyPress, AddressOf validar_KeypressVal
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_KeypressVal(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvDetalleVal.CurrentCell.ColumnIndex
        If columna = 22 Or columna = 23 Or columna = 24 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvDetalleVal_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalleVal.KeyDown
        Select Case e.KeyCode
            Case Keys.F4
                If dgvDetalleVal.Rows.Count > 0 Then
                    Dim IdRegistroDocX As String = ""
                    Dim IdRegistroDocEmprX As String = ""
                    Dim FechaPagoX As DateTime
                    Dim APagarFX As Double = 0
                    'Dim dblMonto As Double = 0
                    IdRegistroDocX = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("IdRegistroDoc").Value), "", dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("IdRegistroDoc").Value)
                    IdRegistroDocEmprX = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("EmprCodIdRegistroDoc").Value), "", dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("EmprCodIdRegistroDoc").Value)
                    APagarFX = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("APagarF").Value), "", dgvDetalleVal.Rows(dgvDetalleVal.CurrentRow.Index).Cells("APagarF").Value)
                    FechaPagoX = dtpFechaGasto.Value
                    If Trim(IdRegistroDocX) <> "" Then
                        Dim frm As New frmDetalleFacVal
                        frm.Owner = Me
                        frm.sIdRegistroDocX = Trim(IdRegistroDocX)
                        frm.sIdRegistroDocEmprX = Trim(IdRegistroDocEmprX)
                        frm.sFechaPagoX = Trim(FechaPagoX)
                        frm.sAPagarFX = APagarFX
                        frm.ShowInTaskbar = False
                        frm.ShowDialog()
                    Else
                        MessageBox.Show("No Existe Detalles", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalleVal.Focus()
                    End If
                Else
                    MessageBox.Show("Seleccione un Registro", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleVal.Focus()
                End If
        End Select
    End Sub

    Private Sub dgvDetalleVal_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDetalleVal.MouseUp
        If dgvDetalleVal.Rows.Count > 0 Then
            Dim iFila As Integer
            CalcularValorizaciones2()
            If dgvDetalleVal.Rows.Count > 0 And dgvDetalleVal.CurrentCell.ColumnIndex = 11 Then
                iFila = dgvDetalleVal.CurrentRow.Index
                'iFila = dgvDetalleVal.CurrentRow.Cells(0).
                dgvDetalleVal.CurrentCell = dgvDetalleVal(0, 0)
                dgvDetalleVal.CurrentCell = dgvDetalleVal(11, iFila)
            End If
        End If
    End Sub

  
    Private Sub dtpFechaGasto_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaGasto.ValueChanged
        'mMostrarGrilla()
    End Sub

    Private Sub dgvListadoReem_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListadoReem.CellContentClick

    End Sub

    Private Sub DgvCuotasPolizas_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvCuotasPolizas.CellContentClick

    End Sub

    Private Sub dgvListado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellContentClick

    End Sub

    Private Sub txtRazon_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRazon.TextChanged

    End Sub

    Private Sub cboBusqueda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBusqueda.SelectedIndexChanged

    End Sub

   
    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub txtRuc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRuc.TextChanged

    End Sub

    Private Sub cboEmpresasLibros_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpresasLibros.SelectedIndexChanged

        Dim IdEmpresa As String = ""
        If (cboEmpresasLibros.SelectedIndex > -1) Then

            Try
                IdEmpresa = Trim(cboEmpresasLibros.SelectedValue.ToString)
                If IdEmpresa <> "System.Data.DataRowView" Then
                    eGastosGenerales = New clsGastosGenerales

                    eGastosGenerales = New clsGastosGenerales
                    cbpBancoPrestado.DataSource = eGastosGenerales.fListarBancos(IdEmpresa)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cbpBancoPrestado.ValueMember = "IdBanco"
                        cbpBancoPrestado.DisplayMember = "NombreBanco"
                        cbpBancoPrestado.SelectedIndex = -1
                    End If

                    cboCuentaPrestada.SelectedIndex = -1

                    'cboCentrodeCostos.DataSource = eGastosGenerales.fListarCCxEmpresa(IdEmpresa)
                    'If eGastosGenerales.iNroRegistros > 0 Then
                    '    cboCentrodeCostos.ValueMember = "CCosCodigo"
                    '    cboCentrodeCostos.DisplayMember = "CCosDescripcion"
                    '    cboCentrodeCostos.SelectedIndex = -1
                    'End If

                    'Dim dtRuc As DataTable
                    'dtRuc = New DataTable
                    'dtRuc = eGastosGenerales.fListarRucxEmpresa(IdEmpresa)
                    'If dtRuc.Rows.Count > 0 Then
                    '    Dim Ruc As String = ""
                    '    Ruc = dtRuc.Rows(0).Item("EmprRUC")
                    '    lblRuc.Text = Trim(Ruc)
                    'End If

                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try


            'Try
            '    eGastosGenerales = New clsGastosGenerales
            '    cbpBancoPrestado.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
            '    If eGastosGenerales.iNroRegistros > 0 Then
            '        cbpBancoPrestado.ValueMember = "IdBanco"
            '        cbpBancoPrestado.DisplayMember = "NombreBanco"
            '        cbpBancoPrestado.SelectedIndex = -1
            '    End If
            'Catch ex As Exception
            'End Try

        Else
            'lblRuc.Text = ""
            'cboCentrodeCostos.DataSource = Nothing
        End If

    End Sub

    Private Sub cbpBancoPrestado_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbpBancoPrestado.SelectionChangeCommitted
        Dim IdBanco As String '= cbpBancoPrestado.SelectedValue.ToString
        If (cboBanco.SelectedIndex > -1) Or (cbpBancoPrestado.SelectedValue.ToString <> "System.Data.DataRowView") Then
            Try
                IdBanco = cbpBancoPrestado.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuentaPrestada.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa(Trim(cboEmpresasLibros.SelectedValue), IdBanco)
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCuentaPrestada.ValueMember = "IdCuenta"
                        cboCuentaPrestada.DisplayMember = "NumeroCuenta"
                        cboCuentaPrestada.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
        End If
    End Sub

 
    Private Sub cboCuentaPrestada_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuentaPrestada.SelectedIndexChanged
        Dim IdCuenta As String = ""
        Dim IdEmprx As String = ""
        Dim dtCodMoneda As DataTable

        'If cboCuentaPrestada.SelectedValue = Nothing Then
        '    Exit Sub
        'End If
        'Dim sDescripcionMoneda As String
        If (cboCuentaPrestada.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuentaPrestada.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtCodMoneda = New DataTable

                    If cboEmpresasLibros.SelectedIndex > -1 Then
                        IdEmprx = (cboEmpresasLibros.SelectedValue)
                    ElseIf cboEmpresasLibros.SelectedIndex = -1 Then
                        IdEmprx = ""
                    End If

                    dtCodMoneda = eGastosGenerales.fTraerCodMonedaCuenta(IdCuenta, IdEmprx)
                    If dtCodMoneda.Rows.Count > 0 Then
                        'sDescripcionMoneda = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonDescripcion")))
                        sMonCodigoDest = Trim(Convert.ToString(dtCodMoneda.Rows(0).Item("MonCodigo")))
                        'txtCodMoneda.Text = Trim(sDescripcionMoneda)
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            'txtCodMoneda.Clear()
            sMonCodigoDest = ""
        End If
    End Sub

    Private Sub chkEntregado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEntregado.CheckedChanged

    End Sub

    Private Sub chkEntregado_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEntregado.Click

        If dgvListado.Rows.Count > 0 Then
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            If Len(Trim(txtCodigo.Text)) > 0 Then
                Dim iResultado As Integer = 0
                'If chkEntregado.Checked = True Then
                iResultado = eMovimientoCajaBanco.fActEntregCheq(48, Trim(txtCodigo.Text), gEmpresa, gUsuario, Convert.ToString(IIf(Me.chkEntregado.Checked = True, 1, 0)))
                'ElseIf chkEntregado.Checked = False Then
                'iResultado = eMovimientoCajaBanco.fCancelarFactura(45, Trim(sCodRegistro), gEmpresa, "00001", 0, 0, gUsuario, Convert.ToString(IIf(Me.chkEntregado.Checked = True, 1, 0)))
                'End If
                If chkEntregado.Checked = True Then
                    'dgvListado.Rows(dgvLibroDet.CurrentRow.Index).Cells(0).Style.BackColor = Color.Yellow
                    dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CheqEntregado").Value = "1"
                ElseIf chkEntregado.Checked = False Then
                    'dgvListado.Rows(dgvLibroDet.CurrentRow.Index).Cells(0).Style.BackColor = Color.White
                    dgvListado.Rows(dgvListado.CurrentRow.Index).Cells("CheqEntregado").Value = "0"
                End If
            End If
        End If

    End Sub

    Private Sub txtLineaCre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLineaCre.TextChanged

    End Sub

    '*------------------------------------------------------------------------------------------------------------
    '*------------------------------------------------------------------------------------------------------------
    '*------------------------------------------------------------------------------------------------------------
    '*------------------------------------------------------------------------------------------------------------
    'Dim V_ID_PROGPAGO As String
    'lblEmpresa.Text=gEmpresa

    Dim objPP As BeanProgramacionPagosDetalle

#Region "Proceso"

    Private Sub ListarPP(ByVal id_empresa As String)

        Dim VL_ProgramacionPagoDetalle As SrvProgramacionPagosDetalle = New SrvProgramacionPagosDetalle()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion

        objPP = New BeanProgramacionPagosDetalle()

        Try
            VL_BeanResultado = VL_ProgramacionPagoDetalle.Fnc_Listar_PPD(id_empresa)
            If VL_BeanResultado.blnExiste = True Then

                dgvPD.DataSource = VL_BeanResultado.dtResultado

            Else

                MessageBox.Show("No se encontr�", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub ActualziarEstadoPPD(ByVal VL_ID_EntregaRendir As String)
        'Fnc_Actualizar_Estado_PPD()

        Dim VL_ProgramacionPagoDetalle As SrvProgramacionPagosDetalle = New SrvProgramacionPagosDetalle()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion

        Dim VL_Estado As String = "3"
        'objEntregaRendir = New BeanlEntregaRendir(VL_ID_EntregaRendir, VL_ID_Estado, idmoneda)

        VL_BeanResultado = VL_ProgramacionPagoDetalle.Fnc_Actualizar_Estado_PPD(VL_ID_EntregaRendir, VL_Estado)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            'MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
    End Sub

#End Region

    Private Sub dgvPD_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles dgvPD.CellDoubleClick
        txtBeneficiario.Text = IIf(dgvPD.CurrentRow.Cells("SolicitadoA").Value = Nothing, "", dgvPD.CurrentRow.Cells("SolicitadoA").Value)
        txtConcepto.Text = IIf(dgvPD.CurrentRow.Cells("Glosa").Value = Nothing, "", Convert.ToString(dgvPD.CurrentRow.Cells("Glosa").Value))
        txtMonto.Text = Convert.ToString(dgvPD.CurrentRow.Cells("IMPORTE").Value)
        cboCentroCosto.SelectedValue = dgvPD.CurrentRow.Cells("idCcosto").Value
        'cboTipoPago.SelectedValue = dgvPD.CurrentRow.Cells("ID_FORMA_PAGO").Value
        txtRuc.Text = IIf(dgvPD.CurrentRow.Cells("PrvRUC").Value = Nothing, "", Convert.ToString(dgvPD.CurrentRow.Cells("Glosa").Value))
        'txtRuc.Text = dgvPD.CurrentRow.Cells("PDIMPORTE").Value

        ID_DocPendiente = dgvPD.CurrentRow.Cells("ID_DocPendiente").Value

    End Sub

    
End Class