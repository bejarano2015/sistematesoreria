Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic

Public Class frmVePartidasContrato

    Private eValorizaciones As clsValorizaciones
    Private ePartidas As clsPartidas
    Private ePagoProveedores As clsPagoProveedores
    Public IdContrato As String = ""
    Public CCosto As String = ""
    Dim PresuActual As Double = 0
    Dim dblParValorizadoAnterior As Double = 0
    Dim GrabarSiNo As Int16 = 0
#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmVePartidasContrato = Nothing
    Public Shared Function Instance() As frmVePartidasContrato
        If frmInstance Is Nothing Then
            frmInstance = New frmVePartidasContrato
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmVePartidasContrato_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region





    Private Sub frmVePartidasContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        'MessageBox.Show(Asc(e.KeyChar))
        If Asc(e.KeyChar) = 27 Then
            'SendKeys.Send("{tab}")
            If Format(PresuActual, "#,##0.00") <> txtPresupuesto.Text Then
                If MessageBox.Show("Se ha modificado el contrato. �Desea guardar los cambios?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    'anterior = PresuActual
                    'actual = txtPresupuesto.Text

                    ValidarDatos2()
                    If GrabarSiNo = 0 Then
                        Exit Sub
                    End If

                    Dim iResultado As Int32
                    Dim sCodigoRegistro As String = ""

                    'eTempo = New clsPlantTempo
                    ePartidas = New clsPartidas
                    'If sTab = 1 Then

                    'If iOpcion = 5 Then
                    '    Mensaje = "Grabar"
                    'ElseIf iOpcion = 6 Then
                    '    Mensaje = "Actualizar"
                    'End If

                    'If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    'frmPrincipal.sFlagGrabar = "1"
                    sCodigoRegistro = Trim(IdContrato)
                    iResultado = ePartidas.fGrabar2(27, gEmpresa, sCodigoRegistro, "", "", "", "", Today, Today, "", Trim(txtPresupuesto.Text), Format(PresuActual, "#,##0.00"), "", "", 0, "", gPeriodo, gUsuario, "", Trim(txtSubTotal.Text), Trim(txtIGV.Text), "", "", "")
                    If iResultado > 0 Then
                        If dgvDetalle.Rows.Count > 0 Then
                            For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                                Dim xDescripcion As String
                                Dim xCant As Double
                                Dim xUnd As String
                                Dim xM1 As Double
                                Dim xM2 As Double
                                Dim xPU As Double
                                Dim xParcial As Double
                                Dim xM2Anterior As Double = 0
                                Dim sCodigoRegistroDet As String

                                Dim xCon_Metrado As Double
                                Dim xCon_PrecioSubContra As Double
                                Dim xCon_TitCodigo As String
                                Dim xCon_ParCodigo As String
                                Dim xCon_ConsCodigo As String

                                xDescripcion = Trim(dgvDetalle.Rows(x).Cells("Descripcion").Value)
                                xCant = dgvDetalle.Rows(x).Cells("Cant").Value
                                xUnd = dgvDetalle.Rows(x).Cells("Und").Value
                                xM1 = dgvDetalle.Rows(x).Cells("M1").Value
                                xM2 = dgvDetalle.Rows(x).Cells("M2").Value
                                xPU = dgvDetalle.Rows(x).Cells("PU").Value
                                xParcial = dgvDetalle.Rows(x).Cells("Parcial").Value
                                sCodigoRegistroDet = dgvDetalle.Rows(x).Cells("idDetalle").Value

                                xCon_Metrado = dgvDetalle.Rows(x).Cells("Con_Metrado").Value
                                xCon_PrecioSubContra = dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value
                                xCon_TitCodigo = dgvDetalle.Rows(x).Cells("Con_TitCodigo").Value
                                xCon_ParCodigo = dgvDetalle.Rows(x).Cells("Con_ParCodigo").Value
                                xCon_ConsCodigo = dgvDetalle.Rows(x).Cells("Con_ConsCodigo").Value

                                If dgvDetalle.Rows(x).Cells("Estado").Value = "0" Then
                                    xM2Anterior = xM2
                                    ePartidas = New clsPartidas
                                    ePartidas.fCodigoDetalle(9, gEmpresa, sCodigoRegistro, gPeriodo, CCosto)
                                    sCodigoRegistroDet = ePartidas.sCodFuturoDet
                                    ePartidas.fGrabarDetalle(10, gEmpresa, sCodigoRegistro, sCodigoRegistroDet, xDescripcion, xCant, xUnd, xM1, xM2, xPU, xParcial, gUsuario, xCon_Metrado, xCon_PrecioSubContra, xCon_TitCodigo, xCon_ParCodigo, xCon_ConsCodigo, xM2Anterior, gPeriodo, CCosto)
                                ElseIf dgvDetalle.Rows(x).Cells("Estado").Value = "1" Then
                                    xM2Anterior = dgvDetalle.Rows(x).Cells("M2Anterior").Value
                                    ePartidas.fGrabarDetalle(11, gEmpresa, sCodigoRegistro, sCodigoRegistroDet, xDescripcion, xCant, xUnd, xM1, xM2, xPU, xParcial, gUsuario, xCon_Metrado, xCon_PrecioSubContra, xCon_TitCodigo, xCon_ParCodigo, xCon_ConsCodigo, xM2Anterior, gPeriodo, CCosto)
                                End If
                            Next
                        End If
                        'mMostrarGrilla()                        
                        'End If
                        MessageBox.Show("Se ha Actualizado el Contrato con Exito", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)

                    End If

                End If

                Dim _formInterface As frmValorizacionesPagosVarios = CType(Me.Owner, frmValorizacionesPagosVarios)
                _formInterface.hace = 1

                Me.Close()
            Else
                'frmVePartidasContrato.Close()
                Dim _formInterface As frmValorizacionesPagosVarios = CType(Me.Owner, frmValorizacionesPagosVarios)

                If Format(PresuActual, "#,##0.00") <> Format(dblParValorizadoAnterior, "#,##0.00") Then


                    eValorizaciones = New clsValorizaciones
                    Dim dtValorizacion As DataTable
                    dtValorizacion = New DataTable
                    dtValorizacion = eValorizaciones.fBuscarValorizacion(75, gEmpresa, Trim(IdContrato), 0, Today(), Today(), gPeriodo, Trim(CCosto))
                    Dim Aumento As Double = 0
                    Dim Disminucion As Double = 0
                    Dim IdTipoAde As String = ""
                    Dim xx As Double = 0

                    If dtValorizacion.Rows.Count > 0 Then
                        For x As Integer = 0 To dtValorizacion.Rows.Count - 1
                            IdTipoAde = IIf(vb.IsDBNull(dtValorizacion.Rows(x).Item("IdTipoPago")) = True, "", dtValorizacion.Rows(x).Item("IdTipoPago")) 'UCase(dtPartida.Rows(0).Item("xret"))
                            If Trim(IdTipoAde) = "00003" Then
                                Aumento = IIf(vb.IsDBNull(dtValorizacion.Rows(x).Item("TotalVal")) = True, "0.00", dtValorizacion.Rows(x).Item("TotalVal")) 'UCase(dtPartida.Rows(0).Item("xret"))
                                xx = PresuActual - Aumento
                            ElseIf Trim(IdTipoAde) = "00004" Then
                                Disminucion = IIf(vb.IsDBNull(dtValorizacion.Rows(x).Item("TotalVal")) = True, "0.00", dtValorizacion.Rows(x).Item("TotalVal")) 'UCase(dtPartida.Rows(0).Item("xret"))
                                xx = PresuActual + Disminucion
                            End If
                        Next
                    End If

                    If Format(xx, "#,##0.00") <> Format(dblParValorizadoAnterior, "#,##0.00") Then
                        _formInterface.hace = 1
                    ElseIf Format(xx, "#,##0.00") = Format(dblParValorizadoAnterior, "#,##0.00") Then
                        _formInterface.hace = 0
                    End If

                Else
                    _formInterface.hace = 0
                End If

                '_formInterface.hace = 0
                Me.Close()
            End If
        End If
    End Sub



    Sub ValidarDatos2()
        If dgvDetalle.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDetalle.Rows.Count - 1

                If Len(Trim(dgvDetalle.Rows(x).Cells("Descripcion").Value)) = 0 Then
                    MessageBox.Show("Ingrese una Descripci�n", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(0, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("Und").Value)) = 0 Then
                    MessageBox.Show("Ingrese la Unidad de Medida", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(2, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("M1").Value)) = 0 Then
                    MessageBox.Show("Ingrese el Porcetaje", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(3, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("M2").Value)) = 0 Then
                    MessageBox.Show("Ingrese el Metrado", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(4, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                'If Len(Trim(dgvDetalle.Rows(x).Cells("PU").Value)) = 0 Then
                '    MessageBox.Show("Ingrese el Precio Unitario", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    dgvDetalle.CurrentCell = dgvDetalle(5, x)
                '    GrabarSiNo = 0
                '    Exit Sub
                'End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("Parcial").Value)) = 0 Then
                    MessageBox.Show("Ingrese Importe Parcial", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(6, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If
            Next
            GrabarSiNo = 1
        ElseIf dgvDetalle.Rows.Count = 0 Then
            GrabarSiNo = 1
        End If
    End Sub


    Private Sub frmVePartidasContrato_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Trim(IdContrato) <> "" Then
            Dim FechaIniContrato As DateTime
            eValorizaciones = New clsValorizaciones
            Dim dtPartida As DataTable
            dtPartida = New DataTable
            dtPartida = eValorizaciones.fListarObras(21, gEmpresa, gPeriodo, "", Trim(IdContrato), Today(), Trim(CCosto))
            If dtPartida.Rows.Count > 0 Then
                'lblPorRete.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("xret")) = True, "0.00", dtPartida.Rows(0).Item("xret")) 'UCase(dtPartida.Rows(0).Item("xret"))
                FechaIniContrato = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("FechaInicio")) = True, Today(), dtPartida.Rows(0).Item("FechaInicio")) 'UCase(dtPartida.Rows(0).Item("FechaInicio"))
                'lblFechaFin.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("FechaFinal")) = True, Today(), dtPartida.Rows(0).Item("FechaFinal")) 'UCase(dtPartida.Rows(0).Item("FechaFinal"))
                'txtObservacion.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("ParObservacion")) = True, "", dtPartida.Rows(0).Item("ParObservacion")) 'UCase(dtPartida.Rows(0).Item("ParObservacion"))
                'lblMoneda.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("MonDescripcion")) = True, "", dtPartida.Rows(0).Item("MonDescripcion")) 'UCase(dtPartida.Rows(0).Item("MonDescripcion"))
                'lblDoc.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("DescripDoc")) = True, "", dtPartida.Rows(0).Item("DescripDoc")) 'UCase(dtPartida.Rows(0).Item("DescripDoc"))
                PresuActual = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("ParValorizado")) = True, "0.00", Format(dtPartida.Rows(0).Item("ParValorizado"), "#,##0.00")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                dblParValorizadoAnterior = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("ParValorizadoAnterior")) = True, "0.00", Format(dtPartida.Rows(0).Item("ParValorizadoAnterior"), "#,##0.00")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                'lblOrden.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("NumOrden")) = True, "", dtPartida.Rows(0).Item("NumOrden")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                'lblContrato.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("NContrato")) = True, "", dtPartida.Rows(0).Item("NContrato")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
            End If

            Dim dtTable As DataTable
            dtTable = New DataTable
            ePagoProveedores = New clsPagoProveedores
            dtTable = ePagoProveedores.fListarParametroIGV(FechaIniContrato)
            If dtTable.Rows.Count > 0 Then
                gIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
                gIgvConvertido = Format(gIgv / 100, "0.00")
            Else
                gIgv = 0
                gIgvConvertido = 0
            End If
            BeLabel16.Text = "IGV " & gIgv & " %"

            ePartidas = New clsPartidas
            Dim dtDetalle As DataTable
            dtDetalle = New DataTable
            dtDetalle = ePartidas.fListarDetalle(12, gEmpresa, IdContrato, Today(), gPeriodo, Trim(CCosto))
            dgvDetalle.AutoGenerateColumns = False
            If dtDetalle.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
                For y As Integer = 0 To dtDetalle.Rows.Count - 1
                    dgvDetalle.Rows.Add()
                    dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetalle.Rows(y).Item("Descripcion").ToString
                    dgvDetalle.Rows(y).Cells("Cant").Value = dtDetalle.Rows(y).Item("Cantidad").ToString
                    dgvDetalle.Rows(y).Cells("Und").Value = dtDetalle.Rows(y).Item("UnidMed").ToString
                    dgvDetalle.Rows(y).Cells("M1").Value = dtDetalle.Rows(y).Item("MetradoPor").ToString
                    dgvDetalle.Rows(y).Cells("Con_Metrado").Value = dtDetalle.Rows(y).Item("Con_Metrado").ToString
                    dgvDetalle.Rows(y).Cells("Con_PrecioSubContra").Value = dtDetalle.Rows(y).Item("Con_PrecioSubContra").ToString
                    dgvDetalle.Rows(y).Cells("M2").Value = dtDetalle.Rows(y).Item("MetradoImpor").ToString
                    dgvDetalle.Rows(y).Cells("PU").Value = dtDetalle.Rows(y).Item("PU").ToString
                    dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetalle.Rows(y).Item("Parcial").ToString
                    dgvDetalle.Rows(y).Cells("idDetalle").Value = dtDetalle.Rows(y).Item("IdPartidaDet").ToString
                    dgvDetalle.Rows(y).Cells("Estado").Value = "1"
                    dgvDetalle.Rows(y).Cells("Con_TitCodigo").Value = dtDetalle.Rows(y).Item("Con_TitCodigo").ToString
                    dgvDetalle.Rows(y).Cells("Con_ParCodigo").Value = dtDetalle.Rows(y).Item("Con_ParCodigo").ToString
                    dgvDetalle.Rows(y).Cells("Con_ConsCodigo").Value = dtDetalle.Rows(y).Item("Con_ConsCodigo").ToString
                    dgvDetalle.Rows(y).Cells("M2Anterior").Value = dtDetalle.Rows(y).Item("M2Anterior").ToString
                Next
                BeLabel18.Text = "Total de Items : " & dtDetalle.Rows.Count
                Calcular()
            ElseIf dtDetalle.Rows.Count = 0 Then
                BeLabel18.Text = "Total de Items : 0"
            End If
        End If

    End Sub


    Dim DblTotal As Double
    Sub Calcular()
        DblTotal = 0
        Dim Contador As Integer = 0
        For x As Integer = 0 To dgvDetalle.Rows.Count - 1
            'If Len(dgvDetalle.Rows(x).Cells("M2").Value) > 0 And Len(dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value) > 0 Then
            '    'dgvDetalle.Rows(x).Cells("Parcial").Value = Format((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value), "#,##0.00")
            '    dgvDetalle.Rows(x).Cells("Parcial").Value = Math.Round((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value), 2)
            'End If

            If Len(dgvDetalle.Rows(x).Cells("Parcial").Value) = 0 Then
                DblTotal = DblTotal + 0
            Else
                DblTotal = DblTotal + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("Parcial").Value)))
            End If
            Contador = Contador + 1
        Next
        txtSubTotal.Text = Format(DblTotal, "#,##0.00")
        Dim ImpIGV As Double = Convert.ToDouble(txtSubTotal.Text) * (gIgv / 100)
        'Dim ImpIGV As Double = ImpBase * gIgvConvertido
        txtIGV.Text = Format(ImpIGV, "#,##0.00")
        'txtTotal.Text = Format(DblTotal + ImpIGV, "#,##0.00")
        txtPresupuesto.Text = Format(DblTotal + ImpIGV, "#,##0.00")
        BeLabel18.Text = "Total de Items : " & Contador
    End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit
        If e.ColumnIndex = 7 Then


            If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")

                If Len(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value) > 0 Then
                    Dim dSaldo As Double = 0
                    dSaldo = Convert.ToDouble(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value)
                    If dSaldo > dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Con_Metrado").Value Then
                        MessageBox.Show("El metrado a contratar es Mayor al metrado de la Partida.", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value = "0.00"
                    End If
                End If

                If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If

        End If

        If e.ColumnIndex = 0 Or e.ColumnIndex = 2 Then
            Dim CadenaCon As String
            If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value
            End If
            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = vb.UCase(CadenaCon)
        End If

        If e.ColumnIndex = 1 Or e.ColumnIndex = 3 Or e.ColumnIndex = 4 Or e.ColumnIndex = 5 Or e.ColumnIndex = 6 Or e.ColumnIndex = 7 Then
            If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If

                If Len(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value) > 0 And Len(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Con_PrecioSubContra").Value) > 0 Then
                    'dgvDetalle.Rows(x).Cells("Parcial").Value = Format((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value), "#,##0.00")
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Parcial").Value = Math.Round((dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value) * (dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Con_PrecioSubContra").Value), 2)
                End If

            Else
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If
        End If

        Calcular()
    End Sub

    Private Sub dgvDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalle.Click
        Calcular()
    End Sub

    Private Sub dgvDetalle_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalle.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If columna = 1 Or columna = 3 Or columna = 4 Or columna = 5 Or columna = 6 Or columna = 7 Then
            Dim caracter As Char = e.KeyChar
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub frmVePartidasContrato_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown

    End Sub

    Private Sub frmVePartidasContrato_RightToLeftLayoutChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.RightToLeftLayoutChanged

    End Sub
End Class