<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSerieRetenciones
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.stsTotales = New System.Windows.Forms.StatusStrip
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtSerie = New ctrLibreria.Controles.BeTextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtNroIni = New ctrLibreria.Controles.BeTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnPermisos = New ctrLibreria.Controles.BeButton
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.dgvLista = New System.Windows.Forms.DataGridView
        Me.IdSerie = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cSerie = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NumeroIni = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Activar = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.BeLabel36 = New ctrLibreria.Controles.BeLabel
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.btnCCostos = New ctrLibreria.Controles.BeButton
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.stsTotales.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(537, 286)
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnCCostos)
        Me.TabPage1.Controls.Add(Me.BeLabel36)
        Me.TabPage1.Controls.Add(Me.Panel8)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Controls.Add(Me.btnPermisos)
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Size = New System.Drawing.Size(529, 260)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.txtNroIni)
        Me.TabPage2.Controls.Add(Me.txtCodigo)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.txtSerie)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.UseVisualStyleBackColor = False
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel2})
        Me.stsTotales.Location = New System.Drawing.Point(3, 233)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(521, 22)
        Me.stsTotales.TabIndex = 5
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel2.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(115, 17)
        Me.ToolStripStatusLabel2.Text = "Total de Registros= 0"
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.Ivory
        Me.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(111, 21)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(185, 20)
        Me.txtCodigo.TabIndex = 29
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(17, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "C�digo"
        '
        'txtSerie
        '
        Me.txtSerie.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSerie.BackColor = System.Drawing.Color.Ivory
        Me.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerie.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.ForeColor = System.Drawing.Color.Black
        Me.txtSerie.KeyEnter = True
        Me.txtSerie.Location = New System.Drawing.Point(111, 50)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSerie.ShortcutsEnabled = False
        Me.txtSerie.Size = New System.Drawing.Size(185, 20)
        Me.txtSerie.TabIndex = 31
        Me.txtSerie.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(17, 52)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Serie"
        '
        'txtNroIni
        '
        Me.txtNroIni.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroIni.BackColor = System.Drawing.Color.Ivory
        Me.txtNroIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroIni.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroIni.ForeColor = System.Drawing.Color.Black
        Me.txtNroIni.KeyEnter = True
        Me.txtNroIni.Location = New System.Drawing.Point(111, 78)
        Me.txtNroIni.Name = "txtNroIni"
        Me.txtNroIni.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroIni.ShortcutsEnabled = False
        Me.txtNroIni.Size = New System.Drawing.Size(185, 20)
        Me.txtNroIni.TabIndex = 33
        Me.txtNroIni.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(17, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "N� Inicial"
        '
        'btnPermisos
        '
        Me.btnPermisos.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnPermisos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPermisos.Location = New System.Drawing.Point(365, 232)
        Me.btnPermisos.Name = "btnPermisos"
        Me.btnPermisos.Size = New System.Drawing.Size(146, 23)
        Me.btnPermisos.TabIndex = 3
        Me.btnPermisos.Text = "Activar Serie Seleccionada"
        Me.btnPermisos.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnPermisos.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdSerie, Me.cSerie, Me.NumeroIni, Me.Column5, Me.Activar})
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-1, -1)
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.Size = New System.Drawing.Size(529, 231)
        Me.dgvLista.TabIndex = 6
        '
        'IdSerie
        '
        Me.IdSerie.DataPropertyName = "IdSerie"
        Me.IdSerie.HeaderText = "C�digo"
        Me.IdSerie.Name = "IdSerie"
        Me.IdSerie.ReadOnly = True
        Me.IdSerie.Width = 140
        '
        'cSerie
        '
        Me.cSerie.DataPropertyName = "Serie"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.cSerie.DefaultCellStyle = DataGridViewCellStyle1
        Me.cSerie.HeaderText = "Serie"
        Me.cSerie.Name = "cSerie"
        Me.cSerie.ReadOnly = True
        '
        'NumeroIni
        '
        Me.NumeroIni.DataPropertyName = "NumeroIni"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.NumeroIni.DefaultCellStyle = DataGridViewCellStyle2
        Me.NumeroIni.HeaderText = "N� Inicial"
        Me.NumeroIni.Name = "NumeroIni"
        Me.NumeroIni.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "Estados"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column5.HeaderText = "Estado"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 105
        '
        'Activar
        '
        Me.Activar.DataPropertyName = "Activo"
        Me.Activar.HeaderText = "Activo"
        Me.Activar.Name = "Activar"
        Me.Activar.Width = 65
        '
        'Timer2
        '
        '
        'BeLabel36
        '
        Me.BeLabel36.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel36.AutoSize = True
        Me.BeLabel36.BackColor = System.Drawing.Color.LightSteelBlue
        Me.BeLabel36.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel36.ForeColor = System.Drawing.Color.Black
        Me.BeLabel36.Location = New System.Drawing.Point(218, 237)
        Me.BeLabel36.Name = "BeLabel36"
        Me.BeLabel36.Size = New System.Drawing.Size(102, 13)
        Me.BeLabel36.TabIndex = 30
        Me.BeLabel36.Text = "Serie Activada"
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Aqua
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Location = New System.Drawing.Point(326, 237)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(33, 14)
        Me.Panel8.TabIndex = 31
        '
        'btnCCostos
        '
        Me.btnCCostos.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnCCostos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCCostos.Location = New System.Drawing.Point(122, 232)
        Me.btnCCostos.Name = "btnCCostos"
        Me.btnCCostos.Size = New System.Drawing.Size(91, 23)
        Me.btnCCostos.TabIndex = 32
        Me.btnCCostos.Text = "Ver C. Costos"
        Me.btnCCostos.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnCCostos.UseVisualStyleBackColor = True
        '
        'frmSerieRetenciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(537, 286)
        Me.MaximizeBox = False
        Me.Name = "frmSerieRetenciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Series Retenciones"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNroIni As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSerie As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnPermisos As ctrLibreria.Controles.BeButton
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents dgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents BeLabel36 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents btnCCostos As ctrLibreria.Controles.BeButton
    Friend WithEvents IdSerie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cSerie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumeroIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activar As System.Windows.Forms.DataGridViewCheckBoxColumn

End Class
