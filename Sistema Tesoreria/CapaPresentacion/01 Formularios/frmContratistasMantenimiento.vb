Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmContratistasMantenimiento

    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Private eContratistas As clsContratistas
    Private eRegistroDocumento As clsRegistroDocumento

    Private v_ruc As String
    Public Sub set_ruc(ByVal ruc As String)
        v_ruc = ruc
    End Sub
    Function get_ruc() As String
        Return v_ruc
    End Function


#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmContratistasMantenimiento = Nothing
    Public Shared Function Instance() As frmContratistasMantenimiento
        If frmInstance Is Nothing Then
            frmInstance = New frmContratistasMantenimiento
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmContratistasMantenimiento_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmContratistasMantenimiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboEstado.DropDownStyle = ComboBoxStyle.DropDownList

        eContratistas = New clsContratistas
        mMostrarGrilla()

        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        dgvLista.Focus()

        cboTipoContra.DataSource = eContratistas.fListar(1, "", "")
        If eContratistas.iNroRegistros > 0 Then
            cboTipoContra.ValueMember = "TipoConCodigo"
            cboTipoContra.DisplayMember = "DesTipoContratista"
            cboTipoContra.SelectedIndex = -1
        End If

        cboEspecialidad.DataSource = eContratistas.fListar(7, "", "")
        If eContratistas.iNroRegistros > 0 Then
            cboEspecialidad.ValueMember = "esp_codigo"
            cboEspecialidad.DisplayMember = "esp_nombre"
            cboEspecialidad.SelectedIndex = -1
        End If

        eRegistroDocumento = New clsRegistroDocumento

        cboTipoDocIdent.DataSource = eRegistroDocumento.fListarTipDocIdentidad()
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboTipoDocIdent.ValueMember = "DIdeCodigo"
            cboTipoDocIdent.DisplayMember = "DIdeDescripcion"
            cboTipoDocIdent.SelectedIndex = -1
        End If


        dgvLista.Width = 625
        dgvLista.Height = 360
        v_NuevoReg()

        cboTipoContra.SelectedValue = "00000000000000000002"

        listandoregistroconprobante()
    End Sub

    Private Sub mMostrarGrilla()
        eContratistas = New clsContratistas
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eContratistas.fListar(0, gEmpresa, "")
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTable
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eContratistas.iNroRegistros
        If eContratistas.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        cboTipoContra.SelectedIndex = -1
        cboEspecialidad.SelectedIndex = -1
        txtNombre.Clear()

        cboTipoDocIdent.SelectedIndex = -1
        NroDoc.Clear()
        txtNomRepr.Clear()

        txtDireccion.Clear()
        txtRuc.Clear()
        txtTelefono.Clear()
        txtEmail.Clear()
    End Sub

    Private Sub DesabilitarControles()
        cboTipoContra.Enabled = False
        cboEspecialidad.Enabled = False
        txtNombre.Enabled = False
        'txtApellido.Enabled = False
        'txtEspecialidad.Enabled = False

        cboTipoDocIdent.Enabled = False
        NroDoc.Enabled = False
        txtNomRepr.Enabled = False

        txtDireccion.Enabled = False
        txtRuc.Enabled = False
        txtTelefono.Enabled = False
        txtEmail.Enabled = False
        cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        cboTipoContra.Enabled = True
        cboEspecialidad.Enabled = True
        txtNombre.Enabled = True
        'txtApellido.Enabled = True
        'txtEspecialidad.Enabled = True

        cboTipoDocIdent.Enabled = True
        NroDoc.Enabled = True
        txtNomRepr.Enabled = True

        txtDireccion.Enabled = True
        txtRuc.Enabled = True
        txtTelefono.Enabled = True
        txtEmail.Enabled = True
        cboEstado.Enabled = True
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        'If sTab = 0 Then
        iOpcion = 3
        sTab = 1
        LimpiarControles()
        HabilitarControles()
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False
        eContratistas = New clsContratistas
        eContratistas.fCodigo(2, gEmpresa)
        txtCodigo.Text = eContratistas.sCodFuturo
        txtCodigo.Enabled = False
        cboTipoContra.Focus()
        Timer1.Enabled = True
        'End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        'If sTab = 0 Then
        sTab = 1
        iOpcion = 4
        VerPosicion()
        HabilitarControles()
        cboEstado.Enabled = True
        txtCodigo.Enabled = False
        Timer1.Enabled = True
        'End If
    End Sub


    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'Dim iResultado As Int32
        Try
            '    Using scope As TransactionScope = New TransactionScope
            If dgvLista.Rows.Count > 0 Then
                'If (MessageBox.Show("Desea Eliminar: " & Chr(13) & Chr(13) & "Codigo: " & Fila("AlmCodigo") & "   " & Chr(13) & "Descripcion: " & Fila("AlmDescripcion"), sNombreSistema, MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                '    'Using scope As TransactionScope = New TransactionScope '(MSDTC)
                '    eAlmacen = New clsAlmacen
                '    iResultado = eAlmacen.fEliminar(Fila("ALMCODIGO"), 3)
                '    'scope.Complete()
                '    'End Using
                '    If iResultado = 1 Then
                '        mMostrarGrilla()
                '    End If
                'Else
                '    Exit Sub
                'End If
                MsgBox("Opcion Bloqueada por el Administrador", MsgBoxStyle.Information, glbNameSistema)
            End If
            'scope.Complete()
            'End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try

    End Sub


    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If
        Panel1.Visible = True
        rdbNombres.Checked = True
        txtBuscar.Clear()
        txtBuscar.Focus()
    End Sub


    Private Sub v_SalidaReg() Handles Me.SalidaReg
        Close()
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdContratista").ToString
                Me.txtNombre.Text = Fila("ConNombre").ToString
                'Me.txtApellido.Text = Fila("ConApellido").ToString
                'Me.txtEspecialidad.Text = Fila("conEspecialidad").ToString
                Me.txtDireccion.Text = Fila("ConDireccion").ToString
                Me.txtRuc.Text = Fila("ConRuc").ToString
                Me.txtTelefono.Text = Fila("ConTelefono").ToString
                Me.txtEmail.Text = Fila("ConEmail").ToString
                If Fila("ConEstado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
                cboTipoContra.SelectedValue = Trim(Fila("TipoConCodigo").ToString())
                cboEspecialidad.SelectedValue = Trim(Fila("esp_codigo").ToString())

                cboTipoDocIdent.SelectedValue = Trim(Fila("DIdeCodigo").ToString())
                NroDoc.Text = Trim(Fila("NroDocIde").ToString())
                txtNomRepr.Text = Trim(Fila("NomRep").ToString())
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.cboTipoContra.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""

        eTempo = New clsPlantTempo
        eContratistas = New clsContratistas
        If sTab = 1 Then
            If Len(Trim(txtCodigo.Text)) > 0 And cboEspecialidad.SelectedIndex <> -1 And cboTipoContra.SelectedIndex <> -1 And Len(Trim(txtNombre.Text)) > 0 And Len(Trim(txtDireccion.Text)) > 0 And Len(Trim(txtRuc.Text)) > 0 And Len(Trim(txtTelefono.Text)) > 0 And Len(Trim(txtEmail.Text)) > 0 And cboTipoDocIdent.SelectedIndex <> -1 And Len(Trim(NroDoc.Text)) > 0 And Len(Trim(txtNomRepr.Text)) > 0 Then
                If iOpcion = 3 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 4 Then
                    Mensaje = "Actualizar"
                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 3 Then
                        eContratistas.fCodigo(2, gEmpresa)
                        sCodigoRegistro = eContratistas.sCodFuturo
                        txtCodigo.Text = Trim(sCodigoRegistro)
                        iResultado = eContratistas.fGrabar(iOpcion, gEmpresa, sCodigoRegistro, Convert.ToString(cboTipoContra.SelectedValue), Convert.ToString(txtNombre.Text.Trim), "", "", Convert.ToString(Me.txtDireccion.Text.Trim), Convert.ToString(Me.txtRuc.Text.Trim), Convert.ToString(Me.txtTelefono.Text.Trim), Convert.ToString(Me.txtEmail.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue), Trim(cboEspecialidad.SelectedValue), Trim(cboTipoDocIdent.SelectedValue), Trim(NroDoc.Text), Trim(txtNomRepr.Text))
                        If iResultado > 0 Then
                            mMostrarGrilla()
                            Timer2.Enabled = True
                        End If
                        sTab = 0
                    ElseIf iOpcion = 4 Then
                        sCodigoRegistro = Trim(txtCodigo.Text)
                        iResultado = eContratistas.fGrabar(iOpcion, gEmpresa, sCodigoRegistro, Convert.ToString(cboTipoContra.SelectedValue), Convert.ToString(txtNombre.Text.Trim), "", "", Convert.ToString(Me.txtDireccion.Text.Trim), Convert.ToString(Me.txtRuc.Text.Trim), Convert.ToString(Me.txtTelefono.Text.Trim), Convert.ToString(Me.txtEmail.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue), Trim(cboEspecialidad.SelectedValue), Trim(cboTipoDocIdent.SelectedValue), Trim(NroDoc.Text), Trim(txtNomRepr.Text))
                        If iResultado > 0 Then
                            mMostrarGrilla()
                        End If
                        sTab = 0
                    End If
                End If
            Else
                If cboTipoContra.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Contratista", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoContra.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtNombre.Text)) = 0 Then
                    MessageBox.Show("Ingrese Datos del Contratista", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNombre.Focus()
                    Exit Sub
                End If

                If cboEspecialidad.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Especialidad", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboEspecialidad.Focus()
                    Exit Sub
                End If

                'If Len(Trim(txtEspecialidad.Text)) = 0 Then
                '    MessageBox.Show("Ingrese la descripcion de Especialidad", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    txtEspecialidad.Focus()
                '    Exit Sub
                'End If

                If Len(Trim(txtDireccion.Text)) = 0 Then
                    MessageBox.Show("Ingrese la Direcci�n del Contratista", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDireccion.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtRuc.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Ruc", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtRuc.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtTelefono.Text)) = 0 Then
                    MessageBox.Show("Ingrese el n�mero de Telefono", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtTelefono.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtEmail.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Email", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtEmail.Focus()
                    Exit Sub
                End If

                If cboTipoDocIdent.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Tipo de Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoDocIdent.Focus()
                    Exit Sub
                End If

                If Len(Trim(NroDoc.Text)) = 0 Then
                    MessageBox.Show("Ingrese el N�mero de Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    NroDoc.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtNomRepr.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Nombre del Representante Legal", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNomRepr.Focus()
                    Exit Sub
                End If

                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub

    Private Sub txtBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscar.TextChanged

        Dim txtCadenaBusqueda As String = ""
        txtCadenaBusqueda = Trim(txtBuscar.Text)

        If Len(txtBuscar.Text) > 0 And rdbNombres.Checked = True Then
            eContratistas = New clsContratistas
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eContratistas.fListar(5, gEmpresa, txtCadenaBusqueda)
            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eContratistas.iNroRegistros
            If eContratistas.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBuscar.Text) > 0 And rdbEspecialidad.Checked = True Then
            eContratistas = New clsContratistas
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eContratistas.fListar(6, gEmpresa, txtCadenaBusqueda)
            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & eContratistas.iNroRegistros
            If eContratistas.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBuscar.Text) = 0 Then
            mMostrarGrilla()
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        'If sTab = 0 Then
        sTab = 1
        VerPosicion()
        DesabilitarControles()
        'End If
    End Sub

    Private Sub rdbNombres_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbNombres.CheckedChanged
        txtBuscar.Clear()
        txtBuscar.Focus()
    End Sub

    Private Sub rdbEspecialidad_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbEspecialidad.CheckedChanged
        txtBuscar.Clear()
        txtBuscar.Focus()
    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click
        Panel1.Visible = False
    End Sub

    Private Sub listandoregistroconprobante()
        Dim v_clsRegistroDocumento As clsRegistroDocumento = New clsRegistroDocumento()
        Dim v_datatable As DataTable = New DataTable()

        v_datatable = v_clsRegistroDocumento.fListarProveedoresxRuc(gEmpresa, v_ruc, "P")
        If (v_datatable.Rows.Count > 0) Then
            txtNombre.Text = v_datatable.Rows(0)("AnaliticoDescripcion")
            txtDireccion.Text = ""
            txtRuc.Text = v_datatable.Rows(0)("ruc")
            txtTelefono.Text = ""
            txtEmail.Text = ""
        End If



    End Sub
End Class
