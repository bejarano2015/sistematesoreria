Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmCaja
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eCaja As clsCaja
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Private eLogin As clsLogin
    Private eUsuario As Usuarios

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCaja = Nothing
    Public Shared Function Instance() As frmCaja
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmCaja
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'Dim CaptionVentana As String = ""
        'CaptionVentana = Trim(Me.Text)
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana " & CaptionVentana & "?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        '    'v()
        '    frmInstance = Nothing
        '    Close()
        'Else
        '    Exit Sub
        'End If
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()

        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1

        cboArea.DataSource = eCaja.fListarArea(gEmpresa)

        If eCaja.iNroRegistros > 0 Then
            cboArea.ValueMember = "AreaCodigo"
            cboArea.DisplayMember = "AreaNombre"
            cboArea.SelectedIndex = -1
        End If


        cboAreas.DataSource = eCaja.fListarArea(gEmpresa)
        If eCaja.iNroRegistros > 0 Then
            cboAreas.ValueMember = "AreaCodigo"
            cboAreas.DisplayMember = "AreaNombre"
            cboAreas.SelectedIndex = -1
        End If

        cboMoneda.DataSource = eCaja.fListarListarMoneda()
        If eCaja.iNroRegistros > 0 Then
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = -1
        End If

        eUsuario = New Usuarios

        cboUsuarios.DataSource = eUsuario.ListaUsuario(gEmpresa, glbSisCodigo)
        If eCaja.iNroRegistros > 0 Then
            cboUsuarios.ValueMember = "UsuCodigo"
            cboUsuarios.DisplayMember = "UsuCodigo"
            cboUsuarios.SelectedIndex = -1
        End If

        dgvLista.Focus()
        dgvLista.Width = 926
        dgvLista.Height = 397

    End Sub

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtDescripcion.Clear()
        txtMontoMinimo.Clear()
        txtMontoMaximo.Clear()
        txtNombre.Clear()
        txtPla.Clear()
        txtCod.Clear()
        txtEmpr.Clear()
        cboEstado.SelectedIndex = 0
        cboArea.SelectedIndex = -1
        cboMoneda.SelectedIndex = -1
    End Sub

    Private Sub DesabilitarControles()
        txtCodigo.Enabled = False
        txtDescripcion.Enabled = False
        txtMontoMinimo.Enabled = False
        txtMontoMaximo.Enabled = False
        txtNombre.Enabled = False
        txtCod.Enabled = False
        txtPla.Enabled = False
        txtEmpr.Enabled = False
        cboArea.Enabled = False
        cboMoneda.Enabled = False
        cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        txtDescripcion.Enabled = True
        txtMontoMinimo.Enabled = True
        txtMontoMaximo.Enabled = True
        txtNombre.Enabled = True
        txtCod.Enabled = True
        txtPla.Enabled = True
        txtEmpr.Enabled = True
        cboArea.Enabled = True
        cboMoneda.Enabled = True
        cboEstado.Enabled = True
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            cboEstado.Enabled = False
            cboUsuarios.SelectedIndex = -1
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            HabilitarControles()
            cboEstado.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'Dim CaptionVentana As String = ""
        'CaptionVentana = Trim(Me.Text)
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana " & CaptionVentana & "?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        '    Close()
        'Else
        '    Exit Sub
        'End If

        Close()
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            DesabilitarControles()
            VerPosicion()
        End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim sIdEmpleado As String = ""
        Dim sIdMoneda As String = ""
        Dim Mensaje As String = ""
        eTempo = New clsPlantTempo
        eCaja = New clsCaja
        If sTab = 1 Then

            If Len(txtDescripcion.Text.Trim) > 0 And Len(txtMontoMinimo.Text.Trim) > 0 And Len(txtMontoMaximo.Text.Trim) > 0 And cboArea.SelectedIndex > -1 And Len(txtCod.Text) = 10 And cboMoneda.SelectedIndex > -1 And cboUsuarios.SelectedIndex > -1 Then
                If Val(txtMontoMaximo.Text.Trim) < Val(txtMontoMinimo.Text.Trim) Then
                    MessageBox.Show("El Monto Base debe ser Mayor al Monto Minimo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtMontoMaximo.Focus()
                    Exit Sub
                End If

                If iOpcion = 1 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 2 Then
                    Mensaje = "Actualizar"
                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then
                        eCaja.fCodigo(gEmpresa)
                        sCodigoRegistro = eCaja.sCodFuturo
                        txtCodigo.Text = sCodigoRegistro
                    Else
                        sCodigoRegistro = Trim(txtCodigo.Text)
                    End If
                    sIdEmpleado = Trim(txtCod.Text)
                    sIdMoneda = Trim(Convert.ToString(cboMoneda.SelectedValue))
                    iResultado = eCaja.fGrabar(Trim(cboArea.SelectedValue), sCodigoRegistro, txtDescripcion.Text.Trim, Val(txtMontoMinimo.Text.Trim), Val(txtMontoMaximo.Text.Trim), cboEstado.SelectedValue, iOpcion, gEmpresa, sIdEmpleado, sIdMoneda, Trim(txtPla.Text), Trim(txtNombre.Text), txtEmpr.Text.Trim, Trim(cboUsuarios.SelectedValue))
                    If iResultado > 0 Then
                        mMostrarGrilla()
                        'Timer2.Enabled = True
                    End If
                    sTab = 0
                End If

            Else

                If Len(txtDescripcion.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese la Descripcion de la Caja", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtDescripcion.Focus()
                    Exit Sub
                End If

                If cboArea.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Area", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboArea.Focus()
                    Exit Sub
                End If

                If cboUsuarios.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Usuario del Sistema como Responsable de esta Caja", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboUsuarios.Focus()
                    Exit Sub
                End If

                If Len(txtCod.Text.Trim) = 0 Then
                    MessageBox.Show("Seleccione un Empleado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNombre.Focus()
                    Exit Sub
                End If

                If cboMoneda.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Moneda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboMoneda.Focus()
                    Exit Sub
                End If

                If Len(txtMontoMinimo.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese el Monto Minimo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtMontoMinimo.Focus()
                    Exit Sub
                End If

                If Len(txtMontoMaximo.Text.Trim) = 0 Then
                    MessageBox.Show("Ingrese el Monto Base", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtMontoMaximo.Focus()
                    Exit Sub
                End If
                'validar cada campo requeriendole al usuario
                'eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Me.dtTable.Rows.Count > 0 Then


            If Fila("Estados") = "Activo" Then
                Dim iResultado As Int32
                Dim Mensaje As String = ""
                Dim iOpcionDel As Integer
                If sTab = 0 Then
                    Try
                        If Me.dgvLista.Rows.Count > 0 Then
                            If Fila("Estados") = "Activo" Then
                                iOpcionDel = 5
                                Mensaje = "�Desea Anular?"
                            End If
                            If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("IdCaja") & "   " & Chr(13) & "Descripci�n: " & Fila("DescripcionCaja"), "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                                eCaja = New clsCaja
                                iResultado = eCaja.fEliminar(Fila("IdCaja"), iOpcionDel, gEmpresa)
                                If iResultado = 1 Then
                                    mMostrarGrilla()
                                End If
                            Else
                                Exit Sub
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                End If
            Else
                MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If

    End Sub

   

    Private Sub mMostrarGrilla()
        eCaja = New clsCaja
        eTempo = New clsPlantTempo
        dtTable = New DataTable

        dtTable = eCaja.fListar(iEstado01, iEstado02, gEmpresa)
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTable
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        Me.stsTotales.Items(0).Text = "Total de Registros= " & eCaja.iNroRegistros
        If eCaja.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Trim(Fila("IdCaja").ToString)
                Me.txtDescripcion.Text = Trim(Fila("DescripcionCaja").ToString)
                Me.txtMontoMinimo.Text = Trim(Fila("MontoMinimo"))
                Me.txtMontoMaximo.Text = Trim(Fila("MontoMaximo"))

                Me.txtNombre.Text = Trim(Fila("Nombres"))
                Me.txtCod.Text = Trim(Fila("Responsable"))
                Me.txtPla.Text = Trim(Fila("TPlaCodigo"))
                Me.txtEmpr.Text = Trim(Fila("EmprResposable"))

                cboArea.SelectedValue = Trim(Fila("CodArea").ToString)
                'cboEmpleado.SelectedValue = Trim(Fila("PadCodigo").ToString)
                cboMoneda.SelectedValue = Trim(Fila("IdMoneda").ToString)
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If

                cboUsuarios.SelectedValue = Trim(Fila("UsuCodigo"))

            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    '----------------------------------------

    'Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.txtDescripcion.Focus()
    '    Timer1.Enabled = False
    'End Sub

    'Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Me.dgvLista.Focus()
    '    Timer2.Enabled = False
    'End Sub

    'Private Sub v_VisualizaReg() Handles Me.VisualizaReg
    '    If sTab = 0 Then
    '        sTab = 1
    '        DesabilitarControles()
    '    End If
    'End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            'Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        Panel2.Visible = True
        cboAreas.SelectedIndex = -1
        txtDes.Clear()
        txtDes.Focus()
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub BeLabel8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel8.Click
        Panel2.Visible = False
    End Sub

    Private Sub txtDes_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDes.TextChanged
        If Len(Trim(txtDes.Text)) > 0 Then
            '----------------------------->
            eCaja = New clsCaja
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eCaja.fBuscarCajas(0, gEmpresa, "", Trim(txtDes.Text), 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eCaja.iNroRegistros
            If eCaja.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
            '----------------------------->
        Else
            '----------------------------->
            eCaja = New clsCaja
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eCaja.fBuscarCajas(4, gEmpresa, "", "", 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eCaja.iNroRegistros
            If eCaja.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
            '----------------------------->
        End If
    End Sub

    Private Sub cboAreas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAreas.SelectedIndexChanged
        Dim IdArea As String = ""
        If (cboAreas.SelectedIndex > -1) Then
            Try
                IdArea = Trim(cboAreas.SelectedValue.ToString)
                If IdArea <> "System.Data.DataRowView" Then
                    '----------------------------->
                    eCaja = New clsCaja
                    eTempo = New clsPlantTempo
                    dtTable = New DataTable
                    dtTable = eCaja.fBuscarCajas(1, gEmpresa, IdArea, "", 0)
                    Me.dgvLista.AutoGenerateColumns = False
                    Me.dgvLista.DataSource = dtTable
                    cmr = Me.BindingContext(Me.dgvLista.DataSource)
                    Me.stsTotales.Items(0).Text = "Total de Registros= " & eCaja.iNroRegistros
                    If eCaja.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                    '----------------------------->
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub txtNombre_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNombre.DoubleClick
        dtTable = New DataTable
        eCaja = New clsCaja
        dtTable = eCaja.fListarListarEmpleados(gEmpresa) 'eCuenta.fListarBancos(gEmpresa) 'Bancos
        If dtTable.Rows.Count > 0 Then
            dgvEmpleados.DataSource = dtTable
            Panel4.Visible = True
            dgvEmpleados.Focus()
        End If
    End Sub

    Private Sub BeTextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNombre.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                dtTable = New DataTable
                'cboEmpleado.DataSource = eCaja.fListarListarEmpleados(gEmpresa)
                'cboEmpleado.ValueMember = "PadCodigo"
                'cboEmpleado.DisplayMember = "Nombres"
                'cboEmpleado.SelectedIndex = -1
                eCaja = New clsCaja
                dtTable = eCaja.fListarListarEmpleados(gEmpresa) 'eCuenta.fListarBancos(gEmpresa) 'Bancos
                If dtTable.Rows.Count > 0 Then
                    dgvEmpleados.DataSource = dtTable
                    Panel4.Visible = True
                    dgvEmpleados.Focus()
                End If
        End Select
    End Sub
    
 
    'Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
    '    'Dim IdEstado As String = ""

    '    If (cboEstados.SelectedIndex > -1) Then
    '        Try
    '            'IdEstado = cboEstados.SelectedValue
    '            'If IdEstado <> "System.Data.DataRowView" Then
    '            eCaja = New clsCaja
    '            eTempo = New clsPlantTempo
    '            dtTable = New DataTable
    '            If cboAreas.SelectedIndex = -1 Then
    '                dtTable = eCaja.fBuscarCajas(2, gEmpresa, "", "", cboEstados.SelectedValue)
    '            ElseIf cboAreas.SelectedIndex > -1 Then
    '                dtTable = eCaja.fBuscarCajas(3, gEmpresa, Trim(cboAreas.SelectedValue), "", cboEstados.SelectedValue)
    '            Else
    '                Exit Sub
    '            End If
    '            Me.dgvLista.AutoGenerateColumns = False
    '            Me.dgvLista.DataSource = dtTable
    '            cmr = Me.BindingContext(Me.dgvLista.DataSource)
    '            Me.stsTotales.Items(0).Text = "Total de Registros= " & eCaja.iNroRegistros
    '            If eCaja.iNroRegistros > 0 Then
    '                frmPrincipal.Modifica.Enabled = True
    '                frmPrincipal.Elimina.Enabled = True
    '                frmPrincipal.Visualiza.Enabled = True
    '                frmPrincipal.fDatos = True
    '            Else
    '                frmPrincipal.Modifica.Enabled = False
    '                frmPrincipal.Elimina.Enabled = False
    '                frmPrincipal.Visualiza.Enabled = False
    '                frmPrincipal.fDatos = False
    '            End If
    '            'End If
    '        Catch ex As System.IndexOutOfRangeException
    '            Exit Sub
    '        End Try
    '    End If
    'End Sub

    'Private Sub cboEmpresas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If (cboEmpresas.SelectedIndex > -1) Then
    '        Try
    '            Dim IdEmpresa As String = ""
    '            IdEmpresa = cboEmpresas.SelectedValue.ToString
    '            If IdEmpresa <> "System.Data.DataRowView" Then

    '                cboEmpleado.DataSource = eCaja.fListarListarEmpleados(IdEmpresa.Trim)
    '                If eCaja.iNroRegistros > 0 Then
    '                    cboEmpleado.ValueMember = "PadCodigo"
    '                    cboEmpleado.DisplayMember = "Nombres"
    '                    cboEmpleado.SelectedIndex = -1
    '                End If

    '                cboArea.DataSource = eCaja.fListarArea(IdEmpresa.Trim)
    '                If eCaja.iNroRegistros > 0 Then
    '                    cboArea.ValueMember = "AreaCodigo"
    '                    cboArea.DisplayMember = "AreaNombre"
    '                    cboArea.SelectedIndex = -1
    '                End If

    '                cboAreas.DataSource = eCaja.fListarArea(IdEmpresa.Trim)
    '                If eCaja.iNroRegistros > 0 Then
    '                    cboAreas.ValueMember = "AreaCodigo"
    '                    cboAreas.DisplayMember = "AreaNombre"
    '                    cboAreas.SelectedIndex = -1
    '                End If


    '                'cboCuenta.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
    '                'cboCuenta.ValueMember = "IdCuenta"
    '                'cboCuenta.DisplayMember = "NumeroCuenta"
    '                'cboCuenta.SelectedIndex = -1
    '                'lblMoneda.Text = ""
    '                'txtNumChequera.Clear()


    '            End If
    '        Catch ex As System.IndexOutOfRangeException
    '            Exit Sub
    '        End Try





    '    End If
    'End Sub

    Private Sub dgvEmpleados_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEmpleados.DoubleClick
        If dgvEmpleados.Rows.Count > 0 Then
            Try
                txtNombre.Text = Trim(dgvEmpleados.Rows(dgvEmpleados.CurrentRow.Index).Cells(2).Value)
                txtCod.Text = Trim(dgvEmpleados.Rows(dgvEmpleados.CurrentRow.Index).Cells(1).Value)
                txtPla.Text = Trim(dgvEmpleados.Rows(dgvEmpleados.CurrentRow.Index).Cells(0).Value)
                txtEmpr.Text = Trim(dgvEmpleados.Rows(dgvEmpleados.CurrentRow.Index).Cells(3).Value)
                Panel4.Visible = False
                cboMoneda.Focus()
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub dgvEmpleados_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmpleados.CellContentClick

    End Sub


    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click
        Panel4.Visible = False
    End Sub


    Private Sub txtNombre_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombre.TextChanged

    End Sub
End Class
