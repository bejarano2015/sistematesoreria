
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteAnexos2

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptF As New rptDepPlazo


    Public RepAnexosTipoCuenta As String = ""
    Public RepAnexosIdBanco As String = ""
    Public RepAnexosIdBanco2 As String = ""

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAnexos2 = Nothing
    Public Shared Function Instance() As frmReporteAnexos2
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteAnexos2
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteAnexos2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteAnexos2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rptF.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        '1
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@CodEmpresa")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@IdBanco")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = RepAnexosIdBanco
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)


        '3
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@TipoCuenta")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = RepAnexosTipoCuenta
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '4
        crParameterFieldDefinitions = rptF.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@IdBanco2")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = RepAnexosIdBanco2
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = rptF

    End Sub

End Class