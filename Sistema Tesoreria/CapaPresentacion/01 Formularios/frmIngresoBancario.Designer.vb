<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngresoBancario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIngresoBancario))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.mnuBotonesMantenimiento = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnModificar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.pnDetalle = New System.Windows.Forms.Panel()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.txtCodigoEmpresa = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.txtCodigoCajaExtraordinaria = New ctrLibreria.Controles.BeTextBox()
        Me.btnBuscaCajaExtraordinaria = New ctrLibreria.Controles.BeButton()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.txtImporte = New System.Windows.Forms.TextBox()
        Me.cboBanco = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.dtpFechaDeposito = New ctrLibreria.Controles.BeDateTimePicker()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox()
        Me.txtCajaExtraordinaria = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroOperacion = New ctrLibreria.Controles.BeTextBox()
        Me.txtCodMoneda = New ctrLibreria.Controles.BeTextBox()
        Me.txtGlosa = New ctrLibreria.Controles.BeTextBox()
        Me.Panel1.SuspendLayout()
        Me.mnuBotonesMantenimiento.SuspendLayout()
        Me.pnDetalle.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.mnuBotonesMantenimiento)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(479, 28)
        Me.Panel1.TabIndex = 171
        '
        'mnuBotonesMantenimiento
        '
        Me.mnuBotonesMantenimiento.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnModificar, Me.btnImprimir, Me.btnSalir})
        Me.mnuBotonesMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.mnuBotonesMantenimiento.Name = "mnuBotonesMantenimiento"
        Me.mnuBotonesMantenimiento.Size = New System.Drawing.Size(477, 25)
        Me.mnuBotonesMantenimiento.TabIndex = 0
        Me.mnuBotonesMantenimiento.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnModificar
        '
        Me.btnModificar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnModificar.Image = CType(resources.GetObject("btnModificar.Image"), System.Drawing.Image)
        Me.btnModificar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(23, 22)
        Me.btnModificar.Text = "&Modificar"
        Me.btnModificar.ToolTipText = "Editar"
        '
        'btnImprimir
        '
        Me.btnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimir.Text = "ToolStripButton1"
        Me.btnImprimir.ToolTipText = "Imprimir Caja"
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'pnDetalle
        '
        Me.pnDetalle.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnDetalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnDetalle.Controls.Add(Me.txtGlosa)
        Me.pnDetalle.Controls.Add(Me.BeLabel3)
        Me.pnDetalle.Controls.Add(Me.BeLabel2)
        Me.pnDetalle.Controls.Add(Me.txtCodigoEmpresa)
        Me.pnDetalle.Controls.Add(Me.BeLabel1)
        Me.pnDetalle.Controls.Add(Me.txtCodigoCajaExtraordinaria)
        Me.pnDetalle.Controls.Add(Me.btnBuscaCajaExtraordinaria)
        Me.pnDetalle.Controls.Add(Me.BeLabel5)
        Me.pnDetalle.Controls.Add(Me.txtImporte)
        Me.pnDetalle.Controls.Add(Me.cboBanco)
        Me.pnDetalle.Controls.Add(Me.BeLabel11)
        Me.pnDetalle.Controls.Add(Me.dtpFechaDeposito)
        Me.pnDetalle.Controls.Add(Me.BeLabel10)
        Me.pnDetalle.Controls.Add(Me.cboCuenta)
        Me.pnDetalle.Controls.Add(Me.txtCajaExtraordinaria)
        Me.pnDetalle.Controls.Add(Me.BeLabel12)
        Me.pnDetalle.Controls.Add(Me.BeLabel15)
        Me.pnDetalle.Controls.Add(Me.BeLabel9)
        Me.pnDetalle.Controls.Add(Me.txtNroOperacion)
        Me.pnDetalle.Controls.Add(Me.txtCodMoneda)
        Me.pnDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnDetalle.Location = New System.Drawing.Point(0, 28)
        Me.pnDetalle.Name = "pnDetalle"
        Me.pnDetalle.Size = New System.Drawing.Size(479, 356)
        Me.pnDetalle.TabIndex = 172
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(23, 167)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel3.TabIndex = 55
        Me.BeLabel3.Text = "Glosa"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(23, 317)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel2.TabIndex = 53
        Me.BeLabel2.Text = "codigo empresa"
        Me.BeLabel2.Visible = False
        '
        'txtCodigoEmpresa
        '
        Me.txtCodigoEmpresa.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigoEmpresa.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigoEmpresa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoEmpresa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoEmpresa.Enabled = False
        Me.txtCodigoEmpresa.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoEmpresa.KeyEnter = True
        Me.txtCodigoEmpresa.Location = New System.Drawing.Point(157, 310)
        Me.txtCodigoEmpresa.Name = "txtCodigoEmpresa"
        Me.txtCodigoEmpresa.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigoEmpresa.ReadOnly = True
        Me.txtCodigoEmpresa.ShortcutsEnabled = False
        Me.txtCodigoEmpresa.Size = New System.Drawing.Size(184, 20)
        Me.txtCodigoEmpresa.TabIndex = 52
        Me.txtCodigoEmpresa.TabStop = False
        Me.txtCodigoEmpresa.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCodigoEmpresa.Visible = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(23, 291)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(128, 13)
        Me.BeLabel1.TabIndex = 50
        Me.BeLabel1.Text = "codigo caja extraordinaria"
        Me.BeLabel1.Visible = False
        '
        'txtCodigoCajaExtraordinaria
        '
        Me.txtCodigoCajaExtraordinaria.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigoCajaExtraordinaria.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCodigoCajaExtraordinaria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoCajaExtraordinaria.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoCajaExtraordinaria.Enabled = False
        Me.txtCodigoCajaExtraordinaria.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoCajaExtraordinaria.KeyEnter = True
        Me.txtCodigoCajaExtraordinaria.Location = New System.Drawing.Point(157, 284)
        Me.txtCodigoCajaExtraordinaria.Name = "txtCodigoCajaExtraordinaria"
        Me.txtCodigoCajaExtraordinaria.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigoCajaExtraordinaria.ReadOnly = True
        Me.txtCodigoCajaExtraordinaria.ShortcutsEnabled = False
        Me.txtCodigoCajaExtraordinaria.Size = New System.Drawing.Size(184, 20)
        Me.txtCodigoCajaExtraordinaria.TabIndex = 51
        Me.txtCodigoCajaExtraordinaria.TabStop = False
        Me.txtCodigoCajaExtraordinaria.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        Me.txtCodigoCajaExtraordinaria.Visible = False
        '
        'btnBuscaCajaExtraordinaria
        '
        Me.btnBuscaCajaExtraordinaria.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnBuscaCajaExtraordinaria.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscaCajaExtraordinaria.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_
        Me.btnBuscaCajaExtraordinaria.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBuscaCajaExtraordinaria.Location = New System.Drawing.Point(348, 33)
        Me.btnBuscaCajaExtraordinaria.Name = "btnBuscaCajaExtraordinaria"
        Me.btnBuscaCajaExtraordinaria.Size = New System.Drawing.Size(26, 22)
        Me.btnBuscaCajaExtraordinaria.TabIndex = 49
        Me.btnBuscaCajaExtraordinaria.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnBuscaCajaExtraordinaria.UseVisualStyleBackColor = True
        Me.btnBuscaCajaExtraordinaria.Visible = False
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(23, 18)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(65, 13)
        Me.BeLabel5.TabIndex = 26
        Me.BeLabel5.Text = "Fecha Pago"
        '
        'txtImporte
        '
        Me.txtImporte.BackColor = System.Drawing.Color.Ivory
        Me.txtImporte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImporte.Location = New System.Drawing.Point(130, 139)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(106, 20)
        Me.txtImporte.TabIndex = 48
        Me.txtImporte.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboBanco
        '
        Me.cboBanco.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBanco.BackColor = System.Drawing.Color.Ivory
        Me.cboBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBanco.ForeColor = System.Drawing.Color.Black
        Me.cboBanco.FormattingEnabled = True
        Me.cboBanco.KeyEnter = True
        Me.cboBanco.Location = New System.Drawing.Point(130, 61)
        Me.cboBanco.Name = "cboBanco"
        Me.cboBanco.Size = New System.Drawing.Size(212, 21)
        Me.cboBanco.TabIndex = 32
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(23, 69)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(38, 13)
        Me.BeLabel11.TabIndex = 31
        Me.BeLabel11.Text = "Banco"
        '
        'dtpFechaDeposito
        '
        Me.dtpFechaDeposito.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaDeposito.CustomFormat = ""
        Me.dtpFechaDeposito.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaDeposito.KeyEnter = True
        Me.dtpFechaDeposito.Location = New System.Drawing.Point(130, 11)
        Me.dtpFechaDeposito.Name = "dtpFechaDeposito"
        Me.dtpFechaDeposito.Size = New System.Drawing.Size(106, 20)
        Me.dtpFechaDeposito.TabIndex = 27
        Me.dtpFechaDeposito.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaDeposito.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(23, 43)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(94, 13)
        Me.BeLabel10.TabIndex = 29
        Me.BeLabel10.Text = "Caja extraordinaria"
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(130, 86)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(212, 21)
        Me.cboCuenta.TabIndex = 34
        '
        'txtCajaExtraordinaria
        '
        Me.txtCajaExtraordinaria.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCajaExtraordinaria.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtCajaExtraordinaria.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCajaExtraordinaria.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCajaExtraordinaria.Enabled = False
        Me.txtCajaExtraordinaria.ForeColor = System.Drawing.Color.Black
        Me.txtCajaExtraordinaria.KeyEnter = True
        Me.txtCajaExtraordinaria.Location = New System.Drawing.Point(130, 36)
        Me.txtCajaExtraordinaria.Name = "txtCajaExtraordinaria"
        Me.txtCajaExtraordinaria.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCajaExtraordinaria.ReadOnly = True
        Me.txtCajaExtraordinaria.ShortcutsEnabled = False
        Me.txtCajaExtraordinaria.Size = New System.Drawing.Size(211, 20)
        Me.txtCajaExtraordinaria.TabIndex = 30
        Me.txtCajaExtraordinaria.TabStop = False
        Me.txtCajaExtraordinaria.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(23, 92)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel12.TabIndex = 33
        Me.BeLabel12.Text = "Nº Cta Cte"
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(23, 145)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(42, 13)
        Me.BeLabel15.TabIndex = 47
        Me.BeLabel15.Text = "Importe"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(23, 121)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(71, 13)
        Me.BeLabel9.TabIndex = 44
        Me.BeLabel9.Text = "Nº Operacion"
        '
        'txtNroOperacion
        '
        Me.txtNroOperacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroOperacion.BackColor = System.Drawing.Color.Ivory
        Me.txtNroOperacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroOperacion.ForeColor = System.Drawing.Color.Black
        Me.txtNroOperacion.KeyEnter = False
        Me.txtNroOperacion.Location = New System.Drawing.Point(130, 113)
        Me.txtNroOperacion.MaxLength = 20
        Me.txtNroOperacion.Name = "txtNroOperacion"
        Me.txtNroOperacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroOperacion.ShortcutsEnabled = False
        Me.txtNroOperacion.Size = New System.Drawing.Size(106, 20)
        Me.txtNroOperacion.TabIndex = 45
        Me.txtNroOperacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtCodMoneda
        '
        Me.txtCodMoneda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodMoneda.BackColor = System.Drawing.Color.Ivory
        Me.txtCodMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodMoneda.Enabled = False
        Me.txtCodMoneda.ForeColor = System.Drawing.Color.Black
        Me.txtCodMoneda.KeyEnter = True
        Me.txtCodMoneda.Location = New System.Drawing.Point(348, 86)
        Me.txtCodMoneda.MaxLength = 20
        Me.txtCodMoneda.Name = "txtCodMoneda"
        Me.txtCodMoneda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodMoneda.ShortcutsEnabled = False
        Me.txtCodMoneda.Size = New System.Drawing.Size(100, 20)
        Me.txtCodMoneda.TabIndex = 35
        Me.txtCodMoneda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtGlosa
        '
        Me.txtGlosa.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtGlosa.BackColor = System.Drawing.Color.Ivory
        Me.txtGlosa.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGlosa.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGlosa.ForeColor = System.Drawing.Color.Black
        Me.txtGlosa.KeyEnter = False
        Me.txtGlosa.Location = New System.Drawing.Point(130, 167)
        Me.txtGlosa.MaxLength = 20
        Me.txtGlosa.Multiline = True
        Me.txtGlosa.Name = "txtGlosa"
        Me.txtGlosa.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtGlosa.ShortcutsEnabled = False
        Me.txtGlosa.Size = New System.Drawing.Size(211, 87)
        Me.txtGlosa.TabIndex = 56
        Me.txtGlosa.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'frmIngresoBancario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(479, 384)
        Me.Controls.Add(Me.pnDetalle)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmIngresoBancario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso bancario - Caja extraordinaria"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.mnuBotonesMantenimiento.ResumeLayout(False)
        Me.mnuBotonesMantenimiento.PerformLayout()
        Me.pnDetalle.ResumeLayout(False)
        Me.pnDetalle.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents mnuBotonesMantenimiento As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnModificar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnDetalle As System.Windows.Forms.Panel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigoEmpresa As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigoCajaExtraordinaria As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnBuscaCajaExtraordinaria As ctrLibreria.Controles.BeButton
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtImporte As System.Windows.Forms.TextBox
    Friend WithEvents cboBanco As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaDeposito As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtCajaExtraordinaria As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroOperacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodMoneda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtGlosa As ctrLibreria.Controles.BeTextBox
End Class
