<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporLibroRetenciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim RadListDataItem9 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem10 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem11 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem12 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem13 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem14 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem15 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem16 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem17 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem18 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem19 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem20 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem1 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem2 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem3 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem4 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboMes = New Telerik.WinControls.UI.RadDropDownList()
        Me.btnGenerar = New System.Windows.Forms.Button()
        Me.cboAnio = New Telerik.WinControls.UI.RadDropDownList()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboDocumentoOrigen = New Telerik.WinControls.UI.RadDropDownList()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.cboMes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboAnio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDocumentoOrigen, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.CachedPageNumberPerDoc = 10
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 61)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(1065, 541)
        Me.CrystalReportViewer1.TabIndex = 1
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cboDocumentoOrigen)
        Me.GroupBox1.Controls.Add(Me.cboMes)
        Me.GroupBox1.Controls.Add(Me.btnGenerar)
        Me.GroupBox1.Controls.Add(Me.cboAnio)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(607, 43)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Buscar por Fechas"
        '
        'cboMes
        '
        Me.cboMes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        RadListDataItem9.Text = "Enero"
        RadListDataItem9.TextWrap = True
        RadListDataItem10.Text = "Febrero"
        RadListDataItem10.TextWrap = True
        RadListDataItem11.Text = "Marzo"
        RadListDataItem11.TextWrap = True
        RadListDataItem12.Text = "Abril"
        RadListDataItem12.TextWrap = True
        RadListDataItem13.Text = "Mayo"
        RadListDataItem13.TextWrap = True
        RadListDataItem14.Text = "Junio"
        RadListDataItem14.TextWrap = True
        RadListDataItem15.Text = "Julio"
        RadListDataItem15.TextWrap = True
        RadListDataItem16.Text = "Agosto"
        RadListDataItem16.TextWrap = True
        RadListDataItem17.Text = "Septiembre"
        RadListDataItem17.TextWrap = True
        RadListDataItem18.Text = "Octubre"
        RadListDataItem18.TextWrap = True
        RadListDataItem19.Text = "Noviembre"
        RadListDataItem19.TextWrap = True
        RadListDataItem20.Text = "Diciembre"
        RadListDataItem20.TextWrap = True
        Me.cboMes.Items.Add(RadListDataItem9)
        Me.cboMes.Items.Add(RadListDataItem10)
        Me.cboMes.Items.Add(RadListDataItem11)
        Me.cboMes.Items.Add(RadListDataItem12)
        Me.cboMes.Items.Add(RadListDataItem13)
        Me.cboMes.Items.Add(RadListDataItem14)
        Me.cboMes.Items.Add(RadListDataItem15)
        Me.cboMes.Items.Add(RadListDataItem16)
        Me.cboMes.Items.Add(RadListDataItem17)
        Me.cboMes.Items.Add(RadListDataItem18)
        Me.cboMes.Items.Add(RadListDataItem19)
        Me.cboMes.Items.Add(RadListDataItem20)
        Me.cboMes.Location = New System.Drawing.Point(157, 17)
        Me.cboMes.Name = "cboMes"
        Me.cboMes.Size = New System.Drawing.Size(114, 20)
        Me.cboMes.TabIndex = 19
        '
        'btnGenerar
        '
        Me.btnGenerar.Location = New System.Drawing.Point(518, 15)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(75, 23)
        Me.btnGenerar.TabIndex = 4
        Me.btnGenerar.Text = "&Generar"
        Me.btnGenerar.UseVisualStyleBackColor = True
        '
        'cboAnio
        '
        Me.cboAnio.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        RadListDataItem1.Text = "2015"
        RadListDataItem1.TextWrap = True
        RadListDataItem2.Text = "2014"
        RadListDataItem2.TextWrap = True
        RadListDataItem3.Text = "2013"
        RadListDataItem3.TextWrap = True
        RadListDataItem4.Text = "2012"
        RadListDataItem4.TextWrap = True
        Me.cboAnio.Items.Add(RadListDataItem1)
        Me.cboAnio.Items.Add(RadListDataItem2)
        Me.cboAnio.Items.Add(RadListDataItem3)
        Me.cboAnio.Items.Add(RadListDataItem4)
        Me.cboAnio.Location = New System.Drawing.Point(40, 17)
        Me.cboAnio.Name = "cboAnio"
        Me.cboAnio.Size = New System.Drawing.Size(78, 20)
        Me.cboAnio.TabIndex = 21
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(124, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Mes"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(28, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Anio"
        '
        'cboDocumentoOrigen
        '
        Me.cboDocumentoOrigen.AutoCompleteDisplayMember = "DESCRIPCION"
        Me.cboDocumentoOrigen.AutoCompleteValueMember = "ID_DOCUMENTO"
        Me.cboDocumentoOrigen.DisplayMember = "DESCRIPCION"
        Me.cboDocumentoOrigen.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboDocumentoOrigen.Location = New System.Drawing.Point(345, 17)
        Me.cboDocumentoOrigen.Name = "cboDocumentoOrigen"
        Me.cboDocumentoOrigen.Size = New System.Drawing.Size(156, 20)
        Me.cboDocumentoOrigen.TabIndex = 88
        Me.cboDocumentoOrigen.ValueMember = "ID_DOCUMENTO"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(277, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 89
        Me.Label3.Text = "Documento"
        '
        'frmReporLibroRetenciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1065, 602)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "frmReporLibroRetenciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Libro de Retenciones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cboMes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboAnio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDocumentoOrigen, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents cboAnio As Telerik.WinControls.UI.RadDropDownList
    Private WithEvents cboMes As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboDocumentoOrigen As Telerik.WinControls.UI.RadDropDownList
End Class
