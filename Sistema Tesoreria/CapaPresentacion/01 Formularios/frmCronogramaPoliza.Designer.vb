<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCronogramaPoliza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.rdbEndosos = New System.Windows.Forms.RadioButton
        Me.rdbPoliza = New System.Windows.Forms.RadioButton
        Me.cboEndosos = New ctrLibreria.Controles.BeComboBox
        Me.txtAseguradora = New ctrLibreria.Controles.BeTextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.BeButton2 = New ctrLibreria.Controles.BeButton
        Me.txtTotal = New ctrLibreria.Controles.BeTextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtSubTotal = New ctrLibreria.Controles.BeTextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtInteres = New ctrLibreria.Controles.BeTextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtMoneda = New ctrLibreria.Controles.BeTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtVigencia = New ctrLibreria.Controles.BeTextBox
        Me.txtDesRamo = New ctrLibreria.Controles.BeTextBox
        Me.txtPoliza = New ctrLibreria.Controles.BeTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.BeButton1 = New ctrLibreria.Controles.BeButton
        Me.dgvCronograma = New System.Windows.Forms.DataGridView
        Me.Cuota = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaVencimiento = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Fecha_Cancelacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Boton = New System.Windows.Forms.DataGridViewButtonColumn
        Me.BotonCancela = New System.Windows.Forms.DataGridViewButtonColumn
        Me.btnImprimir = New ctrLibreria.Controles.BeButton
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvCronograma, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtAseguradora)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.BeButton2)
        Me.GroupBox1.Controls.Add(Me.txtTotal)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtSubTotal)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtInteres)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtMoneda)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtVigencia)
        Me.GroupBox1.Controls.Add(Me.txtDesRamo)
        Me.GroupBox1.Controls.Add(Me.txtPoliza)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(641, 139)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdbEndosos)
        Me.GroupBox2.Controls.Add(Me.rdbPoliza)
        Me.GroupBox2.Controls.Add(Me.cboEndosos)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(180, 93)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(302, 40)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Vigencia"
        '
        'rdbEndosos
        '
        Me.rdbEndosos.AutoSize = True
        Me.rdbEndosos.Location = New System.Drawing.Point(68, 17)
        Me.rdbEndosos.Name = "rdbEndosos"
        Me.rdbEndosos.Size = New System.Drawing.Size(66, 17)
        Me.rdbEndosos.TabIndex = 18
        Me.rdbEndosos.Text = "Endosos"
        Me.rdbEndosos.UseVisualStyleBackColor = True
        '
        'rdbPoliza
        '
        Me.rdbPoliza.AutoSize = True
        Me.rdbPoliza.Checked = True
        Me.rdbPoliza.Location = New System.Drawing.Point(6, 17)
        Me.rdbPoliza.Name = "rdbPoliza"
        Me.rdbPoliza.Size = New System.Drawing.Size(53, 17)
        Me.rdbPoliza.TabIndex = 17
        Me.rdbPoliza.TabStop = True
        Me.rdbPoliza.Text = "Poliza"
        Me.rdbPoliza.UseVisualStyleBackColor = True
        '
        'cboEndosos
        '
        Me.cboEndosos.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEndosos.BackColor = System.Drawing.Color.Ivory
        Me.cboEndosos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEndosos.ForeColor = System.Drawing.Color.Black
        Me.cboEndosos.FormattingEnabled = True
        Me.cboEndosos.KeyEnter = True
        Me.cboEndosos.Location = New System.Drawing.Point(140, 13)
        Me.cboEndosos.Name = "cboEndosos"
        Me.cboEndosos.Size = New System.Drawing.Size(156, 21)
        Me.cboEndosos.TabIndex = 16
        '
        'txtAseguradora
        '
        Me.txtAseguradora.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAseguradora.BackColor = System.Drawing.Color.Ivory
        Me.txtAseguradora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAseguradora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAseguradora.ForeColor = System.Drawing.Color.Black
        Me.txtAseguradora.KeyEnter = True
        Me.txtAseguradora.Location = New System.Drawing.Point(76, 16)
        Me.txtAseguradora.Name = "txtAseguradora"
        Me.txtAseguradora.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAseguradora.ReadOnly = True
        Me.txtAseguradora.ShortcutsEnabled = False
        Me.txtAseguradora.Size = New System.Drawing.Size(171, 20)
        Me.txtAseguradora.TabIndex = 14
        Me.txtAseguradora.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(55, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Asegurad."
        '
        'BeButton2
        '
        Me.BeButton2.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BeButton2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton2.Location = New System.Drawing.Point(559, 39)
        Me.BeButton2.Name = "BeButton2"
        Me.BeButton2.Size = New System.Drawing.Size(69, 23)
        Me.BeButton2.TabIndex = 12
        Me.BeButton2.Text = "Ver &Detalle"
        Me.BeButton2.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton2.UseVisualStyleBackColor = True
        '
        'txtTotal
        '
        Me.txtTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotal.BackColor = System.Drawing.Color.Ivory
        Me.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.ForeColor = System.Drawing.Color.Black
        Me.txtTotal.KeyEnter = True
        Me.txtTotal.Location = New System.Drawing.Point(492, 92)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.ShortcutsEnabled = False
        Me.txtTotal.Size = New System.Drawing.Size(136, 20)
        Me.txtTotal.TabIndex = 6
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(452, 97)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(31, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Total"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotal.BackColor = System.Drawing.Color.Ivory
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotal.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotal.KeyEnter = True
        Me.txtSubTotal.Location = New System.Drawing.Point(364, 67)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotal.ReadOnly = True
        Me.txtSubTotal.ShortcutsEnabled = False
        Me.txtSubTotal.Size = New System.Drawing.Size(119, 20)
        Me.txtSubTotal.TabIndex = 4
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(308, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "SubTotal"
        '
        'txtInteres
        '
        Me.txtInteres.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtInteres.BackColor = System.Drawing.Color.Ivory
        Me.txtInteres.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtInteres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtInteres.ForeColor = System.Drawing.Color.Black
        Me.txtInteres.KeyEnter = True
        Me.txtInteres.Location = New System.Drawing.Point(560, 67)
        Me.txtInteres.Name = "txtInteres"
        Me.txtInteres.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtInteres.ReadOnly = True
        Me.txtInteres.ShortcutsEnabled = False
        Me.txtInteres.Size = New System.Drawing.Size(68, 20)
        Me.txtInteres.TabIndex = 5
        Me.txtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInteres.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(489, 70)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Interes Finan."
        '
        'txtMoneda
        '
        Me.txtMoneda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtMoneda.BackColor = System.Drawing.Color.Ivory
        Me.txtMoneda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtMoneda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMoneda.ForeColor = System.Drawing.Color.Black
        Me.txtMoneda.KeyEnter = True
        Me.txtMoneda.Location = New System.Drawing.Point(76, 92)
        Me.txtMoneda.Name = "txtMoneda"
        Me.txtMoneda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtMoneda.ReadOnly = True
        Me.txtMoneda.ShortcutsEnabled = False
        Me.txtMoneda.Size = New System.Drawing.Size(98, 20)
        Me.txtMoneda.TabIndex = 3
        Me.txtMoneda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 95)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Moneda"
        '
        'txtVigencia
        '
        Me.txtVigencia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtVigencia.BackColor = System.Drawing.Color.Ivory
        Me.txtVigencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtVigencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVigencia.ForeColor = System.Drawing.Color.Black
        Me.txtVigencia.KeyEnter = True
        Me.txtVigencia.Location = New System.Drawing.Point(76, 67)
        Me.txtVigencia.Name = "txtVigencia"
        Me.txtVigencia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtVigencia.ReadOnly = True
        Me.txtVigencia.ShortcutsEnabled = False
        Me.txtVigencia.Size = New System.Drawing.Size(171, 20)
        Me.txtVigencia.TabIndex = 2
        Me.txtVigencia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtDesRamo
        '
        Me.txtDesRamo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDesRamo.BackColor = System.Drawing.Color.Ivory
        Me.txtDesRamo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDesRamo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDesRamo.ForeColor = System.Drawing.Color.Black
        Me.txtDesRamo.KeyEnter = True
        Me.txtDesRamo.Location = New System.Drawing.Point(180, 42)
        Me.txtDesRamo.Name = "txtDesRamo"
        Me.txtDesRamo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDesRamo.ReadOnly = True
        Me.txtDesRamo.ShortcutsEnabled = False
        Me.txtDesRamo.Size = New System.Drawing.Size(303, 20)
        Me.txtDesRamo.TabIndex = 1
        Me.txtDesRamo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtPoliza
        '
        Me.txtPoliza.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPoliza.BackColor = System.Drawing.Color.Ivory
        Me.txtPoliza.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPoliza.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPoliza.ForeColor = System.Drawing.Color.Black
        Me.txtPoliza.KeyEnter = True
        Me.txtPoliza.Location = New System.Drawing.Point(76, 42)
        Me.txtPoliza.Name = "txtPoliza"
        Me.txtPoliza.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPoliza.ReadOnly = True
        Me.txtPoliza.ShortcutsEnabled = False
        Me.txtPoliza.Size = New System.Drawing.Size(98, 20)
        Me.txtPoliza.TabIndex = 0
        Me.txtPoliza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPoliza.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Vigencia"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nro Poliza"
        '
        'BeButton1
        '
        Me.BeButton1.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BeButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton1.Location = New System.Drawing.Point(571, 377)
        Me.BeButton1.Name = "BeButton1"
        Me.BeButton1.Size = New System.Drawing.Size(75, 23)
        Me.BeButton1.TabIndex = 2
        Me.BeButton1.Text = "BeButton1"
        Me.BeButton1.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton1.UseVisualStyleBackColor = True
        '
        'dgvCronograma
        '
        Me.dgvCronograma.AllowUserToAddRows = False
        Me.dgvCronograma.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvCronograma.BackgroundColor = System.Drawing.Color.White
        Me.dgvCronograma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCronograma.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Cuota, Me.FechaVencimiento, Me.Importe, Me.Estado, Me.Fecha_Cancelacion, Me.Boton, Me.BotonCancela})
        Me.dgvCronograma.Location = New System.Drawing.Point(12, 146)
        Me.dgvCronograma.Name = "dgvCronograma"
        Me.dgvCronograma.RowHeadersVisible = False
        Me.dgvCronograma.Size = New System.Drawing.Size(724, 264)
        Me.dgvCronograma.TabIndex = 0
        '
        'Cuota
        '
        Me.Cuota.DataPropertyName = "Cuota"
        Me.Cuota.HeaderText = "Cuota"
        Me.Cuota.Name = "Cuota"
        Me.Cuota.ReadOnly = True
        '
        'FechaVencimiento
        '
        Me.FechaVencimiento.DataPropertyName = "FechaVencimiento"
        Me.FechaVencimiento.HeaderText = "FechaVencimiento"
        Me.FechaVencimiento.Name = "FechaVencimiento"
        Me.FechaVencimiento.ReadOnly = True
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "Importe"
        Me.Importe.HeaderText = "Importe"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        Me.Estado.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Estado.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Fecha_Cancelacion
        '
        Me.Fecha_Cancelacion.DataPropertyName = "Fecha_Cancelacion"
        Me.Fecha_Cancelacion.HeaderText = "Fecha_Cancelacion"
        Me.Fecha_Cancelacion.Name = "Fecha_Cancelacion"
        Me.Fecha_Cancelacion.ReadOnly = True
        '
        'Boton
        '
        Me.Boton.HeaderText = ""
        Me.Boton.Name = "Boton"
        Me.Boton.Text = "Pendiente"
        Me.Boton.UseColumnTextForButtonValue = True
        '
        'BotonCancela
        '
        Me.BotonCancela.HeaderText = ""
        Me.BotonCancela.Name = "BotonCancela"
        Me.BotonCancela.Text = "Cancelar"
        Me.BotonCancela.UseColumnTextForButtonValue = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(667, 12)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(69, 23)
        Me.btnImprimir.TabIndex = 16
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'frmCronogramaPoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.CancelButton = Me.BeButton1
        Me.ClientSize = New System.Drawing.Size(748, 423)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.dgvCronograma)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.BeButton1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmCronogramaPoliza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cronograma de Pagos por Poliza"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvCronograma, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtMoneda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtVigencia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDesRamo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPoliza As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSubTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtInteres As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents BeButton1 As ctrLibreria.Controles.BeButton
    Friend WithEvents dgvCronograma As System.Windows.Forms.DataGridView
    Friend WithEvents BeButton2 As ctrLibreria.Controles.BeButton
    Friend WithEvents txtAseguradora As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbEndosos As System.Windows.Forms.RadioButton
    Friend WithEvents rdbPoliza As System.Windows.Forms.RadioButton
    Friend WithEvents cboEndosos As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Cuota As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaVencimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha_Cancelacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Boton As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents BotonCancela As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnImprimir As ctrLibreria.Controles.BeButton
End Class
