Option Explicit On
Imports System.Xml
Imports MSXML2
Imports System.Math

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports Telerik.WinControls.Data
Imports Telerik.WinControls.UI.Export

Public Class frmRegistroRendicion

    Private eRendiciones As clsRendicion
    Dim odtRendicion As New DataTable
    Dim odtRedencionDetPago As New DataTable
    Dim odtRendicionDetDocAsociados As New DataTable
    Dim odtRetencionDetPorAsociar As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared idRegistroDocumentoCab As Decimal
    Dim strAccion As String
    Dim decImporteRetenido As Decimal
    Dim decImporteRetenidoTotal As Decimal
    Private importeRendido As Decimal
    Private indicador As Boolean = False
    Private estado As Integer
    Private Loaded As Boolean = False

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptRetencion As New rptRetenciones

    Private cCapaDatos As clsCapaDatos

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Loaded = True
        Me.Width = 1000
        Me.Height = 660

        eRendiciones = New clsRendicion
        cCapaDatos = New clsCapaDatos

        cboAnio.Text = Convert.ToString(DateTime.Now.Year)
        cboMes.SelectedIndex = DateTime.Now.Month - 1

        tabMantenimiento.SelectedTab = tabLista
        botonesMantenimiento(mnuMantenimiento, True)

        InitCombos()

        CargarRendicionCab()
        CargarRendicionDetPagos()
    End Sub

    Private Sub tabMantenimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabMantenimiento.SelectedIndexChanged
        If tabMantenimiento.SelectedIndex = 0 Then
            Me.Width = 1000
            Me.Height = 650
            strAccion = Accion.Grabar
            'grpFacturas.Visible = False
            botonesMantenimiento(mnuMantenimiento, True)
        Else
            Me.Width = 925
            Me.Height = 753
            strAccion = Accion.Modifica
            'cboDocumentoOrigen.Enabled = False
            ''grpFacturas.Visible = False
            'btnBuscarAnexo.Enabled = False
            'dtFecha.Enabled = False
            ''txtRuc.ReadOnly = True
            botonesMantenimiento(mnuMantenimiento, False)
        End If
    End Sub

    Private Sub cboAnio_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboAnio.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarRendicionCab()
    End Sub

    Private Sub cboMes_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboMes.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarRendicionCab()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarRendicionCab()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        tabMantenimiento.SelectedTab = tabDetalle
        strAccion = Accion.Grabar

        cboDocumentoOrigen.Enabled = True
        cboEstado.SelectedIndex = 0
        btnBuscarAnexo.Enabled = True
        dtFecha.Enabled = True
        'txtRuc.ReadOnly = False
        odtRedencionDetPago.Clear()
        dtFecha.Value = Date.Today
        'txtNombre.Focus()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        tabMantenimiento.SelectedTab = tabLista
        CargarRendicionCab()
    End Sub

    Private Sub cboDocumentoOrigen_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDocumentoOrigen.SelectedValueChanged
        'odtRetencionDet.Clear()
        'gcRetencionDetalle.DataSource = Nothing
        'gcFacturas.DataSource = Nothing

        'If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then 'Factura de Compra
        '    gcRetencionDetalle.Columns("colImporteRetenido").ReadOnly = False
        'ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
        '    gcRetencionDetalle.Columns("colImporteRetenido").ReadOnly = True ' Orden de Compra
        'End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If gcRendicion.RowCount > 0 Then
            If Not Eliminar() Then Exit Sub
            CargarRendicionCab()
        Else
            MessageBox.Show("Seleccione registro a eliminar. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub gcRendicion_CurrentRowChanged(sender As Object, e As Telerik.WinControls.UI.CurrentRowChangedEventArgs) Handles gcRendicion.CurrentRowChanged
        Try
            idRegistroDocumentoCab = Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_RENDICION").Value)

            txtNroRendicion.Text = Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_RENDICION").Value)
            txtNroDocumento.Text = Convert.ToString(gcRendicion.CurrentRow.Cells("colNroDocumento").Value)
            dtFecha.Value = Convert.ToDateTime(gcRendicion.CurrentRow.Cells("colFecha").Value)
            txtTotal.Text = Convert.ToString(gcRendicion.CurrentRow.Cells("colTOTAL_DOC").Value)
            txtRendido.Text = Convert.ToString(gcRendicion.CurrentRow.Cells("colTOTAL_RENDIDO").Value)
            txtSaldoRendir.Text = Convert.ToString(gcRendicion.CurrentRow.Cells("colSALDO_RENDICION").Value)
            txtRuc.Text = Convert.ToString(gcRendicion.CurrentRow.Cells("colPrvRUC").Value)
            lblNombreAnexo.Text = Convert.ToString(gcRendicion.CurrentRow.Cells("colRazonSocial").Value)
            cboEstado.SelectedIndex = Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_DOCUMENTO").Value)
            cboDocumentoOrigen.SelectedValue = Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_DOCUMENTO").Value)

            CargarRendicionDetPagos()
            CargarRendicionDetDocPorAsociar()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvRetencion_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcRendicion.CellDoubleClick
        If gcRendicion.RowCount > 0 Then
            tabMantenimiento.SelectedTab = tabDetalle
        End If
    End Sub

    Private Sub btnBuscarAnexo_Click(sender As Object, e As EventArgs) Handles btnBuscarAnexo.Click
        Dim Documentos As New frmBuscaDocumento()
        If Documentos.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtRuc.Text = frmBuscaDocumento.ruc
            lblNombreAnexo.Text = frmBuscaDocumento.nombre
            cboMoneda.SelectedValue = frmBuscaDocumento.moneda
            txtNroDocumento.Text = frmBuscaDocumento.numeroDocumento

            'If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then
            '    odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
            '    gcFacturas.DataSource = odtRetencion
            'ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then
            '    odtRetencion = eRetenciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
            '    gcFacturas.DataSource = odtRetencion
            'End If
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        gcDocumentos.CloseEditor()
        If gcDocumentos.RowCount > 0 Then
            'Detalle
            For i As Integer = 0 To gcDocumentos.RowCount - 1
                If CBool(gcDocumentos.Rows(i).Cells("colChkEstado").Value) = True And Convert.ToDecimal(gcDocumentos.Rows(i).Cells("colAplicado").Value) <> 0 Then
                    Dim row As DataRow
                    row = odtRendicionDetDocAsociados.NewRow
                    row("ID_DOC_RENDIDO") = 0
                    row("ID_RENDICION_PAGOS") = 0
                    row("DOC_ORIGEN") = Convert.ToDecimal(gcDocumentos.Rows(i).Cells("colID_DOCUMENTO").Value)
                    row("ID_DOC_ORIGEN") = Convert.ToDecimal(gcDocumentos.Rows(i).Cells("colIdCorrelativo").Value)
                    row("TOTAL_DOCUMENTO") = Round(Convert.ToDecimal(gcDocumentos.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven)
                    row("IMPORTE_RENDIDO") = Round(Convert.ToDecimal(gcDocumentos.Rows(i).Cells("colAplicado").Value), 2, MidpointRounding.ToEven)
                    row("SALDO_A_RENDIR") = Round(Convert.ToDecimal(gcDocumentos.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) - Round(Convert.ToDecimal(gcDocumentos.Rows(i).Cells("colAplicado").Value), 2, MidpointRounding.ToEven)
                    row("FECHA_RENDICION") = Date.Today()

                    odtRendicionDetDocAsociados.Rows.Add(row)
                    'stigvDetFormulaCalculo.UpdateCurrentRow()
                    gcDocumentoAsocPago.DataSource = odtRendicionDetDocAsociados
                    'Me.DialogResult = Windows.Forms.DialogResult.OK
                    ''Me.Close()
                    'gcDocumentos.Rows.Remove(gcDocumentos.CurrentRow)

                    indicador = True
                End If
            Next i

            SumarImporte()
            Modificar()
            CargarRendicionDetPagos()
            'CargarRendicionDetDocAsociados()
            CargarRendicionDetDocPorAsociar()
            tabPagos.SelectedPage = tabPagosAsociados
        End If
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If gcDocumentoAsocPago.RowCount > 0 Then
            If Not Eliminar() Then Exit Sub

            'Dim intFila As Integer = gvTipoGastoDestino.CurrentRow.Index
            'odtTipDestinoGasto.Rows(intFila)("IDTIPOGASTODESTINO") = -1
            gcDocumentoAsocPago.Rows.Remove(gcDocumentoAsocPago.CurrentRow)
            odtRendicionDetDocAsociados.AcceptChanges()

            SumarImporte()

            Dim iResultado As Int32
            Dim idRendicionCab As Decimal = Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_RENDICION").Value)

            ' Actualiza el pago rendido
            iResultado = eRendiciones.fGrabar(Convert.ToDecimal(gcDetPagos.CurrentRow.Cells("colID_RENDICION_PAGOS").Value), decImporteRetenido, Convert.ToDecimal(gcDetPagos.CurrentRow.Cells("colTOTAL_PAGO").Value) - decImporteRetenido, 2)

            ' Actualiza el pago total rendido
            iResultado = eRendiciones.fGrabar(idRendicionCab, decImporteRetenidoTotal, Convert.ToDecimal(txtTotal.Text.Trim) - decImporteRetenidoTotal, 9)

            CargarRendicionDetPagos()
            'CargarRendicionDetDocAsociados()
            CargarRendicionDetDocPorAsociar()
        End If
    End Sub

    Private Sub btnRegistrarDocumento_Click(sender As Object, e As EventArgs) Handles btnRegistrarDocumento.Click
        If MessageBox.Show("�Desea registrar el documento.? ", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
            Dim x As frmRegistroDocumento = frmRegistroDocumento.Instance
            x.CControl = frmPrincipal.Barra
            frmPrincipal.Barra.Enabled = True

            x.Nuevo = True
            x.Modificar = True
            x.Visualizar = True
            x.Eliminar = True
            x.Grabar = False
            x.Cancelar = False
            x.Salir = True
            x.Buscar = True
            x.Exportar = True
            x.Imprimir = True
            x.MdiParent = frmPrincipal
            x.TabControl1.DisablePage(x.TabControl1.TabPages(1))
            x.Show()
        End If
    End Sub

    Private Sub btnRefrescarDocumento_Click(sender As Object, e As EventArgs) Handles btnRefrescarDocumento.Click
        CargarRendicionDetDocPorAsociar()
    End Sub

    Private Sub btnExportar_Click(sender As Object, e As EventArgs) Handles btnExportar.Click
        If gcRendicion.RowCount > 0 Then
            Dim export As ExportToExcelML = New ExportToExcelML(Me.gcRendicion)

            export.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport
            export.ExportVisualSettings = True
            export.SheetName = "Rendiciones"
            export.SummariesExportOption = SummariesOption.DoNotExport

            Dim save As SaveFileDialog = New SaveFileDialog()
            save.Filter = "Excel|*.xls"
            save.Title = "Save como archivo de excel"
            save.ShowDialog()
            export.FileExtension = "xls"
            export.RunExport(save.FileName)
            'export.RunExport(save.FileName + ".xls")
        End If
    End Sub

    Private Sub chkPendienteRendir_ToggleStateChanged(sender As Object, args As Telerik.WinControls.UI.StateChangedEventArgs) Handles chkPendienteRendir.ToggleStateChanged
        If Not Loaded Then Exit Sub
        If chkPendienteRendir.Checked = True Then
            estado = 0
            CargarRendicionCab()
        Else
            estado = 1
            CargarRendicionCab()
        End If
    End Sub

#End Region

#Region " Documentos Asociados al pago  "

    Private Sub gcDetPagos_CurrentRowChanged(sender As Object, e As Telerik.WinControls.UI.CurrentRowChangedEventArgs) Handles gcDetPagos.CurrentRowChanged
        CargarRendicionDetDocAsociados()
    End Sub

#End Region

#Region " Metodos Privados "

    Private Sub InitCombos()
        Try
            Dim odtDocumento As New DataTable
            odtDocumento = eRendiciones.fCargarDocumentos(7)
            cboDocumentoOrigen.DataSource = odtDocumento
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarRendicionCab()
        Try
            'Dim odtData As New DataTable 
            Dim VL_ANIO = Convert.ToString(cboAnio.Text)
            Dim VL_MES = Convert.ToString(cboMes.SelectedIndex + 1)

            odtRendicion = eRendiciones.fCargarRendicion(gEmpresa, estado, VL_ANIO, VL_MES)
            gcRendicion.DataSource = odtRendicion
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarRendicionDetPagos()
        Try
            'Dim odtData As New DataTable
            Dim idRendicionCab As Decimal = If(gcRendicion.RowCount > 0, Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_RENDICION").Value), 0)

            odtRedencionDetPago = eRendiciones.fCargarRendicionDetallePagos(idRendicionCab)
            gcDetPagos.DataSource = odtRedencionDetPago
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarRendicionDetDocAsociados()
        Try
            'Dim odtData As New DataTable
            Dim idrendicionPago As Decimal = If(gcDetPagos.RowCount > 0, Convert.ToDecimal(gcDetPagos.CurrentRow.Cells("colID_RENDICION_PAGOS").Value), 0)

            odtRendicionDetDocAsociados = eRendiciones.fCargarRendicionDetalleDocAsociados(idrendicionPago)
            gcDocumentoAsocPago.DataSource = odtRendicionDetDocAsociados
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarRendicionDetDocPorAsociar()
        Try
            If Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_DOCUMENTO").Value) = 1 Then ' Orden de compra
                'Dim odtData As New DataTable
                Dim idocoCodigo As String = Convert.ToString(gcRendicion.CurrentRow.Cells("colOcoCodigo").Value)
                Dim idpdoCodigo As String = Convert.ToString(gcRendicion.CurrentRow.Cells("colPdoCodigo").Value)

                odtRetencionDetPorAsociar = eRendiciones.fCargarRendicionDetalleDocPorAsociar(idocoCodigo, idpdoCodigo, gEmpresa, 6)
                gcDocumentos.DataSource = odtRetencionDetPorAsociar
            ElseIf Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_DOCUMENTO").Value) = 2 Then ' Entrega a rendir
                odtRetencionDetPorAsociar = eRendiciones.fCargarRendicionDetalleDocPorAsociar(0, 0, gEmpresa, 10)
                gcDocumentos.DataSource = odtRetencionDetPorAsociar
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

#Region " M�todos de BD "

    Private Sub SumarImporte()
        Try
            decImporteRetenido = 0
            decImporteRetenidoTotal = 0

            For i As Integer = 0 To gcDocumentoAsocPago.RowCount - 1
                decImporteRetenido += Round(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colIMPORTE_RENDIDO").Value), 2, MidpointRounding.ToEven)
            Next
            gcDetPagos.CurrentRow.Cells("colTOTAL_RENDIDO").Value = decImporteRetenido

            For i As Integer = 0 To gcDetPagos.RowCount - 1
                decImporteRetenidoTotal += Round(Convert.ToDecimal(gcDetPagos.Rows(i).Cells("colTOTAL_RENDIDO").Value), 2, MidpointRounding.ToEven)
            Next

            txtRendido.Text = decImporteRetenidoTotal
            txtSaldoRendir.Text = Convert.ToDecimal(txtTotal.Text.Trim) - Convert.ToDecimal(txtRendido.Text.Trim)
        Catch ex As Exception
            MessageBox.Show("Error al sumar el importe. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            If indicador = True Then
                Dim iResultado As Int32
                Dim idRendicionCab As Decimal = Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colID_RENDICION").Value)
                Dim idRendicionPagoCab As Decimal = Convert.ToDecimal(gcDetPagos.CurrentRow.Cells("colID_RENDICION_PAGOS").Value)

                ' Actualiza el pago total rendido
                iResultado = eRendiciones.fGrabar(idRendicionCab, decImporteRetenidoTotal, Convert.ToDecimal(txtTotal.Text.Trim) - decImporteRetenidoTotal, 9)

                ' Actualiza el pago rendido
                iResultado = eRendiciones.fGrabar(Convert.ToDecimal(gcDetPagos.CurrentRow.Cells("colID_RENDICION_PAGOS").Value), decImporteRetenido, Convert.ToDecimal(gcDetPagos.CurrentRow.Cells("colTOTAL_PAGO").Value) - decImporteRetenido, 2)

                For i As Integer = 0 To gcDocumentoAsocPago.RowCount - 1
                    If Convert.ToInt32(gcDocumentoAsocPago.Rows(i).Cells("colID_DOC_RENDIDO").Value) = 0 Then
                        iResultado = eRendiciones.fGrabarDet(0, idRendicionPagoCab, Convert.ToDecimal(gcDocumentoAsocPago.CurrentRow.Cells("colDOC_ORIGEN").Value), Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colID_DOC_ORIGEN").Value), Round(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colTOTAL_DOCUMENTO").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colIMPORTE_RENDIDO").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colSALDO_A_RENDIR").Value), 2, MidpointRounding.ToEven), 1)
                        'Else
                        '    iResultado = eRendiciones.fGrabarDet(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colID_DOC_RENDIDO").Value), idRendicionCab, 1, Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colID_DOC_ORIGEN").Value), Round(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colTOTAL_DOCUMENTO").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colIMPORTE_RENDIDO").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDocumentoAsocPago.Rows(i).Cells("colSALDO_A_RENDIR").Value), 2, MidpointRounding.ToEven), 2)
                    End If
                Next
                indicador = False
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo modificar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function Eliminar() As Boolean
        Try
            If MessageBox.Show("�Est� seguro que desea eliminar este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then

                If Not ComprobarDato() Then Exit Function

                Dim iResultado As Int32
                iResultado = eRendiciones.fEliminar(Convert.ToDecimal(gcDocumentoAsocPago.CurrentRow.Cells("colID_DOC_RENDIDO").Value), 3)

                Return True
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo eliminar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Function ComprobarDato() As Boolean
        'Dim iResultado As Int32 = 0
        'Dim decId As Decimal = Convert.ToDecimal(gcRendicion.CurrentRow.Cells("colIdRetencionCab").Value)

        'With cCapaDatos
        '    Try
        '        .sConectarSQL()
        '        .sComandoSQL("SELECT 1 FROM Tesoreria.RetencionesCab RC INNER JOIN Tesoreria.RetencionesDET RD ON RC.IDRETENCIONCAB = RD.IDRETENCIONCAB AND RD.ESTADO = '1' " & _
        '                     "INNER JOIN Tesoreria.DOC_PENDIENTES DP ON RD.NROITEM = DP.ID_DOCORIGEN WHERE RC.IDRETENCIONCAB = " & decId & " AND DP.SALDO <> TOTALSINRETDET AND RC.ESTADO = '1'", CommandType.Text)

        '        iResultado = .fCmdExecuteScalar()
        '        If iResultado >= "1" Then
        '            MessageBox.Show("No se puede modificar/eliminar esta orden de compra porque tiene pagos realizados. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '            Return False
        '        Else
        Return True
        '        End If
        '    Catch ex As Exception
        '        MessageBox.Show("No se pudo comprobar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    End Try
        '    .sDesconectarSQL()
        'End With
    End Function

#End Region

End Class