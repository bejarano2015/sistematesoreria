<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmValorizacionesPagosVarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.CCosto = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel
        Me.cboContratista = New ctrLibreria.Controles.BeComboBox
        Me.cboPartidas = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.cboObra = New ctrLibreria.Controles.BeComboBox
        Me.rdbT1 = New System.Windows.Forms.RadioButton
        Me.rdbT2 = New System.Windows.Forms.RadioButton
        Me.cboAnexo = New ctrLibreria.Controles.BeComboBox
        Me.dgvDetalle = New System.Windows.Forms.DataGridView
        Me.FechaEnvio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DescripcionVal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TotalVal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdValorizacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdTipoPago = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Tipo1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtTotPagar = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnEliminar = New ctrLibreria.Controles.BeButton
        Me.btnNuevo = New ctrLibreria.Controles.BeButton
        Me.btnGrabar = New ctrLibreria.Controles.BeButton
        Me.Fecha = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.txtDescripcion = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.txtCodigoVal = New ctrLibreria.Controles.BeTextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblRetenido = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.CCosto)
        Me.GroupBox1.Controls.Add(Me.BeLabel6)
        Me.GroupBox1.Controls.Add(Me.BeLabel25)
        Me.GroupBox1.Controls.Add(Me.cboContratista)
        Me.GroupBox1.Controls.Add(Me.cboPartidas)
        Me.GroupBox1.Controls.Add(Me.BeLabel5)
        Me.GroupBox1.Controls.Add(Me.BeLabel13)
        Me.GroupBox1.Controls.Add(Me.cboObra)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(585, 117)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Partida"
        '
        'CCosto
        '
        Me.CCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.CCosto.BackColor = System.Drawing.Color.Ivory
        Me.CCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CCosto.ForeColor = System.Drawing.Color.Black
        Me.CCosto.FormattingEnabled = True
        Me.CCosto.KeyEnter = True
        Me.CCosto.Location = New System.Drawing.Point(75, 64)
        Me.CCosto.Name = "CCosto"
        Me.CCosto.Size = New System.Drawing.Size(500, 21)
        Me.CCosto.TabIndex = 7
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(10, 72)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(44, 13)
        Me.BeLabel6.TabIndex = 6
        Me.BeLabel6.Text = "C Costo"
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.ForeColor = System.Drawing.Color.Black
        Me.BeLabel25.Location = New System.Drawing.Point(10, 47)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel25.TabIndex = 2
        Me.BeLabel25.Text = "Contratista"
        '
        'cboContratista
        '
        Me.cboContratista.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboContratista.BackColor = System.Drawing.Color.Ivory
        Me.cboContratista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContratista.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboContratista.ForeColor = System.Drawing.Color.Black
        Me.cboContratista.FormattingEnabled = True
        Me.cboContratista.KeyEnter = True
        Me.cboContratista.Location = New System.Drawing.Point(75, 39)
        Me.cboContratista.Name = "cboContratista"
        Me.cboContratista.Size = New System.Drawing.Size(500, 21)
        Me.cboContratista.TabIndex = 3
        '
        'cboPartidas
        '
        Me.cboPartidas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPartidas.BackColor = System.Drawing.Color.Ivory
        Me.cboPartidas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartidas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPartidas.ForeColor = System.Drawing.Color.Black
        Me.cboPartidas.FormattingEnabled = True
        Me.cboPartidas.KeyEnter = True
        Me.cboPartidas.Location = New System.Drawing.Point(75, 89)
        Me.cboPartidas.Name = "cboPartidas"
        Me.cboPartidas.Size = New System.Drawing.Size(500, 21)
        Me.cboPartidas.TabIndex = 5
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(10, 94)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(47, 13)
        Me.BeLabel5.TabIndex = 4
        Me.BeLabel5.Text = "Contrato"
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(10, 23)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(30, 13)
        Me.BeLabel13.TabIndex = 0
        Me.BeLabel13.Text = "Obra"
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(75, 15)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(500, 21)
        Me.cboObra.TabIndex = 1
        '
        'rdbT1
        '
        Me.rdbT1.AutoSize = True
        Me.rdbT1.Location = New System.Drawing.Point(77, 102)
        Me.rdbT1.Name = "rdbT1"
        Me.rdbT1.Size = New System.Drawing.Size(83, 17)
        Me.rdbT1.TabIndex = 7
        Me.rdbT1.TabStop = True
        Me.rdbT1.Text = "Otros Pagos"
        Me.rdbT1.UseVisualStyleBackColor = True
        '
        'rdbT2
        '
        Me.rdbT2.AutoSize = True
        Me.rdbT2.Location = New System.Drawing.Point(166, 102)
        Me.rdbT2.Name = "rdbT2"
        Me.rdbT2.Size = New System.Drawing.Size(62, 17)
        Me.rdbT2.TabIndex = 8
        Me.rdbT2.TabStop = True
        Me.rdbT2.Text = "Adenda"
        Me.rdbT2.UseVisualStyleBackColor = True
        '
        'cboAnexo
        '
        Me.cboAnexo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAnexo.BackColor = System.Drawing.Color.Ivory
        Me.cboAnexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnexo.ForeColor = System.Drawing.Color.Black
        Me.cboAnexo.FormattingEnabled = True
        Me.cboAnexo.KeyEnter = True
        Me.cboAnexo.Location = New System.Drawing.Point(234, 100)
        Me.cboAnexo.Name = "cboAnexo"
        Me.cboAnexo.Size = New System.Drawing.Size(192, 21)
        Me.cboAnexo.TabIndex = 9
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FechaEnvio, Me.Descripcion, Me.DescripcionVal, Me.TotalVal, Me.IdValorizacion, Me.IdTipoPago, Me.Tipo1})
        Me.dgvDetalle.Location = New System.Drawing.Point(12, 285)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(575, 133)
        Me.dgvDetalle.TabIndex = 2
        '
        'FechaEnvio
        '
        Me.FechaEnvio.DataPropertyName = "FechaEnvio"
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaEnvio.DefaultCellStyle = DataGridViewCellStyle1
        Me.FechaEnvio.HeaderText = "Fecha"
        Me.FechaEnvio.Name = "FechaEnvio"
        Me.FechaEnvio.Width = 70
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle2
        Me.Descripcion.HeaderText = "Tipo"
        Me.Descripcion.Name = "Descripcion"
        '
        'DescripcionVal
        '
        Me.DescripcionVal.DataPropertyName = "DescripcionVal"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionVal.DefaultCellStyle = DataGridViewCellStyle3
        Me.DescripcionVal.HeaderText = "Concepto"
        Me.DescripcionVal.Name = "DescripcionVal"
        Me.DescripcionVal.Width = 300
        '
        'TotalVal
        '
        Me.TotalVal.DataPropertyName = "TotalVal"
        DataGridViewCellStyle4.Format = "N2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.TotalVal.DefaultCellStyle = DataGridViewCellStyle4
        Me.TotalVal.HeaderText = "Monto"
        Me.TotalVal.Name = "TotalVal"
        Me.TotalVal.Width = 80
        '
        'IdValorizacion
        '
        Me.IdValorizacion.DataPropertyName = "IdValorizacion"
        Me.IdValorizacion.HeaderText = "IdValorizacion"
        Me.IdValorizacion.Name = "IdValorizacion"
        Me.IdValorizacion.Visible = False
        '
        'IdTipoPago
        '
        Me.IdTipoPago.DataPropertyName = "IdTipoPago"
        Me.IdTipoPago.HeaderText = "IdTipoPago"
        Me.IdTipoPago.Name = "IdTipoPago"
        Me.IdTipoPago.Visible = False
        '
        'Tipo1
        '
        Me.Tipo1.DataPropertyName = "Tipo1"
        Me.Tipo1.HeaderText = "Tipo2"
        Me.Tipo1.Name = "Tipo1"
        Me.Tipo1.Visible = False
        '
        'txtTotPagar
        '
        Me.txtTotPagar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotPagar.BackColor = System.Drawing.Color.Ivory
        Me.txtTotPagar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotPagar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotPagar.ForeColor = System.Drawing.Color.Black
        Me.txtTotPagar.KeyEnter = True
        Me.txtTotPagar.Location = New System.Drawing.Point(475, 100)
        Me.txtTotPagar.Name = "txtTotPagar"
        Me.txtTotPagar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotPagar.ShortcutsEnabled = False
        Me.txtTotPagar.Size = New System.Drawing.Size(100, 20)
        Me.txtTotPagar.TabIndex = 11
        Me.txtTotPagar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(432, 105)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(37, 13)
        Me.BeLabel1.TabIndex = 10
        Me.BeLabel1.Text = "Monto"
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(77, 46)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaIni.TabIndex = 3
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnEliminar)
        Me.GroupBox3.Controls.Add(Me.btnNuevo)
        Me.GroupBox3.Controls.Add(Me.btnGrabar)
        Me.GroupBox3.Location = New System.Drawing.Point(456, 422)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(131, 50)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'btnEliminar
        '
        Me.btnEliminar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(89, 14)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(35, 30)
        Me.btnEliminar.TabIndex = 2
        Me.btnEliminar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnNuevo.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__File_new_
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(7, 14)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(35, 30)
        Me.btnNuevo.TabIndex = 0
        Me.btnNuevo.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'btnGrabar
        '
        Me.btnGrabar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(48, 14)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(35, 30)
        Me.btnGrabar.TabIndex = 1
        Me.btnGrabar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'Fecha
        '
        Me.Fecha.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.Fecha.AutoSize = True
        Me.Fecha.BackColor = System.Drawing.Color.Transparent
        Me.Fecha.ForeColor = System.Drawing.Color.Black
        Me.Fecha.Location = New System.Drawing.Point(14, 52)
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Size = New System.Drawing.Size(37, 13)
        Me.Fecha.TabIndex = 2
        Me.Fecha.Text = "Fecha"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(13, 103)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(61, 13)
        Me.BeLabel3.TabIndex = 6
        Me.BeLabel3.Text = "Tipo Anexo"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(14, 75)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(53, 13)
        Me.BeLabel2.TabIndex = 4
        Me.BeLabel2.Text = "Concepto"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDescripcion.BackColor = System.Drawing.Color.Ivory
        Me.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescripcion.ForeColor = System.Drawing.Color.Black
        Me.txtDescripcion.KeyEnter = True
        Me.txtDescripcion.Location = New System.Drawing.Point(77, 75)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDescripcion.ShortcutsEnabled = False
        Me.txtDescripcion.Size = New System.Drawing.Size(349, 20)
        Me.txtDescripcion.TabIndex = 5
        Me.txtDescripcion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(14, 26)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel4.TabIndex = 0
        Me.BeLabel4.Text = "Codigo"
        '
        'txtCodigoVal
        '
        Me.txtCodigoVal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigoVal.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtCodigoVal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCodigoVal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoVal.Enabled = False
        Me.txtCodigoVal.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoVal.KeyEnter = True
        Me.txtCodigoVal.Location = New System.Drawing.Point(77, 20)
        Me.txtCodigoVal.Name = "txtCodigoVal"
        Me.txtCodigoVal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigoVal.ShortcutsEnabled = False
        Me.txtCodigoVal.Size = New System.Drawing.Size(211, 20)
        Me.txtCodigoVal.TabIndex = 1
        Me.txtCodigoVal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblRetenido)
        Me.GroupBox2.Controls.Add(Me.Label25)
        Me.GroupBox2.Controls.Add(Me.txtCodigoVal)
        Me.GroupBox2.Controls.Add(Me.BeLabel2)
        Me.GroupBox2.Controls.Add(Me.BeLabel4)
        Me.GroupBox2.Controls.Add(Me.txtDescripcion)
        Me.GroupBox2.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox2.Controls.Add(Me.BeLabel3)
        Me.GroupBox2.Controls.Add(Me.BeLabel1)
        Me.GroupBox2.Controls.Add(Me.Fecha)
        Me.GroupBox2.Controls.Add(Me.txtTotPagar)
        Me.GroupBox2.Controls.Add(Me.rdbT1)
        Me.GroupBox2.Controls.Add(Me.rdbT2)
        Me.GroupBox2.Controls.Add(Me.cboAnexo)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 135)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(585, 131)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Pagos Varios y Adendas"
        '
        'lblRetenido
        '
        Me.lblRetenido.AutoSize = True
        Me.lblRetenido.ForeColor = System.Drawing.Color.DarkRed
        Me.lblRetenido.Location = New System.Drawing.Point(475, 27)
        Me.lblRetenido.Name = "lblRetenido"
        Me.lblRetenido.Size = New System.Drawing.Size(0, 13)
        Me.lblRetenido.TabIndex = 31
        Me.lblRetenido.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label25.Location = New System.Drawing.Point(392, 27)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(77, 13)
        Me.Label25.TabIndex = 30
        Me.Label25.Text = "Total Retenido"
        '
        'frmValorizacionesPagosVarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(607, 483)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.dgvDetalle)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "frmValorizacionesPagosVarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Pagos Varios y Adendas"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboContratista As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboPartidas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents rdbT1 As System.Windows.Forms.RadioButton
    Friend WithEvents rdbT2 As System.Windows.Forms.RadioButton
    Friend WithEvents cboAnexo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents txtTotPagar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnEliminar As ctrLibreria.Controles.BeButton
    Friend WithEvents btnNuevo As ctrLibreria.Controles.BeButton
    Friend WithEvents btnGrabar As ctrLibreria.Controles.BeButton
    Friend WithEvents Fecha As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtDescripcion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCodigoVal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents FechaEnvio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionVal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalVal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdValorizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdTipoPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblRetenido As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents CCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
End Class
