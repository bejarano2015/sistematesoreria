Imports CapaNegocios

Public Class frmConsultaAsegurados_x_Obra2

    Private cSegurosPolizas As clsSegurosPolizas
    Private eSesionCajas As clsSesionCajas
    Private eFunciones As Funciones
    Dim DTobras As DataTable
    Public CodCC As String = ""
#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaAsegurados_x_Obra2 = Nothing
    Public Shared Function Instance() As frmConsultaAsegurados_x_Obra2
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaAsegurados_x_Obra2
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmConsultaAsegurados_x_Obra2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmConsultaAsegurados_x_Obra2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'cSegurosPolizas = New clsSegurosPolizas
        'DTobras = New DataTable
        'DTobras = cSegurosPolizas.fCargar_Obras2(23, gEmpresa)
        'cboObra.DataSource = DTobras
        'cboObra.DisplayMember = "Descripcion"
        'cboObra.ValueMember = "Codigo"
        'cboObra.SelectedIndex = -1
        'cboObra.Focus()
        lblTotalRegistro.Text = ""
        txtCodCC.Focus()
        txtCliente_TextChanged(sender, e)
    End Sub

    'Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    lblTotalRegistro.Text = "Total Registros : 0"
    '    Dim IdObra As String = ""
    '    If (cboObra.SelectedIndex > -1) Then
    '        Try
    '            IdObra = cboObra.SelectedValue.ToString
    '            If IdObra <> "System.Data.DataRowView" Then
    '                cSegurosPolizas = New clsSegurosPolizas
    '                dgvAsegurados.DataSource = cSegurosPolizas.fCargar_AseguradosXObra2(24, gEmpresa, IdObra, "")
    '                lblTotalRegistro.Text = "Total Registros : " & dgvAsegurados.Rows.Count
    '            End If
    '        Catch ex As System.IndexOutOfRangeException
    '            Exit Sub
    '        End Try
    '    Else
    '        'cboObra.DataSource = Nothing
    '    End If
    'End Sub

    Private Sub txtCliente_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCliente.TextChanged
        lblTotalRegistro.Text = "Total Registros : 0"
        'cboObra.SelectedIndex = -1
        cSegurosPolizas = New clsSegurosPolizas
        If Len(Trim(txtCliente.Text)) > 0 And Len(Trim(txtDesCC.Text)) = 0 Then
            'cSegurosPolizas = New clsSegurosPolizas
            dgvAsegurados2.DataSource = cSegurosPolizas.fCargar_AseguradosXObra2(25, gEmpresa, "", Trim(txtCliente.Text))
        ElseIf Len(Trim(txtCliente.Text)) > 0 And Len(Trim(txtDesCC.Text)) > 0 Then
            'cSegurosPolizas = New clsSegurosPolizas
            dgvAsegurados2.DataSource = cSegurosPolizas.fCargar_AseguradosXObra2(27, gEmpresa, Trim(txtCodCC.Text), Trim(txtCliente.Text))
        ElseIf Len(Trim(txtCliente.Text)) = 0 And Len(Trim(txtDesCC.Text)) = 0 Then
            'cSegurosPolizas = New clsSegurosPolizas
            dgvAsegurados2.DataSource = cSegurosPolizas.fCargar_AseguradosXObra2(26, gEmpresa, "", Trim(txtCliente.Text))
        ElseIf Len(Trim(txtCliente.Text)) = 0 And Len(Trim(txtDesCC.Text)) > 0 Then
            'cSegurosPolizas = New clsSegurosPolizas
            dgvAsegurados2.DataSource = cSegurosPolizas.fCargar_AseguradosXObra2(28, gEmpresa, Trim(txtCodCC.Text), Trim(txtCliente.Text))
        End If
        lblTotalRegistro.Text = "Total Registros : " & dgvAsegurados2.Rows.Count
    End Sub

    Private Sub txtCodCC_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCodCC.KeyDown
        Select Case e.KeyCode
            Case Keys.F1
                Dim x As frmCCostosxEmpresa = frmCCostosxEmpresa.Instance
                x.Owner = Me
                x.NroForm = 1
                x.ShowInTaskbar = False
                x.ShowDialog()
                'Dim IdCuenta As String
                'Dim dtCodMoneda As DataTable
                'Dim sDescripcionMoneda As String
                txtCodCC.Text = Trim(CodCC)
                txtCodCC_TextChanged(sender, e)
        End Select
    End Sub

    Private Sub txtCodCC_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodCC.TextChanged
        If Len(Trim(txtCodCC.Text)) > 0 Then
            Try

                Try
                    eSesionCajas = New clsSesionCajas
                    Dim dtDatos As DataTable
                    dtDatos = New DataTable
                    dtDatos = eSesionCajas.fCargarCCostoForms(38, gEmpresa, Trim(txtCodCC.Text))
                    If eSesionCajas.iNroRegistros > 0 Then
                        If Microsoft.VisualBasic.IsDBNull(dtDatos.Rows(0).Item("CCosDescripcion")) = False Then
                            txtDesCC.Text = dtDatos.Rows(0).Item("CCosDescripcion")
                        End If
                    ElseIf eSesionCajas.iNroRegistros = 0 Then
                        txtDesCC.Clear()
                    End If
                Catch ex As Exception
                End Try
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            txtCodCC.Clear()
            txtDesCC.Clear()
        End If

        txtCliente_TextChanged(sender, e)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        'Me.dgDatos.DataSource = dt
        Dim fichero As String = ""


        'SaveFileDialog1.Filter = "Excel(*.xls)|*.xls|"

        SaveFileDialog1.InitialDirectory = "C:\"
        SaveFileDialog1.Title = "Guardar Archivo"
        SaveFileDialog1.Filter = "Excel (*.xls) |*.xls;*.rtf|(*.txt) |*.txt|(*.*) |*.*"
        'SaveFileDialog1.Filter = "Excel (*.xlsx) |*.xlsx;*.rtf|(*.txt) |*.txt|(*.*) |*.*"

        'SaveFileDialog1.InitialDirectory = "C:\Transito\fotos\" xlsx

        If SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            fichero = SaveFileDialog1.FileName
            Try
                'Dim iExp As New Funciones
                'Dim iExp As New Funciones
                eFunciones = New Funciones

                Dim dtBusquedaX As DataTable
                dtBusquedaX = New DataTable
                dtBusquedaX = Funciones.ToTable(dgvAsegurados2, "Jesus")
                'eFunciones.DataTableToExcel(CType(Me.dgvGastos.DataSource, DataTable))
                'eFunciones.DataTableToExcel(dtBusqueda, fichero)
                eFunciones.DataTableToExcel(dtBusquedaX, fichero)

                'Dim miDataTableModificado As DataTable
                'miDataTableModificado = New DataTable
                'miDataTableModificado = CType(dgvGastos.DataSource, DataTable)
                'eFunciones.DataTableToExcel(miDataTableModificado, fichero)


                dtBusquedaX.Dispose()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            'Else
        End If



        '' Set the content type to Excel
        'Response.ContentType = "application/vnd.ms-excel"

        ''Turn off the view state
        'Me.EnableViewState = False

        ''Remove the charset from the Content-Type header
        'Response.Charset = String.Empty

        'Dim myTextWriter As New System.IO.StringWriter()
        'Dim myHtmlTextWriter As New System.Web.UI.HtmlTextWriter(myTextWriter)

        ''Get the HTML for the control
        'myDataGrid.RenderControl(myHtmlTextWriter)

        ''Write the HTML to the browser
        'Response.Write(myTextWriter.ToString())

        ''End the response
        'Response.End()
    End Sub
End Class