Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports System.Data

Public Class frmContratistas
    Private eContratistasControl As clsContratistasControl
    Private eValorizaciones As clsValorizaciones
    Private ePagoProveedores As clsPagoProveedores
    Private ePartidas As clsPartidas

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmContratistas = Nothing
    Public Shared Function Instance() As frmContratistas
        If frmInstance Is Nothing Then
            frmInstance = New frmContratistas
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmContratistas_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmContratistas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eValorizaciones = New clsValorizaciones
        cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today(), "")
        If eValorizaciones.iNroRegistros > 0 Then
            cboObra.ValueMember = "ObraCodigo"
            cboObra.DisplayMember = "ObraDescripcion"
            cboObra.SelectedIndex = -1
        End If
        Dim dTipoCambio As Double
        Dim dtTable As DataTable
        dtTable = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTable = ePagoProveedores.fListarParametroTipoCambio(gEmpresa, Today())
        If dtTable.Rows.Count > 0 Then
            dTipoCambio = Val(dtTable.Rows(0).Item("TcaVenta"))
            lblTipoC.Text = Format(dTipoCambio, "0.00")
        Else
            dTipoCambio = 0.0
            lblTipoC.Text = Format(dTipoCambio, "0.00")
        End If
        lblPorRete.Text = "0.00"
        lblFechaIni.Text = ""
        lblFechaFin.Text = ""
        lblMoneda.Text = ""
        lblDoc.Text = ""
        lblPresu.Text = "0.00"
        lblAbonado.Text = "0.00"
        lblSaldo.Text = "0.00"
        lblPorAbo.Text = "0.00"
        lblFGaran.Text = "0.00"
        lblRetenido.Text = "0.00"
        lblPagoFG.Text = "0.00"
        lblTotFac.Text = "0.00"
        lblPorSaldo.Text = "0.00"
        lblOrden.Text = "0.00"
        lblContrato.Text = "0.00"
        txtPresupuesto.Text = ""
    End Sub

    Private Sub cboObra_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboObra.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            cboContratista.DataSource = eValorizaciones.fListarObras(1, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                cboContratista.ValueMember = "IdContratista"
                cboContratista.DisplayMember = "Nombre"
                cboContratista.SelectedIndex = -1

                CCosto.DataSource = Nothing
                CCosto.SelectedIndex = -1

                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
                txtDescripcionEsp.Clear()
            End If
        End If
        '''''''''''''''''''''''''''''''''
        Limpiar()
        'lblPorRete.Text = "0.00"
        'lblFechaIni.Text = ""
        'lblFechaFin.Text = ""
        'lblMoneda.Text = ""
        'lblDoc.Text = ""
        'lblPresu.Text = "0.00"
        'lblAbonado.Text = "0.00"
        'lblSaldo.Text = "0.00"
        'lblPorAbo.Text = "0.00"
        'lblFGaran.Text = "0.00"
        'lblRetenido.Text = "0.00"
        'lblPagoFG.Text = "0.00"
        'lblTotFac.Text = "0.00"
        'lblPorSaldo.Text = "0.00"
        'lblOrden.Text = ""
        'lblContrato.Text = ""
        'txtPresupuesto.Text = ""
        'txtRuc.Text = ""
        'txtDescripcionEsp.Text = ""
        'For x As Integer = 0 To dgvHistorial.RowCount - 1
        '    dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
        'Next

    End Sub

    Private Sub cboContratista_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboContratista.SelectionChangeCommitted

        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            CCosto.DataSource = eValorizaciones.fListarObras(76, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                CCosto.ValueMember = "CCosCodigo"
                CCosto.DisplayMember = "CCosDescripcion"
                CCosto.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
            End If
            'End If
        End If

        Limpiar()

        'If cboObra.SelectedIndex <> -1 Then
        '    eValorizaciones = New clsValorizaciones
        '    cboPartidas.DataSource = eValorizaciones.fListarObras(15, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today())
        '    If eValorizaciones.iNroRegistros > 0 Then
        '        cboPartidas.ValueMember = "IdPartida"
        '        cboPartidas.DisplayMember = "ParDescripcion"
        '        cboPartidas.SelectedIndex = -1
        '    End If
        '    eValorizaciones = New clsValorizaciones
        '    Dim dtEspecialidad As DataTable
        '    dtEspecialidad = New DataTable
        '    dtEspecialidad = eValorizaciones.fListarObras(20, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today())
        '    If dtEspecialidad.Rows.Count > 0 Then
        '        txtDescripcionEsp.Text = UCase(dtEspecialidad.Rows(0).Item("esp_nombre"))
        '        txtRuc.Text = UCase(dtEspecialidad.Rows(0).Item("ConRuc"))
        '        'txtRuc.Text = UCase(dtEspecialidad.Rows(0).Item("ConRuc"))
        '        'txtRuc.Text = UCase(dtEspecialidad.Rows(0).Item("ConRuc"))
        '    ElseIf dtEspecialidad.Rows.Count = 0 Then
        '        txtDescripcionEsp.Clear()
        '        txtRuc.Clear()
        '    End If
        'ElseIf cboObra.SelectedIndex = -1 Then
        '    cboPartidas.SelectedIndex = -1
        '    txtDescripcionEsp.Clear()
        'End If
        
    End Sub


    Sub Limpiar()
        lblPorRete.Text = "0.00"
        lblFechaIni.Text = ""
        lblFechaFin.Text = ""
        lblMoneda.Text = ""
        lblDoc.Text = ""
        lblPresu.Text = "0.00"
        lblAbonado.Text = "0.00"
        lblSaldo.Text = "0.00"
        lblPorAbo.Text = "0.00"
        lblFGaran.Text = "0.00"
        lblRetenido.Text = "0.00"
        lblPagoFG.Text = "0.00"
        lblTotFac.Text = "0.00"
        lblPorSaldo.Text = "0.00"
        lblOrden.Text = ""
        lblContrato.Text = ""
        txtPresupuesto.Text = ""
        txtObservacion.Text = ""
        For x As Integer = 0 To dgvHistorial.RowCount - 1
            dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
        Next
    End Sub

    Private Sub cboPartidas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectionChangeCommitted
        If cboPartidas.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            Dim dtPartida As DataTable
            dtPartida = New DataTable
            dtPartida = eValorizaciones.fListarObras(21, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboPartidas.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If dtPartida.Rows.Count > 0 Then
                'IIf(vb.IsDBNull(dtPartida.Rows(0).Item("xret")), dtPartida.Rows(0).Item("xret"), "0.00")
                lblPorRete.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("xret")) = True, "0.00", dtPartida.Rows(0).Item("xret")) 'UCase(dtPartida.Rows(0).Item("xret"))
                lblFechaIni.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("FechaInicio")) = True, Today(), dtPartida.Rows(0).Item("FechaInicio")) 'UCase(dtPartida.Rows(0).Item("FechaInicio"))
                lblFechaFin.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("FechaFinal")) = True, Today(), dtPartida.Rows(0).Item("FechaFinal")) 'UCase(dtPartida.Rows(0).Item("FechaFinal"))
                txtObservacion.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("ParObservacion")) = True, "", dtPartida.Rows(0).Item("ParObservacion")) 'UCase(dtPartida.Rows(0).Item("ParObservacion"))
                lblMoneda.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("MonDescripcion")) = True, "", dtPartida.Rows(0).Item("MonDescripcion")) 'UCase(dtPartida.Rows(0).Item("MonDescripcion"))
                lblDoc.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("DescripDoc")) = True, "", dtPartida.Rows(0).Item("DescripDoc")) 'UCase(dtPartida.Rows(0).Item("DescripDoc"))
                lblPresu.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("ParValorizado")) = True, "0.00", Format(dtPartida.Rows(0).Item("ParValorizado"), "#,##0.00")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                lblOrden.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("NumOrden")) = True, "", dtPartida.Rows(0).Item("NumOrden")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                lblContrato.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("NContrato")) = True, "", dtPartida.Rows(0).Item("NContrato")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                txtPresupuesto.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("ubtCodigo")) = True, "", dtPartida.Rows(0).Item("ubtCodigo")) & "-" & IIf(vb.IsDBNull(dtPartida.Rows(0).Item("ubtDescripcion")) = True, "", dtPartida.Rows(0).Item("ubtDescripcion")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                'lblContrato.Text = IIf(vb.IsDBNull(dtPartida.Rows(0).Item("NContrato")) = True, "", dtPartida.Rows(0).Item("NContrato")) ' UCase(dtPartida.Rows(0).Item("ParValorizado"))
                'lblContrato.Text = IIf(xD, 1, 0)
            End If
            For x As Integer = 0 To dgvHistorial.RowCount - 1
                dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
            Next
            dgvHistorial.AutoGenerateColumns = False
            eValorizaciones = New clsValorizaciones
            Dim dtValorizaciones As DataTable
            dtValorizaciones = New DataTable
            dtValorizaciones = eValorizaciones.fListarObras(22, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboPartidas.SelectedValue), Today(), "")
            If dtValorizaciones.Rows.Count > 0 Then
                dgvHistorial.DataSource = dtValorizaciones
                'cargarFormasdePago()
                'cargarTipoDoc()
            ElseIf dtValorizaciones.Rows.Count = 0 Then
                'txtDescripcionEsp.Clear()
            End If
        ElseIf cboPartidas.SelectedIndex = -1 Then
            'cboPartidas.SelectedIndex = -1
            'txtDescripcionEsp.Clear()
            '
        End If
        Calcular()
    End Sub

    Dim Importe As Double = 0
    Dim TotRetenido As Double = 0
    Dim TotNC As Double = 0
    Dim TotDetr As Double = 0
    Dim TotFac As Double = 0
    Dim TotFGaranPagado As Double = 0
    Dim TotAdendasAumento As Double = 0
    Dim TotAdendasDismi As Double = 0

    Sub Calcular()

        Importe = 0
        TotRetenido = 0
        TotFGaranPagado = 0
        TotAdendasAumento = 0
        TotAdendasDismi = 0
        TotNC = 0
        TotDetr = 0
        TotFac = 0

        Dim SaldoTotalPagar As Double = 0
        Dim ImpFac As Double = 0
        Dim ImpNA As Double = 0
        Dim ImpDetr As Double = 0
        'Dim xImpFac As Double = 0
        Dim xTotPagado As Double = 0
        Dim xRetencion As Double = 0
        Dim xNC As Double = 0
        Dim xIdValorizacion As String = ""

        If dgvHistorial.Rows.Count > 0 Then
            For x As Integer = 0 To dgvHistorial.Rows.Count - 1
                ImpFac = IIf(vb.IsDBNull(dgvHistorial.Rows(x).Cells("Column6").Value) = True, "0.00", dgvHistorial.Rows(x).Cells("Column6").Value)
                ImpNA = IIf(vb.IsDBNull(dgvHistorial.Rows(x).Cells("TotNA").Value) = True, "0.00", dgvHistorial.Rows(x).Cells("TotNA").Value)
                ImpDetr = IIf(vb.IsDBNull(dgvHistorial.Rows(x).Cells("Column10").Value) = True, "0.00", dgvHistorial.Rows(x).Cells("Column10").Value)
                xTotPagado = IIf(vb.IsDBNull(dgvHistorial.Rows(x).Cells("Pagado").Value) = True, "0.00", dgvHistorial.Rows(x).Cells("Pagado").Value)
                xRetencion = IIf(vb.IsDBNull(dgvHistorial.Rows(x).Cells("Column11").Value) = True, "0.00", dgvHistorial.Rows(x).Cells("Column11").Value)
                xNC = IIf(vb.IsDBNull(dgvHistorial.Rows(x).Cells("TotNA").Value) = True, "0.00", dgvHistorial.Rows(x).Cells("TotNA").Value)
                'xImpFac = IIf(vb.IsDBNull(dgvHistorial.Rows(x).Cells("Column6").Value) = True, "0.00", dgvHistorial.Rows(x).Cells("Column6").Value)
                'Column11
                SaldoTotalPagar = Math.Round(ImpFac - (ImpNA + ImpDetr + xRetencion), 2) 'Column6 - (TotNA + Column10)
                xIdValorizacion = Trim(dgvHistorial.Rows(x).Cells("IdValorizacion").Value)

                If Trim(dgvHistorial.Rows(x).Cells("IdTipoPago").Value) = "00003" Then
                    dgvHistorial.Rows(x).Cells("TotPagar").Value = Convert.ToDouble(dgvHistorial.Rows(x).Cells("TotPagar").Value)
                ElseIf Trim(dgvHistorial.Rows(x).Cells("IdTipoPago").Value) = "00004" Then
                    dgvHistorial.Rows(x).Cells("TotPagar").Value = Convert.ToDouble(dgvHistorial.Rows(x).Cells("TotPagar").Value)
                Else
                    dgvHistorial.Rows(x).Cells("TotPagar").Value = SaldoTotalPagar
                End If


                Dim xtotPoPag As Double = 0
                xtotPoPag = (xTotPagado * 100) / SaldoTotalPagar

                If xTotPagado = 0 And SaldoTotalPagar = 0 Then
                    xtotPoPag = 0
                End If

                
                dgvHistorial.Rows(x).Cells("Column3").Value = xtotPoPag
               
                Dim iResultadoMod As Integer = 0
                eValorizaciones = New clsValorizaciones
                iResultadoMod = eValorizaciones.fActualizarSaldos(52, SaldoTotalPagar, xTotPagado, 0, Today(), "", gEmpresa, Trim(gEmpresa), Trim(xIdValorizacion), "", "", "", 0, gPeriodo)
                If Trim(dgvHistorial.Rows(x).Cells("IdTipoPago").Value) <> "00002" Then
                    Importe = Importe + Val((Convert.ToDouble(dgvHistorial.Rows(x).Cells("Pagado").Value)))
                ElseIf Trim(dgvHistorial.Rows(x).Cells("IdTipoPago").Value) = "00002" Then
                    TotFGaranPagado = TotFGaranPagado + Val((Convert.ToDouble(dgvHistorial.Rows(x).Cells("Pagado").Value)))
                End If
                If Trim(dgvHistorial.Rows(x).Cells("IdTipoPago").Value) = "00003" Then
                    'dgvHistorial.Rows(x).Cells("Column3").Value = Convert.ToDouble(dgvHistorial.Rows(x).Cells("TotPagar").Value)
                    TotAdendasAumento = TotAdendasAumento + Val((Convert.ToDouble(dgvHistorial.Rows(x).Cells("TotPagar").Value)))
                ElseIf Trim(dgvHistorial.Rows(x).Cells("IdTipoPago").Value) = "00004" Then
                    'dgvHistorial.Rows(x).Cells("Column3").Value = Convert.ToDouble(dgvHistorial.Rows(x).Cells("TotPagar").Value)
                    TotAdendasDismi = TotAdendasDismi + Val((Convert.ToDouble(dgvHistorial.Rows(x).Cells("TotPagar").Value)))
                End If
                TotRetenido = TotRetenido + Val((Convert.ToDouble(dgvHistorial.Rows(x).Cells("Column11").Value)))
                TotNC = TotNC + xNC
                TotDetr = TotDetr + ImpDetr
                TotFac = TotFac + ImpFac 'xImpFac
            Next
            '
            'lblPresu.Text = Format(Convert.ToDouble(lblPresu.Text) + TotAdendasAumento - TotAdendasDismi, "#,##0.00")
            lblPresu.Text = Format(Convert.ToDouble(lblPresu.Text), "#,##0.00")
            lblAbonado.Text = Format(Importe + TotNC + TotDetr + TotRetenido, "#,##0.00")
            lblRetenido.Text = Format(TotRetenido, "#,##0.00")
            lblSaldo.Text = Format((lblPresu.Text - lblAbonado.Text), "#,##0.00")
            lblFGaran.Text = Format(Convert.ToDouble(lblPresu.Text) * (Convert.ToDouble(lblPorRete.Text) / 100), "#,##0.00")
            lblPagoFG.Text = Format(TotFGaranPagado, "#,##0.00")
            lblTotFac.Text = Format(TotFac, "#,##0.00") '"0.00" '

            lblPorAbo.Text = Format(((Convert.ToDouble(lblAbonado.Text)) * 100) / lblPresu.Text, "#,##0.00")
            lblPorSaldo.Text = Format((Convert.ToDouble(lblSaldo.Text) * 100) / lblPresu.Text, "#,##0.00")

        ElseIf dgvHistorial.Rows.Count = 0 Then
            lblPresu.Text = "0.00"
            lblAbonado.Text = "0.00"
            lblSaldo.Text = "0.00"
            lblPorAbo.Text = "0.00"
            lblFGaran.Text = "0.00"
            lblRetenido.Text = "0.00"
            lblPagoFG.Text = "0.00"
            lblTotFac.Text = "0.00"
            lblPorSaldo.Text = "0.00"
        End If
    End Sub

    Private Sub cargarFormasdePago()
        For y As Integer = 0 To dgvHistorial.Rows.Count - 1
            Dim my_strsql As String
            Dim d_objds As New DataSet()
            Dim dgrow As New DataGridViewComboBoxCell
            my_strsql = "SELECT IdRendicion as 'Codigo',Abreviado as 'Cadena' FROM Tesoreria.TipoMovimiento ORDER BY Abreviado"
            Dim objda As New SqlDataAdapter(my_strsql, conexionParaGastosGene)
            objda.Fill(d_objds, "Codigo")
            objda.Fill(d_objds, "Cadena")
            'Finalmente gracias al datagridViewComboBoxCell almacenamos esta
            'consulta como si fuese en un comboBox Normal
            dgrow.DataSource = d_objds.Tables(0).DefaultView
            dgrow.DisplayMember = "Cadena"
            dgrow.ValueMember = "Codigo"
            Try
                'Aqui viene lo bueno: Tanto como la celda de la columna Distrito y el 
                'dgrow  son practicamente comboBoxes... entonces podemos pasarle los
                'datos del uno al otro.
                Me.dgvHistorial.Item("FormaPago", y) = dgrow
            Catch ex As Exception
                MessageBox.Show("Aqui un error: " & ex.ToString)
            End Try
        Next
    End Sub

    Private Sub cargarTipoDoc()

        For y As Integer = 0 To dgvHistorial.Rows.Count - 1
            Dim my_strsql As String
            Dim d_objds As New DataSet()
            Dim dgrow As New DataGridViewComboBoxCell
            my_strsql = "SELECT IdDocumento as 'Codigo',DescripDoc as 'Cadena' FROM Tesoreria.TipoDocumento ORDER BY DescripDoc"
            Dim objda As New SqlDataAdapter(my_strsql, conexionParaGastosGene)
            objda.Fill(d_objds, "Codigo")
            objda.Fill(d_objds, "Cadena")
            'Finalmente gracias al datagridViewComboBoxCell almacenamos esta
            'consulta como si fuese en un comboBox Normal
            dgrow.DataSource = d_objds.Tables(0).DefaultView
            dgrow.DisplayMember = "Cadena"
            dgrow.ValueMember = "Codigo"
            Try
                'Aqui viene lo bueno: Tanto como la celda de la columna Distrito y el 
                'dgrow  son practicamente comboBoxes... entonces podemos pasarle los
                'datos del uno al otro.
                Me.dgvHistorial.Item("TipoDoc", y) = dgrow
            Catch ex As Exception
                MessageBox.Show("Aqui un error: " & ex.ToString)
            End Try
        Next
    End Sub

    Private Sub dgvHistorial_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvHistorial.KeyDown
        Select Case e.KeyCode
            Case Keys.F4
                If dgvHistorial.Rows.Count > 0 Then
                    Dim sIdValorizacion As String = ""
                    Dim xTipo1 As String = ""
                    Dim dblMonto As Double = 0
                    Dim dblODes As Double = 0
                    Dim dblTotNA As Double = 0
                    sIdValorizacion = IIf(Microsoft.VisualBasic.IsDBNull(dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("IdValorizacion").Value), "", dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("IdValorizacion").Value)
                    xTipo1 = IIf(Microsoft.VisualBasic.IsDBNull(dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("Tipo1").Value), "", dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("Tipo1").Value)
                    dblMonto = IIf(Microsoft.VisualBasic.IsDBNull(dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("TotPagar").Value), 0, dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("TotPagar").Value)
                    dblODes = IIf(Microsoft.VisualBasic.IsDBNull(dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("Column12").Value), 0, dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("Column12").Value)
                    dblTotNA = IIf(Microsoft.VisualBasic.IsDBNull(dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("TotNA").Value), 0, dgvHistorial.Rows(dgvHistorial.CurrentRow.Index).Cells("TotNA").Value)
                    If dblODes > 0 Then
                        If dblTotNA < dblODes Then
                            MessageBox.Show("No podr� programar los pagos ya que no se ha ingresado las notas de cr�dito de este comprobante.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        End If
                    End If
                    'Column2
                    If Trim(sIdValorizacion) <> "" And Trim(xTipo1) <> "2" Then
                        Dim frm As New frmValorizacionesPagos
                        frm.Owner = Me
                        frm.sIdValorizacion = Trim(sIdValorizacion)
                        frm.Monto = Trim(dblMonto)
                        frm.ShowInTaskbar = False
                        frm.ShowDialog()
                    Else
                        MessageBox.Show("Item Inhabilitado para Detallar Pagos", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    MessageBox.Show("Seleccione un Registro de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
        End Select
    End Sub

    'Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
    '    If (MessageBox.Show("�Esta seguro de Grabar los calculos del contrato?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
    '        ePartidas = New clsPartidas
    '        Dim iResultado As Integer = 0
    '        iResultado = ePartidas.fGrabar2(16, gEmpresa, Trim(cboPartidas.SelectedValue), Convert.ToDouble(lblPresu.Text), Convert.ToDouble(lblFGaran.Text), Convert.ToDouble(lblPagoFG.Text), Convert.ToDouble(lblTotFac.Text), Convert.ToDouble(lblAbonado.Text) + Convert.ToDouble(lblRetenido.Text), Convert.ToDouble(lblRetenido.Text), Convert.ToDouble(lblSaldo.Text), Convert.ToDouble(lblPorAbo.Text), Convert.ToDouble(lblPorSaldo.Text), gUsuario, gPeriodo)
    '        If iResultado = 1 Then
    '            cboPartidas_SelectionChangeCommitted(sender, e)
    '        End If
    '    End If
    'End Sub

    'Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
    '    If cboPartidas.SelectedIndex = -1 Then
    '        MessageBox.Show("Seleccione un contrato", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        Exit Sub
    '    End If
    '    Try
    '        Dim dtTable2 As DataTable
    '        dtTable2 = New DataTable
    '        eValorizaciones = New clsValorizaciones
    '        'If rdbSaldoVal.Checked = True Then
    '        'dtTable2 = eValorizaciones.ListarValorizacionesxPagar(62, gEmpresa)
    '        'End If
    '        'If rdbSaldoValFac.Checked = True Then
    '        dtTable2 = eValorizaciones.ListarContratoGral(65, gEmpresa, Trim(cboPartidas.SelectedValue), gPeriodo)
    '        'End If
    '        If dtTable2.Rows.Count > 0 Then
    '            'If rdbSaldoVal.Checked = True Then
    '            Muestra_Reporte(RutaAppReportes & "rptReporteContratoGlobal.rpt", dtTable2, "TblLibroDiario", "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa)
    '            'End If
    '            'If rdbSaldoValFac.Checked = True Then
    '            'Muestra_Reporte(RutaAppReportes & "rptValorizacionesxPagarFac2.rpt", dtTable2, "TblLibroDiario", "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa)
    '            'End If
    '        ElseIf dtTable2.Rows.Count = 0 Then
    '            'Dim mensaje As String = ""
    '            'If rdbSaldoVal.Checked = True Then
    '            'mensaje = "No se Encontro ningun Comprobante a Cancelar."
    '            'End If
    '            'MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("Ocurrio un problema en la transacci�n." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    End Try
    'End Sub

 

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If

            f.CRVisor.SelectionFormula = STRfiltro
            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields
        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObra.SelectedIndexChanged

    End Sub

    Private Sub cboContratista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboContratista.SelectedIndexChanged

    End Sub

    'Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click

    'End Sub

    Private Sub txtDescripcionEsp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescripcionEsp.TextChanged

    End Sub

    Private Sub Label12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label12.Click

    End Sub

    Private Sub txtRuc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRuc.TextChanged

    End Sub

    Private Sub Label8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label8.Click

    End Sub

    Private Sub CCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CCosto.SelectedIndexChanged

    End Sub

    Private Sub CCosto_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles CCosto.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            cboPartidas.DataSource = eValorizaciones.fListarObras(15, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If eValorizaciones.iNroRegistros > 0 Then
                cboPartidas.ValueMember = "IdPartida"
                cboPartidas.DisplayMember = "ParDescripcion"
                cboPartidas.SelectedIndex = -1
            End If
            eValorizaciones = New clsValorizaciones
            Dim dtEspecialidad As DataTable
            dtEspecialidad = New DataTable
            dtEspecialidad = eValorizaciones.fListarObras(20, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If dtEspecialidad.Rows.Count > 0 Then
                txtDescripcionEsp.Text = UCase(dtEspecialidad.Rows(0).Item("esp_nombre"))
                txtRuc.Text = UCase(dtEspecialidad.Rows(0).Item("ConRuc"))
                'txtRuc.Text = UCase(dtEspecialidad.Rows(0).Item("ConRuc"))
                'txtRuc.Text = UCase(dtEspecialidad.Rows(0).Item("ConRuc"))
            ElseIf dtEspecialidad.Rows.Count = 0 Then
                txtDescripcionEsp.Clear()
                txtRuc.Clear()
            End If
        ElseIf cboObra.SelectedIndex = -1 Then
            cboPartidas.SelectedIndex = -1
            txtDescripcionEsp.Clear()
        End If
        'lblPorRete.Text = "0.00"
        'lblFechaIni.Text = ""
        'lblFechaFin.Text = ""
        'lblMoneda.Text = ""
        'lblDoc.Text = ""
        'lblPresu.Text = "0.00"
        'lblAbonado.Text = "0.00"
        'lblSaldo.Text = "0.00"
        'lblPorAbo.Text = "0.00"
        'lblFGaran.Text = "0.00"
        'lblRetenido.Text = "0.00"
        'lblPagoFG.Text = "0.00"
        'lblTotFac.Text = "0.00"
        'lblPorSaldo.Text = "0.00"
        'lblOrden.Text = ""
        'lblContrato.Text = ""
        'txtPresupuesto.Text = ""
        'For x As Integer = 0 To dgvHistorial.RowCount - 1
        '    dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
        'Next
        Limpiar()
    End Sub
End Class