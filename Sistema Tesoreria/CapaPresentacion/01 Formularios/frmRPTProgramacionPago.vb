﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmRPTProgramacionPago

    Dim rptPP_ID As New rptPP_ID
    Public Property VG_ID_PROGPAGO As String

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition


#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmRPTProgramacionPago = Nothing

    Public Shared Function Instance() As frmRPTProgramacionPago
        If frmInstance Is Nothing Then
            frmInstance = New frmRPTProgramacionPago
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmRPTProgramacionPago_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub frmRPTProgramacionPago_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        rptPP_ID.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        crParameterFieldDefinitions = rptPP_ID.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@ID_PROGPAGO")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = VG_ID_PROGPAGO
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = rptPP_ID

    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class