Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmTipoDocumento
    Dim WithEvents cmr As CurrencyManager

    Private eTipoDocumento As clsTipoDocumento
    Private eFiltro As clsPlantFiltro
    Private eTempo As New clsPlantTempo
    Private eTempo2 As New clsPlantTempo
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Dim NumFila As Integer
    Dim Fila As DataRow
    'Dim sTab As String = 0
    Dim iOpcion As Integer

    Private Shared frmInstance As frmTipoDocumento = Nothing
    Public Shared Function Instance() As frmTipoDocumento
        If frmInstance Is Nothing Then
            frmInstance = New frmTipoDocumento
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoDocumento_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        Panel2.Visible = True
        cboEstados.SelectedIndex = -1
        txtBusqueda.Clear()
        txtBusqueda.Focus()
    End Sub

    Private Sub frmTipoDocumento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mMostrarGrilla()
        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1

        cboEstados.DataSource = eTempo.fColEstados
        cboEstados.ValueMember = "Col02"
        cboEstados.DisplayMember = "Col01"
        'cboEstados.SelectedIndex = -1
        dgvLista.Focus()
        dgvLista.Width = 524
        dgvLista.Height = 397
    End Sub

    Private Sub mMostrarGrilla()
        eTipoDocumento = New clsTipoDocumento
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = eTipoDocumento.fListar()
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTableLista
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eTipoDocumento.iNroRegistros
        If eTipoDocumento.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        'If sTab = 0 Then
        iOpcion = 1
        'sTab = 1
        LimpiarControles()
        txtDescripcion.Enabled = True
        txtAbre.Enabled = True
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False
        txtDescripcion.Focus()
        'End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        'If sTab = 0 Then
        'sTab = 1
        iOpcion = 2
        VerPosicion()
        txtDescripcion.Enabled = True
        txtAbre.Enabled = True
        cboEstado.Enabled = True
        'End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        'If sTab = 0 Then
        'sTab = 1
        VerPosicion()
        txtDescripcion.Enabled = False
        'cboEstado.Enabled = False
        txtAbre.Enabled = False
        cboEstado.Enabled = False
        'End If
    End Sub


    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                txtCodigo.Text = Trim(Fila("IdDocumento").ToString)
                txtDescripcion.Text = Trim(Fila("DescripDoc").ToString)
                txtAbre.Text = Trim(Fila("AbrDoc").ToString)
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtDescripcion.Clear()
        txtAbre.Clear()
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim sIdEmpleado As String = ""
        Dim sIdMoneda As String = ""
        Dim Mensaje As String = ""
        eTempo = New clsPlantTempo
        eTipoDocumento = New clsTipoDocumento

        If Len(txtDescripcion.Text.Trim) > 0 And Len(txtAbre.Text.Trim) > 0 Then
            If iOpcion = 1 Then
                Mensaje = "Grabar"
            ElseIf iOpcion = 2 Then
                Mensaje = "Actualizar"
            End If

            If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                frmPrincipal.sFlagGrabar = "1"
                If iOpcion = 1 Then
                    eTipoDocumento.fCodigo()
                    sCodigoRegistro = eTipoDocumento.sCodFuturo
                    txtCodigo.Text = Trim(sCodigoRegistro)
                Else
                    sCodigoRegistro = txtCodigo.Text
                End If
                iResultado = eTipoDocumento.fGrabar(iOpcion, sCodigoRegistro, Trim(txtDescripcion.Text), Trim(txtAbre.Text), cboEstado.SelectedValue)
                If iResultado > 0 Then
                    mMostrarGrilla()
                End If
            End If

        Else
            frmPrincipal.sFlagGrabar = "0"
            If Len(txtDescripcion.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese la Descripcion del Documento", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtDescripcion.Focus()
                Exit Sub
            End If

            If Len(txtAbre.Text.Trim) = 0 Then
                MessageBox.Show("Ingrese la Abreviatura del Documento", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtAbre.Focus()
                Exit Sub
            End If
        End If

    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        VerPosicion()
        If Fila("Estados") = "Activo" Then
            Dim iResultado As Int32
            Dim Mensaje As String = ""
            Dim iOpcionDel As Integer
            Try
                If dgvLista.Rows.Count > 0 Then
                    If Fila("Estados") = "Activo" Then
                        iOpcionDel = 4
                        Mensaje = "�Desea Anular?"
                    End If
                    If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("IdDocumento") & "   " & Chr(13) & "Descripci�n: " & Fila("DescripDoc"), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        eTipoDocumento = New clsTipoDocumento
                        iResultado = eTipoDocumento.fEliminar(Fila("IdDocumento"))
                        If iResultado = 1 Then
                            mMostrarGrilla()
                        End If
                    Else
                        Exit Sub
                    End If
                End If
            Catch ex As Exception
            End Try
        Else
            MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        'Else
        'Exit Sub
        'End If
    End Sub

    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged
        If Len(txtBusqueda.Text.Trim) > 0 Then
            eTipoDocumento = New clsTipoDocumento
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoDocumento.fBuscaDocumentos(5, Trim(txtBusqueda.Text), 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoDocumento.iNroRegistros
            If eTipoDocumento.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBusqueda.Text.Trim) = 0 Then
            eTipoDocumento = New clsTipoDocumento
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eTipoDocumento.fBuscaDocumentos(6, "", 0)
            Me.dgvLista.AutoGenerateColumns = False
            Me.dgvLista.DataSource = dtTable
            cmr = Me.BindingContext(Me.dgvLista.DataSource)
            Me.stsTotales.Items(0).Text = "Total de Registros= " & eTipoDocumento.iNroRegistros
            If eTipoDocumento.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        End If
    End Sub

    Private Sub BeLabel7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel7.Click
        Panel2.Visible = False
    End Sub

    Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
        If (cboEstados.SelectedIndex > -1) Then
            Try
                eTipoDocumento = New clsTipoDocumento
                eTempo = New clsPlantTempo
                dtTable = New DataTable
                Dim sValorEstado As String = ""
                sValorEstado = cboEstados.SelectedValue.ToString
                If sValorEstado <> "CapaDatos.clsPlantTempo" Then
                    dtTable = eTipoDocumento.fBuscaDocumentos(7, Trim(txtBusqueda.Text), cboEstados.SelectedValue)
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTable
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eTipoDocumento.iNroRegistros
                    If eTipoDocumento.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                End If
               
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If
    End Sub
End Class
