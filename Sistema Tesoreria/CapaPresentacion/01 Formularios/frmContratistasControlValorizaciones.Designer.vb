<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContratistasControlValorizaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboPartidas = New ctrLibreria.Controles.BeComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboContratista = New ctrLibreria.Controles.BeComboBox
        Me.cboObra = New ctrLibreria.Controles.BeComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.dgvHistorial = New System.Windows.Forms.DataGridView
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Semana = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.X = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.IdValorizacion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Cerrado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.CCosto = New ctrLibreria.Controles.BeComboBox
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(12, 88)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Contrato"
        '
        'cboPartidas
        '
        Me.cboPartidas.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPartidas.BackColor = System.Drawing.Color.Ivory
        Me.cboPartidas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartidas.ForeColor = System.Drawing.Color.Black
        Me.cboPartidas.FormattingEnabled = True
        Me.cboPartidas.KeyEnter = True
        Me.cboPartidas.Location = New System.Drawing.Point(63, 80)
        Me.cboPartidas.Name = "cboPartidas"
        Me.cboPartidas.Size = New System.Drawing.Size(700, 21)
        Me.cboPartidas.TabIndex = 13
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(12, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Obra"
        '
        'cboContratista
        '
        Me.cboContratista.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboContratista.BackColor = System.Drawing.Color.Ivory
        Me.cboContratista.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboContratista.ForeColor = System.Drawing.Color.Black
        Me.cboContratista.FormattingEnabled = True
        Me.cboContratista.KeyEnter = True
        Me.cboContratista.Location = New System.Drawing.Point(473, 22)
        Me.cboContratista.Name = "cboContratista"
        Me.cboContratista.Size = New System.Drawing.Size(290, 21)
        Me.cboContratista.TabIndex = 11
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(63, 22)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(328, 21)
        Me.cboObra.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(410, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Contratista"
        '
        'dgvHistorial
        '
        Me.dgvHistorial.AllowUserToAddRows = False
        Me.dgvHistorial.BackgroundColor = System.Drawing.Color.White
        Me.dgvHistorial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistorial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Concepto, Me.Semana, Me.X, Me.IdValorizacion, Me.Cerrado})
        Me.dgvHistorial.EnableHeadersVisualStyles = False
        Me.dgvHistorial.Location = New System.Drawing.Point(206, 107)
        Me.dgvHistorial.Name = "dgvHistorial"
        Me.dgvHistorial.RowHeadersVisible = False
        Me.dgvHistorial.Size = New System.Drawing.Size(466, 178)
        Me.dgvHistorial.TabIndex = 25
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Concepto"
        Me.Concepto.HeaderText = "Concepto"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.Width = 300
        '
        'Semana
        '
        Me.Semana.HeaderText = "Semana"
        Me.Semana.Name = "Semana"
        '
        'X
        '
        Me.X.HeaderText = "X"
        Me.X.Name = "X"
        Me.X.Width = 40
        '
        'IdValorizacion
        '
        Me.IdValorizacion.DataPropertyName = "IdValorizacion"
        Me.IdValorizacion.HeaderText = "IdValorizacion"
        Me.IdValorizacion.Name = "IdValorizacion"
        Me.IdValorizacion.Visible = False
        '
        'Cerrado
        '
        Me.Cerrado.DataPropertyName = "Cerrado"
        Me.Cerrado.HeaderText = "Cerrado"
        Me.Cerrado.Name = "Cerrado"
        Me.Cerrado.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.CheckBox2)
        Me.GroupBox2.Controls.Add(Me.CheckBox1)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(63, 127)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(96, 70)
        Me.GroupBox2.TabIndex = 31
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Estado"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(11, 42)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(74, 17)
        Me.CheckBox2.TabIndex = 1
        Me.CheckBox2.Text = "En Sesion"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Location = New System.Drawing.Point(11, 19)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(63, 17)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Text = "Cerrado"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(688, 107)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 32
        Me.Button1.Text = "Grabar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'CCosto
        '
        Me.CCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.CCosto.BackColor = System.Drawing.Color.Ivory
        Me.CCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CCosto.ForeColor = System.Drawing.Color.Black
        Me.CCosto.FormattingEnabled = True
        Me.CCosto.KeyEnter = True
        Me.CCosto.Location = New System.Drawing.Point(63, 53)
        Me.CCosto.Name = "CCosto"
        Me.CCosto.Size = New System.Drawing.Size(328, 21)
        Me.CCosto.TabIndex = 34
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(12, 61)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "C Costo"
        '
        'frmContratistasControlValorizaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(777, 288)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CCosto)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dgvHistorial)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboPartidas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboContratista)
        Me.Controls.Add(Me.cboObra)
        Me.MaximizeBox = False
        Me.Name = "frmContratistasControlValorizaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        CType(Me.dgvHistorial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboPartidas As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboContratista As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvHistorial As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Semana As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents X As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IdValorizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cerrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
