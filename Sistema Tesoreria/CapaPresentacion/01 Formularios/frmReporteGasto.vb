Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared


Public Class frmReporteGasto

    Public prmEmpresa As String
    Public prmFecha As String
    Public prmDocumento As String
    Public prmNroDocumento As String
    Public prmDescGasto As String
    Public prmCentroCosto As String
    Public prmTipoGasto As String
    Public prmTotal As String

    Public prmArea As String
    Public prmResponsable As String
    Public prmNroCaja As String
    Public prmDesCaja As String
    Public prmNroSesion As String
    Public prmMonSimbolo As String
    Public prmNroVale As String
    Public Entregadoa As String = ""


#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteGasto = Nothing
    Public Shared Function Instance() As frmReporteGasto
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmReporteGasto
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteGasto_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteGasto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim report As New rptGastoCajaChica

        report.ParameterFields("Empresa").CurrentValues.AddValue(prmEmpresa)
        report.ParameterFields("Fecha").CurrentValues.AddValue(prmFecha)
        report.ParameterFields("Documento").CurrentValues.AddValue(prmDocumento)
        report.ParameterFields("NroDocumento").CurrentValues.AddValue(prmNroDocumento)
        report.ParameterFields("DesGasto").CurrentValues.AddValue(prmDescGasto)
        report.ParameterFields("CentroCosto").CurrentValues.AddValue(prmCentroCosto)
        report.ParameterFields("TipoGasto").CurrentValues.AddValue(prmTipoGasto)
        report.ParameterFields("Importe").CurrentValues.AddValue(prmTotal)

        report.ParameterFields("Area").CurrentValues.AddValue(prmArea)
        report.ParameterFields("Responsable").CurrentValues.AddValue(prmResponsable)
        report.ParameterFields("NroCaja").CurrentValues.AddValue(prmNroCaja)
        report.ParameterFields("DesCaja").CurrentValues.AddValue(prmDesCaja)
        report.ParameterFields("NroSesion").CurrentValues.AddValue(prmNroSesion)
        report.ParameterFields("NroVale").CurrentValues.AddValue(prmNroVale)
        report.ParameterFields("Entregado").CurrentValues.AddValue(Entregadoa)
        report.ParameterFields("MonSimbolo").CurrentValues.AddValue(prmMonSimbolo)
        report.ParameterFields("Ruc").CurrentValues.AddValue(gEmprRuc)
        CrystalReportViewer1.ReportSource = report

    End Sub

    Public Sub Salir()
        If (MessageBox.Show("�Esta seguro de Cerrar la Ventana Actual?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            Me.Close()
        End If
    End Sub

End Class