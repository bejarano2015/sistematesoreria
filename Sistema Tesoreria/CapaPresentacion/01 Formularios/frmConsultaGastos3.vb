Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmConsultaGastos3

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmConsultaGastos3 = Nothing
    Public Shared Function Instance() As frmConsultaGastos3
        If frmInstance Is Nothing Then
            frmInstance = New frmConsultaGastos3
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmConsultaGastos3_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub

    Private Sub frmConsultaGastos3_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Dim dtBusqueda As DataTable
    Private eBusquedaGastos As clsBusquedaGastos
    Private eTempo As clsPlantTempo
    Dim GrabarDet As Int16 = 0
    Dim traer As String = ""

    Private Sub gestionaResaltados(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(1).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(2).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(3).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(4).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(5).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(6).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(7).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(8).Style.BackColor = Color.Silver

        visor.Rows(fila).Cells(9).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(10).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(11).Style.BackColor = Color.Silver

        visor.Rows(fila).Cells(15).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(16).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(17).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(18).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(19).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(20).Style.BackColor = Color.Silver
        visor.Rows(fila).Cells(21).Style.BackColor = Color.Silver
        
    End Sub

    Private Sub gestionaResaltados2(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(11).Style.BackColor = Color.Aqua
    End Sub

    Private Sub gestionaResaltados3(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
        visor.Rows(fila).Cells(0).Style.BackColor = c
        visor.Rows(fila).Cells(1).Style.BackColor = c
        visor.Rows(fila).Cells(2).Style.BackColor = c
        visor.Rows(fila).Cells(3).Style.BackColor = c
        visor.Rows(fila).Cells(4).Style.BackColor = c
        visor.Rows(fila).Cells(5).Style.BackColor = c
        visor.Rows(fila).Cells(6).Style.BackColor = c
        visor.Rows(fila).Cells(7).Style.BackColor = c
        visor.Rows(fila).Cells(8).Style.BackColor = c

        visor.Rows(fila).Cells(9).Style.BackColor = c
        visor.Rows(fila).Cells(10).Style.BackColor = c
        visor.Rows(fila).Cells(11).Style.BackColor = c
        'visor.Rows(fila).Cells(12).Style.BackColor = c

        'visor.Rows(fila).Cells(13).Style.BackColor = c
        'visor.Rows(fila).Cells(14).Style.BackColor = c

        visor.Rows(fila).Cells(15).Style.BackColor = c
        visor.Rows(fila).Cells(16).Style.BackColor = c
        visor.Rows(fila).Cells(17).Style.BackColor = c
        visor.Rows(fila).Cells(18).Style.BackColor = c
        visor.Rows(fila).Cells(19).Style.BackColor = c
        visor.Rows(fila).Cells(20).Style.BackColor = c
        visor.Rows(fila).Cells(21).Style.BackColor = c

        'visor.Rows(fila).Cells(22).Style.BackColor = c
        'visor.Rows(fila).Cells(23).Style.BackColor = c
        'visor.Rows(fila).Cells(24).Style.BackColor = c

    End Sub

    'Private Sub gestionaResaltados3(ByVal visor As DataGridView, ByVal fila As Int32, ByVal c As System.Drawing.Color)
    '    visor.Rows(fila).Cells(17).Style.BackColor = c
    'End Sub
    Dim AplicarPintado As Integer = 0

    Private Sub frmConsultaGastos3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        traer = "NO"
        'rdbRecibidos.Checked = False
        'rdbPorRecibir.Checked = False

        dtpFechaFin.Text = Now.Date()

        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()


        Dim dtBusqueda As DataTable
        eBusquedaGastos = New clsBusquedaGastos
        dtBusqueda = New DataTable

        rdbPorRecibir.Checked = True

        'rdbRecibidos.Checked = False
        'rdbRecibidosxRendir.Checked = False
        'rdbPorRecibirdeAdministracion.Checked = False
        'rdbEnviados.Checked = True

        chkRendidos.Checked = True
        chkPorRendir.Checked = True

        chkRendidos.Checked = True
        chkRendidos.Enabled = True

        chkPorRendir.Checked = True
        chkPorRendir.Enabled = True

        traer = "SI"

        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(11, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "", "")
        End If

        'For x As Integer = 0 To dgvGastos.RowCount - 1
        '    dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
        'Next

        If rdbPorRecibir.Checked = True Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            'dgvGastos_DoubleClick(sender, e)

            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            AplicarPintado = 1
            '    If dtBusqueda.Rows.Count > 0 Then
            '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '            dgvGastos.Rows.Add()
            '            If dtBusqueda.Rows(y).Item(9) = "1" Or dtBusqueda.Rows(y).Item(9) = "2" Then
            '                gestionaResaltados(dgvGastos, y, Color.Orange)
            '            End If
            '            dgvGastos.Rows(y).Cells(9).Value = dtBusqueda.Rows(y).Item(9)
            '            If dtBusqueda.Rows(y).Item(10) > "1" Then
            '                gestionaResaltados2(dgvGastos, y, Color.Silver)
            '            End If
            '            dgvGastos.Rows(y).Cells(10).Value = dtBusqueda.Rows(y).Item(10)
            '            dgvGastos.Rows(y).Cells(0).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(0), 10)
            '            dgvGastos.Rows(y).Cells(15).Value = True
            '            dgvGastos.Rows(y).Cells(15).ReadOnly = False
            '            If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(19)) = True Then
            '                dgvGastos.Rows(y).Cells(21).Value = ""
            '            Else
            '                dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(19))
            '            End If
            '            dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            If dtBusqueda.Rows(y).Item(20) = "01" Then
            '                dgvGastos.Rows(y).Cells("Column20").Value = False
            '                dgvGastos.Rows(y).Cells("Column20").ReadOnly = False
            '                dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
            '                dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
            '            End If
            '            If dtBusqueda.Rows(y).Item(20) = "02" Then
            '                dgvGastos.Rows(y).Cells("Column20").Value = True
            '                dgvGastos.Rows(y).Cells("Column20").ReadOnly = True
            '                dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
            '                dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
            '            End If
            '            dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(16), 10)
            '            dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '            dgvGastos.Rows(y).Cells(1).Value = dtBusqueda.Rows(y).Item(1)
            '            dgvGastos.Rows(y).Cells(2).Value = dtBusqueda.Rows(y).Item(2)
            '            dgvGastos.Rows(y).Cells(3).Value = dtBusqueda.Rows(y).Item(3)
            '            dgvGastos.Rows(y).Cells(4).Value = dtBusqueda.Rows(y).Item(4)
            '            dgvGastos.Rows(y).Cells(5).Value = dtBusqueda.Rows(y).Item(5)
            '            dgvGastos.Rows(y).Cells(6).Value = dtBusqueda.Rows(y).Item(6)
            '            dgvGastos.Rows(y).Cells(7).Value = dtBusqueda.Rows(y).Item(7)
            '            dgvGastos.Rows(y).Cells(8).Value = dtBusqueda.Rows(y).Item(8)
            '            dgvGastos.Rows(y).Cells(11).Value = dtBusqueda.Rows(y).Item(11)
            '            dgvGastos.Rows(y).Cells(12).Value = dtBusqueda.Rows(y).Item(12)
            '            dgvGastos.Rows(y).Cells(13).Value = dtBusqueda.Rows(y).Item(13)
            '            dgvGastos.Rows(y).Cells(14).Value = dtBusqueda.Rows(y).Item(14)
            '            dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
            '        Next
            '    End If
            'End If
            'dtBusqueda.Dispose()
            'Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString
        End If
        chkRendidos.Checked = False
        chkRendidos.Checked = True
    End Sub

    Sub Pintar()
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                'If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                '    gestionaResaltados(dgvGastos, x, Color.Orange)
                'End If
                'If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                '    gestionaResaltados2(dgvGastos, x, Color.Orange)
                'End If
                'If Len(Trim(dgvGastos.Rows(x).Cells("Observacion").Value)) > 0 Then
                '    gestionaResaltados3(dgvGastos, x, Color.LightCoral)
                'End If
                dgvGastos.Rows(x).Cells("Column14").Value = True
                If rdbPorRecibir.Checked = True Then
                    dgvGastos.Rows(x).Cells("Column17").Value = Now.Date()
                End If


                If dgvGastos.Rows(x).Cells("Column13").Value = "01" Then
                    dgvGastos.Rows(x).Cells("Column20").Value = False
                    dgvGastos.Rows(x).Cells("Column19").Value = "A Rendir"
                    dgvGastos.Rows(x).Cells("Column19").Style.ForeColor = Color.Blue
                ElseIf dgvGastos.Rows(x).Cells("Column13").Value = "02" Then
                    dgvGastos.Rows(x).Cells("Column20").Value = True
                    dgvGastos.Rows(x).Cells("Column19").Value = "Rendido"
                    dgvGastos.Rows(x).Cells("Column19").Style.ForeColor = Color.Red
                End If

                If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                    gestionaResaltados(dgvGastos, x, Color.Orange)
                End If
                If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                    gestionaResaltados2(dgvGastos, x, Color.Orange)
                End If

                'dgvGastos.Rows(x).Cells("Column15").Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
                'dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                'dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
            Next
        End If
    End Sub

    Sub Pintar2()
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                'If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                '    gestionaResaltados(dgvGastos, x, Color.Orange)
                'End If
                'If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                '    gestionaResaltados2(dgvGastos, x, Color.Orange)
                'End If
                'If Len(Trim(dgvGastos.Rows(x).Cells("Observacion").Value)) > 0 Then
                '    gestionaResaltados3(dgvGastos, x, Color.LightCoral)
                'End If
                gestionaResaltados3(dgvGastos, x, Color.LimeGreen)
                dgvGastos.Rows(x).Cells("Column14").Value = True
                'dgvGastos.Rows(x).Cells("Column17").Value = Now.Date()
                If rdbPorRecibir.Checked = True Then
                    dgvGastos.Rows(x).Cells("Column17").Value = Now.Date()
                End If
                If dgvGastos.Rows(x).Cells("Column13").Value = "01" Then
                    dgvGastos.Rows(x).Cells("Column20").Value = False
                    dgvGastos.Rows(x).Cells("Column19").Value = "A Rendir"
                    dgvGastos.Rows(x).Cells("Column19").Style.ForeColor = Color.Blue
                ElseIf dgvGastos.Rows(x).Cells("Column13").Value = "02" Then
                    dgvGastos.Rows(x).Cells("Column20").Value = True
                    dgvGastos.Rows(x).Cells("Column19").Value = "Rendido"
                    dgvGastos.Rows(x).Cells("Column19").Style.ForeColor = Color.Red
                End If
                If dgvGastos.Rows(x).Cells("Column7").Value = "1" Or dgvGastos.Rows(x).Cells("Column7").Value = "2" Then
                    gestionaResaltados(dgvGastos, x, Color.Orange)
                End If
                If dgvGastos.Rows(x).Cells("Column8").Value > "1" Then
                    gestionaResaltados2(dgvGastos, x, Color.Orange)
                End If
                'dgvGastos.Rows(x).Cells("Column15").Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
                'dgvGastos.Rows(x).Cells("Column18").Value = "A Rendir"
                'dgvGastos.Rows(x).Cells("Column18").Style.ForeColor = Color.Blue
            Next
        End If
    End Sub



    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.dgvGastos.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                For Each fila As DataGridViewRow In Me.dgvGastos.Rows
                    If Me.CheckBox1.Checked = True Then
                        If Convert.ToBoolean(fila.Cells("Column14").Value) = True Then
                            fila.Cells("Column16").Value = True
                        End If
                    Else
                        fila.Cells("Column16").Value = False
                    End If
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Sub ValidarDatos()
        'Dim count = 0
        If dgvGastos.Rows.Count > 0 Then
            For x As Integer = 0 To dgvGastos.Rows.Count - 1
                If Convert.ToBoolean(dgvGastos.Rows(x).Cells("Column14").Value) = False And Trim(dgvGastos.Rows(x).Cells("Column18").Value) = "" Then
                    MessageBox.Show("Se ha encontrado Documentos Rechazados sin Observaci�n, Ingrese por Favor", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvGastos.CurrentCell = dgvGastos(18, x)
                    dgvGastos.BeginEdit(True)
                    GrabarDet = 0
                    Exit Sub
                End If

                If Convert.ToBoolean(dgvGastos.Rows(x).Cells("Column14").Value) = True And Convert.ToBoolean(dgvGastos.Rows(x).Cells("Column16").Value) = True And Trim(dgvGastos.Rows(x).Cells("Column17").Value) = "" Then
                    MessageBox.Show("Ingrese la Fecha del Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvGastos.CurrentCell = dgvGastos(20, x)
                    GrabarDet = 0
                    Exit Sub
                End If
            Next
            GrabarDet = 1
        End If
    End Sub


    Private Sub btnEnviar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        ValidarDatos()
        If GrabarDet = 1 Then
            Dim ContaRecibidos As Integer = 0
            Dim ContaRechazados As Integer = 0
            Dim rEnviaDoc As Integer = 0
            Dim rEnviaDoc2 As Integer = 0
            If dgvGastos.Rows.Count > 0 Then
                For z As Integer = 0 To dgvGastos.Rows.Count - 1
                    'Column20
                    If Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column16").Value) = True Then
                        If Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column14").Value) = True And Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column16").Value) = True And Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column20").Value) = True Then
                            eBusquedaGastos = New clsBusquedaGastos
                            Dim IdMovimiento As String = ""
                            Dim FechaRecibo As DateTime
                            IdMovimiento = Trim(dgvGastos.Rows(z).Cells("Column9").Value)
                            FechaRecibo = Trim(dgvGastos.Rows(z).Cells("Column17").Value)
                            rEnviaDoc = eBusquedaGastos.fConfirmarDoc(IdMovimiento, gEmpresa, 1, FechaRecibo)
                        End If
                        If Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column14").Value) = True And Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column16").Value) = True And Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column20").Value) = False Then
                            eBusquedaGastos = New clsBusquedaGastos
                            Dim IdMovimiento As String = ""
                            Dim FechaRecibo As DateTime
                            IdMovimiento = Trim(dgvGastos.Rows(z).Cells("Column9").Value)
                            FechaRecibo = Trim(dgvGastos.Rows(z).Cells("Column17").Value)
                            rEnviaDoc = eBusquedaGastos.fConfirmarDoc2(IdMovimiento, gEmpresa, 1, FechaRecibo)
                        End If
                        
                        If rEnviaDoc = 1 Then
                            ContaRecibidos = ContaRecibidos + 1
                        End If
                        'ContaRecibidos = ContaRecibidos + 1
                    ElseIf Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column14").Value) = False And Convert.ToBoolean(dgvGastos.Rows(z).Cells("Column16").Value) = False Then
                        eBusquedaGastos = New clsBusquedaGastos
                        Dim IdMovimiento As String = ""
                        Dim Observacion As String = ""
                        'Dim FechaRecibo As DateTime
                        IdMovimiento = Trim(dgvGastos.Rows(z).Cells("Column9").Value)
                        'FechaRecibo = Trim(dgvGastos.Rows(z).Cells("Column17").Value)
                        Observacion = Trim(dgvGastos.Rows(z).Cells("Column18").Value)
                        rEnviaDoc2 = eBusquedaGastos.fRechazDoc(IdMovimiento, gEmpresa, 0, Observacion)
                        If rEnviaDoc2 = 1 Then
                            ContaRechazados = ContaRechazados + 1
                        End If
                    End If
                Next
            End If
            'If ContaRecibidos = 0 Then
            '    MessageBox.Show("Seleccione Documentos para Recibir!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            '    Exit Sub
            'End If
            'If ContaRecibidos > 0 Then
            MessageBox.Show("Documentos Aceptados " & ContaRecibidos & ", Documentos Rechazados " & ContaRechazados, "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            rdbRecibidos.Focus()
            rdbRecibidos.Checked = True
            dtpFechaFin_ValueChanged(sender, e)
            'Exit Sub
            'End If
            'frmConsultaGastos3_Load(sender, e)
        End If
    End Sub

    Private Sub dgvGastos_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGastos.CellContentClick
        If dgvGastos.Rows.Count > 0 Then
            Dim columna As Integer = dgvGastos.CurrentCell.ColumnIndex
            If columna = 0 Then
                If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value) = False Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value = True
                ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value) = True Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value = False
                End If
                Dim itfVoF As String = ""
                If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value) = False Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").Value = False
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").ReadOnly = True
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Value = "A Rendir"
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Style.ForeColor = Color.Blue
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value = False
                    'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").ReadOnly = True
                    MessageBox.Show("Se va ha rechazar este documento. Ingrese una Observacion!", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    dgvGastos.CurrentCell = dgvGastos(18, dgvGastos.CurrentRow.Index)
                    dgvGastos.BeginEdit(True)
                ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column14").Value) = True Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").Value = False
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column16").ReadOnly = False
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column18").Value() = ""
                    If dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column13").Value = "01" Then
                        dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value = False
                        'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").ReadOnly = True
                        dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Value = "A Rendir"
                        dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Style.ForeColor = Color.Blue
                    ElseIf dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column13").Value = "02" Then
                        dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value = True
                        'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").ReadOnly = True
                        dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Value = "Rendido"
                        dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Style.ForeColor = Color.Red
                    End If
                End If
            ElseIf columna = 1 Then
                If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value) = False Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value = True
                ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value) = True Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value = False
                End If
                If Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value) = False Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Value = "A Rendir"
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Style.ForeColor = Color.Blue
                ElseIf Convert.ToBoolean(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column20").Value) = True Then
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Value = "Rendido"
                    dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column19").Style.ForeColor = Color.Red
                End If
            End If
        End If
    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        If Len(txtTexto.Text) > 0 And cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCriterio.Focus()
            Exit Sub
        End If
        Dim dtBusqueda As DataTable
        eBusquedaGastos = New clsBusquedaGastos
        dtBusqueda = New DataTable
        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
            If Len(Trim(txtTexto.Text)) > 0 And cboCriterio.SelectedIndex > -1 Then
                txtTexto_TextChanged(sender, e)
                Exit Sub
            End If
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(11, Trim(txtTexto.Text), gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "", "")
        End If
        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
            If Len(Trim(txtTexto.Text)) > 0 And cboCriterio.SelectedIndex > -1 Then
                txtTexto_TextChanged(sender, e)
                Exit Sub
            End If
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(50, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "", "")
        End If
        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
            If Len(Trim(txtTexto.Text)) > 0 And cboCriterio.SelectedIndex > -1 Then
                txtTexto_TextChanged(sender, e)
                Exit Sub
            End If
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(51, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "", "")
        End If
        If rdbRecibidos.Checked = True And traer = "SI" Then
            If Len(Trim(txtTexto.Text)) > 0 And cboCriterio.SelectedIndex > -1 Then
                txtTexto_TextChanged(sender, e)
                Exit Sub
            End If
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(20, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        End If
        If rdbRecibidosxRendir.Checked = True And traer = "SI" Then
            If Len(Trim(txtTexto.Text)) > 0 And cboCriterio.SelectedIndex > -1 Then
                txtTexto_TextChanged(sender, e)
                Exit Sub
            End If
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(38, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        End If
        If rdbPorRecibirdeAdministracion.Checked = True And traer = "SI" Then
            If Len(Trim(txtTexto.Text)) > 0 And cboCriterio.SelectedIndex > -1 Then
                txtTexto_TextChanged(sender, e)
                Exit Sub
            End If
            dtBusqueda = eBusquedaGastos.fConsultaGastos3(49, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, 0, "", "")
        End If
        'For x As Integer = 0 To dgvGastos.RowCount - 1
        '    dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
        'Next
        If rdbPorRecibir.Checked = True Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub
            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Silver)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = False
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "01" Then
            '                    dgvGastos.Rows(y).Cells("Column20").Value = False
            '                    dgvGastos.Rows(y).Cells("Column20").ReadOnly = False
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
            '                End If
            '                If dtBusqueda.Rows(y).Item(20) = "02" Then
            '                    dgvGastos.Rows(y).Cells("Column20").Value = True
            '                    dgvGastos.Rows(y).Cells("Column20").ReadOnly = True
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
            '    Next
            'End If
        End If

        If rdbRecibidos.Checked = True Then
            dgvGastos.DataSource = dtBusqueda
            'Pintar()
            Pintar2()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub
            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(15).ReadOnly = False
            '            ElseIf z = 17 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(17).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = True
            '            ElseIf z = 18 Then
            '                dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '                'ElseIf z = 19 Then
            '                '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
            '                '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "02" Then
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(15).Value = True
            '        dgvGastos.Rows(y).Cells(15).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(17).Value = True
            '        dgvGastos.Rows(y).Cells(17).ReadOnly = True

            '        dgvGastos.Rows(y).Cells(19).Value = True
            '        dgvGastos.Rows(y).Cells(19).ReadOnly = True

            '        'dgvGastos.Rows(y).Cells(21).Value = ""
            '        'dgvGastos.Rows(y).Cells(21).ReadOnly = True
            '    Next
            'End If
        End If

        'puza
        If rdbRecibidosxRendir.Checked Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub
            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(15).ReadOnly = False
            '            ElseIf z = 17 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(17).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = True
            '            ElseIf z = 18 Then
            '                dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '                'ElseIf z = 19 Then
            '                '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
            '                '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "01" Then
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(15).Value = True
            '        dgvGastos.Rows(y).Cells(15).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(17).Value = False
            '        dgvGastos.Rows(y).Cells(17).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(19).Value = True
            '        dgvGastos.Rows(y).Cells(19).ReadOnly = True

            '        'dgvGastos.Rows(y).Cells(21).Value = ""
            '        'dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '    Next
            'End If
        End If
        If rdbPorRecibirdeAdministracion.Checked Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub
            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)

            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(15).ReadOnly = False
            '            ElseIf z = 17 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(17).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = True
            '            ElseIf z = 18 Then
            '                'dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                'dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '                'ElseIf z = 19 Then
            '                '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
            '                '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "01" Then
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                'dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                'dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(15).Value = False
            '        dgvGastos.Rows(y).Cells(15).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(17).Value = False
            '        dgvGastos.Rows(y).Cells(17).ReadOnly = True

            '        dgvGastos.Rows(y).Cells(19).Value = False
            '        dgvGastos.Rows(y).Cells(19).ReadOnly = True

            '        'dgvGastos.Rows(y).Cells(21).Value = ""
            '        'dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '    Next
            'End If
        End If
        'dtBusqueda.Dispose()
        'Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString
    End Sub

    Private Sub dtpFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub rdbRecibidos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbRecibidos.CheckedChanged
        btnEnviar.Enabled = True
        CheckBox1.Enabled = False
        CheckBox1.Checked = True

        chkRendidos.Checked = False
        chkRendidos.Enabled = False

        chkPorRendir.Checked = False
        chkPorRendir.Enabled = False
        txtTexto.Clear()
        'rdbPorRecibir.Checked = False
        'rdbRecibidos.Checked = True
        'rdbRecibidosxRendir.Checked = False
        'rdbPorRecibirdeAdministracion.Checked = False

        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub rdbPorRecibir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPorRecibir.CheckedChanged
        btnEnviar.Enabled = True
        CheckBox1.Enabled = True
        CheckBox1.Checked = False

        chkRendidos.Checked = True
        chkRendidos.Enabled = True

        chkPorRendir.Checked = True
        chkPorRendir.Enabled = True

        txtTexto.Clear()
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub txtTexto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTexto.KeyPress
        'Select Case Asc(e.KeyChar)
        '    Case 13
        '        If dtpFechaFin.Value < dtpFechaIni.Value Then
        '            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '            dtpFechaIni.Focus()
        '            Exit Sub
        '        End If

        '        If cboCriterio.SelectedIndex = -1 Then
        '            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '            cboCriterio.Focus()
        '            Exit Sub
        '        End If

        '        Dim dtBusqueda As DataTable
        '        eBusquedaGastos = New clsBusquedaGastos
        '        dtBusqueda = New DataTable

        '        If Len(txtTexto.Text) > 0 Then
        '            If cboCriterio.Text = "CONCEPTO" Then
        '                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '                End If

        '                If rdbRecibidos.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(21, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbRecibidosxRendir.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(39, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbPorRecibirdeAdministracion.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(1, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '                End If

        '            ElseIf cboCriterio.Text = "RESPONSABLE" Then

        '                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '                End If


        '                If rdbRecibidos.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(22, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If
        '                If rdbRecibidosxRendir.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(40, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbPorRecibirdeAdministracion.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '                End If

        '            ElseIf cboCriterio.Text = "CHEQUE" Then

        '                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '                End If

        '                If rdbRecibidos.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(23, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If
        '                If rdbRecibidosxRendir.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(41, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbPorRecibirdeAdministracion.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(3, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '                End If


        '            ElseIf cboCriterio.Text = "MACRO" Then

        '                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '                End If

        '                If rdbRecibidos.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(24, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If
        '                If rdbRecibidosxRendir.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(42, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbPorRecibirdeAdministracion.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(4, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '                End If

        '            ElseIf cboCriterio.Text = "CARTA" Then

        '                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '                End If


        '                If rdbRecibidos.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(25, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If
        '                If rdbRecibidosxRendir.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(43, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbPorRecibirdeAdministracion.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(5, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '                End If

        '            ElseIf cboCriterio.Text = "TRANSFERENCIA" Then

        '                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '                End If

        '                If rdbRecibidos.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(26, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If
        '                If rdbRecibidosxRendir.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(44, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbPorRecibirdeAdministracion.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(6, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '                End If

        '            ElseIf cboCriterio.Text = "OTROS" Then

        '                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '                End If

        '                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '                End If

        '                If rdbRecibidos.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(27, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If
        '                If rdbRecibidosxRendir.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(45, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '                End If

        '                If rdbPorRecibirdeAdministracion.Checked = True Then
        '                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(7, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '                End If

        '            End If
        '        ElseIf Len(txtTexto.Text) = 0 Then
        '            dtpFechaFin_ValueChanged(sender, e)
        '            Exit Sub
        '        End If

        '        For x As Integer = 0 To dgvGastos.RowCount - 1
        '            dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
        '        Next

        '        If rdbPorRecibir.Checked = True Then
        '            If dtBusqueda.Rows.Count > 0 Then
        '                For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '                    dgvGastos.Rows.Add()
        '                    For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                        If z = 9 Then
        '                            If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                                gestionaResaltados(dgvGastos, y, Color.Orange)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 10 Then
        '                            If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                                gestionaResaltados2(dgvGastos, y, Color.Silver)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 0 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                        ElseIf z = 15 Then
        '                            'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                            dgvGastos.Rows(y).Cells(15).Value = True
        '                            'End If
        '                            dgvGastos.Rows(y).Cells(z).ReadOnly = False
        '                        ElseIf z = 19 Then
        '                            If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                                dgvGastos.Rows(y).Cells(21).Value = ""
        '                            Else
        '                                dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                                'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                                '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                                'End If
        '                            End If
        '                            dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                        ElseIf z = 20 Then
        '                            If dtBusqueda.Rows(y).Item(20) = "01" Then
        '                                dgvGastos.Rows(y).Cells("Column20").Value = False
        '                                dgvGastos.Rows(y).Cells("Column20").ReadOnly = False
        '                                dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
        '                                dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
        '                            End If
        '                            If dtBusqueda.Rows(y).Item(20) = "02" Then
        '                                dgvGastos.Rows(y).Cells("Column20").Value = True
        '                                dgvGastos.Rows(y).Cells("Column20").ReadOnly = True
        '                                dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
        '                                dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
        '                            End If
        '                            dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                        ElseIf z = 16 Then
        '                            dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                            dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                            'Else
        '                        ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        End If
        '                    Next
        '                    dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
        '                Next
        '            End If
        '        End If

        '        If rdbRecibidos.Checked = True Then
        '            If dtBusqueda.Rows.Count > 0 Then
        '                For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '                    dgvGastos.Rows.Add()
        '                    gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
        '                    For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                        If z = 9 Then
        '                            If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                                gestionaResaltados(dgvGastos, y, Color.Orange)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 10 Then
        '                            If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                                gestionaResaltados2(dgvGastos, y, Color.Orange)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 0 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                        ElseIf z = 15 Then
        '                            'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                            dgvGastos.Rows(y).Cells(15).Value = True
        '                            'End If
        '                            dgvGastos.Rows(y).Cells(15).ReadOnly = False
        '                        ElseIf z = 17 Then
        '                            'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                            dgvGastos.Rows(y).Cells(17).Value = True
        '                            'End If
        '                            dgvGastos.Rows(y).Cells(z).ReadOnly = True
        '                        ElseIf z = 18 Then
        '                            dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                            dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                            'ElseIf z = 19 Then
        '                            '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
        '                            '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                        ElseIf z = 19 Then
        '                            If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                                dgvGastos.Rows(y).Cells(21).Value = ""
        '                            Else
        '                                dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                                'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                                '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                                'End If
        '                            End If
        '                            dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                        ElseIf z = 20 Then
        '                            If dtBusqueda.Rows(y).Item(20) = "02" Then
        '                                dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
        '                                dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
        '                            End If
        '                            dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                        ElseIf z = 16 Then
        '                            dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                            dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                            'Else
        '                        ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        End If
        '                    Next
        '                    dgvGastos.Rows(y).Cells(15).Value = True
        '                    dgvGastos.Rows(y).Cells(15).ReadOnly = False

        '                    dgvGastos.Rows(y).Cells(17).Value = True
        '                    dgvGastos.Rows(y).Cells(17).ReadOnly = True

        '                    dgvGastos.Rows(y).Cells(19).Value = True
        '                    dgvGastos.Rows(y).Cells(19).ReadOnly = True

        '                    'dgvGastos.Rows(y).Cells(21).Value = ""
        '                    'dgvGastos.Rows(y).Cells(21).ReadOnly = True
        '                Next
        '            End If
        '        End If

        '        If rdbRecibidosxRendir.Checked Then
        '            If dtBusqueda.Rows.Count > 0 Then
        '                For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '                    dgvGastos.Rows.Add()
        '                    'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
        '                    For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                        If z = 9 Then
        '                            If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                                gestionaResaltados(dgvGastos, y, Color.Orange)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 10 Then
        '                            If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                                gestionaResaltados2(dgvGastos, y, Color.Orange)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 0 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                        ElseIf z = 15 Then
        '                            'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                            dgvGastos.Rows(y).Cells(15).Value = True
        '                            'End If
        '                            dgvGastos.Rows(y).Cells(15).ReadOnly = False
        '                        ElseIf z = 17 Then
        '                            'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                            dgvGastos.Rows(y).Cells(17).Value = True
        '                            'End If
        '                            dgvGastos.Rows(y).Cells(z).ReadOnly = True
        '                        ElseIf z = 18 Then
        '                            dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                            dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                            'ElseIf z = 19 Then
        '                            '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
        '                            '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                        ElseIf z = 19 Then
        '                            If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                                dgvGastos.Rows(y).Cells(21).Value = ""
        '                            Else
        '                                dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                                'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                                '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                                'End If
        '                            End If
        '                            dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                        ElseIf z = 20 Then
        '                            If dtBusqueda.Rows(y).Item(20) = "01" Then
        '                                dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
        '                                dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
        '                            End If
        '                            dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                        ElseIf z = 16 Then
        '                            dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                            dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                            'Else
        '                        ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        End If
        '                    Next
        '                    dgvGastos.Rows(y).Cells(15).Value = True
        '                    dgvGastos.Rows(y).Cells(15).ReadOnly = False

        '                    dgvGastos.Rows(y).Cells(17).Value = False
        '                    dgvGastos.Rows(y).Cells(17).ReadOnly = False

        '                    dgvGastos.Rows(y).Cells(19).Value = True
        '                    dgvGastos.Rows(y).Cells(19).ReadOnly = True

        '                    'dgvGastos.Rows(y).Cells(21).Value = ""
        '                    'dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                Next
        '            End If
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked Then
        '            If dtBusqueda.Rows.Count > 0 Then
        '                For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '                    dgvGastos.Rows.Add()
        '                    'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
        '                    For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                        If z = 9 Then
        '                            If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                                gestionaResaltados(dgvGastos, y, Color.Orange)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 10 Then
        '                            If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                                gestionaResaltados2(dgvGastos, y, Color.Orange)
        '                            End If
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        ElseIf z = 0 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)

        '                        ElseIf z = 15 Then
        '                            'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                            dgvGastos.Rows(y).Cells(15).Value = True
        '                            'End If
        '                            dgvGastos.Rows(y).Cells(15).ReadOnly = False
        '                        ElseIf z = 17 Then
        '                            'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                            dgvGastos.Rows(y).Cells(17).Value = True
        '                            'End If
        '                            dgvGastos.Rows(y).Cells(z).ReadOnly = True
        '                        ElseIf z = 18 Then
        '                            'dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                            'dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                            'ElseIf z = 19 Then
        '                            '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
        '                            '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                        ElseIf z = 19 Then
        '                            If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                                dgvGastos.Rows(y).Cells(21).Value = ""
        '                            Else
        '                                dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                                'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                                '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                                'End If
        '                            End If
        '                            dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                        ElseIf z = 20 Then
        '                            If dtBusqueda.Rows(y).Item(20) = "01" Then
        '                                dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
        '                                dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
        '                            End If
        '                            dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                        ElseIf z = 16 Then
        '                            'dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                            'dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                            'Else
        '                        ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                            dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                        End If
        '                    Next
        '                    dgvGastos.Rows(y).Cells(15).Value = False
        '                    dgvGastos.Rows(y).Cells(15).ReadOnly = False

        '                    dgvGastos.Rows(y).Cells(17).Value = False
        '                    dgvGastos.Rows(y).Cells(17).ReadOnly = True

        '                    dgvGastos.Rows(y).Cells(19).Value = False
        '                    dgvGastos.Rows(y).Cells(19).ReadOnly = True

        '                    'dgvGastos.Rows(y).Cells(21).Value = ""
        '                    'dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                Next
        '            End If
        '        End If

        '        dtBusqueda.Dispose()

        '        Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString
        'End Select

    End Sub

    Private Sub txtTexto_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtTexto.MouseMove
        'Timer1.Enabled = True
    End Sub

    Private Sub txtTexto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTexto.TextChanged

        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If

        If cboCriterio.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCriterio.Focus()
            Exit Sub
        End If

        Dim dtBusqueda As DataTable
        eBusquedaGastos = New clsBusquedaGastos
        dtBusqueda = New DataTable

        If Len(txtTexto.Text) > 0 Then
            If cboCriterio.Text = "CONCEPTO" Then
                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
                End If

                If rdbRecibidos.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(21, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbRecibidosxRendir.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(39, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbPorRecibirdeAdministracion.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(1, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
                End If

            ElseIf cboCriterio.Text = "RESPONSABLE" Then

                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
                End If


                If rdbRecibidos.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(22, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If
                If rdbRecibidosxRendir.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(40, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbPorRecibirdeAdministracion.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
                End If

            ElseIf cboCriterio.Text = "CHEQUE" Then

                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
                End If

                If rdbRecibidos.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(23, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If
                If rdbRecibidosxRendir.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(41, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbPorRecibirdeAdministracion.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(3, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
                End If


            ElseIf cboCriterio.Text = "MACRO" Then

                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
                End If

                If rdbRecibidos.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(24, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If
                If rdbRecibidosxRendir.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(42, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbPorRecibirdeAdministracion.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(4, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
                End If

            ElseIf cboCriterio.Text = "CARTA" Then

                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
                End If


                If rdbRecibidos.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(25, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If
                If rdbRecibidosxRendir.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(43, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbPorRecibirdeAdministracion.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(5, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
                End If

            ElseIf cboCriterio.Text = "TRANSFERENCIA" Then

                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
                End If

                If rdbRecibidos.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(26, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If
                If rdbRecibidosxRendir.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(44, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbPorRecibirdeAdministracion.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(6, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
                End If

            ElseIf cboCriterio.Text = "OTROS" Then

                If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
                End If

                If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
                    'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
                End If

                If rdbRecibidos.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(27, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If
                If rdbRecibidosxRendir.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos3(45, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
                End If

                If rdbPorRecibirdeAdministracion.Checked = True Then
                    dtBusqueda = eBusquedaGastos.fConsultaGastos2(7, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
                End If

            End If
        ElseIf Len(txtTexto.Text) = 0 Then
            dtpFechaFin_ValueChanged(sender, e)
            Exit Sub
        End If

        'For x As Integer = 0 To dgvGastos.RowCount - 1
        '    dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
        'Next

        If rdbPorRecibir.Checked = True Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            'dgvGastos_DoubleClick(sender, e)

            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub

            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Silver)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = False
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "01" Then
            '                    dgvGastos.Rows(y).Cells("Column20").Value = False
            '                    dgvGastos.Rows(y).Cells("Column20").ReadOnly = False
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
            '                End If
            '                If dtBusqueda.Rows(y).Item(20) = "02" Then
            '                    dgvGastos.Rows(y).Cells("Column20").Value = True
            '                    dgvGastos.Rows(y).Cells("Column20").ReadOnly = True
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
            '    Next
            'End If
        End If

        If rdbRecibidos.Checked = True Then
            dgvGastos.DataSource = dtBusqueda
            'Pintar()
            Pintar2()
            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub

            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(15).ReadOnly = False
            '            ElseIf z = 17 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(17).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = True
            '            ElseIf z = 18 Then
            '                dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '                'ElseIf z = 19 Then
            '                '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
            '                '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "02" Then
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(15).Value = True
            '        dgvGastos.Rows(y).Cells(15).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(17).Value = True
            '        dgvGastos.Rows(y).Cells(17).ReadOnly = True

            '        dgvGastos.Rows(y).Cells(19).Value = True
            '        dgvGastos.Rows(y).Cells(19).ReadOnly = True

            '        'dgvGastos.Rows(y).Cells(21).Value = ""
            '        'dgvGastos.Rows(y).Cells(21).ReadOnly = True
            '    Next
            'End If
        End If

        If rdbRecibidosxRendir.Checked Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            'dgvGastos_DoubleClick(sender, e)

            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub

            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(15).ReadOnly = False
            '            ElseIf z = 17 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(17).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = True
            '            ElseIf z = 18 Then
            '                dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '                'ElseIf z = 19 Then
            '                '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
            '                '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "01" Then
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(15).Value = True
            '        dgvGastos.Rows(y).Cells(15).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(17).Value = False
            '        dgvGastos.Rows(y).Cells(17).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(19).Value = True
            '        dgvGastos.Rows(y).Cells(19).ReadOnly = True

            '        'dgvGastos.Rows(y).Cells(21).Value = ""
            '        'dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '    Next
            'End If
        End If

        If rdbPorRecibirdeAdministracion.Checked Then
            dgvGastos.DataSource = dtBusqueda
            Pintar()
            'dgvGastos_DoubleClick(sender, e)

            Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count
            dtBusqueda.Dispose()
            Exit Sub

            'If dtBusqueda.Rows.Count > 0 Then
            '    For y As Integer = 0 To dtBusqueda.Rows.Count - 1
            '        dgvGastos.Rows.Add()
            '        'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
            '        For z As Integer = 0 To dtBusqueda.Columns.Count - 1
            '            If z = 9 Then
            '                If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
            '                    gestionaResaltados(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 10 Then
            '                If dtBusqueda.Rows(y).Item(z) > "1" Then
            '                    gestionaResaltados2(dgvGastos, y, Color.Orange)
            '                End If
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            ElseIf z = 0 Then
            '                dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)

            '            ElseIf z = 15 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(15).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(15).ReadOnly = False
            '            ElseIf z = 17 Then
            '                'If dtBusqueda.Rows(y).Item(15) = "1" Then
            '                dgvGastos.Rows(y).Cells(17).Value = True
            '                'End If
            '                dgvGastos.Rows(y).Cells(z).ReadOnly = True
            '            ElseIf z = 18 Then
            '                'dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                'dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '                'ElseIf z = 19 Then
            '                '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
            '                '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
            '            ElseIf z = 19 Then
            '                If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
            '                    dgvGastos.Rows(y).Cells(21).Value = ""
            '                Else
            '                    dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
            '                    'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
            '                    '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
            '                    'End If
            '                End If
            '                dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '            ElseIf z = 20 Then
            '                If dtBusqueda.Rows(y).Item(20) = "01" Then
            '                    dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
            '                    dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
            '                End If
            '                dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
            '            ElseIf z = 16 Then
            '                'dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
            '                'dgvGastos.Rows(y).Cells(16).ReadOnly = True
            '                'Else
            '            ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
            '                dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
            '            End If
            '        Next
            '        dgvGastos.Rows(y).Cells(15).Value = False
            '        dgvGastos.Rows(y).Cells(15).ReadOnly = False

            '        dgvGastos.Rows(y).Cells(17).Value = False
            '        dgvGastos.Rows(y).Cells(17).ReadOnly = True

            '        dgvGastos.Rows(y).Cells(19).Value = False
            '        dgvGastos.Rows(y).Cells(19).ReadOnly = True

            '        'dgvGastos.Rows(y).Cells(21).Value = ""
            '        'dgvGastos.Rows(y).Cells(21).ReadOnly = False
            '    Next
            'End If
        End If

        'dtBusqueda.Dispose()

        'Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString



        'If Len(Trim(txtTexto.Text)) = 0 Then




        'If dtpFechaFin.Value < dtpFechaIni.Value Then
        '    MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    dtpFechaIni.Focus()
        '    Exit Sub
        'End If

        'If cboCriterio.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione un Criterio de Consulta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    cboCriterio.Focus()
        '    Exit Sub
        'End If

        'Dim dtBusqueda As DataTable
        'eBusquedaGastos = New clsBusquedaGastos
        'dtBusqueda = New DataTable

        'If Len(txtTexto.Text) > 0 Then
        '    If cboCriterio.Text = "CONCEPTO" Then
        '        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(13, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '        End If

        '        If rdbRecibidos.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(21, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbRecibidosxRendir.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(39, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos2(1, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '        End If

        '    ElseIf cboCriterio.Text = "RESPONSABLE" Then

        '        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(14, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '        End If


        '        If rdbRecibidos.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(22, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If
        '        If rdbRecibidosxRendir.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(40, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos2(2, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '        End If

        '    ElseIf cboCriterio.Text = "CHEQUE" Then

        '        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(15, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '        End If

        '        If rdbRecibidos.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(23, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If
        '        If rdbRecibidosxRendir.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(41, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos2(3, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '        End If


        '    ElseIf cboCriterio.Text = "MACRO" Then

        '        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(16, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '        End If

        '        If rdbRecibidos.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(24, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If
        '        If rdbRecibidosxRendir.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(42, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos2(4, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '        End If

        '    ElseIf cboCriterio.Text = "CARTA" Then

        '        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(17, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '        End If


        '        If rdbRecibidos.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(25, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If
        '        If rdbRecibidosxRendir.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(43, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos2(5, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '        End If

        '    ElseIf cboCriterio.Text = "TRANSFERENCIA" Then

        '        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(18, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '        End If

        '        If rdbRecibidos.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(26, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If
        '        If rdbRecibidosxRendir.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(44, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos2(6, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '        End If

        '    ElseIf cboCriterio.Text = "OTROS" Then

        '        If (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = True) Or (rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = False) Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = True And chkPorRendir.Checked = False Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "02", "02")
        '        End If

        '        If rdbPorRecibir.Checked = True And traer = "SI" And chkRendidos.Checked = False And chkPorRendir.Checked = True Then
        '            'dtBusqueda = eBusquedaGastos.fConsultaGastos2(0, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, "")
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(19, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 0, "01", "01")
        '        End If

        '        If rdbRecibidos.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(27, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If
        '        If rdbRecibidosxRendir.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos3(45, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 1, 1, "", "")
        '        End If

        '        If rdbPorRecibirdeAdministracion.Checked = True Then
        '            dtBusqueda = eBusquedaGastos.fConsultaGastos2(7, txtTexto.Text.Trim, gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "", 0)
        '        End If

        '    End If
        'ElseIf Len(txtTexto.Text) = 0 Then
        'dtpFechaFin_ValueChanged(sender, e)
        '    Exit Sub
        'End If

        'For x As Integer = 0 To dgvGastos.RowCount - 1
        '    dgvGastos.Rows.Remove(dgvGastos.CurrentRow)
        'Next

        'If rdbPorRecibir.Checked = True Then
        '    If dtBusqueda.Rows.Count > 0 Then
        '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '            dgvGastos.Rows.Add()
        '            For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                If z = 9 Then
        '                    If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                        gestionaResaltados(dgvGastos, y, Color.Orange)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 10 Then
        '                    If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                        gestionaResaltados2(dgvGastos, y, Color.Silver)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 0 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                ElseIf z = 15 Then
        '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                    dgvGastos.Rows(y).Cells(15).Value = True
        '                    'End If
        '                    dgvGastos.Rows(y).Cells(z).ReadOnly = False
        '                ElseIf z = 19 Then
        '                    If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                        dgvGastos.Rows(y).Cells(21).Value = ""
        '                    Else
        '                        dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                        'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                        '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                        'End If
        '                    End If
        '                    dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                ElseIf z = 20 Then
        '                    If dtBusqueda.Rows(y).Item(20) = "01" Then
        '                        dgvGastos.Rows(y).Cells("Column20").Value = False
        '                        dgvGastos.Rows(y).Cells("Column20").ReadOnly = False
        '                        dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
        '                        dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
        '                    End If
        '                    If dtBusqueda.Rows(y).Item(20) = "02" Then
        '                        dgvGastos.Rows(y).Cells("Column20").Value = True
        '                        dgvGastos.Rows(y).Cells("Column20").ReadOnly = True
        '                        dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
        '                        dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
        '                    End If
        '                    dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                ElseIf z = 16 Then
        '                    dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                    dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                    'Else
        '                ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                End If
        '            Next
        '            dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(Now.Date(), 10)
        '        Next
        '    End If
        'End If

        'If rdbRecibidos.Checked = True Then
        '    If dtBusqueda.Rows.Count > 0 Then
        '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '            dgvGastos.Rows.Add()
        '            gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
        '            For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                If z = 9 Then
        '                    If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                        gestionaResaltados(dgvGastos, y, Color.Orange)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 10 Then
        '                    If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                        gestionaResaltados2(dgvGastos, y, Color.Orange)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 0 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                ElseIf z = 15 Then
        '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                    dgvGastos.Rows(y).Cells(15).Value = True
        '                    'End If
        '                    dgvGastos.Rows(y).Cells(15).ReadOnly = False
        '                ElseIf z = 17 Then
        '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                    dgvGastos.Rows(y).Cells(17).Value = True
        '                    'End If
        '                    dgvGastos.Rows(y).Cells(z).ReadOnly = True
        '                ElseIf z = 18 Then
        '                    dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                    'ElseIf z = 19 Then
        '                    '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
        '                    '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                ElseIf z = 19 Then
        '                    If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                        dgvGastos.Rows(y).Cells(21).Value = ""
        '                    Else
        '                        dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                        'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                        '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                        'End If
        '                    End If
        '                    dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                ElseIf z = 20 Then
        '                    If dtBusqueda.Rows(y).Item(20) = "02" Then
        '                        dgvGastos.Rows(y).Cells("Column19").Value = "Rendido"
        '                        dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Red
        '                    End If
        '                    dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                ElseIf z = 16 Then
        '                    dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                    dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                    'Else
        '                ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                End If
        '            Next
        '            dgvGastos.Rows(y).Cells(15).Value = True
        '            dgvGastos.Rows(y).Cells(15).ReadOnly = False

        '            dgvGastos.Rows(y).Cells(17).Value = True
        '            dgvGastos.Rows(y).Cells(17).ReadOnly = True

        '            dgvGastos.Rows(y).Cells(19).Value = True
        '            dgvGastos.Rows(y).Cells(19).ReadOnly = True

        '            'dgvGastos.Rows(y).Cells(21).Value = ""
        '            'dgvGastos.Rows(y).Cells(21).ReadOnly = True
        '        Next
        '    End If
        'End If

        'If rdbRecibidosxRendir.Checked Then
        '    If dtBusqueda.Rows.Count > 0 Then
        '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '            dgvGastos.Rows.Add()
        '            'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
        '            For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                If z = 9 Then
        '                    If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                        gestionaResaltados(dgvGastos, y, Color.Orange)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 10 Then
        '                    If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                        gestionaResaltados2(dgvGastos, y, Color.Orange)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 0 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                ElseIf z = 15 Then
        '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                    dgvGastos.Rows(y).Cells(15).Value = True
        '                    'End If
        '                    dgvGastos.Rows(y).Cells(15).ReadOnly = False
        '                ElseIf z = 17 Then
        '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                    dgvGastos.Rows(y).Cells(17).Value = True
        '                    'End If
        '                    dgvGastos.Rows(y).Cells(z).ReadOnly = True
        '                ElseIf z = 18 Then
        '                    dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                    'ElseIf z = 19 Then
        '                    '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
        '                    '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                ElseIf z = 19 Then
        '                    If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                        dgvGastos.Rows(y).Cells(21).Value = ""
        '                    Else
        '                        dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                        'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                        '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                        'End If
        '                    End If
        '                    dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                ElseIf z = 20 Then
        '                    If dtBusqueda.Rows(y).Item(20) = "01" Then
        '                        dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
        '                        dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
        '                    End If
        '                    dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                ElseIf z = 16 Then
        '                    dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                    dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                    'Else
        '                ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                End If
        '            Next
        '            dgvGastos.Rows(y).Cells(15).Value = True
        '            dgvGastos.Rows(y).Cells(15).ReadOnly = False

        '            dgvGastos.Rows(y).Cells(17).Value = False
        '            dgvGastos.Rows(y).Cells(17).ReadOnly = False

        '            dgvGastos.Rows(y).Cells(19).Value = True
        '            dgvGastos.Rows(y).Cells(19).ReadOnly = True

        '            'dgvGastos.Rows(y).Cells(21).Value = ""
        '            'dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '        Next
        '    End If
        'End If

        'If rdbPorRecibirdeAdministracion.Checked Then
        '    If dtBusqueda.Rows.Count > 0 Then
        '        For y As Integer = 0 To dtBusqueda.Rows.Count - 1
        '            dgvGastos.Rows.Add()
        '            'gestionaResaltados3(dgvGastos, y, Color.LimeGreen)
        '            For z As Integer = 0 To dtBusqueda.Columns.Count - 1
        '                If z = 9 Then
        '                    If dtBusqueda.Rows(y).Item(z) = "1" Or dtBusqueda.Rows(y).Item(z) = "2" Then
        '                        gestionaResaltados(dgvGastos, y, Color.Orange)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 10 Then
        '                    If dtBusqueda.Rows(y).Item(z) > "1" Then
        '                        gestionaResaltados2(dgvGastos, y, Color.Orange)
        '                    End If
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                ElseIf z = 0 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)

        '                ElseIf z = 15 Then
        '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                    dgvGastos.Rows(y).Cells(15).Value = True
        '                    'End If
        '                    dgvGastos.Rows(y).Cells(15).ReadOnly = False
        '                ElseIf z = 17 Then
        '                    'If dtBusqueda.Rows(y).Item(15) = "1" Then
        '                    dgvGastos.Rows(y).Cells(17).Value = True
        '                    'End If
        '                    dgvGastos.Rows(y).Cells(z).ReadOnly = True
        '                ElseIf z = 18 Then
        '                    'dgvGastos.Rows(y).Cells(20).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                    'dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                    'ElseIf z = 19 Then
        '                    '    dgvGastos.Rows(y).Cells(20).Value = dtBusqueda.Rows(y).Item(z)
        '                    '    dgvGastos.Rows(y).Cells(20).ReadOnly = True
        '                ElseIf z = 19 Then
        '                    If Microsoft.VisualBasic.IsDBNull(dtBusqueda.Rows(y).Item(z)) = True Then
        '                        dgvGastos.Rows(y).Cells(21).Value = ""
        '                    Else
        '                        dgvGastos.Rows(y).Cells(21).Value = Trim(dtBusqueda.Rows(y).Item(z))
        '                        'If Len(Trim(dtBusqueda.Rows(y).Item(z))) > 0 Then
        '                        '    'gestionaResaltados3(dgvGastos, y, Color.LightCoral)
        '                        'End If
        '                    End If
        '                    dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '                ElseIf z = 20 Then
        '                    If dtBusqueda.Rows(y).Item(20) = "01" Then
        '                        dgvGastos.Rows(y).Cells("Column19").Value = "A Rendir"
        '                        dgvGastos.Rows(y).Cells("Column19").Style.ForeColor = Color.Blue
        '                    End If
        '                    dgvGastos.Rows(y).Cells("Column19").ReadOnly = True
        '                ElseIf z = 16 Then
        '                    'dgvGastos.Rows(y).Cells(16).Value = Microsoft.VisualBasic.Left(dtBusqueda.Rows(y).Item(z), 10)
        '                    'dgvGastos.Rows(y).Cells(16).ReadOnly = True
        '                    'Else
        '                ElseIf z = 1 Or z = 2 Or z = 3 Or z = 4 Or z = 5 Or z = 6 Or z = 7 Or z = 8 Or z = 11 Or z = 12 Or z = 13 Or z = 14 Then
        '                    dgvGastos.Rows(y).Cells(z).Value = dtBusqueda.Rows(y).Item(z)
        '                End If
        '            Next
        '            dgvGastos.Rows(y).Cells(15).Value = False
        '            dgvGastos.Rows(y).Cells(15).ReadOnly = False

        '            dgvGastos.Rows(y).Cells(17).Value = False
        '            dgvGastos.Rows(y).Cells(17).ReadOnly = True

        '            dgvGastos.Rows(y).Cells(19).Value = False
        '            dgvGastos.Rows(y).Cells(19).ReadOnly = True

        '            'dgvGastos.Rows(y).Cells(21).Value = ""
        '            'dgvGastos.Rows(y).Cells(21).ReadOnly = False
        '        Next
        '    End If
        'End If

        'dtBusqueda.Dispose()

        'Label5.Text = "Total de Registros : " & dgvGastos.Rows.Count.ToString
        'End If
    End Sub

    Private Sub dgvGastos_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvGastos.CellEndEdit
        Dim StrCad As String = String.Empty
        If e.ColumnIndex = 20 Then
            StrCad = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells(e.ColumnIndex).Value
            If StrCad = Nothing Then Exit Sub
            Try
                Dim FechaIng As DateTime
                FechaIng = Convert.ToDateTime(StrCad)
                'If FechaIng = Nothing Then

                'End If
            Catch ex As Exception
                MessageBox.Show("Ingrese una Fecha Valida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvGastos.CurrentCell = dgvGastos(20, dgvGastos.CurrentRow.Index)
                dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells(e.ColumnIndex).Value = ""
                'dgvGastos.CurrentCell = dgvGastos(16, dgvGastos.CurrentRow.Index)
                'dgvGastos.CurrentCell = dgvGastos(16, dgvGastos.CurrentRow.Index - 1)
                'dgvGastos.BeginEdit(True)
                'dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells(e.ColumnIndex).Value = ""
            End Try
        End If
    End Sub

    Private Sub dgvGastos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvGastos.Click
        If rdbPorRecibir.Checked = True And AplicarPintado = 1 Then
            Pintar()
            AplicarPintado = AplicarPintado + 1
        End If
    End Sub

    'Private Sub dgvGastos_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvGastos.DoubleClick
    '    Pintar()
    'End Sub

    Private Sub dgvGastos_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvGastos.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvGastos.CurrentCell.ColumnIndex
        'If columna = 6 Then
        '    Dim caracter As Char = e.KeyChar
        '    ' referencia a la celda   
        '    Dim txt As TextBox = CType(sender, TextBox)
        '    ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
        '    ' es el separador decimal, y que no contiene ya el separador   
        '    If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
        '        e.Handled = False
        '    Else
        '        e.Handled = True
        '    End If
        'End If

        'If columna = 2 Then
        '    'Obtener caracter
        '    Dim caracter As Char = e.KeyChar
        '    'Comprobar si el caracter es un n�mero o el retroceso   
        '    If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter = ChrW(Keys.Space)) = False And (caracter.ToString <> "-") Then
        '        'Me.Text = e.KeyChar
        '        e.KeyChar = Chr(0)
        '    End If
        'End If

        If columna = 20 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
                e.KeyChar = Chr(0)
            End If
        End If

    End Sub

    Private Sub dgvGastos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvGastos.KeyDown
        Select Case e.KeyCode
            Case Keys.F2
                If dgvGastos.Rows.Count > 0 Then
                    Dim sIdMovimiento As String = ""
                    Dim sFormaDet As String = ""
                    Dim xEmpresaAPrestar As String = ""
                    Dim dblMonto As Double = 0
                    sIdMovimiento = IIf(Microsoft.VisualBasic.IsDBNull(dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value), "", dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("Column9").Value)
                    If Trim(sIdMovimiento) <> "" Then
                        Dim frm As New frmDetalleMovimiento2
                        frm.Owner = Me
                        frm.sIdMovimiento = Trim(sIdMovimiento)
                        'frm.Monto = Trim(dblMonto)
                        frm.ShowInTaskbar = False
                        frm.ShowDialog()
                    Else
                        MessageBox.Show("No Existe Detalle", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    MessageBox.Show("Seleccione un Registro de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
        End Select

    End Sub

    Private Sub rdbRecibidosxRendir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbRecibidosxRendir.CheckedChanged
        btnEnviar.Enabled = True
        CheckBox1.Enabled = True
        CheckBox1.Checked = False

        chkRendidos.Checked = False
        chkRendidos.Enabled = False

        chkPorRendir.Checked = False
        chkPorRendir.Enabled = False
        txtTexto.Clear()
        'rdbPorRecibir.Checked = False
        'rdbRecibidos.Checked = False
        'rdbRecibidosxRendir.Checked = True
        'rdbPorRecibirdeAdministracion.Checked = False

        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If

        Dim x As frmReporteDocRecibidos = frmReporteDocRecibidos
        x.MdiParent = frmPrincipal
        x.envOpcion = 1
        x.envTexto = Trim(txtTexto.Text)
        x.envFIni = dtpFechaIni.Value
        x.envFFin = dtpFechaFin.Value
        x.envCampoOrd = ""
        x.envAscDesc = ""
        x.Show()
    End Sub

    Private eFunciones As Funciones

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        'Me.dgDatos.DataSource = dt
        Dim fichero As String = ""


        'SaveFileDialog1.Filter = "Excel(*.xls)|*.xls|"

        SaveFileDialog1.InitialDirectory = "C:\"
        SaveFileDialog1.Title = "Guardar Archivo"
        SaveFileDialog1.Filter = "Excel (*.xls) |*.xls;*.rtf|(*.txt) |*.txt|(*.*) |*.*"
        'SaveFileDialog1.Filter = "Excel (*.xlsx) |*.xlsx;*.rtf|(*.txt) |*.txt|(*.*) |*.*"

        'SaveFileDialog1.InitialDirectory = "C:\Transito\fotos\" xlsx

        If SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            fichero = SaveFileDialog1.FileName
            Try
                'Dim iExp As New Funciones
                'Dim iExp As New Funciones
                eFunciones = New Funciones

                Dim dtBusquedaX As DataTable
                dtBusquedaX = New DataTable
                dtBusquedaX = Funciones.ToTable(dgvGastos, "Jesus")
                'eFunciones.DataTableToExcel(CType(Me.dgvGastos.DataSource, DataTable))
                'eFunciones.DataTableToExcel(dtBusqueda, fichero)
                eFunciones.DataTableToExcel(dtBusquedaX, fichero)

                'Dim miDataTableModificado As DataTable
                'miDataTableModificado = New DataTable
                'miDataTableModificado = CType(dgvGastos.DataSource, DataTable)
                'eFunciones.DataTableToExcel(miDataTableModificado, fichero)


                dtBusquedaX.Dispose()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            'Else
        End If

        

        '' Set the content type to Excel
        'Response.ContentType = "application/vnd.ms-excel"

        ''Turn off the view state
        'Me.EnableViewState = False

        ''Remove the charset from the Content-Type header
        'Response.Charset = String.Empty

        'Dim myTextWriter As New System.IO.StringWriter()
        'Dim myHtmlTextWriter As New System.Web.UI.HtmlTextWriter(myTextWriter)

        ''Get the HTML for the control
        'myDataGrid.RenderControl(myHtmlTextWriter)

        ''Write the HTML to the browser
        'Response.Write(myTextWriter.ToString())

        ''End the response
        'Response.End()
    End Sub

    Private Sub rdbPorRecibirdeAdministracion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPorRecibirdeAdministracion.CheckedChanged
        btnEnviar.Enabled = False
        CheckBox1.Enabled = False
        CheckBox1.Checked = False

        chkRendidos.Checked = False
        chkRendidos.Enabled = False

        chkPorRendir.Checked = False
        chkPorRendir.Enabled = False
        txtTexto.Clear()
        'rdbPorRecibir.Checked = False
        'rdbRecibidos.Checked = False
        'rdbRecibidosxRendir.Checked = False
        'rdbPorRecibirdeAdministracion.Checked = True

        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub chkRendidos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRendidos.CheckedChanged
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub chkPorRendir_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPorRendir.CheckedChanged
        dtpFechaFin_ValueChanged(sender, e)
    End Sub

    Private Sub cboCriterio_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCriterio.SelectedIndexChanged

    End Sub

    Private Sub tipExample1_Popup(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PopupEventArgs) Handles tipExample1.Popup

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Me.txtDescripcion.Focus()
        Me.tipExample1.SetToolTip(Me.txtTexto, "Seleccione e ingrese el criterio de b�squeda y presione Enter")
        Timer1.Enabled = False
    End Sub
End Class