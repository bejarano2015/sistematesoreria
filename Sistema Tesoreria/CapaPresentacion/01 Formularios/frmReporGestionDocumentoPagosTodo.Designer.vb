<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporGestionDocumentoPagosTodo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim RadListDataItem6 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem7 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem8 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem9 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem10 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Me.rvReporte = New Telerik.ReportViewer.WinForms.ReportViewer()
        Me.radGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.cboDocumento = New Telerik.WinControls.UI.RadDropDownList()
        Me.dtFechaInicio = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.radLabel6 = New Telerik.WinControls.UI.RadLabel()
        Me.dtFechaFin = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.radLabel8 = New Telerik.WinControls.UI.RadLabel()
        Me.btnGenerar = New Telerik.WinControls.UI.RadButton()
        CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.radGroupBox1.SuspendLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDocumento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFechaInicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFechaFin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGenerar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'rvReporte
        '
        Me.rvReporte.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rvReporte.Location = New System.Drawing.Point(12, 56)
        Me.rvReporte.Name = "rvReporte"
        Me.rvReporte.Size = New System.Drawing.Size(887, 529)
        Me.rvReporte.TabIndex = 0
        '
        'radGroupBox1
        '
        Me.radGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.radGroupBox1.Controls.Add(Me.RadLabel1)
        Me.radGroupBox1.Controls.Add(Me.cboDocumento)
        Me.radGroupBox1.Controls.Add(Me.dtFechaInicio)
        Me.radGroupBox1.Controls.Add(Me.radLabel6)
        Me.radGroupBox1.Controls.Add(Me.dtFechaFin)
        Me.radGroupBox1.Controls.Add(Me.radLabel8)
        Me.radGroupBox1.HeaderText = ""
        Me.radGroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.radGroupBox1.Name = "radGroupBox1"
        Me.radGroupBox1.Size = New System.Drawing.Size(482, 38)
        Me.radGroupBox1.TabIndex = 40
        '
        'RadLabel1
        '
        Me.RadLabel1.Location = New System.Drawing.Point(5, 10)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(65, 18)
        Me.RadLabel1.TabIndex = 181
        Me.RadLabel1.Text = "Documento"
        '
        'cboDocumento
        '
        Me.cboDocumento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        RadListDataItem6.Text = "ORDEN DE COMPRA"
        RadListDataItem6.TextWrap = True
        RadListDataItem7.Text = "ENTREGA A RENDIR"
        RadListDataItem7.TextWrap = True
        RadListDataItem8.Text = "VALORIZACIÓN"
        RadListDataItem8.TextWrap = True
        RadListDataItem9.Text = "CAJA CHICA"
        RadListDataItem9.TextWrap = True
        RadListDataItem10.Text = "PLANILLAS"
        RadListDataItem10.TextWrap = True
        Me.cboDocumento.Items.Add(RadListDataItem6)
        Me.cboDocumento.Items.Add(RadListDataItem7)
        Me.cboDocumento.Items.Add(RadListDataItem8)
        Me.cboDocumento.Items.Add(RadListDataItem9)
        Me.cboDocumento.Items.Add(RadListDataItem10)
        Me.cboDocumento.Location = New System.Drawing.Point(76, 10)
        Me.cboDocumento.Name = "cboDocumento"
        Me.cboDocumento.Size = New System.Drawing.Size(141, 20)
        Me.cboDocumento.TabIndex = 180
        '
        'dtFechaInicio
        '
        Me.dtFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaInicio.Location = New System.Drawing.Point(262, 10)
        Me.dtFechaInicio.Name = "dtFechaInicio"
        Me.dtFechaInicio.Size = New System.Drawing.Size(94, 20)
        Me.dtFechaInicio.TabIndex = 31
        Me.dtFechaInicio.TabStop = False
        Me.dtFechaInicio.Text = "20/08/2014"
        Me.dtFechaInicio.Value = New Date(2014, 8, 20, 17, 32, 6, 60)
        '
        'radLabel6
        '
        Me.radLabel6.Location = New System.Drawing.Point(359, 8)
        Me.radLabel6.Name = "radLabel6"
        Me.radLabel6.Size = New System.Drawing.Size(15, 18)
        Me.radLabel6.TabIndex = 34
        Me.radLabel6.Text = "al"
        '
        'dtFechaFin
        '
        Me.dtFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaFin.Location = New System.Drawing.Point(375, 10)
        Me.dtFechaFin.Name = "dtFechaFin"
        Me.dtFechaFin.Size = New System.Drawing.Size(94, 20)
        Me.dtFechaFin.TabIndex = 32
        Me.dtFechaFin.TabStop = False
        Me.dtFechaFin.Text = "20/08/2014"
        Me.dtFechaFin.Value = New Date(2014, 8, 20, 17, 32, 6, 60)
        '
        'radLabel8
        '
        Me.radLabel8.Location = New System.Drawing.Point(233, 8)
        Me.radLabel8.Name = "radLabel8"
        Me.radLabel8.Size = New System.Drawing.Size(23, 18)
        Me.radLabel8.TabIndex = 33
        Me.radLabel8.Text = "Del"
        '
        'btnGenerar
        '
        Me.btnGenerar.Location = New System.Drawing.Point(500, 20)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(88, 25)
        Me.btnGenerar.TabIndex = 41
        Me.btnGenerar.Text = "Generar"
        '
        'frmReporGestionDocumentoPagosTodo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(911, 597)
        Me.Controls.Add(Me.btnGenerar)
        Me.Controls.Add(Me.radGroupBox1)
        Me.Controls.Add(Me.rvReporte)
        Me.Name = "frmReporGestionDocumentoPagosTodo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte de Gestion de Documentos - Pagos"
        CType(Me.radGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.radGroupBox1.ResumeLayout(False)
        Me.radGroupBox1.PerformLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDocumento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFechaInicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFechaFin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radLabel8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGenerar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents rvReporte As Telerik.ReportViewer.WinForms.ReportViewer
    Private WithEvents radGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Private WithEvents dtFechaInicio As Telerik.WinControls.UI.RadDateTimePicker
    Private WithEvents radLabel6 As Telerik.WinControls.UI.RadLabel
    Private WithEvents dtFechaFin As Telerik.WinControls.UI.RadDateTimePicker
    Private WithEvents radLabel8 As Telerik.WinControls.UI.RadLabel
    Private WithEvents btnGenerar As Telerik.WinControls.UI.RadButton
    Private WithEvents cboDocumento As Telerik.WinControls.UI.RadDropDownList
    Private WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
End Class
