<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteEstadoDocumentosCCosto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chk3 = New System.Windows.Forms.RadioButton
        Me.rdbCC = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.cboCentroCosto = New ctrLibreria.Controles.BeComboBox
        Me.rdbCancelados = New System.Windows.Forms.RadioButton
        Me.rdbCancelar = New System.Windows.Forms.RadioButton
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.chk4 = New System.Windows.Forms.RadioButton
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chk4)
        Me.GroupBox2.Controls.Add(Me.chk3)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 149)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(368, 66)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cuenta Corriente de Proveedores "
        '
        'chk3
        '
        Me.chk3.AutoSize = True
        Me.chk3.Location = New System.Drawing.Point(22, 19)
        Me.chk3.Name = "chk3"
        Me.chk3.Size = New System.Drawing.Size(235, 17)
        Me.chk3.TabIndex = 6
        Me.chk3.TabStop = True
        Me.chk3.Text = "Deudas (Total - Vencidos) X Empresa Actual"
        Me.chk3.UseVisualStyleBackColor = True
        '
        'rdbCC
        '
        Me.rdbCC.AutoSize = True
        Me.rdbCC.Location = New System.Drawing.Point(22, 96)
        Me.rdbCC.Name = "rdbCC"
        Me.rdbCC.Size = New System.Drawing.Size(271, 17)
        Me.rdbCC.TabIndex = 5
        Me.rdbCC.TabStop = True
        Me.rdbCC.Text = "Cuenta Corriente de Proveedores X Centro de Costo"
        Me.rdbCC.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(19, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Centro de Costo"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(288, 221)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(92, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Ver"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'cboCentroCosto
        '
        Me.cboCentroCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCentroCosto.BackColor = System.Drawing.Color.Ivory
        Me.cboCentroCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroCosto.ForeColor = System.Drawing.Color.Black
        Me.cboCentroCosto.FormattingEnabled = True
        Me.cboCentroCosto.KeyEnter = True
        Me.cboCentroCosto.Location = New System.Drawing.Point(108, 19)
        Me.cboCentroCosto.Name = "cboCentroCosto"
        Me.cboCentroCosto.Size = New System.Drawing.Size(254, 21)
        Me.cboCentroCosto.TabIndex = 2
        '
        'rdbCancelados
        '
        Me.rdbCancelados.AutoSize = True
        Me.rdbCancelados.Location = New System.Drawing.Point(22, 73)
        Me.rdbCancelados.Name = "rdbCancelados"
        Me.rdbCancelados.Size = New System.Drawing.Size(81, 17)
        Me.rdbCancelados.TabIndex = 1
        Me.rdbCancelados.TabStop = True
        Me.rdbCancelados.Text = "Cancelados"
        Me.rdbCancelados.UseVisualStyleBackColor = True
        '
        'rdbCancelar
        '
        Me.rdbCancelar.AutoSize = True
        Me.rdbCancelar.Location = New System.Drawing.Point(22, 50)
        Me.rdbCancelar.Name = "rdbCancelar"
        Me.rdbCancelar.Size = New System.Drawing.Size(86, 17)
        Me.rdbCancelar.TabIndex = 0
        Me.rdbCancelar.TabStop = True
        Me.rdbCancelar.Text = "Por Cancelar"
        Me.rdbCancelar.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'chk4
        '
        Me.chk4.AutoSize = True
        Me.chk4.Location = New System.Drawing.Point(22, 42)
        Me.chk4.Name = "chk4"
        Me.chk4.Size = New System.Drawing.Size(247, 17)
        Me.chk4.TabIndex = 9
        Me.chk4.TabStop = True
        Me.chk4.Text = "Deudas (Total - Vencidos) X Grupo Empresarial"
        Me.chk4.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboCentroCosto)
        Me.GroupBox3.Controls.Add(Me.rdbCancelar)
        Me.GroupBox3.Controls.Add(Me.rdbCC)
        Me.GroupBox3.Controls.Add(Me.rdbCancelados)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(368, 131)
        Me.GroupBox3.TabIndex = 9
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Cuenta Corriente de Proveedores X Centro de Costo"
        '
        'frmReporteEstadoDocumentosCCosto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(393, 254)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Button1)
        Me.MaximizeBox = False
        Me.Name = "frmReporteEstadoDocumentosCCosto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Estado de Comprobantes Por Centro de Costo"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cboCentroCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents rdbCancelados As System.Windows.Forms.RadioButton
    Friend WithEvents rdbCancelar As System.Windows.Forms.RadioButton
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents rdbCC As System.Windows.Forms.RadioButton
    Friend WithEvents chk3 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chk4 As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
End Class
