Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmParametrosRetencion

    'Private eCaja As clsCaja
    'Private eFiltro As clsPlantFiltro
    'Private eTempo As clsPlantTempo
    'Private eLogin As clsLogin
    'Private eUsuario As Usuarios
    'Dim dtTable As DataTable
    'Private eBusquedaRetenciones As clsBusquedaRetenciones
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Dim OpcionGrabar As Integer = 0

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmParametrosRetencion = Nothing
    Public Shared Function Instance() As frmParametrosRetencion
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmParametrosRetencion
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmParametrosRetencion_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmParametrosRetencion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mMostrarGrilla()
    End Sub

    Private Sub mMostrarGrilla()
        'eCaja = New clsCaja
        'eTempo = New clsPlantTempo
        Dim dtTable As DataTable
        dtTable = New DataTable
        eMovimientoCajaBanco = New clsMovimientoCajaBanco
        dtTable = eMovimientoCajaBanco.fListarParametrosRetencion(5)

        If dtTable.Rows.Count > 0 Then
            'dgvListado.Rows(y).Cells("Column3").Value = dtTable.Rows(y).Item("IdMovimiento").ToString
            txtPor.Text = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("Porcentaje").ToString) = True, "0.00", dtTable.Rows(0).Item("Porcentaje").ToString)
            txtImp.Text = IIf(Microsoft.VisualBasic.IsDBNull(dtTable.Rows(0).Item("ImporteParaRet").ToString) = True, "0.00", dtTable.Rows(0).Item("ImporteParaRet").ToString) 'dtTable.Rows(y).Item("FechaMovimiento").ToString
            OpcionGrabar = 7
        ElseIf dtTable.Rows.Count = 0 Then
            txtPor.Text = "0.00"
            txtImp.Text = "0.00"
            OpcionGrabar = 6
        End If
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click


        'If Len(Trim(txtPor.Text)) > 0 And Len(Trim(txtImp.Text)) > 0 Then
        If Len(Trim(txtPor.Text)) = 0 Then
            MessageBox.Show("Ingrese el Porcentaje", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtPor.Focus()
            Exit Sub
        End If
        If Len(Trim(txtPor.Text)) = 0 Then
            MessageBox.Show("Ingrese el Importe para la Retenci�n", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtPor.Focus()
            Exit Sub
        End If
        'End If

        Dim msj As String = ""
        If OpcionGrabar = 6 Then
            msj = "Grabar"
        ElseIf OpcionGrabar = 7 Then
            msj = "Actualizar"
        End If

        If (MessageBox.Show("�Esta seguro de " & msj & "?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            Dim iGrabarRetencion As Integer = 0
            iGrabarRetencion = eMovimientoCajaBanco.fGrabarRetencion(OpcionGrabar, "", "", "", "", Today(), "", "", "", "", 0, Convert.ToDouble(txtPor.Text), Convert.ToDouble(txtImp.Text), 0, 0, 0, gUsuario, "")
            If iGrabarRetencion > 0 Then
                PorcentajeRetencion = Convert.ToDouble(txtPor.Text)
                MontoRetencion = Convert.ToDouble(txtImp.Text)
                mMostrarGrilla()
            End If
        End If
    End Sub

    Private Sub txtPor_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPor.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtPor.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtPor.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

    Private Sub txtImp_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtImp.LostFocus
        Dim MontoEscrito As String = ""
        MontoEscrito = txtImp.Text.Trim
        If MontoEscrito.Trim = "" Then
            MontoEscrito = 0
        End If
        txtImp.Text = Format(Convert.ToDouble(MontoEscrito), "#,##0.00")
    End Sub

End Class