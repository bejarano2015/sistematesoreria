Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmCuentasxPagar

    Private ePagares As clsPagares
    Private eRegistroDocumento As clsRegistroDocumento
    Dim iOpcion As Integer

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCuentasxPagar = Nothing
    Public Shared Function Instance() As frmCuentasxPagar
        If frmInstance Is Nothing Then
            frmInstance = New frmCuentasxPagar
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmCuentasxPagar_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmCuentasxPagar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eRegistroDocumento = New clsRegistroDocumento
        cboMoneda.DataSource = eRegistroDocumento.fListarTipMoneda2()
        If eRegistroDocumento.iNroRegistros > 0 Then
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = -1
        End If

        Dim dtDocumentos As DataTable
        dtDocumentos = New DataTable
        ePagares = New clsPagares
        dtDocumentos = ePagares.fListarPagares(gEmpresa)
        dgvDocumentos.AutoGenerateColumns = False
        dgvDocumentos.DataSource = dtDocumentos
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Limpiar()
        ePagares = New clsPagares
        ePagares.fCodigo(gEmpresa)
        txtCodigo.Text = (ePagares.sCodFuturo)
        iOpcion = 1
    End Sub

    Private Sub Limpiar()
        txtCodigo.Clear()
        cboTipoPagare.SelectedIndex = -1
        txtBeneficiario.Clear()
        txtNumero.Clear()
        txtConcepto.Clear()
        cboMoneda.SelectedIndex = -1
        txtImporte.Clear()
        dtpFechaVcto.Value = Now.Date
        txtTEA.Clear()
        txtInteres.Clear()
    End Sub

    Private Sub Habilitar()
        cboTipoPagare.Enabled = True
        txtBeneficiario.Enabled = True
        txtNumero.Enabled = True
        txtConcepto.Enabled = True
        cboMoneda.Enabled = True
        txtImporte.Enabled = True
        dtpFechaVcto.Enabled = True
        txtTEA.Enabled = True
        txtInteres.Enabled = True
    End Sub

    Private Sub Desabilitar()
        cboTipoPagare.Enabled = False
        txtBeneficiario.Enabled = False
        txtNumero.Enabled = False
        txtConcepto.Enabled = False
        cboMoneda.Enabled = False
        txtImporte.Enabled = False
        dtpFechaVcto.Enabled = False
        txtTEA.Enabled = False
        txtInteres.Enabled = False
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim Mensaje As String = ""
        If iOpcion = 1 Then
            Mensaje = "Grabar"
        ElseIf iOpcion = 2 Then
            Mensaje = "Actualizar"
        End If

        Dim TipoPagare As Integer
        If cboTipoPagare.Text = "PAGARES" Then
            TipoPagare = 1
        ElseIf cboTipoPagare.Text = "FINANCIAMIENTO" Then
            TipoPagare = 2
        End If

        Dim sCodigoRegistro As String = ""
        Dim iResultado As Integer = 0

        If Len(Trim(txtCodigo.Text)) > 0 And cboTipoPagare.SelectedIndex <> -1 And Len(Trim(txtBeneficiario.Text)) > 0 And Len(Trim(txtNumero.Text)) > 0 And Len(Trim(txtConcepto.Text)) > 0 And cboMoneda.SelectedIndex <> -1 And Len(Trim(txtImporte.Text)) > 0 And Len(Trim(txtTEA.Text)) > 0 And Len(Trim(txtInteres.Text)) > 0 Then
            If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                frmPrincipal.sFlagGrabar = "1"
                If iOpcion = 1 Then
                    ePagares = New clsPagares
                    ePagares.fCodigo(gEmpresa)
                    sCodigoRegistro = ePagares.sCodFuturo
                    txtCodigo.Text = Trim(sCodigoRegistro)
                    iResultado = ePagares.fGrabar(iOpcion, sCodigoRegistro, gEmpresa, TipoPagare, Trim(txtBeneficiario.Text), Trim(txtNumero.Text), Trim(txtConcepto.Text), Trim(cboMoneda.SelectedValue), Trim(txtImporte.Text), dtpFechaVcto.Value, Trim(txtTEA.Text), Trim(txtInteres.Text))
                    If iResultado > 0 Then
                        'mMostrarGrilla()
                        'Timer2.Enabled = True
                    End If
                ElseIf iOpcion = 2 Then
                    sCodigoRegistro = Trim(txtCodigo.Text)
                    iResultado = ePagares.fGrabar(iOpcion, sCodigoRegistro, gEmpresa, TipoPagare, Trim(txtBeneficiario.Text), Trim(txtNumero.Text), Trim(txtConcepto.Text), Trim(cboMoneda.SelectedValue), Trim(txtImporte.Text), dtpFechaVcto.Value, Trim(txtTEA.Text), Trim(txtInteres.Text))
                    If iResultado > 0 Then
                        'mMostrarGrilla()
                    End If
                End If
            End If
        Else
            If Len(Trim(txtBeneficiario.Text)) = 0 Then
            End If
            If Len(Trim(txtConcepto.Text)) = 0 Then
            End If
            If Len(Trim(txtConcepto.Text)) = 0 Then
            End If
        End If
    End Sub
End Class