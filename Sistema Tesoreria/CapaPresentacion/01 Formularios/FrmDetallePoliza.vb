Imports CapaNegocios
Imports System.Data
Public Class FrmDetallePoliza
    Private cSegurosPolizas As clsSegurosPolizas
    Friend FrmMantPoliza As New frmMantePoliza
    Friend FrmCronograma As New frmCronogramaPoliza
    Dim DT_Endoso As DataTable
    Dim DT_Endosos As DataTable
    Dim DT_TipoPoliza As DataTable
    Dim DT_Banco As DataTable
    Dim DT_FormaPago As DataTable
    Dim DT_CCosto As DataTable
    Dim dgrow As New DataGridViewComboBoxCell
    Dim DtPoliza As DataTable
    Dim DtDetPoliza As DataTable
    Dim Dset As New DataSet

    Public CodPoliza As String = ""
    Public Ramo As String = ""
    Public Aseguradora As String = ""

    Dim sPorcDeducible As String
    Dim sMinDeducible As String

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As FrmDetallePoliza = Nothing

    Public Shared Function Instance() As FrmDetallePoliza
        If frmInstance Is Nothing Then
            frmInstance = New FrmDetallePoliza
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub FrmDetallePoliza_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub FrmDetallePoliza_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub FrmDetallePoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cSegurosPolizas = New clsSegurosPolizas
        DtPoliza = New DataTable
        DT_Endoso = New DataTable
        DT_Banco = New DataTable
        DT_FormaPago = New DataTable
        DT_CCosto = New DataTable
        DT_TipoPoliza = New DataTable
        DT_Endosos = New DataTable

        If FrmMantPoliza.DgvLista.Rows.Count > 0 Then
            txtNroPoliza.Text = Trim(CodPoliza) 'FrmMantPoliza.DgvLista.Rows(FrmMantPoliza.DgvLista.CurrentRow.Index).Cells("Poliza").Value
            'CodPoliza = FrmMantPoliza.DgvLista.Rows(FrmMantPoliza.DgvLista.CurrentRow.Index).Cells("Poliza").Value
            txtRamo.Text = Trim(Ramo) 'FrmMantPoliza.DgvLista.Rows(FrmMantPoliza.DgvLista.CurrentRow.Index).Cells("Ramo").Value
            txtAseguradora.Text = Trim(Aseguradora) 'FrmMantPoliza.DgvLista.Rows(FrmMantPoliza.DgvLista.CurrentRow.Index).Cells("Aseguradora").Value
        Else
            txtNroPoliza.Text = FrmCronograma.txtPoliza.Text
            txtRamo.Text = FrmCronograma.txtDesRamo.Text
            txtAseguradora.Text = FrmCronograma.txtAseguradora.Text
        End If

        Dset = cSegurosPolizas.fMostrarPoliza(gEmpresa, txtNroPoliza.Text)
        DtPoliza = Dset.Tables(0)
        DtDetPoliza = Dset.Tables(1)
        DT_Endosos = Dset.Tables(2)

        Endoso.Items.Clear()
        TipoPoliza.Items.Clear()
        NumeroPoliza.Items.Clear()
        'DT_Endoso = FrmMantPoliza.DS_Datos.Tables(4)
        DT_Endoso = cSegurosPolizas.fCargaDatos.Tables(4)
        DT_TipoPoliza = cSegurosPolizas.fCargaDatos.Tables(5)
        'DT_TipoPoliza = cSegurosPolizas.fCargaDatos.Tables(6)
        DT_Banco = cSegurosPolizas.ListarBancos(gEmpresa, cSegurosPolizas.CodigoMoneda(DtPoliza.Rows(0).Item("MonCodigo")))
        'DT_Banco = cSegurosPolizas.ListarBancos("01", "01")
        DT_FormaPago = cSegurosPolizas.fCargaDatos.Tables(2)
        DT_CCosto = cSegurosPolizas.fListarCentrosCostos(gEmpresa)
        'dgrow.DataSource = FrmMantPoliza.DS_Datos.Tables(4)
        'dgrow.DisplayMember = "Descripcion"
        'dgrow.ValueMember = "Codigo"

        For i As Integer = 0 To DT_Endoso.Rows.Count - 1
            'dgvDetallePoliza.Item("Endoso", i) = dgrow
            Endoso.Items.Add(Trim(DT_Endoso.Rows(i).Item(0).ToString) & " - " & DT_Endoso.Rows(i).Item(1).ToString)
        Next

        For i As Integer = 0 To DT_Endosos.Rows.Count - 1
            'dgvDetallePoliza.Item("Endoso", i) = dgrow
            NumeroPoliza.Items.Add(Trim(DT_Endosos.Rows(i).Item(0).ToString))
        Next

        NumeroPoliza.Items.Add(Trim(CodPoliza))

        For i As Integer = 0 To DT_TipoPoliza.Rows.Count - 1
            'dgvDetallePoliza.Item("Endoso", i) = dgrow
            TipoPoliza.Items.Add(Trim(DT_TipoPoliza.Rows(i).Item(0).ToString) & " - " & DT_TipoPoliza.Rows(i).Item(1).ToString)
        Next

        For y As Integer = 0 To DT_Banco.Rows.Count - 1
            'dgvDetallePoliza.Item("Endoso", i) = dgrow
            Banco.Items.Add(Trim(DT_Banco.Rows(y).Item(0).ToString) & " - " & DT_Banco.Rows(y).Item(1).ToString)
        Next

        For z As Integer = 0 To DT_FormaPago.Rows.Count - 1
            'dgvDetallePoliza.Item("Endoso", i) = dgrow
            FormaPago.Items.Add(Trim(DT_FormaPago.Rows(z).Item(0).ToString) & " - " & DT_FormaPago.Rows(z).Item(1).ToString)
        Next

        For x As Integer = 0 To DT_CCosto.Rows.Count - 1
            CenCosto.Items.Add(Trim(DT_CCosto.Rows(x).Item(0).ToString) & " - " & DT_CCosto.Rows(x).Item(1).ToString)
        Next

        CenCosto.Width = 150


        If DtDetPoliza.Rows.Count > 0 Then
            'dgvDetallePoliza.DataSource = DtDetPoliza
            For iFilas As Integer = 0 To DtDetPoliza.Rows.Count - 1
                dgvDetallePoliza.Rows.Add()
                dgvDetallePoliza.Rows(iFilas).Cells("TipoPoliza").Value = DtDetPoliza.Rows(iFilas).Item("TipoPoli")
                dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value = DtDetPoliza.Rows(iFilas).Item("CodigoSubPoliza")

                'If DtDetPoliza.Rows(iFilas).Item("CodigoSubPoliza").Value = "02" Then
                'dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value = ""
                'End If

                If IIf(Microsoft.VisualBasic.IsDBNull(DtDetPoliza.Rows(iFilas).Item("CodigoTipoPoliza")) = True, "", DtDetPoliza.Rows(iFilas).Item("CodigoTipoPoliza")) = "01" Then
                    'dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value = ""
                    dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").ReadOnly = True
                End If

                dgvDetallePoliza.Rows(iFilas).Cells("Endoso").Value = DtDetPoliza.Rows(iFilas).Item("CodigoEndoso")
                dgvDetallePoliza.Rows(iFilas).Cells("Banco").Value = DtDetPoliza.Rows(iFilas).Item("IdBanco")
                dgvDetallePoliza.Rows(iFilas).Cells("Fecha_Leasing").Value = DtDetPoliza.Rows(iFilas).Item("FechaLeasing")
                dgvDetallePoliza.Rows(iFilas).Cells("Asegurado").Value = DtDetPoliza.Rows(iFilas).Item("DescripcionAsegurado")
                dgvDetallePoliza.Rows(iFilas).Cells("CenCosto").Value = DtDetPoliza.Rows(iFilas).Item("CenCosto")
                dgvDetallePoliza.Rows(iFilas).Cells("SumaAsegurada").Value = Format(DtDetPoliza.Rows(iFilas).Item("SumaAsegurada"), "#,##0.00")
                dgvDetallePoliza.Rows(iFilas).Cells("Tasa").Value = DtDetPoliza.Rows(iFilas).Item("Tasa")
                dgvDetallePoliza.Rows(iFilas).Cells("PrimaNeta").Value = Format(DtDetPoliza.Rows(iFilas).Item("Prima"), "#,##0.00")
                dgvDetallePoliza.Rows(iFilas).Cells("PrimaGastos").Value = Format(DtDetPoliza.Rows(iFilas).Item("Total"), "#,##0.00")
                dgvDetallePoliza.Rows(iFilas).Cells("FormaPago").Value = DtDetPoliza.Rows(iFilas).Item("CodCondCancelacion")
                dgvDetallePoliza.Rows(iFilas).Cells("PorcDeducible").Value = DtDetPoliza.Rows(iFilas).Item("Porcentaje_Deducible")
                dgvDetallePoliza.Rows(iFilas).Cells("MinimoDeducible").Value = DtDetPoliza.Rows(iFilas).Item("Minimo_Deducible")


            Next

        Else
            dgvDetallePoliza.Rows.Add()
            dgvDetallePoliza.Rows(0).Cells("TipoPoliza").Value = ""
            dgvDetallePoliza.Rows(0).Cells("NumeroPoliza").Value = ""
            dgvDetallePoliza.Rows(0).Cells("Endoso").Value = ""
            dgvDetallePoliza.Rows(0).Cells("Banco").Value = ""
            dgvDetallePoliza.Rows(0).Cells("Fecha_Leasing").Value = ""
            dgvDetallePoliza.Rows(0).Cells("Asegurado").Value = ""
            dgvDetallePoliza.Rows(0).Cells("CenCosto").Value = ""
            dgvDetallePoliza.Rows(0).Cells("SumaAsegurada").Value = 0
            dgvDetallePoliza.Rows(0).Cells("Tasa").Value = ""
            dgvDetallePoliza.Rows(0).Cells("PrimaNeta").Value = 0
            dgvDetallePoliza.Rows(0).Cells("PrimaGastos").Value = 0
            dgvDetallePoliza.Rows(0).Cells("FormaPago").Value = ""
            dgvDetallePoliza.Rows(0).Cells("PorcDeducible").Value = ""
            dgvDetallePoliza.Rows(0).Cells("MinimoDeducible").Value = ""
        End If

        dgvDetallePoliza.CurrentCell = dgvDetallePoliza(0, dgvDetallePoliza.Rows.Count - 1)
        'dgvDetallePoliza.Focus()

    End Sub

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click
        Me.Close()
    End Sub

    Private Sub dgvDetallePoliza_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetallePoliza.CellEndEdit
        Dim columna As Integer = dgvDetallePoliza.CurrentCell.ColumnIndex
        If columna = 0 Then 'si el check prestamo es INACTIVADO la empresa prestada se blanquea
            If dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("TipoPoliza").Value = "01 - Poliza" Then
                dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("NumeroPoliza").Value = Trim(CodPoliza)
                dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("NumeroPoliza").ReadOnly = True
            ElseIf dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("TipoPoliza").Value = "02 - Endoso" Then
                dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("NumeroPoliza").Value = ""
                dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("NumeroPoliza").ReadOnly = False
            End If
        ElseIf columna = 1 Then 'si el check prestamo es INACTIVADO la empresa prestada se blanquea
            If dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("TipoPoliza").Value = "02 - Endoso" Then
                If Trim(dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("NumeroPoliza").Value) = Trim(txtNroPoliza.Text) Then
                    MessageBox.Show("Seleccione otro Numero de Endoso", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("NumeroPoliza").Value = ""
                End If
            End If

        ElseIf columna = 2 Then 'si el check propiedad es cambiado
            'If dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("TipoPoliza").Value = "02 - Endoso" Then
            'If Trim(dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("NumeroPoliza").Value) = Trim(txtNroPoliza.Text) Then
            'MessageBox.Show("Seleccione otro Numero de Endoso", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells("Banco").Value = ""
            'End If
        End If
        'End If
    End Sub

    Private Sub dgvDetallePoliza_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetallePoliza.CellValueChanged
        If e.RowIndex <> -1 Then
            If e.ColumnIndex = 2 Then
                'If dgvDetallePoliza.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString = "01 - Leasing" Then
                '    dgvDetallePoliza.Rows(e.RowIndex).Cells("Banco").ReadOnly = False
                '    dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").ReadOnly = False
                '    'MsgBox("es Leasing")
                'Else
                '    dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value = ""
                '    dgvDetallePoliza.Rows(e.RowIndex).Cells("Banco").Value = ""
                '    dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").ReadOnly = True
                '    dgvDetallePoliza.Rows(e.RowIndex).Cells("Banco").ReadOnly = True
                '    'MsgBox("es Propio")
                'End If

                'dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value = ""
                'dgvDetallePoliza.Rows(e.RowIndex).Cells("Banco").Value = ""
                dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").ReadOnly = False
                dgvDetallePoliza.Rows(e.RowIndex).Cells("Banco").ReadOnly = False

            ElseIf e.ColumnIndex = 4 Then
                If Len(dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value) > 0 Then
                    Dim sTexto1 As String = dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value.ToString
                    dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value = UCase(sTexto1)

                End If
                'Dim sTexto1 As String = dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value.ToString
                'dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value = UCase(sTexto1)


                'If dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value.ToString.Length > 0 Then
                '    If Not dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value.ToString.Contains("/") Or Not dgvDetallePoliza.Rows(e.RowIndex).Cells("Fecha_Leasing").Value.ToString.Contains("-") Then
                '        MessageBox.Show("Ingrese correctamente el Rango para la Fecha de Leasing", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    End If
                'End If

            ElseIf e.ColumnIndex = 5 Then
                'Dim sTexto As String = dgvDetallePoliza.Rows(e.RowIndex).Cells("Asegurado").Value.ToString
                'dgvDetallePoliza.Rows(e.RowIndex).Cells("Asegurado").Value = UCase(sTexto)

                If Len(dgvDetallePoliza.Rows(e.RowIndex).Cells("Asegurado").Value) > 0 Then
                    Dim sTexto As String = dgvDetallePoliza.Rows(e.RowIndex).Cells("Asegurado").Value.ToString
                    dgvDetallePoliza.Rows(e.RowIndex).Cells("Asegurado").Value = UCase(sTexto)
                End If

                dgvDetallePoliza.Rows(e.RowIndex).Cells("PrimaNeta").Value = Format(Math.Round(DtPoliza.Rows(0).Item("Prima"), 2), "#,##0.00")
                dgvDetallePoliza.Rows(e.RowIndex).Cells("PrimaGastos").Value = Format(Math.Round(DtPoliza.Rows(0).Item("Total"), 2), "#,##0.00")
                dgvDetallePoliza.Columns("PrimaNeta").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                dgvDetallePoliza.Columns("PrimaGastos").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            ElseIf e.ColumnIndex = 7 Then
                Dim dMonto As Double = dgvDetallePoliza.Rows(e.RowIndex).Cells("SumaAsegurada").Value
                dgvDetallePoliza.Rows(e.RowIndex).Cells("SumaAsegurada").Value = Format(dMonto, "#,##0.00")
                dgvDetallePoliza.Columns("SumaAsegurada").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                'dgvDetallePoliza.Rows(e.RowIndex).Cells("Importe").Value = Format(Math.Round(dgvDetallePoliza.Rows(e.RowIndex).Cells("PrimaGastos").Value / dgvDetallePoliza.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, 2), "#,##0.00")

            ElseIf e.ColumnIndex = 9 Then
                Dim dMonto As Double = dgvDetallePoliza.Rows(e.RowIndex).Cells("PrimaNeta").Value
                dgvDetallePoliza.Rows(e.RowIndex).Cells("PrimaNeta").Value = Format(dMonto, "#,##0.00")
                dgvDetallePoliza.Columns("PrimaNeta").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            ElseIf e.ColumnIndex = 10 Then
                Dim dMonto As Double = dgvDetallePoliza.Rows(e.RowIndex).Cells("PrimaGastos").Value
                dgvDetallePoliza.Rows(e.RowIndex).Cells("PrimaGastos").Value = Format(dMonto, "#,##0.00")
                dgvDetallePoliza.Columns("PrimaGastos").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


            ElseIf e.ColumnIndex = 12 Then
                ' sPorcDeducible = dgvDetallePoliza.Rows(e.RowIndex).Cells("PorcDeducible").Value.ToString
                'dgvDetallePoliza.Rows(e.RowIndex).Cells("PorcDeducible").Value = sPorcDeducible 'Format(sPorcDeducible, "#,##0.00")
                'dgvDetallePoliza.Columns("PorcDeducible").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                If Len(dgvDetallePoliza.Rows(e.RowIndex).Cells("PorcDeducible").Value) > 0 Then
                    sPorcDeducible = dgvDetallePoliza.Rows(e.RowIndex).Cells("PorcDeducible").Value.ToString
                    dgvDetallePoliza.Rows(e.RowIndex).Cells("PorcDeducible").Value = sPorcDeducible 'Format(sPorcDeducible, "#,##0.00")
                    dgvDetallePoliza.Columns("PorcDeducible").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                End If
            ElseIf e.ColumnIndex = 13 Then
                'sMinDeducible = dgvDetallePoliza.Rows(e.RowIndex).Cells("MinimoDeducible").Value.ToString
                'dgvDetallePoliza.Rows(e.RowIndex).Cells("MinimoDeducible").Value = sMinDeducible 'Format(sMinDeducible, "#,##0.00")
                'dgvDetallePoliza.Columns("MinimoDeducible").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                If Len(dgvDetallePoliza.Rows(e.RowIndex).Cells("MinimoDeducible").Value) > 0 Then
                    sMinDeducible = dgvDetallePoliza.Rows(e.RowIndex).Cells("MinimoDeducible").Value.ToString
                    dgvDetallePoliza.Rows(e.RowIndex).Cells("MinimoDeducible").Value = sMinDeducible 'Format(sMinDeducible, "#,##0.00")
                    dgvDetallePoliza.Columns("MinimoDeducible").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                End If
                'dgvDetallePoliza.Rows.Add()
                'dgvDetallePoliza.CurrentCell = dgvDetallePoliza(0, dgvDetallePoliza.Rows.Count - 1)
            End If
        End If

    End Sub

    Private Sub dgvDetallePoliza_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvDetallePoliza.DataError
        Try

        Catch ex As Exception

        End Try
    End Sub
    Private Sub dgvDetallePoliza_EditingControlShowing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetallePoliza.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub
    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvDetallePoliza.CurrentCell.ColumnIndex
        If columna = 10 Or columna = 9 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                'If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

        'If columna = 9 Then
        '    'Obtener caracter
        '    Dim caracter As Char = e.KeyChar
        '    'Comprobar si el caracter es un n�mero o el retroceso   
        '    If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter = ChrW(Keys.Space)) = False And (caracter.ToString <> "-") Then
        '        'Me.Text = e.KeyChar
        '        e.KeyChar = Chr(0)
        '    End If
        'End If

        'If columna = 0 Then
        '    'Obtener caracter
        '    Dim caracter As Char = e.KeyChar
        '    'Comprobar si el caracter es un n�mero o el retroceso   
        '    If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
        '        e.KeyChar = Chr(0)
        '    End If
        'End If

        'If columna = 1 Then

        '    Dim caracter As Char = e.KeyChar
        '    ' referencia a la celda   
        '    Dim txt As TextBox = CType(sender, TextBox)
        '    ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
        '    ' es el separador decimal, y que no contiene ya el separador
        '    If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Then
        '        e.Handled = False
        '    Else
        '        e.Handled = True
        '    End If


        'If (Char.IsNumber(caracter)) Or (Len(Trim(xRuc))) <= 11 Then
        '    xRuc = xRuc & caracter
        '    e.Handled = False
        'ElseIf caracter = ChrW(Keys.Back) Then
        '    e.Handled = False
        'Else
        '    e.Handled = True
        'End If

        'End If

    End Sub
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim bGrabar As Boolean
        cSegurosPolizas = New clsSegurosPolizas

        If MessageBox.Show("�Desea Grabar?", "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            cSegurosPolizas.fEliminaDetallePoliza(gEmpresa, txtNroPoliza.Text)
            Dim xEndoso As String = ""
            Dim xBanco As String = ""
            Dim xFecha_Leasing As String = ""
            Dim xAsegurado As String = ""
            Dim xCenCosto As String = ""
            Dim xTasa As String = ""
            Dim xFormaPago As String = ""
            Dim xSumaAsegurada As String = ""
            Dim xPorcDeducible As String = ""
            Dim xMinimoDeducible As String = ""
            Dim xTipoPoliza As String = ""
            Dim xNumeroPoliza As String = ""
            Dim xPrimaNeta As String = ""
            Dim xPrimaGastos As String = ""

            For iFilas As Integer = 0 To dgvDetallePoliza.Rows.Count - 1

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Endoso").Value)) = 0 Then
                '    xEndoso = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Endoso").Value)) > 0 Then
                '    xEndoso = Trim(dgvDetallePoliza.Rows(iFilas).Cells("Endoso").Value)
                'End If

                xEndoso = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("Endoso").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("Endoso").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Banco").Value)) = 0 Then
                '    xBanco = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Banco").Value)) > 0 Then
                '    xBanco = Trim(dgvDetallePoliza.Rows(iFilas).Cells("Banco").Value)
                'End If

                xBanco = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("Banco").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("Banco").Value)


                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Fecha_Leasing").Value)) = 0 Then
                '    xFecha_Leasing = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Fecha_Leasing").Value)) > 0 Then
                '    xFecha_Leasing = Trim(dgvDetallePoliza.Rows(iFilas).Cells("Fecha_Leasing").Value)
                'End If

                xFecha_Leasing = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("Fecha_Leasing").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("Fecha_Leasing").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Asegurado").Value)) = 0 Then
                '    xAsegurado = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Asegurado").Value)) > 0 Then
                '    xAsegurado = Trim(dgvDetallePoliza.Rows(iFilas).Cells("Asegurado").Value)
                'End If
                xAsegurado = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("Asegurado").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("Asegurado").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("CenCosto").Value)) = 0 Then
                '    xCenCosto = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("CenCosto").Value)) > 0 Then
                '    xCenCosto = Trim(dgvDetallePoliza.Rows(iFilas).Cells("CenCosto").Value)
                'End If
                xCenCosto = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("CenCosto").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("CenCosto").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Tasa").Value)) = 0 Then
                '    xTasa = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("Tasa").Value)) > 0 Then
                '    xTasa = Trim(dgvDetallePoliza.Rows(iFilas).Cells("Tasa").Value)
                'End If
                xTasa = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("Tasa").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("Tasa").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("FormaPago").Value)) = 0 Then
                '    xFormaPago = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("FormaPago").Value)) > 0 Then
                '    xFormaPago = Trim(dgvDetallePoliza.Rows(iFilas).Cells("FormaPago").Value)
                'End If

                xFormaPago = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("FormaPago").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("FormaPago").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("SumaAsegurada").Value)) = 0 Then
                '    xSumaAsegurada = 0
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("SumaAsegurada").Value)) > 0 Then
                '    xSumaAsegurada = Trim(dgvDetallePoliza.Rows(iFilas).Cells("SumaAsegurada").Value)
                'End If
                xSumaAsegurada = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("SumaAsegurada").Value) = True, 0, dgvDetallePoliza.Rows(iFilas).Cells("SumaAsegurada").Value)

                If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("PorcDeducible").Value)) = 0 Then
                    xPorcDeducible = ""
                ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("PorcDeducible").Value)) > 0 Then
                    xPorcDeducible = Trim(dgvDetallePoliza.Rows(iFilas).Cells("PorcDeducible").Value)
                End If
                'xPorcDeducible = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("PorcDeducible").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("PorcDeducible").Value)

                If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("MinimoDeducible").Value)) = 0 Then
                    xMinimoDeducible = ""
                ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("MinimoDeducible").Value)) > 0 Then
                    xMinimoDeducible = Trim(dgvDetallePoliza.Rows(iFilas).Cells("MinimoDeducible").Value)
                End If
                'xMinimoDeducible = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("MinimoDeducible").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("MinimoDeducible").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("TipoPoliza").Value)) = 0 Then
                '    xTipoPoliza = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("TipoPoliza").Value)) > 0 Then
                '    xTipoPoliza = Trim(dgvDetallePoliza.Rows(iFilas).Cells("TipoPoliza").Value)
                'End If
                xTipoPoliza = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("TipoPoliza").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("TipoPoliza").Value)

                'If Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value)) = 0 Then
                '    xNumeroPoliza = ""
                'ElseIf Len(Trim(dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value)) > 0 Then
                '    xNumeroPoliza = Trim(dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value)
                'End If
                xNumeroPoliza = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value) = True, "", dgvDetallePoliza.Rows(iFilas).Cells("NumeroPoliza").Value)


                xPrimaNeta = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("PrimaNeta").Value) = True, 0, dgvDetallePoliza.Rows(iFilas).Cells("PrimaNeta").Value)
                xPrimaGastos = IIf(Microsoft.VisualBasic.IsDBNull(dgvDetallePoliza.Rows(iFilas).Cells("PrimaGastos").Value) = True, 0, dgvDetallePoliza.Rows(iFilas).Cells("PrimaGastos").Value)

                bGrabar = cSegurosPolizas.fGrabarDetallePoliza(gEmpresa, txtNroPoliza.Text, iFilas + 1, xEndoso, xBanco, xFecha_Leasing, xAsegurado, xCenCosto, xTasa, xFormaPago, 0, xSumaAsegurada, xPorcDeducible, xMinimoDeducible, xTipoPoliza, xNumeroPoliza, xPrimaNeta, xPrimaGastos)

                If bGrabar = False Then
                    MessageBox.Show("Ocurrio un Error al Grabar Detalle en la Fila " & iFilas + 1, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            Next
            MessageBox.Show("Se Grabo el detalle correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        End If
    End Sub

    Private Sub dgvDetallePoliza_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetallePoliza.KeyDown
        Select Case e.KeyCode
            Case Keys.Delete
                If MessageBox.Show("�Desea Eliminar la Fila Actual?", "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    dgvDetallePoliza.Rows.RemoveAt(dgvDetallePoliza.CurrentRow.Index)
                End If
        End Select
    End Sub
    Private Sub dgvDetallePoliza_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetallePoliza.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                If dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(13).Selected = True Then

                    dgvDetallePoliza.Rows.Add()
                    dgvDetallePoliza.CurrentCell = dgvDetallePoliza(0, dgvDetallePoliza.Rows.Count - 1)
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(0).Value = "" 'dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(0).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(1).Value = "" 'dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(0).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(2).Value = "" 'dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(0).Value

                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(3).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(3).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(4).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(4).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(5).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(5).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(6).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(6).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(7).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(7).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(8).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(8).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(9).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(9).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(10).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(10).Value
                    dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index).Cells(11).Value = dgvDetallePoliza.Rows(dgvDetallePoliza.CurrentRow.Index - 1).Cells(11).Value

                End If
        End Select
    End Sub

    Private Sub dgvDetallePoliza_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetallePoliza.CellContentClick

    End Sub

    Private Sub dgvDetallePoliza_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetallePoliza.KeyUp

    End Sub
End Class