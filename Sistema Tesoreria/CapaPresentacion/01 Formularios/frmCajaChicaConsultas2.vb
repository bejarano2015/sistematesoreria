Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic

Public Class frmCajaChicaConsultas2

    Private eSesionCajas As clsSesionCajas
    Private eRegistroDocumento As clsRegistroDocumento
    Private eCompraRequerimientos As clsCompraRequerimientos
    'Private eSesionCajas As clsSesionCajas
    Dim dtTable As DataTable
    Dim TBL As New DataTable
    Dim Grabar As Int16 = 0

    Dim contar1 As Integer = 0
    Dim contar2 As Integer = 0
    Dim contar3 As Integer = 0

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmCajaChicaConsultas2 = Nothing
    Public Shared Function Instance() As frmCajaChicaConsultas2
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmCajaChicaConsultas2
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function


    Private Sub frmCajaChicaConsultas2_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region


    Private Sub frmCajaChicaConsultas2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            'AQUI DEBE SALIR LAS TODAS LAS CAJAS ACTIVAS Y NO ACTIVAS
            'eSesionCajas = New clsSesionCajas
            'LimpiarControles()
            eSesionCajas = New clsSesionCajas
            cboArea.DataSource = eSesionCajas.fBuscarComprob(5, gEmpresa, "", Today(), Today(), 0, 0, gPeriodo)
            'cboArea.DataSource = eSesionCajas.fListarArea(gEmpresa)
            cboArea.ValueMember = "AreaCodigo"
            cboArea.DisplayMember = "AreaNombre"
            cboArea.SelectedIndex = -1
        Catch ex As Exception
        End Try

    End Sub

    Private Sub cboArea_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboArea.SelectedIndexChanged
        Dim IdArea As String
        txtAnio.Clear()
        Try
            IdArea = cboArea.SelectedValue.ToString
            If IdArea <> "System.Data.DataRowView" Then
                'cboCaja.DataSource = eSesionCajas.fListarCajasPorArea(cboArea.SelectedValue, gEmpresa, gPeriodo)
                eSesionCajas = New clsSesionCajas
                cboCaja.DataSource = eSesionCajas.fBuscarComprob(6, gEmpresa, cboArea.SelectedValue, Today(), Today(), 0, 0, gPeriodo)

                cboCaja.ValueMember = "IdCaja"
                cboCaja.DisplayMember = "DescripcionCaja"
                cboCaja.SelectedIndex = -1
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    
    Private Sub txtAnio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAnio.TextChanged
      

        Try
            'AQUI DEBE SALIR LAS TODAS LAS CAJAS ACTIVAS Y NO ACTIVAS
            'eSesionCajas = New clsSesionCajas
            'LimpiarControles()
            eSesionCajas = New clsSesionCajas
            Dim dtCajas As DataTable
            dtCajas = New DataTable
            dtCajas = eSesionCajas.fBuscarComprob(7, gEmpresa, Trim(cboCaja.SelectedValue), Today(), Today(), 0, 0, Trim(txtAnio.Text))
            'cboArea.DataSource = eSesionCajas.fListarArea(gEmpresa)
            'cboArea.ValueMember = "AreaCodigo"
            'cboArea.DisplayMember = "AreaNombre"
            'cboArea.SelectedIndex = -1
            If dtCajas.Rows.Count > 0 Then
                Me.lstCajas.DisplayMember = "NroCaja"
                Me.lstCajas.ValueMember = "IdSesion"
                Me.lstCajas.DataSource = dtCajas
                BeLabel1.Text = dtCajas.Rows.Count & " " & "Elemento (s)"
            ElseIf dtCajas.Rows.Count = 0 Then
                Me.lstCajas.DataSource = Nothing
                BeLabel1.Text = "0 Elemento (s)"
                For x As Integer = 0 To dgvDetalleCajas.RowCount - 1
                    dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                Next
                LimpiarDatos()
            End If

        Catch ex As Exception
        End Try

    End Sub

    Private Sub lstCajas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstCajas.SelectedIndexChanged

    End Sub

    Private Sub lstCajas_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstCajas.SelectedValueChanged
        Dim value As Object = lstCajas.SelectedValue
        If (TypeOf value Is DataRowView) Then Return
        Try
            'IdArea = cboArea.SelectedValue.ToString
            If value <> Nothing Then

                
                LimpiarDatos()
                Call cargarCentroCosto()
                Call cargarTipoDocumento()
                Call cargarEmpresas()
                cargarTipoGastoss(30)
                'dgvDetalleCajas.Enabled = False
                eSesionCajas = New clsSesionCajas
                dtCabecera = eSesionCajas.fBuscarComprob(8, gEmpresa, Trim(value), Today(), Today(), 0, 0, "")
                If dtCabecera.Rows.Count > 0 Then

                    dtpFechaInicio.Value = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("FechaInicio")) = True, Now.Date().ToShortDateString(), dtCabecera.Rows(0).Item("FechaInicio"))
                    dtpFechaFin.Value = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("FechaFin")) = True, Now.Date().ToShortDateString(), dtCabecera.Rows(0).Item("FechaFin"))

                    'dtpFechaInicio.Value = dtCabecera.Rows(0).Item("FechaInicio")
                    'If Microsoft.VisualBasic.IsDBNull(dtCabecera.Rows(0).Item("FechaFin")) = True Then
                    '    dtpFechaFin.Value = Now.Date().ToShortDateString()
                    'Else
                    '    dtpFechaFin.Value = dtCabecera.Rows(0).Item("FechaFin")
                    'End If

                    'txtAporteGeneral.Text = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoInicioSoles")), "#,##0.00")
                    txtAporteGeneral.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("MontoInicioSoles")) = True, "0.00", Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoInicioSoles")), "#,##0.00"))
                    'txtTotGasto.Text = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoTotalGas")), "#,##0.00")
                    txtTotGasto.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("MontoTotalGas")) = True, "0.00", Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoTotalGas")), "#,##0.00"))
                    'txtTotSaldo.Text = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoFinSoles")), "#,##0.00")
                    txtTotSaldo.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("MontoFinSoles")) = True, "0.00", Format(Convert.ToDouble(dtCabecera.Rows(0).Item("MontoFinSoles")), "#,##0.00"))

                    'txtBanco.Text = dtCabecera.Rows(0).Item("Banco")
                    txtBanco.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("Banco")) = True, "", dtCabecera.Rows(0).Item("Banco"))

                    'txtEmpresa.Text = dtCabecera.Rows(0).Item("EmprAbreviado")
                    txtEmpresa.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("EmprAbreviado")) = True, "", dtCabecera.Rows(0).Item("EmprAbreviado"))

                    'txtFormaPago.Text = dtCabecera.Rows(0).Item("FormaPago")
                    txtFormaPago.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("FormaPago")) = True, "", dtCabecera.Rows(0).Item("FormaPago"))

                    'txtNumMov.Text = dtCabecera.Rows(0).Item("NroFormaPago")
                    txtNumMov.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("NroFormaPago")) = True, "", dtCabecera.Rows(0).Item("NroFormaPago"))

                    dtpFechaPago.Value = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("FechaMovimiento")) = True, Now.Date().ToShortDateString(), dtCabecera.Rows(0).Item("FechaMovimiento"))
                    'dtpFechaPago.Value = dtCabecera.Rows(0).Item("FechaMovimiento")

                    'txtTotPago.Text = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("Monto")), "#,##0.00")
                    'txtTotPago.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("Monto")) = True, "", Format(Convert.ToDouble(dtCabecera.Rows(0).Item("Monto")), "#,##0.00"))
                    'txtTotPago.Text = IIf(vb.IsDBNull(dtCabecera.Rows(0).Item("Monto")) = True, "0.00", Format(Convert.ToDouble(dtCabecera.Rows(0).Item("Monto")), "#,##0.00"))
                    If Microsoft.VisualBasic.IsDBNull(dtCabecera.Rows(0).Item("Monto")) = True Then
                        'Dim aa As String = ""
                        txtTotPago.Text = "0.00"
                    ElseIf Microsoft.VisualBasic.IsDBNull(dtCabecera.Rows(0).Item("Monto")) <> True Then
                        txtTotPago.Text = Format(Convert.ToDouble(dtCabecera.Rows(0).Item("Monto")), "#,##0.00")
                    End If

                End If
                dtDetalle = New DataTable
                eSesionCajas = New clsSesionCajas
                dtDetalle = eSesionCajas.fListarDetCerrado(value, gEmpresa, 0) 'txtCodigo.Text
                cargarDetalle()
            End If
        Catch ex As System.IndexOutOfRangeException
            LimpiarDatos()
            For x As Integer = 0 To dgvDetalleCajas.RowCount - 1
                dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
            Next
            Exit Sub
        End Try
    End Sub

    Private Sub LimpiarDatos()
        dtpFechaInicio.Value = Now.Date().ToShortDateString()
        dtpFechaFin.Value = Now.Date().ToShortDateString()
        txtAporteGeneral.Clear()
        txtTotGasto.Clear()
        txtTotSaldo.Clear()
        txtBanco.Clear()
        txtEmpresa.Clear()
        txtFormaPago.Clear()
        txtNumMov.Clear()
        dtpFechaPago.Value = Now.Date().ToShortDateString()
        txtTotPago.Clear()
    End Sub

    Private Sub cargarDetalle()
        Dim y As Integer
        Dim z As Integer
        If dtDetalle.Rows.Count > 0 Then

            'For x = 0 To dgvDetalleCajas.RowCount - 1
            '    dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
            'Next

            For x As Integer = 0 To dgvDetalleCajas.RowCount - 1
                dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
            Next


            For y = 0 To dtDetalle.Rows.Count - 1
                dgvDetalleCajas.Rows.Add()
                For z = 0 To dtDetalle.Columns.Count - 1
                    Dim StrCadCodigo As String = String.Empty
                    If z = 0 Then
                        'fecha
                        Me.dgvDetalleCajas.Rows(y).Cells("Fecha").Value = Microsoft.VisualBasic.Left(dtDetalle.Rows(y).Item("FechaDetMov"), 10)
                    ElseIf z = 1 Then
                        'tipo doc
                        For a As Integer = 0 To TipoDocumento.Items.Count - 1
                            Dim cad As String = TipoDocumento.Items.Item(a)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("IdDocumento")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("TipoDocumento").Value = TipoDocumento.Items.Item(a)
                            End If
                        Next
                    ElseIf z = 2 Then
                        'nro doc
                        Me.dgvDetalleCajas.Rows(y).Cells("Column3").Value = dtDetalle.Rows(y).Item("DocumentoReferencia")
                    ElseIf z = 3 Then
                        For a As Integer = 0 To IdGasto.Items.Count - 1
                            Dim cad As String = IdGasto.Items.Item(a)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("TipoGasto")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("IdGasto").Value = IdGasto.Items.Item(a)
                            End If
                        Next
                    ElseIf z = 4 Then
                        'glosa
                        Me.dgvDetalleCajas.Rows(y).Cells("Glosa").Value = dtDetalle.Rows(y).Item("Descripcion")
                    ElseIf z = 5 Then
                        For a As Integer = 0 To CentroCosto.Items.Count - 1
                            Dim cad As String = CentroCosto.Items.Item(a)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("CCosCodigo")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("CentroCosto").Value = CentroCosto.Items.Item(a)
                            End If
                        Next
                    ElseIf z = 6 Then
                        'total
                        Me.dgvDetalleCajas.Rows(y).Cells("Total").Value = dtDetalle.Rows(y).Item("Total")
                    ElseIf z = 7 Then
                        'nro doc
                        Me.dgvDetalleCajas.Rows(y).Cells("IdDetalleMovimiento").Value = dtDetalle.Rows(y).Item("IdDetalleMovimiento")
                        'ElseIf z = 8 Then
                        'Me.dgvDetalleCajas.Rows(y).Cells("Opcion").Value = "1"
                        'MessageBox.Show(dtDetalle.Rows(y).Item("IdDetalleMovimiento"))
                    ElseIf z = 9 Then
                        If Trim(dtDetalle.Rows(y).Item("NroVale").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("Anular").Value = False
                            dgvDetalleCajas.Rows(y).Cells("Desglozado").Value = dtDetalle.Rows(y).Item("NroVale").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("NroVale").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("Anular").Value = False
                            dgvDetalleCajas.Rows(y).Cells("Opcion").Value = ""
                        End If
                    ElseIf z = 10 Then
                        For w As Integer = 0 To Empresas.Items.Count - 1
                            Dim cad As String = Empresas.Items.Item(w)
                            StrCadCodigo = cad.Substring(0, cad.IndexOf(" ")).Trim
                            If Trim(dtDetalle.Rows(y).Item("EmprPrestada")) = StrCadCodigo Then
                                dgvDetalleCajas.Rows(y).Cells("Empresas").Value = Empresas.Items.Item(w)
                            End If
                        Next
                    ElseIf z = 11 Then
                        If dtDetalle.Rows(y).Item("EstadoPrestamo") = "1" Then
                            dgvDetalleCajas.Rows(y).Cells("Column9").Value = True
                        ElseIf dtDetalle.Rows(y).Item("EstadoPrestamo") = "0" Then
                            dgvDetalleCajas.Rows(y).Cells("Column9").Value = False
                        End If
                    ElseIf z = 12 Then
                        If Trim(dtDetalle.Rows(y).Item("Ruc").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("Column7").Value = dtDetalle.Rows(y).Item("Ruc").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("Ruc").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("Column7").Value = ""
                        End If
                    ElseIf z = 13 Then
                        If Trim(dtDetalle.Rows(y).Item("RazonSocial").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("Column10").Value = dtDetalle.Rows(y).Item("RazonSocial").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("RazonSocial").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("Column10").Value = ""
                        End If
                    ElseIf z = 14 Then
                        If Trim(dtDetalle.Rows(y).Item("OtrosDatos").ToString) <> "" Then
                            dgvDetalleCajas.Rows(y).Cells("OtrosD").Value = dtDetalle.Rows(y).Item("OtrosDatos").ToString
                        ElseIf Trim(dtDetalle.Rows(y).Item("OtrosDatos").ToString) = "" Then
                            dgvDetalleCajas.Rows(y).Cells("OtrosD").Value = ""
                        End If
                    End If
                    If dtDetalle.Rows(y).Item("IdDocumento") = "04" Then
                        dgvDetalleCajas.Rows(y).Cells("Anular").Value = True
                        dgvDetalleCajas.Rows(y).Cells("Anular").ReadOnly = True
                    End If
                    Me.dgvDetalleCajas.Rows(y).Cells("Opcion").Value = "1"
                Next
            Next
        End If
    End Sub


    Private Sub cargarTipoGastoss(ByVal Opcion As Integer)
        IdGasto.Items.Clear()
        eSesionCajas = New clsSesionCajas
        'eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarTipoGastoss(Opcion, gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            'Dim xxx As String = ""
            'xxx = Trim(dtTable.Rows(i).Item("IdTipoGasto")) & " - " & Trim(dtTable.Rows(i).Item("DescTGasto") & " - " & dtTable.Rows(i).Item("Descripcion").ToString.Trim)
            IdGasto.Items.Add(dtTable.Rows(i).Item("IdTipoGasto").ToString.Trim & " - " & dtTable.Rows(i).Item("DescTGasto").ToString.Trim & " - " & dtTable.Rows(i).Item("Descripcion").ToString.Trim)
        Next
    End Sub

    Private Sub cargarCentroCosto()
        CentroCosto.Items.Clear()
        eSesionCajas = New clsSesionCajas
        'eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarCCosto(gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            CentroCosto.Items.Add(Trim(dtTable.Rows(i).Item("CCosCodigo")) & " - " & dtTable.Rows(i).Item("CCosDescripcion"))
        Next
    End Sub

    Private Sub cargarTipoDocumento()
        TipoDocumento.Items.Clear()
        eSesionCajas = New clsSesionCajas
        'eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarTipoDocumento()
        For i As Integer = 0 To dtTable.Rows.Count - 1
            TipoDocumento.Items.Add(Trim(dtTable.Rows(i).Item("IdDocumento")) & " - " & dtTable.Rows(i).Item("DescripDoc"))
        Next
    End Sub

    Private Sub cargarEmpresas()
        Empresas.Items.Clear()
        eSesionCajas = New clsSesionCajas
        'eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eSesionCajas.fCargarEmpresas(gEmpresa)
        For i As Integer = 0 To dtTable.Rows.Count - 1
            Empresas.Items.Add(Trim(dtTable.Rows(i).Item("EmprCodigo")) & " - " & dtTable.Rows(i).Item("EmprDescripcion"))
        Next
    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged

    End Sub
    Private Sub dtpFechaInicio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaInicio.ValueChanged

    End Sub

  
    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        'Dim iResultado As Int32
        Dim iResultadoActCab As Int32
        'Dim iResultado2 As Int32
        'Dim DrFila As DataRow
        Dim i As Integer = 0
        'Dim msg As String
        ValidarDatos2()
        If Grabar = 1 Then
            Dim IdCajaCab As Object = lstCajas.SelectedValue
            If (TypeOf IdCajaCab Is DataRowView) Then Return
            Try
                If IdCajaCab <> Nothing Then
                    If (MessageBox.Show("�Esta seguro de Actualizar la Caja?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                        cargarTBL()
                        Dim pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pIdSesion, pVale, pEmprPresta, pRuc, pRazonSocial, pOtrosDatos As String
                        Dim Cantidad, pAnular, pEstadoPrestamo As Integer
                        eSesionCajas = New clsSesionCajas
                        iResultadoActCab = eSesionCajas.fActualizarCabecera(3, IdCajaCab, Convert.ToString(gEmpresa), 0, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), txtAporteGeneral.Text, txtTotGasto.Text, txtTotSaldo.Text, 1, txtAporteGeneral.Text, txtTotGasto.Text, txtTotSaldo.Text, gUsuario)
                        For x As Integer = 0 To TBL.Rows.Count - 1
                            dtDataBD = New DataTable
                            If vb.IsDBNull(TBL.Rows(x).Item(0)) = True Then
                                pFecha = Today()
                            Else
                                pFecha = TBL.Rows(x).Item(0)
                            End If
                            pTipoDoc = TBL.Rows(x).Item(1)
                            If vb.IsDBNull(TBL.Rows(x).Item(2)) = True Then
                                pNumeroDoc = ""
                            Else
                                pNumeroDoc = TBL.Rows(x).Item(2)
                            End If
                            pTipoGasto = TBL.Rows(x).Item(3)
                            If vb.IsDBNull(TBL.Rows(x).Item(4)) = True Then
                                pDescripcion = ""
                            Else
                                pDescripcion = TBL.Rows(x).Item(4)
                            End If
                            pCentroCosto = TBL.Rows(x).Item(5)
                            If vb.IsDBNull(TBL.Rows(x).Item(6)) = True Then
                                pTotal = "00.00"
                            Else
                                pTotal = TBL.Rows(x).Item(6)
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(7)) = True Then
                                pIdDetalleMovimiento = ""
                            Else
                                pIdDetalleMovimiento = TBL.Rows(x).Item(7)
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(9)) = True Then
                                pAnular = ""
                            Else
                                If dgvDetalleCajas.Rows(x).Cells(9).Value = False Then
                                    pAnular = 0
                                Else
                                    pAnular = 1
                                End If
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(10)) = True Then
                                pVale = ""
                            Else
                                pVale = TBL.Rows(x).Item(10)
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(11)) = True Then
                                pEmprPresta = ""
                            Else
                                pEmprPresta = TBL.Rows(x).Item(11)
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(12)) = True Then
                                pEstadoPrestamo = ""
                            Else
                                pEstadoPrestamo = TBL.Rows(x).Item(12)
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(13)) = True Then
                                pRuc = ""
                            Else
                                pRuc = TBL.Rows(x).Item(13)
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(14)) = True Then
                                pRazonSocial = ""
                            Else
                                pRazonSocial = TBL.Rows(x).Item(14)
                            End If
                            If vb.IsDBNull(TBL.Rows(x).Item(15)) = True Then
                                pOtrosDatos = ""
                            Else
                                pOtrosDatos = TBL.Rows(x).Item(15)
                            End If
                            pIdSesion = IdCajaCab
                            If TBL.Rows(x).Item(8) = "0" Then
                                eSesionCajas.fGrabarItemNuevo(0, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdSesion, gEmpresa, pAnular, pVale, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                            Else
                                eSesionCajas.VerificarDatos(1, Convert.ToDateTime(pFecha), pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, Convert.ToDouble(pTotal), pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos)
                                Cantidad = eSesionCajas.iNroRegistros
                                If Cantidad = 0 Then
                                    eSesionCajas.ActualizarDatos(2, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                    If Trim(pTipoDoc) = "04" And Trim(pNumeroDoc) <> "" Then
                                        Dim nrovale As String = ""
                                        nrovale = Trim(pNumeroDoc)
                                        eSesionCajas.fActVale5(nrovale, Trim(IdCajaCab), gEmpresa, "", Convert.ToDouble(pTotal))
                                    End If
                                End If
                            End If
                        Next
                        Dim b As Integer = 0
                        TBL.Clear()
                        TBL.Reset()
                        TBL.Dispose()
                        For b = 0 To dgvDetalleCajas.RowCount - 1
                            dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                        Next
                        dtDetalle = New DataTable
                        dtDetalle = eSesionCajas.fListarDet(IdCajaCab, gEmpresa, iEstado01)
                        cargarDetalle()
                        MessageBox.Show("Se ha Actualizado la " & Trim(lstCajas.Text), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If

                End If
            Catch ex As Exception

            End Try
        End If

        Calcular()

    End Sub

    Private Sub dgvDetalleCajas_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleCajas.CellContentClick
        'Calcular()
        Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
        If dgvDetalleCajas.Rows.Count > 0 Then
            Try
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index)
                'Calcular()
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
            Catch ex As Exception
            End Try
            'Calcular()
        End If
        If columna = 15 Then 'si el check prestamo es INACTIVADO la empresa prestada se blanquea
            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(columna).Value = False Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(14).Value = ""
            End If
        End If
        Calcular()
    End Sub

    Sub Calcular()

        Dim IdCajaCab As Object = lstCajas.SelectedValue
        If (TypeOf IdCajaCab Is DataRowView) Then Return
        Try
            If IdCajaCab <> Nothing Then


                Dim DblIngreso As Double = 0
                Dim DblEgreso As Double = 0
                Dim DblVuelto As Double = 0
                For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1

                    If Convert.ToBoolean(dgvDetalleCajas.Rows(x).Cells("Anular").Value) = False Then
                        If UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Or UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Or UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                            If Len(dgvDetalleCajas.Rows(x).Cells("Total").Value) = 0 Then
                                DblIngreso = DblIngreso + 0
                            Else
                                DblIngreso = DblIngreso + Val((Convert.ToDouble(dgvDetalleCajas.Rows(x).Cells("Total").Value)))
                            End If
                        ElseIf UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) = "9 - OTROS - INGRESO" Then

                        Else
                            If Len(dgvDetalleCajas.Rows(x).Cells("Total").Value) = 0 Then
                                DblEgreso = DblEgreso + 0
                            Else
                                DblEgreso = DblEgreso + Val((Convert.ToDouble(dgvDetalleCajas.Rows(x).Cells("Total").Value)))
                            End If
                        End If
                    End If

                    If vb.IsDBNull(dgvDetalleCajas.Rows(x).Cells("Column7").Value) = False Then
                        Dim CadenaCon As String = ""
                        Dim CadenaRazon As String = ""
                        If Trim(dgvDetalleCajas.Rows(x).Cells("Column7").Value) = "" Then
                            CadenaCon = ""
                        Else
                            CadenaCon = dgvDetalleCajas.Rows(x).Cells("Column7").Value
                        End If
                        If Trim(dgvDetalleCajas.Rows(x).Cells("Column10").Value) = "" Then
                            CadenaRazon = ""
                        Else
                            CadenaRazon = dgvDetalleCajas.Rows(x).Cells("Column10").Value
                        End If
                        If Len(CadenaCon) = 11 Then
                            Dim Codigo As String = ""
                            Dim CodAcreedor As String = ""
                            Dim CodRuc As String = ""
                            dtTable = New DataTable
                            CodRuc = Trim(CadenaCon)
                            CodAcreedor = "P"
                            eRegistroDocumento = New clsRegistroDocumento
                            dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)
                            If dtTable.Rows.Count > 0 Then
                                dgvDetalleCajas.Rows(x).Cells("Column10").Value = vb.UCase(Trim(dtTable.Rows(0).Item("AnaliticoDescripcion")))
                            Else
                                If Len(Trim(CadenaRazon)) = 0 Then
                                    dgvDetalleCajas.Rows(x).Cells("Column10").Value = ""
                                End If
                            End If
                        End If
                    End If

                Next

                txtAporteGeneral.Text = Format(DblIngreso, "#,##0.00")
                txtTotGasto.Text = Format(DblEgreso, "#,##0.00")
                txtTotSaldo.Text = Format(DblIngreso - DblEgreso, "#,##0.00")

                
            End If
        Catch ex As Exception
        End Try

      
    End Sub



    Sub ValidarDatos2()
        Dim sCadenaMsj As String = ""
        Dim count = 0
        If dgvDetalleCajas.Rows.Count = 0 Then
            'If sVieneDe = 1 Then
            '    sCadenaMsj = "Grabar"
            'ElseIf sVieneDe = 2 Then
            '    sCadenaMsj = "Arquear"
            'ElseIf sVieneDe = 3 Then
            '    sCadenaMsj = "Cerrar la Caja"
            'End If
            'If sVieneDe <> 4 Then
            '    MessageBox.Show("Para " & sCadenaMsj & " deben haber Detalles en la Caja", "Sistema de Tesorer�a")
            'End If
            Grabar = 0
        Else
            dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, 0)
            For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1

                If Len(Trim(dgvDetalleCajas.Rows(x).Cells("Column7").Value)) = 11 And Len(Trim(dgvDetalleCajas.Rows(x).Cells("Column10").Value)) = 0 Then
                    MessageBox.Show("Ingrese un Nombre de Razon Social para el Provedor", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(2, x)
                    Grabar = 0
                    Exit Sub
                End If

                If (Trim(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) <> "" And Trim(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) <> "07 - OTROS") And Trim(dgvDetalleCajas.Rows(x).Cells("Column3").Value) = "" Then
                    MessageBox.Show("Ingrese un Numero de Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(4, x)
                    Grabar = 0
                    Exit Sub
                End If

                If Trim(dgvDetalleCajas.Rows(x).Cells("Column3").Value) <> "" And Trim(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) = "" Then
                    MessageBox.Show("Seleccione un Tipo de Documento", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(3, x)
                    Grabar = 0
                    Exit Sub
                End If

                If Trim(dgvDetalleCajas.Rows(x).Cells("Fecha").Value) = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Fecha de Comprobante", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, x)
                    Grabar = 0
                    Exit Sub
                End If

                If dgvDetalleCajas.Rows(x).Cells("IdGasto").Value = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(5, x)
                    Grabar = 0
                    Exit Sub
                End If

                If dgvDetalleCajas.Rows(x).Cells("CentroCosto").Value = "" Then
                    MessageBox.Show("Se ha encontrado Registros sin Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(8, x)
                    Grabar = 0
                    Exit Sub
                End If

                If Convert.ToBoolean(dgvDetalleCajas.Rows(x).Cells("Column9").Value) = True And Trim(dgvDetalleCajas.Rows(x).Cells("Empresas").Value) = "" Then
                    MessageBox.Show("Seleccione una Empresa a Prestar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(14, x)
                    Grabar = 0
                    Exit Sub
                End If

                If UCase(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").Value) = "04 - VALE" Then
                    Dim xVale As String = ""
                    If Len(dgvDetalleCajas.Rows(x).Cells("Column3").Value) = 0 Then
                        xVale = ""
                    Else
                        xVale = dgvDetalleCajas.Rows(x).Cells("Column3").Value
                    End If

                    Dim value As Object = lstCajas.SelectedValue
                    If (TypeOf value Is DataRowView) Then Return
                    Try
                        If value <> Nothing Then
                            If xVale <> "" Then
                                Dim tblBuscarVale As DataTable
                                tblBuscarVale = New DataTable
                                eSesionCajas = New clsSesionCajas
                                tblBuscarVale = eSesionCajas.BuscarDatosdeVale(Trim(xVale), Trim(value), gEmpresa)
                                If tblBuscarVale.Rows.Count > 0 Then
                                    If Microsoft.VisualBasic.IsDBNull(tblBuscarVale.Rows(0).Item("ANombre")) = True Then
                                        MessageBox.Show("Imprima e Ingrese un responsable para el Vale " & dgvDetalleCajas.Rows(x).Cells("Column3").Value, "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(4, x)
                                        Grabar = 0
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                    'IdArea = cboArea.SelectedValue.ToString
                    
                   
                End If

                If UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "1 - OTROS - APORTE INICIAL" And UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "2 - OTROS - SALDO ANTERIOR" And UCase(dgvDetalleCajas.Rows(x).Cells("IdGasto").Value) <> "3 - OTROS - REEMBOLSO" Then
                    If Val(dgvDetalleCajas.Rows(x).Cells("Total").Value) <= 0 Then
                        MessageBox.Show("Se ha encontrado Registros de Gasto sin Importe", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(9, x)
                        Grabar = 0
                        Exit Sub
                    End If
                    Grabar = 1
                    count = count + 1
                Else
                    count = 1
                End If
            Next
            If count > 0 Then
                Grabar = 1
            Else
                MessageBox.Show("No Existe Ningun Registro de Gasto en la Caja Actual", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Grabar = 0
            End If
        End If
    End Sub

    Sub cargarTBL()
        Dim DrFila As DataRow
        Dim CadenaCon As String = ""
        Dim RazonSocial As String = ""
        Dim iResultado As Integer = 0
        Dim i As Integer = 0

        For a As Integer = 0 To dgvDetalleCajas.Columns.Count - 1
            TBL.Columns.Add(a)
        Next

        For x As Integer = 0 To dgvDetalleCajas.Rows.Count - 1 'FILA

            ''''''''''''''''graba proveedores nuevos
            If Trim(dgvDetalleCajas.Rows(x).Cells("Column7").Value) = "" Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(x).Cells("Column7").Value
            End If

            If Trim(dgvDetalleCajas.Rows(x).Cells("Column10").Value) = "" Then
                RazonSocial = ""
            Else
                RazonSocial = dgvDetalleCajas.Rows(x).Cells("Column10").Value
            End If
            If Len(CadenaCon) = 11 Then
                Dim CodAcreedor As String = ""
                Dim CodRuc As String = ""
                Dim dtTable As DataTable
                dtTable = New DataTable
                CodRuc = Trim(CadenaCon)
                CodAcreedor = "P"
                eRegistroDocumento = New clsRegistroDocumento
                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)
                If dtTable.Rows.Count = 0 Then
                    'graba
                    If Trim(RazonSocial) <> "" Then
                        iResultado = eRegistroDocumento.GrabarAcreedores(1, gEmpresa, Trim(CodAcreedor), Trim(CodRuc), Trim(RazonSocial), "", Trim(CodRuc), "", "J", "", "")
                    End If
                End If
            End If
            ''''''''''''''''graba proveedores nuevos

            DrFila = TBL.NewRow
            For i = 0 To dgvDetalleCajas.Columns.Count - 1 'COLUMNA
                If i = 0 Then
                    'fecha
                    DrFila(0) = dgvDetalleCajas.Rows(x).Cells("Fecha").Value
                ElseIf i = 1 Then
                    'ruc
                    DrFila(13) = dgvDetalleCajas.Rows(x).Cells("Column7").Value
                ElseIf i = 2 Then
                    'razon
                    DrFila(14) = dgvDetalleCajas.Rows(x).Cells("Column10").Value
                ElseIf i = 7 Then
                    'OtrosD
                    DrFila(15) = dgvDetalleCajas.Rows(x).Cells("OtrosD").Value
                ElseIf i = 3 Then
                    'iddoc
                    DrFila(1) = eSesionCajas.fDocumento(dgvDetalleCajas.Rows(x).Cells("TipoDocumento").EditedFormattedValue)
                ElseIf i = 4 Then
                    'numero
                    DrFila(2) = dgvDetalleCajas.Rows(x).Cells("Column3").Value
                ElseIf i = 5 Then
                    'id gasto
                    DrFila(3) = eSesionCajas.fGasto(dgvDetalleCajas.Rows(x).Cells("IdGasto").EditedFormattedValue)
                ElseIf i = 6 Then
                    'glosa
                    DrFila(4) = dgvDetalleCajas.Rows(x).Cells("Glosa").Value
                ElseIf i = 8 Then
                    'centrocosto
                    DrFila(5) = eSesionCajas.fCentroCosto(dgvDetalleCajas.Rows(x).Cells("CentroCosto").EditedFormattedValue)
                ElseIf i = 9 Then
                    'total
                    DrFila(6) = dgvDetalleCajas.Rows(x).Cells("Total").Value
                    '''''''''''''''
                ElseIf i = 10 Then
                    'IdDetalleMovimiento
                    DrFila(7) = dgvDetalleCajas.Rows(x).Cells("IdDetalleMovimiento").Value
                    ''MessageBox.Show(dgvDetalleCajas.Rows(x).Cells("IdDetalleMovimiento").Value)
                ElseIf i = 11 Then
                    'opcion
                    DrFila(8) = dgvDetalleCajas.Rows(x).Cells("Opcion").Value
                    'MessageBox.Show(dgvDetalleCajas.Rows(x).Cells("Opcion").Value)
                    ''''''''''''''''
                ElseIf i = 12 Then
                    'anulado
                    ''DrFila(9) = dgvDetalleCajas.Rows(x).Cells("Opcion").Value
                    If Convert.ToBoolean(dgvDetalleCajas.Rows(x).Cells("Anular").Value) = False Then
                        DrFila(9) = 0
                    Else
                        DrFila(9) = 1
                    End If
                ElseIf i = 13 Then
                    'vale
                    DrFila(10) = dgvDetalleCajas.Rows(x).Cells("Desglozado").Value
                ElseIf i = 14 Then
                    'empresa
                    DrFila(11) = eSesionCajas.fEmpresaPrestada(dgvDetalleCajas.Rows(x).Cells("Empresas").EditedFormattedValue)
                ElseIf i = 15 Then
                    If dgvDetalleCajas.Rows(x).Cells("Column9").Value = True Then
                        DrFila(12) = 1
                    ElseIf dgvDetalleCajas.Rows(x).Cells("Column9").Value = False Then
                        DrFila(12) = 0
                    End If
                End If
            Next
            TBL.Rows.Add(DrFila)
        Next
    End Sub

    Private Sub cboCaja_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCaja.SelectedIndexChanged
        txtAnio.Clear()
    End Sub

    Private Sub dgvDetalleCajas_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalleCajas.CellEndEdit

        Dim StrCad As String = String.Empty
        If e.ColumnIndex = 5 Then
            If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                contar1 = contar1 + 1
            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                contar2 = contar2 + 1
            ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                contar3 = contar3 + 1
            End If
            If contar1 > 1 And UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                MessageBox.Show("Ya existe Registros con este tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
                Exit Sub
            End If
            If contar2 > 1 And UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                MessageBox.Show("Ya existe Registros con este tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
                Exit Sub
            End If
            If contar3 > 1 And UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                MessageBox.Show("Ya existe Registros con este tipo de Gasto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(columna, dgvDetalleCajas.CurrentRow.Index)
                Exit Sub
            End If
        End If

        If e.ColumnIndex = 3 Then
            Dim IdDocumentoCorto As String = ""
            StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value
            If StrCad = Nothing Then Exit Sub
            IdDocumentoCorto = StrCad.Substring(0, StrCad.IndexOf("-")).Trim
            If IdDocumentoCorto = "04" Then


                Dim value As Object = lstCajas.SelectedValue
                If (TypeOf value Is DataRowView) Then Return
                Try
                    If value <> Nothing Then
                        If Trim(value) <> "" Then
                            eSesionCajas.fCodigoVale(1, Trim(value), sIdCaja, sIdArea, gEmpresa)
                            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value = eSesionCajas.sCodVale
                            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").ReadOnly = True
                            'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(9).Value=
                            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Anular").Value() = True
                            eSesionCajas.fGrabarVale(eSesionCajas.sCodVale, 2, Trim(value), sIdCaja, sIdArea, gEmpresa)
                        End If
                    End If
                Catch ex As Exception
                End Try



                
            Else
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").ReadOnly = False
                'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value = ""
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Anular").Value() = False
            End If
        ElseIf e.ColumnIndex = 4 Then

            'Dim CadenaCon As String
            If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value) = "" Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value = ""
            End If

        ElseIf e.ColumnIndex = 5 Then
            StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
            If StrCad = Nothing Then Exit Sub
            Dim IdGastoCorto As String = ""
            IdGastoCorto = StrCad.Substring(0, StrCad.IndexOf("-")).Trim
            If IdGastoCorto = "1" Then
                If sCantidadSesiones = 0 Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = sMontoBase
                Else
                    'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(6).Value = Format(sMontoBase - Val(sSaldoAnterior), "#,##0.00")
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = Format(sMontoBase, "0.00")
                End If
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = "APORTE INICIAL"
            ElseIf IdGastoCorto = "2" Then
                If Val(sSaldoAnterior) <= 0 Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = "0.00"
                Else
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = sSaldoAnterior 'deberia traer la diferencia entre sMontoMaximo-sSaldoAnterior
                End If
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = "SALDO ANTERIOR"
            ElseIf IdGastoCorto = "3" Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = sSaldoReembolso
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = "REEMBOLSO"
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                Dim iResActReembolsoSesion As Int16 = 0
                eSesionCajas = New clsSesionCajas


                Dim value As Object = lstCajas.SelectedValue
                If (TypeOf value Is DataRowView) Then Return
                Try
                    If value <> Nothing Then
                        If Trim(value) <> "" Then
                            iResActReembolsoSesion = eSesionCajas.fActualizarCabecera2(6, Trim(value), Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), Val(sSaldoReembolso), MontoGeneralTotalGasto, MontoGeneralFinal, 0, gUsuario)

                        End If
                    End If
                Catch ex As Exception
                End Try


                'iResActReembolsoSesion = eSesionCajas.fActualizarCabecera2(6, Trim(txtCodigo.Text), Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), Val(sSaldoReembolso), MontoGeneralTotalGasto, MontoGeneralFinal, Convert.ToString(IIf(Me.chkCerrarCaja.Checked = True, 1, 0)), gUsuario)

            Else
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").ReadOnly = False
                'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(4).Value = ""
            End If
        ElseIf e.ColumnIndex = 6 Then

            Dim CadenaCon As String
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value
            End If

            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Glosa").Value = vb.UCase(CadenaCon)

        ElseIf e.ColumnIndex = 7 Then

            Dim CadenaOtrosD As String
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("OtrosD").Value) = True Then
                CadenaOtrosD = ""
            Else
                CadenaOtrosD = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("OtrosD").Value
            End If

            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("OtrosD").Value = vb.UCase(CadenaOtrosD)


        ElseIf e.ColumnIndex = 1 Then
            Dim CadenaCon As String = ""
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column7").Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column7").Value
            End If
            If Len(CadenaCon) = 11 Then
                Dim Codigo As String = ""
                Dim CodAcreedor As String = ""
                Dim CodRuc As String = ""
                dtTable = New DataTable
                CodRuc = Trim(CadenaCon)
                CodAcreedor = "P"
                eRegistroDocumento = New clsRegistroDocumento
                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)
                If dtTable.Rows.Count > 0 Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value = vb.UCase(Trim(dtTable.Rows(0).Item("AnaliticoDescripcion")))
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").ReadOnly = False
                Else
                    MessageBox.Show("El Proveedor con RUC " & Trim(CodRuc) & " no esta Registrado, Registrelo ahora con su Raz�n Social.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value = ""
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").ReadOnly = False
                    'dgvDetalleCajas.CurrentCell = dgvDetalleCajas(2, dgvDetalleCajas.CurrentRow.Index)
                    'dgvDetalleCajas.BeginEdit(True)
                End If
            Else
                If Len(CadenaCon) > 0 Then
                    MessageBox.Show("Ingrese un Ruc V�lido.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value = ""
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").ReadOnly = False
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(1, dgvDetalleCajas.CurrentRow.Index)
                    dgvDetalleCajas.BeginEdit(True)
                End If
                Exit Sub
            End If

        ElseIf e.ColumnIndex = 2 Then

            Dim CadenaCon As String
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value
            End If



            '''''''''''''''''''''''''''''''''
            Dim CadenaRuc As String = ""
            If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column7").Value) = True Then
                CadenaRuc = ""
            Else
                CadenaRuc = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column7").Value
            End If
            If Len(CadenaRuc) = 11 Then
                Dim Codigo As String = ""
                Dim CodAcreedor As String = ""
                Dim CodRuc As String = ""
                dtTable = New DataTable
                CodRuc = Trim(CadenaRuc)
                CodAcreedor = "P"
                eRegistroDocumento = New clsRegistroDocumento
                dtTable = eRegistroDocumento.fListarProveedoresxRuc(gEmpresa, CodRuc, CodAcreedor)

                Dim dtTable2 As DataTable
                dtTable2 = New DataTable
                eRegistroDocumento = New clsRegistroDocumento
                dtTable2 = eRegistroDocumento.fListarProveedoresxRazonSocial(gEmpresa, CodRuc, CodAcreedor, Trim(vb.UCase(Trim(CadenaCon))))
                If dtTable2.Rows.Count > 0 Then
                    MessageBox.Show("Ya Existe el Proveedor " & Trim(vb.UCase(Trim(CadenaCon))) & " con Ruc " & Trim(dtTable2.Rows(0).Item("Ruc")) & ", Cambie de Raz�n Social para el actual Proveedor.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value = ""
                    Exit Sub
                End If

                If dtTable.Rows.Count > 0 Then
                    If vb.UCase(Trim(CadenaCon)) <> vb.UCase(Trim(dtTable.Rows(0).Item("AnaliticoDescripcion"))) Then
                        Dim iResultado As Integer = 0
                        iResultado = eRegistroDocumento.GrabarAcreedores(2, gEmpresa, Trim(CodAcreedor), Trim(CodRuc), Trim(vb.UCase(Trim(CadenaCon))), "", Trim(CodRuc), "", "J", "", "")
                    End If

                End If
            End If
            '''''''''''''''''''''''''''''''''
            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column10").Value = vb.UCase(Trim(CadenaCon))

        ElseIf e.ColumnIndex = 7 Then
            'StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("CentroCosto").Value
            'If StrCad = Nothing Then Exit Sub
            'IdCCostoCorto = StrCad.Substring(0, StrCad.IndexOf("-")).Trim
        ElseIf e.ColumnIndex = 9 Then
            If Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) <> "" Then
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = Format(CDec(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value) = True Then
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = Format(CDec(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value.ToString), "#,##0.00")
                End If
            Else
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Value = "0.00"
            End If
        ElseIf e.ColumnIndex = 0 Then
            StrCad = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value
            If StrCad = Nothing Then Exit Sub
            Try
                If Convert.ToDateTime(StrCad) < Convert.ToDateTime(dtpFechaInicio.Text) Then
                    MessageBox.Show("La Fecha de Registro debe ser Mayor o igual a la Fecha de Apertura", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index)
                    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value = ""
                Else
                    'hacer que cuando se ingrese una fecha se compare con todas las del grid y colocar la fecha mayor en el datapicker final
                    '---------------------------------------
                    Dim i As Integer
                    Dim FechaMayor As DateTime
                    If dgvDetalleCajas.Rows.Count = 1 Then
                        FechaMayor = dgvDetalleCajas.Rows(0).Cells("Fecha").Value
                    Else
                        FechaMayor = dgvDetalleCajas.Rows(0).Cells("Fecha").Value
                        For i = 0 To dgvDetalleCajas.Rows.Count - 1
                            'MessageBox.Show(dgvDetalleCajas.Rows(i).Cells(0).Value)
                            If Trim(dgvDetalleCajas.Rows(i).Cells("Fecha").Value) <> "" Then
                                'dgvDetalleCajas.Rows(i).Cells(0).Value = FechaMayor
                                If (Convert.ToDateTime(dgvDetalleCajas.Rows(i).Cells("Fecha").Value) > FechaMayor) Then
                                    FechaMayor = Convert.ToDateTime(dgvDetalleCajas.Rows(i).Cells("Fecha").Value)
                                End If
                            End If

                        Next
                    End If
                    dtpFechaFin.Value = FechaMayor
                    '---------------------------------------
                End If
            Catch ex As Exception
                MessageBox.Show("Ingrese una Fecha Valida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index)
                'dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, dgvDetalleCajas.CurrentRow.Index - 1)
                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Fecha").Value = ""
            End Try
        End If
        Call Calcular()

    End Sub

    Private Sub dgvDetalleCajas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalleCajas.Click
        Calcular()
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        Dim CodigoIdDetalle As String = ""

        If dgvDetalleCajas.Rows.Count > 0 Then
            Dim value As Object = lstCajas.SelectedValue
            If (TypeOf value Is DataRowView) Then Return
            Try
                If value <> Nothing Then
                    If MessageBox.Show("� Desea Eliminar el Item ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        CodigoIdDetalle = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdDetalleMovimiento").Value
                        'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        'borrar compra de requerimientos
                        eCompraRequerimientos = New clsCompraRequerimientos
                        Dim dtDatosEnReq As DataTable
                        dtDatosEnReq = New DataTable
                        dtDatosEnReq = eCompraRequerimientos.fListar2(10, gEmpresa, "", "", "", Trim(CodigoIdDetalle), Trim(value))
                        If dtDatosEnReq.Rows.Count > 0 Then
                            'xDxDxD
                            Dim xOcoCodigo As String = ""
                            Dim xOcoNumero As String = ""
                            Dim xReqCodigo As String = ""
                            Dim xEmprCodigo As String = ""
                            Dim xIdReqDetalle As String = ""
                            Dim xCantidadUniSol As Decimal = 0
                            Dim xCantidadGrupoSol As Decimal = 0
                            For x As Integer = 0 To dtDatosEnReq.Rows.Count - 1
                                xOcoCodigo = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("OcoCodigo")))
                                xOcoNumero = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("OcoNumero")))
                                xReqCodigo = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("ReqCodigo")))
                                xEmprCodigo = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("EmprCodigo")))
                                xIdReqDetalle = Trim(Convert.ToString(dtDatosEnReq.Rows(x).Item("IdReqDetalle")))
                                xCantidadUniSol = Convert.ToDecimal(dtDatosEnReq.Rows(x).Item("CantidadUniSol"))
                                xCantidadGrupoSol = Convert.ToDecimal(dtDatosEnReq.Rows(x).Item("CantidadGrupoSol"))
                                Dim iResultadoDet As Integer = 0
                                iResultadoDet = eCompraRequerimientos.fGrabarOrdenDet2(11, xOcoCodigo, xOcoNumero, xEmprCodigo, xReqCodigo, xIdReqDetalle, xCantidadUniSol, xCantidadGrupoSol)
                            Next
                        End If
                        'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

                        If CodigoIdDetalle <> Nothing Then

                            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value <> "" Then
                                Dim nrovale As String = ""
                                nrovale = Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value)
                                eSesionCajas.fActVale3(nrovale, Trim(value), gEmpresa, "", 0)
                            End If

                            eSesionCajas.fEliminarItemDetalle(CodigoIdDetalle, Trim(value), gEmpresa, gUsuario)
                            '+++++++++++++++++++++
                            'If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                            '    'contar1 = 0
                            'ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                            '    'contar2 = 0
                            'ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "04 - VALE" Then
                            '    Dim xxxNroVale As String = ""
                            '    xxxNroVale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value
                            '    If xxxNroVale <> Nothing Then
                            '        eSesionCajas.fActVale4(Trim(xxxNroVale), Trim(value), gEmpresa, "", 0)
                            '    End If
                            'ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                            '    'contar3 = 0
                            'End If
                            '+++++++++++++++++++++
                            dgvDetalleCajas.Rows.RemoveAt(dgvDetalleCajas.CurrentRow.Index)
                            'sCodigoActual = Me.txtCodigo.Text
                            Calcular()
                            eSesionCajas.fActualizarCabecera(3, value, Convert.ToString(gEmpresa), sNumeroSesion, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), Convert.ToDecimal(txtAporteGeneral.Text), Convert.ToDecimal(txtTotGasto.Text), Convert.ToDecimal(txtTotSaldo.Text), 1, txtAporteGeneral.Text, txtTotGasto.Text, txtTotSaldo.Text, gUsuario)
                        Else
                            'MsgBox(UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(1).Value))
                            If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("TipoDocumento").Value) = "04 - VALE" Then
                                Dim xxxNroVale As String = ""
                                xxxNroVale = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value
                                If xxxNroVale <> Nothing Then
                                    eSesionCajas.fActVale4(Trim(xxxNroVale), Trim(value), gEmpresa, "", 0)
                                End If
                            End If

                            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value <> "" Then
                                Dim nrovale As String = ""
                                nrovale = Trim(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Desglozado").Value)
                                eSesionCajas.fActVale3(nrovale, Trim(value), gEmpresa, "", 0)
                            End If

                            '+++++++++++++++++++++
                            'If UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "1 - OTROS - APORTE INICIAL" Then
                            '    'contar1 = 0
                            'ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "2 - OTROS - SALDO ANTERIOR" Then
                            '    'contar2 = 0
                            'ElseIf UCase(dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value) = "3 - OTROS - REEMBOLSO" Then
                            '    'contar3 = 0
                            'End If
                            '+++++++++++++++++++++
                            dgvDetalleCajas.Rows.RemoveAt(dgvDetalleCajas.CurrentRow.Index)
                        End If

                    End If


                End If
            Catch ex As Exception
            End Try

           
            
        Else
            MessageBox.Show("No Existe ningun registro para eliminar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub dgvDetalleCajas_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalleCajas.DoubleClick

    End Sub

    Private Sub dgvDetalleCajas_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalleCajas.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim columna As Integer = dgvDetalleCajas.CurrentCell.ColumnIndex
        If columna = 9 Then
            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = "-") Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If

        If columna = 4 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter = ChrW(Keys.Space)) = False And (caracter.ToString <> "-") Then
                'Me.Text = e.KeyChar
                e.KeyChar = Chr(0)
            End If
        End If

        If columna = 0 Then
            'Obtener caracter
            Dim caracter As Char = e.KeyChar
            'Comprobar si el caracter es un n�mero o el retroceso   
            If Not Char.IsNumber(caracter) And (caracter = ChrW(Keys.Back)) = False And (caracter.ToString <> "/") Then
                e.KeyChar = Chr(0)
            End If
        End If

        If columna = 1 Then

            Dim caracter As Char = e.KeyChar
            ' referencia a la celda   
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Then
                e.Handled = False
            Else
                e.Handled = True
            End If

            'If (Char.IsNumber(caracter)) Or (Len(Trim(xRuc))) <= 11 Then
            '    xRuc = xRuc & caracter
            '    e.Handled = False
            'ElseIf caracter = ChrW(Keys.Back) Then
            '    e.Handled = False
            'Else
            '    e.Handled = True
            'End If

        End If


    End Sub


    Private Sub dgvDetalleCajas_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetalleCajas.KeyPress
        Dim A As Integer
        Select Case Asc(e.KeyChar)
            Case 13
                If dgvDetalleCajas.Rows.Count = 0 Then
                    dgvDetalleCajas.Rows.Add()
                    dgvDetalleCajas.Rows(0).Cells("Opcion").Value = "0"
                    dgvDetalleCajas.Rows(0).Cells("Fecha").Value = Now.Date().ToShortDateString
                    dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, 0)
                Else
                    If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Total").Selected = True Then 'Or dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(13).Selected = True Or dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(14).Selected = True Then
                        Dim iResultado As Integer
                        ValidarDatos2()
                        If Grabar = 0 Then
                            Exit Sub
                        End If

                        If Grabar = 1 Then
                            'sCodigoActual = txtCodigo.Text.Trim

                            Dim value As Object = lstCajas.SelectedValue
                            If (TypeOf value Is DataRowView) Then Return
                            Try
                                If value <> Nothing Then
                                    If Trim(value) <> "" Then
                                        eSesionCajas.fCodigoVale(1, Trim(value), sIdCaja, sIdArea, gEmpresa)
                                        dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").Value = eSesionCajas.sCodVale
                                        dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Column3").ReadOnly = True
                                        'dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells(9).Value=
                                        dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("Anular").Value() = True
                                        eSesionCajas.fGrabarVale(eSesionCajas.sCodVale, 2, Trim(value), sIdCaja, sIdArea, gEmpresa)
                                    End If
                                End If
                            Catch ex As Exception
                            End Try

                            Dim pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pIdSesion, pVale, pEmprPresta, pRuc, pRazonSocial, pOtrosDatos As String
                            Dim Cantidad, pAnular, iResultadoActCab, pEstadoPrestamo As Integer
                            iResultadoActCab = eSesionCajas.fActualizarCabecera(3, Trim(value), Convert.ToString(gEmpresa), 0, Convert.ToString(sIdArea), Convert.ToString(sIdCaja), Convert.ToDateTime(Me.dtpFechaInicio.Text.Trim), Convert.ToDateTime(Me.dtpFechaFin.Text.Trim), 0, MontoGeneralTotalGasto, MontoGeneralFinal, 0, txtAporteGeneral.Text, txtTotGasto.Text, txtTotSaldo.Text, gUsuario)
                            cargarTBL()
                            For v As Integer = 0 To TBL.Rows.Count - 1
                                dtDataBD = New DataTable
                                If vb.IsDBNull(TBL.Rows(v).Item(0)) = True Then
                                    pFecha = Today()
                                Else
                                    pFecha = TBL.Rows(v).Item(0)
                                End If
                                pTipoDoc = TBL.Rows(v).Item(1)
                                If vb.IsDBNull(TBL.Rows(v).Item(2)) = True Then
                                    pNumeroDoc = ""
                                Else
                                    pNumeroDoc = TBL.Rows(v).Item(2)
                                End If
                                pTipoGasto = TBL.Rows(v).Item(3)
                                If vb.IsDBNull(TBL.Rows(v).Item(4)) = True Then
                                    pDescripcion = ""
                                Else
                                    pDescripcion = TBL.Rows(v).Item(4)
                                End If
                                pCentroCosto = TBL.Rows(v).Item(5)
                                If vb.IsDBNull(TBL.Rows(v).Item(6)) = True Then
                                    pTotal = "00.00"
                                Else
                                    pTotal = TBL.Rows(v).Item(6)
                                End If
                                If vb.IsDBNull(TBL.Rows(v).Item(7)) = True Then
                                    pIdDetalleMovimiento = ""
                                Else
                                    pIdDetalleMovimiento = TBL.Rows(v).Item(7)
                                End If

                                If vb.IsDBNull(TBL.Rows(v).Item(9)) = True Then
                                    pAnular = ""
                                Else
                                    If dgvDetalleCajas.Rows(v).Cells(9).Value = False Then
                                        pAnular = 0
                                    Else
                                        pAnular = 1
                                    End If
                                End If

                                If vb.IsDBNull(TBL.Rows(v).Item(10)) = True Then
                                    pVale = ""
                                Else
                                    pVale = TBL.Rows(v).Item(10)
                                End If

                                If vb.IsDBNull(TBL.Rows(v).Item(11)) = True Then
                                    pEmprPresta = ""
                                    'MessageBox.Show(TBL.Rows(v).Item(11))
                                Else
                                    pEmprPresta = TBL.Rows(v).Item(11)
                                    'MessageBox.Show(TBL.Rows(v).Item(11))
                                End If

                                If vb.IsDBNull(TBL.Rows(v).Item(12)) = True Then
                                    pEstadoPrestamo = ""
                                    'MessageBox.Show(TBL.Rows(v).Item(12))
                                Else
                                    pEstadoPrestamo = TBL.Rows(v).Item(12)
                                    'MessageBox.Show(TBL.Rows(v).Item(12))
                                End If

                                If vb.IsDBNull(TBL.Rows(v).Item(13)) = True Then
                                    pRuc = ""
                                Else
                                    pRuc = TBL.Rows(v).Item(13)
                                End If

                                If vb.IsDBNull(TBL.Rows(v).Item(14)) = True Then
                                    pRazonSocial = ""
                                Else
                                    pRazonSocial = TBL.Rows(v).Item(14)
                                End If
                                If vb.IsDBNull(TBL.Rows(v).Item(15)) = True Then
                                    pOtrosDatos = ""
                                Else
                                    pOtrosDatos = TBL.Rows(v).Item(15)
                                End If

                                'MessageBox.Show(TBL.Rows(v).Item(8))
                                pIdSesion = Trim(value)
                                If TBL.Rows(v).Item(8) = "0" Then
                                    eSesionCajas.fGrabarItemNuevo(0, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdSesion, gEmpresa, pAnular, pVale, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                Else
                                    eSesionCajas.VerificarDatos(1, Convert.ToDateTime(pFecha), pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, Convert.ToDouble(pTotal), pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos)
                                    Cantidad = eSesionCajas.iNroRegistros
                                    If Cantidad = 0 Then
                                        eSesionCajas.ActualizarDatos(2, pFecha, pTipoDoc, pNumeroDoc, pTipoGasto, pDescripcion, pCentroCosto, pTotal, pIdDetalleMovimiento, pAnular, pVale, pIdSesion, gEmpresa, pEmprPresta, pEstadoPrestamo, pRuc, pRazonSocial, pOtrosDatos, gUsuario)
                                        If Trim(pTipoDoc) = "04" And Trim(pNumeroDoc) <> "" Then
                                            Dim nrovale As String = ""
                                            nrovale = Trim(pNumeroDoc)
                                            eSesionCajas.fActVale5(nrovale, Trim(value), gEmpresa, "", Convert.ToDouble(pTotal))
                                        End If
                                    End If
                                End If
                            Next
                            Dim x As Integer = 0
                            TBL.Clear()
                            TBL.Reset()
                            TBL.Dispose()
                            For x = 0 To dgvDetalleCajas.RowCount - 1
                                dgvDetalleCajas.Rows.Remove(dgvDetalleCajas.CurrentRow)
                            Next
                            dtDetalle = New DataTable
                            dtDetalle = eSesionCajas.fListarDet(value, gEmpresa, iEstado01)
                            cargarDetalle()
                        End If 'If Grabar = 1 Then
                        'End If

                        Dim IntCod As Integer = 0
                        dgvDetalleCajas.Rows.Add()
                        A = dgvDetalleCajas.Rows.Count
                        If dgvDetalleCajas.Rows(A - 2).Cells("Opcion").Value = "0" Or dgvDetalleCajas.Rows(A - 2).Cells("Opcion").Value = "1" Then
                            A = dgvDetalleCajas.Rows.Count
                            dgvDetalleCajas.Rows(A - 1).Cells("Opcion").Value = "0"
                            dgvDetalleCajas.Rows(A - 1).Cells(0).Value = Now.Date().ToShortDateString
                            'If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index - 1).Cells(5).Value <> "" Then
                            If dgvDetalleCajas.Rows(A - 2).Cells("CentroCosto").Value <> "" Then
                                dgvDetalleCajas.Rows(A - 1).Cells("CentroCosto").Value = dgvDetalleCajas.Rows(A - 2).Cells("CentroCosto").Value
                            End If
                        Else
                            dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index + 1).Cells("Opcion").Value = "0"
                            If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value <> "" Then
                                dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index + 1).Cells("IdGasto").Value = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
                            End If
                        End If
                        dgvDetalleCajas.Rows(A - 1).Cells(0).Value = Now.Date().ToShortDateString
                        dgvDetalleCajas.CurrentCell = dgvDetalleCajas(0, A - 1)
                End If
                End If
        End Select

        Call Calcular()
    End Sub
End Class
