<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTipoGasto2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTipoGasto2))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.mnuMantenimiento = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnRefrescar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimirListado = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.pnCuerpo = New System.Windows.Forms.Panel()
        Me.pnPie = New System.Windows.Forms.Panel()
        Me.tabMantenimiento = New System.Windows.Forms.TabControl()
        Me.tabLista = New System.Windows.Forms.TabPage()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.cboEstados = New ctrLibreria.Controles.BeComboBox()
        Me.txtBusqueda = New ctrLibreria.Controles.BeTextBox()
        Me.gvTipoGasto = New System.Windows.Forms.DataGridView()
        Me.IDTIPOGASTO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBRE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ABREAVIATURA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FLGESTADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACTIVO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ESTADO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabDetalle = New System.Windows.Forms.TabPage()
        Me.pnDetalle = New System.Windows.Forms.Panel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.chkEstado = New System.Windows.Forms.CheckBox()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.txtAbreviatura = New ctrLibreria.Controles.BeTextBox()
        Me.Panel1.SuspendLayout()
        Me.mnuMantenimiento.SuspendLayout()
        Me.pnCuerpo.SuspendLayout()
        Me.tabMantenimiento.SuspendLayout()
        Me.tabLista.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.gvTipoGasto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabDetalle.SuspendLayout()
        Me.pnDetalle.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.mnuMantenimiento)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(572, 28)
        Me.Panel1.TabIndex = 171
        '
        'mnuMantenimiento
        '
        Me.mnuMantenimiento.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnCancelar, Me.btnRefrescar, Me.btnEliminar, Me.btnImprimirListado, Me.btnSalir})
        Me.mnuMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.mnuMantenimiento.Name = "mnuMantenimiento"
        Me.mnuMantenimiento.Size = New System.Drawing.Size(570, 25)
        Me.mnuMantenimiento.TabIndex = 0
        Me.mnuMantenimiento.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnCancelar
        '
        Me.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(23, 22)
        Me.btnCancelar.Text = "ToolStripButton1"
        Me.btnCancelar.ToolTipText = "Cancelar"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRefrescar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Grid_properties_
        Me.btnRefrescar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(23, 22)
        Me.btnRefrescar.Text = "&Refrescar"
        Me.btnRefrescar.ToolTipText = "Refrescar"
        '
        'btnEliminar
        '
        Me.btnEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.ToolTipText = "Eliminar"
        '
        'btnImprimirListado
        '
        Me.btnImprimirListado.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimirListado.Image = CType(resources.GetObject("btnImprimirListado.Image"), System.Drawing.Image)
        Me.btnImprimirListado.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimirListado.Name = "btnImprimirListado"
        Me.btnImprimirListado.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimirListado.Text = "ToolStripButton1"
        Me.btnImprimirListado.ToolTipText = "Imprimir Caja"
        Me.btnImprimirListado.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'pnCuerpo
        '
        Me.pnCuerpo.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnCuerpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnCuerpo.Controls.Add(Me.pnPie)
        Me.pnCuerpo.Controls.Add(Me.tabMantenimiento)
        Me.pnCuerpo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnCuerpo.Location = New System.Drawing.Point(0, 28)
        Me.pnCuerpo.Name = "pnCuerpo"
        Me.pnCuerpo.Size = New System.Drawing.Size(572, 309)
        Me.pnCuerpo.TabIndex = 173
        '
        'pnPie
        '
        Me.pnPie.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnPie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnPie.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnPie.Location = New System.Drawing.Point(0, 288)
        Me.pnPie.Name = "pnPie"
        Me.pnPie.Size = New System.Drawing.Size(570, 19)
        Me.pnPie.TabIndex = 174
        '
        'tabMantenimiento
        '
        Me.tabMantenimiento.Controls.Add(Me.tabLista)
        Me.tabMantenimiento.Controls.Add(Me.tabDetalle)
        Me.tabMantenimiento.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.tabMantenimiento.Name = "tabMantenimiento"
        Me.tabMantenimiento.SelectedIndex = 0
        Me.tabMantenimiento.Size = New System.Drawing.Size(570, 307)
        Me.tabMantenimiento.TabIndex = 28
        '
        'tabLista
        '
        Me.tabLista.Controls.Add(Me.Panel2)
        Me.tabLista.Controls.Add(Me.gvTipoGasto)
        Me.tabLista.Location = New System.Drawing.Point(4, 22)
        Me.tabLista.Name = "tabLista"
        Me.tabLista.Padding = New System.Windows.Forms.Padding(3)
        Me.tabLista.Size = New System.Drawing.Size(562, 281)
        Me.tabLista.TabIndex = 0
        Me.tabLista.Text = "Lista"
        Me.tabLista.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.BeLabel1)
        Me.Panel2.Controls.Add(Me.BeLabel6)
        Me.Panel2.Controls.Add(Me.BeLabel4)
        Me.Panel2.Controls.Add(Me.cboEstados)
        Me.Panel2.Controls.Add(Me.txtBusqueda)
        Me.Panel2.Location = New System.Drawing.Point(111, 79)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(295, 83)
        Me.Panel2.TabIndex = 28
        Me.Panel2.Visible = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Red
        Me.BeLabel1.Location = New System.Drawing.Point(273, -1)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(18, 16)
        Me.BeLabel1.TabIndex = 4
        Me.BeLabel1.Text = "X"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.White
        Me.BeLabel6.Location = New System.Drawing.Point(7, 54)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel6.TabIndex = 3
        Me.BeLabel6.Text = "Estado"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.White
        Me.BeLabel4.Location = New System.Drawing.Point(7, 27)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel4.TabIndex = 2
        Me.BeLabel4.Text = "Descripción"
        '
        'cboEstados
        '
        Me.cboEstados.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEstados.BackColor = System.Drawing.Color.Ivory
        Me.cboEstados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstados.ForeColor = System.Drawing.Color.Black
        Me.cboEstados.FormattingEnabled = True
        Me.cboEstados.KeyEnter = True
        Me.cboEstados.Location = New System.Drawing.Point(76, 46)
        Me.cboEstados.Name = "cboEstados"
        Me.cboEstados.Size = New System.Drawing.Size(121, 21)
        Me.cboEstados.TabIndex = 1
        '
        'txtBusqueda
        '
        Me.txtBusqueda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBusqueda.BackColor = System.Drawing.Color.Ivory
        Me.txtBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBusqueda.ForeColor = System.Drawing.Color.Black
        Me.txtBusqueda.KeyEnter = True
        Me.txtBusqueda.Location = New System.Drawing.Point(76, 20)
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBusqueda.ShortcutsEnabled = False
        Me.txtBusqueda.Size = New System.Drawing.Size(196, 20)
        Me.txtBusqueda.TabIndex = 0
        Me.txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'gvTipoGasto
        '
        Me.gvTipoGasto.AllowUserToAddRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.gvTipoGasto.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.gvTipoGasto.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gvTipoGasto.BackgroundColor = System.Drawing.Color.White
        Me.gvTipoGasto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gvTipoGasto.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDTIPOGASTO, Me.NOMBRE, Me.ABREAVIATURA, Me.FLGESTADO, Me.ACTIVO, Me.ESTADO})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gvTipoGasto.DefaultCellStyle = DataGridViewCellStyle2
        Me.gvTipoGasto.Location = New System.Drawing.Point(7, 6)
        Me.gvTipoGasto.Name = "gvTipoGasto"
        Me.gvTipoGasto.ReadOnly = True
        Me.gvTipoGasto.RowHeadersVisible = False
        Me.gvTipoGasto.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gvTipoGasto.Size = New System.Drawing.Size(548, 254)
        Me.gvTipoGasto.TabIndex = 27
        '
        'IDTIPOGASTO
        '
        Me.IDTIPOGASTO.DataPropertyName = "IDTIPOGASTO"
        Me.IDTIPOGASTO.HeaderText = "IDTIPOGASTO"
        Me.IDTIPOGASTO.Name = "IDTIPOGASTO"
        Me.IDTIPOGASTO.ReadOnly = True
        Me.IDTIPOGASTO.Visible = False
        '
        'NOMBRE
        '
        Me.NOMBRE.DataPropertyName = "NOMBRE"
        Me.NOMBRE.HeaderText = "Descripción"
        Me.NOMBRE.Name = "NOMBRE"
        Me.NOMBRE.ReadOnly = True
        Me.NOMBRE.Width = 200
        '
        'ABREAVIATURA
        '
        Me.ABREAVIATURA.DataPropertyName = "ABREAVIATURA"
        Me.ABREAVIATURA.HeaderText = "Abreviatura"
        Me.ABREAVIATURA.Name = "ABREAVIATURA"
        Me.ABREAVIATURA.ReadOnly = True
        '
        'FLGESTADO
        '
        Me.FLGESTADO.DataPropertyName = "FLGESTADO"
        Me.FLGESTADO.HeaderText = "FLGESTADO"
        Me.FLGESTADO.Name = "FLGESTADO"
        Me.FLGESTADO.ReadOnly = True
        Me.FLGESTADO.Visible = False
        '
        'ACTIVO
        '
        Me.ACTIVO.DataPropertyName = "ACTIVO"
        Me.ACTIVO.HeaderText = "ACTIVO"
        Me.ACTIVO.Name = "ACTIVO"
        Me.ACTIVO.ReadOnly = True
        '
        'ESTADO
        '
        Me.ESTADO.DataPropertyName = "ESTADO"
        Me.ESTADO.HeaderText = "ESTADO"
        Me.ESTADO.Name = "ESTADO"
        Me.ESTADO.ReadOnly = True
        Me.ESTADO.Visible = False
        '
        'tabDetalle
        '
        Me.tabDetalle.Controls.Add(Me.pnDetalle)
        Me.tabDetalle.Location = New System.Drawing.Point(4, 22)
        Me.tabDetalle.Name = "tabDetalle"
        Me.tabDetalle.Padding = New System.Windows.Forms.Padding(3)
        Me.tabDetalle.Size = New System.Drawing.Size(562, 281)
        Me.tabDetalle.TabIndex = 1
        Me.tabDetalle.Text = "Detalle"
        Me.tabDetalle.UseVisualStyleBackColor = True
        '
        'pnDetalle
        '
        Me.pnDetalle.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnDetalle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnDetalle.Controls.Add(Me.BeLabel2)
        Me.pnDetalle.Controls.Add(Me.chkEstado)
        Me.pnDetalle.Controls.Add(Me.txtNombre)
        Me.pnDetalle.Controls.Add(Me.BeLabel15)
        Me.pnDetalle.Controls.Add(Me.BeLabel9)
        Me.pnDetalle.Controls.Add(Me.txtAbreviatura)
        Me.pnDetalle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnDetalle.Location = New System.Drawing.Point(3, 3)
        Me.pnDetalle.Name = "pnDetalle"
        Me.pnDetalle.Size = New System.Drawing.Size(556, 275)
        Me.pnDetalle.TabIndex = 173
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(29, 70)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel2.TabIndex = 58
        Me.BeLabel2.Text = "Estado"
        '
        'chkEstado
        '
        Me.chkEstado.AutoSize = True
        Me.chkEstado.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEstado.ForeColor = System.Drawing.Color.Black
        Me.chkEstado.Location = New System.Drawing.Point(79, 70)
        Me.chkEstado.Name = "chkEstado"
        Me.chkEstado.Size = New System.Drawing.Size(15, 14)
        Me.chkEstado.TabIndex = 57
        Me.chkEstado.UseVisualStyleBackColor = True
        '
        'txtNombre
        '
        Me.txtNombre.BackColor = System.Drawing.Color.Ivory
        Me.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Location = New System.Drawing.Point(79, 11)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(240, 20)
        Me.txtNombre.TabIndex = 48
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(12, 39)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(61, 13)
        Me.BeLabel15.TabIndex = 47
        Me.BeLabel15.Text = "Abreviatura"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(29, 13)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(44, 13)
        Me.BeLabel9.TabIndex = 44
        Me.BeLabel9.Text = "Nombre"
        '
        'txtAbreviatura
        '
        Me.txtAbreviatura.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAbreviatura.BackColor = System.Drawing.Color.Ivory
        Me.txtAbreviatura.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAbreviatura.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAbreviatura.ForeColor = System.Drawing.Color.Black
        Me.txtAbreviatura.KeyEnter = False
        Me.txtAbreviatura.Location = New System.Drawing.Point(79, 39)
        Me.txtAbreviatura.MaxLength = 20
        Me.txtAbreviatura.Name = "txtAbreviatura"
        Me.txtAbreviatura.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAbreviatura.ShortcutsEnabled = False
        Me.txtAbreviatura.Size = New System.Drawing.Size(106, 20)
        Me.txtAbreviatura.TabIndex = 45
        Me.txtAbreviatura.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'frmTipoGasto2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(572, 337)
        Me.Controls.Add(Me.pnCuerpo)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmTipoGasto2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tipo de Gasto"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.mnuMantenimiento.ResumeLayout(False)
        Me.mnuMantenimiento.PerformLayout()
        Me.pnCuerpo.ResumeLayout(False)
        Me.tabMantenimiento.ResumeLayout(False)
        Me.tabLista.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.gvTipoGasto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabDetalle.ResumeLayout(False)
        Me.pnDetalle.ResumeLayout(False)
        Me.pnDetalle.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents mnuMantenimiento As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnRefrescar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimirListado As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnCuerpo As System.Windows.Forms.Panel
    Friend WithEvents gvTipoGasto As System.Windows.Forms.DataGridView
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabMantenimiento As System.Windows.Forms.TabControl
    Friend WithEvents tabLista As System.Windows.Forms.TabPage
    Friend WithEvents tabDetalle As System.Windows.Forms.TabPage
    Friend WithEvents pnPie As System.Windows.Forms.Panel
    Friend WithEvents pnDetalle As System.Windows.Forms.Panel
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtAbreviatura As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnCancelar As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboEstados As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtBusqueda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents chkEstado As System.Windows.Forms.CheckBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents IDTIPOGASTO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBRE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ABREAVIATURA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FLGESTADO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACTIVO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ESTADO As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
