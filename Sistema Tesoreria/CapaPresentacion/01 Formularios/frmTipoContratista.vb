Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions


Public Class frmTipoContratista

    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Private eEspecialidad As clsEspecialidad

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmTipoContratista = Nothing
    Public Shared Function Instance() As frmTipoContratista
        If frmInstance Is Nothing Then
            frmInstance = New frmTipoContratista
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmTipoContratista_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmTipoContratista_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboEstado.DropDownStyle = ComboBoxStyle.DropDownList
        eEspecialidad = New clsEspecialidad
        mMostrarGrilla()
        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        dgvLista.Width = 498
        dgvLista.Height = 400
    End Sub

    Private Sub mMostrarGrilla()
        eEspecialidad = New clsEspecialidad
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eEspecialidad.fListar(5, gEmpresa)
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTable
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eEspecialidad.iNroRegistros
        If eEspecialidad.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
    End Sub

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtNombre.Clear()
    End Sub

    Private Sub DesabilitarControles()
        txtNombre.Enabled = False
        cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        txtNombre.Enabled = True
        cboEstado.Enabled = True
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        'If sTab = 0 Then
        iOpcion = 7
        sTab = 1
        LimpiarControles()
        HabilitarControles()
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False
        eEspecialidad = New clsEspecialidad
        eEspecialidad.fCodigo(6)
        txtCodigo.Text = eEspecialidad.sCodFuturo
        txtCodigo.Enabled = False
        txtNombre.Focus()
        'End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        'If sTab = 0 Then
        sTab = 1
        iOpcion = 8
        VerPosicion()
        HabilitarControles()
        cboEstado.Enabled = True
        txtCodigo.Enabled = False
        txtNombre.Focus()
        'End If
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("tipoconcodigo").ToString
                Me.txtNombre.Text = Fila("destipocontratista").ToString
                If Fila("estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'Dim iResultado As Int32
        Try
            '    Using scope As TransactionScope = New TransactionScope
            If dgvLista.Rows.Count > 0 Then
                'If (MessageBox.Show("Desea Eliminar: " & Chr(13) & Chr(13) & "Codigo: " & Fila("AlmCodigo") & "   " & Chr(13) & "Descripcion: " & Fila("AlmDescripcion"), sNombreSistema, MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                '    'Using scope As TransactionScope = New TransactionScope '(MSDTC)
                '    eAlmacen = New clsAlmacen
                '    iResultado = eAlmacen.fEliminar(Fila("ALMCODIGO"), 3)
                '    'scope.Complete()
                '    'End Using
                '    If iResultado = 1 Then
                '        mMostrarGrilla()
                '    End If
                'Else
                '    Exit Sub
                'End If
                MsgBox("Opcion Bloqueada por el Administrador", MsgBoxStyle.Information, glbNameSistema)
            End If
            'scope.Complete()
            'End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try

    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""
        Dim Mensaje As String = ""

        eTempo = New clsPlantTempo
        eEspecialidad = New clsEspecialidad
        If sTab = 1 Then
            If Len(Trim(txtCodigo.Text)) > 0 And Len(Trim(txtNombre.Text)) > 0 Then
                If iOpcion = 7 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 8 Then
                    Mensaje = "Actualizar"
                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 7 Then
                        eEspecialidad.fCodigo(2)
                        sCodigoRegistro = eEspecialidad.sCodFuturo
                        txtCodigo.Text = Trim(sCodigoRegistro)
                        iResultado = eEspecialidad.fGrabar(iOpcion, sCodigoRegistro, Convert.ToString(txtNombre.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue))
                        If iResultado > 0 Then
                            mMostrarGrilla()
                        End If
                        sTab = 0
                    ElseIf iOpcion = 8 Then
                        sCodigoRegistro = Trim(txtCodigo.Text)
                        iResultado = eEspecialidad.fGrabar(iOpcion, sCodigoRegistro, Convert.ToString(txtNombre.Text.Trim), Convert.ToString(Me.cboEstado.SelectedValue))
                        If iResultado > 0 Then
                            mMostrarGrilla()
                        End If
                        sTab = 0
                    End If
                End If
            Else
                If Len(Trim(txtNombre.Text)) = 0 Then
                    MessageBox.Show("Ingrese la Descripcion del tipo de Especialidad", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNombre.Focus()
                    Exit Sub
                End If
                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        'If sTab = 0 Then
        sTab = 1
        VerPosicion()
        DesabilitarControles()
        'End If
    End Sub


End Class
