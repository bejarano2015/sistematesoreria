Imports CapaNegocios
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows
Imports System.Data.SqlClient

Public Class FrmREP_PolizasXPagar

    Private cSegurosPolizas As clsSegurosPolizas
    Dim DT_CuotasXPagar As DataTable

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As FrmREP_PolizasXPagar = Nothing

    Public Shared Function Instance() As FrmREP_PolizasXPagar
        If frmInstance Is Nothing Then
            frmInstance = New FrmREP_PolizasXPagar
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub FrmREP_PolizasXPagar_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmREP_PolizasXPagar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mtbFechaIni.Text = Convert.ToDateTime("01/01/" & Year(Today))
        mtbFechaFin.Text = Today
        mtbFechaIni.Focus()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        cSegurosPolizas = New clsSegurosPolizas
        DT_CuotasXPagar = New DataTable

        Try
            DT_CuotasXPagar.Clear()
            Me.Cursor = Cursors.WaitCursor


            'DT_CuotasXPagar = cSegurosPolizas.fREP_PolizasCuotasXPagar(gEmpresa, mtbFechaIni.Text, mtbFechaFin.Text)
            DT_CuotasXPagar = cSegurosPolizas.fREP_PolizasCuotasXPagar2(34, gEmpresa, mtbFechaIni.Text, mtbFechaFin.Text)
            If DT_CuotasXPagar.Rows.Count = 0 Then
                MessageBox.Show("No hay Informacion que mostrar", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            'Muestra_Reporte("D:\MIGUEL\FUENTES TESORERIA\Sistema Tesoreria21\CapaPresentacion\Reportes\" & "CrPolizasCuotas_x_Pagar.rpt", DT_CuotasXPagar, "tblCuotasXPagar", "", _
            Muestra_Reporte(RutaAppReportes & "CrPolizasCuotas_x_Pagar3.rpt", DT_CuotasXPagar, "tblCuotasXPagar1", "", _
            "StrEmpresa;" & cSegurosPolizas.DescripcionEmpresa(gEmpresa), _
            "StrRUC;" & cSegurosPolizas.RucEmpresa(gEmpresa), _
            "Criterio;" & mtbFechaIni.Text & " HASTA " & mtbFechaFin.Text)
            '"StrUltDiaMes;" & StrDia & " DE " & cboMes.Text & " DEL " & sPeriodo, _
            '"StrFoN;" & FoN, _
            '"StrPeriodo;" & sPeriodo, _
            '"StrMoneda;" & cboMoneda.Text)

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            MessageBox.Show("Verifique las Fechas Ingresadas", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            mtbFechaIni.Focus()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, _
            ByVal myDatos As Object, ByVal STRnombreTabla As String, _
            ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New FrmREPView_PolCuotasXPagar
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)
            'myReporte.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CrystalReportViewer1.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CrystalReportViewer1.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CrystalReportViewer1.ReportSource = myReporte
            f.CrystalReportViewer1.DisplayGroupTree = False
            f.strNombreFom = "Reporte de Polizas por Pagar"
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class