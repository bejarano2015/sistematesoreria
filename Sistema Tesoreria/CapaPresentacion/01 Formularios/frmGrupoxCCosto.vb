Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic

Public Class frmGrupoxCCosto

    Dim WithEvents cmr As CurrencyManager
    Private eTipoGastosG1 As clsTipoGastosG1
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Dim NumFila As Integer
    Dim Fila As DataRow
    Dim iOpcion As Integer

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmGrupoxCCosto = Nothing
    Public Shared Function Instance() As frmGrupoxCCosto
        If frmInstance Is Nothing Then
            frmInstance = New frmGrupoxCCosto
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmGrupoxCCosto_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmGrupoxCCosto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'eTipoGastosG1 = New clsTipoGastosG1
        mMostrarGrilla()
        dgvLista.Height = 263
        dgvLista.Width = 326
    End Sub

    Private Sub mMostrarGrilla()
        eTipoGastosG1 = New clsTipoGastosG1
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        dtTableLista = eTipoGastosG1.fListarGrupoCC()
        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTableLista
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & eTipoGastosG1.iNroRegistros
        If eTipoGastosG1.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
        '
    End Sub


    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                'txtCodigo.Text = Trim(Fila("IdSubGasto").ToString)
                'txtDescripcion.Text = Trim(Fila("DescSubGasto").ToString)
                'If Fila("Estado").ToString = 0 Then
                'Me.cboEstado.SelectedIndex = 0
                'Else
                'Me.cboEstado.SelectedIndex = 1
                'End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub btnSeleccionar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeleccionar.Click
        If Me.dgvLista.RowCount > 0 Then
            Dim x As frmAgruparCC = frmAgruparCC.Instance
            VerPosicion()
            x.strCodigoGrupo = Trim(Fila("IdGrupo").ToString)
            x.strDesGrupo = Trim(Fila("DesGrupo").ToString)
            'x.strCodigoGrupoGeneral = Trim(cboCategoria.SelectedValue)
            'x.strNombreUsuario = Trim(Fila("UsuCodigo").ToString)
            x.Owner = Me
            x.ShowInTaskbar = False
            x.ShowDialog()
        End If
    End Sub
End Class
