Imports CapaNegocios
Imports System.Data


Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows

Public Class frmCronogramaPoliza
    Private cSegurosPolizas As clsSegurosPolizas
    Friend FrmMantPoliza As New frmMantePoliza
    Dim Dset As New DataSet
    Dim Dt_EstadoPoliza As DataTable
    Dim Dt_EndososPoliza As DataTable
    Public sCodPolizaOriginal As String = ""
    Friend sCodPoliza As String = String.Empty
    Friend sDesRamo As String = String.Empty

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCronogramaPoliza = Nothing

    Public Shared Function Instance() As frmCronogramaPoliza
        If frmInstance Is Nothing Then
            frmInstance = New frmCronogramaPoliza
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmCronogramaPoliza_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click
        Me.Close()
    End Sub
#End Region

    Private Sub frmCronogramaPoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cSegurosPolizas = New clsSegurosPolizas
        Dt_EstadoPoliza = New DataTable
        Dt_EndososPoliza = New DataTable

        Dset = cSegurosPolizas.fMostrarCronograma(gEmpresa, Trim(sCodPolizaOriginal))
        Dt_EndososPoliza = cSegurosPolizas.fMostrarCronograma3(gEmpresa, Trim(sCodPolizaOriginal), "")
        'sCodPolizaOriginal = FrmMantPoliza.DgvLista.Rows(FrmMantPoliza.DgvLista.CurrentRow.Index).Cells("Poliza").Value
        'Dt_EstadoPoliza = cSegurosPolizas.ListarEstadoPoliza
        txtAseguradora.Text = Dset.Tables(0).Rows(0).Item("Aseguradora")
        txtPoliza.Text = Dset.Tables(0).Rows(0).Item("Poliza")
        txtDesRamo.Text = Dset.Tables(0).Rows(0).Item("Ramo")
        txtVigencia.Text = Dset.Tables(0).Rows(0).Item("Vig.Inicio") & " Hasta " & Dset.Tables(0).Rows(0).Item("Vig.Termino")
        txtMoneda.Text = Dset.Tables(0).Rows(0).Item("MonDescripcion")
        txtSubTotal.Text = Format(Dset.Tables(0).Rows(0).Item("SubTotal"), "#,##0.00")
        txtInteres.Text = Format(Dset.Tables(0).Rows(0).Item("Interes"), "#,##0.00")
        txtTotal.Text = Format(Dset.Tables(0).Rows(0).Item("Total"), "#,##0.00")

        'For i As Integer = 0 To Dt_EstadoPoliza.Rows.Count - 1
        '    Estado.Items.Add(Trim(Dt_EstadoPoliza.Rows(i).Item(0).ToString) & " - " & Dt_EstadoPoliza.Rows(i).Item(1).ToString)
        'Next

        'dgvCronograma.Rows.Clear()
        'If Dset.Tables(1).Rows.Count - 1 Then
        '    For iFilas As Integer = 0 To Dset.Tables(1).Rows.Count - 1
        '        dgvCronograma.Rows.Add()
        '        dgvCronograma.Rows(iFilas).Cells("Cuota").Value = Dset.Tables(1).Rows(iFilas).Item(0)
        '        dgvCronograma.Rows(iFilas).Cells("Cuota").ReadOnly = True
        '        dgvCronograma.Rows(iFilas).Cells("FechaVencimiento").Value = Format(Dset.Tables(1).Rows(iFilas).Item(1), "dd/MM/yyyy")
        '        dgvCronograma.Rows(iFilas).Cells("FechaVencimiento").ReadOnly = True
        '        dgvCronograma.Rows(iFilas).Cells("Importe").Value = Format(Dset.Tables(1).Rows(iFilas).Item(2), "#,##0.00")
        '        dgvCronograma.Rows(iFilas).Cells("Estado").Value = Dset.Tables(1).Rows(iFilas).Item(3)
        '        If dgvCronograma.Rows(iFilas).Cells("Estado").Value = "CANCELADO" Or dgvCronograma.Rows(iFilas).Cells("Estado").Value = "PROGRAMADO" Then
        '            dgvCronograma.Rows(iFilas).Cells("Boton").ReadOnly = True
        '        End If
        '        dgvCronograma.Rows(iFilas).Cells("Fecha_Cancelacion").Value = Format(Dset.Tables(1).Rows(iFilas).Item(4), "dd/MM/yyyy")
        '    Next
        'End If

        'For xCol As Integer = 0 To dgvCronograma.Columns.Count - 1
        '    dgvCronograma.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
        '    dgvCronograma.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        'Next

        If Dt_EndososPoliza.Rows.Count > 0 Then
            cboEndosos.DataSource = Dt_EndososPoliza
            cboEndosos.ValueMember = "Codigo"
            cboEndosos.DisplayMember = "Codigo"
            cboEndosos.SelectedIndex = -1
        End If

        dgvCronograma.Focus()

    End Sub

    Private Sub dgvCronograma_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCronograma.CellClick
        Dim Mensaje As String = ""
        Dim Mensaje2 As String = ""
        Dim CodigoPolizaBD As String = ""
        Dim CodigoSubPolizaBD As String = ""

        If cboEndosos.SelectedIndex > -1 Then
            CodigoPolizaBD = Trim(sCodPolizaOriginal)
            CodigoSubPolizaBD = Trim(cboEndosos.SelectedValue)
            'Mensaje = "�Desea Programar la Cuota Nro." & dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value & " del Endoso " & cboEndosos.SelectedValue & " para el Pago correspondiente ?"
            Mensaje = "�Desea Asignar como Pendiente la Cuota Nro." & dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value & " del Endoso " & cboEndosos.SelectedValue & " para el Pago correspondiente ?"
            Mensaje2 = "�Desea Cancelar la Cuota Nro." & dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value & " del Endoso " & cboEndosos.SelectedValue & " para el Pago correspondiente ?"
        End If

        If rdbPoliza.Checked = True Then
            CodigoPolizaBD = Trim(sCodPolizaOriginal)
            CodigoSubPolizaBD = Trim(sCodPolizaOriginal)
            'Mensaje = "�Desea Programar la Cuota Nro. " & dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value & " para el Pago correspondiente?"
            Mensaje = "�Desea Asignar como Pendiente la Cuota Nro. " & dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value & " para el Pago correspondiente?"
            Mensaje2 = "�Desea Cancelar la Cuota Nro. " & dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value & " para el Pago correspondiente?"

        End If


        If e.RowIndex <> -1 Then


            If dgvCronograma.Columns(e.ColumnIndex).Name = "Boton" Then
                If MessageBox.Show(Mensaje, "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    cSegurosPolizas.fActualizarEstado(gEmpresa, CodigoSubPolizaBD, CodigoPolizaBD, dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value, "P", gUsuario)
                    rdbPoliza_CheckedChanged(sender, e)
                End If
            End If

            If dgvCronograma.Columns(e.ColumnIndex).Name = "BotonCancela" Then
                If MessageBox.Show(Mensaje2, "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                     cSegurosPolizas.fActualizarEstado(gEmpresa, CodigoSubPolizaBD, CodigoPolizaBD, dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value, "C", gUsuario)
                    rdbPoliza_CheckedChanged(sender, e)
                End If
            End If

            'End If


            'If dgvCronograma.Rows(e.RowIndex).Cells("Estado").Value <> "PROGRAMADO" Then
            '    If dgvCronograma.Rows(e.RowIndex).Cells("Estado").Value <> "CANCELADO" Then
            '        If dgvCronograma.Columns(e.ColumnIndex).Name = "Boton" Then
            '            If MessageBox.Show(Mensaje, "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '                'cSegurosPolizas.fActualizarEstado(gEmpresa, txtPoliza.Text, dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value, Microsoft.VisualBasic.Left(dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Estado").Value, 1), gUsuario)
            '                cSegurosPolizas.fActualizarEstado(gEmpresa, CodigoSubPolizaBD, CodigoPolizaBD, dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value, "R", gUsuario)
            '                'dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Estado").ReadOnly = True
            '                'frmCronogramaPoliza_Load(sender, e)
            '                rdbPoliza_CheckedChanged(sender, e)
            '            End If
            '        End If

            '        If dgvCronograma.Columns(e.ColumnIndex).Name = "BotonCancela" Then
            '            If MessageBox.Show(Mensaje2, "Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '                'cSegurosPolizas.fActualizarEstado(gEmpresa, txtPoliza.Text, dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value, Microsoft.VisualBasic.Left(dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Estado").Value, 1), gUsuario)
            '                cSegurosPolizas.fActualizarEstado(gEmpresa, CodigoSubPolizaBD, CodigoPolizaBD, dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Cuota").Value, "C", gUsuario)
            '                'dgvCronograma.Rows(dgvCronograma.CurrentRow.Index).Cells("Estado").ReadOnly = True
            '                'frmCronogramaPoliza_Load(sender, e)
            '                rdbPoliza_CheckedChanged(sender, e)
            '            End If
            '        End If

            '    End If
            'End If

        End If
    End Sub

    Private Sub BeButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton2.Click

        Dim frmDetaPoliza As New FrmDetallePoliza
        frmDetaPoliza.FrmCronograma = Me
        frmDetaPoliza.CodPoliza = Trim(txtPoliza.Text)
        frmDetaPoliza.ShowDialog()

    End Sub

    Private Sub dgvCronograma_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvCronograma.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                e.Handled = True
        End Select
    End Sub

    Private Sub rdbPoliza_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbPoliza.CheckedChanged
        cSegurosPolizas = New clsSegurosPolizas
        Dim Dt_Cronograma As DataTable
        Dt_Cronograma = New DataTable

        'Dt_Datos = New DataTable
        'Dt_EndososPoliza = New DataTable
        'dgvCronograma.Rows.Clear()
        If rdbPoliza.Checked = True And rdbEndosos.Checked = False Then
            Dt_Cronograma = cSegurosPolizas.fMostrarCronograma2(32, gEmpresa, Trim(sCodPolizaOriginal), Trim(sCodPolizaOriginal))
            cboEndosos.SelectedIndex = -1
        ElseIf rdbPoliza.Checked = False And rdbEndosos.Checked = True Then
            If cboEndosos.SelectedIndex > -1 Then
                Dt_Cronograma = cSegurosPolizas.fMostrarCronograma2(32, gEmpresa, Trim(cboEndosos.SelectedValue), Trim(sCodPolizaOriginal))
            ElseIf cboEndosos.SelectedIndex = -1 Then
                dgvCronograma.Rows.Clear()
                'seleccione un endoso
                Exit Sub
            End If
        End If

        dgvCronograma.Rows.Clear()

        If Dt_Cronograma.Rows.Count - 1 Then
            For iFilas As Integer = 0 To Dt_Cronograma.Rows.Count - 1
                dgvCronograma.Rows.Add()
                dgvCronograma.Rows(iFilas).Cells("Cuota").Value = Dt_Cronograma.Rows(iFilas).Item(0)
                dgvCronograma.Rows(iFilas).Cells("Cuota").ReadOnly = True
                dgvCronograma.Rows(iFilas).Cells("FechaVencimiento").Value = Format(Dt_Cronograma.Rows(iFilas).Item(1), "dd/MM/yyyy")
                dgvCronograma.Rows(iFilas).Cells("FechaVencimiento").ReadOnly = True
                dgvCronograma.Rows(iFilas).Cells("Importe").Value = Format(Dt_Cronograma.Rows(iFilas).Item(2), "#,##0.00")
                dgvCronograma.Rows(iFilas).Cells("Estado").Value = Dt_Cronograma.Rows(iFilas).Item(3)
                If dgvCronograma.Rows(iFilas).Cells("Estado").Value = "CANCELADO" Or dgvCronograma.Rows(iFilas).Cells("Estado").Value = "PROGRAMADO" Then
                    dgvCronograma.Rows(iFilas).Cells("Boton").ReadOnly = True
                End If
                dgvCronograma.Rows(iFilas).Cells("Fecha_Cancelacion").Value = Format(Dt_Cronograma.Rows(iFilas).Item(4), "dd/MM/yyyy")
            Next
        End If

        For xCol As Integer = 0 To dgvCronograma.Columns.Count - 1
            dgvCronograma.Columns(xCol).SortMode = DataGridViewColumnSortMode.NotSortable
            'dgvCronograma.Columns(xCol).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        Next
    End Sub

    Private Sub rdbEndosos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbEndosos.CheckedChanged
        rdbPoliza_CheckedChanged(sender, e)
    End Sub

    Private Sub cboEndosos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEndosos.SelectedIndexChanged
        rdbPoliza_CheckedChanged(sender, e)
    End Sub

    Private Sub dgvCronograma_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCronograma.CellContentClick

    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Dim sOpcion As Integer
        Dim DT_Polizas_Detalle As DataTable
        cSegurosPolizas = New clsSegurosPolizas
        DT_Polizas_Detalle = New DataTable

        DT_Polizas_Detalle.Clear()
        Me.Cursor = Cursors.WaitCursor

        'If ((chkVencidas.Checked = False And chkPorVencer.Checked = False) Or txtDias.Text.Length = 0) Then
        '    MessageBox.Show("Seleccione una de las casillas de Verificacion", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    Me.Cursor = Cursors.Default
        '    chkVencidas.Focus()
        '    Exit Sub
        'End If

        'If (chkVencidas.Checked = True And chkPorVencer.Checked = True) Then
        '    sOpcion = 3
        'ElseIf chkVencidas.Checked = True Then
        '    sOpcion = 1
        'ElseIf chkPorVencer.Checked = True Then
        '    sOpcion = 2
        'End If

        'DT_Polizas_Detalle = cSegurosPolizas.fREP_Cuotas_Vencidas_PorVencer(14, gEmpresa, IIf(cboCia.SelectedValue = Nothing, "", cboCia.SelectedValue), txtDias.Text.Trim, sOpcion)
        DT_Polizas_Detalle = cSegurosPolizas.fREP_Cuotas_Vencidas_PorVencer2(42, gEmpresa, "", Trim(txtPoliza.Text), 300, 3)
        If DT_Polizas_Detalle.Rows.Count = 0 Then
            MessageBox.Show("No hay Informacion que mostrar", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        'Muestra_Reporte("D:\MIGUEL\FUENTES TESORERIA\Sistema Tesoreria21\CapaPresentacion\Reportes\" & "CrCuotasVencidas_PorVencer.rpt", DT_Polizas_Detalle, "tblCuotas_Vencidas_PorVencer", "", _
        Muestra_Reporte(RutaAppReportes & "CrCuotasVencidas_PorVencer3.rpt", DT_Polizas_Detalle, "tblCuotas_Vencidas_PorVencer", "", _
        "StrEmpresa;" & cSegurosPolizas.DescripcionEmpresa(gEmpresa), _
        "StrRUC;" & cSegurosPolizas.RucEmpresa(gEmpresa))

        '"Criterio;" & "Desde: " & mtbFechaIni.Text & " Hasta:" & mtbFechaFin.Text)

        Me.Cursor = Cursors.Default
    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, _
            ByVal myDatos As Object, ByVal STRnombreTabla As String, _
            ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New FrmREPView_PolCuotasXPagar
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)
            'myReporte.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CrystalReportViewer1.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CrystalReportViewer1.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CrystalReportViewer1.ReportSource = myReporte
            f.CrystalReportViewer1.DisplayGroupTree = False
            f.strNombreFom = "Reporte de Cuotas Vencidas / Por Vencer"
            f.Show()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class