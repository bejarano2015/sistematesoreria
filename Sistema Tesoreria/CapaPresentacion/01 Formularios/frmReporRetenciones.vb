Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporRetenciones

    'Public nroArqueo As String
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    'Dim rptGastos As New rptGasGene
    'Dim rptGastos As New rptGasGeneDetallado
    Dim rptRetencion As New rptRetenciones

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmReporRetenciones = Nothing
    Public Shared Function Instance() As frmReporRetenciones
        If frmInstance Is Nothing Then
            frmInstance = New frmReporRetenciones
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

#End Region

    Private Sub frmReporGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'rptRetencion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        ''1
        'crParameterFieldDefinitions = rptRetencion.DataDefinition.ParameterFields()
        'crParameterFieldDefinition = crParameterFieldDefinitions("@IDRETENCIONCAB")
        'crParameterValues = crParameterFieldDefinition.CurrentValues
        'crParameterDiscreteValue = New ParameterDiscreteValue()
        'crParameterDiscreteValue.Value = frmRegistroRetencion.idRegistroDocumentoCab
        'crParameterValues.Add(crParameterDiscreteValue)
        'crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        'CrystalReportViewer1.ReportSource = rptRetencion
        ''rptRetencion.PrintToPrinter(1, False, 0, 1)
    End Sub

    Private Sub frmReporGastos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub
    Private Sub frmReporGastos_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

End Class