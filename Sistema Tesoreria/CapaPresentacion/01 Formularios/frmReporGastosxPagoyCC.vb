Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmReporGastosxPagoyCC

    Private eLibroBancos As clsLibroBancos
    Private eGastosGenerales As clsGastosGenerales

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporGastosxPagoyCC = Nothing
    Public Shared Function Instance() As frmReporGastosxPagoyCC
        If frmInstance Is Nothing Then
            frmInstance = New frmReporGastosxPagoyCC
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporGastosxPagoyCC_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporGastosxPagoyCC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'eLibroBancos = New clsLibroBancos
            'cboCentroCosto.DataSource = eLibroBancos.fListarTipoMovPagos()
            'fListarEstadoDocumentosCCostoyTG()

            eGastosGenerales = New clsGastosGenerales
            cboTipoGasto.DataSource = eGastosGenerales.fListarEstadoDocumentosCCostoyTG(4, gEmpresa, "", 0, Today(), Today())
            If eGastosGenerales.iNroRegistros > 0 Then
                cboTipoGasto.ValueMember = "CodTga"
                cboTipoGasto.DisplayMember = "DesTga"
                cboTipoGasto.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try

        Try
            eGastosGenerales = New clsGastosGenerales
            cboCentroCosto.DataSource = eGastosGenerales.fListarCentrosCostos(gEmpresa)
            If eGastosGenerales.iNroRegistros > 0 Then
                cboCentroCosto.ValueMember = "CCosCodigo"
                cboCentroCosto.DisplayMember = "CCosDescripcion"
                cboCentroCosto.SelectedIndex = -1
            End If
        Catch ex As Exception
        End Try
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        dtpFechaFin.Value = Now.Date()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        If (chkTCC.Checked = False And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex = -1 And cboTipoGasto.SelectedIndex = -1) Then
            MessageBox.Show("Seleccione un criterio de b�squeda.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If (chkTCC.Checked = True And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex = -1 And cboTipoGasto.SelectedIndex = -1) Then
            MessageBox.Show("Seleccione todos o un tipo de gasto.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If (chkTCC.Checked = False And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex > -1 And cboTipoGasto.SelectedIndex = -1) Then
            MessageBox.Show("Seleccione todos o un tipo de gasto.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


        If (chkTCC.Checked = False And chkTTM.Checked = True) And (cboCentroCosto.SelectedIndex = -1 And cboTipoGasto.SelectedIndex = -1) Then
            MessageBox.Show("Seleccione todos o un centro de costo.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If (chkTCC.Checked = False And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex = -1 And cboTipoGasto.SelectedIndex > -1) Then
            MessageBox.Show("Seleccione todos o un centro de costo.", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If

        Dim sResDet As String = ""
        If rdbRes.Checked = True Then
            sResDet = "R"
        End If
        If rdbDet.Checked = True Then
            sResDet = "D"
        End If
        'If cboCentroCosto.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione un Centro de Costo", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    cboCentroCosto.Focus()
        '    Exit Sub
        'End If

        'If cboTipoMov.SelectedIndex = -1 Then
        '    MessageBox.Show("Seleccione un Tipo de Gasto", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    cboTipoMov.Focus()
        '    Exit Sub
        'End If

        Try
            Dim dtTable2 As DataTable
            Dim pIdRetencion As String = ""
            Dim pSerie As String = ""
            dtTable2 = New DataTable
            eGastosGenerales = New clsGastosGenerales

            If (chkTCC.Checked = True And chkTTM.Checked = True) And (cboCentroCosto.SelectedIndex = -1 And cboTipoGasto.SelectedIndex = -1) Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCostoyTG(0, gEmpresa, "", 0, dtpFechaIni.Value, dtpFechaFin.Value)
            End If

            If (chkTCC.Checked = False And chkTTM.Checked = True) And (cboCentroCosto.SelectedIndex > -1 And cboTipoGasto.SelectedIndex = -1) Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCostoyTG(1, gEmpresa, Trim(cboCentroCosto.SelectedValue), 0, dtpFechaIni.Value, dtpFechaFin.Value)
            End If

            If (chkTCC.Checked = True And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex = -1 And cboTipoGasto.SelectedIndex > -1) Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCostoyTG(2, gEmpresa, "", cboTipoGasto.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
            End If

            If (chkTCC.Checked = False And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex > -1 And cboTipoGasto.SelectedIndex > -1) Then
                dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCostoyTG(3, gEmpresa, Trim(cboCentroCosto.SelectedValue), cboTipoGasto.SelectedValue, dtpFechaIni.Value, dtpFechaFin.Value)
            End If

            'If (chkTCC.Checked = False And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex > -1 And cboTipoGasto.SelectedIndex = -1) Then
            '    dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCostoyTG(1, gEmpresa, Trim(cboCentroCosto.SelectedValue), cboTipoGasto.SelectedIndex, dtpFechaIni.Value, dtpFechaFin.Value)
            'End If

            'If (chkTCC.Checked = False And chkTTM.Checked = False) And (cboCentroCosto.SelectedIndex = -1 And cboTipoGasto.SelectedIndex > -1) Then
            '    dtTable2 = eGastosGenerales.fListarEstadoDocumentosCCostoyTG(2, gEmpresa, Trim(cboCentroCosto.SelectedValue), cboTipoGasto.SelectedIndex, dtpFechaIni.Value, dtpFechaFin.Value)
            'End If

            If dtTable2.Rows.Count > 0 Then
                'If rdbCancelar.Checked = True Then
                Muestra_Reporte(RutaAppReportes & "rptGasGeneDetalladoCCyTG.rpt", dtTable2, "TblLibroDiario", "", "ResDet;" & Trim(sResDet), "@FechaIni;" & dtpFechaIni.Value, "@FechaFin;" & dtpFechaFin.Value)
                'End If
                'If rdbCancelados.Checked = True Then
                '    Muestra_Reporte(RutaAppReportes & "rptDocPagados.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & Trim(cboCentroCosto.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & Trim(cboCentroCosto.SelectedValue))
                'End If
                'If rdbCC.Checked = True Then
                '    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & Trim(cboCentroCosto.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & Trim(cboCentroCosto.SelectedValue))
                'End If
                'If chk3.Checked = True Then
                '    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                '    'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                'End If
                'If chk4.Checked = True Then
                '    Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente3.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                '    'Muestra_Reporte(RutaAppReportes & "rptDocPagadosCuentaCorriente2.rpt", dtTable2, "TblLibroDiario", "", "CCosto;" & "", "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "CodCC;" & "")
                'End If
                'Imprimir_Reporte(RutaAppReportes & "rptRetencion.rpt", dtTable2, "TblLibroDiario", "", "MontoLetras;" & MontoLetras)
            ElseIf dtTable2.Rows.Count = 0 Then
                Dim mensaje As String = ""
                'If rdbCancelar.Checked = True Then
                mensaje = "No se Encontro ningun Registro."
                'End If
                'If rdbCancelados.Checked = True Then
                'mensaje = "No se Encontro ningun Comprobante Cancelado."
                'End If
                'If rdbCC.Checked = True Then
                'mensaje = "No se Encontro Cuenta Corriente de Proveedores en este Centro de Costo."
                'End If
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub chkTCC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTCC.CheckedChanged
        If chkTCC.Checked = True Then
            cboCentroCosto.SelectedIndex = -1
        End If
    End Sub

    Private Sub chkTTM_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTTM.CheckedChanged
        If chkTTM.Checked = True Then
            cboTipoGasto.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboCentroCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCentroCosto.SelectedIndexChanged
        If cboCentroCosto.SelectedIndex > -1 Then
            chkTCC.Checked = False
        End If
    End Sub

    Private Sub cboTipoGasto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoGasto.SelectedIndexChanged
        If cboTipoGasto.SelectedIndex > -1 Then
            chkTTM.Checked = False
        End If
    End Sub

End Class