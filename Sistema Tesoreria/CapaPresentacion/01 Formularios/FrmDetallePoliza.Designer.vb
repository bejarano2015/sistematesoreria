<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetallePoliza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDetallePoliza))
        Me.txtNroPoliza = New ctrLibreria.Controles.BeTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtRamo = New ctrLibreria.Controles.BeTextBox
        Me.dgvDetallePoliza = New System.Windows.Forms.DataGridView
        Me.BeButton1 = New ctrLibreria.Controles.BeButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnSalir = New System.Windows.Forms.Button
        Me.btnGrabar = New System.Windows.Forms.Button
        Me.txtAseguradora = New ctrLibreria.Controles.BeTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.TipoPoliza = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.NumeroPoliza = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.Endoso = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.Banco = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.Fecha_Leasing = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Asegurado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CenCosto = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.SumaAsegurada = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Tasa = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PrimaNeta = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PrimaGastos = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FormaPago = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.PorcDeducible = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MinimoDeducible = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvDetallePoliza, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtNroPoliza
        '
        Me.txtNroPoliza.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroPoliza.BackColor = System.Drawing.Color.Ivory
        Me.txtNroPoliza.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroPoliza.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroPoliza.ForeColor = System.Drawing.Color.Black
        Me.txtNroPoliza.KeyEnter = True
        Me.txtNroPoliza.Location = New System.Drawing.Point(398, 12)
        Me.txtNroPoliza.Name = "txtNroPoliza"
        Me.txtNroPoliza.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroPoliza.ReadOnly = True
        Me.txtNroPoliza.ShortcutsEnabled = False
        Me.txtNroPoliza.Size = New System.Drawing.Size(104, 20)
        Me.txtNroPoliza.TabIndex = 1
        Me.txtNroPoliza.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNroPoliza.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(333, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Nro Poliza"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(534, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Ramo"
        '
        'txtRamo
        '
        Me.txtRamo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRamo.BackColor = System.Drawing.Color.Ivory
        Me.txtRamo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRamo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRamo.ForeColor = System.Drawing.Color.Black
        Me.txtRamo.KeyEnter = True
        Me.txtRamo.Location = New System.Drawing.Point(577, 12)
        Me.txtRamo.Name = "txtRamo"
        Me.txtRamo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRamo.ReadOnly = True
        Me.txtRamo.ShortcutsEnabled = False
        Me.txtRamo.Size = New System.Drawing.Size(261, 20)
        Me.txtRamo.TabIndex = 2
        Me.txtRamo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'dgvDetallePoliza
        '
        Me.dgvDetallePoliza.AllowUserToAddRows = False
        Me.dgvDetallePoliza.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetallePoliza.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetallePoliza.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetallePoliza.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TipoPoliza, Me.NumeroPoliza, Me.Endoso, Me.Banco, Me.Fecha_Leasing, Me.Asegurado, Me.CenCosto, Me.SumaAsegurada, Me.Tasa, Me.PrimaNeta, Me.PrimaGastos, Me.FormaPago, Me.PorcDeducible, Me.MinimoDeducible})
        Me.dgvDetallePoliza.Location = New System.Drawing.Point(13, 54)
        Me.dgvDetallePoliza.Name = "dgvDetallePoliza"
        Me.dgvDetallePoliza.RowHeadersVisible = False
        Me.dgvDetallePoliza.Size = New System.Drawing.Size(983, 240)
        Me.dgvDetallePoliza.TabIndex = 0
        '
        'BeButton1
        '
        Me.BeButton1.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BeButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton1.Location = New System.Drawing.Point(920, 217)
        Me.BeButton1.Name = "BeButton1"
        Me.BeButton1.Size = New System.Drawing.Size(75, 23)
        Me.BeButton1.TabIndex = 28
        Me.BeButton1.Text = "BeButton1"
        Me.BeButton1.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btnSalir)
        Me.GroupBox1.Controls.Add(Me.btnGrabar)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.GroupBox1.Location = New System.Drawing.Point(832, 298)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(166, 42)
        Me.GroupBox1.TabIndex = 112
        Me.GroupBox1.TabStop = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnSalir.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.Location = New System.Drawing.Point(86, 10)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(72, 27)
        Me.btnSalir.TabIndex = 1
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnGrabar
        '
        Me.btnGrabar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGrabar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGrabar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnGrabar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGrabar.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.Location = New System.Drawing.Point(10, 10)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(72, 27)
        Me.btnGrabar.TabIndex = 0
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGrabar.UseVisualStyleBackColor = False
        '
        'txtAseguradora
        '
        Me.txtAseguradora.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAseguradora.BackColor = System.Drawing.Color.Ivory
        Me.txtAseguradora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAseguradora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAseguradora.ForeColor = System.Drawing.Color.Black
        Me.txtAseguradora.KeyEnter = True
        Me.txtAseguradora.Location = New System.Drawing.Point(96, 12)
        Me.txtAseguradora.Name = "txtAseguradora"
        Me.txtAseguradora.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAseguradora.ReadOnly = True
        Me.txtAseguradora.ShortcutsEnabled = False
        Me.txtAseguradora.Size = New System.Drawing.Size(200, 20)
        Me.txtAseguradora.TabIndex = 180
        Me.txtAseguradora.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(18, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 114
        Me.Label3.Text = "Aseguradora"
        '
        'TipoPoliza
        '
        Me.TipoPoliza.HeaderText = "Endoso"
        Me.TipoPoliza.Name = "TipoPoliza"
        '
        'NumeroPoliza
        '
        Me.NumeroPoliza.HeaderText = "Numero"
        Me.NumeroPoliza.Name = "NumeroPoliza"
        '
        'Endoso
        '
        Me.Endoso.DataPropertyName = "CodigoEndoso"
        Me.Endoso.HeaderText = "Propiedad"
        Me.Endoso.Name = "Endoso"
        Me.Endoso.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Endoso.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Endoso.Width = 85
        '
        'Banco
        '
        Me.Banco.DataPropertyName = "IdBanco"
        Me.Banco.HeaderText = "Banco"
        Me.Banco.Name = "Banco"
        Me.Banco.Width = 150
        '
        'Fecha_Leasing
        '
        Me.Fecha_Leasing.DataPropertyName = "FechaLeasing"
        Me.Fecha_Leasing.HeaderText = "Fecha_Leasing"
        Me.Fecha_Leasing.Name = "Fecha_Leasing"
        Me.Fecha_Leasing.Width = 85
        '
        'Asegurado
        '
        Me.Asegurado.DataPropertyName = "DescripcionAsegurado"
        Me.Asegurado.HeaderText = "Asegurado"
        Me.Asegurado.MaxInputLength = 3000
        Me.Asegurado.Name = "Asegurado"
        Me.Asegurado.Width = 300
        '
        'CenCosto
        '
        Me.CenCosto.HeaderText = "CenCosto"
        Me.CenCosto.Name = "CenCosto"
        '
        'SumaAsegurada
        '
        Me.SumaAsegurada.DataPropertyName = "SumaAsegurada"
        Me.SumaAsegurada.HeaderText = "SumaAsegurada"
        Me.SumaAsegurada.Name = "SumaAsegurada"
        '
        'Tasa
        '
        Me.Tasa.DataPropertyName = "Tasa"
        Me.Tasa.HeaderText = "Tasa"
        Me.Tasa.Name = "Tasa"
        '
        'PrimaNeta
        '
        Me.PrimaNeta.DataPropertyName = "Prima"
        Me.PrimaNeta.HeaderText = "PrimaNeta"
        Me.PrimaNeta.Name = "PrimaNeta"
        '
        'PrimaGastos
        '
        Me.PrimaGastos.DataPropertyName = "Total"
        Me.PrimaGastos.HeaderText = "PrimaGastos"
        Me.PrimaGastos.Name = "PrimaGastos"
        '
        'FormaPago
        '
        Me.FormaPago.DataPropertyName = "CodCondCancelacion"
        Me.FormaPago.HeaderText = "FormaPago"
        Me.FormaPago.Name = "FormaPago"
        Me.FormaPago.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.FormaPago.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'PorcDeducible
        '
        Me.PorcDeducible.DataPropertyName = "Porcentaje_Deducible"
        Me.PorcDeducible.HeaderText = "% del Deducible"
        Me.PorcDeducible.Name = "PorcDeducible"
        Me.PorcDeducible.Width = 200
        '
        'MinimoDeducible
        '
        Me.MinimoDeducible.DataPropertyName = "Minimo_Deducible"
        Me.MinimoDeducible.HeaderText = "Minimo del Deducible"
        Me.MinimoDeducible.Name = "MinimoDeducible"
        Me.MinimoDeducible.Width = 200
        '
        'FrmDetallePoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.CancelButton = Me.BeButton1
        Me.ClientSize = New System.Drawing.Size(1007, 352)
        Me.Controls.Add(Me.txtAseguradora)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvDetallePoliza)
        Me.Controls.Add(Me.txtRamo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtNroPoliza)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BeButton1)
        Me.Name = "FrmDetallePoliza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detalle de Poliza"
        CType(Me.dgvDetallePoliza, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtNroPoliza As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtRamo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dgvDetallePoliza As System.Windows.Forms.DataGridView
    Friend WithEvents BeButton1 As ctrLibreria.Controles.BeButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents txtAseguradora As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TipoPoliza As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents NumeroPoliza As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Endoso As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Banco As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents Fecha_Leasing As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Asegurado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenCosto As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents SumaAsegurada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tasa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrimaNeta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrimaGastos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FormaPago As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents PorcDeducible As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MinimoDeducible As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
