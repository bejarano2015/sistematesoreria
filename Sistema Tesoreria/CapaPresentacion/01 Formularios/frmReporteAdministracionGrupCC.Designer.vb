<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteAdministracionGrupCC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGrabar = New System.Windows.Forms.Button
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.DgvLista = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.GrupoInverCapital = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GrupoAdmFijos = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GrupoAdmOca = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GrupoGastosObra = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.DgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnGrabar
        '
        Me.btnGrabar.Location = New System.Drawing.Point(302, 4)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(75, 23)
        Me.btnGrabar.TabIndex = 13
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(293, 41)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(56, 17)
        Me.CheckBox1.TabIndex = 12
        Me.CheckBox1.Text = "Todos"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'DgvLista
        '
        Me.DgvLista.AllowUserToAddRows = False
        Me.DgvLista.BackgroundColor = System.Drawing.Color.White
        Me.DgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.GrupoInverCapital, Me.GrupoAdmFijos, Me.GrupoAdmOca, Me.GrupoGastosObra})
        Me.DgvLista.EnableHeadersVisualStyles = False
        Me.DgvLista.Location = New System.Drawing.Point(-2, 64)
        Me.DgvLista.Name = "DgvLista"
        Me.DgvLista.RowHeadersVisible = False
        Me.DgvLista.Size = New System.Drawing.Size(393, 303)
        Me.DgvLista.TabIndex = 11
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "CCosCodigo"
        Me.Column1.HeaderText = "Código"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 70
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "CCosDescripcion"
        Me.Column2.HeaderText = "Centro de Costo"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 200
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Grupo"
        Me.Column3.HeaderText = "Activo"
        Me.Column3.Name = "Column3"
        '
        'GrupoInverCapital
        '
        Me.GrupoInverCapital.DataPropertyName = "GrupoInverCapital"
        Me.GrupoInverCapital.HeaderText = "GrupoInverCapital"
        Me.GrupoInverCapital.Name = "GrupoInverCapital"
        Me.GrupoInverCapital.Visible = False
        '
        'GrupoAdmFijos
        '
        Me.GrupoAdmFijos.DataPropertyName = "GrupoAdmFijos"
        Me.GrupoAdmFijos.HeaderText = "GrupoAdmFijos"
        Me.GrupoAdmFijos.Name = "GrupoAdmFijos"
        Me.GrupoAdmFijos.Visible = False
        '
        'GrupoAdmOca
        '
        Me.GrupoAdmOca.DataPropertyName = "GrupoAdmOca"
        Me.GrupoAdmOca.HeaderText = "GrupoAdmOca"
        Me.GrupoAdmOca.Name = "GrupoAdmOca"
        Me.GrupoAdmOca.Visible = False
        '
        'GrupoGastosObra
        '
        Me.GrupoGastosObra.DataPropertyName = "GrupoGastosObra"
        Me.GrupoGastosObra.HeaderText = "GrupoGastosObra"
        Me.GrupoGastosObra.Name = "GrupoGastosObra"
        Me.GrupoGastosObra.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Label1"
        '
        'frmReporteAdministracionGrupCC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(389, 367)
        Me.Controls.Add(Me.btnGrabar)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.DgvLista)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "frmReporteAdministracionGrupCC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Centro de Costos"
        CType(Me.DgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents DgvLista As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents GrupoInverCapital As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GrupoAdmFijos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GrupoAdmOca As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GrupoGastosObra As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
