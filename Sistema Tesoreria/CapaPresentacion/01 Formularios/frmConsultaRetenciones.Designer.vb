<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaRetenciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvGastos = New System.Windows.Forms.DataGridView
        Me.Anular = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.xEstado = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.xSerie = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Importe = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Moneda = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CCosto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.MonCod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdRetencion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Prestamo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EmpPrestataria = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdMovimiento = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdCheque = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdResumen = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtTexto = New ctrLibreria.Controles.BeTextBox
        Me.cboCriterio = New ctrLibreria.Controles.BeComboBox
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtSoles = New ctrLibreria.Controles.BeTextBox
        Me.btnAnular = New System.Windows.Forms.Button
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel28 = New ctrLibreria.Controles.BeLabel
        CType(Me.dgvGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvGastos
        '
        Me.dgvGastos.AllowUserToAddRows = False
        Me.dgvGastos.BackgroundColor = System.Drawing.Color.White
        Me.dgvGastos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGastos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Anular, Me.xEstado, Me.xSerie, Me.Fecha, Me.Column11, Me.Column2, Me.Column1, Me.Importe, Me.Column6, Me.Moneda, Me.Column7, Me.Column8, Me.CCosto, Me.Column4, Me.Column3, Me.MonCod, Me.IdRetencion, Me.Prestamo, Me.EmpPrestataria, Me.IdMovimiento, Me.Monto, Me.IdCheque, Me.IdResumen})
        Me.dgvGastos.EnableHeadersVisualStyles = False
        Me.dgvGastos.Location = New System.Drawing.Point(2, 60)
        Me.dgvGastos.Name = "dgvGastos"
        Me.dgvGastos.RowHeadersVisible = False
        Me.dgvGastos.Size = New System.Drawing.Size(1181, 234)
        Me.dgvGastos.TabIndex = 3
        '
        'Anular
        '
        Me.Anular.HeaderText = "Anular"
        Me.Anular.Name = "Anular"
        Me.Anular.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Anular.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Anular.Width = 40
        '
        'xEstado
        '
        Me.xEstado.DataPropertyName = "Estados"
        Me.xEstado.HeaderText = "Estado"
        Me.xEstado.Name = "xEstado"
        Me.xEstado.ReadOnly = True
        Me.xEstado.Width = 80
        '
        'xSerie
        '
        Me.xSerie.DataPropertyName = "Serie"
        Me.xSerie.HeaderText = "Serie"
        Me.xSerie.Name = "xSerie"
        Me.xSerie.ReadOnly = True
        Me.xSerie.Visible = False
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle5
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        Me.Fecha.Width = 70
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "Numero"
        Me.Column11.HeaderText = "Numero"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 75
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "Proveedor"
        Me.Column2.HeaderText = "Razon Social"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 165
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "Ruc"
        Me.Column1.HeaderText = "Ruc"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 80
        '
        'Importe
        '
        Me.Importe.DataPropertyName = "PorcentajeRet"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.Importe.DefaultCellStyle = DataGridViewCellStyle6
        Me.Importe.HeaderText = "Retenido"
        Me.Importe.Name = "Importe"
        Me.Importe.ReadOnly = True
        Me.Importe.Width = 80
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "TipoCambio"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle7
        Me.Column6.HeaderText = "T.C."
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 70
        '
        'Moneda
        '
        Me.Moneda.DataPropertyName = "MonSimbolo"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.Moneda.DefaultCellStyle = DataGridViewCellStyle8
        Me.Moneda.HeaderText = "Mon"
        Me.Moneda.Name = "Moneda"
        Me.Moneda.ReadOnly = True
        Me.Moneda.Width = 40
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "Abreviado"
        Me.Column7.HeaderText = "Tipo Mov"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 80
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "NroFormaPago"
        Me.Column8.HeaderText = "Numero"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Width = 70
        '
        'CCosto
        '
        Me.CCosto.DataPropertyName = "CCosDescripcion"
        Me.CCosto.HeaderText = "CCosto"
        Me.CCosto.Name = "CCosto"
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Beneficiario"
        Me.Column4.HeaderText = "Responsable"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 130
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Glosa"
        Me.Column3.HeaderText = "Concepto"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 130
        '
        'MonCod
        '
        Me.MonCod.DataPropertyName = "MonCodigo"
        Me.MonCod.HeaderText = "MonCod"
        Me.MonCod.Name = "MonCod"
        Me.MonCod.ReadOnly = True
        Me.MonCod.Visible = False
        '
        'IdRetencion
        '
        Me.IdRetencion.DataPropertyName = "IdRetencion"
        Me.IdRetencion.HeaderText = "IdRetencion"
        Me.IdRetencion.Name = "IdRetencion"
        Me.IdRetencion.ReadOnly = True
        Me.IdRetencion.Visible = False
        '
        'Prestamo
        '
        Me.Prestamo.DataPropertyName = "PrestamoX"
        Me.Prestamo.HeaderText = "Prestamo"
        Me.Prestamo.Name = "Prestamo"
        Me.Prestamo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.Prestamo.Visible = False
        '
        'EmpPrestataria
        '
        Me.EmpPrestataria.DataPropertyName = "EmprPres"
        Me.EmpPrestataria.HeaderText = "Prestataria"
        Me.EmpPrestataria.Name = "EmpPrestataria"
        '
        'IdMovimiento
        '
        Me.IdMovimiento.DataPropertyName = "IdMovimiento"
        Me.IdMovimiento.HeaderText = "IdMovimiento"
        Me.IdMovimiento.Name = "IdMovimiento"
        Me.IdMovimiento.Visible = False
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "Monto"
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Name = "Monto"
        Me.Monto.Visible = False
        '
        'IdCheque
        '
        Me.IdCheque.DataPropertyName = "IdCheque"
        Me.IdCheque.HeaderText = "IdCheque"
        Me.IdCheque.Name = "IdCheque"
        Me.IdCheque.Visible = False
        '
        'IdResumen
        '
        Me.IdResumen.DataPropertyName = "IdResumen"
        Me.IdResumen.HeaderText = "IdResumen"
        Me.IdResumen.Name = "IdResumen"
        Me.IdResumen.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtTexto)
        Me.GroupBox1.Controls.Add(Me.cboCriterio)
        Me.GroupBox1.Controls.Add(Me.btnImprimir)
        Me.GroupBox1.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox1.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(686, 54)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label4.Location = New System.Drawing.Point(513, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Fecha Final"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(424, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Fecha Inicial"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label2.Location = New System.Drawing.Point(130, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Texto"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Location = New System.Drawing.Point(2, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Criterio"
        '
        'txtTexto
        '
        Me.txtTexto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTexto.BackColor = System.Drawing.Color.Ivory
        Me.txtTexto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTexto.ForeColor = System.Drawing.Color.Black
        Me.txtTexto.KeyEnter = True
        Me.txtTexto.Location = New System.Drawing.Point(133, 27)
        Me.txtTexto.MaxLength = 100
        Me.txtTexto.Name = "txtTexto"
        Me.txtTexto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTexto.ShortcutsEnabled = False
        Me.txtTexto.Size = New System.Drawing.Size(288, 20)
        Me.txtTexto.TabIndex = 3
        Me.txtTexto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboCriterio
        '
        Me.cboCriterio.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCriterio.BackColor = System.Drawing.Color.Ivory
        Me.cboCriterio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCriterio.ForeColor = System.Drawing.Color.Black
        Me.cboCriterio.FormattingEnabled = True
        Me.cboCriterio.Items.AddRange(New Object() {"RUC", "RAZON SOCIAL", "NUMERO", "CONCEPTO", "RESPONSABLE", "CHEQUE", "CARTA", "TRANSFERENCIA"})
        Me.cboCriterio.KeyEnter = True
        Me.cboCriterio.Location = New System.Drawing.Point(6, 27)
        Me.cboCriterio.Name = "cboCriterio"
        Me.cboCriterio.Size = New System.Drawing.Size(121, 21)
        Me.cboCriterio.TabIndex = 1
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Location = New System.Drawing.Point(606, 25)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 8
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(516, 27)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaFin.TabIndex = 7
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(427, 27)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaIni.TabIndex = 5
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'Timer2
        '
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(965, 314)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Label5"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(388, 319)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(116, 13)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Total Retención S/."
        '
        'txtSoles
        '
        Me.txtSoles.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSoles.BackColor = System.Drawing.Color.Ivory
        Me.txtSoles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSoles.Enabled = False
        Me.txtSoles.ForeColor = System.Drawing.Color.Black
        Me.txtSoles.KeyEnter = True
        Me.txtSoles.Location = New System.Drawing.Point(508, 312)
        Me.txtSoles.MaxLength = 100
        Me.txtSoles.Name = "txtSoles"
        Me.txtSoles.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSoles.ShortcutsEnabled = False
        Me.txtSoles.Size = New System.Drawing.Size(85, 20)
        Me.txtSoles.TabIndex = 22
        Me.txtSoles.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSoles.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'btnAnular
        '
        Me.btnAnular.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnular.ForeColor = System.Drawing.Color.Red
        Me.btnAnular.Location = New System.Drawing.Point(2, 313)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(127, 23)
        Me.btnAnular.TabIndex = 25
        Me.btnAnular.Text = "Anular Retención"
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'Panel8
        '
        Me.Panel8.BackColor = System.Drawing.Color.Silver
        Me.Panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel8.Location = New System.Drawing.Point(845, 313)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(33, 14)
        Me.Panel8.TabIndex = 29
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(701, 314)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(138, 13)
        Me.BeLabel1.TabIndex = 30
        Me.BeLabel1.Text = "Pagos Por Préstamo"
        '
        'BeLabel28
        '
        Me.BeLabel28.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel28.AutoSize = True
        Me.BeLabel28.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel28.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel28.ForeColor = System.Drawing.Color.Red
        Me.BeLabel28.Location = New System.Drawing.Point(906, 41)
        Me.BeLabel28.Name = "BeLabel28"
        Me.BeLabel28.Size = New System.Drawing.Size(254, 13)
        Me.BeLabel28.TabIndex = 34
        Me.BeLabel28.Text = "F5 = Detallar Retencion con Comprobantes"
        '
        'frmConsultaRetenciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1184, 348)
        Me.Controls.Add(Me.BeLabel28)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.Panel8)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txtSoles)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvGastos)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmConsultaRetenciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Consulta de Retenciones"
        CType(Me.dgvGastos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvGastos As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTexto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboCriterio As ctrLibreria.Controles.BeComboBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtSoles As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel28 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Anular As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents xEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents xSerie As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Moneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MonCod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdRetencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prestamo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EmpPrestataria As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdMovimiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCheque As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdResumen As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
