Imports CapaNegocios
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows

Public Class frmCuotasVencidas_xVencer
    Private cSegurosPolizas As clsSegurosPolizas
    Dim DT_Polizas_Detalle As DataTable
#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmCuotasVencidas_xVencer = Nothing
    Public Shared Function Instance() As frmCuotasVencidas_xVencer
        If frmInstance Is Nothing Then
            frmInstance = New frmCuotasVencidas_xVencer
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmCuotasVencidas_xVencer_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region
    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    Private Sub frmCuotasVencidas_xVencer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cSegurosPolizas = New clsSegurosPolizas
        cboCia.DataSource = cSegurosPolizas.ListarAseguradoras(gEmpresa)
        cboCia.ValueMember = "Codigo"
        cboCia.DisplayMember = "Descripcion"
        cboCia.SelectedIndex = -1

        cboCia.Enabled = True
        chkTodos.Checked = False
        chkVencidas.Checked = True
        chkPorVencer.Checked = True
        txtDias.Text = 0
        cboCia.Focus()
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Dim sOpcion As Integer
        cSegurosPolizas = New clsSegurosPolizas
        DT_Polizas_Detalle = New DataTable

        DT_Polizas_Detalle.Clear()
        Me.Cursor = Cursors.WaitCursor

        If ((chkVencidas.Checked = False And chkPorVencer.Checked = False) Or txtDias.Text.Length = 0) Then
            MessageBox.Show("Seleccione una de las casillas de Verificacion", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Cursor = Cursors.Default
            chkVencidas.Focus()
            Exit Sub
        End If

        If (chkVencidas.Checked = True And chkPorVencer.Checked = True) Then
            sOpcion = 3
        ElseIf chkVencidas.Checked = True Then
            sOpcion = 1
        ElseIf chkPorVencer.Checked = True Then
            sOpcion = 2
        End If

        'DT_Polizas_Detalle = cSegurosPolizas.fREP_Cuotas_Vencidas_PorVencer(14, gEmpresa, IIf(cboCia.SelectedValue = Nothing, "", cboCia.SelectedValue), txtDias.Text.Trim, sOpcion)
        DT_Polizas_Detalle = cSegurosPolizas.fREP_Cuotas_Vencidas_PorVencer(36, gEmpresa, IIf(cboCia.SelectedValue = Nothing, "", cboCia.SelectedValue), txtDias.Text.Trim, sOpcion)
        If DT_Polizas_Detalle.Rows.Count = 0 Then
            MessageBox.Show("No hay Informacion que mostrar", "Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        'Muestra_Reporte("D:\MIGUEL\FUENTES TESORERIA\Sistema Tesoreria21\CapaPresentacion\Reportes\" & "CrCuotasVencidas_PorVencer.rpt", DT_Polizas_Detalle, "tblCuotas_Vencidas_PorVencer", "", _
        Muestra_Reporte(RutaAppReportes & "CrCuotasVencidas_PorVencer2.rpt", DT_Polizas_Detalle, "tblCuotas_Vencidas_PorVencer", "", _
        "StrEmpresa;" & cSegurosPolizas.DescripcionEmpresa(gEmpresa), _
        "StrRUC;" & cSegurosPolizas.RucEmpresa(gEmpresa))

        '"Criterio;" & "Desde: " & mtbFechaIni.Text & " Hasta:" & mtbFechaFin.Text)

        Me.Cursor = Cursors.Default
    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, _
            ByVal myDatos As Object, ByVal STRnombreTabla As String, _
            ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New FrmREPView_PolCuotasXPagar
            Dim myReporte As New ReportDocument
            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)
            'myReporte.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CrystalReportViewer1.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CrystalReportViewer1.SelectionFormula = STRfiltro
            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte
            'Levanto el formulario del reporte
            f.CrystalReportViewer1.ReportSource = myReporte
            f.CrystalReportViewer1.DisplayGroupTree = False
            f.strNombreFom = "Reporte de Cuotas Vencidas / Por Vencer"
            f.Show()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub chkTodos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTodos.CheckedChanged
        If chkTodos.Checked = True Then
            cboCia.SelectedIndex = -1
            cboCia.Enabled = False
        ElseIf chkTodos.Checked = False Then
            cboCia.Enabled = True
        End If
    End Sub
    Private Sub chkTodos_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles chkTodos.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                chkVencidas.Focus()
        End Select
    End Sub
    Private Sub chkVencidas_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles chkVencidas.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                chkPorVencer.Focus()
        End Select
    End Sub
    Private Sub chkPorVencer_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles chkPorVencer.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                txtDias.Focus()
        End Select
    End Sub

    
End Class