Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared


Public Class frmReporPrestamos

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptGastos As New rptPrestamos

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporPrestamos = Nothing
    Public Shared Function Instance() As frmReporPrestamos
        If frmInstance Is Nothing Then
            frmInstance = New frmReporPrestamos
        End If
        frmInstance.BringToFront()
        Return frmInstance
  End Function

  Private Sub frmReporPrestamos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    'frmPrincipal.Barra.Enabled = False
  End Sub
    Private Sub frmReporPrestamos_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        rptGastos.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '1
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaIni")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = dtpFechaIni.Value
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '2
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@FechaFin")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = dtpFechaFin.Value
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '3
        crParameterFieldDefinitions = rptGastos.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = gEmpresa
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        CrystalReportViewer1.ReportSource = rptGastos

    End Sub

    Private Sub frmReporPrestamos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'dtpFechaIni.Value = "01/01" & "/" & Now.Year()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        dtpFechaFin.Value = Now.Date()
    End Sub
End Class