<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmParametrosRetencion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnGrabar = New ctrLibreria.Controles.BeButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.txtImp = New ctrLibreria.Controles.BeTextBox
        Me.txtPor = New ctrLibreria.Controles.BeTextBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnGrabar
        '
        Me.btnGrabar.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnGrabar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Save_
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(319, 37)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(92, 30)
        Me.btnGrabar.TabIndex = 2
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BeLabel1)
        Me.GroupBox1.Controls.Add(Me.BeLabel9)
        Me.GroupBox1.Controls.Add(Me.txtImp)
        Me.GroupBox1.Controls.Add(Me.txtPor)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(301, 87)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(11, 52)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(164, 13)
        Me.BeLabel1.TabIndex = 21
        Me.BeLabel1.Text = "Importe Para Retención S/."
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(11, 26)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(144, 13)
        Me.BeLabel9.TabIndex = 20
        Me.BeLabel9.Text = "Porcentaje Retencion %"
        '
        'txtImp
        '
        Me.txtImp.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImp.BackColor = System.Drawing.Color.Ivory
        Me.txtImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtImp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImp.ForeColor = System.Drawing.Color.Black
        Me.txtImp.KeyEnter = True
        Me.txtImp.Location = New System.Drawing.Point(178, 50)
        Me.txtImp.MaxLength = 20
        Me.txtImp.Name = "txtImp"
        Me.txtImp.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImp.ShortcutsEnabled = False
        Me.txtImp.Size = New System.Drawing.Size(112, 20)
        Me.txtImp.TabIndex = 19
        Me.txtImp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtImp.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtPor
        '
        Me.txtPor.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPor.BackColor = System.Drawing.Color.Ivory
        Me.txtPor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPor.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPor.ForeColor = System.Drawing.Color.Black
        Me.txtPor.KeyEnter = True
        Me.txtPor.Location = New System.Drawing.Point(178, 24)
        Me.txtPor.MaxLength = 20
        Me.txtPor.Name = "txtPor"
        Me.txtPor.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPor.ShortcutsEnabled = False
        Me.txtPor.Size = New System.Drawing.Size(112, 20)
        Me.txtPor.TabIndex = 18
        Me.txtPor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPor.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'frmParametrosRetencion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(419, 94)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnGrabar)
        Me.MaximizeBox = False
        Me.Name = "frmParametrosRetencion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Parametros de Retenciones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnGrabar As ctrLibreria.Controles.BeButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtImp As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPor As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
End Class
