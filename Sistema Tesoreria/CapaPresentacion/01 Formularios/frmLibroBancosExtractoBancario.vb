Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmLibroBancosExtractoBancario

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmLibroBancosExtractoBancario = Nothing
    Public Shared Function Instance() As frmLibroBancosExtractoBancario
        If frmInstance Is Nothing Then
            frmInstance = New frmLibroBancosExtractoBancario
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmLibroBancosExtractoBancario_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub



    Private Sub frmLibroBancos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eChequera As clsChequera
    Private eLibroBancos As clsLibroBancos
    Private eTempo As clsPlantTempo

    Dim iOpcion As Int16 = 0
    Dim sVisualizarDatos As String = ""
    Dim sCargarCargos As String = ""
    Private eITF As clsITF

    'Public sCodigoDetPosicion As String = ""

    Public CodCuenta As String = ""
    Public Mes As String = ""
    Public Anio As String = ""

    Public BancoDes As String = ""
    Public MonedaDes As String = ""
    Public NroCuentaDes As String = ""
    Public MesDes As String = ""

    Public SaldoInicialExtracto As Double = 0
    Public SaldoFinalExtracto As Double = 0
    Public SaldoExtracto As Double = 0
    'Dim dtDatos As DataTable
    Dim TBL As New DataTable

    Private Sub frmLibroBancosExtractoBancario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            BeLabel7.Text = Trim(BancoDes)
            BeLabel8.Text = Trim(NroCuentaDes)
            BeLabel9.Text = Trim(MonedaDes)
            BeLabel10.Text = Trim(Anio)
            BeLabel12.Text = Trim(MesDes)

            txtSaldoInicialExtracto.Text = Format(SaldoInicialExtracto, "#,##0.00")
            txtSaldoFinalExtracto.Text = Format(SaldoFinalExtracto, "#,##0.00")

            eLibroBancos = New clsLibroBancos
            Dim dtDatos As DataTable
            dtDatos = New DataTable
            dtDatos = eLibroBancos.TraerExtracto(Trim(Anio), Trim(Mes), Trim(CodCuenta), gEmpresa)
            '------------------------------------------------------------>
            Label1.Text = "Total de Registros : " & dtDatos.Rows.Count
            If dtDatos.Rows.Count > 0 Then

                Dim DrFila As DataRow
                Dim CadenaCon As String = ""
                Dim RazonSocial As String = ""
                Dim iResultado As Integer = 0
                'For a As Integer = 0 To dtDatos.Columns.Count - 1
                '    TBL.Columns.Add(a)
                'Next
                TBL.Columns.Add("Fecha")
                TBL.Columns.Add("NroDocumento")
                TBL.Columns.Add("Concepto")
                TBL.Columns.Add("Descripcion")
                TBL.Columns.Add("Abono")
                TBL.Columns.Add("Cargo")
                TBL.Columns.Add("IdLibroDet")
                TBL.Columns.Add("IdLibroCab")
                TBL.Columns.Add("FechaCobro")
                TBL.Columns.Add("Saldo")
                TBL.Columns.Add("TipoOperacion")
                TBL.Columns.Add("Importe")

                'Dim SaldoIniStr As Double = 50000
                'Dim SaldoIni As Double = 50000

                Dim SaldoFinal As Double = 0
                For x As Integer = 0 To dtDatos.Rows.Count - 1
                    DrFila = TBL.NewRow
                    'DrFila(0) = dtDatos.Rows(x).Item("Fecha")
                    DrFila("Fecha") = dtDatos.Rows(x).Item("Fecha")

                    'DrFila(1) = dtDatos.Rows(x).Item("NroDocumento")
                    DrFila("NroDocumento") = dtDatos.Rows(x).Item("NroDocumento")

                    'DrFila(2) = dtDatos.Rows(x).Item("Concepto")
                    DrFila("Concepto") = dtDatos.Rows(x).Item("Concepto")

                    'DrFila(3) = dtDatos.Rows(x).Item("Descripcion")
                    DrFila("Descripcion") = dtDatos.Rows(x).Item("Descripcion")

                    'DrFila(4) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Abono")), "#,##0.00")
                    DrFila("Abono") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Abono")), "#,##0.00")

                    'DrFila(5) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Cargo")), "#,##0.00")
                    DrFila("Cargo") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Cargo")), "#,##0.00")

                    'DrFila(6) = dtDatos.Rows(x).Item("IdLibroDet")
                    DrFila("IdLibroDet") = dtDatos.Rows(x).Item("IdLibroDet")

                    'DrFila(7) = dtDatos.Rows(x).Item("IdLibroCab")
                    DrFila("IdLibroCab") = dtDatos.Rows(x).Item("IdLibroCab")

                    'DrFila(8) = dtDatos.Rows(x).Item("FechaCobro")
                    DrFila("FechaCobro") = dtDatos.Rows(x).Item("FechaCobro")

                    If dtDatos.Rows(x).Item("TipoOperacion") = "D" Then
                        SaldoExtracto = SaldoExtracto + dtDatos.Rows(x).Item("Importe")
                    ElseIf dtDatos.Rows(x).Item("TipoOperacion") = "H" Then
                        SaldoExtracto = SaldoExtracto - dtDatos.Rows(x).Item("Importe")
                    End If
                    'DrFila(9) = Format(Convert.ToDouble(SaldoIni), "#,##0.00")
                    DrFila("Saldo") = Format(Convert.ToDouble(SaldoExtracto), "#,##0.00")

                    'DrFila(10) = dtDatos.Rows(x).Item("TipoOperacion")
                    DrFila("TipoOperacion") = dtDatos.Rows(x).Item("TipoOperacion")

                    'DrFila(11) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Importe")), "#,##0.00")
                    DrFila("Importe") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Importe")), "#,##0.00")

                    TBL.Rows.Add(DrFila)
                Next
                '------------------------------------------------------------>


                'Muestra_Reporte(RutaAppReportes & "rptLibroBancosEstractoMes.rpt", TBL, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFecha.Value & " al " & dtpFecha.Value, "Titulo;" & "ESTRACTO BANCARIO", "SIni;" & SaldoIniStr, "Banco;" & cboBancos.Text, "Mon;" & lblMoneda.Text, "Cta;" & cboCuentas.Text, "Anio;" & cboAno.Text, "Mes;" & cboMes.Text)
                'Muestra_Reporte(RutaAppReportes & "rptLibroBancosEstractoMes.rpt", dtDatos, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFecha.Value & " al " & dtpFecha.Value, "Titulo;" & "ABONOS POR CUENTAS")

                '----Muestra_Reporte(RutaAppReportes & "rptGastosEnviados.rpt", dtTable2, "TblLibroDiario", "", "Banco;" & Trim(cboBanco.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Cuenta;" & Trim(cboCuenta.Text), "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value & " - Usuario: " & UCase(Trim(glbNameUsuario)))

            End If
            dgvLibroDet.DataSource = TBL
            If dtDatos.Rows.Count = 0 Then
                Dim mensaje As String = ""
                mensaje = "No Se Encontraron Registros."
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click
        Try
            'eLibroBancos = New clsLibroBancos
            'Dim dtDatos As DataTable
            'dtDatos = New DataTable
            'dtDatos = eLibroBancos.TraerExtracto(Trim(cboAno.Text), TraerNumeroMes(Me.cboMes.Text), Trim(cboCuentas.SelectedValue), gEmpresa)
            ''------------------------------------------------------------>
            'If dtDatos.Rows.Count > 0 Then
            '    Dim TBL As New DataTable
            '    Dim DrFila As DataRow
            '    Dim CadenaCon As String = ""
            '    Dim RazonSocial As String = ""
            '    Dim iResultado As Integer = 0
            '    'For a As Integer = 0 To dtDatos.Columns.Count - 1
            '    '    TBL.Columns.Add(a)
            '    'Next
            '    TBL.Columns.Add("Fecha")
            '    TBL.Columns.Add("NroDocumento")
            '    TBL.Columns.Add("Concepto")
            '    TBL.Columns.Add("Descripcion")
            '    TBL.Columns.Add("Abono")
            '    TBL.Columns.Add("Cargo")
            '    TBL.Columns.Add("IdLibroDet")
            '    TBL.Columns.Add("IdLibroCab")
            '    TBL.Columns.Add("FechaCobro")
            '    TBL.Columns.Add("Saldo")
            '    TBL.Columns.Add("TipoOperacion")
            '    TBL.Columns.Add("Importe")
            '    Dim SaldoIniStr As Double = 50000
            '    Dim SaldoIni As Double = 50000
            '    Dim SaldoFinal As Double = 0
            '    For x As Integer = 0 To dtDatos.Rows.Count - 1
            '        DrFila = TBL.NewRow
            '        'DrFila(0) = dtDatos.Rows(x).Item("Fecha")
            '        DrFila("Fecha") = dtDatos.Rows(x).Item("Fecha")

            '        'DrFila(1) = dtDatos.Rows(x).Item("NroDocumento")
            '        DrFila("NroDocumento") = dtDatos.Rows(x).Item("NroDocumento")

            '        'DrFila(2) = dtDatos.Rows(x).Item("Concepto")
            '        DrFila("Concepto") = dtDatos.Rows(x).Item("Concepto")

            '        'DrFila(3) = dtDatos.Rows(x).Item("Descripcion")
            '        DrFila("Descripcion") = dtDatos.Rows(x).Item("Descripcion")

            '        'DrFila(4) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Abono")), "#,##0.00")
            '        DrFila("Abono") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Abono")), "#,##0.00")

            '        'DrFila(5) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Cargo")), "#,##0.00")
            '        DrFila("Cargo") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Cargo")), "#,##0.00")

            '        'DrFila(6) = dtDatos.Rows(x).Item("IdLibroDet")
            '        DrFila("IdLibroDet") = dtDatos.Rows(x).Item("IdLibroDet")

            '        'DrFila(7) = dtDatos.Rows(x).Item("IdLibroCab")
            '        DrFila("IdLibroCab") = dtDatos.Rows(x).Item("IdLibroCab")

            '        'DrFila(8) = dtDatos.Rows(x).Item("FechaCobro")
            '        DrFila("FechaCobro") = dtDatos.Rows(x).Item("FechaCobro")

            '        If dtDatos.Rows(x).Item("TipoOperacion") = "D" Then
            '            SaldoIni = SaldoIni + dtDatos.Rows(x).Item("Importe")
            '        ElseIf dtDatos.Rows(x).Item("TipoOperacion") = "H" Then
            '            SaldoIni = SaldoIni - dtDatos.Rows(x).Item("Importe")
            '        End If
            '        'DrFila(9) = Format(Convert.ToDouble(SaldoIni), "#,##0.00")
            '        DrFila("Saldo") = Format(Convert.ToDouble(SaldoIni), "#,##0.00")

            '        'DrFila(10) = dtDatos.Rows(x).Item("TipoOperacion")
            '        DrFila("TipoOperacion") = dtDatos.Rows(x).Item("TipoOperacion")

            '        'DrFila(11) = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Importe")), "#,##0.00")
            '        DrFila("Importe") = Format(Convert.ToDouble(dtDatos.Rows(x).Item("Importe")), "#,##0.00")

            '        TBL.Rows.Add(DrFila)
            '    Next
            '    '------------------------------------------------------------>
            '    Muestra_Reporte(RutaAppReportes & "rptLibroBancosEstractoMes.rpt", TBL, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFecha.Value & " al " & dtpFecha.Value, "Titulo;" & "ESTRACTO BANCARIO", "SIni;" & SaldoIniStr, "Banco;" & cboBancos.Text, "Mon;" & lblMoneda.Text, "Cta;" & cboCuentas.Text, "Anio;" & cboAno.Text, "Mes;" & cboMes.Text)
            '    'Muestra_Reporte(RutaAppReportes & "rptLibroBancosEstractoMes.rpt", dtDatos, "TblLibroDiario", "", "Criterio;" & "Del " & dtpFecha.Value & " al " & dtpFecha.Value, "Titulo;" & "ABONOS POR CUENTAS")
            '    '----Muestra_Reporte(RutaAppReportes & "rptGastosEnviados.rpt", dtTable2, "TblLibroDiario", "", "Banco;" & Trim(cboBanco.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Cuenta;" & Trim(cboCuenta.Text), "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value & " - Usuario: " & UCase(Trim(glbNameUsuario)))
            'End If
            'If dtDatos.Rows.Count = 0 Then
            '    Dim mensaje As String = ""
            '    mensaje = "No Se Encontraron Registros."
            '    MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            'End If


            If TBL.Rows.Count > 0 Then
                Muestra_Reporte(RutaAppReportes & "rptLibroBancosEstractoMes.rpt", TBL, "TblLibroDiario", "", "Criterio;" & "Del " & Today() & " al " & Today(), "Titulo;" & "ESTRACTO BANCARIO", "SIni;" & SaldoInicialExtracto, "Banco;" & BancoDes, "Mon;" & MonedaDes, "Cta;" & NroCuentaDes, "Anio;" & Anio, "Mes;" & MesDes)
            ElseIf TBL.Rows.Count = 0 Then
                MessageBox.Show("No Se Encontraron Registros.", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

           
        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Me.Cursor = Cursors.Default
    End Sub


    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub dgvLibroDet_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLibroDet.CellContentClick

    End Sub

    Private Sub dgvLibroDet_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvLibroDet.SelectionChanged
        If dgvLibroDet.Rows.Count > 0 Then
            Label2.Text = "Registro " & Convert.ToString(dgvLibroDet.CurrentRow.Index + 1) & " de " & Convert.ToString(dgvLibroDet.Rows.Count)
        End If
       
    End Sub
End Class