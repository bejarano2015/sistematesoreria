<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPartidas
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPartidas))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtCodigo = New ctrLibreria.Controles.BeTextBox()
        Me.txtContratista = New ctrLibreria.Controles.BeTextBox()
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox()
        Me.cboTipoDocumento = New ctrLibreria.Controles.BeComboBox()
        Me.txtCodigoContratista = New ctrLibreria.Controles.BeTextBox()
        Me.txtPartida = New ctrLibreria.Controles.BeTextBox()
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker()
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker()
        Me.txtUbicacion = New ctrLibreria.Controles.BeTextBox()
        Me.txtPresupuesto = New ctrLibreria.Controles.BeTextBox()
        Me.txtRetencion = New ctrLibreria.Controles.BeTextBox()
        Me.cboEspecialidad = New ctrLibreria.Controles.BeComboBox()
        Me.txtObservacion = New ctrLibreria.Controles.BeTextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboEstado = New System.Windows.Forms.ComboBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BeLabel23 = New ctrLibreria.Controles.BeLabel()
        Me.txtRuc = New System.Windows.Forms.TextBox()
        Me.cboTipoRetencion = New ctrLibreria.Controles.BeComboBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.dgvContratistas = New ctrLibreria.Controles.BeDataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contratista = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.esp_codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rdbEspecialidad = New System.Windows.Forms.RadioButton()
        Me.rdbNombres = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel()
        Me.txtBusqueda = New ctrLibreria.Controles.BeTextBox()
        Me.cboOpciones = New ctrLibreria.Controles.BeComboBox()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.dgvConsolidado = New System.Windows.Forms.DataGridView()
        Me.X = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ParDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UmdAbreviado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Metrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioPresupuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioSubcontrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EspCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TitCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ParCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConsCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.BeLabel22 = New ctrLibreria.Controles.BeLabel()
        Me.cboUbiTrabajo = New ctrLibreria.Controles.BeComboBox()
        Me.cboCCosto = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel19 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel()
        Me.txtSubTotal = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel()
        Me.txtIGV = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel()
        Me.cboObra = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Und = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.M1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Con_Metrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PU = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Con_PrecioSubContra = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.M2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Parcial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idDetalle = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Con_TitCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Con_ParCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Con_ConsCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.M2Anterior = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel20 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroContrato = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel21 = New ctrLibreria.Controles.BeLabel()
        Me.txtNumeroOrden = New ctrLibreria.Controles.BeTextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.BeLabel25 = New ctrLibreria.Controles.BeLabel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnLiquidar = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dgvLista = New ctrLibreria.Controles.BeDataGridView()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCosCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stsTotales = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.rdbObra = New System.Windows.Forms.RadioButton()
        Me.txtBuscar = New ctrLibreria.Controles.BeTextBox()
        Me.rdbPartida = New System.Windows.Forms.RadioButton()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvContratistas, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvConsolidado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stsTotales.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(978, 691)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.stsTotales)
        Me.TabPage1.Controls.Add(Me.dgvLista)
        Me.TabPage1.Size = New System.Drawing.Size(970, 665)
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Size = New System.Drawing.Size(970, 665)
        Me.TabPage2.UseVisualStyleBackColor = False
        '
        'txtCodigo
        '
        Me.txtCodigo.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigo.BackColor = System.Drawing.Color.Lavender
        Me.txtCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigo.ForeColor = System.Drawing.Color.Black
        Me.txtCodigo.KeyEnter = True
        Me.txtCodigo.Location = New System.Drawing.Point(107, 17)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigo.ShortcutsEnabled = False
        Me.txtCodigo.Size = New System.Drawing.Size(131, 20)
        Me.txtCodigo.TabIndex = 1
        Me.txtCodigo.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtContratista
        '
        Me.txtContratista.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtContratista.BackColor = System.Drawing.Color.LemonChiffon
        Me.txtContratista.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtContratista.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContratista.ForeColor = System.Drawing.Color.Black
        Me.txtContratista.KeyEnter = True
        Me.txtContratista.Location = New System.Drawing.Point(401, 64)
        Me.txtContratista.Name = "txtContratista"
        Me.txtContratista.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtContratista.ReadOnly = True
        Me.txtContratista.ShortcutsEnabled = False
        Me.txtContratista.Size = New System.Drawing.Size(333, 20)
        Me.txtContratista.TabIndex = 8
        Me.txtContratista.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(107, 87)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(131, 21)
        Me.cboMoneda.TabIndex = 10
        '
        'cboTipoDocumento
        '
        Me.cboTipoDocumento.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoDocumento.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDocumento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoDocumento.ForeColor = System.Drawing.Color.Black
        Me.cboTipoDocumento.FormattingEnabled = True
        Me.cboTipoDocumento.KeyEnter = True
        Me.cboTipoDocumento.Location = New System.Drawing.Point(343, 87)
        Me.cboTipoDocumento.Name = "cboTipoDocumento"
        Me.cboTipoDocumento.Size = New System.Drawing.Size(227, 21)
        Me.cboTipoDocumento.TabIndex = 11
        '
        'txtCodigoContratista
        '
        Me.txtCodigoContratista.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCodigoContratista.BackColor = System.Drawing.Color.Lavender
        Me.txtCodigoContratista.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodigoContratista.Enabled = False
        Me.txtCodigoContratista.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodigoContratista.ForeColor = System.Drawing.Color.Black
        Me.txtCodigoContratista.KeyEnter = True
        Me.txtCodigoContratista.Location = New System.Drawing.Point(107, 64)
        Me.txtCodigoContratista.Name = "txtCodigoContratista"
        Me.txtCodigoContratista.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCodigoContratista.ShortcutsEnabled = False
        Me.txtCodigoContratista.Size = New System.Drawing.Size(131, 20)
        Me.txtCodigoContratista.TabIndex = 7
        Me.txtCodigoContratista.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtPartida
        '
        Me.txtPartida.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPartida.BackColor = System.Drawing.Color.Ivory
        Me.txtPartida.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPartida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPartida.ForeColor = System.Drawing.Color.Black
        Me.txtPartida.KeyEnter = True
        Me.txtPartida.Location = New System.Drawing.Point(107, 111)
        Me.txtPartida.Multiline = True
        Me.txtPartida.Name = "txtPartida"
        Me.txtPartida.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPartida.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPartida.ShortcutsEnabled = False
        Me.txtPartida.Size = New System.Drawing.Size(463, 33)
        Me.txtPartida.TabIndex = 12
        Me.txtPartida.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(107, 170)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(131, 20)
        Me.dtpFechaIni.TabIndex = 15
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(343, 170)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(100, 20)
        Me.dtpFechaFin.TabIndex = 16
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'txtUbicacion
        '
        Me.txtUbicacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtUbicacion.BackColor = System.Drawing.Color.Ivory
        Me.txtUbicacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUbicacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUbicacion.ForeColor = System.Drawing.Color.Black
        Me.txtUbicacion.KeyEnter = True
        Me.txtUbicacion.Location = New System.Drawing.Point(107, 193)
        Me.txtUbicacion.Name = "txtUbicacion"
        Me.txtUbicacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtUbicacion.ShortcutsEnabled = False
        Me.txtUbicacion.Size = New System.Drawing.Size(461, 20)
        Me.txtUbicacion.TabIndex = 18
        Me.txtUbicacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtPresupuesto
        '
        Me.txtPresupuesto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPresupuesto.BackColor = System.Drawing.Color.Ivory
        Me.txtPresupuesto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPresupuesto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPresupuesto.Enabled = False
        Me.txtPresupuesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPresupuesto.ForeColor = System.Drawing.Color.Black
        Me.txtPresupuesto.KeyEnter = True
        Me.txtPresupuesto.Location = New System.Drawing.Point(822, 490)
        Me.txtPresupuesto.Name = "txtPresupuesto"
        Me.txtPresupuesto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPresupuesto.ShortcutsEnabled = False
        Me.txtPresupuesto.Size = New System.Drawing.Size(102, 20)
        Me.txtPresupuesto.TabIndex = 39
        Me.txtPresupuesto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPresupuesto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtRetencion
        '
        Me.txtRetencion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRetencion.BackColor = System.Drawing.Color.Ivory
        Me.txtRetencion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRetencion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRetencion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRetencion.ForeColor = System.Drawing.Color.Black
        Me.txtRetencion.KeyEnter = True
        Me.txtRetencion.Location = New System.Drawing.Point(498, 170)
        Me.txtRetencion.Name = "txtRetencion"
        Me.txtRetencion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRetencion.ShortcutsEnabled = False
        Me.txtRetencion.Size = New System.Drawing.Size(70, 20)
        Me.txtRetencion.TabIndex = 17
        Me.txtRetencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRetencion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'cboEspecialidad
        '
        Me.cboEspecialidad.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboEspecialidad.BackColor = System.Drawing.Color.Ivory
        Me.cboEspecialidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEspecialidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEspecialidad.ForeColor = System.Drawing.Color.Black
        Me.cboEspecialidad.FormattingEnabled = True
        Me.cboEspecialidad.KeyEnter = True
        Me.cboEspecialidad.Location = New System.Drawing.Point(107, 216)
        Me.cboEspecialidad.Name = "cboEspecialidad"
        Me.cboEspecialidad.Size = New System.Drawing.Size(464, 21)
        Me.cboEspecialidad.TabIndex = 19
        '
        'txtObservacion
        '
        Me.txtObservacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtObservacion.BackColor = System.Drawing.Color.Ivory
        Me.txtObservacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacion.ForeColor = System.Drawing.Color.Black
        Me.txtObservacion.KeyEnter = True
        Me.txtObservacion.Location = New System.Drawing.Point(107, 240)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtObservacion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservacion.ShortcutsEnabled = False
        Me.txtObservacion.Size = New System.Drawing.Size(464, 44)
        Me.txtObservacion.TabIndex = 20
        Me.txtObservacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboEstado)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(701, 11)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(138, 53)
        Me.GroupBox2.TabIndex = 42
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Estado Contrato"
        '
        'cboEstado
        '
        Me.cboEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.Location = New System.Drawing.Point(11, 19)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(121, 21)
        Me.cboEstado.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BeLabel23)
        Me.GroupBox1.Controls.Add(Me.txtRuc)
        Me.GroupBox1.Controls.Add(Me.cboTipoRetencion)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Controls.Add(Me.btnQuitar)
        Me.GroupBox1.Controls.Add(Me.BeLabel22)
        Me.GroupBox1.Controls.Add(Me.cboUbiTrabajo)
        Me.GroupBox1.Controls.Add(Me.cboCCosto)
        Me.GroupBox1.Controls.Add(Me.BeLabel19)
        Me.GroupBox1.Controls.Add(Me.BeLabel18)
        Me.GroupBox1.Controls.Add(Me.BeLabel17)
        Me.GroupBox1.Controls.Add(Me.txtSubTotal)
        Me.GroupBox1.Controls.Add(Me.BeLabel16)
        Me.GroupBox1.Controls.Add(Me.txtIGV)
        Me.GroupBox1.Controls.Add(Me.BeLabel14)
        Me.GroupBox1.Controls.Add(Me.cboObra)
        Me.GroupBox1.Controls.Add(Me.BeLabel13)
        Me.GroupBox1.Controls.Add(Me.dgvDetalle)
        Me.GroupBox1.Controls.Add(Me.BeLabel12)
        Me.GroupBox1.Controls.Add(Me.BeLabel11)
        Me.GroupBox1.Controls.Add(Me.BeLabel10)
        Me.GroupBox1.Controls.Add(Me.BeLabel9)
        Me.GroupBox1.Controls.Add(Me.BeLabel8)
        Me.GroupBox1.Controls.Add(Me.BeLabel7)
        Me.GroupBox1.Controls.Add(Me.BeLabel6)
        Me.GroupBox1.Controls.Add(Me.BeLabel5)
        Me.GroupBox1.Controls.Add(Me.BeLabel4)
        Me.GroupBox1.Controls.Add(Me.BeLabel3)
        Me.GroupBox1.Controls.Add(Me.BeLabel2)
        Me.GroupBox1.Controls.Add(Me.BeLabel1)
        Me.GroupBox1.Controls.Add(Me.txtContratista)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtCodigo)
        Me.GroupBox1.Controls.Add(Me.txtObservacion)
        Me.GroupBox1.Controls.Add(Me.cboMoneda)
        Me.GroupBox1.Controls.Add(Me.cboEspecialidad)
        Me.GroupBox1.Controls.Add(Me.txtRetencion)
        Me.GroupBox1.Controls.Add(Me.cboTipoDocumento)
        Me.GroupBox1.Controls.Add(Me.txtPresupuesto)
        Me.GroupBox1.Controls.Add(Me.txtCodigoContratista)
        Me.GroupBox1.Controls.Add(Me.txtUbicacion)
        Me.GroupBox1.Controls.Add(Me.txtPartida)
        Me.GroupBox1.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox1.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox1.Controls.Add(Me.BeLabel20)
        Me.GroupBox1.Controls.Add(Me.txtNroContrato)
        Me.GroupBox1.Controls.Add(Me.BeLabel21)
        Me.GroupBox1.Controls.Add(Me.txtNumeroOrden)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.BeLabel25)
        Me.GroupBox1.Controls.Add(Me.GroupBox5)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(7, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(962, 596)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Estado Contratista"
        '
        'BeLabel23
        '
        Me.BeLabel23.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel23.AutoSize = True
        Me.BeLabel23.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel23.ForeColor = System.Drawing.Color.Black
        Me.BeLabel23.Location = New System.Drawing.Point(590, 243)
        Me.BeLabel23.Name = "BeLabel23"
        Me.BeLabel23.Size = New System.Drawing.Size(80, 13)
        Me.BeLabel23.TabIndex = 461
        Me.BeLabel23.Text = "Tipo Retenci�n"
        '
        'txtRuc
        '
        Me.txtRuc.Location = New System.Drawing.Point(239, 64)
        Me.txtRuc.Name = "txtRuc"
        Me.txtRuc.Size = New System.Drawing.Size(156, 20)
        Me.txtRuc.TabIndex = 460
        '
        'cboTipoRetencion
        '
        Me.cboTipoRetencion.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoRetencion.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoRetencion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoRetencion.ForeColor = System.Drawing.Color.Black
        Me.cboTipoRetencion.FormattingEnabled = True
        Me.cboTipoRetencion.Items.AddRange(New Object() {"Base Inponible", "Total"})
        Me.cboTipoRetencion.KeyEnter = True
        Me.cboTipoRetencion.Location = New System.Drawing.Point(593, 261)
        Me.cboTipoRetencion.Name = "cboTipoRetencion"
        Me.cboTipoRetencion.Size = New System.Drawing.Size(121, 21)
        Me.cboTipoRetencion.TabIndex = 458
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.dgvContratistas)
        Me.Panel3.Controls.Add(Me.GroupBox3)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Location = New System.Drawing.Point(239, 87)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(641, 150)
        Me.Panel3.TabIndex = 30
        Me.Panel3.Visible = False
        '
        'dgvContratistas
        '
        Me.dgvContratistas.AllowUserToAddRows = False
        Me.dgvContratistas.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvContratistas.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvContratistas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvContratistas.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvContratistas.BackgroundColor = System.Drawing.Color.White
        Me.dgvContratistas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvContratistas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvContratistas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.Contratista, Me.Column2, Me.Column3, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column11, Me.Column12, Me.esp_codigo})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvContratistas.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvContratistas.EnableHeadersVisualStyles = False
        Me.dgvContratistas.Location = New System.Drawing.Point(3, 38)
        Me.dgvContratistas.MultiSelect = False
        Me.dgvContratistas.Name = "dgvContratistas"
        Me.dgvContratistas.ReadOnly = True
        Me.dgvContratistas.RowHeadersVisible = False
        Me.dgvContratistas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvContratistas.Size = New System.Drawing.Size(633, 106)
        Me.dgvContratistas.TabIndex = 1
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "DesTipoContratista"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Tipo"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 110
        '
        'Contratista
        '
        Me.Contratista.DataPropertyName = "Contratista"
        Me.Contratista.HeaderText = "Contratista"
        Me.Contratista.Name = "Contratista"
        Me.Contratista.ReadOnly = True
        Me.Contratista.Width = 245
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "conEspecialidad"
        Me.Column2.HeaderText = "Especialidad"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 250
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "IdContratista"
        Me.Column3.HeaderText = "IdContratista"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Visible = False
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "TipoConCodigo"
        Me.Column6.HeaderText = "TipoConCodigo"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Visible = False
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "ConNombre"
        Me.Column7.HeaderText = "ConNombre"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Visible = False
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "ConApellido"
        Me.Column8.HeaderText = "ConApellido"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Visible = False
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "conRuc"
        Me.Column9.HeaderText = "conRuc"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Visible = False
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "conDireccion"
        Me.Column10.HeaderText = "conDireccion"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Visible = False
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "conTelefono"
        Me.Column11.HeaderText = "conTelefono"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Visible = False
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "conEmail"
        Me.Column12.HeaderText = "conEmail"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Visible = False
        '
        'esp_codigo
        '
        Me.esp_codigo.DataPropertyName = "esp_codigo"
        Me.esp_codigo.HeaderText = "esp_codigo"
        Me.esp_codigo.Name = "esp_codigo"
        Me.esp_codigo.ReadOnly = True
        Me.esp_codigo.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rdbEspecialidad)
        Me.GroupBox3.Controls.Add(Me.rdbNombres)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(5, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(184, 33)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Buscar Por"
        '
        'rdbEspecialidad
        '
        Me.rdbEspecialidad.AutoSize = True
        Me.rdbEspecialidad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEspecialidad.Location = New System.Drawing.Point(86, 15)
        Me.rdbEspecialidad.Name = "rdbEspecialidad"
        Me.rdbEspecialidad.Size = New System.Drawing.Size(85, 17)
        Me.rdbEspecialidad.TabIndex = 1
        Me.rdbEspecialidad.Text = "Especialidad"
        Me.rdbEspecialidad.UseVisualStyleBackColor = True
        '
        'rdbNombres
        '
        Me.rdbNombres.AutoSize = True
        Me.rdbNombres.Checked = True
        Me.rdbNombres.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbNombres.Location = New System.Drawing.Point(6, 15)
        Me.rdbNombres.Name = "rdbNombres"
        Me.rdbNombres.Size = New System.Drawing.Size(67, 17)
        Me.rdbNombres.TabIndex = 0
        Me.rdbNombres.TabStop = True
        Me.rdbNombres.Text = "Nombres"
        Me.rdbNombres.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(618, 2)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 18)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "X"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.LightSlateGray
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.CheckBox1)
        Me.Panel2.Controls.Add(Me.BeLabel15)
        Me.Panel2.Controls.Add(Me.txtBusqueda)
        Me.Panel2.Controls.Add(Me.cboOpciones)
        Me.Panel2.Controls.Add(Me.btnAgregar)
        Me.Panel2.Controls.Add(Me.dgvConsolidado)
        Me.Panel2.Location = New System.Drawing.Point(25, 331)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(929, 238)
        Me.Panel2.TabIndex = 47
        Me.Panel2.Visible = False
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(14, 31)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(61, 17)
        Me.CheckBox1.TabIndex = 39
        Me.CheckBox1.Text = "Todos"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(0, 14)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel15.TabIndex = 38
        Me.BeLabel15.Text = "Buscar Por:"
        '
        'txtBusqueda
        '
        Me.txtBusqueda.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBusqueda.BackColor = System.Drawing.Color.Lavender
        Me.txtBusqueda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBusqueda.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBusqueda.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBusqueda.ForeColor = System.Drawing.Color.Black
        Me.txtBusqueda.KeyEnter = True
        Me.txtBusqueda.Location = New System.Drawing.Point(315, 9)
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBusqueda.ShortcutsEnabled = False
        Me.txtBusqueda.Size = New System.Drawing.Size(228, 20)
        Me.txtBusqueda.TabIndex = 37
        Me.txtBusqueda.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboOpciones
        '
        Me.cboOpciones.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboOpciones.BackColor = System.Drawing.Color.Ivory
        Me.cboOpciones.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOpciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOpciones.ForeColor = System.Drawing.Color.Black
        Me.cboOpciones.FormattingEnabled = True
        Me.cboOpciones.Items.AddRange(New Object() {"C�digo", "Partida"})
        Me.cboOpciones.KeyEnter = True
        Me.cboOpciones.Location = New System.Drawing.Point(79, 8)
        Me.cboOpciones.Name = "cboOpciones"
        Me.cboOpciones.Size = New System.Drawing.Size(228, 21)
        Me.cboOpciones.TabIndex = 36
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(549, 7)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(68, 23)
        Me.btnAgregar.TabIndex = 35
        Me.btnAgregar.Text = "Agregar"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'dgvConsolidado
        '
        Me.dgvConsolidado.AllowUserToAddRows = False
        Me.dgvConsolidado.BackgroundColor = System.Drawing.Color.White
        Me.dgvConsolidado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvConsolidado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.X, Me.Column14, Me.Column15, Me.Column16, Me.ParDescripcion, Me.UmdAbreviado, Me.Metrado, Me.PrecioPresupuesto, Me.PrecioSubcontrato, Me.EspCodigo, Me.TitCodigo, Me.ParCodigo, Me.ConsCodigo})
        Me.dgvConsolidado.Location = New System.Drawing.Point(4, 49)
        Me.dgvConsolidado.Name = "dgvConsolidado"
        Me.dgvConsolidado.RowHeadersVisible = False
        Me.dgvConsolidado.Size = New System.Drawing.Size(920, 182)
        Me.dgvConsolidado.TabIndex = 34
        '
        'X
        '
        Me.X.HeaderText = "X"
        Me.X.Name = "X"
        Me.X.Width = 20
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "UbtCodigo"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column14.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column14.HeaderText = "Presu"
        Me.Column14.Name = "Column14"
        Me.Column14.Width = 60
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "Spptocodigo"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column15.DefaultCellStyle = DataGridViewCellStyle6
        Me.Column15.HeaderText = "SubPres"
        Me.Column15.Name = "Column15"
        Me.Column15.Width = 70
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "ItemPresupuesto"
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Column16.DefaultCellStyle = DataGridViewCellStyle7
        Me.Column16.HeaderText = "C�digo"
        Me.Column16.Name = "Column16"
        Me.Column16.Width = 80
        '
        'ParDescripcion
        '
        Me.ParDescripcion.DataPropertyName = "ParDescripcion"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ParDescripcion.DefaultCellStyle = DataGridViewCellStyle8
        Me.ParDescripcion.HeaderText = "Partida"
        Me.ParDescripcion.Name = "ParDescripcion"
        Me.ParDescripcion.ReadOnly = True
        Me.ParDescripcion.Width = 300
        '
        'UmdAbreviado
        '
        Me.UmdAbreviado.DataPropertyName = "UmdAbreviado"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UmdAbreviado.DefaultCellStyle = DataGridViewCellStyle9
        Me.UmdAbreviado.HeaderText = "U. Med."
        Me.UmdAbreviado.Name = "UmdAbreviado"
        Me.UmdAbreviado.ReadOnly = True
        Me.UmdAbreviado.Width = 90
        '
        'Metrado
        '
        Me.Metrado.DataPropertyName = "Metrado"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.Format = "N2"
        DataGridViewCellStyle10.NullValue = Nothing
        Me.Metrado.DefaultCellStyle = DataGridViewCellStyle10
        Me.Metrado.HeaderText = "Metrado"
        Me.Metrado.Name = "Metrado"
        Me.Metrado.ReadOnly = True
        Me.Metrado.Width = 90
        '
        'PrecioPresupuesto
        '
        Me.PrecioPresupuesto.DataPropertyName = "PrecioPresupuesto"
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.Format = "N2"
        DataGridViewCellStyle11.NullValue = Nothing
        Me.PrecioPresupuesto.DefaultCellStyle = DataGridViewCellStyle11
        Me.PrecioPresupuesto.HeaderText = "PMO Exp."
        Me.PrecioPresupuesto.Name = "PrecioPresupuesto"
        Me.PrecioPresupuesto.ReadOnly = True
        Me.PrecioPresupuesto.Width = 95
        '
        'PrecioSubcontrato
        '
        Me.PrecioSubcontrato.DataPropertyName = "PrecioSubcontrato"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.Format = "N2"
        DataGridViewCellStyle12.NullValue = Nothing
        Me.PrecioSubcontrato.DefaultCellStyle = DataGridViewCellStyle12
        Me.PrecioSubcontrato.HeaderText = "PMO Int."
        Me.PrecioSubcontrato.Name = "PrecioSubcontrato"
        Me.PrecioSubcontrato.ReadOnly = True
        Me.PrecioSubcontrato.Width = 90
        '
        'EspCodigo
        '
        Me.EspCodigo.DataPropertyName = "EspCodigo"
        Me.EspCodigo.HeaderText = "EspCodigo"
        Me.EspCodigo.Name = "EspCodigo"
        Me.EspCodigo.Visible = False
        '
        'TitCodigo
        '
        Me.TitCodigo.DataPropertyName = "TitCodigo"
        Me.TitCodigo.HeaderText = "TitCodigo"
        Me.TitCodigo.Name = "TitCodigo"
        Me.TitCodigo.Visible = False
        '
        'ParCodigo
        '
        Me.ParCodigo.DataPropertyName = "ParCodigo"
        Me.ParCodigo.HeaderText = "ParCodigo"
        Me.ParCodigo.Name = "ParCodigo"
        Me.ParCodigo.Visible = False
        '
        'ConsCodigo
        '
        Me.ConsCodigo.DataPropertyName = "ConsCodigo"
        Me.ConsCodigo.HeaderText = "ConsCodigo"
        Me.ConsCodigo.Name = "ConsCodigo"
        Me.ConsCodigo.Visible = False
        '
        'btnQuitar
        '
        Me.btnQuitar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitar.Location = New System.Drawing.Point(849, 261)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(107, 23)
        Me.btnQuitar.TabIndex = 46
        Me.btnQuitar.Text = "Quitar Partida"
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'BeLabel22
        '
        Me.BeLabel22.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel22.AutoSize = True
        Me.BeLabel22.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel22.ForeColor = System.Drawing.Color.Black
        Me.BeLabel22.Location = New System.Drawing.Point(24, 291)
        Me.BeLabel22.Name = "BeLabel22"
        Me.BeLabel22.Size = New System.Drawing.Size(66, 13)
        Me.BeLabel22.TabIndex = 44
        Me.BeLabel22.Text = "Presupuesto"
        '
        'cboUbiTrabajo
        '
        Me.cboUbiTrabajo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboUbiTrabajo.BackColor = System.Drawing.Color.Ivory
        Me.cboUbiTrabajo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUbiTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUbiTrabajo.ForeColor = System.Drawing.Color.Black
        Me.cboUbiTrabajo.FormattingEnabled = True
        Me.cboUbiTrabajo.KeyEnter = True
        Me.cboUbiTrabajo.Location = New System.Drawing.Point(107, 287)
        Me.cboUbiTrabajo.Name = "cboUbiTrabajo"
        Me.cboUbiTrabajo.Size = New System.Drawing.Size(464, 21)
        Me.cboUbiTrabajo.TabIndex = 21
        '
        'cboCCosto
        '
        Me.cboCCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCCosto.BackColor = System.Drawing.Color.Ivory
        Me.cboCCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCCosto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCCosto.ForeColor = System.Drawing.Color.Black
        Me.cboCCosto.FormattingEnabled = True
        Me.cboCCosto.KeyEnter = True
        Me.cboCCosto.Location = New System.Drawing.Point(343, 17)
        Me.cboCCosto.Name = "cboCCosto"
        Me.cboCCosto.Size = New System.Drawing.Size(228, 21)
        Me.cboCCosto.TabIndex = 3
        '
        'BeLabel19
        '
        Me.BeLabel19.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel19.AutoSize = True
        Me.BeLabel19.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel19.ForeColor = System.Drawing.Color.Black
        Me.BeLabel19.Location = New System.Drawing.Point(242, 20)
        Me.BeLabel19.Name = "BeLabel19"
        Me.BeLabel19.Size = New System.Drawing.Size(83, 13)
        Me.BeLabel19.TabIndex = 2
        Me.BeLabel19.Text = "Centro de Costo"
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Navy
        Me.BeLabel18.Location = New System.Drawing.Point(104, 446)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(31, 13)
        Me.BeLabel18.TabIndex = 31
        Me.BeLabel18.Text = "xxxx"
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(739, 448)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(58, 13)
        Me.BeLabel17.TabIndex = 34
        Me.BeLabel17.Text = "SubTotal"
        '
        'txtSubTotal
        '
        Me.txtSubTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSubTotal.BackColor = System.Drawing.Color.Lavender
        Me.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSubTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSubTotal.Enabled = False
        Me.txtSubTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubTotal.ForeColor = System.Drawing.Color.Black
        Me.txtSubTotal.KeyEnter = True
        Me.txtSubTotal.Location = New System.Drawing.Point(822, 446)
        Me.txtSubTotal.Name = "txtSubTotal"
        Me.txtSubTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSubTotal.ShortcutsEnabled = False
        Me.txtSubTotal.Size = New System.Drawing.Size(102, 20)
        Me.txtSubTotal.TabIndex = 35
        Me.txtSubTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSubTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(739, 470)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel16.TabIndex = 36
        Me.BeLabel16.Text = "I.G.V."
        '
        'txtIGV
        '
        Me.txtIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIGV.BackColor = System.Drawing.Color.Lavender
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIGV.Enabled = False
        Me.txtIGV.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.ForeColor = System.Drawing.Color.Black
        Me.txtIGV.KeyEnter = True
        Me.txtIGV.Location = New System.Drawing.Point(822, 468)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIGV.ShortcutsEnabled = False
        Me.txtIGV.Size = New System.Drawing.Size(102, 20)
        Me.txtIGV.TabIndex = 37
        Me.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(24, 315)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel14.TabIndex = 32
        Me.BeLabel14.Text = "Detalle"
        '
        'cboObra
        '
        Me.cboObra.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboObra.BackColor = System.Drawing.Color.Ivory
        Me.cboObra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboObra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObra.ForeColor = System.Drawing.Color.Black
        Me.cboObra.FormattingEnabled = True
        Me.cboObra.KeyEnter = True
        Me.cboObra.Location = New System.Drawing.Point(107, 40)
        Me.cboObra.Name = "cboObra"
        Me.cboObra.Size = New System.Drawing.Size(464, 21)
        Me.cboObra.TabIndex = 5
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(24, 46)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(30, 13)
        Me.BeLabel13.TabIndex = 4
        Me.BeLabel13.Text = "Obra"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.Cant, Me.Und, Me.M1, Me.Con_Metrado, Me.PU, Me.Con_PrecioSubContra, Me.M2, Me.Parcial, Me.idDetalle, Me.Estado, Me.Con_TitCodigo, Me.Con_ParCodigo, Me.Con_ConsCodigo, Me.M2Anterior})
        Me.dgvDetalle.Location = New System.Drawing.Point(107, 312)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(817, 131)
        Me.dgvDetalle.TabIndex = 33
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Descripcion.DefaultCellStyle = DataGridViewCellStyle13
        Me.Descripcion.HeaderText = "Descripcion"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        Me.Descripcion.Width = 310
        '
        'Cant
        '
        Me.Cant.DataPropertyName = "Cantidad"
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.Format = "N2"
        DataGridViewCellStyle14.NullValue = Nothing
        Me.Cant.DefaultCellStyle = DataGridViewCellStyle14
        Me.Cant.HeaderText = "Cant"
        Me.Cant.Name = "Cant"
        Me.Cant.ReadOnly = True
        Me.Cant.Visible = False
        Me.Cant.Width = 55
        '
        'Und
        '
        Me.Und.DataPropertyName = "UnidMed"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Und.DefaultCellStyle = DataGridViewCellStyle15
        Me.Und.HeaderText = "U.M."
        Me.Und.Name = "Und"
        Me.Und.ReadOnly = True
        Me.Und.Width = 55
        '
        'M1
        '
        Me.M1.DataPropertyName = "MetradoPor"
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.Format = "N2"
        DataGridViewCellStyle16.NullValue = Nothing
        Me.M1.DefaultCellStyle = DataGridViewCellStyle16
        Me.M1.HeaderText = "Metr. %"
        Me.M1.Name = "M1"
        Me.M1.ReadOnly = True
        Me.M1.Width = 85
        '
        'Con_Metrado
        '
        Me.Con_Metrado.DataPropertyName = "Con_Metrado"
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.Format = "N2"
        DataGridViewCellStyle17.NullValue = Nothing
        Me.Con_Metrado.DefaultCellStyle = DataGridViewCellStyle17
        Me.Con_Metrado.HeaderText = "Mt."
        Me.Con_Metrado.Name = "Con_Metrado"
        Me.Con_Metrado.ReadOnly = True
        Me.Con_Metrado.Width = 50
        '
        'PU
        '
        Me.PU.DataPropertyName = "PU"
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.Format = "N2"
        DataGridViewCellStyle18.NullValue = Nothing
        Me.PU.DefaultCellStyle = DataGridViewCellStyle18
        Me.PU.HeaderText = "PMO Exp."
        Me.PU.Name = "PU"
        Me.PU.ReadOnly = True
        Me.PU.Visible = False
        Me.PU.Width = 60
        '
        'Con_PrecioSubContra
        '
        Me.Con_PrecioSubContra.DataPropertyName = "Con_PrecioSubContra"
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.Format = "N2"
        DataGridViewCellStyle19.NullValue = Nothing
        Me.Con_PrecioSubContra.DefaultCellStyle = DataGridViewCellStyle19
        Me.Con_PrecioSubContra.HeaderText = "PMO Int."
        Me.Con_PrecioSubContra.Name = "Con_PrecioSubContra"
        Me.Con_PrecioSubContra.Width = 90
        '
        'M2
        '
        Me.M2.DataPropertyName = "MetradoImpor"
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.Format = "N2"
        DataGridViewCellStyle20.NullValue = Nothing
        Me.M2.DefaultCellStyle = DataGridViewCellStyle20
        Me.M2.HeaderText = "Mt. Contratar"
        Me.M2.Name = "M2"
        Me.M2.Width = 120
        '
        'Parcial
        '
        Me.Parcial.DataPropertyName = "Parcial"
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.Format = "N2"
        DataGridViewCellStyle21.NullValue = Nothing
        Me.Parcial.DefaultCellStyle = DataGridViewCellStyle21
        Me.Parcial.HeaderText = "Parcial"
        Me.Parcial.Name = "Parcial"
        Me.Parcial.Width = 85
        '
        'idDetalle
        '
        Me.idDetalle.DataPropertyName = "IdPartidaDet"
        Me.idDetalle.HeaderText = "IdDetalle"
        Me.idDetalle.Name = "idDetalle"
        Me.idDetalle.Visible = False
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.Visible = False
        '
        'Con_TitCodigo
        '
        Me.Con_TitCodigo.DataPropertyName = "Con_TitCodigo"
        Me.Con_TitCodigo.HeaderText = "Con_TitCodigo"
        Me.Con_TitCodigo.Name = "Con_TitCodigo"
        Me.Con_TitCodigo.Visible = False
        '
        'Con_ParCodigo
        '
        Me.Con_ParCodigo.DataPropertyName = "Con_ParCodigo"
        Me.Con_ParCodigo.HeaderText = "Con_ParCodigo"
        Me.Con_ParCodigo.Name = "Con_ParCodigo"
        Me.Con_ParCodigo.Visible = False
        '
        'Con_ConsCodigo
        '
        Me.Con_ConsCodigo.DataPropertyName = "Con_ConsCodigo"
        Me.Con_ConsCodigo.HeaderText = "Con_ConsCodigo"
        Me.Con_ConsCodigo.Name = "Con_ConsCodigo"
        Me.Con_ConsCodigo.Visible = False
        '
        'M2Anterior
        '
        Me.M2Anterior.DataPropertyName = "M2Anterior"
        Me.M2Anterior.HeaderText = "M2Anterior"
        Me.M2Anterior.Name = "M2Anterior"
        Me.M2Anterior.Visible = False
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(24, 243)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel12.TabIndex = 28
        Me.BeLabel12.Text = "Observaci�n"
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(24, 221)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel11.TabIndex = 26
        Me.BeLabel11.Text = "Especialidad"
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(449, 174)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel10.TabIndex = 40
        Me.BeLabel10.Text = "% Crat."
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(739, 492)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel9.TabIndex = 38
        Me.BeLabel9.Text = "Tot. Contrato"
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(24, 200)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel8.TabIndex = 24
        Me.BeLabel8.Text = "Ubicaci�n"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(242, 174)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel7.TabIndex = 18
        Me.BeLabel7.Text = "F. Termino"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(24, 177)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(44, 13)
        Me.BeLabel6.TabIndex = 16
        Me.BeLabel6.Text = "F. Inicio"
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(24, 114)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(63, 13)
        Me.BeLabel5.TabIndex = 14
        Me.BeLabel5.Text = "Descripci�n"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(242, 92)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(81, 13)
        Me.BeLabel4.TabIndex = 12
        Me.BeLabel4.Text = "Doc. Rendici�n"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(24, 91)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel3.TabIndex = 9
        Me.BeLabel3.Text = "Moneda"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(24, 67)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(57, 13)
        Me.BeLabel2.TabIndex = 6
        Me.BeLabel2.Text = "Contratista"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(24, 24)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "C�digo"
        '
        'BeLabel20
        '
        Me.BeLabel20.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel20.AutoSize = True
        Me.BeLabel20.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel20.ForeColor = System.Drawing.Color.Black
        Me.BeLabel20.Location = New System.Drawing.Point(242, 151)
        Me.BeLabel20.Name = "BeLabel20"
        Me.BeLabel20.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel20.TabIndex = 22
        Me.BeLabel20.Text = "Contrato N�"
        '
        'txtNroContrato
        '
        Me.txtNroContrato.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNroContrato.BackColor = System.Drawing.Color.Ivory
        Me.txtNroContrato.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroContrato.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroContrato.ForeColor = System.Drawing.Color.Black
        Me.txtNroContrato.KeyEnter = True
        Me.txtNroContrato.Location = New System.Drawing.Point(343, 147)
        Me.txtNroContrato.Name = "txtNroContrato"
        Me.txtNroContrato.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNroContrato.ShortcutsEnabled = False
        Me.txtNroContrato.Size = New System.Drawing.Size(227, 20)
        Me.txtNroContrato.TabIndex = 14
        Me.txtNroContrato.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'BeLabel21
        '
        Me.BeLabel21.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel21.AutoSize = True
        Me.BeLabel21.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel21.ForeColor = System.Drawing.Color.Black
        Me.BeLabel21.Location = New System.Drawing.Point(24, 150)
        Me.BeLabel21.Name = "BeLabel21"
        Me.BeLabel21.Size = New System.Drawing.Size(51, 13)
        Me.BeLabel21.TabIndex = 20
        Me.BeLabel21.Text = "Orden N�"
        '
        'txtNumeroOrden
        '
        Me.txtNumeroOrden.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumeroOrden.BackColor = System.Drawing.Color.Ivory
        Me.txtNumeroOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumeroOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumeroOrden.ForeColor = System.Drawing.Color.Black
        Me.txtNumeroOrden.KeyEnter = True
        Me.txtNumeroOrden.Location = New System.Drawing.Point(107, 147)
        Me.txtNumeroOrden.Name = "txtNumeroOrden"
        Me.txtNumeroOrden.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumeroOrden.ShortcutsEnabled = False
        Me.txtNumeroOrden.Size = New System.Drawing.Size(131, 20)
        Me.txtNumeroOrden.TabIndex = 13
        Me.txtNumeroOrden.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(602, 30)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(16, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 456
        Me.PictureBox1.TabStop = False
        '
        'btnBuscar
        '
        Me.btnBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBuscar.Location = New System.Drawing.Point(736, 261)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(107, 23)
        Me.btnBuscar.TabIndex = 22
        Me.btnBuscar.Text = "Buscar Partidas"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'BeLabel25
        '
        Me.BeLabel25.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel25.AutoSize = True
        Me.BeLabel25.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel25.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel25.ForeColor = System.Drawing.Color.DarkRed
        Me.BeLabel25.Location = New System.Drawing.Point(771, 294)
        Me.BeLabel25.Name = "BeLabel25"
        Me.BeLabel25.Size = New System.Drawing.Size(185, 15)
        Me.BeLabel25.TabIndex = 49
        Me.BeLabel25.Text = "Pulsa Supr = Para eliminar Item detalle"
        Me.BeLabel25.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnLiquidar)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.Location = New System.Drawing.Point(701, 87)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(138, 53)
        Me.GroupBox5.TabIndex = 457
        Me.GroupBox5.TabStop = False
        '
        'btnLiquidar
        '
        Me.btnLiquidar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLiquidar.Location = New System.Drawing.Point(11, 19)
        Me.btnLiquidar.Name = "btnLiquidar"
        Me.btnLiquidar.Size = New System.Drawing.Size(121, 23)
        Me.btnLiquidar.TabIndex = 458
        Me.btnLiquidar.Text = "Liquidar Contrato"
        Me.btnLiquidar.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Image = Global.CapaPreTesoreria.My.Resources.Resources.file
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(738, 217)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(135, 40)
        Me.Button1.TabIndex = 459
        Me.Button1.Text = "Agregar Adenda"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dgvLista
        '
        Me.dgvLista.AllowUserToAddRows = False
        Me.dgvLista.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvLista.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvLista.BackgroundColor = System.Drawing.Color.White
        Me.dgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLista.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column5, Me.Column1, Me.Column13, Me.Column4, Me.CCosCodigo})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Cornsilk
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvLista.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvLista.EnableHeadersVisualStyles = False
        Me.dgvLista.Location = New System.Drawing.Point(-1, -1)
        Me.dgvLista.MultiSelect = False
        Me.dgvLista.Name = "dgvLista"
        Me.dgvLista.ReadOnly = True
        Me.dgvLista.RowHeadersVisible = False
        Me.dgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLista.Size = New System.Drawing.Size(14730, 6612)
        Me.dgvLista.TabIndex = 2
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "IdPartida"
        Me.Column5.HeaderText = "C�digo"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 130
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "ParDescripcion"
        Me.Column1.HeaderText = "Descripci�n"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 360
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "ObraDescripcion"
        Me.Column13.HeaderText = "Obra"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 330
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "Estados"
        Me.Column4.HeaderText = "Estado"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 130
        '
        'CCosCodigo
        '
        Me.CCosCodigo.DataPropertyName = "CCosCodigo"
        Me.CCosCodigo.HeaderText = "CCosCodigo"
        Me.CCosCodigo.Name = "CCosCodigo"
        Me.CCosCodigo.ReadOnly = True
        Me.CCosCodigo.Visible = False
        '
        'stsTotales
        '
        Me.stsTotales.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel2})
        Me.stsTotales.Location = New System.Drawing.Point(3, 636)
        Me.stsTotales.Name = "stsTotales"
        Me.stsTotales.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.stsTotales.Size = New System.Drawing.Size(962, 24)
        Me.stsTotales.TabIndex = 4
        Me.stsTotales.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
            Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.ToolStripStatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.ToolStripStatusLabel2.ForeColor = System.Drawing.Color.White
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(122, 19)
        Me.ToolStripStatusLabel2.Text = "Total de Registros= 0"
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'Timer3
        '
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.GroupBox4)
        Me.Panel1.Location = New System.Drawing.Point(324, 198)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(392, 88)
        Me.Panel1.TabIndex = 5
        Me.Panel1.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(375, -1)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 15)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "X"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.rdbObra)
        Me.GroupBox4.Controls.Add(Me.txtBuscar)
        Me.GroupBox4.Controls.Add(Me.rdbPartida)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(7, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(360, 74)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Buscar Por"
        '
        'rdbObra
        '
        Me.rdbObra.AutoSize = True
        Me.rdbObra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbObra.Location = New System.Drawing.Point(24, 42)
        Me.rdbObra.Name = "rdbObra"
        Me.rdbObra.Size = New System.Drawing.Size(52, 17)
        Me.rdbObra.TabIndex = 3
        Me.rdbObra.TabStop = True
        Me.rdbObra.Text = "Obra"
        Me.rdbObra.UseVisualStyleBackColor = True
        '
        'txtBuscar
        '
        Me.txtBuscar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBuscar.BackColor = System.Drawing.Color.Ivory
        Me.txtBuscar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtBuscar.KeyEnter = True
        Me.txtBuscar.Location = New System.Drawing.Point(102, 29)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBuscar.ShortcutsEnabled = False
        Me.txtBuscar.Size = New System.Drawing.Size(245, 20)
        Me.txtBuscar.TabIndex = 2
        Me.txtBuscar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        '
        'rdbPartida
        '
        Me.rdbPartida.AutoSize = True
        Me.rdbPartida.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbPartida.Location = New System.Drawing.Point(24, 21)
        Me.rdbPartida.Name = "rdbPartida"
        Me.rdbPartida.Size = New System.Drawing.Size(73, 17)
        Me.rdbPartida.TabIndex = 0
        Me.rdbPartida.TabStop = True
        Me.rdbPartida.Text = "Contrato"
        Me.rdbPartida.UseVisualStyleBackColor = True
        '
        'frmPartidas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.ClientSize = New System.Drawing.Size(978, 691)
        Me.MaximizeBox = False
        Me.Name = "frmPartidas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Mantenimiento de Contratos"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgvContratistas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.dgvConsolidado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stsTotales.ResumeLayout(False)
        Me.stsTotales.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtCodigo As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtObservacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboEspecialidad As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtRetencion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPresupuesto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtUbicacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents txtPartida As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCodigoContratista As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboTipoDocumento As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtContratista As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents stsTotales As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbEspecialidad As System.Windows.Forms.RadioButton
    Friend WithEvents rdbNombres As System.Windows.Forms.RadioButton
    Friend WithEvents dgvContratistas As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtBuscar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents rdbPartida As System.Windows.Forms.RadioButton
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents cboObra As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSubTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel19 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents rdbObra As System.Windows.Forms.RadioButton
    Friend WithEvents BeLabel20 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNroContrato As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel21 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtNumeroOrden As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboUbiTrabajo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel22 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvConsolidado As System.Windows.Forms.DataGridView
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contratista As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esp_codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BeLabel25 As ctrLibreria.Controles.BeLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Und As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_Metrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PU As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_PrecioSubContra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Parcial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idDetalle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_TitCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_ParCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Con_ConsCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents M2Anterior As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents btnLiquidar As System.Windows.Forms.Button
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtBusqueda As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboOpciones As ctrLibreria.Controles.BeComboBox
    Friend WithEvents X As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ParDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UmdAbreviado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Metrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioPresupuesto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioSubcontrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EspCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TitCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ParCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConsCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboTipoRetencion As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtRuc As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel23 As ctrLibreria.Controles.BeLabel
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox

End Class
