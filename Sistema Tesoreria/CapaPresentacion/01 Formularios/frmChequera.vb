Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmChequera
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim dtTableLista As DataTable
    Dim dtTable2 As DataTable
    Private cTempo As New clsPlantTempo
    Private eChequera As clsChequera
    'Private eCheque As clsCheque
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Dim moncondigo As String
    Dim sCodigoActual3 As String = ""
    Dim IdEmpresa As String = ""
    Dim IdBanco As String = ""
    Dim IdBanco2 As String = ""
    Dim IdBancos As String = ""
    Dim IdCuenta As String = ""
    Dim NroChequera As String = ""
    'Dim CodMoneda As String = ""

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmChequera = Nothing
    Public Shared Function Instance() As frmChequera
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmChequera
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtNumChequera.Clear()
        txtNumIni.Clear()
        txtNumFin.Clear()
    End Sub

    Private Sub DesabilitarControles()
        cboBanco.Enabled = False
        cboCuenta.Enabled = False
        'txtNumChequera.ReadOnly = True
        txtNumChequera.Enabled = False
        dtpFechaChequera.Enabled = False
        'txtNumIni.ReadOnly = True
        'txtNumFin.ReadOnly = True
        txtNumIni.Enabled = False
        txtNumFin.Enabled = False
        cboEstado.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        cboBanco.Enabled = True
        cboCuenta.Enabled = True
        ' txtNumChequera.ReadOnly = False
        ' txtNumChequera.Enabled = True
        dtpFechaChequera.Enabled = True
        txtNumIni.ReadOnly = False
        txtNumIni.Enabled = True
        txtNumFin.ReadOnly = False
        txtNumFin.Enabled = True
    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        'MessageBox.Show("Buscar")
        'End If

        cboBancos.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        If eChequera.iNroRegistros > 0 Then
            cboBancos.ValueMember = "IdBanco"
            cboBancos.DisplayMember = "NombreBanco"
            cboBancos.SelectedIndex = -1
        End If

        cboEstados.DataSource = eTempo.fColEstados
        cboEstados.ValueMember = "Col02"
        cboEstados.DisplayMember = "Col01"
        cboEstados.SelectedIndex = -1

        'cboCondicion.SelectedIndex = -1
        dgvLista.Focus()

        Panel2.Visible = True
        'CheckBox1.Checked = False
        'cboCondicion.SelectedIndex = -1
        cboEstados.SelectedIndex = -1
        cboCuentas.SelectedIndex = -1
        cboBancos.SelectedIndex = -1
        cboBancos.Focus()
    End Sub

    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub

    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg

        'Dim numerofinal As String
        'al hacer nuevo deberia ir el focus al 1er combo, 
        If sTab = 0 Then
            iOpcion = 1
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            'Timer1.Enabled = True
            cboEstado.Enabled = False
            cboEstado.SelectedIndex = 0
            cboBanco.SelectedIndex = -1
            cboCuenta.SelectedIndex = -1
            'txtNumChequera.Enabled = False
            chkUso.Checked = True
            chkUso.Enabled = False
            txtNumChequera.Enabled = True
        End If
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            VerPosicion()
            DesabilitarControles()
            'DesabilitarControles()
            chkUso.Enabled = False
        End If
    End Sub

    Private Sub v_ModificaReg() Handles Me.ModificaReg
        'mMostrarGrilla()
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2
            VerPosicion()
            DesabilitarControles()
            chkUso.Enabled = True
            cboEstado.Enabled = True
            'txtNumChequera.Enabled = True
            'Timer1.Enabled = True
            'chkUso.Checked = True
            'chkUso.Enabled = True
            'chkLleno.Checked = False
            'chkLleno.Enabled = True
        End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        'Else
        'Exit Sub
        'End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        Dim NroInicial As Integer
        Dim NroFinal As Integer
        Dim iResultado As Int32
        Dim iResultado2 As Int32
        Dim sCodigoActual As String = ""
        Dim Anulado As Int32 = 0
        Dim sCodigoActual2 As String = ""
        Dim sCodigoActual3 As String = ""
        Dim NumeroChequera As String = ""
        Dim UltimoIdCheque As String = ""
        Dim Mensaje As String = ""
        Dim Condicion As String = ""
        Dim sSufijo2 As String = "00000000000000000000"

        Dim i As Integer
        eTempo = New clsPlantTempo
        eChequera = New clsChequera

        If chkUso.Checked = True Then
            Condicion = "En Uso"
        Else
            Condicion = "Lleno"
        End If

        If sTab = 1 Then
            If Len(Trim(txtNumChequera.Text)) > 0 And Len(Trim(txtNumFin.Text)) > 0 And Len(Trim(txtNumIni.Text)) > 0 And cboBanco.SelectedIndex > -1 And cboCuenta.SelectedIndex > -1 Then
                NroInicial = Convert.ToInt32(txtNumIni.Text)
                NroFinal = Convert.ToInt32(txtNumFin.Text)

                If NroInicial < NroFinal And NroInicial <> NroFinal Then
                    If iOpcion = 1 Then
                        Mensaje = "Grabar"
                    ElseIf iOpcion = 2 Then
                        Mensaje = "Actualizar"
                    End If

                    If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                        frmPrincipal.sFlagGrabar = "1"

                        If iOpcion = 1 Then

                            eChequera = New clsChequera
                            Dim dtNumeroChequera As DataTable
                            dtNumeroChequera = New DataTable
                            dtNumeroChequera = eChequera.fBuscarExisteNum("", gEmpresa, Convert.ToString(cboBanco.SelectedValue), Convert.ToString(cboCuenta.SelectedValue), moncondigo, Convert.ToString(txtNumChequera.Text.Trim), Convert.ToString(dtpFechaChequera.Text), Convert.ToString(txtNumIni.Text.Trim), Convert.ToString(txtNumFin.Text.Trim), Condicion, cboEstado.SelectedValue, 33, gUsuario)
                            If dtNumeroChequera.Rows.Count > 0 Then
                                MessageBox.Show("Este Numero de Chequera : " & Trim(txtNumChequera.Text) & " ya Existe en la Cuenta : " & Trim(cboCuenta.Text), "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Exit Sub
                            End If

                            eChequera.fCodigo(gEmpresa)
                            sCodigoActual = eChequera.sCodFuturo
                            iResultado = eChequera.fGrabar(sCodigoActual, gEmpresa, Convert.ToString(cboBanco.SelectedValue), Convert.ToString(cboCuenta.SelectedValue), moncondigo, Convert.ToString(txtNumChequera.Text.Trim), Convert.ToString(dtpFechaChequera.Text), Convert.ToString(txtNumIni.Text.Trim), Convert.ToString(txtNumFin.Text.Trim), Condicion, cboEstado.SelectedValue, iOpcion, gUsuario)
                            eChequera.fListarUltimoIdCheque(gEmpresa)
                            UltimoIdCheque = eChequera.sUltimoIdCheque
                            For i = NroInicial To NroFinal
                                If iOpcion = 1 Then
                                    'eCheque.fCodigo()
                                    'sCodigoActual2 = eCheque.sCodFuturo
                                    iResultado2 = eChequera.fGrabarDetalleCheque(UltimoIdCheque, gEmpresa, moncondigo, sCodigoActual, NroInicial, Convert.ToString(Me.txtNumChequera.Text.Trim), dtpFechaChequera.Text, cboEstado.SelectedValue, gUsuario)
                                    'Else
                                    'iResultado2 = eChequera.fActualizaCheque(sCodigoActual, moncondigo, NroInicial, dtpFechaChequera.Text)
                                    'sacar los cheques grabados en la chequera 
                                    'una vrx sacado cada registro pasarlo de inicio a fin con un do while
                                    'y modificarlo
                                End If
                                NroInicial += 1
                                UltimoIdCheque += 1
                                UltimoIdCheque = Trim(Microsoft.VisualBasic.Right(sSufijo2 & Convert.ToString(Convert.ToString(UltimoIdCheque)), sSufijo2.Length))
                            Next
                            If iResultado > 0 And iResultado2 > 0 Then
                                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
                                MessageBox.Show("Se Grabo la Chequera Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)

                                mMostrarGrilla()
                                Timer2.Enabled = True
                            End If
                            sTab = 0
                        Else
                            sCodigoActual = Trim(Me.txtCodigo.Text)
                            iResultado = eChequera.fActualizarCabecera(sCodigoActual, Condicion, Convert.ToInt32(cboEstado.SelectedValue), gEmpresa)
                            Dim iResultado22 As Integer = 0
                            If cboEstado.SelectedValue = "0" Then
                                iResultado22 = eChequera.fActualizarCabecera2(sCodigoActual, "Lleno", 1, gEmpresa, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue))
                            End If

                            'If Condicion = "Lleno" Then
                            '    iResultado = eChequera.fActualizarCabecera2(sCodigoActual, "Lleno", 1, gEmpresa, Trim(cboBanco.SelectedValue), Trim(cboCuenta.SelectedValue))
                            'End If

                            If cboEstado.SelectedIndex = 1 Then
                                'Anulado = 0
                                iResultado2 = eChequera.fActualizarDetalle(sCodigoActual, gEmpresa, Anulado, 1)
                            ElseIf cboEstado.SelectedIndex = 0 Then
                                'Anulado = 2
                                iResultado2 = eChequera.fActualizarDetalle(sCodigoActual, gEmpresa, Anulado, 0)
                            End If
                            'iResultado2 = eChequera.fActualizarDetalle(sCodigoActual, gEmpresa, Anulado, Convert.ToInt32(cboEstado.SelectedValue))
                            If iResultado > 0 Then
                                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
                                MessageBox.Show("Se Actualizo la Chequera Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                mMostrarGrilla()
                                Timer2.Enabled = True
                            End If
                            sTab = 0
                        End If
                    End If
                Else
                    MsgBox("El Rango de Numeros es Incorrecto")
                End If
            Else
                'If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                If Len(Trim(txtNumChequera.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Numero de Chequera", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNumChequera.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtNumFin.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Numero Final de Chequera", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNumFin.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtNumIni.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Numero Inicial de Chequera", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtNumIni.Focus()
                    Exit Sub
                End If

                If cboBanco.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboBanco.Focus()
                    Exit Sub
                End If

                If cboCuenta.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Numero de Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboCuenta.Focus()
                    Exit Sub
                End If

                'eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                frmPrincipal.sFlagGrabar = "0"
            End If

        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'VerPosicion()

        'If Fila("Estados") = "Activo" Then

        '    Dim iResultado As Int32
        '    Dim Mensaje As String = ""
        '    Dim iActivo As Integer
        '    Dim sCodigoActual2 As String = ""
        '    If sTab = 0 Then
        '        Try
        '            If Me.dgvLista.Rows.Count > 0 Then
        '                If Fila("Estados") = "Activo" Then
        '                    iActivo = 5
        '                    Mensaje = "�Desea Anular la Chequera?"
        '                End If
        '                If (MessageBox.Show(Mensaje & Chr(13) & "C�digo: " & Fila("IdChequera") & "   " & Chr(13) & "N�mero Chequera: " & Fila("NroChequera"), "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        '                    'Using scope As TransactionScope = New TransactionScope '(MSDTC)
        '                    eChequera = New clsChequera
        '                    iResultado = eChequera.fEliminar(gEmpresa, Fila("IdChequera"), iActivo) '18/
        '                    Dim eCheque As New clsCheque
        '                    'scope.Complete()
        '                    'End Using
        '                    If iResultado > 0 Then
        '                        mMostrarGrilla()
        '                    End If
        '                Else
        '                    Exit Sub
        '                End If
        '            End If
        '            mMostrarGrilla()
        '        Catch ex As Exception
        '        End Try
        '    End If

        'Else
        '    MessageBox.Show("El Registro se encuentra Inactivo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End If

        'Dim iResultado As Int32
        Try
            '    Using scope As TransactionScope = New TransactionScope
            If dgvLista.Rows.Count > 0 Then
                'If (MessageBox.Show("Desea Eliminar: " & Chr(13) & Chr(13) & "Codigo: " & Fila("AlmCodigo") & "   " & Chr(13) & "Descripcion: " & Fila("AlmDescripcion"), sNombreSistema, MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                '    'Using scope As TransactionScope = New TransactionScope '(MSDTC)
                '    eAlmacen = New clsAlmacen
                '    iResultado = eAlmacen.fEliminar(Fila("ALMCODIGO"), 3)
                '    'scope.Complete()
                '    'End Using
                '    If iResultado = 1 Then
                '        mMostrarGrilla()
                '    End If
                'Else
                '    Exit Sub
                'End If
                MsgBox("Opcion Bloqueada por el Administrador", MsgBoxStyle.Information, glbNameSistema)
            End If
            'scope.Complete()
            'End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try

    End Sub

    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()
        dtpFechaChequera.Value = Now.Date()
        'dtpFechaInicio.Value = Now.Date()
        'dtpFechaFinal.Value = Now.Date()

        'cboMoneda.DataSource = eChequera.fListarMoneda()
        'cboMoneda.ValueMember = "MonCodigo"
        'cboMoneda.DisplayMember = "MonDescripcion"
        'cboMoneda.SelectedIndex = -1

        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"

        cboBanco.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        cboBanco.ValueMember = "IdBanco"
        cboBanco.DisplayMember = "NombreBanco"
        cboBanco.SelectedIndex = -1

        'cboBancos.DataSource = eChequera.fListarBancosxEmpresa(gEmpresa)
        'cboBancos.ValueMember = "IdBanco"
        'cboBancos.DisplayMember = "NombreBanco"
        'cboBancos.SelectedIndex = -1

        'cboEstados.DataSource = eTempo.fColEstados
        'cboEstados.ValueMember = "Col02"
        'cboEstados.DisplayMember = "Col01"
        'cboEstados.SelectedIndex = -1

        'cboCondicion.SelectedIndex = -1
        'dgvLista.Focus()

        dgvLista.Width = 934
        dgvLista.Height = 420
        'frmPrincipal.fBuscar = False 'Desabilita los botones de buscar
        'frmPrincipal.fExportar = False 'Desabilita los botones de exportar
        'frmPrincipal.fImprimir = False 'Desabilita los botones de imprimir
    End Sub

    Private Sub mMostrarGrilla()
        eChequera = New clsChequera
        eTempo = New clsPlantTempo
        dtTableLista = New DataTable
        'frmPrincipal.Barra.Items(2).Enabled = True
        dtTableLista = eChequera.fListar(iEstado01, iEstado02, gEmpresa)

        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTableLista
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        'VerPosicion()
        stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros

        If eChequera.iNroRegistros > 0 Then
            frmPrincipal.Modifica.Enabled = True
            frmPrincipal.Elimina.Enabled = True
            frmPrincipal.Visualiza.Enabled = True
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If

    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        'Dim CodBanco, Nombrebanco, CodCuenta, NroCuenta As String
        Try
            If Me.dtTableLista.Rows.Count > 0 Then
                Fila = dtTableLista.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdChequera").ToString

                'CodBanco = Fila("IdBanco").ToString
                'eChequera.fBuscarNombreBanco(CodBanco)
                'Nombrebanco = eChequera.sNombrebanco
                'cboBanco.Text = Nombrebanco.ToString

                'CodCuenta = Fila("IdCuenta").ToString
                'eChequera.fBuscarNumeroCuenta(CodCuenta)
                'NroCuenta = eChequera.sNumeroCuenta
                'cboCuenta.Text = NroCuenta.ToString

                'CodBanco = cboBanco.Bounds.ToString()
                'CodCuenta = cboCuenta.Bounds.ToString()


                cboBanco.SelectedValue = Fila("IdBanco").ToString
                cboCuenta.SelectedValue = Fila("IdCuenta").ToString

                dtTable2 = New DataTable
                dtTable2 = eChequera.fListarMoneda(Trim(Fila("IdCuenta").ToString), gEmpresa)
                lblMoneda.Text = dtTable2.Rows(0).Item("MonDescripcion")

                txtNumChequera.Text = Fila("NroChequera").ToString
                Me.dtpFechaChequera.Text = Fila("FechaChequera").ToString
                Me.txtNumIni.Text = Fila("NroInicial").ToString
                Me.txtNumFin.Text = Fila("NroFinal").ToString
                'Me.chkUso.Text = Fila("Condicion").ToString
                If Fila("Condicion").ToString = "En Uso" Then
                    chkUso.Checked = True
                Else
                    chkUso.Checked = False
                End If
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtNumChequera.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Private Sub cboCuenta_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'CargarComboCuenta()
    End Sub

    Private Sub cboCuenta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'IdCuenta = cboCuenta.SelectedValue.ToString

        'If (cboCuenta.SelectedIndex > -1) Then
        '    If (IsNumeric(cboCuenta.SelectedValue.ToString())) Then
        '        dtTable = eChequera.fListarMoneda(Me.cboCuenta.SelectedValue)
        '        moncondigo = dtTable.Rows(0)("MonCodigo")
        '        lblMoneda.Text = dtTable.Rows(0)("MonDescripcion")
        '    End If
        'End If
        'Dim NumeroChequera As String = ""
        'If (cboCuenta.SelectedIndex > -1) Then
        '    Try
        '        IdCuenta = cboCuenta.SelectedValue.ToString
        '        If IdCuenta <> "System.Data.DataRowView" Then
        '            dtTable2 = New DataTable
        '            dtTable2 = eChequera.fListarMoneda(IdCuenta)
        '            moncondigo = dtTable2.Rows(0).Item("MonCodigo")
        '            lblMoneda.Text = dtTable2.Rows(0).Item("MonDescripcion")
        '            eChequera.fSiguienteNumChequera(gEmpresa, IdBanco, IdCuenta)
        '            NroChequera = eChequera.sNumSiguienteChequera
        '            txtNumChequera.Text = NroChequera
        '        End If
        '    Catch ex As System.IndexOutOfRangeException
        '        Exit Sub
        '    End Try
        'End If


        'If (cboCuenta.SelectedIndex > -1) Then
        '    Try
        '        IdCuenta = cboCuenta.SelectedValue.ToString
        '        If IdCuenta <> "System.Data.DataRowView" Then
        '            dtTable2 = New DataTable
        '            dtTable2 = eChequera.fListarChequeras(IdEmpresa, IdCuenta, IdBanco)
        '            Me.dgvLista.AutoGenerateColumns = False
        '            Me.dgvLista.DataSource = dtTable2
        '            cmr = Me.BindingContext(Me.dgvLista.DataSource)
        '            Me.stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros

        '            'NroChequera = eChequera.fSiguienteNumeroChequera()
        '            If iOpcion = 1 Then 'Si le dio Click en Nuevo
        '                If cboEmpresa.SelectedValue.ToString.Trim <> "" And cboBanco.SelectedValue.ToString.Trim <> "" And cboCuenta.SelectedValue.ToString.Trim <> "" Then
        '                    NroChequera = eChequera.fSiguienteNumeroChequera(IdEmpresa, IdCuenta, IdBanco, dtTable.Rows.Count)
        '                    txtNumChequera.Text = NroChequera
        '                End If
        '            End If
        '            'Me.cboCuenta.ValueMember = "IdCuenta"
        '            'Me.cboCuenta.DisplayMember = "NumeroCuenta"
        '            'IdCuenta = cboCuenta.SelectedValue
        '            'MostrarDatosDeCuenta(IdCuenta)
        '        End If
        '    Catch ex As System.IndexOutOfRangeException
        '        Exit Sub
        '    End Try
        'End If
    End Sub

    Private Sub CargarComboCuenta()
        'Try
        '    'If (cboCuenta.SelectedIndex > -1) Then
        '    '    If (IsNumeric(cboCuenta.SelectedValue.ToString())) Then
        '    '        dtTable = eChequera.fListarMoneda(Me.cboCuenta.SelectedValue)
        '    '        moncondigo = dtTable.Rows(0)("MonCodigo")
        '    '        lblMoneda.Text = dtTable.Rows(0)("MonDescripcion")
        '    '    End If
        '    'End If

        '    If (cboEmpresa.SelectedIndex > -1) Then
        '        IdEmpresa = cboEmpresa.SelectedValue
        '        Me.cboBanco.DataSource = eChequera.fListarBancosxEmpresa(IdEmpresa)
        '        Me.cboBanco.ValueMember = "IdBanco"
        '        Me.cboBanco.DisplayMember = "NombreBanco"
        '        If (cboBanco.SelectedIndex > -1) Then
        '            IdBanco = cboBanco.SelectedValue
        '            Me.cboCuenta.DataSource = eChequera.fListarCuentasxBancoyEmpresa(IdEmpresa, IdBanco)
        '            Me.cboCuenta.ValueMember = "IdCuenta"
        '            Me.cboCuenta.DisplayMember = "NumeroCuenta"
        '            IdCuenta = cboCuenta.SelectedValue
        '            'MostrarDatosDeCuenta(IdCuenta)
        '        End If
        '    End If
        'Catch ex As System.IndexOutOfRangeException
        '    Exit Sub
        'End Try
    End Sub

    Private Sub cboEmpresa_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Me.cboBanco.DataSource = eChequera.fListarBancos(Convert.ToString(Me.cboEmpresa.SelectedValue))
        'Me.cboBanco.ValueMember = "IdBanco"
        'Me.cboBanco.DisplayMember = "nombrebanco"
    End Sub

    Private Sub cboBanco_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Me.cboCuenta.DataSource = eChequera.fListarCuenta(Convert.ToString(Me.cboBanco.SelectedValue))
        'Me.cboCuenta.ValueMember = "IdCuenta"
        'Me.cboCuenta.DisplayMember = "NumeroCuenta"
    End Sub
 
    Private Sub cboCuenta_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim NUMEROFINAL As String
        'NUMEROFINAL = eChequera.fCodigoNroFinal(cboEmpresa.SelectedValue, cboBanco.SelectedValue, cboCuenta.SelectedValue)
        'txtNumIni.Text = eChequera.fCodigoCheque(IIf(NUMEROFINAL Is Nothing, "", NUMEROFINAL))
        'sCodigoActual3 = eChequera.sCodFuturo2
    End Sub

    Private Sub cboEmpresa_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ''Dim a As String
        'If (cboEmpresa.SelectedIndex > -1) Then
        '    Try
        '        IdEmpresa = cboEmpresa.SelectedValue.ToString
        '        'a = IdEmpresa
        '        'a = a
        '        If IdEmpresa <> "System.Data.DataRowView" Then
        '            Me.cboBanco.DataSource = eChequera.fListarBancosxEmpresa(IdEmpresa)
        '            Me.cboBanco.ValueMember = "IdBanco"
        '            Me.cboBanco.DisplayMember = "NombreBanco"
        '        End If
        '    Catch ex As System.IndexOutOfRangeException
        '        Exit Sub
        '    End Try
        'End If
    End Sub

    'Private Sub cboBanco1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If (cboBanco1.SelectedIndex > -1) Then
    '        Try
    '            IdBanco2 = cboBanco1.SelectedValue.ToString
    '            If IdBanco2 <> "System.Data.DataRowView" Then
    '                Me.cboCuenta1.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco2)
    '                Me.cboCuenta1.ValueMember = "IdCuenta"
    '                Me.cboCuenta1.DisplayMember = "NumeroCuenta"
    '            End If
    '        Catch ex As System.IndexOutOfRangeException
    '            Exit Sub
    '        End Try
    '    End If
    'End Sub

    Private Sub chkUso_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUso.CheckedChanged
        'Try
        '    chkUso.Checked = True
        '    chkLleno.Checked = False
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub chkLleno_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    chkUso.Checked = False
        '    chkLleno.Checked = True
        'Catch ex As Exception

        'End Try

    End Sub

    Private Sub dgvLista_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLista.CellContentClick

    End Sub

    Private Sub cboBanco_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        'If (cboBanco.SelectedIndex > -1) Then
        '    Try
        '        IdBanco = cboBanco.SelectedValue.ToString
        '        If IdBanco <> "System.Data.DataRowView" Then
        '            Me.cboCuenta.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
        '            Me.cboCuenta.ValueMember = "IdCuenta"
        '            Me.cboCuenta.DisplayMember = "NumeroCuenta"
        '        End If
        '    Catch ex As System.IndexOutOfRangeException
        '        Exit Sub
        '    End Try
        'End If

        If (cboBanco.SelectedIndex > -1) Then
            Try
                'MsgBox(cboBanco.SelectedValue.ToString)
                IdBanco = cboBanco.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    cboCuenta.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    cboCuenta.ValueMember = "IdCuenta"
                    cboCuenta.DisplayMember = "NumeroCuenta"
                    cboCuenta.SelectedIndex = -1
                    lblMoneda.Text = ""
                    txtNumChequera.Clear()
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If

    End Sub

    Private Sub cboCuenta_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuenta.SelectedIndexChanged

        Dim NumeroChequera As String = ""
        If (cboCuenta.SelectedIndex > -1) Then
            Try
                IdCuenta = cboCuenta.SelectedValue.ToString
                If IdCuenta <> "System.Data.DataRowView" Then
                    dtTable2 = New DataTable
                    dtTable2 = eChequera.fListarMoneda(IdCuenta, gEmpresa)
                    moncondigo = dtTable2.Rows(0).Item("MonCodigo")
                    lblMoneda.Text = dtTable2.Rows(0).Item("MonDescripcion")
                    eChequera.fSiguienteNumChequera(gEmpresa, IdBanco, IdCuenta)
                    NroChequera = eChequera.sNumSiguienteChequera
                    txtNumChequera.Text = NroChequera

                    Dim tbl As DataTable
                    tbl = New DataTable
                    tbl = eChequera.fUltimoNumeroCheque(gEmpresa, IdBanco, IdCuenta)

                    If tbl.Rows.Count > 0 Then
                        txtNumIni.Text = tbl.Rows(0).Item("NroFinal") + 1
                    Else
                        txtNumIni.Text = 1 'tbl.Rows(0).Item("NroFinal") + 1
                    End If

                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        End If

    End Sub

    Private Sub cboBancos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBancos.SelectedIndexChanged

        Dim IdBancos As String = ""
        'CheckBox1.Checked = False
        cboCuentas.SelectedIndex = -1
        'lblDesMonCuenta.Text = ""
        cboEstados.SelectedIndex = -1
        'cboCondicion.SelectedIndex = -1

        If (cboBancos.SelectedIndex > -1) Then
            Try
                IdBancos = Trim(cboBancos.SelectedValue.ToString)
                If IdBancos <> "System.Data.DataRowView" Then

                    eChequera = New clsChequera
                    cboCuentas.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBancos)
                    'cboCuentas.Items.Add("Seleccionar")
                    cboCuentas.ValueMember = "IdCuenta"
                    cboCuentas.DisplayMember = "NumeroCuenta"
                    cboCuentas.SelectedIndex = -1
                    '----------------------------->
                    eTempo = New clsPlantTempo
                    dtTableLista = New DataTable
                    dtTableLista = eChequera.fListarConsultaChequera(0, gEmpresa, IdBancos, "", "", 0, "", Today(), Today())
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTableLista
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros
                    If eChequera.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                    '----------------------------->
                End If
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Private Sub cboCuentas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCuentas.SelectedIndexChanged
        Dim IdCuentas As String = ""
        'CheckBox1.Checked = False
        'cboCondicion.SelectedIndex = -1
        cboEstados.SelectedIndex = -1
        'cboMoneda.SelectedIndex = -1
        If (cboCuentas.SelectedIndex > -1) Then

            Dim NumeroChequera As String = ""
            Dim CodCuenta As String = ""
            Try
                CodCuenta = cboCuentas.SelectedValue.ToString
                If CodCuenta <> "System.Data.DataRowView" Then
                    dtTable2 = New DataTable
                    dtTable2 = eChequera.fListarMoneda(CodCuenta, gEmpresa)
                    If dtTable2.Rows.Count > 0 Then
                        lblDesMonCuenta.Text = dtTable2.Rows(0).Item("MonDescripcion")
                    Else
                        lblDesMonCuenta.Text = ""
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try

            Try
                IdCuentas = Trim(cboCuentas.SelectedValue.ToString)
                If IdCuentas <> "System.Data.DataRowView" Then
                    eChequera = New clsChequera
                    'cboCuentas.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    'cboCuentas.ValueMember = "IdCuenta"
                    'cboCuentas.DisplayMember = "NumeroCuenta"
                    'cboCuentas.SelectedIndex = -1

                    '----------------------------->
                    'eRegistroDocumento = New clsRegistroDocumento
                    If cboBancos.SelectedIndex > -1 Then ' y condicion =""
                        'busca chekes banco, cuenta,
                        'ElseIf cboBancos.SelectedIndex > -1 And cboCondicion.SelectedIndex > -1 Then
                        'busca chekes banco, cuenta,condicion
                        eTempo = New clsPlantTempo
                        dtTableLista = New DataTable
                        dtTableLista = eChequera.fListarConsultaChequera(1, gEmpresa, Trim(cboBancos.SelectedValue), IdCuentas, "", 0, "", Today(), Today())
                        dgvLista.AutoGenerateColumns = False
                        dgvLista.DataSource = dtTableLista
                        cmr = BindingContext(dgvLista.DataSource)
                        stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros
                        If eChequera.iNroRegistros > 0 Then
                            frmPrincipal.Modifica.Enabled = True
                            frmPrincipal.Elimina.Enabled = True
                            frmPrincipal.Visualiza.Enabled = True
                            frmPrincipal.fDatos = True
                        Else
                            frmPrincipal.Modifica.Enabled = False
                            frmPrincipal.Elimina.Enabled = False
                            frmPrincipal.Visualiza.Enabled = False
                            frmPrincipal.fDatos = False
                        End If
                    End If
                    '----------------------------->

                End If
            Catch ex As Exception
                Exit Sub
            End Try
        Else
            lblDesMonCuenta.Text = ""
        End If
    End Sub

    'Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If CheckBox1.Checked = True Then
    '        dtpFechaInicio.Enabled = True
    '        dtpFechaFinal.Enabled = True
    '    Else
    '        dtpFechaInicio.Enabled = False
    '        dtpFechaFinal.Enabled = False
    '    End If
    'End Sub

    Private Sub cboEstados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEstados.SelectedIndexChanged
        Dim IdEstado As String = ""
        'CheckBox1.Checked = False
        If (cboEstados.SelectedIndex > -1) Then
            Try
                IdEstado = Trim(cboEstados.SelectedValue.ToString)
                If IdEstado <> "System.Data.DataRowView" Then
                    eChequera = New clsChequera
                    eTempo = New clsPlantTempo
                    dtTableLista = New DataTable
                    If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex = -1 Then
                        'traer chequeras de bancos que estan en algun estado
                        dtTableLista = eChequera.fListarConsultaChequera(3, gEmpresa, Trim(cboBancos.SelectedValue), "", "", Trim(cboEstados.SelectedValue), "", Today(), Today())
                    ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 Then
                        'traer chequeras de bancos,cuenta, que estan en algun estado
                        dtTableLista = eChequera.fListarConsultaChequera(4, gEmpresa, Trim(cboBancos.SelectedValue), Trim(cboCuentas.SelectedValue), "", Trim(cboEstados.SelectedValue), "", Today(), Today())
                    ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 Then
                        'traer chequeras de bancos,cuenta,condicion que estan en algun estado
                        dtTableLista = eChequera.fListarConsultaChequera(5, gEmpresa, Trim(cboBancos.SelectedValue), Trim(cboCuentas.SelectedValue), "", Trim(cboEstados.SelectedValue), "", Today(), Today())
                    ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex = -1 Then
                        'traer chequeras de bancos,condicion que estan en algun estado
                        dtTableLista = eChequera.fListarConsultaChequera(10, gEmpresa, Trim(cboBancos.SelectedValue), "", "", Trim(cboEstados.SelectedValue), "", Today(), Today())
                    Else
                        Exit Sub
                    End If
                    dgvLista.AutoGenerateColumns = False
                    dgvLista.DataSource = dtTableLista
                    cmr = BindingContext(dgvLista.DataSource)
                    stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros
                    If eChequera.iNroRegistros > 0 Then
                        frmPrincipal.Modifica.Enabled = True
                        frmPrincipal.Elimina.Enabled = True
                        frmPrincipal.Visualiza.Enabled = True
                        frmPrincipal.fDatos = True
                    Else
                        frmPrincipal.Modifica.Enabled = False
                        frmPrincipal.Elimina.Enabled = False
                        frmPrincipal.Visualiza.Enabled = False
                        frmPrincipal.fDatos = False
                    End If
                    '----------------------------->
                End If
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    'Private Sub cboMoneda_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim IdMone As String = ""
    '    CheckBox1.Checked = False
    '    'cboCuentas.SelectedIndex = -1
    '    If (cboMoneda.SelectedIndex > -1) Then
    '        Try
    '            IdMone = Trim(cboMoneda.SelectedValue.ToString)
    '            If IdMone <> "System.Data.DataRowView" Then
    '                eChequera = New clsChequera
    '                'cboCuentas.DataSource = eChequera.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
    '                'cboCuentas.ValueMember = "IdCuenta"
    '                'cboCuentas.DisplayMember = "NumeroCuenta"
    '                'cboCuentas.SelectedIndex = -1
    '                '----------------------------->
    '                'eRegistroDocumento = New clsRegistroDocumento
    '                If cboBancos.SelectedIndex > -1 Then
    '                    eTempo = New clsPlantTempo
    '                    dtTable = New DataTable
    '                    dtTable = eChequera.fListarConsultaChequera(2, gEmpresa, Trim(cboBancos.SelectedValue), "", Trim(cboMoneda.SelectedValue), 0, "", dtpFechaInicio.Value, dtpFechaFinal.Value)
    '                    dgvLista.AutoGenerateColumns = False
    '                    dgvLista.DataSource = dtTable
    '                    cmr = BindingContext(dgvLista.DataSource)
    '                    stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros
    '                    If eChequera.iNroRegistros > 0 Then
    '                        frmPrincipal.Modifica.Enabled = True
    '                        frmPrincipal.Elimina.Enabled = True
    '                        frmPrincipal.Visualiza.Enabled = True
    '                        frmPrincipal.fDatos = True
    '                    Else
    '                        frmPrincipal.Modifica.Enabled = False
    '                        frmPrincipal.Elimina.Enabled = False
    '                        frmPrincipal.Visualiza.Enabled = False
    '                        frmPrincipal.fDatos = False
    '                    End If
    '                End If
    '                '----------------------------->
    '            End If
    '        Catch ex As Exception
    '            Exit Sub
    '        End Try
    '    End If
    'End Sub

    'Private Sub cboCondicion_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim Condicion As String = ""
    '    CheckBox1.Checked = False
    '    If (cboCondicion.Text <> "") Then
    '        Try
    '            Condicion = Trim(cboCondicion.Text)


    '            eChequera = New clsChequera
    '            eTempo = New clsPlantTempo
    '            dtTableLista = New DataTable
    '            If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex = -1 And cboEstados.SelectedIndex = -1 Then
    '                'traer chequeras de bancos y condicion
    '                dtTableLista = eChequera.fListarConsultaChequera(6, gEmpresa, Trim(cboBancos.SelectedValue), "", "", 0, Condicion, dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboEstados.SelectedIndex = -1 Then
    '                'traer chequeras de bancos,cuenta, condicion
    '                dtTableLista = eChequera.fListarConsultaChequera(7, gEmpresa, Trim(cboBancos.SelectedValue), Trim(cboCuentas.SelectedValue), "", 0, Condicion, dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex = -1 And cboEstados.SelectedIndex > -1 Then
    '                'traer chequeras de bancos, condicion, estado
    '                dtTableLista = eChequera.fListarConsultaChequera(11, gEmpresa, Trim(cboBancos.SelectedValue), "", "", cboEstados.SelectedValue, Condicion, dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            Else
    '                Exit Sub
    '            End If
    '            dgvLista.AutoGenerateColumns = False
    '            dgvLista.DataSource = dtTableLista
    '            cmr = BindingContext(dgvLista.DataSource)
    '            stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros
    '            If eChequera.iNroRegistros > 0 Then
    '                frmPrincipal.Modifica.Enabled = True
    '                frmPrincipal.Elimina.Enabled = True
    '                frmPrincipal.Visualiza.Enabled = True
    '                frmPrincipal.fDatos = True
    '            Else
    '                frmPrincipal.Modifica.Enabled = False
    '                frmPrincipal.Elimina.Enabled = False
    '                frmPrincipal.Visualiza.Enabled = False
    '                frmPrincipal.fDatos = False
    '            End If
    '            '----------------------------->
    '            'End If
    '        Catch ex As Exception
    '            Exit Sub
    '        End Try
    '    End If
    'End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        Panel2.Visible = False
    End Sub

    'Private Sub dtpFechaFinal_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If CheckBox1.Checked = True Then
    '        If dtpFechaInicio.Value > dtpFechaFinal.Value Then
    '            MessageBox.Show("La Fecha de Inicio supera a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            Exit Sub
    '        Else
    '            eChequera = New clsChequera
    '            eTempo = New clsPlantTempo
    '            dtTable = New DataTable
    '            If cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex = -1 And cboCondicion.SelectedIndex = -1 And cboEstados.SelectedIndex = -1 Then
    '                'traer chequeras de bancos y rango de fechas
    '                dtTable = eChequera.fListarConsultaChequera(8, gEmpresa, Trim(cboBancos.SelectedValue), "", "", 0, "", dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboCondicion.SelectedIndex = -1 And cboEstados.SelectedIndex = -1 Then
    '                'traer chequeras de bancos,cuenta, rango de fechas
    '                dtTable = eChequera.fListarConsultaChequera(9, gEmpresa, Trim(cboBancos.SelectedValue), Trim(cboCuentas.SelectedValue), "", 0, "", dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboCondicion.SelectedIndex > -1 And cboEstados.SelectedIndex = -1 Then
    '                'traer chequeras de bancos,cuenta,condicion rango de fechas
    '                dtTable = eChequera.fListarConsultaChequera(12, gEmpresa, Trim(cboBancos.SelectedValue), Trim(cboCuentas.SelectedValue), "", 0, Trim(cboCondicion.Text), dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex > -1 And cboCondicion.SelectedIndex > -1 And cboEstados.SelectedIndex > -1 Then
    '                'traer chequeras de bancos,cuenta,condicion,estado rango de fechas
    '                dtTable = eChequera.fListarConsultaChequera(13, gEmpresa, Trim(cboBancos.SelectedValue), Trim(cboCuentas.SelectedValue), "", cboEstado.SelectedValue, Trim(cboCondicion.Text), dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex = -1 And cboCondicion.SelectedIndex > -1 And cboEstados.SelectedIndex = -1 Then
    '                'traer chequeras de bancos,condicion, rango de fechas
    '                dtTable = eChequera.fListarConsultaChequera(14, gEmpresa, Trim(cboBancos.SelectedValue), "", "", 0, Trim(cboCondicion.Text), dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            ElseIf cboBancos.SelectedIndex > -1 And cboCuentas.SelectedIndex = -1 And cboCondicion.SelectedIndex > -1 And cboEstados.SelectedIndex > -1 Then
    '                'traer chequeras de bancos,condicion, rango de fechas
    '                dtTable = eChequera.fListarConsultaChequera(15, gEmpresa, Trim(cboBancos.SelectedValue), "", "", cboEstados.SelectedValue, Trim(cboCondicion.Text), dtpFechaInicio.Value, dtpFechaFinal.Value)
    '            Else
    '                Exit Sub
    '            End If

    '            dgvLista.AutoGenerateColumns = False
    '            dgvLista.DataSource = dtTable
    '            cmr = BindingContext(dgvLista.DataSource)
    '            stsTotales.Items(0).Text = "Total de Registros= " & eChequera.iNroRegistros
    '            If eChequera.iNroRegistros > 0 Then
    '                frmPrincipal.Modifica.Enabled = True
    '                frmPrincipal.Elimina.Enabled = True
    '                frmPrincipal.Visualiza.Enabled = True
    '                frmPrincipal.fDatos = True
    '            Else
    '                frmPrincipal.Modifica.Enabled = False
    '                frmPrincipal.Elimina.Enabled = False
    '                frmPrincipal.Visualiza.Enabled = False
    '                frmPrincipal.fDatos = False
    '            End If
    '        End If
    '    End If
    'End Sub

    'Private Sub dtpFechaInicio_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    dtpFechaFinal_ValueChanged(sender, e)
    'End Sub
End Class
