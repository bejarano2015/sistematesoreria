Option Explicit On
Imports System.Xml
Imports MSXML2
Imports System.Math

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad

Public Class frmBuscaProveedor

    Private eRetenciones As clsRetencion
    Dim odtProveedor As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared strCodigo As String
    Public Shared strRuc As String
    Public Shared strNombre As String
    Public Shared strDireccion As String
    Public Shared strCtaDetraccion As String

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eRetenciones = New clsRetencion

        CargarProveedores()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarProveedores()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgvRetencion_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcProveedor.CellDoubleClick
        'btnGrabar.PerformClick()
        btnGrabar_Click(Nothing, Nothing)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        gcProveedor.EndEdit()
        If gcProveedor.RowCount > 0 Then
            strCodigo = Convert.ToString(gcProveedor.CurrentRow.Cells("colAnaliticoCodigo").Value)
            strNombre = Convert.ToString(gcProveedor.CurrentRow.Cells("colAnaliticoDescripcion").Value)
            strRuc = Convert.ToString(gcProveedor.CurrentRow.Cells("colRuc").Value)
            strDireccion = Convert.ToString(gcProveedor.CurrentRow.Cells("colDescripcion").Value)
            strCtaDetraccion = Convert.ToString(gcProveedor.CurrentRow.Cells("colCta_Detraccion").Value)
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

#End Region

#Region " Metodos Privados "

    Private Sub CargarProveedores()
        Try
            'Dim odtData As New DataTable
            odtProveedor = eRetenciones.fCargarProveedores(gEmpresa)
            gcProveedor.DataSource = odtProveedor
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

End Class