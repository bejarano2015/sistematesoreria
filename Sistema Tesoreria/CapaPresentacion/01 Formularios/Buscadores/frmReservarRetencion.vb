Option Explicit On
Imports System.Xml
Imports MSXML2
Imports System.Math

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad

Public Class frmReservarRetencion

    Private eRetenciones As clsRetencion
    Private dtNroRetenciones As New DataTable

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eRetenciones = New clsRetencion

        CrearDataTable()
        NumeroCorrelativo()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgvRetencion_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcNroRetenciones.CellDoubleClick
        'btnGrabar.PerformClick()
        btnGrabar_Click(Nothing, Nothing)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        gcNroRetenciones.EndEdit()
        If gcNroRetenciones.RowCount > 0 Then
            Guardar()

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

#End Region

#Region " Metodos Privados "

    Private Sub CrearDataTable()
        dtNroRetenciones.Columns.Add("serie", Type.GetType("System.String"))
        dtNroRetenciones.Columns.Add("numero", Type.GetType("System.String"))
    End Sub

    Private Sub NumeroCorrelativo()
        Try
            Dim iResultado As String = ""

            iResultado = eRetenciones.fNroCorrelativo("001", 12)
            txtSerie.Text = "001"
            txtNroDocumento.Text = iResultado.ToString
        Catch ex As Exception
            MessageBox.Show("No se pudo obtener el �ltimo registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If IsNumeric(txtNroReservas.Text.Trim) = True Then
            Dim nroReservas As Integer = txtNroReservas.Text.Trim
            dtNroRetenciones.Clear()

            Dim nroMaximo As String
            Dim nroActual As Integer = txtNroDocumento.Text.Trim
            Dim sSufijo As String = "0000000"

            nroActual -= 1
            For i As Integer = 0 To nroReservas - 1
                nroActual += 1
                nroMaximo = Trim(Microsoft.VisualBasic.Right(sSufijo & Convert.ToString(Trim(nroActual)), sSufijo.Length))

                Dim row As DataRow
                row = dtNroRetenciones.NewRow
                row("serie") = txtSerie.Text.Trim
                row("numero") = nroMaximo

                dtNroRetenciones.Rows.Add(row)
                'stigvDetFormulaCalculo.UpdateCurrentRow()
                gcNroRetenciones.DataSource = dtNroRetenciones
            Next i

            lblRegistros.Text = gcNroRetenciones.Rows.Count() & " Registros"
        End If
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If gcNroRetenciones.RowCount > 0 Then
            ''Dim intFila As Integer = gcNroRetenciones.Rows.RemoveAt( .CurrentRow.Index
            ''Dim intFila As Integer = gcNroRetenciones.CurrentRow.Index
            ''odtTipDestinoGasto.Rows(intFila)("IDTIPOGASTODESTINO") = -1
            'gcNroRetenciones.Rows.Remove(gcNroRetenciones.i.CurrentRow)
            'dtNroRetenciones.AcceptChanges()
        End If
    End Sub

#Region " M�todos de BD "
    Private Sub Guardar()
        Try
            Dim iResultado As Int32
            Dim idRetencionCab As Decimal = 0

            For i As Integer = 0 To gcNroRetenciones.RowCount - 1
                iResultado = eRetenciones.fGrabar(0, gEmpresa, 101, Convert.ToString(gcNroRetenciones.Rows(i).Cells("colserie").Value), Convert.ToString(gcNroRetenciones.Rows(i).Cells("colnumero").Value), Date.Today, "", "", "", "", "", 1, 0, 0, "1", "0", 1)
            Next
        Catch ex As Exception
            MessageBox.Show("No se pudo grabar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

End Class