<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBuscaProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBuscaProveedor))
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.mnuMantenimiento = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnRefrescar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.pnCuerpo = New System.Windows.Forms.Panel()
        Me.gcProveedor = New Telerik.WinControls.UI.RadGridView()
        Me.pnPie = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.mnuMantenimiento.SuspendLayout()
        Me.pnCuerpo.SuspendLayout()
        CType(Me.gcProveedor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcProveedor.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.mnuMantenimiento)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(739, 28)
        Me.Panel1.TabIndex = 171
        '
        'mnuMantenimiento
        '
        Me.mnuMantenimiento.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnCancelar, Me.btnRefrescar, Me.btnEliminar, Me.btnImprimir, Me.btnSalir})
        Me.mnuMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.mnuMantenimiento.Name = "mnuMantenimiento"
        Me.mnuMantenimiento.Size = New System.Drawing.Size(737, 25)
        Me.mnuMantenimiento.TabIndex = 0
        Me.mnuMantenimiento.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        Me.btnNuevo.Visible = False
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnCancelar
        '
        Me.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(23, 22)
        Me.btnCancelar.Text = "ToolStripButton1"
        Me.btnCancelar.ToolTipText = "Cancelar"
        Me.btnCancelar.Visible = False
        '
        'btnRefrescar
        '
        Me.btnRefrescar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRefrescar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Grid_properties_
        Me.btnRefrescar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(23, 22)
        Me.btnRefrescar.Text = "&Refrescar"
        Me.btnRefrescar.ToolTipText = "Refrescar"
        '
        'btnEliminar
        '
        Me.btnEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.ToolTipText = "Eliminar"
        Me.btnEliminar.Visible = False
        '
        'btnImprimir
        '
        Me.btnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimir.Text = "ToolStripButton1"
        Me.btnImprimir.ToolTipText = "Imprimir Retención"
        Me.btnImprimir.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'pnCuerpo
        '
        Me.pnCuerpo.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnCuerpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnCuerpo.Controls.Add(Me.gcProveedor)
        Me.pnCuerpo.Controls.Add(Me.pnPie)
        Me.pnCuerpo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnCuerpo.Location = New System.Drawing.Point(0, 28)
        Me.pnCuerpo.Name = "pnCuerpo"
        Me.pnCuerpo.Size = New System.Drawing.Size(739, 369)
        Me.pnCuerpo.TabIndex = 173
        '
        'gcProveedor
        '
        Me.gcProveedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gcProveedor.Location = New System.Drawing.Point(0, 0)
        '
        'gcProveedor
        '
        Me.gcProveedor.MasterTemplate.AllowAddNewRow = False
        Me.gcProveedor.MasterTemplate.AllowColumnReorder = False
        GridViewTextBoxColumn1.FieldName = "EmprCodigo"
        GridViewTextBoxColumn1.HeaderText = "IdEmpresa"
        GridViewTextBoxColumn1.IsVisible = False
        GridViewTextBoxColumn1.Name = "colIdEmpresa"
        GridViewTextBoxColumn1.ReadOnly = True
        GridViewTextBoxColumn2.FieldName = "TipoAnaliticoCodigo"
        GridViewTextBoxColumn2.HeaderText = "TipoAnaliticoCodigo"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "colTipoAnaliticoCodigo"
        GridViewTextBoxColumn2.ReadOnly = True
        GridViewTextBoxColumn2.Width = 91
        GridViewTextBoxColumn3.FieldName = "AnaliticoCodigo"
        GridViewTextBoxColumn3.HeaderText = "AnaliticoCodigo"
        GridViewTextBoxColumn3.IsVisible = False
        GridViewTextBoxColumn3.Name = "colAnaliticoCodigo"
        GridViewTextBoxColumn3.ReadOnly = True
        GridViewTextBoxColumn3.Width = 100
        GridViewTextBoxColumn4.FieldName = "AnaliticoDescripcion"
        GridViewTextBoxColumn4.HeaderText = "Descripción"
        GridViewTextBoxColumn4.Name = "colAnaliticoDescripcion"
        GridViewTextBoxColumn4.ReadOnly = True
        GridViewTextBoxColumn4.Width = 300
        GridViewTextBoxColumn5.FieldName = "Ruc"
        GridViewTextBoxColumn5.HeaderText = "Ruc"
        GridViewTextBoxColumn5.Name = "colRuc"
        GridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn5.Width = 100
        GridViewTextBoxColumn6.FieldName = "Direccion"
        GridViewTextBoxColumn6.HeaderText = "Direccion"
        GridViewTextBoxColumn6.Name = "colDescripcion"
        GridViewTextBoxColumn6.Width = 300
        GridViewTextBoxColumn7.FieldName = "Cta_Detraccion"
        GridViewTextBoxColumn7.HeaderText = "Cta Detracción"
        GridViewTextBoxColumn7.IsVisible = False
        GridViewTextBoxColumn7.Name = "colCta_Detraccion"
        Me.gcProveedor.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7})
        Me.gcProveedor.MasterTemplate.EnableFiltering = True
        Me.gcProveedor.Name = "gcProveedor"
        Me.gcProveedor.ReadOnly = True
        Me.gcProveedor.Size = New System.Drawing.Size(737, 348)
        Me.gcProveedor.TabIndex = 175
        Me.gcProveedor.Text = "radGridView2"
        '
        'pnPie
        '
        Me.pnPie.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnPie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnPie.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnPie.Location = New System.Drawing.Point(0, 348)
        Me.pnPie.Name = "pnPie"
        Me.pnPie.Size = New System.Drawing.Size(737, 19)
        Me.pnPie.TabIndex = 174
        '
        'frmBuscaProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(739, 397)
        Me.Controls.Add(Me.pnCuerpo)
        Me.Controls.Add(Me.Panel1)
        Me.MinimizeBox = False
        Me.Name = "frmBuscaProveedor"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Búsqueda de Proveedores"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.mnuMantenimiento.ResumeLayout(False)
        Me.mnuMantenimiento.PerformLayout()
        Me.pnCuerpo.ResumeLayout(False)
        CType(Me.gcProveedor.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcProveedor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents mnuMantenimiento As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnRefrescar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnCuerpo As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnPie As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.ToolStripButton
    Private WithEvents gcProveedor As Telerik.WinControls.UI.RadGridView
End Class
