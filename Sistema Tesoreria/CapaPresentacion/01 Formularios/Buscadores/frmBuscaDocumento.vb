Option Explicit On
Imports System.Xml
Imports MSXML2
Imports System.Math

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad

Public Class frmBuscaDocumento

    Private eRendiciones As clsRendicion
    Dim odtDocumentos As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared idNroItem As String
    Public Shared numeroDocumento As String
    Public Shared ruc As String
    Public Shared nombre As String
    Public Shared moneda As String

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        eRendiciones = New clsRendicion

        CargarDocumentos()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarDocumentos()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub dgvRetencion_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcDocumentos.CellDoubleClick
        'btnGrabar.PerformClick()
        btnGrabar_Click(Nothing, Nothing)
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        gcDocumentos.EndEdit()
        If gcDocumentos.RowCount > 0 Then
            idNroItem = Convert.ToString(gcDocumentos.CurrentRow.Cells("colNROITEM").Value)
            numeroDocumento = Convert.ToString(gcDocumentos.CurrentRow.Cells("colNroOC").Value)
            ruc = Convert.ToString(gcDocumentos.CurrentRow.Cells("colCodProveedor").Value)
            nombre = Convert.ToString(gcDocumentos.CurrentRow.Cells("colNombreProveedor").Value)
            'nombre = Convert.ToString(gcDocumentos.CurrentRow.Cells("colImporteTotal").Value)
            moneda = Convert.ToString(gcDocumentos.CurrentRow.Cells("colIdMoneda").Value)

            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        End If
    End Sub

#End Region

#Region " Metodos Privados "

    Private Sub CargarDocumentos()
        Try
            'Dim odtData As New DataTable
            odtDocumentos = eRendiciones.fCargarDocumentos(gEmpresa)
            gcDocumentos.DataSource = odtDocumentos
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

End Class