<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReservarRetencion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReservarRetencion))
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.mnuMantenimiento = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnCancelar = New System.Windows.Forms.ToolStripButton()
        Me.btnRefrescar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.pnCuerpo = New System.Windows.Forms.Panel()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.txtNroReservas = New System.Windows.Forms.TextBox()
        Me.btnAceptar = New Telerik.WinControls.UI.RadButton()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.txtNroDocumento = New System.Windows.Forms.TextBox()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel()
        Me.gcNroRetenciones = New Telerik.WinControls.UI.RadGridView()
        Me.pnPie = New System.Windows.Forms.Panel()
        Me.lblRegistros = New ctrLibreria.Controles.BeLabel()
        Me.Panel1.SuspendLayout()
        Me.mnuMantenimiento.SuspendLayout()
        Me.pnCuerpo.SuspendLayout()
        CType(Me.btnAceptar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcNroRetenciones, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gcNroRetenciones.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnPie.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.mnuMantenimiento)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.ForeColor = System.Drawing.Color.DarkBlue
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(355, 28)
        Me.Panel1.TabIndex = 171
        '
        'mnuMantenimiento
        '
        Me.mnuMantenimiento.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGrabar, Me.btnCancelar, Me.btnRefrescar, Me.btnEliminar, Me.btnImprimir, Me.btnSalir})
        Me.mnuMantenimiento.Location = New System.Drawing.Point(0, 0)
        Me.mnuMantenimiento.Name = "mnuMantenimiento"
        Me.mnuMantenimiento.Size = New System.Drawing.Size(353, 25)
        Me.mnuMantenimiento.TabIndex = 0
        Me.mnuMantenimiento.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo"
        Me.btnNuevo.Visible = False
        '
        'btnGrabar
        '
        Me.btnGrabar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(23, 22)
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.ToolTipText = "Grabar"
        '
        'btnCancelar
        '
        Me.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCancelar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Undo_
        Me.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(23, 22)
        Me.btnCancelar.Text = "ToolStripButton1"
        Me.btnCancelar.ToolTipText = "Cancelar"
        Me.btnCancelar.Visible = False
        '
        'btnRefrescar
        '
        Me.btnRefrescar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnRefrescar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Grid_properties_
        Me.btnRefrescar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(23, 22)
        Me.btnRefrescar.Text = "&Refrescar"
        Me.btnRefrescar.ToolTipText = "Refrescar"
        Me.btnRefrescar.Visible = False
        '
        'btnEliminar
        '
        Me.btnEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnEliminar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(23, 22)
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.ToolTipText = "Eliminar"
        Me.btnEliminar.Visible = False
        '
        'btnImprimir
        '
        Me.btnImprimir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(23, 22)
        Me.btnImprimir.Text = "ToolStripButton1"
        Me.btnImprimir.ToolTipText = "Imprimir Retención"
        Me.btnImprimir.Visible = False
        '
        'btnSalir
        '
        Me.btnSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(23, 22)
        Me.btnSalir.Text = "&Salir"
        Me.btnSalir.ToolTipText = "Salir"
        '
        'pnCuerpo
        '
        Me.pnCuerpo.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnCuerpo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnCuerpo.Controls.Add(Me.btnQuitar)
        Me.pnCuerpo.Controls.Add(Me.txtNroReservas)
        Me.pnCuerpo.Controls.Add(Me.btnAceptar)
        Me.pnCuerpo.Controls.Add(Me.BeLabel1)
        Me.pnCuerpo.Controls.Add(Me.txtNroDocumento)
        Me.pnCuerpo.Controls.Add(Me.txtSerie)
        Me.pnCuerpo.Controls.Add(Me.BeLabel29)
        Me.pnCuerpo.Controls.Add(Me.gcNroRetenciones)
        Me.pnCuerpo.Controls.Add(Me.pnPie)
        Me.pnCuerpo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnCuerpo.Location = New System.Drawing.Point(0, 28)
        Me.pnCuerpo.Name = "pnCuerpo"
        Me.pnCuerpo.Size = New System.Drawing.Size(355, 282)
        Me.pnCuerpo.TabIndex = 173
        '
        'btnQuitar
        '
        Me.btnQuitar.Image = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.btnQuitar.Location = New System.Drawing.Point(314, 76)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(35, 23)
        Me.btnQuitar.TabIndex = 182
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'txtNroReservas
        '
        Me.txtNroReservas.BackColor = System.Drawing.Color.Ivory
        Me.txtNroReservas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroReservas.Location = New System.Drawing.Point(100, 46)
        Me.txtNroReservas.Name = "txtNroReservas"
        Me.txtNroReservas.Size = New System.Drawing.Size(52, 20)
        Me.txtNroReservas.TabIndex = 181
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(158, 42)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(57, 24)
        Me.btnAceptar.TabIndex = 180
        Me.btnAceptar.Text = "Generar"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(21, 46)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel1.TabIndex = 179
        Me.BeLabel1.Text = "Nro reservas"
        '
        'txtNroDocumento
        '
        Me.txtNroDocumento.BackColor = System.Drawing.Color.Ivory
        Me.txtNroDocumento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNroDocumento.Enabled = False
        Me.txtNroDocumento.Location = New System.Drawing.Point(158, 14)
        Me.txtNroDocumento.Name = "txtNroDocumento"
        Me.txtNroDocumento.Size = New System.Drawing.Size(96, 20)
        Me.txtNroDocumento.TabIndex = 178
        '
        'txtSerie
        '
        Me.txtSerie.BackColor = System.Drawing.Color.Ivory
        Me.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerie.Enabled = False
        Me.txtSerie.Location = New System.Drawing.Point(100, 14)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(52, 20)
        Me.txtSerie.TabIndex = 177
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.ForeColor = System.Drawing.Color.Black
        Me.BeLabel29.Location = New System.Drawing.Point(11, 16)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel29.TabIndex = 176
        Me.BeLabel29.Text = "Serie - Número"
        '
        'gcNroRetenciones
        '
        Me.gcNroRetenciones.Location = New System.Drawing.Point(14, 76)
        '
        'gcNroRetenciones
        '
        Me.gcNroRetenciones.MasterTemplate.AllowAddNewRow = False
        Me.gcNroRetenciones.MasterTemplate.AllowColumnReorder = False
        GridViewTextBoxColumn1.FieldName = "serie"
        GridViewTextBoxColumn1.HeaderText = "Serie"
        GridViewTextBoxColumn1.Name = "colserie"
        GridViewTextBoxColumn1.ReadOnly = True
        GridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn1.Width = 90
        GridViewTextBoxColumn2.FieldName = "numero"
        GridViewTextBoxColumn2.HeaderText = "Número"
        GridViewTextBoxColumn2.Name = "colnumero"
        GridViewTextBoxColumn2.ReadOnly = True
        GridViewTextBoxColumn2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn2.Width = 150
        Me.gcNroRetenciones.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2})
        Me.gcNroRetenciones.Name = "gcNroRetenciones"
        Me.gcNroRetenciones.ReadOnly = True
        Me.gcNroRetenciones.ShowGroupPanel = False
        Me.gcNroRetenciones.Size = New System.Drawing.Size(294, 179)
        Me.gcNroRetenciones.TabIndex = 175
        Me.gcNroRetenciones.Text = "radGridView2"
        '
        'pnPie
        '
        Me.pnPie.BackColor = System.Drawing.Color.LightSteelBlue
        Me.pnPie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnPie.Controls.Add(Me.lblRegistros)
        Me.pnPie.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnPie.Location = New System.Drawing.Point(0, 261)
        Me.pnPie.Name = "pnPie"
        Me.pnPie.Size = New System.Drawing.Size(353, 19)
        Me.pnPie.TabIndex = 174
        '
        'lblRegistros
        '
        Me.lblRegistros.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblRegistros.AutoSize = True
        Me.lblRegistros.BackColor = System.Drawing.Color.Transparent
        Me.lblRegistros.ForeColor = System.Drawing.Color.Black
        Me.lblRegistros.Location = New System.Drawing.Point(6, 2)
        Me.lblRegistros.Name = "lblRegistros"
        Me.lblRegistros.Size = New System.Drawing.Size(60, 13)
        Me.lblRegistros.TabIndex = 183
        Me.lblRegistros.Text = "0 Registros"
        '
        'frmReservarRetencion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 310)
        Me.Controls.Add(Me.pnCuerpo)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReservarRetencion"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reservar Retenciones"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.mnuMantenimiento.ResumeLayout(False)
        Me.mnuMantenimiento.PerformLayout()
        Me.pnCuerpo.ResumeLayout(False)
        Me.pnCuerpo.PerformLayout()
        CType(Me.btnAceptar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcNroRetenciones.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gcNroRetenciones, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnPie.ResumeLayout(False)
        Me.pnPie.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents mnuMantenimiento As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnRefrescar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnCuerpo As System.Windows.Forms.Panel
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents pnPie As System.Windows.Forms.Panel
    Friend WithEvents btnCancelar As System.Windows.Forms.ToolStripButton
    Private WithEvents gcNroRetenciones As Telerik.WinControls.UI.RadGridView
    Friend WithEvents txtNroDocumento As System.Windows.Forms.TextBox
    Friend WithEvents txtSerie As System.Windows.Forms.TextBox
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents btnAceptar As Telerik.WinControls.UI.RadButton
    Friend WithEvents txtNroReservas As System.Windows.Forms.TextBox
    Friend WithEvents btnQuitar As System.Windows.Forms.Button
    Friend WithEvents lblRegistros As ctrLibreria.Controles.BeLabel
End Class
