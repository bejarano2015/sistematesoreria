Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteAvanceDiario

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    Public IdPartida As String = ""
    Public Fecha As DateTime
    Dim rptFacPorPagar As New rptAvanceDiarioPartida

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteAvanceDiario = Nothing
    Public Shared Function Instance() As frmReporteAvanceDiario
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteAvanceDiario
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteAvanceDiario_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteAvanceDiario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        rptFacPorPagar.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '0
        crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@IdPartida")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(Trim(IdPartida))
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '1
        crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Empresa")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(gDesEmpresa)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        '2
        crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Ruc")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(gEmprRuc)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '3
        crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Trim(gEmpresa)
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '5
        crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@Fecha")
        crParameterValues = crParameterFieldDefinition.CurrentValues
        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = Format(Fecha, "yyyy-MM-dd")
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
        CrystalReportViewer1.ReportSource = rptFacPorPagar

    End Sub

End Class