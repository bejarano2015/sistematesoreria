<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConciliacionBanc
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtSaldoBanco = New System.Windows.Forms.TextBox
        Me.txtTenerBanco = New System.Windows.Forms.TextBox
        Me.dgvDetalle = New System.Windows.Forms.DataGridView
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.txtSaldoLibro = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.txtTotal = New ctrLibreria.Controles.BeTextBox
        Me.txtCheCobra = New ctrLibreria.Controles.BeTextBox
        Me.txtCheNoCobra = New ctrLibreria.Controles.BeTextBox
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.txtDif = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BeLabel4)
        Me.GroupBox1.Controls.Add(Me.txtDif)
        Me.GroupBox1.Controls.Add(Me.txtSaldoBanco)
        Me.GroupBox1.Controls.Add(Me.txtTenerBanco)
        Me.GroupBox1.Controls.Add(Me.dgvDetalle)
        Me.GroupBox1.Controls.Add(Me.BeLabel1)
        Me.GroupBox1.Controls.Add(Me.txtSaldoLibro)
        Me.GroupBox1.Controls.Add(Me.BeLabel12)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(490, 221)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Conciliacion Bancaria"
        '
        'txtSaldoBanco
        '
        Me.txtSaldoBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoBanco.Location = New System.Drawing.Point(319, 39)
        Me.txtSaldoBanco.Name = "txtSaldoBanco"
        Me.txtSaldoBanco.Size = New System.Drawing.Size(134, 20)
        Me.txtSaldoBanco.TabIndex = 7
        Me.txtSaldoBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTenerBanco
        '
        Me.txtTenerBanco.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtTenerBanco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTenerBanco.Enabled = False
        Me.txtTenerBanco.Location = New System.Drawing.Point(319, 13)
        Me.txtTenerBanco.Name = "txtTenerBanco"
        Me.txtTenerBanco.Size = New System.Drawing.Size(134, 20)
        Me.txtTenerBanco.TabIndex = 6
        Me.txtTenerBanco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column5, Me.Column4})
        Me.dgvDetalle.EnableHeadersVisualStyles = False
        Me.dgvDetalle.Location = New System.Drawing.Point(6, 65)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.RowHeadersVisible = False
        Me.dgvDetalle.Size = New System.Drawing.Size(469, 95)
        Me.dgvDetalle.TabIndex = 5
        '
        'Column5
        '
        Me.Column5.HeaderText = "Concepto"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 300
        '
        'Column4
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column4.HeaderText = "Importe"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 145
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(164, 173)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(131, 13)
        Me.BeLabel1.TabIndex = 4
        Me.BeLabel1.Text = "Saldo segun Libro Bancos"
        '
        'txtSaldoLibro
        '
        Me.txtSaldoLibro.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSaldoLibro.BackColor = System.Drawing.Color.Ivory
        Me.txtSaldoLibro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSaldoLibro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSaldoLibro.Enabled = False
        Me.txtSaldoLibro.ForeColor = System.Drawing.Color.Black
        Me.txtSaldoLibro.KeyEnter = True
        Me.txtSaldoLibro.Location = New System.Drawing.Point(319, 166)
        Me.txtSaldoLibro.Name = "txtSaldoLibro"
        Me.txtSaldoLibro.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSaldoLibro.ShortcutsEnabled = False
        Me.txtSaldoLibro.Size = New System.Drawing.Size(134, 20)
        Me.txtSaldoLibro.TabIndex = 3
        Me.txtSaldoLibro.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSaldoLibro.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(164, 46)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(153, 13)
        Me.BeLabel12.TabIndex = 1
        Me.BeLabel12.Text = "Saldo segun Extracto Bancario"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnImprimir)
        Me.GroupBox2.Controls.Add(Me.BeLabel3)
        Me.GroupBox2.Controls.Add(Me.BeLabel2)
        Me.GroupBox2.Controls.Add(Me.txtTotal)
        Me.GroupBox2.Controls.Add(Me.txtCheCobra)
        Me.GroupBox2.Controls.Add(Me.txtCheNoCobra)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 239)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(490, 127)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(319, 94)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(134, 23)
        Me.btnImprimir.TabIndex = 5
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(6, 50)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(0, 13)
        Me.BeLabel3.TabIndex = 4
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(6, 24)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(0, 13)
        Me.BeLabel2.TabIndex = 3
        '
        'txtTotal
        '
        Me.txtTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotal.BackColor = System.Drawing.Color.Ivory
        Me.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.Enabled = False
        Me.txtTotal.ForeColor = System.Drawing.Color.Black
        Me.txtTotal.KeyEnter = True
        Me.txtTotal.Location = New System.Drawing.Point(319, 68)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotal.ShortcutsEnabled = False
        Me.txtTotal.Size = New System.Drawing.Size(134, 20)
        Me.txtTotal.TabIndex = 2
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCheCobra
        '
        Me.txtCheCobra.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCheCobra.BackColor = System.Drawing.Color.Ivory
        Me.txtCheCobra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCheCobra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCheCobra.Enabled = False
        Me.txtCheCobra.ForeColor = System.Drawing.Color.Black
        Me.txtCheCobra.KeyEnter = True
        Me.txtCheCobra.Location = New System.Drawing.Point(319, 42)
        Me.txtCheCobra.Name = "txtCheCobra"
        Me.txtCheCobra.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCheCobra.ShortcutsEnabled = False
        Me.txtCheCobra.Size = New System.Drawing.Size(134, 20)
        Me.txtCheCobra.TabIndex = 1
        Me.txtCheCobra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCheCobra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtCheNoCobra
        '
        Me.txtCheNoCobra.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCheNoCobra.BackColor = System.Drawing.Color.Ivory
        Me.txtCheNoCobra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCheNoCobra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCheNoCobra.Enabled = False
        Me.txtCheNoCobra.ForeColor = System.Drawing.Color.Black
        Me.txtCheNoCobra.KeyEnter = True
        Me.txtCheNoCobra.Location = New System.Drawing.Point(319, 16)
        Me.txtCheNoCobra.Name = "txtCheNoCobra"
        Me.txtCheNoCobra.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCheNoCobra.ShortcutsEnabled = False
        Me.txtCheNoCobra.Size = New System.Drawing.Size(134, 20)
        Me.txtCheNoCobra.TabIndex = 0
        Me.txtCheNoCobra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCheNoCobra.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'Timer1
        '
        '
        'txtDif
        '
        Me.txtDif.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDif.BackColor = System.Drawing.Color.Ivory
        Me.txtDif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDif.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDif.ForeColor = System.Drawing.Color.DarkRed
        Me.txtDif.KeyEnter = True
        Me.txtDif.Location = New System.Drawing.Point(319, 192)
        Me.txtDif.Name = "txtDif"
        Me.txtDif.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDif.ReadOnly = True
        Me.txtDif.ShortcutsEnabled = False
        Me.txtDif.Size = New System.Drawing.Size(134, 20)
        Me.txtDif.TabIndex = 8
        Me.txtDif.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDif.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(164, 199)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel4.TabIndex = 9
        Me.BeLabel4.Text = "Diferencia"
        '
        'frmConciliacionBanc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(514, 377)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.Name = "frmConciliacionBanc"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conciliacion Bancaria"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSaldoLibro As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCheCobra As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtCheNoCobra As ctrLibreria.Controles.BeTextBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents txtTenerBanco As System.Windows.Forms.TextBox
    Friend WithEvents txtSaldoBanco As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtDif As ctrLibreria.Controles.BeTextBox
End Class
