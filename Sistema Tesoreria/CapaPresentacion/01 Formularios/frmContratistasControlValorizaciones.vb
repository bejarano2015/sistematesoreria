Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports System.Data

Public Class frmContratistasControlValorizaciones

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmContratistasControlValorizaciones = Nothing
    Public Shared Function Instance() As frmContratistasControlValorizaciones
        If frmInstance Is Nothing Then
            frmInstance = New frmContratistasControlValorizaciones
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmContratistasControlValorizaciones_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eValorizaciones As clsValorizaciones

    Private Sub frmContratistasControlValorizaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eValorizaciones = New clsValorizaciones
        cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today(), "")
        If eValorizaciones.iNroRegistros > 0 Then
            cboObra.ValueMember = "ObraCodigo"
            cboObra.DisplayMember = "ObraDescripcion"
            cboObra.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboContratista_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboContratista.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            CCosto.DataSource = eValorizaciones.fListarObras(76, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                CCosto.ValueMember = "CCosCodigo"
                CCosto.DisplayMember = "CCosDescripcion"
                CCosto.SelectedIndex = -1
            End If
            'End If
        End If

        For x As Integer = 0 To dgvHistorial.RowCount - 1
            dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
        Next

        

    End Sub

    Private Sub cboObra_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboObra.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then
            eValorizaciones = New clsValorizaciones
            cboContratista.DataSource = eValorizaciones.fListarObras(1, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), "", Today(), "")
            If eValorizaciones.iNroRegistros > 0 Then
                cboContratista.ValueMember = "IdContratista"
                cboContratista.DisplayMember = "Nombre"
                cboContratista.SelectedIndex = -1
                cboPartidas.DataSource = Nothing
                cboPartidas.SelectedIndex = -1
            End If
        End If
        For x As Integer = 0 To dgvHistorial.RowCount - 1
            dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
        Next
    End Sub

    Private Sub cboPartidas_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectionChangeCommitted

        If cboPartidas.SelectedIndex <> -1 Then
            For x As Integer = 0 To dgvHistorial.RowCount - 1
                dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
            Next
            dgvHistorial.AutoGenerateColumns = False
            eValorizaciones = New clsValorizaciones
            Dim dtValorizaciones As DataTable
            dtValorizaciones = New DataTable
            dtValorizaciones = eValorizaciones.fListarObras(69, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboPartidas.SelectedValue), Today(), "")
            For x As Integer = 0 To dgvHistorial.RowCount - 1
                dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
            Next
            If dtValorizaciones.Rows.Count > 0 Then
                'dgvHistorial.DataSource = dtValorizaciones
                For y As Integer = 0 To dtValorizaciones.Rows.Count - 1
                    dgvHistorial.Rows.Add()
                    dgvHistorial.Rows(y).Cells("Concepto").Value = dtValorizaciones.Rows(y).Item("Concepto").ToString
                    dgvHistorial.Rows(y).Cells("IdValorizacion").Value = dtValorizaciones.Rows(y).Item("IdValorizacion").ToString
                    dgvHistorial.Rows(y).Cells("Cerrado").Value = dtValorizaciones.Rows(y).Item("Cerrado").ToString  '"0"
                    dgvHistorial.Rows(y).Cells("Semana").Value = dtValorizaciones.Rows(y).Item("Semana").ToString
                    If Trim(dgvHistorial.Rows(y).Cells("Cerrado").Value) = "1" Then
                        dgvHistorial.Rows(y).Cells("X").Value = True
                        dgvHistorial.Rows(y).Cells("X").ReadOnly = False
                    ElseIf Trim(dgvHistorial.Rows(y).Cells("Cerrado").Value) = "0" Then
                        dgvHistorial.Rows(y).Cells("X").Value = False
                        dgvHistorial.Rows(y).Cells("X").ReadOnly = True
                    End If
                Next
            End If
        End If
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If MsgBox("Esta seguro de aplicar los cambios?", MsgBoxStyle.YesNo, glbNameSistema) = MsgBoxResult.No Then
            Exit Sub
        End If
        Me.dgvHistorial.ClearSelection()
        For z As Integer = 0 To dgvHistorial.Rows.Count - 1
            Dim sIdValorizacion As String = ""
            sIdValorizacion = Trim(dgvHistorial.Rows(z).Cells("IdValorizacion").Value)

            If Microsoft.VisualBasic.IsDBNull(dgvHistorial.Rows(z).Cells("X").Value) = True Then
                'Habilita = Habilita & "0"
            Else
                'eSesionCajas = New clsSesionCajas
                If Convert.ToBoolean(dgvHistorial.Rows(z).Cells("X").Value) = False Then
                    Dim iActCabValorizacion As Integer = 0
                    iActCabValorizacion = eValorizaciones.fActDetalleAvance(70, gEmpresa, "", Trim(sIdValorizacion), gUsuario, 0, gPeriodo)
                    If iActCabValorizacion = 1 Then
                        Dim dtTable2 As DataTable
                        dtTable2 = New DataTable
                        eValorizaciones = New clsValorizaciones
                        dtTable2 = eValorizaciones.fListarObras(68, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(sIdValorizacion), Today(), "")
                        If dtTable2.Rows.Count > 0 Then
                            For x As Integer = 0 To dtTable2.Rows.Count - 1
                                Dim sIdPartidaAvanceDiario As String = ""
                                Dim sIdPartida As String = ""
                                Dim iActDetalleAvance As Integer = 0
                                sIdPartidaAvanceDiario = Trim(dtTable2.Rows(x).Item("IdPartidaAvanceDiario"))
                                sIdPartida = Trim(dtTable2.Rows(x).Item("IdPartida"))
                                iActDetalleAvance = eValorizaciones.fActDetalleAvance(67, gEmpresa, Trim(sIdPartidaAvanceDiario), Trim(sIdPartida), gUsuario, 0, gPeriodo)
                            Next
                        End If
                    End If
                    'Else
                    'Habilita = Habilita & "1"
                    'eSesionCajas.fActivaryDesActivarSesion(33, dgvHistorial.Rows(z).Cells("IdValorizacion").Value, gEmpresa)
                End If
            End If
        Next
        cboPartidas_SelectionChangeCommitted(sender, e)
        MessageBox.Show("Datos Actualizados", "Sistema de Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub

    Private Sub cboPartidas_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPartidas.SelectedIndexChanged

    End Sub

    Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObra.SelectedIndexChanged

    End Sub

    Private Sub cboContratista_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboContratista.SelectedIndexChanged

    End Sub

    Private Sub CCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CCosto.SelectedIndexChanged

    End Sub

    Private Sub CCosto_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles CCosto.SelectionChangeCommitted
        If cboObra.SelectedIndex <> -1 Then

            eValorizaciones = New clsValorizaciones
            cboPartidas.DataSource = eValorizaciones.fListarObras(15, gEmpresa, gPeriodo, Convert.ToString(cboObra.SelectedValue), Trim(cboContratista.SelectedValue), Today(), Trim(CCosto.SelectedValue))
            If eValorizaciones.iNroRegistros > 0 Then
                cboPartidas.ValueMember = "IdPartida"
                cboPartidas.DisplayMember = "ParDescripcion"
                cboPartidas.SelectedIndex = -1
            End If

        ElseIf cboObra.SelectedIndex = -1 Then
            cboPartidas.SelectedIndex = -1
        End If

        For x As Integer = 0 To dgvHistorial.RowCount - 1
            dgvHistorial.Rows.Remove(dgvHistorial.CurrentRow)
        Next
    End Sub
End Class