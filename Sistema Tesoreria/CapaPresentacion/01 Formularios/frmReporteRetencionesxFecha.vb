Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.CrystalReports
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Windows

Imports System.Data.SqlClient
Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data

Public Class frmReporteRetencionesxFecha

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteRetencionesxFecha = Nothing
    Public Shared Function Instance() As frmReporteRetencionesxFecha
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteRetencionesxFecha
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteRetencionesxFecha_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private da As SqlDataAdapter
    Private eBusquedaRetenciones As clsBusquedaRetenciones
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Private eTempo As clsPlantTempo

    Private Sub frmReporteRetencionesxFecha_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        dtpFechaFin.Text = Now.Date()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()
        'ir = "SI"
        'dtpFechaIni_TextChanged(sender, e)
        'Timer2.Enabled = True

        Dim dtTableSeries As DataTable
        dtTableSeries = New DataTable
        eBusquedaRetenciones = New clsBusquedaRetenciones
        cboSeries.DataSource = eBusquedaRetenciones.fConsultaRetenciones(13, "", gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
        If eBusquedaRetenciones.iNroRegistros > 0 Then
            cboSeries.ValueMember = "IdSerie"
            cboSeries.DisplayMember = "Serie"
            cboSeries.SelectedIndex = -1
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If cboSeries.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione una Serie", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboSeries.Focus()
            Exit Sub
        End If

        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If

        Try
            'Next
            Dim dtTable2 As DataTable
            'Dim pIdRetencion As String = ""
            Dim IdSerie As String = ""
            dtTable2 = New DataTable
            eBusquedaRetenciones = New clsBusquedaRetenciones
            'If dgvGastos.Rows.Count > 0 Then
            'pIdRetencion = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("IdRetencion").Value
            'pSerie = dgvGastos.Rows(dgvGastos.CurrentRow.Index).Cells("xSerie").Value
            dtTable2 = eBusquedaRetenciones.fConsultaRetenciones(12, Trim(cboSeries.Text), gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, 0, "")
            'End If
            If dtTable2.Rows.Count > 0 Then
                Muestra_Reporte(RutaAppReportes & "rptRetencionesPorFecha.rpt", dtTable2, "TblLibroDiario", "", "Empresa;" & gDesEmpresa, "RucEmpresa;" & gEmprRuc)
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Me.Cursor = Cursors.Default
    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function


End Class