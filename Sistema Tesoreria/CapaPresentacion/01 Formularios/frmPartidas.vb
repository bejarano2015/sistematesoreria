Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports vb = Microsoft.VisualBasic

Public Class frmPartidas

    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim dblParValorizado As Double = 0
    Private ePagoProveedores As clsPagoProveedores

    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo
    Private ePartidas As clsPartidas
    Private eContratistas As clsContratistas
    Dim GrabarSiNo As Int16 = 0

    Public TieneFrmDetalle As Boolean
#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmPartidas = Nothing
    Public Shared Function Instance() As frmPartidas
        If frmInstance Is Nothing Then
            frmInstance = New frmPartidas
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmPartidas_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmPartidas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        For x As Integer = 0 To dgvLista.Columns.Count - 1
            'dvgListProveedores.Rows.Remove(dvgListProveedores.CurrentRow)
            dgvLista.Columns(x).SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        'mMostrarGrilla()
        ePartidas = New clsPartidas
        eTempo = New clsPlantTempo
        ePartidas = New clsPartidas
        cboEstado.DataSource = eTempo.fColEstado
        cboEstado.ValueMember = "Col02"
        cboEstado.DisplayMember = "Col01"
        cboEstado.SelectedIndex = -1
        dgvLista.Focus()
        cboMoneda.DataSource = ePartidas.fListar(1, "", "", gPeriodo, gUsuario)
        If ePartidas.iNroRegistros > 0 Then
            cboMoneda.ValueMember = "MonCodigo"
            cboMoneda.DisplayMember = "MonDescripcion"
            cboMoneda.SelectedIndex = -1
        End If
        cboTipoDocumento.DataSource = ePartidas.fListar(2, "", "", gPeriodo, gUsuario)
        If ePartidas.iNroRegistros > 0 Then
            cboTipoDocumento.ValueMember = "IdDocumento"
            cboTipoDocumento.DisplayMember = "DescripDoc"
            cboTipoDocumento.SelectedIndex = -1
        End If
        cboEspecialidad.DataSource = ePartidas.fListar(3, "", "", gPeriodo, gUsuario)
        If ePartidas.iNroRegistros > 0 Then
            cboEspecialidad.ValueMember = "esp_codigo"
            cboEspecialidad.DisplayMember = "esp_nombre"
            cboEspecialidad.SelectedIndex = -1
        End If

        'cboObra.DataSource = ePartidas.fListar(8, gEmpresa, "", gPeriodo, gUsuario)
        'If ePartidas.iNroRegistros > 0 Then
        '    cboObra.ValueMember = "ObraCodigo"
        '    cboObra.DisplayMember = "ObraDescripcion"
        '    cboObra.SelectedIndex = -1
        'End If

        If Trim(glbUsuCategoria) = "A" Then
            'cboObra.DataSource = eValorizaciones.fListarObras(0, gEmpresa, gPeriodo, "", "", Today())
            cboObra.DataSource = ePartidas.fListar(8, gEmpresa, "", gPeriodo, gUsuario)
        ElseIf Trim(glbUsuCategoria) = "U" Then
            'cboObra.DataSource = eValorizaciones.fListarObras(73, gEmpresa, gPeriodo, "", gUsuario, Today())
            cboObra.DataSource = ePartidas.fListar(25, gEmpresa, "", gPeriodo, gUsuario)
        End If
        If ePartidas.iNroRegistros > 0 Then
            cboObra.ValueMember = "ObraCodigo"
            cboObra.DisplayMember = "ObraDescripcion"
            cboObra.SelectedIndex = -1
        End If

        cboCCosto.DataSource = ePartidas.fListar(14, gEmpresa, "", gPeriodo, gUsuario)
        If ePartidas.iNroRegistros > 0 Then
            cboCCosto.ValueMember = "CCosCodigo"
            cboCCosto.DisplayMember = "CCosDescripcion"
            cboCCosto.SelectedIndex = -1
        End If

        dgvLista.Width = 970
        dgvLista.Height = 495
        'dtpFechaFin.Value = Now.Date().ToShortDateString()
        'dtpFechaIni.Value = Now.Date().ToShortDateString()

        'Dim dtTable As DataTable
        'dtTable = New DataTable

        'ePagoProveedores = New clsPagoProveedores
        'eTempo = New clsPlantTempo
        'dtTable = ePagoProveedores.fListarParametroIGV()

        ''dtTable = eRegistroDocumento.fListarParametroIgv(gEmpresa)
        'If dtTable.Rows.Count > 0 Then
        '    'dIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
        '    'txtTasaIGV.Text = Format(dIgv, "0.00")
        'Else
        '    'dIgv = 0.0
        'End If
        BeLabel16.Text = "IGV " & gIgv & " %"
        Timer2.Enabled = True
        mMostrarGrilla()
    End Sub


    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If dgvLista.Rows.Count > 0 Then
            If sTab = 0 Then
                'VerPosicion()

                'MessageBox.Show("Imprimir")
                Dim x As frmPartidasReporte = frmPartidasReporte.Instance

                'x.Owner = Me
                'x.ShowInTaskbar = False
                'x.ShowDialog()
                x.MdiParent = frmPrincipal
                x.IdPartida = dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("Column5").Value  'Trim(Fila("IdPartida").ToString()) 'Trim(txtCodigo.Text)
                x.IdCC = dgvLista.Rows(dgvLista.CurrentRow.Index).Cells("CCosCodigo").Value 'Trim(Fila("CCosCodigo").ToString()) 'Trim(cboCCosto.SelectedValue)  'Fila("CCosCodigo").ToString
                x.Show()

                'Column5
                'Column17()

                'If Panel6.Visible = False Then
                '    Panel6.Visible = True
                'End If

                'If Panel6.Visible = True Then
                '    Panel6.Visible = False
                'End If
            End If
        End If
        
    End Sub

    Private Sub mMostrarGrilla()
        ePartidas = New clsPartidas
        eTempo = New clsPlantTempo
        dtTable = New DataTable

        If Trim(glbUsuCategoria) = "A" Then
            'dtTableLista = eRegistroDocumento.fListar(0, gEmpresa, Trim(gUsuario), gPeriodo, "")
            dtTable = ePartidas.fListar(0, gEmpresa, "", gPeriodo, gUsuario)
        ElseIf Trim(glbUsuCategoria) = "U" Then
            dtTable = ePartidas.fListar(22, gEmpresa, "", gPeriodo, gUsuario)
            'dtTableLista = eRegistroDocumento.fListar(50, gEmpresa, Trim(gUsuario), gPeriodo, "")
        End If

        'dtTable = ePartidas.fListar(0, gEmpresa, "", gPeriodo)

        dgvLista.AutoGenerateColumns = False
        dgvLista.DataSource = dtTable
        cmr = BindingContext(dgvLista.DataSource)
        stsTotales.Items(0).Text = "Total de Registros= " & ePartidas.iNroRegistros
        If ePartidas.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If
        Timer2.Enabled = True
    End Sub


    Private Sub LimpiarControles()
        txtCodigo.Clear()
        txtContratista.Clear()
        txtCodigoContratista.Clear()
        cboMoneda.SelectedIndex = -1
        cboTipoDocumento.SelectedIndex = -1
        cboObra.SelectedIndex = -1
        txtPartida.Clear()
        'dtpFechaFin.Value = Now.Date().ToShortDateString
        'dtpFechaIni.Value = Now.Date().ToShortDateString
        txtUbicacion.Clear()
        txtPresupuesto.Clear()
        txtRetencion.Clear()
        cboEspecialidad.SelectedIndex = -1
        txtObservacion.Clear()
        txtNumeroOrden.Clear()
        txtNroContrato.Clear()
        cboUbiTrabajo.SelectedIndex = -1
    End Sub

    Private Sub DesabilitarControles()
        cboCCosto.Enabled = False
        txtCodigo.Enabled = False
        txtContratista.Enabled = False
        'txtCodigoContratista.Enabled = False
        cboMoneda.Enabled = False
        cboTipoDocumento.Enabled = False
        cboObra.Enabled = False
        txtPartida.Enabled = False
        dtpFechaIni.Enabled = False
        dtpFechaFin.Enabled = False
        txtUbicacion.Enabled = False
        txtPresupuesto.Enabled = False
        txtRetencion.Enabled = False
        cboEspecialidad.Enabled = False
        txtObservacion.Enabled = False
        cboEstado.Enabled = False
        dgvDetalle.Enabled = False
        txtNumeroOrden.Enabled = False
        txtNroContrato.Enabled = False
        cboUbiTrabajo.Enabled = False
        btnLiquidar.Enabled = False
    End Sub

    Private Sub HabilitarControles()
        cboCCosto.Enabled = True
        txtCodigo.Enabled = True
        txtContratista.Enabled = True
        'txtCodigoContratista.Enabled = True
        cboMoneda.Enabled = True
        cboTipoDocumento.Enabled = True
        cboObra.Enabled = True
        txtPartida.Enabled = True
        dtpFechaIni.Enabled = True
        dtpFechaFin.Enabled = True
        txtUbicacion.Enabled = True
        txtPresupuesto.Enabled = True
        txtRetencion.Enabled = True
        cboEspecialidad.Enabled = True
        txtObservacion.Enabled = True
        cboEstado.Enabled = True
        dgvDetalle.Enabled = True
        txtNumeroOrden.Enabled = True
        txtNroContrato.Enabled = True
        cboUbiTrabajo.Enabled = True

        btnLiquidar.Enabled = True
    End Sub

    Private Sub v_NuevoReg() Handles Me.NuevoReg
        Panel2.Visible = False
        'If sTab = 0 Then
        iOpcion = 5
        sTab = 1
        LimpiarControles()
        HabilitarControles()
        cboEstado.SelectedIndex = 0
        cboEstado.Enabled = False
        'ePartidas = New clsPartidas
        'ePartidas.fCodigo(4, gEmpresa, gPeriodo)
        'txtCodigo.Text = ePartidas.sCodFuturo
        txtCodigo.Enabled = False
        Timer1.Enabled = True 'txtContratista.Focus()
        Panel3.Visible = False
        dtpFechaIni.Value = Now.Date().ToShortDateString()
        dtpFechaFin.Value = Now.Date().ToShortDateString()
        cboCCosto.SelectedIndex = -1
        For x As Integer = 0 To dgvDetalle.RowCount - 1
            dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
        Next
        btnBuscar.Enabled = True
        btnQuitar.Enabled = True
        txtSubTotal.Clear()
        txtIGV.Clear()
        BeLabel18.Text = ""
        btnLiquidar.Enabled = False
        'End If
    End Sub

    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            'Timer2.Enabled = True
        End If
        mMostrarGrilla()
        Timer2.Enabled = True
        Panel3.Visible = False
    End Sub

    Protected Sub v_ModificaReg() Handles Me.ModificaReg
        Panel2.Visible = False
        'If sTab = 0 Then
        sTab = 1
        iOpcion = 6
        VerPosicion()
        HabilitarControles()
        cboEstado.Enabled = True
        txtCodigo.Enabled = False
        Panel3.Visible = False
        'btnBuscar.Enabled = False
        'btnQuitar.Enabled = False

        If Trim(Fila("IdPartida").ToString()) <> "" Then
            ePartidas = New clsPartidas
            Dim dtDetalle As DataTable
            dtDetalle = New DataTable
            dtDetalle = ePartidas.fListarDetalle(12, gEmpresa, Fila("IdPartida").ToString(), Today(), gPeriodo, Fila("CCosCodigo").ToString())
            dgvDetalle.AutoGenerateColumns = False
            If dtDetalle.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
                For y As Integer = 0 To dtDetalle.Rows.Count - 1
                    dgvDetalle.Rows.Add()
                    dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetalle.Rows(y).Item("Descripcion").ToString
                    dgvDetalle.Rows(y).Cells("Cant").Value = dtDetalle.Rows(y).Item("Cantidad").ToString
                    dgvDetalle.Rows(y).Cells("Und").Value = dtDetalle.Rows(y).Item("UnidMed").ToString
                    dgvDetalle.Rows(y).Cells("M1").Value = dtDetalle.Rows(y).Item("MetradoPor").ToString
                    dgvDetalle.Rows(y).Cells("Con_Metrado").Value = dtDetalle.Rows(y).Item("Con_Metrado").ToString
                    dgvDetalle.Rows(y).Cells("Con_PrecioSubContra").Value = dtDetalle.Rows(y).Item("Con_PrecioSubContra").ToString
                    dgvDetalle.Rows(y).Cells("M2").Value = dtDetalle.Rows(y).Item("MetradoImpor").ToString
                    dgvDetalle.Rows(y).Cells("PU").Value = dtDetalle.Rows(y).Item("PU").ToString
                    dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetalle.Rows(y).Item("Parcial").ToString
                    dgvDetalle.Rows(y).Cells("idDetalle").Value = dtDetalle.Rows(y).Item("IdPartidaDet").ToString
                    dgvDetalle.Rows(y).Cells("Estado").Value = "1"
                    dgvDetalle.Rows(y).Cells("Con_TitCodigo").Value = dtDetalle.Rows(y).Item("Con_TitCodigo").ToString
                    dgvDetalle.Rows(y).Cells("Con_ParCodigo").Value = dtDetalle.Rows(y).Item("Con_ParCodigo").ToString
                    dgvDetalle.Rows(y).Cells("Con_ConsCodigo").Value = dtDetalle.Rows(y).Item("Con_ConsCodigo").ToString
                    dgvDetalle.Rows(y).Cells("M2Anterior").Value = dtDetalle.Rows(y).Item("M2Anterior").ToString
                Next
                BeLabel18.Text = "Total de Items : " & dtDetalle.Rows.Count
                Calcular()
            ElseIf dtDetalle.Rows.Count = 0 Then
                BeLabel18.Text = "Total de Items : 0"
            End If
        End If
        ',P.Estado as EstPar
        If Trim(Fila("EstPar").ToString()) = "1" Then
            DesabilitarControles()
            btnBuscar.Enabled = False
            btnQuitar.Enabled = False
        ElseIf Trim(Fila("EstPar").ToString()) = "0" Then

            btnBuscar.Enabled = True
            btnQuitar.Enabled = True

            cboCCosto.Enabled = False
            'txtCodigo.Enabled = False
            txtContratista.Enabled = False
            'txtCodigoContratista.Enabled = False
            'cboMoneda.Enabled = False
            'cboTipoDocumento.Enabled = False
            cboObra.Enabled = False

        End If

        Timer1.Enabled = True
    End Sub

    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        Panel2.Visible = False
        'If sTab = 0 Then
        sTab = 1
        VerPosicion()
        DesabilitarControles()
        Panel3.Visible = False
        cboCCosto.Enabled = False
        btnBuscar.Enabled = False
        btnQuitar.Enabled = False
        If Trim(Fila("IdPartida").ToString()) <> "" Then
            ePartidas = New clsPartidas
            Dim dtDetalle As DataTable
            dtDetalle = New DataTable
            dtDetalle = ePartidas.fListarDetalle(12, gEmpresa, Fila("IdPartida").ToString(), Today(), gPeriodo, Fila("CCosCodigo").ToString())
            dgvDetalle.AutoGenerateColumns = False
            If dtDetalle.Rows.Count > 0 Then
                For x As Integer = 0 To dgvDetalle.RowCount - 1
                    dgvDetalle.Rows.Remove(dgvDetalle.CurrentRow)
                Next
                For y As Integer = 0 To dtDetalle.Rows.Count - 1
                    dgvDetalle.Rows.Add()
                    dgvDetalle.Rows(y).Cells("Descripcion").Value = dtDetalle.Rows(y).Item("Descripcion").ToString
                    dgvDetalle.Rows(y).Cells("Cant").Value = dtDetalle.Rows(y).Item("Cantidad").ToString
                    dgvDetalle.Rows(y).Cells("Und").Value = dtDetalle.Rows(y).Item("UnidMed").ToString
                    dgvDetalle.Rows(y).Cells("M1").Value = dtDetalle.Rows(y).Item("MetradoPor").ToString
                    dgvDetalle.Rows(y).Cells("Con_Metrado").Value = dtDetalle.Rows(y).Item("Con_Metrado").ToString
                    dgvDetalle.Rows(y).Cells("Con_PrecioSubContra").Value = dtDetalle.Rows(y).Item("Con_PrecioSubContra").ToString
                    dgvDetalle.Rows(y).Cells("M2").Value = dtDetalle.Rows(y).Item("MetradoImpor").ToString
                    dgvDetalle.Rows(y).Cells("PU").Value = dtDetalle.Rows(y).Item("PU").ToString
                    dgvDetalle.Rows(y).Cells("Parcial").Value = dtDetalle.Rows(y).Item("Parcial").ToString
                    dgvDetalle.Rows(y).Cells("idDetalle").Value = dtDetalle.Rows(y).Item("IdPartidaDet").ToString
                    dgvDetalle.Rows(y).Cells("Estado").Value = "1"
                    dgvDetalle.Rows(y).Cells("Con_TitCodigo").Value = dtDetalle.Rows(y).Item("Con_TitCodigo").ToString
                    dgvDetalle.Rows(y).Cells("Con_ParCodigo").Value = dtDetalle.Rows(y).Item("Con_ParCodigo").ToString
                    dgvDetalle.Rows(y).Cells("Con_ConsCodigo").Value = dtDetalle.Rows(y).Item("Con_ConsCodigo").ToString
                Next
                BeLabel18.Text = "Total de Items : " & dtDetalle.Rows.Count
                Calcular()
            ElseIf dtDetalle.Rows.Count = 0 Then
                BeLabel18.Text = "Total de Items : 0"
            End If
        End If

        'End If
    End Sub

    Private Sub v_SalidaReg() Handles Me.SalidaReg
        'If (MessageBox.Show("�Esta seguro de cerrar la ventana?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        Close()
        'Else
        'Exit Sub
        'End If
    End Sub

    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception
        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdPartida").ToString
                Me.txtCodigoContratista.Text = Fila("IdContratista").ToString
                Me.txtContratista.Text = Fila("Contratista").ToString
                If Fila("Estado").ToString = 0 Then
                    Me.cboEstado.SelectedIndex = 0
                Else
                    Me.cboEstado.SelectedIndex = 1
                End If
                cboMoneda.SelectedValue = Trim(Fila("IdMoneda").ToString())
                cboTipoDocumento.SelectedValue = Trim(Fila("TipoDocCodigo").ToString())
                txtPartida.Text = Fila("ParDescripcion").ToString
                dtpFechaFin.Value = Fila("FechaFinal").ToString
                dtpFechaIni.Value = Fila("FechaInicio").ToString
                txtUbicacion.Text = Fila("ParUbicacion").ToString
                txtPresupuesto.Text = Fila("ParValorizado").ToString
                dblParValorizado = Fila("ParValorizado").ToString
                txtRetencion.Text = Fila("xret").ToString
                cboEspecialidad.SelectedValue = Fila("esp_codigo").ToString


                txtObservacion.Text = Fila("ParObservacion").ToString
                txtNumeroOrden.Text = Fila("NumOrden").ToString
                txtNroContrato.Text = Fila("NContrato").ToString
                cboCCosto.SelectedValue = Fila("CCosCodigo").ToString
                cboObra.SelectedValue = Trim(Fila("ObraCodigo").ToString)
                'cboObra_SelectedIndexChanged(sender, e)
                cboUbiTrabajo.SelectedValue = Fila("ubtCodigo").ToString
                cboTipoRetencion.SelectedIndex = Fila("TipoRetencion").ToString
            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'Dim iResultado As Int32
        Try
            '    Using scope As TransactionScope = New TransactionScope
            If dgvLista.Rows.Count > 0 Then
                'If (MessageBox.Show("Desea Eliminar: " & Chr(13) & Chr(13) & "Codigo: " & Fila("AlmCodigo") & "   " & Chr(13) & "Descripcion: " & Fila("AlmDescripcion"), sNombreSistema, MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                '    'Using scope As TransactionScope = New TransactionScope '(MSDTC)
                '    eAlmacen = New clsAlmacen
                '    iResultado = eAlmacen.fEliminar(Fila("ALMCODIGO"), 3)
                '    'scope.Complete()
                '    'End Using
                '    If iResultado = 1 Then
                '        mMostrarGrilla()
                '    End If
                'Else
                '    Exit Sub
                'End If
                MsgBox("Opcion Bloqueada por el Administrador", MsgBoxStyle.Information, glbNameSistema)
            End If
            'scope.Complete()
            'End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try

    End Sub

    Private Sub v_BuscaReg() Handles Me.BuscaReg
        'If sTab = 0 Then
        '    MessageBox.Show("Buscar")
        'End If
        Panel1.Visible = True
        rdbPartida.Checked = True
        txtBuscar.Clear()
        txtBuscar.Focus()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.cboCCosto.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Dim DblTotal As Double = 0

    Sub Calcular()
        DblTotal = 0
        Dim Contador As Integer = 0
        For x As Integer = 0 To dgvDetalle.Rows.Count - 1
            'If Len(dgvDetalle.Rows(x).Cells("M2").Value) > 0 And Len(dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value) > 0 Then
            '    'dgvDetalle.Rows(x).Cells("Parcial").Value = Format((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value), "#,##0.00")
            '    dgvDetalle.Rows(x).Cells("Parcial").Value = Math.Round((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value), 2)
            'End If

            If Len(dgvDetalle.Rows(x).Cells("Parcial").Value) = 0 Then
                DblTotal = DblTotal + 0
            Else
                DblTotal = DblTotal + Val((Convert.ToDouble(dgvDetalle.Rows(x).Cells("Parcial").Value)))
            End If
            Contador = Contador + 1
        Next
        txtSubTotal.Text = Format(DblTotal, "#,##0.00")
        Dim ImpIGV As Double = Convert.ToDouble(txtSubTotal.Text) * (gIgv / 100)
        'Dim ImpIGV As Double = ImpBase * gIgvConvertido
        txtIGV.Text = Format(ImpIGV, "#,##0.00")
        'txtTotal.Text = Format(DblTotal + ImpIGV, "#,##0.00")
        txtPresupuesto.Text = Format(DblTotal + ImpIGV, "#,##0.00")
        BeLabel18.Text = "Total de Items : " & Contador
    End Sub

    Sub ValidarDatos2()
        If dgvDetalle.Rows.Count > 0 Then
            For x As Integer = 0 To dgvDetalle.Rows.Count - 1

                If Len(Trim(dgvDetalle.Rows(x).Cells("Descripcion").Value)) = 0 Then
                    MessageBox.Show("Ingrese una Descripci�n", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(0, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("Und").Value)) = 0 Then
                    MessageBox.Show("Ingrese la Unidad de Medida", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(2, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("M1").Value)) = 0 Then
                    MessageBox.Show("Ingrese el Porcetaje", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(3, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("M2").Value)) = 0 Then
                    MessageBox.Show("Ingrese el Metrado", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(4, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If

                'If Len(Trim(dgvDetalle.Rows(x).Cells("PU").Value)) = 0 Then
                '    MessageBox.Show("Ingrese el Precio Unitario", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    dgvDetalle.CurrentCell = dgvDetalle(5, x)
                '    GrabarSiNo = 0
                '    Exit Sub
                'End If

                If Len(Trim(dgvDetalle.Rows(x).Cells("Parcial").Value)) = 0 Then
                    MessageBox.Show("Ingrese Importe Parcial", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvDetalle.CurrentCell = dgvDetalle(6, x)
                    GrabarSiNo = 0
                    Exit Sub
                End If
            Next
            GrabarSiNo = 1
        ElseIf dgvDetalle.Rows.Count = 0 Then
            GrabarSiNo = 1
        End If
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        If cboEstado.SelectedIndex = 1 Then
            MessageBox.Show("Este Contrato est� Liquidado", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        ValidarDatos2()
        If GrabarSiNo = 0 Then
            Exit Sub
        End If

        Dim iResultado As Int32
        Dim sCodigoRegistro As String = ""

        Dim Mensaje As String = ""
        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha Final es Menor a la Fecha Inicial", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaFin.Focus()
            Exit Sub
        End If

        eTempo = New clsPlantTempo
        ePartidas = New clsPartidas
        If sTab = 1 Then
            If Len(Trim(txtContratista.Text)) > 0 And Len(Trim(txtCodigoContratista.Text)) > 0 And cboObra.SelectedIndex <> -1 And cboMoneda.SelectedIndex <> -1 And cboTipoDocumento.SelectedIndex <> -1 And Len(Trim(txtPartida.Text)) > 0 And Len(Trim(txtPresupuesto.Text)) > 0 And Len(Trim(txtRetencion.Text)) > 0 And cboEspecialidad.SelectedIndex <> -1 And cboEstado.SelectedIndex <> -1 And cboCCosto.SelectedIndex <> -1 And cboUbiTrabajo.SelectedIndex <> -1 And cboTipoRetencion.SelectedIndex <> -1 Then

                If iOpcion = 5 Then
                    Mensaje = "Grabar"
                ElseIf iOpcion = 6 Then
                    Mensaje = "Actualizar"
                End If

                If (MessageBox.Show("�Esta seguro de " & Mensaje & " ahora el registro en proceso?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 5 Then
                        ePartidas = New clsPartidas
                        ePartidas.fCodigo(4, gEmpresa, gPeriodo, Trim(cboCCosto.SelectedValue))
                        sCodigoRegistro = ePartidas.sCodFuturo
                        txtCodigo.Text = Trim(sCodigoRegistro)
                        iResultado = ePartidas.fGrabar(iOpcion, gEmpresa, sCodigoRegistro, Trim(txtCodigoContratista.Text), cboMoneda.SelectedValue, cboTipoDocumento.SelectedValue, Trim(txtPartida.Text), dtpFechaIni.Value, dtpFechaFin.Value, Trim(txtUbicacion.Text), Trim(txtPresupuesto.Text), Trim(txtRetencion.Text), cboEspecialidad.SelectedValue, Trim(txtObservacion.Text), cboEstado.SelectedValue, cboObra.SelectedValue, gPeriodo, gUsuario, Trim(cboCCosto.SelectedValue), Trim(txtSubTotal.Text), Trim(txtIGV.Text), Trim(txtNumeroOrden.Text), Trim(txtNroContrato.Text), Trim(cboUbiTrabajo.SelectedValue), Trim(cboTipoRetencion.SelectedIndex))
                        If iResultado > 0 Then

                            If dgvDetalle.Rows.Count > 0 Then
                                For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                                    Dim xDescripcion As String
                                    Dim xCant As Double
                                    Dim xUnd As String
                                    Dim xM1 As Double
                                    Dim xM2 As Double
                                    Dim xPU As Double
                                    Dim xParcial As Double
                                    Dim xM2Anterior As Double = 0
                                    Dim sCodigoRegistroDet As String

                                    Dim xCon_Metrado As Double
                                    Dim xCon_PrecioSubContra As Double
                                    Dim xCon_TitCodigo As String
                                    Dim xCon_ParCodigo As String
                                    Dim xCon_ConsCodigo As String

                                    xDescripcion = Trim(dgvDetalle.Rows(x).Cells("Descripcion").Value)
                                    xCant = dgvDetalle.Rows(x).Cells("Cant").Value
                                    xUnd = dgvDetalle.Rows(x).Cells("Und").Value
                                    xM1 = dgvDetalle.Rows(x).Cells("M1").Value
                                    xM2 = dgvDetalle.Rows(x).Cells("M2").Value
                                    xPU = dgvDetalle.Rows(x).Cells("PU").Value
                                    xParcial = dgvDetalle.Rows(x).Cells("Parcial").Value
                                    sCodigoRegistroDet = dgvDetalle.Rows(x).Cells("idDetalle").Value

                                    xCon_Metrado = dgvDetalle.Rows(x).Cells("Con_Metrado").Value
                                    xCon_PrecioSubContra = dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value
                                    xCon_TitCodigo = dgvDetalle.Rows(x).Cells("Con_TitCodigo").Value
                                    xCon_ParCodigo = dgvDetalle.Rows(x).Cells("Con_ParCodigo").Value
                                    xCon_ConsCodigo = dgvDetalle.Rows(x).Cells("Con_ConsCodigo").Value
                                    'xM2Anterior = dgvDetalle.Rows(x).Cells("Con_ConsCodigo").Value

                                    If dgvDetalle.Rows(x).Cells("Estado").Value = "0" Then
                                        xM2Anterior = xM2
                                        ePartidas = New clsPartidas
                                        ePartidas.fCodigoDetalle(9, gEmpresa, sCodigoRegistro, gPeriodo, Trim(cboCCosto.SelectedValue))
                                        sCodigoRegistroDet = ePartidas.sCodFuturoDet
                                        ePartidas.fGrabarDetalle(10, gEmpresa, sCodigoRegistro, sCodigoRegistroDet, xDescripcion, xCant, xUnd, xM1, xM2, xPU, xParcial, gUsuario, xCon_Metrado, xCon_PrecioSubContra, xCon_TitCodigo, xCon_ParCodigo, xCon_ConsCodigo, xM2Anterior, gPeriodo, Trim(cboCCosto.SelectedValue))
                                    ElseIf dgvDetalle.Rows(x).Cells("Estado").Value = "1" Then
                                        xM2Anterior = dgvDetalle.Rows(x).Cells("M2Anterior").Value
                                        ePartidas.fGrabarDetalle(11, gEmpresa, sCodigoRegistro, sCodigoRegistroDet, xDescripcion, xCant, xUnd, xM1, xM2, xPU, xParcial, gUsuario, xCon_Metrado, xCon_PrecioSubContra, xCon_TitCodigo, xCon_ParCodigo, xCon_ConsCodigo, xM2Anterior, gPeriodo, Trim(cboCCosto.SelectedValue))
                                    End If
                                Next
                            End If

                            mMostrarGrilla()
                            Timer2.Enabled = True
                        End If
                        sTab = 0
                    ElseIf iOpcion = 6 Then

                        sCodigoRegistro = Trim(txtCodigo.Text)
                        'dblParValorizado

                        iResultado = ePartidas.fGrabar(iOpcion, gEmpresa, sCodigoRegistro, Trim(txtCodigoContratista.Text), cboMoneda.SelectedValue, cboTipoDocumento.SelectedValue, Trim(txtPartida.Text), dtpFechaIni.Value, dtpFechaFin.Value, Trim(txtUbicacion.Text), Trim(txtPresupuesto.Text), Trim(txtRetencion.Text), cboEspecialidad.SelectedValue, Trim(txtObservacion.Text), cboEstado.SelectedValue, cboObra.SelectedValue, gPeriodo, gUsuario, Trim(cboCCosto.SelectedValue), Trim(txtSubTotal.Text), Trim(txtIGV.Text), Trim(txtNumeroOrden.Text), Trim(txtNroContrato.Text), Trim(cboUbiTrabajo.SelectedValue), Trim(cboTipoRetencion.SelectedIndex))

                        If Format(dblParValorizado, "#,##0.00") <> Trim(txtPresupuesto.Text) Then
                            Dim iResultado2 As Integer = 0
                            'ePartidas.fGrabar(iOpcion, gEmpresa, sCodigoRegistro, Trim(txtCodigoContratista.Text), cboMoneda.SelectedValue, cboTipoDocumento.SelectedValue, Trim(txtPartida.Text), dtpFechaIni.Value, dtpFechaFin.Value, Trim(txtUbicacion.Text), Trim(txtPresupuesto.Text), Trim(txtRetencion.Text), cboEspecialidad.SelectedValue, Trim(txtObservacion.Text), cboEstado.SelectedValue, cboObra.SelectedValue, gPeriodo, gUsuario, Trim(cboCCosto.SelectedValue), Trim(txtSubTotal.Text), Trim(txtIGV.Text), Trim(txtNumeroOrden.Text), Trim(txtNroContrato.Text), Trim(cboUbiTrabajo.SelectedValue))
                            iResultado2 = ePartidas.fGrabar2(27, gEmpresa, sCodigoRegistro, "", "", "", "", Today, Today, "", Trim(txtPresupuesto.Text), Format(dblParValorizado, "#,##0.00"), "", "", 0, "", gPeriodo, gUsuario, Trim(cboCCosto.SelectedValue), Trim(txtSubTotal.Text), Trim(txtIGV.Text), "", "", "")
                        End If

                        If iResultado > 0 Then

                            If dgvDetalle.Rows.Count > 0 Then
                                For x As Integer = 0 To dgvDetalle.Rows.Count - 1
                                    Dim xDescripcion As String
                                    Dim xCant As Double
                                    Dim xUnd As String
                                    Dim xM1 As Double
                                    Dim xM2 As Double
                                    Dim xPU As Double
                                    Dim xParcial As Double
                                    Dim xM2Anterior As Double = 0
                                    Dim sCodigoRegistroDet As String

                                    Dim xCon_Metrado As Double
                                    Dim xCon_PrecioSubContra As Double
                                    Dim xCon_TitCodigo As String
                                    Dim xCon_ParCodigo As String
                                    Dim xCon_ConsCodigo As String

                                    xDescripcion = Trim(dgvDetalle.Rows(x).Cells("Descripcion").Value)
                                    xCant = dgvDetalle.Rows(x).Cells("Cant").Value
                                    xUnd = dgvDetalle.Rows(x).Cells("Und").Value
                                    xM1 = dgvDetalle.Rows(x).Cells("M1").Value
                                    xM2 = dgvDetalle.Rows(x).Cells("M2").Value
                                    xPU = dgvDetalle.Rows(x).Cells("PU").Value
                                    xParcial = dgvDetalle.Rows(x).Cells("Parcial").Value
                                    sCodigoRegistroDet = dgvDetalle.Rows(x).Cells("idDetalle").Value

                                    xCon_Metrado = dgvDetalle.Rows(x).Cells("Con_Metrado").Value
                                    xCon_PrecioSubContra = dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value
                                    xCon_TitCodigo = dgvDetalle.Rows(x).Cells("Con_TitCodigo").Value
                                    xCon_ParCodigo = dgvDetalle.Rows(x).Cells("Con_ParCodigo").Value
                                    xCon_ConsCodigo = dgvDetalle.Rows(x).Cells("Con_ConsCodigo").Value


                                    If dgvDetalle.Rows(x).Cells("Estado").Value = "0" Then
                                        xM2Anterior = xM2
                                        ePartidas = New clsPartidas
                                        ePartidas.fCodigoDetalle(9, gEmpresa, sCodigoRegistro, gPeriodo, Trim(cboCCosto.SelectedValue))
                                        sCodigoRegistroDet = ePartidas.sCodFuturoDet
                                        ePartidas.fGrabarDetalle(10, gEmpresa, sCodigoRegistro, sCodigoRegistroDet, xDescripcion, xCant, xUnd, xM1, xM2, xPU, xParcial, gUsuario, xCon_Metrado, xCon_PrecioSubContra, xCon_TitCodigo, xCon_ParCodigo, xCon_ConsCodigo, xM2Anterior, gPeriodo, Trim(cboCCosto.SelectedValue))
                                    ElseIf dgvDetalle.Rows(x).Cells("Estado").Value = "1" Then
                                        xM2Anterior = dgvDetalle.Rows(x).Cells("M2Anterior").Value
                                        ePartidas.fGrabarDetalle(11, gEmpresa, sCodigoRegistro, sCodigoRegistroDet, xDescripcion, xCant, xUnd, xM1, xM2, xPU, xParcial, gUsuario, xCon_Metrado, xCon_PrecioSubContra, xCon_TitCodigo, xCon_ParCodigo, xCon_ConsCodigo, xM2Anterior, gPeriodo, Trim(cboCCosto.SelectedValue))
                                    End If
                                Next
                            End If

                            mMostrarGrilla()
                        End If
                        sTab = 0
                    End If
                End If
            Else
                If cboCCosto.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboCCosto.Focus()
                    Exit Sub
                End If

                If cboObra.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Obra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboObra.Focus()
                    Exit Sub
                End If

                If cboUbiTrabajo.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Ubicaci�n de Trabajo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboUbiTrabajo.Focus()
                    Exit Sub
                End If



                If Len(Trim(txtContratista.Text)) = 0 Then
                    MessageBox.Show("Ingrese un Contratista", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtContratista.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtCodigoContratista.Text)) = 0 Then
                    MessageBox.Show("Ingrese un Contratista", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtContratista.Focus()
                    Exit Sub
                End If

                If cboMoneda.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Moneda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboMoneda.Focus()
                    Exit Sub
                End If

                If cboTipoDocumento.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Documento de Rendicion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboTipoDocumento.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtPartida.Text)) = 0 Then
                    MessageBox.Show("Ingrese la descripcion de la Partida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtPartida.Focus()
                    Exit Sub
                End If

                'If Len(Trim(txtUbicacion.Text)) = 0 Then
                '    MessageBox.Show("Ingrese la Ubicacion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    txtUbicacion.Focus()
                '    Exit Sub
                'End If

                If Len(Trim(txtPresupuesto.Text)) = 0 Then
                    MessageBox.Show("Ingrese el Presupuesto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtPresupuesto.Focus()
                    Exit Sub
                End If

                If Len(Trim(txtRetencion.Text)) = 0 Then
                    MessageBox.Show("Ingrese la Retencion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    txtRetencion.Focus()
                    Exit Sub
                End If

                If cboEspecialidad.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Especialidad", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboEspecialidad.Focus()
                    Exit Sub
                End If
                If cboTipoRetencion.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Tipo de Retenci�n", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboEspecialidad.Focus()
                    Exit Sub
                End If
                'If Len(Trim(txtObservacion.Text)) = 0 Then
                '    MessageBox.Show("Ingrese la Observacion", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                '    txtObservacion.Focus()
                '    Exit Sub
                'End If

                frmPrincipal.sFlagGrabar = "0"
            End If
        End If
    End Sub


        
    Private Sub dtpFechaIni_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaIni.ValueChanged
        'If dtpFechaFin.Value < dtpFechaIni.Value Then
        '    MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    dtpFechaIni.Value = dtpFechaFin.Value
        '    dtpFechaIni.Focus()
        'End If

        Dim dtTable As DataTable
        dtTable = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTable = ePagoProveedores.fListarParametroIGV(dtpFechaIni.Value)
        If dtTable.Rows.Count > 0 Then
            gIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
            gIgvConvertido = Format(gIgv / 100, "0.00")
        Else
            gIgv = 0
            gIgvConvertido = 0
        End If
        BeLabel16.Text = "IGV " & gIgv & " %"

    End Sub

    Private Sub dtpFechaFin_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaFin.ValueChanged, txtContratista.TextChanged
        'If dtpFechaFin.Value < dtpFechaIni.Value Then
        '    MessageBox.Show("La Fecha Final es Menor a la Fecha Inicial", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        '    dtpFechaFin.Value = dtpFechaIni.Value
        '    dtpFechaFin.Focus()
        'End If
    End Sub

    Private Sub txtContratista_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtContratista.KeyPress
        'MessageBox.Show(Asc(e.KeyChar))
       

        Select Case Asc(e.KeyChar)
            Case 13

                If Trim(txtContratista.Text) = "" Then
                    MessageBox.Show("Ingrese n�mero del RUC", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'txtRuc.Focus()
                    Exit Sub
                End If
                If IsNumeric(txtContratista.Text) = True Then
                    If Len(txtContratista.Text) < 11 Then
                        'Limpiar()

                        MessageBox.Show("Ingrese los 11 n�meros del RUC", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'txtRuc.Focus()
                        Exit Sub
                    End If
                    If Val(Mid(Trim(txtContratista.Text), 2, 9)) = 0 Or Trim(txtContratista.Text) = "23333333333" Then
                        'Limpiar()
                        MessageBox.Show("Verificar n�mero del RUC", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'txtRuc.Focus()
                        Exit Sub
                    End If
                    If Verificar_ruc(txtContratista.Text) = False Then
                        'Limpiar()
                        MessageBox.Show("El n�mero del RUC no es v�lido", "", MessageBoxButtons.OK, MessageBoxIcon.Error)

                        'txtRuc.Focus()
                        Exit Sub
                    End If
                    'Call VALRUC(v_Ruc)
                    'OTROVALRUC(v_Ruc)
                Else
                    MessageBox.Show("Solo se aceptan n�meros", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ' txtRuc.Focus()
                End If
                eContratistas = New clsContratistas
                dtTable = New DataTable
                dtTable = eContratistas.fListar(8, gEmpresa, txtContratista.Text)
                If dtTable.Rows.Count > 0 Then
                    txtCodigoContratista.Text = Trim(dtTable.Rows(0)("IdContratista").Value)
                    txtContratista.Text = Trim(dtTable.Rows(0)("Contratista").Value)
                    cboEspecialidad.SelectedValue = Trim(dtTable.Rows(0)("esp_codigo").Value)
                End If
                'Timer3.Enabled = True
                'txtContratista_TextChanged(sender, e)
        End Select


    End Sub

    Private Sub txtContratista_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim txtCadenaBusqueda As String = ""
        txtCadenaBusqueda = Trim(txtContratista.Text)
        Panel3.Visible = True
        'rdbNombres.Checked = True
        If Len(txtContratista.Text) > 0 And rdbNombres.Checked = True Then
            eContratistas = New clsContratistas
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eContratistas.fListar(5, gEmpresa, txtCadenaBusqueda)
            dgvContratistas.AutoGenerateColumns = False
            dgvContratistas.DataSource = dtTable
        ElseIf Len(txtContratista.Text) > 0 And rdbEspecialidad.Checked = True Then
            eContratistas = New clsContratistas
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eContratistas.fListar(6, gEmpresa, txtCadenaBusqueda)
            dgvContratistas.AutoGenerateColumns = False
            dgvContratistas.DataSource = dtTable
        ElseIf Len(txtContratista.Text) = 0 Then
            'mMostrarGrilla()
            eContratistas = New clsContratistas
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            dtTable = eContratistas.fListar(0, gEmpresa, "")
            dgvContratistas.AutoGenerateColumns = False
            dgvContratistas.DataSource = dtTable
        End If
    End Sub

    Private Sub dgvContratistas_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvContratistas.CellContentClick

    End Sub

    Private Sub dgvContratistas_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvContratistas.DoubleClick
        If dgvContratistas.Rows.Count > 0 Then
            Try
                txtCodigoContratista.Text = Trim(dgvContratistas.Rows(dgvContratistas.CurrentRow.Index).Cells("Column3").Value)
                txtContratista.Text = Trim(dgvContratistas.Rows(dgvContratistas.CurrentRow.Index).Cells("Contratista").Value)
                cboEspecialidad.SelectedValue = Trim(dgvContratistas.Rows(dgvContratistas.CurrentRow.Index).Cells("esp_codigo").Value)
                'esp_codigo
                Panel3.Visible = False
                cboMoneda.Focus()
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Private m_EditingRow2 As Integer = -1
    Private Sub dgvContratistas_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvContratistas.EditingControlShowing
        m_EditingRow2 = dgvContratistas.CurrentRow.Index
    End Sub

    Private Sub dgvContratistas_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvContratistas.KeyDown
        If dgvContratistas.Rows.Count > 0 Then
            If e.KeyCode = Keys.Return Then
                Dim cur_cell As DataGridViewCell = dgvContratistas.CurrentCell
                Dim col As Integer = cur_cell.ColumnIndex
                col = (col + 1) Mod dgvContratistas.Columns.Count
                cur_cell = dgvContratistas.CurrentRow.Cells(col)
                dgvContratistas.CurrentCell = cur_cell
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvContratistas_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvContratistas.KeyPress
        If dgvContratistas.Rows.Count > 0 Then
            Select Case Asc(e.KeyChar)
                Case 13
                    Try
                        txtCodigoContratista.Text = Trim(dgvContratistas.Rows(dgvContratistas.CurrentRow.Index).Cells("Column3").Value)
                        txtContratista.Text = Trim(dgvContratistas.Rows(dgvContratistas.CurrentRow.Index).Cells("Contratista").Value)
                        cboEspecialidad.SelectedValue = Trim(dgvContratistas.Rows(dgvContratistas.CurrentRow.Index).Cells("esp_codigo").Value)
                        Panel3.Visible = False
                        cboMoneda.Focus()
                    Catch ex As Exception
                        Exit Sub
                    End Try
            End Select
        End If
    End Sub

    Private Sub dgvContratistas_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvContratistas.SelectionChanged
        If m_EditingRow2 >= 0 Then
            Dim new_row As Integer = m_EditingRow2
            m_EditingRow2 = -1
            dgvContratistas.CurrentCell = dgvContratistas.Rows(new_row).Cells(dgvContratistas.CurrentCell.ColumnIndex)
        End If
    End Sub


    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click
        Panel3.Visible = False
    End Sub
    Private Sub rdbNombres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbNombres.Click
        'If rdbNombres.Checked = True Then

        'End If
        'txtContratista.Clear()
        txtContratista_TextChanged(sender, e)
        txtContratista.Focus()
    End Sub
    Private Sub rdbEspecialidad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbEspecialidad.Click
        'txtContratista.Clear()
        txtContratista_TextChanged(sender, e)
        txtContratista.Focus()
    End Sub

    Private Sub Timer3_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer3.Tick
        If Timer3.Enabled = True Then
            'If Len(Trim(txtContratista.Text)) > 0 Then
            dgvContratistas.Focus()
            If Panel3.Visible = False Then
                Panel3.Visible = True
            End If
            'ElseIf Len(Trim(txtContratista.Text)) = 0 Then
            '    cboMoneda.Focus()
            'End If
        End If
        Timer3.Enabled = False
    End Sub

    Private Sub txtBuscar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscar.TextChanged
        Dim txtCadenaBusqueda As String = ""
        txtCadenaBusqueda = Trim(txtBuscar.Text)

        If Len(txtBuscar.Text) > 0 And rdbPartida.Checked = True Then
            ePartidas = New clsPartidas
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            'dtTable = ePartidas.fListar(7, gEmpresa, txtCadenaBusqueda, gPeriodo)

            If Trim(glbUsuCategoria) = "A" Then
                'dtTableLista = eRegistroDocumento.fListar(0, gEmpresa, Trim(gUsuario), gPeriodo, "")
                dtTable = ePartidas.fListar(7, gEmpresa, txtCadenaBusqueda, gPeriodo, gUsuario)
            ElseIf Trim(glbUsuCategoria) = "U" Then
                dtTable = ePartidas.fListar(23, gEmpresa, txtCadenaBusqueda, gPeriodo, gUsuario)
                'dtTableLista = eRegistroDocumento.fListar(50, gEmpresa, Trim(gUsuario), gPeriodo, "")
            End If

            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & ePartidas.iNroRegistros
            If ePartidas.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBuscar.Text) > 0 And rdbObra.Checked = True Then
            ePartidas = New clsPartidas
            eTempo = New clsPlantTempo
            dtTable = New DataTable
            'dtTable = ePartidas.fListar(15, gEmpresa, txtCadenaBusqueda, gPeriodo)

            If Trim(glbUsuCategoria) = "A" Then
                'dtTableLista = eRegistroDocumento.fListar(0, gEmpresa, Trim(gUsuario), gPeriodo, "")
                dtTable = ePartidas.fListar(15, gEmpresa, txtCadenaBusqueda, gPeriodo, gUsuario)
            ElseIf Trim(glbUsuCategoria) = "U" Then
                dtTable = ePartidas.fListar(24, gEmpresa, txtCadenaBusqueda, gPeriodo, gUsuario)
                'dtTableLista = eRegistroDocumento.fListar(50, gEmpresa, Trim(gUsuario), gPeriodo, "")
            End If

            dgvLista.AutoGenerateColumns = False
            dgvLista.DataSource = dtTable
            cmr = BindingContext(dgvLista.DataSource)
            stsTotales.Items(0).Text = "Total de Registros= " & ePartidas.iNroRegistros
            If ePartidas.iNroRegistros > 0 Then
                frmPrincipal.Modifica.Enabled = True
                frmPrincipal.Elimina.Enabled = True
                frmPrincipal.Visualiza.Enabled = True
                frmPrincipal.fDatos = True
            Else
                frmPrincipal.Modifica.Enabled = False
                frmPrincipal.Elimina.Enabled = False
                frmPrincipal.Visualiza.Enabled = False
                frmPrincipal.fDatos = False
            End If
        ElseIf Len(txtBuscar.Text) = 0 Then
            mMostrarGrilla()
        End If
    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click
        Panel1.Visible = False
    End Sub

    Private Sub dgvDetalle_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellEndEdit

        If e.ColumnIndex = 7 Then


            If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")

                If Len(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value) > 0 Then
                    Dim dSaldo As Double = 0
                    dSaldo = Convert.ToDouble(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value)
                    If dSaldo > dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Con_Metrado").Value Then
                        MessageBox.Show("El metrado a contratar es Mayor al metrado de la Partida.", glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value = "0.00"
                    End If
                End If

                If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If
            Else
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If

        End If

        If e.ColumnIndex = 0 Or e.ColumnIndex = 2 Then
            Dim CadenaCon As String
            If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                CadenaCon = ""
            Else
                CadenaCon = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value
            End If
            dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = vb.UCase(CadenaCon)
        End If

        If e.ColumnIndex = 1 Or e.ColumnIndex = 3 Or e.ColumnIndex = 4 Or e.ColumnIndex = 5 Or e.ColumnIndex = 6 Or e.ColumnIndex = 7 Then
            If Trim(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) <> "" Then
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                If vb.IsDBNull(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value) = True Then
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = Format(CDec(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value.ToString), "#,##0.00")
                End If

                If Len(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value) > 0 And Len(dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Con_PrecioSubContra").Value) > 0 Then
                    'dgvDetalle.Rows(x).Cells("Parcial").Value = Format((dgvDetalle.Rows(x).Cells("M2").Value) * (dgvDetalle.Rows(x).Cells("Con_PrecioSubContra").Value), "#,##0.00")
                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Parcial").Value = Math.Round((dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2").Value) * (dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Con_PrecioSubContra").Value), 2)
                End If

            Else
                dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells(e.ColumnIndex).Value = "0.00"
            End If
        End If

        Calcular()
    End Sub

    Private Sub dgvDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetalle.Click
        Calcular()
    End Sub

    Private Sub dgvDetalle_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvDetalle.KeyPress
        'Dim A As Integer
        'Select Case Asc(e.KeyChar)
        '    Case 13
        '        If dgvDetalle.Rows.Count = 0 Then
        '            dgvDetalle.Rows.Add()
        '            dgvDetalle.Rows(0).Cells("Estado").Value = "0"
        '            dgvDetalle.Rows(0).Cells("M1").Value = "100.00"
        '            'dgvDetalle.Rows(0).Cells("Opcion").Value = "0"
        '            'dgvDetalle.Rows(0).Cells("Fecha").Value = Now.Date().ToShortDateString
        '            dgvDetalle.CurrentCell = dgvDetalle(0, 0)
        '        Else
        '            If dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Parcial").Selected = True Then
        '                Dim IntCod As Integer = 0
        '                dgvDetalle.Rows.Add()
        '                A = dgvDetalle.Rows.Count
        '                If dgvDetalle.Rows(A - 2).Cells("Estado").Value = "0" Or dgvDetalle.Rows(A - 2).Cells("Estado").Value = "1" Then
        '                    A = dgvDetalle.Rows.Count
        '                    dgvDetalle.Rows(A - 1).Cells("Estado").Value = "0"
        '                    dgvDetalle.Rows(A - 1).Cells("M1").Value = "100.00"
        '                    'dgvDetalleCajas.Rows(A - 1).Cells(0).Value = Now.Date().ToShortDateString
        '                    'If dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index - 1).Cells(5).Value <> "" Then
        '                    'If dgvDetalleCajas.Rows(A - 2).Cells("CentroCosto").Value <> "" Then
        '                    '    dgvDetalleCajas.Rows(A - 1).Cells("CentroCosto").Value = dgvDetalleCajas.Rows(A - 2).Cells("CentroCosto").Value
        '                    'End If
        '                Else
        '                    dgvDetalle.Rows(dgvDetalle.CurrentRow.Index + 1).Cells("Estado").Value = "0"
        '                    'If dgvDetalle.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value <> "" Then
        '                    '    dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index + 1).Cells("IdGasto").Value = dgvDetalleCajas.Rows(dgvDetalleCajas.CurrentRow.Index).Cells("IdGasto").Value
        '                    'End If
        '                End If
        '                'dgvDetalle.Rows(A - 1).Cells(0).Value = Now.Date().ToShortDateString
        '                dgvDetalle.CurrentCell = dgvDetalle(0, A - 1)
        '            End If
        '        End If
        'End Select
    End Sub

    Private Sub dgvDetalle_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellContentClick

    End Sub

    Private Sub dgvDetalle_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvDetalle.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvDetalle.CurrentCell.ColumnIndex
        If columna = 1 Or columna = 3 Or columna = 4 Or columna = 5 Or columna = 6 Or columna = 7 Then
            Dim caracter As Char = e.KeyChar
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvDetalle_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvDetalle.KeyDown
        Dim count As Integer = 0
        Dim CodigoIdDetalle As String = ""
        Dim Estado As String = ""
        Dim xM2Anterior As Double = 0
        Dim xCon_ConsCodigo As String = ""
        Select Case e.KeyCode
            Case Keys.Delete
                If dgvDetalle.Rows.Count > 0 Then
                    If MessageBox.Show("� Desea Eliminar el Item ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        CodigoIdDetalle = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("idDetalle").Value
                        Estado = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Estado").Value

                        If Estado = "1" Then
                            xM2Anterior = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2Anterior").Value
                            xCon_ConsCodigo = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Con_ConsCodigo").Value

                            ePartidas = New clsPartidas
                            ePartidas.fGrabarDetalle(13, gEmpresa, Trim(txtCodigo.Text), Trim(CodigoIdDetalle), "", 0, "", 0, 0, 0, 0, gUsuario, 0, 0, "", "", xCon_ConsCodigo, xM2Anterior, gPeriodo, Trim(cboCCosto.SelectedValue))
                            dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                            'sCodigoActual = Me.txtCodigo.Text
                        Else
                            dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                        End If
                        Calcular()
                    End If
                End If
        End Select
    End Sub

    Private Sub rdbPartida_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbPartida.Click
        txtBuscar_TextChanged(sender, e)
        txtBuscar.Focus()
    End Sub

    Private Sub rdbObra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbObra.Click
        txtBuscar_TextChanged(sender, e)
        txtBuscar.Focus()
    End Sub

    Private Sub rdbEspecialidad_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbEspecialidad.CheckedChanged

    End Sub

    Private Sub cboObra_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboObra.SelectedIndexChanged
        If cboCCosto.SelectedIndex <> -1 And cboObra.SelectedIndex <> -1 Then
            ePartidas = New clsPartidas
            cboUbiTrabajo.DataSource = ePartidas.fListarUbiTrabajo(17, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue))
            cboUbiTrabajo.ValueMember = "ubtCodigo"
            cboUbiTrabajo.DisplayMember = "ubtDescripcion"
            cboUbiTrabajo.SelectedIndex = -1
        ElseIf cboObra.SelectedIndex = -1 Then
            cboUbiTrabajo.DataSource = Nothing
        End If
        If Panel2.Visible = True Then
            Panel2.Visible = False
            btnBuscar.Text = "Buscar Partidas"
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        cboOpciones.SelectedIndex = 0
        txtBusqueda.Clear()

        If cboCCosto.SelectedIndex > -1 And cboObra.SelectedIndex > -1 Then 'And cboUbiTrabajo.SelectedIndex > -1 

            If Panel2.Visible = False Then
                Panel2.Visible = True
                btnBuscar.Text = "Ocultar Partidas"
            ElseIf Panel2.Visible = True Then
                Panel2.Visible = False
                btnBuscar.Text = "Buscar Partidas"
            End If

            'ePartidas = New clsPartidas
            'cboEspeciali.DataSource = ePartidas.fListar2(18, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(cboEspeciali.SelectedValue), Trim(cboTitulos.SelectedValue))
            'cboEspeciali.ValueMember = "EspCodigo"
            'cboEspeciali.DisplayMember = "EspDescripcion"
            'cboEspeciali.SelectedIndex = -1
            'cboTitulos.DataSource = Nothing


            'ePartidas = New clsPartidas
            'cbo1erNivel.DataSource = ePartidas.fListar2(18, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(cboEspeciali.SelectedValue), Trim(cboTitulos.SelectedValue))
            'cbo1erNivel.ValueMember = "PtiCodigo"
            'cbo1erNivel.DisplayMember = "PtiDescripcion"
            'cbo1erNivel.SelectedIndex = -1
            'cboEspeciali.DataSource = Nothing
            'cboTitulos.DataSource = Nothing

            ePartidas = New clsPartidas
            Dim dtConsolidado As DataTable
            dtConsolidado = New DataTable
            dtConsolidado = ePartidas.fListar2(20, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), "", "")
            If dgvConsolidado.Rows.Count > 0 Then
                For x As Integer = 0 To dgvConsolidado.RowCount - 1
                    dgvConsolidado.Rows.Remove(dgvConsolidado.CurrentRow)
                Next
            End If
            dgvConsolidado.DataSource = dtConsolidado

            txtBusqueda.Focus()
            'If dgvConsolidado.Rows.Count > 0 Then
            '    For x As Integer = 0 To dgvConsolidado.RowCount - 1
            '        dgvConsolidado.Rows.Remove(dgvConsolidado.CurrentRow)
            '    Next
            'End If

        Else
            If cboCCosto.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboCCosto.Focus()
                Exit Sub
            End If

            If cboObra.SelectedIndex = -1 Then
                MessageBox.Show("Seleccione una Obra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cboObra.Focus()
                Exit Sub
            End If

            'If cboUbiTrabajo.SelectedIndex = -1 Then----quitado 
            '    MessageBox.Show("Seleccione un Presupuesto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    cboUbiTrabajo.Focus()
            '    Exit Sub
            'End If
        End If

    End Sub

    Private Sub cboEspeciali_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

        'If cboCCosto.SelectedIndex <> -1 And cboObra.SelectedIndex <> -1 And cboUbiTrabajo.SelectedIndex <> -1 And cboEspeciali.SelectedIndex <> -1 Then
        '    ePartidas = New clsPartidas
        '    cboTitulos.DataSource = ePartidas.fListar2(19, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(cboEspeciali.SelectedValue), Trim(cboTitulos.SelectedValue))
        '    cboTitulos.ValueMember = "TitCodigo"
        '    cboTitulos.DisplayMember = "TitDescripcion"
        '    cboTitulos.SelectedIndex = -1
        'ElseIf cboEspeciali.SelectedIndex = -1 Then
        '    cboTitulos.DataSource = Nothing
        'End If

    End Sub

    'Private Sub cboEspeciali_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ePartidas = New clsPartidas
    '    cboTitulos.DataSource = ePartidas.fListar2(19, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(cboEspeciali.SelectedValue), Trim(cboTitulos.SelectedValue))
    '    cboTitulos.ValueMember = "TitCodigo"
    '    cboTitulos.DisplayMember = "TitDescripcion"
    '    cboTitulos.SelectedIndex = -1
    '    If dgvConsolidado.Rows.Count > 0 Then
    '        For x As Integer = 0 To dgvConsolidado.RowCount - 1
    '            dgvConsolidado.Rows.Remove(dgvConsolidado.CurrentRow)
    '        Next
    '    End If
    'End Sub

    'Private Sub cboTitulos_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ePartidas = New clsPartidas
    '    Dim dtConsolidado As DataTable
    '    dtConsolidado = New DataTable
    '    dtConsolidado = ePartidas.fListar2(20, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(cboEspeciali.SelectedValue), Trim(cboTitulos.SelectedValue))
    '    If dgvConsolidado.Rows.Count > 0 Then
    '        For x As Integer = 0 To dgvConsolidado.RowCount - 1
    '            dgvConsolidado.Rows.Remove(dgvConsolidado.CurrentRow)
    '        Next
    '    End If
    '    dgvConsolidado.DataSource = dtConsolidado
    'End Sub

    Private Sub cboTitulos_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAgregar.Click
        Dim CountSeleccionados As Integer = 0
        If dgvConsolidado.Rows.Count > 0 Then
            For x As Integer = 0 To dgvConsolidado.Rows.Count - 1
                If Convert.ToBoolean(dgvConsolidado.Rows(x).Cells("X").Value) = True Then
                    CountSeleccionados = CountSeleccionados + 1
                    If dgvDetalle.Rows.Count = 0 Then
                        dgvDetalle.Rows.Add()
                        dgvDetalle.Rows(0).Cells("Descripcion").Value = dgvConsolidado.Rows(x).Cells("ParDescripcion").Value
                        dgvDetalle.Rows(0).Cells("Und").Value = dgvConsolidado.Rows(x).Cells("UmdAbreviado").Value
                        dgvDetalle.Rows(0).Cells("M1").Value = "100.00"
                        dgvDetalle.Rows(0).Cells("Con_Metrado").Value = dgvConsolidado.Rows(x).Cells("Metrado").Value
                        dgvDetalle.Rows(0).Cells("PU").Value = dgvConsolidado.Rows(x).Cells("PrecioPresupuesto").Value
                        dgvDetalle.Rows(0).Cells("Con_PrecioSubContra").Value = dgvConsolidado.Rows(x).Cells("PrecioPresupuesto").Value
                        dgvDetalle.Rows(0).Cells("M2").Value = dgvConsolidado.Rows(x).Cells("Metrado").Value
                        dgvDetalle.Rows(0).Cells("Parcial").Value = dgvConsolidado.Rows(x).Cells("Metrado").Value + dgvConsolidado.Rows(x).Cells("PrecioPresupuesto").Value
                        dgvDetalle.Rows(0).Cells("Estado").Value = "0"
                        dgvDetalle.Rows(0).Cells("Con_TitCodigo").Value = dgvConsolidado.Rows(x).Cells("TitCodigo").Value
                        dgvDetalle.Rows(0).Cells("Con_ParCodigo").Value = dgvConsolidado.Rows(x).Cells("ParCodigo").Value
                        dgvDetalle.Rows(0).Cells("Con_ConsCodigo").Value = dgvConsolidado.Rows(x).Cells("ConsCodigo").Value
                    ElseIf dgvDetalle.Rows.Count > 0 Then
                        Dim Existe As String = ""
                        For q As Integer = 0 To dgvDetalle.Rows.Count - 1
                            If Trim(dgvConsolidado.Rows(x).Cells("ConsCodigo").Value) = Trim(dgvDetalle.Rows(q).Cells("Con_ConsCodigo").Value) Then
                                Existe = "1"
                                Exit For
                            Else
                                Existe = "0"
                            End If
                        Next
                        If Existe = "0" Then
                            dgvDetalle.Rows.Add()
                            Dim A As Integer
                            A = dgvDetalle.Rows.Count
                            dgvDetalle.Rows(A - 1).Cells("Descripcion").Value = dgvConsolidado.Rows(x).Cells("ParDescripcion").Value
                            dgvDetalle.Rows(A - 1).Cells("Und").Value = dgvConsolidado.Rows(x).Cells("UmdAbreviado").Value
                            dgvDetalle.Rows(A - 1).Cells("M1").Value = "100.00"
                            dgvDetalle.Rows(A - 1).Cells("Con_Metrado").Value = dgvConsolidado.Rows(x).Cells("Metrado").Value
                            dgvDetalle.Rows(A - 1).Cells("PU").Value = dgvConsolidado.Rows(x).Cells("PrecioPresupuesto").Value
                            dgvDetalle.Rows(A - 1).Cells("Con_PrecioSubContra").Value = dgvConsolidado.Rows(x).Cells("PrecioSubcontrato").Value
                            dgvDetalle.Rows(A - 1).Cells("M2").Value = "0.00"
                            dgvDetalle.Rows(A - 1).Cells("Parcial").Value = "0.00"
                            dgvDetalle.Rows(A - 1).Cells("Estado").Value = "0"
                            dgvDetalle.Rows(A - 1).Cells("Con_TitCodigo").Value = dgvConsolidado.Rows(x).Cells("TitCodigo").Value
                            dgvDetalle.Rows(A - 1).Cells("Con_ParCodigo").Value = dgvConsolidado.Rows(x).Cells("ParCodigo").Value
                            dgvDetalle.Rows(A - 1).Cells("Con_ConsCodigo").Value = dgvConsolidado.Rows(x).Cells("ConsCodigo").Value
                        End If
                    End If
                End If
            Next
        End If

        If CountSeleccionados = 0 Then
            MessageBox.Show("No ha seleccionado ninguna Partida", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        Panel2.Visible = False

        btnBuscar.Text = "Buscar Partidas"

        If dgvDetalle.Rows.Count > 0 Then
            dgvDetalle.CurrentCell = dgvDetalle(7, 0)
            dgvDetalle.Focus()
        End If
        Calcular()
    End Sub

 
    Private Sub btnQuitar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuitar.Click
        Dim count As Integer = 0
        Dim CodigoIdDetalle As String = ""
        Dim Estado As String = ""
        Dim xM2Anterior As Double = 0
        If dgvDetalle.Rows.Count > 0 Then
            If MessageBox.Show("� Desea Eliminar el Item ?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                CodigoIdDetalle = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("idDetalle").Value
                Estado = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("Estado").Value
                If Estado = "1" Then
                    xM2Anterior = dgvDetalle.Rows(dgvDetalle.CurrentRow.Index).Cells("M2Anterior").Value
                    ePartidas = New clsPartidas
                    ePartidas.fGrabarDetalle(13, gEmpresa, Trim(txtCodigo.Text), Trim(CodigoIdDetalle), "", 0, "", 0, 0, 0, 0, gUsuario, 0, 0, "", "", "", xM2Anterior, gPeriodo, Trim(cboCCosto.SelectedValue))
                    dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                    'sCodigoActual = Me.txtCodigo.Text
                Else
                    dgvDetalle.Rows.RemoveAt(dgvDetalle.CurrentRow.Index)
                End If
                Calcular()
            End If
        End If
    End Sub

    Private Sub dgvConsolidado_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvConsolidado.EditingControlShowing
        Try
            'referencia a la celda   
            Dim validar As TextBox = CType(e.Control, TextBox)
            'agregar el controlador de eventos para el KeyPress   
            AddHandler validar.KeyPress, AddressOf validar_Keypress2
        Catch ex As Exception
        End Try
    End Sub

    Private Sub validar_Keypress2(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim columna As Integer = dgvConsolidado.CurrentCell.ColumnIndex
        If columna = 3 Or columna = 4 Then
            Dim caracter As Char = e.KeyChar
            Dim txt As TextBox = CType(sender, TextBox)
            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
            ' es el separador decimal, y que no contiene ya el separador   
            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
                e.Handled = False
            Else
                e.Handled = True
            End If
        End If
    End Sub


    Private Sub cboUbiTrabajo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUbiTrabajo.SelectedIndexChanged

    End Sub

    Private Sub cboCCosto_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCCosto.SelectedIndexChanged
        If Panel2.Visible = True Then
            Panel2.Visible = False
            btnBuscar.Text = "Buscar Partidas"
        End If
    End Sub

    Private Sub txtObservacion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtObservacion.TextChanged

    End Sub


    Private Sub BeLabel12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeLabel12.Click

    End Sub

    Private Sub cbo1erNivel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    'Private Sub cbo1erNivel_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs)
    '    ePartidas = New clsPartidas
    '    cboEspeciali.DataSource = ePartidas.fListar2(26, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(cbo1erNivel.SelectedValue), Trim(cboTitulos.SelectedValue))
    '    cboEspeciali.ValueMember = "EspCodigo"
    '    cboEspeciali.DisplayMember = "EspDescripcion"
    '    cboEspeciali.SelectedIndex = -1
    '    If dgvConsolidado.Rows.Count > 0 Then
    '        For x As Integer = 0 To dgvConsolidado.RowCount - 1
    '            dgvConsolidado.Rows.Remove(dgvConsolidado.CurrentRow)
    '        Next
    '    End If
    '    cboTitulos.DataSource = Nothing
    'End Sub

    Private Sub btnLiquidar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLiquidar.Click
        If Len(Trim(txtCodigo.Text)) > 0 Then
            If (MessageBox.Show("�Esta seguro de Liquidar el Contrato?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.Yes) Then
                ePartidas = New clsPartidas
                dtTable = New DataTable
                dtTable = ePartidas.fListarDetalle(28, gEmpresa, Trim(txtCodigo.Text), Today(), gPeriodo, Trim(cboCCosto.SelectedValue))
                'dgvContratistas.AutoGenerateColumns = False
                'dgvContratistas.DataSource = dtTable

                Dim iResultadoContrato As Integer = 0
                Dim iResultadoCons As Integer = 0
                If dtTable.Rows.Count > 0 Then
                    Dim IdConsolidado As String = ""
                    Dim Restar As Double = 0
                    For y As Integer = 0 To dtTable.Rows.Count - 1
                        IdConsolidado = Trim(dtTable.Rows(y).Item("Con_ConsCodigo").ToString)
                        Restar = dtTable.Rows(y).Item("Restar").ToString
                        iResultadoCons = ePartidas.fGrabarPartida(29, gEmpresa, Trim(txtCodigo.Text), Trim(txtCodigoContratista.Text), cboMoneda.SelectedValue, cboTipoDocumento.SelectedValue, Trim(txtPartida.Text), dtpFechaIni.Value, dtpFechaFin.Value, Trim(txtUbicacion.Text), Trim(txtPresupuesto.Text), Trim(txtRetencion.Text), cboEspecialidad.SelectedValue, Trim(txtObservacion.Text), cboEstado.SelectedValue, cboObra.SelectedValue, gPeriodo, gUsuario, Trim(cboCCosto.SelectedValue), Restar, Trim(txtIGV.Text), Trim(txtNumeroOrden.Text), Trim(txtNroContrato.Text), Trim(cboUbiTrabajo.SelectedValue), IdConsolidado)
                    Next
                End If

                If iResultadoCons > 0 Then
                    iResultadoContrato = ePartidas.fGrabarPartida(30, gEmpresa, Trim(txtCodigo.Text), Trim(txtCodigoContratista.Text), cboMoneda.SelectedValue, cboTipoDocumento.SelectedValue, Trim(txtPartida.Text), dtpFechaIni.Value, dtpFechaFin.Value, Trim(txtUbicacion.Text), Trim(txtPresupuesto.Text), Trim(txtRetencion.Text), cboEspecialidad.SelectedValue, Trim(txtObservacion.Text), cboEstado.SelectedValue, cboObra.SelectedValue, gPeriodo, gUsuario, Trim(cboCCosto.SelectedValue), Trim(txtSubTotal.Text), Trim(txtIGV.Text), Trim(txtNumeroOrden.Text), Trim(txtNroContrato.Text), Trim(cboUbiTrabajo.SelectedValue), "")

                    If sTab = 1 Then
                        sTab = 0
                        TabControl1.TabPages(0).Focus()
                        'Timer2.Enabled = True
                    End If
                    mMostrarGrilla()
                    DesabilitarControles()
                End If
            End If
        End If
    End Sub



    Private Sub txtBusqueda_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBusqueda.TextChanged

        If Len(Trim(txtBusqueda.Text)) = 0 Then
            ePartidas = New clsPartidas
            Dim dtConsolidado As DataTable
            dtConsolidado = New DataTable
            dtConsolidado = ePartidas.fListar2(20, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), "", "")
            If dgvConsolidado.Rows.Count > 0 Then
                For x As Integer = 0 To dgvConsolidado.RowCount - 1
                    dgvConsolidado.Rows.Remove(dgvConsolidado.CurrentRow)
                Next
            End If
            dgvConsolidado.DataSource = dtConsolidado
            Exit Sub
        ElseIf Len(Trim(txtBusqueda.Text)) > 0 Then
            If cboCCosto.SelectedIndex > -1 And cboObra.SelectedIndex > -1 And cboUbiTrabajo.SelectedIndex > -1 And cboOpciones.SelectedIndex > -1 Then
                'If Panel2.Visible = False Then
                '    Panel2.Visible = True
                '    btnBuscar.Text = "Ocultar Partidas"
                'ElseIf Panel2.Visible = True Then
                '    Panel2.Visible = False
                '    btnBuscar.Text = "Buscar Partidas"
                'End If
                ePartidas = New clsPartidas
                Dim dtConsolidado As DataTable
                dtConsolidado = New DataTable
                If Trim(cboOpciones.Text) = "C�digo" Then
                    dtConsolidado = ePartidas.fListar2(31, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(txtBusqueda.Text), "")

                ElseIf Trim(cboOpciones.Text) = "Partida" Then
                    dtConsolidado = ePartidas.fListar2(32, gEmpresa, Trim(cboCCosto.SelectedValue), Trim(cboObra.SelectedValue), Trim(cboUbiTrabajo.SelectedValue), Trim(txtBusqueda.Text), "")
                End If
                If dgvConsolidado.Rows.Count > 0 Then
                    For x As Integer = 0 To dgvConsolidado.RowCount - 1
                        dgvConsolidado.Rows.Remove(dgvConsolidado.CurrentRow)
                    Next
                End If
                dgvConsolidado.DataSource = dtConsolidado
            Else
                If cboCCosto.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Centro de Costo", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboCCosto.Focus()
                    Exit Sub
                End If

                If cboObra.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Obra", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboObra.Focus()
                    Exit Sub
                End If
                If cboUbiTrabajo.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione un Presupuesto", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboUbiTrabajo.Focus()
                    Exit Sub
                End If
                If cboOpciones.SelectedIndex = -1 Then
                    MessageBox.Show("Seleccione una Opci�n de B�squeda", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cboOpciones.Focus()
                    Exit Sub
                End If
            End If
        End If

       
    End Sub

    Private Sub cboOpciones_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOpciones.SelectedIndexChanged
        txtBusqueda.Clear()
        txtBusqueda.Focus()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (txtCodigo.Text <> "") Then
            Dim x As WFAgregarPartida = WFAgregarPartida.Instance
            x.setfila(Fila)
            x.Show()
        Else
            MessageBox.Show("Solo Se Puede Agregar Adendas a Contratos ya Creados", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If



    End Sub

    Private Sub txtRuc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRuc.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13

                If Trim(txtRuc.Text) = "" Then
                    MessageBox.Show("Ingrese n�mero del RUC", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    'txtRuc.Focus()
                    Exit Sub
                End If
                If IsNumeric(txtRuc.Text) = True Then
                    If Len(txtRuc.Text) < 11 Then
                        'Limpiar()

                        MessageBox.Show("Ingrese los 11 n�meros del RUC", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'txtRuc.Focus()
                        Exit Sub
                    End If
                    If Val(Mid(Trim(txtRuc.Text), 2, 9)) = 0 Or Trim(txtRuc.Text) = "23333333333" Then
                        'Limpiar()
                        MessageBox.Show("Verificar n�mero del RUC", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        'txtRuc.Focus()
                        Exit Sub
                    End If
                    If Verificar_ruc(txtRuc.Text) = False Then
                        'Limpiar()
                        MessageBox.Show("El n�mero del RUC no es v�lido", "", MessageBoxButtons.OK, MessageBoxIcon.Error)

                        'txtRuc.Focus()
                        Exit Sub
                    End If
                    'Call VALRUC(v_Ruc)
                    'OTROVALRUC(v_Ruc)
                Else
                    MessageBox.Show("Solo se aceptan n�meros", "", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ' txtRuc.Focus()
                End If
                eContratistas = New clsContratistas
                dtTable = New DataTable
                dtTable = eContratistas.fListar(8, gEmpresa, txtRuc.Text)
                If dtTable.Rows.Count > 0 Then
                    txtCodigoContratista.Text = Trim(dtTable.Rows(0)("idcontratista")).ToString
                    txtContratista.Text = Trim(dtTable.Rows(0)("Contratista")).ToString
                    cboEspecialidad.SelectedValue = Trim(dtTable.Rows(0)("esp_codigo")).ToString
                Else
                    Dim x As frmContratistasMantenimiento = frmContratistasMantenimiento.Instance
                    Dim Barra As ToolStrip = frmPrincipal.Barra
                    x.CControl = Barra
                    Barra.Enabled = True

                    x.Nuevo = True
                    x.Modificar = True
                    x.Eliminar = True
                    x.Grabar = False
                    x.Cancelar = False

                    x.Buscar = False
                    x.Exportar = False
                    x.Imprimir = False
                    x.Visualizar = True
                    x.Salir = True

                    x.set_ruc(txtRuc.Text)


                    x.MdiParent = frmPrincipal
                    x.TabControl1.SelectedIndex = 1
                    x.TabControl1.DisablePage(x.TabControl1.TabPages(0))
                    x.txtRuc.Text = txtRuc.Text
                    x.Show()

                End If

                'Timer3.Enabled = True
                'txtContratista_TextChanged(sender, e)
        End Select
    End Sub

    Private Sub cboUbiTrabajo_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboUbiTrabajo.SelectionChangeCommitted
        txtPartida.Text = cboUbiTrabajo.Text
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBox1.CheckedChanged

        Me.dgvConsolidado.ClearSelection()

        Try
            Using scope As TransactionScope = New TransactionScope

                For Each fila As DataGridViewRow In Me.dgvConsolidado.Rows
                    If Me.CheckBox1.Checked = True Then
                        fila.Cells("X").Value = True
                    Else
                        fila.Cells("X").Value = False
                    End If

                Next

                scope.Complete()
            End Using


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub
End Class
