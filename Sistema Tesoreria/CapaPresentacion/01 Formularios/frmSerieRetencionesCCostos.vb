Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Windows.Forms

Public Class frmSerieRetencionesCCostos

    Public strCodigoSerie As String
    Public strDesSerie As String
    Dim dtTable As DataTable
    Private eTipoGastosG1 As clsTipoGastosG1
    Private eMovimientoCajaBanco As clsMovimientoCajaBanco

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmSerieRetencionesCCostos = Nothing
    Public Shared Function Instance() As frmSerieRetencionesCCostos
        If frmInstance Is Nothing Then
            frmInstance = New frmSerieRetencionesCCostos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmSerieRetencionesCCostos_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmSerieRetencionesCCostos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ListaPermisoMenu()
        Label1.Text = strDesSerie
    End Sub

    Sub ListaPermisoMenu()
        Try
            dtTable = New DataTable
            eTipoGastosG1 = New clsTipoGastosG1
            dtTable = eTipoGastosG1.fListarCC(gEmpresa)
            Me.DgvLista.AutoGenerateColumns = False
            Me.DgvLista.DataSource = dtTable 'TAB()
            Me.DgvLista.ReadOnly = False
            Me.DgvLista.Columns(0).ReadOnly = True
            Me.DgvLista.Columns(1).ReadOnly = True
            Me.DgvLista.Columns(2).ReadOnly = False
            ActualizaValoresMenu()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Sub ActualizaValoresMenu()
        'Dim n As Integer
        'Dim Cadena As String

        For x As Integer = 0 To DgvLista.Rows.Count - 1
            DgvLista.Rows(x).Cells("Column3").Value = False
        Next

        'If DgvLista.Rows(x).Cells("Column3").Value <> True Then
        'End If

        Me.DgvLista.ClearSelection()
        Try
            Dim dtTableCC As DataTable
            Dim CCCadenaSerie As String = ""
            dtTableCC = New DataTable
            eMovimientoCajaBanco = New clsMovimientoCajaBanco
            dtTableCC = eMovimientoCajaBanco.fListarCCSerie(9, Trim(strCodigoSerie), gEmpresa)

            If dtTableCC.Rows.Count > 0 Then
                CCCadenaSerie = IIf(Microsoft.VisualBasic.IsDBNull(dtTableCC.Rows(0).Item(0)), "", dtTableCC.Rows(0).Item(0))
            End If

            If DgvLista.Rows.Count > 0 Then
                Dim a() As String
                a = Split(Trim(CCCadenaSerie), ",")
                'Dim i As Integer = 0
                'For i = LBound(a) To UBound(a)
                '    For x As Integer = 0 To DgvLista.Rows.Count - 1
                '        Dim CCEmpresa As String = ""
                '        CCEmpresa = Trim(DgvLista.Rows(x).Cells("Column1").Value)
                '        If CCEmpresa = Trim(a(i)) Then
                '            DgvLista.Rows(x).Cells("Column3").Value = True
                '        Else
                '            DgvLista.Rows(x).Cells("Column3").Value = False
                '        End If
                '    Next
                'Next i
                For x As Integer = 0 To DgvLista.Rows.Count - 1
                    Dim CCEmpresa As String = ""
                    CCEmpresa = Trim(DgvLista.Rows(x).Cells("Column1").Value)
                    Dim i As Integer = 0
                    For i = LBound(a) To UBound(a)
                        If CCEmpresa = Trim(a(i)) Then
                            DgvLista.Rows(x).Cells("Column3").Value = True
                            'DgvLista.CurrentCell.ColumnIndex.MaxValue()
                        End If
                    Next
                Next
            End If



            'Using scope As TransactionScope = New TransactionScope
            '    n = 1
            '    For Each fila As DataGridViewRow In Me.DgvLista.Rows
            '        Cadena = strCodigoSerie '"2" 'Trim(Mid(strHabilita, n, 1))
            '        If Cadena <> "" Then
            '            'If fila.Cells("Grupo1").Value = Nothing Then
            '            'End If
            '            If Cadena = "1" Then
            '                If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoInverCapital").Value) = False Then
            '                    If Cadena = Trim(fila.Cells("GrupoInverCapital").Value) Then
            '                        fila.Cells("Column3").Value = True
            '                    Else
            '                        fila.Cells("Column3").Value = False
            '                    End If
            '                Else
            '                    fila.Cells("Column3").Value = False
            '                End If
            '            ElseIf Cadena = "2" Then
            '                If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoAdmFijos").Value) = False Then
            '                    If Cadena = Trim(fila.Cells("GrupoAdmFijos").Value) Then
            '                        fila.Cells("Column3").Value = True
            '                    Else
            '                        fila.Cells("Column3").Value = False
            '                    End If
            '                Else
            '                    fila.Cells("Column3").Value = False
            '                End If
            '            ElseIf Cadena = "3" Then
            '                If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoAdmOca").Value) = False Then
            '                    If Cadena = Trim(fila.Cells("GrupoAdmOca").Value) Then
            '                        fila.Cells("Column3").Value = True
            '                    Else
            '                        fila.Cells("Column3").Value = False
            '                    End If
            '                Else
            '                    fila.Cells("Column3").Value = False
            '                End If
            '            ElseIf Cadena = "4" Then
            '                If Microsoft.VisualBasic.IsDBNull(fila.Cells("GrupoGastosObra").Value) = False Then
            '                    If Cadena = Trim(fila.Cells("GrupoGastosObra").Value) Then
            '                        fila.Cells("Column3").Value = True
            '                    Else
            '                        fila.Cells("Column3").Value = False
            '                    End If
            '                Else
            '                    fila.Cells("Column3").Value = False
            '                End If
            '            End If
            '        End If
            '        n = n + 1
            '    Next
            '    scope.Complete()
            'End Using

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Me.DgvLista.ClearSelection()
        Try
            Using scope As TransactionScope = New TransactionScope
                For Each fila As DataGridViewRow In Me.DgvLista.Rows
                    If Me.CheckBox1.Checked = True Then
                        fila.Cells("Column3").Value = True
                    Else
                        fila.Cells("Column3").Value = False
                    End If
                Next
                scope.Complete()
            End Using
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, glbNameSistema)
        End Try
    End Sub

    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        Dim strGlosaCC As String = ""
        Me.DgvLista.ClearSelection()
        'eTipoGastosG1 = New clsTipoGastosG1
        For z As Integer = 0 To DgvLista.Rows.Count - 1
            If Microsoft.VisualBasic.IsDBNull(DgvLista.Rows(z).Cells(2).Value) = False Then
                If Convert.ToBoolean(DgvLista.Rows(z).Cells(2).Value) = True Then
                    strGlosaCC = strGlosaCC & Trim(DgvLista.Rows(z).Cells("Column1").Value) & ","
                End If
            End If
        Next
        'eTipoGastosG1.fActualizarGasto(18, 0, gEmpresa, "", Trim(DgvLista.Rows(z).Cells(0).Value))

        eMovimientoCajaBanco = New clsMovimientoCajaBanco
        Dim iResultado As Integer = 0
        iResultado = eMovimientoCajaBanco.fGrabarRetencion(8, strCodigoSerie, gEmpresa, gSerie, "", Today(), "", "", "", strGlosaCC, 0, PorcentajeRetencion, 0, 0, 0, 0, gUsuario, "")
        If iResultado = 1 Then
            MessageBox.Show("Se Actualiz� Correctamente", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If
        Me.Close()

    End Sub

    

    Private Sub DgvLista_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DgvLista.Click
        'ActualizaValoresMenu()
    End Sub

    Private Sub DgvLista_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DgvLista.CellContentClick

    End Sub
End Class