Option Explicit On
Imports System.Xml
Imports MSXML2
Imports System.Math

Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Data.Common
Imports System.Data.SqlClient
Imports vb = Microsoft.VisualBasic
Imports CapaEntidad
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmRegistroDetraccion

    Private eDetracciones As clsDetraccion
    Dim odtDetraccion As New DataTable
    Dim odtDetraccionDet As New DataTable

    Dim objResultado As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()

    Public Shared idRegistroDocumentoCab As Decimal
    Dim strAccion As String
    Dim decImporteDetraccion As Decimal
    Dim decImporteTotal As Decimal
    Private Loaded As Boolean = False

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Dim rptRetencion As New rptRetenciones

    Private cCapaDatos As clsCapaDatos

#Region " Procedimiento de evento de controles "

    Private Sub frmSesionCaja_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Loaded = True
        'VL_DOCUMENTO = "RETENCION"
        eDetracciones = New clsDetraccion
        cCapaDatos = New clsCapaDatos

        cboAnio.Text = Convert.ToString(DateTime.Now.Year)
        cboMes.SelectedIndex = DateTime.Now.Month - 1

        tabMantenimiento.SelectedTab = tabLista
        botonesMantenimiento(mnuMantenimiento, True)

        InitCombos()

        CargarDetraccionCab()
        CargarDetraccionDet()
    End Sub

    Private Sub tabMantenimiento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabMantenimiento.SelectedIndexChanged
        If tabMantenimiento.SelectedIndex = 0 Then
            strAccion = Accion.Grabar
            'grpFacturas.Visible = False
            botonesMantenimiento(mnuMantenimiento, True)
        Else
            strAccion = Accion.Modifica
            cboDocumentoOrigen.Enabled = False
            grpFacturas.Visible = False
            btnBuscarAnexo.Enabled = False
            dtFecha.Enabled = False
            'txtRuc.ReadOnly = True
            botonesMantenimiento(mnuMantenimiento, False)
        End If
    End Sub

    Private Sub cboAnio_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboAnio.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarDetraccionCab()
    End Sub

    Private Sub cboMes_SelectedIndexChanged(sender As Object, e As Telerik.WinControls.UI.Data.PositionChangedEventArgs) Handles cboMes.SelectedIndexChanged
        If Not Loaded Then Exit Sub
        CargarDetraccionCab()
    End Sub

    Private Sub btnRefrescar_Click(sender As Object, e As EventArgs) Handles btnRefrescar.Click
        CargarDetraccionCab()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        tabMantenimiento.SelectedTab = tabDetalle
        strAccion = Accion.Grabar
        LimpiarControles()
        cboDocumentoOrigen.SelectedIndex = 2 ' Factura
        cboDocumentoOrigen.Enabled = True
        btnBuscarAnexo.Enabled = True
        dtFecha.Enabled = True
        'txtRuc.ReadOnly = False
        odtDetraccionDet.Clear()
        dtFecha.Value = Date.Today
        'txtNombre.Focus()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        tabMantenimiento.SelectedTab = tabLista
        CargarDetraccionCab()
    End Sub

    Private Sub cboDocumentoOrigen_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDocumentoOrigen.SelectedValueChanged
        odtDetraccionDet.Clear()
        gcDetraccionDetalle.DataSource = Nothing
        gcFacturas.DataSource = Nothing
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        ''Se valida los campos
        gcDetraccionDetalle.EndEdit()
        'If txtRuc.Text.Trim.Length = 0 Then MessageBox.Show("El proveedor es obligatorio.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation) : txtRuc.Focus() : Exit Sub
        SumarImporte()

        'If gcRetencionDetalle.RowCount > 0 Then
        Select Case strAccion
            Case Accion.Grabar
                Guardar()
            Case Accion.Modifica
                If Convert.ToDecimal(Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1) Then
                    If Not ComprobarDato() Then Exit Sub
                End If
                Modificar()
        End Select

        CargarDetraccionCab()
        tabMantenimiento.SelectedTab = tabLista
        'Else
        'MessageBox.Show("No existe registro en detalle retenciones. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        'End If
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If gcDetraccion.RowCount > 0 Then
            If Not Eliminar() Then Exit Sub
            CargarDetraccionCab()
        Else
            MessageBox.Show("Seleccione registro a eliminar. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub dgvRetencion_CurrentRowChanged(sender As Object, e As Telerik.WinControls.UI.CurrentRowChangedEventArgs) Handles gcDetraccion.CurrentRowChanged
        idRegistroDocumentoCab = Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value)

        dtFecha.Value = Convert.ToDateTime(gcDetraccion.CurrentRow.Cells("colFecha").Value)
        txtCodigoAnexo.Text = Convert.ToString(gcDetraccion.CurrentRow.Cells("colCodProveedor").Value)
        txtRuc.Text = Convert.ToString(gcDetraccion.CurrentRow.Cells("colRuc").Value)
        lblNombreAnexo.Text = Convert.ToString(gcDetraccion.CurrentRow.Cells("colNombreProveedor").Value)
        txtNroCtaDetraccion.Text = Convert.ToString(gcDetraccion.CurrentRow.Cells("colNroCtaDetraccion").Value)
        txtNroOperacion.Text = Convert.ToString(gcDetraccion.CurrentRow.Cells("colNroOperacion").Value)
        txtObservacion.Text = Convert.ToString(gcDetraccion.CurrentRow.Cells("colDescripcion").Value)
        cboDocumentoOrigen.SelectedValue = Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value)

        CargarDetraccionDet()
    End Sub

    Private Sub dgvRetencion_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcDetraccion.CellDoubleClick
        If gcDetraccion.RowCount > 0 Then
            tabMantenimiento.SelectedTab = tabDetalle
        End If
    End Sub

    Private Sub txtRuc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRuc.KeyPress
        If Len(txtRuc.Text) = 11 Then
            Select Case Asc(e.KeyChar)
                Case 13
                    Try
                        grpFacturas.Visible = True
                        If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then ' Orden de compra
                            odtDetraccion = eDetracciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
                            gcFacturas.DataSource = odtDetraccion
                        ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then ' Factura
                            odtDetraccion = eDetracciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
                            gcFacturas.DataSource = odtDetraccion
                        ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 4 Then ' Valorizaci�n
                            odtDetraccion = eDetracciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 9)
                            gcFacturas.DataSource = odtDetraccion
                        End If
                    Catch ex As Exception
                        Exit Sub
                    End Try
            End Select
        End If
    End Sub

    Private Sub btnBuscarAnexo_Click(sender As Object, e As EventArgs) Handles btnBuscarAnexo.Click
        Dim Proveedores As New frmBuscaProveedor()
        If Proveedores.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtCodigoAnexo.Text = frmBuscaProveedor.strCodigo
            txtRuc.Text = frmBuscaProveedor.strRuc
            lblNombreAnexo.Text = frmBuscaProveedor.strNombre
            lblDireccion.Text = frmBuscaProveedor.strDireccion
            txtNroCtaDetraccion.Text = frmBuscaProveedor.strCtaDetraccion

            grpFacturas.Visible = True

            If Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 1 Then ' Orden de compra
                odtDetraccion = eDetracciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 8)
                gcFacturas.DataSource = odtDetraccion
            ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 101 Then ' Factura
                odtDetraccion = eDetracciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 4)
                gcFacturas.DataSource = odtDetraccion
            ElseIf Convert.ToDecimal(cboDocumentoOrigen.SelectedValue) = 4 Then ' Valorizaci�n
                odtDetraccion = eDetracciones.fCargarDocumentos(txtRuc.Text, gEmpresa, 9)
                gcFacturas.DataSource = odtDetraccion
            End If
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        gcFacturas.CloseEditor()
        If gcFacturas.RowCount > 0 Then
            'Detalle
            For i As Integer = 0 To gcFacturas.RowCount - 1
                If CBool(gcFacturas.Rows(i).Cells("colChkEstado").Value) = True Then

                    Dim importeTipoCambio As Decimal = 0
                    If Convert.ToString(gcFacturas.CurrentRow.Cells("colIdMoneda").Value) = "02" Then
                        Dim iResultado As Decimal = 0

                        iResultado = eDetracciones.fImporteTipoCambio(CDate(dtFecha.Value), 12)
                        importeTipoCambio = iResultado
                    End If

                    Dim row As DataRow
                    row = odtDetraccionDet.NewRow
                    row("IdDetraccionDet") = 0
                    row("IdDetraccionCab") = 0
                    row("IdRegistroDocumento") = gcFacturas.Rows(i).Cells("colIdRegistro").Value
                    row("EmprCodigo") = gcFacturas.Rows(i).Cells("colEmprCodigo").Value
                    row("IdDocumento") = gcFacturas.Rows(i).Cells("colIdDocumento").Value
                    row("Serie") = gcFacturas.Rows(i).Cells("colSerie").Value
                    row("Numero") = gcFacturas.Rows(i).Cells("colNumero").Value
                    row("Fecha") = gcFacturas.Rows(i).Cells("colFecha").Value
                    row("Porcentaje") = iValorPorcentajeDetraccion * 100
                    row("IdMoneda") = gcFacturas.Rows(i).Cells("colIdMoneda").Value
                    row("ImporteSoles") = If(gcFacturas.Rows(i).Cells("colIdMoneda").Value = "02", Round(gcFacturas.Rows(i).Cells("colImporteTotal").Value * importeTipoCambio, 2, MidpointRounding.ToEven), gcFacturas.Rows(i).Cells("colImporteTotal").Value)
                    row("ImporteTotal") = Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven)

                    Dim porcentajeDetraccion As Decimal = 0
                    porcentajeDetraccion = If(importeTipoCambio = 0, Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * iValorPorcentajeDetraccion, (Round(Convert.ToDecimal(gcFacturas.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * iValorPorcentajeDetraccion) * importeTipoCambio)
                    row("PorcentajeDetraccion") = Round(porcentajeDetraccion, 2, MidpointRounding.ToEven)

                    row("ImporteDetraccion") = Round(Convert.ToDecimal(row("ImporteSoles")), 2, MidpointRounding.ToEven) - Round(Convert.ToDecimal(row("PorcentajeDetraccion")), 2, MidpointRounding.ToEven)

                    odtDetraccionDet.Rows.Add(row)
                    'stigvDetFormulaCalculo.UpdateCurrentRow()
                    gcDetraccionDetalle.DataSource = odtDetraccionDet

                    'Me.DialogResult = Windows.Forms.DialogResult.OK
                    ''Me.Close()
                End If
            Next i
            grpFacturas.Visible = False
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        grpFacturas.Visible = False
    End Sub

    Private Sub gcDetraccionDetalle_CellValueChanged(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles gcDetraccionDetalle.CellValueChanged
        gcDetraccionDetalle.EndEdit()

        If e.Column.Name.ToString() = "colPorcentaje" Then
            Dim decTotal As Decimal = 0

            decTotal = Round(Convert.ToDecimal(gcDetraccionDetalle.CurrentRow.Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) * (Round(Convert.ToDecimal(gcDetraccionDetalle.CurrentRow.Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven) / 100)
            gcDetraccionDetalle.CurrentRow.Cells("colPorcentajeDetraccion").Value = decTotal

            CalcularImporte()
        End If
    End Sub

    Private Sub btnQuitar_Click(sender As Object, e As EventArgs) Handles btnQuitar.Click
        If gcDetraccionDetalle.RowCount > 0 Then
            'Dim intFila As Integer = gvTipoGastoDestino.CurrentRow.Index
            'odtTipDestinoGasto.Rows(intFila)("IDTIPOGASTODESTINO") = -1
            gcDetraccionDetalle.Rows.Remove(gcDetraccionDetalle.CurrentRow)
            odtDetraccionDet.AcceptChanges()
        End If
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimirListado.Click
        ' '' ''Dim x As frmReporRetenciones = frmReporRetenciones.Instance
        ' '' ''x.MdiParent = frmPrincipal
        ' '' ''x.Show()
        'If gcDetraccion.RowCount > 0 Then
        '    If Convert.ToString(gcDetraccion.CurrentRow.Cells("colNumero").Value) = "" And Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 Then
        '        Dim imprimir As New RadAboutBox1()
        '        If imprimir.ShowDialog = Windows.Forms.DialogResult.OK Then
        '            Dim idDocumento = imprimir.cboDocumentos.SelectedValue.ToString()
        '            Dim serie = imprimir.cboSerie.Text

        '            Dim iResultado As Int32

        '            iResultado = eDetracciones.fActualizaSeriNumero(Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), serie, "0", 6)

        '            rptRetencion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '            '1
        '            crParameterFieldDefinitions = rptRetencion.DataDefinition.ParameterFields()
        '            crParameterFieldDefinition = crParameterFieldDefinitions("@IDRETENCIONCAB")
        '            crParameterValues = crParameterFieldDefinition.CurrentValues
        '            crParameterDiscreteValue = New ParameterDiscreteValue()
        '            crParameterDiscreteValue.Value = frmRegistroRetencion.idRegistroDocumentoCab
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '            'CrystalReportViewer1.ReportSource = rptRetencion
        '            rptRetencion.PrintToPrinter(1, False, 0, 1)

        '            CargarDetraccionCab()
        '        End If
        '    ElseIf Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 101 And Convert.ToString(gcDetraccion.CurrentRow.Cells("colNumero").Value) <> "" Then
        '        If MessageBox.Show("�Desea imprimir el documento?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
        '            Dim iResultado As Int32

        '            iResultado = eDetracciones.fActualizaSeriNumero(Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), "", "0", 10)

        '            rptRetencion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
        '            '1
        '            crParameterFieldDefinitions = rptRetencion.DataDefinition.ParameterFields()
        '            crParameterFieldDefinition = crParameterFieldDefinitions("@IDRETENCIONCAB")
        '            crParameterValues = crParameterFieldDefinition.CurrentValues
        '            crParameterDiscreteValue = New ParameterDiscreteValue()
        '            crParameterDiscreteValue.Value = Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value)
        '            crParameterValues.Add(crParameterDiscreteValue)
        '            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        '            'CrystalReportViewer1.ReportSource = rptRetencion
        '            rptRetencion.PrintToPrinter(1, False, 0, 1)
        '            CargarDetraccionCab()
        '        End If
        '    End If
        'End If
    End Sub

#End Region

#Region " Metodos Privados "

    Private Sub LimpiarControles()
        txtCodigoAnexo.Text = "" : txtRuc.Text = "" : lblNombreAnexo.Text = "" : lblDireccion.Text = "" : txtNroCtaDetraccion.Text = "" : txtNroOperacion.Text = "" : txtObservacion.Text = ""
        gcDetraccionDetalle.DataSource = Nothing
    End Sub

    Private Sub InitCombos()
        Try
            Dim odtDocumento As New DataTable
            odtDocumento = eDetracciones.fCargarDocumentos(7)
            cboDocumentoOrigen.DataSource = odtDocumento
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarDetraccionCab()
        Try
            'Dim odtData As New DataTable
            Dim VL_ANIO = Convert.ToString(cboAnio.Text)
            Dim VL_MES = Convert.ToString(cboMes.SelectedIndex + 1)

            odtDetraccion = eDetracciones.fCargarDetraccion(gEmpresa, VL_ANIO, VL_MES)
            gcDetraccion.DataSource = odtDetraccion
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarDetraccionDet()
        Try
            'Dim odtData As New DataTable
            Dim idDetraccionCab As Decimal = If(gcDetraccion.RowCount > 0, Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), 0)

            odtDetraccionDet = eDetracciones.fCargarDetraccionDetalle(idDetraccionCab)
            gcDetraccionDetalle.DataSource = odtDetraccionDet
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CalcularImporte()
        Try
            Dim decTotal As Decimal = 0

            decTotal = Round(Convert.ToDecimal(gcDetraccionDetalle.CurrentRow.Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven) - Round(Convert.ToDecimal(gcDetraccionDetalle.CurrentRow.Cells("colPorcentajeDetraccion").Value), 2, MidpointRounding.ToEven)
            gcDetraccionDetalle.CurrentRow.Cells("colImporteDetraccion").Value = decTotal
        Catch ex As Exception
            MessageBox.Show("Error al calcular el importe. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub SumarImporte()
        Try
            decImporteDetraccion = 0
            decImporteTotal = 0

            For i As Integer = 0 To gcDetraccionDetalle.RowCount - 1
                decImporteDetraccion += Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentajeDetraccion").Value), 2, MidpointRounding.ToEven)
                decImporteTotal += Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven)
            Next

        Catch ex As Exception
            MessageBox.Show("Error al sumar el importe. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

#End Region

#Region " M�todos de BD "

    Private Sub Guardar()
        Try
            Dim iResultado As Int32
            Dim idDetraccionCab As Decimal = 0

            iResultado = eDetracciones.fGrabar(0, gEmpresa, Convert.ToDecimal(cboDocumentoOrigen.SelectedValue), txtNroOperacion.Text.Trim, dtFecha.Value, txtCodigoAnexo.Text.Trim, lblNombreAnexo.Text, txtRuc.Text.Trim, txtNroCtaDetraccion.Text.Trim, lblDireccion.Text, txtObservacion.Text.Trim, 1, decImporteDetraccion, decImporteTotal, 1)
            idDetraccionCab = iResultado

            For i As Integer = 0 To gcDetraccionDetalle.RowCount - 1
                iResultado = eDetracciones.fGrabarDet(0, idDetraccionCab, Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colNumero").Value), dtFecha.Value, Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdMoneda").Value), "30", Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentajeDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven) / 100, 1)
            Next
        Catch ex As Exception
            MessageBox.Show("No se pudo grabar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Modificar()
        Try
            Dim iResultado As Int32

            iResultado = eDetracciones.fEliminar(Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), 3)

            iResultado = eDetracciones.fGrabar(Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), gEmpresa, Convert.ToDecimal(cboDocumentoOrigen.SelectedValue), txtNroOperacion.Text.Trim, dtFecha.Value, txtCodigoAnexo.Text.Trim, lblNombreAnexo.Text, txtRuc.Text.Trim, txtNroCtaDetraccion.Text.Trim, lblDireccion.Text, txtObservacion.Text.Trim, 1, decImporteDetraccion, decImporteTotal, 2)

            For i As Integer = 0 To gcDetraccionDetalle.RowCount - 1
                If Convert.ToInt32(gcDetraccionDetalle.Rows(i).Cells("colIdDetraccionDet").Value) = 0 Then
                    iResultado = eDetracciones.fGrabarDet(0, Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colNumero").Value), dtFecha.Value, Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdMoneda").Value), "30", Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentajeDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven) / 100, 1)
                Else
                    iResultado = eDetracciones.fGrabarDet(Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdDetraccionDet").Value), Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colNumero").Value), dtFecha.Value, Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdMoneda").Value), "30", Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentajeDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven) / 100, 2)
                End If
            Next

        Catch ex As Exception
            MessageBox.Show("No se pudo modificar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function Eliminar() As Boolean
        Try
            If MessageBox.Show("�Est� seguro que desea eliminar este registro?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then

                If Not ComprobarDato() Then Exit Function

                Dim iResultado As Int32
                iResultado = eDetracciones.fEliminar(Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), 3)

                If Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colID_DOCUMENTOORIGEN").Value) = 1 Then ' Orden de Compra
                    For i As Integer = 0 To gcDetraccionDetalle.RowCount - 1
                        'If Convert.ToInt32(gcRetencionDetalle.Rows(i).Cells("colIdRetencionDet").Value) = 0 Then
                        iResultado = eDetracciones.fGrabarDet(Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdDetraccionDet").Value), Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdRegistroDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colEmprCodigo").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdDocumento").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colSerie").Value), Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colNumero").Value), dtFecha.Value, Convert.ToString(gcDetraccionDetalle.Rows(i).Cells("colIdMoneda").Value), "30", Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentaje").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colPorcentajeDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteDetraccion").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteSoles").Value), 2, MidpointRounding.ToEven), Round(Convert.ToDecimal(gcDetraccionDetalle.Rows(i).Cells("colImporteTotal").Value), 2, MidpointRounding.ToEven), 0, 3)
                        'End If
                    Next
                End If

                Return True
            End If
        Catch ex As Exception
            MessageBox.Show("No se pudo eliminar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Function ComprobarDato() As Boolean
        Dim iResultado As Int32 = 0
        Dim decId As Decimal = Convert.ToDecimal(gcDetraccion.CurrentRow.Cells("colIdDetraccionCab").Value)

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("SELECT 1 FROM Tesoreria.DetraccionesCab RC INNER JOIN Tesoreria.DetraccionesDet RD ON RC.IDDETRACCIONCAB = RD.IDDETRACCIONCAB AND RD.ESTADO = '1' " & _
                             "INNER JOIN Tesoreria.DOC_PENDIENTES DP ON RD.NROITEM = DP.ID_DOCORIGEN WHERE RC.IDDETRACCIONCAB = " & decId & " AND DP.SALDO <> TOTALSINRETDET AND RC.ESTADO = '1'", CommandType.Text)

                iResultado = .fCmdExecuteScalar()
                If iResultado >= "1" Then
                    MessageBox.Show("No se puede modificar/eliminar esta orden de compra porque tiene pagos realizados. ", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                MessageBox.Show("No se pudo comprobar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            .sDesconectarSQL()
        End With

    End Function

#End Region

End Class