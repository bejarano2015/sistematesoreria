
Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions

Public Class frmMovimientoCajaBanco
    Dim WithEvents cmr As CurrencyManager

    Dim NumFila As Integer
    Dim iOpcion As Integer '18/06/2007: Modificacion Realizada
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable

    Private eMovimientoCajaBanco As clsMovimientoCajaBanco
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo


#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmMovimientoCajaBanco = Nothing
    Public Shared Function Instance() As frmMovimientoCajaBanco
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmMovimientoCajaBanco
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

    Private Sub frmTipoAlmacen_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub

#End Region

    Private Sub LimpiarControles()
        txtCodigo.Clear()
        'dtpFechaMovimiento.Clear()
        txtNombreUsuario.Clear()
        'cboCheque.Clear()
        'cboEstadoMovimiento.Clear()
        txtNumeroCuenta.Clear()
        'cboRendicion.Clear()
        'cboFormaPago.Clear()
        txtDocumentoReferencia.Clear()
        txtDocumentoOrigen.Clear()

        txtMontoSoles.Clear()
        txtMontoDolares.Clear()
        txtObservacion.Clear()
        'cboSesion.Clear()
        'cboPeriodo.Clear()
        'cboMoneda.Clear()
        'cboEmpresa.Clear()
        'cboTipoComprobante.Clear()
        'cboProveedor.Clear()
        'cboCentroCosto.Clear()
        'cboEstado.Clear()
    End Sub
    Private Sub DesabilitarControles()
        'dtpFechaMovimiento.ReadOnly = True
        txtNombreUsuario.ReadOnly = True
        cboCheque.Enabled = False
        cboEstadoMovimiento.Enabled = False
        txtNumeroCuenta.ReadOnly = True
        'cboRendicion.ReadOnly = True
        'cboFormaPago.ReadOnly = True
        txtDocumentoReferencia.ReadOnly = True
        txtDocumentoOrigen.ReadOnly = True

        txtMontoSoles.ReadOnly = True
        txtMontoDolares.ReadOnly = True
        txtObservacion.ReadOnly = True
        cboSesion.Enabled = False
        cboPeriodo.Enabled = False
        cboMoneda.Enabled = False
        cboEmpresa.Enabled = False
        cboTipoComprobante.Enabled = False
        cboProveedor.Enabled = False
        cboCentroCosto.Enabled = False
        cboEstado.Enabled = False
      
    End Sub
    Private Sub HabilitarControles()

        'dtpFechaMovimiento.ReadOnly = false
        txtNombreUsuario.ReadOnly = False
        cboCheque.Enabled = True
        cboEstadoMovimiento.Enabled = True
        txtNumeroCuenta.ReadOnly = False
        'cboRendicion.ReadOnly = False
        'cboFormaPago.ReadOnly = False
        txtDocumentoReferencia.ReadOnly = False
        txtDocumentoOrigen.ReadOnly = False

        txtMontoSoles.ReadOnly = False
        txtMontoDolares.ReadOnly = False
        txtObservacion.ReadOnly = False
        cboSesion.Enabled = True
        cboPeriodo.Enabled = True
        cboMoneda.Enabled = True
        cboEmpresa.Enabled = True
        cboTipoComprobante.Enabled = True
        cboProveedor.Enabled = True
        cboCentroCosto.Enabled = True
        cboEstado.Enabled = False

    End Sub
    Private Sub v_BuscaReg() Handles Me.BuscaReg
        If sTab = 0 Then
            'MessageBox.Show("Buscar")
        End If
    End Sub
    Private Sub v_ExportaReg() Handles Me.ExportaReg
        If sTab = 0 Then
            'MessageBox.Show("Exportar")
        End If
    End Sub
    Private Sub v_ImprimeReg() Handles Me.ImprimeReg
        If sTab = 0 Then
            'MessageBox.Show("Imprimir")
        End If
    End Sub
    Private Sub v_NuevoReg() Handles Me.NuevoReg
        If sTab = 0 Then
            iOpcion = 1 '18/06/2007: Modificacion Realizada
            sTab = 1
            LimpiarControles()
            HabilitarControles()
            Timer1.Enabled = True
        End If

    End Sub
    Private Sub v_VisualizaReg() Handles Me.VisualizaReg
        If sTab = 0 Then
            sTab = 1
            DesabilitarControles()
        End If
    End Sub
    Private Sub v_ModificaReg() Handles Me.ModificaReg
        If sTab = 0 Then
            sTab = 1
            iOpcion = 2 '18/06/2007: Modificacion Realizada
            VerPosicion()
            HabilitarControles()
            cboEstado.Enabled = True
            Timer1.Enabled = True
        End If
    End Sub
    Private Sub v_CancelarReg() Handles Me.CancelarReg
        If sTab = 1 Then
            sTab = 0
            TabControl1.TabPages(0).Focus()
            Timer2.Enabled = True
        End If
    End Sub
    Private Sub v_SalidaReg() Handles Me.SalidaReg
        Close()
    End Sub

    Private Sub v_GrabaReg() Handles Me.GrabaReg
        'F: Transaccion

        Dim iResultado As Int32
        Dim iDuplicado As Int32 'Modificado el 05/06
        Dim sCodigoActual As String = ""
        eTempo = New clsPlantTempo
        eMovimientoCajaBanco = New clsMovimientoCajaBanco 'Movido el 05/06

        If sTab = 1 Then

            If Me.dtpFechaMovimiento.Text.Trim.Length = 0 Or Me.txtNombreUsuario.Text.Trim.Length = 0 Or Me.cboCheque.Text.Trim.Length = 0 Or Me.cboEstadoMovimiento.Text.Trim.Length = 0 Or Me.txtNumeroCuenta.Text.Trim.Length = 0 Or Me.cboRendicion.Text.Trim.Length = 0 Or Me.cboFormaPago.Text.Trim.Length = 0 Or Me.txtDocumentoReferencia.Text.Trim.Length = 0 Or Me.txtDocumentoOrigen.Text.Trim.Length = 0 Or Me.txtMontoSoles.Text.Trim.Length = 0 Or Me.txtMontoDolares.Text.Trim.Length = 0 Or Me.txtObservacion.Text.Trim.Length = 0 Or Me.cboSesion.Text.Trim.Length = 0 Or Me.cboPeriodo.Text.Trim.Length = 0 Or Me.cboMoneda.Text.Trim.Length = 0 Or Me.cboEmpresa.Text.Trim.Length = 0 Or Me.cboTipoComprobante.Text.Trim.Length = 0 Or Me.cboProveedor.Text.Trim.Length = 0 Or Me.cboCentroCosto.Text.Trim.Length = 0 Or Me.cboEstado.Text.Trim.Length = 0 Then

                eTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgIncompleto))
                frmPrincipal.sFlagGrabar = "0"
            Else

                iDuplicado = eMovimientoCajaBanco.fBuscarDoble(Convert.ToString(Me.dtpFechaMovimiento.Text.Trim), Convert.ToString(Me.txtCodigo.Text.Trim)) 'Modificado el 05/06
                If iDuplicado > 0 Then 'Modificado el 05/06
                    Exit Sub 'Modificado el 05/06
                End If 'Modificado el 05/06

                If (MessageBox.Show("�Esta seguro de Grabar ahora el registro en proceso?", "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                    frmPrincipal.sFlagGrabar = "1"
                    If iOpcion = 1 Then '18/06/2007: Modificacion Realizada
                        'eMovimientoCajaBanco.fCodigo()
                        sCodigoActual = eMovimientoCajaBanco.sCodFuturo
                    Else
                        sCodigoActual = Me.txtCodigo.Text
                    End If
                    'iResultado = 'eMovimientoCajaBanco.fGrabar(sCodigoActual, Convert.ToString(Me.dtpFechaMovimiento.Text.Trim), Convert.ToString(Me.txtNombreUsuario.Text.Trim), Convert.ToString(Me.cboCheque.SelectedValue), Convert.ToString(Me.cboEstadoMovimiento.SelectedValue), Convert.ToString(Me.txtNumeroCuenta.Text.Trim), Convert.ToString(Me.cboRendicion.SelectedValue), Convert.ToString(Me.cboFormaPago.SelectedValue), Convert.ToString(Me.txtDocumentoReferencia.Text.Trim), Convert.ToString(Me.txtDocumentoOrigen.Text.Trim), Convert.ToString(Me.txtMontoSoles.Text.Trim), Convert.ToString(Me.txtMontoDolares.Text.Trim), Convert.ToString(Me.txtObservacion.Text.Trim), Convert.ToString(Me.cboSesion.SelectedValue), Convert.ToString(Me.cboPeriodo.SelectedValue), Convert.ToString(Me.cboMoneda.SelectedValue), Convert.ToString(Me.cboEmpresa.SelectedValue), Convert.ToString(Me.cboTipoComprobante.SelectedValue), Convert.ToString(Me.cboProveedor.SelectedValue), Convert.ToString(Me.cboCentroCosto.SelectedValue), Convert.ToString(Me.cboEstado.SelectedValue), iOpcion) '18/06/2007: Modificacion   

                    If iResultado > 0 Then
                        mMostrarGrilla()
                        Timer2.Enabled = True
                    End If
                    sTab = 0
                End If

            End If
        End If
    End Sub

    Private Sub v_EliminaReg() Handles Me.EliminaReg
        'F: Transaccion

        Dim iResultado As Int32
        Dim Mensaje As String
        Dim iActivo As Integer '18/06/2007: Modificacion Realizada

        If sTab = 0 Then
            Try
                If Me.dgvLista.Rows.Count > 0 Then

                    'If Fila("Estados") = "ACTIVO" Then
                    '    iActivo = 5 '18/06/2007: Modificacion Realizada
                    '    Mensaje = "Desea ANULAR: "
                    'Else
                    iActivo = 3 '18/06/2007: Modificacion Realizada
                    Mensaje = "� Desea Eliminar ?"
                    'End If

                    If (MessageBox.Show(Mensaje & Chr(13) & Chr(13) & "Codigo: " & Fila("IdMovimiento") & "   " & Chr(13) & "Nombre de Usuario: " & Fila("NombreUsuario"), "Sistema de Tesoreria", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                        'Using scope As TransactionScope = New TransactionScope '(MSDTC)
                        eMovimientoCajaBanco = New clsMovimientoCajaBanco
                        iResultado = eMovimientoCajaBanco.fEliminar(Fila("IdMovimiento"), iActivo) '18/06/2007: Modificacion Realizada
                        'scope.Complete()
                        'End Using
                        If iResultado = 1 Then
                            mMostrarGrilla()
                        End If
                    Else
                        Exit Sub
                    End If

                End If
            Catch ex As Exception

            End Try
        End If
    End Sub


    Private Sub frmLinea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mMostrarGrilla()
        dtpFechaMovimiento.Value = Now.Date()
        frmPrincipal.fBuscar = False 'Desabilita los botones de buscar
        frmPrincipal.fExportar = False 'Desabilita los botones de exportar
        frmPrincipal.fImprimir = False 'Desabilita los botones de imprimir
    End Sub

    Private Sub mMostrarGrilla()
        eMovimientoCajaBanco = New clsMovimientoCajaBanco
        eTempo = New clsPlantTempo
        dtTable = New DataTable
        dtTable = eMovimientoCajaBanco.fListar(iEstado01, iEstado02)
        Me.dgvLista.AutoGenerateColumns = False
        Me.dgvLista.DataSource = dtTable
        cmr = Me.BindingContext(Me.dgvLista.DataSource)
        VerPosicion()
        Me.stsTotales.Items(0).Text = "Total de Registros= " & eMovimientoCajaBanco.iNroRegistros
        If eMovimientoCajaBanco.iNroRegistros > 0 Then
            frmPrincipal.fDatos = True
        Else
            frmPrincipal.Modifica.Enabled = False
            frmPrincipal.Elimina.Enabled = False
            frmPrincipal.Visualiza.Enabled = False
            frmPrincipal.fDatos = False
        End If

        Me.cboEstado.DataSource = eTempo.fColEstado
        Me.cboEstado.ValueMember = "Col02"
        Me.cboEstado.DisplayMember = "Col01"

        Me.cboCheque.DataSource = eMovimientoCajaBanco.fListarCheque
        Me.cboCheque.ValueMember = "IdCheque"
        Me.cboCheque.DisplayMember = "NumeroCheque"

        Me.cboEstadoMovimiento.DataSource = eMovimientoCajaBanco.fListarEstadoMovimiento
        Me.cboEstadoMovimiento.ValueMember = "IdEstadoMovimiento"
        Me.cboEstadoMovimiento.DisplayMember = "DescripcionMovimiento"

        Me.cboRendicion.DataSource = eMovimientoCajaBanco.fListarTipoMovimiento
        Me.cboRendicion.ValueMember = "IdRendicion"
        Me.cboRendicion.DisplayMember = "DescripcionRendicion"

        Me.cboFormaPago.DataSource = eMovimientoCajaBanco.fListarCondicionPago
        Me.cboFormaPago.ValueMember = "IdFormaPago"
        Me.cboFormaPago.DisplayMember = "DescripcionFormaPago"

        Me.cboSesion.DataSource = eMovimientoCajaBanco.fListarSesionCaja
        Me.cboSesion.ValueMember = "IdSesion"
        Me.cboSesion.DisplayMember = "Descripcion"

        Me.cboPeriodo.DataSource = eMovimientoCajaBanco.fListarPeriodo
        Me.cboPeriodo.ValueMember = "IdPeriodo"
        Me.cboPeriodo.DisplayMember = "DescripcionPeriodo"

        Me.cboMoneda.DataSource = eMovimientoCajaBanco.fListarMoneda
        Me.cboMoneda.ValueMember = "MonCodigo"
        Me.cboMoneda.DisplayMember = "MonDescripcion"

        Me.cboEmpresa.DataSource = eMovimientoCajaBanco.fListarEmpresa
        Me.cboEmpresa.ValueMember = "EmprCodigo"
        Me.cboEmpresa.DisplayMember = "EmprDescripcion"

        Me.cboTipoComprobante.DataSource = eMovimientoCajaBanco.fListarTipoComprobante
        Me.cboTipoComprobante.ValueMember = "TCompCodigo"
        Me.cboTipoComprobante.DisplayMember = "TCompDescripcion"

        Me.cboProveedor.DataSource = eMovimientoCajaBanco.fListarProveedor
        Me.cboProveedor.ValueMember = "PrvCodigo"
        Me.cboProveedor.DisplayMember = "PrvRazonSocial"

        Me.cboCentroCosto.DataSource = eMovimientoCajaBanco.fListarCCosto
        Me.cboCentroCosto.ValueMember = "CcosCodigo"
        Me.cboCentroCosto.DisplayMember = "CcosDescripcion"

    End Sub


    Private Sub VerPosicion()
        Try
            NumFila = cmr.Position
            CargarFila(NumFila)
        Catch ex As Exception

        End Try
    End Sub

    Sub CargarFila(ByVal NumFila As Long)
        Try
            If Me.dtTable.Rows.Count > 0 Then
                Fila = dtTable.Rows(NumFila)
                Me.txtCodigo.Text = Fila("IdMovimiento").ToString
                Me.dtpFechaMovimiento.Text = Fila("FechaMovimiento").ToString
                Me.txtNombreUsuario.Text = Fila("NombreUsuario").ToString
                Me.cboCheque.Text = Fila("IdCheque").ToString
                Me.cboEstadoMovimiento.Text = Fila("IdEstadoMovimiento").ToString
                Me.txtNumeroCuenta.Text = Fila("NumeroCuenta").ToString
                Me.cboRendicion.Text = Fila("IdRendicion").ToString
                Me.cboFormaPago.Text = Fila("IdFormaPago").ToString
                Me.txtDocumentoReferencia.Text = Fila("DocumentoReferencia").ToString
                Me.txtDocumentoOrigen.Text = Fila("DocumentoOrigen").ToString
                Me.txtMontoSoles.Text = Fila("MontoSoles").ToString
                Me.txtMontoDolares.Text = Fila("MontoDolares").ToString
                Me.txtObservacion.Text = Fila("Observacion").ToString
                Me.cboSesion.Text = Fila("IdSesion").ToString
                Me.cboPeriodo.Text = Fila("IdPeriodo").ToString
                Me.cboMoneda.Text = Fila("MonCodigo").ToString
                Me.cboEmpresa.Text = Fila("EmprCodigo").ToString
                Me.cboTipoComprobante.Text = Fila("TCompCodigo").ToString
                Me.cboProveedor.Text = Fila("PrvCodigo").ToString
                Me.cboCentroCosto.Text = Fila("CCosCodigo").ToString
                Me.cboEstado.Text = Fila("EdocCodigo").ToString

            End If
        Catch ex As System.IndexOutOfRangeException
            Exit Sub
        End Try
    End Sub

    Private Sub dgvLista_CurrentCellChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvLista.CurrentCellChanged
        Try
            VerPosicion()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.txtNombreUsuario.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Me.dgvLista.Focus()
        Timer2.Enabled = False
    End Sub

    Private Sub cboPrestamo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPrestamo.Click

    End Sub

    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click

    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class
