Imports CapaNegocios
Imports CapaDatos
Imports System.Data.SqlClient
Imports System.Transactions
Imports vb = Microsoft.VisualBasic
Imports IWshRuntimeLibrary


Public Class frmRegistroDocumentoEstado

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmRegistroDocumentoEstado = Nothing
    Public Shared Function Instance() As frmRegistroDocumentoEstado
        If frmInstance Is Nothing Then 'OrElse frmBase.IsDisposed = True Then
            frmInstance = New frmRegistroDocumentoEstado
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmRegistroDocumentoEstado_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eRegistroDocumento As clsRegistroDocumento
    Private eTempo As clsPlantTempo

    Private Sub frmRegistroDocumentoEstado_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rdbDescr.Checked = False
        rdbRuc.Checked = False
        rdbNumDoc.Checked = True
        txtFiltrar.Clear()
        txtFiltrar_TextChanged(sender, e)
        txtFiltrar.Focus()
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Timer1.Enabled = False
        If Timer1.Enabled = True Then
            txtFiltrar.Focus()
            rdbNumDoc.Checked = True
            'BeButton4_Click(sender, e)
        End If
        Timer1.Enabled = False
    End Sub

    'Private Sub txtFiltrar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFiltrar.TextChanged
    '    eRegistroDocumento = New clsRegistroDocumento
    '    eTempo = New clsPlantTempo
    '    Dim dtTableLista As DataTable
    '    dtTableLista = New DataTable
    '    If Len(Trim(txtFiltrar.Text)) > 0 Then
    '        If Trim(glbUsuCategoria) = "A" Then
    '            If rdbDescr.Checked = True Then
    '                dtTableLista = eRegistroDocumento.fListar(52, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
    '            End If
    '            If rdbRuc.Checked = True Then
    '                dtTableLista = eRegistroDocumento.fListar(53, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
    '            End If
    '            If rdbNumDoc.Checked = True Then
    '                dtTableLista = eRegistroDocumento.fListar(54, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
    '            End If
    '        ElseIf Trim(glbUsuCategoria) = "U" Then
    '            If rdbDescr.Checked = True Then
    '                dtTableLista = eRegistroDocumento.fListar(56, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
    '            End If
    '            If rdbRuc.Checked = True Then
    '                dtTableLista = eRegistroDocumento.fListar(57, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
    '            End If
    '            If rdbNumDoc.Checked = True Then
    '                dtTableLista = eRegistroDocumento.fListar(58, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
    '            End If
    '        End If
    '    ElseIf Len(Trim(txtFiltrar.Text)) = 0 Then
    '        If Trim(glbUsuCategoria) = "A" Then
    '            dtTableLista = eRegistroDocumento.fListar(51, gEmpresa, Trim(gUsuario), gPeriodo, "")
    '        ElseIf Trim(glbUsuCategoria) = "U" Then
    '            dtTableLista = eRegistroDocumento.fListar(55, gEmpresa, Trim(gUsuario), gPeriodo, "")
    '        End If
    '    End If
    '    dvgListDocs.AutoGenerateColumns = False
    '    'If dtTableLista.Rows.Count > 0 Then
    '    dvgListDocs.DataSource = dtTableLista
    '    stsTotales.Items(0).Text = "Total de Registros= " & eRegistroDocumento.iNroRegistros
    '    'ElseIf dtTableLista.Rows.Count = 0 Then
    '    'dvgListDocs.DataSource = dtTableLista
    '    'End If

    'End Sub

    Private Sub txtFiltrar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFiltrar.TextChanged
        eRegistroDocumento = New clsRegistroDocumento
        eTempo = New clsPlantTempo
        Dim dtTableLista As DataTable
        dtTableLista = New DataTable
        If Len(Trim(txtFiltrar.Text)) > 0 Then
            If Trim(glbUsuCategoria) = "A" Then
                If rdbDescr.Checked = True Then
                    dtTableLista = eRegistroDocumento.fListar(52, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
                End If
                If rdbRuc.Checked = True Then
                    dtTableLista = eRegistroDocumento.fListar(53, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
                End If
                If rdbNumDoc.Checked = True Then
                    dtTableLista = eRegistroDocumento.fListar(54, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
                End If
            ElseIf Trim(glbUsuCategoria) = "U" Then
                If rdbDescr.Checked = True Then
                    dtTableLista = eRegistroDocumento.fListar(56, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
                End If
                If rdbRuc.Checked = True Then
                    dtTableLista = eRegistroDocumento.fListar(57, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
                End If
                If rdbNumDoc.Checked = True Then
                    dtTableLista = eRegistroDocumento.fListar(58, gEmpresa, Trim(gUsuario), gPeriodo, Trim(txtFiltrar.Text))
                End If
            End If
        ElseIf Len(Trim(txtFiltrar.Text)) = 0 Then
            If Trim(glbUsuCategoria) = "A" Then
                dtTableLista = eRegistroDocumento.fListar(51, gEmpresa, Trim(gUsuario), gPeriodo, "")
            ElseIf Trim(glbUsuCategoria) = "U" Then
                dtTableLista = eRegistroDocumento.fListar(55, gEmpresa, Trim(gUsuario), gPeriodo, "")
            End If
        End If
        dvgListDocs.AutoGenerateColumns = False
        'If dtTableLista.Rows.Count > 0 Then
        dvgListDocs.DataSource = dtTableLista
        stsTotales.Items(0).Text = "Total de Registros= " & eRegistroDocumento.iNroRegistros
        'ElseIf dtTableLista.Rows.Count = 0 Then
        'dvgListDocs.DataSource = dtTableLista
        'End If
    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        Dim ContaSeleccionados As Integer = 0

        If dvgListDocs.Rows.Count > 0 Then

            For z As Integer = 0 To dvgListDocs.Rows.Count - 1
                If Convert.ToBoolean(dvgListDocs.Rows(z).Cells("Cancelar").Value) = True Or Convert.ToBoolean(dvgListDocs.Rows(z).Cells("Archivar").Value) = True Then
                    ContaSeleccionados = ContaSeleccionados + 1
                End If
            Next

            Dim mensaje As String = ""
            If ContaSeleccionados = 1 Then
                mensaje = "el Comprobante Seleccionado?"
            ElseIf ContaSeleccionados > 1 Then
                mensaje = "los Comprobantes Seleccionados?"
            ElseIf ContaSeleccionados = 0 Then
                MessageBox.Show("Seleccione Comprobantes para Cancelar/Archivar", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
            If (MessageBox.Show("�Est� seguro de Cancelar/Archivar " & mensaje, "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
                For x As Integer = 0 To dvgListDocs.Rows.Count - 1
                    If Convert.ToBoolean(dvgListDocs.Rows(x).Cells("Cancelar").Value) = True Then
                        Dim iResultado As Integer = 0
                        Dim sCodRegistro As String = ""
                        sCodRegistro = Trim(dvgListDocs.Rows(x).Cells("IdRegistro").Value)
                        eRegistroDocumento = New clsRegistroDocumento
                        iResultado = eRegistroDocumento.fCancelarFactura(45, Trim(sCodRegistro), gEmpresa, "00004", 1, 1, gUsuario, 1)
                    ElseIf Convert.ToBoolean(dvgListDocs.Rows(x).Cells("Cancelar").Value) = False And Convert.ToBoolean(dvgListDocs.Rows(x).Cells("Archivar").Value) = True Then
                        Dim iResultado As Integer = 0
                        Dim sCodRegistro As String = ""
                        sCodRegistro = Trim(dvgListDocs.Rows(x).Cells("IdRegistro").Value)
                        eRegistroDocumento = New clsRegistroDocumento
                        iResultado = eRegistroDocumento.fCancelarFactura(45, Trim(sCodRegistro), gEmpresa, "00001", 0, 0, gUsuario, 0)
                    End If
                Next
                If ContaSeleccionados = 1 Then
                    MessageBox.Show("Se Cancel�/Archiv� " & ContaSeleccionados & " Comprobante", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtFiltrar_TextChanged(sender, e)
                ElseIf ContaSeleccionados > 1 Then
                    MessageBox.Show("Se Cancel�/Archiv� " & ContaSeleccionados & " Comprobantes", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    txtFiltrar_TextChanged(sender, e)
                ElseIf ContaSeleccionados = 0 Then
                    MessageBox.Show("Seleccione Registros para Anular", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If
    End Sub

    Private Sub rdbRuc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbRuc.CheckedChanged

    End Sub

    Private Sub rdbRuc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbRuc.Click
        rdbRuc.Checked = True
        rdbDescr.Checked = False
        rdbNumDoc.Checked = False

        txtFiltrar.MaxLength = 11
        txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        txtFiltrar.Text = ""
        txtFiltrar.Focus()
        txtFiltrar_TextChanged(sender, e)
    End Sub

    Private Sub rdbNumDoc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbNumDoc.CheckedChanged
        
    End Sub

    Private Sub rdbDescr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbDescr.CheckedChanged

    End Sub

    Private Sub rdbDescr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbDescr.Click
        rdbRuc.Checked = False
        rdbDescr.Checked = True
        rdbNumDoc.Checked = False

        txtFiltrar.MaxLength = 150
        txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfan�merico
        txtFiltrar.Text = ""
        txtFiltrar.Focus()
        txtFiltrar_TextChanged(sender, e)
    End Sub

    Private Sub rdbNumDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbNumDoc.Click
        rdbRuc.Checked = False
        rdbDescr.Checked = False
        rdbNumDoc.Checked = True

        txtFiltrar.MaxLength = 20
        txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.N�merico
        txtFiltrar.Text = ""
        txtFiltrar.Focus()
        txtFiltrar_TextChanged(sender, e)
    End Sub
End Class