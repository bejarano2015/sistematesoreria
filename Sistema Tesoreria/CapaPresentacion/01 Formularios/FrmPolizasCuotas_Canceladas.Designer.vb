<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPolizasCuotas_Canceladas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPolizasCuotas_Canceladas))
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.cboCia = New ctrLibreria.Controles.BeComboBox
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.btnSalir = New System.Windows.Forms.Button
        Me.mtbFechaFin = New ctrLibreria.Controles.BeMaskedTextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.mtbFechaIni = New ctrLibreria.Controles.BeMaskedTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(21, 16)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(67, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Aseguradora"
        '
        'cboCia
        '
        Me.cboCia.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCia.BackColor = System.Drawing.Color.Ivory
        Me.cboCia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCia.ForeColor = System.Drawing.Color.Black
        Me.cboCia.FormattingEnabled = True
        Me.cboCia.KeyEnter = True
        Me.cboCia.Location = New System.Drawing.Point(89, 13)
        Me.cboCia.Name = "cboCia"
        Me.cboCia.Size = New System.Drawing.Size(177, 21)
        Me.cboCia.TabIndex = 1
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnImprimir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Image = CType(resources.GetObject("btnImprimir.Image"), System.Drawing.Image)
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(359, 12)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(73, 25)
        Me.btnImprimir.TabIndex = 9
        Me.btnImprimir.Text = "  &Imprimir"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalir.Location = New System.Drawing.Point(359, 42)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(73, 25)
        Me.btnSalir.TabIndex = 11
        Me.btnSalir.Text = "  &Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'mtbFechaFin
        '
        Me.mtbFechaFin.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaFin.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaFin.BeepOnError = True
        Me.mtbFechaFin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaFin.HidePromptOnLeave = True
        Me.mtbFechaFin.KeyEnter = True
        Me.mtbFechaFin.Location = New System.Drawing.Point(256, 46)
        Me.mtbFechaFin.Mask = "00/00/0000"
        Me.mtbFechaFin.Name = "mtbFechaFin"
        Me.mtbFechaFin.Size = New System.Drawing.Size(65, 20)
        Me.mtbFechaFin.TabIndex = 8
        Me.mtbFechaFin.ValidatingType = GetType(Date)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(191, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Fecha Final"
        '
        'mtbFechaIni
        '
        Me.mtbFechaIni.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaIni.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaIni.BeepOnError = True
        Me.mtbFechaIni.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaIni.HidePromptOnLeave = True
        Me.mtbFechaIni.KeyEnter = True
        Me.mtbFechaIni.Location = New System.Drawing.Point(96, 46)
        Me.mtbFechaIni.Mask = "00/00/0000"
        Me.mtbFechaIni.Name = "mtbFechaIni"
        Me.mtbFechaIni.Size = New System.Drawing.Size(65, 20)
        Me.mtbFechaIni.TabIndex = 6
        Me.mtbFechaIni.ValidatingType = GetType(Date)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Fecha Inicio"
        '
        'FrmPolizasCuotas_Canceladas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(446, 80)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.mtbFechaFin)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.mtbFechaIni)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboCia)
        Me.Controls.Add(Me.BeLabel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "FrmPolizasCuotas_Canceladas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Polizas - Cuotas  Canceladas"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCia As ctrLibreria.Controles.BeComboBox
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents mtbFechaFin As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents mtbFechaIni As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
