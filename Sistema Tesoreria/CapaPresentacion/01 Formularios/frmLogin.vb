Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports System.Math

Imports System.Deployment.Application
Public Class frmLogin
    Dim WithEvents cmr As CurrencyManager
    Dim NumFila As Integer
    Dim iOpcion As Integer
    Dim sTab As String = 0
    Dim Fila As DataRow
    Dim dtTable As DataTable
    Private eLogin As clsLogin
    Private ePagoProveedores As clsPagoProveedores
    Private eSeries As clsSeries
    Private eITF As clsITF
    Private eFiltro As clsPlantFiltro
    Private eTempo As clsPlantTempo

    Public swc As Boolean
    Dim Contador As Integer = 0

    Private Sub frmLogin_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If swc = False Then
            End
        End If
    End Sub

    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '''''''''''''tipo cambio del dia

        Try
            If ApplicationDeployment.IsNetworkDeployed Then
                With ApplicationDeployment.CurrentDeployment.CurrentVersion
                    'versionSistema = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString
                    versionSistema = "Versi�n: " & .Major.ToString & "." & .Minor.ToString & "." & .Build.ToString & "." & .Revision.ToString
                End With
            End If
        Catch ex As Exception
        End Try
        Dim dtTable As DataTable
        dtTable = New DataTable
        ePagoProveedores = New clsPagoProveedores
        dtTable = ePagoProveedores.fListarParametroIGV(Today())
        If dtTable.Rows.Count > 0 Then
            gIgv = Val(dtTable.Rows(0).Item("TcaVenta"))
            gIgvConvertido = Format(gIgv / 100, "0.00")
        Else
            gIgv = 0
            gIgvConvertido = 0
        End If

        eLogin = New clsLogin
        cboEmpresa.DataSource = eLogin.fListarEmpresas()
        If eLogin.iNroRegistros > 0 Then
            cboEmpresa.ValueMember = "EmprCodigo"
            cboEmpresa.DisplayMember = "Descripcion"
            cboEmpresa.SelectedIndex = 0
            'txtUsuario.Focus()
        End If

        cboAno.Text = Year(Now)
        txtUsuario.Text = ""
        txtPassword.Text = ""
        Timer1.Enabled = True
        Contador = 0

        'If eLogin.iNroRegistros > 0 Then
        '    txtUsuario.Focus()
        'End If

    End Sub

    Private Sub btnIngresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIngresar.Click
        'Dim ds As DataSet
        'ds = New DataSet

        'Dim TabDet As New DataTable
        'Dim dr1 As DataRow

        If Trim(cboEmpresa.Text) = "" Then
            MsgBox("Seleccione una Empresa!", MsgBoxStyle.Information, glbNameSistema)
            cboEmpresa.Focus()
            Exit Sub
        End If

        If txtUsuario.Text = "" Or txtPassword.Text = "" Then
            If txtUsuario.Text = "" Then
                MsgBox("Ingrese C�digo de Usuario!", MsgBoxStyle.Information, glbNameSistema)
                txtUsuario.Focus()
            ElseIf txtPassword.Text = "" Then
                MsgBox("Ingrese Password de Usuario!", MsgBoxStyle.Information, glbNameSistema)
                txtPassword.Focus()
            End If
            Exit Sub
        End If


        '///// Validando codigo de Usuario  ///////
        'If Not Contador = 3 Then
        eLogin = New clsLogin
        Dim dtDatos As DataTable
        dtDatos = New DataTable
        dtDatos = eLogin.fBuscarUsuario(Trim(cboEmpresa.SelectedValue), glbSisCodigo, Trim(txtUsuario.Text))
        'ds = ObjCli.BuscaUsuario(Me.cboEmpresa.Codigo, glbSisCodigo, Trim(Me.txtUsuario.Text))
        'TabDet = ds.Tables(0)
        If dtDatos.Rows.Count > 0 Then
            'dr1 = TabDet.Rows(0)
            If Not IsDBNull(dtDatos.Rows(0).Item("UsuPwd")) Then
                If UCase(Trim(txtPassword.Text)) = UCase(Trim(dtDatos.Rows(0).Item("UsuPwd"))) Then
                    'Nombre=jesus
                    'CType(Me.Owner, frmPrincipal).StatusStrip.Items(1).Text = Mid(Me.cboEmpresa.Text, 1, 40)   'My.Computer.Name
                    'CType(Me.Owner, frmPrincipal).StatusStrip.Items(2).Text = Mid(Me.cboEmpresa.Text, (Len(Me.cboEmpresa.Text) - 11) + 1, 11)
                    'CType(Me.Owner, frmPrincipal).StatusStrip.Items(3).Text = Trim(cboAno.Text)
                    'CType(Me.Owner, frmPrincipal).StatusStrip.Items(4).Text = Trim(txtUsuario.Text)
                    'frmPrincipal.toFecha.Text=
                    'toEmpresa
                    'toUsuario
                    'toPeriodo
                    'toPC
                    'toFecha
                    '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    'Cargando los parametros globales de sistema
                    gEmpresa = Trim(cboEmpresa.SelectedValue)   '"01"
                    gDesEmpresa = Trim(Mid(cboEmpresa.Text, 1, 40)) '"CONSORCIO DHMONT"
                    gEmprRuc = Trim(Mid(cboEmpresa.Text, (Len(cboEmpresa.Text) - 11) + 1, 11)) '"RUC DE CONSORCIO DHMONT"
                    gPeriodo = Trim(cboAno.Text)   '"2009" 'A�o seleccionado al inicio del sistema

                    glbNameSistema = "Sistema de Tesorer�a" 'Nombre Sistema
                    glbSisCodigo = "03" 'Codigo de sistema

                    gUsuario = Trim(txtUsuario.Text)
                    glbNameUsuario = Trim(dtDatos.Rows(0).Item("UsuNombre"))
                    glbUsuCategoria = Trim(dtDatos.Rows(0).Item("Usucategoria"))


                    gPC = My.Computer.Name
                    gFecha = Now.Date().ToShortDateString

                    'Call CargandoParametrosGlobales()
                    '&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
                    '''''''''''''serie retencion para dia x empresa
                    Dim dtTableSerieRete As DataTable
                    dtTableSerieRete = New DataTable
                    eSeries = New clsSeries
                    dtTableSerieRete = eSeries.fListar(9, gEmpresa, "")
                    If dtTableSerieRete.Rows.Count > 0 Then
                        gSerie = Trim(dtTableSerieRete.Rows(0).Item("Serie"))
                        gNumeroInicioSerie = Trim(dtTableSerieRete.Rows(0).Item("NumeroIni"))
                        'gIgvConvertido = Format(gIgv / 100, "0.00")
                    Else
                        gSerie = ""
                    End If


                    'Dim dtImpuestosxDocumentos As DataTable
                    'dtImpuestosxDocumentos = New DataTable
                    'eLogin = New clsLogin
                    'dtImpuestosxDocumentos = eLogin.fBuscarImpuestosxDocumentos(0, 2)
                    'If dtImpuestosxDocumentos.Rows.Count > 0 Then
                    '    iValorPorcentaje = Round(Convert.ToDecimal(dtImpuestosxDocumentos.Rows(0).Item("VALOR_PORCENTAJE")), 2, MidpointRounding.ToEven) / 100
                    'Else
                    '    iValorPorcentaje = 0
                    'End If


                    'Dim dtDoble As DataTable
                    'dtDoble = New DataTable
                    'eITF = New clsITF
                    'dtDoble = eITF.fBuscarDoble(Trim(gPeriodo), Today())
                    'If dtDoble.Rows.Count > 0 Then
                    '    PorcentajeITF = dtDoble.Rows(0).Item("Porcentaje")
                    'End If

                    Dim eRetenciones As New clsRetencion
                    Dim iResultado As Decimal = 0

                    iResultado = eRetenciones.fImporteTipoCambio(Date.Today, 13)
                    iValorTipoCambio = iResultado

                    Me.swc = True
                    Me.Close()

                Else 'No coincide el pwd
                    Contador = Contador + 1
                    If Contador = 3 Then
                        MsgBox("Usuario y/o Clave Incorrecto. Ud Saldr� del Sistema", MsgBoxStyle.Exclamation, glbNameSistema)
                        Application.Exit()
                    End If
                    MsgBox("Password Incorrecto!", MsgBoxStyle.Critical, glbNameSistema)
                End If
            Else 'no tiene pwd
                Contador = Contador + 1
                If Contador = 3 Then
                    MsgBox("Usuario y/o Clave Incorrecto. Ud Saldr� del Sistema", MsgBoxStyle.Exclamation, glbNameSistema)
                    Application.Exit()
                End If
                MsgBox("Password Incorrecto!", MsgBoxStyle.Critical, glbNameSistema)
            End If
        Else
            Contador = Contador + 1
            If Contador = 3 Then
                MsgBox("Usuario y/o Clave Incorrecto. Ud Saldr� del Sistema", MsgBoxStyle.Exclamation, glbNameSistema)
                Application.Exit()
            End If
            MsgBox("Usuario Incorrecto!", MsgBoxStyle.Information, glbNameSistema)
        End If
        'TabDet.Dispose()
        'ds.Dispose()
        'Else
        '    MsgBox("Usuario y/o Clave Incorrecto. Ud Saldr� del Sistema", MsgBoxStyle.Exclamation, glbNameSistema)
        '    Contador = Contador + 1
        '    If Contador = 3 Then Application.Exit()
        '    'Application.Exit()
        'End If
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        'Close()
        'If (MessageBox.Show("�Esta seguro de Salir del Sistema?", "Sistema de Tesorer�a", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes) Then
        'swc = False
        Application.Exit()
        'Close()
        'Else
        'Exit Sub
        'End If
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        cboEmpresa.Focus()
        Timer1.Enabled = False
    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        Select Case Asc(e.KeyChar)
            Case 13
                btnIngresar_Click(sender, e)
        End Select
    End Sub

End Class