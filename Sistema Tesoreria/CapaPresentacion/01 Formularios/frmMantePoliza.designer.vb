<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMantePoliza
    Inherits CapaPreTesoreria.frmPadre00

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Dise�ador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Dise�ador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Dise�ador de Windows Forms.  
    'No lo modifique con el editor de c�digo.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMantePoliza))
        Me.lblTotalRegistro = New System.Windows.Forms.Label
        Me.label = New System.Windows.Forms.Label
        Me.DgvLista = New ctrLibreria.Controles.BeDataGridView
        Me.txtFiltro = New ctrLibreria.Controles.BeTextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnAgregar = New System.Windows.Forms.Button
        Me.cboRamo = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel15 = New ctrLibreria.Controles.BeLabel
        Me.gbOtros = New System.Windows.Forms.GroupBox
        Me.btnNuevoProv = New System.Windows.Forms.Button
        Me.cboAseguradora = New ctrLibreria.Controles.BeComboBox
        Me.mtbFechaLetraIni = New ctrLibreria.Controles.BeMaskedTextBox
        Me.BeLabel18 = New ctrLibreria.Controles.BeLabel
        Me.cboFormaPago = New ctrLibreria.Controles.BeComboBox
        Me.txtLetras = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel13 = New ctrLibreria.Controles.BeLabel
        Me.txtSumaAseguradora = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel14 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel16 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel17 = New ctrLibreria.Controles.BeLabel
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox
        Me.BeLabel11 = New ctrLibreria.Controles.BeLabel
        Me.gbConceptos = New System.Windows.Forms.GroupBox
        Me.txtTotal = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel12 = New ctrLibreria.Controles.BeLabel
        Me.txtIGV = New ctrLibreria.Controles.BeTextBox
        Me.txtInteres = New ctrLibreria.Controles.BeTextBox
        Me.txtDerechoEmision = New ctrLibreria.Controles.BeTextBox
        Me.txtPrima = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel
        Me.gbVigencia = New System.Windows.Forms.GroupBox
        Me.mtbVigenciaFin = New ctrLibreria.Controles.BeMaskedTextBox
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel
        Me.mtbVigenciaIni = New ctrLibreria.Controles.BeMaskedTextBox
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel
        Me.txtDireccion = New ctrLibreria.Controles.BeTextBox
        Me.txtRUC = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.txtCliente = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.txtPoliza = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboBusqueda = New ctrLibreria.Controles.BeComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.DgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.gbOtros.SuspendLayout()
        Me.gbConceptos.SuspendLayout()
        Me.gbVigencia.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed
        Me.TabControl1.Size = New System.Drawing.Size(807, 431)
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.cboBusqueda)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.lblTotalRegistro)
        Me.TabPage1.Controls.Add(Me.label)
        Me.TabPage1.Controls.Add(Me.DgvLista)
        Me.TabPage1.Controls.Add(Me.txtFiltro)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Size = New System.Drawing.Size(799, 405)
        Me.TabPage1.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.UseVisualStyleBackColor = False
        '
        'lblTotalRegistro
        '
        Me.lblTotalRegistro.AutoSize = True
        Me.lblTotalRegistro.BackColor = System.Drawing.Color.LightSteelBlue
        Me.lblTotalRegistro.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalRegistro.Location = New System.Drawing.Point(113, 383)
        Me.lblTotalRegistro.Name = "lblTotalRegistro"
        Me.lblTotalRegistro.Size = New System.Drawing.Size(0, 13)
        Me.lblTotalRegistro.TabIndex = 24
        '
        'label
        '
        Me.label.AutoSize = True
        Me.label.BackColor = System.Drawing.Color.LightSteelBlue
        Me.label.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label.Location = New System.Drawing.Point(8, 383)
        Me.label.Name = "label"
        Me.label.Size = New System.Drawing.Size(105, 13)
        Me.label.TabIndex = 23
        Me.label.Text = "Total Registros : "
        '
        'DgvLista
        '
        Me.DgvLista.AllowUserToAddRows = False
        Me.DgvLista.AllowUserToDeleteRows = False
        Me.DgvLista.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        Me.DgvLista.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DgvLista.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.DgvLista.BackgroundColor = System.Drawing.Color.Gray
        Me.DgvLista.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.DgvLista.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DgvLista.DefaultCellStyle = DataGridViewCellStyle2
        Me.DgvLista.EnableHeadersVisualStyles = False
        Me.DgvLista.Location = New System.Drawing.Point(12, 88)
        Me.DgvLista.MultiSelect = False
        Me.DgvLista.Name = "DgvLista"
        Me.DgvLista.ReadOnly = True
        Me.DgvLista.RowHeadersVisible = False
        Me.DgvLista.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DgvLista.Size = New System.Drawing.Size(744, 289)
        Me.DgvLista.TabIndex = 2
        '
        'txtFiltro
        '
        Me.txtFiltro.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFiltro.BackColor = System.Drawing.Color.Ivory
        Me.txtFiltro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFiltro.ForeColor = System.Drawing.Color.Black
        Me.txtFiltro.KeyEnter = True
        Me.txtFiltro.Location = New System.Drawing.Point(200, 21)
        Me.txtFiltro.Name = "txtFiltro"
        Me.txtFiltro.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFiltro.ShortcutsEnabled = False
        Me.txtFiltro.Size = New System.Drawing.Size(556, 20)
        Me.txtFiltro.TabIndex = 1
        Me.txtFiltro.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Busqueda por"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnAgregar)
        Me.GroupBox1.Controls.Add(Me.cboRamo)
        Me.GroupBox1.Controls.Add(Me.BeLabel15)
        Me.GroupBox1.Controls.Add(Me.gbOtros)
        Me.GroupBox1.Controls.Add(Me.cboMoneda)
        Me.GroupBox1.Controls.Add(Me.BeLabel11)
        Me.GroupBox1.Controls.Add(Me.gbConceptos)
        Me.GroupBox1.Controls.Add(Me.gbVigencia)
        Me.GroupBox1.Controls.Add(Me.txtDireccion)
        Me.GroupBox1.Controls.Add(Me.txtRUC)
        Me.GroupBox1.Controls.Add(Me.BeLabel4)
        Me.GroupBox1.Controls.Add(Me.BeLabel3)
        Me.GroupBox1.Controls.Add(Me.txtCliente)
        Me.GroupBox1.Controls.Add(Me.BeLabel2)
        Me.GroupBox1.Controls.Add(Me.txtPoliza)
        Me.GroupBox1.Controls.Add(Me.BeLabel1)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(793, 400)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(618, 60)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(99, 23)
        Me.btnAgregar.TabIndex = 15
        Me.btnAgregar.Text = "Agregar Endoso"
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'cboRamo
        '
        Me.cboRamo.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboRamo.BackColor = System.Drawing.Color.Ivory
        Me.cboRamo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRamo.ForeColor = System.Drawing.Color.Black
        Me.cboRamo.FormattingEnabled = True
        Me.cboRamo.KeyEnter = True
        Me.cboRamo.Location = New System.Drawing.Point(114, 92)
        Me.cboRamo.Name = "cboRamo"
        Me.cboRamo.Size = New System.Drawing.Size(210, 21)
        Me.cboRamo.TabIndex = 3
        '
        'BeLabel15
        '
        Me.BeLabel15.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel15.AutoSize = True
        Me.BeLabel15.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel15.ForeColor = System.Drawing.Color.Black
        Me.BeLabel15.Location = New System.Drawing.Point(56, 96)
        Me.BeLabel15.Name = "BeLabel15"
        Me.BeLabel15.Size = New System.Drawing.Size(35, 13)
        Me.BeLabel15.TabIndex = 14
        Me.BeLabel15.Text = "Ramo"
        '
        'gbOtros
        '
        Me.gbOtros.Controls.Add(Me.btnNuevoProv)
        Me.gbOtros.Controls.Add(Me.cboAseguradora)
        Me.gbOtros.Controls.Add(Me.mtbFechaLetraIni)
        Me.gbOtros.Controls.Add(Me.BeLabel18)
        Me.gbOtros.Controls.Add(Me.cboFormaPago)
        Me.gbOtros.Controls.Add(Me.txtLetras)
        Me.gbOtros.Controls.Add(Me.BeLabel13)
        Me.gbOtros.Controls.Add(Me.txtSumaAseguradora)
        Me.gbOtros.Controls.Add(Me.BeLabel14)
        Me.gbOtros.Controls.Add(Me.BeLabel16)
        Me.gbOtros.Controls.Add(Me.BeLabel17)
        Me.gbOtros.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbOtros.Location = New System.Drawing.Point(398, 230)
        Me.gbOtros.Name = "gbOtros"
        Me.gbOtros.Size = New System.Drawing.Size(319, 162)
        Me.gbOtros.TabIndex = 9
        Me.gbOtros.TabStop = False
        Me.gbOtros.Text = "Otros"
        '
        'btnNuevoProv
        '
        Me.btnNuevoProv.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnNuevoProv.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNuevoProv.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnNuevoProv.Image = CType(resources.GetObject("btnNuevoProv.Image"), System.Drawing.Image)
        Me.btnNuevoProv.Location = New System.Drawing.Point(285, 15)
        Me.btnNuevoProv.Name = "btnNuevoProv"
        Me.btnNuevoProv.Size = New System.Drawing.Size(30, 26)
        Me.btnNuevoProv.TabIndex = 100
        Me.btnNuevoProv.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnNuevoProv.UseVisualStyleBackColor = False
        '
        'cboAseguradora
        '
        Me.cboAseguradora.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboAseguradora.BackColor = System.Drawing.Color.Ivory
        Me.cboAseguradora.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAseguradora.ForeColor = System.Drawing.Color.Black
        Me.cboAseguradora.FormattingEnabled = True
        Me.cboAseguradora.KeyEnter = True
        Me.cboAseguradora.Location = New System.Drawing.Point(144, 17)
        Me.cboAseguradora.Name = "cboAseguradora"
        Me.cboAseguradora.Size = New System.Drawing.Size(137, 21)
        Me.cboAseguradora.TabIndex = 0
        '
        'mtbFechaLetraIni
        '
        Me.mtbFechaLetraIni.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbFechaLetraIni.BackColor = System.Drawing.Color.Ivory
        Me.mtbFechaLetraIni.BeepOnError = True
        Me.mtbFechaLetraIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbFechaLetraIni.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbFechaLetraIni.HidePromptOnLeave = True
        Me.mtbFechaLetraIni.KeyEnter = True
        Me.mtbFechaLetraIni.Location = New System.Drawing.Point(144, 126)
        Me.mtbFechaLetraIni.Mask = "00/00/0000"
        Me.mtbFechaLetraIni.Name = "mtbFechaLetraIni"
        Me.mtbFechaLetraIni.Size = New System.Drawing.Size(66, 20)
        Me.mtbFechaLetraIni.TabIndex = 4
        Me.mtbFechaLetraIni.ValidatingType = GetType(Date)
        '
        'BeLabel18
        '
        Me.BeLabel18.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel18.AutoSize = True
        Me.BeLabel18.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel18.ForeColor = System.Drawing.Color.Black
        Me.BeLabel18.Location = New System.Drawing.Point(26, 129)
        Me.BeLabel18.Name = "BeLabel18"
        Me.BeLabel18.Size = New System.Drawing.Size(98, 13)
        Me.BeLabel18.TabIndex = 19
        Me.BeLabel18.Text = "Fecha Inicial Cuota"
        '
        'cboFormaPago
        '
        Me.cboFormaPago.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboFormaPago.BackColor = System.Drawing.Color.Ivory
        Me.cboFormaPago.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormaPago.ForeColor = System.Drawing.Color.Black
        Me.cboFormaPago.FormattingEnabled = True
        Me.cboFormaPago.KeyEnter = True
        Me.cboFormaPago.Location = New System.Drawing.Point(144, 74)
        Me.cboFormaPago.Name = "cboFormaPago"
        Me.cboFormaPago.Size = New System.Drawing.Size(137, 21)
        Me.cboFormaPago.TabIndex = 2
        '
        'txtLetras
        '
        Me.txtLetras.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtLetras.BackColor = System.Drawing.Color.Ivory
        Me.txtLetras.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLetras.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLetras.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLetras.ForeColor = System.Drawing.Color.Black
        Me.txtLetras.KeyEnter = True
        Me.txtLetras.Location = New System.Drawing.Point(144, 100)
        Me.txtLetras.Name = "txtLetras"
        Me.txtLetras.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtLetras.ShortcutsEnabled = False
        Me.txtLetras.Size = New System.Drawing.Size(137, 20)
        Me.txtLetras.TabIndex = 3
        Me.txtLetras.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtLetras.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Entero
        '
        'BeLabel13
        '
        Me.BeLabel13.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel13.AutoSize = True
        Me.BeLabel13.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel13.ForeColor = System.Drawing.Color.Black
        Me.BeLabel13.Location = New System.Drawing.Point(25, 102)
        Me.BeLabel13.Name = "BeLabel13"
        Me.BeLabel13.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel13.TabIndex = 13
        Me.BeLabel13.Text = "Nro. de Cuotas"
        '
        'txtSumaAseguradora
        '
        Me.txtSumaAseguradora.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSumaAseguradora.BackColor = System.Drawing.Color.Ivory
        Me.txtSumaAseguradora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSumaAseguradora.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSumaAseguradora.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSumaAseguradora.ForeColor = System.Drawing.Color.Red
        Me.txtSumaAseguradora.KeyEnter = True
        Me.txtSumaAseguradora.Location = New System.Drawing.Point(144, 48)
        Me.txtSumaAseguradora.Name = "txtSumaAseguradora"
        Me.txtSumaAseguradora.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSumaAseguradora.ShortcutsEnabled = False
        Me.txtSumaAseguradora.Size = New System.Drawing.Size(137, 20)
        Me.txtSumaAseguradora.TabIndex = 1
        Me.txtSumaAseguradora.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSumaAseguradora.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel14
        '
        Me.BeLabel14.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel14.AutoSize = True
        Me.BeLabel14.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel14.ForeColor = System.Drawing.Color.Black
        Me.BeLabel14.Location = New System.Drawing.Point(26, 76)
        Me.BeLabel14.Name = "BeLabel14"
        Me.BeLabel14.Size = New System.Drawing.Size(79, 13)
        Me.BeLabel14.TabIndex = 11
        Me.BeLabel14.Text = "Forma de Pago"
        '
        'BeLabel16
        '
        Me.BeLabel16.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel16.AutoSize = True
        Me.BeLabel16.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel16.ForeColor = System.Drawing.Color.Black
        Me.BeLabel16.Location = New System.Drawing.Point(26, 50)
        Me.BeLabel16.Name = "BeLabel16"
        Me.BeLabel16.Size = New System.Drawing.Size(117, 13)
        Me.BeLabel16.TabIndex = 9
        Me.BeLabel16.Text = "Interes de Financiacion"
        '
        'BeLabel17
        '
        Me.BeLabel17.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel17.AutoSize = True
        Me.BeLabel17.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel17.ForeColor = System.Drawing.Color.Black
        Me.BeLabel17.Location = New System.Drawing.Point(26, 25)
        Me.BeLabel17.Name = "BeLabel17"
        Me.BeLabel17.Size = New System.Drawing.Size(99, 13)
        Me.BeLabel17.TabIndex = 8
        Me.BeLabel17.Text = "Compa�ia Asegura."
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(114, 201)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(123, 21)
        Me.cboMoneda.TabIndex = 7
        '
        'BeLabel11
        '
        Me.BeLabel11.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel11.AutoSize = True
        Me.BeLabel11.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel11.ForeColor = System.Drawing.Color.Black
        Me.BeLabel11.Location = New System.Drawing.Point(56, 205)
        Me.BeLabel11.Name = "BeLabel11"
        Me.BeLabel11.Size = New System.Drawing.Size(46, 13)
        Me.BeLabel11.TabIndex = 11
        Me.BeLabel11.Text = "Moneda"
        '
        'gbConceptos
        '
        Me.gbConceptos.Controls.Add(Me.txtTotal)
        Me.gbConceptos.Controls.Add(Me.BeLabel12)
        Me.gbConceptos.Controls.Add(Me.txtIGV)
        Me.gbConceptos.Controls.Add(Me.txtInteres)
        Me.gbConceptos.Controls.Add(Me.txtDerechoEmision)
        Me.gbConceptos.Controls.Add(Me.txtPrima)
        Me.gbConceptos.Controls.Add(Me.BeLabel10)
        Me.gbConceptos.Controls.Add(Me.BeLabel9)
        Me.gbConceptos.Controls.Add(Me.BeLabel7)
        Me.gbConceptos.Controls.Add(Me.BeLabel8)
        Me.gbConceptos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbConceptos.Location = New System.Drawing.Point(59, 230)
        Me.gbConceptos.Name = "gbConceptos"
        Me.gbConceptos.Size = New System.Drawing.Size(313, 162)
        Me.gbConceptos.TabIndex = 8
        Me.gbConceptos.TabStop = False
        Me.gbConceptos.Text = "Conceptos"
        '
        'txtTotal
        '
        Me.txtTotal.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTotal.BackColor = System.Drawing.Color.Ivory
        Me.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.ForeColor = System.Drawing.Color.Black
        Me.txtTotal.KeyEnter = True
        Me.txtTotal.Location = New System.Drawing.Point(175, 127)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTotal.ShortcutsEnabled = False
        Me.txtTotal.Size = New System.Drawing.Size(115, 20)
        Me.txtTotal.TabIndex = 4
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotal.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel12
        '
        Me.BeLabel12.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel12.AutoSize = True
        Me.BeLabel12.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel12.ForeColor = System.Drawing.Color.Black
        Me.BeLabel12.Location = New System.Drawing.Point(53, 129)
        Me.BeLabel12.Name = "BeLabel12"
        Me.BeLabel12.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel12.TabIndex = 13
        Me.BeLabel12.Text = "Total Importe"
        '
        'txtIGV
        '
        Me.txtIGV.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtIGV.BackColor = System.Drawing.Color.Ivory
        Me.txtIGV.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIGV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIGV.ForeColor = System.Drawing.Color.Black
        Me.txtIGV.KeyEnter = True
        Me.txtIGV.Location = New System.Drawing.Point(175, 100)
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtIGV.ShortcutsEnabled = False
        Me.txtIGV.Size = New System.Drawing.Size(115, 20)
        Me.txtIGV.TabIndex = 3
        Me.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIGV.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtInteres
        '
        Me.txtInteres.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtInteres.BackColor = System.Drawing.Color.Ivory
        Me.txtInteres.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtInteres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtInteres.ForeColor = System.Drawing.Color.Black
        Me.txtInteres.KeyEnter = True
        Me.txtInteres.Location = New System.Drawing.Point(175, 74)
        Me.txtInteres.Name = "txtInteres"
        Me.txtInteres.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtInteres.ShortcutsEnabled = False
        Me.txtInteres.Size = New System.Drawing.Size(115, 20)
        Me.txtInteres.TabIndex = 2
        Me.txtInteres.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInteres.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtDerechoEmision
        '
        Me.txtDerechoEmision.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDerechoEmision.BackColor = System.Drawing.Color.Ivory
        Me.txtDerechoEmision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDerechoEmision.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDerechoEmision.ForeColor = System.Drawing.Color.Black
        Me.txtDerechoEmision.KeyEnter = True
        Me.txtDerechoEmision.Location = New System.Drawing.Point(175, 48)
        Me.txtDerechoEmision.Name = "txtDerechoEmision"
        Me.txtDerechoEmision.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDerechoEmision.ShortcutsEnabled = False
        Me.txtDerechoEmision.Size = New System.Drawing.Size(115, 20)
        Me.txtDerechoEmision.TabIndex = 1
        Me.txtDerechoEmision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDerechoEmision.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtPrima
        '
        Me.txtPrima.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPrima.BackColor = System.Drawing.Color.Ivory
        Me.txtPrima.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPrima.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPrima.ForeColor = System.Drawing.Color.Black
        Me.txtPrima.KeyEnter = True
        Me.txtPrima.Location = New System.Drawing.Point(175, 23)
        Me.txtPrima.Name = "txtPrima"
        Me.txtPrima.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPrima.ShortcutsEnabled = False
        Me.txtPrima.Size = New System.Drawing.Size(115, 20)
        Me.txtPrima.TabIndex = 0
        Me.txtPrima.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPrima.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.ForeColor = System.Drawing.Color.Black
        Me.BeLabel10.Location = New System.Drawing.Point(53, 102)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(34, 13)
        Me.BeLabel10.TabIndex = 11
        Me.BeLabel10.Text = "I.G.V."
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.Black
        Me.BeLabel9.Location = New System.Drawing.Point(53, 76)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(50, 13)
        Me.BeLabel9.TabIndex = 10
        Me.BeLabel9.Text = "Intereses"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.Black
        Me.BeLabel7.Location = New System.Drawing.Point(53, 50)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(102, 13)
        Me.BeLabel7.TabIndex = 9
        Me.BeLabel7.Text = "Derecho de Emision"
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.Black
        Me.BeLabel8.Location = New System.Drawing.Point(53, 25)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(33, 13)
        Me.BeLabel8.TabIndex = 8
        Me.BeLabel8.Text = "Prima"
        '
        'gbVigencia
        '
        Me.gbVigencia.Controls.Add(Me.mtbVigenciaFin)
        Me.gbVigencia.Controls.Add(Me.BeLabel6)
        Me.gbVigencia.Controls.Add(Me.mtbVigenciaIni)
        Me.gbVigencia.Controls.Add(Me.BeLabel5)
        Me.gbVigencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbVigencia.Location = New System.Drawing.Point(58, 45)
        Me.gbVigencia.Name = "gbVigencia"
        Me.gbVigencia.Size = New System.Drawing.Size(414, 41)
        Me.gbVigencia.TabIndex = 2
        Me.gbVigencia.TabStop = False
        Me.gbVigencia.Text = "Vigencia"
        '
        'mtbVigenciaFin
        '
        Me.mtbVigenciaFin.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbVigenciaFin.BackColor = System.Drawing.Color.Ivory
        Me.mtbVigenciaFin.BeepOnError = True
        Me.mtbVigenciaFin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbVigenciaFin.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbVigenciaFin.HidePromptOnLeave = True
        Me.mtbVigenciaFin.KeyEnter = True
        Me.mtbVigenciaFin.Location = New System.Drawing.Point(314, 15)
        Me.mtbVigenciaFin.Mask = "00/00/0000"
        Me.mtbVigenciaFin.Name = "mtbVigenciaFin"
        Me.mtbVigenciaFin.Size = New System.Drawing.Size(66, 20)
        Me.mtbVigenciaFin.TabIndex = 1
        Me.mtbVigenciaFin.ValidatingType = GetType(Date)
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.Black
        Me.BeLabel6.Location = New System.Drawing.Point(236, 18)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(78, 13)
        Me.BeLabel6.TabIndex = 11
        Me.BeLabel6.Text = "Fecha Termino"
        '
        'mtbVigenciaIni
        '
        Me.mtbVigenciaIni.Apariencia = ctrLibreria.Controles.BeMaskedTextBox.FormaTipo.Skin01
        Me.mtbVigenciaIni.BackColor = System.Drawing.Color.Ivory
        Me.mtbVigenciaIni.BeepOnError = True
        Me.mtbVigenciaIni.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbVigenciaIni.ForeColor = System.Drawing.SystemColors.WindowText
        Me.mtbVigenciaIni.HidePromptOnLeave = True
        Me.mtbVigenciaIni.KeyEnter = True
        Me.mtbVigenciaIni.Location = New System.Drawing.Point(123, 15)
        Me.mtbVigenciaIni.Mask = "00/00/0000"
        Me.mtbVigenciaIni.Name = "mtbVigenciaIni"
        Me.mtbVigenciaIni.Size = New System.Drawing.Size(66, 20)
        Me.mtbVigenciaIni.TabIndex = 0
        Me.mtbVigenciaIni.ValidatingType = GetType(Date)
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.Black
        Me.BeLabel5.Location = New System.Drawing.Point(53, 18)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(65, 13)
        Me.BeLabel5.TabIndex = 8
        Me.BeLabel5.Text = "Fecha Inicio"
        '
        'txtDireccion
        '
        Me.txtDireccion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDireccion.BackColor = System.Drawing.Color.Ivory
        Me.txtDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDireccion.ForeColor = System.Drawing.Color.Black
        Me.txtDireccion.KeyEnter = True
        Me.txtDireccion.Location = New System.Drawing.Point(114, 174)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDireccion.ReadOnly = True
        Me.txtDireccion.ShortcutsEnabled = False
        Me.txtDireccion.Size = New System.Drawing.Size(603, 20)
        Me.txtDireccion.TabIndex = 6
        Me.txtDireccion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtRUC
        '
        Me.txtRUC.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtRUC.BackColor = System.Drawing.Color.Ivory
        Me.txtRUC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRUC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRUC.ForeColor = System.Drawing.Color.Black
        Me.txtRUC.KeyEnter = True
        Me.txtRUC.Location = New System.Drawing.Point(114, 148)
        Me.txtRUC.MaxLength = 11
        Me.txtRUC.Name = "txtRUC"
        Me.txtRUC.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtRUC.ReadOnly = True
        Me.txtRUC.ShortcutsEnabled = False
        Me.txtRUC.Size = New System.Drawing.Size(111, 20)
        Me.txtRUC.TabIndex = 5
        Me.txtRUC.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.Black
        Me.BeLabel4.Location = New System.Drawing.Point(56, 180)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel4.TabIndex = 5
        Me.BeLabel4.Text = "Direccion"
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(56, 154)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(39, 13)
        Me.BeLabel3.TabIndex = 4
        Me.BeLabel3.Text = "R.U.C."
        '
        'txtCliente
        '
        Me.txtCliente.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCliente.BackColor = System.Drawing.Color.Ivory
        Me.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCliente.ForeColor = System.Drawing.Color.Black
        Me.txtCliente.KeyEnter = True
        Me.txtCliente.Location = New System.Drawing.Point(114, 122)
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCliente.ReadOnly = True
        Me.txtCliente.ShortcutsEnabled = False
        Me.txtCliente.Size = New System.Drawing.Size(603, 20)
        Me.txtCliente.TabIndex = 4
        Me.txtCliente.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(56, 124)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(39, 13)
        Me.BeLabel2.TabIndex = 2
        Me.BeLabel2.Text = "Cliente"
        '
        'txtPoliza
        '
        Me.txtPoliza.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtPoliza.BackColor = System.Drawing.Color.Ivory
        Me.txtPoliza.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPoliza.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPoliza.ForeColor = System.Drawing.Color.Black
        Me.txtPoliza.KeyEnter = True
        Me.txtPoliza.Location = New System.Drawing.Point(114, 19)
        Me.txtPoliza.Name = "txtPoliza"
        Me.txtPoliza.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtPoliza.ShortcutsEnabled = False
        Me.txtPoliza.Size = New System.Drawing.Size(177, 20)
        Me.txtPoliza.TabIndex = 0
        Me.txtPoliza.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(56, 22)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel1.TabIndex = 0
        Me.BeLabel1.Text = "Nro Poliza"
        '
        'Timer1
        '
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(566, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(190, 13)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "(E) Enter para ver Detalle Poliza"
        '
        'cboBusqueda
        '
        Me.cboBusqueda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboBusqueda.BackColor = System.Drawing.Color.Ivory
        Me.cboBusqueda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBusqueda.ForeColor = System.Drawing.Color.Black
        Me.cboBusqueda.FormattingEnabled = True
        Me.cboBusqueda.Items.AddRange(New Object() {"Ramo", "Poliza"})
        Me.cboBusqueda.KeyEnter = True
        Me.cboBusqueda.Location = New System.Drawing.Point(91, 20)
        Me.cboBusqueda.Name = "cboBusqueda"
        Me.cboBusqueda.Size = New System.Drawing.Size(103, 21)
        Me.cboBusqueda.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(563, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(193, 13)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "(F4) Ver Cronograma de la Poliza"
        '
        'frmMantePoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(807, 431)
        Me.MaximizeBox = False
        Me.Name = "frmMantePoliza"
        Me.Text = "Mantenimiento de Seguros y Polizas"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbOtros.ResumeLayout(False)
        Me.gbOtros.PerformLayout()
        Me.gbConceptos.ResumeLayout(False)
        Me.gbConceptos.PerformLayout()
        Me.gbVigencia.ResumeLayout(False)
        Me.gbVigencia.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblTotalRegistro As System.Windows.Forms.Label
    Friend WithEvents label As System.Windows.Forms.Label
    Friend WithEvents DgvLista As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents txtFiltro As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPoliza As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtDireccion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtRUC As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtCliente As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents gbVigencia As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents mtbVigenciaIni As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents mtbVigenciaFin As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents gbConceptos As System.Windows.Forms.GroupBox
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel11 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtIGV As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtInteres As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDerechoEmision As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtPrima As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtTotal As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel12 As ctrLibreria.Controles.BeLabel
    Friend WithEvents gbOtros As System.Windows.Forms.GroupBox
    Friend WithEvents txtLetras As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel13 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtSumaAseguradora As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel14 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel16 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel17 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboFormaPago As ctrLibreria.Controles.BeComboBox
    Friend WithEvents mtbFechaLetraIni As ctrLibreria.Controles.BeMaskedTextBox
    Friend WithEvents BeLabel18 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents cboRamo As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel15 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboAseguradora As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboBusqueda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnNuevoProv As System.Windows.Forms.Button
    Friend WithEvents btnAgregar As System.Windows.Forms.Button

End Class
