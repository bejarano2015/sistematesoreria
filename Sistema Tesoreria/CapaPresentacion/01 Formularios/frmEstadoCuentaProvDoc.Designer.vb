<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEstadoCuentaProvDoc
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.TabPersonalizado1 = New Controles_DHMont.TabPersonalizado
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.chkTodos = New System.Windows.Forms.CheckBox
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.btnVer = New System.Windows.Forms.Button
        Me.dgvProveedores = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CountDoc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.rdbDescr = New System.Windows.Forms.RadioButton
        Me.txtFiltrar = New ctrLibreria.Controles.BeTextBox
        Me.rdbRuc = New System.Windows.Forms.RadioButton
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel29 = New ctrLibreria.Controles.BeLabel
        Me.txtSol = New ctrLibreria.Controles.BeTextBox
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.lblMoneda = New ctrLibreria.Controles.BeLabel
        Me.lblEtiqueta = New ctrLibreria.Controles.BeLabel
        Me.dgvDocumentos = New System.Windows.Forms.DataGridView
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IdDocumento = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.BeDateTimePicker3 = New ctrLibreria.Controles.BeDateTimePicker
        Me.BeDateTimePicker4 = New ctrLibreria.Controles.BeDateTimePicker
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.cboTodosSubCxEmpr = New ctrLibreria.Controles.BeComboBox
        Me.chkTodosEmprSubc = New System.Windows.Forms.CheckBox
        Me.rdbDetalladoSEmpr = New System.Windows.Forms.RadioButton
        Me.rdbResumidoSEmpr = New System.Windows.Forms.RadioButton
        Me.btnVerTodosEmprSubC = New System.Windows.Forms.Button
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.cboTodosSubC = New ctrLibreria.Controles.BeComboBox
        Me.BeDateTimePicker1 = New ctrLibreria.Controles.BeDateTimePicker
        Me.chkTodosSubC = New System.Windows.Forms.CheckBox
        Me.BeDateTimePicker2 = New ctrLibreria.Controles.BeDateTimePicker
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.RadioButton4 = New System.Windows.Forms.RadioButton
        Me.btnVerTodosSubC = New System.Windows.Forms.Button
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.dtpFechaInicio = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaFinal = New ctrLibreria.Controles.BeDateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboTodosProvxEmpr = New ctrLibreria.Controles.BeComboBox
        Me.chkTodosxEmpr = New System.Windows.Forms.CheckBox
        Me.rdbDetalladoEmpr = New System.Windows.Forms.RadioButton
        Me.rdbResumidoEmpr = New System.Windows.Forms.RadioButton
        Me.btnVerTodosEmpr = New System.Windows.Forms.Button
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtpF2 = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpF1 = New ctrLibreria.Controles.BeDateTimePicker
        Me.Opcion2 = New System.Windows.Forms.RadioButton
        Me.Opcion1 = New System.Windows.Forms.RadioButton
        Me.btnVerPrestamos = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.cboTodosProv = New ctrLibreria.Controles.BeComboBox
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.chkTodos2 = New System.Windows.Forms.CheckBox
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.rdbDetallado = New System.Windows.Forms.RadioButton
        Me.rdbResumido = New System.Windows.Forms.RadioButton
        Me.btnVerTodos = New System.Windows.Forms.Button
        Me.Panel1.SuspendLayout()
        Me.TabPersonalizado1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.TabPersonalizado1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(685, 588)
        Me.Panel1.TabIndex = 1
        '
        'TabPersonalizado1
        '
        Me.TabPersonalizado1.Controls.Add(Me.TabPage3)
        Me.TabPersonalizado1.Controls.Add(Me.TabPage4)
        Me.TabPersonalizado1.Controls.Add(Me.TabPage1)
        Me.TabPersonalizado1.Location = New System.Drawing.Point(-2, 3)
        Me.TabPersonalizado1.Name = "TabPersonalizado1"
        Me.TabPersonalizado1.SelectedIndex = 0
        Me.TabPersonalizado1.Size = New System.Drawing.Size(688, 588)
        Me.TabPersonalizado1.TabIndex = 1
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.dgvProveedores)
        Me.TabPage3.Controls.Add(Me.Label1)
        Me.TabPage3.Controls.Add(Me.GroupBox5)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(680, 562)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "Saldo Proveedores"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkTodos)
        Me.GroupBox3.Controls.Add(Me.RadioButton2)
        Me.GroupBox3.Controls.Add(Me.RadioButton1)
        Me.GroupBox3.Controls.Add(Me.btnVer)
        Me.GroupBox3.Location = New System.Drawing.Point(428, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(240, 57)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Imprimir"
        '
        'chkTodos
        '
        Me.chkTodos.AutoSize = True
        Me.chkTodos.Checked = True
        Me.chkTodos.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTodos.Location = New System.Drawing.Point(11, 25)
        Me.chkTodos.Name = "chkTodos"
        Me.chkTodos.Size = New System.Drawing.Size(56, 17)
        Me.chkTodos.TabIndex = 5
        Me.chkTodos.Text = "Todos"
        Me.chkTodos.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Checked = True
        Me.RadioButton2.Location = New System.Drawing.Point(76, 34)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(70, 17)
        Me.RadioButton2.TabIndex = 4
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Detallado"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(76, 16)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(72, 17)
        Me.RadioButton1.TabIndex = 3
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Resumido"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'btnVer
        '
        Me.btnVer.Location = New System.Drawing.Point(154, 19)
        Me.btnVer.Name = "btnVer"
        Me.btnVer.Size = New System.Drawing.Size(75, 23)
        Me.btnVer.TabIndex = 2
        Me.btnVer.Text = "Ver"
        Me.btnVer.UseVisualStyleBackColor = True
        '
        'dgvProveedores
        '
        Me.dgvProveedores.AllowUserToAddRows = False
        Me.dgvProveedores.BackgroundColor = System.Drawing.Color.White
        Me.dgvProveedores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProveedores.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.CountDoc, Me.Column7})
        Me.dgvProveedores.EnableHeadersVisualStyles = False
        Me.dgvProveedores.Location = New System.Drawing.Point(30, 69)
        Me.dgvProveedores.Name = "dgvProveedores"
        Me.dgvProveedores.RowHeadersVisible = False
        Me.dgvProveedores.Size = New System.Drawing.Size(625, 467)
        Me.dgvProveedores.TabIndex = 2
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "IdProveedor"
        Me.Column1.HeaderText = "Cod Anexo"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "TipoAnaliticoDescripcion"
        Me.Column2.HeaderText = "Tipo Anexo"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "AnaliticoDescripcion"
        Me.Column3.HeaderText = "Nombre"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "MontoSaldo"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle1.Format = "N2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column4.HeaderText = "Saldo"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "MonDescripcion"
        Me.Column5.HeaderText = "Moneda"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "IdTipoProveedor"
        Me.Column6.HeaderText = "TipoProv"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Visible = False
        '
        'CountDoc
        '
        Me.CountDoc.DataPropertyName = "CountDoc"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        Me.CountDoc.DefaultCellStyle = DataGridViewCellStyle2
        Me.CountDoc.HeaderText = "Cant. Fac."
        Me.CountDoc.Name = "CountDoc"
        Me.CountDoc.Width = 90
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "MonCodigo"
        Me.Column7.HeaderText = "MonCodigo"
        Me.Column7.Name = "Column7"
        Me.Column7.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(340, 539)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(315, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Ver Documentos Pendientes de Pago = Doble Click sobre la lista."
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.rdbDescr)
        Me.GroupBox5.Controls.Add(Me.txtFiltrar)
        Me.GroupBox5.Controls.Add(Me.rdbRuc)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 20)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(414, 43)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Buscar Por:"
        '
        'rdbDescr
        '
        Me.rdbDescr.AutoSize = True
        Me.rdbDescr.Location = New System.Drawing.Point(58, 17)
        Me.rdbDescr.Name = "rdbDescr"
        Me.rdbDescr.Size = New System.Drawing.Size(88, 17)
        Me.rdbDescr.TabIndex = 2
        Me.rdbDescr.TabStop = True
        Me.rdbDescr.Text = "Razón Social"
        Me.rdbDescr.UseVisualStyleBackColor = True
        '
        'txtFiltrar
        '
        Me.txtFiltrar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtFiltrar.BackColor = System.Drawing.Color.LightYellow
        Me.txtFiltrar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFiltrar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFiltrar.ForeColor = System.Drawing.Color.Black
        Me.txtFiltrar.KeyEnter = True
        Me.txtFiltrar.Location = New System.Drawing.Point(152, 14)
        Me.txtFiltrar.MaxLength = 50
        Me.txtFiltrar.Name = "txtFiltrar"
        Me.txtFiltrar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtFiltrar.ShortcutsEnabled = False
        Me.txtFiltrar.Size = New System.Drawing.Size(253, 20)
        Me.txtFiltrar.TabIndex = 1
        Me.txtFiltrar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Alfanúmerico
        '
        'rdbRuc
        '
        Me.rdbRuc.AutoSize = True
        Me.rdbRuc.Location = New System.Drawing.Point(7, 17)
        Me.rdbRuc.Name = "rdbRuc"
        Me.rdbRuc.Size = New System.Drawing.Size(45, 17)
        Me.rdbRuc.TabIndex = 0
        Me.rdbRuc.TabStop = True
        Me.rdbRuc.Text = "Ruc"
        Me.rdbRuc.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(680, 562)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "Saldo Documentos"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.BeLabel3)
        Me.GroupBox2.Controls.Add(Me.BeLabel29)
        Me.GroupBox2.Controls.Add(Me.txtSol)
        Me.GroupBox2.Controls.Add(Me.BeLabel2)
        Me.GroupBox2.Controls.Add(Me.BeLabel1)
        Me.GroupBox2.Controls.Add(Me.lblMoneda)
        Me.GroupBox2.Controls.Add(Me.lblEtiqueta)
        Me.GroupBox2.Controls.Add(Me.dgvDocumentos)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(663, 323)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel3.ForeColor = System.Drawing.Color.Black
        Me.BeLabel3.Location = New System.Drawing.Point(25, 297)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel3.TabIndex = 15
        Me.BeLabel3.Text = "Total"
        '
        'BeLabel29
        '
        Me.BeLabel29.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel29.AutoSize = True
        Me.BeLabel29.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel29.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel29.ForeColor = System.Drawing.Color.Black
        Me.BeLabel29.Location = New System.Drawing.Point(429, 297)
        Me.BeLabel29.Name = "BeLabel29"
        Me.BeLabel29.Size = New System.Drawing.Size(40, 13)
        Me.BeLabel29.TabIndex = 14
        Me.BeLabel29.Text = "Total"
        '
        'txtSol
        '
        Me.txtSol.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSol.BackColor = System.Drawing.Color.Ivory
        Me.txtSol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSol.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSol.Enabled = False
        Me.txtSol.ForeColor = System.Drawing.Color.Black
        Me.txtSol.KeyEnter = True
        Me.txtSol.Location = New System.Drawing.Point(477, 295)
        Me.txtSol.Name = "txtSol"
        Me.txtSol.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSol.ShortcutsEnabled = False
        Me.txtSol.Size = New System.Drawing.Size(88, 20)
        Me.txtSol.TabIndex = 5
        Me.txtSol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSol.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(513, 16)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(52, 13)
        Me.BeLabel2.TabIndex = 4
        Me.BeLabel2.Text = "Moneda :"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(25, 16)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(62, 13)
        Me.BeLabel1.TabIndex = 3
        Me.BeLabel1.Text = "Proveedor :"
        '
        'lblMoneda
        '
        Me.lblMoneda.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblMoneda.AutoSize = True
        Me.lblMoneda.BackColor = System.Drawing.Color.Transparent
        Me.lblMoneda.ForeColor = System.Drawing.Color.Black
        Me.lblMoneda.Location = New System.Drawing.Point(499, 38)
        Me.lblMoneda.Name = "lblMoneda"
        Me.lblMoneda.Size = New System.Drawing.Size(0, 13)
        Me.lblMoneda.TabIndex = 2
        '
        'lblEtiqueta
        '
        Me.lblEtiqueta.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblEtiqueta.AutoSize = True
        Me.lblEtiqueta.BackColor = System.Drawing.Color.Transparent
        Me.lblEtiqueta.ForeColor = System.Drawing.Color.Black
        Me.lblEtiqueta.Location = New System.Drawing.Point(25, 38)
        Me.lblEtiqueta.Name = "lblEtiqueta"
        Me.lblEtiqueta.Size = New System.Drawing.Size(0, 13)
        Me.lblEtiqueta.TabIndex = 1
        '
        'dgvDocumentos
        '
        Me.dgvDocumentos.AllowUserToAddRows = False
        Me.dgvDocumentos.BackgroundColor = System.Drawing.Color.White
        Me.dgvDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDocumentos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column8, Me.Column11, Me.TD, Me.Column9, Me.Column10, Me.IdDocumento})
        Me.dgvDocumentos.EnableHeadersVisualStyles = False
        Me.dgvDocumentos.Location = New System.Drawing.Point(28, 64)
        Me.dgvDocumentos.Name = "dgvDocumentos"
        Me.dgvDocumentos.Size = New System.Drawing.Size(563, 225)
        Me.dgvDocumentos.TabIndex = 0
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "FechCreacion"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column8.HeaderText = "Fecha Emi"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "FechV"
        DataGridViewCellStyle4.Format = "d"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Column11.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column11.HeaderText = "Fecha Venc"
        Me.Column11.Name = "Column11"
        '
        'TD
        '
        Me.TD.DataPropertyName = "AbrDoc"
        Me.TD.HeaderText = "TD"
        Me.TD.Name = "TD"
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "Doc"
        Me.Column9.HeaderText = "Número"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "SaldoDocumento"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        DataGridViewCellStyle5.Format = "N2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.Column10.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column10.HeaderText = "Importe"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        '
        'IdDocumento
        '
        Me.IdDocumento.DataPropertyName = "IdDocumento"
        Me.IdDocumento.HeaderText = "IdDocumento"
        Me.IdDocumento.Name = "IdDocumento"
        Me.IdDocumento.Visible = False
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.LightSteelBlue
        Me.TabPage1.Controls.Add(Me.GroupBox8)
        Me.TabPage1.Controls.Add(Me.GroupBox7)
        Me.TabPage1.Controls.Add(Me.GroupBox6)
        Me.TabPage1.Controls.Add(Me.GroupBox4)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(680, 562)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Reportes"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Label13)
        Me.GroupBox8.Controls.Add(Me.BeDateTimePicker3)
        Me.GroupBox8.Controls.Add(Me.BeDateTimePicker4)
        Me.GroupBox8.Controls.Add(Me.Label14)
        Me.GroupBox8.Controls.Add(Me.Label15)
        Me.GroupBox8.Controls.Add(Me.cboTodosSubCxEmpr)
        Me.GroupBox8.Controls.Add(Me.chkTodosEmprSubc)
        Me.GroupBox8.Controls.Add(Me.rdbDetalladoSEmpr)
        Me.GroupBox8.Controls.Add(Me.rdbResumidoSEmpr)
        Me.GroupBox8.Controls.Add(Me.btnVerTodosEmprSubC)
        Me.GroupBox8.Location = New System.Drawing.Point(8, 445)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(665, 106)
        Me.GroupBox8.TabIndex = 10
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Reporte General de Facturas por Pagar Por la Empresa Actual"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(309, 76)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(12, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "y"
        '
        'BeDateTimePicker3
        '
        Me.BeDateTimePicker3.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.BeDateTimePicker3.CustomFormat = ""
        Me.BeDateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.BeDateTimePicker3.KeyEnter = True
        Me.BeDateTimePicker3.Location = New System.Drawing.Point(216, 72)
        Me.BeDateTimePicker3.Name = "BeDateTimePicker3"
        Me.BeDateTimePicker3.Size = New System.Drawing.Size(92, 20)
        Me.BeDateTimePicker3.TabIndex = 12
        Me.BeDateTimePicker3.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.BeDateTimePicker3.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'BeDateTimePicker4
        '
        Me.BeDateTimePicker4.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.BeDateTimePicker4.CustomFormat = ""
        Me.BeDateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.BeDateTimePicker4.KeyEnter = True
        Me.BeDateTimePicker4.Location = New System.Drawing.Point(324, 72)
        Me.BeDateTimePicker4.Name = "BeDateTimePicker4"
        Me.BeDateTimePicker4.Size = New System.Drawing.Size(92, 20)
        Me.BeDateTimePicker4.TabIndex = 10
        Me.BeDateTimePicker4.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.BeDateTimePicker4.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 77)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(208, 13)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Comprobantes por Cancelar Emitidos Entre"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 20)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(211, 13)
        Me.Label15.TabIndex = 8
        Me.Label15.Text = "Seleccione Todos o un solo SubContratista"
        '
        'cboTodosSubCxEmpr
        '
        Me.cboTodosSubCxEmpr.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTodosSubCxEmpr.BackColor = System.Drawing.Color.Ivory
        Me.cboTodosSubCxEmpr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTodosSubCxEmpr.ForeColor = System.Drawing.Color.Black
        Me.cboTodosSubCxEmpr.FormattingEnabled = True
        Me.cboTodosSubCxEmpr.KeyEnter = True
        Me.cboTodosSubCxEmpr.Location = New System.Drawing.Point(9, 43)
        Me.cboTodosSubCxEmpr.Name = "cboTodosSubCxEmpr"
        Me.cboTodosSubCxEmpr.Size = New System.Drawing.Size(650, 21)
        Me.cboTodosSubCxEmpr.TabIndex = 6
        '
        'chkTodosEmprSubc
        '
        Me.chkTodosEmprSubc.AutoSize = True
        Me.chkTodosEmprSubc.Checked = True
        Me.chkTodosEmprSubc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTodosEmprSubc.Location = New System.Drawing.Point(315, 20)
        Me.chkTodosEmprSubc.Name = "chkTodosEmprSubc"
        Me.chkTodosEmprSubc.Size = New System.Drawing.Size(56, 17)
        Me.chkTodosEmprSubc.TabIndex = 5
        Me.chkTodosEmprSubc.Text = "Todos"
        Me.chkTodosEmprSubc.UseVisualStyleBackColor = True
        '
        'rdbDetalladoSEmpr
        '
        Me.rdbDetalladoSEmpr.AutoSize = True
        Me.rdbDetalladoSEmpr.Checked = True
        Me.rdbDetalladoSEmpr.Location = New System.Drawing.Point(499, 73)
        Me.rdbDetalladoSEmpr.Name = "rdbDetalladoSEmpr"
        Me.rdbDetalladoSEmpr.Size = New System.Drawing.Size(70, 17)
        Me.rdbDetalladoSEmpr.TabIndex = 4
        Me.rdbDetalladoSEmpr.TabStop = True
        Me.rdbDetalladoSEmpr.Text = "Detallado"
        Me.rdbDetalladoSEmpr.UseVisualStyleBackColor = True
        '
        'rdbResumidoSEmpr
        '
        Me.rdbResumidoSEmpr.AutoSize = True
        Me.rdbResumidoSEmpr.Location = New System.Drawing.Point(421, 73)
        Me.rdbResumidoSEmpr.Name = "rdbResumidoSEmpr"
        Me.rdbResumidoSEmpr.Size = New System.Drawing.Size(72, 17)
        Me.rdbResumidoSEmpr.TabIndex = 3
        Me.rdbResumidoSEmpr.TabStop = True
        Me.rdbResumidoSEmpr.Text = "Resumido"
        Me.rdbResumidoSEmpr.UseVisualStyleBackColor = True
        '
        'btnVerTodosEmprSubC
        '
        Me.btnVerTodosEmprSubC.Location = New System.Drawing.Point(584, 72)
        Me.btnVerTodosEmprSubC.Name = "btnVerTodosEmprSubC"
        Me.btnVerTodosEmprSubC.Size = New System.Drawing.Size(75, 23)
        Me.btnVerTodosEmprSubC.TabIndex = 2
        Me.btnVerTodosEmprSubC.Text = "Ver"
        Me.btnVerTodosEmprSubC.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Label10)
        Me.GroupBox7.Controls.Add(Me.Label11)
        Me.GroupBox7.Controls.Add(Me.Label12)
        Me.GroupBox7.Controls.Add(Me.cboTodosSubC)
        Me.GroupBox7.Controls.Add(Me.BeDateTimePicker1)
        Me.GroupBox7.Controls.Add(Me.chkTodosSubC)
        Me.GroupBox7.Controls.Add(Me.BeDateTimePicker2)
        Me.GroupBox7.Controls.Add(Me.RadioButton3)
        Me.GroupBox7.Controls.Add(Me.RadioButton4)
        Me.GroupBox7.Controls.Add(Me.btnVerTodosSubC)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 333)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(665, 106)
        Me.GroupBox7.TabIndex = 7
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Reporte General de Facturas por Pagar Por Todas las Empresas"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(139, 81)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(12, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "y"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(211, 13)
        Me.Label11.TabIndex = 8
        Me.Label11.Text = "Seleccione Todos o un solo SubContratista"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(6, 81)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(32, 13)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "Entre"
        '
        'cboTodosSubC
        '
        Me.cboTodosSubC.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTodosSubC.BackColor = System.Drawing.Color.Ivory
        Me.cboTodosSubC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTodosSubC.ForeColor = System.Drawing.Color.Black
        Me.cboTodosSubC.FormattingEnabled = True
        Me.cboTodosSubC.KeyEnter = True
        Me.cboTodosSubC.Location = New System.Drawing.Point(9, 43)
        Me.cboTodosSubC.Name = "cboTodosSubC"
        Me.cboTodosSubC.Size = New System.Drawing.Size(650, 21)
        Me.cboTodosSubC.TabIndex = 6
        '
        'BeDateTimePicker1
        '
        Me.BeDateTimePicker1.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.BeDateTimePicker1.CustomFormat = ""
        Me.BeDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.BeDateTimePicker1.KeyEnter = True
        Me.BeDateTimePicker1.Location = New System.Drawing.Point(154, 77)
        Me.BeDateTimePicker1.Name = "BeDateTimePicker1"
        Me.BeDateTimePicker1.Size = New System.Drawing.Size(92, 20)
        Me.BeDateTimePicker1.TabIndex = 13
        Me.BeDateTimePicker1.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.BeDateTimePicker1.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'chkTodosSubC
        '
        Me.chkTodosSubC.AutoSize = True
        Me.chkTodosSubC.Checked = True
        Me.chkTodosSubC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTodosSubC.Location = New System.Drawing.Point(315, 20)
        Me.chkTodosSubC.Name = "chkTodosSubC"
        Me.chkTodosSubC.Size = New System.Drawing.Size(56, 17)
        Me.chkTodosSubC.TabIndex = 5
        Me.chkTodosSubC.Text = "Todos"
        Me.chkTodosSubC.UseVisualStyleBackColor = True
        '
        'BeDateTimePicker2
        '
        Me.BeDateTimePicker2.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.BeDateTimePicker2.CustomFormat = ""
        Me.BeDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.BeDateTimePicker2.KeyEnter = True
        Me.BeDateTimePicker2.Location = New System.Drawing.Point(44, 77)
        Me.BeDateTimePicker2.Name = "BeDateTimePicker2"
        Me.BeDateTimePicker2.Size = New System.Drawing.Size(92, 20)
        Me.BeDateTimePicker2.TabIndex = 12
        Me.BeDateTimePicker2.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.BeDateTimePicker2.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Checked = True
        Me.RadioButton3.Location = New System.Drawing.Point(499, 73)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(70, 17)
        Me.RadioButton3.TabIndex = 4
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Detallado"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(421, 73)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(72, 17)
        Me.RadioButton4.TabIndex = 3
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "Resumido"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'btnVerTodosSubC
        '
        Me.btnVerTodosSubC.Location = New System.Drawing.Point(584, 70)
        Me.btnVerTodosSubC.Name = "btnVerTodosSubC"
        Me.btnVerTodosSubC.Size = New System.Drawing.Size(75, 23)
        Me.btnVerTodosSubC.TabIndex = 2
        Me.btnVerTodosSubC.Text = "Ver"
        Me.btnVerTodosSubC.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label9)
        Me.GroupBox6.Controls.Add(Me.dtpFechaInicio)
        Me.GroupBox6.Controls.Add(Me.dtpFechaFinal)
        Me.GroupBox6.Controls.Add(Me.Label6)
        Me.GroupBox6.Controls.Add(Me.Label5)
        Me.GroupBox6.Controls.Add(Me.cboTodosProvxEmpr)
        Me.GroupBox6.Controls.Add(Me.chkTodosxEmpr)
        Me.GroupBox6.Controls.Add(Me.rdbDetalladoEmpr)
        Me.GroupBox6.Controls.Add(Me.rdbResumidoEmpr)
        Me.GroupBox6.Controls.Add(Me.btnVerTodosEmpr)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 125)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(665, 106)
        Me.GroupBox6.TabIndex = 9
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Reporte General de Facturas por Pagar Por la Empresa Actual"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(309, 76)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(12, 13)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "y"
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaInicio.CustomFormat = ""
        Me.dtpFechaInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicio.KeyEnter = True
        Me.dtpFechaInicio.Location = New System.Drawing.Point(216, 72)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(92, 20)
        Me.dtpFechaInicio.TabIndex = 12
        Me.dtpFechaInicio.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaInicio.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaFinal
        '
        Me.dtpFechaFinal.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFinal.CustomFormat = ""
        Me.dtpFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFinal.KeyEnter = True
        Me.dtpFechaFinal.Location = New System.Drawing.Point(324, 72)
        Me.dtpFechaFinal.Name = "dtpFechaFinal"
        Me.dtpFechaFinal.Size = New System.Drawing.Size(92, 20)
        Me.dtpFechaFinal.TabIndex = 10
        Me.dtpFechaFinal.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFinal.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 77)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(208, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Comprobantes por Cancelar Emitidos Entre"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 20)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(191, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Seleccione Todos o un solo Proveedor"
        '
        'cboTodosProvxEmpr
        '
        Me.cboTodosProvxEmpr.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTodosProvxEmpr.BackColor = System.Drawing.Color.Ivory
        Me.cboTodosProvxEmpr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTodosProvxEmpr.ForeColor = System.Drawing.Color.Black
        Me.cboTodosProvxEmpr.FormattingEnabled = True
        Me.cboTodosProvxEmpr.KeyEnter = True
        Me.cboTodosProvxEmpr.Location = New System.Drawing.Point(9, 43)
        Me.cboTodosProvxEmpr.Name = "cboTodosProvxEmpr"
        Me.cboTodosProvxEmpr.Size = New System.Drawing.Size(650, 21)
        Me.cboTodosProvxEmpr.TabIndex = 6
        '
        'chkTodosxEmpr
        '
        Me.chkTodosxEmpr.AutoSize = True
        Me.chkTodosxEmpr.Checked = True
        Me.chkTodosxEmpr.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTodosxEmpr.Location = New System.Drawing.Point(315, 20)
        Me.chkTodosxEmpr.Name = "chkTodosxEmpr"
        Me.chkTodosxEmpr.Size = New System.Drawing.Size(56, 17)
        Me.chkTodosxEmpr.TabIndex = 5
        Me.chkTodosxEmpr.Text = "Todos"
        Me.chkTodosxEmpr.UseVisualStyleBackColor = True
        '
        'rdbDetalladoEmpr
        '
        Me.rdbDetalladoEmpr.AutoSize = True
        Me.rdbDetalladoEmpr.Checked = True
        Me.rdbDetalladoEmpr.Location = New System.Drawing.Point(499, 73)
        Me.rdbDetalladoEmpr.Name = "rdbDetalladoEmpr"
        Me.rdbDetalladoEmpr.Size = New System.Drawing.Size(70, 17)
        Me.rdbDetalladoEmpr.TabIndex = 4
        Me.rdbDetalladoEmpr.TabStop = True
        Me.rdbDetalladoEmpr.Text = "Detallado"
        Me.rdbDetalladoEmpr.UseVisualStyleBackColor = True
        '
        'rdbResumidoEmpr
        '
        Me.rdbResumidoEmpr.AutoSize = True
        Me.rdbResumidoEmpr.Location = New System.Drawing.Point(421, 73)
        Me.rdbResumidoEmpr.Name = "rdbResumidoEmpr"
        Me.rdbResumidoEmpr.Size = New System.Drawing.Size(72, 17)
        Me.rdbResumidoEmpr.TabIndex = 3
        Me.rdbResumidoEmpr.TabStop = True
        Me.rdbResumidoEmpr.Text = "Resumido"
        Me.rdbResumidoEmpr.UseVisualStyleBackColor = True
        '
        'btnVerTodosEmpr
        '
        Me.btnVerTodosEmpr.Location = New System.Drawing.Point(584, 72)
        Me.btnVerTodosEmpr.Name = "btnVerTodosEmpr"
        Me.btnVerTodosEmpr.Size = New System.Drawing.Size(75, 23)
        Me.btnVerTodosEmpr.TabIndex = 2
        Me.btnVerTodosEmpr.Text = "Ver"
        Me.btnVerTodosEmpr.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.dtpF2)
        Me.GroupBox4.Controls.Add(Me.dtpF1)
        Me.GroupBox4.Controls.Add(Me.Opcion2)
        Me.GroupBox4.Controls.Add(Me.Opcion1)
        Me.GroupBox4.Controls.Add(Me.btnVerPrestamos)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 237)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(665, 90)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(467, 48)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(12, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "y"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(328, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Entre"
        '
        'dtpF2
        '
        Me.dtpF2.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpF2.CustomFormat = ""
        Me.dtpF2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF2.KeyEnter = True
        Me.dtpF2.Location = New System.Drawing.Point(485, 41)
        Me.dtpF2.Name = "dtpF2"
        Me.dtpF2.Size = New System.Drawing.Size(92, 20)
        Me.dtpF2.TabIndex = 9
        Me.dtpF2.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpF2.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpF1
        '
        Me.dtpF1.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpF1.CustomFormat = ""
        Me.dtpF1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpF1.KeyEnter = True
        Me.dtpF1.Location = New System.Drawing.Point(366, 41)
        Me.dtpF1.Name = "dtpF1"
        Me.dtpF1.Size = New System.Drawing.Size(92, 20)
        Me.dtpF1.TabIndex = 8
        Me.dtpF1.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpF1.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Opcion2
        '
        Me.Opcion2.AutoSize = True
        Me.Opcion2.Location = New System.Drawing.Point(8, 45)
        Me.Opcion2.Name = "Opcion2"
        Me.Opcion2.Size = New System.Drawing.Size(300, 17)
        Me.Opcion2.TabIndex = 7
        Me.Opcion2.Text = "Facturas de esta Empresa Canceladas por otras Empresas"
        Me.Opcion2.UseVisualStyleBackColor = True
        '
        'Opcion1
        '
        Me.Opcion1.AutoSize = True
        Me.Opcion1.Checked = True
        Me.Opcion1.Location = New System.Drawing.Point(8, 27)
        Me.Opcion1.Name = "Opcion1"
        Me.Opcion1.Size = New System.Drawing.Size(300, 17)
        Me.Opcion1.TabIndex = 6
        Me.Opcion1.TabStop = True
        Me.Opcion1.Text = "Facturas de otras Empresas Canceladas por esta Empresa"
        Me.Opcion1.UseVisualStyleBackColor = True
        '
        'btnVerPrestamos
        '
        Me.btnVerPrestamos.Location = New System.Drawing.Point(584, 39)
        Me.btnVerPrestamos.Name = "btnVerPrestamos"
        Me.btnVerPrestamos.Size = New System.Drawing.Size(75, 23)
        Me.btnVerPrestamos.TabIndex = 5
        Me.btnVerPrestamos.Text = "Ver"
        Me.btnVerPrestamos.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.cboTodosProv)
        Me.GroupBox1.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox1.Controls.Add(Me.chkTodos2)
        Me.GroupBox1.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox1.Controls.Add(Me.rdbDetallado)
        Me.GroupBox1.Controls.Add(Me.rdbResumido)
        Me.GroupBox1.Controls.Add(Me.btnVerTodos)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(665, 106)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Reporte General de Facturas por Pagar Por Todas las Empresas"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(139, 81)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(12, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "y"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(191, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Seleccione Todos o un solo Proveedor"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 81)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(32, 13)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "Entre"
        '
        'cboTodosProv
        '
        Me.cboTodosProv.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTodosProv.BackColor = System.Drawing.Color.Ivory
        Me.cboTodosProv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTodosProv.ForeColor = System.Drawing.Color.Black
        Me.cboTodosProv.FormattingEnabled = True
        Me.cboTodosProv.KeyEnter = True
        Me.cboTodosProv.Location = New System.Drawing.Point(9, 43)
        Me.cboTodosProv.Name = "cboTodosProv"
        Me.cboTodosProv.Size = New System.Drawing.Size(650, 21)
        Me.cboTodosProv.TabIndex = 6
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(154, 77)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(92, 20)
        Me.dtpFechaFin.TabIndex = 13
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'chkTodos2
        '
        Me.chkTodos2.AutoSize = True
        Me.chkTodos2.Checked = True
        Me.chkTodos2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTodos2.Location = New System.Drawing.Point(315, 20)
        Me.chkTodos2.Name = "chkTodos2"
        Me.chkTodos2.Size = New System.Drawing.Size(56, 17)
        Me.chkTodos2.TabIndex = 5
        Me.chkTodos2.Text = "Todos"
        Me.chkTodos2.UseVisualStyleBackColor = True
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(44, 77)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(92, 20)
        Me.dtpFechaIni.TabIndex = 12
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'rdbDetallado
        '
        Me.rdbDetallado.AutoSize = True
        Me.rdbDetallado.Checked = True
        Me.rdbDetallado.Location = New System.Drawing.Point(499, 73)
        Me.rdbDetallado.Name = "rdbDetallado"
        Me.rdbDetallado.Size = New System.Drawing.Size(70, 17)
        Me.rdbDetallado.TabIndex = 4
        Me.rdbDetallado.TabStop = True
        Me.rdbDetallado.Text = "Detallado"
        Me.rdbDetallado.UseVisualStyleBackColor = True
        '
        'rdbResumido
        '
        Me.rdbResumido.AutoSize = True
        Me.rdbResumido.Location = New System.Drawing.Point(421, 73)
        Me.rdbResumido.Name = "rdbResumido"
        Me.rdbResumido.Size = New System.Drawing.Size(72, 17)
        Me.rdbResumido.TabIndex = 3
        Me.rdbResumido.TabStop = True
        Me.rdbResumido.Text = "Resumido"
        Me.rdbResumido.UseVisualStyleBackColor = True
        '
        'btnVerTodos
        '
        Me.btnVerTodos.Location = New System.Drawing.Point(584, 70)
        Me.btnVerTodos.Name = "btnVerTodos"
        Me.btnVerTodos.Size = New System.Drawing.Size(75, 23)
        Me.btnVerTodos.TabIndex = 2
        Me.btnVerTodos.Text = "Ver"
        Me.btnVerTodos.UseVisualStyleBackColor = True
        '
        'frmEstadoCuentaProvDoc
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 588)
        Me.Controls.Add(Me.Panel1)
        Me.MaximizeBox = False
        Me.Name = "frmEstadoCuentaProvDoc"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Estado Saldo de Proveedores"
        Me.Panel1.ResumeLayout(False)
        Me.TabPersonalizado1.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvProveedores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvDocumentos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents TabPersonalizado1 As Controles_DHMont.TabPersonalizado
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbDescr As System.Windows.Forms.RadioButton
    Friend WithEvents txtFiltrar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents rdbRuc As System.Windows.Forms.RadioButton
    Friend WithEvents dgvProveedores As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvDocumentos As System.Windows.Forms.DataGridView
    Friend WithEvents lblEtiqueta As ctrLibreria.Controles.BeLabel
    Friend WithEvents lblMoneda As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnVer As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CountDoc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkTodos As System.Windows.Forms.CheckBox
    Friend WithEvents txtSol As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel29 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboTodosProv As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkTodos2 As System.Windows.Forms.CheckBox
    Friend WithEvents rdbDetallado As System.Windows.Forms.RadioButton
    Friend WithEvents rdbResumido As System.Windows.Forms.RadioButton
    Friend WithEvents btnVerTodos As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Opcion2 As System.Windows.Forms.RadioButton
    Friend WithEvents Opcion1 As System.Windows.Forms.RadioButton
    Friend WithEvents btnVerPrestamos As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpF2 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpF1 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboTodosProvxEmpr As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkTodosxEmpr As System.Windows.Forms.CheckBox
    Friend WithEvents rdbDetalladoEmpr As System.Windows.Forms.RadioButton
    Friend WithEvents rdbResumidoEmpr As System.Windows.Forms.RadioButton
    Friend WithEvents btnVerTodosEmpr As System.Windows.Forms.Button
    Friend WithEvents dtpFechaFinal As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdDocumento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaInicio As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboTodosSubC As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeDateTimePicker1 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents chkTodosSubC As System.Windows.Forms.CheckBox
    Friend WithEvents BeDateTimePicker2 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents btnVerTodosSubC As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents BeDateTimePicker3 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents BeDateTimePicker4 As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboTodosSubCxEmpr As ctrLibreria.Controles.BeComboBox
    Friend WithEvents chkTodosEmprSubc As System.Windows.Forms.CheckBox
    Friend WithEvents rdbDetalladoSEmpr As System.Windows.Forms.RadioButton
    Friend WithEvents rdbResumidoSEmpr As System.Windows.Forms.RadioButton
    Friend WithEvents btnVerTodosEmprSubC As System.Windows.Forms.Button
End Class
