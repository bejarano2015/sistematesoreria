<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuarioAreasObras
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DgvListaAreas = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DgvListaObras = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ObraCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Activo = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel
        CType(Me.DgvListaAreas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DgvListaObras, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DgvListaAreas
        '
        Me.DgvListaAreas.AllowUserToAddRows = False
        Me.DgvListaAreas.BackgroundColor = System.Drawing.Color.White
        Me.DgvListaAreas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvListaAreas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3})
        Me.DgvListaAreas.EnableHeadersVisualStyles = False
        Me.DgvListaAreas.Location = New System.Drawing.Point(12, 54)
        Me.DgvListaAreas.Name = "DgvListaAreas"
        Me.DgvListaAreas.RowHeadersVisible = False
        Me.DgvListaAreas.Size = New System.Drawing.Size(427, 110)
        Me.DgvListaAreas.TabIndex = 2
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "AreaNombre"
        Me.Column1.HeaderText = "Area"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 300
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "AreaCodigo"
        Me.Column2.HeaderText = "AreaCodigo"
        Me.Column2.Name = "Column2"
        Me.Column2.Visible = False
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Activo"
        Me.Column3.HeaderText = "Activo"
        Me.Column3.Name = "Column3"
        '
        'DgvListaObras
        '
        Me.DgvListaObras.AllowUserToAddRows = False
        Me.DgvListaObras.BackgroundColor = System.Drawing.Color.White
        Me.DgvListaObras.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DgvListaObras.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.ObraCodigo, Me.Activo})
        Me.DgvListaObras.EnableHeadersVisualStyles = False
        Me.DgvListaObras.Location = New System.Drawing.Point(12, 195)
        Me.DgvListaObras.Name = "DgvListaObras"
        Me.DgvListaObras.RowHeadersVisible = False
        Me.DgvListaObras.Size = New System.Drawing.Size(427, 112)
        Me.DgvListaObras.TabIndex = 3
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "ObraDescripcion"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Obra"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 300
        '
        'ObraCodigo
        '
        Me.ObraCodigo.DataPropertyName = "ObraCodigo"
        Me.ObraCodigo.HeaderText = "ObraCodigo"
        Me.ObraCodigo.Name = "ObraCodigo"
        Me.ObraCodigo.Visible = False
        '
        'Activo
        '
        Me.Activo.DataPropertyName = "Activo"
        Me.Activo.HeaderText = "Activo"
        Me.Activo.Name = "Activo"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(9, 38)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel1.TabIndex = 6
        Me.BeLabel1.Text = "Areas"
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BeLabel2.ForeColor = System.Drawing.Color.Black
        Me.BeLabel2.Location = New System.Drawing.Point(9, 179)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(45, 13)
        Me.BeLabel2.TabIndex = 7
        Me.BeLabel2.Text = "Obras"
        '
        'frmUsuarioAreasObras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(451, 319)
        Me.Controls.Add(Me.BeLabel2)
        Me.Controls.Add(Me.BeLabel1)
        Me.Controls.Add(Me.DgvListaObras)
        Me.Controls.Add(Me.DgvListaAreas)
        Me.MaximizeBox = False
        Me.Name = "frmUsuarioAreasObras"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.DgvListaAreas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DgvListaObras, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DgvListaAreas As System.Windows.Forms.DataGridView
    Friend WithEvents DgvListaObras As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ObraCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Activo As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
