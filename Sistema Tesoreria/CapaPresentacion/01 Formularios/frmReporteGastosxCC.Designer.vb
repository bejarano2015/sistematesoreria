<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteGastosxCC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cboCentroCosto = New ctrLibreria.Controles.BeComboBox
        Me.rdbDet = New System.Windows.Forms.RadioButton
        Me.rdbRes = New System.Windows.Forms.RadioButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboCentroCosto)
        Me.GroupBox1.Controls.Add(Me.rdbDet)
        Me.GroupBox1.Controls.Add(Me.rdbRes)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox1.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(723, 48)
        Me.GroupBox1.TabIndex = 15
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Buscar por Fechas"
        '
        'cboCentroCosto
        '
        Me.cboCentroCosto.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCentroCosto.BackColor = System.Drawing.Color.Ivory
        Me.cboCentroCosto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroCosto.ForeColor = System.Drawing.Color.Black
        Me.cboCentroCosto.FormattingEnabled = True
        Me.cboCentroCosto.KeyEnter = True
        Me.cboCentroCosto.Location = New System.Drawing.Point(20, 17)
        Me.cboCentroCosto.Name = "cboCentroCosto"
        Me.cboCentroCosto.Size = New System.Drawing.Size(216, 21)
        Me.cboCentroCosto.TabIndex = 13
        '
        'rdbDet
        '
        Me.rdbDet.AutoSize = True
        Me.rdbDet.Location = New System.Drawing.Point(330, 19)
        Me.rdbDet.Name = "rdbDet"
        Me.rdbDet.Size = New System.Drawing.Size(70, 17)
        Me.rdbDet.TabIndex = 12
        Me.rdbDet.TabStop = True
        Me.rdbDet.Text = "Detallado"
        Me.rdbDet.UseVisualStyleBackColor = True
        '
        'rdbRes
        '
        Me.rdbRes.AutoSize = True
        Me.rdbRes.Location = New System.Drawing.Point(252, 19)
        Me.rdbRes.Name = "rdbRes"
        Me.rdbRes.Size = New System.Drawing.Size(72, 17)
        Me.rdbRes.TabIndex = 11
        Me.rdbRes.TabStop = True
        Me.rdbRes.Text = "Resumido"
        Me.rdbRes.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(524, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(16, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Al"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(636, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(406, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(23, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Del"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(546, 18)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaFin.TabIndex = 3
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(435, 18)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaIni.TabIndex = 2
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(2009, 7, 15, 0, 0, 0, 0)
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(2, 60)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(1060, 540)
        Me.CrystalReportViewer1.TabIndex = 14
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'frmReporteGastosxCC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1065, 602)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CrystalReportViewer1)
        Me.Name = "frmReporteGastosxCC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Reporte de Gastos Por Centro de Costo"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbRes As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents cboCentroCosto As ctrLibreria.Controles.BeComboBox
    Friend WithEvents rdbDet As System.Windows.Forms.RadioButton
End Class
