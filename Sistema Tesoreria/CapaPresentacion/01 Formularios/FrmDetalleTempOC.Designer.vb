<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetalleTempOC
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvDetalleOC = New System.Windows.Forms.DataGridView
        Me.sel = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblValCompra = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtTotalDet = New System.Windows.Forms.TextBox
        CType(Me.dgvDetalleOC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvDetalleOC
        '
        Me.dgvDetalleOC.AllowUserToAddRows = False
        Me.dgvDetalleOC.BackgroundColor = System.Drawing.Color.White
        Me.dgvDetalleOC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleOC.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.sel})
        Me.dgvDetalleOC.Location = New System.Drawing.Point(12, 33)
        Me.dgvDetalleOC.Name = "dgvDetalleOC"
        Me.dgvDetalleOC.RowHeadersVisible = False
        Me.dgvDetalleOC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalleOC.Size = New System.Drawing.Size(598, 179)
        Me.dgvDetalleOC.TabIndex = 11
        '
        'sel
        '
        Me.sel.FalseValue = "0"
        Me.sel.HeaderText = "sel"
        Me.sel.Name = "sel"
        Me.sel.TrueValue = "1"
        Me.sel.Width = 30
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 227)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Valor de Compra :"
        '
        'lblValCompra
        '
        Me.lblValCompra.AutoSize = True
        Me.lblValCompra.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValCompra.Location = New System.Drawing.Point(122, 228)
        Me.lblValCompra.Name = "lblValCompra"
        Me.lblValCompra.Size = New System.Drawing.Size(0, 13)
        Me.lblValCompra.TabIndex = 13
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(304, 227)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 16)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "="
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(499, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "F5 : Salir y Grabar"
        '
        'txtTotalDet
        '
        Me.txtTotalDet.BackColor = System.Drawing.Color.LightSteelBlue
        Me.txtTotalDet.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtTotalDet.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalDet.Location = New System.Drawing.Point(486, 228)
        Me.txtTotalDet.Name = "txtTotalDet"
        Me.txtTotalDet.Size = New System.Drawing.Size(123, 13)
        Me.txtTotalDet.TabIndex = 17
        Me.txtTotalDet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'FrmDetalleTempOC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(622, 252)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtTotalDet)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblValCompra)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvDetalleOC)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "FrmDetalleTempOC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Detalle Temporal O/C"
        CType(Me.dgvDetalleOC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvDetalleOC As System.Windows.Forms.DataGridView
    Friend WithEvents sel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblValCompra As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTotalDet As System.Windows.Forms.TextBox
End Class
