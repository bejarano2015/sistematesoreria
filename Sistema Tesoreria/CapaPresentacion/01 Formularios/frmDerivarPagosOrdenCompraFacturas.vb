﻿
Imports CapaEntidad
Imports BusinessLogicLayer.Procesos

Public Class frmDerivarPagosOrdenCompraFacturas

    Public Property VL_PROVEEDOR As String
    Public Property VL_RUC As String
    Public Property VL_USUARIO As String
    Public Property VL_OCOCODIGO As String
    Public Property VL_PDOCODIGO As String
    Public Property VL_NOMBREUSUARIO As String
    Public Property VL_OCONUMERO As String
    Public Property VL_IDREGISTRO As String
    Public Property VL_EMPRCODIGO As String

#Region "Procedimientos"

    Private Sub ListarFacturas()
        Dim VL_SrvDerivar As SrvDerivar = New SrvDerivar()
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New BeanResultado.ResultadoSeleccion

        Try
            VL_BeanResultado = VL_SrvDerivar.Fnc_Listar_Facturas_derivadas(VL_OCOCODIGO, VL_PDOCODIGO)
            If VL_BeanResultado.blnExiste = True Then

                dgvLista.DataSource = VL_BeanResultado.dtResultado

            Else

                MessageBox.Show("No se encontró", "Mensaje del sistema")

            End If


        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

    Private Sub DerivarFactura(ByVal VI_IDREGISTRO As String, ByVal VI_EMPRCODIGO As String)
        Dim VL_SrvDerivar As SrvDerivar = New SrvDerivar()
        Dim VL_BeanResultado As BeanResultado.ResultadoTransaccion = New BeanResultado.ResultadoTransaccion

        Try
            VL_BeanResultado = VL_SrvDerivar.Fnc_Derivar_Factura(VI_IDREGISTRO, VI_EMPRCODIGO, VL_OCOCODIGO, VL_PDOCODIGO)

            If VL_BeanResultado.blnResultado = True Then
                MessageBox.Show("Se derivó satisfactoriamente...", "Mensaje del sistema")
            Else
                MessageBox.Show("Error al derivar", "Mensaje del sistema")
            End If

        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Mensaje del Sistema")
        End Try

    End Sub

#End Region


    Private Sub frmDerivarPagosOrdenCompraFacturas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListarFacturas()


    End Sub

    Private Sub btnDerivar_Click(sender As Object, e As EventArgs) Handles btnDerivar.Click

        Dim i As Integer

        For i = 0 To dgvLista.Rows.Count - 1

            If dgvLista.Rows(i).Cells("Seleccion").Value = True Then
                DerivarFactura(dgvLista.Rows(i).Cells("IDREGISTRO").Value.ToString, dgvLista.Rows(i).Cells("EMPRCODIGO").Value.ToString)
            End If



        Next

        ListarFacturas()

    End Sub
End Class