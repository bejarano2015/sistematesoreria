<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaGastos3
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label7 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chkPorRendir = New System.Windows.Forms.CheckBox
        Me.chkRendidos = New System.Windows.Forms.CheckBox
        Me.rdbPorRecibir = New System.Windows.Forms.RadioButton
        Me.rdbPorRecibirdeAdministracion = New System.Windows.Forms.RadioButton
        Me.rdbRecibidosxRendir = New System.Windows.Forms.RadioButton
        Me.rdbRecibidos = New System.Windows.Forms.RadioButton
        Me.Label6 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnImprimir = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtTexto = New ctrLibreria.Controles.BeTextBox
        Me.cboCriterio = New ctrLibreria.Controles.BeComboBox
        Me.dtpFechaFin = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaIni = New ctrLibreria.Controles.BeDateTimePicker
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.dgvGastos = New System.Windows.Forms.DataGridView
        Me.btnEnviar = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.tipExample1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Concepto = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column14 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column20 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column16 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvGastos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(462, 433)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(61, 13)
        Me.Label7.TabIndex = 34
        Me.Label7.Text = "Prestamo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkPorRendir)
        Me.GroupBox2.Controls.Add(Me.chkRendidos)
        Me.GroupBox2.Controls.Add(Me.rdbPorRecibir)
        Me.GroupBox2.Controls.Add(Me.rdbPorRecibirdeAdministracion)
        Me.GroupBox2.Controls.Add(Me.rdbRecibidosxRendir)
        Me.GroupBox2.Controls.Add(Me.rdbRecibidos)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 11)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(419, 78)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Opción de Búsqueda"
        '
        'chkPorRendir
        '
        Me.chkPorRendir.AutoSize = True
        Me.chkPorRendir.Location = New System.Drawing.Point(98, 46)
        Me.chkPorRendir.Name = "chkPorRendir"
        Me.chkPorRendir.Size = New System.Drawing.Size(76, 17)
        Me.chkPorRendir.TabIndex = 38
        Me.chkPorRendir.Text = "Por Rendir"
        Me.chkPorRendir.UseVisualStyleBackColor = True
        '
        'chkRendidos
        '
        Me.chkRendidos.AutoSize = True
        Me.chkRendidos.Location = New System.Drawing.Point(98, 21)
        Me.chkRendidos.Name = "chkRendidos"
        Me.chkRendidos.Size = New System.Drawing.Size(71, 17)
        Me.chkRendidos.TabIndex = 37
        Me.chkRendidos.Text = "Rendidos"
        Me.chkRendidos.UseVisualStyleBackColor = True
        '
        'rdbPorRecibir
        '
        Me.rdbPorRecibir.AutoSize = True
        Me.rdbPorRecibir.Location = New System.Drawing.Point(11, 33)
        Me.rdbPorRecibir.Name = "rdbPorRecibir"
        Me.rdbPorRecibir.Size = New System.Drawing.Size(81, 17)
        Me.rdbPorRecibir.TabIndex = 1
        Me.rdbPorRecibir.TabStop = True
        Me.rdbPorRecibir.Text = "Por Aceptar"
        Me.rdbPorRecibir.UseVisualStyleBackColor = True
        '
        'rdbPorRecibirdeAdministracion
        '
        Me.rdbPorRecibirdeAdministracion.AutoSize = True
        Me.rdbPorRecibirdeAdministracion.Location = New System.Drawing.Point(205, 55)
        Me.rdbPorRecibirdeAdministracion.Name = "rdbPorRecibirdeAdministracion"
        Me.rdbPorRecibirdeAdministracion.Size = New System.Drawing.Size(154, 17)
        Me.rdbPorRecibirdeAdministracion.TabIndex = 3
        Me.rdbPorRecibirdeAdministracion.TabStop = True
        Me.rdbPorRecibirdeAdministracion.Text = "Por Recibir (Administración)"
        Me.rdbPorRecibirdeAdministracion.UseVisualStyleBackColor = True
        '
        'rdbRecibidosxRendir
        '
        Me.rdbRecibidosxRendir.AutoSize = True
        Me.rdbRecibidosxRendir.Location = New System.Drawing.Point(205, 10)
        Me.rdbRecibidosxRendir.Name = "rdbRecibidosxRendir"
        Me.rdbRecibidosxRendir.Size = New System.Drawing.Size(129, 17)
        Me.rdbRecibidosxRendir.TabIndex = 2
        Me.rdbRecibidosxRendir.TabStop = True
        Me.rdbRecibidosxRendir.Text = "Aceptados Por Rendir"
        Me.rdbRecibidosxRendir.UseVisualStyleBackColor = True
        '
        'rdbRecibidos
        '
        Me.rdbRecibidos.AutoSize = True
        Me.rdbRecibidos.Location = New System.Drawing.Point(205, 33)
        Me.rdbRecibidos.Name = "rdbRecibidos"
        Me.rdbRecibidos.Size = New System.Drawing.Size(124, 17)
        Me.rdbRecibidos.TabIndex = 0
        Me.rdbRecibidos.TabStop = True
        Me.rdbRecibidos.Text = "Aceptados Rendidos"
        Me.rdbRecibidos.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(564, 433)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(93, 13)
        Me.Label6.TabIndex = 33
        Me.Label6.Text = "Varios Detalles"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Silver
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Location = New System.Drawing.Point(529, 432)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(33, 14)
        Me.Panel2.TabIndex = 32
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Aqua
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(663, 432)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(33, 14)
        Me.Panel1.TabIndex = 31
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(752, 431)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 30
        Me.Label5.Text = "Label5"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnImprimir)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtTexto)
        Me.GroupBox1.Controls.Add(Me.cboCriterio)
        Me.GroupBox1.Controls.Add(Me.dtpFechaFin)
        Me.GroupBox1.Controls.Add(Me.dtpFechaIni)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(692, 138)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        '
        'btnImprimir
        '
        Me.btnImprimir.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Location = New System.Drawing.Point(609, 106)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 13
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label4.Location = New System.Drawing.Point(517, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Fecha Final"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(428, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(78, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Fecha Inicial"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label2.Location = New System.Drawing.Point(134, 94)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Texto"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Location = New System.Drawing.Point(3, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Criterio"
        '
        'txtTexto
        '
        Me.txtTexto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtTexto.BackColor = System.Drawing.Color.Ivory
        Me.txtTexto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTexto.ForeColor = System.Drawing.Color.Black
        Me.txtTexto.KeyEnter = True
        Me.txtTexto.Location = New System.Drawing.Point(137, 108)
        Me.txtTexto.MaxLength = 100
        Me.txtTexto.Name = "txtTexto"
        Me.txtTexto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtTexto.ShortcutsEnabled = False
        Me.txtTexto.Size = New System.Drawing.Size(288, 20)
        Me.txtTexto.TabIndex = 6
        Me.txtTexto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboCriterio
        '
        Me.cboCriterio.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCriterio.BackColor = System.Drawing.Color.Ivory
        Me.cboCriterio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCriterio.ForeColor = System.Drawing.Color.Black
        Me.cboCriterio.FormattingEnabled = True
        Me.cboCriterio.Items.AddRange(New Object() {"CONCEPTO", "RESPONSABLE", "CHEQUE", "MACRO", "CARTA", "TRANSFERENCIA", "OTROS"})
        Me.cboCriterio.KeyEnter = True
        Me.cboCriterio.Location = New System.Drawing.Point(6, 107)
        Me.cboCriterio.Name = "cboCriterio"
        Me.cboCriterio.Size = New System.Drawing.Size(121, 21)
        Me.cboCriterio.TabIndex = 5
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaFin.CustomFormat = ""
        Me.dtpFechaFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFin.KeyEnter = True
        Me.dtpFechaFin.Location = New System.Drawing.Point(520, 108)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(84, 20)
        Me.dtpFechaFin.TabIndex = 3
        Me.dtpFechaFin.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaFin.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'dtpFechaIni
        '
        Me.dtpFechaIni.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaIni.CustomFormat = ""
        Me.dtpFechaIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIni.KeyEnter = True
        Me.dtpFechaIni.Location = New System.Drawing.Point(431, 108)
        Me.dtpFechaIni.Name = "dtpFechaIni"
        Me.dtpFechaIni.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaIni.TabIndex = 2
        Me.dtpFechaIni.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaIni.Value = New Date(2009, 8, 7, 0, 0, 0, 0)
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(723, 117)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(56, 17)
        Me.CheckBox1.TabIndex = 36
        Me.CheckBox1.Text = "Todos"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'dgvGastos
        '
        Me.dgvGastos.AllowUserToAddRows = False
        Me.dgvGastos.BackgroundColor = System.Drawing.Color.White
        Me.dgvGastos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGastos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Fecha, Me.Concepto, Me.Column11, Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column12, Me.Column14, Me.Column15, Me.Column21, Me.Column20, Me.Column19, Me.Column16, Me.Column17, Me.Column18, Me.Column13})
        Me.dgvGastos.EnableHeadersVisualStyles = False
        Me.dgvGastos.Location = New System.Drawing.Point(5, 148)
        Me.dgvGastos.Name = "dgvGastos"
        Me.dgvGastos.RowHeadersVisible = False
        Me.dgvGastos.Size = New System.Drawing.Size(1070, 280)
        Me.dgvGastos.TabIndex = 28
        '
        'btnEnviar
        '
        Me.btnEnviar.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviar.Location = New System.Drawing.Point(785, 111)
        Me.btnEnviar.Name = "btnEnviar"
        Me.btnEnviar.Size = New System.Drawing.Size(83, 23)
        Me.btnEnviar.TabIndex = 37
        Me.btnEnviar.Text = "Procesar"
        Me.btnEnviar.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(2, 433)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(212, 13)
        Me.Label8.TabIndex = 38
        Me.Label8.Text = "F2 = Detalle de Requerimientos"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(358, 434)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 13)
        Me.Label9.TabIndex = 40
        Me.Label9.Text = "Rendidos"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.LightGreen
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Location = New System.Drawing.Point(423, 433)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(33, 14)
        Me.Panel3.TabIndex = 39
        '
        'Button1
        '
        Me.Button1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Button1.Location = New System.Drawing.Point(962, 430)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(114, 23)
        Me.Button1.TabIndex = 41
        Me.Button1.Text = "Exportar a Excel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'tipExample1
        '
        Me.tipExample1.AutoPopDelay = 5000
        Me.tipExample1.InitialDelay = 100
        Me.tipExample1.ReshowDelay = 100
        '
        'Timer1
        '
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "FechaMovimiento"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle1
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.Width = 70
        '
        'Concepto
        '
        Me.Concepto.DataPropertyName = "Glosa"
        Me.Concepto.HeaderText = "Concepto"
        Me.Concepto.Name = "Concepto"
        Me.Concepto.Width = 70
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "Beneficiario"
        Me.Column11.HeaderText = "Beneficiario"
        Me.Column11.Name = "Column11"
        Me.Column11.Width = 70
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "CCosDescripcion"
        Me.Column1.HeaderText = "C. Costo"
        Me.Column1.Name = "Column1"
        Me.Column1.Width = 75
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "Abreviado"
        Me.Column2.HeaderText = "Banco"
        Me.Column2.Name = "Column2"
        Me.Column2.Width = 60
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "NroFormaPago"
        Me.Column3.HeaderText = "Nº Tran."
        Me.Column3.Name = "Column3"
        Me.Column3.Width = 75
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "DescripcionRendicion"
        Me.Column4.HeaderText = "Trans."
        Me.Column4.Name = "Column4"
        Me.Column4.Width = 55
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "MonSimbolo"
        Me.Column5.HeaderText = "M"
        Me.Column5.Name = "Column5"
        Me.Column5.Width = 25
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "Monto"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column6.HeaderText = "Total"
        Me.Column6.Name = "Column6"
        Me.Column6.Width = 60
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "Prestamo"
        Me.Column7.HeaderText = "P"
        Me.Column7.Name = "Column7"
        Me.Column7.Visible = False
        Me.Column7.Width = 30
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "CountDet"
        Me.Column8.HeaderText = "C"
        Me.Column8.Name = "Column8"
        Me.Column8.Visible = False
        Me.Column8.Width = 30
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "IdMovimiento"
        Me.Column9.HeaderText = "IdMovimento"
        Me.Column9.Name = "Column9"
        Me.Column9.Visible = False
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "EstadoEnvio"
        Me.Column10.HeaderText = "EstadoEnviado"
        Me.Column10.Name = "Column10"
        Me.Column10.Visible = False
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "EstadoRecibo"
        Me.Column12.HeaderText = "EstadoRecibo"
        Me.Column12.Name = "Column12"
        Me.Column12.Visible = False
        '
        'Column14
        '
        Me.Column14.HeaderText = "Rechaz"
        Me.Column14.Name = "Column14"
        Me.Column14.Width = 50
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "FechaEnvio"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.Column15.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column15.HeaderText = "F. Envio"
        Me.Column15.Name = "Column15"
        Me.Column15.Width = 75
        '
        'Column21
        '
        Me.Column21.DataPropertyName = "FechaEnviox"
        Me.Column21.HeaderText = "FechaEnviox"
        Me.Column21.Name = "Column21"
        Me.Column21.Visible = False
        '
        'Column20
        '
        Me.Column20.HeaderText = "Rendir/Rendido"
        Me.Column20.Name = "Column20"
        Me.Column20.Width = 85
        '
        'Column19
        '
        Me.Column19.DataPropertyName = "EstadoRecibo1"
        Me.Column19.HeaderText = "Estado"
        Me.Column19.Name = "Column19"
        Me.Column19.Width = 60
        '
        'Column16
        '
        Me.Column16.HeaderText = "Aceptar"
        Me.Column16.Name = "Column16"
        Me.Column16.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Column16.Width = 60
        '
        'Column17
        '
        Me.Column17.DataPropertyName = "FechaRecibo"
        DataGridViewCellStyle4.Format = "d"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.Column17.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column17.HeaderText = "F. Recibo"
        Me.Column17.Name = "Column17"
        Me.Column17.Width = 80
        '
        'Column18
        '
        Me.Column18.DataPropertyName = "ObservacionRechazo"
        Me.Column18.HeaderText = "Observación"
        Me.Column18.Name = "Column18"
        Me.Column18.Width = 80
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "IdRendicion"
        Me.Column13.HeaderText = "IdRendicion"
        Me.Column13.Name = "Column13"
        Me.Column13.Visible = False
        '
        'frmConsultaGastos3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(1080, 455)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.dgvGastos)
        Me.Controls.Add(Me.btnEnviar)
        Me.MaximizeBox = False
        Me.Name = "frmConsultaGastos3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Control de Rendiciones - Contabilidad"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvGastos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbPorRecibir As System.Windows.Forms.RadioButton
    Friend WithEvents rdbRecibidos As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTexto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboCriterio As ctrLibreria.Controles.BeComboBox
    Friend WithEvents dtpFechaFin As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaIni As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents dgvGastos As System.Windows.Forms.DataGridView
    Friend WithEvents btnEnviar As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents rdbRecibidosxRendir As System.Windows.Forms.RadioButton
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents rdbPorRecibirdeAdministracion As System.Windows.Forms.RadioButton
    Friend WithEvents chkPorRendir As System.Windows.Forms.CheckBox
    Friend WithEvents chkRendidos As System.Windows.Forms.CheckBox
    Friend WithEvents tipExample1 As System.Windows.Forms.ToolTip
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Concepto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
