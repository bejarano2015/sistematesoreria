Imports CapaNegocios
Imports CapaEntidad

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporGestionDocumentoPagos

    'Public nroArqueo As String
    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    'Dim rptGastos As New rptGasGene
    'Dim rptGastos As New rptGasGeneDetallado

    Private eGestionDocumentosPagos As clsGestionDocumentosPagos
    Private odtGestionPago As New DataTable
    Private rptPagos As New rptGestionDocumentosPagos

#Region "Singlenton  = Instancia Unica de Objeto"

    Private Shared frmInstance As frmReporGestionDocumentoPagos = Nothing
    Public Shared Function Instance() As frmReporGestionDocumentoPagos
        If frmInstance Is Nothing Then
            frmInstance = New frmReporGestionDocumentoPagos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function

#End Region

    Private Sub frmReporGastos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eGestionDocumentosPagos = New clsGestionDocumentosPagos()

        CargarReportePagos()
    End Sub

    Private Sub frmReporGastos_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmPrincipal.Barra.Enabled = False
    End Sub
    Private Sub frmReporGastos_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

    Private Sub CargarReportePagos()
        Try
            'Dim odtData As New DataTable
            Dim opcion = frmDerivarPagosOrdenCompra.opcion
            Dim docOrigen = frmDerivarPagosOrdenCompra.docOrigen
            Dim id_DocPendiente = frmDerivarPagosOrdenCompra.id_DocPendiente
            Dim nombreorigen = frmDerivarPagosOrdenCompra.nombreorigen

            odtGestionPago = eGestionDocumentosPagos.fCargarReporte(docOrigen, id_DocPendiente, opcion)

            Dim rptPagos As New rptGestionDocumentosPagos()
            rptPagos.DataSource = odtGestionPago
            rptPagos.AddItems(gDesEmpresa, gEmprRuc, nombreorigen)

            Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
            instanceReportSource.ReportDocument = rptPagos
            rvReporte.ReportSource = instanceReportSource
            rvReporte.RefreshReport()
        Catch ex As Exception
            MessageBox.Show("No se pudo cargar el registro. " & ex.Message, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

End Class