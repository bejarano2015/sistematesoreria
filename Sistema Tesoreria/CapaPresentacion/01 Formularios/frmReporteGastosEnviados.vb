Imports CapaNegocios
Imports CapaDatos
Imports System.Transactions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports vb = Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data

Public Class frmReporteGastosEnviados

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteGastosEnviados = Nothing
    Public Shared Function Instance() As frmReporteGastosEnviados
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteGastosEnviados
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteGastosEnviados_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private eGastosGenerales As clsGastosGenerales
    Private eBusquedaGastos As clsBusquedaGastos

    Private Sub frmReporteGastosEnviados_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        eGastosGenerales = New clsGastosGenerales
        cboBanco.DataSource = eGastosGenerales.fListarBancos(gEmpresa)
        If eGastosGenerales.iNroRegistros > 0 Then
            cboBanco.ValueMember = "IdBanco"
            cboBanco.DisplayMember = "NombreBanco"
            cboBanco.SelectedIndex = -1
        End If

        dtpFechaFin.Text = Now.Date()
        Dim Mes As String = ""
        Mes = Format(Now.Date.Month, "00")
        dtpFechaIni.Text = "01/" & Mes & "/" & Now.Year()

    End Sub

    Private Sub cboBanco_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBanco.SelectedIndexChanged
        Dim IdBanco As String
        If (cboBanco.SelectedIndex > -1) Then
            Try
                IdBanco = cboBanco.SelectedValue.ToString
                If IdBanco <> "System.Data.DataRowView" Then
                    'If rdb1.Checked = True Then
                    'cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa2(gEmpresa, IdBanco)
                    'ElseIf rdb2.Checked = True Then
                    cboCuenta.DataSource = eGastosGenerales.fListarCuentasxBancoyEmpresa(gEmpresa, IdBanco)
                    'End If
                    If eGastosGenerales.iNroRegistros > 0 Then
                        cboCuenta.ValueMember = "IdCuenta"
                        cboCuenta.DisplayMember = "NumeroCuenta"
                        cboCuenta.SelectedIndex = -1
                    End If
                End If
            Catch ex As System.IndexOutOfRangeException
                Exit Sub
            End Try
        Else
            cboCuenta.DataSource = Nothing
        End If
    End Sub

    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        If dtpFechaFin.Value < dtpFechaIni.Value Then
            MessageBox.Show("La Fecha de Inicio es Mayor a la Fecha Final", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            dtpFechaIni.Focus()
            Exit Sub
        End If
        If cboBanco.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione un Banco", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboBanco.Focus()
            Exit Sub
        End If
        If cboCuenta.SelectedIndex = -1 Then
            MessageBox.Show("Seleccione una Cuenta", "Sistema de Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cboCuenta.Focus()
            Exit Sub
        End If
        Try
            Dim dtTable2 As DataTable
            Dim pIdRetencion As String = ""
            Dim pSerie As String = ""
            dtTable2 = New DataTable
            eGastosGenerales = New clsGastosGenerales
            eBusquedaGastos = New clsBusquedaGastos

            If RD1.Checked = True Then
                dtTable2 = eBusquedaGastos.fConsultaGastosEnviados(0, "", gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, cboCuenta.SelectedValue, cboBanco.SelectedValue, gEmprRuc, gDesEmpresa, Trim(gUsuario))
                Muestra_Reporte(RutaAppReportes & "rptGastosEnviados.rpt", dtTable2, "TblLibroDiario", "", "Banco;" & Trim(cboBanco.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Cuenta;" & Trim(cboCuenta.Text), "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value)
            End If
            If RD2.Checked = True Then
                dtTable2 = eBusquedaGastos.fConsultaGastosEnviados(1, "", gEmpresa, dtpFechaIni.Value, dtpFechaFin.Value, cboCuenta.SelectedValue, cboBanco.SelectedValue, gEmprRuc, gDesEmpresa, Trim(gUsuario))
                Muestra_Reporte(RutaAppReportes & "rptGastosEnviados.rpt", dtTable2, "TblLibroDiario", "", "Banco;" & Trim(cboBanco.Text), "Ruc;" & gEmprRuc, "Empresa;" & gDesEmpresa, "Cuenta;" & Trim(cboCuenta.Text), "Criterio;" & "Del " & dtpFechaIni.Value & " al " & dtpFechaFin.Value & " - Usuario: " & UCase(Trim(glbNameUsuario)))
            End If

            If dtTable2.Rows.Count = 0 Then
                Dim mensaje As String = ""
                mensaje = "No Se Encontraron Registros."
                MessageBox.Show(mensaje, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MessageBox.Show("Ocurrio un problema en la transaccion." & Chr(13) & ex.Message, glbNameSistema, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End Try
        Me.Cursor = Cursors.Default

    End Sub

    Public Sub Muestra_Reporte(ByVal STRnombreReporte As String, ByVal myDatos As DataTable, ByVal STRnombreTabla As String, ByVal STRfiltro As String, ByVal ParamArray Parametros() As String)
        Try
            Dim f As New frmReporteConsolidado2
            Dim myReporte As New ReportDocument

            'Cargo el reporte segun ruta
            myReporte.Load(STRnombreReporte)

            'Leo los parametros
            If Parametros.Length > 0 Then
                f.CRVisor.ParameterFieldInfo = Genera_Parametros(Parametros)
            End If
            f.CRVisor.SelectionFormula = STRfiltro

            myReporte.SetDataSource(myDatos)
            'f.Titulo = STRnombreReporte

            'Levanto el formulario del reporte
            f.CRVisor.ReportSource = myReporte
            f.CRVisor.DisplayGroupTree = False
            f.Show()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Shared Function Genera_Parametros(ByVal ParamArray MyMatriz() As String) As ParameterFields
        Dim c As Long, STRnombre As String, STRvalor As String, l As Integer
        Dim parametros As New ParameterFields

        Try
            For c = 0 To MyMatriz.Length - 1
                l = InStr(MyMatriz(c), ";")
                If l > 0 Then
                    STRnombre = Mid(MyMatriz(c), 1, l - 1)
                    STRvalor = Mid(MyMatriz(c), l + 1, Len(MyMatriz(c)) - l)
                    Dim parametro As New ParameterField
                    Dim dVal As New ParameterDiscreteValue
                    parametro.ParameterFieldName = STRnombre
                    dVal.Value = STRvalor
                    parametro.CurrentValues.Add(dVal)
                    parametros.Add(parametro)
                End If
            Next
            Return (parametros)
        Catch ex As Exception
            Throw
        End Try
    End Function


End Class