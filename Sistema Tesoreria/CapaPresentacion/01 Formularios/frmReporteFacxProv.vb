Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared

Public Class frmReporteFacxProv

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition

    Public ResDet As String = ""
    Public TodosUno As String = ""
    Public RucDeUno As String = ""
    Public Moneda As String = ""
    'Dim rptFacPorPagar As New rptFacturasPorPagar
    Dim rptFacPorPagar As New rptFacturasPorPagarDet
    Dim rptFacPorPagar2 As New rptFacturasPorPagarDet2

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmReporteFacxProv = Nothing
    Public Shared Function Instance() As frmReporteFacxProv
        If frmInstance Is Nothing Then
            frmInstance = New frmReporteFacxProv
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmReporteFacxProv_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub
#End Region

    Private Sub frmReporteFacxProv_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If TodosUno = "T" Then
            rptFacPorPagar.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NombreEmpr")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gDesEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmprRuc)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("ResDet")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(ResDet)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@CodProv")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(RucDeUno)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptFacPorPagar.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@IdMoneda")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(Moneda)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptFacPorPagar
        ElseIf TodosUno = "U" Then
            rptFacPorPagar2.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)
            '1
            crParameterFieldDefinitions = rptFacPorPagar2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@EmprCodigo")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '2
            crParameterFieldDefinitions = rptFacPorPagar2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@NombreEmpr")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gDesEmpresa)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            '3
            crParameterFieldDefinitions = rptFacPorPagar2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@RucEmpresa")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(gEmprRuc)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '3
            crParameterFieldDefinitions = rptFacPorPagar2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("ResDet")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(ResDet)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '4
            crParameterFieldDefinitions = rptFacPorPagar2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@CodProv")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(RucDeUno)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            '5
            crParameterFieldDefinitions = rptFacPorPagar2.DataDefinition.ParameterFields()
            crParameterFieldDefinition = crParameterFieldDefinitions("@IdMoneda")
            crParameterValues = crParameterFieldDefinition.CurrentValues
            crParameterDiscreteValue = New ParameterDiscreteValue()
            crParameterDiscreteValue.Value = Trim(Moneda)
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)
            CrystalReportViewer1.ReportSource = rptFacPorPagar2
        End If

      

    End Sub
End Class