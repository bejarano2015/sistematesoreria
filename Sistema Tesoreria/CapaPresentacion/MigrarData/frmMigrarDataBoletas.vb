﻿Imports BusinessLogicLayer.MigrarData
Imports CapaEntidad

Public Class FrmMigrarDataBoletas

#Region "DECLARACIONES"

    Public Const AvisoMSGBOX As String = "SISTEMA TESORERIA"
    Dim VL_SRV_MIGRAR As SrvMigrarData
    Dim dt As DataTable
    Dim VL_BASEDATOS As String
    Dim VL_OBRACODIGO As String
    Dim VL_FECHA_INICIAL As String
    Dim VL_FECHA_FIN As String
    Dim VL_DOCUMENTO As String
    Dim VL_USUARIO As String
    Dim VL_ID_OBRA As Int16
    Dim VL_EMPRE_CODIGO As String
    Dim VL_SUMA As Int16
    Dim VL_EMPRESA As String
#End Region

    Private Sub FrmMigrarDataBoletas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LISTAR_EMPRESA()
        VL_DOCUMENTO = "VENTAS"
        LISTAR_DOCUMENTOS()


        If rgListaTodoHistorial.RowCount > 5000 Then
            Dim msg = MessageBox.Show("Tabla Historial hay mas de 5000 registros.. Desea Eliminar y Liberar Espacio......??", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            If msg = DialogResult.Yes Then
                TRUNCATE_TABLE()
                LISTAR_HISTORIAL_DATOS()
            End If
        End If

        VL_EMPRESA = gEmpresa


        LISTAR_HISTORIAL_DATOS()
        LISTAR_HISTORIAL()

        dtFechaFin.Value = Now
        lblTotalDatos.Text = "Total : 0"
        lblContarHistorial.Text = " Total : " + rgListaTodoHistorial.RowCount.ToString()

        PAGE.SelectedPage = PAGE_CONSULTAS
        'PAGE_CONSULTAS.Enabled = False
        'PAGE_MIGRAR_DATA.Enabled = False
    End Sub

#Region "BOTON"
    Private Sub btnConsultar_Click(sender As Object, e As EventArgs) Handles btnConsultar.Click
        VL_BASEDATOS = cboObra.SelectedValue.ToString()
        VL_OBRACODIGO = VL_BASEDATOS.Substring(0, 4)
        VL_BASEDATOS = VL_BASEDATOS.Substring(8, VL_BASEDATOS.Length - 8)

        VL_FECHA_INICIAL = dtFechaInicial.Value.ToShortDateString()
        VL_FECHA_FIN = dtFechaFin.Value.ToShortDateString()

        LISTAR_DATA(VL_BASEDATOS, VL_OBRACODIGO, VL_FECHA_INICIAL, VL_FECHA_FIN)
        lblTotalDatos.Text = "Total : " + rgListaDATA.RowCount.ToString()

        If rgListaDATA.RowCount > 0 Then
            btnGuardarDatos.Enabled = True
        Else
            btnGuardarDatos.Enabled = False
        End If
    End Sub

    Private Sub btnGuardarDatos_Click(sender As Object, e As EventArgs) Handles btnGuardarDatos.Click
        AGREGAR_DATOS()
        LISTAR_DATOS_TABLA_INTERMEDIA()
        PAGE.SelectedPage = PAGE_MIGRAR_DATA
        'PAGE_CONSULTAS.Enabled = False
        'PAGE_MIGRAR_DATA.Enabled = True
        lblContarT_Intermedia.Text = rgListaMigrados.RowCount.ToString()
        If rgListaMigrados.RowCount > 0 Then
            btnGuardarMigrar.Enabled = True
        Else
            btnGuardarMigrar.Enabled = False
        End If

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardarMigrar.Click

        AGREGAR_DATOS_HISTORIAL()
        MIGRAR_DATOS()

        LISTAR_HISTORIAL()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        'Dispose()
        PAGE.SelectedPage = PAGE_CONSULTAS
        'PAGE_CONSULTAS.Enabled = True
        'PAGE_MIGRAR_DATA.Enabled = False
    End Sub

    Private Sub LISTAR_DATA(BASEDATOS As String, OBRA_CODIGO As String, FINICIAL As String, FFIN As String)

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        dt = New DataTable()
        Try
            VL_BeanResultado = VL_SRV_MIGRAR.SRV_LISTAR_DATA(BASEDATOS, OBRA_CODIGO, FINICIAL, FFIN)

            If VL_BeanResultado.blnExiste = True Then
                rgListaDATA.DataSource = VL_BeanResultado.dtResultado
            Else
                rgListaDATA.DataSource = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), AvisoMSGBOX)
        End Try

    End Sub

#End Region

#Region "LISTAR EMPRESA Y OBRA"

    Private Sub TRUNCATE_TABLE()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_TRUNCATE_TABLE()
        If VL_BeanResultado.blnExiste = True Then

            MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)

        End If
    End Sub
    Private Sub LISTAR_EMPRESA()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_LISTAR_EMPRESAS()
        If VL_BeanResultado.blnExiste = True Then
            Me.cboEmpresa.DataSource = VL_BeanResultado.dtResultado
            Me.cboEmpresa.DisplayMember = "Empresa"
            Me.cboEmpresa.ValueMember = "EMPR_CODIGO"
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)
            cboEmpresa.DataSource = Nothing
        End If
    End Sub


    Private Sub LISTAR_OBRA(ID_EMPRESA As String)

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        dt = New DataTable()
        Try
            VL_BeanResultado = VL_SRV_MIGRAR.SRV_LISTAR_OBRAS(ID_EMPRESA)

            If VL_BeanResultado.blnExiste = True Then
                cboObra.DataSource = VL_BeanResultado.dtResultado
                cboObra.DisplayMember = "Obra"
                cboObra.ValueMember = "BaseDatos"

            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)
                cboObra.DataSource = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), AvisoMSGBOX)
        End Try

    End Sub

    Private Sub LISTAR_DOCUMENTOS()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_LISTAR_DOCUMENTOS(VL_DOCUMENTO)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboDocumento.DataSource = VL_BeanResultado.dtResultado
            Me.cboDocumento.DisplayMember = "DESCRIPCION"
            Me.cboDocumento.ValueMember = "ID_DOCUMENTO"

        End If
    End Sub

    Private Sub LISTAR_DATOS_TABLA_INTERMEDIA()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_LISTAR_DATA_INTERMEDIA()
        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaMigrados.DataSource = VL_BeanResultado.dtResultado

        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)
            rgListaMigrados.DataSource = Nothing
        End If
    End Sub

    Private Sub LISTAR_HISTORIAL_DATOS()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_LISTAR_HISTORIAL_DATOS()
        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaTodoHistorial.DataSource = VL_BeanResultado.dtResultado

        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)
            rgListaMigrados.DataSource = Nothing
        End If
    End Sub

#End Region

#Region "AGREGAR"

    Private Sub AGREGAR_DATOS()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BASEDATOS = cboObra.SelectedValue.ToString()
        VL_OBRACODIGO = VL_BASEDATOS.Substring(0, 4)
        VL_BASEDATOS = VL_BASEDATOS.Substring(8, VL_BASEDATOS.Length - 8)

        VL_FECHA_INICIAL = dtFechaInicial.Value.ToShortDateString()
        VL_FECHA_FIN = dtFechaFin.Value.ToShortDateString()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_AGREGAR_DATO(VL_OBRACODIGO, VL_BASEDATOS, VL_FECHA_INICIAL, VL_FECHA_FIN)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub


    Private Sub MIGRAR_DATOS()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BASEDATOS = cboObra.SelectedValue.ToString()
        VL_OBRACODIGO = VL_BASEDATOS.Substring(0, 4)
        VL_BASEDATOS = VL_BASEDATOS.Substring(8, VL_BASEDATOS.Length - 8)

        VL_DOCUMENTO = cboDocumento.SelectedValue.ToString()
        VL_USUARIO = gUsuario

        If VL_OBRACODIGO = "0001" And VL_BASEDATOS = "GESTIONERP" Then 'CIUDAD DEL SOL - COLLIQUE - ETAP 1
            VL_ID_OBRA = 1
        ElseIf VL_OBRACODIGO = "0002" And VL_BASEDATOS = "GESTIONERP" Then 'CIUDAD DEL SOL - COLLIQUE - ETAP 2 - SUB 01
            VL_ID_OBRA = 6
        ElseIf VL_OBRACODIGO = "0003" And VL_BASEDATOS = "GESTIONERP" Then 'CIUDAD DEL SOL - COLLIQUE - ETAP 2 - SUB 02
            VL_ID_OBRA = 9
        ElseIf VL_OBRACODIGO = "0049" And VL_BASEDATOS = "GESTIONERPCHICLAYO" Then 'JOCKEY RESIDENCIAL - CHICLAYO - ETAP 8 
            VL_ID_OBRA = 3
        ElseIf VL_OBRACODIGO = "0050" And VL_BASEDATOS = "GESTIONERPCHICLAYO" Then 'JOCKEY RESIDENCIAL - CHICLAYO - ETAP 9
            VL_ID_OBRA = 11
        ElseIf VL_OBRACODIGO = "0051" And VL_BASEDATOS = "GESTIONERPCHICLAYO" Then 'JOCKEY RESIDENCIAL - CHICLAYO - ETAP 4
            VL_ID_OBRA = 12
        ElseIf VL_OBRACODIGO = "0001" And VL_BASEDATOS = "GESTIONERPPISCO" Then 'LOTES - PISCO - ETAP 1
            VL_ID_OBRA = 4
        ElseIf VL_OBRACODIGO = "0002" And VL_BASEDATOS = "VISTAMARERP" Then 'TORRES DE VISTAMAR
            VL_ID_OBRA = 10

        End If


        VL_BeanResultado = VL_SRV_MIGRAR.SRV_MIGRAR_DATOS(VL_ID_OBRA, VL_DOCUMENTO, VL_USUARIO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub


#End Region

#Region "MANTENIMIENTO DE  HISTORIAL"

    Private Sub LISTAR_HISTORIAL()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_LISTAR_HISTORIAL()
        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaHistorial.DataSource = VL_BeanResultado.dtResultado

        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, AvisoMSGBOX)
            rgListaHistorial.DataSource = Nothing
        End If
    End Sub

    Private Sub AGREGAR_DATOS_HISTORIAL()

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BASEDATOS = cboObra.SelectedValue.ToString()
        VL_OBRACODIGO = VL_BASEDATOS.Substring(0, 4)
        VL_BASEDATOS = VL_BASEDATOS.Substring(8, VL_BASEDATOS.Length - 8)
        VL_EMPRE_CODIGO = cboEmpresa.SelectedValue.ToString()
        VL_DOCUMENTO = cboDocumento.SelectedValue.ToString()
        VL_FECHA_INICIAL = dtFechaInicial.Value.ToShortDateString()
        VL_FECHA_FIN = dtFechaFin.Value.ToShortDateString()
        VL_SUMA = lblContarT_Intermedia.Text


        If VL_OBRACODIGO = "0001" And VL_BASEDATOS = "GESTIONERP" Then 'CIUDAD DEL SOL - COLLIQUE - ETAP 1
            VL_ID_OBRA = 1
        ElseIf VL_OBRACODIGO = "0002" And VL_BASEDATOS = "GESTIONERP" Then 'CIUDAD DEL SOL - COLLIQUE - ETAP 2 - SUB 01
            VL_ID_OBRA = 6
        ElseIf VL_OBRACODIGO = "0003" And VL_BASEDATOS = "GESTIONERP" Then 'CIUDAD DEL SOL - COLLIQUE - ETAP 2 - SUB 02
            VL_ID_OBRA = 9
        ElseIf VL_OBRACODIGO = "0049" And VL_BASEDATOS = "GESTIONERPCHICLAYO" Then 'JOCKEY RESIDENCIAL - CHICLAYO - ETAP 8 
            VL_ID_OBRA = 3
        ElseIf VL_OBRACODIGO = "0050" And VL_BASEDATOS = "GESTIONERPCHICLAYO" Then 'JOCKEY RESIDENCIAL - CHICLAYO - ETAP 9
            VL_ID_OBRA = 11
        ElseIf VL_OBRACODIGO = "0051" And VL_BASEDATOS = "GESTIONERPCHICLAYO" Then 'JOCKEY RESIDENCIAL - CHICLAYO - ETAP 4
            VL_ID_OBRA = 12
        ElseIf VL_OBRACODIGO = "0001" And VL_BASEDATOS = "GESTIONERPPISCO" Then 'LOTES - PISCO - ETAP 1
            VL_ID_OBRA = 4
        ElseIf VL_OBRACODIGO = "0002" And VL_BASEDATOS = "VISTAMARERP" Then 'TORRES DE VISTAMAR
            VL_ID_OBRA = 10

        End If

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_AGREGAR_DATOS_HISTORIAL(VL_EMPRE_CODIGO, VL_ID_OBRA, VL_DOCUMENTO, VL_SUMA, VL_FECHA_INICIAL, VL_FECHA_FIN)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ELIMINAR_DATOS_HISTORIAL(ID_HISTORIAL As Int16)

        VL_SRV_MIGRAR = New SrvMigrarData()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        VL_BeanResultado = VL_SRV_MIGRAR.SRV_ELIMINAR_DATOS_HISTORIAL(ID_HISTORIAL)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

#End Region


#Region "EVENTOS DE COMBO BOX"
    Private Sub cboEmpresa_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboEmpresa.SelectedValueChanged
        If cboEmpresa.SelectedValue IsNot Nothing Then
            If cboEmpresa.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                Dim ID As String = cboEmpresa.SelectedValue
                LISTAR_OBRA(ID)

            End If
        End If
    End Sub

#End Region


#Region "MENU CONTEXTUAL"
    Private Sub MECO_ELIMINAR_Click(sender As Object, e As EventArgs) Handles MECO_ELIMINAR.Click
        If rgListaHistorial.CurrentRow.Cells("HISTORIAL_CODIGO").Value > 0 Then
            Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...Recuerde que al Eliminar estaria eliminando los datos migrados y afectaria la tabla DOC_VENTAS y DOC_VENTASLINEA...??", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            Dim ID As Int16
            ID = rgListaHistorial.CurrentRow.Cells("HISTORIAL_CODIGO").Value.ToString()
            If msg = DialogResult.Yes Then
                ELIMINAR_DATOS_HISTORIAL(ID)
                LISTAR_HISTORIAL()
            End If
        Else
            MessageBox.Show("No se puede Eliminar...", AvisoMSGBOX)
        End If
    End Sub


#End Region


End Class
