﻿Imports CapaEntidad
Imports BusinessLogicLayer.MigrarData
Imports CapaEntidad.MigrarData
Public Class FrmClientes


#Region "DECLARACIONES"

    Dim VL_BOTON As String
    Dim VL_OBJETO As BeanClientes
    Dim VL_SRV_CLIENTES As SrvClientes
    Dim VL_CODIGO_CLIE As String
    Public Const AvisoMSGBOX As String = "SISTEMA TESORERIA"

#End Region
    Private Sub FrmClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboEstadoCivil.SelectedIndex = 0
        cboSexo.SelectedIndex = 0

        PAGE_CONSULTA.Enabled = True
        PAGE_MANTENIMIENTO.Enabled = False

        cboEstado.SelectedIndex = 0
        LISTAR_CLIENTES()
        LISTAR_DEPARTAMENTOS()
        cboDepartamento.SelectedIndex = 14
        LISTAR_TIPO_DOC()
        LISTAR_EMPRESA()
        LISTAR_OBRA()
    End Sub


#Region "BOTONES"
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        VL_BOTON = "0"

        PAGE_CONSULTA.Enabled = False
        PAGE_MANTENIMIENTO.Enabled = True
        RAD_PAGE.SelectedPage = PAGE_MANTENIMIENTO

    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If (VL_BOTON = "0") Then  ''AGREGAR VENTAS

            'If txtSerie.Text = "" Then

            '    MessageBox.Show("Ingrese Serie..'", "SISTEMA TESORERIA")

            'ElseIf txtNumero.Text = "" Then
            '    MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
            'ElseIf txtImpuesto.Text = "" Then
            '    MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            'ElseIf txtTotal.Text = "" Then
            '    MessageBox.Show("Ingrese Total...", "SISTEMA TESORERIA")
            'ElseIf txtNeto.Text = "" Then
            '    MessageBox.Show("Ingrese Neto...", "SISTEMA TESORERIA")
            'ElseIf txtDescuento.Text = "" Then
            '    MessageBox.Show("Ingrese Descuento...", "SISTEMA TESORERIA")
            'ElseIf txtSubTotal.Text = "" Then
            '    MessageBox.Show("Ingrese SubTotal...", "SISTEMA TESORERIA")
            'Else

            AGREGAR_DATOS()

            LISTAR_CLIENTES()


            RAD_PAGE.SelectedPage = PAGE_CONSULTA

            'End If

        End If

        If (VL_BOTON = "1") Then ''ACTUALIZAR VENTA
            'If txtSerie.Text = "" Then

            '    MessageBox.Show("Ingrese Serie..'")

            'ElseIf txtNumero.Text = "" Then
            '    MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
            '    '    'ElseIf txtObservaciones.Text = "" Then
            '    '    MessageBox.Show("Ingrese Observaciones...", "SISTEMA TESORERIA")
            'ElseIf txtImpuesto.Text = "" Then
            '    MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            'ElseIf txtTotal.Text = "" Then
            '    MessageBox.Show("Ingrese Total...", "SISTEMA TESORERIA")
            'ElseIf txtNeto.Text = "" Then
            '    MessageBox.Show("Ingrese Neto...", "SISTEMA TESORERIA")
            'ElseIf txtDescuento.Text = "" Then
            '    MessageBox.Show("Ingrese Descuento...", "SISTEMA TESORERIA")
            'ElseIf txtSubTotal.Text = "" Then
            '    MessageBox.Show("Ingrese SubTotal...", "SISTEMA TESORERIA")
            'Else

            ACTUALIZAR_DATOS()
            LISTAR_CLIENTES()
            'End If
        End If
        RAD_PAGE.SelectedPage = PAGE_CONSULTA
        PAGE_CONSULTA.Enabled = True
        PAGE_MANTENIMIENTO.Enabled = False
        LIMPIAR()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click

        PAGE_CONSULTA.Enabled = True
        PAGE_MANTENIMIENTO.Enabled = False

        RAD_PAGE.SelectedPage = PAGE_CONSULTA
        LIMPIAR()

    End Sub
#End Region

#Region "LISTAR  UBIGEO"

    Private Sub LISTAR_DEPARTAMENTOS()

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        'Dim ID_DETALLE = Grilla.CurrentRow.Cells("IdDetalle").Value()

        VL_BeanResultado = VL_SRV_CLIENTES.SRV_LISTAR_DEPA()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboDepartamento.DataSource = VL_BeanResultado.dtResultado
            Me.cboDepartamento.DisplayMember = "UBDE_DESCRIPCION"
            Me.cboDepartamento.ValueMember = "UBDE_CODIGO"
        End If

    End Sub

    Private Sub LISTAR_PROVINCIAS(ID_PRO As String)

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        'Dim ID_DETALLE = Grilla.CurrentRow.Cells("IdDetalle").Value()

        VL_BeanResultado = VL_SRV_CLIENTES.SRV_LISTAR_PROV(ID_PRO)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboProvincia.DataSource = VL_BeanResultado.dtResultado
            Me.cboProvincia.DisplayMember = "UBPR_DESCRIPCION"
            Me.cboProvincia.ValueMember = "UBPR_CODIGO"

        End If
    End Sub

    Private Sub LISTAR_DISTRITO(ID_PROV As String)

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        'Dim ID_DETALLE = Grilla.CurrentRow.Cells("IdDetalle").Value()

        VL_BeanResultado = VL_SRV_CLIENTES.SRV_LISTAR_DISTR(ID_PROV)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboDistrito.DataSource = VL_BeanResultado.dtResultado
            Me.cboDistrito.DisplayMember = "UBDS_DESCRIPCION"
            Me.cboDistrito.ValueMember = "UBDS_CODIGO"
        End If
    End Sub
#End Region

#Region "LISTAR, AGREGAR, ACTUALIZAR Y ELIMINAR"

    Private Sub LISTAR_CLIENTES()

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SRV_CLIENTES.SRV_LISTAR_CLIENTES()

            If VL_BeanResultado.blnExiste = True Then
                rgListaClientes.DataSource = VL_BeanResultado.dtResultado

            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), AvisoMSGBOX)
        End Try

    End Sub

    Private Sub LISTAR_OBRA()

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SRV_CLIENTES.SRV_LISTAR_OBRA()

            If VL_BeanResultado.blnExiste = True Then
                cboObrasCliente.DataSource = VL_BeanResultado.dtResultado
                cboObrasCliente.DisplayMember = "OBRA"
                cboObrasCliente.ValueMember = "ID_OBRA"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), AvisoMSGBOX)
        End Try

    End Sub
    Private Sub LISTAR_EMPRESA()

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SRV_CLIENTES.SRV_LISTAR_EMPRESA()

            If VL_BeanResultado.blnExiste = True Then
                cboEmpresa.DataSource = VL_BeanResultado.dtResultado
                cboEmpresa.DisplayMember = "EmprDescripcion"
                cboEmpresa.ValueMember = "EmprCodigo"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), AvisoMSGBOX)
        End Try

    End Sub

    Private Sub LISTAR_TIPO_DOC()

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SRV_CLIENTES.SRV_LISTAR_TIPO_DOC()

            If VL_BeanResultado.blnExiste = True Then
                cboTipoDocumento.DataSource = VL_BeanResultado.dtResultado
                cboTipoDocumento.DisplayMember = "TPDC_DESCRIPCION"
                cboTipoDocumento.ValueMember = "TPDC_CODIGO"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString(), AvisoMSGBOX)
        End Try

    End Sub

    Private Sub AGREGAR_DATOS()

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        VL_OBJETO = New BeanClientes()

        VL_OBJETO.TIPO_DOC = cboTipoDocumento.SelectedValue.ToString()
        VL_OBJETO.UBDS_CODIGO = cboDistrito.SelectedValue.ToString()
        VL_OBJETO.NOMBRES = txtClieNombres.Text
        VL_OBJETO.APE_PATERNO = txtClieApePaterno.Text
        VL_OBJETO.APE_MATERNO = txtClieApeMaterno.Text
        VL_OBJETO.NRO_DOC = txtClieNumeroDoc.Text
        VL_OBJETO.SEXO = cboSexo.Text
        VL_OBJETO.FECHA_NAC = dtFechaNac.Value.ToLongDateString()
        VL_OBJETO.ESTADO_CIVIL = cboEstadoCivil.Text
        VL_OBJETO.DIRECCION = txtClieDireccion.Text
        VL_OBJETO.REFERENCIA = txtClieReferencia.Text
        VL_OBJETO.TELEFONO = txtClieTelefono.Text
        VL_OBJETO.EMAIL = txtClieEmail.Text
        VL_OBJETO.OCUPACION = txtClieOcupacion.Text
        VL_OBJETO.EMPRESA = txtEmpresa.Text
        VL_OBJETO.ID_OBRA = cboObrasCliente.SelectedValue.ToString()

        VL_BeanResultado = VL_SRV_CLIENTES.SRV_AGREGAR(VL_OBJETO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ACTUALIZAR_DATOS()

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        VL_OBJETO = New BeanClientes()

        VL_OBJETO.CODIGO = txtClieCodigo.Text
        VL_OBJETO.TIPO_DOC = cboTipoDocumento.SelectedValue.ToString()
        VL_OBJETO.UBDS_CODIGO = cboDistrito.SelectedValue.ToString()
        VL_OBJETO.NOMBRES = txtClieNombres.Text
        VL_OBJETO.APE_PATERNO = txtClieApePaterno.Text
        VL_OBJETO.APE_MATERNO = txtClieApeMaterno.Text
        VL_OBJETO.NRO_DOC = txtClieNumeroDoc.Text
        VL_OBJETO.SEXO = cboSexo.Text
        VL_OBJETO.FECHA_NAC = dtFechaNac.Value.ToLongDateString()
        VL_OBJETO.ESTADO_CIVIL = cboEstadoCivil.Text
        VL_OBJETO.DIRECCION = txtClieDireccion.Text
        VL_OBJETO.REFERENCIA = txtClieReferencia.Text
        VL_OBJETO.TELEFONO = txtClieTelefono.Text
        VL_OBJETO.EMAIL = txtClieEmail.Text
        VL_OBJETO.OCUPACION = txtClieOcupacion.Text
        VL_OBJETO.EMPRESA = txtEmpresa.Text
        VL_OBJETO.ID_OBRA = cboObrasCliente.SelectedValue.ToString()

        VL_BeanResultado = VL_SRV_CLIENTES.SRV_ACTUALIZAR(VL_OBJETO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub
    Private Sub ELIMINAR_DATOS(CODIGO_CLIE As String)

        VL_SRV_CLIENTES = New SrvClientes()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_CODIGO As Int16 = rgListaClientes.CurrentRow.Cells("CLIE_ID_OBRA").Value.ToString()

        VL_BeanResultado = VL_SRV_CLIENTES.SRV_ELIMINAR(CODIGO_CLIE, VL_CODIGO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub


#End Region


#Region "LIMPIAR CONTROLES"

    Sub LIMPIAR()
        txtClieCodigo.Text = ""
        txtClieNombres.Text = ""
        txtClieApeMaterno.Text = ""
        txtClieApePaterno.Text = ""
        cboTipoDocumento.SelectedIndex = 0
        txtClieNumeroDoc.Text = ""
        cboSexo.SelectedIndex = 0
        dtFechaNac.Value = Now
        cboEstadoCivil.SelectedIndex = 0
        txtClieDireccion.Text = ""
        txtClieReferencia.Text = ""
        txtClieTelefono.Text = ""
        txtClieEmail.Text = ""
        txtClieOcupacion.Text = ""
        txtEmpresa.Text = ""
        cboEmpresa.SelectedIndex = 0
    End Sub


#End Region

    Private Sub rgListaClientes_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles rgListaClientes.CellDoubleClick

        VL_BOTON = "1"
        RAD_PAGE.SelectedPage = PAGE_MANTENIMIENTO
        PAGE_CONSULTA.Enabled = False
        PAGE_MANTENIMIENTO.Enabled = True

        txtClieCodigo.Text = rgListaClientes.CurrentRow.Cells("CLIE_CODIGO").Value.ToString()
        txtClieNombres.Text = rgListaClientes.CurrentRow.Cells("CLIE_NOMBRES").Value.ToString()
        txtClieApePaterno.Text = rgListaClientes.CurrentRow.Cells("CLIE_APELLIDOPATERNO").Value.ToString()
        txtClieApeMaterno.Text = rgListaClientes.CurrentRow.Cells("CLIE_APELLIDOMATERNO").Value.ToString()
        cboTipoDocumento.SelectedValue = rgListaClientes.CurrentRow.Cells("TPDC_CODIGO").Value.ToString()
        txtClieNumeroDoc.Text = rgListaClientes.CurrentRow.Cells("CLIE_NRODOCUMENTO").Value.ToString()
        cboSexo.Text = rgListaClientes.CurrentRow.Cells("CLIE_SEXO").Value.ToString()
        dtFechaNac.Value = IIf(rgListaClientes.CurrentRow.Cells("CLIE_FECHANACIMIENTO").Value.ToString() = "", "01/01/1900", rgListaClientes.CurrentRow.Cells("CLIE_FECHANACIMIENTO").Value.ToString())
        cboEstadoCivil.Text = rgListaClientes.CurrentRow.Cells("CLIE_ESTADOCIVIL").Value.ToString()
        txtClieDireccion.Text = rgListaClientes.CurrentRow.Cells("CLIE_DIRECCION").Value.ToString()
        txtClieReferencia.Text = rgListaClientes.CurrentRow.Cells("CLIE_REFERENCIA").Value.ToString()
        txtClieTelefono.Text = rgListaClientes.CurrentRow.Cells("CLIE_TELEFONO").Value.ToString()
        txtClieEmail.Text = rgListaClientes.CurrentRow.Cells("CLIE_EMAIL").Value.ToString()
        txtClieOcupacion.Text = rgListaClientes.CurrentRow.Cells("CLIE_OCUPACION").Value.ToString()
        txtEmpresa.Text = rgListaClientes.CurrentRow.Cells("CLIE_EMPRESA").Value.ToString()
        cboObrasCliente.SelectedValue = rgListaClientes.CurrentRow.Cells("CLIE_ID_OBRA").Value.ToString()
        cboObrasCliente.Text = rgListaClientes.CurrentRow.Cells("OBRA").Value.ToString()
        cboEstado.SelectedIndex = rgListaClientes.CurrentRow.Cells("CLIE_ESTADO").Value.ToString()

        'cboObra.SelectedValue = VL_OBRA
        cboDepartamento.SelectedValue = rgListaClientes.CurrentRow.Cells("UBDE_CODIGO").Value.ToString()
        cboProvincia.SelectedValue = rgListaClientes.CurrentRow.Cells("UBPR_CODIGO").Value.ToString()
        cboDistrito.SelectedValue = rgListaClientes.CurrentRow.Cells("UBDS_CODIGO").Value.ToString()

    End Sub


#Region "MENU CONTEXTUAL"
    Private Sub MECO_ELIMINAR_Click(sender As Object, e As EventArgs) Handles MECO_ELIMINAR.Click
        If rgListaClientes.CurrentRow.Cells("CLIE_CODIGO").Value <> "" Then
            Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
            Dim VL_CODIGO As String
            VL_CODIGO = rgListaClientes.CurrentRow.Cells("CLIE_CODIGO").Value.ToString()
            If msg = DialogResult.Yes Then
                ELIMINAR_DATOS(VL_CODIGO)
                LISTAR_CLIENTES()
            End If
        Else
            MessageBox.Show("No se puede Eliminar...", AvisoMSGBOX)
        End If
    End Sub
#End Region

#Region "EVENTOS DE COMBOX"
    Private Sub cboDepartamento_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDepartamento.SelectedValueChanged
        If cboDepartamento.SelectedValue IsNot Nothing Then
            If cboDepartamento.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                Dim ID_DEP = cboDepartamento.SelectedValue
                LISTAR_PROVINCIAS(ID_DEP)
            End If
        End If
    End Sub

    Private Sub cboProvincia_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboProvincia.SelectedValueChanged
        If cboProvincia.SelectedValue IsNot Nothing Then
            If cboProvincia.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                Dim ID_PRO = cboProvincia.SelectedValue
                LISTAR_DISTRITO(ID_PRO)
            End If

        End If
    End Sub

#End Region


End Class
