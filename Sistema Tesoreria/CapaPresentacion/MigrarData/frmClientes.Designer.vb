﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmClientes
    Inherits Telerik.WinControls.UI.RadForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn12 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn13 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn14 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn15 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn16 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn17 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn18 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn19 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn20 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn21 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn22 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn23 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim RadListDataItem3 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem4 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem5 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem6 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem7 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem8 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem1 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem2 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Me.RAD_PAGE = New Telerik.WinControls.UI.RadPageView()
        Me.PAGE_CONSULTA = New Telerik.WinControls.UI.RadPageViewPage()
        Me.RadGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.rgListaClientes = New Telerik.WinControls.UI.RadGridView()
        Me.RadGroupBox1 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.btnNuevo = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel16 = New Telerik.WinControls.UI.RadLabel()
        Me.PAGE_MANTENIMIENTO = New Telerik.WinControls.UI.RadPageViewPage()
        Me.RadGroupBox4 = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel13 = New Telerik.WinControls.UI.RadLabel()
        Me.GRUPO_VENTA_LINEA = New Telerik.WinControls.UI.RadGroupBox()
        Me.dtFechaNac = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.txtEmpresa = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel12 = New Telerik.WinControls.UI.RadLabel()
        Me.txtClieTelefono = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieDireccion = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieEmail = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieReferencia = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieNumeroDoc = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieOcupacion = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieApeMaterno = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieApePaterno = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieNombres = New Telerik.WinControls.UI.RadTextBox()
        Me.txtClieCodigo = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel11 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel10 = New Telerik.WinControls.UI.RadLabel()
        Me.cboSexo = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel9 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel7 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel5 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel29 = New Telerik.WinControls.UI.RadLabel()
        Me.cboEstadoCivil = New Telerik.WinControls.UI.RadDropDownList()
        Me.cboTipoDocumento = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel30 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel17 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel14 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel4 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel26 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel6 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel8 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel23 = New Telerik.WinControls.UI.RadLabel()
        Me.btnCancelar = New Telerik.WinControls.UI.RadButton()
        Me.btnGuardar = New Telerik.WinControls.UI.RadButton()
        Me.txtNombres = New Telerik.WinControls.UI.RadTextBox()
        Me.txtDocVenta = New Telerik.WinControls.UI.RadTextBox()
        Me.txtReferencia = New Telerik.WinControls.UI.RadTextBox()
        Me.txtGlosa = New Telerik.WinControls.UI.RadTextBox()
        Me.txtCodigo = New Telerik.WinControls.UI.RadTextBox()
        Me.txtApePaterno = New Telerik.WinControls.UI.RadTextBox()
        Me.txtApeMaterno = New Telerik.WinControls.UI.RadTextBox()
        Me.txtTelefono = New Telerik.WinControls.UI.RadTextBox()
        Me.txtEmail = New Telerik.WinControls.UI.RadTextBox()
        Me.cboEmpresa = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel15 = New Telerik.WinControls.UI.RadLabel()
        Me.MECO = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MECO_ELIMINAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.cboEstado = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel18 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel19 = New Telerik.WinControls.UI.RadLabel()
        Me.cboObrasCliente = New Telerik.WinControls.UI.RadDropDownList()
        Me.cboDepartamento = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel20 = New Telerik.WinControls.UI.RadLabel()
        Me.cboProvincia = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel21 = New Telerik.WinControls.UI.RadLabel()
        Me.cboDistrito = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel22 = New Telerik.WinControls.UI.RadLabel()
        CType(Me.RAD_PAGE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RAD_PAGE.SuspendLayout()
        Me.PAGE_CONSULTA.SuspendLayout()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox3.SuspendLayout()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaClientes.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox1.SuspendLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox2.SuspendLayout()
        CType(Me.RadLabel16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PAGE_MANTENIMIENTO.SuspendLayout()
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox4.SuspendLayout()
        CType(Me.RadLabel13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GRUPO_VENTA_LINEA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRUPO_VENTA_LINEA.SuspendLayout()
        CType(Me.dtFechaNac, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieTelefono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieDireccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieReferencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieNumeroDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieOcupacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieApeMaterno, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieApePaterno, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieNombres, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtClieCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboSexo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEstadoCivil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboTipoDocumento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNombres, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDocVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtReferencia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGlosa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCodigo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtApePaterno, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtApeMaterno, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTelefono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtEmail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MECO.SuspendLayout()
        CType(Me.cboEstado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboObrasCliente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDepartamento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboProvincia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDistrito, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RAD_PAGE
        '
        Me.RAD_PAGE.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RAD_PAGE.BackColor = System.Drawing.Color.Silver
        Me.RAD_PAGE.Controls.Add(Me.PAGE_CONSULTA)
        Me.RAD_PAGE.Controls.Add(Me.PAGE_MANTENIMIENTO)
        Me.RAD_PAGE.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RAD_PAGE.Location = New System.Drawing.Point(1, 0)
        Me.RAD_PAGE.Name = "RAD_PAGE"
        Me.RAD_PAGE.SelectedPage = Me.PAGE_CONSULTA
        Me.RAD_PAGE.Size = New System.Drawing.Size(1127, 584)
        Me.RAD_PAGE.TabIndex = 55
        Me.RAD_PAGE.Text = "RadPageView1"
        CType(Me.RAD_PAGE.GetChildAt(0), Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None
        CType(Me.RAD_PAGE.GetChildAt(0), Telerik.WinControls.UI.RadPageViewStripElement).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Top
        '
        'PAGE_CONSULTA
        '
        Me.PAGE_CONSULTA.Controls.Add(Me.RadGroupBox3)
        Me.PAGE_CONSULTA.Controls.Add(Me.RadGroupBox1)
        Me.PAGE_CONSULTA.Controls.Add(Me.btnNuevo)
        Me.PAGE_CONSULTA.Controls.Add(Me.RadGroupBox2)
        Me.PAGE_CONSULTA.Location = New System.Drawing.Point(10, 37)
        Me.PAGE_CONSULTA.Name = "PAGE_CONSULTA"
        Me.PAGE_CONSULTA.Size = New System.Drawing.Size(1106, 536)
        Me.PAGE_CONSULTA.Text = "Lista de Clientes"
        '
        'RadGroupBox3
        '
        Me.RadGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel2)
        Me.RadGroupBox3.Controls.Add(Me.rgListaClientes)
        Me.RadGroupBox3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox3.HeaderText = ""
        Me.RadGroupBox3.Location = New System.Drawing.Point(3, 66)
        Me.RadGroupBox3.Name = "RadGroupBox3"
        Me.RadGroupBox3.Size = New System.Drawing.Size(1100, 404)
        Me.RadGroupBox3.TabIndex = 64
        '
        'RadLabel2
        '
        Me.RadLabel2.Location = New System.Drawing.Point(212, 79)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(2, 2)
        Me.RadLabel2.TabIndex = 48
        '
        'rgListaClientes
        '
        Me.rgListaClientes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rgListaClientes.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.rgListaClientes.ContextMenuStrip = Me.MECO
        Me.rgListaClientes.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgListaClientes.EnableHotTracking = False
        Me.rgListaClientes.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rgListaClientes.ForeColor = System.Drawing.Color.Black
        Me.rgListaClientes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgListaClientes.Location = New System.Drawing.Point(5, 5)
        '
        'rgListaClientes
        '
        Me.rgListaClientes.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom
        Me.rgListaClientes.MasterTemplate.AllowAddNewRow = False
        Me.rgListaClientes.MasterTemplate.AllowCellContextMenu = False
        Me.rgListaClientes.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgListaClientes.MasterTemplate.AllowColumnReorder = False
        Me.rgListaClientes.MasterTemplate.AllowDeleteRow = False
        Me.rgListaClientes.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn1.FieldName = "CLIE_CODIGO"
        GridViewTextBoxColumn1.HeaderText = "CLIE_CODIGO"
        GridViewTextBoxColumn1.Name = "CLIE_CODIGO"
        GridViewTextBoxColumn2.FieldName = "CLIE_APELLIDOPATERNO"
        GridViewTextBoxColumn2.HeaderText = "CLIE_APELLIDOPATERNO"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "CLIE_APELLIDOPATERNO"
        GridViewTextBoxColumn3.FieldName = "CLIE_APELLIDOMATERNO"
        GridViewTextBoxColumn3.HeaderText = "CLIE_APELLIDOMATERNO"
        GridViewTextBoxColumn3.IsVisible = False
        GridViewTextBoxColumn3.Name = "CLIE_APELLIDOMATERNO"
        GridViewTextBoxColumn4.FieldName = "CLIE_NOMBRES"
        GridViewTextBoxColumn4.HeaderText = "CLIE_NOMBRES"
        GridViewTextBoxColumn4.IsVisible = False
        GridViewTextBoxColumn4.Name = "CLIE_NOMBRES"
        GridViewTextBoxColumn5.FieldName = "CLIENTE"
        GridViewTextBoxColumn5.HeaderText = "NOMBRE CLIENTE"
        GridViewTextBoxColumn5.Name = "CLIENTE"
        GridViewTextBoxColumn5.Width = 300
        GridViewTextBoxColumn6.FieldName = "TPDC_CODIGO"
        GridViewTextBoxColumn6.HeaderText = "TPDC_CODIGO"
        GridViewTextBoxColumn6.IsVisible = False
        GridViewTextBoxColumn6.Name = "TPDC_CODIGO"
        GridViewTextBoxColumn7.FieldName = "TPDC_DESCRIPCION"
        GridViewTextBoxColumn7.HeaderText = "TIPO DOC."
        GridViewTextBoxColumn7.Name = "TPDC_DESCRIPCION"
        GridViewTextBoxColumn7.Width = 90
        GridViewTextBoxColumn8.FieldName = "CLIE_NRODOCUMENTO"
        GridViewTextBoxColumn8.HeaderText = "Nº DOC"
        GridViewTextBoxColumn8.Name = "CLIE_NRODOCUMENTO"
        GridViewTextBoxColumn8.Width = 80
        GridViewTextBoxColumn9.FieldName = "CLIE_SEXO"
        GridViewTextBoxColumn9.HeaderText = "SEXO"
        GridViewTextBoxColumn9.Name = "CLIE_SEXO"
        GridViewTextBoxColumn9.Width = 80
        GridViewTextBoxColumn10.FieldName = "CLIE_FECHANACIMIENTO"
        GridViewTextBoxColumn10.FormatString = "{0:d}"
        GridViewTextBoxColumn10.HeaderText = "FECHA  NAC."
        GridViewTextBoxColumn10.Name = "CLIE_FECHANACIMIENTO"
        GridViewTextBoxColumn10.Width = 80
        GridViewTextBoxColumn11.FieldName = "CLIE_ESTADOCIVIL"
        GridViewTextBoxColumn11.HeaderText = "ESTADO CIVIL"
        GridViewTextBoxColumn11.Name = "CLIE_ESTADOCIVIL"
        GridViewTextBoxColumn11.Width = 100
        GridViewTextBoxColumn12.FieldName = "CLIE_DIRECCION"
        GridViewTextBoxColumn12.HeaderText = "DIRECCION"
        GridViewTextBoxColumn12.Name = "CLIE_DIRECCION"
        GridViewTextBoxColumn12.Width = 150
        GridViewTextBoxColumn13.FieldName = "CLIE_REFERENCIA"
        GridViewTextBoxColumn13.HeaderText = "REFERENCIA"
        GridViewTextBoxColumn13.Name = "CLIE_REFERENCIA"
        GridViewTextBoxColumn13.Width = 150
        GridViewTextBoxColumn14.FieldName = "CLIE_TELEFONO"
        GridViewTextBoxColumn14.HeaderText = "TELEFONO"
        GridViewTextBoxColumn14.Name = "CLIE_TELEFONO"
        GridViewTextBoxColumn14.Width = 80
        GridViewTextBoxColumn15.FieldName = "CLIE_EMAIL"
        GridViewTextBoxColumn15.HeaderText = "EMAIL"
        GridViewTextBoxColumn15.Name = "CLIE_EMAIL"
        GridViewTextBoxColumn15.Width = 100
        GridViewTextBoxColumn16.FieldName = "CLIE_OCUPACION"
        GridViewTextBoxColumn16.HeaderText = "OCUPACION"
        GridViewTextBoxColumn16.Name = "CLIE_OCUPACION"
        GridViewTextBoxColumn16.Width = 100
        GridViewTextBoxColumn17.FieldName = "CLIE_EMPRESA"
        GridViewTextBoxColumn17.HeaderText = "EMPRESA"
        GridViewTextBoxColumn17.Name = "CLIE_EMPRESA"
        GridViewTextBoxColumn17.Width = 100
        GridViewTextBoxColumn18.FieldName = "CLIE_ESTADO"
        GridViewTextBoxColumn18.HeaderText = "ESTADO"
        GridViewTextBoxColumn18.Name = "CLIE_ESTADO"
        GridViewTextBoxColumn19.FieldName = "CLIE_ID_OBRA"
        GridViewTextBoxColumn19.HeaderText = "CLIE_ID_OBRA"
        GridViewTextBoxColumn19.Name = "CLIE_ID_OBRA"
        GridViewTextBoxColumn20.FieldName = "OBRA"
        GridViewTextBoxColumn20.HeaderText = "OBRA"
        GridViewTextBoxColumn20.Name = "OBRA"
        GridViewTextBoxColumn20.Width = 250
        GridViewTextBoxColumn21.FieldName = "UBDS_CODIGO"
        GridViewTextBoxColumn21.HeaderText = "UBDS_CODIGO"
        GridViewTextBoxColumn21.Name = "UBDS_CODIGO"
        GridViewTextBoxColumn22.FieldName = "UBPR_CODIGO"
        GridViewTextBoxColumn22.HeaderText = "UBPR_CODIGO"
        GridViewTextBoxColumn22.Name = "UBPR_CODIGO"
        GridViewTextBoxColumn23.FieldName = "UBDE_CODIGO"
        GridViewTextBoxColumn23.HeaderText = "UBDE_CODIGO"
        GridViewTextBoxColumn23.Name = "UBDE_CODIGO"
        Me.rgListaClientes.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8, GridViewTextBoxColumn9, GridViewTextBoxColumn10, GridViewTextBoxColumn11, GridViewTextBoxColumn12, GridViewTextBoxColumn13, GridViewTextBoxColumn14, GridViewTextBoxColumn15, GridViewTextBoxColumn16, GridViewTextBoxColumn17, GridViewTextBoxColumn18, GridViewTextBoxColumn19, GridViewTextBoxColumn20, GridViewTextBoxColumn21, GridViewTextBoxColumn22, GridViewTextBoxColumn23})
        Me.rgListaClientes.MasterTemplate.EnableFiltering = True
        Me.rgListaClientes.MasterTemplate.EnableGrouping = False
        Me.rgListaClientes.MasterTemplate.EnableSorting = False
        Me.rgListaClientes.MasterTemplate.ShowRowHeaderColumn = False
        Me.rgListaClientes.Name = "rgListaClientes"
        Me.rgListaClientes.ReadOnly = True
        Me.rgListaClientes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgListaClientes.ShowGroupPanel = False
        Me.rgListaClientes.Size = New System.Drawing.Size(1090, 382)
        Me.rgListaClientes.TabIndex = 47
        Me.rgListaClientes.Text = "RadGridView1"
        '
        'RadGroupBox1
        '
        Me.RadGroupBox1.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadGroupBox1.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox1.HeaderText = ""
        Me.RadGroupBox1.Location = New System.Drawing.Point(8, 501)
        Me.RadGroupBox1.Name = "RadGroupBox1"
        Me.RadGroupBox1.Size = New System.Drawing.Size(933, 25)
        Me.RadGroupBox1.TabIndex = 63
        '
        'RadLabel1
        '
        Me.RadLabel1.Location = New System.Drawing.Point(212, 79)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(2, 2)
        Me.RadLabel1.TabIndex = 48
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Location = New System.Drawing.Point(959, 493)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(127, 30)
        Me.btnNuevo.TabIndex = 59
        Me.btnNuevo.Text = "Nuevo"
        '
        'RadGroupBox2
        '
        Me.RadGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox2.Controls.Add(Me.RadLabel16)
        Me.RadGroupBox2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox2.HeaderText = "Consultas"
        Me.RadGroupBox2.Location = New System.Drawing.Point(3, 7)
        Me.RadGroupBox2.Name = "RadGroupBox2"
        Me.RadGroupBox2.Size = New System.Drawing.Size(929, 44)
        Me.RadGroupBox2.TabIndex = 62
        Me.RadGroupBox2.Text = "Consultas"
        '
        'RadLabel16
        '
        Me.RadLabel16.Location = New System.Drawing.Point(212, 79)
        Me.RadLabel16.Name = "RadLabel16"
        Me.RadLabel16.Size = New System.Drawing.Size(2, 2)
        Me.RadLabel16.TabIndex = 48
        '
        'PAGE_MANTENIMIENTO
        '
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.GRUPO_VENTA_LINEA)
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.btnCancelar)
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.btnGuardar)
        Me.PAGE_MANTENIMIENTO.Location = New System.Drawing.Point(10, 37)
        Me.PAGE_MANTENIMIENTO.Name = "PAGE_MANTENIMIENTO"
        Me.PAGE_MANTENIMIENTO.Size = New System.Drawing.Size(1106, 536)
        Me.PAGE_MANTENIMIENTO.Text = "Mantenimiento"
        '
        'RadGroupBox4
        '
        Me.RadGroupBox4.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RadGroupBox4.Controls.Add(Me.cboObrasCliente)
        Me.RadGroupBox4.Controls.Add(Me.RadLabel19)
        Me.RadGroupBox4.Controls.Add(Me.cboEmpresa)
        Me.RadGroupBox4.Controls.Add(Me.RadLabel15)
        Me.RadGroupBox4.Controls.Add(Me.RadLabel13)
        Me.RadGroupBox4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox4.HeaderText = " Empresa "
        Me.RadGroupBox4.Location = New System.Drawing.Point(651, 254)
        Me.RadGroupBox4.Name = "RadGroupBox4"
        Me.RadGroupBox4.Size = New System.Drawing.Size(425, 98)
        Me.RadGroupBox4.TabIndex = 64
        Me.RadGroupBox4.Text = " Empresa "
        '
        'RadLabel13
        '
        Me.RadLabel13.Location = New System.Drawing.Point(212, 79)
        Me.RadLabel13.Name = "RadLabel13"
        Me.RadLabel13.Size = New System.Drawing.Size(2, 2)
        Me.RadLabel13.TabIndex = 48
        '
        'GRUPO_VENTA_LINEA
        '
        Me.GRUPO_VENTA_LINEA.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.cboDistrito)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel22)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.cboEstado)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel18)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.cboProvincia)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel21)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.cboDepartamento)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel20)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadGroupBox4)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.dtFechaNac)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtEmpresa)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel12)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieTelefono)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieDireccion)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieEmail)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieReferencia)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieNumeroDoc)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieOcupacion)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieApeMaterno)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieApePaterno)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieNombres)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.txtClieCodigo)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel11)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel10)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.cboSexo)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel9)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel7)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel5)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel3)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel29)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.cboEstadoCivil)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.cboTipoDocumento)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel30)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel17)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel14)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel4)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel26)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel6)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel8)
        Me.GRUPO_VENTA_LINEA.Controls.Add(Me.RadLabel23)
        Me.GRUPO_VENTA_LINEA.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRUPO_VENTA_LINEA.HeaderText = "Mantenimiento Datos Clientes"
        Me.GRUPO_VENTA_LINEA.Location = New System.Drawing.Point(3, 0)
        Me.GRUPO_VENTA_LINEA.Name = "GRUPO_VENTA_LINEA"
        Me.GRUPO_VENTA_LINEA.Size = New System.Drawing.Size(1100, 380)
        Me.GRUPO_VENTA_LINEA.TabIndex = 62
        Me.GRUPO_VENTA_LINEA.Text = "Mantenimiento Datos Clientes"
        '
        'dtFechaNac
        '
        Me.dtFechaNac.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFechaNac.Location = New System.Drawing.Point(129, 189)
        Me.dtFechaNac.Name = "dtFechaNac"
        Me.dtFechaNac.Size = New System.Drawing.Size(187, 20)
        Me.dtFechaNac.TabIndex = 83
        Me.dtFechaNac.TabStop = False
        Me.dtFechaNac.Text = "08/01/2015"
        Me.dtFechaNac.Value = New Date(2015, 1, 8, 17, 18, 29, 710)
        '
        'txtEmpresa
        '
        Me.txtEmpresa.Location = New System.Drawing.Point(435, 188)
        Me.txtEmpresa.Name = "txtEmpresa"
        Me.txtEmpresa.Size = New System.Drawing.Size(192, 20)
        Me.txtEmpresa.TabIndex = 82
        '
        'RadLabel12
        '
        Me.RadLabel12.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel12.Location = New System.Drawing.Point(371, 188)
        Me.RadLabel12.Name = "RadLabel12"
        Me.RadLabel12.Size = New System.Drawing.Size(58, 18)
        Me.RadLabel12.TabIndex = 81
        Me.RadLabel12.Text = "Empresa :"
        '
        'txtClieTelefono
        '
        Me.txtClieTelefono.Location = New System.Drawing.Point(435, 222)
        Me.txtClieTelefono.Name = "txtClieTelefono"
        Me.txtClieTelefono.Size = New System.Drawing.Size(192, 20)
        Me.txtClieTelefono.TabIndex = 80
        '
        'txtClieDireccion
        '
        Me.txtClieDireccion.AutoSize = False
        Me.txtClieDireccion.Location = New System.Drawing.Point(130, 316)
        Me.txtClieDireccion.Multiline = True
        Me.txtClieDireccion.Name = "txtClieDireccion"
        Me.txtClieDireccion.Size = New System.Drawing.Size(496, 36)
        Me.txtClieDireccion.TabIndex = 79
        '
        'txtClieEmail
        '
        Me.txtClieEmail.Location = New System.Drawing.Point(129, 283)
        Me.txtClieEmail.Name = "txtClieEmail"
        Me.txtClieEmail.Size = New System.Drawing.Size(497, 20)
        Me.txtClieEmail.TabIndex = 78
        '
        'txtClieReferencia
        '
        Me.txtClieReferencia.Location = New System.Drawing.Point(129, 255)
        Me.txtClieReferencia.Name = "txtClieReferencia"
        Me.txtClieReferencia.Size = New System.Drawing.Size(498, 20)
        Me.txtClieReferencia.TabIndex = 77
        '
        'txtClieNumeroDoc
        '
        Me.txtClieNumeroDoc.Location = New System.Drawing.Point(435, 151)
        Me.txtClieNumeroDoc.Name = "txtClieNumeroDoc"
        Me.txtClieNumeroDoc.Size = New System.Drawing.Size(192, 20)
        Me.txtClieNumeroDoc.TabIndex = 76
        '
        'txtClieOcupacion
        '
        Me.txtClieOcupacion.Location = New System.Drawing.Point(435, 115)
        Me.txtClieOcupacion.Name = "txtClieOcupacion"
        Me.txtClieOcupacion.Size = New System.Drawing.Size(192, 20)
        Me.txtClieOcupacion.TabIndex = 75
        '
        'txtClieApeMaterno
        '
        Me.txtClieApeMaterno.Location = New System.Drawing.Point(435, 81)
        Me.txtClieApeMaterno.Name = "txtClieApeMaterno"
        Me.txtClieApeMaterno.Size = New System.Drawing.Size(192, 20)
        Me.txtClieApeMaterno.TabIndex = 74
        '
        'txtClieApePaterno
        '
        Me.txtClieApePaterno.Location = New System.Drawing.Point(129, 81)
        Me.txtClieApePaterno.Name = "txtClieApePaterno"
        Me.txtClieApePaterno.Size = New System.Drawing.Size(187, 20)
        Me.txtClieApePaterno.TabIndex = 73
        '
        'txtClieNombres
        '
        Me.txtClieNombres.Location = New System.Drawing.Point(130, 53)
        Me.txtClieNombres.Name = "txtClieNombres"
        Me.txtClieNombres.Size = New System.Drawing.Size(497, 20)
        Me.txtClieNombres.TabIndex = 72
        '
        'txtClieCodigo
        '
        Me.txtClieCodigo.Location = New System.Drawing.Point(130, 26)
        Me.txtClieCodigo.Name = "txtClieCodigo"
        Me.txtClieCodigo.ReadOnly = True
        Me.txtClieCodigo.Size = New System.Drawing.Size(186, 20)
        Me.txtClieCodigo.TabIndex = 71
        '
        'RadLabel11
        '
        Me.RadLabel11.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel11.Location = New System.Drawing.Point(364, 116)
        Me.RadLabel11.Name = "RadLabel11"
        Me.RadLabel11.Size = New System.Drawing.Size(69, 18)
        Me.RadLabel11.TabIndex = 70
        Me.RadLabel11.Text = "Ocupacion :"
        '
        'RadLabel10
        '
        Me.RadLabel10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel10.Location = New System.Drawing.Point(82, 285)
        Me.RadLabel10.Name = "RadLabel10"
        Me.RadLabel10.Size = New System.Drawing.Size(41, 18)
        Me.RadLabel10.TabIndex = 69
        Me.RadLabel10.Text = "Email :"
        '
        'cboSexo
        '
        Me.cboSexo.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        RadListDataItem3.Text = "MASCULINO"
        RadListDataItem3.TextWrap = True
        RadListDataItem4.Text = "FEMENINO"
        RadListDataItem4.TextWrap = True
        Me.cboSexo.Items.Add(RadListDataItem3)
        Me.cboSexo.Items.Add(RadListDataItem4)
        Me.cboSexo.Location = New System.Drawing.Point(130, 115)
        Me.cboSexo.Name = "cboSexo"
        '
        '
        '
        Me.cboSexo.RootElement.StretchVertically = True
        Me.cboSexo.Size = New System.Drawing.Size(187, 20)
        Me.cboSexo.TabIndex = 68
        '
        'RadLabel9
        '
        Me.RadLabel9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel9.Location = New System.Drawing.Point(373, 222)
        Me.RadLabel9.Name = "RadLabel9"
        Me.RadLabel9.Size = New System.Drawing.Size(60, 18)
        Me.RadLabel9.TabIndex = 67
        Me.RadLabel9.Text = "Telofono :"
        '
        'RadLabel7
        '
        Me.RadLabel7.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel7.Location = New System.Drawing.Point(354, 82)
        Me.RadLabel7.Name = "RadLabel7"
        Me.RadLabel7.Size = New System.Drawing.Size(79, 18)
        Me.RadLabel7.TabIndex = 66
        Me.RadLabel7.Text = "Ap. Materno :"
        '
        'RadLabel5
        '
        Me.RadLabel5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel5.Location = New System.Drawing.Point(48, 82)
        Me.RadLabel5.Name = "RadLabel5"
        Me.RadLabel5.Size = New System.Drawing.Size(75, 18)
        Me.RadLabel5.TabIndex = 64
        Me.RadLabel5.Text = "Ap. Paterno :"
        '
        'RadLabel3
        '
        Me.RadLabel3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel3.Location = New System.Drawing.Point(72, 26)
        Me.RadLabel3.Name = "RadLabel3"
        Me.RadLabel3.Size = New System.Drawing.Size(51, 18)
        Me.RadLabel3.TabIndex = 61
        Me.RadLabel3.Text = "Código :"
        '
        'RadLabel29
        '
        Me.RadLabel29.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel29.Location = New System.Drawing.Point(61, 316)
        Me.RadLabel29.Name = "RadLabel29"
        Me.RadLabel29.Size = New System.Drawing.Size(62, 18)
        Me.RadLabel29.TabIndex = 47
        Me.RadLabel29.Text = "Dirección :"
        '
        'cboEstadoCivil
        '
        Me.cboEstadoCivil.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        RadListDataItem5.Text = "SOLTERO"
        RadListDataItem5.TextWrap = True
        RadListDataItem6.Text = "CASADO"
        RadListDataItem6.TextWrap = True
        RadListDataItem7.Text = "DIVORCIADO"
        RadListDataItem7.TextWrap = True
        RadListDataItem8.Text = "VIUDO"
        RadListDataItem8.TextWrap = True
        Me.cboEstadoCivil.Items.Add(RadListDataItem5)
        Me.cboEstadoCivil.Items.Add(RadListDataItem6)
        Me.cboEstadoCivil.Items.Add(RadListDataItem7)
        Me.cboEstadoCivil.Items.Add(RadListDataItem8)
        Me.cboEstadoCivil.Location = New System.Drawing.Point(130, 220)
        Me.cboEstadoCivil.Name = "cboEstadoCivil"
        '
        '
        '
        Me.cboEstadoCivil.RootElement.StretchVertically = True
        Me.cboEstadoCivil.Size = New System.Drawing.Size(186, 20)
        Me.cboEstadoCivil.TabIndex = 56
        '
        'cboTipoDocumento
        '
        Me.cboTipoDocumento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboTipoDocumento.Location = New System.Drawing.Point(129, 151)
        Me.cboTipoDocumento.Name = "cboTipoDocumento"
        '
        '
        '
        Me.cboTipoDocumento.RootElement.StretchVertically = True
        Me.cboTipoDocumento.Size = New System.Drawing.Size(187, 20)
        Me.cboTipoDocumento.TabIndex = 55
        '
        'RadLabel30
        '
        Me.RadLabel30.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel30.Location = New System.Drawing.Point(85, 116)
        Me.RadLabel30.Name = "RadLabel30"
        Me.RadLabel30.Size = New System.Drawing.Size(38, 18)
        Me.RadLabel30.TabIndex = 47
        Me.RadLabel30.Text = "Sexo :"
        '
        'RadLabel17
        '
        Me.RadLabel17.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel17.Location = New System.Drawing.Point(21, 152)
        Me.RadLabel17.Name = "RadLabel17"
        Me.RadLabel17.Size = New System.Drawing.Size(102, 18)
        Me.RadLabel17.TabIndex = 47
        Me.RadLabel17.Text = "Tipo Documento :"
        '
        'RadLabel14
        '
        Me.RadLabel14.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel14.Location = New System.Drawing.Point(16, 189)
        Me.RadLabel14.Name = "RadLabel14"
        Me.RadLabel14.Size = New System.Drawing.Size(107, 18)
        Me.RadLabel14.TabIndex = 50
        Me.RadLabel14.Text = "Fecha Nacimiento :"
        '
        'RadLabel4
        '
        Me.RadLabel4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel4.Location = New System.Drawing.Point(54, 255)
        Me.RadLabel4.Name = "RadLabel4"
        Me.RadLabel4.Size = New System.Drawing.Size(69, 18)
        Me.RadLabel4.TabIndex = 49
        Me.RadLabel4.Text = "Referencia :"
        '
        'RadLabel26
        '
        Me.RadLabel26.Location = New System.Drawing.Point(212, 79)
        Me.RadLabel26.Name = "RadLabel26"
        Me.RadLabel26.Size = New System.Drawing.Size(2, 2)
        Me.RadLabel26.TabIndex = 48
        '
        'RadLabel6
        '
        Me.RadLabel6.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel6.Location = New System.Drawing.Point(341, 152)
        Me.RadLabel6.Name = "RadLabel6"
        Me.RadLabel6.Size = New System.Drawing.Size(92, 18)
        Me.RadLabel6.TabIndex = 1
        Me.RadLabel6.Text = "Nº Documento :"
        '
        'RadLabel8
        '
        Me.RadLabel8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel8.Location = New System.Drawing.Point(49, 222)
        Me.RadLabel8.Name = "RadLabel8"
        Me.RadLabel8.Size = New System.Drawing.Size(74, 18)
        Me.RadLabel8.TabIndex = 2
        Me.RadLabel8.Text = "Estado Civil :"
        '
        'RadLabel23
        '
        Me.RadLabel23.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel23.Location = New System.Drawing.Point(62, 52)
        Me.RadLabel23.Name = "RadLabel23"
        Me.RadLabel23.Size = New System.Drawing.Size(61, 18)
        Me.RadLabel23.TabIndex = 46
        Me.RadLabel23.Text = "Nombres :"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(934, 408)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(134, 29)
        Me.btnCancelar.TabIndex = 60
        Me.btnCancelar.Text = "Cancelar"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(729, 408)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(141, 29)
        Me.btnGuardar.TabIndex = 59
        Me.btnGuardar.Text = "Guardar"
        '
        'txtNombres
        '
        Me.txtNombres.Location = New System.Drawing.Point(159, 51)
        Me.txtNombres.Name = "txtNombres"
        Me.txtNombres.Size = New System.Drawing.Size(476, 20)
        Me.txtNombres.TabIndex = 36
        '
        'txtDocVenta
        '
        Me.txtDocVenta.Location = New System.Drawing.Point(468, 150)
        Me.txtDocVenta.Name = "txtDocVenta"
        Me.txtDocVenta.ReadOnly = True
        Me.txtDocVenta.Size = New System.Drawing.Size(100, 20)
        Me.txtDocVenta.TabIndex = 35
        '
        'txtReferencia
        '
        Me.txtReferencia.Location = New System.Drawing.Point(158, 255)
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(487, 20)
        Me.txtReferencia.TabIndex = 54
        '
        'txtGlosa
        '
        Me.txtGlosa.AutoSize = False
        Me.txtGlosa.Location = New System.Drawing.Point(158, 318)
        Me.txtGlosa.Name = "txtGlosa"
        Me.txtGlosa.Size = New System.Drawing.Size(487, 35)
        Me.txtGlosa.TabIndex = 47
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(158, 25)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(100, 20)
        Me.txtCodigo.TabIndex = 62
        '
        'txtApePaterno
        '
        Me.txtApePaterno.Location = New System.Drawing.Point(159, 81)
        Me.txtApePaterno.Name = "txtApePaterno"
        Me.txtApePaterno.Size = New System.Drawing.Size(186, 20)
        Me.txtApePaterno.TabIndex = 63
        '
        'txtApeMaterno
        '
        Me.txtApeMaterno.Location = New System.Drawing.Point(455, 81)
        Me.txtApeMaterno.Name = "txtApeMaterno"
        Me.txtApeMaterno.Size = New System.Drawing.Size(180, 20)
        Me.txtApeMaterno.TabIndex = 65
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(473, 220)
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(100, 20)
        Me.txtTelefono.TabIndex = 52
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(157, 279)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(487, 20)
        Me.txtEmail.TabIndex = 70
        '
        'cboEmpresa
        '
        Me.cboEmpresa.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboEmpresa.Location = New System.Drawing.Point(76, 65)
        Me.cboEmpresa.Name = "cboEmpresa"
        '
        '
        '
        Me.cboEmpresa.RootElement.StretchVertically = True
        Me.cboEmpresa.Size = New System.Drawing.Size(297, 20)
        Me.cboEmpresa.TabIndex = 59
        '
        'RadLabel15
        '
        Me.RadLabel15.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel15.Location = New System.Drawing.Point(14, 65)
        Me.RadLabel15.Name = "RadLabel15"
        Me.RadLabel15.Size = New System.Drawing.Size(61, 18)
        Me.RadLabel15.TabIndex = 58
        Me.RadLabel15.Text = "Empresa  :"
        '
        'MECO
        '
        Me.MECO.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MECO_ELIMINAR})
        Me.MECO.Name = "MECO"
        Me.MECO.Size = New System.Drawing.Size(119, 26)
        Me.MECO.Text = "Opciones"
        '
        'MECO_ELIMINAR
        '
        Me.MECO_ELIMINAR.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.MECO_ELIMINAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_ELIMINAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_ELIMINAR.Name = "MECO_ELIMINAR"
        Me.MECO_ELIMINAR.Size = New System.Drawing.Size(118, 22)
        Me.MECO_ELIMINAR.Text = "Eliminar"
        '
        'cboEstado
        '
        Me.cboEstado.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        RadListDataItem1.Text = "ACTIVO"
        RadListDataItem1.TextWrap = True
        RadListDataItem2.Text = "NO ACTIVO"
        RadListDataItem2.TextWrap = True
        Me.cboEstado.Items.Add(RadListDataItem1)
        Me.cboEstado.Items.Add(RadListDataItem2)
        Me.cboEstado.Location = New System.Drawing.Point(743, 142)
        Me.cboEstado.Name = "cboEstado"
        '
        '
        '
        Me.cboEstado.RootElement.StretchVertically = True
        Me.cboEstado.Size = New System.Drawing.Size(233, 20)
        Me.cboEstado.TabIndex = 61
        '
        'RadLabel18
        '
        Me.RadLabel18.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel18.Location = New System.Drawing.Point(688, 142)
        Me.RadLabel18.Name = "RadLabel18"
        Me.RadLabel18.Size = New System.Drawing.Size(48, 18)
        Me.RadLabel18.TabIndex = 60
        Me.RadLabel18.Text = "Estado :"
        '
        'RadLabel19
        '
        Me.RadLabel19.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel19.Location = New System.Drawing.Point(21, 33)
        Me.RadLabel19.Name = "RadLabel19"
        Me.RadLabel19.Size = New System.Drawing.Size(39, 18)
        Me.RadLabel19.TabIndex = 65
        Me.RadLabel19.Text = "Obra :"
        '
        'cboObrasCliente
        '
        Me.cboObrasCliente.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboObrasCliente.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboObrasCliente.Location = New System.Drawing.Point(66, 33)
        Me.cboObrasCliente.Name = "cboObrasCliente"
        Me.cboObrasCliente.Size = New System.Drawing.Size(348, 20)
        Me.cboObrasCliente.TabIndex = 66
        '
        'cboDepartamento
        '
        Me.cboDepartamento.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboDepartamento.Location = New System.Drawing.Point(743, 52)
        Me.cboDepartamento.Name = "cboDepartamento"
        '
        '
        '
        Me.cboDepartamento.RootElement.StretchVertically = True
        Me.cboDepartamento.Size = New System.Drawing.Size(233, 20)
        Me.cboDepartamento.TabIndex = 85
        '
        'RadLabel20
        '
        Me.RadLabel20.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel20.Location = New System.Drawing.Point(651, 54)
        Me.RadLabel20.Name = "RadLabel20"
        Me.RadLabel20.Size = New System.Drawing.Size(90, 18)
        Me.RadLabel20.TabIndex = 84
        Me.RadLabel20.Text = "Departamento :"
        '
        'cboProvincia
        '
        Me.cboProvincia.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboProvincia.Location = New System.Drawing.Point(743, 81)
        Me.cboProvincia.Name = "cboProvincia"
        '
        '
        '
        Me.cboProvincia.RootElement.StretchVertically = True
        Me.cboProvincia.Size = New System.Drawing.Size(233, 20)
        Me.cboProvincia.TabIndex = 87
        '
        'RadLabel21
        '
        Me.RadLabel21.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel21.Location = New System.Drawing.Point(679, 83)
        Me.RadLabel21.Name = "RadLabel21"
        Me.RadLabel21.Size = New System.Drawing.Size(62, 18)
        Me.RadLabel21.TabIndex = 86
        Me.RadLabel21.Text = "Provincia :"
        '
        'cboDistrito
        '
        Me.cboDistrito.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboDistrito.Location = New System.Drawing.Point(743, 110)
        Me.cboDistrito.Name = "cboDistrito"
        '
        '
        '
        Me.cboDistrito.RootElement.StretchVertically = True
        Me.cboDistrito.Size = New System.Drawing.Size(233, 20)
        Me.cboDistrito.TabIndex = 89
        '
        'RadLabel22
        '
        Me.RadLabel22.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel22.Location = New System.Drawing.Point(689, 112)
        Me.RadLabel22.Name = "RadLabel22"
        Me.RadLabel22.Size = New System.Drawing.Size(52, 18)
        Me.RadLabel22.TabIndex = 88
        Me.RadLabel22.Text = "Distrito :"
        '
        'FrmClientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1130, 596)
        Me.Controls.Add(Me.RAD_PAGE)
        Me.Name = "FrmClientes"
        '
        '
        '
        Me.RootElement.ApplyShapeToControl = True
        Me.Text = "Mantenimiento de Clientes"
        CType(Me.RAD_PAGE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RAD_PAGE.ResumeLayout(False)
        Me.PAGE_CONSULTA.ResumeLayout(False)
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox3.ResumeLayout(False)
        Me.RadGroupBox3.PerformLayout()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaClientes.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaClientes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox1.ResumeLayout(False)
        Me.RadGroupBox1.PerformLayout()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox2.ResumeLayout(False)
        Me.RadGroupBox2.PerformLayout()
        CType(Me.RadLabel16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PAGE_MANTENIMIENTO.ResumeLayout(False)
        CType(Me.RadGroupBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox4.ResumeLayout(False)
        Me.RadGroupBox4.PerformLayout()
        CType(Me.RadLabel13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GRUPO_VENTA_LINEA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRUPO_VENTA_LINEA.ResumeLayout(False)
        Me.GRUPO_VENTA_LINEA.PerformLayout()
        CType(Me.dtFechaNac, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieTelefono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieDireccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieReferencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieNumeroDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieOcupacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieApeMaterno, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieApePaterno, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieNombres, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtClieCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboSexo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEstadoCivil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboTipoDocumento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNombres, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDocVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtReferencia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGlosa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCodigo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtApePaterno, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtApeMaterno, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTelefono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtEmail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MECO.ResumeLayout(False)
        CType(Me.cboEstado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboObrasCliente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDepartamento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboProvincia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDistrito, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RAD_PAGE As Telerik.WinControls.UI.RadPageView
    Friend WithEvents PAGE_CONSULTA As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents rgListaClientes As Telerik.WinControls.UI.RadGridView
    Friend WithEvents PAGE_MANTENIMIENTO As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents RadGroupBox3 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadGroupBox1 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents btnNuevo As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadGroupBox2 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel16 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents GRUPO_VENTA_LINEA As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel10 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboSexo As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel9 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel7 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel5 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents btnCancelar As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnGuardar As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadLabel29 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboEstadoCivil As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents cboTipoDocumento As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel30 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel17 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel14 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel4 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel26 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel6 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel8 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel23 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtNombres As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtDocVenta As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtReferencia As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtGlosa As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtCodigo As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtApePaterno As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtApeMaterno As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtTelefono As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtEmail As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel11 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtClieTelefono As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieDireccion As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieEmail As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieReferencia As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieNumeroDoc As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieOcupacion As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieApeMaterno As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieApePaterno As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieNombres As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtClieCodigo As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtEmpresa As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel12 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadGroupBox4 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel13 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents dtFechaNac As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents cboEmpresa As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel15 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents MECO As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MECO_ELIMINAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboEstado As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel18 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel19 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboObrasCliente As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents cboDistrito As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel22 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboProvincia As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel21 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboDepartamento As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel20 As Telerik.WinControls.UI.RadLabel
End Class

