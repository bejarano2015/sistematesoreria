﻿Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad
Imports CapaEntidad.Mantenimiento
Public Class WFAgregarPartidas
    Public WithEvents cmr As CurrencyManager
    Friend frm As New frmPartidas

    Dim VL_SrvPartida As SrvPartida()
    Dim VL_BeanPartida As BeanPartida()


#Region "Instanciar"
    Private Shared frmInstance As WFAgregarPartidas = Nothing
    Public Shared Function Instance() As WFAgregarPartidas
        If frmInstance Is Nothing Then
            frmInstance = New WFAgregarPartidas
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
#End Region


#Region "Decaraciones "
    Dim VL_ParCodigo As String
    Dim VL_ParDescripcion As String
#End Region


#Region "acciones heredadas"
    Private Sub v_SalidaReg() Handles Me.SalidaReg
        frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub
    Private Sub WFAgregarPartidas_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub

#End Region




#Region "Proceso"
    Private Sub ListarParidas()
        Dim VL_SrvPartida As New SrvPartida()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim VL_BeanPartida = New BeanPartida("")

        Try
            VL_BeanResultado = VL_SrvPartida.Fnc_listar_partida(VL_BeanPartida)

            If VL_BeanResultado.blnExiste = True Then
                cboPartidas.DataSource = VL_BeanResultado.dtResultado
                cboPartidas.DisplayMember = "PARDESCRIPCION"
                cboPartidas.ValueMember = "PARCODIGO"

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub ListarUnidadMedida()

        Dim VL_SrvUnidadMedida As New SrvUnidadMedida()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvUnidadMedida.Fnc_listar_UnidadMedida()

            If VL_BeanResultado.blnExiste = True Then
                cboUnidadMedida.DataSource = VL_BeanResultado.dtResultado
                cboUnidadMedida.DisplayMember = "UmdDescripcion"
                cboUnidadMedida.ValueMember = "UmdCodigo"

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub


#End Region

    Private Sub txtFindPartida_TextChanged(sender As Object, e As EventArgs)
        ListarParidas()
    End Sub

    Private Sub AgregarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AgregarToolStripMenuItem.Click

        'For i As Integer = 0 To dgvPartidas.RowCount - 1
        '    If (dgvPartidas.Rows(i).Selected = True) Then
        '        VL_ParCodigo = dgvPartidas.Rows(i).Cells(0).Value.ToString()
        '        VL_ParDescripcion = dgvPartidas.Rows(i).Cells(1).Value.ToString()

        '        dgvPresupuesto.Rows.Add()
        '    End If

        'Next

    End Sub

    Private Sub WFAgregarPartidas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListarParidas()
        ListarUnidadMedida()
    End Sub

End Class
