﻿Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad.Mantenimiento
Imports CapaEntidad.Proceso
Imports CapaEntidad
Imports BusinessLogicLayer.Procesos
Public Class WFGestionDocumento



#Region "Instanciar"
    Private Shared frmInstance As WFGestionDocumento = Nothing
    Public Shared Function Instance() As WFGestionDocumento
        If frmInstance Is Nothing Then
            frmInstance = New WFGestionDocumento
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
#End Region


#Region "Procedimientos"

    Private Sub ListarTipoDocumento()

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()

        Try
            VL_BeanResultado = VL_GestionDocumento.Fnc_Listar_TipoDocumento()

            If VL_BeanResultado.blnExiste = True Then
                cboDocumentoConcepto.DataSource = VL_BeanResultado.dtResultado
                cboDocumentoConcepto.DisplayMember = "DescripDoc"
                cboDocumentoConcepto.ValueMember = "IdDocumento"

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub ListarBancoEmpresa(ByVal VL_NOMBRE As String)

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()

        Try
            VL_BeanResultado = VL_GestionDocumento.Fnc_Listar_BancoEmpresa(VL_NOMBRE)

            If VL_BeanResultado.blnExiste = True Then
                rgvBancoEmpresa.DataSource = VL_BeanResultado.dtResultado


            Else

                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub ListarEmpresa()

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()

        Try
            VL_BeanResultado = VL_GestionDocumento.Fnc_Listar_Empresa()

            If VL_BeanResultado.blnExiste = True Then
                cboEmpresa.DataSource = VL_BeanResultado.dtResultado
                cboEmpresa.DisplayMember = "EmprDescripcion"
                cboEmpresa.ValueMember = "EmprCodigo"

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub Registrar_GestionDocumentario()

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        objGestionDocumentaria = New CapaEntidad.Proceso.BeanGestionDocumentaria(VL_IDDOCUMENTO, VL_NRODOCUMENTO, VL_BANCO_EMPRESA_CODIGO, VL_EMPRCODIGO, VL_DIASRENOVACION, VL_GLOSA)

        VL_BeanResultado = VL_GestionDocumento.Fnc_Registrar_GestionDocumentario_Grabar(objGestionDocumentaria)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub
    Private Sub Listar_GestionDocumentario()

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()

        Try
            VL_BeanResultado = VL_GestionDocumento.Fnc_GestionDocumentario_Listar()

            If VL_BeanResultado.blnExiste = True Then
                rgvLista.DataSource = VL_BeanResultado.dtResultado

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

#End Region


#Region "Variables"

    Dim VL_IDCABECERA As Integer
    Dim VL_IDDOCUMENTO As String
    Dim VL_NRODOCUMENTO As String
    Dim VL_BANCO_EMPRESA_CODIGO As String
    Dim VL_EMPRCODIGO As String
    Dim VL_DIASRENOVACION As Integer
    Dim VL_GLOSA As String



    Dim objGestionDocumentaria As BeanGestionDocumentaria




#End Region


    Private Sub WFGestionDocumento_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ListarEmpresa()
        ListarTipoDocumento()
        Listar_GestionDocumentario()

    End Sub

    Private Sub RadTextBox3_TextChanged(sender As Object, e As EventArgs) Handles txtBancoEmpresa.TextChanged

        If txtBancoEmpresa.Text.Length > 0 Then

            If rgvBancoEmpresa.Rows.Count > 0 Then
                rgvBancoEmpresa.Visible = True
            Else
                rgvBancoEmpresa.Visible = False
            End If
        Else

            rgvBancoEmpresa.Visible = False

        End If

        ListarBancoEmpresa(txtBancoEmpresa.Text)



    End Sub

    Private Sub rgvBancoEmpresa_Click(sender As Object, e As EventArgs) Handles rgvBancoEmpresa.Click

        If rgvBancoEmpresa.Rows.Count > 0 Then

            VL_BANCO_EMPRESA_CODIGO = rgvBancoEmpresa.CurrentRow.Cells("CODIGO").Value
            rgvBancoEmpresa.Visible = False
        End If



    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        VL_IDDOCUMENTO = cboDocumentoConcepto.SelectedValue.ToString()
        VL_NRODOCUMENTO = txtNroDocumento.Text
        VL_DIASRENOVACION = Convert.ToInt16(txtDiasRenovacion.Text)
        VL_EMPRCODIGO = cboEmpresa.SelectedValue.ToString()
        VL_GLOSA = txtGlosa.Text


        Registrar_GestionDocumentario()

    End Sub

    Private Sub rmiDetalle_Click(sender As Object, e As EventArgs) Handles rmiDetalle.Click

        Using frm As New WFGestionDocumentoDetalle(VL_IDCABECERA)


            Dim result = frm.ShowDialog

            If result = DialogResult.OK Then

                MessageBox.Show("salio")

            End If


        End Using



    End Sub

    Private Sub rgvLista_ContextMenuOpening(sender As Object, e As Telerik.WinControls.UI.ContextMenuOpeningEventArgs) Handles rgvLista.ContextMenuOpening


        VL_IDCABECERA = Convert.ToInt16(rgvLista.CurrentRow.Cells("IDCABECERA").Value)


    End Sub
End Class