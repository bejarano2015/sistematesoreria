﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WFAgregarPartida
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtParcial = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtPrecio = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMetrado = New System.Windows.Forms.TextBox()
        Me.cboPartidas = New System.Windows.Forms.ComboBox()
        Me.cboUnidadMedida = New System.Windows.Forms.ComboBox()
        Me.cboTipoPartida = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnAgregarPartida = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.dgvPresupuesto = New System.Windows.Forms.DataGridView()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.EmprCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCosCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UbtCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConsCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ItemPresupuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ParDescripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ParCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Metrado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioPresupuesto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PrecioSubcontrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UMedida = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PtiCodigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Partida = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPresupuesto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtParcial)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtPrecio)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtMetrado)
        Me.GroupBox1.Controls.Add(Me.cboPartidas)
        Me.GroupBox1.Controls.Add(Me.cboUnidadMedida)
        Me.GroupBox1.Location = New System.Drawing.Point(48, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(682, 139)
        Me.GroupBox1.TabIndex = 32
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Descripcion"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Und.Medida"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(31, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Metrado"
        '
        'txtParcial
        '
        Me.txtParcial.Location = New System.Drawing.Point(112, 114)
        Me.txtParcial.Name = "txtParcial"
        Me.txtParcial.Size = New System.Drawing.Size(100, 20)
        Me.txtParcial.TabIndex = 22
        Me.txtParcial.Text = "0.00"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(40, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Precio"
        '
        'txtPrecio
        '
        Me.txtPrecio.Location = New System.Drawing.Point(112, 89)
        Me.txtPrecio.Name = "txtPrecio"
        Me.txtPrecio.Size = New System.Drawing.Size(100, 20)
        Me.txtPrecio.TabIndex = 21
        Me.txtPrecio.Text = "0.00"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(38, 121)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Parcial"
        '
        'txtMetrado
        '
        Me.txtMetrado.Location = New System.Drawing.Point(112, 63)
        Me.txtMetrado.Name = "txtMetrado"
        Me.txtMetrado.Size = New System.Drawing.Size(100, 20)
        Me.txtMetrado.TabIndex = 20
        Me.txtMetrado.Text = "0.00"
        '
        'cboPartidas
        '
        Me.cboPartidas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPartidas.FormattingEnabled = True
        Me.cboPartidas.Location = New System.Drawing.Point(112, 13)
        Me.cboPartidas.Name = "cboPartidas"
        Me.cboPartidas.Size = New System.Drawing.Size(482, 21)
        Me.cboPartidas.TabIndex = 18
        '
        'cboUnidadMedida
        '
        Me.cboUnidadMedida.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUnidadMedida.FormattingEnabled = True
        Me.cboUnidadMedida.Location = New System.Drawing.Point(112, 38)
        Me.cboUnidadMedida.Name = "cboUnidadMedida"
        Me.cboUnidadMedida.Size = New System.Drawing.Size(121, 21)
        Me.cboUnidadMedida.TabIndex = 19
        '
        'cboTipoPartida
        '
        Me.cboTipoPartida.FormattingEnabled = True
        Me.cboTipoPartida.Items.AddRange(New Object() {"Adenda", "Diferencial"})
        Me.cboTipoPartida.Location = New System.Drawing.Point(160, 30)
        Me.cboTipoPartida.Name = "cboTipoPartida"
        Me.cboTipoPartida.Size = New System.Drawing.Size(121, 21)
        Me.cboTipoPartida.TabIndex = 31
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(61, 33)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Tipo Partida"
        '
        'btnAgregarPartida
        '
        Me.btnAgregarPartida.Location = New System.Drawing.Point(636, 194)
        Me.btnAgregarPartida.Name = "btnAgregarPartida"
        Me.btnAgregarPartida.Size = New System.Drawing.Size(75, 23)
        Me.btnAgregarPartida.TabIndex = 29
        Me.btnAgregarPartida.Text = "Agregar"
        Me.btnAgregarPartida.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(429, 411)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 28
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'dgvPresupuesto
        '
        Me.dgvPresupuesto.AllowUserToAddRows = False
        Me.dgvPresupuesto.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EmprCodigo, Me.CCosCodigo, Me.UbtCodigo, Me.ConsCodigo, Me.ItemPresupuesto, Me.ParDescripcion, Me.ParCodigo, Me.Metrado, Me.PrecioPresupuesto, Me.PrecioSubcontrato, Me.UMedida, Me.PtiCodigo, Me.Partida})
        Me.dgvPresupuesto.Location = New System.Drawing.Point(91, 223)
        Me.dgvPresupuesto.Name = "dgvPresupuesto"
        Me.dgvPresupuesto.Size = New System.Drawing.Size(682, 171)
        Me.dgvPresupuesto.TabIndex = 27
        '
        'btnGrabar
        '
        Me.btnGrabar.Location = New System.Drawing.Point(670, 411)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(75, 23)
        Me.btnGrabar.TabIndex = 26
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'EmprCodigo
        '
        Me.EmprCodigo.DataPropertyName = "EmprCodigo"
        Me.EmprCodigo.HeaderText = "Empresa"
        Me.EmprCodigo.Name = "EmprCodigo"
        Me.EmprCodigo.Visible = False
        '
        'CCosCodigo
        '
        Me.CCosCodigo.DataPropertyName = "CCosCodigo"
        Me.CCosCodigo.HeaderText = "CCosCodigo"
        Me.CCosCodigo.Name = "CCosCodigo"
        Me.CCosCodigo.Visible = False
        '
        'UbtCodigo
        '
        Me.UbtCodigo.DataPropertyName = "UbtCodigo"
        Me.UbtCodigo.HeaderText = "UbtCodigo"
        Me.UbtCodigo.Name = "UbtCodigo"
        Me.UbtCodigo.Visible = False
        '
        'ConsCodigo
        '
        Me.ConsCodigo.DataPropertyName = "ConsCodigo"
        Me.ConsCodigo.HeaderText = "ConsCodigo"
        Me.ConsCodigo.Name = "ConsCodigo"
        Me.ConsCodigo.Visible = False
        '
        'ItemPresupuesto
        '
        Me.ItemPresupuesto.DataPropertyName = "ItemPresupuesto"
        Me.ItemPresupuesto.HeaderText = "Item"
        Me.ItemPresupuesto.Name = "ItemPresupuesto"
        Me.ItemPresupuesto.Width = 10
        '
        'ParDescripcion
        '
        Me.ParDescripcion.DataPropertyName = "ParDescripcion"
        Me.ParDescripcion.HeaderText = "Descripcion"
        Me.ParDescripcion.Name = "ParDescripcion"
        Me.ParDescripcion.Width = 200
        '
        'ParCodigo
        '
        Me.ParCodigo.DataPropertyName = "ParCodigo"
        Me.ParCodigo.HeaderText = "ParCodigo"
        Me.ParCodigo.Name = "ParCodigo"
        Me.ParCodigo.Visible = False
        '
        'Metrado
        '
        Me.Metrado.DataPropertyName = "Metrado"
        Me.Metrado.HeaderText = "Metrado"
        Me.Metrado.Name = "Metrado"
        '
        'PrecioPresupuesto
        '
        Me.PrecioPresupuesto.DataPropertyName = "PrecioPresupuesto"
        Me.PrecioPresupuesto.HeaderText = "PrecioPresupuesto"
        Me.PrecioPresupuesto.Name = "PrecioPresupuesto"
        '
        'PrecioSubcontrato
        '
        Me.PrecioSubcontrato.DataPropertyName = "PrecioSubcontrato"
        Me.PrecioSubcontrato.HeaderText = "PrecioSubcontrato"
        Me.PrecioSubcontrato.Name = "PrecioSubcontrato"
        Me.PrecioSubcontrato.Visible = False
        '
        'UMedida
        '
        Me.UMedida.DataPropertyName = "UMedida"
        Me.UMedida.HeaderText = "Und_codigo"
        Me.UMedida.Name = "UMedida"
        Me.UMedida.Visible = False
        '
        'PtiCodigo
        '
        Me.PtiCodigo.DataPropertyName = "PtiCodigo"
        Me.PtiCodigo.HeaderText = "PtiCodigo"
        Me.PtiCodigo.Name = "PtiCodigo"
        Me.PtiCodigo.Visible = False
        '
        'Partida
        '
        Me.Partida.DataPropertyName = "Partida"
        Me.Partida.HeaderText = "Partida"
        Me.Partida.Name = "Partida"
        '
        'WFAgregarPartida
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(903, 522)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cboTipoPartida)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnAgregarPartida)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.dgvPresupuesto)
        Me.Controls.Add(Me.btnGrabar)
        Me.Name = "WFAgregarPartida"
        Me.Text = "WFAgregarPartida"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvPresupuesto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtParcial As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPrecio As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtMetrado As System.Windows.Forms.TextBox
    Friend WithEvents cboPartidas As System.Windows.Forms.ComboBox
    Friend WithEvents cboUnidadMedida As System.Windows.Forms.ComboBox
    Friend WithEvents cboTipoPartida As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnAgregarPartida As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents dgvPresupuesto As System.Windows.Forms.DataGridView
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents EmprCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCosCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UbtCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConsCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ItemPresupuesto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ParDescripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ParCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Metrado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioPresupuesto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioSubcontrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UMedida As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PtiCodigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Partida As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
