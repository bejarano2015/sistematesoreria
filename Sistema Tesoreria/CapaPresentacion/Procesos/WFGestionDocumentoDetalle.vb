﻿Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad.Mantenimiento
Imports CapaEntidad.Proceso
Imports CapaEntidad
Imports BusinessLogicLayer.Procesos


Public Class WFGestionDocumentoDetalle

#Region "Procedimientos"

    Private Sub Registrar_GestionDocumentarioDetalle()

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        objGestionDocumentariaDetalle = New CapaEntidad.Proceso.BeanGestionDocumentariaDetalle(VL_IDCABECERA, VL_FECHAINICIO, VL_FECHAVENCIMIENTO, VL_IMPORTEPACTADO, VL_MONEDA, VL_UNIDADESAFECTADAS)

        VL_BeanResultado = VL_GestionDocumento.Fnc_Registrar_GestionDocumentario_Detalle_Grabar(objGestionDocumentariaDetalle)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub
    Private Sub Actualizar_GestionDocumentarioDetalle()

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        objGestionDocumentariaDetalle = New CapaEntidad.Proceso.BeanGestionDocumentariaDetalle(VL_IDCABECERA, VL_IDDETALLE, VL_FECHAINICIO, VL_FECHAVENCIMIENTO, VL_IMPORTEPACTADO, VL_MONEDA, VL_UNIDADESAFECTADAS)

        VL_BeanResultado = VL_GestionDocumento.Fnc_Actualizar_GestionDocumentario_Detalle_Grabar(objGestionDocumentariaDetalle)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub
    Private Sub Listar_GestionDocumentarioDetalle()

        Dim VL_GestionDocumento As New SrvGestionDocumento()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSelect()

        Try
            VL_BeanResultado = VL_GestionDocumento.Fnc_Listar_GestionDocumentarioDetalle_Listar(VL_IDCABECERA)

            If VL_BeanResultado.blnExiste = True Then
                rgvDetalle.DataSource = VL_BeanResultado.dtResultado

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub
    Private Sub Proc_Enabled(ByVal VL_BOOLEAN As Boolean)

        dtpInicio.Enabled = VL_BOOLEAN
        dtpFin.Enabled = VL_BOOLEAN
        txtImportePactado.Enabled = VL_BOOLEAN
        cboMoneda.Enabled = VL_BOOLEAN
        txtUnidadesAfectadas.Enabled = VL_BOOLEAN

    End Sub



#End Region


#Region "Variables"

    Dim VL_IDCABECERA As String
    Dim VL_IDDETALLE As String = ""
    Dim VL_FECHAINICIO As DateTime
    Dim VL_FECHAVENCIMIENTO As DateTime
    Dim VL_IMPORTEPACTADO As Decimal
    Dim VL_MONEDA As String
    Dim VL_UNIDADESAFECTADAS As String


    Dim objGestionDocumentariaDetalle As BeanGestionDocumentariaDetalle

#End Region


    Public Sub New(ByVal VL_IN_IDCABECERA)

        ' Llamada necesaria para el diseñador.
        InitializeComponent()
        VL_IDCABECERA = VL_IN_IDCABECERA
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub rgvDetalle_ContextMenuOpening(sender As Object, e As Telerik.WinControls.UI.ContextMenuOpeningEventArgs) Handles rgvDetalle.ContextMenuOpening

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        VL_FECHAINICIO = dtpInicio.Text
        VL_FECHAVENCIMIENTO = dtpFin.Text
        VL_IMPORTEPACTADO = Convert.ToDecimal(txtImportePactado.Text)

        VL_MONEDA = "01"
        If cboMoneda.Text = "Dolares" Then
            VL_MONEDA = "02"
        End If

        VL_UNIDADESAFECTADAS = txtUnidadesAfectadas.Text

        If VL_IDDETALLE = "" Then

            Registrar_GestionDocumentarioDetalle()
        Else

            Actualizar_GestionDocumentarioDetalle()

            VL_IDDETALLE = ""

        End If



        Listar_GestionDocumentarioDetalle()


        Proc_Enabled(False)
    End Sub

    Private Sub WFGestionDocumentoDetalle_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Listar_GestionDocumentarioDetalle()
    End Sub

    Private Sub RadPageView1_SelectedPageChanged(sender As Object, e As EventArgs) Handles RadPageView1.SelectedPageChanged

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        Proc_Enabled(True)
    End Sub

    Private Sub rmiEditar_Click(sender As Object, e As EventArgs) Handles rmiEditar.Click
        VL_IDDETALLE = Convert.ToInt16(rgvDetalle.CurrentRow.Cells("idDetalle").Value)
        dtpInicio.Text = Convert.ToString(rgvDetalle.CurrentRow.Cells("FechaInicio").Value)
        dtpFin.Text = Convert.ToString(rgvDetalle.CurrentRow.Cells("FechaVencimiento").Value)
        txtImportePactado.Text = Convert.ToString(rgvDetalle.CurrentRow.Cells("ImportePactado").Value)

        cboMoneda.Text = "Nuevos Soles"
        If Convert.ToString(rgvDetalle.CurrentRow.Cells("Moneda").Value) = "Dolares" Then
            cboMoneda.Text = "Dolares"
        End If

        txtUnidadesAfectadas.Text = Convert.ToString(rgvDetalle.CurrentRow.Cells("UnidadesAfectadas").Value)

        Proc_Enabled(True)

    End Sub
End Class