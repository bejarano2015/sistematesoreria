﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WFGestionDocumentoDetalle
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim RadListDataItem1 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim RadListDataItem2 As Telerik.WinControls.UI.RadListDataItem = New Telerik.WinControls.UI.RadListDataItem()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.RadPageView1 = New Telerik.WinControls.UI.RadPageView()
        Me.RadPageViewPage1 = New Telerik.WinControls.UI.RadPageViewPage()
        Me.btnGuardar = New Telerik.WinControls.UI.RadButton()
        Me.RadLabel5 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel4 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.cboMoneda = New Telerik.WinControls.UI.RadDropDownList()
        Me.txtUnidadesAfectadas = New Telerik.WinControls.UI.RadTextBox()
        Me.txtImportePactado = New Telerik.WinControls.UI.RadTextBox()
        Me.dtpInicio = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.dtpFin = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.rgvDetalle = New Telerik.WinControls.UI.RadGridView()
        Me.RadContextMenuManager1 = New Telerik.WinControls.UI.RadContextMenuManager()
        Me.rcmContextMenu = New Telerik.WinControls.UI.RadContextMenu(Me.components)
        Me.RadMenuHeaderItem1 = New Telerik.WinControls.UI.RadMenuHeaderItem()
        Me.rmiEditar = New Telerik.WinControls.UI.RadMenuItem()
        Me.rmiEliminar = New Telerik.WinControls.UI.RadMenuItem()
        Me.btnNuevo = New Telerik.WinControls.UI.RadButton()
        CType(Me.RadPageView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPageView1.SuspendLayout()
        Me.RadPageViewPage1.SuspendLayout()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboMoneda, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtUnidadesAfectadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtImportePactado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtpInicio, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtpFin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgvDetalle.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadPageView1
        '
        Me.RadPageView1.Controls.Add(Me.RadPageViewPage1)
        Me.RadPageView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadPageView1.Location = New System.Drawing.Point(0, 0)
        Me.RadPageView1.Name = "RadPageView1"
        Me.RadPageView1.SelectedPage = Me.RadPageViewPage1
        Me.RadPageView1.Size = New System.Drawing.Size(973, 386)
        Me.RadPageView1.TabIndex = 0
        Me.RadPageView1.Text = "RadPageView1"
        CType(Me.RadPageView1.GetChildAt(0), Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None
        '
        'RadPageViewPage1
        '
        Me.RadPageViewPage1.Controls.Add(Me.btnNuevo)
        Me.RadPageViewPage1.Controls.Add(Me.btnGuardar)
        Me.RadPageViewPage1.Controls.Add(Me.RadLabel5)
        Me.RadPageViewPage1.Controls.Add(Me.RadLabel4)
        Me.RadPageViewPage1.Controls.Add(Me.RadLabel3)
        Me.RadPageViewPage1.Controls.Add(Me.RadLabel2)
        Me.RadPageViewPage1.Controls.Add(Me.RadLabel1)
        Me.RadPageViewPage1.Controls.Add(Me.cboMoneda)
        Me.RadPageViewPage1.Controls.Add(Me.txtUnidadesAfectadas)
        Me.RadPageViewPage1.Controls.Add(Me.txtImportePactado)
        Me.RadPageViewPage1.Controls.Add(Me.dtpInicio)
        Me.RadPageViewPage1.Controls.Add(Me.dtpFin)
        Me.RadPageViewPage1.Controls.Add(Me.rgvDetalle)
        Me.RadPageViewPage1.Location = New System.Drawing.Point(10, 37)
        Me.RadPageViewPage1.Name = "RadPageViewPage1"
        Me.RadPageViewPage1.Size = New System.Drawing.Size(952, 338)
        Me.RadPageViewPage1.Text = "Detalle"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(839, 47)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(110, 24)
        Me.btnGuardar.TabIndex = 11
        Me.btnGuardar.Text = "Guardar"
        '
        'RadLabel5
        '
        Me.RadLabel5.Location = New System.Drawing.Point(512, 27)
        Me.RadLabel5.Name = "RadLabel5"
        Me.RadLabel5.Size = New System.Drawing.Size(105, 18)
        Me.RadLabel5.TabIndex = 10
        Me.RadLabel5.Text = "Unidades Afectadas"
        '
        'RadLabel4
        '
        Me.RadLabel4.Location = New System.Drawing.Point(375, 27)
        Me.RadLabel4.Name = "RadLabel4"
        Me.RadLabel4.Size = New System.Drawing.Size(48, 18)
        Me.RadLabel4.TabIndex = 9
        Me.RadLabel4.Text = "Moneda"
        '
        'RadLabel3
        '
        Me.RadLabel3.Location = New System.Drawing.Point(260, 27)
        Me.RadLabel3.Name = "RadLabel3"
        Me.RadLabel3.Size = New System.Drawing.Size(89, 18)
        Me.RadLabel3.TabIndex = 8
        Me.RadLabel3.Text = "Importe Pactado"
        '
        'RadLabel2
        '
        Me.RadLabel2.Location = New System.Drawing.Point(134, 27)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(53, 18)
        Me.RadLabel2.TabIndex = 7
        Me.RadLabel2.Text = "Fecha Fin"
        '
        'RadLabel1
        '
        Me.RadLabel1.Location = New System.Drawing.Point(18, 27)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(65, 18)
        Me.RadLabel1.TabIndex = 6
        Me.RadLabel1.Text = "Fecha Inicio"
        '
        'cboMoneda
        '
        Me.cboMoneda.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboMoneda.Enabled = False
        RadListDataItem1.Text = "Nuevos Soles"
        RadListDataItem1.TextWrap = True
        RadListDataItem2.Text = "Dolares"
        RadListDataItem2.TextWrap = True
        Me.cboMoneda.Items.Add(RadListDataItem1)
        Me.cboMoneda.Items.Add(RadListDataItem2)
        Me.cboMoneda.Location = New System.Drawing.Point(375, 51)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(125, 20)
        Me.cboMoneda.TabIndex = 5
        '
        'txtUnidadesAfectadas
        '
        Me.txtUnidadesAfectadas.Enabled = False
        Me.txtUnidadesAfectadas.Location = New System.Drawing.Point(512, 51)
        Me.txtUnidadesAfectadas.Name = "txtUnidadesAfectadas"
        Me.txtUnidadesAfectadas.Size = New System.Drawing.Size(159, 20)
        Me.txtUnidadesAfectadas.TabIndex = 4
        '
        'txtImportePactado
        '
        Me.txtImportePactado.Enabled = False
        Me.txtImportePactado.Location = New System.Drawing.Point(260, 51)
        Me.txtImportePactado.Name = "txtImportePactado"
        Me.txtImportePactado.Size = New System.Drawing.Size(100, 20)
        Me.txtImportePactado.TabIndex = 3
        '
        'dtpInicio
        '
        Me.dtpInicio.Enabled = False
        Me.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInicio.Location = New System.Drawing.Point(18, 51)
        Me.dtpInicio.Name = "dtpInicio"
        Me.dtpInicio.Size = New System.Drawing.Size(110, 20)
        Me.dtpInicio.TabIndex = 2
        Me.dtpInicio.TabStop = False
        Me.dtpInicio.Text = "17/03/2015"
        Me.dtpInicio.Value = New Date(2015, 3, 17, 15, 14, 25, 905)
        '
        'dtpFin
        '
        Me.dtpFin.Enabled = False
        Me.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFin.Location = New System.Drawing.Point(134, 51)
        Me.dtpFin.Name = "dtpFin"
        Me.dtpFin.Size = New System.Drawing.Size(110, 20)
        Me.dtpFin.TabIndex = 1
        Me.dtpFin.TabStop = False
        Me.dtpFin.Text = "17/03/2015"
        Me.dtpFin.Value = New Date(2015, 3, 17, 15, 14, 20, 814)
        '
        'rgvDetalle
        '
        Me.rgvDetalle.BackColor = System.Drawing.Color.FromArgb(CType(CType(233, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.rgvDetalle.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgvDetalle.EnableHotTracking = False
        Me.rgvDetalle.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.rgvDetalle.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rgvDetalle.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgvDetalle.Location = New System.Drawing.Point(2, 90)
        '
        'rgvDetalle
        '
        Me.rgvDetalle.MasterTemplate.AllowAddNewRow = False
        Me.rgvDetalle.MasterTemplate.AllowColumnReorder = False
        GridViewTextBoxColumn1.FieldName = "idCabecera"
        GridViewTextBoxColumn1.HeaderText = "column1"
        GridViewTextBoxColumn1.IsVisible = False
        GridViewTextBoxColumn1.Name = "idCabecera"
        GridViewTextBoxColumn2.FieldName = "idDetalle"
        GridViewTextBoxColumn2.HeaderText = "column2"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "idDetalle"
        GridViewTextBoxColumn3.FieldName = "FechaInicio"
        GridViewTextBoxColumn3.HeaderText = "Fecha Inicio"
        GridViewTextBoxColumn3.Name = "FechaInicio"
        GridViewTextBoxColumn3.Width = 90
        GridViewTextBoxColumn4.FieldName = "FechaVencimiento"
        GridViewTextBoxColumn4.HeaderText = "Fecha Fin"
        GridViewTextBoxColumn4.Name = "FechaVencimiento"
        GridViewTextBoxColumn4.Width = 90
        GridViewTextBoxColumn5.FieldName = "importepactado"
        GridViewTextBoxColumn5.HeaderText = "Importe Pactado"
        GridViewTextBoxColumn5.Name = "importepactado"
        GridViewTextBoxColumn5.Width = 130
        GridViewTextBoxColumn6.FieldName = "Moneda"
        GridViewTextBoxColumn6.HeaderText = "Moneda"
        GridViewTextBoxColumn6.Name = "Moneda"
        GridViewTextBoxColumn6.Width = 120
        GridViewTextBoxColumn7.FieldName = "unidadesafectadas"
        GridViewTextBoxColumn7.HeaderText = "Unidades Afectadas"
        GridViewTextBoxColumn7.Name = "unidadesafectadas"
        GridViewTextBoxColumn7.Width = 320
        Me.rgvDetalle.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7})
        Me.rgvDetalle.Name = "rgvDetalle"
        Me.RadContextMenuManager1.SetRadContextMenu(Me.rgvDetalle, Me.rcmContextMenu)
        Me.rgvDetalle.ReadOnly = True
        Me.rgvDetalle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgvDetalle.ShowGroupPanel = False
        Me.rgvDetalle.Size = New System.Drawing.Size(947, 245)
        Me.rgvDetalle.TabIndex = 0
        Me.rgvDetalle.Text = "RadGridView1"
        '
        'rcmContextMenu
        '
        Me.rcmContextMenu.Items.AddRange(New Telerik.WinControls.RadItem() {Me.RadMenuHeaderItem1, Me.rmiEditar, Me.rmiEliminar})
        '
        'RadMenuHeaderItem1
        '
        Me.RadMenuHeaderItem1.AccessibleDescription = "Opciones"
        Me.RadMenuHeaderItem1.AccessibleName = "Opciones"
        Me.RadMenuHeaderItem1.Name = "RadMenuHeaderItem1"
        Me.RadMenuHeaderItem1.Text = "Opciones"
        Me.RadMenuHeaderItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'rmiEditar
        '
        Me.rmiEditar.AccessibleDescription = "Editar"
        Me.rmiEditar.AccessibleName = "Editar"
        Me.rmiEditar.Name = "rmiEditar"
        Me.rmiEditar.Text = "Editar"
        Me.rmiEditar.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'rmiEliminar
        '
        Me.rmiEliminar.AccessibleDescription = "Eliminar"
        Me.rmiEliminar.AccessibleName = "Eliminar"
        Me.rmiEliminar.Name = "rmiEliminar"
        Me.rmiEliminar.Text = "Eliminar"
        Me.rmiEliminar.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'btnNuevo
        '
        Me.btnNuevo.Location = New System.Drawing.Point(723, 47)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(110, 24)
        Me.btnNuevo.TabIndex = 12
        Me.btnNuevo.Text = "Nuevo"
        '
        'WFGestionDocumentoDetalle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(973, 386)
        Me.Controls.Add(Me.RadPageView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "WFGestionDocumentoDetalle"
        CType(Me.RadPageView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPageView1.ResumeLayout(False)
        Me.RadPageViewPage1.ResumeLayout(False)
        Me.RadPageViewPage1.PerformLayout()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboMoneda, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtUnidadesAfectadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtImportePactado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtpInicio, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtpFin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgvDetalle.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadPageView1 As Telerik.WinControls.UI.RadPageView
    Friend WithEvents RadPageViewPage1 As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents RadLabel5 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel4 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboMoneda As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents txtUnidadesAfectadas As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtImportePactado As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents dtpInicio As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents dtpFin As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents rgvDetalle As Telerik.WinControls.UI.RadGridView
    Friend WithEvents btnGuardar As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadContextMenuManager1 As Telerik.WinControls.UI.RadContextMenuManager
    Friend WithEvents rcmContextMenu As Telerik.WinControls.UI.RadContextMenu
    Friend WithEvents RadMenuHeaderItem1 As Telerik.WinControls.UI.RadMenuHeaderItem
    Friend WithEvents rmiEditar As Telerik.WinControls.UI.RadMenuItem
    Friend WithEvents rmiEliminar As Telerik.WinControls.UI.RadMenuItem
    Friend WithEvents btnNuevo As Telerik.WinControls.UI.RadButton
End Class
