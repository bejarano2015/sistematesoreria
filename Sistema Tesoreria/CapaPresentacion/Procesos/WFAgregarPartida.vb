﻿Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad.Mantenimiento
Imports CapaEntidad.Proceso
Imports CapaEntidad
Imports BusinessLogicLayer.Procesos

Public Class WFAgregarPartida
    Friend frm As New frmPartidas
    Public Sub setfila(ByVal fila As DataRow)
        Me.fila = fila
    End Sub

#Region "Instanciar"
    Private Shared frmInstance As WFAgregarPartida = Nothing
    Public Shared Function Instance() As WFAgregarPartida
        If frmInstance Is Nothing Then
            frmInstance = New WFAgregarPartida
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
#End Region

#Region "Decaraciones "

    Private fila As DataRow
   

    Dim VL_SrvPartida As SrvPartida()
    Dim VL_BeanPartida As BeanPartida()

    Dim VL_BeanConsolidado As BeanConsolidado()

    'CONSOLIDADO 
    Dim vl_opcion As String
    Dim vl_EmprCodigo As String
    Dim vl_CCosCodigo As String
    Dim vl_UbtCodigo As String
    Dim vl_EspCodigo As String
    Dim vl_TitCodigo As String
    Dim vl_ParCodigo As String
    Dim vl_ObraCodigo As String
    Dim vl_ConsCodigo As String
    Dim vl_UsuCreacion As String
    Dim vl_UMedida As String
    Dim vl_Metrado As Double
    Dim vl_PrecioPresupuesto As Double
    Dim vl_PrecioSubcontrato As Double
    Dim vl_MonCodigo As String
    Dim vl_PtiCodigo As String
    Dim vl_ItemPresupuesto As String
    Dim vl_SpptoCodigo As String
    Dim vl_TipoPartida As String
    Dim vl_N4Codigo As String

    Dim vl_IdPartida As String
    Dim vl_periodo As String


#End Region

#Region "procedimientos"


    Private Sub ListarUnidadMedida()

        Dim VL_SrvUnidadMedida As New SrvUnidadMedida()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        Try
            VL_BeanResultado = VL_SrvUnidadMedida.Fnc_listar_UnidadMedida()

            If VL_BeanResultado.blnExiste = True Then
                cboUnidadMedida.DataSource = VL_BeanResultado.dtResultado
                cboUnidadMedida.DisplayMember = "UmdDescripcion"
                cboUnidadMedida.ValueMember = "UmdCodigo"

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

    Private Sub ListarPartidas()
        Dim VL_SrvPartida As New SrvPartida()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim VL_BeanPartida = New BeanPartida("")

        Try
            VL_BeanResultado = VL_SrvPartida.Fnc_listar_partida(VL_BeanPartida)

            If VL_BeanResultado.blnExiste = True Then
                cboPartidas.DataSource = VL_BeanResultado.dtResultado
                cboPartidas.DisplayMember = "PARDESCRIPCION"
                cboPartidas.ValueMember = "PARCODIGO"

            Else


                MessageBox.Show(VL_BeanResultado.strMensaje)

            End If
        Catch ex As Exception

            MessageBox.Show(ex.ToString())
        End Try


    End Sub

 

#End Region
#Region "procesos"
    Private Sub AgregarPartida()
        'Dim rowactual As Integer
        Try


            Dim VL_SrvConsolidado As New SrvConsolidado()
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


            vl_opcion = "1"
            vl_EmprCodigo = gEmpresa
            vl_CCosCodigo = fila("CCosCodigo").ToString()
            vl_UbtCodigo = fila("UbtCodigo").ToString()
            vl_EspCodigo = "00000"
            vl_TitCodigo = "00000"
            vl_ParCodigo = cboPartidas.SelectedValue.ToString()
            vl_ObraCodigo = fila("ObraCodigo").ToString()
            vl_ConsCodigo = "" 'se genera automaticamente en el store
            vl_UsuCreacion = gUsuario
            vl_UMedida = cboUnidadMedida.SelectedValue.ToString()
            vl_Metrado = IIf(cboTipoPartida.Text = "Adenda", txtMetrado.Text, "-" + txtMetrado.Text)
            vl_PrecioPresupuesto = txtPrecio.Text.ToString()
            vl_PrecioSubcontrato = txtPrecio.Text
            vl_MonCodigo = fila("Idmoneda").ToString()
            vl_PtiCodigo = IIf(cboTipoPartida.Text = "Adenda", "00165", "00166")
            vl_ItemPresupuesto = "" ''recalcular el item
            vl_SpptoCodigo = "00000"
            vl_TipoPartida = "E"

            vl_N4Codigo = "00000"

            Dim VL_BeanConsolidado = New BeanConsolidado(vl_opcion, vl_EmprCodigo, vl_CCosCodigo, vl_UbtCodigo, vl_EspCodigo, vl_TitCodigo, vl_ParCodigo, vl_ObraCodigo, vl_ConsCodigo, vl_UsuCreacion, vl_UMedida, vl_Metrado, vl_PrecioPresupuesto, vl_PrecioSubcontrato, vl_MonCodigo, vl_PtiCodigo, vl_ItemPresupuesto, vl_SpptoCodigo, vl_TipoPartida, vl_N4Codigo)

            VL_BeanResultado = VL_SrvConsolidado.Fnc_Registrar_Consolidado(VL_BeanConsolidado)
            If VL_BeanResultado.blnResultado = True Then
                dgvPresupuesto.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If




        Catch ex As Exception

        End Try

    End Sub

    Private Function AgregarPartidaContrato() As Boolean
        'Dim rowactual As Integer
        Try


            Dim VL_SrvConsolidado As New SrvConsolidado()
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


            vl_EmprCodigo = gEmpresa
            vl_CCosCodigo = fila("CCosCodigo").ToString()
            vl_UbtCodigo = fila("UbtCodigo").ToString()
            vl_IdPartida = fila("IdPartida").ToString()
            vl_periodo = gPeriodo

            Dim VL_BeanConsolidado = New BeanConsolidado(vl_EmprCodigo, vl_CCosCodigo, vl_UbtCodigo, vl_IdPartida, vl_periodo, gUsuario)

            VL_BeanResultado = VL_SrvConsolidado.Fnc_Registrar_PartidoContrato(VL_BeanConsolidado)
            If VL_BeanResultado.blnResultado = True Then
                dgvPresupuesto.DataSource = VL_BeanResultado.dtResultado
                Return True

            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return False
            End If




        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Sub limpiarItemsGuardados()
        Try


            Dim VL_SrvConsolidado As New SrvConsolidado()
            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


            vl_EmprCodigo = gEmpresa
            vl_CCosCodigo = fila("CCosCodigo").ToString()
            vl_UbtCodigo = fila("UbtCodigo").ToString()
            vl_IdPartida = fila("IdPartida").ToString()
            vl_periodo = gPeriodo

            Dim VL_BeanConsolidado = New BeanConsolidado(vl_EmprCodigo, vl_CCosCodigo, vl_UbtCodigo, gUsuario)

            VL_BeanResultado = VL_SrvConsolidado.Fnc_Limpiar_Consolidado(VL_BeanConsolidado)
            If VL_BeanResultado.blnResultado = True Then
                dgvPresupuesto.DataSource = VL_BeanResultado.dtResultado
            Else
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreia", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If




        Catch ex As Exception

        End Try
    End Sub

#End Region


    Private Sub btnAgregarPartida_Click(sender As Object, e As EventArgs) Handles btnAgregarPartida.Click
        If ValidarIngreso() Then
            AgregarPartida()
        Else
            MessageBox.Show("Coloque Metrado y precio Mayores a Cero", "Sistema Tesoreria", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
    Function ValidarIngreso() As Boolean
        If (Convert.ToDouble(txtMetrado.Text) * Convert.ToDouble(txtPrecio.Text)) > 0 Then
            Return True
        End If
        Return False
    End Function
    Private Sub WFAgregarPartida_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListarPartidas()
        ListarUnidadMedida()
        cboTipoPartida.SelectedIndex = 0
        limpiarItemsGuardados()
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click

        If (AgregarPartidaContrato()) Then

            Me.Close()
            frm.TieneFrmDetalle = False
            frmInstance = Nothing

        End If

    End Sub

    Private Sub WFAgregarPartida_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
        frm.TieneFrmDetalle = False
        frmInstance = Nothing
    End Sub

    Private Sub txtMetrado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMetrado.KeyPress
        Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros2(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        ElseIf (txtMetrado.Text.Contains(".")) And (KeyAscii = 46) Then
            e.Handled = True
        ElseIf (txtMetrado.Text.Contains("-")) And (KeyAscii = 45) Then
            e.Handled = True
        ElseIf KeyAscii = 13 Then
            txtMetrado.Focus()
        End If
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
       Dim KeyAscii As Short = CShort(Asc(e.KeyChar))
        KeyAscii = CShort(SoloNumeros2(KeyAscii))
        If KeyAscii = 0 Then
            e.Handled = True
        ElseIf (txtPrecio.Text.Contains(".")) And (KeyAscii = 46) Then

            e.Handled = True
        ElseIf (txtPrecio.Text.Contains("-")) And (KeyAscii = 45) Then
            e.Handled = True
        ElseIf KeyAscii = 13 Then
            txtPrecio.Focus()
        End If
    End Sub

    Private Sub txtMetrado_Leave(sender As Object, e As EventArgs) Handles txtMetrado.Leave
        txtParcial.Text = Convert.ToString(Convert.ToDouble(txtMetrado.Text) * Convert.ToDouble(txtPrecio.Text))
    End Sub

    Private Sub txtPrecio_Leave(sender As Object, e As EventArgs) Handles txtPrecio.Leave
        txtParcial.Text = Convert.ToString(Convert.ToDouble(txtMetrado.Text) * Convert.ToDouble(txtPrecio.Text))
    End Sub


End Class