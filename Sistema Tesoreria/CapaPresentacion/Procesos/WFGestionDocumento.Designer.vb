﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WFGestionDocumento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.RadPageView1 = New Telerik.WinControls.UI.RadPageView()
        Me.RadPageViewPage1 = New Telerik.WinControls.UI.RadPageViewPage()
        Me.rgvLista = New Telerik.WinControls.UI.RadGridView()
        Me.RadPageViewPage2 = New Telerik.WinControls.UI.RadPageViewPage()
        Me.RadLabel6 = New Telerik.WinControls.UI.RadLabel()
        Me.rgvBancoEmpresa = New Telerik.WinControls.UI.RadGridView()
        Me.txtBancoEmpresa = New Telerik.WinControls.UI.RadTextBox()
        Me.btnCancelar = New Telerik.WinControls.UI.RadButton()
        Me.btnGuardar = New Telerik.WinControls.UI.RadButton()
        Me.RadLabel5 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel4 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.cboEmpresa = New Telerik.WinControls.UI.RadDropDownList()
        Me.cboDocumentoConcepto = New Telerik.WinControls.UI.RadDropDownList()
        Me.txtGlosa = New Telerik.WinControls.UI.RadTextBox()
        Me.txtDiasRenovacion = New Telerik.WinControls.UI.RadTextBox()
        Me.txtNroDocumento = New Telerik.WinControls.UI.RadTextBox()
        Me.RadContextMenuManager1 = New Telerik.WinControls.UI.RadContextMenuManager()
        Me.RadContextMenu1 = New Telerik.WinControls.UI.RadContextMenu(Me.components)
        Me.RadMenuHeaderItem1 = New Telerik.WinControls.UI.RadMenuHeaderItem()
        Me.rmiDetalle = New Telerik.WinControls.UI.RadMenuItem()
        CType(Me.RadPageView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPageView1.SuspendLayout()
        Me.RadPageViewPage1.SuspendLayout()
        CType(Me.rgvLista, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgvLista.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadPageViewPage2.SuspendLayout()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgvBancoEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgvBancoEmpresa.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtBancoEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboDocumentoConcepto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGlosa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDiasRenovacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNroDocumento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RadPageView1
        '
        Me.RadPageView1.Controls.Add(Me.RadPageViewPage1)
        Me.RadPageView1.Controls.Add(Me.RadPageViewPage2)
        Me.RadPageView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RadPageView1.Location = New System.Drawing.Point(0, 0)
        Me.RadPageView1.Name = "RadPageView1"
        Me.RadPageView1.SelectedPage = Me.RadPageViewPage1
        Me.RadPageView1.Size = New System.Drawing.Size(915, 515)
        Me.RadPageView1.TabIndex = 0
        Me.RadPageView1.Text = "RadPageView1"
        Me.RadPageView1.ThemeName = "ControlDefault"
        CType(Me.RadPageView1.GetChildAt(0), Telerik.WinControls.UI.RadPageViewStripElement).StripButtons = Telerik.WinControls.UI.StripViewButtons.None
        '
        'RadPageViewPage1
        '
        Me.RadPageViewPage1.Controls.Add(Me.rgvLista)
        Me.RadPageViewPage1.Location = New System.Drawing.Point(10, 37)
        Me.RadPageViewPage1.Name = "RadPageViewPage1"
        Me.RadPageViewPage1.Size = New System.Drawing.Size(894, 467)
        Me.RadPageViewPage1.Text = "Listado"
        '
        'rgvLista
        '
        Me.rgvLista.BackColor = System.Drawing.Color.FromArgb(CType(CType(233, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.rgvLista.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgvLista.EnableHotTracking = False
        Me.rgvLista.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.rgvLista.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rgvLista.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgvLista.Location = New System.Drawing.Point(3, 111)
        '
        'rgvLista
        '
        Me.rgvLista.MasterTemplate.AllowAddNewRow = False
        Me.rgvLista.MasterTemplate.AllowColumnReorder = False
        GridViewTextBoxColumn1.EnableExpressionEditor = False
        GridViewTextBoxColumn1.FieldName = "IDCABECERA"
        GridViewTextBoxColumn1.HeaderText = "column1"
        GridViewTextBoxColumn1.IsVisible = False
        GridViewTextBoxColumn1.Name = "IDCABECERA"
        GridViewTextBoxColumn2.EnableExpressionEditor = False
        GridViewTextBoxColumn2.FieldName = "IDDOCUMENTO"
        GridViewTextBoxColumn2.HeaderText = "column2"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "IDDOCUMENTO"
        GridViewTextBoxColumn3.EnableExpressionEditor = False
        GridViewTextBoxColumn3.FieldName = "DescripDoc"
        GridViewTextBoxColumn3.HeaderText = "Documento"
        GridViewTextBoxColumn3.Name = "DescripDoc"
        GridViewTextBoxColumn3.Width = 130
        GridViewTextBoxColumn4.EnableExpressionEditor = False
        GridViewTextBoxColumn4.FieldName = "NRODOCUMENTO"
        GridViewTextBoxColumn4.HeaderText = "Nro Documento"
        GridViewTextBoxColumn4.Name = "NRODOCUMENTO"
        GridViewTextBoxColumn4.Width = 100
        GridViewTextBoxColumn5.EnableExpressionEditor = False
        GridViewTextBoxColumn5.FieldName = "IDBANCOEMPRESA"
        GridViewTextBoxColumn5.HeaderText = "column5"
        GridViewTextBoxColumn5.IsVisible = False
        GridViewTextBoxColumn5.Name = "IDBANCOEMPRESA"
        GridViewTextBoxColumn6.EnableExpressionEditor = False
        GridViewTextBoxColumn6.FieldName = "descripcion"
        GridViewTextBoxColumn6.HeaderText = "Banco / Empresa"
        GridViewTextBoxColumn6.Name = "descripcion"
        GridViewTextBoxColumn6.Width = 200
        GridViewTextBoxColumn7.EnableExpressionEditor = False
        GridViewTextBoxColumn7.FieldName = "EMPRCODIGO"
        GridViewTextBoxColumn7.HeaderText = "column7"
        GridViewTextBoxColumn7.IsVisible = False
        GridViewTextBoxColumn7.Name = "EMPRCODIGO"
        GridViewTextBoxColumn8.EnableExpressionEditor = False
        GridViewTextBoxColumn8.FieldName = "DIASRENOVACION"
        GridViewTextBoxColumn8.HeaderText = "Dias Renov."
        GridViewTextBoxColumn8.Name = "DIASRENOVACION"
        GridViewTextBoxColumn8.Width = 80
        GridViewTextBoxColumn9.EnableExpressionEditor = False
        GridViewTextBoxColumn9.FieldName = "GLOSA"
        GridViewTextBoxColumn9.HeaderText = "Glosa"
        GridViewTextBoxColumn9.Name = "GLOSA"
        GridViewTextBoxColumn9.Width = 350
        Me.rgvLista.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8, GridViewTextBoxColumn9})
        Me.rgvLista.Name = "rgvLista"
        Me.RadContextMenuManager1.SetRadContextMenu(Me.rgvLista, Me.RadContextMenu1)
        Me.rgvLista.ReadOnly = True
        Me.rgvLista.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgvLista.ShowGroupPanel = False
        Me.rgvLista.Size = New System.Drawing.Size(888, 353)
        Me.rgvLista.TabIndex = 0
        Me.rgvLista.Text = "rgvLista"
        '
        'RadPageViewPage2
        '
        Me.RadPageViewPage2.Controls.Add(Me.RadLabel6)
        Me.RadPageViewPage2.Controls.Add(Me.rgvBancoEmpresa)
        Me.RadPageViewPage2.Controls.Add(Me.txtBancoEmpresa)
        Me.RadPageViewPage2.Controls.Add(Me.btnCancelar)
        Me.RadPageViewPage2.Controls.Add(Me.btnGuardar)
        Me.RadPageViewPage2.Controls.Add(Me.RadLabel5)
        Me.RadPageViewPage2.Controls.Add(Me.RadLabel4)
        Me.RadPageViewPage2.Controls.Add(Me.RadLabel3)
        Me.RadPageViewPage2.Controls.Add(Me.RadLabel2)
        Me.RadPageViewPage2.Controls.Add(Me.RadLabel1)
        Me.RadPageViewPage2.Controls.Add(Me.cboEmpresa)
        Me.RadPageViewPage2.Controls.Add(Me.cboDocumentoConcepto)
        Me.RadPageViewPage2.Controls.Add(Me.txtGlosa)
        Me.RadPageViewPage2.Controls.Add(Me.txtDiasRenovacion)
        Me.RadPageViewPage2.Controls.Add(Me.txtNroDocumento)
        Me.RadPageViewPage2.Location = New System.Drawing.Point(10, 37)
        Me.RadPageViewPage2.Name = "RadPageViewPage2"
        Me.RadPageViewPage2.Size = New System.Drawing.Size(894, 467)
        Me.RadPageViewPage2.Text = "Mantener"
        '
        'RadLabel6
        '
        Me.RadLabel6.Location = New System.Drawing.Point(119, 158)
        Me.RadLabel6.Name = "RadLabel6"
        Me.RadLabel6.Size = New System.Drawing.Size(125, 18)
        Me.RadLabel6.TabIndex = 14
        Me.RadLabel6.Text = "Número de Documento"
        '
        'rgvBancoEmpresa
        '
        Me.rgvBancoEmpresa.BackColor = System.Drawing.Color.FromArgb(CType(CType(233, Byte), Integer), CType(CType(240, Byte), Integer), CType(CType(249, Byte), Integer))
        Me.rgvBancoEmpresa.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgvBancoEmpresa.Font = New System.Drawing.Font("Segoe UI", 8.25!)
        Me.rgvBancoEmpresa.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rgvBancoEmpresa.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgvBancoEmpresa.Location = New System.Drawing.Point(250, 126)
        '
        '
        '
        Me.rgvBancoEmpresa.MasterTemplate.AllowAddNewRow = False
        Me.rgvBancoEmpresa.MasterTemplate.AllowColumnReorder = False
        GridViewTextBoxColumn10.EnableExpressionEditor = False
        GridViewTextBoxColumn10.FieldName = "CODIGO"
        GridViewTextBoxColumn10.HeaderText = "Codigo"
        GridViewTextBoxColumn10.Name = "CODIGO"
        GridViewTextBoxColumn11.EnableExpressionEditor = False
        GridViewTextBoxColumn11.FieldName = "DESCRIPCION"
        GridViewTextBoxColumn11.HeaderText = "Descripcion"
        GridViewTextBoxColumn11.Name = "DESCRIPCION"
        GridViewTextBoxColumn11.Width = 450
        Me.rgvBancoEmpresa.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn10, GridViewTextBoxColumn11})
        Me.rgvBancoEmpresa.Name = "rgvBancoEmpresa"
        Me.rgvBancoEmpresa.ReadOnly = True
        Me.rgvBancoEmpresa.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgvBancoEmpresa.ShowGroupPanel = False
        Me.rgvBancoEmpresa.Size = New System.Drawing.Size(602, 150)
        Me.rgvBancoEmpresa.TabIndex = 12
        Me.rgvBancoEmpresa.Text = "RadGridView1"
        Me.rgvBancoEmpresa.Visible = False
        '
        'txtBancoEmpresa
        '
        Me.txtBancoEmpresa.Location = New System.Drawing.Point(250, 106)
        Me.txtBancoEmpresa.Name = "txtBancoEmpresa"
        Me.txtBancoEmpresa.Size = New System.Drawing.Size(461, 20)
        Me.txtBancoEmpresa.TabIndex = 11
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(403, 282)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(110, 24)
        Me.btnCancelar.TabIndex = 10
        Me.btnCancelar.Text = "Cancelar"
        '
        'btnGuardar
        '
        Me.btnGuardar.Location = New System.Drawing.Point(250, 282)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(110, 24)
        Me.btnGuardar.TabIndex = 9
        Me.btnGuardar.Text = "Guardar"
        '
        'RadLabel5
        '
        Me.RadLabel5.Location = New System.Drawing.Point(210, 210)
        Me.RadLabel5.Name = "RadLabel5"
        Me.RadLabel5.Size = New System.Drawing.Size(34, 18)
        Me.RadLabel5.TabIndex = 8
        Me.RadLabel5.Text = "Glosa"
        '
        'RadLabel4
        '
        Me.RadLabel4.Location = New System.Drawing.Point(155, 184)
        Me.RadLabel4.Name = "RadLabel4"
        Me.RadLabel4.Size = New System.Drawing.Size(89, 18)
        Me.RadLabel4.TabIndex = 7
        Me.RadLabel4.Text = "Dias Renovación"
        '
        'RadLabel3
        '
        Me.RadLabel3.Location = New System.Drawing.Point(195, 132)
        Me.RadLabel3.Name = "RadLabel3"
        Me.RadLabel3.Size = New System.Drawing.Size(49, 18)
        Me.RadLabel3.TabIndex = 6
        Me.RadLabel3.Text = "Empresa"
        '
        'RadLabel2
        '
        Me.RadLabel2.Location = New System.Drawing.Point(154, 106)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(90, 18)
        Me.RadLabel2.TabIndex = 5
        Me.RadLabel2.Text = "Banco / Empresa"
        '
        'RadLabel1
        '
        Me.RadLabel1.Location = New System.Drawing.Point(95, 80)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(149, 18)
        Me.RadLabel1.TabIndex = 4
        Me.RadLabel1.Text = "Tipo Documento / Concepto"
        '
        'cboEmpresa
        '
        Me.cboEmpresa.Location = New System.Drawing.Point(250, 132)
        Me.cboEmpresa.Name = "cboEmpresa"
        Me.cboEmpresa.Size = New System.Drawing.Size(263, 20)
        Me.cboEmpresa.TabIndex = 3
        '
        'cboDocumentoConcepto
        '
        Me.cboDocumentoConcepto.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboDocumentoConcepto.Location = New System.Drawing.Point(250, 80)
        Me.cboDocumentoConcepto.Name = "cboDocumentoConcepto"
        Me.cboDocumentoConcepto.Size = New System.Drawing.Size(263, 20)
        Me.cboDocumentoConcepto.TabIndex = 1
        '
        'txtGlosa
        '
        Me.txtGlosa.Location = New System.Drawing.Point(250, 210)
        Me.txtGlosa.Name = "txtGlosa"
        Me.txtGlosa.Size = New System.Drawing.Size(461, 20)
        Me.txtGlosa.TabIndex = 1
        '
        'txtDiasRenovacion
        '
        Me.txtDiasRenovacion.Location = New System.Drawing.Point(250, 184)
        Me.txtDiasRenovacion.Name = "txtDiasRenovacion"
        Me.txtDiasRenovacion.Size = New System.Drawing.Size(38, 20)
        Me.txtDiasRenovacion.TabIndex = 0
        '
        'txtNroDocumento
        '
        Me.txtNroDocumento.Location = New System.Drawing.Point(250, 158)
        Me.txtNroDocumento.Name = "txtNroDocumento"
        Me.txtNroDocumento.Size = New System.Drawing.Size(91, 20)
        Me.txtNroDocumento.TabIndex = 13
        '
        'RadContextMenu1
        '
        Me.RadContextMenu1.Items.AddRange(New Telerik.WinControls.RadItem() {Me.RadMenuHeaderItem1, Me.rmiDetalle})
        '
        'RadMenuHeaderItem1
        '
        Me.RadMenuHeaderItem1.AccessibleDescription = "Opciones"
        Me.RadMenuHeaderItem1.AccessibleName = "Opciones"
        Me.RadMenuHeaderItem1.Name = "RadMenuHeaderItem1"
        Me.RadMenuHeaderItem1.Text = "Opciones"
        Me.RadMenuHeaderItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'rmiDetalle
        '
        Me.rmiDetalle.AccessibleDescription = "Añadir Detalle"
        Me.rmiDetalle.AccessibleName = "Añadir Detalle"
        Me.rmiDetalle.Name = "rmiDetalle"
        Me.rmiDetalle.Text = "Añadir Detalle"
        Me.rmiDetalle.Visibility = Telerik.WinControls.ElementVisibility.Visible
        '
        'WFGestionDocumento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(915, 515)
        Me.Controls.Add(Me.RadPageView1)
        Me.Name = "WFGestionDocumento"
        Me.Text = "Gestion Documentaria"
        CType(Me.RadPageView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPageView1.ResumeLayout(False)
        Me.RadPageViewPage1.ResumeLayout(False)
        CType(Me.rgvLista.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgvLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadPageViewPage2.ResumeLayout(False)
        Me.RadPageViewPage2.PerformLayout()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgvBancoEmpresa.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgvBancoEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtBancoEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboEmpresa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboDocumentoConcepto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGlosa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDiasRenovacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNroDocumento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RadPageView1 As Telerik.WinControls.UI.RadPageView
    Friend WithEvents RadPageViewPage1 As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents RadPageViewPage2 As Telerik.WinControls.UI.RadPageViewPage
    Friend WithEvents RadLabel5 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel4 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboEmpresa As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents cboDocumentoConcepto As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents txtGlosa As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtDiasRenovacion As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents btnCancelar As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnGuardar As Telerik.WinControls.UI.RadButton
    Friend WithEvents rgvBancoEmpresa As Telerik.WinControls.UI.RadGridView
    Friend WithEvents txtBancoEmpresa As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel6 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtNroDocumento As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents rgvLista As Telerik.WinControls.UI.RadGridView
    Friend WithEvents RadContextMenuManager1 As Telerik.WinControls.UI.RadContextMenuManager
    Friend WithEvents RadContextMenu1 As Telerik.WinControls.UI.RadContextMenu
    Friend WithEvents RadMenuHeaderItem1 As Telerik.WinControls.UI.RadMenuHeaderItem
    Friend WithEvents rmiDetalle As Telerik.WinControls.UI.RadMenuItem
End Class
