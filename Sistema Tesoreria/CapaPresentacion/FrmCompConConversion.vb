Imports System.Data
'Imports CrystalDecisions.CrystalReports
'Imports CapaNegocios
'Imports IWshRuntimeLibrary
'Imports Myhelp
Public Class FrmCompConConversion
    '    Private cCapaReCompConConv As clsRegComprobConConv
    '    Private ObjAyuda As ClsAyuda
    '    Private ObjAyudaQuery As ClsAyudaQuery
    '    Private cFuncGen As CapaNegocios.ClsFuncionesGenerales = New CapaNegocios.ClsFuncionesGenerales

    '    Dim DsetDatos As New DataSet
    '    Dim StrMascara As String = String.Empty
    '    Dim DTtcXfecha As DataTable
    '    Dim NomPc As New WshNetwork
    '    Dim DTMonedas As New DataTable
    '    Dim STRmonSist As String
    '    Dim BolEditReg As Boolean = False
    '    Dim DTCambioActual As New DataTable
    '    Dim BolIngAuto As Boolean = False
    '    Dim DTnivelesCta As New DataTable

    '    Dim DblTCsegunFec As Double
    '    Dim IntColumIndex As Integer
    '    Dim TblParamHonorarios As DataTable
    '    Dim CodCuenta As String
    '    Dim BolRetencion As Boolean
    '    Dim Str_CoV As String
    '    Dim StrCodTipoAnex As String
    '    Dim StrAbrevMonCab As String

    '    Dim CodSecuencia_Edit As String = String.Empty
    '    Dim D_o_H As String

    '    Dim MontoOrigenDoc As Double = 0
    '    Dim MonedaOrigenDoc As String = String.Empty
    '    Dim StrTipoAnexo As String

    '    Private Sub FrmCompConConversion_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '        If Cargar_Datos() = False Then
    '            MessageBox.Show("Faltan algunos parametros que debe asignar para poder realizar la Operacion de Registro de Ventas", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Me.Close()
    '        End If

    '        Call Bloquer_Controles()
    '        Call Limpiar_Controls_alCargar()
    '        lblDebe.Text = Format(0, "#,##0.000")
    '        lblHaber.Text = Format(0, "#,##0.000")
    '        txtNumComp.Text = String.Empty
    '        dtpFechaTCambio.Visible = False
    '        gbFechaTCambio.Visible = False

    '    End Sub

    '    Private Function Cargar_Datos() As Boolean
    '        Dim TblDocumentos As New DataTable
    '        TblParamHonorarios = New DataTable
    '        cCapaReCompConConv = New clsRegComprobConConv

    '        Try
    '            DsetDatos.Clear()
    '            DsetDatos = cCapaReCompConConv.CargarDatosForm(sEmpresa, sPeriodo)

    '            DTMonedas = DsetDatos.Tables("Monedas")
    '            STRmonSist = MonedaSistema(DTMonedas)

    '            If STRmonSist = String.Empty Then Return False
    '            If DTMonedas.Rows.Count <= 0 Then
    '                MessageBox.Show("No existen Monedas para iniciar la operacion", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                Return False
    '            End If

    '            With DsetDatos
    '                'Combo SubDiario
    '                cboSubdiario.DataSource = .Tables("Subdiarios")
    '                cboSubdiario.DisplayMember = "Descripcion"
    '                cboSubdiario.ValueMember = "Codigo"
    '                cboSubdiario.SelectedIndex = -1

    '                'Combo Monedas
    '                cboMoneda.DataSource = .Tables("MonedasCab")
    '                cboMoneda.DisplayMember = "Descripcion"
    '                cboMoneda.ValueMember = "Codigo"
    '                cboMoneda.SelectedIndex = -1

    '                'Combo Conversion T. Cambio
    '                cboTipoCambio.DataSource = .Tables("ConversionTC")
    '                cboTipoCambio.DisplayMember = "Descripcion"
    '                cboTipoCambio.ValueMember = "ID"
    '                cboTipoCambio.SelectedIndex = -1

    '                'Combo Monedas de Asiento
    '                cboMonedaAsiento.DataSource = .Tables("MonedasDet")
    '                cboMonedaAsiento.DisplayMember = "Descripcion"
    '                cboMonedaAsiento.ValueMember = "Codigo"
    '                cboMonedaAsiento.SelectedIndex = -1

    '                'Tipo Documentos para el Registro de Asiento
    '                cboTipoDocDet.DataSource = .Tables("TipoDocumentoDet")
    '                cboTipoDocDet.DisplayMember = "Descripcion"
    '                cboTipoDocDet.ValueMember = "Codigo"
    '                cboTipoDocDet.SelectedIndex = -1

    '                'Numeros de Decimales
    '                BytFormatConta = cCapaReCompConConv.FormatDecimal(sEmpresa)

    '                'Formateando el MaskedTextBox con los Niveles de Cuenta
    '                If .Tables("NivelesCta").Rows.Count > 0 Then
    '                    DTnivelesCta = .Tables("NivelesCta")
    '                    StrMascara = cCapaReCompConConv.Niveles_Formato(DTnivelesCta)
    '                    mtbCuenta.Mask = StrMascara
    '                Else
    '                    MessageBox.Show("No se pudo cargar el Formulario, ocurrio un Error al cargar los Niveles de Cuenta", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                    Return False
    '                End If

    '                Return True
    '            End With
    '        Catch ex As Exception
    '            Return False
    '        End Try

    '    End Function

    '    Private Function MonedaSistema(ByVal Tabla As DataTable) As String
    '        Dim MonSist As String = String.Empty
    '        For i As Integer = 0 To Tabla.Rows.Count - 1
    '            If Tabla.Rows(i).Item("MonNacional") = "1" Then
    '                MonSist = Tabla.Rows(i).Item("Abreviado")
    '                Return MonSist
    '            Else
    '                MonSist = String.Empty
    '            End If
    '        Next
    '        Return MonSist
    '    End Function

    '    Private Sub Bloquer_Controles()
    '        gbCabecera.Enabled = False
    '        btnGenerar.Enabled = False
    '        btnNuevoDet.Enabled = False
    '        btnGrabar.Enabled = False
    '        btnEditar.Enabled = False
    '        btnCancelar.Enabled = False
    '        btnEliminar.Enabled = False
    '        TabRegGeneral.Enabled = False
    '    End Sub

    '    Private Sub Limpiar_Controls_alCargar()
    '        gbCabecera.Enabled = False
    '        LimpiarCabecera()
    '        DstAsientos.Tables.Clear()
    '        TabRegGeneral.SelectedIndex = 0
    '        dgvAsiento.DataSource = Nothing
    '        lblDebe.Text = Format(0, "#,##0.000")
    '        lblHaber.Text = Format(0, "#,##0.000")
    '        btnGenerar.Enabled = False
    '        txtNumComp.Text = String.Empty
    '        btnNuevoCab.Focus()
    '    End Sub

    '    Private Sub LimpiarCabecera()
    '        Dim text As Object
    '        For Each text In gbCabecera.Controls
    '            If TypeOf text Is TextBox Then text.Text = ""
    '            If TypeOf text Is ComboBox Then text.selectedIndex = -1
    '            If TypeOf text Is DateTimePicker Then text.value = Today
    '        Next
    '        cboSubdiario.SelectedIndex = -1
    '        Call Bloquear_Detalle()
    '    End Sub

    '    Private Sub Bloquear_Detalle()
    '        TabRegGeneral.Enabled = False
    '        btnNuevoDet.Enabled = False
    '        btnGrabar.Enabled = False
    '        btnEditar.Enabled = False
    '        btnCancelar.Enabled = False
    '        btnEliminar.Enabled = False
    '    End Sub

    '    Private Sub btnNuevoCab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoCab.Click

    '        If dgvAsiento.Rows.Count > 0 Then
    '            If MessageBox.Show("Desea Registrar un Nuevo Comprobante?", "Contabilidad", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
    '                Call LimparRegistroOpeGenerales()
    '            End If
    '        Else
    '            Call LimparRegistroOpeGenerales()
    '            Call CalcularDiferencia()
    '        End If

    '    End Sub

    '    Private Sub LimparRegistroOpeGenerales()
    '        gbCabecera.Enabled = True
    '        gbTipoConversion.Visible = False
    '        gbEspecial.Visible = False
    '        gbFechaTCambio.Visible = False
    '        dtpFechaTCambio.Visible = False
    '        LimpiarCabecera()
    '        cboSubdiario.Focus()
    '        DstAsientos.Tables.Clear()
    '        TabRegGeneral.SelectedIndex = 0
    '        dgvAsiento.DataSource = Nothing
    '        lblDebe.Text = Format(0, "#,##0.000")
    '        lblHaber.Text = Format(0, "#,##0.000")
    '        btnGenerar.Enabled = False
    '        txtNumComp.Text = String.Empty

    '        'Creando Tablas Temporales para los Asientos x Moneda
    '        For z As Integer = 0 To DTMonedas.Rows.Count - 1
    '            CrearTablasAsientos(DTMonedas.Rows(z).Item("Abreviado"))
    '            CrearTablaDestinos(DTMonedas.Rows(z).Item("Abreviado"))
    '            CrearTablaTransferencias(DTMonedas.Rows(z).Item("Abreviado"))
    '        Next
    '    End Sub

    '    Private Sub cboMoneda_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectionChangeCommitted
    '        StrAbrevMonCab = cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)

    '        If cboMoneda.SelectedIndex <> -1 Then
    '            If StrAbrevMonCab = STRmonSist Then
    '                txtValorCambio.Text = Format(1, "#,##0.000")
    '                cboTipoCambio.Text = "COMPRA (T.Diaria)"
    '            Else
    '                DTtcXfecha = New DataTable
    '                cCapaReCompConConv = New clsRegComprobConConv
    '                DTtcXfecha = cCapaReCompConConv.Buscar_TCxFecha(sEmpresa, cboMoneda.SelectedValue, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"))
    '                If DTtcXfecha.Rows.Count <= 0 Then
    '                    MessageBox.Show("No existe Tipo de Cambio para la fecha asignada, Registre Tipo de Cambio", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                    txtValorCambio.Text = Format(0, "#,##0.000")
    '                    cboMoneda.SelectedIndex = -1
    '                    cboMoneda.Focus()
    '                Else
    '                    For x As Integer = 0 To DTtcXfecha.Rows.Count - 1
    '                        If (DTtcXfecha.Rows(x).Item("Compra") = 0 Or DTtcXfecha.Rows(x).Item("Venta") = 0) Then
    '                            MessageBox.Show("No existe Tipo de Cambio para la fecha asignada, Registre Tipo de Cambio", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                            cboMoneda.Focus()
    '                            txtValorCambio.Text = Format(0, "#,##0.000")
    '                        End If
    '                    Next
    '                End If
    '                txtValorCambio.Text = Format(0, "#,##0.000")
    '                cboTipoCambio.SelectedIndex = -1
    '            End If
    '            If dtpFechaTCambio.Visible = True Then
    '                dtpFechaTCambio.Visible = False
    '                gbFechaTCambio.Visible = False
    '                gbTipoConversion.Visible = False
    '            End If
    '        End If
    '    End Sub

    '    Private Sub Timer2_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer2.Tick
    '        If cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue) <> STRmonSist Then
    '            If cboTipoCambio.SelectedValue = "3" Then
    '                txtValorCambio.Focus()
    '            ElseIf cboTipoCambio.SelectedValue = "4" Then
    '                dtpFechaTCambio.Focus()
    '            End If
    '        Else
    '            rbCompra.Focus()
    '        End If
    '        Timer2.Enabled = False
    '    End Sub

    '    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
    '        If BolIngAuto = True Then
    '            btnGenerar.Focus()
    '        Else
    '            btnNuevoDet.Focus()
    '        End If
    '        Timer1.Enabled = False
    '    End Sub

    '    Private Sub cboTipoCambio_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoCambio.SelectionChangeCommitted
    '        Dim drFilaMoneda() As DataRow

    '        If cboTipoCambio.Text = "ESPECIAL" Then
    '            DTtcXfecha = cCapaReCompConConv.SelectMonedas_Especial(sEmpresa, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"))
    '            txtValorCambio.Text = Format(0, "#,##0.000")
    '            If DTtcXfecha.Rows.Count = 0 Then
    '                MessageBox.Show("No se a registrado el Tipo de Cambio para la fecha asignada", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                gbFechaTCambio.Visible = False
    '                gbTipoConversion.Visible = False
    '                dtpFechaTCambio.Visible = False
    '                txtValorCambio.Text = Format(0, "#,##0.000")
    '                Me.Focus()
    '                Exit Sub
    '            ElseIf DTtcXfecha.Rows.Count >= 1 Then

    '                gbEspecial.Visible = True
    '                gbFechaTCambio.Visible = False
    '                gbTipoConversion.Visible = False
    '                dtpFechaTCambio.Visible = False

    '                dgvEpecial.DataSource = DTtcXfecha
    '                dgvEpecial.Columns(0).Visible = False
    '                dgvEpecial.Columns(1).ReadOnly = True
    '                dgvEpecial.Columns(1).Width = 50
    '                dgvEpecial.Columns(2).Width = 70

    '                dgvEpecial.RowHeadersVisible = False
    '                dgvEpecial.Focus()
    '                Exit Sub
    '            Else
    '                txtValorCambio.Enabled = True
    '                gbEspecial.Visible = False
    '                gbFechaTCambio.Visible = False
    '                gbTipoConversion.Visible = False
    '                dtpFechaTCambio.Visible = False
    '                txtValorCambio.Text = Format(0, "#,##0.000")
    '                Timer2.Enabled = True
    '                Exit Sub
    '            End If
    '        ElseIf cboTipoCambio.Text = "DE ACUERDO A FECHA" Then
    '            gbEspecial.Visible = False
    '            txtValorCambio.Enabled = False
    '            dtpFechaTCambio.Visible = True
    '            txtValorCambio.Text = Format(0, "#,##0.000")
    '            Timer2.Enabled = True
    '            Exit Sub
    '        Else
    '            gbEspecial.Visible = False
    '            txtValorCambio.Enabled = False
    '            dtpFechaTCambio.Visible = False
    '            gbTipoConversion.Visible = False
    '        End If
    '        DTtcXfecha = New DataTable
    '        cCapaReCompConConv = New clsRegComprobConConv

    '        DTtcXfecha = cCapaReCompConConv.Buscar_TCxFecha(sEmpresa, IIf(cboMoneda.SelectedValue = Nothing, String.Empty, cboMoneda.SelectedValue), Format(dtpFechaVoucher.Value, "dd/MM/yyyy"))
    '        drFilaMoneda = DTtcXfecha.Select("Moneda = '" & cboMoneda.SelectedValue & "'")

    '        If drFilaMoneda.Length > 0 Then
    '            If cboTipoCambio.Text = "COMPRA (T.Diaria)" Then
    '                txtValorCambio.Text = Format(drFilaMoneda(0).Item("Compra"), "#,##0.000")
    '            ElseIf cboTipoCambio.Text = "VENTA (T.Diaria)" Then
    '                txtValorCambio.Text = Format(drFilaMoneda(0).Item("Venta"), "#,##0.000")
    '            Else
    '                txtValorCambio.Text = Format(0, "#,##0.000")
    '            End If
    '        Else
    '            If StrAbrevMonCab = STRmonSist Then
    '                txtValorCambio.Text = Format(1, "#,##0.000")
    '            Else
    '                MessageBox.Show("No se a registrado el Tipo de Cambio para la fecha asignada", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                txtValorCambio.Text = Format(0, "#,##0.000")
    '            End If
    '        End If
    '    End Sub

    '    Private Sub dtpFechaTCambio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpFechaTCambio.KeyPress
    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                DTtcXfecha = New DataTable
    '                Dim DrFilaShow() As DataRow
    '                Dim TmpTabla As New DataTable

    '                If StrAbrevMonCab <> STRmonSist Then
    '                    DTtcXfecha = cCapaReCompConConv.Buscar_Todos_TCxFecha(sEmpresa, Format(dtpFechaTCambio.Value, "dd/MM/yyyy"))
    '                    TmpTabla = DTtcXfecha.Copy
    '                    TmpTabla.Rows.Clear()
    '                    DrFilaShow = DTtcXfecha.Select("Moneda='" & StrAbrevMonCab & "'")

    '                    If DTtcXfecha.Rows.Count > 0 Then
    '                        gbTipoConversion.Visible = False
    '                        gbFechaTCambio.Visible = True
    '                        If DrFilaShow.Length > 0 Then
    '                            TmpTabla.ImportRow(DrFilaShow(0))
    '                        End If
    '                        dgvFechaTCambio.DataSource = TmpTabla
    '                        dgvFechaTCambio.Columns(0).Visible = False
    '                        dgvFechaTCambio.RowHeadersVisible = False
    '                        For z As Integer = 0 To dgvFechaTCambio.Columns.Count - 1
    '                            dgvFechaTCambio.Columns(z).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
    '                            dgvFechaTCambio.Columns(z).SortMode = DataGridViewColumnSortMode.NotSortable
    '                        Next
    '                        dgvFechaTCambio.Focus()
    '                    Else
    '                        MessageBox.Show("No existe Tipo de Cambio para la Fecha indicada", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                        dtpFechaTCambio.Focus()
    '                    End If
    '                Else
    '                    DTtcXfecha = cCapaReCompConConv.Buscar_Todos_TCxFecha(sEmpresa, Format(dtpFechaTCambio.Value, "dd/MM/yyyy"))
    '                    If DTtcXfecha.Rows.Count > 0 Then
    '                        gbFechaTCambio.Visible = False
    '                        gbTipoConversion.Visible = True
    '                        txtValorCambio.Text = Format(1, "#,##0.000")
    '                        Timer2.Enabled = True
    '                    Else
    '                        MessageBox.Show("No existe Tipo de Cambio para la Fecha indicada", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                        gbTipoConversion.Visible = False
    '                        dtpFechaTCambio.Focus()
    '                    End If
    '                End If
    '        End Select
    '    End Sub

    '    Private Sub dgvFechaTCambio_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvFechaTCambio.CellEnter
    '        If (dgvFechaTCambio.Columns(e.ColumnIndex).Name = "Compra" Or dgvFechaTCambio.Columns(e.ColumnIndex).Name = "Venta") Then
    '            Str_CoV = dgvFechaTCambio.Columns(e.ColumnIndex).Name
    '        Else
    '            Str_CoV = String.Empty
    '        End If
    '    End Sub

    '    Private Sub dgvFechaTCambio_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvFechaTCambio.KeyDown
    '        Select Case e.KeyCode
    '            Case Keys.Escape
    '                gbFechaTCambio.Visible = False
    '                dtpFechaTCambio.Focus()
    '        End Select
    '    End Sub

    '    Private Sub dgvFechaTCambio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dgvFechaTCambio.KeyPress
    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                If dgvFechaTCambio.Rows.Count > 0 Then
    '                    If dgvFechaTCambio.Columns(dgvFechaTCambio.CurrentCell.ColumnIndex).Name = "Moneda" Then
    '                        MessageBox.Show("Debe elegir el valor de Compra o Venta", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                        dgvFechaTCambio.Focus()
    '                        gbFechaTCambio.Visible = True
    '                    Else
    '                        DblTCsegunFec = 0
    '                        DblTCsegunFec = dgvFechaTCambio.Rows(dgvFechaTCambio.CurrentRow.Index).Cells(dgvFechaTCambio.Columns(dgvFechaTCambio.CurrentCell.ColumnIndex).Name).Value
    '                        txtValorCambio.Text = Format(Math.Round(DblTCsegunFec, 3), "#,##0.000")
    '                        gbFechaTCambio.Visible = False
    '                        txtGlosaCab.Focus()
    '                    End If
    '                End If
    '        End Select
    '    End Sub

    '    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
    '        Me.Close()
    '    End Sub

    '    Private Sub dtpFechaVoucher_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpFechaVoucher.ValueChanged
    '        dtpFechaTCambio.Value = dtpFechaVoucher.Value
    '        dtpFechaTCambio.Value = dtpFechaVoucher.Value
    '        dtpFechaDocDet.Value = dtpFechaVoucher.Value
    '        dtpFechaVcmtoDet.Value = dtpFechaVoucher.Value
    '    End Sub

    '    Private Function ValidarIngresoCabecera() As Boolean
    '        Dim TablaCambios As New DataTable

    '        TablaCambios = cCapaReCompConConv.Buscar_Todos_TCxFecha(sEmpresa, dtpFechaVoucher.Value)

    '        If TablaCambios.Rows.Count = 0 Then
    '            MessageBox.Show("No se existen Tipos de Cambio a la fecha indicada", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            btnSalir.Focus()
    '            Return False
    '        ElseIf cboSubdiario.SelectedIndex = -1 Then
    '            MessageBox.Show("Debe elegir un Subdiario", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            cboSubdiario.Focus()
    '            Return False
    '        ElseIf cboMoneda.SelectedIndex = -1 Then
    '            MessageBox.Show("Debe elegir una Moneda para la operacion", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            cboMoneda.Focus()
    '            Return False
    '        ElseIf cboTipoCambio.SelectedIndex = -1 Then
    '            MessageBox.Show("Debe elegir un tipo de cambio para la Moneda", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            cboTipoCambio.Focus()
    '            Return False
    '        ElseIf txtGlosaCab.Text = String.Empty Then
    '            MessageBox.Show("Ingrese la Glosa", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            txtGlosaCab.Focus()
    '            Return False
    '        ElseIf (txtValorCambio.Text = "0.000" Or CDbl(txtValorCambio.Text) = 0) Then
    '            If cboTipoCambio.SelectedValue = "3" Then
    '                For z As Integer = 0 To dgvEpecial.Rows.Count - 1
    '                    If dgvEpecial.Rows(z).Cells(2).Value <= 0 Then
    '                        MessageBox.Show("Verifique los Tipos Cambio Especial", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                        cboTipoCambio.Focus()
    '                        Return False
    '                    End If
    '                Next
    '            Else
    '                MessageBox.Show("No existe Tipo de Cambio para la Moneda de eligio, Verifique", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                Return False
    '            End If
    '        End If




    '        Return True
    '    End Function

    '    Private Sub txtGlosaCab_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtGlosaCab.KeyPress
    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                If ValidarIngresoCabecera() = False Then Exit Sub
    '                cboMonedaAsiento.SelectedValue = cboMoneda.SelectedValue
    '                Call Al_HacerCancelar()
    '                Call Limpiar_DesGrab()
    '                Timer1.Enabled = True
    '                gbCabecera.Enabled = False
    '                BolIngAuto = False
    '        End Select
    '    End Sub

    '    Private Function Devuelve_TC_Especial(ByVal Grilla As System.Windows.Forms.DataGridView, ByVal Moneda As String) As Double
    '        Dim DblValor As Double = 0

    '        For x As Integer = 0 To Grilla.Rows.Count - 1
    '            If Grilla.Rows(x).Cells("Moneda").Value = Moneda Then DblValor = Grilla.Rows(x).Cells("Valor").Value
    '        Next

    '        Return DblValor
    '    End Function

    '    Private Function MontoTC_segunComboFecha(ByVal TablaCambios As DataTable, ByVal dFecha As Date, _
    '                                         ByVal strMoneda As String, ByVal StrFormatConversion As String) As Double
    '        Dim DrFilaCambio() As DataRow
    '        Dim MontoTC As Double

    '        DrFilaCambio = TablaCambios.Select("Fecha='" & dFecha & "' AND Moneda='" & strMoneda & "'")

    '        If DrFilaCambio.Length > 0 Then
    '            If StrFormatConversion = "Compra" Then
    '                MontoTC = DrFilaCambio(0).Item("Compra")
    '            ElseIf StrFormatConversion = "Venta" Then
    '                MontoTC = DrFilaCambio(0).Item("Venta")
    '            ElseIf StrFormatConversion = String.Empty Then
    '                MontoTC = 0
    '            End If
    '        End If
    '        Return MontoTC
    '    End Function

    '    Private Function Asiento_Segun_Doc(ByVal TablaPar As DataTable, ByVal ComboDoc As String) As String
    '        Dim DrFilasDoc() As DataRow = TablaPar.Select("Grupo='4'")
    '        Dim DoH As String = String.Empty

    '        If DrFilasDoc.Length > 0 Then
    '            For i As Integer = 0 To DrFilasDoc.Length - 1
    '                If DrFilasDoc(i).Item("Codigo") = ComboDoc Then
    '                    DoH = DrFilasDoc(i).Item("Otros")
    '                    Exit For
    '                End If
    '            Next
    '        End If
    '        Return DoH
    '    End Function

    '    Private Sub Sumar_Importes(ByVal Tabla As DataTable)
    '        Dim DblDebe As Double = 0
    '        Dim DblHaber As Double = 0

    '        Tabla.DefaultView.Sort = "Secuencia ASC"
    '        For z As Integer = 0 To Tabla.Rows.Count - 1
    '            DblDebe = DblDebe + Tabla.Rows(z).Item("Debe")
    '            DblHaber = DblHaber + Tabla.Rows(z).Item("Haber")
    '        Next

    '        lblDebe.Text = Math.Round(DblDebe, BytFormatConta)
    '        lblHaber.Text = Math.Round(DblHaber, BytFormatConta)
    '    End Sub

    '    Private Sub Ajustar_Celdas(ByVal DT As DataTable)
    '        For z As Integer = 0 To DT.Columns.Count - 1
    '            dgvAsiento.Columns(z).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
    '        Next
    '    End Sub

    '    Private Sub Deshabilitar_Columans_Sort(ByVal Tabla As DataTable)
    '        For y As Integer = 0 To Tabla.Columns.Count - 1
    '            dgvAsiento.Columns(y).SortMode = DataGridViewColumnSortMode.NotSortable
    '        Next
    '    End Sub

    '    Private Sub cboMonedaAsiento_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMonedaAsiento.SelectionChangeCommitted
    '        dgvAsiento.DataSource = DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMonedaAsiento.SelectedValue))
    '        Sumar_Importes(DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMonedaAsiento.SelectedValue)))

    '        If cboMoneda.SelectedValue <> cboMonedaAsiento.SelectedValue Then
    '            btnNuevoDet.Enabled = False
    '            btnGrabar.Enabled = False
    '            btnEditar.Enabled = False
    '            btnCancelar.Enabled = False
    '            btnEliminar.Enabled = False
    '            btnGenerar.Enabled = False
    '        Else

    '            If TabRegGeneral.SelectedIndex = 0 Then
    '                btnNuevoDet.Enabled = True
    '                btnGrabar.Enabled = False
    '                btnEditar.Enabled = True
    '                btnCancelar.Enabled = False
    '                btnEliminar.Enabled = True
    '                btnGenerar.Enabled = True
    '            Else
    '                btnNuevoDet.Enabled = False
    '                btnGrabar.Enabled = True
    '                btnEditar.Enabled = False
    '                btnCancelar.Enabled = True
    '                btnEliminar.Enabled = False
    '                btnGenerar.Enabled = False
    '            End If
    '        End If
    '        Call CalcularDiferencia()
    '    End Sub

    '    Private Sub rbCompra_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles rbCompra.KeyPress
    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                txtGlosaCab.Focus()
    '        End Select
    '    End Sub

    '    Private Sub rbVenta_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles rbVenta.KeyPress
    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                txtGlosaCab.Focus()
    '        End Select
    '    End Sub

    '    Private Function AsignarCuenta(ByVal DTCodCuenta As DataTable, ByVal Moneda As String) As String
    '        Dim DrCuenta() As DataRow
    '        Dim CodCta As String

    '        DrCuenta = DTCodCuenta.Select("Codigo='" & Moneda & "'")
    '        If DrCuenta.Length > 0 Then
    '            CodCta = DrCuenta(0).Item("Codigo")
    '        Else
    '            CodCta = String.Empty
    '        End If
    '        Return CodCta
    '    End Function

    '    Private Sub btnNuevoDet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoDet.Click
    '        TabRegGeneral.SelectedIndex = 1
    '        If TabRegGeneral.Enabled = False Then TabRegGeneral.Enabled = True
    '        Dim text As Object
    '        For Each text In gbRegistro.Controls
    '            If TypeOf text Is TextBox Then
    '                text.Text = ""
    '                text.enabled = False
    '            End If
    '        Next

    '        Call Al_HacerNuevo()
    '        gbRegistro.Enabled = True

    '        mtbCuenta.Text = String.Empty
    '        lblDescripCuenta.Text = String.Empty
    '        lblDescAnexDet.Text = String.Empty
    '        lblDescAreaDet.Text = String.Empty
    '        lblDescCCostoDet.Text = String.Empty
    '        cboTipoDocDet.SelectedIndex = -1
    '        cboTipoDocDet.Enabled = False
    '        dtpFechaDocDet.Enabled = False
    '        dtpFechaVcmtoDet.Enabled = False
    '        BolEditReg = False
    '        btnFiltro.Enabled = False
    '        If mtbCuenta.Enabled = False Then mtbCuenta.Enabled = True
    '        mtbCuenta.Focus()

    '    End Sub

    '    Private Sub Al_HacerNuevo()
    '        btnNuevoDet.Enabled = False
    '        btnGrabar.Enabled = True
    '        btnEditar.Enabled = False
    '        btnCancelar.Enabled = True
    '        btnEliminar.Enabled = False
    '    End Sub

    '    Private Sub Al_HacerCancelar()
    '        btnNuevoDet.Enabled = True
    '        btnGrabar.Enabled = False
    '        btnEditar.Enabled = True
    '        btnCancelar.Enabled = False
    '        btnEliminar.Enabled = True
    '    End Sub

    '    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
    '        Call Al_HacerCancelar()
    '        Call Limpiar_DesGrab()
    '        TabRegGeneral.SelectedIndex = 0
    '        BolEditReg = False
    '        btnNuevoDet.Focus()
    '    End Sub

    '    Private Sub Limpiar_DesGrab()
    '        Dim Text As Object
    '        For Each Text In gbRegistro.Controls
    '            If TypeOf Text Is TextBox Then Text.Text = ""
    '        Next
    '        mtbCuenta.Text = String.Empty
    '        lblDescripCuenta.Text = String.Empty
    '        cboTipoDocDet.SelectedIndex = -1
    '    End Sub

    '    Private Sub mtbCuenta_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles mtbCuenta.DoubleClick
    '        ObjAyuda = New Myhelp.ClsAyuda
    '        With ObjAyuda
    '            .strSERVER = STRSERVER
    '            .strBASEDATOS = STRBASEDATOS
    '            .strTITULOAYUDA = "Listado de Cuentas"
    '            .IntOPCION = 3
    '            .strCODEMPRESA = sEmpresa
    '            .StrPERIODO = sPeriodo
    '            .strQUERY = "Contabilidad.UspAyuda_Select"
    '            .Mostrar()
    '            If .drFILA Is Nothing Then
    '                Exit Sub
    '            Else
    '                mtbCuenta.Text = Trim(.drFILA.Item(0).ToString)
    '                lblDescripCuenta.Text = Trim(.drFILA.Item(1).ToString)
    '                StrTipoAnexo = Trim(.drFILA.Item(5).ToString)
    '            End If
    '        End With
    '    End Sub

    '    Private Sub mtbCuenta_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles mtbCuenta.GotFocus
    '        If BolEditReg = False Then
    '            Dim text As Object
    '            For Each text In gbRegistro.Controls
    '                If TypeOf text Is TextBox Then
    '                    text.Text = ""
    '                    text.enabled = False
    '                End If
    '            Next

    '            Call Al_HacerNuevo()
    '            gbRegistro.Enabled = True

    '            'mtbCuenta.Text = String.Empty
    '            lblDescripCuenta.Text = String.Empty
    '            lblDescAnexDet.Text = String.Empty
    '            lblDescAreaDet.Text = String.Empty
    '            lblDescCCostoDet.Text = String.Empty
    '            cboTipoDocDet.SelectedIndex = -1
    '            cboTipoDocDet.Enabled = False
    '            dtpFechaDocDet.Enabled = False
    '            dtpFechaVcmtoDet.Enabled = False
    '            If mtbCuenta.Enabled = False Then mtbCuenta.Enabled = True
    '            mtbCuenta.Focus()
    '        End If

    '    End Sub

    '    Private Sub mtbCuenta_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles mtbCuenta.KeyDown
    '        Select Case e.KeyCode
    '            Case Keys.F1
    '                ObjAyuda = New Myhelp.ClsAyuda
    '                With ObjAyuda
    '                    .strSERVER = STRSERVER
    '                    .strBASEDATOS = STRBASEDATOS
    '                    .strTITULOAYUDA = "Listado de Cuentas"
    '                    .IntOPCION = 3
    '                    .strCODEMPRESA = sEmpresa
    '                    .StrPERIODO = sPeriodo
    '                    .strQUERY = "Contabilidad.UspAyuda_Select"
    '                    .Mostrar()
    '                    If .drFILA Is Nothing Then
    '                        Exit Sub
    '                    Else
    '                        mtbCuenta.Text = Trim(.drFILA.Item(0).ToString)
    '                        lblDescripCuenta.Text = Trim(.drFILA.Item(1).ToString)
    '                        StrTipoAnexo = Trim(.drFILA.Item(5).ToString)
    '                    End If
    '                End With
    '        End Select
    '    End Sub

    '    Private Sub mtbCuenta_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtbCuenta.KeyPress
    '        Dim DrCuenta As DataRow

    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                DrCuenta = cCapaReCompConConv.Habilitar_Deshabilitar_Controles_SegunCta(sEmpresa, mtbCuenta.Text, lblDescripCuenta.Text, sPeriodo)

    '                If DrCuenta.Table.Rows.Count > 0 Then
    '                    If IsDBNull(DrCuenta.Item("tipoanaliticocodigo")) Then
    '                        StrCodTipoAnex = String.Empty
    '                    Else
    '                        StrCodTipoAnex = Trim(DrCuenta.Item("tipoanaliticocodigo"))
    '                    End If

    '                    If BolEditReg = False Then
    '                        txtAnexoDet.Text = String.Empty
    '                        txtCCostoDet.Text = String.Empty
    '                        cboTipoDocDet.SelectedIndex = -1
    '                        txtSerieDocDet.Text = String.Empty
    '                        txtNumDocDet.Text = String.Empty
    '                        dtpFechaDocDet.Value = dtpFechaVoucher.Value
    '                        dtpFechaVcmtoDet.Value = dtpFechaVoucher.Value
    '                        txtAreaDet.Text = String.Empty
    '                        txtDebe.Text = String.Empty
    '                        txtHaber.Text = String.Empty
    '                        txtGlosaDet.Text = String.Empty
    '                    End If

    '                    If Not IsDBNull(DrCuenta.Item("tipoanaliticocodigo")) Then
    '                        txtAnexoDet.Enabled = True
    '                    Else
    '                        txtAnexoDet.Enabled = False
    '                    End If
    '                    If Trim(DrCuenta.Item("f_CenCosto")) = "1" Then
    '                        txtCCostoDet.Enabled = True
    '                    Else
    '                        txtCCostoDet.Enabled = False
    '                    End If
    '                    If Trim(DrCuenta.Item("f_cuentaDocRef")) = "1" Then
    '                        cboTipoDocDet.Enabled = True
    '                        txtSerieDocDet.Enabled = True
    '                        txtNumDocDet.Enabled = True
    '                        dtpFechaDocDet.Enabled = True
    '                        'dtpFechaDocDet.Value = dtpFechaVoucher.Value
    '                        'dtpFechaVoucher.Value = dtpFechaDocDet.Value
    '                        If BolEditReg = False Then btnFiltro.Enabled = True
    '                        'btnFiltro.Enabled = True
    '                    Else
    '                        cboTipoDocDet.Enabled = False
    '                        txtSerieDocDet.Enabled = False
    '                        txtNumDocDet.Enabled = False
    '                        dtpFechaDocDet.Enabled = False
    '                        btnFiltro.Enabled = False
    '                    End If
    '                    If Trim(DrCuenta.Item("f_cuentaFechaVenc")) = "1" Then
    '                        dtpFechaVcmtoDet.Enabled = True
    '                    Else
    '                        dtpFechaVcmtoDet.Enabled = False
    '                    End If
    '                    If Trim(DrCuenta.Item("f_Area")) = "1" Then
    '                        txtAreaDet.Enabled = True
    '                    Else
    '                        txtAreaDet.Enabled = False
    '                    End If
    '                    txtDebe.Enabled = True
    '                    txtHaber.Enabled = True
    '                    txtGlosaDet.Enabled = True

    '                    If txtAnexoDet.Enabled = True Then
    '                        txtAnexoDet.Focus()
    '                    ElseIf txtCCostoDet.Enabled = True Then
    '                        txtCCostoDet.Focus()
    '                    ElseIf cboTipoDocDet.Enabled = True Then
    '                        cboTipoDocDet.Focus()
    '                    ElseIf dtpFechaVcmtoDet.Enabled = True Then
    '                        dtpFechaVcmtoDet.Focus()
    '                    ElseIf txtAreaDet.Enabled = True Then
    '                        txtAreaDet.Focus()
    '                    Else
    '                        txtDebe.Focus()
    '                    End If
    '                End If
    '        End Select
    '    End Sub

    '    Private Sub txtAnexoDet_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnexoDet.DoubleClick
    '        ObjAyudaQuery = New Myhelp.ClsAyudaQuery

    '        With ObjAyudaQuery
    '            .strSERVER = STRSERVER
    '            .strBASEDATOS = STRBASEDATOS
    '            .strTITULOAYUDA = "Listado de Anexos"
    '            .strQUERY = "SELECT analiticocodigo as Codigo, analiticoDescripcion as Descripcion, tipoanaliticocodigo as [Tipo Anexo],MonCodigo as Moneda, Ruc " & _
    '                        "FROM Contabilidad.ct_analitico " & _
    '                        "WHERE EmprCodigo='" & sEmpresa & "' AND tipoanaliticocodigo='" & StrTipoAnexo & "'"
    '            .MostrarDatos()
    '            If .drFILA Is Nothing Then
    '                Exit Sub
    '            Else
    '                txtAnexoDet.Text = Trim(.drFILA.Item(0).ToString)
    '            End If
    '        End With
    '    End Sub

    '    Private Sub txtAnexoDet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAnexoDet.KeyDown
    '        Select Case e.KeyCode
    '            Case Keys.F1
    '                ObjAyudaQuery = New Myhelp.ClsAyudaQuery

    '                With ObjAyudaQuery
    '                    .strSERVER = STRSERVER
    '                    .strBASEDATOS = STRBASEDATOS
    '                    .strTITULOAYUDA = "Listado de Anexos"
    '                    .strQUERY = "SELECT analiticocodigo as Codigo, analiticoDescripcion as Descripcion, tipoanaliticocodigo as [Tipo Anexo],MonCodigo as Moneda, Ruc " & _
    '                                "FROM Contabilidad.ct_analitico " & _
    '                                "WHERE EmprCodigo='" & sEmpresa & "' AND tipoanaliticocodigo='" & StrTipoAnexo & "'"
    '                    .MostrarDatos()
    '                    If .drFILA Is Nothing Then
    '                        Exit Sub
    '                    Else
    '                        txtAnexoDet.Text = Trim(.drFILA.Item(0).ToString)
    '                        lblDescAnexDet.Text = Trim(.drFILA.Item(1).ToString)
    '                    End If
    '                End With
    '        End Select
    '    End Sub

    '    Private Function Validar_Ingreso_Asiento() As Boolean
    '        If DTnivelesCta.Rows.Count > 0 Then
    '            Dim ContDigNivel As Integer = 0
    '            For z As Integer = 3 To 7
    '                ContDigNivel += DTnivelesCta.Rows(0)(z)
    '            Next
    '            Select Case DTnivelesCta.Rows(0).Item("idNivel")
    '                Case 2
    '                    ContDigNivel += 1
    '                Case 3
    '                    ContDigNivel += 2
    '                Case 4
    '                    ContDigNivel += 3
    '            End Select
    '            If mtbCuenta.Text.Length <> ContDigNivel Then
    '                MessageBox.Show("No ingreso ninguna codigo de cuenta o verifique el formato de la Cuenta, Verifique", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                mtbCuenta.Focus()
    '                Return False
    '            ElseIf (txtDebe.Text = String.Empty And txtHaber.Text = String.Empty) Then
    '                MessageBox.Show("Debe ingresar un monto para el Debe o para el Haber", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                txtDebe.Focus()
    '                Return False
    '            ElseIf (Val(txtDebe.Text) = 0 And Val(txtHaber.Text) = 0) Then
    '                MessageBox.Show("Debe ingresar un monto para el Debe o para el Haber", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                txtDebe.Focus()
    '                Return False
    '            ElseIf txtCCostoDet.Enabled = True Then
    '                If BolEditReg = False Then
    '                    If txtCCostoDet.Text = String.Empty Then
    '                        MessageBox.Show("Debe ingresar el codigo de Centro de Costo", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                        txtCCostoDet.Focus()
    '                        Return False
    '                    ElseIf txtAreaDet.Enabled = True Then
    '                        If txtAreaDet.Text = String.Empty Then
    '                            MessageBox.Show("Debe ingresar el Area para la Cuenta", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                            txtAreaDet.Focus()
    '                            Return False
    '                        End If
    '                    End If
    '                End If
    '            End If
    '            Return True
    '        Else
    '            MessageBox.Show("No existe ninguna configuracion para los Niveles de Cuenta, Verifique", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Return False
    '        End If
    '    End Function

    '    Private Sub btnGrabar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click

    '        Dim DrFilaCab As DataRow
    '        Dim i As Integer = 0
    '        Dim StrTabla As String
    '        Dim TCvoucher As Double = 0
    '        Dim myTC As Double
    '        Dim MonedaVuelta As String = String.Empty
    '        cCapaReCompConConv = New clsRegComprobConConv


    '        If BolEditReg = True Then
    '            Call Eliminar_RegistroTemporal()
    '        End If

    '        If Validar_Ingreso_Asiento() = False Then Exit Sub


    '        Do While DTMonedas.Rows.Count > i
    '            StrTabla = "TmpAsiento" & DTMonedas.Rows(i).Item("Abreviado")
    '            MonedaVuelta = DTMonedas.Rows(i).Item("Abreviado")
    '            DrFilaCab = DstAsientos.Tables(StrTabla).NewRow

    '            ''****** Genero Asiento de Destinos en caso si lo tuviera
    '            'GenerarAsientosDestinos(sEmpresa, Trim(mtbCuenta.Text), sPeriodo, MonedaVuelta, Trim(mtbCuenta.Text))
    '            ''****** Genero Asiento de Transferencias segun la cuenta
    '            'If txtCCostoDet.Enabled = True And txtCCostoDet.Text <> String.Empty Then
    '            '    GenerarAsientoTransferencias(sEmpresa, Trim(txtCCostoDet.Text), sPeriodo, MonedaVuelta, Trim(mtbCuenta.Text))
    '            'End If

    '            If BolEditReg = True Then
    '                DrFilaCab("Secuencia") = CodSecuencia_Edit
    '            Else
    '                DrFilaCab("Secuencia") = Generar_Secuencia(DstAsientos.Tables(StrTabla))
    '            End If

    '            DrFilaCab("Codigo_Cuenta") = Trim(mtbCuenta.Text)
    '            DrFilaCab("Codigo_Anexo") = Trim(txtAnexoDet.Text)
    '            DrFilaCab("Cod_Tipo_Anexo") = StrCodTipoAnex
    '            If cboTipoDocDet.SelectedIndex = -1 Then
    '                DrFilaCab("Tipo_Doc") = String.Empty
    '            Else
    '                DrFilaCab("Tipo_Doc") = cboTipoDocDet.SelectedValue
    '            End If
    '            DrFilaCab("Serie_Doc") = Trim(txtSerieDocDet.Text)
    '            DrFilaCab("Nro_Doc") = Trim(txtNumDocDet.Text)
    '            DrFilaCab("MonedaTrans") = MonedaVuelta

    '            If cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue) = STRmonSist Then
    '                If cboTipoCambio.SelectedValue = "3" Then
    '                    TCvoucher = 1
    '                Else
    '                    TCvoucher = Val(txtValorCambio.Text)
    '                End If
    '            Else
    '                If cboTipoCambio.SelectedValue = "3" Then
    '                    TCvoucher = Devuelve_TC_Especial(dgvEpecial, cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '                Else
    '                    TCvoucher = txtValorCambio.Text
    '                End If
    '            End If

    '            If MonedaVuelta = STRmonSist Then
    '                myTC = 1

    '                DrFilaCab("MontoTC") = Format(myTC, "#,##0.000")
    '                DrFilaCab("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")

    '                If MonedaVuelta = cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue) Then
    '                    If (txtDebe.Text = String.Empty Or Val(txtDebe.Text) = 0) Then
    '                        DrFilaCab("Debe") = Format(0, "#,##0.000")
    '                        DrFilaCab("Haber") = Math.Round(CDbl(txtHaber.Text) * TCvoucher / myTC, BytFormatConta)
    '                    Else
    '                        DrFilaCab("Debe") = Math.Round(CDbl(txtDebe.Text) * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaCab("Haber") = Format(0, "#,##0.000")
    '                    End If
    '                Else
    '                    If (txtDebe.Text = String.Empty Or Val(txtDebe.Text) = 0) Then
    '                        DrFilaCab("Debe") = Format(0, "#,##0.000")
    '                        DrFilaCab("Haber") = Math.Round(CDbl(txtHaber.Text) * TCvoucher / myTC, BytFormatConta)
    '                    Else
    '                        DrFilaCab("Debe") = Math.Round(CDbl(txtDebe.Text) * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaCab("Haber") = Format(0, "#,##0.000")
    '                    End If
    '                End If
    '            Else

    '                If dtpFechaTCambio.Visible = False Then
    '                    If (cboTipoCambio.Text = "ESPECIAL" Or cboTipoCambio.SelectedValue = "3") Then
    '                        myTC = Devuelve_TC_Especial(dgvEpecial, MonedaVuelta)
    '                    Else
    '                        myTC = cCapaReCompConConv.TipoCambio_Actual(sEmpresa, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), cFuncGen.CodigoMoneda(MonedaVuelta), cboTipoCambio.Text)
    '                    End If
    '                Else
    '                    If gbTipoConversion.Visible = True Then
    '                        myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, MonedaVuelta, IIf(rbCompra.Checked = True, "Compra", "Venta"))
    '                    Else
    '                        myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, MonedaVuelta, Str_CoV)
    '                    End If
    '                End If

    '                If myTC = 0 Then
    '                    MessageBox.Show("No ingreso Tipo de Cambio para la Moneda en " & Trim(DTMonedas.Rows(i).Item("Descripcion")), "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                    Exit Sub
    '                End If
    '                DrFilaCab("MontoTC") = Format(myTC, "#,##0.000")
    '                DrFilaCab("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")


    '                If MonedaVuelta = cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue) Then
    '                    If (txtDebe.Text = String.Empty Or Val(txtDebe.Text) = 0) Then
    '                        DrFilaCab("Debe") = Format(0, "#,##0.000")
    '                        DrFilaCab("Haber") = Math.Round(CDbl(txtHaber.Text) * TCvoucher / myTC, BytFormatConta)
    '                    Else
    '                        DrFilaCab("Debe") = Math.Round(CDbl(txtDebe.Text) * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaCab("Haber") = Format(0, "#,##0.000")
    '                    End If
    '                Else
    '                    If (txtDebe.Text = String.Empty Or Val(txtDebe.Text) = 0) Then
    '                        DrFilaCab("Debe") = Format(0, "#,##0.000")
    '                        DrFilaCab("Haber") = Math.Round(CDbl(txtHaber.Text) * TCvoucher / myTC, BytFormatConta)
    '                    Else
    '                        DrFilaCab("Debe") = Math.Round(CDbl(txtDebe.Text) * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaCab("Haber") = Format(0, "#,##0.000")
    '                    End If
    '                End If
    '            End If

    '            DrFilaCab("FechaDoc") = Format(dtpFechaDocDet.Value, "dd/MM/yyyy")
    '            If dtpFechaVcmtoDet.Enabled = True Then
    '                DrFilaCab("Caduca_Fecha") = Format(dtpFechaVcmtoDet.Value, "dd/MM/yyyy")
    '            Else
    '                DrFilaCab("Caduca_Fecha") = System.DBNull.Value
    '            End If

    '            DrFilaCab("Ind_Trans_Dif") = "0"
    '            DrFilaCab("Gen_Cta_Destino") = ""
    '            DrFilaCab("ComentarioCab") = Trim(txtGlosaDet.Text)
    '            DrFilaCab("ID_CentroCosto") = Trim(txtCCostoDet.Text)
    '            DrFilaCab("Area") = Trim(txtAreaDet.Text)
    '            If MonedaVuelta = STRmonSist Then
    '                DrFilaCab("Moneda_Sist") = "1"
    '            Else
    '                DrFilaCab("Moneda_Sist") = "0"
    '            End If
    '            If MonedaVuelta = cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue) Then
    '                DrFilaCab("DocTransact") = "1"
    '            Else
    '                DrFilaCab("DocTransact") = "0"
    '            End If
    '            DrFilaCab("MontoOriginal") = 0
    '            DrFilaCab("ExisteDoc") = "0"

    '            DstAsientos.Tables(StrTabla).Rows.Add(DrFilaCab)
    '            i += 1
    '        Loop

    '        dgvAsiento.DataSource = DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '        Call Sumar_Importes(DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)))

    '        For x As Integer = 0 To DTMonedas.Rows.Count - 1
    '            Call Ajustar_Celdas(DstAsientos.Tables("TmpAsiento" & DTMonedas.Rows(x).Item("Abreviado")))
    '            Call Deshabilitar_Columans_Sort(DstAsientos.Tables("TmpAsiento" & DTMonedas.Rows(x).Item("Abreviado")))
    '        Next

    '        'DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).DefaultView.Sort() = "Secuencia ASC"
    '        Call CalcularDiferencia()

    '        TabRegGeneral.SelectedIndex = 0
    '        Call Al_HacerCancelar()
    '        Call Limpiar_DesGrab()
    '        Timer1.Enabled = True
    '        btnNuevoDet.Focus()
    '        btnGenerar.Enabled = True
    '    End Sub

    '    Private Sub Eliminar_RegistroTemporal()
    '        Dim DrFilaEliminar() As DataRow
    '        'Dim DrFilaDestino() As DataRow
    '        'Dim DrFilaTransferencias() As DataRow
    '        Dim CodSecuencia As String
    '        Dim StrCodCta As String = String.Empty

    '        If dgvAsiento.Rows.Count > 0 Then
    '            CodSecuencia = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Secuencia").Value
    '            CodSecuencia_Edit = CodSecuencia
    '            StrCodCta = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Codigo_Cuenta").Value

    '            For z As Integer = 0 To DTMonedas.Rows.Count - 1
    '                Dim NomTabla As String = "TmpAsiento" & DTMonedas.Rows(z).Item("Abreviado")
    '                DrFilaEliminar = DstAsientos.Tables(NomTabla).Select("Secuencia='" & CodSecuencia & "'")

    '                'Dim NomTablaDestino As String = "TmpAsientoDestino" & DTMonedas.Rows(z).Item("Abreviado")
    '                'Dim NomTablaTransferencias As String = "TmpTransferencias" & DTMonedas.Rows(z).Item("Abreviado")
    '                'DrFilaDestino = DstAsientos.Tables(NomTablaDestino).Select("CtaConDestino='" & StrCodCta & "'")
    '                'DrFilaTransferencias = DstAsientos.Tables(NomTablaTransferencias).Select("CtaConDestino='" & StrCodCta & "'")

    '                If DrFilaEliminar.Length > 0 Then
    '                    DstAsientos.Tables(NomTabla).Rows.Remove(DrFilaEliminar(0))
    '                    'Call Gen_Secuencia_DesElim(DstAsientos.Tables(NomTabla))
    '                    'If cboMonedaAsiento.SelectedValue = DTMonedas.Rows(z).Item("Codigo") Then
    '                    '    Call Sumar_Importes(DstAsientos.Tables(NomTabla))
    '                    'End If
    '                End If

    '                'If DrFilaDestino.Length > 0 Then
    '                '    For Rows As Integer = 0 To DrFilaDestino.Length - 1
    '                '        DstAsientos.Tables(NomTablaDestino).Rows.Remove(DrFilaDestino(Rows))
    '                '    Next
    '                'End If

    '                'If DrFilaTransferencias.Length > 0 Then
    '                '    For Rows As Integer = 0 To DrFilaTransferencias.Length - 1
    '                '        DstAsientos.Tables(NomTablaTransferencias).Rows.Remove(DrFilaTransferencias(Rows))
    '                '    Next
    '                'End If
    '            Next
    '        End If
    '    End Sub

    '    Private Sub btnEditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
    '        Try
    '            If dgvAsiento.Rows.Count > 0 Then
    '                txtSecuencia.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Secuencia").Value
    '                mtbCuenta.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Codigo_Cuenta").Value
    '                If (Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Codigo_Anexo").Value) Or dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Codigo_Anexo").Value <> String.Empty) Then
    '                    txtAnexoDet.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Codigo_Anexo").Value
    '                Else
    '                    txtAnexoDet.Text = String.Empty
    '                End If
    '                If (Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("ID_CentroCosto").Value) Or dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("ID_CentroCosto").Value <> String.Empty) Then
    '                    txtCCostoDet.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("ID_CentroCosto").Value
    '                Else
    '                    txtCCostoDet.Text = String.Empty
    '                End If

    '                Call LLenarComboDocuments(IIf(IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Cod_Tipo_Anexo").Value), String.Empty, dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Cod_Tipo_Anexo").Value))
    '                If (Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Tipo_Doc").Value) Or dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Tipo_Doc").Value <> String.Empty) Then
    '                    cboTipoDocDet.SelectedValue = Trim(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Tipo_Doc").Value)
    '                Else
    '                    cboTipoDocDet.SelectedIndex = -1
    '                End If
    '                If (Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Serie_Doc").Value) Or dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Serie_Doc").Value <> String.Empty) Then
    '                    txtSerieDocDet.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Serie_Doc").Value
    '                Else
    '                    txtSerieDocDet.Text = String.Empty
    '                End If
    '                If (Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Serie_Doc").Value) Or dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Serie_Doc").Value <> String.Empty) Then
    '                    txtSerieDocDet.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Serie_Doc").Value
    '                Else
    '                    txtSerieDocDet.Text = String.Empty
    '                End If
    '                If (Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Nro_Doc").Value) Or dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Nro_Doc").Value <> String.Empty) Then
    '                    txtNumDocDet.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Nro_Doc").Value
    '                Else
    '                    txtNumDocDet.Text = String.Empty
    '                End If
    '                If Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("FechaDoc").Value) Then
    '                    dtpFechaDocDet.Value = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("FechaDoc").Value
    '                Else
    '                    dtpFechaDocDet.Enabled = False
    '                End If
    '                If Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Caduca_Fecha").Value) Then
    '                    dtpFechaVcmtoDet.Value = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Caduca_Fecha").Value
    '                Else
    '                    dtpFechaVcmtoDet.Enabled = False
    '                End If
    '                If (Not IsDBNull(dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Area").Value) Or dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Area").Value <> String.Empty) Then
    '                    txtAreaDet.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Area").Value
    '                Else
    '                    txtAreaDet.Text = String.Empty
    '                End If
    '                txtDebe.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Debe").Value
    '                txtHaber.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Haber").Value
    '                txtGlosaDet.Text = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("ComentarioCab").Value

    '                Dim Text As Object
    '                For Each Text In gbRegistro.Controls
    '                    If TypeOf Text Is TextBox Then
    '                        Text.enabled = True
    '                    End If
    '                Next
    '                dtpFechaDocDet.Enabled = True
    '                dtpFechaVcmtoDet.Enabled = True
    '                cboTipoDocDet.Enabled = True
    '                txtSecuencia.Enabled = False
    '                TabRegGeneral.SelectedIndex = 1
    '                BolEditReg = True
    '                btnFiltro.Enabled = False
    '                mtbCuenta.Focus()
    '                Call Al_HacerNuevo()
    '            End If
    '        Catch ex As Exception
    '            MsgBox(ex.Message)
    '        End Try

    '    End Sub

    '    Private Sub TabRegGeneral_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabRegGeneral.SelectedIndexChanged
    '        If TabRegGeneral.SelectedIndex = 0 Then
    '            If cboMoneda.SelectedValue = cboMonedaAsiento.SelectedValue Then
    '                btnNuevoDet.Enabled = True
    '                btnGrabar.Enabled = False
    '                btnEditar.Enabled = True
    '                btnCancelar.Enabled = False
    '                btnEliminar.Enabled = True
    '                btnGenerar.Enabled = True
    '            Else
    '                btnNuevoDet.Enabled = False
    '                btnGrabar.Enabled = False
    '                btnEditar.Enabled = False
    '                btnCancelar.Enabled = False
    '                btnEliminar.Enabled = False
    '                btnGenerar.Enabled = False

    '            End If
    '        Else
    '            If btnGenerar.Enabled = True Then
    '                If cboMoneda.SelectedValue = cboMonedaAsiento.SelectedValue Then
    '                    btnNuevoDet.Enabled = False
    '                    btnGrabar.Enabled = True
    '                    btnEditar.Enabled = False
    '                    btnCancelar.Enabled = True
    '                    btnEliminar.Enabled = False
    '                    btnGenerar.Enabled = False
    '                Else
    '                    btnNuevoDet.Enabled = False
    '                    btnGrabar.Enabled = False
    '                    btnEditar.Enabled = False
    '                    btnCancelar.Enabled = False
    '                    btnEliminar.Enabled = False
    '                    btnGenerar.Enabled = False
    '                End If
    '            End If
    '        End If
    '    End Sub

    '    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    '        Dim DrFilaELiminar() As DataRow
    '        Dim DrFilaDestino() As DataRow
    '        Dim DrFilaTransferencias() As DataRow
    '        Dim CodSecuencia As String
    '        Dim StrCodCta As String = String.Empty
    '        Dim StrAbrevMonDet As String = cFuncGen.AbreviaturaMoneda(cboMonedaAsiento.SelectedValue)

    '        If dgvAsiento.Rows.Count > 0 Then
    '            CodSecuencia = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Secuencia").Value
    '            StrCodCta = dgvAsiento.Rows(dgvAsiento.CurrentRow.Index).Cells("Codigo_Cuenta").Value

    '            For z As Integer = 0 To DTMonedas.Rows.Count - 1
    '                Dim NomTabla As String = "TmpAsiento" & DTMonedas.Rows(z).Item("Abreviado")
    '                Dim NomTablaDestino As String = "TmpAsientoDestino" & DTMonedas.Rows(z).Item("Abreviado")
    '                Dim NomTablaTransferencias As String = "TmpTransferencias" & DTMonedas.Rows(z).Item("Abreviado")
    '                DrFilaELiminar = DstAsientos.Tables(NomTabla).Select("Secuencia='" & CodSecuencia & "'")
    '                DrFilaDestino = DstAsientos.Tables(NomTablaDestino).Select("CtaConDestino='" & StrCodCta & "'")
    '                DrFilaTransferencias = DstAsientos.Tables(NomTablaTransferencias).Select("CtaConDestino='" & StrCodCta & "'")

    '                If DrFilaELiminar.Length > 0 Then
    '                    DstAsientos.Tables(NomTabla).Rows.Remove(DrFilaELiminar(0))
    '                    Call Gen_Secuencia_DesElim(DstAsientos.Tables(NomTabla))
    '                    If cboMonedaAsiento.SelectedValue = DTMonedas.Rows(z).Item("Codigo") Then
    '                        Call Sumar_Importes(DstAsientos.Tables(NomTabla))
    '                    End If
    '                End If

    '                If DrFilaDestino.Length > 0 Then
    '                    For Rows As Integer = 0 To DrFilaDestino.Length - 1
    '                        DstAsientos.Tables(NomTablaDestino).Rows.Remove(DrFilaDestino(Rows))
    '                    Next
    '                End If

    '                If DrFilaTransferencias.Length > 0 Then
    '                    For Rows As Integer = 0 To DrFilaTransferencias.Length - 1
    '                        DstAsientos.Tables(NomTablaTransferencias).Rows.Remove(DrFilaTransferencias(Rows))
    '                    Next
    '                End If
    '            Next
    '        End If

    '        Call Sumar_Importes(DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMonedaAsiento.SelectedValue)))
    '        Call CalcularDiferencia()

    '    End Sub

    '    Public Sub Gen_Secuencia_DesElim(ByVal TABLA As DataTable)
    '        For x As Integer = 0 To TABLA.Rows.Count - 1
    '            TABLA.Rows(x).Item("Secuencia") = Microsoft.VisualBasic.Right(New String("0", 5) + Convert.ToString(x + 1), 5)
    '        Next
    '    End Sub

    '    Private Sub mtbCuenta_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles mtbCuenta.TextChanged
    '        Dim DescCta As String
    '        cCapaReCompConConv = New clsRegComprobConConv

    '        If mtbCuenta.Text = String.Empty Then
    '            lblDescripCuenta.Text = String.Empty
    '        Else
    '            DescCta = cCapaReCompConConv.Descrip_Cuenta(sEmpresa, mtbCuenta.Text.Trim)
    '            lblDescripCuenta.Text = DescCta
    '        End If
    '    End Sub

    '    Private Sub txtAnexoDet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAnexoDet.TextChanged
    '        Dim DescAnexo As String
    '        cCapaReCompConConv = New clsRegComprobConConv

    '        If txtAnexoDet.Text = String.Empty Then
    '            lblDescAnexDet.Text = String.Empty
    '        Else
    '            DescAnexo = cCapaReCompConConv.Descrip_Anexo(sEmpresa, txtAnexoDet.Text, StrCodTipoAnex)
    '            lblDescAnexDet.Text = DescAnexo
    '        End If
    '    End Sub

    '    Private Function CuadraAsiento(ByVal DSET As DataSet, ByVal TablaImportes As DataTable) As Integer
    '        Dim IntResult As Integer = 0

    '        IntResult = cCapaReCompConConv.CuadraAsientoForzado(DSET, TablaImportes, BytFormatConta)
    '        If IntResult = 1 Then
    '            Call Sumar_Importes(DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMonedaAsiento.SelectedValue)))
    '        Else
    '            MessageBox.Show("Ocurrio un Problema al tratar de cuadrar Montos", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Return 0
    '        End If
    '        Return 1
    '    End Function

    '    Private Sub btnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
    '        Dim StrNumVoucher As String
    '        Dim TablaImportes As New DataTable
    '        Dim DTtiposCambio As New DataTable
    '        cCapaReCompConConv = New clsRegComprobConConv

    '        DTCambioActual = cCapaReCompConConv.TipoCambio_AllMonedas(sEmpresa, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"))

    '        If (lblDebe.Text = "0.000" And lblHaber.Text = "0.000") Then
    '            MessageBox.Show("Los montos del Debe y Haber no pueden ser CERO, Verifique", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Exit Sub
    '        ElseIf lblDebe.Text <> lblHaber.Text Then
    '            MessageBox.Show("Los montos del Debe y Haber no cuadran, Verifique", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Exit Sub
    '        End If

    '        TablaImportes = cCapaReCompConConv.ImportesAsientos(DstAsientos, DTMonedas)
    '        If CuadraAsiento(DstAsientos, TablaImportes) = 0 Then
    '            Exit Sub
    '        Else
    '            TablaImportes = cCapaReCompConConv.ImportesAsientos(DstAsientos, DTMonedas)
    '        End If

    '        If TablaImportes.Rows.Count = 0 Or TablaImportes Is Nothing Then
    '            MessageBox.Show("Los montos del Debe y Haber no cuadran, Verifique", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '            Exit Sub
    '        End If

    '        '*********** Generando Asientos de Transferencias y Destinos ***********'
    '        Call GenTransferencias()

    '        DTtiposCambio = Devuelve_ValorTC(cboTipoCambio.SelectedValue, dgvEpecial, Format(dtpFechaTCambio.Value, "dd/MM/yyyy"))

    '        StrNumVoucher = cCapaReCompConConv.Insertar_Voucher_CompConConversion(sEmpresa, cboSubdiario.SelectedValue, Format(dtpFechaVoucher.Value, "MM"), cboMoneda.SelectedValue, _
    '                        Format(dtpFechaVoucher.Value, "yyyy"), Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), CDbl(lblDebe.Text), CDbl(lblHaber.Text), Val(txtValorCambio.Text), _
    '                        Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), cboTipoCambio.SelectedValue, Trim(txtGlosaCab.Text), 0, STRmonSist, _
    '                        cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue), sUser, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), 1, NomPc.ComputerName, DstAsientos, DTMonedas, DTCambioActual, TablaImportes, _
    '                         DTtiposCambio, Format(dtpFechaTCambio.Value, "dd/MM/yyyy"))

    '        If StrNumVoucher <> String.Empty Then
    '            txtNumComp.Text = StrNumVoucher
    '            MessageBox.Show("Se Genero el Comprobante: " & StrNumVoucher, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)

    '            btnGenerar.Enabled = False
    '            btnNuevoDet.Enabled = False
    '            btnGrabar.Enabled = False
    '            btnEditar.Enabled = False
    '            btnCancelar.Enabled = False
    '            btnEliminar.Enabled = False
    '            gbRegistro.Enabled = False
    '            TabRegGeneral.Enabled = False
    '            btnNuevoCab.Focus()
    '        Else
    '            MessageBox.Show("Ocurrio un Error al generar el voucher de venta", "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '        End If
    '    End Sub

    '    Private Sub GenTransferencias()
    '        Dim i As Integer
    '        Dim iFila As Integer
    '        Dim sTabla As String
    '        Dim IsGasto As String
    '        Dim sCenCosto As String
    '        Dim StrCodCuenta As String
    '        Dim DblImporteTransf As Double
    '        Dim StrArea As String

    '        DblImporteTransf = 0
    '        i = 0
    '        Do While DTMonedas.Rows.Count > i
    '            sTabla = "TmpAsiento" & DTMonedas.Rows(i).Item("Abreviado")
    '            iFila = 0
    '            IsGasto = String.Empty

    '            Do While DstAsientos.Tables(sTabla).Rows.Count > iFila
    '                StrCodCuenta = DstAsientos.Tables(sTabla).Rows(iFila).Item("Codigo_Cuenta")
    '                sCenCosto = IIf(IsDBNull(DstAsientos.Tables(sTabla).Rows(iFila).Item("ID_CentroCosto")), String.Empty, DstAsientos.Tables(sTabla).Rows(iFila).Item("ID_CentroCosto"))
    '                IsGasto = cCapaReCompConConv.Devuelve_TipoCtaCont(sEmpresa, sPeriodo, StrCodCuenta)
    '                StrArea = IIf(IsDBNull(DstAsientos.Tables(sTabla).Rows(iFila).Item("Area")), String.Empty, DstAsientos.Tables(sTabla).Rows(iFila).Item("Area"))

    '                If sCenCosto <> String.Empty And IsGasto = "V" Then
    '                    If DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).Rows(iFila).Item("Debe") > 0 Then
    '                        DblImporteTransf = DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).Rows(iFila).Item("Debe")
    '                    ElseIf DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).Rows(iFila).Item("Haber") > 0 Then
    '                        DblImporteTransf = DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).Rows(iFila).Item("Haber")
    '                    End If

    '                    '********** Genero Asiento de Destinos **********'
    '                    GenerarAsientosDestinos(sEmpresa, StrCodCuenta, StrArea, sCenCosto, _
    '                                 sPeriodo, DTMonedas.Rows(i).Item("Abreviado"), DblImporteTransf)

    '                    '********** Genero Asiento de Transferencias **********'
    '                    If sCenCosto <> String.Empty Then
    '                        GenerarAsientoTransferencias(sEmpresa, sCenCosto, sPeriodo, _
    '                                         DTMonedas.Rows(i).Item("Abreviado"), StrArea, StrCodCuenta, DblImporteTransf)
    '                    End If
    '                End If
    '                iFila += 1
    '            Loop
    '            i += 1
    '        Loop
    '    End Sub

    '    Private Function Devuelve_ValorTC(ByVal TCambio As String, ByVal GrillaEspecial As System.Windows.Forms.DataGridView, ByVal dtpDeAcuerdoFecha As Date) As DataTable
    '        Dim TablaTC As New DataTable
    '        Dim DrTC As DataRow
    '        Dim ValorTC As Double = 0

    '        TablaTC.Columns.Clear()
    '        TablaTC.Columns.Add("Moneda")
    '        TablaTC.Columns.Add("MontoTC")

    '        For x As Integer = 0 To DTMonedas.Rows.Count - 1
    '            DrTC = TablaTC.NewRow
    '            If DTMonedas.Rows(x).Item("Abreviado") <> STRmonSist Then
    '                Select Case TCambio
    '                    Case "1"    'Compra
    '                        ValorTC = cFuncGen.DevuelveMontoTCxMoneda(sEmpresa, dtpFechaVoucher.Value, DTMonedas.Rows(x).Item("Abreviado"), TCambio)
    '                        DrTC("Moneda") = DTMonedas.Rows(x).Item("Abreviado")
    '                        DrTC("MontoTC") = ValorTC
    '                    Case "2"    'Venta
    '                        ValorTC = cFuncGen.DevuelveMontoTCxMoneda(sEmpresa, dtpFechaVoucher.Value, DTMonedas.Rows(x).Item("Abreviado"), TCambio)
    '                        DrTC("Moneda") = DTMonedas.Rows(x).Item("Abreviado")
    '                        DrTC("MontoTC") = ValorTC
    '                    Case "3"    'Especial
    '                        For i As Integer = 0 To GrillaEspecial.Rows.Count - 1
    '                            DrTC = TablaTC.NewRow
    '                            DrTC("Moneda") = GrillaEspecial.Rows(i).Cells("Moneda").Value
    '                            DrTC("MontoTC") = GrillaEspecial.Rows(i).Cells("Valor").Value
    '                            TablaTC.Rows.Add(DrTC)
    '                        Next
    '                        Exit For
    '                    Case "4"    'De Acuerdo a Fecha
    '                        ValorTC = cFuncGen.DevuelveMontoTCxMoneda(sEmpresa, dtpDeAcuerdoFecha, DTMonedas.Rows(x).Item("Abreviado"), Str_CoV)
    '                        DrTC("Moneda") = DTMonedas.Rows(x).Item("Abreviado")
    '                        DrTC("MontoTC") = ValorTC
    '                End Select
    '                TablaTC.Rows.Add(DrTC)
    '            End If
    '        Next
    '        Return TablaTC
    '    End Function

    '    Private Sub dtpFechaDocDet_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpFechaDocDet.KeyPress
    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                If dtpFechaVcmtoDet.Enabled = True Then
    '                    dtpFechaVcmtoDet.Focus()
    '                ElseIf txtAreaDet.Enabled = True Then
    '                    txtAreaDet.Focus()
    '                Else
    '                    txtDebe.Focus()
    '                End If
    '        End Select
    '    End Sub

    '    Private Sub dtpFechaVcmtoDet_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtpFechaVcmtoDet.KeyPress
    '        Select Case Asc(e.KeyChar)
    '            Case 13
    '                If txtAreaDet.Enabled = True Then
    '                    txtAreaDet.Focus()
    '                Else
    '                    txtDebe.Focus()
    '                End If
    '        End Select
    '    End Sub

    '    Private Sub txtDebe_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDebe.LostFocus
    '        Try
    '            Dim DblDebe As Double = IIf(txtDebe.Text = String.Empty, 0, txtDebe.Text)
    '            txtDebe.Text = Format(DblDebe, "#,##0.000")
    '            If DblDebe > 0 Then txtHaber.Enabled = False
    '        Catch ex As Exception

    '        End Try
    '    End Sub

    '    Private Sub txtDebe_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDebe.TextChanged
    '        If txtDebe.Text.Length = 0 Then
    '            txtHaber.Text = String.Empty
    '            txtHaber.Enabled = True
    '        End If
    '    End Sub

    '    Private Sub txtHaber_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHaber.LostFocus
    '        Try
    '            Dim DblHaber As Double = IIf(txtHaber.Text = String.Empty, 0, txtHaber.Text)
    '            txtHaber.Text = Format(DblHaber, "#,##0.000")
    '            If DblHaber > 0 Then txtDebe.Enabled = False
    '        Catch ex As Exception

    '        End Try
    '    End Sub

    '    Private Sub txtHaber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHaber.TextChanged
    '        If txtHaber.Text.Length = 0 Then
    '            txtDebe.Text = String.Empty
    '            txtDebe.Enabled = True
    '        End If
    '    End Sub

    '    Private Sub txtAreaDet_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAreaDet.DoubleClick
    '        ObjAyuda = New Myhelp.ClsAyuda
    '        With ObjAyuda
    '            .strSERVER = STRSERVER
    '            .strBASEDATOS = STRBASEDATOS
    '            .strTITULOAYUDA = "Listado de Areas"
    '            .IntOPCION = 12
    '            .strCODEMPRESA = sEmpresa
    '            .StrPERIODO = sPeriodo
    '            .strQUERY = "Contabilidad.UspAyuda_Select"
    '            .Mostrar()
    '            If .drFILA Is Nothing Then
    '                Exit Sub
    '            Else
    '                txtAreaDet.Text = Trim(.drFILA.Item(0).ToString)
    '                lblDescAnexDet.Text = Trim(.drFILA.Item(1).ToString)
    '            End If
    '        End With
    '    End Sub

    '    Private Sub txtAreaDet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtAreaDet.KeyDown
    '        Select Case e.KeyCode
    '            Case Keys.F1
    '                ObjAyuda = New Myhelp.ClsAyuda
    '                With ObjAyuda
    '                    .strSERVER = STRSERVER
    '                    .strBASEDATOS = STRBASEDATOS
    '                    .strTITULOAYUDA = "Listado de Areas"
    '                    .IntOPCION = 12
    '                    .strCODEMPRESA = sEmpresa
    '                    .StrPERIODO = sPeriodo
    '                    .strQUERY = "Contabilidad.UspAyuda_Select"
    '                    .Mostrar()
    '                    If .drFILA Is Nothing Then
    '                        Exit Sub
    '                    Else
    '                        txtAreaDet.Text = Trim(.drFILA.Item(0).ToString)
    '                        lblDescAreaDet.Text = Trim(.drFILA.Item(1).ToString)
    '                    End If
    '                End With
    '        End Select
    '    End Sub

    '    Private Sub txtAreaDet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAreaDet.TextChanged

    '        If txtAreaDet.Text = String.Empty Then
    '            lblDescAreaDet.Text = String.Empty
    '        Else
    '            lblDescAreaDet.Text = cCapaReCompConConv.Devolver_Desc_Area(sEmpresa, Trim(txtAreaDet.Text))
    '        End If
    '    End Sub

    '    Private Sub txtCCostoDet_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCCostoDet.DoubleClick
    '        ObjAyuda = New Myhelp.ClsAyuda
    '        With ObjAyuda
    '            .strSERVER = STRSERVER
    '            .strBASEDATOS = STRBASEDATOS
    '            .strTITULOAYUDA = "Listado de Centros de Costo"
    '            .IntOPCION = 13
    '            .strCODEMPRESA = sEmpresa
    '            .StrPERIODO = sPeriodo
    '            .strQUERY = "Contabilidad.UspAyuda_Select"
    '            .Mostrar()
    '            If .drFILA Is Nothing Then
    '                Exit Sub
    '            Else
    '                txtCCostoDet.Text = Trim(.drFILA.Item(0).ToString)
    '                lblDescCCostoDet.Text = Trim(.drFILA.Item(1).ToString)
    '            End If
    '        End With
    '    End Sub

    '    Private Sub txtCCostoDet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCCostoDet.KeyDown
    '        Select Case e.KeyCode
    '            Case Keys.F1
    '                ObjAyuda = New Myhelp.ClsAyuda
    '                With ObjAyuda
    '                    .strSERVER = STRSERVER
    '                    .strBASEDATOS = STRBASEDATOS
    '                    .strTITULOAYUDA = "Listado de Centros de Costo"
    '                    .IntOPCION = 13
    '                    .strCODEMPRESA = sEmpresa
    '                    .StrPERIODO = sPeriodo
    '                    .strQUERY = "Contabilidad.UspAyuda_Select"
    '                    .Mostrar()
    '                    If .drFILA Is Nothing Then
    '                        Exit Sub
    '                    Else
    '                        txtCCostoDet.Text = Trim(.drFILA.Item(0).ToString)
    '                        lblDescCCostoDet.Text = Trim(.drFILA.Item(1).ToString)
    '                    End If
    '                End With
    '        End Select
    '    End Sub

    '    Private Sub txtCCostoDet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCCostoDet.TextChanged
    '        If txtCCostoDet.Text = String.Empty Then
    '            lblDescCCostoDet.Text = String.Empty
    '        Else
    '            lblDescCCostoDet.Text = cCapaReCompConConv.Devolver_Desc_CenCosto(sEmpresa, Trim(txtCCostoDet.Text))
    '        End If
    '    End Sub

    '    Private Sub dgvEpecial_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEpecial.CellEndEdit
    '        If IsDBNull(dgvEpecial.Item("Valor", dgvEpecial.CurrentRow.Index).Value) Then dgvEpecial.Item("Valor", dgvEpecial.CurrentRow.Index).Value = 0
    '        Dim dblValor As Double = dgvEpecial.Item("Valor", dgvEpecial.CurrentRow.Index).Value
    '        dgvEpecial.Item("Valor", dgvEpecial.CurrentRow.Index).Value = Format(Math.Round(dblValor, 3), "#,##0.000")

    '    End Sub

    '    Private Sub dgvEpecial_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvEpecial.EditingControlShowing
    '        Try
    '            'referencia a la celda   
    '            Dim validar As TextBox = CType(e.Control, TextBox)
    '            'agregar el controlador de eventos para el KeyPress   
    '            AddHandler validar.KeyPress, AddressOf validar_Keypress
    '        Catch ex As Exception
    '        End Try
    '    End Sub

    '    Private Sub validar_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)

    '        Dim columna As Integer = dgvEpecial.CurrentCell.ColumnIndex
    '        If columna = 2 Then
    '            Dim caracter As Char = e.KeyChar
    '            ' referencia a la celda   
    '            Dim txt As TextBox = CType(sender, TextBox)
    '            ' comprobar si es un n�mero con isNumber, si es el backspace, si el caracter   
    '            ' es el separador decimal, y que no contiene ya el separador   
    '            If (Char.IsNumber(caracter)) Or (caracter = ChrW(Keys.Back)) Or (caracter = ".") And (txt.Text.Contains(".") = False) Then
    '                e.Handled = False
    '            Else
    '                e.Handled = True
    '            End If
    '        End If
    '    End Sub

    '    Private Sub dgvEpecial_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvEpecial.KeyDown
    '        Select Case e.KeyCode
    '            Case Keys.Escape
    '                gbEspecial.Visible = False
    '                cboTipoCambio.Focus()
    '        End Select
    '    End Sub

    '    Private Sub btnCerrarEspecial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrarEspecial.Click
    '        gbEspecial.Visible = False
    '        cboTipoCambio.Focus()
    '    End Sub

    '    Private Sub BeButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BeButton1.Click
    '        gbFechaTCambio.Visible = False
    '        dtpFechaTCambio.Focus()
    '    End Sub

    '    'Private Function GenerarAsientosDestinos(ByVal CodEmpresa As String, ByVal CodCuenta As String, ByVal Perido As String, ByVal pMoneda As String, ByVal CtaDestino As String) As Integer
    '    '    Dim DrDestinos As DataRow
    '    '    Dim myTC As Single = 0
    '    '    Dim TCvoucher As Double = 0
    '    '    Dim MontoDestino As Double = 0
    '    '    Dim StrTabla As String = "TmpAsientoDestino" & pMoneda
    '    '    Dim DrFilaDestino As DataRow
    '    '    Dim Area As String = String.Empty
    '    '    Dim StrMonCab As String = cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)

    '    '    DrDestinos = cCapaReCompConConv.Cuenta_Destino(CodCuenta, sEmpresa, Perido)

    '    '    If DrDestinos Is Nothing Then Return 0

    '    '    If CDbl(IIf(txtDebe.Text = String.Empty, 0, txtDebe.Text)) > 0 Then
    '    '        MontoDestino = CDbl(IIf(txtDebe.Text = String.Empty, 0, txtDebe.Text))
    '    '        D_o_H = "H"
    '    '        D_o_H = "D"
    '    '    End If
    '    '    If txtAreaDet.Text <> String.Empty Then
    '    '        Area = Trim(txtAreaDet.Text)
    '    '    End If

    '    '    'D_o_H = Asiento_Segun_Doc(DsetDatos.Tables("Parametros_Compra"), IIf(cboTipoDocDet.SelectedValue = Nothing, "", cboTipoDocDet.SelectedValue))

    '    '    Try
    '    '        For z As Integer = 0 To DrDestinos.ItemArray.Length - 1
    '    '            DrFilaDestino = DstAsientos.Tables(StrTabla).NewRow
    '    '            DrFilaDestino("Secuencia") = String.Empty 'Generar_Secuencia(DstAsientos.Tables(StrTabla))
    '    '            DrFilaDestino("Codigo_Cuenta") = DrDestinos(z)  'Ingresando Ctas. Cargo y Abono
    '    '            DrFilaDestino("Codigo_Anexo") = String.Empty
    '    '            DrFilaDestino("Cod_Tipo_Anexo") = String.Empty
    '    '            DrFilaDestino("Tipo_Doc") = String.Empty
    '    '            DrFilaDestino("Serie_Doc") = String.Empty
    '    '            DrFilaDestino("Nro_Doc") = String.Empty
    '    '            DrFilaDestino("MonedaTrans") = pMoneda

    '    '            If StrMonCab = STRmonSist Then
    '    '                If cboTipoCambio.SelectedValue = "3" Then
    '    '                    TCvoucher = 1
    '    '                Else
    '    '                    TCvoucher = Val(txtValorCambio.Text)
    '    '                End If
    '    '            Else
    '    '                If cboTipoCambio.SelectedValue = "3" Then
    '    '                    TCvoucher = Devuelve_TC_Especial(dgvEpecial, cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '    '                Else
    '    '                    TCvoucher = txtValorCambio.Text
    '    '                End If
    '    '            End If

    '    '            If pMoneda = STRmonSist Then
    '    '                myTC = 1
    '    '                'TCvoucher = Val(txtValorCambio.Text)
    '    '                DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '    '                DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")

    '    '                If z = 0 Then   'Si es cargo
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    End If

    '    '                Else            'Si no Abono
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    End If

    '    '                End If
    '    '            Else
    '    '                If dtpFechaTCambio.Visible = False Then
    '    '                    If (cboTipoCambio.Text = "ESPECIAL" Or cboTipoCambio.SelectedValue = "3") Then
    '    '                        myTC = Devuelve_TC_Especial(dgvEpecial, pMoneda)
    '    '                    Else
    '    '                        myTC = cCapaReCompConConv.TipoCambio_Actual(sEmpresa, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), cFuncGen.CodigoMoneda(pMoneda), cboTipoCambio.Text)
    '    '                    End If
    '    '                Else
    '    '                    If gbTipoConversion.Visible = True Then
    '    '                        myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, IIf(rbCompra.Checked = True, "Compra", "Venta"))
    '    '                    Else
    '    '                        myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, Str_CoV)
    '    '                    End If
    '    '                End If

    '    '                If myTC = 0 Then
    '    '                    MessageBox.Show("No ingreso Tipo de Cambio para la Moneda en " & pMoneda, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    '                    Return 0
    '    '                End If

    '    '                DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '    '                DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")

    '    '                If z = 0 Then   'Si es Cargo
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    End If

    '    '                Else            'Si no Abono
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    End If

    '    '                End If
    '    '            End If

    '    '            DrFilaDestino("FechaDoc") = Format(dtpFechaDocDet.Value, "dd/MM/yyyy")
    '    '            DrFilaDestino("Caduca_Fecha") = Format(dtpFechaVcmtoDet.Value, "dd/MM/yyyy")
    '    '            DrFilaDestino("Ind_Trans_Dif") = "0"
    '    '            DrFilaDestino("Gen_Cta_Destino") = "1"
    '    '            DrFilaDestino("ComentarioCab") = Trim(txtGlosaDet.Text)
    '    '            DrFilaDestino("ID_CentroCosto") = Trim(txtCCostoDet.Text)
    '    '            DrFilaDestino("Area") = Area
    '    '            If pMoneda = STRmonSist Then
    '    '                DrFilaDestino("Moneda_Sist") = "1"
    '    '            Else
    '    '                DrFilaDestino("Moneda_Sist") = "0"
    '    '            End If
    '    '            If pMoneda = cboMoneda.SelectedValue Then
    '    '                DrFilaDestino("DocTransact") = "1"
    '    '            Else
    '    '                DrFilaDestino("DocTransact") = "0"
    '    '            End If
    '    '            DrFilaDestino("MontoOriginal") = 0
    '    '            DrFilaDestino("ExisteDoc") = "0"
    '    '            DrFilaDestino("CtaConDestino") = CtaDestino

    '    '            DstAsientos.Tables(StrTabla).Rows.Add(DrFilaDestino)
    '    '        Next
    '    '    Catch ex As Exception
    '    '        MessageBox.Show(ex.Message, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    '        Return 0
    '    '    End Try
    '    '    Return 1
    '    'End Function

    '    'Private Function GenerarAsientoTransferencias(ByVal Empresa As String, ByVal CodCenCosto As String, ByVal Perido As String, ByVal pMoneda As String, ByVal CtaTransferencia As String) As Integer
    '    '    Dim DrDestinos As DataRow
    '    '    Dim DrTransferencias As DataRow
    '    '    Dim myTC As Single = 0
    '    '    Dim TCvoucher As Double = 0
    '    '    Dim MontoDestino As Double = 0
    '    '    Dim StrTabla As String = "TmpTransferencias" & pMoneda
    '    '    Dim DrFilaDestino As DataRow
    '    '    Dim Area As String = String.Empty

    '    '    DrDestinos = cCapaReCompConConv.GenerarTransferenciaFull(CodCenCosto, sEmpresa)
    '    '    DrTransferencias = cCapaReCompConConv.GenerarTransferencia(CodCenCosto, sEmpresa)
    '    '    If DrTransferencias Is Nothing Then Return 0

    '    '    If CDbl(IIf(txtDebe.Text = String.Empty, 0, txtDebe.Text)) > 0 Then
    '    '        MontoDestino = CDbl(IIf(txtDebe.Text = String.Empty, 0, txtDebe.Text))
    '    '    ElseIf CDbl(IIf(txtHaber.Text = String.Empty, 0, txtHaber.Text)) > 0 Then
    '    '        MontoDestino = CDbl(IIf(txtHaber.Text = String.Empty, 0, txtHaber.Text))
    '    '    End If
    '    '    If txtAreaDet.Text <> String.Empty Then
    '    '        Area = Trim(txtAreaDet.Text)
    '    '    End If

    '    '    If CDbl(IIf(txtDebe.Text = String.Empty, 0, txtDebe.Text)) > 0 Then
    '    '        MontoDestino = CDbl(IIf(txtDebe.Text = String.Empty, 0, txtDebe.Text))
    '    '        D_o_H = "H"
    '    '    ElseIf CDbl(IIf(txtHaber.Text = String.Empty, 0, txtHaber.Text)) > 0 Then
    '    '        MontoDestino = CDbl(IIf(txtHaber.Text = String.Empty, 0, txtHaber.Text))
    '    '        D_o_H = "D"
    '    '    End If

    '    '    'D_o_H = Asiento_Segun_Doc(DsetDatos.Tables("Parametros_Compra"), cboTipoDocDet.SelectedValue)

    '    '    Try
    '    '        For z As Integer = 0 To DrTransferencias.ItemArray.Length - 1
    '    '            DrFilaDestino = DstAsientos.Tables(StrTabla).NewRow
    '    '            DrFilaDestino("Secuencia") = String.Empty 'Generar_Secuencia(DstAsientos.Tables(StrTabla))
    '    '            DrFilaDestino("Codigo_Cuenta") = DrTransferencias(z)  'Ingresando Ctas. Cargo y Abono
    '    '            DrFilaDestino("Codigo_Anexo") = String.Empty
    '    '            DrFilaDestino("Cod_Tipo_Anexo") = String.Empty
    '    '            DrFilaDestino("Tipo_Doc") = String.Empty
    '    '            DrFilaDestino("Serie_Doc") = String.Empty
    '    '            DrFilaDestino("Nro_Doc") = String.Empty
    '    '            DrFilaDestino("MonedaTrans") = pMoneda

    '    '            If cboMoneda.SelectedValue = STRmonSist Then
    '    '                If cboTipoCambio.SelectedValue = "3" Then
    '    '                    TCvoucher = 1
    '    '                Else
    '    '                    TCvoucher = Val(txtValorCambio.Text)
    '    '                End If
    '    '            Else
    '    '                If cboTipoCambio.SelectedValue = "3" Then
    '    '                    TCvoucher = Devuelve_TC_Especial(dgvEpecial, cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '    '                Else
    '    '                    TCvoucher = txtValorCambio.Text
    '    '                End If
    '    '            End If

    '    '            If pMoneda = STRmonSist Then
    '    '                myTC = 1
    '    '                'TCvoucher = Val(txtValorCambio.Text)
    '    '                DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '    '                DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")

    '    '                If z = 0 Then   'Si es cargo
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    End If

    '    '                Else            'Si no Abono
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    End If

    '    '                End If
    '    '            Else
    '    '                If dtpFechaTCambio.Visible = False Then
    '    '                    If (cboTipoCambio.Text = "ESPECIAL" Or cboTipoCambio.SelectedValue = "3") Then
    '    '                        myTC = Devuelve_TC_Especial(dgvEpecial, pMoneda)
    '    '                    Else
    '    '                        myTC = cCapaReCompConConv.TipoCambio_Actual(sEmpresa, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), cFuncGen.CodigoMoneda(pMoneda), cboTipoCambio.Text)
    '    '                    End If
    '    '                Else
    '    '                    If gbTipoConversion.Visible = True Then
    '    '                        myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, IIf(rbCompra.Checked = True, "Compra", "Venta"))
    '    '                    Else
    '    '                        myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, Str_CoV)
    '    '                    End If
    '    '                End If

    '    '                If myTC = 0 Then
    '    '                    MessageBox.Show("No ingreso Tipo de Cambio para la Moneda en " & pMoneda, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '    '                    Return 0
    '    '                End If

    '    '                DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '    '                DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")

    '    '                If z = 0 Then   'Si es Cargo
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    End If

    '    '                Else            'Si no Abono
    '    '                    If D_o_H = "H" Then
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '    '                            DrFilaDestino("Haber") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                        End If
    '    '                    Else
    '    '                        If pMoneda = cboMoneda.SelectedValue Then
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        Else
    '    '                            DrFilaDestino("Debe") = Math.Round(MontoDestino * TCvoucher / myTC, BytFormatConta)
    '    '                            DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '    '                        End If
    '    '                    End If

    '    '                End If
    '    '            End If

    '    '            DrFilaDestino("FechaDoc") = Format(dtpFechaDocDet.Value, "dd/MM/yyyy")
    '    '            DrFilaDestino("Caduca_Fecha") = Format(dtpFechaVcmtoDet.Value, "dd/MM/yyyy")
    '    '            DrFilaDestino("Ind_Trans_Dif") = "0"
    '    '            DrFilaDestino("Gen_Cta_Destino") = "1"
    '    '            DrFilaDestino("ComentarioCab") = Trim(txtGlosaDet.Text)
    '    '            DrFilaDestino("ID_CentroCosto") = CodCenCosto
    '    '            DrFilaDestino("Area") = Area
    '    '            If pMoneda = STRmonSist Then
    '    '                DrFilaDestino("Moneda_Sist") = "1"
    '    '            Else
    '    '                DrFilaDestino("Moneda_Sist") = "0"
    '    '            End If
    '    '            If pMoneda = cboMoneda.SelectedValue Then
    '    '                DrFilaDestino("DocTransact") = "1"
    '    '            Else
    '    '                DrFilaDestino("DocTransact") = "0"
    '    '            End If
    '    '            DrFilaDestino("MontoOriginal") = 0
    '    '            DrFilaDestino("ExisteDoc") = "0"
    '    '            DrFilaDestino("CtaConDestino") = CtaTransferencia

    '    '            DstAsientos.Tables(StrTabla).Rows.Add(DrFilaDestino)
    '    '        Next
    '    '    Catch ex As Exception
    '    '        MessageBox.Show(ex.Message, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    '        Return 0
    '    '    End Try
    '    '    Return 1
    '    'End Function

    '    Private Sub LLenarComboDocuments(CodTipoAnexo as String)
    '        Dim DTdocumentos As New DataTable

    '        DTdocumentos = cCapaReCompConConv.LLenaTablaDocumentos(CodTipoAnexo)
    '        With cboTipoDocDet
    '            .DataSource = DTdocumentos
    '            .ValueMember = "Codigo"
    '            .DisplayMember = "Descripcion"
    '        End With
    '    End Sub

    '    Private Sub cboTipoDocDet_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoDocDet.GotFocus
    '        If BolEditReg = False Then
    '            Call LLenarComboDocuments(StrCodTipoAnex)
    '            cboTipoDocDet.SelectedIndex = -1
    '        End If
    '    End Sub

    '    Private Sub btnFiltro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltro.Click
    '        Try
    '            ObjAyudaQuery = New ClsAyudaQuery
    '            cCapaReCompConConv = New clsRegComprobConConv

    '            MontoOrigenDoc = 0

    '            With ObjAyudaQuery
    '                .strSERVER = STRSERVER
    '                .strBASEDATOS = STRBASEDATOS
    '                .strTITULOAYUDA = "Documentos Pendientes"
    '                .strQUERY = cCapaReCompConConv.FiltroDocsPendientes(sEmpresa, sPeriodo, Trim(mtbCuenta.Text), StrTipoAnexo, _
    '                            txtAnexoDet.Text, cboTipoDocDet.SelectedIndex, cboTipoDocDet.SelectedValue)
    '                .MostrarDatos()
    '                If .drFILA Is Nothing Then
    '                    Exit Sub
    '                Else
    '                    If cboTipoDocDet.SelectedIndex = -1 Then
    '                        cboTipoDocDet.SelectedValue = .drFILA(0)
    '                    End If
    '                    txtSerieDocDet.Text = .drFILA(1)
    '                    txtNumDocDet.Text = .drFILA(2)
    '                    dtpFechaDocDet.Value = .drFILA(5)
    '                    If Not IsDBNull(.drFILA(6)) Then
    '                        dtpFechaVcmtoDet.Enabled = True
    '                        dtpFechaVcmtoDet.Value = .drFILA(6)
    '                    Else
    '                        dtpFechaVcmtoDet.Enabled = False
    '                    End If
    '                    MontoOrigenDoc = .drFILA(4)
    '                    MonedaOrigenDoc = .drFILA(3)
    '                End If
    '            End With
    '            dtpFechaDocDet.Focus()
    '        Catch ex As Exception
    '            MsgBox(Convert.ToString(ex.Message))
    '        End Try
    '    End Sub

    '    Private Sub CalcularDiferencia()
    '        Dim DblDebe As Double
    '        Dim DblHaber As Double
    '        If cboMoneda.SelectedIndex <> -1 Then
    '            For x As Integer = 0 To DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).Rows.Count - 1
    '                DblDebe = DblDebe + DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).Rows(x).Item("Debe")
    '                DblHaber = DblHaber + DstAsientos.Tables("TmpAsiento" & cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)).Rows(x).Item("Haber")
    '            Next
    '        End If
    '        lblDiferencia.Text = Format(DblDebe - DblHaber, "#,##0.00")
    '    End Sub

    '    Private Sub txtNumDocDet_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumDocDet.DoubleClick
    '        If cboTipoDocDet.SelectedValue = "CH" Then
    '            cCapaReCompConConv = New clsRegComprobConConv
    '            ObjAyudaQuery = New ClsAyudaQuery

    '            With ObjAyudaQuery
    '                .strSERVER = STRSERVER
    '                .strBASEDATOS = STRBASEDATOS
    '                .strTITULOAYUDA = "Listado de Cheques"
    '                .strQUERY = cCapaReCompConConv.Lista_Cheques(sEmpresa, sPeriodo, Trim(txtAnexoDet.Text), cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '                .MostrarDatos()
    '                If .drFILA Is Nothing Then
    '                    Exit Sub
    '                Else
    '                    txtNumDocDet.Text = .drFILA(0)
    '                End If
    '            End With
    '        End If
    '    End Sub

    '    Private Sub txtNumDocDet_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtNumDocDet.KeyDown
    '        Select Case e.KeyCode
    '            Case Keys.F1
    '                If cboTipoDocDet.SelectedValue = "CH" Then
    '                    cCapaReCompConConv = New clsRegComprobConConv
    '                    ObjAyudaQuery = New ClsAyudaQuery

    '                    With ObjAyudaQuery
    '                        .strSERVER = STRSERVER
    '                        .strBASEDATOS = STRBASEDATOS
    '                        .strTITULOAYUDA = "Listado de Cheques"
    '                        .strQUERY = cCapaReCompConConv.Lista_Cheques(sEmpresa, sPeriodo, Trim(txtAnexoDet.Text), cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '                        .MostrarDatos()
    '                        If .drFILA Is Nothing Then
    '                            Exit Sub
    '                        Else
    '                            txtNumDocDet.Text = .drFILA(0)
    '                        End If
    '                    End With
    '                End If
    '        End Select
    '    End Sub

    '    Private Sub txtSerieDocDet_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSerieDocDet.TextChanged
    '        If txtNumDocDet.Text = String.Empty And txtSerieDocDet.Text = String.Empty Then
    '            cboTipoDocDet.SelectedIndex = -1
    '            dtpFechaDocDet.Value = dtpFechaVoucher.Value
    '            dtpFechaVcmtoDet.Value = dtpFechaVoucher.Value
    '        End If
    '    End Sub

    '    Private Sub txtNumDocDet_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumDocDet.TextChanged
    '        If txtNumDocDet.Text = String.Empty And txtSerieDocDet.Text = String.Empty Then
    '            cboTipoDocDet.SelectedIndex = -1
    '            dtpFechaDocDet.Value = dtpFechaVoucher.Value
    '            dtpFechaVcmtoDet.Value = dtpFechaVoucher.Value
    '        End If
    '    End Sub

    '    Private Function GenerarAsientosDestinos(ByVal CodEmpresa As String, ByVal CodCuenta As String, _
    '                                   ByVal pArea As String, ByVal pCenCosto As String, ByVal Periodo As String, _
    '                                   ByVal pMoneda As String, ByVal MontoTrasnfDestino As Double) As Integer

    '        Dim DrDestinos As DataRow
    '        Dim myTC As Single = 0
    '        Dim TCvoucher As Double = 0
    '        Dim StrTabla As String = "TmpAsientoDestino" & pMoneda
    '        Dim DrFilaDestino As DataRow
    '        'Dim D_o_H As String
    '        Dim StrAbrevMoneda As String = cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)

    '        DrDestinos = cCapaReCompConConv.Cuenta_Destino(CodCuenta, sEmpresa, Periodo)

    '        If DrDestinos Is Nothing Then Return 0

    '        'D_o_H = Asiento_Segun_Doc(DsetDatos.Tables("Parametros_Compra"), cboTipoDocCab.SelectedValue)

    '        Try
    '            For z As Integer = 0 To DrDestinos.ItemArray.Length - 1
    '                DrFilaDestino = DstAsientos.Tables(StrTabla).NewRow
    '                DrFilaDestino("Secuencia") = String.Empty
    '                DrFilaDestino("Codigo_Cuenta") = DrDestinos(z)  'Ingresando Ctas. Cargo y Abono
    '                DrFilaDestino("Codigo_Anexo") = String.Empty
    '                DrFilaDestino("Cod_Tipo_Anexo") = String.Empty
    '                DrFilaDestino("Tipo_Doc") = String.Empty
    '                DrFilaDestino("Serie_Doc") = String.Empty
    '                DrFilaDestino("Nro_Doc") = String.Empty
    '                DrFilaDestino("MonedaTrans") = pMoneda

    '                If StrAbrevMoneda = STRmonSist Then
    '                    If cboTipoCambio.SelectedValue = "3" Then
    '                        TCvoucher = 1
    '                    Else
    '                        TCvoucher = Val(txtValorCambio.Text)
    '                    End If
    '                Else
    '                    If cboTipoCambio.SelectedValue = "3" Then
    '                        TCvoucher = Devuelve_TC_Especial(dgvEpecial, cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '                    Else
    '                        TCvoucher = txtValorCambio.Text
    '                    End If
    '                End If

    '                If pMoneda = STRmonSist Then
    '                    myTC = 1
    '                    DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '                    If cboTipoCambio.SelectedValue = "4" Then
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaTCambio.Value, "dd/MM/yyyy")
    '                    Else
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")
    '                    End If

    '                    If z = 0 Then   'Si es cargo
    '                        DrFilaDestino("Debe") = Math.Round(MontoTrasnfDestino * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '                    Else          'Si no Abono
    '                        DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '                        DrFilaDestino("Haber") = Math.Round(MontoTrasnfDestino * TCvoucher / myTC, BytFormatConta)
    '                    End If

    '                Else

    '                    If dtpFechaTCambio.Visible = False Then
    '                        If (cboTipoCambio.Text = "ESPECIAL" Or cboTipoCambio.SelectedValue = "3") Then
    '                            myTC = Devuelve_TC_Especial(dgvEpecial, pMoneda)
    '                        Else
    '                            myTC = cCapaReCompConConv.TipoCambio_Actual(sEmpresa, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), cFuncGen.CodigoMoneda(pMoneda), cboTipoCambio.Text)
    '                        End If
    '                    Else
    '                        If gbTipoConversion.Visible = True Then
    '                            myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, IIf(rbCompra.Checked = True, "Compra", "Venta"))
    '                        Else
    '                            myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, Str_CoV)
    '                        End If
    '                    End If

    '                    If myTC = 0 Then
    '                        MessageBox.Show("No ingreso Tipo de Cambio para la Moneda en " & pMoneda, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                        Return 0
    '                    End If

    '                    DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '                    If cboTipoCambio.SelectedValue = "4" Then
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaTCambio.Value, "dd/MM/yyyy")
    '                    Else
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")
    '                    End If

    '                    If z = 0 Then   'Si es Cargo
    '                        DrFilaDestino("Debe") = Math.Round(MontoTrasnfDestino * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '                    Else            'Si no Abono
    '                        DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '                        DrFilaDestino("Haber") = Math.Round(MontoTrasnfDestino * TCvoucher / myTC, BytFormatConta)
    '                    End If
    '                End If

    '                DrFilaDestino("FechaDoc") = System.DBNull.Value     'Format(dtpEmision.Value, "dd/MM/yyyy")
    '                DrFilaDestino("Caduca_Fecha") = System.DBNull.Value     'Format(dtpVmto.Value, "dd/MM/yyyy")
    '                DrFilaDestino("Ind_Trans_Dif") = "0"
    '                DrFilaDestino("Gen_Cta_Destino") = "1"
    '                DrFilaDestino("ComentarioCab") = Trim(txtGlosaCab.Text)
    '                DrFilaDestino("ID_CentroCosto") = pCenCosto      'Trim(txtCCostoDet.Text)
    '                DrFilaDestino("Area") = pArea

    '                If pMoneda = STRmonSist Then
    '                    DrFilaDestino("Moneda_Sist") = "1"
    '                Else
    '                    DrFilaDestino("Moneda_Sist") = "0"
    '                End If
    '                If pMoneda = StrAbrevMoneda Then
    '                    DrFilaDestino("DocTransact") = "1"
    '                Else
    '                    DrFilaDestino("DocTransact") = "0"
    '                End If
    '                DrFilaDestino("MontoOriginal") = 0
    '                DrFilaDestino("ExisteDoc") = "0"
    '                DrFilaDestino("CtaConDestino") = CodCuenta

    '                DstAsientos.Tables(StrTabla).Rows.Add(DrFilaDestino)
    '            Next
    '        Catch ex As Exception
    '            MessageBox.Show(ex.Message, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            Return 0
    '        End Try
    '        Return 1
    '    End Function

    '    Private Function GenerarAsientoTransferencias(ByVal Empresa As String, ByVal CodCenCosto As String, _
    '                                           ByVal Perido As String, ByVal pMoneda As String, ByVal pArea As String, _
    '                                           ByVal CodCuentaTransf As String, ByVal MontoTransfDestino As Double) As Integer

    '        Dim DrTransferencias As DataRow
    '        Dim myTC As Single = 0
    '        Dim TCvoucher As Double = 0
    '        Dim StrTabla As String = "TmpTransferencias" & pMoneda
    '        Dim DrFilaDestino As DataRow
    '        'Dim D_o_H As String
    '        Dim StrAbrevMoneda As String = cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue)


    '        DrTransferencias = cCapaReCompConConv.GenerarTransferencia(CodCenCosto, sEmpresa)
    '        If DrTransferencias Is Nothing Then Return 0

    '        'D_o_H = Asiento_Segun_Doc(DsetDatos.Tables("Parametros_Compra"), cboTipoDocCab.SelectedValue)

    '        Try
    '            For z As Integer = 0 To DrTransferencias.ItemArray.Length - 1
    '                DrFilaDestino = DstAsientos.Tables(StrTabla).NewRow
    '                DrFilaDestino("Secuencia") = String.Empty
    '                DrFilaDestino("Codigo_Cuenta") = DrTransferencias(z)  'Ingresando Ctas. Cargo y Abono
    '                DrFilaDestino("Codigo_Anexo") = String.Empty
    '                DrFilaDestino("Cod_Tipo_Anexo") = String.Empty
    '                DrFilaDestino("Tipo_Doc") = String.Empty
    '                DrFilaDestino("Serie_Doc") = String.Empty
    '                DrFilaDestino("Nro_Doc") = String.Empty
    '                DrFilaDestino("MonedaTrans") = pMoneda

    '                If StrAbrevMoneda = STRmonSist Then
    '                    If cboTipoCambio.SelectedValue = "3" Then
    '                        TCvoucher = 1
    '                    Else
    '                        TCvoucher = Val(txtValorCambio.Text)
    '                    End If
    '                Else
    '                    If cboTipoCambio.SelectedValue = "3" Then
    '                        TCvoucher = Devuelve_TC_Especial(dgvEpecial, cFuncGen.AbreviaturaMoneda(cboMoneda.SelectedValue))
    '                    Else
    '                        TCvoucher = txtValorCambio.Text
    '                    End If
    '                End If

    '                If pMoneda = STRmonSist Then

    '                    myTC = 1
    '                    DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '                    If cboTipoCambio.SelectedValue = "4" Then
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaTCambio.Value, "dd/MM/yyyy")
    '                    Else
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")
    '                    End If

    '                    If z = 0 Then   'Si es cargo
    '                        DrFilaDestino("Debe") = Math.Round(MontoTransfDestino * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '                    Else            'Si no Abono
    '                        DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '                        DrFilaDestino("Haber") = Math.Round(MontoTransfDestino * TCvoucher / myTC, BytFormatConta)
    '                    End If

    '                Else

    '                    If dtpFechaTCambio.Visible = False Then
    '                        If (cboTipoCambio.Text = "ESPECIAL" Or cboTipoCambio.SelectedValue = "3") Then
    '                            myTC = Devuelve_TC_Especial(dgvEpecial, pMoneda)
    '                        Else
    '                            myTC = cCapaReCompConConv.TipoCambio_Actual(sEmpresa, Format(dtpFechaVoucher.Value, "dd/MM/yyyy"), cFuncGen.CodigoMoneda(pMoneda), cboTipoCambio.Text)
    '                        End If
    '                    Else
    '                        If gbTipoConversion.Visible = True Then
    '                            myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, IIf(rbCompra.Checked = True, "Compra", "Venta"))
    '                        Else
    '                            myTC = MontoTC_segunComboFecha(DTtcXfecha, dtpFechaTCambio.Value, pMoneda, Str_CoV)
    '                        End If
    '                    End If

    '                    If myTC = 0 Then
    '                        MessageBox.Show("No ingreso Tipo de Cambio para la Moneda en " & pMoneda, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Information)
    '                        Return 0
    '                    End If

    '                    DrFilaDestino("MontoTC") = Format(myTC, "#,##0.000")
    '                    If cboTipoCambio.SelectedValue = "4" Then
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaTCambio.Value, "dd/MM/yyyy")
    '                    Else
    '                        DrFilaDestino("FechaTC") = Format(dtpFechaVoucher.Value, "dd/MM/yyyy")
    '                    End If

    '                    If z = 0 Then   'Si es Cargo
    '                        DrFilaDestino("Debe") = Math.Round(MontoTransfDestino * TCvoucher / myTC, BytFormatConta)
    '                        DrFilaDestino("Haber") = Format(0, "#,##0.000")
    '                    Else            'Si no Abono
    '                        DrFilaDestino("Debe") = Format(0, "#,##0.000")
    '                        DrFilaDestino("Haber") = Math.Round(MontoTransfDestino * TCvoucher / myTC, BytFormatConta)
    '                    End If

    '                End If

    '                DrFilaDestino("FechaDoc") = System.DBNull.Value   'Format(dtpFechaDocDet.Value, "dd/MM/yyyy")
    '                DrFilaDestino("Caduca_Fecha") = System.DBNull.Value    'Format(dtpFechaVcmtoDet.Value, "dd/MM/yyyy")
    '                DrFilaDestino("Ind_Trans_Dif") = "0"
    '                DrFilaDestino("Gen_Cta_Destino") = "1"
    '                DrFilaDestino("ComentarioCab") = Trim(txtGlosaCab.Text)
    '                DrFilaDestino("ID_CentroCosto") = CodCenCosto
    '                DrFilaDestino("Area") = pArea
    '                If pMoneda = STRmonSist Then
    '                    DrFilaDestino("Moneda_Sist") = "1"
    '                Else
    '                    DrFilaDestino("Moneda_Sist") = "0"
    '                End If
    '                If pMoneda = StrAbrevMoneda Then
    '                    DrFilaDestino("DocTransact") = "1"
    '                Else
    '                    DrFilaDestino("DocTransact") = "0"
    '                End If
    '                DrFilaDestino("MontoOriginal") = 0
    '                DrFilaDestino("ExisteDoc") = "0"
    '                DrFilaDestino("CtaConDestino") = CodCuentaTransf

    '                DstAsientos.Tables(StrTabla).Rows.Add(DrFilaDestino)
    '            Next
    '        Catch ex As Exception
    '            MessageBox.Show(ex.Message, "Contabilidad", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            Return 0
    '        End Try
    '        Return 1
    '    End Function



    Private Sub btnNuevoCab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevoCab.Click

    End Sub
End Class