﻿Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.ReportSource
Imports CrystalDecisions.Shared
Imports BusinessLogicLayer.Ventas.Reportes
Imports CapaEntidad
Imports System.Drawing.Printing

Public Class frmReporteBoletas
#Region "declaraciones"

    Private ID_DOC_VENTA As String = 0
    Private NombreReporte As String = ""
    'Dim rptBoletas As New rptBoletas
    'Dim rpt As New CrystalReport2
    Dim rptImpresion As New rptImpresion
    Public Property VG_ID_PROGPAGO As String

    Dim crParameterValues As ParameterValues
    Dim crParameterDiscreteValue As ParameterDiscreteValue
    Dim crParameterFieldDefinitions As ParameterFieldDefinitions
    Dim crParameterFieldDefinition As ParameterFieldDefinition
    Private Shared frmInstance As frmReporteBoletas = Nothing

#End Region
    

    Public Sub New(ByVal VL_NOMREPORTE As String, ByVal VL_ID_DOC_VENTA As String)
        InitializeComponent()
        NombreReporte = VL_NOMREPORTE
        ID_DOC_VENTA = VL_ID_DOC_VENTA

    End Sub
    'Public Sub New(VL_ID_DOC_VENTA As Int32)
    '    InitializeComponent()
    '    ID_DOC_VENTA = VL_ID_DOC_VENTA
    'End Sub
    'Private Sub frmReporteBoletas_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    '    If NombreReporte = "4" Then
    '        VER_IMPRESION()
    '    ElseIf NombreReporte = "5" Then
    '        IMPRIMIR()
    '    End If
    'End Sub
    Private tbCurrent As CrystalDecisions.CrystalReports.Engine.Table
    Private tliCurrent As CrystalDecisions.Shared.TableLogOnInfo
    'Private rptDoc As New ReportDocument



    Sub IMPRIMIR()
        rptImpresion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        crParameterFieldDefinitions = rptImpresion.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@ID_DOC_VENTA")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = ID_DOC_VENTA
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        rptImpresion.PrintToPrinter(1, False, 0, 1)
        'CrystalReportViewer1.ReportSource = rptBoletas
    End Sub
    Sub VER_IMPRESION()

        rptImpresion.DataSourceConnections(0).SetConnection(bdServerReporte, bdBDReporte, bdUserReporte, bdPassReporte)

        crParameterFieldDefinitions = rptImpresion.DataDefinition.ParameterFields()
        crParameterFieldDefinition = crParameterFieldDefinitions("@ID_DOC_VENTA")
        crParameterValues = crParameterFieldDefinition.CurrentValues

        crParameterDiscreteValue = New ParameterDiscreteValue()
        crParameterDiscreteValue.Value = ID_DOC_VENTA
        crParameterValues.Add(crParameterDiscreteValue)
        crParameterFieldDefinition.ApplyCurrentValues(crParameterValues)

        'CrystalReportViewer1.ReportSource = rpt
        CrystalReportViewer1.ReportSource = rptImpresion

    End Sub
    'Private Sub CargarBoletamesImpresion()
    '    Dim VL_SrvBoleta As New SrvBoleta()
    '    Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

    '    Try
    '        VL_BeanResultado = VL_SrvBoleta.Fnc_Gargar_boltaxmes_expediente(ID_DOC_VENTA)

    '        If VL_BeanResultado.blnExiste = True Then
    '            Dim rpBoleta As New rptBoletas()
    '            'rpBoleta.DataSource = (VL_BeanResultado.dtResultado)
    '            rpBoleta.DataSource = (VL_BeanResultado.dtResultado)
    '            CrystalReportViewer1.ReportSource = rpBoleta
    '            Me.CrystalReportViewer1.RefreshReport()

    '            Dim printerSettings As New PrinterSettings()
    '            Dim printController As PrintController = New StandardPrintController()

    '            'Dim reportProcessor As New ReportProcessor()
    '            Dim reportProcessor As New reportP
    '            reportProcessor.PrintController = printController
    '            reportProcessor.PrintReport(rpBoleta, printerSettings)
    '            Me.Close()
    '        Else
    '            MessageBox.Show(VL_BeanResultado.strMensaje)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.ToString())
    '    End Try
    'End Sub

    Private Sub frmReporteBoletas_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        frmInstance = Nothing
    End Sub

    Private Sub CrystalReportViewer1_Load(sender As Object, e As EventArgs) Handles CrystalReportViewer1.Load
        If NombreReporte = "4" Then
            VER_IMPRESION()
        ElseIf NombreReporte = "5" Then
            IMPRIMIR()
        End If
    End Sub
End Class