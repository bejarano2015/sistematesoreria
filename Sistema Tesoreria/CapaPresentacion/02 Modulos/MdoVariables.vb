﻿Option Explicit On
Imports System.Xml
Imports MSXML2

Imports CapaEntidad
'Imports System.ComponentModel
Imports System.Collections
Imports System.ComponentModel

Module MdoVariables

#Region "DECLARACIONES"
    '-----------------------------------------------------------
    'Variables para Reportes
    Public bdServerReporte As String = "192.168.1.200"
    Public bdBDReporte As String = "ERPDHMONT"
    Public bdUserReporte As String = "sa"
    Public bdPassReporte As String = "!gestionerp2013"

    'variable conexion para consultas de movimientos
    'SERVIDOR
    Public conexion As String = "Data Source=192.168.1.200;Initial Catalog=ERPDHMONT;User Id=sa; Password=!gestionerp2013;Current Language=english"

    'Public conexRPT As String = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=192.168.1.200;Initial Catalog=ERPDHMONT;User Id=sa; Password=!gestionerp2013;Current Language=english"
    'LOCAL
    'Public conexion As String = "Data Source=(local);Initial Catalog=ERPDHMONT;Integrated Security=True"

    'variable conexionParaGastosGene para traer ccostos por empresa de los documentos a pagar
    'SERVIDOR
    Public conexionParaGastosGene As String = "Data Source=192.168.1.200;Initial Catalog=ERPDHMONT;User Id=sa; Password=!gestionerp2013;Current Language=english"
    'LOCAL
    'Public conexionParaGastosGene As String = "Data Source=(local);Initial Catalog=ERPDHMONT;Integrated Security=True"


    Public RutaAppReportes As String = ""

    'Public bdServerReporte As String = "SIS014"
    'Public bdBDReporte As String = "ERPDHMONT"
    'Public bdUserReporte As String = "sa"
    'Public bdPassReporte As String = ""

    Public PorcentajeITF As Double = 0 '= 0.06
    Public gIgv As Double
    Public gIgvConvertido As Double
    'retencion
    Public gSerie As String = ""
    Public gNumeroInicioSerie As String = ""
    'tipo de cambio
    Public PorcentajeRetencion As Double = 0
    Public MontoRetencion As Double = 0
    Public VL_DOCUMENTO As String = ""

    Public iValorPorcentajeRetencion As Decimal
    Public iValorPorcentajeDetraccion As Decimal = 0.04
    Public iValorPorcentajePercepcion As Decimal = 0.02
    Public iValorTipoCambio As Decimal

    '-----------------------------------------------------------
    Dim objres As BeanResultado.ResultadoSelect = New BeanResultado.ResultadoSelect()


    Dim obj_Proveedor As BeanProveedor = New BeanProveedor

    Public versionSistema As String = ""
#End Region

    Public Sub KillProcess(ByVal processName As String)
        On Error GoTo ErrHandler
        Dim oWMI
        Dim ret
        Dim sService
        Dim oWMIServices
        Dim oWMIService
        Dim oServices
        Dim oService
        Dim servicename
        oWMI = GetObject("winmgmts:")
        oServices = oWMI.InstancesOf("win32_process")
        For Each oService In oServices
            servicename = LCase(Trim(CStr(oService.Name) & ""))
            If InStr(1, servicename, LCase(processName), vbTextCompare) > 0 Then
                ret = oService.Terminate
                Exit For

            End If
        Next
        oServices = Nothing
        oWMI = Nothing
ErrHandler:
        Err.Clear()
    End Sub

    'frmPeriodo
    Public Enum Accion
        Nuevo = 1     '
        Grabar = 2    '
        Modifica = 3  ' 
        Elimina = 4   '
        Cancelar = 5  '
    End Enum

    Public Sub botonesMantenimiento(ByVal bmbotonesmantenimientos As ToolStrip, ByVal Activar As Boolean)
        bmbotonesmantenimientos.Items("btnNuevo").Enabled = Activar ' Nuevo
        bmbotonesmantenimientos.Items("btnGrabar").Enabled = Not Activar ' Grabar
        bmbotonesmantenimientos.Items("btnRefrescar").Enabled = Activar ' Refrescar
        bmbotonesmantenimientos.Items("btnCancelar").Enabled = Not Activar ' Cancelar
        'bmbotonesmantenimientos.Items("btnAnular").Enabled = Activar ' Anular
        bmbotonesmantenimientos.Items("btnEliminar").Enabled = Activar ' Eliminar
        bmbotonesmantenimientos.Items("btnImprimirListado").Enabled = Activar ' Imprimir Listado
        'bmbotonesmantenimientos.Items("btnImprimir").Enabled = Not Activar ' Imprimir
        'bmbotonesmantenimientos.Items("btnBuscar").Enabled = Activar ' Bucar
    End Sub

    Public Function TraerNombreMes(ByVal NumeroMes As String) As String
        If NumeroMes = "01" Then
            TraerNombreMes = "Enero"
        ElseIf NumeroMes = "02" Then
            TraerNombreMes = "Febrero"
        ElseIf NumeroMes = "03" Then
            TraerNombreMes = "Marzo"
        ElseIf NumeroMes = "04" Then
            TraerNombreMes = "Abril"
        ElseIf NumeroMes = "05" Then
            TraerNombreMes = "Mayo"
        ElseIf NumeroMes = "06" Then
            TraerNombreMes = "Junio"
        ElseIf NumeroMes = "07" Then
            TraerNombreMes = "Julio"
        ElseIf NumeroMes = "08" Then
            TraerNombreMes = "Agosto"
        ElseIf NumeroMes = "09" Then
            TraerNombreMes = "Setiembre"
        ElseIf NumeroMes = "10" Then
            TraerNombreMes = "Octubre"
        ElseIf NumeroMes = "11" Then
            TraerNombreMes = "Noviembre"
        Else
            TraerNombreMes = "Diciembre"
        End If
    End Function



    Public Function TraerNumeroMes(ByVal NombreMes As String) As String
        If NombreMes = "Enero" Then
            TraerNumeroMes = "01"
        ElseIf NombreMes = "Febrero" Then
            TraerNumeroMes = "02"
        ElseIf NombreMes = "Marzo" Then
            TraerNumeroMes = "03"
        ElseIf NombreMes = "Abril" Then
            TraerNumeroMes = "04"
        ElseIf NombreMes = "Mayo" Then
            TraerNumeroMes = "05"
        ElseIf NombreMes = "Junio" Then
            TraerNumeroMes = "06"
        ElseIf NombreMes = "Julio" Then
            TraerNumeroMes = "07"
        ElseIf NombreMes = "Agosto" Then
            TraerNumeroMes = "08"
        ElseIf NombreMes = "Setiembre" Then
            TraerNumeroMes = "09"
        ElseIf NombreMes = "Octubre" Then
            TraerNumeroMes = "10"
        ElseIf NombreMes = "Noviembre" Then
            TraerNumeroMes = "11"
        Else
            TraerNumeroMes = "12"
        End If
    End Function

    Public Function ReturnMontoParaCalcItf(ByVal Monto As Double) As Double

        'Dim aaa As Double
        Dim x As Integer
        Dim c As Integer
        'aaa = txtMonto.Text

        'Dim MontoParaCalcItf As Double = 0

        If Monto < 1000 Then
            Return 0
            Exit Function
        End If

        For x = 1 To 10
            Monto = Monto \ 10
            If Monto < 10 Then Exit For
        Next

        'BeTextBox2.Text = aaa

        For c = 1 To x

            Monto = Monto * 10

        Next
        'MontoParaCalcItf = Monto
        Return Monto

    End Function

    'Variables Globales
    Public glbNameSistema As String = "Sistema de Tesorería"
    Public glbSisCodigo As String = "03"
    Public glbNameBD As String = "ERPDHMONT"

    Public gPeriodo As String = "" '"2009"
    Public gEmpresa As String = "" '= "01"
    Public gDesEmpresa As String = "" '"CONSORCIO DHMONT & CG & M"

    Public gEmprRuc As String = ""

    'Public gUsuarioCod As String = "00001"
    Public gUsuario As String = "" '"JESUS MACHA"
    Public glbNameUsuario As String = ""
    Public glbUsuCategoria As String = ""

    Public gPC As String = "" 'My.Computer.Name
    Public gFecha As String = "" ' "JESUS MACHA"

    Public gTipoCambio As Decimal = 3.15
    Public sMoneda As String
    Public sUsuario As String
    Public sProveedor As String
    Public sFechaFin As Date
    Public sEstadoArqueo As String
    Public iEstado01 As Integer = 0
    Public iEstado02 As Integer = 1

    'Variables Temporales para frmSesionCajas
    Public sIdArea As String = ""
    Public sDesArea As String = ""

    Public sIdCaja As String = ""
    Public sDesCaja As String = ""


    Public sDesEmpleado As String = ""
    Public sDesEmpleadoId As String = ""
    Public IdSesionCabecera As String

    Public sNumeroSesion As String = ""
    Public iNroCaja As Integer

    Public sMontoMinimo As String = ""
    Public sMontoBase As String = ""
    Public sSaldoAnterior As String = ""

    Public sSaldoReembolso As String = ""
    Public sSaldoBase As String = ""
    Public iContadorCabecera As Integer
    Public sDescripcionMoneda As String = ""
    Public sMonedaCaja As String = ""
    Public sMonSimbolo As String = ""
    Public sUsuarioDeCaja As String = ""

    Public sTPlaCodigo As String = ""
    Public sEmprResposable As String = ""

    Public dtCabecera As DataTable
    Public dtCabecera2 As DataTable
    'Public dtCabecera3 As DataTable
    Public sCantidadSesiones As Integer
    Public dtDetalle As DataTable
    Public dtMonto As DataTable
    Public dtExisteAporteIni As DataTable

    Public dtDataBD As DataTable

    Public DblAperturaDelDia As Decimal
    Public DblEgresoDelDia As Decimal
    Public DblSaldoCaja As Decimal

    'Public MontoGeneralInicio As Double = 0
    Public MontoGeneralTotalGasto As Double = 0
    Public MontoGeneralFinal As Double = 0

    Public ListarAporteInicialMasGastos As Int16 = 0
    Public ListarSaldoAnteriorMasGastos As Int16 = 0
    Public ListarSaldoAnteriorMasReembolso As Int16 = 0
    Public sExisteReembolso As Int16 = 0

    Public EstadoServicioruc As Boolean

    Function SoloNumeros(ByVal Keyascii As Short) As Short
        If InStr("1234567890", Chr(Keyascii)) = 0 Then
            SoloNumeros = 0
        Else
            SoloNumeros = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros = Keyascii
            Case 13
                SoloNumeros = Keyascii
        End Select
    End Function

    Function SoloNumeros2(ByVal Keyascii As Short) As Short
        If InStr("1234567890.-", Chr(Keyascii)) = 0 Then
            SoloNumeros2 = 0
        Else
            SoloNumeros2 = Keyascii
        End If
        Select Case Keyascii
            Case 8
                SoloNumeros2 = Keyascii
            Case 13
                SoloNumeros2 = Keyascii
        End Select
    End Function


    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    Public DstAsientos As New DataSet
    Dim StrSecuencia As String

    Public Sub CrearTablasAsientos(ByVal StrMonedas As String)
        If DstAsientos.Tables.Contains("TmpAsiento" & StrMonedas) Then
            DstAsientos.Tables.Remove("TmpAsiento" & StrMonedas)
        End If

        DstAsientos.Tables.Add("TmpAsiento" & StrMonedas)
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Clear()
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Secuencia", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Codigo_Cuenta", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Tipo_Doc", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Serie_Doc", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Nro_Doc", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Debe", GetType(System.Decimal))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Haber", GetType(System.Decimal))

        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("MonedaTrans", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("MontoTC", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("FechaTC", GetType(System.DateTime))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("ID_CentroCosto", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Cod_Tipo_Anexo", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Codigo_Anexo", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Area", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("FechaDoc", GetType(System.DateTime))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Caduca_Fecha", GetType(System.DateTime))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Ind_Trans_Dif", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("ComentarioCab", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Gen_Cta_Destino", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("Moneda_Sist", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("DocTransact", GetType(System.String))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("MontoOriginal", GetType(System.Double))
        DstAsientos.Tables("TmpAsiento" & StrMonedas).Columns.Add("ExisteDoc", GetType(System.Double))
    End Sub

    Public Function Generar_Secuencia(ByVal TABLA As DataTable) As String
        Dim Cadena As String = "0000"
        If TABLA.Rows.Count = 0 Then
            StrSecuencia = Cadena & "1"
        Else
            StrSecuencia = Right(New String("0", 5) + Convert.ToString(TABLA.Rows.Count + 1), 5)
        End If
        Return StrSecuencia
    End Function


    'INSTANCIAMOS LA ENTIDAD PROVEEDOR PARA CARGARLA CON DATOS ONLINE
    Dim v_BeanProveedor As BeanProveedor = New BeanProveedor()

#Region "VALIDACION RUC"
    Public Function VerRuc(ByVal v_ruc As String) As BeanResultado.ResultadoSelect
        ValidarRuc(v_ruc)

        Return objres
    End Function
    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    Private Function ValidarRuc(ByVal v_Ruc As String) As Boolean
        If Trim(v_Ruc) = "" Then
            objres.blnExiste = False
            objres.strMensaje = "Ingrese número del RUC"
            'txtRuc.Focus()
            Exit Function
        End If
        If IsNumeric(v_Ruc) = True Then
            If Len(v_Ruc) < 11 Then
                'Limpiar()
                objres.blnExiste = False
                objres.strMensaje = "Ingrese los 11 números del RUC"
                'txtRuc.Focus()
                Exit Function
            End If
            If Val(Mid(Trim(v_Ruc), 2, 9)) = 0 Or Trim(v_Ruc) = "23333333333" Then
                'Limpiar()
                objres.blnExiste = False
                objres.strMensaje = "Verificar número del RUC"
                'txtRuc.Focus()
                Exit Function
            End If
            If Verificar_ruc(v_Ruc) = False Then
                'Limpiar()
                objres.blnExiste = False
                objres.strMensaje = "El número del RUC no es válido"
                'txtRuc.Focus()
                Exit Function
            End If
            Call VALRUC(v_Ruc)
            'OTROVALRUC(v_Ruc)
        Else
            'Limpiar()
            objres.blnExiste = False
            objres.strMensaje = "Solo se aceptan números"
            ' txtRuc.Focus()
        End If
    End Function
    Function Verificar_ruc(ByVal xNum As String) As Boolean
        Dim li_suma, li_residuo, li_diferencia, li_compara As Integer
        li_suma = (CInt(Mid(xNum, 1, 1)) * 5) + (CInt(Mid(xNum, 2, 1)) * 4) + (CInt(Mid(xNum, 3, 1)) * 3) + (CInt(Mid(xNum, 4, 1)) * 2) + (CInt(Mid(xNum, 5, 1)) * 7) + (CInt(Mid(xNum, 6, 1)) * 6) + (CInt(Mid(xNum, 7, 1)) * 5) + (CInt(Mid(xNum, 8, 1)) * 4) + (CInt(Mid(xNum, 9, 1)) * 3) + (CInt(Mid(xNum, 10, 1)) * 2)
        li_compara = CInt(Mid(xNum, 11, 1))
        li_residuo = li_suma Mod 11
        li_diferencia = Int(11 - li_residuo)
        If li_diferencia > 9 Then li_diferencia = li_diferencia - 10
        If li_diferencia <> li_compara Then
            Verificar_ruc = False
        Else
            Verificar_ruc = True
        End If
    End Function
    ''VARIAVLES  PARA VALIDACION DE RUC
    Dim xDat As String
    Dim xRazSoc As String, xEst As String, xCon As String, xDir As String
    Dim xRazSocX As Long, xEstX As Long, xConX As Long, xDirX As Long
    Dim xRazSocY As Long, xEstY As Long, xConY As Long, xDirY As Long
    Private Sub VALRUC(ByVal xNum As String)
        Dim list_Proveedor As List(Of BeanProveedor) = New List(Of BeanProveedor)()
        On Error Resume Next
        Dim xWml As New XMLHTTP
        xWml.open("GET", "http://www.sunat.gob.pe/w/wapS01Alias?ruc=" & xNum, False)
        xWml.setRequestHeader("Content-Type", "text/xml")
        xWml.setRequestHeader("charset", "utf-8")

        xWml.send()

        If xWml.status = 200 Then
            'Limpiar()
            xDat = xWml.responseText
            'Dim DS As DataSet = New DataSet()
            'DS.ReadXml(xDat)
            If Len(xDat) <= 635 Then
                objres.blnExiste = False
                objres.strMensaje = "El numero Ruc ingresado no existe en la Base de datos de la SUNAT"
                xWml = Nothing
                'txtRuc.Focus()
                Exit Sub
            End If

            xDat = Replace(xDat, "N&#xFA;mero Ruc. </b> " & xNum & " - ", "RazonSocial:")
            xDat = Replace(xDat, "Estado.</b>", "Estado:")
            xDat = Replace(xDat, "Agente Retenci&#xF3;n IGV.", "ARIGV:")
            xDat = Replace(xDat, "Situaci&#xF3;n.<b> ", "Situacion:")
            xDat = Replace(xDat, "Direcci&#xF3;n.</b><br/>", "Direccion:")
            xDat = Replace(xDat, "     ", " ")
            xDat = Replace(xDat, "    ", " ")
            xDat = Replace(xDat, "   ", " ")
            xDat = Replace(xDat, "  ", " ")
            xDat = Replace(xDat, "( ", "(")
            xDat = Replace(xDat, " )", ")")
            xDat = Replace(xDat, "&amp;quot;", """")
            xDat = Replace(xDat, "&amp;", "&")
            xDat = Replace(xDat, "&#xF1;", "Ñ")


            xRazSocX = InStr(1, xDat, "RazonSocial:", vbTextCompare)
            xRazSocY = InStr(1, xDat, " <br/></small>", vbTextCompare)
            xRazSocX = xRazSocX + 12
            xRazSoc = Mid(xDat, xRazSocX, (xRazSocY - xRazSocX))

            xEstX = InStr(1, xDat, "Estado:", vbTextCompare)
            xEstY = InStr(1, xDat, "</small><br/>", vbTextCompare)
            xEstX = xEstX + 7
            xEst = Mid(xDat, xEstX, xEstY - xEstX) 'Mid(xDat, xEstX, ((xEstY - 34) - xEstX))

            xConX = InStr(1, xDat, "Situacion:", vbTextCompare)
            xConY = InStr(1, xDat, "</b></small><br/>", vbTextCompare)
            xDirY = xConX - 23
            xConX = xConX + 10
            xCon = Mid(xDat, xConX, (xConY - xConX))

            xDirX = InStr(1, xDat, "Direccion:", vbTextCompare)
            xDirX = xDirX + 10
            xDir = Mid(xDat, xDirX, (xDirY - xDirX))

            xRazSoc = Replace(xRazSoc, "&#209;", "Ñ")
            xRazSoc = Replace(xRazSoc, "&#xD1;", "Ñ")
            xRazSoc = Replace(xRazSoc, "&#193;", "Á")
            xRazSoc = Replace(xRazSoc, "&#201;", "É")
            xRazSoc = Replace(xRazSoc, "&#205;", "Í")
            xRazSoc = Replace(xRazSoc, "&#211;", "Ó")
            xRazSoc = Replace(xRazSoc, "&#218;", "Ú")
            xRazSoc = Replace(xRazSoc, "&#xC1;", "Á")
            xRazSoc = Replace(xRazSoc, "&#xC9;", "É")
            xRazSoc = Replace(xRazSoc, "&#xCD;", "Í")
            xRazSoc = Replace(xRazSoc, "&#xD3;", "Ó")
            xRazSoc = Replace(xRazSoc, "&#xDA;", "Ú")

            xDir = Replace(xDir, "&#209;", "Ñ")
            xDir = Replace(xDir, "&#xD1;", "Ñ")
            xDir = Replace(xDir, "&#193;", "Á")
            xDir = Replace(xDir, "&#201;", "É")
            xDir = Replace(xDir, "&#205;", "Í")
            xDir = Replace(xDir, "&#211;", "Ó")
            xDir = Replace(xDir, "&#218;", "Ú")
            xDir = Replace(xDir, "&#xC1;", "Á")
            xDir = Replace(xDir, "&#xC9;", "É")
            xDir = Replace(xDir, "&#xCD;", "Í")
            xDir = Replace(xDir, "&#xD3;", "Ó")
            xDir = Replace(xDir, "&#xDA;", "Ú")

            obj_Proveedor.Ruc = xNum
            obj_Proveedor.RazonSocial = xRazSoc
            obj_Proveedor.Estado = xEst

            If xCon = "Condici￿e domicilio por Verificar; Est￿mpedido de solicitar NUEVA autorizaci￿e Impresi￿e comprobante de Pago." Then
                obj_Proveedor.Condicion = "NO HABIDO" 'Condición de domicilio por Verificar; Está impedido de solicitar NUEVA autorización de Impresión de comprobante de Pago.

            Else
                obj_Proveedor.Condicion = xCon
            End If

            'obj_Proveedor.Condicion = xCon
            obj_Proveedor.Direccion = xDir
            list_Proveedor.Add(obj_Proveedor)
            objres.dtResultado = ToDataTable(list_Proveedor)
            objres.blnExiste = True
            objres.strMensaje = "EL RUC INGRESADO SI EXISTE EN LA BASE DE DATOS DE LA SUNAT"
        Else

            'Limpiar()
            objres.blnExiste = False
            objres.strMensaje = "No responde el servicio de la SUNAT"
        End If
        xWml = Nothing
    End Sub
    Private Sub OTROVALRUC(ByVal xNum As String)
        On Error Resume Next
        Dim xWml As New XMLHTTP
        xWml.open("POST", "http://www.sunat.gob.pe/w/wapS01Alias?ruc=" & xNum, False)
        xWml.send()
        If xWml.status = 200 Then
            'Limpiar()
            xDat = xWml.responseText
            If Len(xDat) <= 635 Then
                objres.blnExiste = False
                objres.strMensaje = "El numero Ruc ingresado no existe en la Base de datos de la SUNAT"
                xWml = Nothing
                'txtRuc.Focus()
                Exit Sub

            End If

            Dim xTabla() As String

            xDat = Replace(xDat, "     ", " ")
            xDat = Replace(xDat, "    ", " ")
            xDat = Replace(xDat, "   ", " ")
            xDat = Replace(xDat, "  ", " ")
            xDat = Replace(xDat, "( ", "(")
            xDat = Replace(xDat, " )", ")")

            xTabla = Split(xDat, "<small>")

            xTabla(1) = Replace(xTabla(1), "<b>N&#xFA;mero Ruc. </b> " & xNum & " - ", "")
            xTabla(1) = Replace(xTabla(1), " <br/></small>", "")

            xTabla(4) = Replace(xTabla(4), "<b>Estado.</b>", "")
            xTabla(4) = Replace(xTabla(4), "</small><br/>", "")

            xTabla(7) = Replace(xTabla(7), "<b>Direcci&#xF3;n.</b><br/>", "")
            xTabla(7) = Replace(xTabla(7), "</small><br/>", "")

            xTabla(8) = Replace(xTabla(8), "Situaci&#xF3;n.<b> ", "")
            xTabla(8) = Replace(xTabla(8), "</b></small><br/>", "")

            xRazSoc = CStr(xTabla(1))
            xEst = CStr(xTabla(4))
            xDir = CStr(xTabla(7))
            xCon = CStr(xTabla(8))

            xRazSoc = Replace(xRazSoc, "&#209;", "Ñ")
            xRazSoc = Replace(xRazSoc, "&#xD1;", "Ñ")
            xRazSoc = Replace(xRazSoc, "&#193;", "Á")
            xRazSoc = Replace(xRazSoc, "&#201;", "É")
            xRazSoc = Replace(xRazSoc, "&#205;", "Í")
            xRazSoc = Replace(xRazSoc, "&#211;", "Ó")
            xRazSoc = Replace(xRazSoc, "&#218;", "Ú")
            xRazSoc = Replace(xRazSoc, "&#xC1;", "Á")
            xRazSoc = Replace(xRazSoc, "&#xC9;", "É")
            xRazSoc = Replace(xRazSoc, "&#xCD;", "Í")
            xRazSoc = Replace(xRazSoc, "&#xD3;", "Ó")
            xRazSoc = Replace(xRazSoc, "&#xDA;", "Ú")

            xRazSoc = Mid(xRazSoc, 1, Len(xRazSoc) - 3)

            xDir = Replace(xDir, "&#209;", "Ñ")
            xDir = Replace(xDir, "&#xD1;", "Ñ")
            xDir = Replace(xDir, "&#193;", "Á")
            xDir = Replace(xDir, "&#201;", "É")
            xDir = Replace(xDir, "&#205;", "Í")
            xDir = Replace(xDir, "&#211;", "Ó")
            xDir = Replace(xDir, "&#218;", "Ú")
            xDir = Replace(xDir, "&#xC1;", "Á")
            xDir = Replace(xDir, "&#xC9;", "É")
            xDir = Replace(xDir, "&#xCD;", "Í")
            xDir = Replace(xDir, "&#xD3;", "Ó")
            xDir = Replace(xDir, "&#xDA;", "Ú")

            xEst = Mid(xEst, 1, Len(xEst) - 6)
            xCon = Mid(xCon, 1, Len(xCon) - 3)
            xDir = Mid(xDir, 1, Len(xDir) - 3)
            'Dim formvalruc As frmValidarRuc = New frmValidarRuc(xNum, xRazSoc, xEst, xCon, xDir)
            'formvalruc.ShowDialog()
            obj_Proveedor.Ruc = xNum
            obj_Proveedor.RazonSocial = xRazSoc
            obj_Proveedor.Estado = xEst
            obj_Proveedor.Condicion = xCon
            obj_Proveedor.Direccion = xDir
            'list_Proveedor.Add(obj_Proveedor)
            'objres.dtResultado = ToDataTable(list_Proveedor)
            objres.blnExiste = True
            objres.strMensaje = "EL RUC INGRESADO SI EXISTE EN LA BASE DE DATOS DE LA SUNAT"
        Else

            'Limpiar()
            objres.blnExiste = False
            objres.strMensaje = "No responde el servicio de la SUNAT"
        End If
        xWml = Nothing
    End Sub



    '<System.Runtime.CompilerServices.Extension> _
    Public Function ToDataTable(Of T)(data As IList(Of T)) As DataTable
        Dim props As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
        Dim table As New DataTable()
        For i As Integer = 0 To props.Count - 1
            Dim prop As PropertyDescriptor = props(i)
            table.Columns.Add(prop.Name, prop.PropertyType)
        Next
        Dim values As Object() = New Object(props.Count - 1) {}
        For Each item As T In data
            For i As Integer = 0 To values.Length - 1
                values(i) = props(i).GetValue(item)
            Next
            table.Rows.Add(values)
        Next
        Return table
    End Function

#End Region
End Module
