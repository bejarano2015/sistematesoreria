Imports System.Data
Imports System.IO
'Imports Microsoft.Office.Interop

Public Class Funciones


    'Public Sub ToTable(ByVal dgv As DataGridView, ByVal tableName As String)

    Public Shared Function ToTable(ByVal dgv As DataGridView, ByVal tableName As String) As DataTable

        'dgv = New DataGridView

        Dim table As New DataTable
        'table = New DataTable
        'var(table = New DataTable(tableName))

        Dim iCol As Integer = 0
        'Dim iColVisibles As Integer = 0
        Do While (iCol < dgv.Columns.Count)
            'MessageBox.Show(dgv.Columns(iCol).HeaderText)
            'If dgv.Columns(iCol).Visible = True Then
            table.Columns.Add(dgv.Columns(iCol).HeaderText)
            'iColVisibles = iColVisibles + 1
            'End If
            iCol = (iCol + 1)
            'Dim dataGridView As DataGridView
        Loop

        Dim i As Integer = 0
        'Do While (i < iCol)
        Do While (i < dgv.Rows.Count)
            'Dim boundRow As Object = CType(dgv.Rows(i).DataBoundItem, DataRowView)
            'Dim cells As Object = New Object((boundRow.Row.ItemArray.Length) - 1) {}
            'Dim cells As DataRow
            Dim iCol2 As Integer = 0
            Dim dr As DataRow
            dr = table.NewRow()
            ''Dim a As Integer = dgv.Rows(i).Cells.Count
            Do While (iCol2 < dgv.Rows(i).Cells.Count)
                'dt.NewRow()
                'Do While (iCol < boundRow.Row.ItemArray.Length)
                'cells(iCol2) = dgv.Rows(i).Cells(iCol2)
                dr(iCol2) = dgv.Rows(i).Cells(iCol2).Value
                iCol2 = (iCol2 + 1)
            Loop
            table.Rows.Add(dr)
            i = (i + 1)
        Loop

        'Dim i As Integer = 0
        'Do While (i < dgv.Rows.Count)
        '    'Obtiene el DataBound de la fila y copia los valores de la fila 

        '    'Dim boundRow As DataRowView = DirectCast(dgv.Rows(i).DataBoundItem, DataRowView)

        '    Dim boundRow As Object = CType(dgv.Rows(i).DataBoundItem, DataRowView)
        '    Dim cells As Object = New Object((boundRow.Row.ItemArray.Length) - 1) {}
        '    'Dim iCol As Integer = 0
        '    Do While (iCol < boundRow.Row.ItemArray.Length)
        '        cells(iCol) = boundRow.Row.ItemArray(iCol)
        '        iCol = (iCol + 1)
        '    Loop
        '    table.Rows.Add(cells)
        '    i = (i + 1)
        'Loop

        Return table
    End Function

    Public Sub DataTableToExcel(ByVal pDataTable As DataTable, ByVal vFileName As String)

        'Dim vFileName As String = "C:\Jesus.xls" 'Path.GetTempFileName()

        'If Not System.IO.File.Exists(vFileName) Then
        '    MsgBox("No se encontr� el Libro: " & vFileName, MsgBoxStyle.Critical, "Ruta inv�lida")
        '    Exit Sub
        'End If

        'FileOpen(1, vFileName, OpenMode.Output)

        FileOpen(1, vFileName, OpenMode.Append)
        Dim sb As String = ""
        Dim dc As DataColumn
        For Each dc In pDataTable.Columns
            sb &= dc.Caption & Microsoft.VisualBasic.ControlChars.Tab
        Next
        PrintLine(1, sb)

        Dim i As Integer = 0
        Dim filas As Integer = 0
        Dim dr As DataRow
        For Each dr In pDataTable.Rows
            i = 0 : sb = ""
            For Each dc In pDataTable.Columns
                If Not IsDBNull(dr(i)) Then
                    'sb &= CStr(dr(i)) & Microsoft.VisualBasic.ControlChars.Tab
                    sb &= Microsoft.VisualBasic.Replace(CStr(dr(i)), Chr(13) + Chr(10), "") & Microsoft.VisualBasic.ControlChars.Tab
                Else
                    sb &= Microsoft.VisualBasic.ControlChars.Tab
                End If

                i += 1
            Next
            filas = filas + 1
            PrintLine(1, sb)
        Next
        FileClose()
        'MsgBox("El Archivo se Descargo Correctamente en : " & vFileName,")
        MessageBox.Show("El Archivo se Descargo en " & vFileName, "Sistema Tesorer�a", MessageBoxButtons.OK, MessageBoxIcon.Information)
        'TextToExcel(vFileName)
        'Process.Start("Excel.exe", vFileName)

    End Sub

    Public Sub TextToExcel(ByVal pFileName As String)

        'Dim vFormato As Excel.XlRangeAutoFormat

        Dim vCultura As System.Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
        System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")

        'Dim Exc As Excel.Application = New Excel.Application
        'Exc.Workbooks.OpenText(pFileName, , , , Excel.XlTextQualifier.xlTextQualifierNone, , True)

        'Dim Wb As Excel.Workbook = Exc.ActiveWorkbook
        'Dim Ws As Excel.Worksheet = Wb.ActiveSheet

        ''Se le indica el formato al que queremos exportarlo
        Dim valor As Integer = 1
        'If valor > -1 Then
        '    Select Case valor
        '        Case 0 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatNone
        '        Case 1 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatSimple
        '        Case 2 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatClassic1
        '        Case 3 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatClassic2
        '        Case 4 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatClassic3
        '        Case 5 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatAccounting1
        '        Case 6 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatAccounting2
        '        Case 7 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatAccounting3
        '        Case 8 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatAccounting4
        '        Case 9 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatColor1
        '        Case 10 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatColor2
        '        Case 11 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatColor3
        '        Case 12 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatList1
        '        Case 13 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatList2
        '        Case 14 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormatList3
        '        Case 15 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormat3DEffects1
        '        Case 16 : vFormato = Excel.XlRangeAutoFormat.xlRangeAutoFormat3DEffects2
        '    End Select

        '    Ws.Range(Ws.Cells(1, 1), Ws.Cells(Ws.UsedRange.Rows.Count, Ws.UsedRange.Columns.Count)).AutoFormat(vFormato)

        '    pFileName = Path.GetTempFileName.Replace("tmp", "xls")
        '    File.Delete(pFileName)
        '    Exc.ActiveWorkbook.SaveAs(pFileName, Excel.XlTextQualifier.xlTextQualifierNone - 1)
        'End If
        'Exc.Quit()

        'Ws = Nothing
        'Wb = Nothing
        'Exc = Nothing

        'GC.Collect()

        If valor > -1 Then
            'Dim p As System.Diagnostics.Process = New System.Diagnostics.Process
            'p.EnableRaisingEvents = False
            'p.Start("Excel.exe", pFileName)

            Dim p As System.Diagnostics.Process = New System.Diagnostics.Process
            p.EnableRaisingEvents = False
            Process.Start("Excel.exe", pFileName)

        End If
        System.Threading.Thread.CurrentThread.CurrentCulture = vCultura
    End Sub


End Class
