﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Threading.Tasks
Imports System.Drawing
Imports System.Net
Imports System.IO
Imports System.Web
Imports System.Text.RegularExpressions

Public Class ConsultaRuc
    Public Enum Resul
        Ok = 0
        NoResul = 1
        ErrorCapcha = 2
        [Error] = 3
    End Enum
    Private state As Resul
    Private _Nombres As String
    Private _ApePaterno As String
    Private _ApeMaterno As String
    Private _Estado As String
    Private _Telefono As String
    Private _Condicion As String
    Private myCookie As CookieContainer

    Public ReadOnly Property GetCapcha As Image
        Get
            Return ReadCapcha()
        End Get

    End Property
    Public ReadOnly Property Nombres As String
        Get
            Return _Nombres
        End Get

    End Property
    Public ReadOnly Property ApePaterno As String
        Get
            Return _ApePaterno
        End Get

    End Property
    Public ReadOnly Property ApeMaterno As String
        Get
            Return _ApeMaterno
        End Get

    End Property
    Public ReadOnly Property Estado As String
        Get
            Return _Estado
        End Get

    End Property
    Public ReadOnly Property Telefono As String
        Get
            Return _Telefono
        End Get

    End Property
    Public ReadOnly Property GetResul As Resul
        Get
            Return state
        End Get

    End Property
    Public Sub New()
        Try
            myCookie = Nothing
            myCookie = New CookieContainer()
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
            ReadCapcha()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function ValidarCertificado(sender As Object, certificate As System.Security.Cryptography.X509Certificates.X509Certificate, chain As System.Security.Cryptography.X509Certificates.X509Chain, sslPolicyErrors As System.Net.Security.SslPolicyErrors) As [Boolean]
        Return True
    End Function
    Private Function ReadCapcha() As Image
        Try
            System.Net.ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf ValidarCertificado)
            Dim myWebRequest As HttpWebRequest = DirectCast(WebRequest.Create("http://www.sunat.gob.pe/cl-ti-itmrconsruc/captcha?accion=image&magic=2"), HttpWebRequest)
            myWebRequest.CookieContainer = myCookie
            myWebRequest.Proxy = Nothing
            myWebRequest.Credentials = CredentialCache.DefaultCredentials
            Dim myWebResponse As HttpWebResponse = DirectCast(myWebRequest.GetResponse(), HttpWebResponse)
            Dim myImgStream As Stream = myWebResponse.GetResponseStream()
            Return Image.FromStream(myImgStream)
        Catch ex As Exception
            Throw ex

        End Try

    End Function

    Public Sub GetInfo(numDni As String, ImgCapcha As String)
        Dim xRazSoc As String = ""
        Dim xEst As String = ""
        Dim xCon As String = ""
        Dim xDir As String = ""
        Dim xAg As String = ""
        Dim xTel As String = ""
        Dim xCondicion As String = ""

        Try
            'A este link le pasamos los datos , RUC y valor del captcha
            Dim myUrl As String = [String].Format("http://www.sunat.gob.pe/cl-ti-itmrconsruc/jcrS00Alias?accion=consPorRuc&nroRuc={0}&codigo={1}", numDni, ImgCapcha)
            Dim myWebRequest As HttpWebRequest = DirectCast(WebRequest.Create(myUrl), HttpWebRequest)
            myWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0"
            myWebRequest.CookieContainer = myCookie
            myWebRequest.Credentials = CredentialCache.DefaultCredentials
            myWebRequest.Proxy = Nothing
            Dim myHttpWebResponse As HttpWebResponse = DirectCast(myWebRequest.GetResponse(), HttpWebResponse)
            Dim myStream As Stream = myHttpWebResponse.GetResponseStream()
            Dim myStreamReader As New StreamReader(myStream)
            'Leemos los datos
            Dim xDat As String = HttpUtility.HtmlDecode(myStreamReader.ReadToEnd())

            Dim _split As String() = xDat.Split(New Char() {"<"c, ">"c, ControlChars.Lf, ControlChars.Cr})
            Dim _resul As New List(Of String)()
            For i As Integer = 0 To _split.Length - 1
                If Not String.IsNullOrEmpty(_split(i).Trim()) Then
                    _resul.Add(_split(i).Trim())
                End If
            Next
            Select Case _resul.Count
                Case 77
                    state = Resul.ErrorCapcha
                    Exit Select
                Case Is >= 635
                    state = Resul.Ok
                    Exit Select
                Case 147
                    state = Resul.NoResul
                    Exit Select
                Case Else
                    state = Resul.[Error]
                    Exit Select
            End Select

            If state = Resul.Ok Then
                Dim tabla() As String
                xDat = Replace(xDat, "     ", " ")
                xDat = Replace(xDat, "    ", " ")
                xDat = Replace(xDat, "   ", " ")
                xDat = Replace(xDat, "  ", " ")
                xDat = Replace(xDat, "( ", "(")
                xDat = Replace(xDat, " )", ")")
                'Lo convertimos a tabla o mejor dicho a un arreglo de string como se ve declarado arriba
                tabla = Regex.Split(xDat, "<td class")
                'MsgBox(tabla.Count & " - " & _resul.Count)
                'separamos el arreglo que hasta ese momento tenia 1 solo item , y lo dividimos por la etiqueta tdclass
                'Esto lo hice porque cuando es persona el ruc empieza con 1
                'y tiene un resultado distinto a cuando es empresa ...
                If numDni.StartsWith("1") Then
                    'hacemos el parseo
                    tabla(1) = tabla(1).Replace("=""bg"" colspan=3>" & numDni & " - ", "")
                    tabla(1) = tabla(1).Replace("</td>" & vbCrLf & " </tr>" & vbCrLf & " <tr>" & vbCrLf & "", "")
                    tabla(12) = tabla(12).Replace("=""bg"" colspan=1>", "")
                    tabla(12) = tabla(12).Replace("</td>", "")

                    tabla(15) = tabla(15).Replace("=""bg"" colspan=3>", "")
                    tabla(15) = tabla(15).Replace("</td>", "")
                    tabla(15) = tabla(15).Replace("</tr>", "")
                    tabla(15) = tabla(15).Replace("<tr>", "")

                    tabla(17) = tabla(17).Replace("=""bg"" colspan=3>", "")
                    tabla(17) = tabla(17).Replace("</td>" & vbCrLf & " </tr>" & vbCrLf & "<!-- SE COMENTO POR INDICACION DEL PASE PAS20134EA20000207 -->" & vbCrLf & "<!-- <tr> -->" & vbCrLf & "<!-- ", "")
                    tabla(19) = tabla(19).Replace("=""bg"" colspan=1>", "")
                    tabla(19) = tabla(19).Replace("</td> -->" & vbCrLf & "<!-- ", "")

                    xRazSoc = Trim(CStr(tabla(1)))

                    xCon = Trim(tabla(15))

                    xEst = Trim(CStr(tabla(12)))
                    xDir = CStr(tabla(17))
                    xTel = Trim(CStr(tabla(19)))


                ElseIf numDni.StartsWith("2") Then
                    'bueno aqui es empresa ...
                    tabla(1) = tabla(1).Replace("=""bg"" colspan=3>" & numDni & " - ", "")
                    tabla(1) = tabla(1).Replace("</td>" & vbCrLf & " </tr>" & vbCrLf & " <tr>" & vbCrLf & "", "")
                    tabla(10) = tabla(10).Replace("=""bg"" colspan=1>", "")
                    tabla(10) = tabla(10).Replace("</td>", "")

                    tabla(15) = tabla(15).Replace("=""bg"" colspan=3>", "")
                    tabla(15) = tabla(15).Replace("</td>", "")
                    tabla(15) = tabla(15).Replace("</tr>", "")
                    tabla(15) = tabla(15).Replace("<tr>", "")

                    tabla(16) = tabla(16).Replace("=""bg"" colspan=3>", "")
                    tabla(17) = tabla(17).Replace("=""bg"" colspan=1>", "")
                    tabla(17) = tabla(17).Replace("</td> -->" & vbCrLf & "<!-- ", "")

                    xCon = Trim(tabla(15))
                    xRazSoc = Trim(CStr(tabla(1)))
                    xEst = Trim(CStr(tabla(10)))
                    xDir = CStr(tabla(15))
                    xTel = Trim(CStr(tabla(17)))
                    xCondicion = CStr(tabla(16))


                End If
                'los resultados
                'Como ven en el arreglo se pueden obtener mas datos pero yaa pues el parseo lo hacen uds...
                _Nombres = xRazSoc
                _ApeMaterno = xDir
                _Estado = xEst
                _Telefono = xTel
                _Condicion = xCon

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
