﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDocumentosSunat
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmDocumentosSunat))
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.TabPersonalizado1 = New Controles_DHMont.TabPersonalizado()
        Me.Page_Consulta = New System.Windows.Forms.TabPage()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.rgListaDoc = New Telerik.WinControls.UI.RadGridView()
        Me.MECO = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MECO_MODIFICAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_ELIMINAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.Page_Mantenimiento = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblTexto = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboCuentaCtble = New System.Windows.Forms.ComboBox()
        Me.txtNumeroDoc = New System.Windows.Forms.TextBox()
        Me.cboDoc_Sunat = New System.Windows.Forms.ComboBox()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGurdar = New System.Windows.Forms.Button()
        Me.TabPersonalizado1.SuspendLayout()
        Me.Page_Consulta.SuspendLayout()
        CType(Me.rgListaDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaDoc.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MECO.SuspendLayout()
        Me.Page_Mantenimiento.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabPersonalizado1
        '
        Me.TabPersonalizado1.Controls.Add(Me.Page_Consulta)
        Me.TabPersonalizado1.Controls.Add(Me.Page_Mantenimiento)
        Me.TabPersonalizado1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPersonalizado1.Location = New System.Drawing.Point(3, 12)
        Me.TabPersonalizado1.Name = "TabPersonalizado1"
        Me.TabPersonalizado1.SelectedIndex = 0
        Me.TabPersonalizado1.Size = New System.Drawing.Size(686, 368)
        Me.TabPersonalizado1.TabIndex = 1
        '
        'Page_Consulta
        '
        Me.Page_Consulta.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Page_Consulta.Controls.Add(Me.btnNuevo)
        Me.Page_Consulta.Controls.Add(Me.rgListaDoc)
        Me.Page_Consulta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Page_Consulta.Location = New System.Drawing.Point(4, 22)
        Me.Page_Consulta.Name = "Page_Consulta"
        Me.Page_Consulta.Padding = New System.Windows.Forms.Padding(3)
        Me.Page_Consulta.Size = New System.Drawing.Size(678, 342)
        Me.Page_Consulta.TabIndex = 0
        Me.Page_Consulta.Text = "Consultas"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.SystemColors.HighlightText
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(519, 305)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(78, 23)
        Me.btnNuevo.TabIndex = 1
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'rgListaDoc
        '
        Me.rgListaDoc.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.rgListaDoc.ContextMenuStrip = Me.MECO
        Me.rgListaDoc.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgListaDoc.Location = New System.Drawing.Point(0, 0)
        '
        'rgListaDoc
        '
        Me.rgListaDoc.MasterTemplate.AllowAddNewRow = False
        Me.rgListaDoc.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn1.FieldName = "ID_DOCUMENTO"
        GridViewTextBoxColumn1.HeaderText = "Nº Documento"
        GridViewTextBoxColumn1.Name = "ID_DOCUMENTO"
        GridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn1.Width = 90
        GridViewTextBoxColumn2.FieldName = "DESCRIPCION"
        GridViewTextBoxColumn2.HeaderText = "Descripción"
        GridViewTextBoxColumn2.Name = "DESCRIPCION"
        GridViewTextBoxColumn2.Width = 200
        GridViewTextBoxColumn3.FieldName = "Cod_DocSunat"
        GridViewTextBoxColumn3.HeaderText = "Cod_DocSunat"
        GridViewTextBoxColumn3.IsVisible = False
        GridViewTextBoxColumn3.Name = "Cod_DocSunat"
        GridViewTextBoxColumn4.FieldName = "Descripcion_sunat"
        GridViewTextBoxColumn4.HeaderText = "Doc. Sunat"
        GridViewTextBoxColumn4.Name = "Descripcion_sunat"
        GridViewTextBoxColumn4.Width = 200
        GridViewTextBoxColumn5.FieldName = "ID_CTA_CTBLE"
        GridViewTextBoxColumn5.HeaderText = "ID_CTA_CTBLE"
        GridViewTextBoxColumn5.IsVisible = False
        GridViewTextBoxColumn5.Name = "ID_CTA_CTBLE"
        GridViewTextBoxColumn6.FieldName = "CUENTA"
        GridViewTextBoxColumn6.HeaderText = "Cuenta Contable"
        GridViewTextBoxColumn6.Name = "CUENTA"
        GridViewTextBoxColumn6.Width = 200
        Me.rgListaDoc.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6})
        Me.rgListaDoc.Name = "rgListaDoc"
        Me.rgListaDoc.ReadOnly = True
        Me.rgListaDoc.Size = New System.Drawing.Size(678, 291)
        Me.rgListaDoc.TabIndex = 0
        Me.rgListaDoc.Text = "RadGridView1"
        '
        'MECO
        '
        Me.MECO.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MECO_MODIFICAR, Me.MECO_ELIMINAR})
        Me.MECO.Name = "MECO"
        Me.MECO.Size = New System.Drawing.Size(153, 70)
        '
        'MECO_MODIFICAR
        '
        Me.MECO_MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_MODIFICAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_MODIFICAR.Image = CType(resources.GetObject("MECO_MODIFICAR.Image"), System.Drawing.Image)
        Me.MECO_MODIFICAR.Name = "MECO_MODIFICAR"
        Me.MECO_MODIFICAR.Size = New System.Drawing.Size(152, 22)
        Me.MECO_MODIFICAR.Text = "Modificar"
        '
        'MECO_ELIMINAR
        '
        Me.MECO_ELIMINAR.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.MECO_ELIMINAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_ELIMINAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_ELIMINAR.Name = "MECO_ELIMINAR"
        Me.MECO_ELIMINAR.Size = New System.Drawing.Size(152, 22)
        Me.MECO_ELIMINAR.Text = "Eliminar"
        '
        'Page_Mantenimiento
        '
        Me.Page_Mantenimiento.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.Page_Mantenimiento.Controls.Add(Me.GroupBox1)
        Me.Page_Mantenimiento.Controls.Add(Me.btnCancelar)
        Me.Page_Mantenimiento.Controls.Add(Me.btnGurdar)
        Me.Page_Mantenimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Page_Mantenimiento.Location = New System.Drawing.Point(4, 22)
        Me.Page_Mantenimiento.Name = "Page_Mantenimiento"
        Me.Page_Mantenimiento.Padding = New System.Windows.Forms.Padding(3)
        Me.Page_Mantenimiento.Size = New System.Drawing.Size(678, 342)
        Me.Page_Mantenimiento.TabIndex = 1
        Me.Page_Mantenimiento.Text = "Mantenimiento"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.lblTexto)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cboCuentaCtble)
        Me.GroupBox1.Controls.Add(Me.txtNumeroDoc)
        Me.GroupBox1.Controls.Add(Me.cboDoc_Sunat)
        Me.GroupBox1.Controls.Add(Me.txtDescripcion)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(665, 179)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(6, 140)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(109, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Cuenta Contable :"
        '
        'lblTexto
        '
        Me.lblTexto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTexto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTexto.Location = New System.Drawing.Point(330, 91)
        Me.lblTexto.Name = "lblTexto"
        Me.lblTexto.Size = New System.Drawing.Size(329, 67)
        Me.lblTexto.TabIndex = 11
        Me.lblTexto.Text = "Label5"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(17, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nº Documento:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(28, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Descripción :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(31, 94)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Doc. Sunat :"
        '
        'cboCuentaCtble
        '
        Me.cboCuentaCtble.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentaCtble.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCuentaCtble.FormattingEnabled = True
        Me.cboCuentaCtble.Items.AddRange(New Object() {"1", "2"})
        Me.cboCuentaCtble.Location = New System.Drawing.Point(119, 137)
        Me.cboCuentaCtble.Name = "cboCuentaCtble"
        Me.cboCuentaCtble.Size = New System.Drawing.Size(205, 21)
        Me.cboCuentaCtble.TabIndex = 7
        '
        'txtNumeroDoc
        '
        Me.txtNumeroDoc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumeroDoc.Location = New System.Drawing.Point(119, 19)
        Me.txtNumeroDoc.Name = "txtNumeroDoc"
        Me.txtNumeroDoc.Size = New System.Drawing.Size(100, 20)
        Me.txtNumeroDoc.TabIndex = 4
        '
        'cboDoc_Sunat
        '
        Me.cboDoc_Sunat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDoc_Sunat.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDoc_Sunat.FormattingEnabled = True
        Me.cboDoc_Sunat.Location = New System.Drawing.Point(119, 91)
        Me.cboDoc_Sunat.Name = "cboDoc_Sunat"
        Me.cboDoc_Sunat.Size = New System.Drawing.Size(205, 21)
        Me.cboDoc_Sunat.TabIndex = 6
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(119, 53)
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(486, 20)
        Me.txtDescripcion.TabIndex = 5
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.SystemColors.HighlightText
        Me.btnCancelar.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Arrow_first_
        Me.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(420, 205)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(83, 31)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnGurdar
        '
        Me.btnGurdar.BackColor = System.Drawing.SystemColors.HighlightText
        Me.btnGurdar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnGurdar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnGurdar.Image = CType(resources.GetObject("btnGurdar.Image"), System.Drawing.Image)
        Me.btnGurdar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGurdar.Location = New System.Drawing.Point(204, 205)
        Me.btnGurdar.Name = "btnGurdar"
        Me.btnGurdar.Size = New System.Drawing.Size(85, 31)
        Me.btnGurdar.TabIndex = 8
        Me.btnGurdar.Text = "Guardar"
        Me.btnGurdar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGurdar.UseVisualStyleBackColor = False
        '
        'FrmDocumentosSunat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(694, 387)
        Me.Controls.Add(Me.TabPersonalizado1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "FrmDocumentosSunat"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Documentos Comerciales"
        Me.TabPersonalizado1.ResumeLayout(False)
        Me.Page_Consulta.ResumeLayout(False)
        CType(Me.rgListaDoc.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaDoc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MECO.ResumeLayout(False)
        Me.Page_Mantenimiento.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabPersonalizado1 As Controles_DHMont.TabPersonalizado
    Friend WithEvents Page_Consulta As System.Windows.Forms.TabPage
    Friend WithEvents Page_Mantenimiento As System.Windows.Forms.TabPage
    Friend WithEvents rgListaDoc As Telerik.WinControls.UI.RadGridView
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents cboCuentaCtble As System.Windows.Forms.ComboBox
    Friend WithEvents cboDoc_Sunat As System.Windows.Forms.ComboBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents txtNumeroDoc As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGurdar As System.Windows.Forms.Button
    Friend WithEvents MECO As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MECO_MODIFICAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MECO_ELIMINAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblTexto As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
End Class
