﻿Imports CapaEntidad.Documentos_Sunat
Imports BusinessLogicLayer.Documentos_Sunat
Imports CapaEntidad

Public Class FrmDocumentosSunat

    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim VL_SRV_DOC As SrvDocumentosSunat
    Dim OBJ_DOC As New BeanDocumentosSunat
    Dim VL_BOTON As String
    Public Const AvisoMSGBOX As String = "AVISO SISTEMA TESORERIA"

    Private Sub FrmDocumentosSunat_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TabPersonalizado1.SelectedIndex = 0
        TabPersonalizado1.DisablePage(Page_Mantenimiento)
        LISTAR_DOCUMENTOS_SUNAT()
        LISTAR_CUENTAS_CONTABLES()
        LISTAR_DOC_SUNAT()
    End Sub

#Region "LISTAR , AGREGAR, MODIFICAR Y ELIMINAR "

    Private Sub LISTAR_CUENTAS_CONTABLES()

        VL_SRV_DOC = New SrvDocumentosSunat()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_DOC.SRV_LISTAR_CUENTAS()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboCuentaCtble.DataSource = VL_BeanResultado.dtResultado
            Me.cboCuentaCtble.DisplayMember = "CUENTA"
            Me.cboCuentaCtble.ValueMember = "ID_CTA_CTBLE"
        End If
    End Sub

    Private Sub LISTAR_DOCUMENTOS_SUNAT()

        VL_SRV_DOC = New SrvDocumentosSunat()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_DOC.SRV_LISTAR_DOCUMENTOS_SUNAT()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboDoc_Sunat.DataSource = VL_BeanResultado.dtResultado
            Me.cboDoc_Sunat.DisplayMember = "Descripcion"
            Me.cboDoc_Sunat.ValueMember = "Cod_DocSunat"
        End If
    End Sub

    Private Sub LISTAR_DOC_SUNAT()

        VL_SRV_DOC = New SrvDocumentosSunat()
        'dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_DOC.SRV_LISTAR_DOC_SUNAT

        If VL_BeanResultado.blnExiste = True Then
            '    'Me.dgTablaImpuestos.AutoGenerateColumns = False
            Me.rgListaDoc.DataSource = VL_BeanResultado.dtResultado
        End If
    End Sub

    Private Sub AGREGAR_DOC_SUNAT()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        OBJ_DOC = New BeanDocumentosSunat(txtNumeroDoc.Text, txtDescripcion.Text, cboDoc_Sunat.SelectedValue.ToString(), cboCuentaCtble.SelectedValue.ToString(), 1)

        VL_BeanResultado = VL_SRV_DOC.SRV_AGREGAR_DOC_SUNAT(OBJ_DOC)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub MODIFICAR_DOC_SUNAT()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        OBJ_DOC = New BeanDocumentosSunat(txtNumeroDoc.Text, txtDescripcion.Text, cboDoc_Sunat.SelectedValue.ToString(), cboCuentaCtble.SelectedValue.ToString(), 2)

        VL_BeanResultado = VL_SRV_DOC.SRV_ACTUALIZAR_DOC_SUNAT(OBJ_DOC)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

    Private Sub ELIMINAR_DOC_SUNAT()

       Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim ID_DOC As String
        ID_DOC = rgListaDoc.CurrentRow.Cells("ID_DOCUMENTO").Value
        OBJ_DOC = New BeanDocumentosSunat(ID_DOC, "", "", "", 3)

        VL_BeanResultado = VL_SRV_DOC.SRV_ELIMINAR_DOC_SUNAT(OBJ_DOC)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub
#End Region

    Sub limpiarcontroles()
        txtNumeroDoc.Text = ""
        txtDescripcion.Text = ""
        lblTexto.Text = ""
        cboCuentaCtble.SelectedIndex = 0
        cboDoc_Sunat.SelectedIndex = 0
    End Sub
#Region "MECO"
    Private Sub MECO_MODIFICAR_Click(sender As Object, e As EventArgs) Handles MECO_MODIFICAR.Click
        VL_BOTON = "1"
        txtNumeroDoc.Enabled = False
        TabPersonalizado1.SelectedIndex = 1
        TabPersonalizado1.EnablePage(Page_Mantenimiento)

        txtNumeroDoc.Text = rgListaDoc.CurrentRow.Cells("ID_DOCUMENTO").Value
        txtDescripcion.Text = rgListaDoc.CurrentRow.Cells("DESCRIPCION").Value
        cboDoc_Sunat.SelectedValue = rgListaDoc.CurrentRow.Cells("Cod_DocSunat").Value
        cboCuentaCtble.SelectedValue = rgListaDoc.CurrentRow.Cells("ID_CTA_CTBLE").Value
    End Sub

    Private Sub MECO_ELIMINAR_Click(sender As Object, e As EventArgs) Handles MECO_ELIMINAR.Click

        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR_DOC_SUNAT()
            LISTAR_DOC_SUNAT()

        End If
    End Sub
#End Region


#Region "BOTONES"
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        TabPersonalizado1.SelectedIndex = 1
        TabPersonalizado1.EnablePage(Page_Mantenimiento)
        VL_BOTON = "0"
    End Sub

    Private Sub btnGurdar_Click(sender As Object, e As EventArgs) Handles btnGurdar.Click
        If (VL_BOTON = "0") Then  ''AGREGAR IMPUESTO

            If txtNumeroDoc.Text = "" Then

                MessageBox.Show("Ingrese Numero de Documento...", "SISTEMA TESORERIA")

            ElseIf txtDescripcion.Text = "" Then
                MessageBox.Show("Ingrese una Descripción...", "SISTEMA TESORERIA")

            Else
                AGREGAR_DOC_SUNAT()
                LISTAR_DOC_SUNAT()
                limpiarcontroles()
                TabPersonalizado1.SelectedIndex = 0
            End If

        End If

        If (VL_BOTON = "1") Then ''EDITAR IMPUESTO

            If txtNumeroDoc.Text = "" Then

                MessageBox.Show("Ingrese Numero de Documento...", "SISTEMA TESORERIA")

            ElseIf txtDescripcion.Text = "" Then

                MessageBox.Show("Ingrese una Descripción...", "SISTEMA TESORERIA")
            Else
                MODIFICAR_DOC_SUNAT()
                LISTAR_DOC_SUNAT()
                limpiarcontroles()
                TabPersonalizado1.SelectedIndex = 0
                txtNumeroDoc.Enabled = True
            End If
        End If
        ''VL_BOTON = ""
    End Sub
    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        TabPersonalizado1.DisablePage(Page_Mantenimiento)
        TabPersonalizado1.SelectedIndex = 0
        limpiarcontroles()
        txtNumeroDoc.Enabled = True
    End Sub
#End Region

#Region "EVENTO SELECTED VALUE"
    Private Sub cboDoc_Sunat_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboDoc_Sunat.SelectedValueChanged
        Dim TEXTO3 As String
        TEXTO3 = cboDoc_Sunat.Text
        lblTexto.Text = TEXTO3
    End Sub

#End Region


#Region "VALIDACION DE SOLO LETRAS Y NUMEROS"
    Public Sub SoloLetras(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Public Sub SoloNumeros(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
            e.Handled = True
        Else
            e.Handled = False
        End If
    End Sub
#End Region



    Private Sub txtNumeroDoc_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumeroDoc.KeyPress
        SoloNumeros(txtNumeroDoc.Text, e)
    End Sub
End Class