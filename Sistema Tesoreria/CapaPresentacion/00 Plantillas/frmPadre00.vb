Imports System.Windows.Forms
Imports System.Windows.Forms.TabControl
Imports Controles_DHMont.TabPersonalizado

Public Class frmPadre00

#Region "Eventos de formularios hijos"
    Public Event NuevoReg()
    Public Event ModificaReg()
    Public Event EliminaReg()
    Public Event GrabaReg()
    Public Event SalidaReg()
    Public Event CancelarReg()
    Public Event ImprimeReg()
    Public Event BuscaReg()
    Public Event ExportaReg()
    Public Event VisualizaReg()
#End Region

#Region "Propiedades y Variables"
    Private nNuevo As Boolean
    Private nModificar As Boolean
    Private nEliminar As Boolean
    Private nGrabar As Boolean
    Private nCancelar As Boolean
    Private nBuscar As Boolean
    Private nExportar As Boolean
    Private nImprimir As Boolean
    Private nVisualizar As Boolean
    Private nSalir As Boolean
    Private nCOntrol As Object

    Public Property Nuevo() As Boolean
        Get
            Return nNuevo
        End Get
        Set(ByVal value As Boolean)
            nNuevo = value
        End Set
    End Property

    Public Property Modificar() As Boolean
        Get
            Return nModificar
        End Get
        Set(ByVal value As Boolean)
            nModificar = value
        End Set
    End Property

    Public Property Visualizar() As Boolean
        Get
            Return nVisualizar
        End Get
        Set(ByVal value As Boolean)
            nVisualizar = value
        End Set
    End Property

    Public Property Eliminar() As Boolean
        Get
            Return nEliminar
        End Get
        Set(ByVal value As Boolean)
            nEliminar = value
        End Set
    End Property

    Public Property Grabar() As Boolean
        Get
            Return nGrabar
        End Get
        Set(ByVal value As Boolean)
            nGrabar = value
        End Set
    End Property

    Public Property Buscar() As Boolean
        Get
            Return nBuscar
        End Get
        Set(ByVal value As Boolean)
            nBuscar = value
        End Set
    End Property

    Public Property Exportar() As Boolean
        Get
            Return nExportar
        End Get
        Set(ByVal value As Boolean)
            nExportar = value
        End Set
    End Property

    Public Property Imprimir() As Boolean
        Get
            Return nImprimir
        End Get
        Set(ByVal value As Boolean)
            nImprimir = value
        End Set
    End Property

    Public Property Salir() As Boolean
        Get
            Return nSalir
        End Get
        Set(ByVal value As Boolean)
            nSalir = value
        End Set
    End Property

    Public Property Cancelar() As Boolean
        Get
            Return nCancelar
        End Get
        Set(ByVal value As Boolean)
            nCancelar = value
        End Set
    End Property

    Public Property CControl() As Object
        Get
            Return nCOntrol
        End Get
        Set(ByVal value As Object)
            nCOntrol = value
        End Set
    End Property

#End Region

    Public Sub vNuevoRegistro()
        RaiseEvent NuevoReg()
    End Sub

    Public Sub vModificaRegistro()
        RaiseEvent ModificaReg()
    End Sub

    Public Sub vEliminaRegistro()
        RaiseEvent EliminaReg()
    End Sub

    Public Sub vGrabaRegistro()
        RaiseEvent GrabaReg()
    End Sub

    Public Sub vImprimeRegistro()
        RaiseEvent ImprimeReg()
    End Sub

    Public Sub vBuscaRegistro()
        RaiseEvent BuscaReg()
    End Sub

    Public Sub vExportaRegistro()
        RaiseEvent ExportaReg()
    End Sub

    Public Sub vVisualizaRegistro()
        RaiseEvent VisualizaReg()
    End Sub

    Public Sub vSalir()
        RaiseEvent SalidaReg()
    End Sub

    Public Sub vCancelar()
        RaiseEvent CancelarReg()
    End Sub

    Public Sub Activo(ByVal menu As ToolStrip)
        menu.Items(0).Enabled = Nuevo
        menu.Items(1).Enabled = Modificar
        menu.Items(2).Enabled = Eliminar
        menu.Items(3).Enabled = Grabar
        menu.Items(4).Enabled = Cancelar

        menu.Items(8).Enabled = Buscar
        menu.Items(9).Enabled = Exportar
        menu.Items(10).Enabled = Imprimir
        menu.Items(11).Enabled = Visualizar
        menu.Items(13).Enabled = Salir
    End Sub

    Private Sub frmPadre00_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'MessageBox.Show(sender.ToString.Trim)
        If TabControl1.SelectedIndex = 0 Then
            Nuevo = True
            If sender.ToString.Trim = "CapaPresentacionTesoreria.frmCheque, Text: Cheques" Then
                Nuevo = False
            End If

            Modificar = True
            Eliminar = True
            Grabar = False
            Cancelar = False
            Visualizar = True
            Buscar = True
            Exportar = True
            Imprimir = True
            Salir = True
            'If frmCheque.NuevoNo = 1 Then
            '    Nuevo = False
            'End If
            If frmPrincipal.fDatos = True Then
                Modificar = True
                Visualizar = True
                Eliminar = True
            Else
                'Cuando el Numero de Registros de un Mantenimiento es 0
                Nuevo = True
                If sender.ToString.Trim = "CapaPresentacionTesoreria.frmCheque, Text: Cheques" Then
                    Nuevo = False
                End If
                Modificar = False
                Eliminar = False
                Grabar = False
                Cancelar = False
                Visualizar = False
                Buscar = False
                Exportar = False
                Imprimir = False
                Visualizar = False
                Salir = True
            End If
        Else
            'If frmCheque.NuevoNo = 1 Then
            '    Nuevo = False
            'End If
            Nuevo = False
            If sender.ToString.Trim = "CapaPresentacionTesoreria.frmCheque, Text: Cheques" Then
                Nuevo = False
            End If
            Modificar = False
            Eliminar = False
            Grabar = True
            Cancelar = True
            Visualizar = False
            Buscar = False
            Exportar = False
            Imprimir = False
            Salir = True
        End If
        Activo(CControl)
        TabControl1.TabPages(0).Focus()
    End Sub

    Private Sub frmPadre00_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not Me.ActiveMdiChild Is Nothing Then
            CType(Me.ActiveMdiChild, CapaPreTesoreria.frmPadre00).vSalir()
            If Me.ActiveMdiChild Is Nothing Then
                Nuevo = False
                Modificar = False
                Visualizar = False
                Eliminar = False
                Grabar = False
                Buscar = False
                Exportar = False
                Imprimir = False
                Salir = False
                Cancelar = False
                Activo(CControl)
            End If
        Else
            Nuevo = False
            Modificar = False
            Visualizar = False
            Eliminar = False
            Grabar = False
            Buscar = False
            Exportar = False
            Imprimir = False
            Salir = False
            Cancelar = False
            Activo(CControl)
        End If
    End Sub

    Private Sub frmPadre00_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmPrincipal.sFlagEdicion = "0" 'Nueva Modificacion
        frmPrincipal.sFlagVisualizar = "0" 'Ultima Modificacion
        TabControl1.DrawMode = TabDrawMode.OwnerDrawFixed
        Left = 0 : Top = 0
    End Sub
End Class