﻿Imports CapaEntidad.Cobranza
Imports BusinessLogicLayer.Cobranza
Imports CapaEntidad
Imports BusinessEntityLayer.Login
Imports System.Math


Public Class frmCobranza

#Region "DECLARACIONES"
    Dim OBJ_COBRANZA As New BeanCobranza
    Dim OBJ_COBRANZA_DETALLE As New BeanDetalleCobranza
    Dim VL_SRV_COBRANZA As SrvCobranza
    Dim VL_SRV_COBRANZA_DETALLE As SrvDetalleCobranza
    Dim dtTable As DataTable
    Dim VL_BOTON As String
    Dim VL_BOTON_DETALLES As String
    Public Const AvisoMSGBOX As String = "SISTEMA TESORERIA"
#End Region


    Private Sub frmCobranza_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LISTAR_COBRANZA()
        LISTAR_CLIENTES(rgListaCobranzas.CurrentRow.Cells("Id_Obra").Value.ToString() + "")
        LISTAR_MONEDAS()
        LISTAR_BANCOS()

        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = False
        dtFecha.Value = Now
    End Sub

#Region "LISTAR COMBOS"
    Private Sub LISTAR_MONEDAS()

        VL_SRV_COBRANZA = New SrvCobranza()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_COBRANZA.SRV_LISTAR_MONEDAS()
        If VL_BeanResultado.blnExiste = True Then
            Me.cboMonedas.DataSource = VL_BeanResultado.dtResultado
            Me.cboMonedas.DisplayMember = "MonDescripcion"
            Me.cboMonedas.ValueMember = "MonCodigo"

        End If
    End Sub

    Private Sub LISTAR_CLIENTES(Id_Obra As String)

        VL_SRV_COBRANZA = New SrvCobranza()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_SRV_COBRANZA.SRV_LISTAR_CLIENTES(Id_Obra)

        If VL_BeanResultado.blnExiste = True Then
            Me.cboClientes.DataSource = VL_BeanResultado.dtResultado
            Me.cboClientes.DisplayMember = "NOMBRES"
            Me.cboClientes.ValueMember = "CLIE_CODIGO"
        End If
    End Sub

    Private Sub LISTAR_BANCOS()

        VL_SRV_COBRANZA = New SrvCobranza()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim EMPRESA_CODIGO As String = gEmpresa
        VL_BeanResultado = VL_SRV_COBRANZA.SRV_LISTAR_BANCOS(EMPRESA_CODIGO)

        If VL_BeanResultado.blnExiste = True Then

            Me.cboBancos.DataSource = VL_BeanResultado.dtResultado
            Me.cboBancos.DisplayMember = "NombreBanco"
            Me.cboBancos.ValueMember = "IdBanco"


        End If
    End Sub

    Private Sub LISTAR_CUENTA_BANCOS(ID_BANCO As String)

        VL_SRV_COBRANZA = New SrvCobranza()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim EMPRESA_CODIGO As String = gEmpresa
        'Dim ID_BANCO As String = cboBancos.SelectedValue
        VL_BeanResultado = VL_SRV_COBRANZA.SRV_LISTAR_CUENTAS_BANCOS(EMPRESA_CODIGO, ID_BANCO)

        If VL_BeanResultado.blnExiste = True Then

            Me.cboCtaCorriente.DataSource = VL_BeanResultado.dtResultado
            Me.cboCtaCorriente.DisplayMember = "Observacion"
            Me.cboCtaCorriente.ValueMember = "IdCuenta"


        End If

    End Sub

    Private Sub LISTAR_CLIENTES_DETALLES(ID As String, ID_OBRA As String)

        VL_SRV_COBRANZA = New SrvCobranza()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        VL_BeanResultado = VL_SRV_COBRANZA.SRV_LISTAR_CLIENTES_DETALLES(ID, ID_OBRA)

        If VL_BeanResultado.blnExiste = True Then

            dtTable = VL_BeanResultado.dtResultado

            txtTipoDocumento.Text = dtTable.Rows(0)(2).ToString()
            txtDoc.Text = dtTable.Rows(0)(3).ToString()
            txtDireccion.Text = dtTable.Rows(0)(4).ToString()

        End If
    End Sub

#End Region

#Region "MANTENIMIENTO COBRANZA  LISTAR ,INSERT, UPDATE, DELETE"
    Private Sub LISTAR_COBRANZA()

        VL_SRV_COBRANZA = New SrvCobranza()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()
        Dim EMPRESA_CODIGO As String = gEmpresa
        Dim ID_COBRANZA As Int32 = 0

        VL_BeanResultado = VL_SRV_COBRANZA.SRV_LISTAR_COBRANZAS(EMPRESA_CODIGO, ID_COBRANZA)

        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaCobranzas.DataSource = VL_BeanResultado.dtResultado
            txtNumCobranza.Text = ID_COBRANZA
        End If
    End Sub

    Private Sub AGREGAR_COBRANZA()

        VL_SRV_COBRANZA = New SrvCobranza()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim CLIENTES As String = cboClientes.SelectedValue.ToString()
        Dim MONEDA As String = cboMonedas.SelectedValue.ToString()

        Dim BANCOS As String = cboBancos.SelectedValue.ToString()
        Dim CTA_CORRIENTE As String = cboCtaCorriente.SelectedValue.ToString()

        Dim USUARIO As String = gUsuario
        Dim TC As Decimal = IIf(txtTC.Text = "", 0, txtTC.Text)
        Dim TOTAL As Decimal = IIf(txtTotal.Text = "", 0D, txtTotal.Text)

        'OBJ_COBRANZA = New BeanCobranza(txtNumCobranza.Text, CLIENTES, dtFecha.Value.ToString(), TC, BANCOS, CTA_CORRIENTE, txtNumOperacion.Text,
        '                                txtSerie.Text, txtNumero.Text, MONEDA, txtTotal.Text, txtGlosa.Text, 1, 0, "", USUARIO, 1)
        OBJ_COBRANZA = New BeanCobranza(CLIENTES, dtFecha.Value.ToString(), TC, BANCOS, CTA_CORRIENTE, txtNumOperacion.Text,
                                        txtSerie.Text, txtNumero.Text, MONEDA, TOTAL, txtGlosa.Text, 1, 0, "", USUARIO, 1)

        VL_BeanResultado = VL_SRV_COBRANZA.SRV_AGREGAR_COBRANZA(OBJ_COBRANZA)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ACTUALIZAR_COBRANZA()

        VL_SRV_COBRANZA = New SrvCobranza()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim CLIENTES As String = cboClientes.SelectedValue.ToString()
        Dim MONEDA As String = cboMonedas.SelectedValue.ToString()

        Dim BANCOS As String = cboBancos.SelectedValue.ToString()
        Dim CTA_CORRIENTE As String = cboCtaCorriente.SelectedValue.ToString()

        Dim USUARIO As String = gUsuario

        OBJ_COBRANZA = New BeanCobranza(txtNumCobranza.Text, CLIENTES, dtFecha.Value.ToString(), txtTC.Text, BANCOS, CTA_CORRIENTE, txtNumOperacion.Text,
                                        txtSerie.Text, txtNumero.Text, MONEDA, txtTotal.Text, txtGlosa.Text, 0, 0, "", USUARIO, 2)


        VL_BeanResultado = VL_SRV_COBRANZA.SRV_ACTUALIZAR_COBRANZA(OBJ_COBRANZA)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If


    End Sub

    Private Sub ELIMINAR_COBRANZA()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim ID As Int16 = rgListaCobranzas.CurrentRow.Cells("ID_DocVenta").Value
        OBJ_COBRANZA.ID_COBRANZA = ID
        OBJ_COBRANZA = New BeanCobranza(ID, "", "", "", "", "", "", "", "", "", 0, "", 0, 0, "", "", 3)

        VL_BeanResultado = VL_SRV_COBRANZA.SRV_ELIMINAR_COBRANZA(OBJ_COBRANZA)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

#End Region

#Region "MANTENIMIENTO COBRANZA DETALLE INSERT, UPDATE Y DELETE"
    Private Sub LISTAR_COBRANZA_DETALLE()

        VL_SRV_COBRANZA_DETALLE = New SrvDetalleCobranza()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        'Dim ID As Int16 = rgListaCobranzas.CurrentRow.Cells("ID_Cobranza").Value
        Dim ID As Int16 = txtNumCobranzaDetalles.Text
        Dim ID_COBRANZA_DETALLE As Int32 = 0


        VL_BeanResultado = VL_SRV_COBRANZA_DETALLE.SRV_LISTAR_COBRANZA_DETALLE(ID, ID_COBRANZA_DETALLE)



        If VL_BeanResultado.blnExiste = True Then
            Me.rgListaCobranzaDetalle.DataSource = VL_BeanResultado.dtResultado
            txtCDetalle.Text = ID_COBRANZA_DETALLE
        End If
    End Sub

    Private Sub AGREGAR_COBRANZA_DETALLE()

        VL_SRV_COBRANZA_DETALLE = New SrvDetalleCobranza()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()


        Dim IMPORTE As Decimal = IIf(txtImporte.Text = "", 0, txtImporte.Text)

        'OBJ_COBRANZA_DETALLE = New BeanDetalleCobranza(txtCDetalle.Text, txtNumCobranzaDetalles.Text, txtDocVenta.Text, IMPORTE, 1)
        OBJ_COBRANZA_DETALLE = New BeanDetalleCobranza(txtNumCobranzaDetalles.Text, txtDocVenta.Text, IMPORTE, 1)


        VL_BeanResultado = VL_SRV_COBRANZA_DETALLE.SRV_AGREGAR_COBRANZA_DETALLE(OBJ_COBRANZA_DETALLE)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ACTUALIZAR_COBRANZA_DETALLE()

        VL_SRV_COBRANZA_DETALLE = New SrvDetalleCobranza()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim ID_COBRANZA_DET As Int16 = rgListaCobranzaDetalle.CurrentRow.Cells("ID_CobranzaDetalle").Value
        txtCDetalle.Text = ID_COBRANZA_DET
        Dim IMPORTE As Decimal = IIf(txtImporte.Text = "", 0, txtImporte.Text)

        OBJ_COBRANZA_DETALLE = New BeanDetalleCobranza(ID_COBRANZA_DET, txtNumCobranzaDetalles.Text, txtDocVenta.Text, IMPORTE, 2)


        VL_BeanResultado = VL_SRV_COBRANZA_DETALLE.SRV_ACTUALIZAR_COBRANZA_DETALLE(OBJ_COBRANZA_DETALLE)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub ELIMINAR_COBRANZA_DETALLE()

        VL_SRV_COBRANZA_DETALLE = New SrvDetalleCobranza()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim ID_COBRANZA_DET As Int16 = rgListaCobranzaDetalle.CurrentRow.Cells("ID_CobranzaDetalle").Value
        OBJ_COBRANZA_DETALLE.ID_COBRANZA_DETALLE = ID_COBRANZA_DET
        'txtCDetalle.Text = ID_COBRANZA_DET
        Dim IMPORTE As Decimal = IIf(txtImporte.Text = "", 0, txtImporte.Text)

        OBJ_COBRANZA_DETALLE = New BeanDetalleCobranza(ID_COBRANZA_DET, 0, 0, 0, 3)


        VL_BeanResultado = VL_SRV_COBRANZA_DETALLE.SRV_ELIMINAR_COBRANZA_DETALLE(OBJ_COBRANZA_DETALLE)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

#End Region

#Region "BOTONES"
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        VL_BOTON = "0"
        TabControl1.SelectedIndex = 1
        GRUPO_COB_DETALLES.Enabled = False
        GRUPO_COBRANZAS.Enabled = True
        btnAgregarDetalles.Enabled = False
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = True
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = False
      
        rgListaCobranzaDetalle.DataSource = ""
        LIMPIAR_CONTROLES()
        LISTAR_COBRANZA()
    End Sub
    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If (VL_BOTON = "0") Then  ''AGREGAR

            'If txtSerie.Text = "" Then

            '    MessageBox.Show("Ingrese Serie..'", "SISTEMA TESORERIA")

            'ElseIf txtNumero.Text = "" Then
            '    MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
            'ElseIf txtImpuesto.Text = "" Then
            '    MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            'ElseIf txtTotal.Text = "" Then
            '    MessageBox.Show("Ingrese Total...", "SISTEMA TESORERIA")
            'ElseIf txtNeto.Text = "" Then
            '    MessageBox.Show("Ingrese Neto...", "SISTEMA TESORERIA")
            'ElseIf txtDescuento.Text = "" Then
            '    MessageBox.Show("Ingrese Descuento...", "SISTEMA TESORERIA")
            'ElseIf txtSubTotal.Text = "" Then
            '    MessageBox.Show("Ingrese SubTotal...", "SISTEMA TESORERIA")
            'Else

            AGREGAR_COBRANZA()
            'LISTAR_COBRANZA()
            GRUPO_COB_DETALLES.Enabled = False
            TabControl1.SelectedIndex = 0
            'End If

        End If

        If (VL_BOTON = "1") Then ''ACTUALIZAR 
            'If txtSerie.Text = "" Then

            '    MessageBox.Show("Ingrese Serie..'")

            'ElseIf txtNumero.Text = "" Then
            '    MessageBox.Show("Ingrese Número...", "SISTEMA TESORERIA")
            '    '    'ElseIf txtObservaciones.Text = "" Then
            '    '    MessageBox.Show("Ingrese Observaciones...", "SISTEMA TESORERIA")
            'ElseIf txtImpuesto.Text = "" Then
            '    MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            'ElseIf txtTotal.Text = "" Then
            '    MessageBox.Show("Ingrese Total...", "SISTEMA TESORERIA")
            'ElseIf txtNeto.Text = "" Then
            '    MessageBox.Show("Ingrese Neto...", "SISTEMA TESORERIA")
            'ElseIf txtDescuento.Text = "" Then
            '    MessageBox.Show("Ingrese Descuento...", "SISTEMA TESORERIA")
            'ElseIf txtSubTotal.Text = "" Then
            '    MessageBox.Show("Ingrese SubTotal...", "SISTEMA TESORERIA")
            'Else


            ACTUALIZAR_COBRANZA()


            GRUPO_COB_DETALLES.Enabled = True

            'End If
        End If

        LISTAR_COBRANZA()
        TabControl1.SelectedIndex = 0
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = False
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = True
        LIMPIAR_CONTROLES()

        txtNumCobranza.Enabled = True
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        TabControl1.SelectedIndex = 0
        GRUPO_COB_DETALLES.Enabled = True
        GRUPO_COBRANZAS.Enabled = True
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = True
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = False
        LIMPIAR_CONTROLES()
    End Sub
    Private Sub btnAgregarDetalles_Click(sender As Object, e As EventArgs) Handles btnAgregarDetalles.Click
        GRUPO_COB_DETALLES.Enabled = True
        GRUPO_COBRANZAS.Enabled = False
        VL_BOTON_DETALLES = "3"
    End Sub
    Private Sub btnGuardarDetalles_Click(sender As Object, e As EventArgs) Handles btnGuardarDetalles.Click
        If (VL_BOTON_DETALLES = "3") Then  ''AGREGAR 

            'If txtSerie.Text = "" Then
            '    MessageBox.Show("Ingrese Serie..'", "SISTEMA TESORERIA")
            'Else
            AGREGAR_COBRANZA_DETALLE()
            'LISTAR_COBRANZA_DETALLE()
            'End If

        End If

        If (VL_BOTON_DETALLES = "4") Then ''ACTUALIZAR 
            'If txtSerie.Text = "" Then
            '    MessageBox.Show("Ingrese Serie..'")
            'Else

            ACTUALIZAR_COBRANZA_DETALLE()

            GRUPO_COB_DETALLES.Enabled = True

            'End If
        End If
        LISTAR_COBRANZA_DETALLE()
        GRUPO_COB_DETALLES.Enabled = False
        GRUPO_COBRANZAS.Enabled = True
        LIMPIAR_DETALLES()
        TOTAL()
    End Sub

    Private Sub btnCancelarDetalles_Click(sender As Object, e As EventArgs) Handles btnCancelarDetalles.Click
        GRUPO_COB_DETALLES.Enabled = False
        GRUPO_COBRANZAS.Enabled = True
        LIMPIAR_DETALLES()

    End Sub
#End Region



#Region "SELECCION COMBO"
    Private Sub cboBancos_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboBancos.SelectedValueChanged

        If cboBancos.SelectedValue IsNot Nothing Then
            If cboBancos.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                Dim ID_BANCO As String = cboBancos.SelectedValue
                LISTAR_CUENTA_BANCOS(ID_BANCO)

            End If
        End If
    End Sub

#End Region

#Region "MENU CONTEXTUAL"
    Private Sub MECO_MODIFICAR_Click(sender As Object, e As EventArgs) Handles MECO_MODIFICAR.Click

    End Sub
    Private Sub MECO_ELIMINAR_Click(sender As Object, e As EventArgs) Handles MECO_ELIMINAR.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR_COBRANZA()
            LISTAR_COBRANZA()
        End If
    End Sub

    Private Sub MECO_UPDATE_Click(sender As Object, e As EventArgs) Handles MECO_UPDATE.Click
        VL_BOTON_DETALLES = "4"
        GRUPO_COB_DETALLES.Enabled = True
        GRUPO_COBRANZAS.Enabled = False

        txtCDetalle.Text = rgListaCobranzaDetalle.CurrentRow.Cells("ID_CobranzaDetalle").Value '' = "", 0D, rgListaCobranzaDetalle.CurrentRow.Cells("ID_CobranzaDetalle").Value)
        txtDocVenta.Text = rgListaCobranzaDetalle.CurrentRow.Cells("ID_DocVenta").Value ''= "", 0D, rgListaCobranzaDetalle.CurrentRow.Cells("ID_DocVenta").Value)
        txtNumCobranzaDetalles.Text = rgListaCobranzaDetalle.CurrentRow.Cells("ID_Cobranza").Value ''= "", 0, rgListaCobranzaDetalle.CurrentRow.Cells("ID_Cobranza").Value)
        txtImporte.Text = rgListaCobranzaDetalle.CurrentRow.Cells("Importe").Value '' = "", 0D, rgListaCobranzaDetalle.CurrentRow.Cells("Importe").Value)
    End Sub

    Private Sub MECO_DELETE_Click(sender As Object, e As EventArgs) Handles MECO_DELETE.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR_COBRANZA_DETALLE()
            LISTAR_COBRANZA_DETALLE()
        End If
    End Sub

#End Region

#Region "CELL DOUBLE CLICK GRILLA"

    Private Sub rgListaCobranzas_CellDoubleClick(sender As Object, e As Telerik.WinControls.UI.GridViewCellEventArgs) Handles rgListaCobranzas.CellDoubleClick
        VL_BOTON = "1"
        GRUPO_COB_DETALLES.Enabled = False
        btnAgregarDetalles.Enabled = True
        Me.TabControl1.TabPages("TAB_MANTENIMIENTO").Enabled = True
        Me.TabControl1.TabPages("TAB_CONSULTA").Enabled = False

        LIMPIAR_CONTROLES()
        'LIMPIAR_DETALLES()

        TabControl1.SelectedIndex = 1
        txtNumCobranza.Enabled = False
        txtNumCobranza.Text = rgListaCobranzas.CurrentRow.Cells("ID_Cobranza").Value
        cboClientes.SelectedValue = rgListaCobranzas.CurrentRow.Cells("ID_Cliente").Value
        dtFecha.Value = rgListaCobranzas.CurrentRow.Cells("Fecha").Value + ""
        txtTC.Text = rgListaCobranzas.CurrentRow.Cells("TC").Value + 0
        cboBancos.SelectedValue = rgListaCobranzas.CurrentRow.Cells("IdBanco").Value
        cboCtaCorriente.SelectedValue = rgListaCobranzas.CurrentRow.Cells("IdCtaCorriente").Value
        txtNumOperacion.Text = rgListaCobranzas.CurrentRow.Cells("NroOperacion").Value + ""
        txtSerie.Text = rgListaCobranzas.CurrentRow.Cells("Serie").Value + ""
        txtNumero.Text = rgListaCobranzas.CurrentRow.Cells("Numero").Value + ""
        cboMonedas.SelectedValue = rgListaCobranzas.CurrentRow.Cells("ID_Moneda").Value
        txtTotal.Text = Convert.ToDecimal(rgListaCobranzas.CurrentRow.Cells("Total").Value)
        txtGlosa.Text = rgListaCobranzas.CurrentRow.Cells("Glosa").Value

        txtNumCobranzaDetalles.Text = rgListaCobranzas.CurrentRow.Cells("ID_Cobranza").Value ''SE LLENA PARA AGREGAR LA VENTA LINEA

        LISTAR_COBRANZA_DETALLE()
        'LISTAR_COBRANZA()
        LISTAR_CLIENTES_DETALLES(rgListaCobranzas.CurrentRow.Cells("ID_Cliente").Value.ToString(), rgListaCobranzas.CurrentRow.Cells("Id_Obra").Value.ToString() + "")
        TOTAL()
    End Sub

#End Region

    Private Sub TOTAL()
        Dim neto As Decimal = 0D

        For i As Integer = 0 To rgListaCobranzaDetalle.Rows.Count - 1

            If rgListaCobranzaDetalle.Rows(i).Cells("Importe").Value <> -1 Then
                neto += Convert.ToDecimal(rgListaCobranzaDetalle.Rows(i).Cells("Importe").Value) + 0
            End If
        Next

        txtTotal.Text = Round(CDec(neto), 2, MidpointRounding.ToEven)

    End Sub

#Region "LIMPIAR CONTROLES"

    Sub LIMPIAR_CONTROLES()
        cboClientes.SelectedIndex = 0
        cboBancos.SelectedIndex = 0
        cboMonedas.SelectedIndex = 0
        cboCtaCorriente.SelectedIndex = 0
        dtFecha.Value = Now
        ''txtNumCobranza.Text = ""
        txtSerie.Text = ""
        txtNumero.Text = ""
        txtGlosa.Text = ""
        txtNumOperacion.Text = ""
        txtTC.Text = ""
        txtTotal.Text = ""
    End Sub
    Sub LIMPIAR_DETALLES()
        'txtCDetalle.Text = ""
        txtImporte.Text = ""
        'txtNumCobranzaDetalles.Text = ""
        'txtDocVenta.Text = ""

    End Sub
#End Region

    Private Sub cboClientes_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboClientes.SelectedValueChanged
        If cboClientes.SelectedValue IsNot Nothing Then
            If cboClientes.SelectedValue.ToString() <> "System.Data.DataRowView" Then
                Dim ID As String = cboClientes.SelectedValue
                Dim ID_OBRA As String = rgListaCobranzas.CurrentRow.Cells("Id_Obra").Value.ToString() + ""
                LISTAR_CLIENTES_DETALLES(ID, ID_OBRA)

            End If
        End If
    End Sub
End Class