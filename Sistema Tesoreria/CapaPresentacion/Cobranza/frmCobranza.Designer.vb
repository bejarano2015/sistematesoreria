﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCobranza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCobranza))
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn9 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn10 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn11 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn12 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn13 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn14 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn15 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn16 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn17 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn18 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn19 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn20 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn21 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn22 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn23 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn24 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn25 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn26 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn27 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn28 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn29 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn30 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.MECO_COBRANZAS = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MECO_MODIFICAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_ELIMINAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_COBRANZA_DETALLE = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MECO_UPDATE = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_DELETE = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1 = New Controles_DHMont.TabPersonalizado()
        Me.TAB_CONSULTA = New System.Windows.Forms.TabPage()
        Me.btnNuevo = New Telerik.WinControls.UI.RadButton()
        Me.rgListaCobranzas = New Telerik.WinControls.UI.RadGridView()
        Me.TAB_MANTENIMIENTO = New System.Windows.Forms.TabPage()
        Me.btnAgregarDetalles = New Telerik.WinControls.UI.RadButton()
        Me.RadGroupBox6 = New Telerik.WinControls.UI.RadGroupBox()
        Me.rgListaCobranzaDetalle = New Telerik.WinControls.UI.RadGridView()
        Me.GRUPO_COB_DETALLES = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel11 = New Telerik.WinControls.UI.RadLabel()
        Me.txtImporte = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel8 = New Telerik.WinControls.UI.RadLabel()
        Me.txtDocVenta = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel6 = New Telerik.WinControls.UI.RadLabel()
        Me.txtNumCobranzaDetalles = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel4 = New Telerik.WinControls.UI.RadLabel()
        Me.txtCDetalle = New Telerik.WinControls.UI.RadTextBox()
        Me.btnGuardarDetalles = New Telerik.WinControls.UI.RadButton()
        Me.btnCancelarDetalles = New Telerik.WinControls.UI.RadButton()
        Me.GRUPO_COBRANZAS = New Telerik.WinControls.UI.RadGroupBox()
        Me.btnCancelar = New Telerik.WinControls.UI.RadButton()
        Me.btnGuardar = New Telerik.WinControls.UI.RadButton()
        Me.bancos = New Telerik.WinControls.UI.RadGroupBox()
        Me.RadLabel13 = New Telerik.WinControls.UI.RadLabel()
        Me.cboBancos = New Telerik.WinControls.UI.RadDropDownList()
        Me.cboCtaCorriente = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel18 = New Telerik.WinControls.UI.RadLabel()
        Me.cboMonedas = New Telerik.WinControls.UI.RadDropDownList()
        Me.RadLabel12 = New Telerik.WinControls.UI.RadLabel()
        Me.RadGroupBox3 = New Telerik.WinControls.UI.RadGroupBox()
        Me.txtNumOperacion = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel2 = New Telerik.WinControls.UI.RadLabel()
        Me.txtTotal = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel29 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel10 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel3 = New Telerik.WinControls.UI.RadLabel()
        Me.txtGlosa = New Telerik.WinControls.UI.RadTextBox()
        Me.txtTC = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel1 = New Telerik.WinControls.UI.RadLabel()
        Me.txtNumCobranza = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel9 = New Telerik.WinControls.UI.RadLabel()
        Me.RadDateTimePicker1 = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.dtFecha = New Telerik.WinControls.UI.RadDateTimePicker()
        Me.RadLabel7 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel5 = New Telerik.WinControls.UI.RadLabel()
        Me.txtSerie = New Telerik.WinControls.UI.RadTextBox()
        Me.txtNumero = New Telerik.WinControls.UI.RadTextBox()
        Me.RadGroupBox2 = New Telerik.WinControls.UI.RadGroupBox()
        Me.txtDoc = New Telerik.WinControls.UI.RadTextBox()
        Me.txtTipoDocumento = New Telerik.WinControls.UI.RadTextBox()
        Me.txtDireccion = New Telerik.WinControls.UI.RadTextBox()
        Me.RadLabel27 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel25 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel24 = New Telerik.WinControls.UI.RadLabel()
        Me.RadLabel19 = New Telerik.WinControls.UI.RadLabel()
        Me.cboClientes = New Telerik.WinControls.UI.RadDropDownList()
        Me.MECO_COBRANZAS.SuspendLayout()
        Me.MECO_COBRANZA_DETALLE.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TAB_CONSULTA.SuspendLayout()
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaCobranzas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaCobranzas.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TAB_MANTENIMIENTO.SuspendLayout()
        CType(Me.btnAgregarDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox6.SuspendLayout()
        CType(Me.rgListaCobranzaDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgListaCobranzaDetalle.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GRUPO_COB_DETALLES, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRUPO_COB_DETALLES.SuspendLayout()
        CType(Me.RadLabel11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtImporte, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDocVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumCobranzaDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGuardarDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnCancelarDetalles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GRUPO_COBRANZAS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GRUPO_COBRANZAS.SuspendLayout()
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bancos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.bancos.SuspendLayout()
        CType(Me.RadLabel13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboBancos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboCtaCorriente, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboMonedas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox3.SuspendLayout()
        CType(Me.txtNumOperacion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTotal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtGlosa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumCobranza, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadLabel9.SuspendLayout()
        CType(Me.RadDateTimePicker1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtFecha, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtSerie, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtNumero, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.RadGroupBox2.SuspendLayout()
        CType(Me.txtDoc, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtTipoDocumento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtDireccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RadLabel19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cboClientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MECO_COBRANZAS
        '
        Me.MECO_COBRANZAS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MECO_MODIFICAR, Me.MECO_ELIMINAR})
        Me.MECO_COBRANZAS.Name = "MECO"
        Me.MECO_COBRANZAS.Size = New System.Drawing.Size(201, 48)
        '
        'MECO_MODIFICAR
        '
        Me.MECO_MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_MODIFICAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_MODIFICAR.Image = CType(resources.GetObject("MECO_MODIFICAR.Image"), System.Drawing.Image)
        Me.MECO_MODIFICAR.Name = "MECO_MODIFICAR"
        Me.MECO_MODIFICAR.Size = New System.Drawing.Size(200, 22)
        Me.MECO_MODIFICAR.Text = "Modificar Nº de Boleta"
        '
        'MECO_ELIMINAR
        '
        Me.MECO_ELIMINAR.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.MECO_ELIMINAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_ELIMINAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_ELIMINAR.Name = "MECO_ELIMINAR"
        Me.MECO_ELIMINAR.Size = New System.Drawing.Size(200, 22)
        Me.MECO_ELIMINAR.Text = "Eliminar"
        '
        'MECO_COBRANZA_DETALLE
        '
        Me.MECO_COBRANZA_DETALLE.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MECO_UPDATE, Me.MECO_DELETE})
        Me.MECO_COBRANZA_DETALLE.Name = "MECO"
        Me.MECO_COBRANZA_DETALLE.Size = New System.Drawing.Size(128, 48)
        '
        'MECO_UPDATE
        '
        Me.MECO_UPDATE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_UPDATE.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_UPDATE.Image = CType(resources.GetObject("MECO_UPDATE.Image"), System.Drawing.Image)
        Me.MECO_UPDATE.Name = "MECO_UPDATE"
        Me.MECO_UPDATE.Size = New System.Drawing.Size(127, 22)
        Me.MECO_UPDATE.Text = "Modificar"
        '
        'MECO_DELETE
        '
        Me.MECO_DELETE.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.MECO_DELETE.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_DELETE.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_DELETE.Name = "MECO_DELETE"
        Me.MECO_DELETE.Size = New System.Drawing.Size(127, 22)
        Me.MECO_DELETE.Text = "Eliminar"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TAB_CONSULTA)
        Me.TabControl1.Controls.Add(Me.TAB_MANTENIMIENTO)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(2, 3)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1072, 611)
        Me.TabControl1.TabIndex = 0
        '
        'TAB_CONSULTA
        '
        Me.TAB_CONSULTA.BackColor = System.Drawing.Color.Gainsboro
        Me.TAB_CONSULTA.Controls.Add(Me.btnNuevo)
        Me.TAB_CONSULTA.Controls.Add(Me.rgListaCobranzas)
        Me.TAB_CONSULTA.Location = New System.Drawing.Point(4, 22)
        Me.TAB_CONSULTA.Name = "TAB_CONSULTA"
        Me.TAB_CONSULTA.Padding = New System.Windows.Forms.Padding(3)
        Me.TAB_CONSULTA.Size = New System.Drawing.Size(1064, 585)
        Me.TAB_CONSULTA.TabIndex = 0
        Me.TAB_CONSULTA.Text = "Consulta"
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevo.Location = New System.Drawing.Point(921, 538)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(110, 24)
        Me.btnNuevo.TabIndex = 5
        Me.btnNuevo.Text = "Nuevo"
        '
        'rgListaCobranzas
        '
        Me.rgListaCobranzas.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rgListaCobranzas.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.rgListaCobranzas.ContextMenuStrip = Me.MECO_COBRANZAS
        Me.rgListaCobranzas.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgListaCobranzas.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rgListaCobranzas.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rgListaCobranzas.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgListaCobranzas.Location = New System.Drawing.Point(12, 9)
        '
        'rgListaCobranzas
        '
        Me.rgListaCobranzas.MasterTemplate.AllowAddNewRow = False
        Me.rgListaCobranzas.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgListaCobranzas.MasterTemplate.AllowColumnReorder = False
        Me.rgListaCobranzas.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn1.FieldName = "ID_Cobranza"
        GridViewTextBoxColumn1.HeaderText = "Nº Cobranza"
        GridViewTextBoxColumn1.Name = "ID_Cobranza"
        GridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn1.Width = 80
        GridViewTextBoxColumn2.FieldName = "ID_Cliente"
        GridViewTextBoxColumn2.HeaderText = "ID_Cliente"
        GridViewTextBoxColumn2.IsVisible = False
        GridViewTextBoxColumn2.Name = "ID_Cliente"
        GridViewTextBoxColumn3.FieldName = "NOMBRE_CLIENTE"
        GridViewTextBoxColumn3.HeaderText = "Nombre Cliente"
        GridViewTextBoxColumn3.Name = "NOMBRE_CLIENTE"
        GridViewTextBoxColumn3.Width = 220
        GridViewTextBoxColumn4.FieldName = "Fecha"
        GridViewTextBoxColumn4.FormatString = "{0:d}"
        GridViewTextBoxColumn4.HeaderText = "Fecha"
        GridViewTextBoxColumn4.Name = "Fecha"
        GridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn4.Width = 90
        GridViewTextBoxColumn5.AllowFiltering = False
        GridViewTextBoxColumn5.FieldName = "TC"
        GridViewTextBoxColumn5.HeaderText = "TC"
        GridViewTextBoxColumn5.Name = "TC"
        GridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn5.Width = 70
        GridViewTextBoxColumn6.FieldName = "IdBanco"
        GridViewTextBoxColumn6.HeaderText = "IdBanco"
        GridViewTextBoxColumn6.IsVisible = False
        GridViewTextBoxColumn6.Name = "IdBanco"
        GridViewTextBoxColumn7.FieldName = "NombreBanco"
        GridViewTextBoxColumn7.HeaderText = "Nombre Banco"
        GridViewTextBoxColumn7.Name = "NombreBanco"
        GridViewTextBoxColumn7.Width = 150
        GridViewTextBoxColumn8.AllowFiltering = False
        GridViewTextBoxColumn8.FieldName = "IdCtaCorriente"
        GridViewTextBoxColumn8.HeaderText = "IdCtaCorriente"
        GridViewTextBoxColumn8.IsVisible = False
        GridViewTextBoxColumn8.Name = "IdCtaCorriente"
        GridViewTextBoxColumn9.FieldName = "NumeroCuenta"
        GridViewTextBoxColumn9.HeaderText = "Número Cuenta"
        GridViewTextBoxColumn9.Name = "NumeroCuenta"
        GridViewTextBoxColumn9.Width = 150
        GridViewTextBoxColumn10.FieldName = "NroOperacion"
        GridViewTextBoxColumn10.HeaderText = "Nº Operación"
        GridViewTextBoxColumn10.Name = "NroOperacion"
        GridViewTextBoxColumn10.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn10.Width = 80
        GridViewTextBoxColumn11.FieldName = "Serie"
        GridViewTextBoxColumn11.HeaderText = "Serie"
        GridViewTextBoxColumn11.Name = "Serie"
        GridViewTextBoxColumn11.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn11.Width = 70
        GridViewTextBoxColumn12.FieldName = "Numero"
        GridViewTextBoxColumn12.HeaderText = "Número"
        GridViewTextBoxColumn12.Name = "Numero"
        GridViewTextBoxColumn12.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn12.Width = 100
        GridViewTextBoxColumn13.FieldName = "ID_Moneda"
        GridViewTextBoxColumn13.HeaderText = "ID_Moneda"
        GridViewTextBoxColumn13.IsVisible = False
        GridViewTextBoxColumn13.Name = "ID_Moneda"
        GridViewTextBoxColumn14.FieldName = "MonDescripcion"
        GridViewTextBoxColumn14.HeaderText = "Moneda"
        GridViewTextBoxColumn14.Name = "MonDescripcion"
        GridViewTextBoxColumn14.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn14.Width = 100
        GridViewTextBoxColumn15.AllowFiltering = False
        GridViewTextBoxColumn15.FieldName = "Total"
        GridViewTextBoxColumn15.HeaderText = "Total"
        GridViewTextBoxColumn15.Name = "Total"
        GridViewTextBoxColumn15.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn15.Width = 100
        GridViewTextBoxColumn16.AllowFiltering = False
        GridViewTextBoxColumn16.FieldName = "Glosa"
        GridViewTextBoxColumn16.HeaderText = "Glosa"
        GridViewTextBoxColumn16.Name = "Glosa"
        GridViewTextBoxColumn16.Width = 450
        GridViewTextBoxColumn17.FieldName = "Id_Obra"
        GridViewTextBoxColumn17.HeaderText = "Id_Obra"
        GridViewTextBoxColumn17.IsVisible = False
        GridViewTextBoxColumn17.Name = "Id_Obra"
        GridViewTextBoxColumn18.FieldName = "OBRA"
        GridViewTextBoxColumn18.HeaderText = "Obra"
        GridViewTextBoxColumn18.Name = "OBRA"
        GridViewTextBoxColumn18.Width = 200
        GridViewTextBoxColumn19.AllowFiltering = False
        GridViewTextBoxColumn19.FieldName = "Asiento"
        GridViewTextBoxColumn19.HeaderText = "Asiento"
        GridViewTextBoxColumn19.IsVisible = False
        GridViewTextBoxColumn19.Name = "Asiento"
        GridViewTextBoxColumn20.AllowFiltering = False
        GridViewTextBoxColumn20.FieldName = "AsientoOK"
        GridViewTextBoxColumn20.HeaderText = "AsientoOK"
        GridViewTextBoxColumn20.IsVisible = False
        GridViewTextBoxColumn20.Name = "AsientoOK"
        GridViewTextBoxColumn21.AllowFiltering = False
        GridViewTextBoxColumn21.FieldName = "AsientoMsg"
        GridViewTextBoxColumn21.HeaderText = "AsientoMsg"
        GridViewTextBoxColumn21.IsVisible = False
        GridViewTextBoxColumn21.Name = "AsientoMsg"
        GridViewTextBoxColumn22.FieldName = "ID_Usuario"
        GridViewTextBoxColumn22.HeaderText = "ID_Usuario"
        GridViewTextBoxColumn22.IsVisible = False
        GridViewTextBoxColumn22.Name = "ID_Usuario"
        GridViewTextBoxColumn23.FieldName = "UsuNombre"
        GridViewTextBoxColumn23.HeaderText = "UsuNombre"
        GridViewTextBoxColumn23.Name = "UsuNombre"
        GridViewTextBoxColumn23.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn23.Width = 150
        GridViewTextBoxColumn24.AllowFiltering = False
        GridViewTextBoxColumn24.FieldName = "Estampa"
        GridViewTextBoxColumn24.HeaderText = "Estampa"
        GridViewTextBoxColumn24.IsVisible = False
        GridViewTextBoxColumn24.Name = "Estampa"
        Me.rgListaCobranzas.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8, GridViewTextBoxColumn9, GridViewTextBoxColumn10, GridViewTextBoxColumn11, GridViewTextBoxColumn12, GridViewTextBoxColumn13, GridViewTextBoxColumn14, GridViewTextBoxColumn15, GridViewTextBoxColumn16, GridViewTextBoxColumn17, GridViewTextBoxColumn18, GridViewTextBoxColumn19, GridViewTextBoxColumn20, GridViewTextBoxColumn21, GridViewTextBoxColumn22, GridViewTextBoxColumn23, GridViewTextBoxColumn24})
        Me.rgListaCobranzas.MasterTemplate.EnableFiltering = True
        Me.rgListaCobranzas.MasterTemplate.EnableGrouping = False
        Me.rgListaCobranzas.MasterTemplate.EnableSorting = False
        Me.rgListaCobranzas.Name = "rgListaCobranzas"
        Me.rgListaCobranzas.ReadOnly = True
        Me.rgListaCobranzas.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgListaCobranzas.Size = New System.Drawing.Size(1046, 503)
        Me.rgListaCobranzas.TabIndex = 4
        Me.rgListaCobranzas.Text = "RadGridView1"
        '
        'TAB_MANTENIMIENTO
        '
        Me.TAB_MANTENIMIENTO.BackColor = System.Drawing.Color.Gainsboro
        Me.TAB_MANTENIMIENTO.Controls.Add(Me.btnAgregarDetalles)
        Me.TAB_MANTENIMIENTO.Controls.Add(Me.RadGroupBox6)
        Me.TAB_MANTENIMIENTO.Controls.Add(Me.GRUPO_COB_DETALLES)
        Me.TAB_MANTENIMIENTO.Controls.Add(Me.GRUPO_COBRANZAS)
        Me.TAB_MANTENIMIENTO.Location = New System.Drawing.Point(4, 22)
        Me.TAB_MANTENIMIENTO.Name = "TAB_MANTENIMIENTO"
        Me.TAB_MANTENIMIENTO.Padding = New System.Windows.Forms.Padding(3)
        Me.TAB_MANTENIMIENTO.Size = New System.Drawing.Size(1064, 585)
        Me.TAB_MANTENIMIENTO.TabIndex = 1
        Me.TAB_MANTENIMIENTO.Text = "Mantenimiento"
        '
        'btnAgregarDetalles
        '
        Me.btnAgregarDetalles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgregarDetalles.Location = New System.Drawing.Point(899, 288)
        Me.btnAgregarDetalles.Name = "btnAgregarDetalles"
        Me.btnAgregarDetalles.Size = New System.Drawing.Size(136, 31)
        Me.btnAgregarDetalles.TabIndex = 3
        Me.btnAgregarDetalles.Text = "Agregar Detalle"
        '
        'RadGroupBox6
        '
        Me.RadGroupBox6.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox6.Controls.Add(Me.rgListaCobranzaDetalle)
        Me.RadGroupBox6.HeaderText = ""
        Me.RadGroupBox6.Location = New System.Drawing.Point(7, 331)
        Me.RadGroupBox6.Name = "RadGroupBox6"
        Me.RadGroupBox6.Size = New System.Drawing.Size(1049, 248)
        Me.RadGroupBox6.TabIndex = 2
        '
        'rgListaCobranzaDetalle
        '
        Me.rgListaCobranzaDetalle.BackColor = System.Drawing.SystemColors.ButtonShadow
        Me.rgListaCobranzaDetalle.ContextMenuStrip = Me.MECO_COBRANZA_DETALLE
        Me.rgListaCobranzaDetalle.Cursor = System.Windows.Forms.Cursors.Default
        Me.rgListaCobranzaDetalle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold)
        Me.rgListaCobranzaDetalle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.rgListaCobranzaDetalle.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.rgListaCobranzaDetalle.Location = New System.Drawing.Point(9, 12)
        '
        'rgListaCobranzaDetalle
        '
        Me.rgListaCobranzaDetalle.MasterTemplate.AllowAddNewRow = False
        Me.rgListaCobranzaDetalle.MasterTemplate.AllowColumnHeaderContextMenu = False
        Me.rgListaCobranzaDetalle.MasterTemplate.AllowColumnReorder = False
        Me.rgListaCobranzaDetalle.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn25.FieldName = "ID_CobranzaDetalle"
        GridViewTextBoxColumn25.HeaderText = "Nº Cobranza Detalle"
        GridViewTextBoxColumn25.Name = "ID_CobranzaDetalle"
        GridViewTextBoxColumn25.Width = 120
        GridViewTextBoxColumn26.FieldName = "ID_Cobranza"
        GridViewTextBoxColumn26.HeaderText = "Nº Cobranza"
        GridViewTextBoxColumn26.Name = "ID_Cobranza"
        GridViewTextBoxColumn26.Width = 120
        GridViewTextBoxColumn27.FieldName = "ID_DocVenta"
        GridViewTextBoxColumn27.HeaderText = "Nº Doc. Venta"
        GridViewTextBoxColumn27.Name = "ID_DocVenta"
        GridViewTextBoxColumn27.Width = 150
        GridViewTextBoxColumn28.FieldName = "Serie"
        GridViewTextBoxColumn28.HeaderText = "Serie"
        GridViewTextBoxColumn28.Name = "Serie"
        GridViewTextBoxColumn28.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn28.Width = 80
        GridViewTextBoxColumn29.FieldName = "Numero"
        GridViewTextBoxColumn29.HeaderText = "Número"
        GridViewTextBoxColumn29.Name = "Numero"
        GridViewTextBoxColumn29.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn29.Width = 150
        GridViewTextBoxColumn30.FieldName = "Importe"
        GridViewTextBoxColumn30.HeaderText = "Importe"
        GridViewTextBoxColumn30.Name = "Importe"
        GridViewTextBoxColumn30.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn30.Width = 150
        Me.rgListaCobranzaDetalle.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn25, GridViewTextBoxColumn26, GridViewTextBoxColumn27, GridViewTextBoxColumn28, GridViewTextBoxColumn29, GridViewTextBoxColumn30})
        Me.rgListaCobranzaDetalle.MasterTemplate.EnableFiltering = True
        Me.rgListaCobranzaDetalle.MasterTemplate.EnableGrouping = False
        Me.rgListaCobranzaDetalle.MasterTemplate.EnableSorting = False
        Me.rgListaCobranzaDetalle.Name = "rgListaCobranzaDetalle"
        Me.rgListaCobranzaDetalle.ReadOnly = True
        Me.rgListaCobranzaDetalle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.rgListaCobranzaDetalle.Size = New System.Drawing.Size(1033, 229)
        Me.rgListaCobranzaDetalle.TabIndex = 5
        Me.rgListaCobranzaDetalle.Text = "RadGridView1"
        '
        'GRUPO_COB_DETALLES
        '
        Me.GRUPO_COB_DETALLES.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.RadLabel11)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.txtImporte)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.RadLabel8)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.txtDocVenta)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.RadLabel6)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.txtNumCobranzaDetalles)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.RadLabel4)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.txtCDetalle)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.btnGuardarDetalles)
        Me.GRUPO_COB_DETALLES.Controls.Add(Me.btnCancelarDetalles)
        Me.GRUPO_COB_DETALLES.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRUPO_COB_DETALLES.HeaderText = "Cobranza Detalle "
        Me.GRUPO_COB_DETALLES.Location = New System.Drawing.Point(7, 246)
        Me.GRUPO_COB_DETALLES.Name = "GRUPO_COB_DETALLES"
        Me.GRUPO_COB_DETALLES.Size = New System.Drawing.Size(827, 79)
        Me.GRUPO_COB_DETALLES.TabIndex = 1
        Me.GRUPO_COB_DETALLES.Text = "Cobranza Detalle "
        '
        'RadLabel11
        '
        Me.RadLabel11.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel11.Location = New System.Drawing.Point(367, 47)
        Me.RadLabel11.Name = "RadLabel11"
        Me.RadLabel11.Size = New System.Drawing.Size(58, 18)
        Me.RadLabel11.TabIndex = 58
        Me.RadLabel11.Text = "Importe  :"
        '
        'txtImporte
        '
        Me.txtImporte.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImporte.Location = New System.Drawing.Point(431, 47)
        Me.txtImporte.Name = "txtImporte"
        Me.txtImporte.Size = New System.Drawing.Size(160, 20)
        Me.txtImporte.TabIndex = 59
        Me.txtImporte.Text = "0.00"
        '
        'RadLabel8
        '
        Me.RadLabel8.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel8.Location = New System.Drawing.Point(334, 19)
        Me.RadLabel8.Name = "RadLabel8"
        Me.RadLabel8.Size = New System.Drawing.Size(91, 18)
        Me.RadLabel8.TabIndex = 56
        Me.RadLabel8.Text = "Nº  Doc. Venta :"
        '
        'txtDocVenta
        '
        Me.txtDocVenta.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDocVenta.Location = New System.Drawing.Point(431, 19)
        Me.txtDocVenta.Name = "txtDocVenta"
        Me.txtDocVenta.Size = New System.Drawing.Size(160, 20)
        Me.txtDocVenta.TabIndex = 57
        '
        'RadLabel6
        '
        Me.RadLabel6.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel6.Location = New System.Drawing.Point(35, 47)
        Me.RadLabel6.Name = "RadLabel6"
        Me.RadLabel6.Size = New System.Drawing.Size(80, 18)
        Me.RadLabel6.TabIndex = 54
        Me.RadLabel6.Text = "Nº Cobranza :"
        '
        'txtNumCobranzaDetalles
        '
        Me.txtNumCobranzaDetalles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumCobranzaDetalles.Location = New System.Drawing.Point(121, 47)
        Me.txtNumCobranzaDetalles.Name = "txtNumCobranzaDetalles"
        Me.txtNumCobranzaDetalles.ReadOnly = True
        Me.txtNumCobranzaDetalles.Size = New System.Drawing.Size(160, 20)
        Me.txtNumCobranzaDetalles.TabIndex = 55
        '
        'RadLabel4
        '
        Me.RadLabel4.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel4.Location = New System.Drawing.Point(21, 19)
        Me.RadLabel4.Name = "RadLabel4"
        Me.RadLabel4.Size = New System.Drawing.Size(94, 18)
        Me.RadLabel4.TabIndex = 52
        Me.RadLabel4.Text = "Nº Cob. Detalle :"
        '
        'txtCDetalle
        '
        Me.txtCDetalle.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCDetalle.Location = New System.Drawing.Point(121, 19)
        Me.txtCDetalle.Name = "txtCDetalle"
        Me.txtCDetalle.ReadOnly = True
        Me.txtCDetalle.Size = New System.Drawing.Size(160, 20)
        Me.txtCDetalle.TabIndex = 53
        '
        'btnGuardarDetalles
        '
        Me.btnGuardarDetalles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardarDetalles.Location = New System.Drawing.Point(695, 19)
        Me.btnGuardarDetalles.Name = "btnGuardarDetalles"
        Me.btnGuardarDetalles.Size = New System.Drawing.Size(116, 24)
        Me.btnGuardarDetalles.TabIndex = 1
        Me.btnGuardarDetalles.Text = "Guardar"
        '
        'btnCancelarDetalles
        '
        Me.btnCancelarDetalles.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelarDetalles.Location = New System.Drawing.Point(695, 47)
        Me.btnCancelarDetalles.Name = "btnCancelarDetalles"
        Me.btnCancelarDetalles.Size = New System.Drawing.Size(117, 24)
        Me.btnCancelarDetalles.TabIndex = 0
        Me.btnCancelarDetalles.Text = "Cancelar"
        '
        'GRUPO_COBRANZAS
        '
        Me.GRUPO_COBRANZAS.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.GRUPO_COBRANZAS.Controls.Add(Me.btnCancelar)
        Me.GRUPO_COBRANZAS.Controls.Add(Me.btnGuardar)
        Me.GRUPO_COBRANZAS.Controls.Add(Me.bancos)
        Me.GRUPO_COBRANZAS.Controls.Add(Me.RadGroupBox3)
        Me.GRUPO_COBRANZAS.Controls.Add(Me.RadGroupBox2)
        Me.GRUPO_COBRANZAS.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GRUPO_COBRANZAS.HeaderText = "Cobranzas"
        Me.GRUPO_COBRANZAS.Location = New System.Drawing.Point(6, 6)
        Me.GRUPO_COBRANZAS.Name = "GRUPO_COBRANZAS"
        Me.GRUPO_COBRANZAS.Size = New System.Drawing.Size(1050, 234)
        Me.GRUPO_COBRANZAS.TabIndex = 0
        Me.GRUPO_COBRANZAS.Text = "Cobranzas"
        '
        'btnCancelar
        '
        Me.btnCancelar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(905, 193)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(110, 30)
        Me.btnCancelar.TabIndex = 61
        Me.btnCancelar.Text = "Cancelar"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(770, 194)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(110, 30)
        Me.btnGuardar.TabIndex = 60
        Me.btnGuardar.Text = "Guardar"
        '
        'bancos
        '
        Me.bancos.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.bancos.Controls.Add(Me.RadLabel13)
        Me.bancos.Controls.Add(Me.cboBancos)
        Me.bancos.Controls.Add(Me.cboCtaCorriente)
        Me.bancos.Controls.Add(Me.RadLabel18)
        Me.bancos.Controls.Add(Me.cboMonedas)
        Me.bancos.Controls.Add(Me.RadLabel12)
        Me.bancos.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bancos.HeaderText = " Banco "
        Me.bancos.Location = New System.Drawing.Point(5, 121)
        Me.bancos.Name = "bancos"
        Me.bancos.Size = New System.Drawing.Size(363, 103)
        Me.bancos.TabIndex = 59
        Me.bancos.Text = " Banco "
        '
        'RadLabel13
        '
        Me.RadLabel13.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel13.Location = New System.Drawing.Point(42, 21)
        Me.RadLabel13.Name = "RadLabel13"
        Me.RadLabel13.Size = New System.Drawing.Size(45, 18)
        Me.RadLabel13.TabIndex = 17
        Me.RadLabel13.Text = "Banco :"
        '
        'cboBancos
        '
        Me.cboBancos.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboBancos.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboBancos.Location = New System.Drawing.Point(94, 20)
        Me.cboBancos.Name = "cboBancos"
        Me.cboBancos.Size = New System.Drawing.Size(255, 20)
        Me.cboBancos.TabIndex = 7
        '
        'cboCtaCorriente
        '
        Me.cboCtaCorriente.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboCtaCorriente.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCtaCorriente.Location = New System.Drawing.Point(94, 45)
        Me.cboCtaCorriente.Name = "cboCtaCorriente"
        Me.cboCtaCorriente.Size = New System.Drawing.Size(255, 20)
        Me.cboCtaCorriente.TabIndex = 46
        '
        'RadLabel18
        '
        Me.RadLabel18.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel18.Location = New System.Drawing.Point(4, 46)
        Me.RadLabel18.Name = "RadLabel18"
        Me.RadLabel18.Size = New System.Drawing.Size(83, 18)
        Me.RadLabel18.TabIndex = 47
        Me.RadLabel18.Text = "Cta Corriente :"
        '
        'cboMonedas
        '
        Me.cboMonedas.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboMonedas.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMonedas.Location = New System.Drawing.Point(94, 71)
        Me.cboMonedas.Name = "cboMonedas"
        Me.cboMonedas.Size = New System.Drawing.Size(255, 20)
        Me.cboMonedas.TabIndex = 44
        '
        'RadLabel12
        '
        Me.RadLabel12.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel12.Location = New System.Drawing.Point(31, 73)
        Me.RadLabel12.Name = "RadLabel12"
        Me.RadLabel12.Size = New System.Drawing.Size(56, 18)
        Me.RadLabel12.TabIndex = 45
        Me.RadLabel12.Text = "Moneda :"
        '
        'RadGroupBox3
        '
        Me.RadGroupBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox3.Controls.Add(Me.txtNumOperacion)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel2)
        Me.RadGroupBox3.Controls.Add(Me.txtTotal)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel29)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel10)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel3)
        Me.RadGroupBox3.Controls.Add(Me.txtGlosa)
        Me.RadGroupBox3.Controls.Add(Me.txtTC)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel1)
        Me.RadGroupBox3.Controls.Add(Me.txtNumCobranza)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel9)
        Me.RadGroupBox3.Controls.Add(Me.dtFecha)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel7)
        Me.RadGroupBox3.Controls.Add(Me.RadLabel5)
        Me.RadGroupBox3.Controls.Add(Me.txtSerie)
        Me.RadGroupBox3.Controls.Add(Me.txtNumero)
        Me.RadGroupBox3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox3.HeaderText = "Documento "
        Me.RadGroupBox3.Location = New System.Drawing.Point(374, 17)
        Me.RadGroupBox3.Name = "RadGroupBox3"
        Me.RadGroupBox3.Size = New System.Drawing.Size(671, 168)
        Me.RadGroupBox3.TabIndex = 50
        Me.RadGroupBox3.Text = "Documento "
        '
        'txtNumOperacion
        '
        Me.txtNumOperacion.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumOperacion.Location = New System.Drawing.Point(92, 44)
        Me.txtNumOperacion.Name = "txtNumOperacion"
        Me.txtNumOperacion.Size = New System.Drawing.Size(160, 20)
        Me.txtNumOperacion.TabIndex = 55
        '
        'RadLabel2
        '
        Me.RadLabel2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel2.Location = New System.Drawing.Point(321, 46)
        Me.RadLabel2.Name = "RadLabel2"
        Me.RadLabel2.Size = New System.Drawing.Size(26, 18)
        Me.RadLabel2.TabIndex = 52
        Me.RadLabel2.Text = "TC :"
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.Color.White
        Me.txtTotal.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(354, 68)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(135, 20)
        Me.txtTotal.TabIndex = 56
        Me.txtTotal.Text = "0.00"
        '
        'RadLabel29
        '
        Me.RadLabel29.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel29.Location = New System.Drawing.Point(44, 117)
        Me.RadLabel29.Name = "RadLabel29"
        Me.RadLabel29.Size = New System.Drawing.Size(42, 18)
        Me.RadLabel29.TabIndex = 57
        Me.RadLabel29.Text = "Glosa :"
        '
        'RadLabel10
        '
        Me.RadLabel10.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel10.Location = New System.Drawing.Point(307, 69)
        Me.RadLabel10.Name = "RadLabel10"
        Me.RadLabel10.Size = New System.Drawing.Size(40, 18)
        Me.RadLabel10.TabIndex = 55
        Me.RadLabel10.Text = "Total :"
        '
        'RadLabel3
        '
        Me.RadLabel3.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel3.Location = New System.Drawing.Point(2, 44)
        Me.RadLabel3.Name = "RadLabel3"
        Me.RadLabel3.Size = New System.Drawing.Size(84, 18)
        Me.RadLabel3.TabIndex = 54
        Me.RadLabel3.Text = "Nº Operación :"
        '
        'txtGlosa
        '
        Me.txtGlosa.AutoSize = False
        Me.txtGlosa.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGlosa.Location = New System.Drawing.Point(92, 119)
        Me.txtGlosa.Multiline = True
        Me.txtGlosa.Name = "txtGlosa"
        Me.txtGlosa.Size = New System.Drawing.Size(553, 40)
        Me.txtGlosa.TabIndex = 58
        '
        'txtTC
        '
        Me.txtTC.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTC.Location = New System.Drawing.Point(354, 43)
        Me.txtTC.Name = "txtTC"
        Me.txtTC.Size = New System.Drawing.Size(135, 20)
        Me.txtTC.TabIndex = 53
        '
        'RadLabel1
        '
        Me.RadLabel1.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel1.Location = New System.Drawing.Point(6, 20)
        Me.RadLabel1.Name = "RadLabel1"
        Me.RadLabel1.Size = New System.Drawing.Size(80, 18)
        Me.RadLabel1.TabIndex = 50
        Me.RadLabel1.Text = "Nº Cobranza :"
        '
        'txtNumCobranza
        '
        Me.txtNumCobranza.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumCobranza.Location = New System.Drawing.Point(92, 20)
        Me.txtNumCobranza.Name = "txtNumCobranza"
        Me.txtNumCobranza.ReadOnly = True
        Me.txtNumCobranza.Size = New System.Drawing.Size(160, 20)
        Me.txtNumCobranza.TabIndex = 51
        '
        'RadLabel9
        '
        Me.RadLabel9.Controls.Add(Me.RadDateTimePicker1)
        Me.RadLabel9.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel9.Location = New System.Drawing.Point(304, 20)
        Me.RadLabel9.Name = "RadLabel9"
        Me.RadLabel9.Size = New System.Drawing.Size(43, 18)
        Me.RadLabel9.TabIndex = 48
        Me.RadLabel9.Text = "Fecha :"
        '
        'RadDateTimePicker1
        '
        Me.RadDateTimePicker1.Location = New System.Drawing.Point(47, 0)
        Me.RadDateTimePicker1.Name = "RadDateTimePicker1"
        Me.RadDateTimePicker1.Size = New System.Drawing.Size(127, 20)
        Me.RadDateTimePicker1.TabIndex = 1
        Me.RadDateTimePicker1.TabStop = False
        Me.RadDateTimePicker1.Text = "jueves, 08 de enero de 2015"
        Me.RadDateTimePicker1.Value = New Date(2015, 1, 8, 17, 17, 46, 477)
        '
        'dtFecha
        '
        Me.dtFecha.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha.Location = New System.Drawing.Point(354, 18)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(135, 20)
        Me.dtFecha.TabIndex = 49
        Me.dtFecha.TabStop = False
        Me.dtFecha.Text = "08/01/2015"
        Me.dtFecha.Value = New Date(2015, 1, 8, 17, 18, 29, 710)
        '
        'RadLabel7
        '
        Me.RadLabel7.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel7.Location = New System.Drawing.Point(47, 68)
        Me.RadLabel7.Name = "RadLabel7"
        Me.RadLabel7.Size = New System.Drawing.Size(39, 18)
        Me.RadLabel7.TabIndex = 6
        Me.RadLabel7.Text = "Serie :"
        '
        'RadLabel5
        '
        Me.RadLabel5.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel5.Location = New System.Drawing.Point(30, 94)
        Me.RadLabel5.Name = "RadLabel5"
        Me.RadLabel5.Size = New System.Drawing.Size(56, 18)
        Me.RadLabel5.TabIndex = 4
        Me.RadLabel5.Text = "Número :"
        '
        'txtSerie
        '
        Me.txtSerie.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerie.Location = New System.Drawing.Point(92, 68)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(160, 20)
        Me.txtSerie.TabIndex = 43
        '
        'txtNumero
        '
        Me.txtNumero.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumero.Location = New System.Drawing.Point(92, 93)
        Me.txtNumero.Name = "txtNumero"
        Me.txtNumero.Size = New System.Drawing.Size(160, 20)
        Me.txtNumero.TabIndex = 40
        '
        'RadGroupBox2
        '
        Me.RadGroupBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping
        Me.RadGroupBox2.Controls.Add(Me.txtDoc)
        Me.RadGroupBox2.Controls.Add(Me.txtTipoDocumento)
        Me.RadGroupBox2.Controls.Add(Me.txtDireccion)
        Me.RadGroupBox2.Controls.Add(Me.RadLabel27)
        Me.RadGroupBox2.Controls.Add(Me.RadLabel25)
        Me.RadGroupBox2.Controls.Add(Me.RadLabel24)
        Me.RadGroupBox2.Controls.Add(Me.RadLabel19)
        Me.RadGroupBox2.Controls.Add(Me.cboClientes)
        Me.RadGroupBox2.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadGroupBox2.HeaderText = "Datos del Cliente "
        Me.RadGroupBox2.Location = New System.Drawing.Point(5, 17)
        Me.RadGroupBox2.Name = "RadGroupBox2"
        Me.RadGroupBox2.Size = New System.Drawing.Size(363, 101)
        Me.RadGroupBox2.TabIndex = 49
        Me.RadGroupBox2.Text = "Datos del Cliente "
        '
        'txtDoc
        '
        Me.txtDoc.Enabled = False
        Me.txtDoc.Location = New System.Drawing.Point(230, 73)
        Me.txtDoc.Name = "txtDoc"
        Me.txtDoc.Size = New System.Drawing.Size(122, 20)
        Me.txtDoc.TabIndex = 36
        '
        'txtTipoDocumento
        '
        Me.txtTipoDocumento.Enabled = False
        Me.txtTipoDocumento.Location = New System.Drawing.Point(85, 73)
        Me.txtTipoDocumento.Name = "txtTipoDocumento"
        Me.txtTipoDocumento.Size = New System.Drawing.Size(87, 20)
        Me.txtTipoDocumento.TabIndex = 35
        '
        'txtDireccion
        '
        Me.txtDireccion.Enabled = False
        Me.txtDireccion.Location = New System.Drawing.Point(85, 48)
        Me.txtDireccion.Name = "txtDireccion"
        Me.txtDireccion.Size = New System.Drawing.Size(267, 20)
        Me.txtDireccion.TabIndex = 34
        '
        'RadLabel27
        '
        Me.RadLabel27.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel27.Location = New System.Drawing.Point(16, 75)
        Me.RadLabel27.Name = "RadLabel27"
        Me.RadLabel27.Size = New System.Drawing.Size(63, 18)
        Me.RadLabel27.TabIndex = 33
        Me.RadLabel27.Text = "Tipo Doc. :"
        '
        'RadLabel25
        '
        Me.RadLabel25.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel25.Location = New System.Drawing.Point(173, 75)
        Me.RadLabel25.Name = "RadLabel25"
        Me.RadLabel25.Size = New System.Drawing.Size(60, 18)
        Me.RadLabel25.TabIndex = 29
        Me.RadLabel25.Text = "Nro. Doc :"
        '
        'RadLabel24
        '
        Me.RadLabel24.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel24.Location = New System.Drawing.Point(17, 48)
        Me.RadLabel24.Name = "RadLabel24"
        Me.RadLabel24.Size = New System.Drawing.Size(62, 18)
        Me.RadLabel24.TabIndex = 28
        Me.RadLabel24.Text = "Dirección :"
        '
        'RadLabel19
        '
        Me.RadLabel19.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadLabel19.Location = New System.Drawing.Point(25, 21)
        Me.RadLabel19.Name = "RadLabel19"
        Me.RadLabel19.Size = New System.Drawing.Size(54, 18)
        Me.RadLabel19.TabIndex = 27
        Me.RadLabel19.Text = "Clientes :"
        '
        'cboClientes
        '
        Me.cboClientes.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList
        Me.cboClientes.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClientes.Location = New System.Drawing.Point(85, 21)
        Me.cboClientes.Name = "cboClientes"
        Me.cboClientes.Size = New System.Drawing.Size(267, 20)
        Me.cboClientes.TabIndex = 10
        '
        'frmCobranza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1074, 626)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmCobranza"
        Me.Text = "Cobranza"
        Me.MECO_COBRANZAS.ResumeLayout(False)
        Me.MECO_COBRANZA_DETALLE.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TAB_CONSULTA.ResumeLayout(False)
        CType(Me.btnNuevo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaCobranzas.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaCobranzas, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TAB_MANTENIMIENTO.ResumeLayout(False)
        CType(Me.btnAgregarDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox6.ResumeLayout(False)
        CType(Me.rgListaCobranzaDetalle.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgListaCobranzaDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GRUPO_COB_DETALLES, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRUPO_COB_DETALLES.ResumeLayout(False)
        Me.GRUPO_COB_DETALLES.PerformLayout()
        CType(Me.RadLabel11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtImporte, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDocVenta, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumCobranzaDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGuardarDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnCancelarDetalles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GRUPO_COBRANZAS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GRUPO_COBRANZAS.ResumeLayout(False)
        CType(Me.btnCancelar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnGuardar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bancos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.bancos.ResumeLayout(False)
        Me.bancos.PerformLayout()
        CType(Me.RadLabel13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboBancos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboCtaCorriente, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboMonedas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox3.ResumeLayout(False)
        Me.RadGroupBox3.PerformLayout()
        CType(Me.txtNumOperacion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTotal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtGlosa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumCobranza, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadLabel9.ResumeLayout(False)
        Me.RadLabel9.PerformLayout()
        CType(Me.RadDateTimePicker1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtFecha, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtSerie, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtNumero, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadGroupBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.RadGroupBox2.ResumeLayout(False)
        Me.RadGroupBox2.PerformLayout()
        CType(Me.txtDoc, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtTipoDocumento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtDireccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RadLabel19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cboClientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As Controles_DHMont.TabPersonalizado
    Friend WithEvents TAB_CONSULTA As System.Windows.Forms.TabPage
    Friend WithEvents TAB_MANTENIMIENTO As System.Windows.Forms.TabPage
    Friend WithEvents GRUPO_COBRANZAS As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents rgListaCobranzas As Telerik.WinControls.UI.RadGridView
    Friend WithEvents RadGroupBox2 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents txtDoc As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtTipoDocumento As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtDireccion As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel27 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel25 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel24 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel19 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboClientes As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel3 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadGroupBox3 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents txtNumOperacion As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel2 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtTC As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel13 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboBancos As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel1 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtNumCobranza As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel12 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents cboMonedas As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel9 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadDateTimePicker1 As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents dtFecha As Telerik.WinControls.UI.RadDateTimePicker
    Friend WithEvents cboCtaCorriente As Telerik.WinControls.UI.RadDropDownList
    Friend WithEvents RadLabel18 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel7 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents RadLabel5 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtSerie As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtNumero As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents bancos As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents RadLabel29 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtGlosa As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents txtTotal As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel10 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents btnAgregarDetalles As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadGroupBox6 As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents rgListaCobranzaDetalle As Telerik.WinControls.UI.RadGridView
    Friend WithEvents GRUPO_COB_DETALLES As Telerik.WinControls.UI.RadGroupBox
    Friend WithEvents btnGuardarDetalles As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnCancelarDetalles As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnCancelar As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnGuardar As Telerik.WinControls.UI.RadButton
    Friend WithEvents btnNuevo As Telerik.WinControls.UI.RadButton
    Friend WithEvents RadLabel11 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtImporte As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel8 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtDocVenta As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel6 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtNumCobranzaDetalles As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents RadLabel4 As Telerik.WinControls.UI.RadLabel
    Friend WithEvents txtCDetalle As Telerik.WinControls.UI.RadTextBox
    Friend WithEvents MECO_COBRANZA_DETALLE As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MECO_UPDATE As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MECO_DELETE As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MECO_COBRANZAS As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MECO_MODIFICAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MECO_ELIMINAR As System.Windows.Forms.ToolStripMenuItem
End Class
