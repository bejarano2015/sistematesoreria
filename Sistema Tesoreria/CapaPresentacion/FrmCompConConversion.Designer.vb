<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCompConConversion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmCompConConversion))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbBotonera = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnGenerar = New System.Windows.Forms.Button
        Me.btnSalir = New System.Windows.Forms.Button
        Me.btnNuevoCab = New System.Windows.Forms.Button
        Me.btnCancelar = New System.Windows.Forms.Button
        Me.btnEliminar = New System.Windows.Forms.Button
        Me.btnGrabar = New System.Windows.Forms.Button
        Me.btnEditar = New System.Windows.Forms.Button
        Me.btnNuevoDet = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.gbCabecera = New System.Windows.Forms.GroupBox
        Me.lblDiferencia = New System.Windows.Forms.Label
        Me.Label32 = New System.Windows.Forms.Label
        Me.gbEspecial = New System.Windows.Forms.GroupBox
        Me.btnCerrarEspecial = New ctrLibreria.Controles.BeButton
        Me.dgvEpecial = New System.Windows.Forms.DataGridView
        Me.gbFechaTCambio = New System.Windows.Forms.GroupBox
        Me.BeButton1 = New ctrLibreria.Controles.BeButton
        Me.dgvFechaTCambio = New System.Windows.Forms.DataGridView
        Me.dtpFechaTCambio = New ctrLibreria.Controles.BeDateTimePicker
        Me.dtpFechaVoucher = New ctrLibreria.Controles.BeDateTimePicker
        Me.lblTipoAnexo = New System.Windows.Forms.Label
        Me.txtGlosaCab = New ctrLibreria.Controles.BeTextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtValorCambio = New ctrLibreria.Controles.BeTextBox
        Me.cboTipoCambio = New ctrLibreria.Controles.BeComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.cboMoneda = New ctrLibreria.Controles.BeComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboSubdiario = New ctrLibreria.Controles.BeComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.gbTipoConversion = New System.Windows.Forms.GroupBox
        Me.rbVenta = New System.Windows.Forms.RadioButton
        Me.rbCompra = New System.Windows.Forms.RadioButton
        Me.TabRegGeneral = New System.Windows.Forms.TabControl
        Me.TabAsiento = New System.Windows.Forms.TabPage
        Me.cboMonedaAsiento = New ctrLibreria.Controles.BeComboBox
        Me.lblHaber = New System.Windows.Forms.Label
        Me.lblDebe = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.dgvAsiento = New ctrLibreria.Controles.BeDataGridView
        Me.TabDatos = New System.Windows.Forms.TabPage
        Me.gbRegistro = New System.Windows.Forms.GroupBox
        Me.btnFiltro = New ctrLibreria.Controles.BeButton
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.PictureBox10 = New System.Windows.Forms.PictureBox
        Me.lblDescAreaDet = New System.Windows.Forms.Label
        Me.txtAreaDet = New ctrLibreria.Controles.BeTextBox
        Me.Label40 = New System.Windows.Forms.Label
        Me.dtpFechaVcmtoDet = New System.Windows.Forms.DateTimePicker
        Me.Label13 = New System.Windows.Forms.Label
        Me.dtpFechaDocDet = New System.Windows.Forms.DateTimePicker
        Me.Label37 = New System.Windows.Forms.Label
        Me.lblDescCCostoDet = New System.Windows.Forms.Label
        Me.txtCCostoDet = New ctrLibreria.Controles.BeTextBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.lblDescAnexDet = New System.Windows.Forms.Label
        Me.txtGlosaDet = New ctrLibreria.Controles.BeTextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtHaber = New ctrLibreria.Controles.BeTextBox
        Me.txtDebe = New ctrLibreria.Controles.BeTextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtNumDocDet = New ctrLibreria.Controles.BeTextBox
        Me.txtSerieDocDet = New ctrLibreria.Controles.BeTextBox
        Me.cboTipoDocDet = New ctrLibreria.Controles.BeComboBox
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtAnexoDet = New ctrLibreria.Controles.BeTextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.lblDescripCuenta = New System.Windows.Forms.Label
        Me.mtbCuenta = New System.Windows.Forms.MaskedTextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.txtSecuencia = New ctrLibreria.Controles.BeTextBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.txtNumComp = New ctrLibreria.Controles.BeTextBox
        Me.gbBotonera.SuspendLayout()
        Me.gbCabecera.SuspendLayout()
        Me.gbEspecial.SuspendLayout()
        CType(Me.dgvEpecial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbFechaTCambio.SuspendLayout()
        CType(Me.dgvFechaTCambio, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbTipoConversion.SuspendLayout()
        Me.TabRegGeneral.SuspendLayout()
        Me.TabAsiento.SuspendLayout()
        CType(Me.dgvAsiento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabDatos.SuspendLayout()
        Me.gbRegistro.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbBotonera
        '
        Me.gbBotonera.Controls.Add(Me.Label2)
        Me.gbBotonera.Controls.Add(Me.Label1)
        Me.gbBotonera.Controls.Add(Me.btnGenerar)
        Me.gbBotonera.Controls.Add(Me.btnSalir)
        Me.gbBotonera.Controls.Add(Me.btnNuevoCab)
        Me.gbBotonera.Controls.Add(Me.btnCancelar)
        Me.gbBotonera.Controls.Add(Me.btnEliminar)
        Me.gbBotonera.Controls.Add(Me.btnGrabar)
        Me.gbBotonera.Controls.Add(Me.btnEditar)
        Me.gbBotonera.Controls.Add(Me.btnNuevoDet)
        Me.gbBotonera.Location = New System.Drawing.Point(7, -1)
        Me.gbBotonera.Name = "gbBotonera"
        Me.gbBotonera.Size = New System.Drawing.Size(95, 467)
        Me.gbBotonera.TabIndex = 32
        Me.gbBotonera.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(20, 273)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Detalle"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(20, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "Cabecera"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnGenerar
        '
        Me.btnGenerar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGenerar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGenerar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnGenerar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerar.Image = CType(resources.GetObject("btnGenerar.Image"), System.Drawing.Image)
        Me.btnGenerar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerar.Location = New System.Drawing.Point(8, 80)
        Me.btnGenerar.Name = "btnGenerar"
        Me.btnGenerar.Size = New System.Drawing.Size(73, 25)
        Me.btnGenerar.TabIndex = 6
        Me.btnGenerar.Text = "  &Generar"
        Me.btnGenerar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGenerar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnGenerar.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalir.Location = New System.Drawing.Point(8, 111)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(73, 25)
        Me.btnSalir.TabIndex = 7
        Me.btnSalir.Text = "  &Salir"
        Me.btnSalir.UseVisualStyleBackColor = False
        '
        'btnNuevoCab
        '
        Me.btnNuevoCab.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnNuevoCab.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNuevoCab.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnNuevoCab.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoCab.Image = CType(resources.GetObject("btnNuevoCab.Image"), System.Drawing.Image)
        Me.btnNuevoCab.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoCab.Location = New System.Drawing.Point(8, 47)
        Me.btnNuevoCab.Name = "btnNuevoCab"
        Me.btnNuevoCab.Size = New System.Drawing.Size(73, 25)
        Me.btnNuevoCab.TabIndex = 5
        Me.btnNuevoCab.Text = "&Nuevo"
        Me.btnNuevoCab.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoCab.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNuevoCab.UseVisualStyleBackColor = False
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCancelar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Image = CType(resources.GetObject("btnCancelar.Image"), System.Drawing.Image)
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(9, 389)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(77, 25)
        Me.btnCancelar.TabIndex = 3
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEliminar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.Image = CType(resources.GetObject("btnEliminar.Image"), System.Drawing.Image)
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(9, 420)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(77, 25)
        Me.btnEliminar.TabIndex = 4
        Me.btnEliminar.Text = "&Eliminar"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'btnGrabar
        '
        Me.btnGrabar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnGrabar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGrabar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnGrabar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGrabar.Image = CType(resources.GetObject("btnGrabar.Image"), System.Drawing.Image)
        Me.btnGrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGrabar.Location = New System.Drawing.Point(9, 327)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(77, 25)
        Me.btnGrabar.TabIndex = 1
        Me.btnGrabar.Text = "&Grabar"
        Me.btnGrabar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGrabar.UseVisualStyleBackColor = False
        '
        'btnEditar
        '
        Me.btnEditar.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnEditar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEditar.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditar.Image = CType(resources.GetObject("btnEditar.Image"), System.Drawing.Image)
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.Location = New System.Drawing.Point(9, 358)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(77, 25)
        Me.btnEditar.TabIndex = 2
        Me.btnEditar.Text = "  &Editar "
        Me.btnEditar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditar.UseVisualStyleBackColor = False
        '
        'btnNuevoDet
        '
        Me.btnNuevoDet.BackColor = System.Drawing.Color.WhiteSmoke
        Me.btnNuevoDet.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNuevoDet.FlatAppearance.BorderColor = System.Drawing.SystemColors.HighlightText
        Me.btnNuevoDet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNuevoDet.Image = CType(resources.GetObject("btnNuevoDet.Image"), System.Drawing.Image)
        Me.btnNuevoDet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevoDet.Location = New System.Drawing.Point(8, 296)
        Me.btnNuevoDet.Name = "btnNuevoDet"
        Me.btnNuevoDet.Size = New System.Drawing.Size(77, 25)
        Me.btnNuevoDet.TabIndex = 0
        Me.btnNuevoDet.Text = "&Nuevo"
        Me.btnNuevoDet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevoDet.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(428, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(146, 13)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Numero de Comprobante"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbCabecera
        '
        Me.gbCabecera.Controls.Add(Me.lblDiferencia)
        Me.gbCabecera.Controls.Add(Me.Label32)
        Me.gbCabecera.Controls.Add(Me.gbEspecial)
        Me.gbCabecera.Controls.Add(Me.gbFechaTCambio)
        Me.gbCabecera.Controls.Add(Me.dtpFechaTCambio)
        Me.gbCabecera.Controls.Add(Me.dtpFechaVoucher)
        Me.gbCabecera.Controls.Add(Me.lblTipoAnexo)
        Me.gbCabecera.Controls.Add(Me.txtGlosaCab)
        Me.gbCabecera.Controls.Add(Me.Label20)
        Me.gbCabecera.Controls.Add(Me.Label12)
        Me.gbCabecera.Controls.Add(Me.Label11)
        Me.gbCabecera.Controls.Add(Me.txtValorCambio)
        Me.gbCabecera.Controls.Add(Me.cboTipoCambio)
        Me.gbCabecera.Controls.Add(Me.Label9)
        Me.gbCabecera.Controls.Add(Me.cboMoneda)
        Me.gbCabecera.Controls.Add(Me.Label8)
        Me.gbCabecera.Controls.Add(Me.Label5)
        Me.gbCabecera.Controls.Add(Me.cboSubdiario)
        Me.gbCabecera.Controls.Add(Me.Label4)
        Me.gbCabecera.Controls.Add(Me.gbTipoConversion)
        Me.gbCabecera.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.gbCabecera.Location = New System.Drawing.Point(111, 34)
        Me.gbCabecera.Name = "gbCabecera"
        Me.gbCabecera.Size = New System.Drawing.Size(658, 186)
        Me.gbCabecera.TabIndex = 35
        Me.gbCabecera.TabStop = False
        '
        'lblDiferencia
        '
        Me.lblDiferencia.AutoSize = True
        Me.lblDiferencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiferencia.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDiferencia.Location = New System.Drawing.Point(574, 167)
        Me.lblDiferencia.Name = "lblDiferencia"
        Me.lblDiferencia.Size = New System.Drawing.Size(0, 13)
        Me.lblDiferencia.TabIndex = 200
        Me.lblDiferencia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label32.Location = New System.Drawing.Point(503, 167)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(73, 13)
        Me.Label32.TabIndex = 199
        Me.Label32.Text = "Diferencia :"
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbEspecial
        '
        Me.gbEspecial.BackColor = System.Drawing.Color.WhiteSmoke
        Me.gbEspecial.Controls.Add(Me.btnCerrarEspecial)
        Me.gbEspecial.Controls.Add(Me.dgvEpecial)
        Me.gbEspecial.Location = New System.Drawing.Point(209, 69)
        Me.gbEspecial.Name = "gbEspecial"
        Me.gbEspecial.Size = New System.Drawing.Size(144, 108)
        Me.gbEspecial.TabIndex = 143
        Me.gbEspecial.TabStop = False
        Me.gbEspecial.Visible = False
        '
        'btnCerrarEspecial
        '
        Me.btnCerrarEspecial.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnCerrarEspecial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCerrarEspecial.Location = New System.Drawing.Point(95, 85)
        Me.btnCerrarEspecial.Name = "btnCerrarEspecial"
        Me.btnCerrarEspecial.Size = New System.Drawing.Size(43, 20)
        Me.btnCerrarEspecial.TabIndex = 145
        Me.btnCerrarEspecial.Text = "Cerrar"
        Me.btnCerrarEspecial.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnCerrarEspecial.UseVisualStyleBackColor = True
        '
        'dgvEpecial
        '
        Me.dgvEpecial.AllowUserToAddRows = False
        Me.dgvEpecial.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.SkyBlue
        Me.dgvEpecial.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEpecial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.SkyBlue
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvEpecial.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvEpecial.Location = New System.Drawing.Point(6, 7)
        Me.dgvEpecial.Name = "dgvEpecial"
        Me.dgvEpecial.RowHeadersVisible = False
        Me.dgvEpecial.Size = New System.Drawing.Size(131, 76)
        Me.dgvEpecial.TabIndex = 0
        '
        'gbFechaTCambio
        '
        Me.gbFechaTCambio.BackColor = System.Drawing.Color.WhiteSmoke
        Me.gbFechaTCambio.Controls.Add(Me.BeButton1)
        Me.gbFechaTCambio.Controls.Add(Me.dgvFechaTCambio)
        Me.gbFechaTCambio.Location = New System.Drawing.Point(327, 69)
        Me.gbFechaTCambio.Name = "gbFechaTCambio"
        Me.gbFechaTCambio.Size = New System.Drawing.Size(177, 85)
        Me.gbFechaTCambio.TabIndex = 140
        Me.gbFechaTCambio.TabStop = False
        '
        'BeButton1
        '
        Me.BeButton1.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.BeButton1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BeButton1.Location = New System.Drawing.Point(129, 62)
        Me.BeButton1.Name = "BeButton1"
        Me.BeButton1.Size = New System.Drawing.Size(43, 20)
        Me.BeButton1.TabIndex = 146
        Me.BeButton1.Text = "Cerrar"
        Me.BeButton1.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.BeButton1.UseVisualStyleBackColor = True
        '
        'dgvFechaTCambio
        '
        Me.dgvFechaTCambio.AllowUserToAddRows = False
        Me.dgvFechaTCambio.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SkyBlue
        Me.dgvFechaTCambio.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvFechaTCambio.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.SkyBlue
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvFechaTCambio.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvFechaTCambio.Location = New System.Drawing.Point(6, 7)
        Me.dgvFechaTCambio.Name = "dgvFechaTCambio"
        Me.dgvFechaTCambio.ReadOnly = True
        Me.dgvFechaTCambio.Size = New System.Drawing.Size(164, 54)
        Me.dgvFechaTCambio.TabIndex = 0
        '
        'dtpFechaTCambio
        '
        Me.dtpFechaTCambio.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaTCambio.CustomFormat = ""
        Me.dtpFechaTCambio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaTCambio.KeyEnter = True
        Me.dtpFechaTCambio.Location = New System.Drawing.Point(261, 69)
        Me.dtpFechaTCambio.Name = "dtpFechaTCambio"
        Me.dtpFechaTCambio.Size = New System.Drawing.Size(85, 20)
        Me.dtpFechaTCambio.TabIndex = 6
        Me.dtpFechaTCambio.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaTCambio.Value = New Date(2009, 1, 13, 0, 0, 0, 0)
        '
        'dtpFechaVoucher
        '
        Me.dtpFechaVoucher.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01
        Me.dtpFechaVoucher.CustomFormat = ""
        Me.dtpFechaVoucher.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVoucher.KeyEnter = True
        Me.dtpFechaVoucher.Location = New System.Drawing.Point(562, 14)
        Me.dtpFechaVoucher.Name = "dtpFechaVoucher"
        Me.dtpFechaVoucher.Size = New System.Drawing.Size(85, 20)
        Me.dtpFechaVoucher.TabIndex = 2
        Me.dtpFechaVoucher.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha
        Me.dtpFechaVoucher.Value = New Date(2009, 1, 13, 0, 0, 0, 0)
        '
        'lblTipoAnexo
        '
        Me.lblTipoAnexo.AutoSize = True
        Me.lblTipoAnexo.Location = New System.Drawing.Point(268, 73)
        Me.lblTipoAnexo.Name = "lblTipoAnexo"
        Me.lblTipoAnexo.Size = New System.Drawing.Size(0, 13)
        Me.lblTipoAnexo.TabIndex = 85
        Me.lblTipoAnexo.Visible = False
        '
        'txtGlosaCab
        '
        Me.txtGlosaCab.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtGlosaCab.BackColor = System.Drawing.Color.Ivory
        Me.txtGlosaCab.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGlosaCab.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGlosaCab.ForeColor = System.Drawing.Color.Black
        Me.txtGlosaCab.KeyEnter = True
        Me.txtGlosaCab.Location = New System.Drawing.Point(90, 96)
        Me.txtGlosaCab.MaxLength = 100
        Me.txtGlosaCab.Name = "txtGlosaCab"
        Me.txtGlosaCab.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Tres
        Me.txtGlosaCab.ShortcutsEnabled = False
        Me.txtGlosaCab.Size = New System.Drawing.Size(456, 20)
        Me.txtGlosaCab.TabIndex = 16
        Me.txtGlosaCab.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Black
        Me.Label20.Location = New System.Drawing.Point(5, 98)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(83, 13)
        Me.Label20.TabIndex = 84
        Me.Label20.Text = "Glosa Cabecera"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(377, 98)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(67, 13)
        Me.Label12.TabIndex = 68
        Me.Label12.Text = "Fecha Vmto."
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Black
        Me.Label11.Location = New System.Drawing.Point(214, 98)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 13)
        Me.Label11.TabIndex = 66
        Me.Label11.Text = "Fecha Emision"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtValorCambio
        '
        Me.txtValorCambio.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtValorCambio.BackColor = System.Drawing.Color.Ivory
        Me.txtValorCambio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtValorCambio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtValorCambio.Enabled = False
        Me.txtValorCambio.ForeColor = System.Drawing.Color.Black
        Me.txtValorCambio.KeyEnter = True
        Me.txtValorCambio.Location = New System.Drawing.Point(211, 69)
        Me.txtValorCambio.MaxLength = 11
        Me.txtValorCambio.Name = "txtValorCambio"
        Me.txtValorCambio.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Tres
        Me.txtValorCambio.ShortcutsEnabled = False
        Me.txtValorCambio.Size = New System.Drawing.Size(44, 20)
        Me.txtValorCambio.TabIndex = 5
        Me.txtValorCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtValorCambio.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'cboTipoCambio
        '
        Me.cboTipoCambio.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoCambio.BackColor = System.Drawing.Color.Ivory
        Me.cboTipoCambio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoCambio.ForeColor = System.Drawing.Color.Black
        Me.cboTipoCambio.FormattingEnabled = True
        Me.cboTipoCambio.KeyEnter = True
        Me.cboTipoCambio.Location = New System.Drawing.Point(90, 68)
        Me.cboTipoCambio.Name = "cboTipoCambio"
        Me.cboTipoCambio.Size = New System.Drawing.Size(117, 21)
        Me.cboTipoCambio.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(4, 72)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(81, 13)
        Me.Label9.TabIndex = 59
        Me.Label9.Text = "Tipo de Cambio"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboMoneda
        '
        Me.cboMoneda.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMoneda.BackColor = System.Drawing.Color.Ivory
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.ForeColor = System.Drawing.Color.Black
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.KeyEnter = True
        Me.cboMoneda.Location = New System.Drawing.Point(90, 40)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(83, 21)
        Me.cboMoneda.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(4, 43)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 13)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "Tipo de Moneda"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(478, 17)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(80, 13)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Fecha Voucher"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'cboSubdiario
        '
        Me.cboSubdiario.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboSubdiario.BackColor = System.Drawing.Color.Ivory
        Me.cboSubdiario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSubdiario.ForeColor = System.Drawing.Color.Black
        Me.cboSubdiario.FormattingEnabled = True
        Me.cboSubdiario.KeyEnter = True
        Me.cboSubdiario.Location = New System.Drawing.Point(54, 13)
        Me.cboSubdiario.Name = "cboSubdiario"
        Me.cboSubdiario.Size = New System.Drawing.Size(326, 21)
        Me.cboSubdiario.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(4, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Subdiario"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'gbTipoConversion
        '
        Me.gbTipoConversion.Controls.Add(Me.rbVenta)
        Me.gbTipoConversion.Controls.Add(Me.rbCompra)
        Me.gbTipoConversion.Location = New System.Drawing.Point(355, 62)
        Me.gbTipoConversion.Name = "gbTipoConversion"
        Me.gbTipoConversion.Size = New System.Drawing.Size(171, 28)
        Me.gbTipoConversion.TabIndex = 142
        Me.gbTipoConversion.TabStop = False
        Me.gbTipoConversion.Visible = False
        '
        'rbVenta
        '
        Me.rbVenta.AutoSize = True
        Me.rbVenta.Location = New System.Drawing.Point(96, 8)
        Me.rbVenta.Name = "rbVenta"
        Me.rbVenta.Size = New System.Drawing.Size(53, 17)
        Me.rbVenta.TabIndex = 1
        Me.rbVenta.TabStop = True
        Me.rbVenta.Text = "Venta"
        Me.rbVenta.UseVisualStyleBackColor = True
        '
        'rbCompra
        '
        Me.rbCompra.AutoSize = True
        Me.rbCompra.Location = New System.Drawing.Point(15, 8)
        Me.rbCompra.Name = "rbCompra"
        Me.rbCompra.Size = New System.Drawing.Size(61, 17)
        Me.rbCompra.TabIndex = 0
        Me.rbCompra.TabStop = True
        Me.rbCompra.Text = "Compra"
        Me.rbCompra.UseVisualStyleBackColor = True
        '
        'TabRegGeneral
        '
        Me.TabRegGeneral.Controls.Add(Me.TabAsiento)
        Me.TabRegGeneral.Controls.Add(Me.TabDatos)
        Me.TabRegGeneral.Location = New System.Drawing.Point(111, 226)
        Me.TabRegGeneral.Name = "TabRegGeneral"
        Me.TabRegGeneral.SelectedIndex = 0
        Me.TabRegGeneral.Size = New System.Drawing.Size(659, 244)
        Me.TabRegGeneral.TabIndex = 36
        '
        'TabAsiento
        '
        Me.TabAsiento.Controls.Add(Me.cboMonedaAsiento)
        Me.TabAsiento.Controls.Add(Me.lblHaber)
        Me.TabAsiento.Controls.Add(Me.lblDebe)
        Me.TabAsiento.Controls.Add(Me.Label24)
        Me.TabAsiento.Controls.Add(Me.Label23)
        Me.TabAsiento.Controls.Add(Me.Label22)
        Me.TabAsiento.Controls.Add(Me.dgvAsiento)
        Me.TabAsiento.Location = New System.Drawing.Point(4, 22)
        Me.TabAsiento.Name = "TabAsiento"
        Me.TabAsiento.Padding = New System.Windows.Forms.Padding(3)
        Me.TabAsiento.Size = New System.Drawing.Size(651, 218)
        Me.TabAsiento.TabIndex = 0
        Me.TabAsiento.Text = "Asiento Contable"
        Me.TabAsiento.UseVisualStyleBackColor = True
        '
        'cboMonedaAsiento
        '
        Me.cboMonedaAsiento.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboMonedaAsiento.BackColor = System.Drawing.Color.White
        Me.cboMonedaAsiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonedaAsiento.ForeColor = System.Drawing.Color.Black
        Me.cboMonedaAsiento.FormattingEnabled = True
        Me.cboMonedaAsiento.KeyEnter = True
        Me.cboMonedaAsiento.Location = New System.Drawing.Point(61, 16)
        Me.cboMonedaAsiento.Name = "cboMonedaAsiento"
        Me.cboMonedaAsiento.Size = New System.Drawing.Size(83, 21)
        Me.cboMonedaAsiento.TabIndex = 74
        '
        'lblHaber
        '
        Me.lblHaber.AutoSize = True
        Me.lblHaber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHaber.ForeColor = System.Drawing.Color.Black
        Me.lblHaber.Location = New System.Drawing.Point(565, 19)
        Me.lblHaber.Name = "lblHaber"
        Me.lblHaber.Size = New System.Drawing.Size(0, 13)
        Me.lblHaber.TabIndex = 73
        Me.lblHaber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDebe
        '
        Me.lblDebe.AutoSize = True
        Me.lblDebe.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDebe.ForeColor = System.Drawing.Color.Black
        Me.lblDebe.Location = New System.Drawing.Point(419, 19)
        Me.lblDebe.Name = "lblDebe"
        Me.lblDebe.Size = New System.Drawing.Size(0, 13)
        Me.lblDebe.TabIndex = 72
        Me.lblDebe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.Label24.Location = New System.Drawing.Point(348, 19)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(70, 13)
        Me.Label24.TabIndex = 62
        Me.Label24.Text = "Total Debe"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.Label23.Location = New System.Drawing.Point(489, 19)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(74, 13)
        Me.Label23.TabIndex = 61
        Me.Label23.Text = "Total Haber"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Black
        Me.Label22.Location = New System.Drawing.Point(12, 19)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(46, 13)
        Me.Label22.TabIndex = 59
        Me.Label22.Text = "Moneda"
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgvAsiento
        '
        Me.dgvAsiento.AllowUserToAddRows = False
        Me.dgvAsiento.AllowUserToDeleteRows = False
        Me.dgvAsiento.AllowUserToResizeRows = False
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        Me.dgvAsiento.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvAsiento.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01
        Me.dgvAsiento.BackgroundColor = System.Drawing.Color.Gray
        Me.dgvAsiento.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical
        Me.dgvAsiento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.Ivory
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.NavajoWhite
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAsiento.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvAsiento.EnableHeadersVisualStyles = False
        Me.dgvAsiento.Location = New System.Drawing.Point(6, 43)
        Me.dgvAsiento.MultiSelect = False
        Me.dgvAsiento.Name = "dgvAsiento"
        Me.dgvAsiento.ReadOnly = True
        Me.dgvAsiento.RowHeadersVisible = False
        Me.dgvAsiento.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAsiento.Size = New System.Drawing.Size(637, 167)
        Me.dgvAsiento.TabIndex = 0
        '
        'TabDatos
        '
        Me.TabDatos.Controls.Add(Me.gbRegistro)
        Me.TabDatos.Location = New System.Drawing.Point(4, 22)
        Me.TabDatos.Name = "TabDatos"
        Me.TabDatos.Padding = New System.Windows.Forms.Padding(3)
        Me.TabDatos.Size = New System.Drawing.Size(651, 218)
        Me.TabDatos.TabIndex = 1
        Me.TabDatos.Text = "Registro Contable"
        Me.TabDatos.UseVisualStyleBackColor = True
        '
        'gbRegistro
        '
        Me.gbRegistro.Controls.Add(Me.btnFiltro)
        Me.gbRegistro.Controls.Add(Me.PictureBox2)
        Me.gbRegistro.Controls.Add(Me.PictureBox1)
        Me.gbRegistro.Controls.Add(Me.PictureBox3)
        Me.gbRegistro.Controls.Add(Me.PictureBox10)
        Me.gbRegistro.Controls.Add(Me.lblDescAreaDet)
        Me.gbRegistro.Controls.Add(Me.txtAreaDet)
        Me.gbRegistro.Controls.Add(Me.Label40)
        Me.gbRegistro.Controls.Add(Me.dtpFechaVcmtoDet)
        Me.gbRegistro.Controls.Add(Me.Label13)
        Me.gbRegistro.Controls.Add(Me.dtpFechaDocDet)
        Me.gbRegistro.Controls.Add(Me.Label37)
        Me.gbRegistro.Controls.Add(Me.lblDescCCostoDet)
        Me.gbRegistro.Controls.Add(Me.txtCCostoDet)
        Me.gbRegistro.Controls.Add(Me.Label36)
        Me.gbRegistro.Controls.Add(Me.lblDescAnexDet)
        Me.gbRegistro.Controls.Add(Me.txtGlosaDet)
        Me.gbRegistro.Controls.Add(Me.Label21)
        Me.gbRegistro.Controls.Add(Me.txtHaber)
        Me.gbRegistro.Controls.Add(Me.txtDebe)
        Me.gbRegistro.Controls.Add(Me.Label30)
        Me.gbRegistro.Controls.Add(Me.Label29)
        Me.gbRegistro.Controls.Add(Me.txtNumDocDet)
        Me.gbRegistro.Controls.Add(Me.txtSerieDocDet)
        Me.gbRegistro.Controls.Add(Me.cboTipoDocDet)
        Me.gbRegistro.Controls.Add(Me.Label28)
        Me.gbRegistro.Controls.Add(Me.txtAnexoDet)
        Me.gbRegistro.Controls.Add(Me.Label27)
        Me.gbRegistro.Controls.Add(Me.lblDescripCuenta)
        Me.gbRegistro.Controls.Add(Me.mtbCuenta)
        Me.gbRegistro.Controls.Add(Me.Label26)
        Me.gbRegistro.Controls.Add(Me.txtSecuencia)
        Me.gbRegistro.Controls.Add(Me.Label25)
        Me.gbRegistro.Location = New System.Drawing.Point(7, 2)
        Me.gbRegistro.Name = "gbRegistro"
        Me.gbRegistro.Size = New System.Drawing.Size(634, 209)
        Me.gbRegistro.TabIndex = 0
        Me.gbRegistro.TabStop = False
        '
        'btnFiltro
        '
        Me.btnFiltro.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno
        Me.btnFiltro.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnFiltro.Location = New System.Drawing.Point(425, 95)
        Me.btnFiltro.Name = "btnFiltro"
        Me.btnFiltro.Size = New System.Drawing.Size(101, 20)
        Me.btnFiltro.TabIndex = 6
        Me.btnFiltro.Text = "Filtro Documentos"
        Me.btnFiltro.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno
        Me.btnFiltro.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(441, 124)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(20, 16)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 134
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(145, 71)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(20, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 133
        Me.PictureBox1.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.White
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(148, 45)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(20, 16)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 132
        Me.PictureBox3.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.BackColor = System.Drawing.Color.White
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(322, 18)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(20, 16)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox10.TabIndex = 131
        Me.PictureBox10.TabStop = False
        '
        'lblDescAreaDet
        '
        Me.lblDescAreaDet.AutoSize = True
        Me.lblDescAreaDet.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblDescAreaDet.Location = New System.Drawing.Point(471, 126)
        Me.lblDescAreaDet.Name = "lblDescAreaDet"
        Me.lblDescAreaDet.Size = New System.Drawing.Size(0, 13)
        Me.lblDescAreaDet.TabIndex = 130
        '
        'txtAreaDet
        '
        Me.txtAreaDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAreaDet.BackColor = System.Drawing.Color.White
        Me.txtAreaDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAreaDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAreaDet.ForeColor = System.Drawing.Color.Black
        Me.txtAreaDet.KeyEnter = True
        Me.txtAreaDet.Location = New System.Drawing.Point(387, 122)
        Me.txtAreaDet.MaxLength = 11
        Me.txtAreaDet.Name = "txtAreaDet"
        Me.txtAreaDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAreaDet.ShortcutsEnabled = False
        Me.txtAreaDet.Size = New System.Drawing.Size(76, 20)
        Me.txtAreaDet.TabIndex = 9
        Me.txtAreaDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.Color.Black
        Me.Label40.Location = New System.Drawing.Point(357, 126)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(29, 13)
        Me.Label40.TabIndex = 128
        Me.Label40.Text = "Area"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtpFechaVcmtoDet
        '
        Me.dtpFechaVcmtoDet.CalendarMonthBackground = System.Drawing.Color.Ivory
        Me.dtpFechaVcmtoDet.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVcmtoDet.Location = New System.Drawing.Point(252, 122)
        Me.dtpFechaVcmtoDet.Name = "dtpFechaVcmtoDet"
        Me.dtpFechaVcmtoDet.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaVcmtoDet.TabIndex = 8
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(186, 125)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(67, 13)
        Me.Label13.TabIndex = 127
        Me.Label13.Text = "Fecha Vmto."
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dtpFechaDocDet
        '
        Me.dtpFechaDocDet.CalendarMonthBackground = System.Drawing.Color.Ivory
        Me.dtpFechaDocDet.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaDocDet.Location = New System.Drawing.Point(77, 122)
        Me.dtpFechaDocDet.Name = "dtpFechaDocDet"
        Me.dtpFechaDocDet.Size = New System.Drawing.Size(83, 20)
        Me.dtpFechaDocDet.TabIndex = 7
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.Black
        Me.Label37.Location = New System.Drawing.Point(9, 125)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(63, 13)
        Me.Label37.TabIndex = 126
        Me.Label37.Text = "Fecha Doc."
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDescCCostoDet
        '
        Me.lblDescCCostoDet.AutoSize = True
        Me.lblDescCCostoDet.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblDescCCostoDet.Location = New System.Drawing.Point(175, 73)
        Me.lblDescCCostoDet.Name = "lblDescCCostoDet"
        Me.lblDescCCostoDet.Size = New System.Drawing.Size(0, 13)
        Me.lblDescCCostoDet.TabIndex = 122
        '
        'txtCCostoDet
        '
        Me.txtCCostoDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtCCostoDet.BackColor = System.Drawing.Color.White
        Me.txtCCostoDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCCostoDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCCostoDet.ForeColor = System.Drawing.Color.Black
        Me.txtCCostoDet.KeyEnter = True
        Me.txtCCostoDet.Location = New System.Drawing.Point(91, 69)
        Me.txtCCostoDet.MaxLength = 11
        Me.txtCCostoDet.Name = "txtCCostoDet"
        Me.txtCCostoDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtCCostoDet.ShortcutsEnabled = False
        Me.txtCCostoDet.Size = New System.Drawing.Size(76, 20)
        Me.txtCCostoDet.TabIndex = 2
        Me.txtCCostoDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.Color.Black
        Me.Label36.Location = New System.Drawing.Point(9, 73)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(83, 13)
        Me.Label36.TabIndex = 120
        Me.Label36.Text = "Centro de Costo"
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDescAnexDet
        '
        Me.lblDescAnexDet.AutoSize = True
        Me.lblDescAnexDet.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblDescAnexDet.Location = New System.Drawing.Point(178, 47)
        Me.lblDescAnexDet.Name = "lblDescAnexDet"
        Me.lblDescAnexDet.Size = New System.Drawing.Size(0, 13)
        Me.lblDescAnexDet.TabIndex = 118
        '
        'txtGlosaDet
        '
        Me.txtGlosaDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtGlosaDet.BackColor = System.Drawing.Color.White
        Me.txtGlosaDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtGlosaDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGlosaDet.ForeColor = System.Drawing.Color.Black
        Me.txtGlosaDet.KeyEnter = True
        Me.txtGlosaDet.Location = New System.Drawing.Point(84, 179)
        Me.txtGlosaDet.MaxLength = 100
        Me.txtGlosaDet.Name = "txtGlosaDet"
        Me.txtGlosaDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Tres
        Me.txtGlosaDet.ShortcutsEnabled = False
        Me.txtGlosaDet.Size = New System.Drawing.Size(496, 20)
        Me.txtGlosaDet.TabIndex = 12
        Me.txtGlosaDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Black
        Me.Label21.Location = New System.Drawing.Point(9, 184)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(70, 13)
        Me.Label21.TabIndex = 104
        Me.Label21.Text = "Glosa Detalle"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtHaber
        '
        Me.txtHaber.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtHaber.BackColor = System.Drawing.Color.White
        Me.txtHaber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHaber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtHaber.ForeColor = System.Drawing.Color.Black
        Me.txtHaber.KeyEnter = True
        Me.txtHaber.Location = New System.Drawing.Point(208, 150)
        Me.txtHaber.MaxLength = 11
        Me.txtHaber.Name = "txtHaber"
        Me.txtHaber.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtHaber.ShortcutsEnabled = False
        Me.txtHaber.Size = New System.Drawing.Size(68, 20)
        Me.txtHaber.TabIndex = 11
        Me.txtHaber.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'txtDebe
        '
        Me.txtDebe.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtDebe.BackColor = System.Drawing.Color.White
        Me.txtDebe.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDebe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDebe.ForeColor = System.Drawing.Color.Black
        Me.txtDebe.KeyEnter = True
        Me.txtDebe.Location = New System.Drawing.Point(48, 150)
        Me.txtDebe.MaxLength = 11
        Me.txtDebe.Name = "txtDebe"
        Me.txtDebe.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtDebe.ShortcutsEnabled = False
        Me.txtDebe.Size = New System.Drawing.Size(68, 20)
        Me.txtDebe.TabIndex = 10
        Me.txtDebe.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.ForeColor = System.Drawing.Color.Black
        Me.Label30.Location = New System.Drawing.Point(154, 155)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(36, 13)
        Me.Label30.TabIndex = 101
        Me.Label30.Text = "Haber"
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.Black
        Me.Label29.Location = New System.Drawing.Point(9, 155)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(33, 13)
        Me.Label29.TabIndex = 100
        Me.Label29.Text = "Debe"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNumDocDet
        '
        Me.txtNumDocDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumDocDet.BackColor = System.Drawing.Color.White
        Me.txtNumDocDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumDocDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumDocDet.ForeColor = System.Drawing.Color.Black
        Me.txtNumDocDet.KeyEnter = True
        Me.txtNumDocDet.Location = New System.Drawing.Point(326, 95)
        Me.txtNumDocDet.MaxLength = 11
        Me.txtNumDocDet.Name = "txtNumDocDet"
        Me.txtNumDocDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumDocDet.ShortcutsEnabled = False
        Me.txtNumDocDet.Size = New System.Drawing.Size(93, 20)
        Me.txtNumDocDet.TabIndex = 5
        Me.txtNumDocDet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumDocDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtSerieDocDet
        '
        Me.txtSerieDocDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSerieDocDet.BackColor = System.Drawing.Color.White
        Me.txtSerieDocDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerieDocDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerieDocDet.ForeColor = System.Drawing.Color.Black
        Me.txtSerieDocDet.KeyEnter = True
        Me.txtSerieDocDet.Location = New System.Drawing.Point(278, 95)
        Me.txtSerieDocDet.MaxLength = 11
        Me.txtSerieDocDet.Name = "txtSerieDocDet"
        Me.txtSerieDocDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtSerieDocDet.ShortcutsEnabled = False
        Me.txtSerieDocDet.Size = New System.Drawing.Size(42, 20)
        Me.txtSerieDocDet.TabIndex = 4
        Me.txtSerieDocDet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSerieDocDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'cboTipoDocDet
        '
        Me.cboTipoDocDet.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboTipoDocDet.BackColor = System.Drawing.Color.White
        Me.cboTipoDocDet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDocDet.ForeColor = System.Drawing.Color.Black
        Me.cboTipoDocDet.FormattingEnabled = True
        Me.cboTipoDocDet.KeyEnter = True
        Me.cboTipoDocDet.Location = New System.Drawing.Point(111, 95)
        Me.cboTipoDocDet.Name = "cboTipoDocDet"
        Me.cboTipoDocDet.Size = New System.Drawing.Size(161, 21)
        Me.cboTipoDocDet.TabIndex = 3
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.Black
        Me.Label28.Location = New System.Drawing.Point(9, 98)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(101, 13)
        Me.Label28.TabIndex = 96
        Me.Label28.Text = "Tipo de Documento"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtAnexoDet
        '
        Me.txtAnexoDet.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAnexoDet.BackColor = System.Drawing.Color.White
        Me.txtAnexoDet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtAnexoDet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAnexoDet.ForeColor = System.Drawing.Color.Black
        Me.txtAnexoDet.KeyEnter = True
        Me.txtAnexoDet.Location = New System.Drawing.Point(70, 43)
        Me.txtAnexoDet.MaxLength = 11
        Me.txtAnexoDet.Name = "txtAnexoDet"
        Me.txtAnexoDet.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAnexoDet.ShortcutsEnabled = False
        Me.txtAnexoDet.Size = New System.Drawing.Size(100, 20)
        Me.txtAnexoDet.TabIndex = 1
        Me.txtAnexoDet.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Black
        Me.Label27.Location = New System.Drawing.Point(9, 47)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(37, 13)
        Me.Label27.TabIndex = 92
        Me.Label27.Text = "Anexo"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblDescripCuenta
        '
        Me.lblDescripCuenta.AutoSize = True
        Me.lblDescripCuenta.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblDescripCuenta.Location = New System.Drawing.Point(351, 20)
        Me.lblDescripCuenta.Name = "lblDescripCuenta"
        Me.lblDescripCuenta.Size = New System.Drawing.Size(0, 13)
        Me.lblDescripCuenta.TabIndex = 91
        '
        'mtbCuenta
        '
        Me.mtbCuenta.BackColor = System.Drawing.Color.White
        Me.mtbCuenta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.mtbCuenta.Location = New System.Drawing.Point(266, 16)
        Me.mtbCuenta.Name = "mtbCuenta"
        Me.mtbCuenta.Size = New System.Drawing.Size(78, 20)
        Me.mtbCuenta.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Black
        Me.Label26.Location = New System.Drawing.Point(174, 20)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(86, 13)
        Me.Label26.TabIndex = 88
        Me.Label26.Text = "Cuenta Contable"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtSecuencia
        '
        Me.txtSecuencia.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtSecuencia.BackColor = System.Drawing.Color.White
        Me.txtSecuencia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSecuencia.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSecuencia.Enabled = False
        Me.txtSecuencia.ForeColor = System.Drawing.Color.Black
        Me.txtSecuencia.KeyEnter = True
        Me.txtSecuencia.Location = New System.Drawing.Point(70, 16)
        Me.txtSecuencia.MaxLength = 11
        Me.txtSecuencia.Name = "txtSecuencia"
        Me.txtSecuencia.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Tres
        Me.txtSecuencia.ShortcutsEnabled = False
        Me.txtSecuencia.Size = New System.Drawing.Size(70, 20)
        Me.txtSecuencia.TabIndex = 87
        Me.txtSecuencia.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.[Decimal]
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Black
        Me.Label25.Location = New System.Drawing.Point(9, 20)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(58, 13)
        Me.Label25.TabIndex = 86
        Me.Label25.Text = "Secuencia"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtNumComp
        '
        Me.txtNumComp.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumComp.BackColor = System.Drawing.Color.Ivory
        Me.txtNumComp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtNumComp.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumComp.Enabled = False
        Me.txtNumComp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumComp.ForeColor = System.Drawing.Color.Black
        Me.txtNumComp.KeyEnter = True
        Me.txtNumComp.Location = New System.Drawing.Point(577, 12)
        Me.txtNumComp.MaxLength = 20
        Me.txtNumComp.Name = "txtNumComp"
        Me.txtNumComp.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumComp.ShortcutsEnabled = False
        Me.txtNumComp.Size = New System.Drawing.Size(179, 20)
        Me.txtNumComp.TabIndex = 34
        Me.txtNumComp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtNumComp.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'FrmCompConConversion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(778, 485)
        Me.Controls.Add(Me.TabRegGeneral)
        Me.Controls.Add(Me.txtNumComp)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.gbBotonera)
        Me.Controls.Add(Me.gbCabecera)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "FrmCompConConversion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = ":: Comprobantes con Conversion ::"
        Me.gbBotonera.ResumeLayout(False)
        Me.gbBotonera.PerformLayout()
        Me.gbCabecera.ResumeLayout(False)
        Me.gbCabecera.PerformLayout()
        Me.gbEspecial.ResumeLayout(False)
        CType(Me.dgvEpecial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbFechaTCambio.ResumeLayout(False)
        CType(Me.dgvFechaTCambio, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbTipoConversion.ResumeLayout(False)
        Me.gbTipoConversion.PerformLayout()
        Me.TabRegGeneral.ResumeLayout(False)
        Me.TabAsiento.ResumeLayout(False)
        Me.TabAsiento.PerformLayout()
        CType(Me.dgvAsiento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabDatos.ResumeLayout(False)
        Me.gbRegistro.ResumeLayout(False)
        Me.gbRegistro.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbBotonera As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnGenerar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnNuevoCab As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
    Friend WithEvents btnEditar As System.Windows.Forms.Button
    Friend WithEvents btnNuevoDet As System.Windows.Forms.Button
    Friend WithEvents txtNumComp As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents gbCabecera As System.Windows.Forms.GroupBox
    Friend WithEvents gbEspecial As System.Windows.Forms.GroupBox
    Friend WithEvents btnCerrarEspecial As ctrLibreria.Controles.BeButton
    Friend WithEvents dgvEpecial As System.Windows.Forms.DataGridView
    Friend WithEvents gbFechaTCambio As System.Windows.Forms.GroupBox
    Friend WithEvents BeButton1 As ctrLibreria.Controles.BeButton
    Friend WithEvents dgvFechaTCambio As System.Windows.Forms.DataGridView
    Friend WithEvents dtpFechaTCambio As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents dtpFechaVoucher As ctrLibreria.Controles.BeDateTimePicker
    Friend WithEvents lblTipoAnexo As System.Windows.Forms.Label
    Friend WithEvents txtGlosaCab As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtValorCambio As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboTipoCambio As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboMoneda As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboSubdiario As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents gbTipoConversion As System.Windows.Forms.GroupBox
    Friend WithEvents rbVenta As System.Windows.Forms.RadioButton
    Friend WithEvents rbCompra As System.Windows.Forms.RadioButton
    Friend WithEvents TabRegGeneral As System.Windows.Forms.TabControl
    Friend WithEvents TabAsiento As System.Windows.Forms.TabPage
    Friend WithEvents cboMonedaAsiento As ctrLibreria.Controles.BeComboBox
    Friend WithEvents lblHaber As System.Windows.Forms.Label
    Friend WithEvents lblDebe As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents dgvAsiento As ctrLibreria.Controles.BeDataGridView
    Friend WithEvents TabDatos As System.Windows.Forms.TabPage
    Friend WithEvents gbRegistro As System.Windows.Forms.GroupBox
    Friend WithEvents lblDescAreaDet As System.Windows.Forms.Label
    Friend WithEvents txtAreaDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaVcmtoDet As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaDocDet As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents lblDescCCostoDet As System.Windows.Forms.Label
    Friend WithEvents txtCCostoDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents lblDescAnexDet As System.Windows.Forms.Label
    Friend WithEvents txtGlosaDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtHaber As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtDebe As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtNumDocDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtSerieDocDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents cboTipoDocDet As ctrLibreria.Controles.BeComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtAnexoDet As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents lblDescripCuenta As System.Windows.Forms.Label
    Friend WithEvents mtbCuenta As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtSecuencia As ctrLibreria.Controles.BeTextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents btnFiltro As ctrLibreria.Controles.BeButton
    Friend WithEvents lblDiferencia As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
End Class
