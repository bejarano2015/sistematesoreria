﻿Imports CapaEntidad.Impuestos
Imports BusinessLogicLayer.Impuestos
Imports BusinessLogicLayer.Mantenimiento
Imports CapaEntidad

Public Class frmImpuestos

    Dim Fila As DataRow
    Dim dtTable As DataTable
    Dim VL_IMPUESTOS As SrvImpuesto
    Dim VL_IMPUESTOS_VIGENTES As SrvImpuestoVigente
    Dim OBJ_IMPUESTO As New BeanImpuestos
    Dim OBJ_IMPUESTO_VIG As New BeanImpuesto_Vigente
    Dim VL_BOTON As String
    Public Const AvisoMSGBOX As String = "SISTEMA TESORERIA"

#Region "Singlenton  = Instancia Unica de Objeto"
    Private Shared frmInstance As frmImpuestos = Nothing
    Public Shared Function Instance() As frmImpuestos
        If frmInstance Is Nothing Then
            frmInstance = New frmImpuestos
        End If
        frmInstance.BringToFront()
        Return frmInstance
    End Function
    Private Sub frmPartidas_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmInstance = Nothing
    End Sub
#End Region


    Private Sub frmImpuestos_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        LISTAR_IMPUESTO()
        LISTAR_CUENTAS_CONTABLES()
        cboPorcentaje.SelectedIndex = 0
        cboVigente.SelectedIndex = 0
        TabPersonalizado1.SelectedIndex = 0
        TabPersonalizado1.DisablePage(PAGE_MANTENIMIENTO)

        'VL_BOTON = "0"
    End Sub

#Region "VALIDAR CAMPOS DE TEXTBOX"

    Sub limpiarcontroles()
        txtNombre.Text = ""
        txtAbreviacion.Text = ""
        txtImpuesto.Text = ""
        txtNumImpuesto.Text = ""
        cboCuenta.SelectedIndex = 0
        cboPorcentaje.SelectedIndex = 0
        cboVigente.SelectedIndex = 0
    End Sub
#End Region

#Region "IMPUESTO ---LISTAR, AGREGAR, ACTUALIZAR Y ELIMINAR"

    Private Sub LISTAR_CUENTAS_CONTABLES()

        VL_IMPUESTOS = New SrvImpuesto()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_IMPUESTOS.SRV_LISTAR_CUENTAS()

        If VL_BeanResultado.blnExiste = True Then
            Me.cboCuenta.DataSource = VL_BeanResultado.dtResultado
            Me.cboCuenta.DisplayMember = "CUENTA"
            Me.cboCuenta.ValueMember = "ID_CTA_CTBLE"
        End If
    End Sub

    Private Sub LISTAR_IMPUESTO()

        VL_IMPUESTOS = New SrvImpuesto()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        VL_BeanResultado = VL_IMPUESTOS.SRV_LISTAR_IMPUESTOS()

        If VL_BeanResultado.blnExiste = True Then
            'Me.dgTablaImpuestos.AutoGenerateColumns = False
            'Me.dgTablaImpuestos.DataSource = VL_BeanResultado.dtResultado
            Me.rgLista.DataSource = VL_BeanResultado.dtResultado
        End If
    End Sub


    Private Sub AGREGAR_IMPUESTO()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        OBJ_IMPUESTO = New BeanImpuestos(txtNombre.Text, txtAbreviacion.Text, cboCuenta.SelectedValue.ToString(), 1)


        VL_BeanResultado = VL_IMPUESTOS.SRV_AGREGAR_IMPUESTO(OBJ_IMPUESTO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If

    End Sub

    Private Sub MODIFICAR_IMPUESTO()
        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
        
        OBJ_IMPUESTO.IMP_NOMBRE = txtNombre.Text
        OBJ_IMPUESTO.IMP_ABREVIACION = txtAbreviacion.Text
        OBJ_IMPUESTO.ID_CUENTA = cboCuenta.SelectedValue.ToString()
        OBJ_IMPUESTO.ID_IMPUESTO = txtNumImpuesto.Text
        OBJ_IMPUESTO.TIPO = 2

        VL_BeanResultado = VL_IMPUESTOS.SRV_ACTUALIZAR_IMPUESTO(OBJ_IMPUESTO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria")
            Return
        End If
    End Sub

    Private Sub ELIMINAR_IMPUESTO()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ID As String = rgLista.CurrentRow.Cells("ID_Impuesto").Value

        OBJ_IMPUESTO.ID_IMPUESTO = VL_ID
        OBJ_IMPUESTO.TIPO = 3

        VL_BeanResultado = VL_IMPUESTOS.SRV_ELIMINAR_IMPUESTO(OBJ_IMPUESTO)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub
#End Region


#Region "IMPUESTOS VIGENTES--  LISTAR, AGREGAR, ACTUALIZAR Y ELIMINAR"
    Private Sub LISTAR_IMPUESTO_VIGENTE()

        VL_IMPUESTOS_VIGENTES = New SrvImpuestoVigente()
        dtTable = New DataTable()
        Dim VL_BeanResultado As New BeanResultado.ResultadoSeleccion()

        OBJ_IMPUESTO_VIG.ID_IMPUESTO = rgLista.CurrentRow.Cells("ID_Impuesto").Value

        VL_BeanResultado = VL_IMPUESTOS_VIGENTES.SRV_LISTAR_IMPUESTOS_VIG(OBJ_IMPUESTO_VIG)

        If VL_BeanResultado.blnExiste = True Then
            'Me.dgTablaImpuestos.AutoGenerateColumns = False
            Me.dgImpVigentes.DataSource = VL_BeanResultado.dtResultado
        End If
    End Sub


    Private Sub AGREGAR_IMPUESTO_VIG()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ES_PORCENTAJE As String = cboPorcentaje.SelectedItem
        Dim VL_VIGENTE As String = cboVigente.SelectedItem

        If (VL_ES_PORCENTAJE = "SI") Then
            VL_ES_PORCENTAJE = "1"
        Else
            VL_ES_PORCENTAJE = "0"
        End If

        If (VL_VIGENTE = "SI") Then
            VL_VIGENTE = "1"
        Else
            VL_VIGENTE = "0"
        End If

        OBJ_IMPUESTO_VIG = New BeanImpuesto_Vigente(txtNumImpuesto.Text, txtImpuesto.Text, VL_ES_PORCENTAJE, VL_VIGENTE,
                                                    fecha_desde.Value.ToString(), fecha_hasta.Value.ToString(), 1)


        VL_BeanResultado = VL_IMPUESTOS_VIGENTES.SRV_AGREGAR_IMPUESTO_VIG(OBJ_IMPUESTO_VIG)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If

    End Sub

    Private Sub MODIFICAR_IMPUESTO_VIG()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim VL_ES_PORCENTAJE As String = cboPorcentaje.SelectedItem
        Dim VL_VIGENTE As String = cboVigente.SelectedItem

        If (VL_ES_PORCENTAJE = "SI") Then
            VL_ES_PORCENTAJE = "1"
        Else
            VL_ES_PORCENTAJE = "0"
        End If

        If (VL_VIGENTE = "SI") Then
            VL_VIGENTE = "1"
        Else
            VL_VIGENTE = "0"
        End If

        Dim ID As Int16 = dgImpVigentes.Item("ID_ImpuestoVigencia", dgImpVigentes.CurrentRow.Index).Value
        OBJ_IMPUESTO_VIG.ID_IMPUESTO_VIGENCIA = ID

        OBJ_IMPUESTO_VIG = New BeanImpuesto_Vigente(ID, txtNumImpuesto.Text, txtImpuesto.Text, VL_ES_PORCENTAJE, VL_VIGENTE,
                                                    fecha_desde.Value.ToString(), fecha_hasta.Value.ToString(), 2)


        VL_BeanResultado = VL_IMPUESTOS_VIGENTES.SRV_ACTUALIZAR_IMPUESTO_VIG(OBJ_IMPUESTO_VIG)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub
    Private Sub ELIMINAR_IMPUESTO_VIG()

        Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()

        Dim objTec As String = dgImpVigentes.Item("ID_ImpuestoVigencia", dgImpVigentes.CurrentRow.Index).Value

        OBJ_IMPUESTO_VIG.ID_IMPUESTO_VIGENCIA = objTec
        OBJ_IMPUESTO_VIG.TIPO = 3


        VL_BeanResultado = VL_IMPUESTOS_VIGENTES.SRV_ELIMINAR_IMPUESTO_VIG(OBJ_IMPUESTO_VIG)

        If VL_BeanResultado.blnResultado = False Then
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        Else
            MessageBox.Show(VL_BeanResultado.strMensaje, "SISTEMA TESORERIA")
            Return
        End If
    End Sub

#End Region


#Region "EVENTOS DE GRILLA"

    Private Sub dgTablaImpuestos_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        'txtNumImpuesto.Text = dgTablaImpuestos.CurrentRow.Cells("ID_Impuesto").Value
        'txtNombre.Text = dgTablaImpuestos.CurrentRow.Cells("Nombre").Value
        'txtAbreviacion.Text = dgTablaImpuestos.CurrentRow.Cells("Abreviacion").Value

        'cboCuenta.SelectedValue = dgTablaImpuestos.CurrentRow.Cells("ID_Cuenta").Value

        'TabPersonalizado1.SelectedIndex = 1
    End Sub

#End Region

#Region "MENU CONTEXTUAL"
    Private Sub MECO_MODIFICAR_Click(sender As Object, e As EventArgs) Handles MECO_MODIFICAR.Click
        VL_BOTON = "1"
        txtNumImpuesto.Text = rgLista.CurrentRow.Cells("ID_Impuesto").Value
        txtNombre.Text = rgLista.CurrentRow.Cells("Nombre").Value
        txtAbreviacion.Text = rgLista.CurrentRow.Cells("Abreviacion").Value

        cboCuenta.SelectedValue = rgLista.CurrentRow.Cells("ID_Cuenta").Value
        TabPersonalizado1.SelectedIndex = 1
        TabPersonalizado1.EnablePage(PAGE_MANTENIMIENTO)
        txtAbreviacion.Enabled = True
        txtNombre.Enabled = True
        cboCuenta.Enabled = True

    End Sub

    Private Sub MECO_ELIMINAR_Click(sender As Object, e As EventArgs) Handles MECO_ELIMINAR.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR_IMPUESTO()
            LISTAR_IMPUESTO()
            LISTAR_IMPUESTO_VIGENTE()
        End If
    End Sub

    Private Sub MECO_VER_Click(sender As Object, e As EventArgs) Handles MECO_VER.Click
        txtNumImpuesto.Text = rgLista.CurrentRow.Cells("ID_Impuesto").Value
        TabPersonalizado1.SelectedIndex = 1
        TabPersonalizado1.EnablePage(PAGE_MANTENIMIENTO)
        LISTAR_IMPUESTO_VIGENTE()
    End Sub

    Private Sub MECO_VIGENCIA_Click(sender As Object, e As EventArgs) Handles MECO_VIGENCIA.Click
        VL_BOTON = "2" 'agregar impuesto vigente

        GRUPO_VIGENTE.Enabled = True
        txtNumImpuesto.Text = rgLista.CurrentRow.Cells("ID_Impuesto").Value
        txtNombre.Text = rgLista.CurrentRow.Cells("Nombre").Value
        txtAbreviacion.Text = rgLista.CurrentRow.Cells("Abreviacion").Value

        TabPersonalizado1.EnablePage(PAGE_MANTENIMIENTO)
        txtAbreviacion.Enabled = False
        txtNombre.Enabled = False
        cboCuenta.Enabled = False
        cboCuenta.SelectedValue = rgLista.CurrentRow.Cells("ID_Cuenta").Value
        TabPersonalizado1.SelectedIndex = 1
        LISTAR_IMPUESTO_VIGENTE()
    End Sub


    Private Sub ACTUALIZAR_Click(sender As Object, e As EventArgs) Handles ACTUALIZAR.Click

        VL_BOTON = "3"
        GRUPO_VIGENTE.Enabled = True

        txtNombre.Text = rgLista.CurrentRow.Cells("Nombre").Value
        txtAbreviacion.Text = rgLista.CurrentRow.Cells("Abreviacion").Value
        cboCuenta.SelectedValue = rgLista.CurrentRow.Cells("ID_Cuenta").Value

        txtNumImpuesto.Text = dgImpVigentes.CurrentRow.Cells("ID_ImpuestoS").Value
        txtImpuesto.Text = dgImpVigentes.CurrentRow.Cells("Porcentaje_Valor").Value
        cboPorcentaje.SelectedItem = dgImpVigentes.CurrentRow.Cells("EsPorcentaje").Value
        cboVigente.SelectedItem = dgImpVigentes.CurrentRow.Cells("Vigente").Value
        fecha_desde.Value = dgImpVigentes.CurrentRow.Cells("Vigencia_Desde").Value
        fecha_hasta.Value = dgImpVigentes.CurrentRow.Cells("Vigencia_Hasta").Value

    End Sub

    Private Sub ELIMINAR_Click(sender As Object, e As EventArgs) Handles ELIMINAR.Click
        Dim msg = MessageBox.Show("Esta Seguro que desea Eliminar...", AvisoMSGBOX, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If msg = DialogResult.Yes Then
            ELIMINAR_IMPUESTO_VIG()
            LISTAR_IMPUESTO()
            LISTAR_IMPUESTO_VIGENTE()
        End If
    End Sub

#End Region

#Region "BOTONES"
    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        TabPersonalizado1.SelectedIndex = 1
        VL_BOTON = "0"
        txtAbreviacion.Enabled = True
        txtNombre.Enabled = True
        cboCuenta.Enabled = True
        TabPersonalizado1.EnablePage(PAGE_MANTENIMIENTO)

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        If (VL_BOTON = "0") Or txtNumImpuesto.Text = "" Then  ''AGREGAR IMPUESTO

            txtImpuesto.Text = "0"

            If txtNombre.Text = "" Then

                MessageBox.Show("Ingrese Nombre..'")

            ElseIf txtAbreviacion.Text = "" Then
                MessageBox.Show("Ingrese Abreviación...", "SISTEMA TESORERIA")

            ElseIf txtImpuesto.Text = "" Then
                MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            Else
                AGREGAR_IMPUESTO()
                LISTAR_IMPUESTO()
                LISTAR_IMPUESTO_VIGENTE()
                limpiarcontroles()
                GRUPO_VIGENTE.Enabled = False
                TabPersonalizado1.SelectedIndex = 0
            End If

        End If

        If (VL_BOTON = "1") Then ''EDITAR IMPUESTO
            txtImpuesto.Text = "0"
            If txtNombre.Text = "" Then

                MessageBox.Show("Ingrese Nombre...!", "SISTEMA TESORERIA")

            ElseIf txtAbreviacion.Text = "" Then
                MessageBox.Show("Ingrese Abreviación...!", "SISTEMA TESORERIA")

            ElseIf txtImpuesto.Text = "" Then
                MessageBox.Show("Ingrese Impuesto...!", "SISTEMA TESORERIA")
            Else
                MODIFICAR_IMPUESTO()
                LISTAR_IMPUESTO()
                LISTAR_IMPUESTO_VIGENTE()
                limpiarcontroles()
                GRUPO_VIGENTE.Enabled = False
                TabPersonalizado1.SelectedIndex = 0
            End If
        End If



        If (VL_BOTON = "2") Then ''AGREGAR IMPUESTO VIGENTE

            If txtNombre.Text = "" Then

                MessageBox.Show("Ingrese Nombre...!", "SISTEMA TESORERIA")

            ElseIf txtAbreviacion.Text = "" Then
                MessageBox.Show("Ingrese Abreviación...", "SISTEMA TESORERIA")

            ElseIf txtImpuesto.Text = "" Then
                MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            Else
                AGREGAR_IMPUESTO_VIG()
                limpiarcontroles()

                LISTAR_IMPUESTO_VIGENTE()
                LISTAR_IMPUESTO()
                GRUPO_VIGENTE.Enabled = False

            End If

        End If

        If (VL_BOTON = "3") Then  ''EDITAR IMPUESTO VIGENTE
            If txtNombre.Text = "" Then

                MessageBox.Show("Ingrese Nombre...", "SISTEMA TESORERIA")

            ElseIf txtAbreviacion.Text = "" Then
                MessageBox.Show("Ingrese Abreviación...", "SISTEMA TESORERIA")

            ElseIf txtImpuesto.Text = "" Then
                MessageBox.Show("Ingrese Impuesto...", "SISTEMA TESORERIA")
            Else
                MODIFICAR_IMPUESTO_VIG()

                LISTAR_IMPUESTO_VIGENTE()
                LISTAR_IMPUESTO()
                limpiarcontroles()
                GRUPO_VIGENTE.Enabled = False

            End If
        End If

        ''VL_BOTON = ""
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If (VL_BOTON = "0") Then
            TabPersonalizado1.SelectedIndex = 0
        End If
        If (VL_BOTON = "") Then
            Dispose()
        End If
        If (VL_BOTON = "1") Then
            TabPersonalizado1.SelectedIndex = 0

        End If
        If (VL_BOTON = "2") Then
            TabPersonalizado1.SelectedIndex = 0
            GRUPO_VIGENTE.Enabled = False
        End If
        If (VL_BOTON = "3") Then
            TabPersonalizado1.SelectedIndex = 0
            GRUPO_VIGENTE.Enabled = False
        End If

        VL_BOTON = ""

        txtAbreviacion.Enabled = False
        txtNombre.Enabled = False
        cboCuenta.Enabled = False
        limpiarcontroles()
        TabPersonalizado1.DisablePage(PAGE_MANTENIMIENTO)
    End Sub

#End Region

  
#Region "VALIDACION DE SOLO LETRAS Y NUMEROS"
    Public Sub SoloLetras(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Public Sub SoloNumeros(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Char.IsDigit(e.KeyChar) = False And Char.IsControl(e.KeyChar) = False Then
            e.Handled = True
        Else
            e.Handled = False
        End If
    End Sub

    Public Function NumeroDec(ByVal e As System.Windows.Forms.KeyPressEventArgs, ByRef texto As TextBox) As Boolean
        If UCase(e.KeyChar) Like "[!0-9.-]" Then
            Return True
        End If
        Dim c As Short = 0
        If UCase(e.KeyChar) Like "[.]" Then
            If InStr(Texto.Text, ".") > 0 Then
                Return True
            Else
                Return False
            End If
        End If
    End Function
#End Region

#Region "EVENTOS KEY PRESS"

    Private Sub txtBuscar_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBuscar.KeyPress
        e.Handled = True
    End Sub
    Private Sub txtNumImpuesto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumImpuesto.KeyPress
        SoloNumeros(txtNumImpuesto.Text, e)
    End Sub

    Private Sub txtImpuesto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtImpuesto.KeyPress
        e.Handled = NumeroDec(e, txtImpuesto)
    End Sub

    Private Sub txtAbreviacion_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtAbreviacion.KeyPress
        SoloLetras(txtAbreviacion.Text, e)
    End Sub
#End Region

  
End Class