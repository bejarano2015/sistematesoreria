﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImpuestos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImpuestos))
        Dim GridViewTextBoxColumn1 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn2 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn3 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn4 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn5 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn6 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn7 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Dim GridViewTextBoxColumn8 As Telerik.WinControls.UI.GridViewTextBoxColumn = New Telerik.WinControls.UI.GridViewTextBoxColumn()
        Me.dgImpVigentes = New System.Windows.Forms.DataGridView()
        Me.EsPorcentajeS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EsPorcentaje = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ID_ImpuestoVigencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ID_ImpuestoS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AbreviacionS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ID_CuentaS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Porcentaje_Valor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vigencia_desde = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vigencia_hasta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VigenteS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Vigente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CONTEX = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ACTUALIZAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.ELIMINAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.MENUCONTEXTUAL = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.MECO_MODIFICAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_ELIMINAR = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_VIGENCIA = New System.Windows.Forms.ToolStripMenuItem()
        Me.MECO_VER = New System.Windows.Forms.ToolStripMenuItem()
        Me.BeLabel1 = New ctrLibreria.Controles.BeLabel()
        Me.txtBuscar = New ctrLibreria.Controles.BeTextBox()
        Me.TabPersonalizado1 = New Controles_DHMont.TabPersonalizado()
        Me.PAGE_CONSULTA = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rgLista = New Telerik.WinControls.UI.RadGridView()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.PAGE_MANTENIMIENTO = New System.Windows.Forms.TabPage()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.GRUPO_VIGENTE = New System.Windows.Forms.GroupBox()
        Me.fecha_hasta = New System.Windows.Forms.DateTimePicker()
        Me.fecha_desde = New System.Windows.Forms.DateTimePicker()
        Me.cboVigente = New ctrLibreria.Controles.BeComboBox()
        Me.cboPorcentaje = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel10 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel6 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel4 = New ctrLibreria.Controles.BeLabel()
        Me.BeLabel9 = New ctrLibreria.Controles.BeLabel()
        Me.txtImpuesto = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel8 = New ctrLibreria.Controles.BeLabel()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BeLabel7 = New ctrLibreria.Controles.BeLabel()
        Me.txtNumImpuesto = New ctrLibreria.Controles.BeTextBox()
        Me.txtAbreviacion = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel2 = New ctrLibreria.Controles.BeLabel()
        Me.txtNombre = New ctrLibreria.Controles.BeTextBox()
        Me.BeLabel3 = New ctrLibreria.Controles.BeLabel()
        Me.cboCuenta = New ctrLibreria.Controles.BeComboBox()
        Me.BeLabel5 = New ctrLibreria.Controles.BeLabel()
        Me.lblCuenta = New ctrLibreria.Controles.BeLabel()
        CType(Me.dgImpVigentes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONTEX.SuspendLayout()
        Me.MENUCONTEXTUAL.SuspendLayout()
        Me.TabPersonalizado1.SuspendLayout()
        Me.PAGE_CONSULTA.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.rgLista, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.rgLista.MasterTemplate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PAGE_MANTENIMIENTO.SuspendLayout()
        Me.GRUPO_VIGENTE.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgImpVigentes
        '
        Me.dgImpVigentes.AllowUserToAddRows = False
        Me.dgImpVigentes.AllowUserToDeleteRows = False
        Me.dgImpVigentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgImpVigentes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EsPorcentajeS, Me.EsPorcentaje, Me.ID_ImpuestoVigencia, Me.ID_ImpuestoS, Me.NombreS, Me.AbreviacionS, Me.ID_CuentaS, Me.Porcentaje_Valor, Me.Vigencia_desde, Me.Vigencia_hasta, Me.VigenteS, Me.Vigente})
        Me.dgImpVigentes.ContextMenuStrip = Me.CONTEX
        Me.dgImpVigentes.Location = New System.Drawing.Point(6, 19)
        Me.dgImpVigentes.Name = "dgImpVigentes"
        Me.dgImpVigentes.ReadOnly = True
        Me.dgImpVigentes.Size = New System.Drawing.Size(675, 146)
        Me.dgImpVigentes.TabIndex = 0
        '
        'EsPorcentajeS
        '
        Me.EsPorcentajeS.DataPropertyName = "EsPorcentaje"
        Me.EsPorcentajeS.HeaderText = "EsPorcentajeS"
        Me.EsPorcentajeS.Name = "EsPorcentajeS"
        Me.EsPorcentajeS.ReadOnly = True
        Me.EsPorcentajeS.Visible = False
        '
        'EsPorcentaje
        '
        Me.EsPorcentaje.DataPropertyName = "PORCENTAJE_ES"
        Me.EsPorcentaje.HeaderText = "Es Porcentaje"
        Me.EsPorcentaje.Name = "EsPorcentaje"
        Me.EsPorcentaje.ReadOnly = True
        Me.EsPorcentaje.Width = 150
        '
        'ID_ImpuestoVigencia
        '
        Me.ID_ImpuestoVigencia.DataPropertyName = "ID_ImpuestoVigencia"
        Me.ID_ImpuestoVigencia.HeaderText = "ID_ImpuestoVigencia"
        Me.ID_ImpuestoVigencia.Name = "ID_ImpuestoVigencia"
        Me.ID_ImpuestoVigencia.ReadOnly = True
        Me.ID_ImpuestoVigencia.Visible = False
        '
        'ID_ImpuestoS
        '
        Me.ID_ImpuestoS.DataPropertyName = "ID_Impuesto"
        Me.ID_ImpuestoS.HeaderText = "ID_ImpuestoS"
        Me.ID_ImpuestoS.Name = "ID_ImpuestoS"
        Me.ID_ImpuestoS.ReadOnly = True
        Me.ID_ImpuestoS.Visible = False
        '
        'NombreS
        '
        Me.NombreS.DataPropertyName = "Nombre"
        Me.NombreS.HeaderText = "NombreS"
        Me.NombreS.Name = "NombreS"
        Me.NombreS.ReadOnly = True
        Me.NombreS.Visible = False
        '
        'AbreviacionS
        '
        Me.AbreviacionS.DataPropertyName = "Abreviacion"
        Me.AbreviacionS.HeaderText = "AbreviacionS"
        Me.AbreviacionS.Name = "AbreviacionS"
        Me.AbreviacionS.ReadOnly = True
        Me.AbreviacionS.Visible = False
        '
        'ID_CuentaS
        '
        Me.ID_CuentaS.DataPropertyName = "ID_Cuenta"
        Me.ID_CuentaS.HeaderText = "ID_CuentaS"
        Me.ID_CuentaS.Name = "ID_CuentaS"
        Me.ID_CuentaS.ReadOnly = True
        Me.ID_CuentaS.Visible = False
        '
        'Porcentaje_Valor
        '
        Me.Porcentaje_Valor.DataPropertyName = "Valor_Porcentaje"
        Me.Porcentaje_Valor.HeaderText = "Valor Porcentaje"
        Me.Porcentaje_Valor.Name = "Porcentaje_Valor"
        Me.Porcentaje_Valor.ReadOnly = True
        Me.Porcentaje_Valor.Width = 150
        '
        'Vigencia_desde
        '
        Me.Vigencia_desde.DataPropertyName = "VigenciaDesde"
        Me.Vigencia_desde.HeaderText = "Vigencia Desde"
        Me.Vigencia_desde.Name = "Vigencia_desde"
        Me.Vigencia_desde.ReadOnly = True
        Me.Vigencia_desde.Width = 150
        '
        'Vigencia_hasta
        '
        Me.Vigencia_hasta.DataPropertyName = "VigenciaHasta"
        Me.Vigencia_hasta.HeaderText = "Vigencia Hasta"
        Me.Vigencia_hasta.Name = "Vigencia_hasta"
        Me.Vigencia_hasta.ReadOnly = True
        Me.Vigencia_hasta.Width = 150
        '
        'VigenteS
        '
        Me.VigenteS.DataPropertyName = "Vigente"
        Me.VigenteS.HeaderText = "VigenteS"
        Me.VigenteS.Name = "VigenteS"
        Me.VigenteS.ReadOnly = True
        Me.VigenteS.Visible = False
        '
        'Vigente
        '
        Me.Vigente.DataPropertyName = "VIGENTE_ES"
        Me.Vigente.HeaderText = "Vigente"
        Me.Vigente.Name = "Vigente"
        Me.Vigente.ReadOnly = True
        '
        'CONTEX
        '
        Me.CONTEX.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ACTUALIZAR, Me.ELIMINAR})
        Me.CONTEX.Name = "CONTEX"
        Me.CONTEX.Size = New System.Drawing.Size(128, 48)
        Me.CONTEX.Text = "Opciones:"
        '
        'ACTUALIZAR
        '
        Me.ACTUALIZAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ACTUALIZAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ACTUALIZAR.Name = "ACTUALIZAR"
        Me.ACTUALIZAR.Size = New System.Drawing.Size(127, 22)
        Me.ACTUALIZAR.Text = "Modificar"
        '
        'ELIMINAR
        '
        Me.ELIMINAR.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.ELIMINAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ELIMINAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ELIMINAR.Name = "ELIMINAR"
        Me.ELIMINAR.Size = New System.Drawing.Size(127, 22)
        Me.ELIMINAR.Text = "Eliminar"
        '
        'MENUCONTEXTUAL
        '
        Me.MENUCONTEXTUAL.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MECO_MODIFICAR, Me.MECO_ELIMINAR, Me.MECO_VIGENCIA, Me.MECO_VER})
        Me.MENUCONTEXTUAL.Name = "MENUCONTEXTUAL"
        Me.MENUCONTEXTUAL.Size = New System.Drawing.Size(170, 92)
        '
        'MECO_MODIFICAR
        '
        Me.MECO_MODIFICAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_MODIFICAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_MODIFICAR.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MECO_MODIFICAR.Image = CType(resources.GetObject("MECO_MODIFICAR.Image"), System.Drawing.Image)
        Me.MECO_MODIFICAR.Name = "MECO_MODIFICAR"
        Me.MECO_MODIFICAR.Size = New System.Drawing.Size(169, 22)
        Me.MECO_MODIFICAR.Text = "Modificar"
        '
        'MECO_ELIMINAR
        '
        Me.MECO_ELIMINAR.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Delete_
        Me.MECO_ELIMINAR.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_ELIMINAR.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_ELIMINAR.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MECO_ELIMINAR.Name = "MECO_ELIMINAR"
        Me.MECO_ELIMINAR.Size = New System.Drawing.Size(169, 22)
        Me.MECO_ELIMINAR.Text = "Eliminar"
        '
        'MECO_VIGENCIA
        '
        Me.MECO_VIGENCIA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_VIGENCIA.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_VIGENCIA.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MECO_VIGENCIA.Image = CType(resources.GetObject("MECO_VIGENCIA.Image"), System.Drawing.Image)
        Me.MECO_VIGENCIA.Name = "MECO_VIGENCIA"
        Me.MECO_VIGENCIA.Size = New System.Drawing.Size(169, 22)
        Me.MECO_VIGENCIA.Text = "Agregar Vigencia"
        '
        'MECO_VER
        '
        Me.MECO_VER.BackgroundImage = Global.CapaPreTesoreria.My.Resources.Resources._16__Search_
        Me.MECO_VER.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.MECO_VER.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MECO_VER.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.MECO_VER.Image = CType(resources.GetObject("MECO_VER.Image"), System.Drawing.Image)
        Me.MECO_VER.Name = "MECO_VER"
        Me.MECO_VER.Size = New System.Drawing.Size(169, 22)
        Me.MECO_VER.Text = "Ver"
        '
        'BeLabel1
        '
        Me.BeLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel1.AutoSize = True
        Me.BeLabel1.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel1.ForeColor = System.Drawing.Color.Black
        Me.BeLabel1.Location = New System.Drawing.Point(13, 23)
        Me.BeLabel1.Name = "BeLabel1"
        Me.BeLabel1.Size = New System.Drawing.Size(58, 13)
        Me.BeLabel1.TabIndex = 2
        Me.BeLabel1.Text = "Nombre :"
        '
        'txtBuscar
        '
        Me.txtBuscar.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtBuscar.BackColor = System.Drawing.Color.Ivory
        Me.txtBuscar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBuscar.ForeColor = System.Drawing.Color.Black
        Me.txtBuscar.KeyEnter = True
        Me.txtBuscar.Location = New System.Drawing.Point(80, 23)
        Me.txtBuscar.Name = "txtBuscar"
        Me.txtBuscar.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtBuscar.ShortcutsEnabled = False
        Me.txtBuscar.Size = New System.Drawing.Size(276, 20)
        Me.txtBuscar.TabIndex = 3
        Me.txtBuscar.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'TabPersonalizado1
        '
        Me.TabPersonalizado1.Controls.Add(Me.PAGE_CONSULTA)
        Me.TabPersonalizado1.Controls.Add(Me.PAGE_MANTENIMIENTO)
        Me.TabPersonalizado1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPersonalizado1.Location = New System.Drawing.Point(0, 15)
        Me.TabPersonalizado1.Name = "TabPersonalizado1"
        Me.TabPersonalizado1.SelectedIndex = 0
        Me.TabPersonalizado1.Size = New System.Drawing.Size(721, 450)
        Me.TabPersonalizado1.TabIndex = 2
        '
        'PAGE_CONSULTA
        '
        Me.PAGE_CONSULTA.Controls.Add(Me.GroupBox2)
        Me.PAGE_CONSULTA.Controls.Add(Me.txtBuscar)
        Me.PAGE_CONSULTA.Controls.Add(Me.BeLabel1)
        Me.PAGE_CONSULTA.Location = New System.Drawing.Point(4, 22)
        Me.PAGE_CONSULTA.Name = "PAGE_CONSULTA"
        Me.PAGE_CONSULTA.Padding = New System.Windows.Forms.Padding(3)
        Me.PAGE_CONSULTA.Size = New System.Drawing.Size(713, 424)
        Me.PAGE_CONSULTA.TabIndex = 0
        Me.PAGE_CONSULTA.Text = "Consulta"
        Me.PAGE_CONSULTA.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rgLista)
        Me.GroupBox2.Controls.Add(Me.btnNuevo)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(699, 412)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        '
        'rgLista
        '
        Me.rgLista.ContextMenuStrip = Me.MENUCONTEXTUAL
        Me.rgLista.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rgLista.Location = New System.Drawing.Point(8, 16)
        '
        'rgLista
        '
        Me.rgLista.MasterTemplate.AllowAddNewRow = False
        Me.rgLista.MasterTemplate.AllowDragToGroup = False
        GridViewTextBoxColumn1.FieldName = "ID_Impuesto"
        GridViewTextBoxColumn1.HeaderText = "Nº Impuesto"
        GridViewTextBoxColumn1.Name = "ID_Impuesto"
        GridViewTextBoxColumn1.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn1.Width = 80
        GridViewTextBoxColumn2.FieldName = "Nombre"
        GridViewTextBoxColumn2.HeaderText = "Nombre"
        GridViewTextBoxColumn2.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        GridViewTextBoxColumn2.Name = "Nombre"
        GridViewTextBoxColumn2.Width = 150
        GridViewTextBoxColumn3.FieldName = "Abreviacion"
        GridViewTextBoxColumn3.HeaderText = "Abreviación"
        GridViewTextBoxColumn3.Name = "Abreviacion"
        GridViewTextBoxColumn3.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn3.Width = 80
        GridViewTextBoxColumn4.FieldName = "Valor_Porcentaje"
        GridViewTextBoxColumn4.HeaderText = "Valor Porcentaje"
        GridViewTextBoxColumn4.Name = "Valor_Porcentaje"
        GridViewTextBoxColumn4.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn4.Width = 100
        GridViewTextBoxColumn5.FieldName = "VigenciaDesde"
        GridViewTextBoxColumn5.FormatString = "{0:dd/MM/yyyy}"
        GridViewTextBoxColumn5.HeaderText = "Vigencia Desde"
        GridViewTextBoxColumn5.Name = "VigenciaDesde"
        GridViewTextBoxColumn5.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn5.Width = 100
        GridViewTextBoxColumn6.FieldName = "VigenciaHasta"
        GridViewTextBoxColumn6.FormatString = "{0:dd/MM/yyyy}"
        GridViewTextBoxColumn6.HeaderText = "Vigencia Hasta"
        GridViewTextBoxColumn6.Name = "VigenciaHasta"
        GridViewTextBoxColumn6.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter
        GridViewTextBoxColumn6.Width = 100
        GridViewTextBoxColumn7.FieldName = "ID_Cuenta"
        GridViewTextBoxColumn7.HeaderText = "ID_Cuenta"
        GridViewTextBoxColumn7.IsVisible = False
        GridViewTextBoxColumn7.Name = "ID_Cuenta"
        GridViewTextBoxColumn8.FieldName = "CUENTA"
        GridViewTextBoxColumn8.HeaderText = "Cuenta Contable"
        GridViewTextBoxColumn8.HeaderTextAlignment = System.Drawing.ContentAlignment.MiddleLeft
        GridViewTextBoxColumn8.Name = "CUENTA"
        GridViewTextBoxColumn8.Width = 150
        Me.rgLista.MasterTemplate.Columns.AddRange(New Telerik.WinControls.UI.GridViewDataColumn() {GridViewTextBoxColumn1, GridViewTextBoxColumn2, GridViewTextBoxColumn3, GridViewTextBoxColumn4, GridViewTextBoxColumn5, GridViewTextBoxColumn6, GridViewTextBoxColumn7, GridViewTextBoxColumn8})
        Me.rgLista.Name = "rgLista"
        Me.rgLista.ReadOnly = True
        Me.rgLista.Size = New System.Drawing.Size(685, 335)
        Me.rgLista.TabIndex = 3
        Me.rgLista.Text = "RadGridView1"
        '
        'btnNuevo
        '
        Me.btnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnNuevo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnNuevo.Image = CType(resources.GetObject("btnNuevo.Image"), System.Drawing.Image)
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(579, 372)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(77, 23)
        Me.btnNuevo.TabIndex = 2
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'PAGE_MANTENIMIENTO
        '
        Me.PAGE_MANTENIMIENTO.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.btnCancelar)
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.GRUPO_VIGENTE)
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.btnGuardar)
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.GroupBox3)
        Me.PAGE_MANTENIMIENTO.Controls.Add(Me.GroupBox1)
        Me.PAGE_MANTENIMIENTO.Location = New System.Drawing.Point(4, 22)
        Me.PAGE_MANTENIMIENTO.Name = "PAGE_MANTENIMIENTO"
        Me.PAGE_MANTENIMIENTO.Padding = New System.Windows.Forms.Padding(3)
        Me.PAGE_MANTENIMIENTO.Size = New System.Drawing.Size(713, 424)
        Me.PAGE_MANTENIMIENTO.TabIndex = 1
        Me.PAGE_MANTENIMIENTO.Text = "Mantenimiento"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCancelar.Image = CType(resources.GetObject("btnCancelar.Image"), System.Drawing.Image)
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(599, 165)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(97, 31)
        Me.btnCancelar.TabIndex = 12
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'GRUPO_VIGENTE
        '
        Me.GRUPO_VIGENTE.Controls.Add(Me.fecha_hasta)
        Me.GRUPO_VIGENTE.Controls.Add(Me.fecha_desde)
        Me.GRUPO_VIGENTE.Controls.Add(Me.cboVigente)
        Me.GRUPO_VIGENTE.Controls.Add(Me.cboPorcentaje)
        Me.GRUPO_VIGENTE.Controls.Add(Me.BeLabel10)
        Me.GRUPO_VIGENTE.Controls.Add(Me.BeLabel6)
        Me.GRUPO_VIGENTE.Controls.Add(Me.BeLabel4)
        Me.GRUPO_VIGENTE.Controls.Add(Me.BeLabel9)
        Me.GRUPO_VIGENTE.Controls.Add(Me.txtImpuesto)
        Me.GRUPO_VIGENTE.Controls.Add(Me.BeLabel8)
        Me.GRUPO_VIGENTE.Enabled = False
        Me.GRUPO_VIGENTE.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GRUPO_VIGENTE.Location = New System.Drawing.Point(9, 126)
        Me.GRUPO_VIGENTE.Name = "GRUPO_VIGENTE"
        Me.GRUPO_VIGENTE.Size = New System.Drawing.Size(444, 106)
        Me.GRUPO_VIGENTE.TabIndex = 1
        Me.GRUPO_VIGENTE.TabStop = False
        Me.GRUPO_VIGENTE.Text = "Mantenimiento Vigencia"
        '
        'fecha_hasta
        '
        Me.fecha_hasta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fecha_hasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.fecha_hasta.Location = New System.Drawing.Point(320, 57)
        Me.fecha_hasta.Name = "fecha_hasta"
        Me.fecha_hasta.Size = New System.Drawing.Size(94, 20)
        Me.fecha_hasta.TabIndex = 22
        '
        'fecha_desde
        '
        Me.fecha_desde.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fecha_desde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.fecha_desde.Location = New System.Drawing.Point(320, 19)
        Me.fecha_desde.Name = "fecha_desde"
        Me.fecha_desde.Size = New System.Drawing.Size(94, 20)
        Me.fecha_desde.TabIndex = 21
        '
        'cboVigente
        '
        Me.cboVigente.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboVigente.BackColor = System.Drawing.Color.Ivory
        Me.cboVigente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVigente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboVigente.ForeColor = System.Drawing.Color.Black
        Me.cboVigente.FormattingEnabled = True
        Me.cboVigente.Items.AddRange(New Object() {"SI", "NO"})
        Me.cboVigente.KeyEnter = True
        Me.cboVigente.Location = New System.Drawing.Point(103, 49)
        Me.cboVigente.Name = "cboVigente"
        Me.cboVigente.Size = New System.Drawing.Size(85, 21)
        Me.cboVigente.TabIndex = 20
        '
        'cboPorcentaje
        '
        Me.cboPorcentaje.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboPorcentaje.BackColor = System.Drawing.Color.Ivory
        Me.cboPorcentaje.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPorcentaje.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPorcentaje.ForeColor = System.Drawing.Color.Black
        Me.cboPorcentaje.FormattingEnabled = True
        Me.cboPorcentaje.Items.AddRange(New Object() {"SI", "NO"})
        Me.cboPorcentaje.KeyEnter = True
        Me.cboPorcentaje.Location = New System.Drawing.Point(102, 76)
        Me.cboPorcentaje.Name = "cboPorcentaje"
        Me.cboPorcentaje.Size = New System.Drawing.Size(84, 21)
        Me.cboPorcentaje.TabIndex = 19
        '
        'BeLabel10
        '
        Me.BeLabel10.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel10.AutoSize = True
        Me.BeLabel10.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel10.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.BeLabel10.Location = New System.Drawing.Point(3, 79)
        Me.BeLabel10.Name = "BeLabel10"
        Me.BeLabel10.Size = New System.Drawing.Size(94, 13)
        Me.BeLabel10.TabIndex = 18
        Me.BeLabel10.Text = "Es Porcentaje :"
        '
        'BeLabel6
        '
        Me.BeLabel6.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel6.AutoSize = True
        Me.BeLabel6.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel6.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.BeLabel6.Location = New System.Drawing.Point(236, 21)
        Me.BeLabel6.Name = "BeLabel6"
        Me.BeLabel6.Size = New System.Drawing.Size(77, 13)
        Me.BeLabel6.TabIndex = 5
        Me.BeLabel6.Text = "Vig. Desde :"
        '
        'BeLabel4
        '
        Me.BeLabel4.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel4.AutoSize = True
        Me.BeLabel4.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel4.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.BeLabel4.Location = New System.Drawing.Point(31, 24)
        Me.BeLabel4.Name = "BeLabel4"
        Me.BeLabel4.Size = New System.Drawing.Size(66, 13)
        Me.BeLabel4.TabIndex = 3
        Me.BeLabel4.Text = "Impuesto :"
        '
        'BeLabel9
        '
        Me.BeLabel9.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel9.AutoSize = True
        Me.BeLabel9.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel9.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.BeLabel9.Location = New System.Drawing.Point(39, 52)
        Me.BeLabel9.Name = "BeLabel9"
        Me.BeLabel9.Size = New System.Drawing.Size(58, 13)
        Me.BeLabel9.TabIndex = 16
        Me.BeLabel9.Text = "Vigente :"
        '
        'txtImpuesto
        '
        Me.txtImpuesto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtImpuesto.BackColor = System.Drawing.Color.White
        Me.txtImpuesto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtImpuesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtImpuesto.ForeColor = System.Drawing.Color.Black
        Me.txtImpuesto.KeyEnter = True
        Me.txtImpuesto.Location = New System.Drawing.Point(104, 23)
        Me.txtImpuesto.Name = "txtImpuesto"
        Me.txtImpuesto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtImpuesto.ShortcutsEnabled = False
        Me.txtImpuesto.Size = New System.Drawing.Size(84, 20)
        Me.txtImpuesto.TabIndex = 14
        Me.txtImpuesto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel8
        '
        Me.BeLabel8.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel8.AutoSize = True
        Me.BeLabel8.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel8.ForeColor = System.Drawing.Color.DarkSlateBlue
        Me.BeLabel8.Location = New System.Drawing.Point(236, 57)
        Me.BeLabel8.Name = "BeLabel8"
        Me.BeLabel8.Size = New System.Drawing.Size(74, 13)
        Me.BeLabel8.TabIndex = 15
        Me.BeLabel8.Text = "Vig. Hasta :"
        '
        'btnGuardar
        '
        Me.btnGuardar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnGuardar.Image = CType(resources.GetObject("btnGuardar.Image"), System.Drawing.Image)
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.Location = New System.Drawing.Point(507, 166)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(86, 30)
        Me.btnGuardar.TabIndex = 11
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgImpVigentes)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 238)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(687, 171)
        Me.GroupBox3.TabIndex = 12
        Me.GroupBox3.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.BeLabel7)
        Me.GroupBox1.Controls.Add(Me.txtNumImpuesto)
        Me.GroupBox1.Controls.Add(Me.txtAbreviacion)
        Me.GroupBox1.Controls.Add(Me.BeLabel2)
        Me.GroupBox1.Controls.Add(Me.txtNombre)
        Me.GroupBox1.Controls.Add(Me.BeLabel3)
        Me.GroupBox1.Controls.Add(Me.cboCuenta)
        Me.GroupBox1.Controls.Add(Me.BeLabel5)
        Me.GroupBox1.Controls.Add(Me.lblCuenta)
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(8, 14)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(688, 106)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Mantenimiento Impuesto"
        '
        'BeLabel7
        '
        Me.BeLabel7.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel7.AutoSize = True
        Me.BeLabel7.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel7.Location = New System.Drawing.Point(305, 21)
        Me.BeLabel7.Name = "BeLabel7"
        Me.BeLabel7.Size = New System.Drawing.Size(82, 13)
        Me.BeLabel7.TabIndex = 13
        Me.BeLabel7.Text = "Abreviación :"
        '
        'txtNumImpuesto
        '
        Me.txtNumImpuesto.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNumImpuesto.BackColor = System.Drawing.Color.White
        Me.txtNumImpuesto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumImpuesto.Enabled = False
        Me.txtNumImpuesto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumImpuesto.ForeColor = System.Drawing.Color.Black
        Me.txtNumImpuesto.KeyEnter = True
        Me.txtNumImpuesto.Location = New System.Drawing.Point(100, 18)
        Me.txtNumImpuesto.Name = "txtNumImpuesto"
        Me.txtNumImpuesto.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNumImpuesto.ShortcutsEnabled = False
        Me.txtNumImpuesto.Size = New System.Drawing.Size(113, 20)
        Me.txtNumImpuesto.TabIndex = 8
        Me.txtNumImpuesto.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'txtAbreviacion
        '
        Me.txtAbreviacion.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtAbreviacion.BackColor = System.Drawing.Color.White
        Me.txtAbreviacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAbreviacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAbreviacion.ForeColor = System.Drawing.Color.Black
        Me.txtAbreviacion.KeyEnter = True
        Me.txtAbreviacion.Location = New System.Drawing.Point(390, 18)
        Me.txtAbreviacion.Name = "txtAbreviacion"
        Me.txtAbreviacion.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtAbreviacion.ShortcutsEnabled = False
        Me.txtAbreviacion.Size = New System.Drawing.Size(183, 20)
        Me.txtAbreviacion.TabIndex = 10
        Me.txtAbreviacion.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel2
        '
        Me.BeLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel2.AutoSize = True
        Me.BeLabel2.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel2.Location = New System.Drawing.Point(16, 18)
        Me.BeLabel2.Name = "BeLabel2"
        Me.BeLabel2.Size = New System.Drawing.Size(84, 13)
        Me.BeLabel2.TabIndex = 1
        Me.BeLabel2.Text = "Nº Impuesto :"
        '
        'txtNombre
        '
        Me.txtNombre.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01
        Me.txtNombre.BackColor = System.Drawing.Color.White
        Me.txtNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.ForeColor = System.Drawing.Color.Black
        Me.txtNombre.KeyEnter = True
        Me.txtNombre.Location = New System.Drawing.Point(100, 44)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos
        Me.txtNombre.ShortcutsEnabled = False
        Me.txtNombre.Size = New System.Drawing.Size(473, 20)
        Me.txtNombre.TabIndex = 9
        Me.txtNombre.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno
        '
        'BeLabel3
        '
        Me.BeLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel3.AutoSize = True
        Me.BeLabel3.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel3.Location = New System.Drawing.Point(36, 45)
        Me.BeLabel3.Name = "BeLabel3"
        Me.BeLabel3.Size = New System.Drawing.Size(58, 13)
        Me.BeLabel3.TabIndex = 2
        Me.BeLabel3.Text = "Nombre :"
        '
        'cboCuenta
        '
        Me.cboCuenta.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01
        Me.cboCuenta.BackColor = System.Drawing.Color.Ivory
        Me.cboCuenta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuenta.ForeColor = System.Drawing.Color.Black
        Me.cboCuenta.FormattingEnabled = True
        Me.cboCuenta.KeyEnter = True
        Me.cboCuenta.Location = New System.Drawing.Point(100, 69)
        Me.cboCuenta.Name = "cboCuenta"
        Me.cboCuenta.Size = New System.Drawing.Size(181, 21)
        Me.cboCuenta.TabIndex = 7
        '
        'BeLabel5
        '
        Me.BeLabel5.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.BeLabel5.AutoSize = True
        Me.BeLabel5.BackColor = System.Drawing.Color.Transparent
        Me.BeLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.BeLabel5.Location = New System.Drawing.Point(36, 72)
        Me.BeLabel5.Name = "BeLabel5"
        Me.BeLabel5.Size = New System.Drawing.Size(55, 13)
        Me.BeLabel5.TabIndex = 4
        Me.BeLabel5.Text = "Cuenta :"
        '
        'lblCuenta
        '
        Me.lblCuenta.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.BackColor = System.Drawing.Color.Transparent
        Me.lblCuenta.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lblCuenta.Location = New System.Drawing.Point(305, 77)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(60, 13)
        Me.lblCuenta.TabIndex = 6
        Me.lblCuenta.Text = "lblCuenta"
        '
        'frmImpuestos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.LightSteelBlue
        Me.ClientSize = New System.Drawing.Size(733, 477)
        Me.Controls.Add(Me.TabPersonalizado1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmImpuestos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mantenimiento de Impuestos"
        CType(Me.dgImpVigentes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONTEX.ResumeLayout(False)
        Me.MENUCONTEXTUAL.ResumeLayout(False)
        Me.TabPersonalizado1.ResumeLayout(False)
        Me.PAGE_CONSULTA.ResumeLayout(False)
        Me.PAGE_CONSULTA.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.rgLista.MasterTemplate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.rgLista, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PAGE_MANTENIMIENTO.ResumeLayout(False)
        Me.GRUPO_VIGENTE.ResumeLayout(False)
        Me.GRUPO_VIGENTE.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgImpVigentes As System.Windows.Forms.DataGridView
    Friend WithEvents BeLabel1 As ctrLibreria.Controles.BeLabel
    Friend WithEvents txtBuscar As ctrLibreria.Controles.BeTextBox
    Friend WithEvents TabPersonalizado1 As Controles_DHMont.TabPersonalizado
    Friend WithEvents PAGE_CONSULTA As System.Windows.Forms.TabPage
    Friend WithEvents PAGE_MANTENIMIENTO As System.Windows.Forms.TabPage
    Friend WithEvents lblCuenta As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel6 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel5 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel4 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel3 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel2 As ctrLibreria.Controles.BeLabel
    Friend WithEvents cboCuenta As ctrLibreria.Controles.BeComboBox
    Friend WithEvents txtAbreviacion As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNombre As ctrLibreria.Controles.BeTextBox
    Friend WithEvents txtNumImpuesto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents MENUCONTEXTUAL As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents MECO_MODIFICAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MECO_ELIMINAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents txtImpuesto As ctrLibreria.Controles.BeTextBox
    Friend WithEvents BeLabel7 As ctrLibreria.Controles.BeLabel
    Friend WithEvents MECO_VIGENCIA As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GRUPO_VIGENTE As System.Windows.Forms.GroupBox
    Friend WithEvents fecha_hasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents fecha_desde As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboVigente As ctrLibreria.Controles.BeComboBox
    Friend WithEvents cboPorcentaje As ctrLibreria.Controles.BeComboBox
    Friend WithEvents BeLabel10 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel9 As ctrLibreria.Controles.BeLabel
    Friend WithEvents BeLabel8 As ctrLibreria.Controles.BeLabel
    Friend WithEvents MECO_VER As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EsPorcentajeS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EsPorcentaje As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ID_ImpuestoVigencia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ID_ImpuestoS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AbreviacionS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ID_CuentaS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Porcentaje_Valor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vigencia_desde As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vigencia_hasta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VigenteS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Vigente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CONTEX As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ACTUALIZAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ELIMINAR As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents rgLista As Telerik.WinControls.UI.RadGridView
End Class
