﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Mantenimiento;
using CapaEntidad;


namespace BusinessLogicLayer.Mantenimiento
{
    public class SrvVistoBueno
    {
        DaoVistoBueno VL_DaoVistoBueno = new DaoVistoBueno();

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UsuariosVB(Int32 id, Decimal vl_USUARIO)
        {
            return VL_DaoVistoBueno.Fnc_Listar_UsuariosVB(id, vl_USUARIO);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Consulta_VB(Int32 VL_OPCION,Decimal VL_ID_DOCPENDIENTE, Decimal VL_ID_DOCUMENTO)
        {
            return VL_DaoVistoBueno.Fnc_Listar_Consulta_VB(VL_OPCION, VL_ID_DOCPENDIENTE, VL_ID_DOCUMENTO);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_InsertarVarios_VB(Int32 VL_OPCION, Decimal VL_ID_VistoBueno, Decimal VL_ID_Usuario, String VL_VistoBueno, Int32 VL_Estado)
        {
            return VL_DaoVistoBueno.Fnc_InsertarVarios_VB(VL_OPCION, VL_ID_VistoBueno, VL_ID_Usuario, VL_VistoBueno, VL_Estado);
        }
    }
}
