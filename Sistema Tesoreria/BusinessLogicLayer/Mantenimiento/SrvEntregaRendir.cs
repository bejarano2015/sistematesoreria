﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Mantenimiento;
using CapaEntidad;
using DataAccessLayer.Mantenimiento;


namespace BusinessLogicLayer.Mantenimiento
{
    public class SrvEntregaRendir
    {

        
        DAOEntregaRendir VL_DAOEntregaRendir = new DAOEntregaRendir();

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Obras( )
        {

            return VL_DAOEntregaRendir.Fnc_Listar_Obras();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Empresas()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_Empresas();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Empleados()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_Empleados();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Girado()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_Girado();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_AreaPorEmpresa(BeanEntregaRendir objEntregaRendir)
        {
            return VL_DAOEntregaRendir.Fnc_Listar_AreaPorEmpresa(objEntregaRendir);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Monedas()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_Monedas();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_EntregaRendir()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_Estado_EntregaRendir();
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Entrega_Rendir(BeanEntregaRendir objEntregaRendir)
        {
            return VL_DAOEntregaRendir.Fnc_Registrar_Entrega_Rendir(objEntregaRendir);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Autoriza()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_Autoriza();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_EntregaRendir(DateTime VL_FechaInicio, DateTime VL_FechaFin)
        {
            return VL_DAOEntregaRendir.Fnc_Listar_EntregaRendir(VL_FechaInicio, VL_FechaFin);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_EntregaRendir_1(DateTime VL_FechaInicio, DateTime VL_FechaFin, String VL_Emp)
        {
            return VL_DAOEntregaRendir.Fnc_Listar_EntregaRendir_1(VL_FechaInicio, VL_FechaFin, VL_Emp);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Buscar_Por_Empresa_Solicitante(BeanEntregaRendir objEntregaRendir)
        {
            return VL_DAOEntregaRendir.Fnc_Buscar_Por_Empresa_Solicitante(objEntregaRendir);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Buscar_Por_Fechas(BeanEntregaRendir objEntregaRendir)
        {
            return VL_DAOEntregaRendir.Fnc_Buscar_Por_Fechas(objEntregaRendir);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_BEmpresas()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_BEmpresas();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_BEmpleados()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_BEmpleados();
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Entrega_Rendir(BeanEntregaRendir objEntregaRendir)
        {
            return VL_DAOEntregaRendir.Fnc_Actualizar_Entrega_Rendir(objEntregaRendir);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_ER(BeanEntregaRendir objEntregaRendir)
        {
            return VL_DAOEntregaRendir.Fnc_Actualizar_ER(objEntregaRendir);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_TipoDocumentos()
        {
            return VL_DAOEntregaRendir.Fnc_Listar_TipoDocumentos();
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_NP(String VL_NUM_DOC_IDENTIDAD, String VL_NOMBRES, String VL_APELLIDO_PATERNO, String VL_APELLIDO_MATERNO, Int32 VL_ESTADO)
        {
            return VL_DAOEntregaRendir.Fnc_Insertar_NP(VL_NUM_DOC_IDENTIDAD, VL_NOMBRES, VL_APELLIDO_PATERNO, VL_APELLIDO_MATERNO, VL_ESTADO);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Entrega_Rendir(String ID_EntregaRendir, String DocOrigen)
        {
            return VL_DAOEntregaRendir.Fnc_Eliminar_Entrega_Rendir(ID_EntregaRendir,DocOrigen);
        }
    }
}
