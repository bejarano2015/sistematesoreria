﻿using CapaEntidad.Mantenimiento;
using DataAccessLayer.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Mantenimiento
{
   public class SrvPartida
    {
       DaoPartida VL_DaoPartida = new DaoPartida();

       public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_partida(BeanPartida vl_BeanPartida)
        {

            return VL_DaoPartida.Fnc_listar_partida(vl_BeanPartida);
        }

    }
}
