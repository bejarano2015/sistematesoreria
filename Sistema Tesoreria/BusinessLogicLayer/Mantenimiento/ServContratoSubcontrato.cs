﻿using CapaEntidad.Mantenimiento;
using DataAccessLayer.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Mantenimiento
{
    public class ServContratoSubcontrato
    {
        DAOContratoSubcontrato VL_DAOContratoSubcontrato = new DAOContratoSubcontrato();

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_contrato(string emprcodigo)
        {
            return VL_DAOContratoSubcontrato.Fnc_listar_contrato(emprcodigo);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_ContratoSubcontrato(BeanContratoSubcontratos vl_BeanContratoSubcontratos)
        {
            return VL_DAOContratoSubcontrato.Fnc_Registrar_ContratoSubcontrato(vl_BeanContratoSubcontratos);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_ContratoSubcontrato(BeanContratoSubcontratos vl_BeanContratoSubcontratos)
        {
            return VL_DAOContratoSubcontrato.Fnc_Eliminar_ContratoSubcontrato(vl_BeanContratoSubcontratos);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Modificar_ContratoSubcontrato(BeanContratoSubcontratos vl_BeanContratoSubcontratos)
        {
            return VL_DAOContratoSubcontrato.Fnc_Modificar_ContratoSubcontrato(vl_BeanContratoSubcontratos);
        }
    }
}
