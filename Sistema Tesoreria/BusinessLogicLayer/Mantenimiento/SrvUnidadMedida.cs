﻿using DataAccessLayer.Mantenimiento;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.Mantenimiento
{
    public class SrvUnidadMedida
    {
        DaoUnidadMedida VL_DaoUnidadMedida = new DaoUnidadMedida();

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_UnidadMedida()
        {

            return VL_DaoUnidadMedida.Fnc_listar_UnidadMedida();
        }

    }
    
}
