﻿using CapaEntidad.MigrarData;
using DataAccessLayer.MigrarData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.MigrarData
{
    public class SrvClientes
    {
        public SrvClientes() { }

        DAOClientes VL_DATA = new DAOClientes();
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_TIPO_DOC()
        {
            return VL_DATA.FNC_LISTAR_TIPO_DOC();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CLIENTES()
        {
            return VL_DATA.FNC_LISTAR_CLIENTES();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_EMPRESA()
        {
            return VL_DATA.FNC_LISTAR_EMPRESA();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_OBRA()
        {
            return VL_DATA.FNC_LISTAR_OBRA();
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR(BeanClientes OBJ_CLIENTE)
        {
            return VL_DATA.FNC_AGREGAR(OBJ_CLIENTE);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR(BeanClientes OBJ_CLIENTE)
        {
            return VL_DATA.FNC_ACTUALIZAR(OBJ_CLIENTE);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR(String CODIGO_CLIE, int ID_OBRA)
        {
            return VL_DATA.FNC_ELIMINAR(CODIGO_CLIE, ID_OBRA);
        }


        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DEPA()
        {
            return VL_DATA.FNC_LISTAR_DEPARTAMENTOS();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_PROV(String ID_DEPA)
        {
            return VL_DATA.FNC_LISTAR_PROVINCIA(ID_DEPA);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DISTR(String ID_DIST)
        {
            return VL_DATA.FNC_LISTAR_DISTRITO(ID_DIST);
        }
    }
}
