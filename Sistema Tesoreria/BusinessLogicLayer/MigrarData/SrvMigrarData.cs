﻿using DataAccessLayer.MigrarData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.MigrarData
{
    public class SrvMigrarData
    {
        public SrvMigrarData() { }

        DAOMigrarData VL_MIGRAR_DATA = new DAOMigrarData();
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_EMPRESAS()
        {
            return VL_MIGRAR_DATA.FNC_LISTAR_EMPRESA();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_OBRAS(String EMPRESA)
        {
            return VL_MIGRAR_DATA.FNC_LISTAR_OBRAS(EMPRESA);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DATA(String BASEDATOS, String OBRA_CODIGO, String FINICIAL, String FFIN)
        {
            return VL_MIGRAR_DATA.FNC_LISTAR_DATOS(BASEDATOS,OBRA_CODIGO,FINICIAL,FFIN);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DOCUMENTOS(String ORIGEN)
        {
            return VL_MIGRAR_DATA.FNC_LISTAR_DOCUMENTOS(ORIGEN);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DATA_INTERMEDIA()
        {
            return VL_MIGRAR_DATA.FNC_LISTAR_DATOS_INTERMEDIA();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_HISTORIAL_DATOS()
        {
            return VL_MIGRAR_DATA.FNC_LISTAR_HISTORIAL_DATOS();
        }



        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_DATO(String OBRA_CODIGO, String BASE_DATOS, String FECHA_INICIAL, String FECHA_FIN)
        {
            return VL_MIGRAR_DATA.FNC_AGREGAR_DATOS(OBRA_CODIGO, BASE_DATOS,FECHA_INICIAL,FECHA_FIN);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_MIGRAR_DATOS(int OBRA_CODIGO, String DOCUMENTO, String USUARIO)
        {
            return VL_MIGRAR_DATA.FNC_MIGRAR_DATOS(OBRA_CODIGO,DOCUMENTO,USUARIO);
        }


        //AGREGAR DATOS HISTORIAL
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_DATOS_HISTORIAL(String EMPRE_CODIGO, int OBRA_CODIGO, String DOC_CODIGO, int COD_ULTIMO,  String FECHA_INICIAL, String FECHA_FIN)
        {
            return VL_MIGRAR_DATA.FNC_AGREGAR_DATOS_HISTORIAL(EMPRE_CODIGO, OBRA_CODIGO, DOC_CODIGO, COD_ULTIMO,FECHA_INICIAL,FECHA_FIN);
        }

        //ELIMINAR DATOS HISTORIAL
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_DATOS_HISTORIAL(int HISTORIAL_CODIGO)
        {
            return VL_MIGRAR_DATA.FNC_ELIMINAR_DATOS_HISTORIAL(HISTORIAL_CODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_HISTORIAL()
        {
            return VL_MIGRAR_DATA.FNC_LISTAR_HISTORIAL();
        }


        //TRUNCATE TABLA 
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_TRUNCATE_TABLE()
        {
            return VL_MIGRAR_DATA.FNC_TRUNCATE_TABLE();
        }
    }
}
