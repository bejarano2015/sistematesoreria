﻿using CapaEntidad.Cobranza;
using DataAccessLayer.Cobranza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Cobranza
{
     public class SrvDetalleCobranza
    {
         public SrvDetalleCobranza() { }

         DAODetalleCobranza VL_COBR_DETALLES = new DAODetalleCobranza();
         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_COBRANZA_DETALLE(Int16 ID_COBRANZA, ref Int32 ID_COBRANZA_DETALLE)
         {
             return VL_COBR_DETALLES.FNC_LISTAR_COBRANZA_DETALLE(ID_COBRANZA, ref ID_COBRANZA_DETALLE);
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_COBRANZA_DETALLE(BeanDetalleCobranza OBJ_COBRANZA)
         {
             return VL_COBR_DETALLES.FNC_AGREGAR_COBRANZA_DETALLES(OBJ_COBRANZA);
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_COBRANZA_DETALLE(BeanDetalleCobranza OBJ_COBRANZA)
         {
             return VL_COBR_DETALLES.FNC_ACTUALIZAR_COBRANZA_DETALLES(OBJ_COBRANZA);
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_COBRANZA_DETALLE(BeanDetalleCobranza OBJ_COBRANZA)
         {
             return VL_COBR_DETALLES.FNC_ELIMINAR_COBRANZA_DETALLES(OBJ_COBRANZA);
         }
   
    }
}
