﻿using CapaEntidad.Cobranza;
using DataAccessLayer.Cobranza;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Cobranza
{
     public class SrvCobranza
    {
         public SrvCobranza() { }

         DAOCobranza VL_COBRANZA = new DAOCobranza();

         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_COBRANZAS(String ID_EMPRESA, ref Int32 ID_COBRANZA)
         {
             return VL_COBRANZA.FNC_LISTAR_COBRANZA(ID_EMPRESA, ref ID_COBRANZA);
         }
         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_MONEDAS()
         {
             return VL_COBRANZA.FNC_LISTAR_MONEDAS();
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CLIENTES(string ID_CLIENTE)
         {
             return VL_COBRANZA.FNC_LISTAR_CLIENTES(ID_CLIENTE);
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CLIENTES_DETALLES(string ID_CLIENTE,String ID_OBRA)
         {
             return VL_COBRANZA.FNC_LISTAR_CLIENTES_DETALLE(ID_CLIENTE, ID_OBRA);
         }
         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_BANCOS(String ID_EMPRESA)
         {
             return VL_COBRANZA.FNC_LISTAR_BANCOS(ID_EMPRESA);
         }
         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CUENTAS_BANCOS(String ID_EMPRESA, String ID_BANCO)
         {
             return VL_COBRANZA.FNC_LISTAR_CUENTAS_BANCOS(ID_EMPRESA,ID_BANCO);
         }
         public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_COBRANZA(BeanCobranza OBJ_COBRANZA)
         {
             return VL_COBRANZA.FNC_AGREGAR_COBRANZA(OBJ_COBRANZA);
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_COBRANZA(BeanCobranza OBJ_COBRANZA)
         {
             return VL_COBRANZA.FNC_ACTUALIZAR_COBRANZA(OBJ_COBRANZA);
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_COBRANZA(BeanCobranza OBJ_COBRANZA)
         {
             return VL_COBRANZA.FNC_ELIMINAR_COBRANZA(OBJ_COBRANZA);
         }
   
    }
}
