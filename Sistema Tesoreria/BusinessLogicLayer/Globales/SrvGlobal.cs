﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CapaEntidad;
using DataAccessLayer.Globales;


namespace BusinessLogicLayer.Globales
{
    public class SrvGlobal
    {
        DAOGlobal VL_DAOGlobal = new DAOGlobal();

        //public BusinessEntityLayer.Resultado.BeanResultado.ResultadoSelect Fnc_Logueo(BeanLogin objLogin)
        //{
        //    return VL_DAOGlobal.Bdinicializar(objLogin);
        //}

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Empresas()
        {
            return VL_DAOGlobal.Fnc_Listar_Empresas();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_EmpresasLogin(String VL_USUA_CODIGO, String VL_SIST_CODIGO)
        {
            return VL_DAOGlobal.Fnc_Listar_EmpresasLogin(VL_USUA_CODIGO, VL_SIST_CODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_ObrasEmpresa(string VL_EMPR_CODIGO)
        {
            return VL_DAOGlobal.Fnc_Listar_ObrasEmpresa(VL_EMPR_CODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_ObrasEmpresaLogin(string VL_EMPR_CODIGO, string VL_USUA_CODIGO, string VL_SIST_CODIGO)
        {
            return VL_DAOGlobal.Fnc_Listar_ObrasEmpresaLogin(VL_EMPR_CODIGO,VL_USUA_CODIGO,VL_SIST_CODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoDepartamento()
        {
            return VL_DAOGlobal.Fnc_Listar_UbigeoDepartamento();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoProvincia(string VL_UBDE_CODIGO)
        {
            return VL_DAOGlobal.Fnc_Listar_UbigeoProvincia(VL_UBDE_CODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoDistrito(string VL_UBPR_CODIGO)
        {
            return VL_DAOGlobal.Fnc_Listar_UbigeoDistrito(VL_UBPR_CODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoDistritoCriterio(string VL_UBDE_CODIGO)
        {
            return VL_DAOGlobal.Fnc_Listar_UbigeoDistritoCriterio(VL_UBDE_CODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_TipoDocumento()
        {
            return VL_DAOGlobal.Fnc_Listar_TipoDocumento();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_TipoPersona()
        {
            return VL_DAOGlobal.Fnc_Listar_TipoPersona();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Registro_Economico()
        {
            return VL_DAOGlobal.Fnc_Listar_Registro_Economico();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_Inmueble()
        {
            return VL_DAOGlobal.Fnc_Listar_Estado_Inmueble();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Destino_Pago()
        {
            return VL_DAOGlobal.Fnc_Listar_Destino_Pago();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Tipo_Pago()
        {
            return VL_DAOGlobal.Fnc_Listar_Tipo_Pago();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Forma_Pago()
        {
            return VL_DAOGlobal.Fnc_Listar_Forma_Pago();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Tipo_Liberacion()
        {
            return VL_DAOGlobal.Fnc_Listar_Tipo_Liberacion ();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_Registral()
        {
            return VL_DAOGlobal.Fnc_Listar_Estado_Registral();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Usuarios()
        {
            return VL_DAOGlobal.Fnc_Listar_Usuarios();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Obras(string vl_emrpcodigo)
        {
            return VL_DAOGlobal.Fnc_listar_Obra(vl_emrpcodigo);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_ContratoxObra(string vl_emrpcodigo, string vl_obraCodigo, string vl_co_proveedor)
        {
            return VL_DAOGlobal.Fnc_listar_ContratoxObra(vl_emrpcodigo, vl_obraCodigo, vl_co_proveedor);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_DatosContrato(string vl_co_contrato)
        {
            return VL_DAOGlobal.Fnc_listar_DatosContrato(vl_co_contrato);
        }

    }
}
