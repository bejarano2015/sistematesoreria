﻿using CapaEntidad.Impuestos;
using DataAccessLayer.Impuestos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Impuestos
{
   public class SrvImpuesto
    {
       DAOImpuesto VL_IMPUESTO = new DAOImpuesto();
       public SrvImpuesto() { }

       public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CUENTAS()
       {
           return VL_IMPUESTO.FNC_LISTAR_CUENTAS();
       }

       public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_IMPUESTOS() 
       {
           return VL_IMPUESTO.FNC_LISTAR_IMPUESTOS();
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_IMPUESTO(BeanImpuestos OBJ_IMPUESTO)
       {
           return VL_IMPUESTO.FNC_AGREGAR_IMPUESTOS(OBJ_IMPUESTO);
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_IMPUESTO(BeanImpuestos OBJ_IMPUESTO)
       {
           return VL_IMPUESTO.FNC_ACTUALIZAR_IMPUESTOS(OBJ_IMPUESTO);
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_IMPUESTO(BeanImpuestos OBJ_IMPUESTO)
       {
           return VL_IMPUESTO.FNC_ELIMINAR_IMPUESTO(OBJ_IMPUESTO);
       }
    }
}
