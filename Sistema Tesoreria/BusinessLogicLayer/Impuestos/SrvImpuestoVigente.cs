﻿using CapaEntidad.Impuestos;
using DataAccessLayer.Impuestos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Impuestos
{
  public  class SrvImpuestoVigente
    {
      public  SrvImpuestoVigente(){ }

        DAOImpuestoVigentes VL_IMPUESTO = new DAOImpuestoVigentes();
      
       public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_IMPUESTOS_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE) 
       {
           return VL_IMPUESTO.FNC_LISTAR_IMPUESTOS_VIG(OBJ_IMPUESTO_VIGENTE);
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_IMPUESTO_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE)
       {
           return VL_IMPUESTO.FNC_AGREGAR_IMPUESTOS_VIG(OBJ_IMPUESTO_VIGENTE);
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_IMPUESTO_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE)
       {
           return VL_IMPUESTO.FNC_ACTUALIZAR_IMPUESTOS_VIG(OBJ_IMPUESTO_VIGENTE);
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_IMPUESTO_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE)
       {
           return VL_IMPUESTO.FNC_ELIMINAR_IMPUESTO_VIG(OBJ_IMPUESTO_VIGENTE);
       }

    }
}
