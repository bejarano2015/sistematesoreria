﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Mantenimiento;
using CapaEntidad;
using DataAccessLayer.Procesos;
using CapaEntidad.Proceso;

namespace BusinessLogicLayer.Procesos
{
    public class SrvOrdenesCompra
    {
        DAOOrdenesCompra VL_DAOOrdenesCompra = new DAOOrdenesCompra();

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_OC(BeanOrdenesCompra objOrdenesCompra)
        {
            return VL_DAOOrdenesCompra.Fnc_Actualizar_OC(objOrdenesCompra);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_OC(BeanOrdenesCompra objOrdenesCompra)
        {
            return VL_DAOOrdenesCompra.Fnc_Actualizar_Estado_OC(objOrdenesCompra);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Doc_Pendientes(Decimal VL_ID_DocPendiente, Decimal VL_EnProceso, Decimal VL_Saldo, Decimal VL_Amortizado)
        {
            return VL_DAOOrdenesCompra.Fnc_Actualizar_Doc_Pendientes(VL_ID_DocPendiente, VL_EnProceso, VL_Saldo, VL_Amortizado);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Caja_Chicas(String VL_Fecha_Inicio, String VL_Fecha_Fin, String VL_Emp)
        {
            return VL_DAOOrdenesCompra.Fnc_Listar_Caja_Chicas(VL_Fecha_Inicio, VL_Fecha_Fin, VL_Emp);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Valorizacion(String VL_Fecha_Inicio, String VL_Fecha_Fin, String VL_Emp)
         {
             return VL_DAOOrdenesCompra.Fnc_Listar_Valorizacion(VL_Fecha_Inicio, VL_Fecha_Fin, VL_Emp);
         }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_Doc_Pendientes(String VL_ID_DocPendiente, String VL_Estado)
         {
             return VL_DAOOrdenesCompra.Fnc_Actualizar_Estado_Doc_Pendientes(VL_ID_DocPendiente, VL_Estado);
         }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Revisado_Doc_Pendientes(String VL_ID_DocPendiente, String VL_RevisadoDetraccion, String VL_RevisadoRetencion)
        {
            return VL_DAOOrdenesCompra.Fnc_Actualizar_Revisado_Doc_Pendientes(VL_ID_DocPendiente, VL_RevisadoDetraccion, VL_RevisadoRetencion);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Planillas(String VL_Fecha_Inicio, String VL_Fecha_Fin, String VL_Emp)
        {
            return VL_DAOOrdenesCompra.Fnc_Listar_Planillas(VL_Fecha_Inicio, VL_Fecha_Fin, VL_Emp);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_detalle_Factura(Decimal VL_ID_PD, Decimal VL_ID_dc, Decimal VL_Importe)
        {
            return VL_DAOOrdenesCompra.Fnc_Insertar_detalle_Factura(VL_ID_PD, VL_ID_dc, VL_Importe);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_Visto_Bueno(Decimal VL_usuario, Decimal VL_Tip_Doc, Decimal VL_Id_docpendiente)
        {
            return VL_DAOOrdenesCompra.Fnc_Insertar_Visto_Bueno(VL_usuario, VL_Tip_Doc, VL_Id_docpendiente);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_ID_USUARIO(String VL_Emp, String VL_SisCodigo, String VL_UsuCodigo)
        {
            return VL_DAOOrdenesCompra.Fnc_Listar_ID_USUARIO(VL_Emp, VL_SisCodigo, VL_UsuCodigo);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Validacion(String ID_DOCUMENTO, String ID_DOCPENDIENTE)
        {
            return VL_DAOOrdenesCompra.Fnc_Listar_Validacion(ID_DOCUMENTO, ID_DOCPENDIENTE);

        }

    }
}
