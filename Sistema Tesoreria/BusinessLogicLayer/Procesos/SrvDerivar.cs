﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using DataAccessLayer.Procesos;

namespace BusinessLogicLayer.Procesos
{
    public class SrvDerivar
    {

        DAODerivar VL_DAODerivar = new DataAccessLayer.Procesos.DAODerivar();

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Ordenes_Derivadas(String VL_FECHAINICIO, String VL_FECHAFIN, String VL_EmprCodigo)
        {

            return VL_DAODerivar.Fnc_Listar_Ordenes_Derivadas(VL_FECHAINICIO, VL_FECHAFIN, VL_EmprCodigo);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Areas()
        {
            return VL_DAODerivar.Fnc_Listar_Areas();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Facturas_derivadas(String VL_OCOCODIGO, String VL_PDOCODIGO)
        {
            return VL_DAODerivar.Fnc_Listar_Facturas_derivadas(VL_OCOCODIGO, VL_PDOCODIGO);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Derivar_Factura(String VL_IDREGISTRO, String VL_EMPRCODIGO, String VL_OCOCODIGO, String VL_PDOCODIGO)
        {
            return VL_DAODerivar.Fnc_Derivar_Factura(VL_IDREGISTRO, VL_EMPRCODIGO, VL_OCOCODIGO, VL_PDOCODIGO);
        }

      

    }
}
