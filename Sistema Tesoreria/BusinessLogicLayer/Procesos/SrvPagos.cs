﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Mantenimiento;
using CapaEntidad;
using DataAccessLayer.Procesos;
using CapaEntidad.Proceso;

namespace BusinessLogicLayer.Procesos
{
    public class SrvPagos
    {
        DAOPagos VL_DAOPagos = new DAOPagos();
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Pagos(String VL_IdCorrelativoCajaBanco, String VL_ID_DocPendiente, String VL_Empresa)
        {
            return VL_DAOPagos.Fnc_Registrar_Pagos(VL_IdCorrelativoCajaBanco, VL_ID_DocPendiente, VL_Empresa);
        }
    }
}
