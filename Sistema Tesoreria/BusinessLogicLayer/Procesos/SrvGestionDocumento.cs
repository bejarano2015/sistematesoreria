﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidad;
using DataAccessLayer.Procesos;

namespace BusinessLogicLayer.Procesos
{
    public class SrvGestionDocumento
    {
        DAOGestionDocumento VL_DAOGestionDocumento = new DAOGestionDocumento();

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_TipoDocumento()
        {
            return VL_DAOGestionDocumento.Fnc_Listar_TipoDocumento();
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Empresa()
        {
            return VL_DAOGestionDocumento.Fnc_Listar_Empresa();
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_BancoEmpresa(String VL_BANCO_EMPRESA)
        {
            return VL_DAOGestionDocumento.Fnc_Listar_BancoEmpresa(VL_BANCO_EMPRESA);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_GestionDocumentario_Grabar(CapaEntidad.Proceso.BeanGestionDocumentaria objGestionDocumentaria)
        {
            return VL_DAOGestionDocumento.Fnc_Registrar_GestionDocumentario_Grabar(objGestionDocumentaria);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_GestionDocumentario_Listar()
        {
            return VL_DAOGestionDocumento.Fnc_Listar_GestionDocumentario_Listar();
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_GestionDocumentario_Detalle_Grabar(CapaEntidad.Proceso.BeanGestionDocumentariaDetalle objGestionDocumentariaDetalle)
        {
            return VL_DAOGestionDocumento.Fnc_Registrar_GestionDocumentario_Detalle_Grabar(objGestionDocumentariaDetalle);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_GestionDocumentario_Detalle_Grabar(CapaEntidad.Proceso.BeanGestionDocumentariaDetalle objGestionDocumentariaDetalle)
        {
            return VL_DAOGestionDocumento.Fnc_Actualizar_GestionDocumentario_Detalle_Grabar(objGestionDocumentariaDetalle);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_GestionDocumentarioDetalle_Listar(int VL_IDCABECERA)
        {
            return VL_DAOGestionDocumento.Fnc_Listar_GestionDocumentarioDetalle_Listar(VL_IDCABECERA);
        }


    }
}
