﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Mantenimiento;
using CapaEntidad;
using DataAccessLayer.Procesos;
using CapaEntidad.Proceso;

namespace BusinessLogicLayer.Procesos
{
    public class SrvProgramacionPagosDetalle
    {
         DAOProgramacionPagosDetalle VL_DAOProgramacionPagosDetalle = new DAOProgramacionPagosDetalle();

         public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Programacion_Pagos_Detalle(BeanProgramacionPagosDetalle objProgramacionPagosDetalle)
         {
             return VL_DAOProgramacionPagosDetalle.Fnc_Registrar_Programacion_Pagos_Detalle(objProgramacionPagosDetalle);
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_ProgramacionPagosDetalle(String VL_ID_PROGPAGO)
         {
             return VL_DAOProgramacionPagosDetalle.Fnc_Listar_ProgramacionPagosDetalle(VL_ID_PROGPAGO);
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Estado_Programacion_Pagos_Detalle(String VL_ID_PROPAGO_DET)
         {
             return VL_DAOProgramacionPagosDetalle.Fnc_Eliminar_Estado_Programacion_Pagos_Detalle(VL_ID_PROPAGO_DET);
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_PPD(String id_empresa)
         {
             return VL_DAOProgramacionPagosDetalle.Fnc_Listar_PPD(id_empresa);
         }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_FormaPago( )
         {
             return VL_DAOProgramacionPagosDetalle.Fnc_Listar_FormaPago();
         }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_ActualizarFormaPago_PPD(String VL_ID_PROGPAGO, String VL_ID_FORMA_PAGO)
        {
            return VL_DAOProgramacionPagosDetalle.Fnc_ActualizarFormaPago_PPD(VL_ID_PROGPAGO, VL_ID_FORMA_PAGO);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_PPD(String VL_ID_PROGPAGO, String VL_ESTADO)
        {
            return VL_DAOProgramacionPagosDetalle.Fnc_Actualizar_Estado_PPD(VL_ID_PROGPAGO, VL_ESTADO);
        }

    }
}
