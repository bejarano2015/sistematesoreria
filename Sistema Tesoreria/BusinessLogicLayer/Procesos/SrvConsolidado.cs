﻿using CapaEntidad.Proceso;
using DataAccessLayer.Procesos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Procesos
{
    public class SrvConsolidado 
    {

        DAOConsolidadoPartida vl_DAOConsolidadoPartida = new DAOConsolidadoPartida();
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Consolidado(BeanConsolidado beanConsolidado)
        {
            return vl_DAOConsolidadoPartida.Fnc_Registrar_Consolidado(beanConsolidado);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_PartidoContrato(BeanConsolidado beanConsolidado)
        {
            return vl_DAOConsolidadoPartida.Fnc_Registrar_PartidaContrato(beanConsolidado);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Limpiar_Consolidado(BeanConsolidado beanConsolidado)
        {
            return vl_DAOConsolidadoPartida.Fnc_Limpiar_Consolidado(beanConsolidado);
        }
    }
}
