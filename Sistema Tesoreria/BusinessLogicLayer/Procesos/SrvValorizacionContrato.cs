﻿using CapaEntidad.Proceso;
using DataAccessLayer.Procesos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Procesos
{
    public class SrvValorizacionContrato
    {
        DAOValorizacionesContrato VL_DAOValorizacionesContrato = new DAOValorizacionesContrato();

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Valorizacioncontrato(BeanValorizacionContrato vl_BeanValorizacionContrato)
        {
            return VL_DAOValorizacionesContrato.Fnc_Registrar_Valorizacioncontrato(vl_BeanValorizacionContrato);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_Valorizacionescontrato(int  co_Contrato)
        {
            return VL_DAOValorizacionesContrato.Fnc_listar_Valorizacionescontrato(co_Contrato);
        }
    }
}
