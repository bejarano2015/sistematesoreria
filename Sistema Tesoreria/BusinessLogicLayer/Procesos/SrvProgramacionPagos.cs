﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Mantenimiento;
using CapaEntidad;
using DataAccessLayer.Procesos;
using CapaEntidad.Proceso;

namespace BusinessLogicLayer.Procesos
{
    public class SrvProgramacionPagos
    {
        
        DAOProgramacionPagos VL_DAOProgramacionPagos = new DAOProgramacionPagos();

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Programacion_Pagos(BeanProgramacionPagos objProgramacionPagos)
        {
            return VL_DAOProgramacionPagos.Fnc_Registrar_Programacion_Pagos(objProgramacionPagos);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Programacion_Pagos(String VL_id_estado, DateTime VL_FECHA_INICIO, DateTime VL_FECHA_FIN)
        {
            return VL_DAOProgramacionPagos.Fnc_Listar_Programacion_Pagos(VL_id_estado, VL_FECHA_INICIO, VL_FECHA_FIN);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_Programacion_Pagos(BeanProgramacionPagos objProgramacionPagos)
        {
            return VL_DAOProgramacionPagos.Fnc_Actualizar_Estado_Programacion_Pagos(objProgramacionPagos);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Estado_Programacion_Pagos(BeanProgramacionPagos objProgramacionPagos)
        {
            return VL_DAOProgramacionPagos.Fnc_Eliminar_Estado_Programacion_Pagos(objProgramacionPagos);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Total_Programacion_Pagos(String VL_ID_PADRE, String VL_TOTAL)
        {
            return VL_DAOProgramacionPagos.Fnc_Actualizar_Total_Programacion_Pagos(VL_ID_PADRE, VL_TOTAL);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Programacion_Pagos(String VL_ID_PROGPAGO)
        {
            return VL_DAOProgramacionPagos.Fnc_Eliminar_Programacion_Pagos(VL_ID_PROGPAGO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_PP()
        {
            return VL_DAOProgramacionPagos.Fnc_Listar_Estado_PP();
        }

         public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Listar_Combos_Varios_Reportes(String id)
        {
            return VL_DAOProgramacionPagos.Fnc_Listar_Combos_Varios_Reportes(id);
        }

         public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Generar_Reportes(DateTime VL_FechaInicio, DateTime VL_FechaFin, String VL_Estado, String VL_Moneda, String VL_Empresa)
         {
             return VL_DAOProgramacionPagos.Fnc_Generar_Reportes(VL_FechaInicio, VL_FechaFin, VL_Estado, VL_Moneda, VL_Empresa);
         }
    }
}
