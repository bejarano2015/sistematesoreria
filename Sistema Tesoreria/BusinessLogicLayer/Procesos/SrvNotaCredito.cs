﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Mantenimiento;
using CapaEntidad;
using DataAccessLayer.Procesos;
using CapaEntidad.Proceso;

namespace BusinessLogicLayer.Procesos
{
    public class SrvNotaCredito
    {
        DAONotaCredito VL_DAONotaCredito = new DAONotaCredito();

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Montos_Por_Proveedor(String VL_Emp, String VL_Ruc)
        {
            return VL_DAONotaCredito.Fnc_Listar_Montos_Por_Proveedor(VL_Emp, VL_Ruc);
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_NotaCredito(String VL_Emp,String VL_Ruc, String VL_Moneda)
        {
            return VL_DAONotaCredito.Fnc_Listar_NotaCredito(VL_Emp, VL_Ruc, VL_Moneda);
        }

         public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_NotaCredito(Decimal VL_IdCorrelativo, Decimal Id_DocPendiente)
        {
            return VL_DAONotaCredito.Fnc_Insertar_NotaCredito(VL_IdCorrelativo, Id_DocPendiente);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_NotaCredito(Decimal Id_DocPendiente)
         {
             return VL_DAONotaCredito.Fnc_Eliminar_NotaCredito(Id_DocPendiente);
         }
    }
}
