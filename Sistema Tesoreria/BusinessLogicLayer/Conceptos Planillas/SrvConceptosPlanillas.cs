﻿using CapaEntidad.Conceptos_Planillas;
using DataAccessLayer.Conceptos_Planilla;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Conceptos_Planillas
{
    public class SrvConceptosPlanillas
    {
        DaoConceptosPlanillas VL_CONCEPTOS = new DaoConceptosPlanillas();
        public SrvConceptosPlanillas() { }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_EMPRESA()
        {
            return VL_CONCEPTOS.FNC_LISTAR_EMPRESA();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_OBRAS(String ID_EMPRESA)
        {
            return VL_CONCEPTOS.FNC_LISTAR_OBRAS(ID_EMPRESA);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_PERIODOS(int ID_PERIODO)
        {
            return VL_CONCEPTOS.FNC_LISTAR_PERIODOS(ID_PERIODO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CONCEPTOS_PLANILLAS()
        {
            return VL_CONCEPTOS.FNC_LISTAR_CONCEPTOS_PLANILLA();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_MONEDAS()
        {
            return VL_CONCEPTOS.FNC_LISTAR_MONEDAS();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_PAGO_PLANILLAS()
        {
            return VL_CONCEPTOS.FNC_LISTAR_PAGOS_PLANILLA();
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR(BeanConceptoPlanillas OBJ)
        {
            return VL_CONCEPTOS.FNC_AGREGAR(OBJ);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR(BeanConceptoPlanillas OBJ)
        {
            return VL_CONCEPTOS.FNC_ACTUALIZAR(OBJ);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR(int ID_DOC)
        {
            return VL_CONCEPTOS.FNC_ELIMINAR(ID_DOC);
        }
    }
}
