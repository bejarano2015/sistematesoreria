﻿using CapaEntidad.Item;
using DataAccessLayer.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Item
{
   public class SrvItem
    {
       public  SrvItem(){}

       DAOItem VL_ITEM = new DAOItem();

       public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CUENTAS()
       {
           return VL_ITEM.FNC_LISTAR_CUENTAS();
       }

       public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_ITEM()
       {
           return VL_ITEM.FNC_LISTAR_ITEM();
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_ITEM(BeanItem OBJ_ITEM)
       {
           return VL_ITEM.FNC_AGREGAR_ITEM(OBJ_ITEM);
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_ITEM(BeanItem OBJ_ITEM)
       {
           return VL_ITEM.FNC_ACTUALIZAR_ITEM(OBJ_ITEM);
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_ITEM(BeanItem OBJ_ITEM)
       {
           return VL_ITEM.FNC_ELIMINAR_ITEM(OBJ_ITEM);
       }
    }
}
