﻿using CapaEntidad.Documentos_Sunat;
using DataAccessLayer.Documentos_Sunat;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Documentos_Sunat
{
  public  class SrvDocumentosSunat
    {
      DAODocumentosSunat VL_DOC_SUNAT = new DAODocumentosSunat();
      public SrvDocumentosSunat(){}

      public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CUENTAS()
      {
          return VL_DOC_SUNAT.FNC_LISTAR_CUENTAS();
      }
      public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DOCUMENTOS_SUNAT()
      {
          return VL_DOC_SUNAT.FNC_LISTAR_DOCUMENTOS_SUNAT();
      }
      public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DOC_SUNAT()
      {
          return VL_DOC_SUNAT.FNC_LISTAR_DOC_SUNAT();
      }

      public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_DOC_SUNAT(BeanDocumentosSunat OBJ_DOC_SUNAT)
      {
          return VL_DOC_SUNAT.FNC_AGREGAR_DOC_SUNAT(OBJ_DOC_SUNAT);
      }

      public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_DOC_SUNAT(BeanDocumentosSunat OBJ_DOC_SUNAT)
      {
          return VL_DOC_SUNAT.FNC_ACTUALIZAR_DOC_SUNAT(OBJ_DOC_SUNAT);
      }

      public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_DOC_SUNAT(BeanDocumentosSunat OBJ_DOC_SUNAT)
      {
          return VL_DOC_SUNAT.FNC_ELIMINAR_DOC_SUNAT(OBJ_DOC_SUNAT);
      }
    }
}
