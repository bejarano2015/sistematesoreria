﻿using CapaEntidad.Ventas;
using DataAccessLayer.Ventas;

namespace BusinessLogicLayer.Ventas
{
     public class SrvDocVentasImpuesto
    {
         public SrvDocVentasImpuesto(){}

         DAODocVentaImpuesto VL_VENTAS = new DAODocVentaImpuesto();

         public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DOC_VENTAS_IMPUESTOS(int ID_DOC_VENTA)
         {
             return VL_VENTAS.FNC_LISTAR_DOC_VENTA(ID_DOC_VENTA);
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_DOC_VENTAS(BeanDocVentasImpuesto OBJ_VENTAS)
         {
             return VL_VENTAS.FNC_AGREGAR_DOC_VENTAS(OBJ_VENTAS);
         }

         //public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_DOC_VENTAS(BeanDocVentasImpuesto OBJ_VENTAS)
         //{
         //    return VL_VENTAS.FNC_ACTUALIZAR_DOC_VENTAS(OBJ_VENTAS);
         //}

         //public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_DOC_VENTAS(int ID_DOC, int TIPO)
         //{
         //    return VL_VENTAS.FNC_ELIMINAR_DOC_VENTAS(ID_DOC, TIPO);
         //}
    }
}
