﻿using DataAccessLayer.Ventas.Reportes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Ventas.Reportes
{
    public class SrvNotaCredito
    {
        DAONotaCredito VL_NOTA_CREDITO = new DAONotaCredito();
        public SrvNotaCredito() { }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Gargar_boltaxmes_expediente(int ID_DOC_VENTA)
        {
            return VL_NOTA_CREDITO.Fnc_Listar_NotaCredito(ID_DOC_VENTA);
        }
    }
}
