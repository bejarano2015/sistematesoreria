﻿using CapaEntidad.Ventas.Reportes;
using DataAccessLayer.Ventas.Reportes;
using System;

namespace BusinessLogicLayer.Ventas.Reportes
{
   public class SrvBoleta
    {
        DAOBoleta VL_DAOBoleta = new DAOBoleta();
        //public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Cargarpagos_x_mes(string vl_año, string vl_periodo, string vl_fecinicio, string vl_fecfin, string vl_filtro)
        //{
        //    return VL_DAOBoleta.Fnc_Listar_boltaxmes(vl_año, vl_periodo, vl_fecinicio, vl_fecfin, vl_filtro);
        //}
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Gargar_boltaxmes_expediente(int ID_DOC_VENTA)
        {
            return VL_DAOBoleta.Fnc_Listar_boltaxmes_expediente(ID_DOC_VENTA);
        }
        //public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Gargar_boltaxmes_expediente_Imprimir(string vl_año, string vl_periodo, string vl_expe_codigo, string vl_fecinicio, string vl_fecfin, string vl_filtro)
        //{
        //    return VL_DAOBoleta.Fnc_Listar_boltaxmes_expediente_Imprimir(vl_año, vl_periodo, vl_expe_codigo, vl_fecinicio, vl_fecfin, vl_filtro);
        //}

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_Boleta(BeanBoleta objBoleta)
        {
            return VL_DAOBoleta.Fnc_Insertar_Boleta(objBoleta);
        }
    }
}
