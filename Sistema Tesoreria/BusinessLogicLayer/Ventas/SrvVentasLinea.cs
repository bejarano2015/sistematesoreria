﻿using CapaEntidad.Ventas;
using DataAccessLayer.Ventas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Ventas
{
    public class SrvVentasLinea
    {
        DAOVentaLinea VL_VENTAS = new DAOVentaLinea();
        public SrvVentasLinea() { }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_ITEM_VENTAS()
        {
            return VL_VENTAS.FNC_LISTAR_ITEM_VENTAS();
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_VENTAS_LINEA(int ID_VENTA)
        {
            return VL_VENTAS.FNC_LISTAR_VENTAS_LINEAS(ID_VENTA);
        }


        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_VENTAS_LINEA(BeanVentaLinea OBJ_VENTAS)
        {
            return VL_VENTAS.FNC_AGREGAR_VENTAS_LINEA(OBJ_VENTAS);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_VENTAS_LINEA(BeanVentaLinea OBJ_VENTAS)
        {
            return VL_VENTAS.FNC_ACTUALIZAR_VENTAS_LINEA(OBJ_VENTAS);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_VENTAS_LINEA(BeanVentaLinea OBJ_VENTAS)
        {
            return VL_VENTAS.FNC_ELIMINAR_VENTAS_LINEA(OBJ_VENTAS);
        }
    }
}
