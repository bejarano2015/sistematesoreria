﻿using CapaEntidad.Ventas;
using DataAccessLayer.Ventas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogicLayer.Ventas
{
    public class SrvVentas
    {
        public SrvVentas() { }

        DAOVentas VL_VENTAS = new DAOVentas();

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CLIENTES_DETALLES(string ID_CLIENTE, string ID_OBRA)
        {
            return VL_VENTAS.FNC_LISTAR_CLIENTES_DETALLE(ID_CLIENTE, ID_OBRA);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_MONEDAS()
        {
            return VL_VENTAS.FNC_LISTAR_MONEDAS();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_CLIENTES(string id_obra)
        {
            return VL_VENTAS.FNC_LISTAR_CLIENTES(id_obra);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_FORMA_PAGO()
        {
            return VL_VENTAS.FNC_LISTAR_FORMA_PAGO();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_OBRA()
        {
            return VL_VENTAS.FNC_LISTAR_OBRA();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_VENDEDOR()
        {
            return VL_VENTAS.FNC_LISTAR_VENDEDOR();
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DOCUMENTOS(String DOCUMENTO)
        {
            return VL_VENTAS.FNC_LISTAR_DOCUMENTOS(DOCUMENTO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_COSTOS(String EMPRESA_CODIDO)
        {
            return VL_VENTAS.FNC_LISTAR_COSTOS(EMPRESA_CODIDO);
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_VENTAS()
        {
            return VL_VENTAS.FNC_LISTAR_VENTAS();
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_VENTAS(BeanVentas OBJ_VENTAS)
        {
            return VL_VENTAS.FNC_AGREGAR_VENTAS(OBJ_VENTAS);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_VENTAS(BeanVentas OBJ_VENTAS)
        {
            return VL_VENTAS.FNC_ACTUALIZAR_VENTAS(OBJ_VENTAS);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_VENTAS(int ID_DOC,int TIPO )
        {
            return VL_VENTAS.FNC_ELIMINAR_VENTAS(ID_DOC,TIPO);
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_SERIE(int ID_DOCUMENTO)
        {
            return VL_VENTAS.FNC_LISTAR_SERIE(ID_DOCUMENTO);
        }


        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_VENTAS_NUMERADOR(int ID_DOC_VENTA, String SERIE, int ID_DOCUMENTO)
        {
            return VL_VENTAS.FNC_ACTUALIZAR_VENTAS_NUMERADOR(ID_DOC_VENTA, SERIE, ID_DOCUMENTO);
        }

        // public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_NUMERADOR(int ID_DOCUMENTO, String SERIE, ref String NUMERO)
        //{
        //    return VL_VENTAS.FNC_ACTUALIZAR_NUMERADOR(ID_DOCUMENTO, SERIE,ref NUMERO);
        //}

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ANULAR_DOC_VENTA(int ID_DOC_VENTA)
        {
            return VL_VENTAS.FNC_ANULAR_DOC_VENTA(ID_DOC_VENTA);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ANULAR_REIMPRIMIR(int ID_DOC_VENTA, ref int ID_NUEVO_DOC)
        {
            return VL_VENTAS.FNC_ANULAR_REIMPRIMIR(ID_DOC_VENTA,ref ID_NUEVO_DOC);
        }

      //SERIE Y NUMERACION

        public CapaEntidad.BeanResultado.ResultadoSeleccion SRV_LISTAR_DOCU_NUMERADOR(string ID_DOCU)
        {
            return VL_VENTAS.FNC_LISTAR_DOCU_NUMERADOR(ID_DOCU);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_AGREGAR_NUMERADOR_CONT(String ID_DOC, String SERIE, string NUMERO,String USUARIO, int TIPO)
        {
            return VL_VENTAS.FNC_AGREGAR_NUMERADOR_CONT(ID_DOC, SERIE, NUMERO,USUARIO, TIPO);
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ACTUALIZAR_NUMERADOR_CONT(int ID_DOC_NUM, String ID_DOC, String SERIE, string NUMERO, String USUARIO, int TIPO)
        {
            return VL_VENTAS.FNC_ACTUALIZAR_NUMERADOR_CONT(ID_DOC_NUM, ID_DOC, SERIE, NUMERO,USUARIO, TIPO);
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion SRV_ELIMINAR_NUMERADOR_CONT(int ID_DOC_NUM, String ID_DOC, String SERIE, string NUMERO, String USUARIO, int TIPO)
        {
            return VL_VENTAS.FNC_ELIMINAR_NUMERADOR_CONT(ID_DOC_NUM, ID_DOC, SERIE, NUMERO,USUARIO, TIPO);
        }
    }
}
