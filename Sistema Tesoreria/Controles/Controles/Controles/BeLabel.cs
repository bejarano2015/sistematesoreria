using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeLabel : System.Windows.Forms.Label 
    {
        #region Variables

        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02, Skin03 }

        #endregion

        #region Metodos

        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {
                        case "Skin01":
                            this.BackColor = System.Drawing.Color.Transparent;
                            //this.ForeColor = System.Drawing.SystemColors.ControlText;
                            this.ForeColor = System.Drawing.Color.White; 
                            this.TextAlign = System.Drawing.ContentAlignment.TopLeft;
                            this.AutoSize = true;
                            break;
                        case "Skin02":
                            this.BackColor = System.Drawing.Color.DimGray;
                            this.ForeColor = System.Drawing.Color.Black;
                            this.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
                            this.AutoSize = false;
                            break;
                        case "Skin03":
                            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
                            this.ForeColor = System.Drawing.Color.White;
                            this.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
                            this.AutoSize = false;
                            break;
                    }
                }
                vForma = value;
            }
        }
        public BeLabel()
        {
            //this.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Ninguno;//
            this.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01; 
            InitializeComponent();
        }

        #endregion
    }
}
