using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeDataGrid : System.Windows.Forms.DataGrid 
    {
        #region Variables

        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos

        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":
                            this.FlatMode = false;
                            this.AlternatingBackColor = System.Drawing.SystemColors.Window;
                            this.BackColor = System.Drawing.SystemColors.Window;
                            this.BackgroundColor = System.Drawing.SystemColors.AppWorkspace;
                            this.CaptionBackColor = System.Drawing.SystemColors.ActiveCaption;
                            this.CaptionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
                            this.ForeColor = System.Drawing.SystemColors.WindowText;
                            this.GridLineColor = System.Drawing.SystemColors.Control;
                            this.HeaderBackColor = System.Drawing.SystemColors.Control;
                            this.HeaderForeColor = System.Drawing.SystemColors.ControlText;
                            this.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
                            this.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
                            this.CaptionVisible = true;
                            break;
                        case "Skin02":
                            this.FlatMode = false;
                            this.AlternatingBackColor = System.Drawing.SystemColors.Window;
                            this.BackColor = System.Drawing.SystemColors.Window;
                            this.BackgroundColor = System.Drawing.Color.Black;
                            this.CaptionBackColor = System.Drawing.SystemColors.ActiveCaption;
                            this.CaptionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
                            this.ForeColor = System.Drawing.SystemColors.WindowText;
                            this.GridLineColor = System.Drawing.SystemColors.Control;
                            this.HeaderBackColor = System.Drawing.SystemColors.Control;
                            this.HeaderForeColor = System.Drawing.SystemColors.ControlText;
                            this.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
                            this.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
                            this.CaptionVisible = true;
                            break;

                    }
                }
                vForma = value;
            }
        }
        public BeDataGrid()
        {
            this.Apariencia = ctrLibreria.Controles.BeDataGrid.FormaTipo.Ninguno;//
            InitializeComponent();
        }

        #endregion
    }
}
