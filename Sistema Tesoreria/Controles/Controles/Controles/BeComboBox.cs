using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeComboBox : System.Windows.Forms.ComboBox 
    {
        #region Variables

        private bool vEnter;
        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos

        public BeComboBox()
        {
            //this.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Ninguno;//
            this.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01; 
            this.KeyEnter = true;//
            KeyPress += new System.Windows.Forms.KeyPressEventHandler(PulsarTecla);
            InitializeComponent();
        }
        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":
                            //this.BackColor = System.Drawing.Color.White;
                            this.BackColor = System.Drawing.Color.Ivory ;
                            //this.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
                            this.ForeColor = System.Drawing.Color.Black;
                            break;
                        case "Skin02":
                            this.BackColor = System.Drawing.Color.Black;
                            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                            this.ForeColor = System.Drawing.Color.Lime;
                            break;
                    }
                }
                vForma = value;
            }
        }
        [Description("Indica si enfocara otro control al pulsar Enter"), Browsable(true), Category("Agregados")]
        public bool KeyEnter
        {
            get { return vEnter; }
            set { this.vEnter = value; }
        }
        void PulsarTecla(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' & vEnter == Convert.ToBoolean("True"))
            {
                e.Handled = true;
                SendKeys.Send("{Tab}");
                return;
            }
        }

        #endregion
    }
}
