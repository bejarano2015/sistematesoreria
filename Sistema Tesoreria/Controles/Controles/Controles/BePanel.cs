using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BePanel : System.Windows.Forms.Panel 
    {
        #region Variables

        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos

        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":
                            this.BackColor = System.Drawing.Color.Transparent;
                            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                            this.ForeColor = System.Drawing.SystemColors.ControlText;
                            break;
                        case "Skin02":
                            this.BackColor = System.Drawing.Color.DimGray;
                            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
                            this.ForeColor = System.Drawing.SystemColors.ControlText;
                            break;
                    }
                }
                vForma = value;
            }
        }
        public BePanel()
        {
            this.Apariencia = ctrLibreria.Controles.BePanel.FormaTipo.Ninguno;//
            InitializeComponent();
        }

        #endregion
    }
}
