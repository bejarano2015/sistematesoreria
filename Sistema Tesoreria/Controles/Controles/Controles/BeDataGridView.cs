using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeDataGridView : System.Windows.Forms.DataGridView
    {
        #region Variables

        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos

        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {
                        case "Skin01":
                            this.BackgroundColor = System.Drawing.Color.White;
                            //Inicio dhMont
                            this.AlternatingRowsDefaultCellStyle.BackColor = Color.Ivory;
                            this.AlternatingRowsDefaultCellStyle.ForeColor = Color.Black;
                            this.AlternatingRowsDefaultCellStyle.SelectionBackColor = Color.NavajoWhite;
                            this.AlternatingRowsDefaultCellStyle.SelectionForeColor = Color.Black;

                            this.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;

                            this.DefaultCellStyle.BackColor = Color.Cornsilk;
                            this.DefaultCellStyle.ForeColor = Color.Black;
                            this.DefaultCellStyle.SelectionBackColor = Color.NavajoWhite;
                            this.DefaultCellStyle.ForeColor = Color.Black;

                            this.EnableHeadersVisualStyles = false;

                            this.RowHeadersVisible = false;

                            //this.RowHeadersWidth = 41;

                            //Fin dhMont






                            break;
                        case "Skin02":
                            this.BackgroundColor = System.Drawing.Color.Black;
                            break;
                    }
                }
                vForma = value;
            }
        }

        public BeDataGridView()
        {
            //this.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Ninguno;//
            this.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01;
            InitializeComponent();
        }

        #endregion
    }
}
