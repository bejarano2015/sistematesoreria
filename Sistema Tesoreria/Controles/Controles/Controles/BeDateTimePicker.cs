using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeDateTimePicker : System.Windows.Forms.DateTimePicker
    {
        #region Variables

        private bool vEnter;
        private ValidacionTipo vTipo;
        public enum ValidacionTipo { Fecha, Tiempo }
        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos

        public BeDateTimePicker()
        {
            //this.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Ninguno;//
            this.Apariencia = ctrLibreria.Controles.BeDateTimePicker.FormaTipo.Skin01; 
            this.KeyEnter = true;//
            this.Tipo = ctrLibreria.Controles.BeDateTimePicker.ValidacionTipo.Fecha;//
            KeyPress += new System.Windows.Forms.KeyPressEventHandler(PulsarTecla);
            InitializeComponent();
        }
        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":

                            this.CalendarForeColor = System.Drawing.SystemColors.ControlText;
                            this.CalendarMonthBackground = System.Drawing.SystemColors.Window;
                            this.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
                            this.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
                            break;
                        case "Skin02":
                            this.CalendarForeColor = System.Drawing.SystemColors.ButtonFace;
                            this.CalendarMonthBackground = System.Drawing.Color.Black;
                            this.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
                            this.CalendarTitleForeColor = System.Drawing.Color.Lime;
                            break;

                    }
                }
                vForma = value;
            }
        }
        [Description("Indica si enfocara otro control al pulsar Enter"), Browsable(true), Category("Agregados")]
        public bool KeyEnter
        {
            get { return vEnter; }
            set { this.vEnter = value; }
        }
        [Description("Indica el tipo de datetimepicker a usar"), Browsable(true), Category("Agregados")]
        public ValidacionTipo Tipo
        {
            get { return vTipo; }
            set
            {
                switch (Convert.ToString(value))
                {
                    case "Fecha":
                        this.CustomFormat = "";
                        this.Format = System.Windows.Forms.DateTimePickerFormat.Short;
                        this.ShowUpDown = false;
                        break;

                    case "Tiempo":
                        this.CustomFormat = "hh:mm tt";
                        this.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
                        this.ShowUpDown = true;
                        break;
                }
                vTipo = value;

            }
        }
        void PulsarTecla(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' & vEnter == Convert.ToBoolean("True"))
            {
                e.Handled = true;
                SendKeys.Send("{Tab}");
                return;
            }
        }

        #endregion
    }
}
