using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{

     

    public partial class BeButton : System.Windows.Forms.Button 
    {
        #region Variables

        private ValidacionTipo vTipo;
        public enum ValidacionTipo { Ninguno, Actualizar,Adicionar, Agregar, Anular, Ayuda, Buscar, DesFiltrar, Eliminar, Ejecutar, Estadistico, Examinar, Exportar, Filtrar, Cerrar, Grabar, Importar, Imprimir, Modificar, Nuevo }
        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos



       

        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":
                            //this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                            this.BackColor = System.Drawing.SystemColors.Control;
                            this.FlatAppearance.BorderColor = System.Drawing.Color.Black;
                            this.ForeColor = System.Drawing.SystemColors.ControlText;
                            break;
                        case "Skin02":
                            //this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                            this.BackColor = System.Drawing.Color.Black;
                            this.FlatAppearance.BorderColor = System.Drawing.Color.White;
                            this.ForeColor = System.Drawing.Color.White;
                            break;
                    }
                }
                vForma = value;
            }
        }
        [Description("Indica el tipo de boton a usar"), Browsable(true), Category("Agregados")]
        public ValidacionTipo Tipo
        {
            get { return vTipo; }
            set 
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {
                        case "Actualizar":
                            this.Text = "&Actualizar";
                            this.Image = global::ctrLibreria.Properties.Resources.EditTableHS;
                            break;

                        case "Adicionar":
                            this.Text = "A&dicionar";
                            this.Image = global::ctrLibreria.Properties.Resources.AddTableHS;
                            break;

                        case "Agregar":
                            this.Text = "Ag&regar";
                            this.Image = global::ctrLibreria.Properties.Resources.AddTableHS; 
                            break;

                        case "Anular":
                            this.Text = "Anu&lar";
                            this.Image = global::ctrLibreria.Properties.Resources.DeleteHS;
                            break;

                        case "Ayuda":
                            this.Text = "Ay&uda";
                            this.Image = global::ctrLibreria.Properties.Resources.Help;
                            break;

                        case "Buscar":
                            this.Text = "&Buscar";
                            this.Image = global::ctrLibreria.Properties.Resources.FindHS;
                            break;

                        case "DesFiltrar":
                            this.Text = "DesFil&trar";
                            this.Image = global::ctrLibreria.Properties.Resources.DesFiltrar;
                            break;

                        case "Eliminar":
                            this.Text = "&Eliminar";
                            this.Image = global::ctrLibreria.Properties.Resources.DeleteHS;
                            break;

                        case "Ejecutar":
                            this.Text = "Ej&ecutar";
                            this.Image = global::ctrLibreria.Properties.Resources.Filtrar;
                            break;

                        case "Estadistico":
                            this.Text = "Es&tadistico";
                            this.Image = global::ctrLibreria.Properties.Resources.Estadistico;
                            break;

                        case "Examinar":
                            this.Text = "Ex&aminar";
                            this.Image = global::ctrLibreria.Properties.Resources.Examinar;
                            break;

                        case "Exportar":
                            this.Text = "Expo&rtar";
                            this.Image = global::ctrLibreria.Properties.Resources.Importar_Exportar;
                            break;

                        case "Filtrar":
                            this.Text = "&Filtrar";
                            this.Image = global::ctrLibreria.Properties.Resources.Filtrar;
                            break;

                        case "Cerrar":
                            this.Text = "&Cerrar";
                            this.Image = global::ctrLibreria.Properties.Resources.Cerrar;
                            break;

                        case "Grabar":
                            this.Text = "Gr&abar";
                            this.Image = global::ctrLibreria.Properties.Resources.Grabar;
                            break;

                        case "Importar":
                            this.Text = "&Importar";
                            this.Image = global::ctrLibreria.Properties.Resources.Importar_Exportar; 
                            break;

                        case "Imprimir":
                            this.Text = "I&mprimir";
                            this.Image = global::ctrLibreria.Properties.Resources.Imprimir;
                            break;

                        case "Modificar":
                            this.Text = "Mo&dificar";
                            this.Image = global::ctrLibreria.Properties.Resources.EditTableHS;
                            break;

                        case "Nuevo":
                            this.Text = "Nue&vo";
                            this.Image = global::ctrLibreria.Properties.Resources.DocumentHS;
                            break;
                           
                    }
                }

                vTipo = value;
            }
        }

        public BeButton()
        {
            this.Apariencia = ctrLibreria.Controles.BeButton.FormaTipo.Ninguno;//
            this.Tipo = ctrLibreria.Controles.BeButton.ValidacionTipo.Ninguno;//
            InitializeComponent();
        }

        #endregion
    }
}
