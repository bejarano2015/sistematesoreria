using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeCheckedListBox : System.Windows.Forms.CheckedListBox 
    {
        #region Variables

        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos

        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":
                            this.BackColor = System.Drawing.Color.White;
                            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
                            this.ForeColor = System.Drawing.Color.Black;
                            break;
                        case "Skin02":
                            this.BackColor = System.Drawing.Color.Black;
                            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                            this.ForeColor = System.Drawing.Color.Lime;
                            break;
                    }
                }
                vForma = value;
            }
        }
        public BeCheckedListBox()
        {
            this.Apariencia = ctrLibreria.Controles.BeCheckedListBox.FormaTipo.Ninguno;//
            InitializeComponent();
        }

        #endregion
    }
}
