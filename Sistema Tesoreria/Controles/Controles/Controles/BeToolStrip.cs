using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeToolStrip : System.Windows.Forms.ToolStrip 
    {
        #region Variables

        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }

        #endregion

        #region Metodos

        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":
                            this.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
                            break;
                        case "Skin02":
                            this.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
                            break;

                    }
                }
                vForma = value;
            }
        }
        public BeToolStrip()
        {
            this.Apariencia = ctrLibreria.Controles.BeToolStrip.FormaTipo.Ninguno;//
            InitializeComponent();
        }

        #endregion
    }
}
