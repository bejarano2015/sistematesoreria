namespace ctrLibreria.Controles
{
    partial class BeDataGridView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // BeDataGridView
            //
            /*
            this.AllowUserToAddRows = true;
            this.AllowUserToDeleteRows = true;
            this.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ReadOnly = false;
            this.MultiSelect = true;
            */

            
            this.AllowUserToAddRows = false;
            this.AllowUserToDeleteRows = false;
            this.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ReadOnly = true;
            this.MultiSelect = false;
             
            //this.RowHeadersWidth = 30;
            //dhMont
            this.RowHeadersWidth = 41;
            //this.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            //dhMont
            this.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.Name = "BeDataGridView";
            this.ResumeLayout(false);
        }

        #endregion
    }
}
