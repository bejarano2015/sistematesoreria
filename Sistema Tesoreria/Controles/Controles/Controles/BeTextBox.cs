using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace ctrLibreria.Controles
{
    public partial class BeTextBox : System.Windows.Forms.TextBox
    {
        #region Variables

        private FormaTipo vForma;
        public enum FormaTipo { Ninguno, Skin01, Skin02 }
        private ValidacionTipo vTipo;
        private string sTipo;
        private ValidacionDec vDec;
        private string sDec;
        private Int32 iDec;
        private string Ceros;
        private bool vEnter;
        private int Diseno=0;
        private decimal trnsEntero;
        private string TextoTrim;
        private string sDecimales;
        private decimal vValor;
        private string ConDecimales = "No";
        public enum ValidacionTipo { Alfabetico, Alfan�merico, Codigo, Decimal, Entero, Email, Ninguno, N�merico }
        public enum ValidacionDec { Dos ,Tres,Cuatro }

        #endregion

        #region Comentarios

        //Desenfoque usar en HotKeys
        //Alfabetico:   Alfabetico y espacios
        //Alfan�merico: Alfabetico y espacios y numeros 
        //Codigo:       Alfabetico y numeros  
        //Decimal:      Numeros y punto (con extructura: 0.00)
        //Entero:       Numeros enteros 
        //E-Email:      Alfabetico y numeros y . - _ @
        //Ninguno:      Ninguno
        //N�merico:     Numeros 

        #endregion

        #region Metodos

        public BeTextBox()
        {
            //this.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Ninguno;//
            this.Apariencia = ctrLibreria.Controles.BeTextBox.FormaTipo.Skin01; 
            this.Tipo = ctrLibreria.Controles.BeTextBox.ValidacionTipo.Ninguno;//
            this.KeyEnter = true;//
            this.NroDecimales = ctrLibreria.Controles.BeTextBox.ValidacionDec.Dos;//
            //No cambia en modo de dise�o
            this.CharacterCasing = CharacterCasing.Upper;

            //No cambia en modo de dise�o
            this.ShortcutsEnabled = Convert.ToBoolean("False");
            //this.ContextMenu = new ContextMenu();
            KeyPress += new System.Windows.Forms.KeyPressEventHandler(PulsarTecla);
            Enter += new EventHandler(Entrada);
            Click += new EventHandler(Clickear);
            Leave += new EventHandler(PerderFoco);
            InitializeComponent();
        }
        [Description("Indica el skin a usar"), Browsable(true), Category("Agregados")]
        public FormaTipo Apariencia
        {
            get { return vForma; }
            set
            {
                if (Convert.ToString(value) != "Ninguno")
                {
                    switch (Convert.ToString(value))
                    {

                        case "Skin01":
                            //this.BackColor = System.Drawing.Color.White;
                            this.ForeColor = System.Drawing.Color.Black;
                            //Inicio Dhmont
                            this.BackColor = Color.Ivory; 
                            //Fin Dhmont
                            break;
                        case "Skin02":
                            this.BackColor = System.Drawing.Color.Black;
                            this.ForeColor = System.Drawing.Color.Lime;
                            break;
                    }
                }
                vForma = value;
            }
        }
        [Description("Indica si enfocara otro control al pulsar Enter"), Browsable(true), Category("Agregados")]
        public bool KeyEnter
        {
        get { return vEnter; }
        set { this.vEnter = value; }
        }
        [Description("Indica el tipo de validacion del texto"), Browsable(true), Category("Agregados")]
        public ValidacionTipo Tipo
        {
            get { return vTipo; }
            set { this.vTipo = value; }
        }
        [Description("Indica el N�mero de decimales a usar"), Browsable(true), Category("Agregados")]
        public ValidacionDec NroDecimales
        {
            get { return vDec; }
            set { this.vDec = value; }
        }
        void Clickear(object sender, EventArgs e)
        {
        ((TextBox)sender).SelectAll();
        }
        void PerderFoco(object sender, EventArgs e)
        {
        this.Desenfoque();
        }
        void Entrada(object sender, EventArgs e)
        {
            Diseno = 0;
            sTipo = Convert.ToString(vTipo);
            sDec=Convert.ToString(vDec);

            switch(sDec)
            {
                case "Dos": 
                    iDec = 2;
                    Ceros = "00";
                    break;
                case "Tres":
                    iDec = 3;
                    Ceros = "000";
                    break;
                case "Cuatro":
                    iDec = 4;
                    Ceros = "0000";
                    break;
            }

            switch (sTipo)
            {
                case "Decimal":
                    if (this.MaxLength < 4)
                    { Diseno = 1; }
                    break;
                case "Email":
                    if (this.MaxLength < 7)
                    { Diseno = 1; }
                    break;
            }

            if (Diseno == 1)
            {
            MessageBox.Show("Acci�n no Permitida, Error en Tiempo de Dise�o Caracteres Menores a lo Permitido", "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;
            }

            ((TextBox)sender).SelectAll();

        }
        void PulsarTecla(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
           if (e.KeyChar == '\r' & vEnter == Convert.ToBoolean("True"))
           {
           e.Handled = true;
           this.Desenfoque(); //Agregado el 19/03/08
           SendKeys.Send("{Tab}");

           return;
           }

           if (Diseno == 0)
           {
              switch (sTipo)
              {

              case "Alfabetico":
                    if (Char.IsDigit(e.KeyChar) ^ Char.IsSymbol(e.KeyChar) ^ Char.IsPunctuation(e.KeyChar) ^ e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�')
                    { e.Handled = true; }
                    break;

              case "Alfan�merico":
                    if (Char.IsSymbol(e.KeyChar) ^ Char.IsPunctuation(e.KeyChar) ^ e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�')
                    { e.Handled = true; }
                    break;

              case "Codigo":
                   if (Char.IsSymbol(e.KeyChar) ^ Char.IsPunctuation(e.KeyChar) ^ Char.IsSeparator(e.KeyChar) ^ e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�')
                   { e.Handled = true; }
                   break;

              case "Decimal":
                   if (this.Text.IndexOf('.') != -1 & e.KeyChar == '.')
                   {
                   e.Handled = true;
                   return;
                   }
                        
                   if (Char.IsDigit(e.KeyChar) ^ e.KeyChar == '.')
                   { return; }

                   if (char.IsLetter(e.KeyChar) ^ Char.IsSymbol(e.KeyChar) ^ Char.IsPunctuation(e.KeyChar) ^ Char.IsSeparator(e.KeyChar))
                   { e.Handled = true; }
                   break;

              case "Entero":
                   if (Char.IsLetter(e.KeyChar) ^ Char.IsSymbol(e.KeyChar) ^ Char.IsPunctuation(e.KeyChar) ^ Char.IsSeparator(e.KeyChar))
                   { e.Handled = true; }
                   break;

              case "Email":
                   if (this.Text.IndexOf('@') != -1 & e.KeyChar == '@')
                   {
                   e.Handled = true;
                   return;
                   }
                   if (e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�' ^ e.KeyChar == '�')
                   {
                   e.Handled = true;
                   return;
                   }
                   if (Char.IsDigit(e.KeyChar) ^ char.IsLetter(e.KeyChar) ^ e.KeyChar == '.' ^ e.KeyChar == '-' ^ e.KeyChar == '_' ^ e.KeyChar == '@')
                   { return; }

                   if (Char.IsSymbol(e.KeyChar) ^ Char.IsPunctuation(e.KeyChar) ^ Char.IsSeparator(e.KeyChar))
                   { e.Handled = true; }

                   break;

              case "Ninguno":
                   break;

              case "N�merico":
                   if (Char.IsLetter(e.KeyChar) ^ Char.IsSymbol(e.KeyChar) ^ Char.IsPunctuation(e.KeyChar) ^ Char.IsSeparator(e.KeyChar))
                   { e.Handled = true; }
                   break;
              }
           }
           else 
           {
           MessageBox.Show("Acci�n no Permitida, Revise la Cantidad de Caracteres Permitidos", "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
           e.Handled = true;
           return;
           }
        }
        public void Desenfoque()
        {
        TextoTrim = this.Text.Trim();
        this.Text = TextoTrim;

        if (Diseno == 0)
        {
            switch (sTipo)
            {
                case "Alfabetico":
                    break;

                case "Alfan�merico":
                    break;

                case "Codigo":
                    break;

                case "Decimal":
                    if (this.Text.Length != 0)
                    {
                        int lPunto = this.Text.IndexOf('.');
                        try
                        {
                                //Caso . , .X
                                if (lPunto==0)
                                {
                                    if (this.Text.Length != 1)
                                    {
                                    sDecimales = this.Text.Substring(lPunto + 1, (this.Text.Length - (lPunto + 1)));
                                    ConDecimales="Si";                                    
                                    }
                                    vValor= Convert.ToDecimal("0"); 
                                }

                                //Caso X
                                if(lPunto==-1)
                                {
                                    //Quitando cero inicial
                                    vValor= Convert.ToDecimal(this.Text); 
                                }

                                //Caso X. y X.X
                                if (lPunto > 0)
                                {
                                    sDecimales = this.Text.Substring(lPunto + 1, (this.Text.Length - (lPunto + 1)));
                                    if (sDecimales.Length >0)
                                    {
                                    ConDecimales="Si";                                    
                                    }

                                    //Quitando cero inicial
                                    vValor=Convert.ToDecimal(this.Text.Substring(0, lPunto + 1));
                                }                            


                            if (ConDecimales=="Si")
                            {
                                if ( sDecimales.Length  <= iDec)
                                {
                                    switch (Convert.ToString(iDec - sDecimales.Length))
                                    {
                                        case "0": 
                                            Ceros = sDecimales;
                                            break;
                                        case "1":
                                            Ceros = sDecimales +"0";
                                            break;
                                        case "2":
                                            Ceros = sDecimales +"00";
                                            break;
                                        case "3":
                                            Ceros = sDecimales +"000";
                                            break;
                                    }
                                   
                                }
                                else
                                {
                                    //Redondeo >=5
                                    int iCaracter = Convert.ToInt32(sDecimales.Substring(iDec ,1));
                                    if (iCaracter >= 5)
                                    {
                                        Ceros = Convert.ToString(Convert.ToInt32(sDecimales.Substring(0, iDec)) + 1);
                                    }
                                    else
                                    { 
                                        Ceros = Convert.ToString(Convert.ToInt32(sDecimales.Substring(0, iDec)));
                                    }
                                    
                                }
                            }
                            
                            this.Text = Convert.ToString(vValor) + "." + Ceros;
                            ConDecimales = "No";
                        }

                        catch
                        {
                            MessageBox.Show("Acci�n no Permitida, Error en Tiempo de Ejecuci�n", "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Text = "";
                            return;
                        }

                    }
                    break;

                case "Entero":
                    if (this.Text.Length != 0)
                    {
                        try
                        {
                            trnsEntero = Convert.ToDecimal(this.Text);
                            this.Text = Convert.ToString(trnsEntero);
                        }
                        catch
                        {
                            MessageBox.Show("Acci�n no Permitida, Error en Tiempo de Ejecuci�n", "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Text = "";
                            return;
                        }
                    }
                    break;

                case "Email":
                    break;

                case "Ninguno":
                    break;

                case "N�merico":
                    break;

            }
        }
    }

        #endregion
    }
}

