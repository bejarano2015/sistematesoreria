﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Cobranza
{
     public class BeanDetalleCobranza
    {
         public BeanDetalleCobranza() { }

         public int ID_COBRANZA_DETALLE;
         public int ID_COBRANZA;
         public int ID_DOC_VENTA;
         public Decimal IMPORTE;
         public int TIPO;

         public BeanDetalleCobranza( int VL_ID_COBRANZA, int VL_ID_DOC_VENTA, Decimal VL_IMPORTE, int VL_TIPO)
         {
             //ID_COBRANZA_DETALLE = VL_ID_COBRANZA_DETALLE;
             ID_COBRANZA = VL_ID_COBRANZA;
             ID_DOC_VENTA = VL_ID_DOC_VENTA;
             IMPORTE = VL_IMPORTE;
             TIPO = VL_TIPO;
         }
         public BeanDetalleCobranza(int VL_ID_COBRANZA_DETALLE, int VL_ID_COBRANZA, int VL_ID_DOC_VENTA, Decimal VL_IMPORTE, int VL_TIPO)
         {
             ID_COBRANZA_DETALLE = VL_ID_COBRANZA_DETALLE;
             ID_COBRANZA = VL_ID_COBRANZA;
             ID_DOC_VENTA = VL_ID_DOC_VENTA;
             IMPORTE = VL_IMPORTE;
             TIPO = VL_TIPO;
         }
    }
}
