﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Cobranza
{
     public class BeanCobranza
    {
         public BeanCobranza() { }

         public int ID_COBRANZA;
         public String ID_CLIENTE;
         public String FECHA;
         public Decimal TC;
         public String ID_BANCO;
         public String ID_CTA_CORRIENTE;
         public String NRO_OPERACION;
         public String SERIE;
         public String NUMERO;
         public String ID_MONEDA;
         public Decimal TOTAL;
         public String GLOSA;
         public int ASIENTO;
         public int ASIENTOOK;
         public String ASIENTOMSG;
         public String ID_USUARIO;
         public int TIPO;

         public BeanCobranza(String VL_ID_CLIENTE, String VL_FECHA, Decimal VL_TC, String VL_ID_BANCO, String VL_ID_CTA_CORRIENTE, String VL_NRO_OPERACION, String VL_SERIE, String VL_NUMERO, String VL_ID_MONEDA, Decimal VL_TOTAL, String VL_GLOSA, int VL_ASIENTO, int VL_ASIENTOOK, String VL_ASIENTOMSG, String VL_ID_USUARIO, int VL_TIPO)
         {
             //ID_COBRANZA = VL_ID_COBRANZA;
             ID_CLIENTE = VL_ID_CLIENTE;
             FECHA = VL_FECHA;
             TC = VL_TC;
             ID_BANCO = VL_ID_BANCO;
             ID_CTA_CORRIENTE = VL_ID_CTA_CORRIENTE;
             NRO_OPERACION = VL_NRO_OPERACION;
             SERIE = VL_SERIE;
             NUMERO = VL_NUMERO;
             ID_MONEDA = VL_ID_MONEDA;
             TOTAL = VL_TOTAL;
             GLOSA = VL_GLOSA;
             ASIENTO = VL_ASIENTO;
             ASIENTOOK = VL_ASIENTOOK;
             ASIENTOMSG = VL_ASIENTOMSG;
             ID_USUARIO = VL_ID_USUARIO;
             TIPO = VL_TIPO;
         }

         public BeanCobranza(int VL_ID_COBRANZA, String VL_ID_CLIENTE, String VL_FECHA, Decimal VL_TC, String VL_ID_BANCO, String VL_ID_CTA_CORRIENTE, String VL_NRO_OPERACION,String VL_SERIE,                   String VL_NUMERO, String VL_ID_MONEDA, Decimal VL_TOTAL, String VL_GLOSA, int VL_ASIENTO, int VL_ASIENTOOK, String VL_ASIENTOMSG,String VL_ID_USUARIO, int                            VL_TIPO)
         {
             ID_COBRANZA = VL_ID_COBRANZA;
             ID_CLIENTE = VL_ID_CLIENTE;
             FECHA = VL_FECHA;
             TC = VL_TC;
             ID_BANCO = VL_ID_BANCO;
             ID_CTA_CORRIENTE = VL_ID_CTA_CORRIENTE;
             NRO_OPERACION = VL_NRO_OPERACION;
             SERIE = VL_SERIE;
             NUMERO = VL_NUMERO;
             ID_MONEDA = VL_ID_MONEDA;
             TOTAL = VL_TOTAL;
             GLOSA = VL_GLOSA;
             ASIENTO = VL_ASIENTO;
             ASIENTOOK = VL_ASIENTOOK;
             ASIENTOMSG = VL_ASIENTOMSG;
             ID_USUARIO = VL_ID_USUARIO;
             TIPO = VL_TIPO;
         }
    }
}
