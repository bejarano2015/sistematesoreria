﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntityLayer.Login
{
    public class BeanLogin
    {
        public String USUA_CODIGO { get; set; }
        public String EMPL_CODIGO { get; set; }
        public String USUA_NICKNAME { get; set; }
        public String USUA_PASSWORD { get; set; }
        public String USUA_OBSERVACION { get; set; }
        public String USUA_SESION { get; set; }
        public String USUA_ESTADO { get; set; }
        public String OBRA_CODIGO { get; set; }
        public String SIST_CODIGO { get; set; }

        public BeanLogin()
        {
        }

        public BeanLogin(string VL_USUA_NICKNAME, string VL_USUA_PASSWORD)
        {
            USUA_NICKNAME = VL_USUA_NICKNAME;
            USUA_PASSWORD = VL_USUA_PASSWORD;
        }

        public BeanLogin(string VL_USUA_NICKNAME, string VL_OBRA_CODIGO, string VL_SIST_CODIGO)
        {
            USUA_NICKNAME = VL_USUA_NICKNAME;
            OBRA_CODIGO = VL_OBRA_CODIGO;
            SIST_CODIGO = VL_SIST_CODIGO;
        }

    }
}
