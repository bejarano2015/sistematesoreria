﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Impuestos
{
   public class BeanImpuesto_Vigente
    {

       public BeanImpuesto_Vigente() { }
       
       public int ID_IMPUESTO_VIGENCIA { get; set; }
        public int ID_IMPUESTO { get; set; }
       public string VALOR_PORCENTAJE { get; set; }
       public int ES_PORCENTAJE { get; set; }
       public int VIGENTE { get; set; }
       public String VIG_DESDE { get; set; }

       public String VIG_HASTA { get; set; }
       public String TIPO { get; set; }


       public BeanImpuesto_Vigente(int VL_ID_IMPUESTO, string VL_VALOR_PORCENTAJE, int VL_ES_PORCENTAJE, int VL_VIGENTE, String VL_VIG_DESDE, String VL_VIG_HASTA, String VL_TIPO)
        {
                    //ID_IMPUESTO_VIGENCIA = VL_ID_IMPUESTO_VIGENCIA;
                    ID_IMPUESTO = VL_ID_IMPUESTO;
                    VALOR_PORCENTAJE = VL_VALOR_PORCENTAJE;
                    ES_PORCENTAJE = VL_ES_PORCENTAJE;
                    VIGENTE=VL_VIGENTE;
                    VIG_DESDE=VL_VIG_DESDE;
                    VIG_HASTA=VL_VIG_HASTA;
                   TIPO=VL_TIPO;
        }

      
       public BeanImpuesto_Vigente(int VL_ID_IMPUESTO_VIGENCIA, int VL_ID_IMPUESTO, string VL_VALOR_PORCENTAJE, int VL_ES_PORCENTAJE, int VL_VIGENTE, String VL_VIG_DESDE, String VL_VIG_HASTA, String VL_TIPO)
       {
           ID_IMPUESTO_VIGENCIA = VL_ID_IMPUESTO_VIGENCIA;
           ID_IMPUESTO = VL_ID_IMPUESTO;
           VALOR_PORCENTAJE = VL_VALOR_PORCENTAJE;
           ES_PORCENTAJE = VL_ES_PORCENTAJE;
           VIGENTE = VL_VIGENTE;
           VIG_DESDE = VL_VIG_DESDE;
           VIG_HASTA = VL_VIG_HASTA;
           TIPO = VL_TIPO;
       }

          }
}
