﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Impuestos
{
   public class BeanImpuestos
    {
       public BeanImpuestos() { }

       public int ID_IMPUESTO { get; set; }
       public String IMP_NOMBRE { get; set; }
       public String IMP_ABREVIACION { get; set; }
       public String ID_CUENTA { get; set; }
       public String TIPO { get; set; }


       public BeanImpuestos(string VL_IMP_NOMBRE, string VL_IMP_ABREVIACION, string VL_ID_CUENTA, string VL_TIPO)
        {
            IMP_NOMBRE = VL_IMP_NOMBRE;
            IMP_ABREVIACION = VL_IMP_ABREVIACION;
            ID_CUENTA = VL_ID_CUENTA;
           TIPO=VL_TIPO;
        }

       public BeanImpuestos(string VL_IMP_NOMBRE, string VL_IMP_ABREVIACION, string VL_ID_CUENTA, int VL_ID_IMPUESTO,string VL_TIPO)
       {
           IMP_NOMBRE = VL_IMP_NOMBRE;
           IMP_ABREVIACION = VL_IMP_ABREVIACION;
           ID_CUENTA = VL_ID_CUENTA;
           ID_IMPUESTO = VL_ID_IMPUESTO;
            TIPO=VL_TIPO;
       }
    }
}
