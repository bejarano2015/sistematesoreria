﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Ventas.Reportes
{

    public class BeanBoleta
    {
        public string BOLE_CODIGO { get; set; }
        public string CLIE_CODIGO { get; set; }
        public string EXPE_CODIGO { get; set; }
        public string BOLE_NUMERO { get; set; }
        public string BOLE_SERIE { get; set; }
        public DateTime? BOLE_FECHA { get; set; }
        public string BOLE_ESTADO { get; set; }
        public string BOLE_PERIODO { get; set; }
        public string BOLE_ANIO { get; set; }
        public string BOLE_MONTO { get; set; }
        public string BOLE_CODIGOPAGO { get; set; } //RsyS 21/11/2014


        //SOBRECARGA PARA LA INSECION
        public BeanBoleta(string VL_CLIE_CODIGO, string VL_EXPE_CODIGO, string VL_BOLE_NUMERO, string VL_BOLE_SERIE
        , DateTime? VL_BOLE_FECHA, string VL_BOLE_MONTO, string VL_BOLE_CODIGOPAGO)
        {
            CLIE_CODIGO = VL_CLIE_CODIGO;
            EXPE_CODIGO = VL_EXPE_CODIGO;
            BOLE_NUMERO = VL_BOLE_NUMERO;
            BOLE_SERIE = VL_BOLE_SERIE;
            BOLE_FECHA = VL_BOLE_FECHA;
            BOLE_MONTO = VL_BOLE_MONTO;
            BOLE_CODIGOPAGO = VL_BOLE_CODIGOPAGO;//RsyS 21/11/2014

        }
        // REORDENAR NUMEROS 
        public BeanBoleta(string VL_BOLE_CODIGO, string VL_BOLE_NUMERO)
        {
            BOLE_CODIGO = VL_BOLE_CODIGO;
            BOLE_NUMERO = VL_BOLE_NUMERO;
        }
        public BeanBoleta()
        {
        }
    }
}
