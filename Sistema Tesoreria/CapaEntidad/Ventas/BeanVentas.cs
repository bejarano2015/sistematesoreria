﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Ventas
{
     public class BeanVentas
    {
         public BeanVentas() { }

         public int ID_DOC_VENTA { get; set; }
         public String FECHA { get; set; } 
         public int ESTADO { get; set; }
        public int ID_DOCUMENTO { get; set; }
        public String SERIE { get; set; }
        public String NUMERO { get; set; }
        public String ID_CLIENTE { get; set; }
        public String CLIENTE_DIRECCION { get; set; }
        public String ID_MONEDA { get; set; }
        public int ID_VENDEDOR { get; set; }
        public Decimal NETO { get; set; }
        public Decimal DESCUENTO { get; set; }
        public Decimal SUB_TOTAL { get; set; }
        public Decimal IMPUESTO { get; set; }
        public Decimal TOTAL { get; set; }
        public String OBSERVACIONES { get; set; }
        public int ID_PAGO { get; set; }
        public int ID_OBRA { get; set; }
        public String ID_COSTO { get; set; }
        public int ASIENTO { get; set; }
         public String ASIENTO_OK { get; set; }
         public String ASIENTO_MSG { get; set; }
         public String EMITIDA { get; set; }
         public String ID_USUARIO { get; set; }
         public int TIPO { get; set; }//OPCION PARA EL MANTENIMIENTO 1= AGREGAR, 2=ACTUALIZAR, 3=ELIMINAR

         public BeanVentas(String VL_FECHA, int VL_ESTADO, int VL_ID_DOCUMENTO, String VL_SERIE, String VL_NUMERO, String VL_ID_CLIENTE, 
             String VL_CLIENTE_DIRECCION, String VL_ID_MONEDA, int VL_ID_VENDEDOR, Decimal VL_NETO,
        Decimal VL_DESCUENTO, Decimal VL_SUB_TOTAL, Decimal VL_IMPUESTO, Decimal VL_TOTAL, String VL_OBSERVACIONES, int VL_ID_PAGO, int VL_ID_OBRA,
             String VL_ID_COSTO, int VL_ASIENTO, String VL_ASIENTO_OK, String VL_ASIENTO_MSG, String VL_EMITIDA, String VL_ID_USUARIO, int VL_TIPO) 
        {

                FECHA = VL_FECHA;
                ESTADO = VL_ESTADO;
                ID_DOCUMENTO=VL_ID_DOCUMENTO;
                SERIE=VL_SERIE;
                NUMERO = VL_NUMERO;
                ID_CLIENTE = VL_ID_CLIENTE;
                CLIENTE_DIRECCION = VL_CLIENTE_DIRECCION;
                ID_MONEDA = VL_ID_MONEDA;
                ID_VENDEDOR = VL_ID_VENDEDOR;
                NETO = VL_NETO;
                DESCUENTO = VL_DESCUENTO;
                SUB_TOTAL = VL_SUB_TOTAL;
                IMPUESTO = VL_IMPUESTO;
                TOTAL = VL_TOTAL;
                OBSERVACIONES = VL_OBSERVACIONES;
                ID_PAGO = VL_ID_PAGO;
                ID_OBRA = VL_ID_OBRA;
                ID_COSTO = VL_ID_COSTO;
                ASIENTO = VL_ASIENTO;
                ASIENTO_OK = VL_ASIENTO_OK;
                ASIENTO_MSG = VL_ASIENTO_MSG;
                EMITIDA = VL_EMITIDA;
                ID_USUARIO = VL_ID_USUARIO;

                TIPO = VL_TIPO;

        }

         public BeanVentas(String VL_FECHA, int VL_ESTADO, int VL_ID_DOCUMENTO, String VL_SERIE, String VL_NUMERO, String VL_ID_CLIENTE, 
             String VL_CLIENTE_DIRECCION, String VL_ID_MONEDA, int VL_ID_VENDEDOR, Decimal VL_NETO,
     Decimal VL_DESCUENTO, Decimal VL_SUB_TOTAL, Decimal VL_IMPUESTO, Decimal VL_TOTAL, String VL_OBSERVACIONES, int VL_ID_PAGO, int VL_ID_OBRA,
             String VL_ID_COSTO, int VL_ASIENTO, String VL_ASIENTO_OK, String VL_ASIENTO_MSG, String VL_EMITIDA, String VL_ID_USUARIO, int VL_ID_DOC_VENTA, int VL_TIPO)
        {

            FECHA = VL_FECHA;
            ESTADO = VL_ESTADO;
            ID_DOCUMENTO = VL_ID_DOCUMENTO;
            SERIE = VL_SERIE;
            NUMERO = VL_NUMERO;
            ID_CLIENTE = VL_ID_CLIENTE;
            CLIENTE_DIRECCION = VL_CLIENTE_DIRECCION;
            ID_MONEDA = VL_ID_MONEDA;
            ID_VENDEDOR = VL_ID_VENDEDOR;
            NETO = VL_NETO;
            DESCUENTO = VL_DESCUENTO;
            SUB_TOTAL = VL_SUB_TOTAL;
            IMPUESTO = VL_IMPUESTO;
            TOTAL = VL_TOTAL;
            OBSERVACIONES = VL_OBSERVACIONES;
            ID_PAGO = VL_ID_PAGO;
            ID_OBRA = VL_ID_OBRA;
            ID_COSTO = VL_ID_COSTO;
            ASIENTO = VL_ASIENTO;
            ASIENTO_OK = VL_ASIENTO_OK;
            ASIENTO_MSG = VL_ASIENTO_MSG;
            EMITIDA = VL_EMITIDA;
            ID_USUARIO = VL_ID_USUARIO;
            ID_DOC_VENTA = VL_ID_DOC_VENTA;

            TIPO = VL_TIPO;
        }

    }
}
