﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Ventas
{
  public  class BeanVentaLinea
    {
      public BeanVentaLinea() { }

      public int ID_DOC_VENTA_LINEA { get; set; }
      public int ID_DOC_VENTA { get; set; }
      public String ID_ITEM_VENTA { get; set; }
      public Decimal CANTIDAD { get; set; }
      public Decimal PRECIO { get; set; }
      public Decimal DESCUENTO { get; set; }
      public Decimal IMPORTE { get; set; }
      public String ID_OBRA { get; set; }
      public String ID_COSTO { get; set; }
      public String GLOSA { get; set; }
      public int TIPO { get; set; }

      public BeanVentaLinea(int VL_ID_DOC_VENTA, String VL_ID_ITEM_VENTA, Decimal VL_CANTIDAD, Decimal VL_PRECIO, Decimal VL_DESCUENTO, Decimal VL_IMPORTE, String VL_ID_OBRA,
          String VL_ID_COSTO, String VL_GLOSA, int VL_TIPO)
      {
          ID_DOC_VENTA = VL_ID_DOC_VENTA;
          ID_ITEM_VENTA = VL_ID_ITEM_VENTA;
          CANTIDAD = VL_CANTIDAD;
          PRECIO = VL_PRECIO;
          DESCUENTO = VL_DESCUENTO;
          IMPORTE = VL_IMPORTE;
          ID_OBRA = VL_ID_OBRA;
          ID_COSTO = VL_ID_COSTO;
          GLOSA = VL_GLOSA;
          TIPO = VL_TIPO;
      }

      public BeanVentaLinea(int VL_ID_DOC_VENTA, String VL_ID_ITEM_VENTA, Decimal VL_CANTIDAD, Decimal VL_PRECIO, Decimal VL_DESCUENTO, Decimal VL_IMPORTE, String VL_ID_OBRA,
         String VL_ID_COSTO, String VL_GLOSA, int VL_ID_DOC_VENTA_LINEA, int VL_TIPO)
      {
          ID_DOC_VENTA = VL_ID_DOC_VENTA;
          ID_ITEM_VENTA = VL_ID_ITEM_VENTA;
          CANTIDAD = VL_CANTIDAD;
          PRECIO = VL_PRECIO;
          DESCUENTO = VL_DESCUENTO;
          IMPORTE = VL_IMPORTE;
          ID_OBRA = VL_ID_OBRA;
          ID_COSTO = VL_ID_COSTO;
          GLOSA = VL_GLOSA;
          ID_DOC_VENTA_LINEA = VL_ID_DOC_VENTA_LINEA;
          TIPO = VL_TIPO;
          
      }
    }
}
