﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Ventas
{
    public class BeanDocVentasImpuesto
    {
        public BeanDocVentasImpuesto() { }

        public int ID_DOCU { get; set; }
        public int ID_IMPUESTO_VIGENCIA { get; set; }
        public decimal BASE_IMPONIBLE { get; set; }
        public decimal IMPORTE { get; set; }
        public int TIPO { get; set; }

        public BeanDocVentasImpuesto(int VL_ID_DOCU, int VL_ID_IMPUESTO_VIGENCIA, decimal VL_BASE_IMPONIBLE, decimal VL_IMPORTE)
        {
            ID_DOCU = VL_ID_DOCU;
            ID_IMPUESTO_VIGENCIA = VL_ID_IMPUESTO_VIGENCIA;
            BASE_IMPONIBLE = VL_BASE_IMPONIBLE;
            IMPORTE = VL_IMPORTE;
          
        }

        public BeanDocVentasImpuesto(int VL_ID_DOCU, int VL_ID_IMPUESTO_VIGENCIA, decimal VL_BASE_IMPONIBLE, decimal VL_IMPORTE, int VL_TIPO)
        {
            ID_DOCU = VL_ID_DOCU;
            ID_IMPUESTO_VIGENCIA = VL_ID_IMPUESTO_VIGENCIA;
            BASE_IMPONIBLE = VL_BASE_IMPONIBLE;
            IMPORTE = VL_IMPORTE;
            TIPO = VL_TIPO;
        }
    }
}
