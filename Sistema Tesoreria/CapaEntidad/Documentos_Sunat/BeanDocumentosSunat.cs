﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Documentos_Sunat
{
   public class BeanDocumentosSunat
    {
        public BeanDocumentosSunat(){}

        public int ID_DOCUMENTO { get; set; }
        public String DESCRIPCION { get; set; }
        public String COD_DOC_SUNAT { get; set; }
        public String ID_CUENTA_CTBLE { get; set; }
        public String TIPO { get; set; }


        public BeanDocumentosSunat(int VL_ID_DOCUMENTO, string VL_DESCRIPCION, string VL_COD_DOC_SUNAT, String VL_ID_CUENTA_CTBLE, string VL_TIPO)
       {
           ID_DOCUMENTO = VL_ID_DOCUMENTO;
           DESCRIPCION = VL_DESCRIPCION;
           COD_DOC_SUNAT = VL_COD_DOC_SUNAT;
           ID_CUENTA_CTBLE = VL_ID_CUENTA_CTBLE;
           TIPO=VL_TIPO;
       }
    }
}
