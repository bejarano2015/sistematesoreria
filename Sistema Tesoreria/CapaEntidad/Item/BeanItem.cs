﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Item
{
   public class BeanItem
    {
        public BeanItem(){}
        public int ID_ITEM { get; set; }
        public String ITEM { get; set; }
        public String ID_CUENTA { get; set; }
        public String ESTADO { get; set; }
        public int TIPO { get; set; }

        public BeanItem(int VL_ID_ITEM, String VL_ITEM, String VL_ID_CUENTA, String VL_ESTADO, int VL_TIPO) 
        { 
                ID_ITEM=VL_ID_ITEM;
                ITEM=VL_ITEM;
                ID_CUENTA=VL_ID_CUENTA;
                ESTADO=VL_ESTADO;
                TIPO = VL_TIPO;
        }
    }
}
