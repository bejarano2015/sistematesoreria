﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Mantenimiento
{
   public class BeanContratoSubcontratos
    {
      public int  Co_Contrato {set;get;}
      public string   Co_Empresa {set;get;}
      public string Co_Obra{set;get;}
      public string Co_Proveedor{set;get;}
      public int Nu_Contrato{set;get;}
      public string Ss_MontoContrato{set;get;}
      public string Tx_DescrpcionContrato{set;get;}
      public decimal Po_Scar{set;get;}
      public int Fl_ModoPagoRetencion{set;get;}
      public decimal Ss_ValorizacionAcumulada { set; get; }
      public decimal Ss_SaldoValorizacion { set; get; }
      public decimal Ss_ScarAcumulada { set; get; }
      public decimal Ss_DescuentoAcumulado { set; get; }
      public decimal Ss_AmortizacionAcumulada { set; get; }
      public string  usuario { set; get; }   
  
        //listar
      public BeanContratoSubcontratos()
      { 
      
      }
        //elimibar
      public BeanContratoSubcontratos(int Co_Contrato)
      {
          this.Co_Contrato = Co_Contrato;

      }
        //grabar
      public BeanContratoSubcontratos(string Co_Empresa, string Co_Obra,string Co_Proveedor, int Nu_Contrato,string Ss_MontoContrato,string Tx_DescrpcionContrato,decimal Po_Scar,int Fl_ModoPagoRetencion,string usuario  )
      {
          this.Co_Empresa= Co_Empresa;
          this.Co_Obra =Co_Obra;          
          this.Co_Proveedor = Co_Proveedor;          
          this.Nu_Contrato = Nu_Contrato;
          this.Ss_MontoContrato = Ss_MontoContrato;
          this.Tx_DescrpcionContrato = Tx_DescrpcionContrato;
          this.Po_Scar = Po_Scar;
          this.Fl_ModoPagoRetencion = Fl_ModoPagoRetencion;
          this.usuario = usuario; 
      }
      //modificar
      public BeanContratoSubcontratos(int Co_Contrato, string Co_Empresa, string Co_Obra, string Co_Proveedor, int Nu_Contrato, string Ss_MontoContrato, string Tx_DescrpcionContrato, decimal Po_Scar, int Fl_ModoPagoRetencion, string usuario)
      {
          this.Co_Contrato = Co_Contrato;
          this.Co_Empresa = Co_Empresa;
          this.Co_Obra = Co_Obra;
          this.Co_Proveedor = Co_Proveedor;
          this.Nu_Contrato = Nu_Contrato;
          this.Ss_MontoContrato = Ss_MontoContrato;
          this.Tx_DescrpcionContrato = Tx_DescrpcionContrato;
          this.Po_Scar = Po_Scar;
          this.Fl_ModoPagoRetencion = Fl_ModoPagoRetencion;
          this.usuario = usuario;
      }

    }
}
