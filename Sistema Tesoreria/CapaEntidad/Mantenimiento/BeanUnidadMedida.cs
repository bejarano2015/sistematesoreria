﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad.Mantenimiento
{
    public class BeanUnidadMedida
    {
        public string UmdCodigo { get; set; }
        public string UmdDescripcion { get; set; }
        public BeanUnidadMedida()
        { 
        
        }
    }
}
