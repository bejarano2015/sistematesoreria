﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
   public  class BeanProveedor
    {
        public string Ruc { get; set; }
        public string RazonSocial { get; set; }
        public string Estado { get; set; }
        public string Condicion { get; set; }
        public string Direccion { get; set; }
        
      
      
        // select NUMEROS 
        public BeanProveedor(string v_Ruc, string v_RazonSocial, string v_Estado, string v_Condicion, string v_Direccion)
        {
            Ruc = v_Ruc;
            RazonSocial = v_RazonSocial;
            Estado = v_Estado;
            Condicion = v_Condicion;
            Direccion = v_Direccion;
        }
        public BeanProveedor()
        {
        }
        public struct ResultadoSelect
        {
            public BeanProveedor objProveedor;
            public bool blnExiste;
            public string strMensaje;
        }
    }
}
