﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad
{
    public class BeanEntregaRendir
    {
       public string ID_EntregaRendir {set;get;}
       public string EmprCodigo {set;get;}
       public string SolCodigo { set; get; }
       public string ObraAreaCodigo { set; get; }
       public string MonCodigo {set;get;}
       public Decimal Monto {set;get;}
       public string Concepto {set;get;}
       public string ID_ESTADO {set;get;}
       public DateTime Fecha {set;get;}
       public string AutCodigo { set; get; }
       public string GirCodigo { set; get; }
       public string AreaCodigo { set; get; }
       public string SolTPla { set; get; }
       public string AutTPla { set; get; }
       public string GirTPla { set; get; }
       public DateTime FechaInicio { set; get; }
       public DateTime FechaFin { set; get; }
       public string ObrEmprCodigo { set; get; }
       public string ID_DOCUMENTO { set; get; }


       public BeanEntregaRendir(string V_EmprCodigo, string V_SolCodigo, DateTime V_FechaInicio,DateTime V_FechaFin)
       {
           EmprCodigo = V_EmprCodigo;
           //SolTPla = V_SolTPla;
           SolCodigo = V_SolCodigo;
           FechaInicio = V_FechaInicio;
           FechaFin = V_FechaFin;
          
       }  


       public BeanEntregaRendir(string V_EmprCodigo)
       {
           EmprCodigo = V_EmprCodigo;
       }

       public BeanEntregaRendir(string V_EmprCodigo, string V_AreaCodigo)
       {
           EmprCodigo = V_EmprCodigo;
           AreaCodigo = V_AreaCodigo;

       }

       public BeanEntregaRendir(string V_ID_EntregaRendir, string V_ID_ESTADO, string V_MonCodigo)
       {
           ID_EntregaRendir = V_ID_EntregaRendir;
           ID_ESTADO = V_ID_ESTADO;
           MonCodigo = V_MonCodigo;

       }

       public BeanEntregaRendir(DateTime VL_fechainicio, DateTime VL_Fechafin, string V_EmprCodigo, string V_SolCodigo)
       {
           FechaInicio = VL_fechainicio;
           FechaFin = VL_Fechafin;
           EmprCodigo = V_EmprCodigo;
           SolCodigo = V_SolCodigo;
       }


       public BeanEntregaRendir(string V_EmprCodigo, string V_SolCodigo, string V_ObraAreaCodigo,
           string V_MonCodigo, Decimal V_Monto, string V_Concepto, string V_ID_ESTADO, DateTime V_Fecha, string V_AutCodigo, string V_GirCodigo
           , string V_AreaCodigo, string V_ObrEmprCodigo, string V_ID_DOCUMENTO,string V_X)
       {
           
           EmprCodigo=V_EmprCodigo;
           SolCodigo = V_SolCodigo;
           ObraAreaCodigo = V_ObraAreaCodigo;
           MonCodigo=V_MonCodigo;
           Monto=V_Monto;
           Concepto=V_Concepto;
           ID_ESTADO=V_ID_ESTADO;
           Fecha = V_Fecha;
           AutCodigo= V_AutCodigo;
           GirCodigo=V_GirCodigo;
           AreaCodigo = V_AreaCodigo;
           //SolTPla = V_SolTPla;
           //AutTPla = V_AutTPla;
           //GirTPla = V_GirTPla;
           ObrEmprCodigo = V_ObrEmprCodigo;
           ID_DOCUMENTO = V_ID_DOCUMENTO;
           V_X = "0";
           

       }

       public BeanEntregaRendir(string V_ID_EntregaRendir, string V_EmprCodigo, string V_SolCodigo, string V_ObraAreaCodigo,
        string V_MonCodigo, Decimal V_Monto, string V_Concepto, string V_ID_ESTADO, DateTime V_Fecha, string V_AutCodigo, string V_GirCodigo
        , string V_AreaCodigo,  string V_ObrEmprCodigo)
       {
           ID_EntregaRendir = V_ID_EntregaRendir;
           EmprCodigo = V_EmprCodigo;
           SolCodigo = V_SolCodigo;
           ObraAreaCodigo = V_ObraAreaCodigo;
           MonCodigo = V_MonCodigo;
           Monto = V_Monto;
           Concepto = V_Concepto;
           ID_ESTADO = V_ID_ESTADO;
           Fecha = V_Fecha;
           AutCodigo = V_AutCodigo;
           GirCodigo = V_GirCodigo;
           AreaCodigo = V_AreaCodigo;
           //SolTPla = V_SolTPla;
           //AutTPla = V_AutTPla;
           //GirTPla = V_GirTPla;
           ObrEmprCodigo = V_ObrEmprCodigo;


       }

       

    }
}
