﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad
{
    public class EntAlmacenIngreso
    {
        public string NroDocReferencia { get; set; }
        public string PrvRUC { get; set; }
        public string PrvRazonSocial { get; set; }
        public string aIngCodigo { get; set; }
        public string OcoCodigo { get; set; }
        
      
      
        // select NUMEROS 
        public EntAlmacenIngreso(string vl_OcoCodigo, string vl_NroDocReferencia)
        {
            OcoCodigo = OcoCodigo;
            NroDocReferencia = vl_NroDocReferencia;
        }
        public EntAlmacenIngreso()
        {
        }
    }
}
