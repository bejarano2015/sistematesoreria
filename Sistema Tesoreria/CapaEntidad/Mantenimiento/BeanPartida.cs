﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidad.Mantenimiento
{
   public  class BeanPartida
    {
        public string ParCodigo { get; set; }
        public string ParDescripcion { get; set; }
        public  BeanPartida(String ParDescripcion)
        {
            this.ParDescripcion = ParDescripcion;
        }

    }
}
