﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Conceptos_Planillas
{
    public class BeanConceptoPlanillas
    {
        public BeanConceptoPlanillas() { }

        public int ID_PAGO_PLANILLA { set; get; }        
        public int ID_CONCEPTO { set; get; }
        public string ID_EMPRESA { set; get; }
        public string ID_OBRA { set; get; }
        public string FECHA { set; get; }
        public Decimal IMPORTE { set; get; }
        public string ID_MONEDA { set; get; }
        public string GLOSA { set; get; }
        public int AÑO { set; get; }
        public int ID_MES { set; get; }
        public int ID_PERIODO { set; get; }
        public int FLAG { set; get; }
        public int TIPO { set; get; }


        public BeanConceptoPlanillas(int VL_ID_CONCEPTO, string VL_ID_EMPRESA, string VL_ID_OBRA, string VL_FECHA,
      Decimal VL_IMPORTE, string VL_ID_MONEDA, string VL_GLOSA, int VL_AÑO, int VL_ID_MES, int VL_ID_PERIODO, int VL_FLAG)
        {
            ID_CONCEPTO = VL_ID_CONCEPTO;
            ID_EMPRESA = VL_ID_EMPRESA;
            ID_OBRA = VL_ID_OBRA;
            FECHA = VL_FECHA;
            ID_MONEDA = VL_ID_MONEDA;
            IMPORTE = VL_IMPORTE;
            GLOSA = VL_GLOSA;
            AÑO = VL_AÑO;
            ID_MES = VL_ID_MES;
            ID_PERIODO = VL_ID_PERIODO;
            FLAG = VL_FLAG;

        }
        public BeanConceptoPlanillas(int VL_ID_PAGO_PLANILLA, int VL_ID_CONCEPTO  ,string VL_ID_EMPRESA , string VL_ID_OBRA , string VL_FECHA,
       Decimal VL_IMPORTE, string VL_ID_MONEDA, string VL_GLOSA, int VL_AÑO, int VL_ID_MES, int VL_ID_PERIODO,int VL_FLAG)
        {
            ID_PAGO_PLANILLA = VL_ID_PAGO_PLANILLA;
            ID_CONCEPTO = VL_ID_CONCEPTO;
            ID_EMPRESA = VL_ID_EMPRESA;
            ID_OBRA = VL_ID_OBRA;
            FECHA = VL_FECHA;
            ID_MONEDA = VL_ID_MONEDA;
            IMPORTE = VL_IMPORTE;
            GLOSA = VL_GLOSA;
            AÑO = VL_AÑO;
            ID_MES = VL_ID_MES;
            ID_PERIODO = VL_ID_PERIODO;
            FLAG = VL_FLAG;
        
        }
    }
}
