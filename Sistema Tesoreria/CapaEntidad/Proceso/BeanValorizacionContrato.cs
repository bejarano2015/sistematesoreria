﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Proceso
{
    public class BeanValorizacionContrato
    {

      public int   Co_Valorizacion {get;set;}
      public int Contrato_Subcontratista_Co_Contrato{get;set;}
      public string Co_RegistroDocumento{get;set;}
      public int Nu_Valorizacion{get;set;}
      public decimal Ss_MontoValorizacion{get;set;}
      public decimal Ss_MontoAdelanto{get;set;}
      public decimal Ss_MontoDescuento{get;set;}
      public decimal Ss_MontoAmortizacion{get;set;}
      public decimal Ss_SCAR{get;set;}
      public decimal Ss_FacturaSubtotal{get;set;}
      public decimal Ss_FacturaIgv{get;set;}
      public decimal Ss_FacturaTotal{get;set;}
      public decimal Ss_NetoPagar { get; set; }
      public string usuario{get;set;}
      public int Nu_Semana { get; set; }


     public  BeanValorizacionContrato()
      {     }
        // insertarValorizaciones
     public BeanValorizacionContrato( int Contrato_Subcontratista_Co_Contrato,string Co_RegistroDocumento,int Nu_Valorizacion,decimal Ss_MontoValorizacion,decimal Ss_MontoAdelanto,decimal Ss_MontoDescuento,
       decimal Ss_MontoAmortizacion, decimal Ss_SCAR, decimal Ss_FacturaSubtotal, decimal Ss_FacturaIgv, decimal Ss_FacturaTotal, decimal Ss_NetoPagar, string usuario, int Nu_Semana)
     {
         this.Contrato_Subcontratista_Co_Contrato = Contrato_Subcontratista_Co_Contrato;
         this.Co_RegistroDocumento = Co_RegistroDocumento;
         this.Nu_Valorizacion = Nu_Valorizacion;
         this.Ss_MontoValorizacion = Ss_MontoValorizacion;
         this.Ss_MontoAdelanto = Ss_MontoAdelanto;
         this.Ss_MontoDescuento = Ss_MontoDescuento;
         this.Ss_MontoAmortizacion = Ss_MontoAmortizacion;
         this.Ss_SCAR = Ss_SCAR;
         this.Ss_FacturaSubtotal = Ss_FacturaSubtotal;
         this.Ss_FacturaIgv = Ss_FacturaIgv;
         this.Ss_FacturaTotal = Ss_FacturaTotal;
         this.Ss_NetoPagar = Ss_NetoPagar;
         this.usuario = usuario;
         this.Nu_Semana = Nu_Semana;
     
     }

    }
}
