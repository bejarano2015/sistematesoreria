﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Proceso
{
    public class BeanConsolidado
    {
        //------------------


        public int opcion { get; set; }
        public string EmprCodigo { get; set; }
        public string CCosCodigo { get; set; }
        public string UbtCodigo { get; set; }
        public string EspCodigo { get; set; }
        public string TitCodigo { get; set; }
        public string ParCodigo { get; set; }
        public string ConsCodigo { get; set; }
        public string ObraCodigo { get; set; }
        public string UsuCreacion { get; set; }
        public string PcCreacion { get; set; }
        public DateTime FechCreacion { get; set; }
        public string UsuActualiza { get; set; }
        public string PcActualiza { get; set; }
        public DateTime FechActualiza { get; set; }
        public string UMedida { get; set; }
        public double Metrado { get; set; }
        public double PrecioPresupuesto { get; set; }
        public double PrecioSubcontrato { get; set; }
        public double Avance_Metrado { get; set; }
        public string MonCodigo { get; set; }
        public string PtiCodigo { get; set; }
        public string ItemPresupuesto { get; set; }
        public double PrecioMaterialExp { get; set; }
        public double PrecioMaterialInt { get; set; }
        public double PrecioEquiposExp { get; set; }
        public double PrecioEquiposInt { get; set; }
        public double PrecioSubPartidaExp { get; set; }
        public double PrecioSubPartidaInt { get; set; }
        public string SpptoCodigo { get; set; }
        public string TipoPartida { get; set; }
        public string N4Codigo { get; set; }
        public string Periodo { get; set; }

        public BeanConsolidado()
        { }
        public BeanConsolidado(string vl_EmprCodigo, string vl_CCosCodigo, string vl_UbtCodigo, string vl_ParCodigo, string vl_Periodo, string vl_UsuCreacion)
        {
            this.EmprCodigo = vl_EmprCodigo;
            this.CCosCodigo = vl_CCosCodigo;
            this.UbtCodigo = vl_UbtCodigo;
            this.ParCodigo = vl_ParCodigo;
            this.Periodo = vl_Periodo;
            this.UsuCreacion = vl_UsuCreacion;
        }

        public BeanConsolidado(string vl_EmprCodigo, string vl_CCosCodigo, string vl_UbtCodigo, string vl_UsuCreacion)
        {
            this.EmprCodigo = vl_EmprCodigo;
            this.CCosCodigo = vl_CCosCodigo;
            this.UbtCodigo = vl_UbtCodigo;
            this.UsuCreacion = vl_UsuCreacion;
        }
        public BeanConsolidado(int vl_opcion, string vl_EmprCodigo, string vl_CCosCodigo, string vl_UbtCodigo, string vl_EspCodigo, string vl_TitCodigo, string vl_ParCodigo, string vl_ObraCodigo, string vl_ConsCodigo, string vl_UsuCreacion
            , string vl_UMedida, double vl_Metrado, double vl_PrecioPresupuesto, double vl_PrecioSubcontrato, string vl_MonCodigo, string vl_PtiCodigo, string vl_ItemPresupuesto, string vl_SpptoCodigo, string vl_TipoPartida, string vl_N4Codigo)
        {
            this.opcion = vl_opcion;
            this.EmprCodigo = vl_EmprCodigo;
            this.CCosCodigo = vl_CCosCodigo;
            this.UbtCodigo = vl_UbtCodigo;
            this.EspCodigo = vl_EspCodigo;
            this.TitCodigo = vl_TitCodigo;
            this.ParCodigo = vl_ParCodigo;
            this.ObraCodigo = vl_ObraCodigo;
            this.ConsCodigo = vl_ConsCodigo;
            this.UsuCreacion = vl_UsuCreacion;
            this.UMedida = vl_UMedida;
            this.Metrado = vl_Metrado;
            this.PrecioPresupuesto = vl_PrecioPresupuesto;
            this.PrecioSubcontrato = vl_PrecioSubcontrato;
            this.MonCodigo = vl_MonCodigo;
            this.PtiCodigo = vl_PtiCodigo;
            this.ItemPresupuesto = vl_ItemPresupuesto;
            this.SpptoCodigo = vl_SpptoCodigo;
            this.TipoPartida = vl_TipoPartida;
            this.N4Codigo = vl_N4Codigo;
        }

    }
}
