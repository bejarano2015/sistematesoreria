﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Proceso
{
    public class BeanProgramacionPagos
    {
        public DateTime Fecha { get; set; }
        public DateTime Vigencia { get; set; }
        public string Id_moneda { get; set; }
        public string Total { get; set; }
        public string Estado { get; set; }
        public decimal ID_PADRE { get; set; }
        public string ID_PROGPAGO { get; set; }

        public BeanProgramacionPagos(DateTime V_Fecha, DateTime V_Vigencia, string V_Id_moneda, string V_Total, string V_Estado)
        {
            Fecha = V_Fecha;
            Vigencia = V_Vigencia;
            Id_moneda = V_Id_moneda;
            Total = V_Total;
            Estado = V_Estado;
           

        }

        public BeanProgramacionPagos(string V_Estado)
        {
            Estado = V_Estado;

        }

        public BeanProgramacionPagos(string VL_ID_PROGPAGO, string VL_Id_Moneda)
        {
            ID_PROGPAGO = VL_ID_PROGPAGO;
            Id_moneda = VL_ID_PROGPAGO;
        }

        public BeanProgramacionPagos(string VL_ID_PROGPAGO, string VL_Estado,string VL_Id_Moneda)
        {
            ID_PROGPAGO = VL_ID_PROGPAGO;
            Estado = VL_Estado;
            Id_moneda = VL_ID_PROGPAGO;
        }

    }
}
