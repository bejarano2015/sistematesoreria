﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Proceso
{
    public class BeanGestionDocumentariaDetalle
    {

        public int IDCABECERA { get; set; }

        public int IDDETALLE { get; set; }
        public DateTime FECHAINICIO { get; set; }
        public DateTime FECHAVENCIMIENTO { get; set; }
        public Decimal IMPORTEPACTADO { get; set; }
        public String MONEDA { get; set; }
        public String UNIDADESAFECTADAS { get; set; }

        public BeanGestionDocumentariaDetalle()
        {        }

        public BeanGestionDocumentariaDetalle(int VL_IDCABECERA, int VL_IDDETALLE, DateTime VL_FECHAINICIO, DateTime VL_FECHAVENCIMIENTO, Decimal VL_IMPORTEPACTADO, String VL_MONEDA, String VL_UNIDADESAFECTADAS) // EDITAR
        {

            this.IDCABECERA = VL_IDCABECERA;
            this.IDDETALLE = VL_IDDETALLE;
            this.FECHAINICIO = VL_FECHAINICIO;
            this.FECHAVENCIMIENTO = VL_FECHAVENCIMIENTO;
            this.IMPORTEPACTADO = VL_IMPORTEPACTADO;
            this.MONEDA = VL_MONEDA;
            this.UNIDADESAFECTADAS = VL_UNIDADESAFECTADAS;
        }

        public BeanGestionDocumentariaDetalle(int VL_IDCABECERA, DateTime VL_FECHAINICIO, DateTime VL_FECHAVENCIMIENTO, Decimal VL_IMPORTEPACTADO, String VL_MONEDA, String VL_UNIDADESAFECTADAS) // GRABAR
        {
            this.IDCABECERA = VL_IDCABECERA;
            this.FECHAINICIO = VL_FECHAINICIO;
            this.FECHAVENCIMIENTO = VL_FECHAVENCIMIENTO;
            this.IMPORTEPACTADO = VL_IMPORTEPACTADO;
            this.MONEDA = VL_MONEDA;
            this.UNIDADESAFECTADAS = VL_UNIDADESAFECTADAS;
        }
    }
}
