﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Proceso
{
    public class BeanGestionDocumentaria
    {

        public int IDCABECERA { get; set; }
        public String IDDOCUMENTO { get; set; }
        public String NRODOCUMENTO { get; set; }
        public String IDBANCOEMPRESA { get; set; }
        public String EMPRCODIGO { get; set; }
        public int DIASRENOVACION { get; set; }
        public String GLOSA { get; set; }


        public BeanGestionDocumentaria()
        { }

        public BeanGestionDocumentaria(int VL_CABECERA,String VL_IDDOCUMENTO, String VL_NRODOCUMENTO, String VL_IDBANCOEMPRESA, String VL_EMPRCODIGO, int VL_DIASRENOVACION, String VL_GLOSA)
        {
            this.IDCABECERA = VL_CABECERA;
            this.IDDOCUMENTO = VL_IDDOCUMENTO;
            this.NRODOCUMENTO = VL_NRODOCUMENTO;
            this.IDBANCOEMPRESA = VL_IDBANCOEMPRESA;
            this.EMPRCODIGO = VL_EMPRCODIGO;
            this.DIASRENOVACION = VL_DIASRENOVACION;
            this.GLOSA = VL_GLOSA;
        }

        public BeanGestionDocumentaria(String VL_IDDOCUMENTO, String VL_NRODOCUMENTO, String VL_IDBANCOEMPRESA, String VL_EMPRCODIGO, int VL_DIASRENOVACION, String VL_GLOSA)
        {
            this.IDDOCUMENTO = VL_IDDOCUMENTO;
            this.NRODOCUMENTO = VL_NRODOCUMENTO;
            this.IDBANCOEMPRESA = VL_IDBANCOEMPRESA;
            this.EMPRCODIGO = VL_EMPRCODIGO;
            this.DIASRENOVACION = VL_DIASRENOVACION;
            this.GLOSA = VL_GLOSA;
        }
    }
}
