﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad
{
    public class BeanRegistroDocumento
    {      
        public string IdRegistro { get; set; }
        public string EmprCodigo { get; set; }
        public string TipoAcreedorId { get; set; }
        public string CodProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public string IdDocumento { get; set; }
        public string Serie { get; set; }
        public string NroDocumento { get; set; }
        public DateTime FechaDocumento { get; set; }
        public string IdMoneda { get; set; }
        public string ValorCompra { get; set; }
        public string ImporteIgv { get; set; }
        public string TasaIgv { get; set; }
        public string ImporteTotal { get; set; }
        public string IdArea { get; set; }
        public string Usuario { get; set; }
        public string PcCreacion { get; set; }
        public string IGV { get; set; }
        public string IdOC { get; set; }
        public string NroOC { get; set; }
        public string CCosCodigo { get; set; }
        public string Periodo { get; set; }
        public string TIngreso { get; set; }
       
     
      
        // select NUMEROS 
        public BeanRegistroDocumento(string EmprCodigo, string CodProveedor, string Serie, string NroDocumento, DateTime FechaDocumento, string ImporteTotal, string IdArea, 
            string Usuario, string PcCreacion,string IdOC, string CCosCodigo, string Periodo)
        {
           this. EmprCodigo = EmprCodigo;
           this.CodProveedor = CodProveedor;
           this.Serie = Serie;
           this.NroDocumento = NroDocumento;
           this.FechaDocumento = FechaDocumento;
           this.ImporteTotal = ImporteTotal;
           this.IdArea = IdArea;
           this.Usuario = Usuario;
           this.PcCreacion = PcCreacion;
           this.IdOC = IdOC;
           this.NroOC = NroOC;
           this.CCosCodigo = CCosCodigo;
           this.Periodo = Periodo;
        }
        public BeanRegistroDocumento()
        {
        }
        public BeanRegistroDocumento(string EmprCodigo,string Periodo)
        {
            this.EmprCodigo = EmprCodigo;
            this.Periodo = Periodo;
        }
       
    }
}
