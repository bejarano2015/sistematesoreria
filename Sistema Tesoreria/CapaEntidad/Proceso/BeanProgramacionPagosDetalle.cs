﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Proceso
{
    public class BeanProgramacionPagosDetalle
    {
        public int ID_PROGPAGO { get; set; }
        public decimal ID_DOC_ORIGEN { get; set; }
        public string SERIE_DOC_ORIGEN { get; set; }
        public string NRO_DOC_ORIGEN { get; set; }
        public string ID_MONEDA { get; set; }
        public decimal IMPORTE { get; set; }
        public int ESTADO { get; set; }
        public string ID_PROPAGO_DET { get; set; }
        public decimal ID_PADRE_D { get; set; }
        public decimal TOTALBRUTO { get; set; }


        public BeanProgramacionPagosDetalle(int V_ID_PROGPAGO, Decimal V_ID_DOC_ORIGEN, String V_SERIE_DOC_ORIGEN, String V_NRO_DOC_ORIGEN,String V_ID_MONEDA,Decimal V_IMPORTE, int V_ESTADO,Decimal VL_TOTALBRUTO )
        {
            ID_PROGPAGO = V_ID_PROGPAGO;
            ID_DOC_ORIGEN = V_ID_DOC_ORIGEN;
            SERIE_DOC_ORIGEN = V_SERIE_DOC_ORIGEN;
            NRO_DOC_ORIGEN = V_NRO_DOC_ORIGEN;
            ID_MONEDA = V_ID_MONEDA;
            IMPORTE = V_IMPORTE;
            ESTADO = V_ESTADO;
            TOTALBRUTO = VL_TOTALBRUTO;    


        }

        public BeanProgramacionPagosDetalle()
        {

        }

        public BeanProgramacionPagosDetalle(int VL_ID_PROGPAGO)
        {
            ID_PROGPAGO = VL_ID_PROGPAGO;
        }

   
        

    }
}
