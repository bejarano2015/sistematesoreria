﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.Proceso
{
    public class BeanOrdenesCompra
    {

        public string OCOCODIGO { get; set; }
        public string PDOCODIGO { get; set; }
        public string OCONUMERO { get; set; }
        public string DERI_ESTADO { get; set; }


        public BeanOrdenesCompra(string V_OCOCODIGO, string V_PDOCODIGO, string V_OCONUMERO, string V_DERI_ESTADO)
        {
            OCOCODIGO = V_OCOCODIGO;
            PDOCODIGO = V_PDOCODIGO;
            OCONUMERO = V_OCONUMERO;
            DERI_ESTADO = V_DERI_ESTADO;


        }

        public BeanOrdenesCompra(string V_OCOCODIGO,  string V_DERI_ESTADO)
        {
            OCOCODIGO = V_OCOCODIGO;
            DERI_ESTADO = V_DERI_ESTADO;


        }

        
    }
}
