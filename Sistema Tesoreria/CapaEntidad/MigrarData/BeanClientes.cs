﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CapaEntidad.MigrarData
{
    public class BeanClientes
    {
        public BeanClientes() { }

         public String CODIGO { get; set; }
         public String TIPO_DOC { get; set; }
         public String UBDS_CODIGO { get; set; }
         public String NOMBRES { get; set; }
         public String APE_PATERNO { get; set; }
         public String APE_MATERNO { get; set; }       
         public String NRO_DOC { get; set; }
         public String SEXO { get; set; }
         public DateTime FECHA_NAC { get; set; }
         public String ESTADO_CIVIL { get; set; }
         public String DIRECCION { get; set; }
         public String REFERENCIA { get; set; }
         public String TELEFONO { get; set; }
         public String EMAIL { get; set; }
         public String OCUPACION { get; set; }
         public String EMPRESA { get; set; }
         public String ID_OBRA { get; set; }

         public BeanClientes(String VL_CODIGO, String VL_TIPO_DOC, String VL_UBDS_CODIGO, String VL_NOMBRES, String VL_APE_PATERNO, String VL_APE_MATERNO, String VL_NRO_DOC, String VL_SEXO, DateTime VL_FECHA_NAC, String VL_ESTADO_CIVIL, String VL_DIRECCION, String VL_REFERENCIA, String VL_TELEFONO, String VL_EMAIL, String VL_OCUPACION, String VL_EMPRESA, String VL_ID_OBRA)
         {
             CODIGO = VL_CODIGO;
             TIPO_DOC = VL_TIPO_DOC;
             UBDS_CODIGO = VL_UBDS_CODIGO;
             NOMBRES = VL_NOMBRES;
             APE_PATERNO = VL_APE_PATERNO;
             APE_MATERNO = VL_APE_MATERNO;          
             NRO_DOC = VL_NRO_DOC;
             SEXO = VL_SEXO;
             FECHA_NAC = VL_FECHA_NAC;
             ESTADO_CIVIL = VL_ESTADO_CIVIL;
             DIRECCION = VL_DIRECCION;
             REFERENCIA = VL_REFERENCIA;
             TELEFONO = VL_TELEFONO;
             EMAIL = VL_EMAIL;
             OCUPACION = VL_OCUPACION;
             EMPRESA = VL_EMPRESA;
             ID_OBRA = VL_ID_OBRA;
         }

         public BeanClientes(String VL_TIPO_DOC, String VL_UBDS_CODIGO, String VL_NOMBRES, String VL_APE_PATERNO, String VL_APE_MATERNO, String VL_NRO_DOC, String VL_SEXO, DateTime VL_FECHA_NAC, String VL_ESTADO_CIVIL, String VL_DIRECCION, String VL_REFERENCIA, String VL_TELEFONO, String VL_EMAIL, String VL_OCUPACION, String VL_EMPRESA, String VL_ID_OBRA)
         {
             TIPO_DOC = VL_TIPO_DOC;
             UBDS_CODIGO = VL_UBDS_CODIGO;
             NOMBRES = VL_NOMBRES;
             APE_PATERNO = VL_APE_PATERNO;
             APE_MATERNO = VL_APE_MATERNO;
             NRO_DOC = VL_NRO_DOC;
             SEXO = VL_SEXO;
             FECHA_NAC = VL_FECHA_NAC;
             ESTADO_CIVIL = VL_ESTADO_CIVIL;
             DIRECCION = VL_DIRECCION;
             REFERENCIA = VL_REFERENCIA;
             TELEFONO = VL_TELEFONO;
             EMAIL = VL_EMAIL;
             OCUPACION = VL_OCUPACION;
             EMPRESA = VL_EMPRESA;
             ID_OBRA = VL_ID_OBRA;
         }

    }
}
