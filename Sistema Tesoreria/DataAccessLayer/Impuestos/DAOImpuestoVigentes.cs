﻿using CapaEntidad;
using CapaEntidad.Impuestos;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Impuestos
{
  public  class DAOImpuestoVigentes
    {

      public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_IMPUESTOS_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_LISTAR_IMPUESTOS_VIGENTES]");

                VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO", DbType.String, OBJ_IMPUESTO_VIGENTE.ID_IMPUESTO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

      public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_IMPUESTOS_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE)
      {
          SqlDatabase VL_Database;
          DbCommand VL_Command;
          DbConnection VL_Connection;
          DbTransaction VL_Transact;
          BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

          VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
          VL_Connection = VL_Database.CreateConnection();
          VL_Connection.Open();
          VL_Transact = VL_Connection.BeginTransaction();


          try
          {
              VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_MANTENIMIENTO_IMPUESTOS_VIGENTES]");

              VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO", DbType.Int16, OBJ_IMPUESTO_VIGENTE.ID_IMPUESTO);
              VL_Database.AddInParameter(VL_Command, "@ES_PORCENTAJE", DbType.Int16, OBJ_IMPUESTO_VIGENTE.ES_PORCENTAJE);
              VL_Database.AddInParameter(VL_Command, "@VALOR_PORCENTAJE", DbType.Decimal, OBJ_IMPUESTO_VIGENTE.VALOR_PORCENTAJE);
              VL_Database.AddInParameter(VL_Command, "@VIG_DESDE", DbType.String, OBJ_IMPUESTO_VIGENTE.VIG_DESDE);
              VL_Database.AddInParameter(VL_Command, "@VIG_HASTA", DbType.String, OBJ_IMPUESTO_VIGENTE.VIG_HASTA);
              VL_Database.AddInParameter(VL_Command, "@VIGENTE", DbType.String, OBJ_IMPUESTO_VIGENTE.VIGENTE);
              VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO_VIGENCIA", DbType.String, "");
              VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.String, OBJ_IMPUESTO_VIGENTE.TIPO);

              VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
              VL_Transact.Commit();
              VL_BeanResultado.blnResultado = true;
              VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


          }
          catch (System.Data.SqlClient.SqlException ex)
          {
              VL_BeanResultado.blnResultado = false;
              VL_BeanResultado.strMensaje = ex.Message.ToString();
              VL_Transact.Rollback();
          }
          finally
          {
              if (VL_Connection.State == ConnectionState.Open)
              {
                  VL_Connection.Close();
              }
              VL_Database = null;
              VL_Command = null;
          }
          return VL_BeanResultado;
      }

      public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_IMPUESTOS_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE)
      {
          SqlDatabase VL_Database;
          DbCommand VL_Command;
          DbConnection VL_Connection;
          DbTransaction VL_Transact;
          BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

          VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
          VL_Connection = VL_Database.CreateConnection();
          VL_Connection.Open();
          VL_Transact = VL_Connection.BeginTransaction();


          try
          {
              VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_MANTENIMIENTO_IMPUESTOS_VIGENTES]");

              VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO", DbType.Int16, OBJ_IMPUESTO_VIGENTE.ID_IMPUESTO);
              VL_Database.AddInParameter(VL_Command, "@ES_PORCENTAJE", DbType.Int16, OBJ_IMPUESTO_VIGENTE.ES_PORCENTAJE);
              VL_Database.AddInParameter(VL_Command, "@VALOR_PORCENTAJE", DbType.Decimal, OBJ_IMPUESTO_VIGENTE.VALOR_PORCENTAJE);
              VL_Database.AddInParameter(VL_Command, "@VIG_DESDE", DbType.String, OBJ_IMPUESTO_VIGENTE.VIG_DESDE);
              VL_Database.AddInParameter(VL_Command, "@VIG_HASTA", DbType.String, OBJ_IMPUESTO_VIGENTE.VIG_HASTA);
              VL_Database.AddInParameter(VL_Command, "@VIGENTE", DbType.String, OBJ_IMPUESTO_VIGENTE.VIGENTE);
              VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO_VIGENCIA", DbType.String, OBJ_IMPUESTO_VIGENTE.ID_IMPUESTO_VIGENCIA);
              VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.String, OBJ_IMPUESTO_VIGENTE.TIPO);

              VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
              VL_Transact.Commit();
              VL_BeanResultado.blnResultado = true;
              VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


          }
          catch (System.Data.SqlClient.SqlException ex)
          {
              VL_BeanResultado.blnResultado = false;
              VL_BeanResultado.strMensaje = ex.Message.ToString();
              VL_Transact.Rollback();
          }
          finally
          {
              if (VL_Connection.State == ConnectionState.Open)
              {
                  VL_Connection.Close();
              }
              VL_Database = null;
              VL_Command = null;
          }
          return VL_BeanResultado;
      }

      public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_IMPUESTO_VIG(BeanImpuesto_Vigente OBJ_IMPUESTO_VIGENTE)
      {
          SqlDatabase VL_Database;
          DbCommand VL_Command;
          DbConnection VL_Connection;
          DbTransaction VL_Transact;
          BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

          VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
          VL_Connection = VL_Database.CreateConnection();
          VL_Connection.Open();
          VL_Transact = VL_Connection.BeginTransaction();

          try
          {
              VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_MANTENIMIENTO_IMPUESTOS_VIGENTES]");

              VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO", DbType.Int16,0);
              VL_Database.AddInParameter(VL_Command, "@ES_PORCENTAJE", DbType.Int16,0);
              VL_Database.AddInParameter(VL_Command, "@VALOR_PORCENTAJE", DbType.Decimal,0);
              VL_Database.AddInParameter(VL_Command, "@VIG_DESDE", DbType.String, "");
              VL_Database.AddInParameter(VL_Command, "@VIG_HASTA", DbType.String, "");
              VL_Database.AddInParameter(VL_Command, "@VIGENTE", DbType.String, "");

              VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO_VIGENCIA", DbType.Int16, OBJ_IMPUESTO_VIGENTE.ID_IMPUESTO_VIGENCIA);
              VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.String, OBJ_IMPUESTO_VIGENTE.TIPO);

              VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
              VL_Transact.Commit();
              VL_BeanResultado.blnResultado = true;
              VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


          }
          catch (System.Data.SqlClient.SqlException ex)
          {
              VL_BeanResultado.blnResultado = false;
              VL_BeanResultado.strMensaje = ex.Message.ToString();
              VL_Transact.Rollback();
          }
          finally
          {
              if (VL_Connection.State == ConnectionState.Open)
              {
                  VL_Connection.Close();
              }
              VL_Database = null;
              VL_Command = null;
          }
          return VL_BeanResultado;
      }


    }
}
