﻿using CapaEntidad;
using CapaEntidad.Ventas;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Ventas
{
     public class DAOVentas
     {
         public DAOVentas() { }


         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_MONEDAS()
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_MONEDAS]");


                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_COSTOS(String EMPRESA_CODIDO)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             
             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_CCOSTOS]");

                 VL_Database.AddInParameter(VL_Command, "@EMPRESA_CODIGO", DbType.String,EMPRESA_CODIDO);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DOCUMENTOS(String DOCUMENTO)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_LISTAR_DOCUMENTOS]");
                 VL_Database.AddInParameter(VL_Command, "@ORIGEN", DbType.String, DOCUMENTO);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_SERIE(int ID_DOCUMENTO)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_DOCUEMNTOS_SERIE]");
                 VL_Database.AddInParameter(VL_Command, "@ID_Documento", DbType.Int32, ID_DOCUMENTO);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CLIENTES(string id_obra)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_CLIENTES]");
                 VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, id_obra);


                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_FORMA_PAGO()
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_FORMA_DE_PAGO]");


                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_OBRA()
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_OBRA]");


                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_VENDEDOR()
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_VENDEDOR]");


                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }
         
         //public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_VENTAS_LINEAS()
         //{
         //    SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
         //    BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

         //    DbCommand VL_Command;

         //    try
         //    {
         //        VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_VENTAS_LINEA]");


         //        VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
         //        VL_BeanResultado.blnExiste = true;
         //    }
         //    catch (System.Data.SqlClient.SqlException ex)
         //    {
         //        VL_BeanResultado.blnExiste = false;
         //        VL_BeanResultado.strMensaje = ex.Message.ToString();
         //    }
         //    finally
         //    {

         //        VL_Database = null;
         //        VL_Command = null;
         //    }
         //    return VL_BeanResultado;
         //}

         // public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_ITEM_VENTAS()
         //{
         //    SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
         //    BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

         //    DbCommand VL_Command;

         //    try
         //    {
         //        VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_ITEM_VENTA]");


         //        VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
         //        VL_BeanResultado.blnExiste = true;
         //    }
         //    catch (System.Data.SqlClient.SqlException ex)
         //    {
         //        VL_BeanResultado.blnExiste = false;
         //        VL_BeanResultado.strMensaje = ex.Message.ToString();
         //    }
         //    finally
         //    {

         //        VL_Database = null;
         //        VL_Command = null;
         //    }
         //    return VL_BeanResultado;
         //}

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CLIENTES_DETALLE(string ID_CLIENTE, string ID_OBRA)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_CLIENTES_DETALLE]");

                 VL_Database.AddInParameter(VL_Command, "@CLIE_CODIGO", DbType.String, ID_CLIENTE);
                 VL_Database.AddInParameter(VL_Command, "@OBRA", DbType.String, ID_OBRA);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }
                
         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_VENTAS()
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_VENTAS]");

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_VENTAS(BeanVentas OBJ_VENTAS)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_VENTAS]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DocVenta", DbType.Int16, 0);
                 VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.String, OBJ_VENTAS.FECHA);
                 VL_Database.AddInParameter(VL_Command, "@Estado", DbType.Int16, OBJ_VENTAS.ESTADO);
                 VL_Database.AddInParameter(VL_Command, "@ID_Documento", DbType.Int16, OBJ_VENTAS.ID_DOCUMENTO);
                 VL_Database.AddInParameter(VL_Command, "@Serie", DbType.String, OBJ_VENTAS.SERIE);
                 VL_Database.AddInParameter(VL_Command, "@Numero", DbType.String, OBJ_VENTAS.NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@ID_Cliente", DbType.String, OBJ_VENTAS.ID_CLIENTE);
                 VL_Database.AddInParameter(VL_Command, "@ClienteDireccion", DbType.String, OBJ_VENTAS.CLIENTE_DIRECCION);
                 VL_Database.AddInParameter(VL_Command, "@ID_Moneda", DbType.String, OBJ_VENTAS.ID_MONEDA);
                 VL_Database.AddInParameter(VL_Command, "@ID_Vendedor", DbType.Int16, OBJ_VENTAS.ID_VENDEDOR);
                 VL_Database.AddInParameter(VL_Command, "@Neto", DbType.Decimal, OBJ_VENTAS.NETO);
                 VL_Database.AddInParameter(VL_Command, "@Dcto", DbType.Decimal, OBJ_VENTAS.DESCUENTO);
                 VL_Database.AddInParameter(VL_Command, "@SubTotal", DbType.Decimal, OBJ_VENTAS.SUB_TOTAL);
                 VL_Database.AddInParameter(VL_Command, "@Impuestos", DbType.Decimal, OBJ_VENTAS.IMPUESTO);
                 VL_Database.AddInParameter(VL_Command, "@Total", DbType.Decimal, OBJ_VENTAS.TOTAL);
                 VL_Database.AddInParameter(VL_Command, "@Observaciones", DbType.String, OBJ_VENTAS.OBSERVACIONES);
                 VL_Database.AddInParameter(VL_Command, "@Id_Pago", DbType.Int16, OBJ_VENTAS.ID_PAGO);
                 VL_Database.AddInParameter(VL_Command, "@Id_Obra", DbType.Int16, OBJ_VENTAS.ID_OBRA);
                 VL_Database.AddInParameter(VL_Command, "@Id_CCosto", DbType.String, OBJ_VENTAS.ID_COSTO);
                 VL_Database.AddInParameter(VL_Command, "@Asiento", DbType.Int16, OBJ_VENTAS.ASIENTO);
                 VL_Database.AddInParameter(VL_Command, "@AsientoOK", DbType.String, OBJ_VENTAS.ASIENTO_OK);
                 VL_Database.AddInParameter(VL_Command, "@AsientoMsg", DbType.String, OBJ_VENTAS.ASIENTO_MSG);
                 VL_Database.AddInParameter(VL_Command, "@Emitida", DbType.String, OBJ_VENTAS.EMITIDA);
                 VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.String, OBJ_VENTAS.ID_USUARIO);
                 VL_Database.AddInParameter(VL_Command, "@expe_codigo", DbType.String, "");

                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_VENTAS.TIPO);

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_VENTAS(BeanVentas OBJ_VENTAS)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_VENTAS]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DocVenta", DbType.Int16, OBJ_VENTAS.ID_DOC_VENTA);
                 VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.String, OBJ_VENTAS.FECHA);
                 VL_Database.AddInParameter(VL_Command, "@Estado", DbType.Int16, OBJ_VENTAS.ESTADO);
                 VL_Database.AddInParameter(VL_Command, "@ID_Documento", DbType.Int16, OBJ_VENTAS.ID_DOCUMENTO);
                 VL_Database.AddInParameter(VL_Command, "@Serie", DbType.String, OBJ_VENTAS.SERIE);
                 VL_Database.AddInParameter(VL_Command, "@Numero", DbType.String, OBJ_VENTAS.NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@ID_Cliente", DbType.String, OBJ_VENTAS.ID_CLIENTE);
                 VL_Database.AddInParameter(VL_Command, "@ClienteDireccion", DbType.String, OBJ_VENTAS.CLIENTE_DIRECCION);
                 VL_Database.AddInParameter(VL_Command, "@ID_Moneda", DbType.String, OBJ_VENTAS.ID_MONEDA);
                 VL_Database.AddInParameter(VL_Command, "@ID_Vendedor", DbType.Int16, OBJ_VENTAS.ID_VENDEDOR);
                 VL_Database.AddInParameter(VL_Command, "@Neto", DbType.Decimal, OBJ_VENTAS.NETO);
                 VL_Database.AddInParameter(VL_Command, "@Dcto", DbType.Decimal, OBJ_VENTAS.DESCUENTO);
                 VL_Database.AddInParameter(VL_Command, "@SubTotal", DbType.Decimal, OBJ_VENTAS.SUB_TOTAL);
                 VL_Database.AddInParameter(VL_Command, "@Impuestos", DbType.Decimal, OBJ_VENTAS.IMPUESTO);
                 VL_Database.AddInParameter(VL_Command, "@Total", DbType.Decimal, OBJ_VENTAS.TOTAL);
                 VL_Database.AddInParameter(VL_Command, "@Observaciones", DbType.String, OBJ_VENTAS.OBSERVACIONES);
                 VL_Database.AddInParameter(VL_Command, "@Id_Pago", DbType.Int16, OBJ_VENTAS.ID_PAGO);
                 VL_Database.AddInParameter(VL_Command, "@Id_Obra", DbType.Int16, OBJ_VENTAS.ID_OBRA);
                 VL_Database.AddInParameter(VL_Command, "@Id_CCosto", DbType.String, OBJ_VENTAS.ID_COSTO);
                 VL_Database.AddInParameter(VL_Command, "@Asiento", DbType.String, OBJ_VENTAS.ASIENTO);
                 VL_Database.AddInParameter(VL_Command, "@AsientoOK", DbType.String, OBJ_VENTAS.ASIENTO_OK);
                 VL_Database.AddInParameter(VL_Command, "@AsientoMsg", DbType.String, OBJ_VENTAS.ASIENTO_MSG);
                 VL_Database.AddInParameter(VL_Command, "@Emitida", DbType.String, OBJ_VENTAS.EMITIDA);
                 VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.String, OBJ_VENTAS.ID_USUARIO);
                 VL_Database.AddInParameter(VL_Command, "@expe_codigo", DbType.String, "");

                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_VENTAS.TIPO);

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_VENTAS(int ID_DOC, int TIPO)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_VENTAS]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DocVenta", DbType.Int16, ID_DOC);
                 VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@Estado", DbType.Int16, 0);
                 VL_Database.AddInParameter(VL_Command, "@ID_Documento", DbType.Int16, 0);
                 VL_Database.AddInParameter(VL_Command, "@Serie", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@Numero", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@ID_Cliente", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@ClienteDireccion", DbType.String,"");
                 VL_Database.AddInParameter(VL_Command, "@ID_Moneda", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@ID_Vendedor", DbType.Int16, 0);
                 VL_Database.AddInParameter(VL_Command, "@Neto", DbType.Decimal, 0);
                 VL_Database.AddInParameter(VL_Command, "@Dcto", DbType.Decimal, 0);
                 VL_Database.AddInParameter(VL_Command, "@SubTotal", DbType.Decimal, 0);
                 VL_Database.AddInParameter(VL_Command, "@Impuestos", DbType.Decimal, 0);
                 VL_Database.AddInParameter(VL_Command, "@Total", DbType.Decimal, 0);
                 VL_Database.AddInParameter(VL_Command, "@Observaciones", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@Id_Pago", DbType.Int16, 0);
                 VL_Database.AddInParameter(VL_Command, "@Id_Obra", DbType.Int16, 0);
                 VL_Database.AddInParameter(VL_Command, "@Id_CCosto", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@Asiento", DbType.String,"");
                 VL_Database.AddInParameter(VL_Command, "@AsientoOK", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@AsientoMsg", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@Emitida", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.String, "");
                 VL_Database.AddInParameter(VL_Command, "@expe_codigo", DbType.String, "");

                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, TIPO);

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_VENTAS_NUMERADOR(int ID_DOC_VENTA, String SERIE, int ID_DOCUMENTO)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_NUMERADOR]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int32, ID_DOC_VENTA);
                 VL_Database.AddInParameter(VL_Command, "@SERIE", DbType.String, SERIE);
                 VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.Int32, ID_DOCUMENTO);
          

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }


         //public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_NUMERADOR(int ID_DOCUMENTO, String SERIE, ref String NUMERO)
         //{
         //    SqlDatabase VL_Database;
         //    DbCommand VL_Command;
         //    DbConnection VL_Connection;
         //    DbTransaction VL_Transact;
         //    BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

         //    VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
         //    VL_Connection = VL_Database.CreateConnection();
         //    VL_Connection.Open();
         //    VL_Transact = VL_Connection.BeginTransaction();


         //    try
         //    {
         //        VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_NUMERADOR]");

         //        VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.Int16, ID_DOCUMENTO);
         //        VL_Database.AddInParameter(VL_Command, "@SERIE", DbType.String, SERIE);
         //        VL_Database.AddInParameter(VL_Command, "@NUMERO", DbType.String, NUMERO);
      
         //          NUMERO = (String)VL_Database.GetParameterValue(VL_Command, "@NUMERO");

         //        VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
         //        VL_Transact.Commit();
         //        VL_BeanResultado.blnResultado = true;
         //        VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

         //        NUMERO = (String)VL_Database.GetParameterValue(VL_Command, "@NUMERO");

         //    }
         //    catch (System.Data.SqlClient.SqlException ex)
         //    {
         //        VL_BeanResultado.blnResultado = false;
         //        VL_BeanResultado.strMensaje = ex.Message.ToString();
         //        VL_Transact.Rollback();
         //    }
         //    finally
         //    {
         //        if (VL_Connection.State == ConnectionState.Open)
         //        {
         //            VL_Connection.Close();
         //        }
         //        VL_Database = null;
         //        VL_Command = null;
         //    }
         //    return VL_BeanResultado;
         //}
         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ANULAR_DOC_VENTA(int ID_DOC_VENTA)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_ANULAR_DOC]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int32, ID_DOC_VENTA);
                
              

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

               }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ANULAR_REIMPRIMIR(int ID_DOC_VENTA, ref int ID_NUEVO_DOC)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_ANULAR_Y_REIMPRIMIR]");
                 VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int32, ID_DOC_VENTA);

                 VL_Database.AddOutParameter(VL_Command, "@ID_DOCVENTA", DbType.Int32, 10); //output

                 //ID_NUEVO_DOC = (Int32)VL_Database.GetParameterValue(VL_Command, "@ID_DOCVENTA"); //se 

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

                 ID_NUEVO_DOC = (Int32)VL_Database.GetParameterValue(VL_Command, "@ID_DOCVENTA"); 
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         #region "SERIE NUMERADOR"
         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DOCU_NUMERADOR(string ID_DOCU)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_LISTAR_MANTE_NUMERADOR]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.String, ID_DOCU);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_NUMERADOR_CONT(String ID_DOC, String SERIE, string NUMERO, String USUARIO,int TIPO)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_NUMERADOR_CONTA]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DOC_NUMERO", DbType.Int16, 0);
                 VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.String, ID_DOC);
                 VL_Database.AddInParameter(VL_Command, "@SERIE", DbType.String, SERIE);
                 VL_Database.AddInParameter(VL_Command, "@NUMERO", DbType.String, NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@USUARIO", DbType.String, USUARIO);
                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int32, TIPO);


                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_NUMERADOR_CONT(int ID_DOC_NUM, String ID_DOC, String SERIE, string NUMERO, String USUARIO, int TIPO)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_NUMERADOR_CONTA]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DOC_NUMERO", DbType.Int16, ID_DOC_NUM);
                 VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.String, ID_DOC);
                 VL_Database.AddInParameter(VL_Command, "@SERIE", DbType.String, SERIE);
                 VL_Database.AddInParameter(VL_Command, "@NUMERO", DbType.String, NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@USUARIO", DbType.String, USUARIO);
                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int32, TIPO);


                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }


         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_NUMERADOR_CONT(int ID_DOC_NUM, String ID_DOC, String SERIE, string NUMERO, string USUARIO, int TIPO)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_NUMERADOR_CONTA]");

                 VL_Database.AddInParameter(VL_Command, "@ID_DOC_NUMERO", DbType.Int16, ID_DOC_NUM);
                 VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.String, ID_DOC);
                 VL_Database.AddInParameter(VL_Command, "@SERIE", DbType.String, SERIE);
                 VL_Database.AddInParameter(VL_Command, "@NUMERO", DbType.String, NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@USUARIO", DbType.String, USUARIO);
                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int32, TIPO);


                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }
#endregion
     }
}
