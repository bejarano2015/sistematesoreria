﻿using CapaEntidad;
using CapaEntidad.Ventas;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Ventas
{
    public class DAODocVentaImpuesto
    {
        public DAODocVentaImpuesto() { }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DOC_VENTA(int ID_DOC_VENTA)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[LISTAR_DOC_VENTAIMPUESTO]");

                VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int16, ID_DOC_VENTA);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_DOC_VENTAS(BeanDocVentasImpuesto OBJ_VENTAS)
       {
           SqlDatabase VL_Database;
           DbCommand VL_Command;
           DbConnection VL_Connection;
           DbTransaction VL_Transact;
           BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

           VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           VL_Connection = VL_Database.CreateConnection();
           VL_Connection.Open();
           VL_Transact = VL_Connection.BeginTransaction();

           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[MANTENIMIENTO_DOC_VENTAIMPUESTO]");
               VL_Database.AddInParameter(VL_Command, "@ID_DOCU", DbType.Int16, OBJ_VENTAS.ID_DOCU);
               VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO_VIGENTE", DbType.Int16, OBJ_VENTAS.ID_IMPUESTO_VIGENCIA);
               VL_Database.AddInParameter(VL_Command, "@BASE_IMPONIBLE", DbType.Decimal, OBJ_VENTAS.BASE_IMPONIBLE);
               VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, OBJ_VENTAS.IMPORTE);
                     
               //VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_VENTAS.TIPO);

               VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
               VL_Transact.Commit();
               VL_BeanResultado.blnResultado = true;
               VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente....!";

           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnResultado = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
               VL_Transact.Rollback();
           }
           finally
           {
               if (VL_Connection.State == ConnectionState.Open)
               {
                   VL_Connection.Close();
               }
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }

        //public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_DOC_VENTAS(BeanDocVentasImpuesto OBJ_VENTAS)
        //{
        //    SqlDatabase VL_Database;
        //    DbCommand VL_Command;
        //    DbConnection VL_Connection;
        //    DbTransaction VL_Transact;
        //    BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

        //    VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
        //    VL_Connection = VL_Database.CreateConnection();
        //    VL_Connection.Open();
        //    VL_Transact = VL_Connection.BeginTransaction();


        //    try
        //    {
        //        VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[MANTENIMIENTO_DOC_VENTAIMPUESTO]");

        //        VL_Database.AddInParameter(VL_Command, "@ID_DOCU", DbType.Int16, OBJ_VENTAS.ID_DOCU);
        //        VL_Database.AddInParameter(VL_Command, "@ID_IMPUESTO_VIGENTE", DbType.Int16, OBJ_VENTAS.ID_IMPUESTO_VIGENCIA);
        //        VL_Database.AddInParameter(VL_Command, "@BASE_IMPONIBLE", DbType.Decimal, OBJ_VENTAS.BASE_IMPONIBLE);
        //        VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, OBJ_VENTAS.IMPORTE);

        //        VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_VENTAS.TIPO);

        //        VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
        //        VL_Transact.Commit();
        //        VL_BeanResultado.blnResultado = true;
        //        VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        VL_BeanResultado.blnResultado = false;
        //        VL_BeanResultado.strMensaje = ex.Message.ToString();
        //        VL_Transact.Rollback();
        //    }
        //    finally
        //    {
        //        if (VL_Connection.State == ConnectionState.Open)
        //        {
        //            VL_Connection.Close();
        //        }
        //        VL_Database = null;
        //        VL_Command = null;
        //    }
        //    return VL_BeanResultado;
        //}


        //public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_DOC_VENTAS(int ID_DOC, int TIPO)
        //{
        //    SqlDatabase VL_Database;
        //    DbCommand VL_Command;
        //    DbConnection VL_Connection;
        //    DbTransaction VL_Transact;
        //    BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

        //    VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
        //    VL_Connection = VL_Database.CreateConnection();
        //    VL_Connection.Open();
        //    VL_Transact = VL_Connection.BeginTransaction();

        //    try
        //    {
        //        VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[MANTENIMIENTO_DOC_VENTAIMPUESTO]");

        //        VL_Database.AddInParameter(VL_Command, "@ID_DOCU", DbType.Int16, ID_DOC);
           
        //        VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, TIPO);

        //        VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
        //        VL_Transact.Commit();
        //        VL_BeanResultado.blnResultado = true;
        //        VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        VL_BeanResultado.blnResultado = false;
        //        VL_BeanResultado.strMensaje = ex.Message.ToString();
        //        VL_Transact.Rollback();
        //    }
        //    finally
        //    {
        //        if (VL_Connection.State == ConnectionState.Open)
        //        {
        //            VL_Connection.Close();
        //        }
        //        VL_Database = null;
        //        VL_Command = null;
        //    }
        //    return VL_BeanResultado;
        //}
  
   }
    
    
}
