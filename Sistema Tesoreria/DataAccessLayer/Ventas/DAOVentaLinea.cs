﻿using CapaEntidad;
using CapaEntidad.Ventas;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Ventas
{
   public class DAOVentaLinea
    {
       public DAOVentaLinea() { }

       public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_VENTAS_LINEAS(int ID_VENTA)
       {
           SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

           DbCommand VL_Command;

           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_VENTAS_LINEA]");

               VL_Database.AddInParameter(VL_Command, "@ID_DocVenta", DbType.Int16, ID_VENTA);

               VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
               VL_BeanResultado.blnExiste = true;
           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnExiste = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
           }
           finally
           {
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }

       public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_ITEM_VENTAS()
       {
           SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

           DbCommand VL_Command;

           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_ITEM_VENTA]");


               VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
               VL_BeanResultado.blnExiste = true;
           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnExiste = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
           }
           finally
           {
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }


       public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_VENTAS_LINEA(BeanVentaLinea OBJ_VENTAS)
       {
           SqlDatabase VL_Database;
           DbCommand VL_Command;
           DbConnection VL_Connection;
           DbTransaction VL_Transact;
           BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

           VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           VL_Connection = VL_Database.CreateConnection();
           VL_Connection.Open();
           VL_Transact = VL_Connection.BeginTransaction();


           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_VENTAS_LINEA]");

               VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA_LINEA", DbType.Int16, 0);
               VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int16, OBJ_VENTAS.ID_DOC_VENTA);
               VL_Database.AddInParameter(VL_Command, "@ID_ITEM_VENTA", DbType.String, OBJ_VENTAS.ID_ITEM_VENTA);
               VL_Database.AddInParameter(VL_Command, "@CANTIDAD", DbType.Decimal, OBJ_VENTAS.CANTIDAD);
               VL_Database.AddInParameter(VL_Command, "@PRECIO", DbType.Decimal, OBJ_VENTAS.PRECIO);
               VL_Database.AddInParameter(VL_Command, "@DESCUENTO", DbType.Decimal, OBJ_VENTAS.DESCUENTO);
               VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, OBJ_VENTAS.IMPORTE);
               VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, OBJ_VENTAS.ID_OBRA);
               VL_Database.AddInParameter(VL_Command, "@ID_COSTO", DbType.String, OBJ_VENTAS.ID_COSTO);
               VL_Database.AddInParameter(VL_Command, "@GLOSA", DbType.String, OBJ_VENTAS.GLOSA);
              
               VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_VENTAS.TIPO);

               VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
               VL_Transact.Commit();
               VL_BeanResultado.blnResultado = true;
               VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente....!";

           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnResultado = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
               VL_Transact.Rollback();
           }
           finally
           {
               if (VL_Connection.State == ConnectionState.Open)
               {
                   VL_Connection.Close();
               }
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_VENTAS_LINEA(BeanVentaLinea OBJ_VENTAS)
       {
           SqlDatabase VL_Database;
           DbCommand VL_Command;
           DbConnection VL_Connection;
           DbTransaction VL_Transact;
           BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

           VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           VL_Connection = VL_Database.CreateConnection();
           VL_Connection.Open();
           VL_Transact = VL_Connection.BeginTransaction();


           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_VENTAS_LINEA]");

               VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA_LINEA", DbType.Int16, OBJ_VENTAS.ID_DOC_VENTA_LINEA);
               VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int16, OBJ_VENTAS.ID_DOC_VENTA);
               VL_Database.AddInParameter(VL_Command, "@ID_ITEM_VENTA", DbType.String, OBJ_VENTAS.ID_ITEM_VENTA);
               VL_Database.AddInParameter(VL_Command, "@CANTIDAD", DbType.Decimal, OBJ_VENTAS.CANTIDAD);
               VL_Database.AddInParameter(VL_Command, "@PRECIO", DbType.Decimal, OBJ_VENTAS.PRECIO);
               VL_Database.AddInParameter(VL_Command, "@DESCUENTO", DbType.Decimal, OBJ_VENTAS.DESCUENTO);
               VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, OBJ_VENTAS.IMPORTE);
               VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, OBJ_VENTAS.ID_OBRA);
               VL_Database.AddInParameter(VL_Command, "@ID_COSTO", DbType.String, OBJ_VENTAS.ID_COSTO);
               VL_Database.AddInParameter(VL_Command, "@GLOSA", DbType.String, OBJ_VENTAS.GLOSA);

               VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_VENTAS.TIPO);

               VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
               VL_Transact.Commit();
               VL_BeanResultado.blnResultado = true;
               VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnResultado = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
               VL_Transact.Rollback();
           }
           finally
           {
               if (VL_Connection.State == ConnectionState.Open)
               {
                   VL_Connection.Close();
               }
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_VENTAS_LINEA(BeanVentaLinea OBJ_VENTAS)
       {
           SqlDatabase VL_Database;
           DbCommand VL_Command;
           DbConnection VL_Connection;
           DbTransaction VL_Transact;
           BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

           VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           VL_Connection = VL_Database.CreateConnection();
           VL_Connection.Open();
           VL_Transact = VL_Connection.BeginTransaction();

           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_VENTAS_LINEA]");

               VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA_LINEA", DbType.Int16, OBJ_VENTAS.ID_DOC_VENTA_LINEA);
               VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int16, OBJ_VENTAS.ID_DOC_VENTA);
               VL_Database.AddInParameter(VL_Command, "@ID_ITEM_VENTA", DbType.String, OBJ_VENTAS.ID_ITEM_VENTA);
               VL_Database.AddInParameter(VL_Command, "@CANTIDAD", DbType.Decimal, OBJ_VENTAS.CANTIDAD);
               VL_Database.AddInParameter(VL_Command, "@PRECIO", DbType.Decimal, OBJ_VENTAS.PRECIO);
               VL_Database.AddInParameter(VL_Command, "@DESCUENTO", DbType.Decimal, OBJ_VENTAS.DESCUENTO);
               VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, OBJ_VENTAS.IMPORTE);
               VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, OBJ_VENTAS.ID_OBRA);
               VL_Database.AddInParameter(VL_Command, "@ID_COSTO", DbType.String, OBJ_VENTAS.ID_COSTO);
               VL_Database.AddInParameter(VL_Command, "@GLOSA", DbType.String, OBJ_VENTAS.GLOSA);

               VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_VENTAS.TIPO);

               VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
               VL_Transact.Commit();
               VL_BeanResultado.blnResultado = true;
               VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnResultado = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
               VL_Transact.Rollback();
           }
           finally
           {
               if (VL_Connection.State == ConnectionState.Open)
               {
                   VL_Connection.Close();
               }
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }
  
   }
}
