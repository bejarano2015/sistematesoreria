﻿using CapaEntidad;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Ventas.Reportes
{
    public class DAONotaCredito
    {
        public DAONotaCredito() { }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_NotaCredito(int ID_DOC_VENTA)
        {

            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_CONSULTA_PAGOSXMES_EXPEDIENTE]");
                VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int32, ID_DOC_VENTA);


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;
        }
    }
}
