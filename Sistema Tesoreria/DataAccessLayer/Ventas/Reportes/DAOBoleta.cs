﻿using CapaEntidad;
using CapaEntidad.Ventas.Reportes;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Ventas.Reportes
{
    public class DAOBoleta
    {
        DataAccessLayer.Globales.DAOGlobal VL_VarGlobal = new DataAccessLayer.Globales.DAOGlobal();

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_boltaxmes(string vl_año, string vl_periodo, string vl_fecinicio, string vl_fecfin, string vl_filtro)
        {

            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_CONSULTA_PAGOSXMES]");
                VL_Database.AddInParameter(VL_Command, "@OBRA_CODIGO", DbType.String, "0001");
                VL_Database.AddInParameter(VL_Command, "@AÑO", DbType.String, vl_año);
                VL_Database.AddInParameter(VL_Command, "@PERIODO", DbType.String, vl_periodo);
                VL_Database.AddInParameter(VL_Command, "@FINICIO", DbType.String, vl_fecinicio);
                VL_Database.AddInParameter(VL_Command, "@FFINAL", DbType.String, vl_fecfin);
                VL_Database.AddInParameter(VL_Command, "@FILTRO", DbType.String, vl_filtro);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_boltaxmes_expediente(int ID_DOC_VENTA)
        {

            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_CONSULTA_PAGOSXMES_EXPEDIENTE]");
                VL_Database.AddInParameter(VL_Command, "@ID_DOC_VENTA", DbType.Int32, ID_DOC_VENTA);
       

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;
        }

        //public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_boltaxmes_expediente_Imprimir(string vl_año, string vl_periodo, string vl_expe_codigo, string vl_fecinicio, string vl_fecfin, string vl_filtro)
        //{

        //    SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
        //    BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

        //    DbCommand VL_Command;

        //    try
        //    {

        //        VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_CONSULTA_PAGOSXMESIMPRIMIR]");
        //        VL_Database.AddInParameter(VL_Command, "@OBRA_CODIGO", DbType.String, "0001");
        //        VL_Database.AddInParameter(VL_Command, "@AÑO", DbType.String, vl_año);
        //        VL_Database.AddInParameter(VL_Command, "@PERIODO", DbType.String, vl_periodo);
        //        VL_Database.AddInParameter(VL_Command, "@FINICIO", DbType.String, vl_fecinicio);
        //        VL_Database.AddInParameter(VL_Command, "@FFINAL", DbType.String, vl_fecfin);
        //        VL_Database.AddInParameter(VL_Command, "@FILTRO", DbType.String, vl_filtro);

        //        VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
        //        VL_BeanResultado.blnExiste = true;
        //    }
        //    catch (System.Data.SqlClient.SqlException ex)
        //    {
        //        VL_BeanResultado.blnExiste = false;
        //        VL_BeanResultado.strMensaje = ex.Message.ToString();
        //    }
        //    finally
        //    {
        //        VL_Database = null;
        //        VL_Command = null;
        //    }

        //    return VL_BeanResultado;

        //}


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_Boleta(BeanBoleta objBoleta)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("INMOBILIARIO.[USP_PROCESO_BOLETA_INSERTAR]");

                VL_Database.AddInParameter(VL_Command, "@EXPE_CODIGO", DbType.String, objBoleta.EXPE_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@BOLE_NUMERO", DbType.String, objBoleta.BOLE_NUMERO);
                VL_Database.AddInParameter(VL_Command, "@BOLE_SERIE", DbType.String, objBoleta.BOLE_SERIE);
                VL_Database.AddInParameter(VL_Command, "@BOLE_FECHA", DbType.Date, objBoleta.BOLE_FECHA);
                VL_Database.AddInParameter(VL_Command, "@BOLE_MONTO", DbType.String, objBoleta.BOLE_MONTO);
                VL_Database.AddInParameter(VL_Command, "@BOLE_CODIGOPAGO", DbType.String, objBoleta.BOLE_CODIGOPAGO);



                //VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }


    }
}
