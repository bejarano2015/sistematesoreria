﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaEntidad;
using BusinessEntityLayer.Login;

namespace DataAccessLayer.Globales
{
    public class DAOGlobal
    {
        public static String VG_CONECTION_STRING="";

        public CapaEntidad.BeanResultado.ResultadoSeleccion Bdinicializar(BeanLogin objLogin)
        {
            StringBuilder conn = new StringBuilder();
            conn.Append("Data Source=192.168.1.200;Initial Catalog=ERPDHMONT;Persist Security Info=True;");
            //conn.Append("Data Source=(localhost);Initial Catalog=ERPDHMONT;Persist Security Info=True;");
            conn.Append("User ID=" + objLogin.USUA_NICKNAME + ";");
            conn.Append("Password=" + objLogin.USUA_PASSWORD);

            SqlDatabase VL_Database = new SqlDatabase(conn.ToString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_USUARIO_INFORMACION]");

                VL_Database.AddInParameter(VL_Command, "@USUA_NICKNAME", DbType.String, objLogin.USUA_NICKNAME);
                VL_Database.AddInParameter(VL_Command, "@USUA_PASSWORD", DbType.String, objLogin.USUA_PASSWORD);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = "EL USUARIO O LA CONTRASEÑA NO COINCIDEN";
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            VG_CONECTION_STRING = conn.ToString();

            return VL_BeanResultado;
                
        }

        public string BdConectionString()
        {
            return VG_CONECTION_STRING;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_EmpresasLogin(String VL_USUA_CODIGO, String VL_SIST_CODIGO) //LISTAR LAS EMPRESAS 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_EMPRESA_LISTAR_LOGIN]");

                VL_Database.AddInParameter(VL_Command, "@USUA_CODIGO", DbType.String, VL_USUA_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@SIST_CODIGO", DbType.String, VL_SIST_CODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Empresas() //LISTAR LAS EMPRESAS 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_EMPRESA_LISTAR]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_ObrasEmpresaLogin(string VL_EMPR_CODIGO, string VL_USUA_CODIGO, string VL_SIST_CODIGO) //LISTAR LAS OBRAS POR EMPRESAS 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_OBRAS_LISTAR_LOGIN]");

                VL_Database.AddInParameter(VL_Command, "@EMPR_CODIGO", DbType.String, VL_EMPR_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@USUA_CODIGO", DbType.String, VL_USUA_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@SIST_CODIGO", DbType.String, VL_SIST_CODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_ObrasEmpresa(string VL_EMPR_CODIGO) //LISTAR LAS OBRAS POR EMPRESAS 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_OBRAS_LISTAR]");

                VL_Database.AddInParameter(VL_Command, "@EMPR_CODIGO", DbType.String, VL_EMPR_CODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoDepartamento() //LISTAR LOS DEPARTAMENTOS PARA EL UBIGEO 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_UBIGEODEPARTAMENTO_LISTAR]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoProvincia(string VL_UBDE_CODIGO) //LISTAR LAS PROVINCIAS PARA EL UBIGEO 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_UBIGEOPROVINCIA_LISTAR]");

                VL_Database.AddInParameter(VL_Command, "@UBDE_CODIGO", DbType.String, VL_UBDE_CODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoDistrito(string VL_UBPR_CODIGO) //LISTAR LAS DISTRITOS PARA EL UBIGEO 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_UBIGEODISTRITO_LISTAR]");

                VL_Database.AddInParameter(VL_Command, "@UBPR_CODIGO", DbType.String, VL_UBPR_CODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UbigeoDistritoCriterio(string VL_UBDE_CODIGO) //LISTAR LAS DISTRITOS PARA EL UBIGEO 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_UBIGEODISTRITO_LISTARCRITERIO]");

                VL_Database.AddInParameter(VL_Command, "@UBDE_CODIGO", DbType.String, VL_UBDE_CODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_TipoDocumento() //LISTAR LOS TIPOS DE DOCUMENTO 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_TIPODOCUMENTO_LISTAR]");
			
                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_TipoPersona() //LISTAR LOS TIPOS DE Persona Vinculo 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_TIPOPERSONA_LISTAR]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Registro_Economico() //LISTAR REGISTRO ECONOMICO 
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_REGIMENECONOMICO_LISTAR]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_Inmueble() //LISTAR ESTADO INMUEBLE
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_MANTENIMIENTO_ESTADOINMUEBLE_LISTAR]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Destino_Pago() //LISTAR ESTADO INMUEBLE
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_MANTENIMIENTO_DESTINOPAGO_FILTRO]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Tipo_Pago() //LISTAR ESTADO INMUEBLE
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_MANTENIMIENTO_TIPOPAGO_FILTRO]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Forma_Pago() //LISTAR ESTADO INMUEBLE
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_MANTENIMIENTO_FORMAPAGO_FILTRO]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Tipo_Liberacion() //LISTAR tipoLIberacion para generar codigo
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_MANTENIMIENTO_TIPOLIBERACION_FILTRO]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_Registral() //LISTAR tipoLIberacion para generar codigo
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_MANTENIMIENTO_ESTADOREGISTRAL_FILTRO]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Usuarios() //LISTAR tipoLIberacion para generar codigo
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[GLOBAL].[USP_GLOBAL_USUARIOS]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        //BORRAR DATOS DE HISTORIAL TOTAL
        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_TRUNCATE_TABLE()
        {

            SqlDatabase VL_Database = new SqlDatabase(BdConectionString());
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_BORRAR_HISTORIAL_BOLETAS]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_Obra(string vl_emprcodigo)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Inventario].[USP_LlenarCombos_Generico]");
                VL_Database.AddInParameter(VL_Command, "@Campos", DbType.String, "ObraCodigo,ObraDescripcion");
                VL_Database.AddInParameter(VL_Command, "@Tabla", DbType.String, "Comun.Obras ");
                VL_Database.AddInParameter(VL_Command, "@FILTRO", DbType.String, "where EmprCodigo='" + vl_emprcodigo + "'");
                VL_Database.AddInParameter(VL_Command, "@Orden", DbType.String, "ObraDescripcion");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_ContratoxObra(string vl_emprcodigo, string vl_ObraCodigo, string vl_co_proveedor)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Inventario].[USP_LlenarCombos_Generico]");
                VL_Database.AddInParameter(VL_Command, "@Campos", DbType.String, " * ");
                VL_Database.AddInParameter(VL_Command, "@Tabla", DbType.String, "Tesoreria.Contrato_Subcontratista ");
                VL_Database.AddInParameter(VL_Command, "@FILTRO", DbType.String, "where Co_Empresa='" + vl_emprcodigo + "'  and Co_Obra='" + vl_ObraCodigo + "' and Co_Proveedor='" + vl_co_proveedor + "'");
                VL_Database.AddInParameter(VL_Command, "@Orden", DbType.String, "Nu_Contrato");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_DatosContrato(string vl_Co_contrato)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Inventario].[USP_LlenarCombos_Generico]");
                VL_Database.AddInParameter(VL_Command, "@Campos", DbType.String, " * ");
                VL_Database.AddInParameter(VL_Command, "@Tabla", DbType.String, " Tesoreria.Contrato_Subcontratista  ");
                VL_Database.AddInParameter(VL_Command, "@FILTRO", DbType.String, "where Co_Contrato='" + vl_Co_contrato + "' ");
                VL_Database.AddInParameter(VL_Command, "@Orden", DbType.String, "Nu_Contrato");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

    }
}
