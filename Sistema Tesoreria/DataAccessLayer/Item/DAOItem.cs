﻿using CapaEntidad;
using CapaEntidad.Item;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Item
{
   public class DAOItem
    {
       public  DAOItem(){}

       public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CUENTAS()
       {
           SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

           DbCommand VL_Command;

           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_CUENTAS]");

               VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
               VL_BeanResultado.blnExiste = true;
           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnExiste = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
           }
           finally
           {

               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }
     
       public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_ITEM()
       {
           SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

           DbCommand VL_Command;

           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTA_ITEMS]");

              
               VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
               VL_BeanResultado.blnExiste = true;
           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnExiste = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
           }
           finally
           {

               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }


       public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_ITEM(BeanItem OBJ_ITEM)
       {
           SqlDatabase VL_Database;
           DbCommand VL_Command;
           DbConnection VL_Connection;
           DbTransaction VL_Transact;
           BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

           VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           VL_Connection = VL_Database.CreateConnection();
           VL_Connection.Open();
           VL_Transact = VL_Connection.BeginTransaction();


           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_ITEMS]");

                VL_Database.AddInParameter(VL_Command, "@ID_ITEM", DbType.Int16, OBJ_ITEM.ID_ITEM);
                VL_Database.AddInParameter(VL_Command, "@ITEM", DbType.String, OBJ_ITEM.ITEM);
                VL_Database.AddInParameter(VL_Command, "@ID_CUENTA", DbType.String, OBJ_ITEM.ID_CUENTA);
                VL_Database.AddInParameter(VL_Command, "@ESTADO", DbType.String, OBJ_ITEM.ESTADO);
                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_ITEM.TIPO);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnResultado = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
               VL_Transact.Rollback();
           }
           finally
           {
               if (VL_Connection.State == ConnectionState.Open)
               {
                   VL_Connection.Close();
               }
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_ITEM(BeanItem OBJ_ITEM)
       {
           SqlDatabase VL_Database;
           DbCommand VL_Command;
           DbConnection VL_Connection;
           DbTransaction VL_Transact;
           BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

           VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           VL_Connection = VL_Database.CreateConnection();
           VL_Connection.Open();
           VL_Transact = VL_Connection.BeginTransaction();


           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_ITEMS]");

               VL_Database.AddInParameter(VL_Command, "@ID_ITEM", DbType.Int16, OBJ_ITEM.ID_ITEM);
               VL_Database.AddInParameter(VL_Command, "@ITEM", DbType.String, OBJ_ITEM.ITEM);
               VL_Database.AddInParameter(VL_Command, "@ID_CUENTA", DbType.String, OBJ_ITEM.ID_CUENTA);
               VL_Database.AddInParameter(VL_Command, "@ESTADO", DbType.String, OBJ_ITEM.ESTADO);
               VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_ITEM.TIPO);

               VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
               VL_Transact.Commit();
               VL_BeanResultado.blnResultado = true;
               VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnResultado = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
               VL_Transact.Rollback();
           }
           finally
           {
               if (VL_Connection.State == ConnectionState.Open)
               {
                   VL_Connection.Close();
               }
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }

       public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_ITEM(BeanItem OBJ_ITEM)
       {
           SqlDatabase VL_Database;
           DbCommand VL_Command;
           DbConnection VL_Connection;
           DbTransaction VL_Transact;
           BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

           VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
           VL_Connection = VL_Database.CreateConnection();
           VL_Connection.Open();
           VL_Transact = VL_Connection.BeginTransaction();

           try
           {
               VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_ITEMS]");

               VL_Database.AddInParameter(VL_Command, "@ID_ITEM", DbType.Int16, OBJ_ITEM.ID_ITEM);
               VL_Database.AddInParameter(VL_Command, "@ITEM", DbType.String, OBJ_ITEM.ITEM);
               VL_Database.AddInParameter(VL_Command, "@ID_CUENTA", DbType.String, OBJ_ITEM.ID_CUENTA);
               VL_Database.AddInParameter(VL_Command, "@ESTADO", DbType.Int16, OBJ_ITEM.ESTADO);
               VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_ITEM.TIPO);

               VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
               VL_Transact.Commit();
               VL_BeanResultado.blnResultado = true;
               VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


           }
           catch (System.Data.SqlClient.SqlException ex)
           {
               VL_BeanResultado.blnResultado = false;
               VL_BeanResultado.strMensaje = ex.Message.ToString();
               VL_Transact.Rollback();
           }
           finally
           {
               if (VL_Connection.State == ConnectionState.Open)
               {
                   VL_Connection.Close();
               }
               VL_Database = null;
               VL_Command = null;
           }
           return VL_BeanResultado;
       }
   
    }
}
