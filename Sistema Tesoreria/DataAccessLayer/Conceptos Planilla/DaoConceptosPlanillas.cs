﻿using CapaEntidad;
using CapaEntidad.Conceptos_Planillas;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Conceptos_Planilla
{
    public class DaoConceptosPlanillas
    {
        public DaoConceptosPlanillas() { }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_EMPRESA()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Inventario].[USP_LISTAR_EMPRESA]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_OBRAS(String ID_EMPRESA)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Inventario].[USP_LISTAR_OBRAS]");
                VL_Database.AddInParameter(VL_Command, "@ID_EMPRESA", DbType.String, ID_EMPRESA);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_PERIODOS(int ID_PERIODO)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_LISTAR_PERIODOS]");
                VL_Database.AddInParameter(VL_Command, "@ID_PERIODO", DbType.Int16, ID_PERIODO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CONCEPTOS_PLANILLA()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_LISTAR_CONCEPTOS_PLANILLA]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_MONEDAS()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_MONEDAS]");


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_PAGOS_PLANILLA()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_LISTAR_PAGO_PLANILLAS]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR(BeanConceptoPlanillas OBJ)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_MANTENIMIENTO_CONCEPTOS]");

                VL_Database.AddInParameter(VL_Command, "@ID_PAGO_PLANILLA", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@ID_CONCEPTO", DbType.Int16, OBJ.ID_CONCEPTO);
                VL_Database.AddInParameter(VL_Command, "@ID_EMPRESA", DbType.String, OBJ.ID_EMPRESA);
                VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, OBJ.ID_OBRA);
                VL_Database.AddInParameter(VL_Command, "@FECHA", DbType.String, OBJ.FECHA);
                VL_Database.AddInParameter(VL_Command, "@ID_MONEDA", DbType.String, OBJ.ID_MONEDA);
                VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, OBJ.IMPORTE);
                VL_Database.AddInParameter(VL_Command, "@GLOSA", DbType.String, OBJ.GLOSA);
                VL_Database.AddInParameter(VL_Command, "@AÑO", DbType.Int16, OBJ.AÑO);
                VL_Database.AddInParameter(VL_Command, "@ID_MES", DbType.Int16, OBJ.ID_MES);
                VL_Database.AddInParameter(VL_Command, "@ID_PERIODO", DbType.Int16, OBJ.ID_PERIODO);
                VL_Database.AddInParameter(VL_Command, "@FLAG", DbType.Int16, OBJ.FLAG);
            
                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 1);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR(BeanConceptoPlanillas OBJ)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_MANTENIMIENTO_CONCEPTOS]");

                VL_Database.AddInParameter(VL_Command, "@ID_PAGO_PLANILLA", DbType.Int16,OBJ.ID_PAGO_PLANILLA);
                VL_Database.AddInParameter(VL_Command, "@ID_CONCEPTO", DbType.Int16, OBJ.ID_CONCEPTO);
                VL_Database.AddInParameter(VL_Command, "@ID_EMPRESA", DbType.String, OBJ.ID_EMPRESA);
                VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, OBJ.ID_OBRA);
                VL_Database.AddInParameter(VL_Command, "@FECHA", DbType.String, OBJ.FECHA);
                VL_Database.AddInParameter(VL_Command, "@ID_MONEDA", DbType.String, OBJ.ID_MONEDA);
                VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, OBJ.IMPORTE);
                VL_Database.AddInParameter(VL_Command, "@GLOSA", DbType.String, OBJ.GLOSA);
                VL_Database.AddInParameter(VL_Command, "@AÑO", DbType.Int16, OBJ.AÑO);
                VL_Database.AddInParameter(VL_Command, "@ID_MES", DbType.Int16, OBJ.ID_MES);
                VL_Database.AddInParameter(VL_Command, "@ID_PERIODO", DbType.Int16, OBJ.ID_PERIODO);
                VL_Database.AddInParameter(VL_Command, "@FLAG", DbType.Int16, OBJ.FLAG);

                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 2);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR(int ID_DOC)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_MANTENIMIENTO_CONCEPTOS]");

                VL_Database.AddInParameter(VL_Command, "@ID_PAGO_PLANILLA", DbType.Int16, ID_DOC);
                VL_Database.AddInParameter(VL_Command, "@ID_CONCEPTO", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@ID_EMPRESA", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@FECHA", DbType.String,"");
                VL_Database.AddInParameter(VL_Command, "@ID_MONEDA", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, 0);
                VL_Database.AddInParameter(VL_Command, "@GLOSA", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@AÑO", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@ID_MES", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@ID_PERIODO", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@FLAG", DbType.Int16, 0);

                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 3);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

   
    }
}
