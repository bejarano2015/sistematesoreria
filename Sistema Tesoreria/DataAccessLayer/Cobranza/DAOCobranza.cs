﻿using CapaEntidad;
using CapaEntidad.Cobranza;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Cobranza
{
     public class DAOCobranza
    {
         public DAOCobranza() { }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_COBRANZA(String ID_EMPRESA, ref Int32 ID_COBRANZA)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_LISTAR_COBRANZAS]");

                 VL_Database.AddInParameter(VL_Command, "@EMPRESA_CODIGO", DbType.String, ID_EMPRESA);
                 VL_Database.AddOutParameter(VL_Command, "@ID_COBRANZA", DbType.Int32, 10);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;

                 ID_COBRANZA = (Int32)VL_Database.GetParameterValue(VL_Command, "@ID_COBRANZA");
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_MONEDAS()
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_MONEDAS]");


                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CLIENTES(String ID_OBRA)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_CLIENTES]");
                 VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, ID_OBRA);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CLIENTES_DETALLE(string ID_CLIENTE,String ID_OBRA)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[COMUN].[USP_LISTAR_CLIENTES_DETALLE]");
                 VL_Database.AddInParameter(VL_Command, "@CLIE_CODIGO", DbType.String, ID_CLIENTE);
                 VL_Database.AddInParameter(VL_Command, "@OBRA", DbType.String, ID_OBRA);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }
               
         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_BANCOS(String ID_EMPRESA)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_LISTAR_BANCOS]");

                 VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, ID_EMPRESA);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CUENTAS_BANCOS(String ID_EMPRESA, String ID_BANCO)
         {
             SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

             DbCommand VL_Command;

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_LISTAR_CUENTAS_BANCOS]");

                 VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, ID_EMPRESA);
                 VL_Database.AddInParameter(VL_Command, "@IdBanco", DbType.String, ID_BANCO);

                 VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                 VL_BeanResultado.blnExiste = true;
             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnExiste = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
             }
             finally
             {

                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_COBRANZA(BeanCobranza OBJ_COBRANZA)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();

             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_MANTENIMIENTO_COBRANZAS]");

                 //VL_Database.AddInParameter(VL_Command, "@ID_Cobranza", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA);
                 VL_Database.AddOutParameter(VL_Command, "@ID_Cobranza", DbType.Int32, 10);

                 VL_Database.AddInParameter(VL_Command, "@ID_Cliente", DbType.String, OBJ_COBRANZA.ID_CLIENTE);
                 VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.String, OBJ_COBRANZA.FECHA);
                 VL_Database.AddInParameter(VL_Command, "@TC", DbType.Decimal, OBJ_COBRANZA.TC);
                 VL_Database.AddInParameter(VL_Command, "@IdBanco", DbType.String, OBJ_COBRANZA.ID_BANCO);
                 VL_Database.AddInParameter(VL_Command, "@IdCtaCorriente", DbType.String, OBJ_COBRANZA.ID_CTA_CORRIENTE);
                 VL_Database.AddInParameter(VL_Command, "@NroOperacion", DbType.String, OBJ_COBRANZA.NRO_OPERACION);
                 VL_Database.AddInParameter(VL_Command, "@Serie", DbType.String, OBJ_COBRANZA.SERIE);
                 VL_Database.AddInParameter(VL_Command, "@Numero", DbType.String, OBJ_COBRANZA.NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@ID_Moneda", DbType.String, OBJ_COBRANZA.ID_MONEDA);
                 VL_Database.AddInParameter(VL_Command, "@Total", DbType.Decimal, OBJ_COBRANZA.TOTAL);
                 VL_Database.AddInParameter(VL_Command, "@Glosa", DbType.String, OBJ_COBRANZA.GLOSA);
                 VL_Database.AddInParameter(VL_Command, "@Asiento", DbType.Int16, OBJ_COBRANZA.ASIENTO);
                 VL_Database.AddInParameter(VL_Command, "@AsientoOK", DbType.Int16, OBJ_COBRANZA.ASIENTOOK);
                 VL_Database.AddInParameter(VL_Command, "@AsientoMsg", DbType.String, OBJ_COBRANZA.ASIENTOMSG);
                 VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.String, OBJ_COBRANZA.ID_USUARIO);

                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_COBRANZA.TIPO);

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_COBRANZA(BeanCobranza OBJ_COBRANZA)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();


             try
             {
                 VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_MANTENIMIENTO_COBRANZAS]");

                 VL_Database.AddInParameter(VL_Command, "@ID_Cobranza", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA);
                 VL_Database.AddInParameter(VL_Command, "@ID_Cliente", DbType.String, OBJ_COBRANZA.ID_CLIENTE);
                 VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.String, OBJ_COBRANZA.FECHA);
                 VL_Database.AddInParameter(VL_Command, "@TC", DbType.Decimal, OBJ_COBRANZA.TC);
                 VL_Database.AddInParameter(VL_Command, "@IdBanco", DbType.String, OBJ_COBRANZA.ID_BANCO);
                 VL_Database.AddInParameter(VL_Command, "@IdCtaCorriente", DbType.String, OBJ_COBRANZA.ID_CTA_CORRIENTE);
                 VL_Database.AddInParameter(VL_Command, "@NroOperacion", DbType.String, OBJ_COBRANZA.NRO_OPERACION);
                 VL_Database.AddInParameter(VL_Command, "@Serie", DbType.String, OBJ_COBRANZA.SERIE);
                 VL_Database.AddInParameter(VL_Command, "@Numero", DbType.String, OBJ_COBRANZA.NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@ID_Moneda", DbType.String, OBJ_COBRANZA.ID_MONEDA);
                 VL_Database.AddInParameter(VL_Command, "@Total", DbType.Decimal, OBJ_COBRANZA.TOTAL);
                 VL_Database.AddInParameter(VL_Command, "@Glosa", DbType.String, OBJ_COBRANZA.GLOSA);
                 VL_Database.AddInParameter(VL_Command, "@Asiento", DbType.Int16, OBJ_COBRANZA.ASIENTO);
                 VL_Database.AddInParameter(VL_Command, "@AsientoOK", DbType.Int16, OBJ_COBRANZA.ASIENTOOK);
                 VL_Database.AddInParameter(VL_Command, "@AsientoMsg", DbType.String, OBJ_COBRANZA.ASIENTOMSG);
                 VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.String, OBJ_COBRANZA.ID_USUARIO);

                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_COBRANZA.TIPO);

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }

         public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_COBRANZA(BeanCobranza OBJ_COBRANZA)
         {
             SqlDatabase VL_Database;
             DbCommand VL_Command;
             DbConnection VL_Connection;
             DbTransaction VL_Transact;
             BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

             VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
             VL_Connection = VL_Database.CreateConnection();
             VL_Connection.Open();
             VL_Transact = VL_Connection.BeginTransaction();

             try
             {
                  VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_MANTENIMIENTO_COBRANZAS]");

                 VL_Database.AddInParameter(VL_Command, "@ID_Cobranza", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA);
                 VL_Database.AddInParameter(VL_Command, "@ID_Cliente", DbType.String, OBJ_COBRANZA.ID_CLIENTE);
                 VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.String, OBJ_COBRANZA.FECHA);
                 VL_Database.AddInParameter(VL_Command, "@TC", DbType.Decimal, OBJ_COBRANZA.TC);
                 VL_Database.AddInParameter(VL_Command, "@IdBanco", DbType.String, OBJ_COBRANZA.ID_BANCO);
                 VL_Database.AddInParameter(VL_Command, "@IdCtaCorriente", DbType.String, OBJ_COBRANZA.ID_CTA_CORRIENTE);
                 VL_Database.AddInParameter(VL_Command, "@NroOperacion", DbType.String, OBJ_COBRANZA.NRO_OPERACION);
                 VL_Database.AddInParameter(VL_Command, "@Serie", DbType.String, OBJ_COBRANZA.SERIE);
                 VL_Database.AddInParameter(VL_Command, "@Numero", DbType.String, OBJ_COBRANZA.NUMERO);
                 VL_Database.AddInParameter(VL_Command, "@ID_Moneda", DbType.String, OBJ_COBRANZA.ID_MONEDA);
                 VL_Database.AddInParameter(VL_Command, "@Total", DbType.Decimal, OBJ_COBRANZA.TOTAL);
                 VL_Database.AddInParameter(VL_Command, "@Glosa", DbType.String, OBJ_COBRANZA.GLOSA);
                 VL_Database.AddInParameter(VL_Command, "@Asiento", DbType.Int16, OBJ_COBRANZA.ASIENTO);
                 VL_Database.AddInParameter(VL_Command, "@AsientoOK", DbType.Int16, OBJ_COBRANZA.ASIENTOOK);
                 VL_Database.AddInParameter(VL_Command, "@AsientoMsg", DbType.String, OBJ_COBRANZA.ASIENTOMSG);
                 VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.String, OBJ_COBRANZA.ID_USUARIO);

                 VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_COBRANZA.TIPO);

                 VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                 VL_Transact.Commit();
                 VL_BeanResultado.blnResultado = true;
                 VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

             }
             catch (System.Data.SqlClient.SqlException ex)
             {
                 VL_BeanResultado.blnResultado = false;
                 VL_BeanResultado.strMensaje = ex.Message.ToString();
                 VL_Transact.Rollback();
             }
             finally
             {
                 if (VL_Connection.State == ConnectionState.Open)
                 {
                     VL_Connection.Close();
                 }
                 VL_Database = null;
                 VL_Command = null;
             }
             return VL_BeanResultado;
         }
        
    }
}
