﻿using CapaEntidad;
using CapaEntidad.Cobranza;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Cobranza
{
    
    public class DAODetalleCobranza
    {
        public DAODetalleCobranza(){}

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_COBRANZA_DETALLE(Int16 ID_COBRANZA, ref Int32 ID_COBRANZA_DETALLE)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_LISTAR_COBRANZAS_DETALLE]");

                VL_Database.AddInParameter(VL_Command, "@ID_COBRANZA", DbType.Int16, ID_COBRANZA);
                VL_Database.AddOutParameter(VL_Command, "@ID_CobranzaDetalle", DbType.Int32, 10);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;

                ID_COBRANZA_DETALLE = (Int32)VL_Database.GetParameterValue(VL_Command, "@ID_CobranzaDetalle");
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_COBRANZA_DETALLES(BeanDetalleCobranza OBJ_COBRANZA)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_MANTENIMIENTO_COBRANZAS_DETALLE]");

                //VL_Database.AddInParameter(VL_Command, "@ID_CobranzaDetalle", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA_DETALLE);                

                VL_Database.AddInParameter(VL_Command, "@ID_Cobranza", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA);
                VL_Database.AddInParameter(VL_Command, "@ID_DocVenta", DbType.Int16, OBJ_COBRANZA.ID_DOC_VENTA);
                VL_Database.AddInParameter(VL_Command, "@Importe", DbType.Decimal, OBJ_COBRANZA.IMPORTE);              

                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_COBRANZA.TIPO);
               
                 VL_Database.AddOutParameter(VL_Command, "@ID_CobranzaDetalle", DbType.Int32, 4);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR_COBRANZA_DETALLES(BeanDetalleCobranza OBJ_COBRANZA)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_MANTENIMIENTO_COBRANZAS_DETALLE]");

                VL_Database.AddInParameter(VL_Command, "@ID_CobranzaDetalle", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA_DETALLE);
                VL_Database.AddInParameter(VL_Command, "@ID_Cobranza", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA);
                VL_Database.AddInParameter(VL_Command, "@ID_DocVenta", DbType.Int16, OBJ_COBRANZA.ID_DOC_VENTA);
                VL_Database.AddInParameter(VL_Command, "@Importe", DbType.Decimal, OBJ_COBRANZA.IMPORTE);


                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_COBRANZA.TIPO);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_COBRANZA_DETALLES(BeanDetalleCobranza OBJ_COBRANZA)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[TESORERIA].[USP_MANTENIMIENTO_COBRANZAS_DETALLE]");

                VL_Database.AddInParameter(VL_Command, "@ID_CobranzaDetalle", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA_DETALLE);
                VL_Database.AddInParameter(VL_Command, "@ID_Cobranza", DbType.Int16, OBJ_COBRANZA.ID_COBRANZA);
                VL_Database.AddInParameter(VL_Command, "@ID_DocVenta", DbType.Int16, OBJ_COBRANZA.ID_DOC_VENTA);
                VL_Database.AddInParameter(VL_Command, "@Importe", DbType.Decimal, OBJ_COBRANZA.IMPORTE);


                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, OBJ_COBRANZA.TIPO);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente...!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        
    }
}
