﻿using CapaEntidad;
using CapaEntidad.MigrarData;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.MigrarData
{
    public class DAOClientes
    {
        public DAOClientes() { }


        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_TIPO_DOC()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_LISTAR_TIPO_DOC]");
               

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_CLIENTES()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_LISTAR_CLIENTES_BOLETAS]");


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_EMPRESA()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_LISTAR_EMPRESA]");


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_OBRA()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_LISTAR_OBRA]");


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR(BeanClientes OBJ_CLIENTE)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_CLIENTES_BOLETAS]");

                VL_Database.AddInParameter(VL_Command, "@CLIE_CODIGO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@TPDC_CODIGO", DbType.String, OBJ_CLIENTE.TIPO_DOC);
                VL_Database.AddInParameter(VL_Command, "@UBDS_CODIGO", DbType.String, OBJ_CLIENTE.UBDS_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_NOMBRES", DbType.String, OBJ_CLIENTE.NOMBRES);
                VL_Database.AddInParameter(VL_Command, "@CLIE_APELLIDOPATERNO", DbType.String, OBJ_CLIENTE.APE_PATERNO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_APELLIDOMATERNO", DbType.String, OBJ_CLIENTE.APE_MATERNO);        
                VL_Database.AddInParameter(VL_Command, "@CLIE_NRODOCUMENTO", DbType.String, OBJ_CLIENTE.NRO_DOC);
                VL_Database.AddInParameter(VL_Command, "@CLIE_SEXO", DbType.String, OBJ_CLIENTE.SEXO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_FECHANACIMIENTO", DbType.DateTime, OBJ_CLIENTE.FECHA_NAC);
                VL_Database.AddInParameter(VL_Command, "@CLIE_ESTADOCIVIL", DbType.String, OBJ_CLIENTE.ESTADO_CIVIL);
                VL_Database.AddInParameter(VL_Command, "@CLIE_DIRECCION", DbType.String, OBJ_CLIENTE.DIRECCION);
                VL_Database.AddInParameter(VL_Command, "@CLIE_REFERENCIA", DbType.String, OBJ_CLIENTE.REFERENCIA);
                VL_Database.AddInParameter(VL_Command, "@CLIE_TELEFONO", DbType.String, OBJ_CLIENTE.TELEFONO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_EMAIL", DbType.String, OBJ_CLIENTE.EMAIL);
                VL_Database.AddInParameter(VL_Command, "@CLIE_OCUPACION", DbType.String, OBJ_CLIENTE.OCUPACION);
                VL_Database.AddInParameter(VL_Command, "@CLIE_EMPRESA", DbType.String, OBJ_CLIENTE.EMPRESA);
                VL_Database.AddInParameter(VL_Command, "@CLIE_ID_OBRA", DbType.String, OBJ_CLIENTE.ID_OBRA);             
                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 1);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ACTUALIZAR(BeanClientes OBJ_CLIENTE)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_CLIENTES_BOLETAS]");

                VL_Database.AddInParameter(VL_Command, "@CLIE_CODIGO", DbType.String, OBJ_CLIENTE.CODIGO);
                VL_Database.AddInParameter(VL_Command, "@TPDC_CODIGO", DbType.String, OBJ_CLIENTE.TIPO_DOC);
                VL_Database.AddInParameter(VL_Command, "@UBDS_CODIGO", DbType.String, OBJ_CLIENTE.UBDS_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_NOMBRES", DbType.String, OBJ_CLIENTE.NOMBRES);
                VL_Database.AddInParameter(VL_Command, "@CLIE_APELLIDOPATERNO", DbType.String, OBJ_CLIENTE.APE_PATERNO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_APELLIDOMATERNO", DbType.String, OBJ_CLIENTE.APE_MATERNO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_NRODOCUMENTO", DbType.String, OBJ_CLIENTE.NRO_DOC);
                VL_Database.AddInParameter(VL_Command, "@CLIE_SEXO", DbType.String, OBJ_CLIENTE.SEXO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_FECHANACIMIENTO", DbType.DateTime, OBJ_CLIENTE.FECHA_NAC);
                VL_Database.AddInParameter(VL_Command, "@CLIE_ESTADOCIVIL", DbType.String, OBJ_CLIENTE.ESTADO_CIVIL);
                VL_Database.AddInParameter(VL_Command, "@CLIE_DIRECCION", DbType.String, OBJ_CLIENTE.DIRECCION);
                VL_Database.AddInParameter(VL_Command, "@CLIE_REFERENCIA", DbType.String, OBJ_CLIENTE.REFERENCIA);
                VL_Database.AddInParameter(VL_Command, "@CLIE_TELEFONO", DbType.String, OBJ_CLIENTE.TELEFONO);
                VL_Database.AddInParameter(VL_Command, "@CLIE_EMAIL", DbType.String, OBJ_CLIENTE.EMAIL);
                VL_Database.AddInParameter(VL_Command, "@CLIE_OCUPACION", DbType.String, OBJ_CLIENTE.OCUPACION);
                VL_Database.AddInParameter(VL_Command, "@CLIE_EMPRESA", DbType.String, OBJ_CLIENTE.EMPRESA);
                VL_Database.AddInParameter(VL_Command, "@CLIE_ID_OBRA", DbType.String, OBJ_CLIENTE.ID_OBRA);
                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 2);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR(String CODIGO_CLIE, int ID_OBRA)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_CLIENTES_BOLETAS]");

                VL_Database.AddInParameter(VL_Command, "@CLIE_CODIGO", DbType.String, CODIGO_CLIE);
                VL_Database.AddInParameter(VL_Command, "@TPDC_CODIGO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@UBDS_CODIGO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_NOMBRES", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_APELLIDOPATERNO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_APELLIDOMATERNO", DbType.String,"");                
                VL_Database.AddInParameter(VL_Command, "@CLIE_NRODOCUMENTO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_SEXO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_FECHANACIMIENTO", DbType.DateTime, null);
                VL_Database.AddInParameter(VL_Command, "@CLIE_ESTADOCIVIL", DbType.String,"");
                VL_Database.AddInParameter(VL_Command, "@CLIE_DIRECCION", DbType.String,"");
                VL_Database.AddInParameter(VL_Command, "@CLIE_REFERENCIA", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_TELEFONO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_EMAIL", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_OCUPACION", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_EMPRESA", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CLIE_ID_OBRA", DbType.String, ID_OBRA);
                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 3);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        #region "LISTAR USBIGEO"

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DEPARTAMENTOS()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[INVENTARIO].[USP_LISTAR_DEPARTAMENTOS]");
                //VL_Database.AddInParameter(VL_Command, "@ID_DEPARTAMENTO", DbType.String, ID_DEPA);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_PROVINCIA(String ID_DEPA)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[INVENTARIO].[USP_LISTAR_PROVINCIA]");
                VL_Database.AddInParameter(VL_Command, "@ID_DEPARTAMENTO", DbType.String, ID_DEPA);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DISTRITO(String ID_DIS)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[INVENTARIO].[USP_LISTAR_DISTRITO]");
                VL_Database.AddInParameter(VL_Command, "@ID_PROVINCIA", DbType.String, ID_DIS);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        #endregion




    }
}
