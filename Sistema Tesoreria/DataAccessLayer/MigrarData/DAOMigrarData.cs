﻿using CapaEntidad;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.MigrarData
{
    public class DAOMigrarData
    {
        public DAOMigrarData() { }

        String VG_CONECTION_STRING = "Data Source=192.168.1.200;Initial Catalog=CONSORCIODHMONT;Persist Security Info=True;User ID=SA;Password=!gestionerp2013";
        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_EMPRESA()
        {
            SqlDatabase VL_Database = new SqlDatabase(VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[dbo].[USP_LISTAR_EMPRESASGENERALES]");


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_OBRAS(String EMPRESA)
        {
            SqlDatabase VL_Database = new SqlDatabase(VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[dbo].[USP_LISTAR_OBRASGENERALES]");
                VL_Database.AddInParameter(VL_Command, "@EMPR_CODIGO", DbType.String, EMPRESA);


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DATOS(String BASEDATOS,String OBRA_CODIGO, String FINICIAL, String FFIN)
        {
            VG_CONECTION_STRING = "Data Source=192.168.1.200;Initial Catalog="+BASEDATOS+";Persist Security Info=True;User ID=SA;Password=!gestionerp2013";
            SqlDatabase VL_Database = new SqlDatabase(VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[INMOBILIARIO].[USP_CONSULTA_PAGOSXMES]");
                VL_Database.AddInParameter(VL_Command, "@OBRA_CODIGO", DbType.String, OBRA_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@AÑO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@PERIODO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@FINICIO", DbType.String, FINICIAL);
                VL_Database.AddInParameter(VL_Command, "@FFINAL", DbType.String, FFIN);
                VL_Database.AddInParameter(VL_Command, "@FILTRO", DbType.String, "2");


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DOCUMENTOS(String DOCUMENTO)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_LISTAR_DOCUMENTOS]");
                VL_Database.AddInParameter(VL_Command, "@ORIGEN", DbType.String, DOCUMENTO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_DATOS_INTERMEDIA()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_DATOS_TABLA_INTERMEDIA]");
         
                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

       public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_HISTORIAL_DATOS()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_HISTORIAL_DATOS]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }



        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_DATOS(String OBRA_CODIGO, String BASE_DATOS, String FECHA_INICIAL, String FECHA_FIN)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_MANTENIMIENTO_MIGRAR_DATA]");

                VL_Database.AddInParameter(VL_Command, "@OBRA_CODIGO", DbType.String, OBRA_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@BASE_DATOS", DbType.String, BASE_DATOS);
                VL_Database.AddInParameter(VL_Command, "@FECHA_INICIAL", DbType.String, FECHA_INICIAL);
                VL_Database.AddInParameter(VL_Command, "@FECHA_FIN", DbType.String, FECHA_FIN);
              
                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }



        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_MIGRAR_DATOS(int OBRA_CODIGO, String DOCUMENTO, String USUARIO)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MIGRAR_DATOS_BOLETAS]");

                VL_Database.AddInParameter(VL_Command, "@ID_OBRA", DbType.String, OBRA_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@DOCUMENTO", DbType.String, DOCUMENTO);
                VL_Database.AddInParameter(VL_Command, "@USUARIO", DbType.String, USUARIO);
               

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        //INSERTAR HISTORIAL DE  MMIGRACIONES
        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_AGREGAR_DATOS_HISTORIAL(String EMPRE_CODIGO,int OBRA_CODIGO, String DOC_CODIGO,int COD_ULTIMO,
            String FECHA_INICIAL, String FECHA_FIN)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_HISTORIAL]");

                VL_Database.AddInParameter(VL_Command, "@CODIGO_HISTORIAL", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@EMPR_CODIGO", DbType.String, EMPRE_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@OBRA_CODIGO", DbType.Int16, OBRA_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@DOC_CODIGO", DbType.String, DOC_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@CODIGO_ULTIMO", DbType.Int16, COD_ULTIMO);
                VL_Database.AddInParameter(VL_Command, "@FECHA_INI", DbType.String, FECHA_INICIAL);
                VL_Database.AddInParameter(VL_Command, "@FECHA_FIN", DbType.String, FECHA_FIN);
                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 1);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        //ELIMINAR DATO HISTORIAL
        public CapaEntidad.BeanResultado.ResultadoTransaccion FNC_ELIMINAR_DATOS_HISTORIAL(int HISTORIAL_CODIGO)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_MANTENIMIENTO_HISTORIAL]");

                VL_Database.AddInParameter(VL_Command, "@CODIGO_HISTORIAL", DbType.Int16, HISTORIAL_CODIGO);
                VL_Database.AddInParameter(VL_Command, "@EMPR_CODIGO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@OBRA_CODIGO", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@DOC_CODIGO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@CODIGO_ULTIMO", DbType.Int16, 0);
                VL_Database.AddInParameter(VL_Command, "@FECHA_INI", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@FECHA_FIN", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@TIPO", DbType.Int16, 3);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_LISTAR_HISTORIAL()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[VENTAS].[USP_LISTAR_HISTORIAL_MIGRACION]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        //BORRAR DATOS DE HISTORIAL TOTAL
        public CapaEntidad.BeanResultado.ResultadoSeleccion FNC_TRUNCATE_TABLE()
        {

            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            DbCommand VL_Command;

            try
            {

                VL_Command = VL_Database.GetStoredProcCommand("[Ventas].[USP_BORRAR_HISTORIAL_BOLETAS]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;

        }


    }
}
