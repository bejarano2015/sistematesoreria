﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;

namespace DataAccessLayer.Mantenimiento
{
    public class DaoVistoBueno
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_UsuariosVB(Int32 id, Decimal vl_USUARIO)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_MANTENIMIENTO_VBUSUARIO");
                VL_Database.AddInParameter(VL_Command, "@ID", DbType.Int32, id);
                VL_Database.AddInParameter(VL_Command, "@ID_VistoBueno", DbType.Decimal, 1);
                VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.Decimal, vl_USUARIO);
                VL_Database.AddInParameter(VL_Command, "@ID_UsuarioVistoBueno", DbType.Decimal, 1);


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Consulta_VB(Int32 VL_OPCION,Decimal VL_ID_DOCPENDIENTE, Decimal VL_ID_DOCUMENTO)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Consulta_VB");
                VL_Database.AddInParameter(VL_Command, "@OPCION", DbType.Int32, VL_OPCION);
                VL_Database.AddInParameter(VL_Command, "@ID_DocPendiente", DbType.Decimal, VL_ID_DOCPENDIENTE);
                VL_Database.AddInParameter(VL_Command, "@ID_Documento", DbType.Decimal, VL_ID_DOCUMENTO);
                


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                //VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_InsertarVarios_VB(Int32 VL_OPCION, Decimal VL_ID_VistoBueno, Decimal VL_ID_Usuario, String VL_VistoBueno, Int32 VL_Estado)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Insertar_VistoBuenos");

                VL_Database.AddInParameter(VL_Command, "@ID", DbType.Int32, VL_OPCION);
                VL_Database.AddInParameter(VL_Command, "@ID_VistoBueno", DbType.Decimal, VL_ID_VistoBueno);
                VL_Database.AddInParameter(VL_Command, "@ID_Usuario", DbType.Decimal, VL_ID_Usuario);
                VL_Database.AddInParameter(VL_Command, "@VistoBueno", DbType.String, VL_VistoBueno);
                VL_Database.AddInParameter(VL_Command, "@Estado", DbType.Int32, VL_Estado);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;
        }
    }
}
