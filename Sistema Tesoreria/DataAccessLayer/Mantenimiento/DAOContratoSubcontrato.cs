﻿using CapaEntidad;
using CapaEntidad.Mantenimiento;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Mantenimiento
{
    public class DAOContratoSubcontrato
    {
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_contrato( string emprcodigo)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.Usp_ContratoSubcontratoListar");
                VL_Database.AddInParameter(VL_Command, "@emprcodigo", DbType.String, emprcodigo);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_ContratoSubcontrato(BeanContratoSubcontratos vl_BeanContratoSubcontratos)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();
            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_CONTRATOSUBCONTRATO_INSERTAR]");
                //VL_Database.AddInParameter(VL_Command, "@OPCION", DbType.String, beanConsolidado.opcion);
                VL_Database.AddInParameter(VL_Command, "@Co_Empresa", DbType.String, vl_BeanContratoSubcontratos.Co_Empresa);
                VL_Database.AddInParameter(VL_Command, "@Co_Obra", DbType.String, vl_BeanContratoSubcontratos.Co_Obra);
                VL_Database.AddInParameter(VL_Command, "@Co_Proveedor", DbType.String, vl_BeanContratoSubcontratos.Co_Proveedor);
                VL_Database.AddInParameter(VL_Command, "@Nu_Contrato", DbType.Int32, vl_BeanContratoSubcontratos.Nu_Contrato);
                VL_Database.AddInParameter(VL_Command, "@Ss_MontoContrato", DbType.Decimal, vl_BeanContratoSubcontratos.Ss_MontoContrato);
                VL_Database.AddInParameter(VL_Command, "@Tx_DescrpcionContrato", DbType.String, vl_BeanContratoSubcontratos.Tx_DescrpcionContrato);
                VL_Database.AddInParameter(VL_Command, "@Po_Scar", DbType.String, vl_BeanContratoSubcontratos.Po_Scar);
                VL_Database.AddInParameter(VL_Command, "@Fl_ModoPagoRetencion", DbType.String, vl_BeanContratoSubcontratos.Fl_ModoPagoRetencion);
                VL_Database.AddInParameter(VL_Command, "@usuario", DbType.String, vl_BeanContratoSubcontratos.usuario);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_ContratoSubcontrato(BeanContratoSubcontratos vl_BeanContratoSubcontratos)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();
            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_CONTRATOSUBCONTRATO_ELIMINAR");
                VL_Database.AddInParameter(VL_Command, "@usuario", DbType.String, vl_BeanContratoSubcontratos.usuario );
                VL_Database.AddInParameter(VL_Command, "@Co_Contrato", DbType.String, vl_BeanContratoSubcontratos.Co_Contrato);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Modificar_ContratoSubcontrato(BeanContratoSubcontratos vl_BeanContratoSubcontratos)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();
            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_CONTRATOSUBCONTRATO_ACTUALIZAR");
                VL_Database.AddInParameter(VL_Command, "@Co_Contrato", DbType.String, vl_BeanContratoSubcontratos.Co_Contrato);
                VL_Database.AddInParameter(VL_Command, "@Co_Empresa", DbType.String, vl_BeanContratoSubcontratos.Co_Empresa);
                VL_Database.AddInParameter(VL_Command, "@Co_Obra", DbType.String, vl_BeanContratoSubcontratos.Co_Obra);
                VL_Database.AddInParameter(VL_Command, "@Co_Proveedor", DbType.String, vl_BeanContratoSubcontratos.Co_Proveedor);
                VL_Database.AddInParameter(VL_Command, "@Nu_Contrato", DbType.Int32, vl_BeanContratoSubcontratos.Nu_Contrato);
                VL_Database.AddInParameter(VL_Command, "@Ss_MontoContrato", DbType.Decimal, vl_BeanContratoSubcontratos.Ss_MontoContrato);
                VL_Database.AddInParameter(VL_Command, "@Tx_DescrpcionContrato", DbType.String, vl_BeanContratoSubcontratos.Tx_DescrpcionContrato);
                VL_Database.AddInParameter(VL_Command, "@Po_Scar", DbType.String, vl_BeanContratoSubcontratos.Po_Scar);
                VL_Database.AddInParameter(VL_Command, "@Fl_ModoPagoRetencion", DbType.String, vl_BeanContratoSubcontratos.Fl_ModoPagoRetencion);
                VL_Database.AddInParameter(VL_Command, "@usuario", DbType.String, vl_BeanContratoSubcontratos.usuario);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
         
    }
}
