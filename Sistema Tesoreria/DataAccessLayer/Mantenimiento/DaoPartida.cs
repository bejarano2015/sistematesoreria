﻿using CapaEntidad;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 using CapaEntidad.Mantenimiento;
using System.Data; 
namespace DataAccessLayer.Mantenimiento
{
   public class DaoPartida
    {
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_partida(BeanPartida vl_BeanPartida)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.[USP_PARTIDA_LISTAR]");
                VL_Database.AddInParameter(VL_Command, "@ParDescripcion", DbType.String, vl_BeanPartida.ParDescripcion);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
    }
}
