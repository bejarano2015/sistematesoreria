﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;


namespace DataAccessLayer.Mantenimiento
{
    public class DAOEntregaRendir
    {
         CapaDatos.DaoGlobal VL_VarGlobal=new CapaDatos.DaoGlobal();
        

         #region "Listar"

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_BEmpresas()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Listar_BEmpresas");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_TipoDocumentos()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Listar_TipoDocumento");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
    
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Empresas()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Listar_Empresas");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Obras()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Listar_Obras");
                //VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);
               

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Empleados()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Planilla.USP_Listar_Padron_Correlativo");
               // VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);
                

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Girado()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Listar_Girado");
                // VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_BEmpleados()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Planilla.USP_Listar_Padron_Correlativo");


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Autoriza()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Planilla.USP_Listar_Padron_Correlativo");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_AreaPorEmpresa(BeanEntregaRendir objEntregaRendir)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Comun.Usp_ListarAreaPorEmpresa");
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Monedas()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Listar_Monedas");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_EntregaRendir(DateTime VL_FechaInicio, DateTime VL_FechaFin)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.ListarEntregaRendir");
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.Date, VL_FechaInicio);
                VL_Database.AddInParameter(VL_Command, "@FECHAFIN", DbType.Date, VL_FechaFin);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_EntregaRendir_1(DateTime VL_FechaInicio, DateTime VL_FechaFin, String VL_Emp)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.ListarEntregaRendir_1");
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.Date, VL_FechaInicio);
                VL_Database.AddInParameter(VL_Command, "@FECHAFIN", DbType.Date, VL_FechaFin);
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, VL_Emp);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_EntregaRendir()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Listar_Estado_EntregaRendir");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

#endregion

        #region "Procesos"

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Entrega_Rendir(BeanEntregaRendir objEntregaRendir)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new  BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_insertar_Entrega_Rendir");

     
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);
                VL_Database.AddInParameter(VL_Command, "@SolCodigo", DbType.String, objEntregaRendir.SolCodigo);
                VL_Database.AddInParameter(VL_Command, "@ObraAreaCodigo", DbType.String, objEntregaRendir.ObraAreaCodigo);
                VL_Database.AddInParameter(VL_Command, "@MonCodigo", DbType.String, objEntregaRendir.MonCodigo);
                VL_Database.AddInParameter(VL_Command, "@Monto", DbType.Decimal, objEntregaRendir.Monto);
                VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.Date, objEntregaRendir.Fecha);
                VL_Database.AddInParameter(VL_Command, "@Concepto", DbType.String, objEntregaRendir.Concepto);
                VL_Database.AddInParameter(VL_Command, "@ID_ESTADO", DbType.String, objEntregaRendir.ID_ESTADO);
                VL_Database.AddInParameter(VL_Command, "@AutCodigo", DbType.String, objEntregaRendir.AutCodigo);
                VL_Database.AddInParameter(VL_Command, "@GirCodigo", DbType.String, objEntregaRendir.GirCodigo);
                VL_Database.AddInParameter(VL_Command, "@AreaCodigo", DbType.String, objEntregaRendir.AreaCodigo);
                //VL_Database.AddInParameter(VL_Command, "@SolTPla", DbType.String, objEntregaRendir.SolTPla);
                //VL_Database.AddInParameter(VL_Command, "@AutTPla", DbType.String, objEntregaRendir.AutTPla);
                //VL_Database.AddInParameter(VL_Command, "@GirTPla", DbType.String, objEntregaRendir.GirTPla);
                VL_Database.AddInParameter(VL_Command, "@ObrEmprCodigo", DbType.String, objEntregaRendir.ObrEmprCodigo);
                VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.String, objEntregaRendir.ID_DOCUMENTO);
                

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Entrega_Rendir(String ID_EntregaRendir, String DocOrigen)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Eliminar_EntregaRendir");

                VL_Database.AddInParameter(VL_Command, "@ID_EntregaRendir", DbType.String, ID_EntregaRendir);
                VL_Database.AddInParameter(VL_Command, "@DocOrigen", DbType.String, DocOrigen);
                
                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Entrega_Rendir(BeanEntregaRendir objEntregaRendir)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("contabilidad.usp_ModificarEntregaRendir");

                VL_Database.AddInParameter(VL_Command, "@ID_EntregaRendir", DbType.String, objEntregaRendir.ID_EntregaRendir);
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);
                VL_Database.AddInParameter(VL_Command, "@SolCodigo", DbType.String, objEntregaRendir.SolCodigo);
                VL_Database.AddInParameter(VL_Command, "@ObraAreaCodigo", DbType.String, objEntregaRendir.ObraAreaCodigo);
                VL_Database.AddInParameter(VL_Command, "@MonCodigo", DbType.String, objEntregaRendir.MonCodigo);
                VL_Database.AddInParameter(VL_Command, "@Monto", DbType.Decimal, objEntregaRendir.Monto);
                VL_Database.AddInParameter(VL_Command, "@Concepto", DbType.String, objEntregaRendir.Concepto);
                VL_Database.AddInParameter(VL_Command, "@ID_ESTADO", DbType.String, objEntregaRendir.ID_ESTADO);
                VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.Date, objEntregaRendir.Fecha);
                VL_Database.AddInParameter(VL_Command, "@AutCodigo", DbType.String, objEntregaRendir.AutCodigo);
                VL_Database.AddInParameter(VL_Command, "@GirCodigo", DbType.String, objEntregaRendir.GirCodigo);
                VL_Database.AddInParameter(VL_Command, "@AreaCodigo", DbType.String, objEntregaRendir.AreaCodigo);
                //VL_Database.AddInParameter(VL_Command, "@SolTPla", DbType.String, objEntregaRendir.SolTPla);
                //VL_Database.AddInParameter(VL_Command, "@AutTPla", DbType.String, objEntregaRendir.AutTPla);
                //VL_Database.AddInParameter(VL_Command, "@GirTPla", DbType.String, objEntregaRendir.GirTPla);
                VL_Database.AddInParameter(VL_Command, "@ObrEmprCodigo", DbType.String, objEntregaRendir.ObrEmprCodigo);





                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Buscar_Por_Empresa_Solicitante(BeanEntregaRendir objEntregaRendir)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.usp_BuscarEntregaRendirPorEmpresaSolicitante");
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);
                //VL_Database.AddInParameter(VL_Command, "@SolTPla", DbType.String, objEntregaRendir.SolTPla);
                VL_Database.AddInParameter(VL_Command, "@SolCodigo", DbType.String, objEntregaRendir.SolCodigo);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Buscar_Por_Fechas(BeanEntregaRendir objEntregaRendir)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.usp_BuscarEntregaRendirPorFecha");
                VL_Database.AddInParameter(VL_Command, "@FechaInicio", DbType.DateTime, objEntregaRendir.FechaInicio);
                VL_Database.AddInParameter(VL_Command, "@FechaFin", DbType.DateTime, objEntregaRendir.FechaFin);
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, objEntregaRendir.EmprCodigo);
                

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        #endregion 


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_ER(BeanEntregaRendir objEntregaRendir)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("contabilidad.USP_Actualizar_estado_ER");

                VL_Database.AddInParameter(VL_Command, "@ID_EntregaRendir", DbType.String, objEntregaRendir.ID_EntregaRendir);
                VL_Database.AddInParameter(VL_Command, "@id_estado", DbType.String, objEntregaRendir.ID_ESTADO);



                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_NP(String VL_NUM_DOC_IDENTIDAD, String VL_NOMBRES, String VL_APELLIDO_PATERNO, String VL_APELLIDO_MATERNO, Int32 VL_ESTADO)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Insertar_NP");


                VL_Database.AddInParameter(VL_Command, "@NUM_DOC_IDENTIDAD", DbType.String, VL_NUM_DOC_IDENTIDAD);
                VL_Database.AddInParameter(VL_Command, "@NOMBRES", DbType.String, VL_NOMBRES);
                VL_Database.AddInParameter(VL_Command, "@APELLIDO_PATERNO", DbType.String, VL_APELLIDO_PATERNO);
                VL_Database.AddInParameter(VL_Command, "@APELLIDO_MATERNO", DbType.String, VL_APELLIDO_MATERNO);
                VL_Database.AddInParameter(VL_Command, "@ESTADO", DbType.Int32, VL_ESTADO);
                


                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;
        }
    }
}
