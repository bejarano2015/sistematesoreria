﻿using CapaEntidad;
using CapaEntidad.Mantenimiento;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Mantenimiento
{
    public class DaoUnidadMedida
    {
        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_UnidadMedida()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Inventario].[USP_LlenarCombos_Generico]");
                VL_Database.AddInParameter(VL_Command, "@Campos", DbType.String, "UmdCodigo,UmdDescripcion ");
                VL_Database.AddInParameter(VL_Command, "@Tabla", DbType.String, "Logistica.UnidadMedida ");
                VL_Database.AddInParameter(VL_Command, "@FILTRO", DbType.String, "");
                VL_Database.AddInParameter(VL_Command, "@Orden", DbType.String, "UmdDescripcion");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
    }
}
