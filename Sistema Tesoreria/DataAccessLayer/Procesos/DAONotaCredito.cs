﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;
using CapaEntidad.Proceso;
using System.Data.SqlClient;

namespace DataAccessLayer.Procesos
{
    public class DAONotaCredito
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_NotaCredito(Decimal VL_IdCorrelativo, Decimal Id_DocPendiente)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Usp_InsertarNotaCredito");
                VL_Database.AddInParameter(VL_Command, "@IdCorrelativo", DbType.Decimal, VL_IdCorrelativo);
                VL_Database.AddInParameter(VL_Command, "@Id_DocPendiente", DbType.Decimal, Id_DocPendiente);

            



                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_NotaCredito( Decimal Id_DocPendiente)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Usp_Eliminar_NotaCredito");
                //VL_Database.AddInParameter(VL_Command, "@IdCorrelativo", DbType.Decimal, VL_IdCorrelativo);
                VL_Database.AddInParameter(VL_Command, "@Id_DocPendiente", DbType.Decimal, Id_DocPendiente);





                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_NotaCredito(String VL_Emp,String VL_Ruc, String VL_Moneda)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Usp_Listar_NotaCredito");
                VL_Database.AddInParameter(VL_Command, "@Empresa", DbType.String, VL_Emp);
                VL_Database.AddInParameter(VL_Command, "@RUC", DbType.String, VL_Ruc);
                VL_Database.AddInParameter(VL_Command, "@Moneda", DbType.String, VL_Moneda);

               

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Montos_Por_Proveedor(String VL_Emp, String VL_Ruc)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Usp_Listar_Montos_Por_Proveedor");
                VL_Database.AddInParameter(VL_Command, "@Empresa", DbType.String, VL_Emp);
                VL_Database.AddInParameter(VL_Command, "@RUC", DbType.String, VL_Ruc);



                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }




    }
}
