﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;

namespace DataAccessLayer.Procesos
{
    public class DAOGestionDocumento
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_TipoDocumento()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_ListarTipoDocumento");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Empresa()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Comun].[USP_LISTAR_EMPRESA]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_BancoEmpresa(String VL_BANCO_EMPRESA)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_LISTAR_BANCO_EMPRESA");
                VL_Database.AddInParameter(VL_Command, "@Nombre", DbType.String, VL_BANCO_EMPRESA);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_GestionDocumentario_Grabar(CapaEntidad.Proceso.BeanGestionDocumentaria objGestionDocumentaria)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_GESTION_DOCUMENTO_VARIOS_GRABAR");

                VL_Database.AddInParameter(VL_Command, "@IDDOCUMENTO", DbType.String, objGestionDocumentaria.IDDOCUMENTO);
                VL_Database.AddInParameter(VL_Command, "@NRODOCUMENTO", DbType.String, objGestionDocumentaria.NRODOCUMENTO);
                VL_Database.AddInParameter(VL_Command, "@IDBANCOEMPRESA", DbType.String, objGestionDocumentaria.IDBANCOEMPRESA);
                VL_Database.AddInParameter(VL_Command, "@EMPRCODIGO", DbType.String, objGestionDocumentaria.EMPRCODIGO);
                VL_Database.AddInParameter(VL_Command, "@DIASRENOVACION", DbType.Int16, objGestionDocumentaria.DIASRENOVACION);
                VL_Database.AddInParameter(VL_Command, "@GLOSA", DbType.String, objGestionDocumentaria.GLOSA);


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }
        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_GestionDocumentario_Listar()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_GESTION_DOCUMENTO_VARIOS_LISTAR]");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_GestionDocumentario_Detalle_Grabar(CapaEntidad.Proceso.BeanGestionDocumentariaDetalle objGestionDocumentariaDetalle)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_GESTION_DOCUMENTO_VARIOS_DETALLE_GRABAR]");

                VL_Database.AddInParameter(VL_Command, "@IDCABECERA", DbType.String, objGestionDocumentariaDetalle.IDCABECERA);
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.DateTime, objGestionDocumentariaDetalle.FECHAINICIO);
                VL_Database.AddInParameter(VL_Command, "@FECHAVENCIMIENTO", DbType.DateTime, objGestionDocumentariaDetalle.FECHAVENCIMIENTO);
                VL_Database.AddInParameter(VL_Command, "@IMPORTEPACTADO", DbType.Decimal, objGestionDocumentariaDetalle.IMPORTEPACTADO);
                VL_Database.AddInParameter(VL_Command, "@MONEDA", DbType.String, objGestionDocumentariaDetalle.MONEDA);
                VL_Database.AddInParameter(VL_Command, "@UNIDADESAFECTADAS", DbType.String, objGestionDocumentariaDetalle.UNIDADESAFECTADAS);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }
        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_GestionDocumentarioDetalle_Listar(int VL_IDCABECERA)
        {

            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_GESTION_DOCUMENTO_VARIOS_DETALLE_LISTAR]");

                VL_Database.AddInParameter(VL_Command, "@IDCABECERA", DbType.Int16, VL_IDCABECERA);
                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_GestionDocumentario_Detalle_Grabar(CapaEntidad.Proceso.BeanGestionDocumentariaDetalle objGestionDocumentariaDetalle)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_GESTION_DOCUMENTO_VARIOS_DETALLE_ACTUALIZAR]");

                VL_Database.AddInParameter(VL_Command, "@IDCABECERA", DbType.String, objGestionDocumentariaDetalle.IDCABECERA);
                VL_Database.AddInParameter(VL_Command, "@IDDETALLE", DbType.String, objGestionDocumentariaDetalle.IDDETALLE);
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.DateTime, objGestionDocumentariaDetalle.FECHAINICIO);
                VL_Database.AddInParameter(VL_Command, "@FECHAVENCIMIENTO", DbType.DateTime, objGestionDocumentariaDetalle.FECHAVENCIMIENTO);
                VL_Database.AddInParameter(VL_Command, "@IMPORTEPACTADO", DbType.Decimal, objGestionDocumentariaDetalle.IMPORTEPACTADO);
                VL_Database.AddInParameter(VL_Command, "@MONEDA", DbType.String, objGestionDocumentariaDetalle.MONEDA);
                VL_Database.AddInParameter(VL_Command, "@UNIDADESAFECTADAS", DbType.String, objGestionDocumentariaDetalle.UNIDADESAFECTADAS);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

    }
}
