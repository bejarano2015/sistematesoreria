﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;

namespace DataAccessLayer.Procesos
{
    public class DAODerivar
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Ordenes_Derivadas(/*String VL_OCONUMERO,*/ String VL_FECHAINICIO, String VL_FECHAFIN/*, String VL_AREACODIGO*/, String VL_EmprCodigo)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_ASIGNAR_ORDEN_COMPRA_A_CANCELAR]");
               // VL_Database.AddInParameter(VL_Command, "@OCONUMERO", DbType.String, VL_OCONUMERO);
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.String, VL_FECHAINICIO);
                VL_Database.AddInParameter(VL_Command, "@FECHAFIN", DbType.String, VL_FECHAFIN);
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, VL_EmprCodigo);
              //  VL_Database.AddInParameter(VL_Command, "@AREACODIGO", DbType.String, VL_AREACODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Facturas_derivadas(String VL_OCOCODIGO, String VL_PDOCODIGO)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Logistica].[USP_REGISTRO_DOCUMENTO_ORDEN_COMPRA_LISTAR]");
                VL_Database.AddInParameter(VL_Command, "@IDOC", DbType.String, VL_OCOCODIGO);
                VL_Database.AddInParameter(VL_Command, "@PDOCODIGO", DbType.String, VL_PDOCODIGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Areas()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("COMUN.USP_LISTAR_AREA");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Derivar_Factura(String VL_IDREGISTRO, String VL_EMPRCODIGO, String VL_OCOCODIGO, String VL_PDOCODIGO)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_DERIVAR_FACTURA");

                VL_Database.AddInParameter(VL_Command, "@IDREGISTRO", DbType.String, VL_IDREGISTRO);
                VL_Database.AddInParameter(VL_Command, "@EMPRCODIGO", DbType.String, VL_EMPRCODIGO);
                VL_Database.AddInParameter(VL_Command, "@IDOC", DbType.String, VL_OCOCODIGO);
                VL_Database.AddInParameter(VL_Command, "@PDOCODIGO", DbType.String, VL_PDOCODIGO);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

    }
}
