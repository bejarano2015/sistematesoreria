﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;
using CapaEntidad.Proceso;
using System.Data.SqlClient;

namespace DataAccessLayer.Procesos
{
    public class DAOOrdenesCompra
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_OC(BeanOrdenesCompra objOrdenesCompra)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Actualizar_Estado_OC");

                VL_Database.AddInParameter(VL_Command, "@OCOCODIGO", DbType.String, objOrdenesCompra.OCOCODIGO);
                VL_Database.AddInParameter(VL_Command, "@PDOCODIGO", DbType.String, objOrdenesCompra.PDOCODIGO);
                VL_Database.AddInParameter(VL_Command, "@OCONUMERO", DbType.String, objOrdenesCompra.OCONUMERO);
                VL_Database.AddInParameter(VL_Command, "@DERI_ESTADO", DbType.String, objOrdenesCompra.DERI_ESTADO);



                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_OC(BeanOrdenesCompra objOrdenesCompra)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Actualizar_Estado_OC_PPD ");

                VL_Database.AddInParameter(VL_Command, "@OCONUMERO", DbType.String, objOrdenesCompra.OCONUMERO);
                VL_Database.AddInParameter(VL_Command, "@DERI_ESTADO", DbType.String, objOrdenesCompra.DERI_ESTADO);




                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Doc_Pendientes(Decimal VL_ID_DocPendiente, Decimal VL_EnProceso, Decimal VL_Saldo, Decimal VL_Amortizado)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_ACTUALIZAR_DOC_PENDIENTES");

                VL_Database.AddInParameter(VL_Command, "@ID_DocPendiente", DbType.Decimal, VL_ID_DocPendiente);
                VL_Database.AddInParameter(VL_Command, "@EnProceso", DbType.Decimal, VL_EnProceso);
                VL_Database.AddInParameter(VL_Command, "@Saldo", DbType.Decimal, VL_Saldo);
                VL_Database.AddInParameter(VL_Command, "@Amortizado", DbType.Decimal, VL_Amortizado);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Caja_Chicas(String VL_Fecha_Inicio, String VL_Fecha_Fin, String VL_Emp)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Listar_derivar_CajaChica");
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.String, VL_Fecha_Inicio);
                VL_Database.AddInParameter(VL_Command, "@FECHAFIN", DbType.String, VL_Fecha_Fin);
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, VL_Emp);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Valorizacion(String VL_Fecha_Inicio, String VL_Fecha_Fin, String VL_Emp)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Listar_Valorizacion");
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.String, VL_Fecha_Inicio);
                VL_Database.AddInParameter(VL_Command, "@FECHAFIN", DbType.String, VL_Fecha_Fin);
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, VL_Emp);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        //PLANILLAS
        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Planillas(String VL_Fecha_Inicio, String VL_Fecha_Fin, String VL_Emp)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Listar_Planillas");
                VL_Database.AddInParameter(VL_Command, "@FECHAINICIO", DbType.String, VL_Fecha_Inicio);
                VL_Database.AddInParameter(VL_Command, "@FECHAFIN", DbType.String, VL_Fecha_Fin);
                VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.String, VL_Emp);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_Doc_Pendientes(String VL_ID_DocPendiente, String VL_Estado)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Actualizar_Estado_Doc_Pendiente");

                VL_Database.AddInParameter(VL_Command, "@ID_DocPendiente", DbType.String, VL_ID_DocPendiente);
                VL_Database.AddInParameter(VL_Command, "@Estado", DbType.String, VL_Estado);
              
                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Revisado_Doc_Pendientes(String VL_ID_DocPendiente, String VL_RevisadoDetraccion, String VL_RevisadoRetencion)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Actualizar_Revisado_DOCPENDIENTE");

                VL_Database.AddInParameter(VL_Command, "@ID_DocPendiente", DbType.String, VL_ID_DocPendiente);
                VL_Database.AddInParameter(VL_Command, "@RevisadoDetraccion", DbType.String, VL_RevisadoDetraccion);
                VL_Database.AddInParameter(VL_Command, "@RevisadoRetencion", DbType.String, VL_RevisadoRetencion);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;

        }

        //Facturas
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_detalle_Factura(Decimal VL_ID_PD, Decimal VL_ID_dc, Decimal VL_Importe)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_INSERTAR_PROGRAMACION_DOC_COMPRA");


                VL_Database.AddInParameter(VL_Command, "@id_prod_pago_det", DbType.Decimal, VL_ID_PD);
                VL_Database.AddInParameter(VL_Command, "@id_doc_compra", DbType.Decimal, VL_ID_dc);
                VL_Database.AddInParameter(VL_Command, "@importe_programado", DbType.Decimal, VL_Importe);
                

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;
        }

        //vb

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Insertar_Visto_Bueno(Decimal VL_usuario, Decimal VL_Tip_Doc, Decimal VL_Id_docpendiente)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_VistoBueno");


                VL_Database.AddInParameter(VL_Command, "@usuario", DbType.Decimal, VL_usuario);
                VL_Database.AddInParameter(VL_Command, "@docorigen", DbType.Decimal, VL_Tip_Doc);
                VL_Database.AddInParameter(VL_Command, "@ID_DOCPENDIENTE", DbType.Decimal, VL_Id_docpendiente);


                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (SqlException  ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Errors[0].Message;
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;

        }

        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_ID_USUARIO(String VL_Emp, String VL_SisCodigo, String VL_UsuCodigo)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Comun.USP_Usuario_id");
                VL_Database.AddInParameter(VL_Command, "@EmpCodigo", DbType.String, VL_Emp);
                VL_Database.AddInParameter(VL_Command, "@SisCodigo", DbType.String, VL_SisCodigo);
                VL_Database.AddInParameter(VL_Command, "@UsuCodigo", DbType.String, VL_UsuCodigo);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        public CapaEntidad.BeanResultado.ResultadoSelect Fnc_Listar_Validacion(String ID_DOCUMENTO, String ID_DOCPENDIENTE)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSelect VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSelect();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_vbSINO");
                VL_Database.AddInParameter(VL_Command, "@ID_DOCUMENTO", DbType.String, ID_DOCUMENTO);
                VL_Database.AddInParameter(VL_Command, "@ID_DOCPENDIENTE", DbType.String, ID_DOCPENDIENTE);
                

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
    }
}
