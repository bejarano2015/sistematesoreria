﻿using CapaEntidad;
using CapaEntidad.Proceso;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Procesos
{
    public class DAOValorizacionesContrato
    {
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Valorizacioncontrato(BeanValorizacionContrato VL_BeanValorizacionContrato)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();
            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[USP_VALORIZACIONESCONTRATO_INSERTAR]");
                //VL_Database.AddInParameter(VL_Command, "@OPCION", DbType.String, beanConsolidado.opcion);
                VL_Database.AddInParameter(VL_Command, "@Contrato_Subcontratista_Co_Contrato", DbType.String, VL_BeanValorizacionContrato.Contrato_Subcontratista_Co_Contrato);
                VL_Database.AddInParameter(VL_Command, "@Co_RegistroDocumento", DbType.String, VL_BeanValorizacionContrato.Co_RegistroDocumento);
                VL_Database.AddInParameter(VL_Command, "@Nu_Valorizacion", DbType.String, VL_BeanValorizacionContrato.Nu_Valorizacion);
                VL_Database.AddInParameter(VL_Command, "@Ss_MontoValorizacion", DbType.Int32, VL_BeanValorizacionContrato.Ss_MontoValorizacion);
                VL_Database.AddInParameter(VL_Command, "@Ss_MontoAdelanto", DbType.Decimal, VL_BeanValorizacionContrato.Ss_MontoAdelanto);
                VL_Database.AddInParameter(VL_Command, "@Ss_MontoDescuento", DbType.String, VL_BeanValorizacionContrato.Ss_MontoDescuento);
                VL_Database.AddInParameter(VL_Command, "@Ss_MontoAmortizacion", DbType.String, VL_BeanValorizacionContrato.Ss_MontoAmortizacion);
                VL_Database.AddInParameter(VL_Command, "@Ss_SCAR", DbType.String, VL_BeanValorizacionContrato.Ss_SCAR);
                VL_Database.AddInParameter(VL_Command, "@Ss_FacturaSubtotal", DbType.String, VL_BeanValorizacionContrato.Ss_FacturaSubtotal);
                VL_Database.AddInParameter(VL_Command, "@Ss_FacturaIgv", DbType.String, VL_BeanValorizacionContrato.Ss_FacturaIgv);
                VL_Database.AddInParameter(VL_Command, "@Ss_FacturaTotal", DbType.String, VL_BeanValorizacionContrato.Ss_FacturaTotal);
                VL_Database.AddInParameter(VL_Command, "@Ss_NetoPagar", DbType.String, VL_BeanValorizacionContrato.Ss_NetoPagar);
                VL_Database.AddInParameter(VL_Command, "@usuario", DbType.String, VL_BeanValorizacionContrato.usuario);
                VL_Database.AddInParameter(VL_Command, "@Nu_Semana", DbType.String, VL_BeanValorizacionContrato.Nu_Semana);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_listar_Valorizacionescontrato(int vl_Co_contrato)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Tesoreria].[Usp_VALORIZACIONESCONTRATO_LISTAR]");
                VL_Database.AddInParameter(VL_Command, "@Contrato_Subcontratista_Co_Contrato", DbType.Int32, vl_Co_contrato);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
    }
}
