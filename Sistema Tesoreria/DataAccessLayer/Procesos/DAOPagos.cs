﻿using System.Text;
using System;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;
using CapaEntidad.Proceso;


namespace DataAccessLayer.Procesos
{
    public class DAOPagos
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Pagos(String VL_IdCorrelativoCajaBanco, String VL_ID_DocPendiente, String VL_Empresa)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();
            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Insertar_Pagos");

                VL_Database.AddInParameter(VL_Command, "@IdCorrelativoCajaBanco", DbType.String, VL_IdCorrelativoCajaBanco);
                VL_Database.AddInParameter(VL_Command, "@ID_DocPendiente", DbType.String, VL_ID_DocPendiente);
                VL_Database.AddInParameter(VL_Command, "@ID_EMPRESA", DbType.String, VL_Empresa);

                
                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
               // VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

    }
}
