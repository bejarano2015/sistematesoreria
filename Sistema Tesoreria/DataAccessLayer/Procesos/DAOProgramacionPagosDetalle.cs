﻿using System.Text;
using System;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;
using CapaEntidad.Proceso;

namespace DataAccessLayer.Procesos
{
    public class DAOProgramacionPagosDetalle
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Programacion_Pagos_Detalle(BeanProgramacionPagosDetalle objProgramacionPagosDetalle)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_INSERTAR_PROGRAMACIONPAGODETALLE");

               // VL_Database.AddInParameter(VL_Command, "@ID_PADRE_D", DbType.Int32, objProgramacionPagosDetalle.ID_PADRE_D);
                VL_Database.AddInParameter(VL_Command, "@ID_PROGPAGO", DbType.Decimal, objProgramacionPagosDetalle.ID_PROGPAGO);
                VL_Database.AddInParameter(VL_Command, "@ID_DOC_ORIGEN", DbType.Decimal, objProgramacionPagosDetalle.ID_DOC_ORIGEN);
                VL_Database.AddInParameter(VL_Command, "@SERIE_DOC_ORIGEN", DbType.String, objProgramacionPagosDetalle.SERIE_DOC_ORIGEN);
                VL_Database.AddInParameter(VL_Command, "@NRO_DOC_ORIGEN", DbType.String, objProgramacionPagosDetalle.NRO_DOC_ORIGEN);
                VL_Database.AddInParameter(VL_Command, "@ID_MONEDA", DbType.String, objProgramacionPagosDetalle.ID_MONEDA);
                VL_Database.AddInParameter(VL_Command, "@IMPORTE", DbType.Decimal, objProgramacionPagosDetalle.IMPORTE);
                VL_Database.AddInParameter(VL_Command, "@ESTADO", DbType.Int32, objProgramacionPagosDetalle.ESTADO);
                VL_Database.AddInParameter(VL_Command, "@TOTALBRUTO", DbType.Decimal, objProgramacionPagosDetalle.TOTALBRUTO);
                //VL_Database.AddInParameter(VL_Command, "@ID_PADRE_D", DbType.Decimal, objProgramacionPagosDetalle.ID_PADRE_D);


                VL_BeanResultado.ParametroOutPut3 = Convert.ToInt32(VL_Database.ExecuteScalar(VL_Command, VL_Transact));

                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_ProgramacionPagosDetalle(String VL_ID_PROGPAGO)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Listar_Programacion_Pago_Detalle");
                VL_Database.AddInParameter(VL_Command, "@ID_PROGPAGO", DbType.String, VL_ID_PROGPAGO);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Estado_Programacion_Pagos_Detalle(String VL_ID_PROPAGO_DET)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {


                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_ELIMINAR_PPD");

                VL_Database.AddInParameter(VL_Command, "@ID_PROPAGO_DET", DbType.String, VL_ID_PROPAGO_DET);




                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        // LISTA PARA LLENAR CAJA Y BANCOS

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_PPD(String id_empresa)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Contabilidad].[USP_Listar_PPD]");
                VL_Database.AddInParameter(VL_Command, "@ID_Empresa", DbType.String, id_empresa);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_FormaPago( )
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Listar_FormaPago");
                

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_ActualizarFormaPago_PPD(String VL_ID_PROGPAGO, String VL_ID_FORMA_PAGO)
        {   
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Actualizar_Programacion_Pago_PPD");

                VL_Database.AddInParameter(VL_Command, "@ID_PROPAGO_DET", DbType.String, VL_ID_PROGPAGO);
                VL_Database.AddInParameter(VL_Command, "@ID_FORMA_PAGO", DbType.String, VL_ID_FORMA_PAGO);
                


                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_PPD(String VL_ID_PROGPAGO, String VL_ESTADO)
        {   
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Actualizar_Estado_PPD");

                VL_Database.AddInParameter(VL_Command, "@ID_PROPAGO_DET", DbType.String, VL_ID_PROGPAGO);
                VL_Database.AddInParameter(VL_Command, "@ESTADO", DbType.String, VL_ESTADO);
                


                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        
    }
}
