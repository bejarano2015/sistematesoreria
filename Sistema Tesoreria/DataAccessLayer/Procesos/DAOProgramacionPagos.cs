﻿using System.Text;
using System;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Common;
using System.Data;
using System.Data.Common;
using CapaDatos;
using CapaEntidad;
using CapaEntidad.Proceso;

namespace DataAccessLayer.Procesos
{
    public class DAOProgramacionPagos
    {
        CapaDatos.DaoGlobal VL_VarGlobal = new CapaDatos.DaoGlobal();

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Programacion_Pagos(BeanProgramacionPagos objProgramacionPagos)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new  BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_Insertar_ProgramacionPago");

                VL_Database.AddInParameter(VL_Command, "@ID_PADRE", DbType.Decimal, objProgramacionPagos.ID_PADRE);
                VL_Database.AddInParameter(VL_Command, "@Fecha", DbType.DateTime, objProgramacionPagos.Fecha);
                VL_Database.AddInParameter(VL_Command, "@vigencia", DbType.DateTime, objProgramacionPagos.Vigencia);
                VL_Database.AddInParameter(VL_Command, "@Id_moneda", DbType.String, objProgramacionPagos.Id_moneda);
                VL_Database.AddInParameter(VL_Command, "@Total", DbType.String, objProgramacionPagos.Total);
                VL_Database.AddInParameter(VL_Command, "@Estado", DbType.String, objProgramacionPagos.Estado);

                VL_BeanResultado.ParametroOutPut4 = Convert.ToInt32(VL_Database.ExecuteScalar(VL_Command, VL_Transact));

                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Programacion_Pagos(String VL_id_estado, DateTime VL_FECHA_INICIO, DateTime VL_FECHA_FIN)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("contabilidad.USP_Programacion_Pago");
                VL_Database.AddInParameter(VL_Command, "@id_estado", DbType.String, VL_id_estado);
                VL_Database.AddInParameter(VL_Command, "@fechainicio", DbType.Date, VL_FECHA_INICIO);
                VL_Database.AddInParameter(VL_Command, "@fechafin  ", DbType.Date, VL_FECHA_FIN);


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Estado_Programacion_Pagos(BeanProgramacionPagos objProgramacionPagos)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Contabilidad.USP_ACTUALIZAR_ESTADO_PP");

                VL_Database.AddInParameter(VL_Command, "@ID_PROPAGO", DbType.String, objProgramacionPagos.ID_PROGPAGO);
                VL_Database.AddInParameter(VL_Command, "@ID_estado", DbType.String, objProgramacionPagos.Estado);
                

                VL_BeanResultado.ParametroOutPut3 = Convert.ToDecimal(VL_Database.ExecuteScalar(VL_Command, VL_Transact));

                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Estado_Programacion_Pagos(BeanProgramacionPagos objProgramacionPagos)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_ELIMINAR_PP");

                VL_Database.AddInParameter(VL_Command, "@ID_PROGPAGO", DbType.Decimal, objProgramacionPagos.ID_PADRE);
                

                VL_BeanResultado.ParametroOutPut3 = Convert.ToDecimal(VL_Database.ExecuteScalar(VL_Command, VL_Transact));

                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Actualizar_Total_Programacion_Pagos(String VL_ID_PADRE, String VL_TOTAL)
        {

            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_ACTUALIZAR_TOTAL_PP");

                VL_Database.AddInParameter(VL_Command, "@ID_PROGPAGO", DbType.Decimal, VL_ID_PADRE);
                VL_Database.AddInParameter(VL_Command, "@TOTAL", DbType.Decimal, VL_TOTAL);

                VL_BeanResultado.ParametroOutPut3 = Convert.ToDecimal(VL_Database.ExecuteScalar(VL_Command, VL_Transact));

                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Eliminar_Programacion_Pagos(String VL_ID_PROGPAGO)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_ELIMINAR_PP");

                VL_Database.AddInParameter(VL_Command, "@ID_PROGPAGO", DbType.String, VL_ID_PROGPAGO);
                

                VL_BeanResultado.ParametroOutPut3 = Convert.ToDecimal(VL_Database.ExecuteScalar(VL_Command, VL_Transact));

                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                //VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {

                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Listar_Estado_PP()
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Listar_Estado_PP");

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {

                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Listar_Combos_Varios_Reportes(String id)
        {
          
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(VL_VarGlobal.BdConectionString());
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();


            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.Reporte_Cargar_combos");

                VL_Database.AddInParameter(VL_Command, "@id", DbType.String, id);

                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                
               // VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";


            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }


            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoSeleccion Fnc_Generar_Reportes(DateTime VL_FechaInicio, DateTime VL_FechaFin, String VL_Estado, String VL_Moneda, String VL_Empresa)
        {
            SqlDatabase VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoSeleccion();
            DbCommand VL_Command;

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("Tesoreria.USP_Reporte_Programacion_Pago");

                VL_Database.AddInParameter(VL_Command, "@fechainicio", DbType.DateTime, VL_FechaInicio);
                VL_Database.AddInParameter(VL_Command, "@fechafin", DbType.DateTime, VL_FechaFin);
                VL_Database.AddInParameter(VL_Command, "@idestado", DbType.String, VL_Estado);
                VL_Database.AddInParameter(VL_Command, "@moneda", DbType.String, VL_Moneda);
                VL_Database.AddInParameter(VL_Command, "@CodigoEmpr", DbType.String, VL_Empresa);

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables[0];
                VL_BeanResultado.blnExiste = true;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                VL_BeanResultado.blnExiste = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
            }
            finally
            {
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }
        

    }
}
