﻿using CapaEntidad;
using CapaEntidad.Proceso;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;


using System.Data;namespace DataAccessLayer.Procesos
{
   public  class DAOConsolidadoPartida
    {
        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_Consolidado(BeanConsolidado beanConsolidado)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_PARTIDASUBCONTRATA_INSERTAR");
                //VL_Database.AddInParameter(VL_Command, "@OPCION", DbType.String, beanConsolidado.opcion);
                VL_Database.AddInParameter(VL_Command, "@emprcodigo", DbType.String, beanConsolidado.EmprCodigo);
                VL_Database.AddInParameter(VL_Command, "@CCosCodigo", DbType.String, beanConsolidado.CCosCodigo);
                VL_Database.AddInParameter(VL_Command, "@UbtCodigo", DbType.String, beanConsolidado.UbtCodigo);
                VL_Database.AddInParameter(VL_Command, "@EspCodigo", DbType.String, beanConsolidado.EspCodigo);
                VL_Database.AddInParameter(VL_Command, "@TitCodigo", DbType.String, beanConsolidado.TitCodigo);
                VL_Database.AddInParameter(VL_Command, "@ParCodigo", DbType.String, beanConsolidado.ParCodigo);
                VL_Database.AddInParameter(VL_Command, "@ObraCodigo", DbType.String, beanConsolidado.ObraCodigo);
                VL_Database.AddInParameter(VL_Command, "@ConsCodigo", DbType.String, beanConsolidado.ConsCodigo);
                VL_Database.AddInParameter(VL_Command, "@UsuCreacion", DbType.String, beanConsolidado.UsuCreacion);
                VL_Database.AddInParameter(VL_Command, "@UMedida", DbType.String, beanConsolidado.UMedida);

                 VL_Database.AddInParameter(VL_Command, "@Metrado", DbType.String, beanConsolidado.Metrado);
                VL_Database.AddInParameter(VL_Command, "@PrecioPresupuesto", DbType.String, beanConsolidado.PrecioPresupuesto);
                VL_Database.AddInParameter(VL_Command, "@PrecioSubcontrato", DbType.String, beanConsolidado.PrecioSubcontrato);
                VL_Database.AddInParameter(VL_Command, "@MonCodigo", DbType.String, beanConsolidado.MonCodigo);//'@MonCodigo', 
                VL_Database.AddInParameter(VL_Command, "@PtiCodigo", DbType.String, beanConsolidado.PtiCodigo);
                VL_Database.AddInParameter(VL_Command, "@ItemPresupuesto", DbType.String, beanConsolidado.ItemPresupuesto);
                VL_Database.AddInParameter(VL_Command, "@SpptoCodigo", DbType.String, beanConsolidado.SpptoCodigo);//@SpptoCodigo
                VL_Database.AddInParameter(VL_Command, "@TipoPartida", DbType.String, beanConsolidado.TipoPartida);
                VL_Database.AddInParameter(VL_Command, "@N4Codigo", DbType.String, beanConsolidado.N4Codigo);


                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }


        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Registrar_PartidaContrato(BeanConsolidado beanConsolidado)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();
            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("[Logistica].[USP_CONSOLIDADO_INSERTAR]");
                //VL_Database.AddInParameter(VL_Command, "@OPCION", DbType.String, beanConsolidado.opcion);
                VL_Database.AddInParameter(VL_Command, "@emprcodigo", DbType.String, beanConsolidado.EmprCodigo);
                VL_Database.AddInParameter(VL_Command, "@CCosCodigo", DbType.String, beanConsolidado.CCosCodigo);
                VL_Database.AddInParameter(VL_Command, "@UbtCodigo", DbType.String, beanConsolidado.UbtCodigo);
                VL_Database.AddInParameter(VL_Command, "@IdPartida", DbType.String, beanConsolidado.ParCodigo);
                VL_Database.AddInParameter(VL_Command, "@periodo", DbType.String, beanConsolidado.Periodo);
                VL_Database.AddInParameter(VL_Command, "@usucodigo", DbType.String, beanConsolidado.UsuCreacion );

                VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                //VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";
            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }
            return VL_BeanResultado;
        }

        public CapaEntidad.BeanResultado.ResultadoTransaccion Fnc_Limpiar_Consolidado(BeanConsolidado beanConsolidado)
        {
            SqlDatabase VL_Database;
            DbCommand VL_Command;
            DbConnection VL_Connection;
            DbTransaction VL_Transact;
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new CapaEntidad.BeanResultado.ResultadoTransaccion();

            VL_Database = new SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING);
            VL_Connection = VL_Database.CreateConnection();
            VL_Connection.Open();
            VL_Transact = VL_Connection.BeginTransaction();

            try
            {
                VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_PARTIDASUBCONTRATA_LIMPIAR");
                //VL_Database.AddInParameter(VL_Command, "@OPCION", DbType.String, beanConsolidado.opcion);
                VL_Database.AddInParameter(VL_Command, "@emprcodigo", DbType.String, beanConsolidado.EmprCodigo);
                VL_Database.AddInParameter(VL_Command, "@CCosCodigo", DbType.String, beanConsolidado.CCosCodigo);
                VL_Database.AddInParameter(VL_Command, "@UbtCodigo", DbType.String, beanConsolidado.UbtCodigo);
                VL_Database.AddInParameter(VL_Command, "@UsuCreacion", DbType.String, beanConsolidado.UsuCreacion);


                //VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command, VL_Transact).Tables[0];
                VL_Database.ExecuteNonQuery(VL_Command, VL_Transact);
                VL_Transact.Commit();
                VL_BeanResultado.blnResultado = true;
                VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!";

            }
            catch (Exception ex)
            {
                VL_BeanResultado.blnResultado = false;
                VL_BeanResultado.strMensaje = ex.Message.ToString();
                VL_Transact.Rollback();
            }
            finally
            {
                if (VL_Connection.State == ConnectionState.Open)
                {
                    VL_Connection.Close();
                }
                VL_Database = null;
                VL_Command = null;
            }

            return VL_BeanResultado;

        }
    }
}
