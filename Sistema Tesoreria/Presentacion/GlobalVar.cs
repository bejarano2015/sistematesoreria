﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;
using System.Data;


namespace GestionInmobiliario
{
	public static class GlobalVar
	{
		#region "Logueo"

		public static DataTable DtPermisos= new DataTable();
        public static String VG_NICKNAME;
        public static String VG_NOMBREUSUARIO;
        public static String VG_USUA_CODIGO;
        public static String VG_PERFIL;
        public static bool VG_Permiso;
		public static int VG_Creacion;
		public static int VG_Edicion;
		public static int VG_Eliminacion;
		public static int VG_Impresion;

		#endregion



        #region "Variables Generales"

        public static string VG_OBRA_CODIGO = "";
        public static string VG_OBRA_DESCRIPCION = "";
        public static string VG_SIST_CODIGO = "01";

        #endregion

        public const string VG_MensajeSistema = "Sistema Inmobiliario";
		public static bool GlobalBoolean;
		public static string VG_Pc = IPGlobalProperties.GetIPGlobalProperties().HostName;

	}
}
