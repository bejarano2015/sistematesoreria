﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using CapaNegocios;
using DataAccessLayer.Mantenimiento;
using DataAccessLayer.Globales;
using BusinessLogicLayer.Globales;
using BusinessLogicLayer.Mantenimiento;
using CapaEntidad;


namespace Presentacion
{
    public partial class Form1 : Form
    {

        BeanEntregaRendir objEntregaRendir;
        public Form1()
        {
            InitializeComponent();
        }

        #region "Listar"


        private void ListarEmpresas()
        {
            SrvEntregaRendir VL_SrvEntregaRendir = new SrvEntregaRendir();
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            try
            {
                VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Empresas();

                if (VL_BeanResultado.blnExiste == true)
                {
                    cboEmpresa.DataSource = VL_BeanResultado.dtResultado;
                    cboEmpresa.DisplayMember = "EmprDescripcion";
                    cboEmpresa.ValueMember = "EmprCodigo";

                    //cboEmpresa.SelectedIndex = 0;
                    //cboObras.SelectedText = cboObras.SelectedItem[0].;

                }
                else
                {

                    MessageBox.Show(VL_BeanResultado.strMensaje);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }

        private void ListarArea()
        {
            SrvEntregaRendir VL_SrvEntregaRendir = new SrvEntregaRendir();
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();
            objEntregaRendir = new BeanEntregaRendir(cboEmpresa.SelectedValue.ToString());

            try
            {
                VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_AreaPorEmpresa(objEntregaRendir);

                if (VL_BeanResultado.blnExiste == true)
                {
                    cboArea.DataSource = VL_BeanResultado.dtResultado;
                    cboArea.DisplayMember = "AreaNombre";
                    cboArea.ValueMember = "AreaCodigo";

                    //cboObras.SelectedText = cboObras.SelectedItem[0].;

                }
                else
                {

                    MessageBox.Show(VL_BeanResultado.strMensaje);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }

        private void ListarObras()
        {
            SrvEntregaRendir VL_SrvEntregaRendir = new SrvEntregaRendir();
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();
            objEntregaRendir = new BeanEntregaRendir(cboEmpresa.SelectedValue.ToString());

            try
            {
                VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Obras(objEntregaRendir);

                if (VL_BeanResultado.blnExiste == true)
                {
                    cboObraArea.DataSource = VL_BeanResultado.dtResultado;
                    cboObraArea.DisplayMember = "ObraDescripcion";
                    cboObraArea.ValueMember = "ObraCodigo";

                    //cboObras.SelectedText = cboObras.SelectedItem[0].;

                }
                else
                {

                    MessageBox.Show(VL_BeanResultado.strMensaje);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }

        private void ListarPadron(System.Windows.Forms.ComboBox cboReferencia)
        {
            SrvEntregaRendir VL_SrvEntregaRendir = new SrvEntregaRendir();
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();
            objEntregaRendir = new BeanEntregaRendir(cboEmpresa.SelectedValue.ToString(), cboArea.SelectedValue.ToString());

            try
            {
                VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Empleados(objEntregaRendir);

                if (VL_BeanResultado.blnExiste == true)
                {
                    cboReferencia.DataSource = VL_BeanResultado.dtResultado;
                    cboReferencia.DisplayMember = "Nombres";
                    cboReferencia.ValueMember = "PadCodigo";

                    //cboObras.SelectedText = cboObras.SelectedItem[0].;

                }
                else
                {

                    MessageBox.Show(VL_BeanResultado.strMensaje);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }

        private void ListarAutoriza()
        {
            SrvEntregaRendir VL_SrvEntregaRendir = new SrvEntregaRendir();
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            try
            {
                VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Autoriza();

                if (VL_BeanResultado.blnExiste == true)
                {
                    cboAutoriza.DataSource = VL_BeanResultado.dtResultado;
                    cboAutoriza.DisplayMember = "Nombres";
                    cboAutoriza.ValueMember = "PadCodigo";

                    //cboEmpresa.SelectedIndex = 0;
                    //cboObras.SelectedText = cboObras.SelectedItem[0].;

                }
                else
                {

                    MessageBox.Show(VL_BeanResultado.strMensaje);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
        }

        private void ListarMonedas()
        {
            SrvEntregaRendir VL_SrvEntregaRendir = new SrvEntregaRendir();
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            try
            {
                VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Monedas();

                if (VL_BeanResultado.blnExiste == true)
                {
                    cboMoneda.DataSource = VL_BeanResultado.dtResultado;
                    cboMoneda.DisplayMember = "MonDescripcion";
                    cboMoneda.ValueMember = "MonCodigo";

                    //cboObras.SelectedText = cboObras.SelectedItem[0].;

                }
                else
                {

                    MessageBox.Show(VL_BeanResultado.strMensaje);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }

        private void ListarEstado()
        {
            SrvEntregaRendir VL_SrvEntregaRendir = new SrvEntregaRendir();
            BeanResultado.ResultadoSeleccion VL_BeanResultado = new BeanResultado.ResultadoSeleccion();

            try
            {
                VL_BeanResultado = VL_SrvEntregaRendir.Fnc_Listar_Estado_EntregaRendir();

                if (VL_BeanResultado.blnExiste == true)
                {
                    cboEstado.DataSource = VL_BeanResultado.dtResultado;
                    cboEstado.DisplayMember = "Estado";
                    cboEstado.ValueMember = "ID_ESTADO";

                    //cboObras.SelectedText = cboObras.SelectedItem[0].;

                }
                else
                {

                    MessageBox.Show(VL_BeanResultado.strMensaje);

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }


        }


        #endregion

        #region "procesos"

        private void RegistrarEntregaRendir()
        {

            SrvEntregaRendir VL_SrvProgramacion = new SrvEntregaRendir();
            BeanResultado.ResultadoTransaccion VL_BeanResultado = new BeanResultado.ResultadoTransaccion();

            objEntregaRendir = new BeanEntregaRendir(
                cboEmpresa.SelectedValue.ToString()
                , cboSolicitante.SelectedValue.ToString()
                , cboArea.SelectedValue.ToString()
                , cboMoneda.SelectedValue.ToString()
                , txtMonto.Text
                , txtGlosa.Text
                , cboEstado.SelectedValue.ToString(), dtpFecha.Value
                , cboAutoriza.SelectedValue.ToString()
                , cboGirado.SelectedValue.ToString()
                , cboObraArea.SelectedValue.ToString());



            VL_BeanResultado = VL_SrvProgramacion.Fnc_Registrar_Entrega_Rendir(objEntregaRendir);

            if (VL_BeanResultado.blnResultado == false)
            {
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria");
                return;
            }
            else
            {
                MessageBox.Show(VL_BeanResultado.strMensaje, "Sistema Tesoreria");
                return;
            }

        }

        #endregion







        private void Form1_Load(object sender, EventArgs e)
        {
            ListarEmpresas();
            ListarAutoriza();
            ListarEstado();
            ListarMonedas();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            RegistrarEntregaRendir();
        }

     

        private void cboEmpresa_SelectedValueChanged_1(object sender, EventArgs e)
        {
            ListarArea();
            ListarObras();
        }

        private void cboArea_SelectedValueChanged(object sender, EventArgs e)
        {
            ListarPadron(cboSolicitante);
            ListarPadron(cboGirado);
        
        }

        private void cboArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ListarPadron(cboSolicitante);
            //ListarPadron(cboGirado);
        }
    }
}
