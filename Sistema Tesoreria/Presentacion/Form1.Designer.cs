﻿namespace Presentacion
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cboArea = new ctrLibreria.Controles.BeComboBox();
            this.beLabel1 = new ctrLibreria.Controles.BeLabel();
            this.cboSolicitante = new ctrLibreria.Controles.BeComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboMoneda = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboEstado = new System.Windows.Forms.ComboBox();
            this.dtpFecha = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMonto = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtGlosa = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cboAutoriza = new ctrLibreria.Controles.BeComboBox();
            this.cboGirado = new ctrLibreria.Controles.BeComboBox();
            this.cboObraArea = new ctrLibreria.Controles.BeComboBox();
            this.beLabel2 = new ctrLibreria.Controles.BeLabel();
            this.cboEmpresa = new ctrLibreria.Controles.BeComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.beComboBox1 = new ctrLibreria.Controles.BeComboBox();
            this.beComboBox2 = new ctrLibreria.Controles.BeComboBox();
            this.beComboBox3 = new ctrLibreria.Controles.BeComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.beLabel3 = new ctrLibreria.Controles.BeLabel();
            this.label14 = new System.Windows.Forms.Label();
            this.beDataGridView1 = new ctrLibreria.Controles.BeDataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.beDataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cboArea
            // 
            this.cboArea.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.cboArea.BackColor = System.Drawing.Color.Ivory;
            this.cboArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboArea.ForeColor = System.Drawing.Color.Black;
            this.cboArea.FormattingEnabled = true;
            this.cboArea.KeyEnter = true;
            this.cboArea.Location = new System.Drawing.Point(224, 166);
            this.cboArea.Name = "cboArea";
            this.cboArea.Size = new System.Drawing.Size(251, 21);
            this.cboArea.TabIndex = 0;
            this.cboArea.SelectedIndexChanged += new System.EventHandler(this.cboArea_SelectedIndexChanged);
            this.cboArea.SelectedValueChanged += new System.EventHandler(this.cboArea_SelectedValueChanged);
            // 
            // beLabel1
            // 
            this.beLabel1.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01;
            this.beLabel1.AutoSize = true;
            this.beLabel1.BackColor = System.Drawing.Color.Transparent;
            this.beLabel1.ForeColor = System.Drawing.Color.Black;
            this.beLabel1.Location = new System.Drawing.Point(161, 174);
            this.beLabel1.Name = "beLabel1";
            this.beLabel1.Size = new System.Drawing.Size(29, 13);
            this.beLabel1.TabIndex = 1;
            this.beLabel1.Text = "Area";
            // 
            // cboSolicitante
            // 
            this.cboSolicitante.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.cboSolicitante.BackColor = System.Drawing.Color.Ivory;
            this.cboSolicitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSolicitante.ForeColor = System.Drawing.Color.Black;
            this.cboSolicitante.FormattingEnabled = true;
            this.cboSolicitante.KeyEnter = true;
            this.cboSolicitante.Location = new System.Drawing.Point(224, 247);
            this.cboSolicitante.Name = "cboSolicitante";
            this.cboSolicitante.Size = new System.Drawing.Size(251, 21);
            this.cboSolicitante.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Empresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(161, 250);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Solicitante";
            // 
            // cboMoneda
            // 
            this.cboMoneda.FormattingEnabled = true;
            this.cboMoneda.Location = new System.Drawing.Point(281, 344);
            this.cboMoneda.Name = "cboMoneda";
            this.cboMoneda.Size = new System.Drawing.Size(121, 21);
            this.cboMoneda.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(221, 351);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Moneda";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Codigo";
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(89, 55);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(100, 20);
            this.txtCodigo.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(221, 390);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Estado";
            // 
            // cboEstado
            // 
            this.cboEstado.FormattingEnabled = true;
            this.cboEstado.Location = new System.Drawing.Point(281, 382);
            this.cboEstado.Name = "cboEstado";
            this.cboEstado.Size = new System.Drawing.Size(121, 21);
            this.cboEstado.TabIndex = 4;
            // 
            // dtpFecha
            // 
            this.dtpFecha.Location = new System.Drawing.Point(63, 81);
            this.dtpFecha.Name = "dtpFecha";
            this.dtpFecha.Size = new System.Drawing.Size(200, 20);
            this.dtpFecha.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "label6";
            // 
            // txtMonto
            // 
            this.txtMonto.Location = new System.Drawing.Point(345, 29);
            this.txtMonto.Name = "txtMonto";
            this.txtMonto.Size = new System.Drawing.Size(100, 20);
            this.txtMonto.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(302, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Monto";
            // 
            // txtGlosa
            // 
            this.txtGlosa.Location = new System.Drawing.Point(42, 338);
            this.txtGlosa.Multiline = true;
            this.txtGlosa.Name = "txtGlosa";
            this.txtGlosa.Size = new System.Drawing.Size(162, 82);
            this.txtGlosa.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(39, 309);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Glosa";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(53, 204);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 12;
            this.btnGuardar.Text = "Guadar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(161, 212);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Autoriza";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(167, 316);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Girado a ";
            // 
            // cboAutoriza
            // 
            this.cboAutoriza.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.cboAutoriza.BackColor = System.Drawing.Color.Ivory;
            this.cboAutoriza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAutoriza.ForeColor = System.Drawing.Color.Black;
            this.cboAutoriza.FormattingEnabled = true;
            this.cboAutoriza.KeyEnter = true;
            this.cboAutoriza.Location = new System.Drawing.Point(224, 209);
            this.cboAutoriza.Name = "cboAutoriza";
            this.cboAutoriza.Size = new System.Drawing.Size(251, 21);
            this.cboAutoriza.TabIndex = 2;
            // 
            // cboGirado
            // 
            this.cboGirado.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.cboGirado.BackColor = System.Drawing.Color.Ivory;
            this.cboGirado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGirado.ForeColor = System.Drawing.Color.Black;
            this.cboGirado.FormattingEnabled = true;
            this.cboGirado.KeyEnter = true;
            this.cboGirado.Location = new System.Drawing.Point(223, 308);
            this.cboGirado.Name = "cboGirado";
            this.cboGirado.Size = new System.Drawing.Size(251, 21);
            this.cboGirado.TabIndex = 2;
            // 
            // cboObraArea
            // 
            this.cboObraArea.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.cboObraArea.BackColor = System.Drawing.Color.Ivory;
            this.cboObraArea.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboObraArea.ForeColor = System.Drawing.Color.Black;
            this.cboObraArea.FormattingEnabled = true;
            this.cboObraArea.KeyEnter = true;
            this.cboObraArea.Location = new System.Drawing.Point(224, 274);
            this.cboObraArea.Name = "cboObraArea";
            this.cboObraArea.Size = new System.Drawing.Size(251, 21);
            this.cboObraArea.TabIndex = 0;
            // 
            // beLabel2
            // 
            this.beLabel2.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01;
            this.beLabel2.AutoSize = true;
            this.beLabel2.BackColor = System.Drawing.Color.Transparent;
            this.beLabel2.ForeColor = System.Drawing.Color.Black;
            this.beLabel2.Location = new System.Drawing.Point(158, 282);
            this.beLabel2.Name = "beLabel2";
            this.beLabel2.Size = new System.Drawing.Size(62, 13);
            this.beLabel2.TabIndex = 1;
            this.beLabel2.Text = "Obras/Area";
            // 
            // cboEmpresa
            // 
            this.cboEmpresa.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.cboEmpresa.BackColor = System.Drawing.Color.Ivory;
            this.cboEmpresa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEmpresa.ForeColor = System.Drawing.Color.Black;
            this.cboEmpresa.FormattingEnabled = true;
            this.cboEmpresa.KeyEnter = true;
            this.cboEmpresa.Location = new System.Drawing.Point(224, 117);
            this.cboEmpresa.Name = "cboEmpresa";
            this.cboEmpresa.Size = new System.Drawing.Size(251, 21);
            this.cboEmpresa.TabIndex = 13;
            this.cboEmpresa.SelectedValueChanged += new System.EventHandler(this.cboEmpresa_SelectedValueChanged_1);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(892, 64);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(654, 64);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 9;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(734, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "label6";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(980, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "label6";
            // 
            // beComboBox1
            // 
            this.beComboBox1.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.beComboBox1.BackColor = System.Drawing.Color.Ivory;
            this.beComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.beComboBox1.ForeColor = System.Drawing.Color.Black;
            this.beComboBox1.FormattingEnabled = true;
            this.beComboBox1.KeyEnter = true;
            this.beComboBox1.Location = new System.Drawing.Point(723, 126);
            this.beComboBox1.Name = "beComboBox1";
            this.beComboBox1.Size = new System.Drawing.Size(121, 21);
            this.beComboBox1.TabIndex = 14;
            // 
            // beComboBox2
            // 
            this.beComboBox2.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.beComboBox2.BackColor = System.Drawing.Color.Ivory;
            this.beComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.beComboBox2.ForeColor = System.Drawing.Color.Black;
            this.beComboBox2.FormattingEnabled = true;
            this.beComboBox2.KeyEnter = true;
            this.beComboBox2.Location = new System.Drawing.Point(723, 166);
            this.beComboBox2.Name = "beComboBox2";
            this.beComboBox2.Size = new System.Drawing.Size(121, 21);
            this.beComboBox2.TabIndex = 14;
            // 
            // beComboBox3
            // 
            this.beComboBox3.Apariencia = ctrLibreria.Controles.BeComboBox.FormaTipo.Skin01;
            this.beComboBox3.BackColor = System.Drawing.Color.Ivory;
            this.beComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.beComboBox3.ForeColor = System.Drawing.Color.Black;
            this.beComboBox3.FormattingEnabled = true;
            this.beComboBox3.KeyEnter = true;
            this.beComboBox3.Location = new System.Drawing.Point(723, 204);
            this.beComboBox3.Name = "beComboBox3";
            this.beComboBox3.Size = new System.Drawing.Size(121, 21);
            this.beComboBox3.TabIndex = 14;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(651, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "Empresa";
            // 
            // beLabel3
            // 
            this.beLabel3.Apariencia = ctrLibreria.Controles.BeLabel.FormaTipo.Skin01;
            this.beLabel3.AutoSize = true;
            this.beLabel3.BackColor = System.Drawing.Color.Transparent;
            this.beLabel3.ForeColor = System.Drawing.Color.Black;
            this.beLabel3.Location = new System.Drawing.Point(651, 174);
            this.beLabel3.Name = "beLabel3";
            this.beLabel3.Size = new System.Drawing.Size(62, 13);
            this.beLabel3.TabIndex = 1;
            this.beLabel3.Text = "Obras/Area";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(651, 209);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Solicitante";
            // 
            // beDataGridView1
            // 
            this.beDataGridView1.AllowUserToAddRows = false;
            this.beDataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Ivory;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.NavajoWhite;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.beDataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.beDataGridView1.Apariencia = ctrLibreria.Controles.BeDataGridView.FormaTipo.Skin01;
            this.beDataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.beDataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleVertical;
            this.beDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Cornsilk;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.NavajoWhite;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.beDataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.beDataGridView1.EnableHeadersVisualStyles = false;
            this.beDataGridView1.Location = new System.Drawing.Point(603, 295);
            this.beDataGridView1.MultiSelect = false;
            this.beDataGridView1.Name = "beDataGridView1";
            this.beDataGridView1.ReadOnly = true;
            this.beDataGridView1.RowHeadersVisible = false;
            this.beDataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.beDataGridView1.Size = new System.Drawing.Size(240, 150);
            this.beDataGridView1.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1117, 469);
            this.Controls.Add(this.beDataGridView1);
            this.Controls.Add(this.beComboBox3);
            this.Controls.Add(this.beComboBox2);
            this.Controls.Add(this.beComboBox1);
            this.Controls.Add(this.cboEmpresa);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.txtGlosa);
            this.Controls.Add(this.txtMonto);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.dtpFecha);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboEstado);
            this.Controls.Add(this.cboMoneda);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboGirado);
            this.Controls.Add(this.cboAutoriza);
            this.Controls.Add(this.cboSolicitante);
            this.Controls.Add(this.beLabel3);
            this.Controls.Add(this.beLabel2);
            this.Controls.Add(this.beLabel1);
            this.Controls.Add(this.cboObraArea);
            this.Controls.Add(this.cboArea);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.beDataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ctrLibreria.Controles.BeComboBox cboArea;
        private ctrLibreria.Controles.BeLabel beLabel1;
        private ctrLibreria.Controles.BeComboBox cboSolicitante;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboMoneda;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboEstado;
        private System.Windows.Forms.DateTimePicker dtpFecha;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMonto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtGlosa;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private ctrLibreria.Controles.BeComboBox cboAutoriza;
        private ctrLibreria.Controles.BeComboBox cboGirado;
        private ctrLibreria.Controles.BeComboBox cboObraArea;
        private ctrLibreria.Controles.BeLabel beLabel2;
        private ctrLibreria.Controles.BeComboBox cboEmpresa;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private ctrLibreria.Controles.BeComboBox beComboBox1;
        private ctrLibreria.Controles.BeComboBox beComboBox2;
        private ctrLibreria.Controles.BeComboBox beComboBox3;
        private System.Windows.Forms.Label label13;
        private ctrLibreria.Controles.BeLabel beLabel3;
        private System.Windows.Forms.Label label14;
        private ctrLibreria.Controles.BeDataGridView beDataGridView1;
    }
}

