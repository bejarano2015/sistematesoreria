﻿Imports System.Data
Imports System.Data.SqlClient
Public Class ClsOtros
    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"


    Public Function fListarNroreferenciaAlmacenIngreso(ByVal vOcoCodigo As String, ByVal vNroDocReferencia As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "[Inventario].[USP_LlenarCombos_Generico]"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Campos", DbType.String, 50, " NroDocReferencia,PrvRUC ,PrvRazonSocial ")
            cCapaDatos.sParametroSQL("@Tabla", DbType.String, 50, "Logistica.AlmacenIngreso")
            cCapaDatos.sParametroSQL("@FILTRO", DbType.String, 50, "OcoCodigo='" + vOcoCodigo + "' and   NroDocReferencia='" + vNroDocReferencia + "'")
            cCapaDatos.sParametroSQL("@Orden", DbType.String, 50, "")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function
    Public Function fObtenerIgv() As String
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "[Inventario].[USP_LlenarCombos_Generico]"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Campos", DbType.String, 50, " Abreviatura ")
            cCapaDatos.sParametroSQL("@Tabla", DbType.String, 50, "Comun.ParamImpuestos")
            cCapaDatos.sParametroSQL("@FILTRO", DbType.String, 50, "Codigo=''IGV'' AND GETDATE()  BETWEEN FechaIni AND FechaFin   ")
            cCapaDatos.sParametroSQL("@Orden", DbType.String, 50, "")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable.Rows(0)("Abreviatura")
    End Function
End Class
