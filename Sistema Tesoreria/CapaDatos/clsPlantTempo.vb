Public Class clsPlantTempo
    Private _Col01 As String
    Private _Col02 As String
    Private aLista As New ArrayList
    Private aListaOpcion As New ArrayList
    Private aListas As New ArrayList
    Private aLista2 As New ArrayList
    Private aLista3 As New ArrayList
    Private aLista4 As New ArrayList
    Private aLista5 As New ArrayList
    Public Const sMsgIncompleto As String = "Llenado Incompleto de Datos, pulse una tecla para continuar..."
    Public Const sMsgDuplicado As String = "Llenado Duplicado de Datos, pulse una tecla para continuar..."
    Public Const sMsgNoDisponible As String = "El Registro No se Encuentra Disponible, pulse una tecla para continuar..."
    Public Const sMsgEliminacion As String = "Anulacion o Eliminacion de Registro Completada, pulse una tecla para continuar..."
    Public Const sMsgGrabacion As String = "Proceso de Grabación Concluido, pulse una tecla para continuar..."
    Public Const sMsgNoGrabacion As String = "Proceso de Grabación Interrumpido, pulse una tecla para continuar..."

    Public Property Col01()
        Get
            Return _Col01
        End Get
        Set(ByVal value)
            _Col01 = value
        End Set
    End Property

    Public Property Col02()
        Get
            Return _Col02
        End Get
        Set(ByVal value)
            _Col02 = value
        End Set
    End Property

    Public Sub New()
        _Col01 = ""
        _Col02 = ""
    End Sub

    Public Sub New(ByVal Col01 As String, ByVal Col02 As String)
        _Col01 = Col01
        _Col02 = Col02
    End Sub

    Public Sub sAgregarLista(ByVal Col01 As String, ByVal Col02 As String)
        aLista.Add(New clsPlantTempo(Col01, Col02))
    End Sub

    Public Sub sAgregarListaOpcion(ByVal Col01 As String, ByVal Col02 As String)
        'aLista.Add(New clsPlantTempo(Col01, Col02))
        aListaOpcion.Add(New clsPlantTempo(Col01, Col02))
    End Sub

    Public Sub sAgregarListaRendir(ByVal Col01 As String, ByVal Col02 As String)
        aLista5.Add(New clsPlantTempo(Col01, Col02))
    End Sub

    Public Sub sAgregarListas(ByVal Col01 As String, ByVal Col02 As String)
        aListas.Add(New clsPlantTempo(Col01, Col02))
    End Sub

    Public Sub sAgregarLista2(ByVal Col01 As String, ByVal Col02 As String)
        aLista2.Add(New clsPlantTempo(Col01, Col02))
    End Sub

    Public Sub sAgregarLista4(ByVal Col01 As String, ByVal Col02 As String)
        aLista4.Add(New clsPlantTempo(Col01, Col02))
    End Sub

    Public Sub sAgregarLista3(ByVal Col01 As String, ByVal Col02 As String)
        aLista3.Add(New clsPlantTempo(Col01, Col02))
    End Sub

    Public Function fListarTabla() As ArrayList
        Return aLista
    End Function

    Public Function fListarTablaOpcion() As ArrayList
        Return aListaOpcion
    End Function

    Public Function fListarTablaRendir() As ArrayList
        Return aLista5
    End Function

    Public Function fListarTablas() As ArrayList
        Return aListas
    End Function

    Public Function fListarTabla2() As ArrayList
        Return aLista2
    End Function

    Public Function fListarTabla4() As ArrayList
        Return aLista4
    End Function

    Public Function fListarTabla3() As ArrayList
        Return aLista3
    End Function

    Public Function fColCategoria() As ArrayList
        sAgregarLista(clsEnteCategoria.cCategoryIdAlias, clsEnteCategoria.cCategoryId)
        sAgregarLista(clsEnteCategoria.cCategoryNameAlias, clsEnteCategoria.cCategoryName)
        Return fListarTabla()
    End Function

    Public Function fColEstado() As ArrayList
        sAgregarLista("Activo", "0")
        sAgregarLista("Inactivo", "1")
        Return fListarTabla()
    End Function

    Public Function fColOpcion() As ArrayList
        sAgregarListaOpcion("ABONO", "D")
        sAgregarListaOpcion("CARGO", "H")
        Return fListarTablaOpcion()
    End Function


    Public Function fColEstados() As ArrayList
        sAgregarListas("Activo", "0")
        sAgregarListas("Inactivo", "1")
        Return fListarTablas()
    End Function

    Public Function fColEstado2() As ArrayList
        sAgregarLista2("Pendiente", "0")
        sAgregarLista2("Emitido", "1")
        sAgregarLista2("Anulado", "2")
        Return fListarTabla2()
    End Function

    Public Function fColEstado4() As ArrayList
        sAgregarLista4("Pendiente", "0")
        sAgregarLista4("Emitido", "1")
        sAgregarLista4("Anulado", "2")
        Return fListarTabla4()
    End Function

    Public Function fColEstado3() As ArrayList
        sAgregarLista3("Lleno", "0")
        sAgregarLista3("En Uso", "1")
        Return fListarTabla3()
    End Function


    Public Function fColEstadoRendido() As ArrayList
        sAgregarListaRendir("A Rendir", "01")
        sAgregarListaRendir("Rendido", "02")
        Return fListarTablaRendir()
    End Function


    Public Sub sMensaje(ByVal vCadena As String)
        MessageBox.Show(vCadena, "Asistente del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    '2da Parte
    Public Function fColCortesia() As ArrayList
        sAgregarLista(UCase("Ms."), UCase("Ms."))
        sAgregarLista(UCase("Dr."), UCase("Dr."))
        sAgregarLista(UCase("Mr."), UCase("Mr."))
        sAgregarLista(UCase("Mrs."), UCase("Mrs."))
        Return fListarTabla()
    End Function

    Public Function fColTitulo() As ArrayList
        sAgregarLista(UCase("Sales Representative"), UCase("Sales Representative"))
        sAgregarLista(UCase("Vice President, Sales"), UCase("Vice President, Sales"))
        sAgregarLista(UCase("Sales Manager"), UCase("Sales Manager"))
        sAgregarLista(UCase("Inside Sales Coordinator"), UCase("Inside Sales Coordinator"))
        Return fListarTabla()
    End Function

    Public Function fColPais() As ArrayList
        sAgregarLista(UCase("USA"), UCase("USA"))
        sAgregarLista(UCase("UK"), UCase("UK"))
        Return fListarTabla()
    End Function


End Class
