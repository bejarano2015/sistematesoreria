﻿Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports CapaEntidad
Imports System.Data.Common

Public Class DaoRegistroDocumento
    Dim VL_VarGlobal As New CapaDatos.DaoGlobal()
    Public Function Fnc_Registrar_Documentos(objBeanRegistroDocumento As BeanRegistroDocumento) As CapaEntidad.BeanResultado.ResultadoTransaccion

        Dim VL_Database As SqlDatabase
        Dim VL_Command As DbCommand
        Dim VL_Connection As DbConnection
        Dim VL_Transact As DbTransaction
        Dim VL_BeanResultado As BeanResultado.ResultadoTransaccion = New CapaEntidad.BeanResultado.ResultadoTransaccion()

        VL_Database = New SqlDatabase(VL_VarGlobal.BdConectionString)
        VL_Connection = VL_Database.CreateConnection()
        VL_Connection.Open()
        VL_Transact = VL_Connection.BeginTransaction()

        Try
            VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_MANTENIMIENTO_REGISTRODOCUMENTO_INSERTAR")
            VL_Database.AddInParameter(VL_Command, "@EmprCodigo", DbType.[String], objBeanRegistroDocumento.EmprCodigo)
            VL_Database.AddInParameter(VL_Command, "@CodProveedor", DbType.[String], objBeanRegistroDocumento.CodProveedor)
            VL_Database.AddInParameter(VL_Command, "@Serie", DbType.[String], objBeanRegistroDocumento.Serie)
            VL_Database.AddInParameter(VL_Command, "@NroDocumento", DbType.[String], objBeanRegistroDocumento.NroDocumento)
            VL_Database.AddInParameter(VL_Command, "@FechaDocumento", DbType.DateTime, objBeanRegistroDocumento.FechaDocumento)
            VL_Database.AddInParameter(VL_Command, "@ImporteTotal", DbType.[String], objBeanRegistroDocumento.ImporteTotal)
            VL_Database.AddInParameter(VL_Command, "@IdArea", DbType.[String], objBeanRegistroDocumento.IdArea)
            VL_Database.AddInParameter(VL_Command, "@Usuario", DbType.[String], objBeanRegistroDocumento.Usuario)
            VL_Database.AddInParameter(VL_Command, "@PcCreacion", DbType.[String], objBeanRegistroDocumento.PcCreacion)
            VL_Database.AddInParameter(VL_Command, "@IdOC", DbType.[String], objBeanRegistroDocumento.IdOC)
            'VL_Database.AddInParameter(VL_Command, "@NroOC", DbType.[String], objBeanRegistroDocumento.NroOC)
            VL_Database.AddInParameter(VL_Command, "@CCosCodigo", DbType.[String], objBeanRegistroDocumento.CCosCodigo)
            VL_Database.AddInParameter(VL_Command, "@Periodo", DbType.[String], objBeanRegistroDocumento.Periodo)


            VL_Database.ExecuteNonQuery(VL_Command, VL_Transact)
            VL_Transact.Commit()
            VL_BeanResultado.blnResultado = True

            VL_BeanResultado.strMensaje = "Se realizó operación Exitosamente!"
        Catch ex As Exception
            VL_BeanResultado.blnResultado = False
            VL_BeanResultado.strMensaje = ex.Message.ToString()
            VL_Transact.Rollback()
        Finally
            If VL_Connection.State = ConnectionState.Open Then
                VL_Connection.Close()
            End If
            VL_Database = Nothing
            VL_Command = Nothing
        End Try

        Return VL_BeanResultado

    End Function

    Public Function Fnc_listar_documentoPendienteAsignacion(vl_RegistroDocumento As BeanRegistroDocumento) As CapaEntidad.BeanResultado.ResultadoSeleccion
        Dim VL_Database As New SqlDatabase(CapaDatos.DaoGlobal.VG_CONECTION_STRING)
        Dim VL_BeanResultado As BeanResultado.ResultadoSeleccion = New CapaEntidad.BeanResultado.ResultadoSeleccion()
        Dim VL_Command As DbCommand

        Try
            VL_Command = VL_Database.GetStoredProcCommand("TESORERIA.USP_REGISTRODOCUMENTO_VALORIZACIONES_LISTAR")

            VL_Database.AddInParameter(VL_Command, "@emprcodigo", DbType.[String], vl_RegistroDocumento.EmprCodigo)
            VL_Database.AddInParameter(VL_Command, "@periodo", DbType.[String], vl_RegistroDocumento.Periodo)


            VL_BeanResultado.dtResultado = VL_Database.ExecuteDataSet(VL_Command).Tables(0)
            VL_BeanResultado.blnExiste = True
        Catch ex As System.Data.SqlClient.SqlException
            VL_BeanResultado.blnExiste = False
            VL_BeanResultado.strMensaje = ex.Message.ToString()
        Finally
            VL_Database = Nothing
            VL_Command = Nothing
        End Try
        Return VL_BeanResultado
    End Function
End Class
