Imports System.Data
Imports System.Data.SqlClient

Public Class clsEnteCategoria
    Private _CategoryId As Int32
    Private _CategoryName As String
    Private _Description As String

    Private aLista As ArrayList
    Private cCapaDatos As clsCapaDatos
    Private reaSQL As SqlDataReader
    Dim bEstado As Boolean = False

    Public iContador As Integer = "0"
    Public Const cCategoryId As String = "CategoryId"
    Public Const cCategoryName As String = "CategoryName"
    Public Const cDescription As String = "Description"
    Public Const cCategoryIdAlias As String = "ID"
    Public Const cCategoryNameAlias As String = "NOMBRE"
    Public Const cDescriptionAlias As String = "DESCRIPCION"
    Private cTempo As New clsPlantTempo

    Public Sub New()
        _CategoryName = "NULO"
        _Description = "NULO"
    End Sub

    Public Sub New(ByVal CategoryId As Int32, ByVal CategoryName As String, ByVal Description As String)
        _CategoryId = CategoryId
        _CategoryName = CategoryName
        _Description = Description
    End Sub

    Public Property pCategoryId()
        Get
            Return _CategoryId
        End Get
        Set(ByVal value)
            _CategoryId = value
        End Set
    End Property

    Public Property pCategoryName()
        Get
            Return _CategoryName
        End Get
        Set(ByVal value)
            _CategoryName = value
        End Set
    End Property

    Public Property pDescription()
        Get
            Return _Description
        End Get
        Set(ByVal value)
            _Description = value
        End Set
    End Property

    Public Function fListarCategorias(ByVal vCadena As String) As ArrayList
        aLista = New ArrayList
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("select categoryID,categoryName,Description from categories" & " " & Trim(vCadena), CommandType.Text)

            reaSQL = cCapaDatos.fCmdExecuteReader()
            While reaSQL.Read
                Dim col01 As Int32 = UCase(reaSQL.GetInt32(reaSQL.GetOrdinal("categoryID")))
                Dim col02 As String = UCase(reaSQL.GetString(reaSQL.GetOrdinal("categoryName")))
                Dim col03 As String = UCase(reaSQL.GetString(reaSQL.GetOrdinal("Description")))
                aLista.Add(New clsEnteCategoria(col01, col02, col03))
                iContador += 1
            End While

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return aLista
    End Function

    Public Function fBuscarCategoria(ByVal vValor As Int32) As Boolean
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("select * from categories where categoryID=@categoryID", CommandType.Text)
            cCapaDatos.sParametroSQL("categoryID", DbType.Int32, 4, vValor)
            reaSQL = cCapaDatos.fCmdExecuteReader()
            reaSQL.Read()
            pCategoryId = reaSQL.GetInt32(reaSQL.GetOrdinal("categoryID"))
            pCategoryName = UCase(reaSQL.GetString(reaSQL.GetOrdinal("categoryName")))
            pDescription = UCase(reaSQL.GetString(reaSQL.GetOrdinal("Description")))
            bEstado = True
        Catch ex As Exception
            bEstado = False
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return bEstado
    End Function

    Public Function fCodigoCategoria() As Int32 'No usado
        Dim iCodigo As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("select top 1 categoryId from categories order by categoryId desc", CommandType.Text)
            iCodigo = Convert.ToInt32(cCapaDatos.fCmdExecuteScalar())
            If iCodigo = 0 Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo
    End Function

    Public Function fGrabarCategoria(ByVal vcategoryID As Int32, ByVal vcategoryName As String, ByVal vDescription As String, ByVal vNuevo As Boolean) As Boolean
        Dim iResultado As Int32
        cCapaDatos = New clsCapaDatos

        Try
            If vcategoryName = "" Then
                vcategoryName = _CategoryName
            End If
            If vDescription = "" Then
                vDescription = _Description
            End If
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            If vNuevo = True Then
                cCapaDatos.sComandoSQL("insert into categories(categoryName,Description)values(@categoryName,@Description)", CommandType.Text)
            Else
                cCapaDatos.sComandoSQL("update categories set categoryName=@categoryName,Description=@Description where categoryID=@categoryID", CommandType.Text)
            End If

            cCapaDatos.sParametroSQL("@categoryID", DbType.Int32, 4, vcategoryID)
            cCapaDatos.sParametroSQL("@categoryName", DbType.String, 15, vcategoryName)
            cCapaDatos.sParametroSQL("@Description", DbType.String, 600, vDescription)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = "1" Then
                bEstado = True
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                bEstado = False
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

            'Throw New System.Exception() '/ Agregado 
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return bEstado
    End Function

    Public Function fEliminarCategoria(ByVal categoryID As Int32) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL("delete from categories where categoryID=@categoryID", CommandType.Text)
            cCapaDatos.sParametroSQL("@categoryID", DbType.Int32, 4, categoryID)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

            'Throw New System.Exception() '/Agregado
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Sub sExportarCategorias(ByVal vCadena As String)
        'cCapaDatos = New clsCapaDatos

        'Try
        '    cCapaDatos.sConectarSQL()
        '    cCapaDatos.sComandoSQL("select categoryID,categoryName,Description from categories" & " " & Trim(vCadena), CommandType.Text)
        '    reaSQL = cCapaDatos.fCmdExecuteReader()

        '    Dim oExcel As Excel.Application
        '    Dim oBooks As Excel.Workbooks
        '    Dim oBook As Excel.WorkbookClass
        '    Dim oSheet As Excel.Worksheet

        '    oExcel = CreateObject("Excel.Application")
        '    oExcel.Visible = True
        '    oBooks = oExcel.Workbooks
        '    oBook = oExcel.Workbooks.Add
        '    oSheet = oBook.Sheets(1)

        '    Const cPrimeraFila = 1
        '    Dim iRow As Int64 = 1

        '    oSheet.Cells.Font.Size = 8
        '    oSheet.Cells(cPrimeraFila, 1) = cCategoryIdAlias
        '    oSheet.Cells(cPrimeraFila, 2) = cCategoryNameAlias
        '    oSheet.Cells(cPrimeraFila, 3) = cDescriptionAlias
        '    oSheet.Cells(cPrimeraFila, 1).font.bold = True
        '    oSheet.Cells(cPrimeraFila, 2).font.bold = True
        '    oSheet.Cells(cPrimeraFila, 3).font.bold = True
        '    oSheet.Columns(1).ColumnWidth = 17
        '    oSheet.Columns(2).ColumnWidth = 30
        '    oSheet.Columns(3).ColumnWidth = 60

        '    oSheet.Range("A1:C1").BorderAround(1, Excel.XlBorderWeight.xlMedium, Excel.XlColorIndex.xlColorIndexAutomatic)

        '    While reaSQL.Read
        '        Dim iCurrRow As Int64 = cPrimeraFila + iRow
        '        oSheet.Cells(iCurrRow, 1) = UCase(reaSQL.GetInt32(reaSQL.GetOrdinal("categoryID")))
        '        oSheet.Cells(iCurrRow, 1).font.bold = True
        '        oSheet.Cells(cPrimeraFila + iRow + 1, 2).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = 1
        '        oSheet.Cells(iCurrRow, 2) = UCase(reaSQL.GetString(reaSQL.GetOrdinal("categoryName")))
        '        oSheet.Cells(cPrimeraFila + iRow + 1, 3).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = 1
        '        oSheet.Cells(iCurrRow, 3) = UCase(reaSQL.GetString(reaSQL.GetOrdinal("Description")))
        '        iRow += 1
        '    End While

        '    oSheet.Cells(cPrimeraFila + iRow + 1, 1).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = 9
        '    oSheet.Cells(cPrimeraFila + iRow + 1, 1) = "TOTAL REGISTROS"
        '    oSheet.Cells(cPrimeraFila + iRow + 1, 1).font.bold = True
        '    oSheet.Cells(cPrimeraFila + iRow + 1, 2).Borders(Excel.XlBordersIndex.xlEdgeTop).LineStyle = 9
        '    oSheet.Cells(cPrimeraFila + iRow + 1, 2) = "=contar(A" & (cPrimeraFila + 1) & ":A" & (cPrimeraFila + iRow - 1).ToString & ")"
        '    oSheet.Cells(cPrimeraFila + iRow + 1, 2).font.bold = True

        '    oBook = Nothing
        '    oBooks = Nothing
        '    oExcel = Nothing

        'Catch ex As Exception
        '    cTempo.sMensaje(Convert.ToString(ex.Message))
        'Finally
        '    cCapaDatos.sDesconectarSQL()
        'End Try

    End Sub

End Class

'/Comprobar Transacciones
'/Controles Desconectados