Imports System.Data
Imports System.Data.SqlClient

Public Class clsCapaDatos
    'Entorno Conectado
    Private cTempo As New clsPlantTempo
    Private cnSQL As SqlConnection
    Private trSQL As SqlTransaction
    Private btrSQL As Boolean = False
    Private cmdSQL As SqlCommand
    Private bcmdSQL As Boolean = False
    Private prmSQL As SqlParameter

    'Entorno Desconectado
    'Private buiSQL As SqlCommandBuilder
    Private adpSQL As SqlDataAdapter
    Private badpSQL As Boolean = False
    Private dtSet As DataSet
    Private bdtSet As Boolean = False
    Private dtTable As Data.DataTable
    Private bdtTable As Boolean = False
    Private dtView As DataView
    Private dtRow As DataRow

    Public Sub sConectarSQL()


        Try
            'cnSQL = New SqlConnection("Data Source=(local);Initial Catalog=ERPDHMONT;Integrated Security=True")
            cnSQL = New SqlConnection("Data Source=192.168.1.200;Initial Catalog=ERPDHMONT;User Id=sa; Password=!gestionerp2013;;Current Language=english")
            If cnSQL.State = ConnectionState.Closed Then
                cnSQL.Open()
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try

    End Sub

    Public Sub sDesconectarSQL()
        Try
            If bcmdSQL = True Then
                cmdSQL.Dispose()
                bcmdSQL = False
            End If

            If badpSQL = True Then
                adpSQL.Dispose()
                badpSQL = False
            End If

            If bdtSet = True Then
                dtSet.Dispose()
                bdtSet = False
            End If

            If bdtTable = True Then
                dtTable.Dispose()
                bdtTable = False
            End If

            If btrSQL = True Then
                trSQL.Dispose()
                btrSQL = False
            End If

            If cnSQl.State = ConnectionState.Open Then
                cnSQl.Close()
                cnSQl.Dispose()
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try

    End Sub

    Public Sub sComenzarTransaccion()
        btrSQL = True
        trSQL = cnSQl.BeginTransaction
    End Sub

    Public Sub sConfirmarTransaccion()
        trSQL.Commit()
        'cmdSQL.Transaction.Commit()
    End Sub

    Public Sub sCancelarTransaccion()
        trSQL.Rollback()
        'cmdSQL.Transaction.Rollback()
    End Sub

    Public Sub sComandoSQL(ByVal vSQL As String, ByVal vTipo As System.Data.CommandType)
        cmdSQL = New SqlCommand
        cmdSQL.Connection = cnSQl
        cmdSQL.CommandText = vSQL
        cmdSQL.CommandType = vTipo
        If btrSQL = True Then
            cmdSQL.Transaction = trSQL
            'cmdSQL.Transaction = cnSQl.BeginTransaction
        End If
    End Sub

    Public Sub sParametroSQL(ByVal vNombre As String, ByVal vTipo As System.Data.DbType, ByVal vTama�o As Integer, ByVal vValor As Object)
        prmSQL = New SqlParameter
        prmSQL.ParameterName = vNombre
        prmSQL.Direction = ParameterDirection.Input
        prmSQL.DbType = vTipo
        prmSQL.Size = vTama�o
        prmSQL.Value = vValor
        cmdSQL.Parameters.Add(prmSQL)
    End Sub

    Public Function fCmdExecuteNonQuery() As Integer
        bcmdSQL = True
        Return cmdSQL.ExecuteNonQuery()
    End Function

    Public Function fCmdExecuteScalar() As Object
        bcmdSQL = True
        Return cmdSQL.ExecuteScalar()
    End Function

    Public Function fCmdExecuteReader() As SqlDataReader
        bcmdSQL = True
        Return cmdSQL.ExecuteReader(CommandBehavior.CloseConnection)
    End Function

    Public Sub fLimpiarParametros()
        bcmdSQL = True
        cmdSQL.Parameters.Clear()
    End Sub


    'Segunda Parte

    'Nota: sAdaptadorSQL y fAdpExecuteNonQuery si utilizan el mismo TipoCommand en los 2 funciona para todos los crud sin importar tipo
    Public Sub sAdaptadorSQL(ByVal vTipo As String)
        adpSQL = New SqlDataAdapter
        'buiSQL = New SqlCommandBuilder(adpSQL)
        Select Case UCase(vTipo.Trim)
            Case "C" : adpSQL.InsertCommand = cmdSQL
            Case "R" : adpSQL.SelectCommand = cmdSQL 'Falta
            Case "U" : adpSQL.UpdateCommand = cmdSQL
            Case "D" : adpSQL.DeleteCommand = cmdSQL
        End Select
    End Sub

    Public Function fAdpExecuteNonQuery(ByVal vTipo As String) As Integer
        badpSQL = True
        Select Case UCase(vTipo.Trim)
            Case "C" : Return adpSQL.InsertCommand.ExecuteNonQuery
            Case "R" : Return adpSQL.SelectCommand.ExecuteNonQuery 'Falta
            Case "U" : Return adpSQL.UpdateCommand.ExecuteNonQuery
            Case "D" : Return adpSQL.DeleteCommand.ExecuteNonQuery
        End Select
    End Function

    Public Function fAdpExecuteScalar() As Object
        badpSQL = True
        Return adpSQL.SelectCommand.ExecuteScalar
    End Function

    Public Function fAdpExecuteReader() As SqlDataReader 'Falta
        badpSQL = True
        Return adpSQL.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection)
    End Function

    Public Function fListarTabla() As Data.DataTable
        bdtTable = True
        dtTable = New Data.DataTable("Datatable")
        dtTable.Clear()
        adpSQL.Fill(dtTable)
        Return dtTable
    End Function

    Public Function fListarSet() As DataSet
        bdtSet = True
        dtSet = New DataSet("Dataset")
        dtSet.Clear()
        adpSQL.Fill(dtSet, "Temporal")
        Return dtSet
    End Function

    Public Function fCargarDatatableLoad(ByVal vreaSQL As SqlDataReader) As DataTable
        bdtTable = True
        dtTable = New Data.DataTable("Datatable")
        dtTable.Clear()
        dtTable.Load(vreaSQL, LoadOption.OverwriteChanges)
        vreaSQL.Close()
        Return dtTable
    End Function

    Public Function fCargarDatasetLoad(ByVal vreaSQL As SqlDataReader) As DataSet
        bdtSet = True
        dtSet = New DataSet("Dataset")
        dtSet.Clear()
        dtSet.Load(vreaSQL, LoadOption.OverwriteChanges, "Temporal")
        vreaSQL.Close()
        Return dtSet
    End Function

    Public Sub fLimpiaParametrosSQL()
        bcmdSQL = True
        cmdSQL.Parameters.Clear()
    End Sub

End Class