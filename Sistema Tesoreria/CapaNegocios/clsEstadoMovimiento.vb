Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsEstadoMovimiento

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspEstadoMovimiento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DescripcionMovimiento", DbType.String, 50, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdEstadoMovimiento As String, ByVal vDescripcionMovimiento As String, ByVal vEstado As Integer, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspEstadoMovimiento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, vIdEstadoMovimiento)
            cCapaDatos.sParametroSQL("@DescripcionMovimiento", DbType.String, 50, vDescripcionMovimiento)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vEstado As Integer, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspEstadoMovimiento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, Codigo)
            cCapaDatos.sParametroSQL("@DescripcionMovimiento", DbType.String, 50, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCodigo() As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspEstadoMovimiento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DescripcionMovimiento", DbType.String, 50, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspEstadoMovimiento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, Codigo)
            cCapaDatos.sParametroSQL("@DescripcionMovimiento", DbType.String, 50, Descripcion)
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))
            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fBuscaEstadoMov(ByVal iOpcion As Integer, ByVal Des As String, ByVal vCodEmpresa As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspEstadoMovimiento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DescripcionMovimiento", DbType.String, 50, Des)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
