Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsPagoProveedores

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodFuturoDetCro As String
    Public iNroRegistros As Integer = "0"

    Public Function fListarProveedores(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarBancos(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDocumentosaEntregar(ByVal iOpcion As Integer, ByVal vCodigoEmpresa As String, ByVal TipoPago As String, ByVal TextoBuscar As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, Trim(TipoPago))
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, Trim(TextoBuscar))
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDocumentosaConsultados(ByVal iOpcion As Integer, ByVal vCodigoEmpresa As String, ByVal Ruc As String, ByVal TextoBuscar As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, Trim(Ruc))
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, Trim(TextoBuscar))
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarAreas(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCajasxArea(ByVal vCodigoEmpresa As String, ByVal IdArea As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, IdArea)
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'fListarCajasxArea

    Public Function fListarTipoProveedores(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarMonedas() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'fListarTipoProveedores
    Public Function fListarCajas(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    'fListarParametroTipoCambio
    Public Function fListarParametroTipoCambio(ByVal EmprCodigo As String, ByVal Fecha As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarTipoMovPagos() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 18)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarParametroIGV(ByVal FechaIGV As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(FechaIGV, "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCuentasxBancoyEmpresa(ByVal vCodigoEmpresa As String, ByVal vCodigoBanco As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vCodigoBanco)
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'fListarProveedoresPorTipo

    'Public Function fListarProveedoresPorTipo(ByVal vCodigoEmpresa As String, ByVal vCodigoTipoProv As String) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspPagoProveedores"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, vCodigoTipoProv)
    '        cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
    '        cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function


    Public Function fListarProveedoresPorTipo(ByVal vCodigoEmpresa As String, ByVal vCodigoTipoProv As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, vCodigoTipoProv)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'fTraerDatosProveedor

    Public Function fTraerDatosProveedor(ByVal CodigoProv As String, ByVal IdTipoProv As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, IdTipoProv)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, CodigoProv)
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerDatosProveedor2(ByVal RUC As String, ByVal IdTipoProv As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 30)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, IdTipoProv)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, RUC)
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerLineaDeCreditoProv(ByVal CodEmpresa As String, ByVal AnaliticoCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 17)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, AnaliticoCodigo)
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerDocumentos(ByVal CodigoProv As String, ByVal IdTipoProv As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, IdTipoProv)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, CodigoProv)
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerDocumentosPorMoneda(ByVal iOpcion As Integer, ByVal CodigoProv As String, ByVal IdTipoProv As String, ByVal CodEmpresa As String, ByVal CodMoneda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, IdTipoProv)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, CodigoProv)
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, CodMoneda)
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fTraerResExiste(ByVal IdTipoAnexo As String, ByVal CodEmpr As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Anio As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function ActualizarDocumentos(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodRegDoc As String, ByVal DetrPer As String, ByVal Monto As Double, ByVal OrdCom As String, ByVal Estado As Integer, ByVal IdResumen As String, ByVal iEstadoPrePro As Integer, ByVal iEstadoPro As Integer, ByVal Saldo As Double, ByVal CCostoCod As String, ByVal IdOrden As String, ByVal Situacion As String, ByVal Prestamo As Integer, ByVal EmprPrestamo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, CodRegDoc)
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, DetrPer)
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, OrdCom)
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, Prestamo)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, iEstadoPrePro)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, iEstadoPro)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, CCostoCod)
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, IdOrden)
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, Situacion)
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, EmprPrestamo)
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function
    'GrabarResumen

    Public Function GrabarResumen(ByVal IdResumen As String, ByVal CodEmpresa As String, ByVal IdTipoAnexo As String, ByVal IdMoneda As String, ByVal IdAnexo As String, ByVal MontoTotal As Double, ByVal TipoCambio As Double, ByVal NroSemana As String, ByVal CantDoc As Integer, ByVal Glosa As String, ByVal Estado As Integer, ByVal NomAnexo As String, ByVal sRuc As String, ByVal Anio As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, IdMoneda)
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, MontoTotal)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, CantDoc)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, NomAnexo)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, sRuc)

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function ActualizaLineaCredito(ByVal IdLinea As String, ByVal CodEmpresa As String, ByVal Dias As Integer, ByVal SolesLinea As Decimal, ByVal DolaresLinea As Decimal) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdLinea)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, SolesLinea)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, DolaresLinea)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, Dias)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function ActualizaLineaCredito2(ByVal iOpcion As Integer, ByVal IdLinea As String, ByVal CodEmpresa As String, ByVal Dias As Integer, ByVal SolesLinea As Decimal, ByVal DolaresLinea As Decimal) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdLinea)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, SolesLinea)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, DolaresLinea)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, Dias)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    Public Function GenerarCodResumen(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function GenerarCodDetCro(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoDetCro = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function GrabarDetalleCro(ByVal iOpcion As Integer, ByVal IdDetalleCronograma As String, ByVal IdCronograma As String, ByVal AbrDoc As String, ByVal NroDocumento As String, ByVal FechaDocumento As DateTime, ByVal FechaVencimiento As DateTime, ByVal Importe As Double, ByVal MontoPagar As Double, ByVal Saldo As Double, ByVal DP As String, ByVal MontoDP As Double, ByVal NroOrden As String, ByVal XMarca As Integer, ByVal IdRegistro As String, ByVal MonedaDes As String, ByVal SimboloMo As String, ByVal CodEmpresa As String, ByVal Estado As Integer, ByVal CodEmpresaDoc As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, IdDetalleCronograma)
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdCronograma)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDocumento)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaDocumento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(FechaVencimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, MontoPagar)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, DP)
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, MontoDP)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, NroOrden)
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, XMarca)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdRegistro)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, AbrDoc)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, SimboloMo)
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, MonedaDes)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Trim(CodEmpresaDoc))
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function ActDetalleCro(ByVal iOpcion As Integer, ByVal IdDetalleCronograma As String, ByVal IdCronograma As String, ByVal AbrDoc As String, ByVal NroDocumento As String, ByVal FechaDocumento As DateTime, ByVal FechaVencimiento As DateTime, ByVal Importe As Double, ByVal MontoPagar As Double, ByVal Saldo As Double, ByVal DP As String, ByVal MontoDP As Double, ByVal NroOrden As String, ByVal XMarca As Integer, ByVal IdRegistro As String, ByVal MonedaDes As String, ByVal SimboloMo As String, ByVal CodEmpresa As String, ByVal Estado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, IdDetalleCronograma)
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdCronograma)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDocumento)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaDocumento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(FechaVencimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, MontoPagar)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, DP)
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, MontoDP)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, NroOrden)
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, XMarca)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdRegistro)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, AbrDoc)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, SimboloMo)
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, MonedaDes)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fListTipAnexos(ByVal CodEmpr As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fLisAnexosPorTipo(ByVal CodEmpr As String, ByVal IdTipAn As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipAn)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fLisCronoxAnexos(ByVal CodEmpr As String, ByVal IdAn As String, ByVal IdTipAn As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipAn)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAn)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fLisDetCronogramas(ByVal IdCronograma As String, ByVal CodEmpresa As String) As DataTable
        'Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdCronograma)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function ActualizarDetalleCro(ByVal iOpcion As Integer, ByVal IdDetalleCronograma As String, ByVal IdCronograma As String, ByVal DescripDoc As String, ByVal NroDocumento As String, ByVal FechaDocumento As DateTime, ByVal FechaVencimiento As DateTime, ByVal ImporteSoles As Double, ByVal MontoDolares As Double, ByVal DP As String, ByVal MontoDP As Double, ByVal NroOrden As String, ByVal XMarca As Integer, ByVal IdRegistro As String, ByVal AbrDoc As String, ByVal CodEmpresa As String, ByVal Estado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, IdDetalleCronograma)
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdCronograma)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDocumento)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, ImporteSoles)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, MontoDolares)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, DP)
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, MontoDP)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, NroOrden)
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, XMarca)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdRegistro)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, AbrDoc)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function ActualizarDocumento(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal IdRegDoc As String, ByVal EstadoPrePro As Integer, ByVal EstadoPro As Integer, ByVal Saldo As Double, ByVal CCosCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdRegDoc)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, CCosCodigo)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, EstadoPrePro)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, EstadoPro)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function ActualizarDoc(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodRegDoc As String, ByVal ActSaldo As Double) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, ActSaldo)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, CodRegDoc)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function EliminarDetalleCro(ByVal iOpcion As Integer, ByVal IdDetCro As String, ByVal IdCronograma As String, ByVal Empresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, IdDetCro)
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdCronograma)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    Public Function ActResumen(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal IdResumen As String, ByVal MontoTotal As Double, ByVal MontoDolares As Double, ByVal Cantidad As Integer, ByVal Glosa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, MontoTotal)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, Cantidad)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fListarSemanas(ByVal CodEmpr As String, ByVal Anio As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    'Public Function fLisDetCronogramas2(ByVal IdCronograma As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
    '    'Dim iResultado As Int32 = 0
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspGrabaDetCrono"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
    '        cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdCronograma)
    '        cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
    '        cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
    '        cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
    '        cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    Public Function fLisDetCronogramas2(ByVal NroSemana As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fLisAnexosxSemanayTipAnex(ByVal IdTipoAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fLisDetCronogramas3(ByVal IdTipoAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fLisTipoAnexosxSemana(ByVal NroSemana As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fLisDetCronogramas4(ByVal IdTipoAnexo As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fTraerResExisteSoles(ByVal IdTipoAnexo As String, ByVal CodEmpr As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal IdMoneda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 14)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerResExisteDolares(ByVal IdTipoAnexo As String, ByVal CodEmpr As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal IdMoneda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'letras

    Public Function fTraerResExisteLetraProv(ByVal IdTipoAnexo As String, ByVal CodEmpr As String, ByVal IdAnexo As String, ByVal NroLetra As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, NroLetra)

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function GenerarCodLetra(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 24)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function GrabarLetra(ByVal iOpcion As String, ByVal IdLetra As String, ByVal NroLetra As String, ByVal CodEmpresa As String, ByVal FechaGiro As DateTime, ByVal FechaVen As DateTime, ByVal Lugar As String, ByVal Referencia As String, ByVal CantidadDocLetra As String, ByVal Total As Double, ByVal Saldo As Double, ByVal IdTipoAnexo As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Periodo As String, ByVal IdMoneda As String, ByVal Usuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, IdMoneda)
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdLetra)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, Total)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, CantidadDocLetra)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")

            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, NroLetra)
            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(FechaGiro, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(FechaVen, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, Lugar)
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, Referencia)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, Usuario)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function ActSaldoDoc(ByVal iOpcion As Integer, ByVal IdDocumento As String, ByVal CodEmpresa As String, ByVal Saldo As Double, ByVal IdLetra As String, ByVal IdCronograma As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdCronograma)
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, IdLetra)
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdDocumento)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")

            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    'fin letras

    'Public Function GrabarResumen(ByVal IdResumen As String, ByVal CodEmpresa As String, ByVal IdTipoAnexo As String, ByVal IdMoneda As String, ByVal IdAnexo As String, ByVal MontoTotal As Double, ByVal TipoCambio As Double, ByVal NroSemana As String, ByVal CantDoc As Integer, ByVal Glosa As String, ByVal Estado As Integer, ByVal NomAnexo As String, ByVal sRuc As String, ByVal Anio As String) As Integer
    '    Dim iResultado As Int32 = 0
    '    cCapaDatos = New clsCapaDatos
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspActRegDoc"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
    '        cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, IdMoneda)
    '        cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
    '        cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
    '        cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
    '        cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, MontoTotal)
    '        cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
    '        cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
    '        cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, CantDoc)
    '        cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, Glosa)
    '        cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
    '        cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, Estado)
    '        cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, NomAnexo)
    '        cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, sRuc)

    '        cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
    '        cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
    '        cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
    '        cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")
    '        iResultado = cCapaDatos.fCmdExecuteNonQuery()
    '        'If iResultado >= "1" Then
    '        '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
    '        'Else
    '        '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
    '        'End If
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return iResultado
    'End Function


    Public Function fTraerResPagadoSol(ByVal IdTipoAnexo As String, ByVal CodEmpr As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal IdMoneda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 20)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerResPagadoDol(ByVal IdTipoAnexo As String, ByVal CodEmpr As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal IdMoneda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 21)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fLisDetCronogramas5(ByVal IdMoneda As String, ByVal IdTipoAnexo As String, ByVal IdAnexo As String, ByVal NroSemana As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, IdMoneda)
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, NroSemana)
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerSaldoDocumento(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodRegDoc As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, CodRegDoc)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function BuscarSaldoProvexMoneda(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal IdTipoProv As String, ByVal CodigoProv As String, ByVal CodMoneda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, IdTipoProv)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, CodigoProv)
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, CodMoneda)
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            '--------------------------->
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'Public Function BuscarDocenProg(ByVal iOpcion As Integer, ByVal IdResumen As String, ByVal NroDoc As String, ByVal CodEmpresa As String, ByVal CodMoneda As String) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspPagoProveedores"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, IdTipoProv)
    '        cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, CodigoProv)
    '        cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, CodMoneda)
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    Public Function BuscarDocenProg(ByVal iOpcion As Integer, ByVal IdResumen As String, ByVal NroDoc As String, ByVal CodEmpresa As String) As DataTable
        'Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGrabaDetCrono"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdDetalleCronograma", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCronograma", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoPagar", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MtoDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@XMarca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SimboloMo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@MonDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function GenerarCodIdSaldoProv(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 17)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function GrabarSaldoProv(ByVal iOpcion As Integer, ByVal IdSaldo As String, ByVal IdTipoProveedor As String, ByVal IdProveedor As String, ByVal EmprCodigo As String, ByVal MontoSaldo As Double, ByVal IdMoneda As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdSaldo)
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, IdMoneda)
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoProveedor)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, MontoSaldo)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCargarCCosto(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fActualizarEntregaDoc(ByVal CodEmpresa As String, ByVal IdDocumento As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdTipoProv", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdParametro", DbType.String, 20, IdDocumento)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaTipoCambio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    Public Function fListMovxUsuario(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal Usuario As String, ByVal FIni As DateTime, ByVal FFin As DateTime, ByVal CCostoCod As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspActRegDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DetrPer", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@MontoDetrPer", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroSemana", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@CantidadDoc", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@GlosaNumPago", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@EstadoResumen", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NomAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@sRuc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@CCostoCod", DbType.String, 10, CCostoCod)
            cCapaDatos.sParametroSQL("@IdOrden", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroLetra", DbType.String, 50, Usuario)

            cCapaDatos.sParametroSQL("@FechaGiro", DbType.DateTime, 10, Format(FIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(FFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@LugarGiro", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@ReferenciaGiro", DbType.String, 500, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdUsuario", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class