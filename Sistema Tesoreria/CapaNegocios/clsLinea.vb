Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsLinea

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal Estado01 As String, ByVal Estado02 As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            'Sentencia SQL Modificable
            sTextoSQL = "Select Estado = CASE EstCodigo WHEN 'A' THEN 'ACTIVO' WHEN 'D' THEN 'DESACTIVADO' ELSE 'N.A.' END,* from Linea where EstCodigo in ('" & Estado01 & "','" & Estado02 & "') order by linnroitem desc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fGrabar(ByVal VLINCODIGO As String, ByVal VLINDESCRIPCION As String, ByVal VLINABREVIADO As String, ByVal VEMPRCODIGO As String, ByVal VESTADO As String, ByVal vNuevo As Boolean) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            If vNuevo = True Then

                'Sentencia SQL Modificable
                cCapaDatos.sComandoSQL("INSERT INTO LINEA(LINCODIGO,LINDESCRIPCION,LINABREVIADO,EMPRCODIGO) VALUES( @LINCODIGO,@LINDESCRIPCION,@LINABREVIADO,@EMPRCODIGO)", CommandType.Text)
            Else
                'Sentencia SQL Modificable
                cCapaDatos.sComandoSQL("UPDATE LINEA SET LINDESCRIPCION=@LINDESCRIPCION,LINUSUACTUALIZA = USER_NAME(),LINPCACTUALIZA=HOST_NAME(),LINFCHACTUALIZA = GETDATE(),LINABREVIADO=@LINABREVIADO,EMPRCODIGO=@EMPRCODIGO,ESTCODIGO=@ESTCODIGO WHERE LINCODIGO=@LINCODIGO", CommandType.Text)
            End If

            'Sentencia SQL Modificable
            cCapaDatos.sParametroSQL("@LINCODIGO", DbType.String, 3, VLINCODIGO)
            cCapaDatos.sParametroSQL("@LINDESCRIPCION", DbType.String, 50, VLINDESCRIPCION)
            cCapaDatos.sParametroSQL("@LINABREVIADO", DbType.String, 15, VLINABREVIADO)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, VEMPRCODIGO)
            cCapaDatos.sParametroSQL("@ESTCODIGO", DbType.String, 1, VESTADO)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vActivo As Boolean) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()

            If vActivo = True Then
                'Sentencia SQL Modificable
                sTextoSQL = "UPDATE LINEA SET ESTCODIGO='D' WHERE LINCODIGO=@LINCODIGO"
            Else
                'Sentencia SQL Modificable
                sTextoSQL = "DELETE FROM LINEA WHERE LINCODIGO=@LINCODIGO"
            End If

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            'Sentencia SQL Modificable
            cCapaDatos.sParametroSQL("@LINCODIGO", DbType.String, 3, Codigo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fCodigo() As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "000"
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("select top 1 lincodigo from linea order by linnroitem desc", CommandType.Text)

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            'Sentencia SQL Modificable
            sTextoSQL = "SELECT count(*) FROM LINEA WHERE LINDESCRIPCION=@LINDESCRIPCION AND LINCODIGO<>@LINCODIGO"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            'Sentencia SQL Modificable
            cCapaDatos.sParametroSQL("@LINCODIGO", DbType.String, 3, Codigo)
            cCapaDatos.sParametroSQL("@LINDESCRIPCION", DbType.String, 50, Descripcion)
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))

            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado

    End Function

End Class
