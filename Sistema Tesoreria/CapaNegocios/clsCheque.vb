Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsCheque

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal Empresa As String, ByVal vEstado01 As Integer, ByVal vEstado02 As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCheque"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoEmitido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Responsable", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fGrabar(ByVal Empresa As String, ByVal vIdCheque As String, ByVal vIdChequera As String, ByVal vIdMonedaCheque As String, ByVal vNumeroCheque As String, ByVal vNroChequera As String, ByVal vFechaEmision As DateTime, ByVal vMontoEmitido As Decimal, ByVal vResponsable As String, ByVal vAnulado As Integer, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCheque"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, vIdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, vIdMonedaCheque)
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, vNumeroCheque)
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, vNroChequera)
            cCapaDatos.sParametroSQL("@FechaEmision", DbType.DateTime, 10, vFechaEmision)
            cCapaDatos.sParametroSQL("@MontoEmitido", DbType.Decimal, 20, vMontoEmitido)
            cCapaDatos.sParametroSQL("@Responsable", DbType.String, 150, vResponsable)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, vAnulado)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEliminar(ByVal Empresa As String, ByVal Codigo As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCheque"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoEmitido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Responsable", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCodigo() As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCheque"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoEmitido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Responsable", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCheque"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, Descripcion)
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@MontoEmitido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Responsable", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))

            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado

    End Function

    Public Function fListarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCheque"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoEmitido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Responsable", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarChequera() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCheque"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoEmitido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Responsable", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarConsultaCheques(ByVal iOpcion As Integer, ByVal vEmpresa As String, ByVal vBanco As String, ByVal vCuenta As String, ByVal vIdChequera As String, ByVal vNumeroMes As Integer, ByVal Anio As String, ByVal F1 As DateTime, ByVal F2 As DateTime, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspListarConsultaCheques"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vCuenta)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(F1, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(F2, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 9, Anio)
            cCapaDatos.sParametroSQL("@Mes", DbType.Int32, 1, vNumeroMes)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
