Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsPercepcion

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

#Region " Rendiciones Cabecera"

    Public Function fCargarPercepcion(ByVal vIdEmpresa As String, ByVal vAnio As String, ByVal vMes As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, vAnio)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, vMes)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function

    Public Function fCargarDocumentos(ByVal vCodProveedor As String, ByVal vIdEmpresa As String, ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, vCodProveedor)
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdPercepcionCab As Decimal, ByVal vIdEmpresa As String, ByVal vIdDocumentoOrigen As Decimal, ByVal vSerie As String, ByVal vNumero As String, ByVal vFecha As DateTime, ByVal vCodProveedor As String, ByVal vNombreProveedor As String, ByVal vRuc As String, ByVal vDireccion As String, ByVal vDescripcion As String, ByVal vTipoCambio As Decimal, ByVal vImporteRetenido As Decimal, ByVal vImporteTotal As Decimal, ByVal vFlgReservado As String, ByVal vFlgAnulado As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, vIdPercepcionCab)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, vIdDocumentoOrigen)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, vSerie)
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, vNumero)
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, vFecha)
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, vCodProveedor)
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, vNombreProveedor)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, vRuc)
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, vDireccion)
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, vDescripcion)
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, vTipoCambio)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, vImporteRetenido)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, vImporteTotal)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, vFlgReservado)
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, vFlgAnulado)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            'iResultado = cCapaDatos.fCmdExecuteNonQuery()
            iResultado = cCapaDatos.fCmdExecuteScalar()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fEliminar(ByVal vIdPercepcionCab As Decimal, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, vIdPercepcionCab)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fCargarProveedores(ByVal vIdEmpresa As String)
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fActualizaSeriNumero(ByVal vIdRetencionCab As Decimal, ByVal vSerie As String, ByVal vFlgReservado As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, vIdRetencionCab)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, vSerie)
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, vFlgReservado)
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fCargarDocumentos(ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fAnular(ByVal vIdRetencionCab As Decimal) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, vIdRetencionCab)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fNroCorrelativo(ByVal vSerie As String, ByVal vOpcion As Integer) As String
        Dim iResultado As String = ""
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, vSerie)
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteScalar()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

#End Region

#Region " Retención Detalle "

    Public Function fCargarPercepcionDetalle(ByVal vIdPercepcionCab As Decimal) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionDet"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONDET", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, vIdPercepcionCab)
            cCapaDatos.sParametroSQL("@IDREGISTRODOCUMENTO", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDDOCUMENTO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, 0)
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 15, 0)
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IDMONEDA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PORCENTAJE", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@PORCENTAJERETENCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTESOLES", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ValorPorcentaje", DbType.Decimal, 18, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarDet(ByVal vIdPercepcionDet As Decimal, ByVal vIdPercepcionCab As Decimal, ByVal vIdRegistroDocumento As String, ByVal vEmprCodigo As String, ByVal vIdDocumento As String, ByVal vSerie As String, ByVal vNumero As String, ByVal vFecha As DateTime, ByVal vIdMoneda As String, ByVal vPorcentaje As Decimal, ByVal vPorcentajeRetencion As Decimal, ByVal vImporteRetenido As Decimal, ByVal vImporteSoles As Decimal, ByVal vImporteTotal As Decimal, ByVal vValorPorcentaje As Decimal, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionDet"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONDET", DbType.Decimal, 18, vIdPercepcionDet)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, vIdPercepcionCab)
            cCapaDatos.sParametroSQL("@IDREGISTRODOCUMENTO", DbType.String, 20, vIdRegistroDocumento)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IDDOCUMENTO", DbType.String, 2, vIdDocumento)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, vSerie)
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 15, vNumero)
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, vFecha)
            cCapaDatos.sParametroSQL("@IDMONEDA", DbType.String, 2, vIdMoneda)
            cCapaDatos.sParametroSQL("@PORCENTAJE", DbType.Decimal, 20, vPorcentaje)
            cCapaDatos.sParametroSQL("@PORCENTAJERETENCION", DbType.Decimal, 20, vPorcentajeRetencion)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, vImporteRetenido)
            cCapaDatos.sParametroSQL("@IMPORTESOLES", DbType.Decimal, 20, vImporteSoles)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, vImporteTotal)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ValorPorcentaje", DbType.Decimal, 18, vValorPorcentaje)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fBuscarImpuestosxDocumentos(ByVal vIdOpcion As Integer, ByVal vIdImpuesto As String, ByVal vFecha As Date) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Comun.uspImpuestosxDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ID_IMPUESTO", DbType.String, 2, vIdImpuesto)
            cCapaDatos.sParametroSQL("@FECHA", DbType.Date, 10, vFecha)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fImporteTipoCambio(ByVal vFecha As Date, ByVal vOpcion As Integer) As Decimal
        Dim iResultado As Decimal = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPercepcionCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDPERCEPCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, vFecha)
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTERETENIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FLGRESERVADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FLGANULADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteScalar()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

#End Region

End Class

