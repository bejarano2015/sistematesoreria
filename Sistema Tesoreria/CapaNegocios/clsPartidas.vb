Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsPartidas

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodFuturoDet As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal txtBusqueda As String, ByVal Periodo As String, ByVal Usuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, txtBusqueda)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, Trim(Usuario))

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarUbiTrabajo(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal IdCentroCosto As String, ByVal IdObra As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, IdObra)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, IdCentroCosto)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListar2(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal IdCentroCosto As String, ByVal IdObra As String, ByVal IdUbicacion As String, ByVal IdEspecialidad As String, ByVal IdTitulo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, IdUbicacion)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, IdEspecialidad)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, IdTitulo)
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, IdObra)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, IdCentroCosto)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    Public Function fListarDetalle(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal IdPartida As String, ByVal FIni As DateTime, ByVal Periodo As String, ByVal CC As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, IdPartida)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, CC)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigo(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal Periodo As String, ByVal CCosto As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, CCosto)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, 0)

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fCodigoDetalle(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal IdPartida As String, ByVal Periodo As String, ByVal CC As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, IdPartida)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, CC)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, 0)

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoDet = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal sCodigoRegistro As String, ByVal IdContratista As String, ByVal IdMoneda As String, ByVal TipoDoc As String, ByVal DesPartida As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Ubicacion As String, ByVal Presupuesto As Double, ByVal Retencion As Double, ByVal IdEspecialidad As String, ByVal Observacion As String, ByVal Estado As Integer, ByVal CodObra As String, ByVal Periodo As String, ByVal User As String, ByVal CCosto As String, ByVal SubTotal As Double, ByVal SubIgv As Double, ByVal NumeroOrden As String, ByVal NumeroContrato As String, ByVal IdUbiTrabajo As String, ByVal TipoRetencion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, sCodigoRegistro)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, IdContratista)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, TipoDoc)
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, DesPartida)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, Ubicacion)
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, Presupuesto)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, IdEspecialidad)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, Observacion)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, SubTotal)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, SubIgv)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, Retencion)
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, CodObra)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, User)

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, Trim(IdUbiTrabajo))
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, CCosto)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, NumeroOrden)
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, NumeroContrato)

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, TipoRetencion)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function




    Public Function fGrabarPartida(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal sCodigoRegistro As String, ByVal IdContratista As String, ByVal IdMoneda As String, ByVal TipoDoc As String, ByVal DesPartida As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Ubicacion As String, ByVal Presupuesto As Double, ByVal Retencion As Double, ByVal IdEspecialidad As String, ByVal Observacion As String, ByVal Estado As Integer, ByVal CodObra As String, ByVal Periodo As String, ByVal User As String, ByVal CCosto As String, ByVal SubTotal As Double, ByVal SubIgv As Double, ByVal NumeroOrden As String, ByVal NumeroContrato As String, ByVal IdUbiTrabajo As String, ByVal ConsCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, sCodigoRegistro)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, IdContratista)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, TipoDoc)
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, DesPartida)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, Ubicacion)
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, Presupuesto)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, IdEspecialidad)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, Observacion)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, SubTotal)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, SubIgv)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, Retencion)
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, CodObra)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, User)

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, Trim(IdUbiTrabajo))
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, CCosto)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, NumeroOrden)
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, NumeroContrato)

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, ConsCodigo)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fGrabar2(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal sCodigoRegistro As String, ByVal IdContratista As String, ByVal IdMoneda As String, ByVal TipoDoc As String, ByVal DesPartida As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Ubicacion As String, ByVal Presupuesto As Double, ByVal Retencion As Double, ByVal IdEspecialidad As String, ByVal Observacion As String, ByVal Estado As Integer, ByVal CodObra As String, ByVal Periodo As String, ByVal User As String, ByVal CCosto As String, ByVal SubTotal As Double, ByVal SubIgv As Double, ByVal NumeroOrden As String, ByVal NumeroContrato As String, ByVal IdUbiTrabajo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, sCodigoRegistro)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, IdContratista)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, TipoDoc)
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, DesPartida)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, Ubicacion)
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, Presupuesto)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, IdEspecialidad)
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, Observacion)
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, SubTotal)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, SubIgv)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, Retencion)
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, CodObra)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, User)

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, Trim(IdUbiTrabajo))
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, CCosto)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, NumeroOrden)
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, NumeroContrato)

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fGrabar2(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal sCodigoRegistro As String, ByVal MontoContratoActual As Double, ByVal MontoFGActual As Double, ByVal PagoFGActual As Double, ByVal PagoFacActual As Double, ByVal TotPagado As Double, ByVal TotRetenido As Double, ByVal Saldo As Double, ByVal xporabono As Double, ByVal xporsaldo As Double, ByVal User As String, ByVal Periodo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, sCodigoRegistro)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, xporabono)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, xporsaldo)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, User)

            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, MontoContratoActual)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, MontoFGActual)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, PagoFGActual)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, PagoFacActual)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, TotPagado)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, TotRetenido)

            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fGrabarDetalle(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal IdPartida As String, ByVal IdPartidaDet As String, ByVal DescripcionDet As String, ByVal Cant As Double, ByVal Und As String, ByVal M1 As Double, ByVal M2 As Double, ByVal PU As Double, ByVal Parcial As Double, ByVal Usuario As String, ByVal Con_Metrado As Double, ByVal Con_PrecioSubContra As Double, ByVal Con_TitCodigo As String, ByVal Con_ParCodigo As String, ByVal Con_ConsCodigo As String, ByVal M2Anterior As Double, ByVal Periodo As String, ByVal CC As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartida"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, IdPartida)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoDocCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ParDescripcion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ParUbicacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ParValorizado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ParObservacion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@xparpresrea", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xtotabonado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporabono", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xporsaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@xret", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodObra", DbType.String, 10, CC)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, Usuario)
            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, IdPartidaDet)
            cCapaDatos.sParametroSQL("@DescripcionDet", DbType.String, 200, DescripcionDet)
            cCapaDatos.sParametroSQL("@Cant", DbType.Decimal, 20, Cant)
            cCapaDatos.sParametroSQL("@Und", DbType.String, 10, Und)
            cCapaDatos.sParametroSQL("@M1", DbType.Decimal, 20, M1)
            cCapaDatos.sParametroSQL("@M2", DbType.Decimal, 20, M2)
            cCapaDatos.sParametroSQL("@PU", DbType.Decimal, 20, PU)
            cCapaDatos.sParametroSQL("@Parcial", DbType.Decimal, 20, Parcial)
            cCapaDatos.sParametroSQL("@NumeroOrden", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroContrato", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@MontoContratoActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFGActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PagoFacActual", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotPagado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, M2Anterior)


            cCapaDatos.sParametroSQL("@Con_Metrado", DbType.Decimal, 20, Con_Metrado)
            cCapaDatos.sParametroSQL("@Con_PrecioSubContra", DbType.Decimal, 20, Con_PrecioSubContra)
            cCapaDatos.sParametroSQL("@Con_TitCodigo", DbType.String, 5, Con_TitCodigo)
            cCapaDatos.sParametroSQL("@Con_ParCodigo", DbType.String, 10, Con_ParCodigo)
            cCapaDatos.sParametroSQL("@Con_ConsCodigo", DbType.String, 10, Con_ConsCodigo)
            cCapaDatos.sParametroSQL("@TipoRetencion", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    Public Function fCodigoDetDiario(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal IdPartida As String, ByVal periodo As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartidaDiaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdPartidaAvanceDiario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, IdPartida)
            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Semana", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@AvanceDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@CC", DbType.String, 10, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fListarDetalleDia(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal IdPartida As String, ByVal FechaDia As DateTime, ByVal periodo As String, ByVal CC As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartidaDiaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdPartidaAvanceDiario", DbType.String, 20, CC)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, IdPartida)
            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaDia, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Semana", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@AvanceDia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, periodo)
            cCapaDatos.sParametroSQL("@CC", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarDetalleAvanceDiario(ByVal iOpcion As Integer, ByVal IdPartidaAvanceDiario As String, ByVal IdPartida As String, ByVal IdPartidaDet As String, ByVal CodEmpresa As String, ByVal Fecha As DateTime, ByVal Semana As Integer, ByVal AvanceDia As Double, ByVal User As String, ByVal Perido As String, ByVal CC As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoPartidaDiaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdPartidaAvanceDiario", DbType.String, 20, IdPartidaAvanceDiario)
            cCapaDatos.sParametroSQL("@IdPartida", DbType.String, 20, IdPartida)
            cCapaDatos.sParametroSQL("@IdPartidaDet", DbType.String, 20, IdPartidaDet)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Semana", DbType.Int32, 2, Semana)
            cCapaDatos.sParametroSQL("@AvanceDia", DbType.Decimal, 20, AvanceDia)
            cCapaDatos.sParametroSQL("@User", DbType.String, 20, User)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Perido)
            cCapaDatos.sParametroSQL("@CC", DbType.String, 10, CC)
            'CC

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

End Class
