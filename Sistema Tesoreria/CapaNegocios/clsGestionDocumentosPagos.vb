Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsGestionDocumentosPagos

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

    Public Function fCargarReporte(ByVal vDocOrigen As Decimal, ByVal vId_DocPendiente As Decimal, ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspReporteGestionDocumentosPagos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@DOCORIGEN", DbType.Decimal, 18, vDocOrigen)
            cCapaDatos.sParametroSQL("@ID_DOCPENDIENTE", DbType.Decimal, 18, vId_DocPendiente)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarReporteTodo(ByVal vDocOrigen As Decimal, ByVal vFechaInicio As DateTime, ByVal vFechaFin As DateTime, ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspReporteGestionDocumentosPagosTodo"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@DOCORIGEN", DbType.Decimal, 18, vDocOrigen)
            cCapaDatos.sParametroSQL("@FECHAINICIO", DbType.Date, 10, vFechaInicio)
            cCapaDatos.sParametroSQL("@FECHAFIN", DbType.Date, 10, vFechaFin)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class

