Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsBusquedaGastos

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fConsultaGastos(ByVal iOpcion As Integer, ByVal Texto As String, ByVal CodEmpresa As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, Texto)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            'iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
            iNroRegistros = dtTable.Rows.Count
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fConsultaGastos2(ByVal iOpcion As Integer, ByVal Texto As String, ByVal CodEmpresa As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal EstadoEnvio As Integer, ByVal IdFormaPago As String, ByVal EstadoRecibo As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, Texto)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, EstadoEnvio)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, EstadoRecibo)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fConsultaGastos3(ByVal iOpcion As Integer, ByVal Texto As String, ByVal CodEmpresa As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal EstadoEnvio As Integer, ByVal EstadoRecibo As Integer, ByVal Rendicion1 As String, ByVal Rendicion2 As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, Texto)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, EstadoEnvio)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, EstadoRecibo)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, Rendicion1)
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, Rendicion2)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fEnviarDoc(ByVal vIdMovimiento As String, ByVal CodEmpresa As String, ByVal EstadoEnvio As Integer, ByVal FechaEnvio As DateTime, ByVal ObservacionRechazo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, EstadoEnvio)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(FechaEnvio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, ObservacionRechazo)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, vIdMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEnviarDocRendido(ByVal vIdMovimiento As String, ByVal CodEmpresa As String, ByVal EstadoEnvio As Integer, ByVal FechaEnvio As DateTime, ByVal ObservacionRechazo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 36)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, EstadoEnvio)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(FechaEnvio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, ObservacionRechazo)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, vIdMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fConfirmarDoc(ByVal vIdMovimiento As String, ByVal CodEmpresa As String, ByVal EstadoRecibo As Integer, ByVal FechaRecibo As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, EstadoRecibo)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(FechaRecibo, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, vIdMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fConfirmarDoc2(ByVal vIdMovimiento As String, ByVal CodEmpresa As String, ByVal EstadoRecibo As Integer, ByVal FechaRecibo As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 37)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, EstadoRecibo)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(FechaRecibo, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, vIdMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fRechazDoc(ByVal vIdMovimiento As String, ByVal CodEmpresa As String, ByVal EstadoEnvio As Integer, ByVal Observacion As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, EstadoEnvio)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, Observacion)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, vIdMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fGrabarRendidoOPorRendir(ByVal iOpcion As Integer, ByVal vIdMovimiento As String, ByVal CodEmpresa As String, ByVal IdRendicion As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaGastos2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, vIdMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Rendicion1", DbType.String, 2, IdRendicion)
            cCapaDatos.sParametroSQL("@Rendicion2", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    '-----------------------------------------------------------------------------------------------


    Public Function fConsultaGastosEnviados(ByVal iOpcion As Integer, ByVal Texto As String, ByVal CodEmpresa As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal IdCuenta As String, ByVal IdBanco As String, ByVal xRUC As String, ByVal xDESEMPRESA As String, ByVal User As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspImprimirGastosEnviados"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, Texto)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@EstadoEnvio", DbType.Int32, 1, EstadoEnvio)
            'cCapaDatos.sParametroSQL("@FechaEnvio", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@EstadoRecibo", DbType.Int32, 1, EstadoRecibo)
            'cCapaDatos.sParametroSQL("@FechaRecibo", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@ObservacionRechazo", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@CampoOrdenar", DbType.String, 50, User)
            cCapaDatos.sParametroSQL("@AscDesc", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@xRUC", DbType.String, 15, xRUC)
            cCapaDatos.sParametroSQL("@xDESEMPRESA", DbType.String, 100, xDESEMPRESA)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fConsultaIngresosaBancos(ByVal iOpcion As Integer, ByVal Texto As String, ByVal CodEmpresa As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal IdCuenta As String, ByVal IdBanco As String, ByVal xRUC As String, ByVal xDESEMPRESA As String, ByVal User As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspImprimirIngresosXCuenta"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, Texto)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@CampoOrdenar", DbType.String, 50, User)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class