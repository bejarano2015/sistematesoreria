Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsContratistasControl

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal txtBusqueda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoContratistas"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoConCodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConNombre", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@ConApellido", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 200, txtBusqueda)
            cCapaDatos.sParametroSQL("@ConRuc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@ConDireccion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@ConTelefono", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConEmail", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@ConEstado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
