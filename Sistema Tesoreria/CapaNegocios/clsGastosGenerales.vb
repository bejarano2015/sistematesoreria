Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsGastosGenerales

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodTransId As String
    Public sCodFuturoTrans As String = "0"
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosCtasxBanc As Integer = "0"
    Public iNroRegistrosChequeras As Integer = "0"

    Public Function fListarBancos(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarMonedas() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 43)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function CategoriaGastosGenerales() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 45)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    Public Function fListarCategoria() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 44)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarBancos2(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCuentasxBancoyEmpresa(ByVal vCodigoEmpresa As String, ByVal vCodigoBanco As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vCodigoBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistrosCtasxBanc = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCuentasxBancoyEmpresa2(ByVal vCodigoEmpresa As String, ByVal vCodigoBanco As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vCodigoBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fTraerCodMonedaCuenta(ByVal vCodigoCuenta As String, ByVal empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vCodigoCuenta)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function
    Public Function fTraerCodMonedaCuentaCombo(ByVal vCodigoCuenta As String, ByVal empresa As String, ByVal IdBanco As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vCodigoCuenta)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function
    Public Function fListarCentrosCostos(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarEmpresas(ByVal Opcion As Integer, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, Opcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarProveedores() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 49)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarProveedoresxEmpresa(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 51)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarProveedores2(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 50)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarEmpresasUno(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    Public Function fListarCCxEmpresa(ByVal IdEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 25)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarRucxEmpresa(ByVal IdEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, IdEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTiposdeGasto(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTiposMovimiento() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'fListarTiposMovimiento

    Public Function TraerDescripCosto(ByVal vCentroCosto As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, vCentroCosto)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function

    Public Function fListarFormaPago() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarTipoProveedores(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarProveedoresPorTipo(ByVal CodEmpresa As String, ByVal CodIdAnexo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, CodIdAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCronogramas(ByVal CodIdTipoAnexo As String, ByVal CodIdAnexo As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, CodIdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, CodIdAnexo)
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCronogramas2(ByVal CodRuc As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, CodRuc)
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCronogramas3(ByVal DescProv As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 14)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, DescProv)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCronogramas4(ByVal CodRuc As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, CodRuc)
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCronogramas5(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarMaximaSemana(ByVal CodEmpresa As String, ByVal Anio As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 35)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, Trim(Anio))
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarSemanasdePago(ByVal CodEmpresa As String, ByVal Anio As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 40)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, Trim(Anio))
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarFacturasxSemana(ByVal CodEmpresa As String, ByVal Anio As String, ByVal Semana As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 36)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, Semana)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, Trim(Anio))
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarFacturasxSemanaRuc(ByVal CodEmpresa As String, ByVal Anio As String, ByVal Semana As String, ByVal Ruc As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 37)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, Semana)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, Trim(Anio))
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, Trim(Ruc))
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarFacturasxSemanaRazon(ByVal CodEmpresa As String, ByVal Anio As String, ByVal Semana As String, ByVal Razon As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 38)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, Semana)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, Trim(Anio))
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, Trim(Razon))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarLetrasxSemana(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal Ruc As String, ByVal Razon As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, Trim(Razon))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCronogramas6(ByVal CodEmpresa As String, ByVal IdAnexo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 17)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCronogramas7(ByVal CodEmpresa As String, ByVal NombreAnexo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 18)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, NombreAnexo)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCronogramas8(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarSaldoDocs(ByVal CodEmpresa As String, ByVal CodAnexo As String, ByVal TipoAnexo As String, ByVal CodMoneda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 20)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, TipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, CodAnexo)
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, CodMoneda)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarChequerasxBancoCuenyEmpr(ByVal IdBanco As String, ByVal IdCuenta As String, ByVal Empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 29)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistrosChequeras = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarChequesBancoyCuentas(ByVal IdBanco As String, ByVal IdCuenta As String, ByVal Empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTransaccion(ByVal IdTransaccion As String, ByVal Empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 34)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdTransaccion)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigoTransaccion(ByVal Empresa As String, ByVal Periodo As String) As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoTrans = iCodigo 'Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function fCodigoCarta(ByVal Empresa As String, ByVal Periodo As String) As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoTrans = iCodigo 'Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fIdTransaccion(ByVal Empresa As String) As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 33)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodTransId = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function fIdCarta(ByVal Empresa As String) As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 42)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodTransId = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function fListarChequesxChequeraEmpr(ByVal IdChequera As String, ByVal Empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 30)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdChequera)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCantidadChequesxChequeraEmpr(ByVal IdChequera As String, ByVal Empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 31)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdChequera)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fMontoResumen(ByVal IdResumen As String, ByVal Empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 39)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarMovimientoCaja(ByVal IdMovimiento As String, ByVal IdTipoMov As String, ByVal IdCheque As String, ByVal MonCodigo As String, ByVal IdEstadoMovimiento As String, ByVal IRendicion As String, ByVal IdFormaPago As String, ByVal IdSesion As String, ByVal IdPeriodo As String, ByVal FechaMovimiento As DateTime, ByVal MontoSoles As Double, ByVal MontoDolares As Double, ByVal DocumentoOrigen As String, ByVal DocumentoReferencia As String, ByVal NombreUsuario As String, ByVal Observacion As String, ByVal EmprCodigo As String, ByVal TComCodigo As String, ByVal Estado As Integer, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdCosCodigo As String, ByVal TipoCambio As Double, ByVal NroVaucher As String, ByVal IdBanco As String, ByVal NumeroCuenta As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCaja"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, "")
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, "")
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fListar(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarxFecha(ByVal vCodigoEmpresa As String, ByVal FechaGasto As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 27)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(FechaGasto, "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerNroChequeradeCheque(ByVal CodigoCheque As String, ByVal Empresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 21)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, CodigoCheque)
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarEstadoDocumentosxEmpresaAlaFecha(ByVal iOpcion As Integer, ByVal Empresa As String, ByVal CodProveedor As String, ByVal FechaBase As DateTime, ByVal FechaBaseIni As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, CodProveedor)
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(FechaBase, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(FechaBaseIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarEstadoDocumentosCCosto(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodCC As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaEstadoDocumentosCCosto"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CodCC)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarEstadoDocumentosCCostoyTG(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodCC As String, ByVal TipoGasto As Integer, ByVal FIni As DateTime, ByVal FFin As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspReporteGastosVariosxCCyTG"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CCosto", DbType.String, 10, CodCC)
            cCapaDatos.sParametroSQL("@CodTipGas", DbType.Int32, 2, TipoGasto)
            cCapaDatos.sParametroSQL("@FechaIni", DbType.DateTime, 10, Format(FIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(FFin, "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarGastosDeObra(ByVal Opcion As Integer, ByVal EmprCodigo As String, ByVal Mes As Integer, ByVal MesCadena As String, ByVal Anio As String, ByVal Empresa As String, ByVal RUC As String, ByVal TC As Decimal) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspReporteGastosDeObra2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, Opcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Mes", DbType.Int32, 1, Mes)
            cCapaDatos.sParametroSQL("@MesCadena", DbType.String, 20, MesCadena)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@Empresa", DbType.String, 400, Empresa)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, RUC)
            cCapaDatos.sParametroSQL("@TC", DbType.Decimal, 20, TC)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarSubContratistas() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 55)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function
    Public Function fListarSubContratistasxEmpresa(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspGastosGenerales"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 56)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCentroC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreAnexo", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            'cCapaDatos.sParametroSQL("@FechaMovimientoIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class