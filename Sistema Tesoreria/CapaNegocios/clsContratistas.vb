Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsContratistas

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal txtBusqueda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoContratistas"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoConCodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConNombre", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@ConApellido", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 200, txtBusqueda)
            cCapaDatos.sParametroSQL("@ConRuc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@ConDireccion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@ConTelefono", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConEmail", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@ConEstado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocIde", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NomRep", DbType.String, 400, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fCodigo(ByVal iOpcion As Integer, ByVal CodEmpr As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoContratistas"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoConCodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConNombre", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@ConApellido", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@ConRuc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@ConDireccion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@ConTelefono", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConEmail", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@ConEstado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocIde", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NomRep", DbType.String, 400, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal sCodigoRegistro As String, ByVal vIdTipoCon As String, ByVal vNombre As String, ByVal vApellido As String, ByVal Especialidad As String, ByVal Direccion As String, ByVal Ruc As String, ByVal Telefono As String, ByVal Email As String, ByVal vEstado As Integer, ByVal esp_codigo As String, ByVal IdDoc As String, ByVal NroDoc As String, ByVal NomRepre As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoContratistas"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, sCodigoRegistro)
            cCapaDatos.sParametroSQL("@TipoConCodigo", DbType.String, 20, vIdTipoCon)
            cCapaDatos.sParametroSQL("@ConNombre", DbType.String, 400, vNombre)
            cCapaDatos.sParametroSQL("@ConApellido", DbType.String, 150, vApellido)
            cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 200, Especialidad)
            cCapaDatos.sParametroSQL("@ConRuc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@ConDireccion", DbType.String, 150, Direccion)
            cCapaDatos.sParametroSQL("@ConTelefono", DbType.String, 20, Telefono)
            cCapaDatos.sParametroSQL("@ConEmail", DbType.String, 100, Email)
            cCapaDatos.sParametroSQL("@ConEstado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, esp_codigo)

            cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, IdDoc)
            cCapaDatos.sParametroSQL("@NroDocIde", DbType.String, 20, NroDoc)
            cCapaDatos.sParametroSQL("@NomRep", DbType.String, 400, NomRepre)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    'Public Function fListar(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal txtBusqueda As String) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspMantenimientoContratistas"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
    '        cCapaDatos.sParametroSQL("@IdContratista", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@TipoConCodigo", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@ConNombre", DbType.String, 400, "")
    '        cCapaDatos.sParametroSQL("@ConApellido", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 200, txtBusqueda)
    '        cCapaDatos.sParametroSQL("@ConRuc", DbType.String, 11, "")
    '        cCapaDatos.sParametroSQL("@ConDireccion", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@ConTelefono", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@ConEmail", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@ConEstado", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@esp_codigo", DbType.String, 20, "")


    '        cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@NroDocIde", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@NomRep", DbType.String, 400, "")

    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

End Class
