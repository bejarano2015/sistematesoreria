'Imports System.Data
'Imports System.Data.SqlClient

'Imports Microsoft.Practices.EnterpriseLibrary.Data
'Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql

Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class Usuarios

    'Dim cNameBd As String
    'Dim cNameStoreP As String = "planilla.uspUsuario"

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function BuscaUsuario(ByVal CodEmpresa As String, ByVal SisCodigo As String, ByVal Usuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, Usuario)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function BuscaAreasxUsuario(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal SisCodigo As String, ByVal Usuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, Usuario)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function ListaUsuario(ByVal CodEmpresa As String, ByVal SisCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function EliminaUsuario(ByVal CodigoEmpresa As String, ByVal SisCodigo As String, ByVal Usuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, CodigoEmpresa)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, Usuario)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    ' @EmpCodigo,@SisCodigo,@UsuCodigo,@UsuPwd,@UsuNombre,@UsuCategoria

    Public Function fGrabarUsuario(ByVal vOpcion As Integer, ByVal EmprCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String, ByVal UsuPwd As String, ByVal UsuNombre As String, ByVal UsuCategoria As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, UsuCodigo)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, UsuPwd)
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, UsuNombre)
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, UsuCategoria)
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function ListaPermisoMenu(ByVal CodEmpresa As String, ByVal SisCodigo As String, ByVal Usuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, Usuario)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function Listar(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal SisCodigo As String, ByVal Usuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, Usuario)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function ListarExiste(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal SisCodigo As String, ByVal Usuario As String, ByVal Area As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, Usuario)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, Area)
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function ActualizaPermisoMenu(ByVal EmprCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String, ByVal Habilita As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, UsuCodigo)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, Habilita)
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fGrabarUsuarioOpciones(ByVal vOpcion As Integer, ByVal EmprCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String, ByVal Area As String, ByVal Activo As Integer, ByVal UsuNombre As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspUsuario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmpCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 11, UsuCodigo)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuPwd", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@UsuNombre", DbType.String, 50, UsuNombre)
            cCapaDatos.sParametroSQL("@UsuCategoria", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Habilita", DbType.String, 200, Area)
            cCapaDatos.sParametroSQL("@Activo", DbType.Int32, 1, Activo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    '    Public Function ListaUsuario(ByVal EmpCodigo As String, ByVal SisCodigo As String) As DataSet
    '        Try

    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim dbCommand As SqlCommand = db.GetStoredProcCommand(cNameStoreP, 0, EmpCodigo, SisCodigo, "", "", "", "", "", "", 0)

    '            Dim ProductDataSet As DataSet = db.ExecuteDataSet(dbCommand)

    '            Return ProductDataSet
    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function

    '    Public Function BuscaUsuario(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String) As DataSet
    '        Try

    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim dbCommand As SqlCommand = db.GetStoredProcCommand(cNameStoreP, 4, EmpCodigo, SisCodigo, UsuCodigo, "", "", "", "", "", 0)

    '            Dim ProductDataSet As DataSet = db.ExecuteDataSet(dbCommand)

    '            Return ProductDataSet
    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function
    '    Public Function AgregaUsuario(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String, ByVal UsuPwd As String, ByVal UsuNombre As String, ByVal UsuCategoria As String) As Integer
    '        Try

    '            Dim n As Integer
    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim cmd As SqlCommand = db.GetStoredProcCommand(cNameStoreP)

    '            db.AddInParameter(cmd, "@Opcion", DbType.Int32, 1)
    '            db.AddInParameter(cmd, "@EmpCodigo", DbType.String, EmpCodigo)
    '            db.AddInParameter(cmd, "@SisCodigo", DbType.String, SisCodigo)
    '            db.AddInParameter(cmd, "@UsuCodigo", DbType.String, UsuCodigo)
    '            db.AddInParameter(cmd, "@TPlaCodigo", DbType.String, "")
    '            db.AddInParameter(cmd, "@UsuPwd", DbType.String, UsuPwd)
    '            db.AddInParameter(cmd, "@UsuNombre", DbType.String, UsuNombre)
    '            db.AddInParameter(cmd, "@UsuCategoria", DbType.String, UsuCategoria)
    '            db.AddInParameter(cmd, "@Habilita", DbType.String, "")
    '            db.AddInParameter(cmd, "@Activo", DbType.Int32, 0)
    '            n = db.ExecuteNonQuery(cmd)
    '            Return n

    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function

    '    Public Function ActualizaUsuario(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String, ByVal UsuPwd As String, ByVal UsuNombre As String, ByVal UsuCategoria As String) As Integer
    '        Try
    '            Dim n As Integer
    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim cmd As SqlCommand = db.GetStoredProcCommand(cNameStoreP)

    '            db.AddInParameter(cmd, "@Opcion", DbType.Int32, 2)
    '            db.AddInParameter(cmd, "@EmpCodigo", DbType.String, EmpCodigo)
    '            db.AddInParameter(cmd, "@SisCodigo", DbType.String, SisCodigo)
    '            db.AddInParameter(cmd, "@UsuCodigo", DbType.String, UsuCodigo)
    '            db.AddInParameter(cmd, "@TPlaCodigo", DbType.String, "")
    '            db.AddInParameter(cmd, "@UsuPwd", DbType.String, UsuPwd)
    '            db.AddInParameter(cmd, "@UsuNombre", DbType.String, UsuNombre)
    '            db.AddInParameter(cmd, "@UsuCategoria", DbType.String, UsuCategoria)
    '            db.AddInParameter(cmd, "@Habilita", DbType.String, "")
    '            db.AddInParameter(cmd, "@Activo", DbType.Int32, 0)
    '            n = db.ExecuteNonQuery(cmd)
    '            Return n

    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function
    '    Public Function ListaPermiso(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String) As DataSet
    '        Try

    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim dbCommand As SqlCommand = db.GetStoredProcCommand(cNameStoreP, 5, EmpCodigo, SisCodigo, UsuCodigo, "", "", "", "", "", 0)

    '            Dim ProductDataSet As DataSet = db.ExecuteDataSet(dbCommand)

    '            Return ProductDataSet
    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function
    '    Public Function ActualizaPermiso(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String, _
    '    ByVal TPlaCodigo As String, ByVal Activo As Integer) As Integer
    '        Try
    '            Dim n As Integer
    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim cmd As SqlCommand = db.GetStoredProcCommand(cNameStoreP)

    '            db.AddInParameter(cmd, "@Opcion", DbType.Int32, 6)
    '            db.AddInParameter(cmd, "@EmpCodigo", DbType.String, EmpCodigo)
    '            db.AddInParameter(cmd, "@SisCodigo", DbType.String, SisCodigo)
    '            db.AddInParameter(cmd, "@UsuCodigo", DbType.String, UsuCodigo)
    '            db.AddInParameter(cmd, "@TPlaCodigo", DbType.String, TPlaCodigo)
    '            db.AddInParameter(cmd, "@UsuPwd", DbType.String, "")
    '            db.AddInParameter(cmd, "@UsuNombre", DbType.String, "")
    '            db.AddInParameter(cmd, "@UsuCategoria", DbType.String, "")
    '            db.AddInParameter(cmd, "@Habilita", DbType.String, "")
    '            db.AddInParameter(cmd, "@Activo", DbType.Int32, Activo)
    '            n = db.ExecuteNonQuery(cmd)
    '            Return n

    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function
    '    Public Function ListaPermisoMenu(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String) As DataSet
    '        Try

    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim dbCommand As SqlCommand = db.GetStoredProcCommand(cNameStoreP, 7, EmpCodigo, SisCodigo, UsuCodigo, "", "", "", "", "", 0)

    '            Dim ProductDataSet As DataSet = db.ExecuteDataSet(dbCommand)

    '            Return ProductDataSet
    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function
    '    Public Function ActualizaPermisoMenu(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String, _
    'ByVal Habilita As String) As Integer
    '        Try
    '            Dim n As Integer
    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim cmd As SqlCommand = db.GetStoredProcCommand(cNameStoreP)

    '            db.AddInParameter(cmd, "@Opcion", DbType.Int32, 8)
    '            db.AddInParameter(cmd, "@EmpCodigo", DbType.String, EmpCodigo)
    '            db.AddInParameter(cmd, "@SisCodigo", DbType.String, SisCodigo)
    '            db.AddInParameter(cmd, "@UsuCodigo", DbType.String, UsuCodigo)
    '            db.AddInParameter(cmd, "@TPlaCodigo", DbType.String, "")
    '            db.AddInParameter(cmd, "@UsuPwd", DbType.String, "")
    '            db.AddInParameter(cmd, "@UsuNombre", DbType.String, "")
    '            db.AddInParameter(cmd, "@UsuCategoria", DbType.String, "")
    '            db.AddInParameter(cmd, "@Habilita", DbType.String, Habilita)
    '            db.AddInParameter(cmd, "@Activo", DbType.Int32, 0)
    '            n = db.ExecuteNonQuery(cmd)
    '            Return n

    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function
    '    Public Function BuscaAccesoTPlanilla(ByVal EmpCodigo As String, ByVal SisCodigo As String, ByVal UsuCodigo As String) As DataSet
    '        Try

    '            Dim db As Database = DatabaseFactory.CreateDatabase(cNameBd)
    '            Dim dbCommand As SqlCommand = db.GetStoredProcCommand(cNameStoreP, 9, EmpCodigo, SisCodigo, UsuCodigo, "", "", "", "", "", 0)

    '            Dim ProductDataSet As DataSet = db.ExecuteDataSet(dbCommand)

    '            Return ProductDataSet
    '        Catch ex As Exception
    '            MsgBox(ex.Message, MsgBoxStyle.Critical, "Sistema de Planilla")
    '        End Try
    '    End Function
    '    Sub New(ByVal strCadena As String)
    '        cNameBd = strCadena
    '    End Sub


End Class
