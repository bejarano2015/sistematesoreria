Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsEspecialidad

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal iOpcion As Integer, ByVal txtBusqueda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoEspecialidad"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdEspecialidad", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fCodigo(ByVal iOpcion As Integer) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoEspecialidad"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdEspecialidad", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal iOpcion As Integer, ByVal sCodigoRegistro As String, ByVal Especialidad As String, ByVal vEstado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMantenimientoEspecialidad"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdEspecialidad", DbType.String, 20, sCodigoRegistro)
            cCapaDatos.sParametroSQL("@ConEspecialidad", DbType.String, 150, Especialidad)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

End Class
