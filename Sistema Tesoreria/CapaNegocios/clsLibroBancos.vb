Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsLibroBancos

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private dtTable2 As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodFuturoDet As String
    Public sNumSiguienteChequera As String
    Public sUltimoIdCheque As String
    Public sNombrebanco As String
    Public sNumeroCuenta As String
    Public iNroRegistros As Integer = "0"

    Public Function fListarTipoMov(ByVal iOpcion As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")

            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")

            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTipoMovPagos() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 20)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerIdLibro(ByVal ano As String, ByVal mes As String, ByVal IdCuenta As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, ano)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerIdLibro2(ByVal IdLibroCab As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 34)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, IdLibroCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerConciliadoDetLibro(ByVal iOpcion As Integer, ByVal IdLibroCab As String, ByVal IdLibroDet As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, IdLibroCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, IdLibroDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerLibrosSuperiores(ByVal Mes As String, ByVal Anio As String, ByVal IdCuenta As String, ByVal CodEmpresa As String, ByVal Fecha As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 35)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerLibrosSuperiores2(ByVal iOpcion As Integer, ByVal Mes As String, ByVal Anio As String, ByVal IdCuenta As String, ByVal CodEmpresa As String, ByVal Fecha As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerLibros(ByVal IdCuenta As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 31)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerRegDetalleAnt(ByVal CodEmpresa As String, ByVal CodAnterior As String, ByVal sCodigoCab As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, sCodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodAnterior)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function TraerMesyAnoLibro(ByVal CodEmpresa As String, ByVal CodLibro As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 30)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodLibro)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function TraerSiExisteItf(ByVal CodEmpresa As String, ByVal CodLibro As String, ByVal ItfIdLibroCab As String, ByVal ItfIdLibroDet As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 49)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodLibro)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, ItfIdLibroCab)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, ItfIdLibroDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function TraerSiTieneCuentaDestino(ByVal CodEmpresa As String, ByVal CodLibro As String, ByVal IdLibroDet As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 50)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodLibro)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, IdLibroDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigo(ByVal CodEmpresa As String) As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fCodigoDet(ByVal CodEmpresa As String, ByVal CodCodigo As String) As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodCodigo)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoDet = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal Opcion As Integer, ByVal CodigoCab As String, ByVal CodEmpresa As String, ByVal Cuenta As String, ByVal Mes As String, ByVal Ano As String, ByVal SaldoInicial As Double, ByVal Saldo As Double, ByVal Fecha As DateTime, ByVal SaldoPrenda As Double, ByVal SaldoGarantia As Double, ByVal SaldoDepPlaz As Double, ByVal SaldoRetencion As Double, ByVal SaldoIniExtracto As Double) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, Opcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Ano)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, Cuenta)
            'SaldoInicial()
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, SaldoIniExtracto)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, SaldoPrenda)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, SaldoGarantia)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, SaldoDepPlaz)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, SaldoRetencion)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
            'Catch ex As Exception
            'cTempo.sMensaje(Convert.ToString(ex.Message))

            'ex.InnerException.Message.
            'cTempo.sMensaje(Convert.ToString(ex.Message.))
            'cCapaDatos.fCmdExecuteNonQuery.ToString.Length.
            'cTempo.sMensaje(Convert.ToString(cCapaDatos.fCmdExecuteNonQuery.))

        Catch errores As System.Data.SqlClient.SqlException
            iResultadoCabecera = errores.Number
            '    MessageBox.Show(ex.Errors[0].Number.ToString()) 
            '    errores.ErrorCode.Number.ToString()

        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActualizarLibroAsignado(ByVal Opcion As Integer, ByVal CodigoCab As String, ByVal CodEmpresa As String, ByVal SaldoPrenda As Double, ByVal SaldoGarantia As Double, ByVal SaldoDepPlaz As Double, ByVal SaldoRetencion As Double) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, Opcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            'SaldoInicial()
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, SaldoPrenda)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, SaldoGarantia)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, SaldoDepPlaz)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, SaldoRetencion)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fGrabar2(ByVal Opcion As Integer, ByVal CodigoCab As String, ByVal CodEmpresa As String, ByVal Cuenta As String, ByVal Mes As String, ByVal Ano As String, ByVal SaldoInicial As Double, ByVal Saldo As Double, ByVal CheCobradosSI As Double, ByVal CheCobradosNO As Double, ByVal Fecha As DateTime) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, Opcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Ano)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, Cuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, CheCobradosSI)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, CheCobradosNO)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fGrabarDet(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal Saldo As Double, ByVal Marca As Integer, ByVal Marca2 As Integer, ByVal NroMovimientoAnx As String, ByVal FechaAperturaAnx As DateTime, ByVal FechaVencimientoAnx As DateTime, ByVal MontoAnx As Double, ByVal CodMonedaAnx As String, ByVal CodCtaCargoAnx As String, ByVal ClienteAnx As String, ByVal CtaClienteAnx As String, ByVal ObraAnx As String, ByVal TipoCtaAnx As String, ByVal ItfIdLibroCab As String, ByVal ItfIdLibroDet As String) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)

            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, Concepto)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, Descripcion)
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, NroMovimientoAnx)
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(FechaAperturaAnx, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(FechaVencimientoAnx, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, MontoAnx)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, CodMonedaAnx)
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, CodCtaCargoAnx)
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, ClienteAnx)
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, CtaClienteAnx)
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, ObraAnx)
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, TipoCtaAnx)

            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")

            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, ItfIdLibroCab)
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, ItfIdLibroDet)
            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    

    Public Function fActSaldoLibro(ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal SaldoInicial As Double, ByVal SaldoNew As Double, ByVal ChequesPagados As Double, ByVal ChequesNoPagados As Double, ByVal FechaActCabecera As DateTime) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, ChequesPagados)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, ChequesNoPagados)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaActCabecera, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera

    End Function

    Public Function fActSaldoLibroGarantia(ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal SaldoInicial As Double, ByVal SaldoNew As Double, ByVal ChequesPagados As Double, ByVal ChequesNoPagados As Double, ByVal FechaActCabecera As DateTime, ByVal SaldoNewGarantia As Double) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 44)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, ChequesPagados)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, ChequesNoPagados)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaActCabecera, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, SaldoNewGarantia)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActSaldoLibroPrenda(ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal SaldoInicial As Double, ByVal SaldoNew As Double, ByVal ChequesPagados As Double, ByVal ChequesNoPagados As Double, ByVal FechaActCabecera As DateTime, ByVal SaldoNewPrenda As Double) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 45)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, ChequesPagados)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, ChequesNoPagados)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaActCabecera, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, SaldoNewPrenda)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActSaldoDepPlaz(ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal SaldoInicial As Double, ByVal SaldoNew As Double, ByVal ChequesPagados As Double, ByVal ChequesNoPagados As Double, ByVal FechaActCabecera As DateTime, ByVal SaldoNewDepPlaz As Double) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 46)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, ChequesPagados)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, ChequesNoPagados)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaActCabecera, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, SaldoNewDepPlaz)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActSaldoRetencion(ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal SaldoInicial As Double, ByVal SaldoNew As Double, ByVal ChequesPagados As Double, ByVal ChequesNoPagados As Double, ByVal FechaActCabecera As DateTime, ByVal SaldoNewRetencion As Double) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 47)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, ChequesPagados)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, ChequesNoPagados)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaActCabecera, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, SaldoNewRetencion)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActSaldoLibro2(ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal SaldoInicial As Double, ByVal SaldoNew As Double, ByVal ChequesPagados As Double, ByVal ChequesNoPagados As Double, ByVal FechaActCabecera As DateTime, ByVal SaldoPrendaS As Double, ByVal SaldoGarantiaS As Double, ByVal SaldoDepPlazS As Double, ByVal SaldoRetencionS As Double) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 36)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, ChequesPagados)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, ChequesNoPagados)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaActCabecera, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, SaldoPrendaS)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, SaldoGarantiaS)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, SaldoDepPlazS)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, SaldoRetencionS)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActDet(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal SaldoNew As Double, ByVal Marca As Integer, ByVal Marca2 As Integer, ByVal NroMovimientoAnx As String, ByVal FechaAperturaAnx As DateTime, ByVal FechaVencimientoAnx As DateTime, ByVal MontoAnx As Double, ByVal CodMonedaAnx As String, ByVal CodCtaCargoAnx As String, ByVal ClienteAnx As String, ByVal CtaClienteAnx As String, ByVal ObraAnx As String, ByVal TipoCtaAnx As String) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)

            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, Concepto)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, Descripcion)
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, NroMovimientoAnx)
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(FechaAperturaAnx, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(FechaVencimientoAnx, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, MontoAnx)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, CodMonedaAnx)
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, CodCtaCargoAnx)
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, ClienteAnx)
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, CtaClienteAnx)
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, ObraAnx)
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, TipoCtaAnx)

            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActMarcaDet(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal SaldoNew As Double, ByVal Marca As Integer, ByVal Marca2 As Integer) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 29)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)

            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")


            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActSaldoDet(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Saldo As Decimal) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, Saldo)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActDet2(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal SaldoNew As Double, ByVal Marca As Integer, ByVal Marca2 As Integer) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 21)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActDelete2(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal SaldoNew As Double, ByVal Marca As Integer, ByVal Marca2 As Integer) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function





    Public Function fIdenti(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal iIdent As Integer, ByVal FechaPago As DateTime) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaPago, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, iIdent)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function


    Public Function fActDeleteTransaccion(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal SaldoNew As Double, ByVal Marca As Integer, ByVal Marca2 As Integer, ByVal Periodo As String) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 27)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fActDeleteCarta(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal SaldoNew As Double, ByVal Marca As Integer, ByVal Marca2 As Integer, ByVal Periodo As String) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 48)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function
    Public Function fActDet3(ByVal CodEmpresa As String, ByVal CodigoDet As String, ByVal CodigoCab As String, ByVal Fecha As DateTime, ByVal TipoMov As String, ByVal NroDoc As String, ByVal Concepto As String, ByVal Descripcion As String, ByVal TipoOpe As String, ByVal Importe As Double, ByVal SaldoNew As Double, ByVal Marca As Integer, ByVal Marca2 As Integer) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoNew)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, CodigoDet)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, TipoMov)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDoc)


            

            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, Concepto)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, Descripcion)
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")



            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, TipoOpe)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Marca)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, Marca2)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function TraerLibroDet(ByVal iOpcion As String, ByVal CodLibro As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodLibro)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")


            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")


            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fConsultaCheque(ByVal iOpcion As String, ByVal Texto As String, ByVal CodEmpresa As String, ByVal IdCuenta As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@Concepto", DbType.String, 100, Texto)
            'cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, Texto)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")

            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fConsultaCuentasLibros(ByVal iOpcion As String, ByVal Mes As String, ByVal Anio As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@Concepto", DbType.String, 100, "")
            'cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")

            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")

            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCheqNoCobrados(ByVal CodCuenta As String, ByVal CodEmpresa As String, ByVal Mes As String, ByVal Anio As String, ByVal Fecha As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, CodCuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fResumenLibro(ByVal CodCuenta As String, ByVal CodEmpresa As String, ByVal Mes As String, ByVal Anio As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, Anio)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, CodCuenta)
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'TraerSaldoLibroAnt
    Public Function TraerSaldoLibroAnt(ByVal CodLibro As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodLibro)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function TraerMaxFechadeLibro(ByVal CodEmpresa As String, ByVal IdLibro As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 25)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, IdLibro)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, 0)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    Public Function TraerExtracto(ByVal ano As String, ByVal mes As String, ByVal IdCuenta As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspImprimirLibroBancosExtracto"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            'cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, mes)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, ano)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fActSaldosExtractoLibro(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal SaldoInicial As Double, ByVal SaldoFin As Double, ByVal ChequesPagados As Double, ByVal ChequesNoPagados As Double, ByVal FechaActCabecera As DateTime) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, SaldoInicial)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, SaldoFin)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, ChequesPagados)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, ChequesNoPagados)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaActCabecera, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function


    Public Function fActCierreMesLibro(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodigoCab As String, ByVal Cierre As Integer) As Integer
        Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspLibroBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Anio", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdLibroCab", DbType.String, 20, CodigoCab)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@SaldoInicial", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Saldo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ChePagados", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CheNoPagados", DbType.Decimal, 20, 0)

            cCapaDatos.sParametroSQL("@IdLibroDet", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TipoMov", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NroMovimientoAnx", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaAperturaAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimientoAnx", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@MontoAnx", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@CodMonedaAnx", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CodCtaCargoAnx", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ClienteAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@CtaClienteAnx", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@ObraAnx", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@TipoCtaAnx", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@TipoOperacion", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Marca", DbType.Int32, 1, Cierre)
            cCapaDatos.sParametroSQL("@Marca2", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@SaldoPrenda", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoGarantia", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoDepPlaz", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SaldoRetencion", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroCab", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@ItfIdLibroDet", DbType.String, 20, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function


End Class
