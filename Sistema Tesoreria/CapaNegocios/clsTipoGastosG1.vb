Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsTipoGastosG1

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = 0

    Public Function fListar(ByVal idGrupo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, idGrupo)
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarGrupoCC() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCategoria() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarGrupoXCategoria(ByVal IdCategoria As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdCategoria)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigo() As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "0"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = iCodigo 'Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal vOpcion As Integer, ByVal vCodRegistro As String, ByVal vDescripcion As String, ByVal vEstado As Integer, ByVal IdGrupo As Integer) As Integer

        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, vCodRegistro)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, vDescripcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "") '----------------->
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, IdGrupo)
            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fEliminar(ByVal vOpcion As Integer, ByVal vIdCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, vIdCodigo)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fBuscaTipoGastos(ByVal iOpcion As Integer, ByVal Des As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, Des)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            'cCapaDatos.sParametroSQL("@Colum", DbType.
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    '-----------------------------------------------------------------------------------------------------
    Public Function fListarAnexos(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCC(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fActualizarGasto(ByVal vOpcion As Integer, ByVal IdTipoGasto As Integer, ByVal EmprCodigo As String, ByVal CodGastoGrupo1 As String, ByVal Column As String) As Integer
        'Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastosG1"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@DescTGasto", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Grupo1", DbType.String, 3, CodGastoGrupo1)
            cCapaDatos.sParametroSQL("@Colum", DbType.String, 15, Column)
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.Int32, 2, 0)
            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    '

End Class
