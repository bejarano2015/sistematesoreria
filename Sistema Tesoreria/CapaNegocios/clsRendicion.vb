Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsRendicion

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

#Region " Retenciones Cabecera"

    Public Function fCargarRendicion(ByVal vIdEmpresa As String, ByVal vflgEstado As Integer, ByVal vAnio As String, ByVal vMes As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ID_RENDICION", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@ID_DOCPENDIENTE", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOC", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RETENCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_DETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RENDIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@SALDO_RENDICION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO_RENDICION", DbType.String, 1, vflgEstado)
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ID_EMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@IDOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@PDOCODIGOOC", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SALDO", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, vAnio)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, vMes)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdRencionPago As Decimal, ByVal vTotalRendido As Decimal, ByVal vSaldoRendicion As Decimal, ByVal vOpcion As Integer) As Integer  ', ByVal vNumero As String, ByVal vFecha As DateTime, ByVal vCodProveedor As String, ByVal vNombreProveedor As String, ByVal vRuc As String, ByVal vDireccion As String, ByVal vDescripcion As String, ByVal vTipoCambio As Decimal, ByVal vImporteRetenido As Decimal, ByVal vImporteTotal As Decimal, ByVal vFlgReservado As String, ByVal vFlgAnulado As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ID_RENDICION", DbType.Decimal, 18, vIdRencionPago)
            cCapaDatos.sParametroSQL("@ID_DOCPENDIENTE", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOC", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RETENCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_DETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RENDIDO", DbType.Decimal, 20, vTotalRendido)
            cCapaDatos.sParametroSQL("@SALDO_RENDICION", DbType.Decimal, 20, vSaldoRendicion)
            cCapaDatos.sParametroSQL("@ESTADO_RENDICION", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ID_EMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@PDOCODIGOOC", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 20, vIdRencionPago)
            cCapaDatos.sParametroSQL("@SALDO", DbType.Decimal, 20, vSaldoRendicion)
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'iResultado = cCapaDatos.fCmdExecuteScalar()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fEliminar(ByVal vIdRendicionDet As Decimal, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesDet"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ID_DOC_RENDIDO", DbType.Decimal, 18, vIdRendicionDet)
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@DOC_ORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@ID_DOC_ORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOCUMENTO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTE_RENDIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@SALDO_A_RENDIR", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FECHA_RENDICION", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fCargarDocumentos(ByVal vIdEmpresa As String)
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@ID_RENDICION", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@ID_DOCPENDIENTE", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOC", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RETENCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_DETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RENDIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@SALDO_RENDICION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO_RENDICION", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ID_EMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@IDOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@PDOCODIGOOC", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SALDO", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarDocumentos(ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ID_RENDICION", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@ID_DOCPENDIENTE", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOC", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RETENCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_DETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RENDIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@SALDO_RENDICION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO_RENDICION", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ID_EMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@PDOCODIGOOC", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SALDO", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

#End Region

#Region " Rendición Detalle "

    Public Function fCargarRendicionDetallePagos(ByVal vIdRendicionCab As Decimal) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@ID_RENDICION", DbType.Decimal, 18, vIdRendicionCab)
            cCapaDatos.sParametroSQL("@ID_DOCPENDIENTE", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOC", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RETENCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_DETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RENDIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@SALDO_RENDICION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO_RENDICION", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ID_EMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@PDOCODIGOOC", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SALDO", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarRendicionDetalleDocPorAsociar(ByVal vidDoc As String, ByVal vpdoCodigoOC As String, ByVal vIdEmpresa As String, ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ID_RENDICION", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@ID_DOCPENDIENTE", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOC", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RETENCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_DETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@TOTAL_RENDIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@SALDO_RENDICION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO_RENDICION", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@ID_EMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@IDOC", DbType.String, 10, vidDoc)
            cCapaDatos.sParametroSQL("@PDOCODIGOOC", DbType.String, 3, vpdoCodigoOC)
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@SALDO", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarDet(ByVal vIdRendicionDet As Decimal, ByVal vIdRendicionCab As Decimal, ByVal vDocOrigen As Decimal, ByVal vIdDocOrigen As Decimal, ByVal vTotalDocumento As Decimal, ByVal vImporteRendido As String, ByVal vSaldoRendir As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesDet"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ID_DOC_RENDIDO", DbType.Decimal, 18, vIdRendicionDet)
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 18, vIdRendicionCab)
            cCapaDatos.sParametroSQL("@DOC_ORIGEN", DbType.Decimal, 18, vDocOrigen)
            cCapaDatos.sParametroSQL("@ID_DOC_ORIGEN", DbType.Decimal, 18, vIdDocOrigen)
            cCapaDatos.sParametroSQL("@TOTAL_DOCUMENTO", DbType.Decimal, 20, vTotalDocumento)
            cCapaDatos.sParametroSQL("@IMPORTE_RENDIDO", DbType.Decimal, 20, vImporteRendido)
            cCapaDatos.sParametroSQL("@SALDO_A_RENDIR", DbType.Decimal, 20, vSaldoRendir)
            cCapaDatos.sParametroSQL("@FECHA_RENDICION", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fCargarRendicionDetalleDocAsociados(ByVal vidRendicion As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRendicionesDet"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ID_DOC_RENDIDO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@ID_RENDICION_PAGOS", DbType.Decimal, 18, vidRendicion)
            cCapaDatos.sParametroSQL("@DOC_ORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@ID_DOC_ORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@TOTAL_DOCUMENTO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTE_RENDIDO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@SALDO_A_RENDIR", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@FECHA_RENDICION", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

#End Region

End Class

