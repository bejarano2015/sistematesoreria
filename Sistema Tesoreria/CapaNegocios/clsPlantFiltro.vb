Public Class clsPlantFiltro

    Public sCadena As String
    Private sNexo As String
    Private bInicio As Boolean = False

    Public Sub sOrdenCadena(ByVal vColumna As String, ByVal vOrden As String)
        sCadena = sCadena + " ORDER BY " + vColumna + " " + vOrden
    End Sub

    Public Sub sSqlAndIgual(ByVal vColumna As String, ByVal vValor As String)

        If bInicio = False Then
            sNexo = " WHERE "
        Else
            sNexo = " AND "
        End If
        sCadena = sCadena + sNexo + vColumna + " = " + vValor
        bInicio = True
    End Sub

    Public Sub sSqlAndLike(ByVal vColumna As String, ByVal vValor As String)

        If bInicio = False Then
            sNexo = " WHERE "
        Else
            sNexo = " AND "
        End If

        sCadena = sCadena + sNexo + vColumna + " LIKE '%" + vValor + "%'"
        bInicio = True
    End Sub

    Public Sub sSqlAndBetween(ByVal vColumna As String, ByVal vValor01 As String, ByVal vValor02 As String, ByVal vTipo As System.Data.DbType)
        Select Case vTipo
            Case DbType.DateTime
                vValor01 = "#" + vValor01 + "#"
                vValor02 = "#" + vValor02 + "#"
        End Select

        If bInicio = False Then
            sNexo = " WHERE "
        Else
            sNexo = " AND "
        End If

        sCadena = sCadena + sNexo + vColumna + " BETWEEN " + vValor01 + " AND " + vValor02
        bInicio = True
    End Sub

    Public Sub sSqlOrIgual(ByVal vColumna As String, ByVal vValor As String)

        If bInicio = False Then
            sNexo = " WHERE "
        Else
            sNexo = " OR "
        End If
        sCadena = sCadena + sNexo + vColumna + " = " + vValor
        bInicio = True
    End Sub

    Public Sub sSqlOrLike(ByVal vColumna As String, ByVal vValor As String)

        If bInicio = False Then
            sNexo = " WHERE "
        Else
            sNexo = " OR "
        End If

        sCadena = sCadena + sNexo + vColumna + " LIKE '%" + vValor + "%'"
        bInicio = True
    End Sub

    Public Sub sSqlOrBetween(ByVal vColumna As String, ByVal vValor01 As String, ByVal vValor02 As String, ByVal vTipo As System.Data.DbType)
        Select Case vTipo
            Case DbType.DateTime
                vValor01 = "#" + vValor01 + "#"
                vValor02 = "#" + vValor02 + "#"
        End Select

        If bInicio = False Then
            sNexo = " WHERE "
        Else
            sNexo = " OR "
        End If

        sCadena = sCadena + sNexo + vColumna + " BETWEEN " + vValor01 + " AND " + vValor02
        bInicio = True
    End Sub

End Class
