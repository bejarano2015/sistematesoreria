Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsTipo

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal Estado01 As String, ByVal Estado02 As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            'Sentencia SQL Modificable
            sTextoSQL = "Select Estado = CASE EstCodigo WHEN 'A' THEN 'ACTIVO' WHEN 'D' THEN 'DESACTIVADO' ELSE 'N.A.' END,* from Tipo where EstCodigo in ('" & Estado01 & "','" & Estado02 & "')  order by Tipnroitem desc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fGrabar(ByVal VTIPCODIGO As String, ByVal VTIPDESCRIPCION As String, ByVal VTIPABREVIADO As String, ByVal VESTADO As String, ByVal vNuevo As Boolean) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            If vNuevo = True Then
                'Sentencia SQL Modificable
                cCapaDatos.sComandoSQL("INSERT INTO TIPO(TIPCODIGO,TIPDESCRIPCION,TIPABREVIADO) VALUES( @TIPCODIGO,@TIPDESCRIPCION,@TIPABREVIADO)", CommandType.Text)
            Else
                'Sentencia SQL Modificable
                cCapaDatos.sComandoSQL("UPDATE TIPO SET TIPDESCRIPCION=@TIPDESCRIPCION,TIPABREVIADO=@TIPABREVIADO,TIPUSUACTUALIZA= USER_NAME(),TIPPCACTUALIZA=HOST_NAME(),TIPFCHACTUALLIZA= GETDATE(),ESTCODIGO=@ESTCODIGO WHERE TIPCODIGO=@TIPCODIGO", CommandType.Text)
            End If

            'Sentencia SQL Modificable
            cCapaDatos.sParametroSQL("@TIPCODIGO", DbType.String, 3, VTIPCODIGO)
            cCapaDatos.sParametroSQL("@TIPDESCRIPCION", DbType.String, 50, VTIPDESCRIPCION)
            cCapaDatos.sParametroSQL("@TIPABREVIADO", DbType.String, 15, VTIPABREVIADO)
            cCapaDatos.sParametroSQL("@ESTCODIGO", DbType.String, 1, VESTADO)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vActivo As Boolean) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            If vActivo = True Then
                'Sentencia SQL Modificable
                sTextoSQL = "UPDATE TIPO SET ESTCODIGO='D' WHERE TIPCODIGO=@TIPCODIGO"
            Else
                'Sentencia SQL Modificable
                sTextoSQL = "DELETE FROM TIPO WHERE TIPCODIGO=@TIPCODIGO"
            End If

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            'Sentencia SQL Modificable
            cCapaDatos.sParametroSQL("@TIPCODIGO", DbType.String, 3, Codigo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fCodigo() As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "000"
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("select top 1 TipCodigo from Tipo order by Tipnroitem desc", CommandType.Text)

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            'Sentencia SQL Modificable
            sTextoSQL = "SELECT count(*) FROM TIPO WHERE TIPDESCRIPCION=@TIPDESCRIPCION AND TIPCODIGO<>@TIPCODIGO"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            'Sentencia SQL Modificable
            cCapaDatos.sParametroSQL("@TIPCODIGO", DbType.String, 3, Codigo)
            cCapaDatos.sParametroSQL("@TIPDESCRIPCION", DbType.String, 50, Descripcion)
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))

            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado

    End Function


End Class
