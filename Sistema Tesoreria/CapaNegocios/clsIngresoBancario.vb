Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsIngresoBancario

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

    Public Function fCargarIngresoBancario(ByVal vIdCaja As String, ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspIngresoBancario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@FechaDeposito", DbType.DateTime, 10, "01/01/1900")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCtaCorriente", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroOperacion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 500, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdCajaExtraordinaria As String, ByVal vEmprCodigo As String, ByVal vFechaDeposito As DateTime, ByVal vIdBanco As String, ByVal vIdCtaCorriente As String, ByVal vNroOperacion As String, ByVal vImporte As Decimal, ByVal vGlosa As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspIngresoBancario"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCajaExtraordinaria)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@FechaDeposito", DbType.DateTime, 10, vFechaDeposito)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vIdBanco)
            cCapaDatos.sParametroSQL("@IdCtaCorriente", DbType.String, 20, vIdCtaCorriente)
            cCapaDatos.sParametroSQL("@NroOperacion", DbType.String, 20, vNroOperacion)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Format(vImporte, "##,##0.00"))
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 500, vGlosa)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fCargarIngresoBancarioListado(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspIngresoBancario2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdIngresoBancario", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@FechaDeposito", DbType.DateTime, 10, "01/01/1900")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCtaCorriente", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroOperacion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 500, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarListado(ByVal vIdingresoBancario As Decimal, ByVal vEmprCodigo As String, ByVal vFechaDeposito As DateTime, ByVal vIdBanco As String, ByVal vIdCtaCorriente As String, ByVal vNroOperacion As String, ByVal vImporte As Decimal, ByVal vGlosa As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspIngresoBancario2"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdIngresoBancario", DbType.Decimal, 18, vIdingresoBancario)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@FechaDeposito", DbType.DateTime, 10, vFechaDeposito)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vIdBanco)
            cCapaDatos.sParametroSQL("@IdCtaCorriente", DbType.String, 20, vIdCtaCorriente)
            cCapaDatos.sParametroSQL("@NroOperacion", DbType.String, 20, vNroOperacion)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Format(vImporte, "##,##0.00"))
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 500, vGlosa)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

End Class

