Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsPagares

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodFuturoDetCro As String
    Public iNroRegistros As Integer = "0"

    Public Function fListarPagares(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagares"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdPagare", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@IdTipoPagare", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@FechaVcto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TEA", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Interes", DbType.Decimal, 20, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigo(ByVal CodEmpr As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspPagares"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdPagare", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdTipoPagare", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@FechaVcto", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TEA", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Interes", DbType.Decimal, 20, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function fGrabar(ByVal iOpcion As Integer, ByVal IdPagare As String, ByVal CodEmpresa As String, ByVal IdTipoPagare As Integer, ByVal Beneficiario As String, ByVal Concepto As String, ByVal Numero As String, ByVal Moneda As String, ByVal Importe As Double, ByVal Fecha As DateTime, ByVal TEA As Double, ByVal Interes As Double) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdPagare", DbType.String, 20, IdPagare)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdTipoPagare", DbType.Int32, 1, IdTipoPagare)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 200, Beneficiario)
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 50, Numero)
            cCapaDatos.sParametroSQL("@Concepto", DbType.String, 400, Concepto)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 2, Moneda)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@FechaVcto", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@TEA", DbType.Decimal, 20, TEA)
            cCapaDatos.sParametroSQL("@Interes", DbType.Decimal, 20, Interes)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

End Class
