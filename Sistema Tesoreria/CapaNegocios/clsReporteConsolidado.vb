Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsReporteConsolidado

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function ConsultarConsolidado(ByVal iOpcion As Integer, ByVal EmprCodigo As String, ByVal ubtCodigo As String, ByVal EspCodigo As String, ByVal TitCodigo As String, ByVal Empresa As String, ByVal Ruc As String, ByVal SisCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspImprimirConsolidado"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@ubtCodigo", DbType.String, 3, ubtCodigo)
            cCapaDatos.sParametroSQL("@EspCodigo", DbType.String, 5, EspCodigo)
            cCapaDatos.sParametroSQL("@TitCodigo", DbType.String, 5, TitCodigo)
            cCapaDatos.sParametroSQL("@Empresa", DbType.String, 250, Empresa)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@SisCodigo", DbType.String, 2, SisCodigo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
