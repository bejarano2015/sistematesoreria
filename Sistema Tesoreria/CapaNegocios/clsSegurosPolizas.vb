Imports CapaDatos

Public Class clsSegurosPolizas
    Private cCapaDatos As clsCapaDatos
    Private cTempo As New clsPlantTempo
    Dim DTable As DataTable
    Dim DSet As New DataSet
    Dim sbSQL As New System.Text.StringBuilder

    Public Function DescripcionEmpresa(ByVal CodigoEmpresa As String) As String
        Dim StrCodEmpresa As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT EmprDescripcion ")
                sbSQL.Append("FROM COMUN.EMPRESA ")
                sbSQL.Append("WHERE EmprCodigo='" & CodigoEmpresa & "'")

                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                StrCodEmpresa = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            StrCodEmpresa = String.Empty
        End Try
        Return StrCodEmpresa
    End Function
    Public Function RucEmpresa(ByVal CodigoEmpresa As String) As String
        Dim StrCodEmpresa As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT EmprRUC ")
                sbSQL.Append("FROM COMUN.EMPRESA ")
                sbSQL.Append("WHERE EmprCodigo='" & CodigoEmpresa & "'")

                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                StrCodEmpresa = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            StrCodEmpresa = String.Empty
        End Try
        Return StrCodEmpresa
    End Function
    Public Function DireccionEmpresa(ByVal CodigoEmpresa As String) As String
        Dim StrCodEmpresa As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT ISNULL(EmprDireccionFiscal,'') ")
                sbSQL.Append("FROM COMUN.EMPRESA ")
                sbSQL.Append("WHERE EmprCodigo='" & CodigoEmpresa & "'")

                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                StrCodEmpresa = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            StrCodEmpresa = String.Empty
        End Try
        Return StrCodEmpresa
    End Function
    Public Function CodigoMoneda(ByVal MonAbreviado As String) As String
        Dim StrCodCodigo As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT MonCodigo FROM Comun.Moneda WHERE MonAbreviado='" & MonAbreviado & "'")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                StrCodCodigo = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            StrCodCodigo = String.Empty
        End Try
        Return StrCodCodigo
    End Function
    Public Function ListarBancos(ByVal sCodigoEmpresa As String, ByVal sMoneda As String) As DataTable
        DTable = New DataTable
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT IdBanco as Codigo,NombreBanco as Descripcion,Abreviado ")
                sbSQL.Append("FROM Tesoreria.Banco ")
                sbSQL.Append("WHERE EmprCodigo='" & sCodigoEmpresa & "' AND CodMoneda='" & sMoneda & "'")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                DTable = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function
    Public Function ListarEstadoPoliza() As DataTable
        DTable = New DataTable
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT * FROM Tesoreria.EstadoPagoPoliza")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                DTable = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function CodigosCombos(ByVal Cadena As String) As String
        Dim IdCodigos As String
        If Cadena = "" Then
            IdCodigos = ""
        Else
            IdCodigos = Cadena.Substring(0, Cadena.IndexOf(" ")).Trim
        End If
        Return IdCodigos
    End Function

    Public Function ListarAseguradoras(ByVal sEmpresa As String) As DataTable
        DTable = New DataTable
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT AnaliticoCodigo as Codigo, AnaliticoDescripcion as Descripcion FROM Contabilidad.ct_Analitico WHERE EmprCodigo='" & sEmpresa & "' AND TipoAnaliticoCodigo='P' AND f_Poliza='S'")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                DTable = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fVerificaPagosPoliza(ByVal sEmprCodigo As String, ByVal sCodigoPoliza As String, ByVal sCodigoSubPoliza As String) As Integer
        Dim iRegistro As Integer = 0
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT COUNT(*) FROM Tesoreria.CronogramaPoliza WHERE EmprCodigo='" & sEmprCodigo & "' AND CodigoPoliza='" & sCodigoPoliza & "' AND CodigoSubPoliza='" & sCodigoSubPoliza & "' AND f_Cancelacion='C'")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                iRegistro = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try
        Return iRegistro
    End Function

    Public Function fVerificaPagosPoliza2(ByVal sEmprCodigo As String, ByVal sCodigoPoliza As String) As Integer
        Dim iRegistro As Integer = 0
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT COUNT(*) FROM Tesoreria.CronogramaPoliza WHERE EmprCodigo='" & sEmprCodigo & "' AND CodigoPoliza='" & sCodigoPoliza & "' AND CodigoSubPoliza='" & sCodigoPoliza & "' AND f_Cancelacion='C'")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                iRegistro = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try
        Return iRegistro
    End Function

    Public Function fVerificaDuplicadoPolizaEndoso(ByVal sEmpresa As String, ByVal sCodigoPoliza As String, ByVal sCodigoSubPoliza As String) As DataTable
        DTable = New DataTable
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT * FROM Tesoreria.SubPoliza WHERE EmprCodigo='" & sEmpresa & "' AND CodigoPoliza='" & sCodigoPoliza & "' AND CodigoSubPoliza='" & sCodigoSubPoliza & "'")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                DTable = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fPolizas_Vencidos_PorVencer(ByVal sEmpresa As String, ByVal sNumPoliza As String, ByVal sDias As String, ByVal sOpcion As String, ByVal sOpVencidos As String) As DataTable
        Dim DT_Polizas As New DataTable
        cCapaDatos = New clsCapaDatos
        'sOpcion(A)
        'sOpcion(B)

        'sOpVencidos(1)=Vencidos
        'sOpVencidos(2)=Por Vencer
        'sOpVencidos(3)=Ambos

        Try
            With cCapaDatos
                .sConectarSQL()
                sbSQL.Length = 0
                If sOpcion = "A" Then
                    sbSQL.Append("SELECT a.AnaliticoDescripcion as Aseguradora,r.Descripcion as Categoria, p.CodigoPoliza, r.Descripcion,p.Vigencia_FechaInicio as Inicio,p.Vigencia_FechaFin as Caduca,DATEDIFF(DAY,GETDATE(),p.Vigencia_FechaFin) as Dias,")
                    sbSQL.Append("CASE WHEN datediff(day,getdate(),p.Vigencia_FechaFin)<0 THEN 'VENCIDO' WHEN ABS(datediff(day,getdate(),p.Vigencia_FechaFin))<='" & sDias & "' THEN 'POR VENCER' WHEN ABS(datediff(day,getdate(),p.Vigencia_FechaFin))>'" & sDias & "' THEN 'POR VENCER' END as Estado ")
                    sbSQL.Append("FROM Tesoreria.Poliza p ")
                    sbSQL.Append("LEFT JOIN Tesoreria.Ramo r ON r.Codigo=p.CodigoRamo ")
                    sbSQL.Append("LEFT JOIN contabilidad.ct_Analitico a ON a.EmprCodigo=p.EmprCodigo AND a.RUC=p.CodigoAseguradora ")
                    sbSQL.Append("WHERE a.f_poliza='S' AND p.EmprCodigo='" & sEmpresa & "' AND DATEDIFF(DAY,GETDATE(),p.Vigencia_FechaFin)<='" & sDias & "'")
                    If (sOpVencidos <> "3" And sOpVencidos = "1") Then
                        sbSQL.Append("AND DATEDIFF(DAY,GETDATE(),p.Vigencia_FechaFin)<0")
                    ElseIf (sOpVencidos <> "3" And sOpVencidos = "2") Then
                        sbSQL.Append("AND DATEDIFF(DAY,GETDATE(),p.Vigencia_FechaFin)>0")
                    End If

                ElseIf sOpcion = "B" Then
                    sbSQL.Append("SELECT a.AnaliticoDescripcion as Aseguradora,r.Descripcion as Categoria, c.CodigoPoliza as Poliza, c.NroCuota2 as Cuota, c.FechaVencCuota as Vencimiento, p.MonCodigo as Mon, c.ImporteCuota as Importe, ")
                    sbSQL.Append("DATEDIFF(DAY,GETDATE(),c.FechaVencCuota) as Dias,")
                    sbSQL.Append("CASE WHEN datediff(day,getdate(),c.FechaVencCuota)<0 THEN 'VENCIDO' WHEN ABS(datediff(day,getdate(),c.FechaVencCuota))<='" & sDias & "' THEN 'POR VENCER' ")
                    sbSQL.Append("WHEN ABS(datediff(day,getdate(),c.FechaVencCuota))>'" & sDias & "' THEN 'POR VENCER'END AS Estado ")
                    sbSQL.Append("FROM Tesoreria.CronogramaPoliza c ")
                    sbSQL.Append("INNER JOIN Tesoreria.Poliza p ON p.EmprCodigo=c.EmprCodigo AND p.CodigoPoliza=c.CodigoPoliza AND c.CodigoSubPoliza=p.CodigoPoliza ")
                    sbSQL.Append("LEFT JOIN contabilidad.ct_Analitico a ON a.EmprCodigo=c.EmprCodigo AND a.RUC=p.CodigoAseguradora ")
                    sbSQL.Append("LEFT JOIN Tesoreria.Ramo r ON r.Codigo=p.CodigoRamo ")
                    sbSQL.Append("WHERE a.f_poliza='S' AND c.EmprCodigo='" & sEmpresa & "' AND DATEDIFF(DAY,GETDATE(),c.FechaVencCuota)<='" & sDias & "' AND c.f_Cancelacion!='C' ")
                    If sNumPoliza <> "" Then
                        sbSQL.Append("AND c.CodigoPoliza='" & sNumPoliza & "'")
                    End If
                    If (sOpVencidos <> "3" And sOpVencidos = "1") Then
                        sbSQL.Append("AND DATEDIFF(DAY,GETDATE(),c.FechaVencCuota)<0")
                    ElseIf (sOpVencidos <> "3" And sOpVencidos = "2") Then
                        sbSQL.Append("AND DATEDIFF(DAY,GETDATE(),c.FechaVencCuota)>0")
                    End If
                End If

                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                DT_Polizas = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            DT_Polizas = Nothing
        End Try
        Return DT_Polizas

    End Function


    Public Function fListarCentrosSubPoliza(ByVal iOpcion As Integer, ByVal sCodEmpresa As String, ByVal CodigoPoliza As String, ByVal NroDias As Integer, ByVal sOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, CodigoPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, NroDias)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, sOpcion)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function


    Public Function fListarCentrosCostos(ByVal sCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 9)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fCargaDatos() As DataSet
        Dim Dset As New DataSet
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            Dset = cCapaDatos.fListarSet
            'TABLA(0) = Ramo
            'TABLA(1) = Moneda
            'TABLA(2) = Forma de Pago
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return Dset

    End Function
    Public Function fListarPolizas(ByVal sCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            'cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 20)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fGrabarPolizas(ByVal sCodEmpresa As String, ByVal sNroPoliza As String, ByVal sCodRamo As String, ByVal dFechaVigIni As Date, _
                                ByVal dFechaVigFin As Date, ByVal sRUC As String, ByVal sDireccion As String, ByVal sMoneda As String, _
                                ByVal dPrima As Decimal, ByVal dDerechoEmision As Decimal, ByVal dInteres As Decimal, ByVal dIGV As Decimal, _
                                ByVal dTotal As Decimal, ByVal sCodAseguradora As String, ByVal dSumaAsegura As Decimal, ByVal sFormaPago As String, _
                                ByVal iNUmLetra As Integer, ByVal dFechaLetraIni As Date, ByVal BolEditar As Boolean, ByVal sUsuario As String) As Boolean
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            If BolEditar = True Then
                cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 4)
            Else
                cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 2)
            End If
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, sCodRamo)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, dFechaVigIni)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, dFechaVigFin)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, sCodEmpresa)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, sRUC)
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, sDireccion)
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, sMoneda)
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, dPrima)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, dDerechoEmision)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, dInteres)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, dIGV)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, dTotal)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, sCodAseguradora)
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, dSumaAsegura)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, sFormaPago)
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, iNUmLetra)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, dFechaLetraIni)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, sUsuario)

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return False
        End Try
        Return True
    End Function


    Public Function fGrabarEndosos(ByVal sCodEmpresa As String, ByVal sNroSubPoliza As String, ByVal sNroPoliza As String, ByVal sCodRamo As String, ByVal dFechaVigIni As Date, _
                                ByVal dFechaVigFin As Date, ByVal sRUC As String, ByVal sDireccion As String, ByVal sMoneda As String, _
                                ByVal dPrima As Decimal, ByVal dDerechoEmision As Decimal, ByVal dInteres As Decimal, ByVal dIGV As Decimal, _
                                ByVal dTotal As Decimal, ByVal sCodAseguradora As String, ByVal dSumaAsegura As Decimal, ByVal sFormaPago As String, _
                                ByVal iNUmLetra As Integer, ByVal dFechaLetraIni As Date, ByVal BolEditar As Boolean, ByVal sUsuario As String) As Boolean
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            If BolEditar = True Then
                cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 30)
            Else
                cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 29)
            End If
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, sCodRamo)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, dFechaVigIni)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, dFechaVigFin)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, sCodEmpresa)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, sRUC)
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, sDireccion)
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, sMoneda)
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, dPrima)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, dDerechoEmision)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, dInteres)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, dIGV)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, dTotal)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, sCodAseguradora)
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, dSumaAsegura)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, sFormaPago)
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, iNUmLetra)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, dFechaLetraIni)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, sUsuario)

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, sNroSubPoliza)
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return False
        End Try
        Return True
    End Function

    Public Function fMostrarPoliza(ByVal sCodEmpresa As String, ByVal sNroPoliza As String) As DataSet
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            'cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DSet = cCapaDatos.fListarSet
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DSet
    End Function


    Public Function fEliminaPolizayDetallePoliza(ByVal iOpcion As Integer, ByVal sCodEmpresa As String, ByVal sNroPoliza As String, ByVal IdProveedor As String, ByVal EmprCodigo As String, ByVal MontoSaldo As Double, ByVal IdMoneda As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos = New clsCapaDatos
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Sub fEliminaDetallePoliza(ByVal sCodEmpresa As String, ByVal sNroPoliza As String)
        cCapaDatos = New clsCapaDatos
        cCapaDatos.sConectarSQL()
        cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
        cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 5)
        cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
        cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
        cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
        cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
        cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
        cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
        cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
        cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
        cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
        cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
        cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
        cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
        cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
        cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
        cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
        cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
        cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
        cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
        cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
        cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
        cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

        cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
        cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
        cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
        cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
        cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
        cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
        cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
        cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
        cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
        cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
        cCapaDatos.fCmdExecuteNonQuery()
    End Sub
    Public Function fGrabarDetallePoliza(ByVal sCodEmpresa As String, ByVal sNroPoliza As String, ByVal sCodDetaPoliza As String, ByVal sEndoso As String, _
                    ByVal sIdBanco As String, ByVal sFechaLeasing As String, ByVal sDescAsegurado As String, ByVal sCCosto As String, ByVal sTasa As String, ByVal sFormaPago As String, _
                    ByVal iNroLetras As Integer, ByVal dImporte As Decimal, ByVal sPorcDeducible As String, ByVal sMinimoDeducible As String, ByVal CodigoTipoPoliza As String, ByVal CodigoSubPoliza As String, ByVal dPrimaNeta As Decimal, ByVal dPrimaGastos As Decimal) As Boolean
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, CodigosCombos(sCCosto))             'CENTRO COSTO
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, CodigosCombos(CodigoTipoPoliza))
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, dPrimaNeta)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, dPrimaGastos)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, CodigosCombos(sFormaPago))
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, Trim(CodigoSubPoliza))

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, sCodDetaPoliza)
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, CodigosCombos(sEndoso))
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, CodigosCombos(sIdBanco))
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, IIf(sFechaLeasing Is Nothing, System.DBNull.Value, sFechaLeasing))
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, sDescAsegurado)
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, sTasa)
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, iNroLetras)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, dImporte)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, sPorcDeducible)
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, sMinimoDeducible)
            cCapaDatos.fCmdExecuteNonQuery()
            Return True
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return False
        End Try
    End Function
    Public Sub fGenerarCronogramaPagoPoliza(ByVal sCodEmpresa As String, ByVal sNroPoliza As String, ByVal dTotal As Decimal, _
                                ByVal dSumaAsegura As Decimal, ByVal iNUmLetra As Integer, ByVal dFechaLetraIni As Date, ByVal sUsuario As String)
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, sCodEmpresa)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, dTotal)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, dSumaAsegura)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, iNUmLetra)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, dFechaLetraIni)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, sUsuario)

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try
    End Sub

    Public Sub fGenerarCronogramaPagoPoliza2(ByVal sCodEmpresa As String, ByVal sNroSubPoliza As String, ByVal sNroPoliza As String, ByVal dTotal As Decimal, _
                                ByVal dSumaAsegura As Decimal, ByVal iNUmLetra As Integer, ByVal dFechaLetraIni As Date, ByVal sUsuario As String)
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 31)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, sCodEmpresa)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, dTotal)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, dSumaAsegura)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, iNUmLetra)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, dFechaLetraIni)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, sUsuario)

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, sNroSubPoliza)
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try
    End Sub

    Public Function fMostrarCronograma(ByVal sCodEmpresa As String, ByVal sNroPoliza As String) As DataSet
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            'cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 21)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DSet = cCapaDatos.fListarSet
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DSet
    End Function


    'Public Function fMostrarCronograma2(ByVal sCodEmpresa As String, ByVal sNroPoliza As String) As DataSet
    '    cCapaDatos = New clsCapaDatos
    '    DTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
    '        'cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 8)
    '        cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 35)
    '        cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
    '        cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
    '        cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
    '        cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
    '        cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
    '        cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
    '        cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
    '        cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
    '        cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
    '        cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
    '        cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
    '        cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
    '        cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
    '        cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
    '        cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
    '        cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
    '        cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
    '        cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

    '        cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
    '        cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
    '        cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
    '        cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
    '        cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
    '        cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
    '        cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        DSet = cCapaDatos.fListarSet
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '        Return Nothing
    '    End Try
    '    Return DSet
    'End Function

    Public Function fMostrarCronograma3(ByVal sEmpresa As String, ByVal sCodigoPoliza As String, ByVal sCodigoSubPoliza As String) As DataTable
        DTable = New DataTable
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("SELECT CodigoSubPoliza as Codigo FROM Tesoreria.SubPoliza WHERE EmprCodigo='" & sEmpresa & "' AND CodigoPoliza='" & sCodigoPoliza & "'")
                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .sAdaptadorSQL("R")
                DTable = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fMostrarCronograma2(ByVal iOpcion As Integer, ByVal sCodEmpresa As String, ByVal sNroPoliza As String, ByVal sNroPolizaOriginal As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            'cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sNroPoliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, sNroPolizaOriginal)
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function


    


    Public Sub fActualizarEstado(ByVal sEmpresa As String, ByVal sCodigoSubPoliza As String, ByVal sCodigoPoliza As String, ByVal iCuota As Integer, ByVal sEstado As String, ByVal sUsuario As String)
        Dim StrCodCodigo As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("UPDATE Tesoreria.CronogramaPoliza ")
                sbSQL.Append("SET f_Cancelacion='" & sEstado & "',Fecha_Cancelacion=GETDATE(),UsuCreacion='" & sUsuario & "',FechaCreacion=GETDATE(),PcCreacion=HOST_NAME()")
                sbSQL.Append("WHERE EmprCodigo='" & sEmpresa & "' AND CodigoSubPoliza='" & sCodigoSubPoliza & "' AND CodigoPoliza='" & sCodigoPoliza & "' AND NroCuota2='" & iCuota & "'")

                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .fCmdExecuteNonQuery()
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try
    End Sub

    Public Sub fUpdateEstadoEgresos(ByVal sEmpresa As String, ByVal sCodigoPoliza As String, ByVal iCuota As Integer, ByVal sEstado As String, ByVal sUsuario As String, ByVal sID_EgresoCaja As String, ByVal sEndoso As String)
        Dim StrCodCodigo As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("UPDATE Tesoreria.CronogramaPoliza ")
                sbSQL.Append("SET f_Cancelacion='" & sEstado & "',Fecha_Cancelacion=GETDATE(),UsuCreacion='" & sUsuario & "',FechaCreacion=GETDATE(),PcCreacion=HOST_NAME(), ID_EgresoCaja='" & sID_EgresoCaja & "'")
                sbSQL.Append("WHERE EmprCodigo='" & sEmpresa & "' AND CodigoPoliza='" & sCodigoPoliza & "' AND NroCuota='" & iCuota & "' AND CodigoSubPoliza='" & sEndoso & "'")

                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .fCmdExecuteNonQuery()
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try
    End Sub
    Public Sub fUpdateEstadoEgresos_Delete(ByVal sEmpresa As String, ByVal sEstado As String, ByVal sID_EgresoCaja As String)
        Dim StrCodCodigo As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                sbSQL.Length = 0
                sbSQL.Append("UPDATE Tesoreria.CronogramaPoliza ")
                sbSQL.Append("SET f_Cancelacion='" & sEstado & "',ID_EgresoCaja='' ")
                sbSQL.Append("WHERE EmprCodigo='" & sEmpresa & "' AND ID_EgresoCaja='" & sID_EgresoCaja & "'")

                .sComandoSQL(sbSQL.ToString, CommandType.Text)
                .fCmdExecuteNonQuery()
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        End Try
    End Sub
    Public Function fCuotasPolizasProgramadas(ByVal sCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, sCodEmpresa)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fREP_PolizasCuotasXPagar(ByVal sCodEmpresa As String, ByVal dFechaIni As Date, ByVal dFechaFin As Date) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 11)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Format(dFechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Format(dFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function


    Public Function fREP_PolizasCuotasXPagar2(ByVal iOpcion As Integer, ByVal sCodEmpresa As String, ByVal dFechaIni As Date, ByVal dFechaFin As Date) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Format(dFechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Format(dFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fREP_Polizas_Detalle(ByVal iOpcion As Integer, ByVal sCodEmpresa As String, ByVal sCodCia As String, ByVal dFechaIni As Date, ByVal dFechaFin As Date) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Format(dFechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Format(dFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, sCodCia)
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fREP_Cuotas_Canceladas(ByVal iOpcion As Integer, ByVal sCodEmpresa As String, ByVal sCodCia As String, ByVal dFechaIni As Date, ByVal dFechaFin As Date) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Format(dFechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Format(dFechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, sCodCia)
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fREP_Cuotas_Vencidas_PorVencer(ByVal iOpcion As Integer, ByVal sEmpresa As String, ByVal sCodCia As String, ByVal sDias As String, ByVal sOpVencidos As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, sCodCia)
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, sDias)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, sOpVencidos)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fREP_Cuotas_Vencidas_PorVencer2(ByVal iOpcion As Integer, ByVal sEmpresa As String, ByVal sCodCia As String, ByVal sCodPOliza As String, ByVal sDias As String, ByVal sOpVencidos As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, sCodPOliza)
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, sCodCia)
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, sDias)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, sOpVencidos)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fCargar_Obras(ByVal sCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 15)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function


    Public Function fCargar_Obras2(ByVal iOpcion As Integer, ByVal sCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function


    Public Function fCargar_AseguradosXObra(ByVal sCodEmpresa As String, ByVal sCodObra As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 16)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, sCodObra)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

    Public Function fCargar_AseguradosXObra2(ByVal iOpcion As Integer, ByVal sCodEmpresa As String, ByVal sCodObra As String, ByVal Cadena As String) As DataTable
        cCapaDatos = New clsCapaDatos
        DTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Tesoreria.Seguros_y_Polizas", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, sCodEmpresa)
            cCapaDatos.sParametroSQL("@CODIGOPOLIZA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHAREGISTRO", DbType.Date, 15, Format(Today, "dd/MM/yyyy"))
            cCapaDatos.sParametroSQL("@CODIGORAMO", DbType.String, 10, sCodObra)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAINI", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@VIGENCIA_FECHAFIN", DbType.DateTime, 10, Today)
            cCapaDatos.sParametroSQL("@CODIGOCLIENTE_EMPRESA", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 100, Cadena)
            cCapaDatos.sParametroSQL("@MONCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PRIMA", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@DERECHOEMISION", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@INTERESES", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@CODIGOASEGURADORA", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@SUMAASEGURADORA", DbType.Decimal, 22, 0)     '*** INTERES DE FINANCIACION
            cCapaDatos.sParametroSQL("@CODCONDCANCELACION", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NUMLETRA", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@FECHAINILETRA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@CODIGODETAPOLIZA", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CODIGOENDOSO", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IDBANCO", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@FECHALEASING", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DESCRIPCIONASEGURADO", DbType.String, 3000, "")
            cCapaDatos.sParametroSQL("@TASA", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@NROLETRA", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MINIMO_DEDUCIBLE", DbType.String, 20, "")
            cCapaDatos.sAdaptadorSQL("R")
            DTable = cCapaDatos.fListarTabla
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            Return Nothing
        End Try
        Return DTable
    End Function

End Class
