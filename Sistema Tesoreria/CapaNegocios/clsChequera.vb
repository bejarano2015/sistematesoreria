Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsChequera

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private dtTable2 As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodFuturo2 As String
    Public sNumSiguienteChequera As String
    Public sUltimoIdCheque As String
    Public sNombrebanco As String
    Public sNumeroCuenta As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer, ByVal vEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdChequera As String, ByVal vEmprCodigo As String, ByVal vIdBanco As String, ByVal vIdCuenta As String, ByVal vIdMoneda As String, ByVal vNroChequera As String, ByVal vFechaChequera As DateTime, ByVal vNroInicial As String, ByVal vNroFinal As String, ByVal vCondicion As String, ByVal vEstado As Integer, ByVal vOpcion As Integer, ByVal Usuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vIdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vIdCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, vIdMoneda)
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, vNroChequera)
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, vFechaChequera)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, vNroInicial)
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, vNroFinal)
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, vCondicion)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, Usuario)
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), ""))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, vIdMoneda)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEliminar(ByVal CodEmpresa As String, ByVal Codigo As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCodigo(ByVal CodEmpr As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, Descripcion)
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))

            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado

    End Function

    Public Function fListarBancos(ByVal Codigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Codigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCuenta(ByVal Codigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, Codigo)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarEmpresa() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarBancosxEmpresa(ByVal vEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTiposCuenta() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 30)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCuentasxBancoyEmpresa(ByVal vEmpresa As String, ByVal vBanco As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 17)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCuentasEncargada(ByVal vEmpresa As String, ByVal vBanco As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 31)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function
    'fListarChequerasxBanyCuen

    Public Function fListarChequerasxBanyCuen(ByVal vEmpresa As String, ByVal vBanco As String, ByVal vCuenta As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 21)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarDetalleCheque(ByVal vIdCheque As String, ByVal CodEmpresa As String, ByVal vIdMonedaCheque As String, ByVal vIdChequera As String, ByVal vNumeroCheque As String, ByVal vNroChequera As String, ByVal vFechaCheque As DateTime, ByVal vEstado As Integer, ByVal gUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, gUsuario)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, vNroChequera)
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, vIdCheque)
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, vNumeroCheque)
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), vFechaCheque)) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, vIdMonedaCheque)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fListarMoneda(ByVal vIdCuenta As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vIdCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fActualizaCheque(ByVal VIdCheque As String, ByVal VIdMonedaCheque As String, ByVal VNumeroCheque As String, ByVal VFechaCheque As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, VNumeroCheque)
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), VFechaCheque)) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, VIdMonedaCheque)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fCodigoCheque(ByVal VNroFinal As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos

        Try
            'cCapaDatos.sConectarSQL()
            'sTextoSQL = "Tesoreria.uspChequera"
            'cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            'cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 14)
            'cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            'cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            'cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            'cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 3, "")
            'cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            'cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            'cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, VNroFinal)
            'cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            'cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            'cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            'cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")

            'iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If VNroFinal = "" Then
                iCodigo = 1
            Else
                iCodigo = VNroFinal + 1
            End If
            sCodFuturo2 = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            'cCapaDatos.sDesconectarSQL()
        End Try

        Return sCodFuturo2
    End Function

    Public Function fListarChequeras(ByVal vEmpresa As String, ByVal vIdCuenta As String, ByVal vIdBanco As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, "")
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vIdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vIdCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'Public Function fCodigoNroFinal(ByVal VEmprCodigo As String, ByVal VIdBanco As String, ByVal VIdCuenta As String) As String
    '    cCapaDatos = New clsCapaDatos
    '    Dim codigo As String
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspChequera"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 15)
    '        cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, VEmprCodigo)
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, VIdBanco)
    '        cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, VIdCuenta)
    '        cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
    '        cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
    '        cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
    '        cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
    '        codigo = cCapaDatos.fCmdExecuteScalar
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try

    '    Return codigo
    'End Function

    Public Function fSiguienteNumChequera(ByVal vEmprCodigo As String, ByVal vBanco As String, ByVal vCuenta As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            'iCodigo = cCapaDatos.fCmdExecuteScalar()
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sNumSiguienteChequera = iCodigo 'Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function



    Public Function fUltimoNumeroCheque(ByVal vEmprCodigo As String, ByVal vBanco As String, ByVal vCuenta As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 29)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarUltimoIdCheque(ByVal Empresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Empresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sUltimoIdCheque = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fActualizarCabecera(ByVal vIdChequera As String, ByVal vCondicion As String, ByVal vEstado As Integer, ByVal CodEmpr As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 24)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, vCondicion)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActualizarCabecera2(ByVal vIdChequera As String, ByVal vCondicion As String, ByVal vEstado As Integer, ByVal CodEmpr As String, ByVal IdBanco As String, ByVal IdCuenta As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, vCondicion)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActualizarDetalle(ByVal vIdChequera As String, ByVal CodEmpresa As String, ByVal vAnulado As Int32, ByVal vEstado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 25)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, vAnulado)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fBuscarNombreBanco(ByVal vCodBanco As String) As String
        Dim iCodigo As String = ""
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vCodBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            sNombrebanco = iCodigo
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fBuscarExisteNum(ByVal vIdChequera As String, ByVal vEmprCodigo As String, ByVal vIdBanco As String, ByVal vIdCuenta As String, ByVal vIdMoneda As String, ByVal vNroChequera As String, ByVal vFechaChequera As DateTime, ByVal vNroInicial As String, ByVal vNroFinal As String, ByVal vCondicion As String, ByVal vEstado As Integer, ByVal vOpcion As Integer, ByVal Usuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, vIdChequera)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vIdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vIdCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, vIdMoneda)
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, vNroChequera)
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, vFechaChequera)
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, vNroInicial)
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, vNroFinal)
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, vCondicion)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, Usuario)
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), ""))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, vIdMoneda)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fBuscarNumeroCuenta(ByVal vCodCuenta As String) As String
        Dim iCodigo As String = ""
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 27)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vCodCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaChequera", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroInicial", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NroFinal", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaCheque", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMonedaCheque", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            sNumeroCuenta = iCodigo
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fListarConsultaChequera(ByVal iOpcion As String, ByVal CodEmpresa As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal IdMoneda As String, ByVal Estado As Integer, ByVal Condicion As String, ByVal FechaInicio As DateTime, ByVal FechaFinal As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspConsultaChequera"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@Condicion", DbType.String, 30, Condicion)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaInicio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFinal, "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
