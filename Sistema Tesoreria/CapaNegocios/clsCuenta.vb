Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsCuenta

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer, ByVal CodEmpr As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarBancos(ByVal CodEmpr As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fGrabar(ByVal vIdCuenta As String, ByVal vIdBanco As String, ByVal vNumeroCuenta As String, ByVal vMonCodigo As String, ByVal vObservacion As String, ByVal vEmprCodigo As String, ByVal vEstado As Integer, ByVal vOpcion As Integer, ByVal vTipoCta As String, ByVal vCtaContable As String, ByVal vEstadoLibro As Integer, ByVal vTipoCuenta As String, ByVal EstadoCargo As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, vIdCuenta)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vIdBanco)
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, vNumeroCuenta)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, vMonCodigo)
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, vObservacion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, vEstadoLibro)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, vTipoCuenta)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, vTipoCta)
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, vCtaContable)
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, EstadoCargo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer, ByVal EmprCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCodigo(ByVal CodEmpr As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, Descripcion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))

            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado

    End Function

    Public Function fListarBanco(ByVal vEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTipCuentaBanco() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarCtaContable(ByVal vPeriodo As String, ByVal vEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmpresa)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, vPeriodo)
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarTipoCtaContable() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCuentaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoLibro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@TipoCta", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TipoCtaBank", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@cuentacodigo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EstadoCargo", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable

    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspCuentaBanco"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 10)
    '        cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, vEstado01)
    '        cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, vEstado02)
    '        cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try

    '    Return dtTable
    'End Function

    Public Function fBuscarCuentasBanco(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal IdBanco As String, ByVal Numero As String, ByVal Moneda As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBuscarCuentasBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 50, Numero)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, Moneda)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
