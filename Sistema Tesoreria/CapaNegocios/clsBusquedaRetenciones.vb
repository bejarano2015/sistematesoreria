Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsBusquedaRetenciones

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fConsultaRetenciones(ByVal iOpcion As Integer, ByVal Texto As String, ByVal CodEmpresa As String, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime, ByVal Estado As Integer, ByVal Serie As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBusquedaRetenciones"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@Texto", DbType.String, 100, Texto)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@FechaInicio", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFinal", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, Serie)
            'cCapaDatos.sParametroSQL("@Serie", dbtype.S7tring,5,serie)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            'iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
            iNroRegistros = dtTable.Rows.Count
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fConsultaMontoLetras(ByVal Monto As Double) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.sp_Num2Let"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Numero", DbType.Decimal, 20, Monto)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            'iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
            iNroRegistros = dtTable.Rows.Count
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
