Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsSaldoProveedores

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSaldoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdSaldo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaRegistro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@MontoFacturado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoCancelado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoSaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fGrabar(ByVal VIdSaldo As String, ByVal VPrvCodigo As String, ByVal VEmprCodigo As String, ByVal VFechaRegistro As DateTime, ByVal VMontoFacturado As Decimal, ByVal VMontoCancelado As Decimal, ByVal VMontoSaldo As Decimal, ByVal VESTADO As Integer, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSaldoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, VESTADO)
            cCapaDatos.sParametroSQL("@IdSaldo", DbType.String, 20, VIdSaldo)
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, VPrvCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, VEmprCodigo)
            cCapaDatos.sParametroSQL("@FechaRegistro", DbType.DateTime, 10, Format(Today(), VFechaRegistro)) 'Fecha)
            cCapaDatos.sParametroSQL("@MontoFacturado", DbType.Decimal, 20, VMontoFacturado)
            cCapaDatos.sParametroSQL("@MontoCancelado", DbType.Decimal, 20, VMontoCancelado)
            cCapaDatos.sParametroSQL("@MontoSaldo", DbType.Decimal, 20, VMontoSaldo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSaldoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdSaldo", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaRegistro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@MontoFacturado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoCancelado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoSaldo", DbType.Decimal, 20, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fCodigo() As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "000"
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSaldoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdSaldo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaRegistro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@MontoFacturado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoCancelado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoSaldo", DbType.Decimal, 20, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo
    End Function

    'Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer

    '    Dim iResultado As Int32 = 0
    '    cCapaDatos = New clsCapaDatos

    '    Try
    '        cCapaDatos.sConectarSQL()
    'sTextoSQL = "Tesoreria.uspSaldoProveedores"
    'cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    'cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
    'cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
    'cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
    'cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
    'cCapaDatos.sParametroSQL("@IdSaldo", DbType.String, 20, Codigo)
    'cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
    'cCapaDatos.sParametroSQL("@FechaRegistro", DbType.DateTime, 10, "")
    'cCapaDatos.sParametroSQL("@MontoFacturado", DbType.Decimal, 20, 0)
    'cCapaDatos.sParametroSQL("@MontoCancelado", DbType.Decimal, 20, 0)
    'cCapaDatos.sParametroSQL("@MontoSaldo", DbType.Decimal, 20, 0)

    '        iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))

    '        If iResultado > 0 Then
    '            cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
    '        End If

    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try

    '    Return iResultado

    'End Function

    Public Function fListarEmpresa() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSaldoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdSaldo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaRegistro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@MontoFacturado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoCancelado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoSaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarProveedor() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspSaldoProveedores"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdSaldo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@FechaRegistro", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@MontoFacturado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoCancelado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoSaldo", DbType.Decimal, 20, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

End Class
