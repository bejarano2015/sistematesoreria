Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos
Imports System.Text

Public Class clsRegistroDocumento

    Dim sbCadena As New StringBuilder
    Dim DTlector As SqlDataReader
    Dim Row As DataRow      'Para campos de Auditoria

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodFuturoLinea As String
    Public sCodFuturoSaldoDoc As String

    Public iNroRegistros As Integer = "0"

    Public Function fCodigo(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fCodigoLinea(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 38)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoLinea = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
            'Linea
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fCodigoSaldoDo(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 30)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoSaldoDoc = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fListar(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal sUsuario As String, ByVal sPeriodo As String, ByVal Texto As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, sUsuario)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, Texto)
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, sPeriodo)
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarDocsPorRango(ByVal iOpcion As Integer, ByVal FechaRecepcion1 As DateTime, ByVal FechaRecepcion2 As DateTime, ByVal EmprCodigo As String, ByVal NombreEmpresa As String, ByVal RucEmpresa As String, ByVal RucProv As String, ByVal sUsuario As String, ByVal sPeriodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDocIngresadosPorRango"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@FechaRecepcion1", DbType.DateTime, 10, Format(FechaRecepcion1, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion2", DbType.DateTime, 10, Format(FechaRecepcion2, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@NombreEmpr", DbType.String, 150, NombreEmpresa)
            cCapaDatos.sParametroSQL("@RucEmpresa", DbType.String, 11, RucEmpresa)
            cCapaDatos.sParametroSQL("@RucProv", DbType.String, 11, RucProv)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, sUsuario)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, sPeriodo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fBuscarDocumento(ByVal IdDocumento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, IdDocumento)
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdDocumento)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarBusqueda(ByVal CodEmpresa As String, ByVal FechaInicio As DateTime, ByVal FechaFinal As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaInicio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(FechaFinal, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'Public Function fListarDocRef() As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspRegistroDocumento"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Codigo", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Nombre", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@FechaDoc", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaVen", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@MontoIgv", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoTasa", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, 0)
    '        cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")

    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
    '        cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
    '        cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")

    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

    Public Function fListarDocOrigen() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTipDoc() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTipDocIdentidad() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 17)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTipMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 14)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarTipMoneda2() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDepartamentos(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarAcreedor(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarAcreedor2(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 27)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarParametroIgv(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 21)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarLinea(ByVal iOpcion As Integer, ByVal CodLinea As String, ByVal CodProvLog As String, ByVal CodEmpresa As String, ByVal Dias As Integer, ByVal SaldoSolesLima As Decimal, ByVal SaldoDolaresLima As Decimal, ByVal AnaliticoCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodProvLog)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Trim(AnaliticoCodigo))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, CodLinea)
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            'IdOC
            'NroOC

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion)) 
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fGrabar_recepcion_conta(ByVal CodEmpresa As String, ByVal vl_idregistrto As String, ByVal vl_periodo As String, ByVal vl_rece_conta As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 59)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Trim(""))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, vl_idregistrto)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, vl_rece_conta)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, vl_periodo)
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            'IdOC
            'NroOC

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion)) 
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function
    Public Function fGrabar(ByVal iOpcion As Integer, ByVal IdAcreedor As String, ByVal CodAcreedor As String, ByVal NombreAcreedor As String, ByVal IdDocumento As String, ByVal NroSerie As String, ByVal NroDocumento As String, ByVal FechaBase As DateTime, ByVal Dias As Int32, ByVal FechaDocumento As DateTime, ByVal FechaVencimiento As DateTime, ByVal IdMoneda As String, ByVal ImporteIgv As Double, ByVal TasaIGV As Double, ByVal ImporteTotal As Double, ByVal ValorDeclarado As Decimal, ByVal ValorCalculado As Double, ByVal Inafecto As Double, ByVal IdArea As String, ByVal Descripcion As String, ByVal Situacion As String, ByVal FechaCreacion As DateTime, ByVal FechaRecepcion As DateTime, ByVal Usuario As String, ByVal IdRegistro As String, ByVal EmprCodigo As String, ByVal EstadoPrePro As Integer, ByVal EstadoPro As Integer, ByVal ValorCompra As Decimal, ByVal IGV As Integer, ByVal DP As String, ByVal PorcDP As Double, ByVal IdOC As String, ByVal NroOC As String, ByVal Archivado As Integer, ByVal NCCodFT As String, ByVal NCCodFTEmpr As String, ByVal NCGlosa As String, ByVal NCImporte As Double, ByVal sPeriodo As String, ByVal TIngreso As String, ByVal VL_PdoCodigoOC As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumentoCONPDOCODIGO"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, Trim(IdAcreedor))
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Trim(CodAcreedor))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, Trim(NombreAcreedor))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, Trim(IdDocumento))
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, Trim(NroSerie))
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(NroDocumento))
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(FechaBase, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, Dias)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaDocumento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(FechaVencimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, Trim(IdMoneda))
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, ImporteIgv)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, TasaIGV)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, ImporteTotal)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, ValorDeclarado)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, ValorCalculado)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, Inafecto)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, Trim(IdArea))
            'cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, Trim(Situacion))
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(FechaCreacion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(FechaRecepcion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, Trim(Usuario))
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Trim(IdRegistro))
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Trim(EmprCodigo))
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, EstadoPrePro)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, EstadoPro)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, ValorCompra)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, IGV)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, DP)
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, PorcDP)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, IdOC)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, NroOC)
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, Archivado)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, Trim(NCCodFT))
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, Trim(NCCodFTEmpr))
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, Trim(NCGlosa))
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, NCImporte)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Trim(sPeriodo))
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, Trim(TIngreso))
            cCapaDatos.sParametroSQL("@PdoCodigoOC", DbType.String, 3, VL_PdoCodigoOC)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                'NO MUESTRA NINGUN MENSAJE SI SE GUARDA
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCancelarFactura(ByVal iOpcion As Integer, ByVal IdRegistro As String, ByVal EmprCodigo As String, ByVal Situacion As String, ByVal ActivoDes As Integer, ByVal Cancelado As Integer, ByVal Usuario As String, ByVal Archivado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, Trim(Situacion))
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, Trim(Usuario))
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Trim(IdRegistro))
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Trim(EmprCodigo))
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, ActivoDes)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, Cancelado)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, Archivado)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEditarEstadoOrden(ByVal EstadoPro As Integer, ByVal IdOC As String, ByVal EmprCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 34)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Trim(EmprCodigo))
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, EstadoPro)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, IdOC)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            'IdOC
            'NroOC

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fGrabar2(ByVal iOpcion As Integer, ByVal IdAcreedor As String, ByVal CodAcreedor As String, ByVal NombreAcreedor As String, ByVal IdDocumento As String, ByVal NroSerie As String, ByVal NroDocumento As String, ByVal FechaBase As DateTime, ByVal Dias As Int32, ByVal FechaDocumento As DateTime, ByVal FechaVencimiento As DateTime, ByVal IdMoneda As String, ByVal ImporteIgv As Double, ByVal TasaIGV As Double, ByVal ImporteTotal As Double, ByVal ValorDeclarado As Decimal, ByVal ValorCalculado As Double, ByVal Inafecto As Double, ByVal IdArea As String, ByVal Descripcion As String, ByVal Situacion As String, ByVal FechaCreacion As DateTime, ByVal FechaRecepcion As DateTime, ByVal Usuario As String, ByVal IdRegistro As String, ByVal EmprCodigo As String, ByVal EstadoPrePro As Integer, ByVal EstadoPro As Integer, ByVal ValorCompra As Decimal, ByVal IGV As Integer, ByVal DP As String, ByVal PorcDP As Double) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, Trim(IdAcreedor))
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Trim(CodAcreedor))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, Trim(NombreAcreedor))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, Trim(IdDocumento))
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, Trim(NroSerie))
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(NroDocumento))
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(FechaBase, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, Dias)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaDocumento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(FechaVencimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, Trim(IdMoneda))
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, ImporteIgv)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, TasaIGV)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, ImporteTotal)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, ValorDeclarado)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, ValorCalculado)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, Inafecto)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, Trim(IdArea))
            'cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, Trim(Situacion))
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(FechaCreacion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(FechaRecepcion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, Trim(Usuario))
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Trim(IdRegistro))
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Trim(EmprCodigo))
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, EstadoPrePro)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, EstadoPro)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, ValorCompra)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, IGV)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, DP)
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, PorcDP)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fGrabarSaldoDoc(ByVal iOpcion As Integer, ByVal IdAcreedor As String, ByVal CodAcreedor As String, ByVal NombreAcreedor As String, ByVal IdDocumento As String, ByVal NroSerie As String, ByVal NroDocumento As String, ByVal FechaBase As DateTime, ByVal Dias As Int32, ByVal FechaDocumento As DateTime, ByVal FechaVencimiento As DateTime, ByVal IdMoneda As String, ByVal ImporteIgv As Double, ByVal TasaIGV As Double, ByVal ImporteTotal As Double, ByVal ValorDeclarado As Decimal, ByVal ValorCalculado As Double, ByVal Inafecto As Double, ByVal IdArea As String, ByVal Descripcion As String, ByVal Situacion As String, ByVal FechaCreacion As DateTime, ByVal FechaRecepcion As DateTime, ByVal Usuario As String, ByVal IdRegistro As String, ByVal EmprCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, Trim(IdAcreedor))
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Trim(CodAcreedor))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, Trim(NombreAcreedor))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, Trim(IdDocumento))
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, Trim(NroSerie))
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(NroDocumento))
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(FechaBase, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, Dias)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaDocumento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(FechaVencimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, Trim(IdMoneda))
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, ImporteIgv)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, TasaIGV)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, ImporteTotal)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, ValorDeclarado)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, ValorCalculado)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, Inafecto)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, Trim(IdArea))
            'cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, Trim(Situacion))
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(FechaCreacion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(FechaRecepcion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, Trim(Usuario))
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Trim(IdRegistro))
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Trim(EmprCodigo))
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActuSaldoDoc(ByVal iOpcion As Integer, ByVal IdAcreedor As String, ByVal CodAcreedor As String, ByVal NombreAcreedor As String, ByVal IdDocumento As String, ByVal NroSerie As String, ByVal NroDocumento As String, ByVal FechaBase As DateTime, ByVal Dias As Int32, ByVal FechaDocumento As DateTime, ByVal FechaVencimiento As DateTime, ByVal IdMoneda As String, ByVal ImporteIgv As Double, ByVal TasaIGV As Double, ByVal ImporteTotal As Double, ByVal ValorDeclarado As Decimal, ByVal ValorCalculado As Double, ByVal Inafecto As Double, ByVal IdArea As String, ByVal Descripcion As String, ByVal Situacion As String, ByVal FechaCreacion As DateTime, ByVal FechaRecepcion As DateTime, ByVal Usuario As String, ByVal IdRegistro As String, ByVal EmprCodigo As String, ByVal EstadoPrePro As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, Trim(IdAcreedor))
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Trim(CodAcreedor))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, Trim(NombreAcreedor))
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, Trim(IdDocumento))
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, Trim(NroSerie))
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(NroDocumento))
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(FechaBase, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, Dias)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaDocumento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(FechaVencimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, Trim(IdMoneda))
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, ImporteIgv)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, TasaIGV)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, ImporteTotal)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, ValorDeclarado)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, ValorCalculado)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, Inafecto)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, Trim(IdArea))
            'cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, Trim(Situacion))
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(FechaCreacion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(FechaRecepcion, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, Trim(Usuario))
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Trim(IdRegistro))
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Trim(EmprCodigo))
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, EstadoPrePro)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function GrabarAcreedores(ByVal vOpcion As Integer, ByVal CodEmpresa As String, ByVal TipoAnaliticoCod As String, ByVal AnaliticoCodigo As String, ByVal AnaliticoDescripcion As String, ByVal Direccion As String, ByVal ruc As Decimal, ByVal MonedaCod As String, ByVal TipoPersona As String, ByVal IdDocIdentidad As String, ByVal NroDocIdentidad As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroAcreedor"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, TipoAnaliticoCod)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, AnaliticoCodigo)
            cCapaDatos.sParametroSQL("@AnaliticoDescripcion", DbType.String, 50, AnaliticoDescripcion)
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 100, Direccion)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, ruc)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonedaCod)
            cCapaDatos.sParametroSQL("@TipoPersona", DbType.String, 1, TipoPersona)
            cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, IdDocIdentidad)
            cCapaDatos.sParametroSQL("@NroDocIdentidad", DbType.String, 20, NroDocIdentidad)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If Trim(Direccion) <> "" Then
                If iResultado >= "1" Then
                    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
                Else
                    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
                End If
            End If
            

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function
    Public Function ActualizarAcreedor(ByVal CodEmpresa As String, ByVal TipoAnaliticoCod As String, ByVal AnaliticoCodigo As String, ByVal AnaliticoDescripcion As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroAcreedor"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, TipoAnaliticoCod)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, AnaliticoCodigo)
            cCapaDatos.sParametroSQL("@AnaliticoDescripcion", DbType.String, 100, AnaliticoDescripcion)
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoPersona", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocIdentidad", DbType.String, 20, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If Trim(Direccion) <> "" Then
            '    If iResultado >= "1" Then
            '        cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            '    Else
            '        cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            '    End If
            'End If


        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function GrabarAcreedores3(ByVal vOpcion As Integer, ByVal CodEmpresa As String, ByVal TipoAnaliticoCod As String, ByVal AnaliticoCodigo As String, ByVal AnaliticoDescripcion As String, ByVal Direccion As String, ByVal ruc As Decimal, ByVal MonedaCod As String, ByVal TipoPersona As String, ByVal IdDocIdentidad As String, ByVal NroDocIdentidad As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroAcreedor"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, TipoAnaliticoCod)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, AnaliticoCodigo)
            cCapaDatos.sParametroSQL("@AnaliticoDescripcion", DbType.String, 50, AnaliticoDescripcion)
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 100, Direccion)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, ruc)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonedaCod)
            cCapaDatos.sParametroSQL("@TipoPersona", DbType.String, 1, TipoPersona)
            cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, IdDocIdentidad)
            cCapaDatos.sParametroSQL("@NroDocIdentidad", DbType.String, 20, NroDocIdentidad)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function GrabarAcreedores2(ByVal vOpcion As Integer, ByVal CodEmpresa As String, ByVal TipoAnaliticoCod As String, ByVal AnaliticoCodigo As String, ByVal AnaliticoDescripcion As String, ByVal Direccion As String, ByVal ruc As Decimal, ByVal MonedaCod As String, ByVal TipoPersona As String, ByVal IdDocIdentidad As String, ByVal NroDocIdentidad As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroAcreedor"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, TipoAnaliticoCod)
            cCapaDatos.sParametroSQL("@AnaliticoCodigo", DbType.String, 11, AnaliticoCodigo)
            cCapaDatos.sParametroSQL("@AnaliticoDescripcion", DbType.String, 50, AnaliticoDescripcion)
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 100, Direccion)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, ruc)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonedaCod)
            cCapaDatos.sParametroSQL("@TipoPersona", DbType.String, 1, TipoPersona)
            cCapaDatos.sParametroSQL("@DIdeCodigo", DbType.String, 2, IdDocIdentidad)
            cCapaDatos.sParametroSQL("@NroDocIdentidad", DbType.String, 20, NroDocIdentidad)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    'fListarProveedores
    Public Function fListarProveedores(ByVal CodEmpresa As String, ByVal CodRuc As String, ByVal CodAcreedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, CodRuc)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarProveedores5(ByVal CodEmpresa As String, ByVal CodRuc As String, ByVal CodAcreedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 44)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, CodRuc)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    'fListarProveedores2
    Public Function fListarProveedores2(ByVal CodEmpresa As String, ByVal NroIdent As String, ByVal CodAcreedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, NroIdent)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarProveedores3(ByVal CodEmpresa As String, ByVal Codigo As String, ByVal CodAcreedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 18)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarProveedores4(ByVal CodEmpresa As String, ByVal sDescripcion As String, ByVal CodAcreedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, sDescripcion)
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarOrdenes(ByVal CodEmpresa As String, ByVal Ruc As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 33)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(Ruc))
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarOrdenesTodos(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal NroOrden As String, ByVal Ruc As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Trim(NroOrden))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(Ruc))
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Trim(NroOrden))
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarOrdenes2(ByVal CodEmpresa As String, ByVal Numero As String, ByVal Ruc As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 35)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(Ruc))
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Numero)
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

      Public Function fListarOrdenes3(ByVal CodEmpresa As String, ByVal Numero As String, ByVal Ruc As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 41)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, Trim(Ruc))
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Numero)
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    Public Function fListarProveedoresxRuc(ByVal CodEmpresa As String, ByVal CodRuc As String, ByVal CodAcreedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, CodRuc)
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarProveedoresxRazonSocial(ByVal CodEmpresa As String, ByVal CodRuc As String, ByVal CodAcreedor As String, ByVal NombreAcreedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 36)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, NombreAcreedor)
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, CodRuc)
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    'fListarProveedoresxRuc
    'fListarProveedores4
    'fListarProveedores3

    Public Function VerificarDocExiste(ByVal TipoDoc As String, ByVal NroSerie As String, ByVal NroDocumento As String, ByVal CodProveedor As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 25)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, CodProveedor)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, TipoDoc)
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, NroSerie)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, NroDocumento)
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function



    Public Function VerificarLinea(ByVal CodProveedor As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 37)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, CodProveedor)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    Public Function fEliminar2(ByVal Codigo As String, ByVal vOpcion As Integer, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado = 0 Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fBuscarDoble(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodTipAnex As String, ByVal Codigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodTipAnex)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))
            'If iResultado > 0 Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fBuscarDoble2(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodigoRuc As String, ByVal CodTipAcreedor As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, CodTipAcreedor)
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, CodigoRuc)
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))
            'If iResultado > 0 Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fListarConsultaDoc(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal IdTipoAnexo As String, ByVal IdAnexo As String, ByVal IdTipoDocumento As String, ByVal FechaInicio As DateTime, ByVal FechaFinal As DateTime, ByVal CodArea As String, ByVal IdMoneda As String, ByVal Periodo As String, ByVal Usuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.ListarConsultaDoc"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdAnexo", DbType.String, 11, IdAnexo)
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, IdTipoDocumento)
            cCapaDatos.sParametroSQL("@FechaCreacionIni", DbType.DateTime, 10, Format(FechaInicio, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaCreacionFin", DbType.DateTime, 10, Format(FechaFinal, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, CodArea)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, Usuario)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarSituacion() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX    GENERA ASIENTO DE COMPRAS AUTOMATICO    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    Public Function Consulta_OC(ByVal pCodEmpresa As String, ByVal pPeriodo As String, ByVal pOC_Codigo As String, _
                            ByVal pOC_Numero As String, ByVal pProveedor As String) As SqlDataReader
        cCapaDatos = New clsCapaDatos
        Dim dReader As SqlDataReader

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
            cCapaDatos.sParametroSQL("@PERIODO", DbType.String, 4, pPeriodo)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@OCOCODIGO", DbType.String, 10, pOC_Codigo)
            cCapaDatos.sParametroSQL("@OCONUMERO", DbType.String, 15, pOC_Numero)
            cCapaDatos.sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHATC", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
            cCapaDatos.sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@PRVCODIGO", DbType.String, 50, pProveedor)
            cCapaDatos.sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

            cCapaDatos.sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

            cCapaDatos.sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

            dReader = cCapaDatos.fCmdExecuteReader

        Catch ex As Exception
            Throw ex
            dReader.Close()
        End Try
        Return dReader
    End Function

    Public Function PlantillaCompras(ByVal CODEMPRESA As String, ByVal PERIODO As String, ByVal ABRVMONEDA As String, _
                                    ByVal CODMONEDA As String, ByVal CODIGO_OC As String, ByVal NUMERO_OC As String, _
                                    ByVal COD_GESTION As String, ByVal FLAG_IGV As String) As DataTable
        cCapaDatos = New clsCapaDatos
        Dim DTabla As New DataTable

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 1)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, COD_GESTION)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, CODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, PERIODO)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, CODMONEDA)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, ABRVMONEDA)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, CODIGO_OC)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, NUMERO_OC)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, FLAG_IGV)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .sAdaptadorSQL("R")
                DTabla = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
        End Try
        Return DTabla
    End Function

    Public Function Datos_OC(ByVal CODEMPRESA As String, ByVal PERIODO As String, ByVal CODMONEDA As String, _
                               ByVal CODIGO_OC As String, ByVal NUMERO_OC As String, ByVal COD_GESTION As String) As DataSet

        cCapaDatos = New clsCapaDatos
        Dim Dset As New DataSet

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 3)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, COD_GESTION)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, CODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, PERIODO)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, CODMONEDA)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, CODIGO_OC)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, NUMERO_OC)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .sAdaptadorSQL("R")
                Dset = .fListarSet
                Call Dset_NombreTablas(Dset)
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
        End Try
        Return Dset
    End Function

    Public Function TipoCambio_Actual(ByVal pFECHA As Date, ByVal pMONEDA As String) As Double
        Dim DReader As SqlDataReader
        Dim DblCoV As Double = 0
        cCapaDatos = New clsCapaDatos

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 4)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, pMONEDA)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, pFECHA)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                DReader = .fCmdExecuteReader

                If DReader.HasRows = True Then
                    DReader.Read()
                    DblCoV = DReader.Item("Venta")
                End If

            Catch ex As Exception
                DblCoV = 0
                DReader.Close()
            End Try
        End With
        Return DblCoV
    End Function

    Private Sub Dset_NombreTablas(ByVal dst As DataSet)
        'Asigando Nombre a Tablas que estan dentro del DataSet
        For x As Integer = 0 To dst.Tables.Count - 1
            If Not x = dst.Tables.Count - 1 Then
                dst.Tables(x).TableName = dst.Tables(dst.Tables.Count - 1).Rows(0).Item(x)
            End If
        Next
    End Sub

    Public Function MonedaSistema(ByVal Tabla As DataTable) As String
        Dim MonSist As String = String.Empty
        For i As Integer = 0 To Tabla.Rows.Count - 1
            If Tabla.Rows(i).Item("MonNacional") = "1" Then
                MonSist = Tabla.Rows(i).Item("Abreviado")
                Return MonSist
            Else
                MonSist = String.Empty
            End If
        Next
        Return MonSist
    End Function

    Public Function DocumentosCompra(ByVal CODEMPRESA As String) As DataTable
        cCapaDatos = New clsCapaDatos
        Dim DTabla As New DataTable

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 2)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, CODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .sAdaptadorSQL("R")
                DTabla = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
        End Try
        Return DTabla
    End Function

    Public Function AbrevDocumento(ByVal pCodDocumento As String) As String
        cCapaDatos = New clsCapaDatos
        Dim DTabla As New DataTable
        Dim CadSQL As String
        Dim sAbrev As String

        Try
            With cCapaDatos
                .sConectarSQL()
                CadSQL = "SELECT AbrDoc FROM Tesoreria.TipoDocumento WHERE IdDocumento='" & pCodDocumento & "'"
                .sComandoSQL(CadSQL, CommandType.Text)
                sAbrev = .fCmdExecuteScalar()
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            sAbrev = String.Empty
        End Try
        Return sAbrev
    End Function

    Public Function CargarMonedas() As DataTable
        Dim TblMonedas As New DataTable
        Dim SQLCad As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()

                SQLCad = "SELECT MonCodigo as Codigo, MonAbreviado as Abreviado, MonNacional FROM Comun.Moneda"

                .sComandoSQL(SQLCad, CommandType.Text)
                .sAdaptadorSQL("R")
                TblMonedas = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            TblMonedas = Nothing
        End Try
        Return TblMonedas
    End Function

    Public Function AbreviaturaMoneda(ByVal StrCodMoneda As String) As String
        Dim StrAbrevMon As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("SELECT MonAbreviado FROM Comun.Moneda WHERE MonCodigo='" & StrCodMoneda & "'", CommandType.Text)
                .sAdaptadorSQL("R")
                StrAbrevMon = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            MsgBox(Convert.ToString(ex.Message))
            StrAbrevMon = String.Empty
        End Try
        Return StrAbrevMon
    End Function

    Public Function CodigoMoneda(ByVal AbrevMoneda As String) As String
        Dim StrCodMonSist As String = String.Empty
        cCapaDatos = New clsCapaDatos
        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("SELECT MonCodigo FROM Comun.Moneda WHERE MonAbreviado='" & AbrevMoneda & "'", CommandType.Text)
                .sAdaptadorSQL("R")
                StrCodMonSist = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
            StrCodMonSist = String.Empty
        End Try
        Return StrCodMonSist
    End Function


    Public Function Cta_Con_Anexo(ByVal CodEmpresa As String, ByVal CodCta As String, ByVal pPerido As String) As Boolean
        Dim DRlector As SqlDataReader
        cCapaDatos = New clsCapaDatos

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("SELECT cuentacodigo as Codigo, cuentadescripcion as Descripcion,isnull(tipoanaliticocodigo,'') as TipoAnexo " & _
                            "FROM CONTABILIDAD.ct_cuenta " & _
                            "WHERE EmprCodigo='" & CodEmpresa & "' AND cuentacodigo='" & CodCta & "' AND Periodo='" & pPerido & "' AND tipoanaliticocodigo<>''", CommandType.Text)
                DRlector = .fCmdExecuteReader
                If DRlector.HasRows = True Then
                    DRlector.Read()
                    If DRlector("TipoAnexo") = String.Empty Then
                        Return False
                    ElseIf DRlector("TipoAnexo") <> String.Empty Then
                        Return True
                    End If
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End With
    End Function

    Public Function Con_CenCosto(ByVal CODEMPRESA As String, ByVal PERIODO As String, ByVal CUENTA As String) As String
        cCapaDatos = New clsCapaDatos
        Dim CenCosto As String

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 5)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, CODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, PERIODO)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, CUENTA)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                CenCosto = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            CenCosto = String.Empty
        End Try
        Return CenCosto
    End Function

    Public Function Devuelve_TipoCtaCont(ByVal CODEMPRESA As String, ByVal PERIODO As String, ByVal CUENTA As String) As String
        cCapaDatos = New clsCapaDatos
        Dim sTipoCuenta As String

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 6)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, CODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, PERIODO)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, CUENTA)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                sTipoCuenta = .fCmdExecuteScalar
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            sTipoCuenta = String.Empty
        End Try
        Return sTipoCuenta
    End Function

    Public Function ImportesAsientos(ByVal Dset As DataSet, ByVal TablaMonedas As DataTable) As DataTable
        Dim TblImportes As New DataTable
        Dim DblDEBE As Double = 0
        Dim DblHABER As Double = 0
        Dim StrNameTabla As String = String.Empty
        Dim StrAbrevMon As String

        TblImportes.Clear()
        TblImportes.Columns.Add("Moneda", GetType(System.String))
        TblImportes.Columns.Add("Debe", GetType(System.Decimal))
        TblImportes.Columns.Add("Haber", GetType(System.Decimal))



        For IMonedas As Integer = 0 To TablaMonedas.Rows.Count - 1
            StrAbrevMon = AbreviaturaMoneda(TablaMonedas.Rows(IMonedas).Item("Codigo"))
            StrNameTabla = "TmpAsiento" & StrAbrevMon
            For IFilas As Integer = 0 To Dset.Tables(StrNameTabla).Rows.Count - 1
                DblDEBE += Dset.Tables(StrNameTabla).Rows(IFilas).Item("Debe")
                DblHABER += Dset.Tables(StrNameTabla).Rows(IFilas).Item("Haber")
            Next
            TblImportes.Rows.Add(TablaMonedas.Rows(IMonedas).Item("Abreviado"), DblDEBE, DblHABER)
            DblDEBE = 0
            DblHABER = 0
        Next

        Return TblImportes
    End Function

    Public Function TC_Actual_X_Moneda(ByVal FECHATC As Date) As DataTable
        cCapaDatos = New clsCapaDatos
        Dim DTabla As New DataTable

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 7)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, FECHATC)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .sAdaptadorSQL("R")
                DTabla = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
        End Try
        Return DTabla
    End Function

    Public Function GenerarCodigoVoucher(ByVal EMPRESA As String, ByVal PERIODO As String, ByVal MES As String, _
                                  ByVal SUBDIARIO As String, ByVal USER As String, ByVal COMPUTER As String) As String
        Dim StrCODIGO As String = String.Empty
        cCapaDatos = New clsCapaDatos

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("Contabilidad.uspRegistro_Compras", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 20)
                .sParametroSQL("@EMPRESACODIGO", DbType.String, 3, EMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, PERIODO)
                .sParametroSQL("@MONEDACODIGO", DbType.String, 2, String.Empty)
                .sParametroSQL("@TIPOCAMBIOFECHA", DbType.Date, 10, Format(Today, "dd/MM/yyyy"))
                .sParametroSQL("@ANALITICOCODIGO", DbType.String, 11, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, SUBDIARIO)
                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@IDMES", DbType.String, 2, MES)
                .sParametroSQL("@VOUCHER_FECHA", DbType.Date, 10, Today)
                .sParametroSQL("@MONTO_D", DbType.Double, 22, 0)
                .sParametroSQL("@MONTO_H", DbType.Double, 22, 0)
                .sParametroSQL("@MONTO_TIPOCAMBIO", DbType.Double, 22, 0)
                .sParametroSQL("@FECHA_TIPOCAMBIO", DbType.Date, 15, Today)
                .sParametroSQL("@ID_FORMATOTCAMBIO", DbType.String, 1, String.Empty)
                .sParametroSQL("@GLOSA_CABECERA", DbType.String, 200, String.Empty)
                .sParametroSQL("@F_VOUCHER_AUTOMATIC", DbType.String, 1, String.Empty)
                .sParametroSQL("@CODIGO_AREA", DbType.String, 1, String.Empty)
                .sParametroSQL("@F_MON_SIST", DbType.String, 1, String.Empty)
                .sParametroSQL("@F_DOCUMENT_TRANSAC", DbType.String, 1, String.Empty)
                .sParametroSQL("@USUARIOCODIGO", DbType.String, 8, USER)
                .sParametroSQL("@FECHAACT", DbType.Date, 15, Today)
                .sParametroSQL("@ESTADO", DbType.Int32, 1, 1)
                .sParametroSQL("@PC", DbType.String, 15, COMPUTER)
                .sParametroSQL("@GLOSADETALLE", DbType.String, 100, String.Empty)

                .sParametroSQL("@SECUENCIA", DbType.String, 5, String.Empty)
                .sParametroSQL("@TIPO_DOC", DbType.String, 15, String.Empty)
                .sParametroSQL("@SERIE_DOC", DbType.String, 10, String.Empty)
                .sParametroSQL("@NUMERO_DOC", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHA_DOC", DbType.Date, 15, Today)
                .sParametroSQL("@CUENTACODIGO", DbType.String, 20, String.Empty)
                .sParametroSQL("@IND_X_DIFCAMBIO", DbType.String, 1, String.Empty)
                .sParametroSQL("@CUENTA_DESTINO", DbType.String, 30, String.Empty)
                .sParametroSQL("@TIPOANALITICOCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@COMENT_DOCUMENTO", DbType.String, 200, String.Empty)
                .sParametroSQL("@CENTROCOSTOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@FECHA_CADUCIDAD", DbType.Date, 15, Today)
                .sParametroSQL("@MONTO_ORIGINAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@F_EXIST_DOC", DbType.String, 1, String.Empty)
                .sParametroSQL("@USUARIOMODIF", DbType.String, 15, String.Empty)
                .sParametroSQL("@FECHAMODIF", DbType.Date, 15, Today)
                .sParametroSQL("@PCMODIF", DbType.String, 15, String.Empty)
                .sParametroSQL("@IDFORM", DbType.String, 1, String.Empty)

                StrCODIGO = .fCmdExecuteScalar()
                .sDesconectarSQL()
            Catch ex As Exception
                MsgBox(Convert.ToString(ex.Message))
                .sConectarSQL()
                Return ""
            End Try
        End With
        Return StrCODIGO
    End Function

    Public Function DevuelveMontoTCxMoneda(ByVal CodEmpresa As String, ByVal FechaTC As Date, ByVal AbrevMon As String, ByVal CoV As String) As Double
        Dim DblMontoTC As Double
        Dim DRlector As SqlDataReader
        cCapaDatos = New clsCapaDatos

        With cCapaDatos
            Try
                .sConectarSQL()
                .sComandoSQL("Contabilidad.uspComprobantesVarios", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 1, 1)
                .sParametroSQL("@EMPRESACODIGO", DbType.String, 3, CodEmpresa)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MONEDACODIGO", DbType.String, 2, AbrevMon)
                .sParametroSQL("@TIPOCAMBIOFECHA", DbType.Date, 10, FechaTC)
                .sParametroSQL("@ANALITICOCODIGO", DbType.String, 11, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)
                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@IDMES", DbType.String, 2, String.Empty)
                .sParametroSQL("@VOUCHER_FECHA", DbType.Date, 10, Today)
                .sParametroSQL("@MONTO_D", DbType.Double, 22, 0)
                .sParametroSQL("@MONTO_H", DbType.Double, 22, 0)
                .sParametroSQL("@MONTO_TIPOCAMBIO", DbType.Double, 22, 0)
                .sParametroSQL("@FECHA_TIPOCAMBIO", DbType.Date, 15, Today)
                .sParametroSQL("@ID_FORMATOTCAMBIO", DbType.String, 1, String.Empty)
                .sParametroSQL("@GLOSA_CABECERA", DbType.String, 200, String.Empty)
                .sParametroSQL("@F_VOUCHER_AUTOMATIC", DbType.String, 1, String.Empty)
                .sParametroSQL("@CODIGO_AREA", DbType.String, 5, String.Empty)
                .sParametroSQL("@F_MON_SIST", DbType.String, 1, String.Empty)
                .sParametroSQL("@F_DOCUMENT_TRANSAC", DbType.String, 1, String.Empty)
                .sParametroSQL("@USUARIOCODIGO", DbType.String, 8, String.Empty)
                .sParametroSQL("@FECHAACT", DbType.Date, 15, Today)
                .sParametroSQL("@ESTADO", DbType.Int32, 1, 1)
                .sParametroSQL("@PC", DbType.String, 15, String.Empty)
                .sParametroSQL("@GLOSADETALLE", DbType.String, 100, String.Empty)

                .sParametroSQL("@SECUENCIA", DbType.String, 5, String.Empty)
                .sParametroSQL("@TIPO_DOC", DbType.String, 15, String.Empty)
                .sParametroSQL("@SERIE_DOC", DbType.String, 10, String.Empty)
                .sParametroSQL("@NUMERO_DOC", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHA_DOC", DbType.Date, 15, Today)
                .sParametroSQL("@CUENTACODIGO", DbType.String, 20, String.Empty)
                .sParametroSQL("@IND_X_DIFCAMBIO", DbType.String, 1, String.Empty)
                .sParametroSQL("@CUENTA_DESTINO", DbType.String, 30, String.Empty)
                .sParametroSQL("@TIPOANALITICOCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@COMENT_DOCUMENTO", DbType.String, 200, String.Empty)
                .sParametroSQL("@CENTROCOSTOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@FECHA_CADUCIDAD", DbType.Date, 15, Today)
                .sParametroSQL("@MONTO_ORIGINAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@F_EXIST_DOC", DbType.String, 1, String.Empty)
                .sParametroSQL("@USUARIOMODIF", DbType.String, 15, String.Empty)
                .sParametroSQL("@FECHAMODIF", DbType.Date, 15, Today)
                .sParametroSQL("@PCMODIF", DbType.String, 15, String.Empty)

                DRlector = .fCmdExecuteReader

                If DRlector.HasRows = True Then
                    DRlector.Read()
                    If (CoV = "1" Or CoV = "Compra") Then
                        DblMontoTC = DRlector("Compra")
                    ElseIf (CoV = "2" Or CoV = "Venta") Then
                        DblMontoTC = DRlector("Venta")
                    End If
                Else
                    DblMontoTC = 0
                End If
                DRlector.Close()
                .sConectarSQL()
            Catch ex As Exception
                DblMontoTC = 0
                .sConectarSQL()
                Return 0
            End Try
        End With
        Return DblMontoTC
    End Function

    Public Function CondicionIngresoOC(ByVal CodEmpresa As String, ByVal Periodo As String, ByVal Moneda As String, _
                                    ByVal CodigoOrden As String, ByVal NumeroOrden As String) As Integer
        Dim iCant As Integer
        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto2", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 5)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 2, CodEmpresa)
                .sParametroSQL("@PERIODO", DbType.String, 4, Periodo)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@MONEDA", DbType.String, 2, Moneda)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, CodigoOrden)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, NumeroOrden)
                .sParametroSQL("@SECUENCIA", DbType.String, 5, String.Empty)
                .sParametroSQL("@CUENTACODIGO", DbType.String, 20, String.Empty)
                .sParametroSQL("@TIPOANALITICOCODIGO", DbType.String, 2, String.Empty)
                .sParametroSQL("@ANALITICOCODIGO", DbType.String, 11, String.Empty)
                .sParametroSQL("@CENTROCOSTOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@MONTO_D", DbType.Decimal, 22, 0)
                .sParametroSQL("@MONTO_H", DbType.Decimal, 22, 0)
                .sParametroSQL("@TIPO_DOC", DbType.String, 15, String.Empty)
                .sParametroSQL("@SERIE_DOC", DbType.String, 15, String.Empty)
                .sParametroSQL("@NUMERO_DOC", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHA_DOC", DbType.Date, 15, Today)
                .sParametroSQL("@FECHA_CADUCIDAD", DbType.Date, 15, Today)
                .sParametroSQL("@CODIGO_AREA", DbType.String, 5, String.Empty)
                .sParametroSQL("@ANEXREF", DbType.String, 11, String.Empty)
                .sParametroSQL("@GLOSADET", DbType.String, 200, String.Empty)

                .sParametroSQL("@VALCOMPRA", DbType.Double, 22, 0)
                .sParametroSQL("@VALIGV", DbType.Double, 22, 0)
                .sParametroSQL("@VALTOTAL", DbType.Double, 22, 0)
                .sParametroSQL("@UPDUSER", DbType.String, 20, String.Empty)
                .sParametroSQL("@UPDPC", DbType.String, 11, String.Empty)
                .sParametroSQL("@UPDFECHA", DbType.Date, 11, Format(Today, "dd/MM/yyyy"))
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 11, Today)
                .sParametroSQL("@MONTOTC", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 11, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                iCant = .fCmdExecuteScalar()
            End With

        Catch ex As Exception
            cTempo.sMensaje("Error en la busqueda de O/C para Insertar o Actualizar")
            Return -1
        End Try
        Return iCant
    End Function

    Private Function GenCodigoGestion(ByVal pCodEmpresa As String, ByVal pPeriodo As String, ByVal pMes As String) As String
        Dim sCod As String

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 11)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPeriodo)
                .sParametroSQL("@MES", DbType.String, 2, pMes)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                sCod = .fCmdExecuteScalar()
            End With

        Catch ex As Exception
            cTempo.sMensaje("Error al generar el Codigo Autogenerado")
            Return String.Empty
        End Try
        Return sCod
    End Function

    Public Function VariosIngresosOC(ByVal pCODEMPRESA As String, ByVal pPERIODO As String, ByVal pMES As String, ByVal pMONEDA As String, _
                                ByVal pCodORDEN As String, ByVal pNumORDEN As String, ByVal pFECHAVOUCHER As Date, _
                                ByVal pFACTCODIGO As String, ByVal pFACTSERIE As String, ByVal pFACTNUMERO As String, _
                                ByVal pFECH_EMI As Date, ByVal pFECH_VEN As Date, ByVal pBASEIMPONIBLE As Double, ByVal PMONTOIGV As Double, _
                                ByVal pIMPORTETOTAL As Double, ByVal pCENCOSTO As String, ByVal pCODANEXO As String, ByVal pGLOSA As String, _
                                ByVal pTASAIGV As Double, ByVal pFLAGIGV As String, ByVal pFLAGDETRAC As Integer, ByVal pFLAGPERCEP As Integer, _
                                ByVal pUSER As String, ByVal pPC As String, ByVal pFECHAREG As Date, ByVal DTMONEDAS As DataTable, _
                                ByVal DSTDETALLE As DataSet, ByVal BOL_EDIT As Boolean, ByVal EDIT_CODGESTION As String) As String

        Try
            'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            Dim StrCodGestion As String
            Dim IntFila As Integer = 0

            cCapaDatos = New clsCapaDatos

            StrCodGestion = GenCodigoGestion(pCODEMPRESA, pPERIODO, pMES)

            With cCapaDatos
                .sConectarSQL()
                .sComenzarTransaccion()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto3", CommandType.StoredProcedure)
                If BOL_EDIT = False Then
                    .sParametroSQL("@OPCION", DbType.Int32, 2, 1)
                Else
                    .sParametroSQL("@OPCION", DbType.Int32, 2, 4)
                    StrCodGestion = EDIT_CODGESTION
                End If
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, StrCodGestion)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 2, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                .sParametroSQL("@MES", DbType.String, 2, pMES)
                .sParametroSQL("@MONEDA", DbType.String, 2, pMONEDA)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, pCodORDEN)
                .sParametroSQL("@OCOSERIE", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, pNumORDEN)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, pFECHAVOUCHER)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Double, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Double, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 50, pFACTCODIGO)
                .sParametroSQL("@FACTSERIE", DbType.String, 50, pFACTSERIE)
                .sParametroSQL("@FACTNUMERO", DbType.String, 50, pFACTNUMERO)
                .sParametroSQL("@FECHA_EMI", DbType.Date, 15, pFECH_EMI)
                .sParametroSQL("@FECHA_VEN", DbType.Date, 15, pFECH_VEN)
                .sParametroSQL("@VALCOMPRA_TESO", DbType.Double, 22, pBASEIMPONIBLE)
                .sParametroSQL("@MONTOIGV_TESO", DbType.Double, 22, PMONTOIGV)
                .sParametroSQL("@TOTAL_TESO", DbType.Double, 22, pIMPORTETOTAL)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, pCENCOSTO)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, pCODANEXO)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 250, pGLOSA)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, pFLAGIGV)
                .sParametroSQL("@TASA_IGV", DbType.Double, 22, pTASAIGV)
                .sParametroSQL("@IMPORTE_IGV", DbType.Double, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Double, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.Int32, 1, pFLAGDETRAC)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.Int32, 1, pFLAGPERCEP)

                .sParametroSQL("@USUCREACION", DbType.String, 20, pUSER)
                .sParametroSQL("@PCCREACION", DbType.String, 30, pPC)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, pFECHAREG)
                .sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .fCmdExecuteNonQuery()

                'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

                'If DSTDETALLE.Tables.Count > 0 Then

                '    .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto2", CommandType.StoredProcedure)

                '    Dim StrTablaDet As String = String.Empty
                '    Dim IntFilasDet As Integer = 0

                '    Do While DTMONEDAS.Rows.Count > IntFila
                '        StrTablaDet = "TmpAsiento" & DTMONEDAS.Rows(IntFila).Item("Abreviado")
                '        IntFilasDet = 0

                '        Do While DSTDETALLE.Tables(StrTablaDet).Rows.Count > IntFilasDet
                '            .sParametroSQL("@OPCION", DbType.Int32, 2, 1)
                '            .sParametroSQL("@CODIGOGESTION", DbType.String, 13, StrCodGestion)
                '            .sParametroSQL("@EMPRCODIGO", DbType.String, 2, pCODEMPRESA)
                '            .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                '            .sParametroSQL("@MES", DbType.String, 2, pMES)
                '            .sParametroSQL("@MONEDA", DbType.String, 2, DTMONEDAS.Rows(IntFila).Item("Abreviado"))
                '            .sParametroSQL("@OCOCODIGO", DbType.String, 10, pCodORDEN)
                '            .sParametroSQL("@OCONUMERO", DbType.String, 15, pNumORDEN)
                '            .sParametroSQL("@SECUENCIA", DbType.String, 5, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Secuencia"))
                '            .sParametroSQL("@CUENTACODIGO", DbType.String, 20, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Codigo_Cuenta"))
                '            .sParametroSQL("@TIPOANALITICOCODIGO", DbType.String, 2, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Cod_Tipo_Anexo"))
                '            .sParametroSQL("@ANALITICOCODIGO", DbType.String, 11, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Codigo_Anexo"))
                '            .sParametroSQL("@CENTROCOSTOCODIGO", DbType.String, 10, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("ID_CentroCosto"))
                '            .sParametroSQL("@MONTO_D", DbType.Decimal, 22, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Debe"))
                '            .sParametroSQL("@MONTO_H", DbType.Decimal, 22, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Haber"))
                '            .sParametroSQL("@TIPO_DOC", DbType.String, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Tipo_Doc"))
                '            .sParametroSQL("@SERIE_DOC", DbType.String, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Serie_Doc"))
                '            .sParametroSQL("@NUMERO_DOC", DbType.String, 30, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Nro_Doc"))
                '            .sParametroSQL("@FECHA_DOC", DbType.Date, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("FechaDoc"))
                '            .sParametroSQL("@FECHA_CADUCIDAD", DbType.Date, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Caduca_Fecha"))
                '            .sParametroSQL("@CODIGO_AREA", DbType.String, 5, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Area"))
                '            .sParametroSQL("@ANEXREF", DbType.String, 11, String.Empty)
                '            .sParametroSQL("@GLOSADET", DbType.String, 200, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("ComentarioCab"))

                '            .sParametroSQL("@VALCOMPRA", DbType.Double, 22, 0)
                '            .sParametroSQL("@VALIGV", DbType.Double, 22, 0)
                '            .sParametroSQL("@VALTOTAL", DbType.Double, 22, 0)
                '            .sParametroSQL("@UPDUSER", DbType.String, 20, String.Empty)
                '            .sParametroSQL("@UPDPC", DbType.String, 11, String.Empty)
                '            .sParametroSQL("@UPDFECHA", DbType.Date, 11, Today)
                '            .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 11, Today)
                '            .sParametroSQL("@MONTOTC", DbType.Decimal, 22, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("MontoTC"))
                '            .sParametroSQL("@FECHATC", DbType.Date, 11, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("FechaTC"))

                '            .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                '            .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                '            .fCmdExecuteNonQuery()
                '            .fLimpiaParametrosSQL()

                '            IntFilasDet += 1
                '        Loop
                '        .fLimpiaParametrosSQL()
                '        IntFila += 1
                '    Loop
                'End If

                .sConfirmarTransaccion()
            End With
            Return StrCodGestion
        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion()
            Return String.Empty
        End Try

    End Function

    Public Function Insertar_Asiento_Temp(ByVal DSTDETALLE As DataSet, ByVal DTMONEDAS As DataTable, ByVal StrCodGestion As String, _
                    ByVal pCODEMPRESA As String, ByVal pPERIODO As String, ByVal pMES As String, ByVal pCodORDEN As String, ByVal pNumORDEN As String) As Integer
        cCapaDatos = New clsCapaDatos
        Dim IntFila As Integer
        '&&&&&&&&&&&&&&&&&&&&&& INSERTANDO REGISTROS EN TABLA TEMPORAL ASIENTOS &&&&&&&&&&&&&&&&&&&&&&
        With cCapaDatos

            If DSTDETALLE.Tables.Count > 0 Then
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto2", CommandType.StoredProcedure)

                Dim StrTablaDet As String = String.Empty
                Dim IntFilasDet As Integer = 0

                Do While DTMONEDAS.Rows.Count > IntFila
                    StrTablaDet = "TmpAsiento" & DTMONEDAS.Rows(IntFila).Item("Abreviado")
                    IntFilasDet = 0

                    Do While DSTDETALLE.Tables(StrTablaDet).Rows.Count > IntFilasDet
                        .sParametroSQL("@OPCION", DbType.Int32, 2, 1)
                        .sParametroSQL("@CODIGOGESTION", DbType.String, 13, StrCodGestion)
                        .sParametroSQL("@EMPRCODIGO", DbType.String, 2, pCODEMPRESA)
                        .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                        .sParametroSQL("@MES", DbType.String, 2, pMES)
                        .sParametroSQL("@MONEDA", DbType.String, 2, DTMONEDAS.Rows(IntFila).Item("Abreviado"))
                        .sParametroSQL("@OCOCODIGO", DbType.String, 10, pCodORDEN)
                        .sParametroSQL("@OCONUMERO", DbType.String, 15, pNumORDEN)
                        .sParametroSQL("@SECUENCIA", DbType.String, 5, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Secuencia"))
                        .sParametroSQL("@CUENTACODIGO", DbType.String, 20, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Codigo_Cuenta"))
                        .sParametroSQL("@TIPOANALITICOCODIGO", DbType.String, 2, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Cod_Tipo_Anexo"))
                        .sParametroSQL("@ANALITICOCODIGO", DbType.String, 11, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Codigo_Anexo"))
                        .sParametroSQL("@CENTROCOSTOCODIGO", DbType.String, 10, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("ID_CentroCosto"))
                        .sParametroSQL("@MONTO_D", DbType.Decimal, 22, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Debe"))
                        .sParametroSQL("@MONTO_H", DbType.Decimal, 22, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Haber"))
                        .sParametroSQL("@TIPO_DOC", DbType.String, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Tipo_Doc"))
                        .sParametroSQL("@SERIE_DOC", DbType.String, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Serie_Doc"))
                        .sParametroSQL("@NUMERO_DOC", DbType.String, 30, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Nro_Doc"))
                        .sParametroSQL("@FECHA_DOC", DbType.Date, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("FechaDoc"))
                        .sParametroSQL("@FECHA_CADUCIDAD", DbType.Date, 15, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Caduca_Fecha"))
                        .sParametroSQL("@CODIGO_AREA", DbType.String, 5, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("Area"))
                        .sParametroSQL("@ANEXREF", DbType.String, 11, String.Empty)
                        .sParametroSQL("@GLOSADET", DbType.String, 200, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("ComentarioCab"))

                        .sParametroSQL("@VALCOMPRA", DbType.Double, 22, 0)
                        .sParametroSQL("@VALIGV", DbType.Double, 22, 0)
                        .sParametroSQL("@VALTOTAL", DbType.Double, 22, 0)
                        .sParametroSQL("@UPDUSER", DbType.String, 20, String.Empty)
                        .sParametroSQL("@UPDPC", DbType.String, 11, String.Empty)
                        .sParametroSQL("@UPDFECHA", DbType.Date, 11, Today)
                        .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 11, Today)
                        .sParametroSQL("@MONTOTC", DbType.Decimal, 22, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("MontoTC"))
                        .sParametroSQL("@FECHATC", DbType.Date, 11, DSTDETALLE.Tables(StrTablaDet).Rows(IntFilasDet).Item("FechaTC"))

                        .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                        .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                        .fCmdExecuteNonQuery()
                        .fLimpiaParametrosSQL()

                        IntFilasDet += 1
                    Loop
                    .fLimpiaParametrosSQL()
                    IntFila += 1
                Loop
            End If
        End With
    End Function

    Public Function Insertar_Voucher_Compras_Tempo(ByVal pCODEMPRESA As String, ByVal pSUBDIARIO As String, ByVal pMES As String, ByVal pMONEDA As String, _
                    ByVal pPERIODO As String, ByVal pFECHAVOUCHER As Date, ByVal pDEBE As Double, ByVal pHABER As Double, _
                    ByVal pMONTOTC As Double, ByVal pFECHATC As Date, ByVal pFORMATOTC As String, ByVal pGLOSACAB As String, _
                    ByVal pISAUTOMATIC As String, ByVal pMONSIST As String, ByVal pMONDOCTRANS As String, ByVal pUSER As String, _
                    ByVal pFECHA As Date, ByVal pESTADO As String, ByVal pCOMPUTER As String, ByVal DSTDETALLE As DataSet, _
                    ByVal DTMONEDAS As DataTable, ByVal DTCAMBIOACTUAL As DataTable, ByVal TblMontosAsiento As DataTable, _
                    ByVal TCCabecera As DataTable, ByVal pFechaOtros As Date, ByVal FormatConta As Integer, ByVal pCodForm As String, _
                    ByVal IsEdit As Integer, ByVal sNUMVOUCHER As String, ByVal pCODANEXO As String, _
                    ByVal pDOC As String, ByVal pSERIE As String, ByVal pNUMERO As String, _
                    ByVal pValCOMPRA As Double, ByVal pValIGV As Double, ByVal pValTOTAL As Double, _
                    ByVal pFECH_EMI As Date, ByVal pFECH_VEN As Date, ByVal pCodORDEN As String, _
                    ByVal pNumORDEN As String, ByVal pCODGESTION As String, _
                    ByVal BOL_EDIT As Boolean, ByVal EDIT_CODGESTION As String, ByVal CENCOSTO As String) As String

        Dim IntFila As Integer = 0
        Dim DBLcambioActual As Double = 0
        Dim TablaCab As String = String.Empty
        cCapaDatos = New clsCapaDatos

        If pCODGESTION = String.Empty Then pCODGESTION = GenCodigoGestion(pCODEMPRESA, pPERIODO, pMES)

        Try
            Dim StrCodGestion As String = String.Empty
            With cCapaDatos
                '&&&&&&&&&&&&&&&&&&&&&& ACTUALIZA TABLA Comun.Gestion_Com_Cab &&&&&&&&&&&&&&&&&&&&&&
                .sConectarSQL()
                .sComenzarTransaccion()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto2", CommandType.StoredProcedure)
                If BOL_EDIT = False Then
                    .sParametroSQL("@OPCION", DbType.Int32, 2, 2)
                    StrCodGestion = pCODGESTION
                ElseIf BOL_EDIT = True Then
                    .sParametroSQL("@OPCION", DbType.Int32, 2, 6)
                    StrCodGestion = EDIT_CODGESTION
                End If
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, StrCodGestion)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 2, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                .sParametroSQL("@MES", DbType.String, 2, pMES)
                .sParametroSQL("@MONEDA", DbType.String, 2, CodigoMoneda(pMONEDA))
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, pCodORDEN)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, pNumORDEN)
                .sParametroSQL("@SECUENCIA", DbType.String, 5, String.Empty)
                .sParametroSQL("@CUENTACODIGO", DbType.String, 20, String.Empty)
                .sParametroSQL("@TIPOANALITICOCODIGO", DbType.String, 2, String.Empty)
                .sParametroSQL("@ANALITICOCODIGO", DbType.String, 11, String.Empty)
                .sParametroSQL("@CENTROCOSTOCODIGO", DbType.String, 10, CENCOSTO)
                .sParametroSQL("@MONTO_D", DbType.Decimal, 22, 0)
                .sParametroSQL("@MONTO_H", DbType.Decimal, 22, 0)
                .sParametroSQL("@TIPO_DOC", DbType.String, 15, Trim(AbrevDocumento(pDOC)))
                .sParametroSQL("@SERIE_DOC", DbType.String, 15, pSERIE)
                .sParametroSQL("@NUMERO_DOC", DbType.String, 30, pNUMERO)
                .sParametroSQL("@FECHA_DOC", DbType.Date, 15, pFECH_EMI)
                .sParametroSQL("@FECHA_CADUCIDAD", DbType.Date, 15, pFECH_VEN)
                .sParametroSQL("@CODIGO_AREA", DbType.String, 5, String.Empty)
                .sParametroSQL("@ANEXREF", DbType.String, 11, String.Empty)
                .sParametroSQL("@GLOSADET", DbType.String, 200, String.Empty)

                .sParametroSQL("@VALCOMPRA", DbType.Double, 22, pValCOMPRA)
                .sParametroSQL("@VALIGV", DbType.Double, 22, pValIGV)
                .sParametroSQL("@VALTOTAL", DbType.Double, 22, pValTOTAL)
                .sParametroSQL("@UPDUSER", DbType.String, 20, pUSER)
                .sParametroSQL("@UPDPC", DbType.String, 11, pCOMPUTER)
                .sParametroSQL("@UPDFECHA", DbType.Date, 11, Format(Today, "dd/MM/yyyy"))
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 11, pFECHAVOUCHER)
                .sParametroSQL("@MONTOTC", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 11, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .fCmdExecuteNonQuery()
                .sConfirmarTransaccion()
            End With

            Return StrCodGestion

        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            cCapaDatos.sCancelarTransaccion()
            Return String.Empty
        End Try

    End Function


    Public Function InsertCabecera_SinOC(ByVal pCODEMPRESA As String, ByVal pMES As String, ByVal pCODMONEDA As String, _
                    ByVal pPERIODO As String, ByVal pFECHAVOUCHER As Date, ByVal pCODIGOFACT As String, ByVal pSERIEFACT As String, _
                    ByVal pNUMEROFACT As String, ByVal pFECHAEMI As Date, ByVal pFECHAVEN As Date, ByVal pVALCOMPRA As Double, _
                    ByVal pMONTOIGV As Double, ByVal pIMPORTETOTAL As Double, ByVal pCODANEXO As String, ByVal pGLOSA As String, _
                    ByVal pTASAIGV As Double, ByVal pFLAGIGV As String, ByVal pFLAGDETRAC As String, ByVal pFLAGPERCEP As String, _
                    ByVal pUSER As String, ByVal pPC As String, ByVal pFECHACREACION As String, ByVal ID_REGISTRO As String, _
                    ByVal pCodOrden As String, ByVal pNumOrden As String) As Boolean

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComenzarTransaccion()

                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 9)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                .sParametroSQL("@MES", DbType.String, 2, pMES)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, pCODMONEDA)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, pCodOrden)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, pNumOrden)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, pFECHAEMI)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, pFECHAVOUCHER)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, pIMPORTETOTAL)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, pCODIGOFACT)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, pSERIEFACT)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, pNUMEROFACT)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, pCODANEXO)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, pGLOSA)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, pFLAGIGV)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, pTASAIGV)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, pMONTOIGV)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, pVALCOMPRA)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, pFLAGDETRAC)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, pFLAGPERCEP)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, ID_REGISTRO)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, pFECHAVEN)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, pUSER)
                .sParametroSQL("@PCCREACION", DbType.String, 30, pPC)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, pFECHACREACION)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .fCmdExecuteNonQuery()
                .sConfirmarTransaccion()
            End With
            Return True
        Catch ex As Exception
            cTempo.sMensaje("Error al Insertar los parametros para el Asiento de Compras sin O/C")
            cCapaDatos.sCancelarTransaccion()
            Return False
        End Try
    End Function

    Public Function CenCostoOrigen_OC_varias(ByVal pCODEMPRESA As String, ByVal pPERIODO As String, _
                                            ByVal Codigo_OC As String, ByVal Numero_OC As String) As String
        Dim sCenCosto As String
        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 12)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, Codigo_OC)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, Numero_OC)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                sCenCosto = .fCmdExecuteScalar()
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            sCenCosto = String.Empty
        End Try

        Return sCenCosto
    End Function

    Public Function TempDetalleOC(ByVal CODEMPRESA As String, ByVal PERIODO As String, ByVal CODIGO_OC As String, _
                                    ByVal NUMERO_OC As String) As DataTable
        cCapaDatos = New clsCapaDatos
        Dim DTabla As New DataTable

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 13)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, CODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, PERIODO)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, CODIGO_OC)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, NUMERO_OC)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .sAdaptadorSQL("R")
                DTabla = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
        End Try
        Return DTabla
    End Function

    Public Function Listar_Ordenes(ByVal CodEmpresa As String, ByVal CodProveedor As String, ByVal NroOrden As String) As DataTable
        cCapaDatos = New clsCapaDatos
        Dim DTabla As New DataTable

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 14)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, CodEmpresa)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, NroOrden)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, CodProveedor)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .sAdaptadorSQL("R")
                DTabla = .fListarTabla
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
        End Try
        Return DTabla
    End Function

    Public Function Saldo_OC(ByVal pCodEmpresa As String, ByVal pOC_Codigo As String, ByVal pOC_Numero As String) As Double
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 15)
            cCapaDatos.sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
            cCapaDatos.sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@OCOCODIGO", DbType.String, 10, pOC_Codigo)
            cCapaDatos.sParametroSQL("@OCONUMERO", DbType.String, 15, pOC_Numero)
            cCapaDatos.sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHATC", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
            cCapaDatos.sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

            cCapaDatos.sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

            cCapaDatos.sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

            Return cCapaDatos.fCmdExecuteScalar

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Function UPDATE_SALDO_OC(ByVal pCodEmpresa As String, ByVal pOC_Codigo As String, ByVal pOC_Numero As String, _
                                    ByVal pImporte_Total As Double) As Boolean
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 16)
            cCapaDatos.sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
            cCapaDatos.sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@OCOCODIGO", DbType.String, 10, pOC_Codigo)
            cCapaDatos.sParametroSQL("@OCONUMERO", DbType.String, 15, pOC_Numero)
            cCapaDatos.sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, pImporte_Total)
            cCapaDatos.sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHATC", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
            cCapaDatos.sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

            cCapaDatos.sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

            cCapaDatos.sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

            cCapaDatos.fCmdExecuteNonQuery()
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Public Function UPDATE_COD_TESO(ByVal pCodGestion As String, ByVal pCodEmpresa As String, ByVal pPeriodo As String, _
                                    ByVal pMes As String, ByVal pMoneda As String, ByVal pCod_Tesoreria As String) As Boolean
        cCapaDatos = New clsCapaDatos

        Try
            sbCadena.Length = 0
            sbCadena.Append("UPDATE Comun.Gestion_Com_Cab02 ")
            sbCadena.Append("SET IdRegistro_Teso='" & pCod_Tesoreria & "' ")
            sbCadena.Append("WHERE CodigoGestion='" & pCodGestion & "' AND EmprCodigo='" & pCodEmpresa & "' ")
            'sbCadena.Append("AND Periodo='" & pPeriodo & "' AND Mes='" & pMes & "' AND Moneda='" & pMoneda & "'")
            sbCadena.Append("AND Periodo='" & pPeriodo & "' AND Moneda='" & pMoneda & "'")


            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL(sbCadena.ToString, CommandType.Text)
                .fCmdExecuteNonQuery()
            End With
            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try
    End Function

    Public Function CODIGO_GESTION(ByVal pCodEmpresa As String, ByVal pCodigoFact As String, ByVal pSerieFact As String, _
                                    ByVal pNumeroFact As String, ByVal pIdRegistro As String) As SqlDataReader
        cCapaDatos = New clsCapaDatos
        Dim dReader As SqlDataReader

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 17)
            cCapaDatos.sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
            cCapaDatos.sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
            cCapaDatos.sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHATC", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FACTCODIGO", DbType.String, 3, pCodigoFact)
            cCapaDatos.sParametroSQL("@FACTSERIE", DbType.String, 20, pSerieFact)
            cCapaDatos.sParametroSQL("@FACTNUMERO", DbType.String, 30, pNumeroFact)
            cCapaDatos.sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

            cCapaDatos.sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, pIdRegistro) 'Cod Registro Documentos
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

            cCapaDatos.sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

            dReader = cCapaDatos.fCmdExecuteReader
        Catch ex As Exception
            MsgBox(ex.Message)
            dReader = Nothing
            dReader.Close()
        End Try
        Return dReader
    End Function

    Public Function DatosFactura_Teso(ByVal pCodEmpresa As String, ByVal pIdRegistro As String) As SqlDataReader
        cCapaDatos = New clsCapaDatos
        Dim dReader As SqlDataReader

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 18)
            cCapaDatos.sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
            cCapaDatos.sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
            cCapaDatos.sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHATC", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
            cCapaDatos.sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

            cCapaDatos.sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, pIdRegistro) 'Cod Registro Documentos
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

            cCapaDatos.sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

            dReader = cCapaDatos.fCmdExecuteReader
        Catch ex As Exception
            MsgBox(ex.Message)
            dReader = Nothing
            dReader.Close()
        End Try
        Return dReader
    End Function

    Public Function UPDATECabecera_SinOC(ByVal pSTRCODGESTION As String, ByVal pCODEMPRESA As String, ByVal pPERIODO As String, _
                            ByVal pMES As String, ByVal pMONEDA As String, ByVal pFECHAVOUCHER As Date, ByVal pFACTCODIGO As String, _
                            ByVal pFACTSERIE As String, ByVal pFACTNUMERO As String, ByVal pFECH_EMI As Date, ByVal pFECH_VEN As Date, _
                            ByVal pVALCOMPRA As Double, ByVal PMONTOIGV As Double, ByVal pIMPORTETOTAL As Double, _
                            ByVal pCODANEXO As String, ByVal pFLAGIGV As String, ByVal pTASAIGV As Double, _
                            ByVal pFLAGDETRAC As Integer, ByVal pFLAGPERCEP As Integer, ByVal pUSER As String, ByVal pPC As String, _
                            ByVal pFECHAREG As Date, ByVal sCODIGOACTUAL As String) As Boolean
        Try
            cCapaDatos = New clsCapaDatos
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto3", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 3)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, pSTRCODGESTION)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 2, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                .sParametroSQL("@MES", DbType.String, 2, pMES)
                .sParametroSQL("@MONEDA", DbType.String, 2, pMONEDA)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCOSERIE", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, pFECHAVOUCHER)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Double, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Double, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 50, pFACTCODIGO)
                .sParametroSQL("@FACTSERIE", DbType.String, 50, pFACTSERIE)
                .sParametroSQL("@FACTNUMERO", DbType.String, 50, pFACTNUMERO)
                .sParametroSQL("@FECHA_EMI", DbType.Date, 15, pFECH_EMI)
                .sParametroSQL("@FECHA_VEN", DbType.Date, 15, pFECH_VEN)
                .sParametroSQL("@VALCOMPRA_TESO", DbType.Double, 22, pVALCOMPRA)
                .sParametroSQL("@MONTOIGV_TESO", DbType.Double, 22, PMONTOIGV)
                .sParametroSQL("@TOTAL_TESO", DbType.Double, 22, pIMPORTETOTAL)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, pCODANEXO)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 250, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, pFLAGIGV)
                .sParametroSQL("@TASA_IGV", DbType.Double, 22, pTASAIGV)
                .sParametroSQL("@IMPORTE_IGV", DbType.Double, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Double, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.Int32, 1, pFLAGDETRAC)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.Int32, 1, pFLAGPERCEP)

                .sParametroSQL("@USUCREACION", DbType.String, 20, pUSER)
                .sParametroSQL("@PCCREACION", DbType.String, 30, pPC)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, pFECHAREG)
                .sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .fCmdExecuteNonQuery()

            End With
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Delete_Registro(ByVal IDREGISTRO As String, ByVal pCODEMPRESA As String, ByVal pPERIODO As String) As Boolean
        Try
            cCapaDatos = New clsCapaDatos
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto3", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 5)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 2, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@MONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCOSERIE", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Double, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Double, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 50, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 50, String.Empty)
                .sParametroSQL("@FECHA_EMI", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@FECHA_VEN", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@VALCOMPRA_TESO", DbType.Double, 22, 0)
                .sParametroSQL("@MONTOIGV_TESO", DbType.Double, 22, 0)
                .sParametroSQL("@TOTAL_TESO", DbType.Double, 22, 0)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 250, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Double, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Double, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Double, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.Int32, 1, 0)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.Int32, 1, 0)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, IDREGISTRO)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .fCmdExecuteNonQuery()

            End With
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Devuelve_MesOC(ByVal pCODGESTION As String, ByVal pCODEMPRESA As String, ByVal pCODORDEN As String, ByVal pNUMORDEN As String) As String
        Try
            cCapaDatos = New clsCapaDatos
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto3", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 6)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, pCODGESTION)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 2, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@MONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, pCODORDEN)
                .sParametroSQL("@OCOSERIE", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, pNUMORDEN)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Double, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Double, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 50, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 50, String.Empty)
                .sParametroSQL("@FECHA_EMI", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@FECHA_VEN", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@VALCOMPRA_TESO", DbType.Double, 22, 0)
                .sParametroSQL("@MONTOIGV_TESO", DbType.Double, 22, 0)
                .sParametroSQL("@TOTAL_TESO", DbType.Double, 22, 0)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 250, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Double, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Double, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Double, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.Int32, 1, 0)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.Int32, 1, 0)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, System.DBNull.Value)
                .sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .fCmdExecuteNonQuery()

            End With
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function OC_Pasadas(ByVal pCodEmpresa As String, ByVal pProveedor As String, ByVal pOC_Codigo As String, ByVal pOC_Numero As String) As SqlDataReader
        cCapaDatos = New clsCapaDatos
        Dim dReader As SqlDataReader

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 2, 19)
            cCapaDatos.sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
            cCapaDatos.sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
            cCapaDatos.sParametroSQL("@OCOCODIGO", DbType.String, 10, pOC_Codigo)
            cCapaDatos.sParametroSQL("@OCONUMERO", DbType.String, 15, pOC_Numero)
            cCapaDatos.sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHATC", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
            cCapaDatos.sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
            cCapaDatos.sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@PRVCODIGO", DbType.String, 11, pProveedor)
            cCapaDatos.sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
            cCapaDatos.sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

            cCapaDatos.sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
            cCapaDatos.sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
            cCapaDatos.sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
            cCapaDatos.sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

            cCapaDatos.sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
            cCapaDatos.sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

            cCapaDatos.sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
            cCapaDatos.sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

            dReader = cCapaDatos.fCmdExecuteReader
        Catch ex As Exception
            MsgBox(ex.Message)
            dReader = Nothing
            dReader.Close()
        End Try
        Return dReader
    End Function


    Public Function Datos_OC_LOGIST(ByVal pCodEmpresa As String, ByVal pProveedor As String, ByVal pOC_Codigo As String, ByVal pOC_Numero As String) As DataSet

        cCapaDatos = New clsCapaDatos
        Dim Dset As New DataSet

        Try
            With cCapaDatos
                .sConectarSQL()
                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 20)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCodEmpresa)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, pOC_Codigo)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, pOC_Numero)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, String.Empty)
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, pProveedor)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .sAdaptadorSQL("R")
                Dset = .fListarSet
                Call Dset_NombreTablas(Dset)
            End With
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
        End Try
        Return Dset
    End Function

    Public Function ExistePlanCuentas(ByVal pCODEMPRESA As String) As Integer
        Dim iContPlanCuenta As Integer = 0
        Try
            With cCapaDatos
                .sConectarSQL()

                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 21)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, String.Empty)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, String.Empty)
                .sParametroSQL("@MES", DbType.String, 2, String.Empty)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, String.Empty)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, Today)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, 0)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, String.Empty)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, String.Empty)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, String.Empty)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                iContPlanCuenta = .fCmdExecuteScalar()

            End With
            Return iContPlanCuenta
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            Return 0
        End Try
    End Function

    Public Function fInsertarDetalleTemp(ByVal pCODEMPRESA As String, ByVal pCODGESTION As String, ByVal pPERIODO As String, ByVal pMES As String, _
                    ByVal pMONEDA As String, ByVal pOCOCODIGO As String, ByVal pOCONUMERO As String, ByVal pITEM As String, ByVal pCODMATERIAL As String, _
                    ByVal pCUENTA As String, ByVal pIMPORTE As Decimal, ByVal pFECHA_OC As Date, ByVal pUSUARIO As String) As Integer
        Dim iContPlanCuenta As Integer = 0
        Try
            With cCapaDatos
                .sConectarSQL()

                .sComandoSQL("Contabilidad.GenerarAsientosComprasAuto", CommandType.StoredProcedure)
                .sParametroSQL("@OPCION", DbType.Int32, 2, 22)
                .sParametroSQL("@CODIGOGESTION", DbType.String, 13, pCODGESTION)
                .sParametroSQL("@EMPRCODIGO", DbType.String, 3, pCODEMPRESA)
                .sParametroSQL("@PERIODO", DbType.String, 4, pPERIODO)
                .sParametroSQL("@MES", DbType.String, 2, pMES)
                .sParametroSQL("@CODMONEDA", DbType.String, 2, pMONEDA)
                .sParametroSQL("@ABRVMONEDA", DbType.String, 2, String.Empty)
                .sParametroSQL("@OCOCODIGO", DbType.String, 10, pOCOCODIGO)
                .sParametroSQL("@OCONUMERO", DbType.String, 15, pOCONUMERO)
                .sParametroSQL("@OCOFECHAEMISION", DbType.Date, 15, pFECHA_OC)
                .sParametroSQL("@FECHABASEVOUCHER", DbType.Date, 15, Today)
                .sParametroSQL("@TOTAL_IMPORTE", DbType.Decimal, 22, 0)
                .sParametroSQL("@TCAMBIO", DbType.Decimal, 22, 0)
                .sParametroSQL("@FECHATC", DbType.Date, 15, Today)
                .sParametroSQL("@TIPO", DbType.String, 1, String.Empty)
                .sParametroSQL("@FACTCODIGO", DbType.String, 3, String.Empty)
                .sParametroSQL("@FACTSERIE", DbType.String, 20, String.Empty)
                .sParametroSQL("@FACTNUMERO", DbType.String, 30, String.Empty)
                .sParametroSQL("@NACIONAL", DbType.Int32, 1, 0)
                .sParametroSQL("@CCOSCODIGO", DbType.String, 10, String.Empty)
                .sParametroSQL("@COD_TIPOANEXO", DbType.String, 1, "P")
                .sParametroSQL("@PRVCODIGO", DbType.String, 50, String.Empty)
                .sParametroSQL("@OCOPARAQUE", DbType.String, 200, String.Empty)
                .sParametroSQL("@FLAG_IGV", DbType.String, 1, String.Empty)
                .sParametroSQL("@TASA_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@IMPORTE_IGV", DbType.Decimal, 22, 0)
                .sParametroSQL("@SUBTOTAL", DbType.Decimal, 22, 0)
                .sParametroSQL("@FLAG_DETRACCION", DbType.String, 1, String.Empty)
                .sParametroSQL("@FLAG_PERCEPCION", DbType.String, 1, String.Empty)

                .sParametroSQL("@NROITEM", DbType.Int32, 2, pITEM)
                .sParametroSQL("@CODIGO_MATERIAL", DbType.String, 50, pCODMATERIAL)
                .sParametroSQL("@CODIGO_CTACONTAB", DbType.String, 20, pCUENTA)
                .sParametroSQL("@IMPORTE", DbType.Decimal, 22, pIMPORTE)
                .sParametroSQL("@FECHAENTREGA", DbType.Date, 15, Today)
                .sParametroSQL("@DRQAGREGADO", DbType.String, 200, String.Empty)

                .sParametroSQL("@USUCREACION", DbType.String, 20, pUSUARIO)
                .sParametroSQL("@PCCREACION", DbType.String, 30, String.Empty)
                .sParametroSQL("@FECHCREACION", DbType.Date, 15, Today)

                .sParametroSQL("@NUMERO_VOUCHER", DbType.String, 20, String.Empty)
                .sParametroSQL("@CODIGOSUBDIARIO", DbType.String, 2, String.Empty)

                .fCmdExecuteNonQuery()
            End With
            Return 1
        Catch ex As Exception
            cTempo.sMensaje(ex.Message)
            Return 0
        End Try
    End Function

    'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


    Public Function fListarFacturas(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal CodProveedor As String, ByVal NroDoc As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, CodProveedor)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, NroDoc)
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fActualizarCamposFactura(ByVal iOpcion As Integer, ByVal IdRegistro As String, ByVal EmprCodigo As String, ByVal NCGlosa As String, ByVal NCImporte As Double) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaBase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, "")
            'cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, Trim(Descripcion))
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, Trim(IdRegistro))
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, Trim(EmprCodigo))
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, NCGlosa)
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, NCImporte)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fListarOrigenCaja(ByVal iOpcion As Integer, ByVal CodEmpresa As String, ByVal IdRegistro As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRegistroDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@TipoAcreedorId", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodProveedor", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroSerie", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Fechabase", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Dias", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaVencimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ImporteIgv", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TasaIGV", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ImporteTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorDeclarado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ValorCalculado", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Inafecto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Situacion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FechaCreacion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaRecepcion", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, IdRegistro)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@EstadoPrePro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoPro", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ValorCompra", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@DP", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@PorcDP", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdOC", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@Archivado", DbType.Int16, 1, 0)

            cCapaDatos.sParametroSQL("@NCCodFT", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NCCodFTEmpr", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NCGlosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@NCImporte", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@TIngreso", DbType.String, 4, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
