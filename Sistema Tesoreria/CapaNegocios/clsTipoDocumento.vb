Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsTipoDocumento

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String

    Public iNroRegistros As Integer = 0

    Public Function fListar() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdTipoDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigo() As String '--->
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@IdTipoDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal vOpcion As Integer, ByVal vCodRegistro As String, ByVal vDescripcion As String, ByVal vAbrDoc As String, ByVal Estado As Integer) As Integer
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdTipoDocumento", DbType.String, 2, vCodRegistro)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 30, vDescripcion)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, vAbrDoc)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fEliminar(ByVal vIdCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@IdTipoDocumento", DbType.String, 2, vIdCodigo)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fBuscaDocumentos(ByVal iOpcion As Integer, ByVal Desc As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdTipoDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 20, Desc)
            cCapaDatos.sParametroSQL("@AbrDoc", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


End Class
