﻿Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos
Public Class NegOtros
    Private vClsOtros As New ClsOtros
    Public Function fListarNroreferenciaAlmacenIngreso(ByVal vOcoCodigo As String, ByVal vNroDocReferencia As String) As DataTable
        Return vClsOtros.fListarNroreferenciaAlmacenIngreso(vOcoCodigo, vNroDocReferencia)
    End Function
    Public Function fObtenerIgv() As String
        Return vClsOtros.fObtenerIgv
    End Function
End Class
