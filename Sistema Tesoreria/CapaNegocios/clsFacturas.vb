Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsFacturas
    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable

    Private sTextoSQL As String
    Public sCodActual As String
    Public sPrefijo As String
    Public sSufijo As String
    Public sCodFuturo As String
    Public sCodFuturoCabCargo As String
    Public sCodFuturoIndexDetCargo As String
    Public sCodFuturoNumeroCargo As String
    Public sCodFuturoDetAnexoCargo As String
    Public iNroRegistros As Integer = "0"

    Public Function fBuscarDocumentos(ByVal iOpcion As Integer, ByVal EmprCodigo As String, ByVal CriterioBusqueda As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, CriterioBusqueda)
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function

    Public Function fBuscarDocumentos2(ByVal iOpcion As Integer, ByVal EmprCodigo As String, ByVal CriterioBusqueda As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspBusquedaDocPagadosEntregados"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, CriterioBusqueda)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function

    Public Function fListarOrdenes(ByVal iOpcion As Integer, ByVal EmprCodigo As String, ByVal NroOrden As String, ByVal Ruc As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Trim(Ruc))
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, Trim(NroOrden))
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function


    'Public Function fBuscarDocumentosPagadosEntregados(ByVal iOpcion As Integer, ByVal EmprCodigo As String, ByVal CriterioBusqueda As String) As DataTable

    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Logistica.uspCargosDeDocumentos"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
    '        cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, Trim(CriterioBusqueda))
    '        cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
    '        cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

    '        cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
    '        cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
    '        cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

    '        cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
    '        cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
    '        cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable

    'End Function


    Public Function fBuscarDocumentosPagadosEntregados(ByVal iOpcion As Integer, ByVal EmprCodigo As String, ByVal CriterioBusqueda As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, CriterioBusqueda)
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarProveedoresxRuc(ByVal EmprCodigo As String, ByVal TipoProv As String, ByVal Ruc As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 33)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, TipoProv)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function


    Public Function fCargarCCosto(ByVal vCodigoEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarTipoDoc() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 27)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fExisteNumSemanCargo(ByVal vCodigoEmpresa As String, ByVal NumeroSemana As Integer, ByVal Periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, NumeroSemana)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function GenerarCodCabeceraCargo(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoCabCargo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function GenerarIndexDetCargo(ByVal CodEmpresa As String, ByVal IdCargoLogistica As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 40)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargoLogistica)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            'sCodFuturoCabCargo
            sCodFuturoIndexDetCargo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function GenerarNumeroCabeceraCargo(ByVal CodEmpresa As String, ByVal Periodo As String, ByVal Mes As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 21)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iCodigo = Trim(Right(Convert.ToString(cCapaDatos.fCmdExecuteScalar()), 5))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoNumeroCargo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function


    Public Function GrabarCabeceraCargo(ByVal IdCargo As String, ByVal CodEmpresa As String, ByVal NroSemana As Integer, ByVal Periodo As String, ByVal IdGrupo As String, ByVal Numero As String, ByVal Glosa As String, ByVal Mes As String, ByVal Fecha As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, NroSemana)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, IdGrupo)
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, Numero)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, Glosa)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function ActualizarCabeceraCargo(ByVal IdCargo As String, ByVal CodEmpresa As String, ByVal NroSemana As Integer, ByVal Glosa As String, ByVal Mes As String, ByVal Fecha As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 25)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, NroSemana)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, Glosa)
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, Mes)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function ActualizarDocumentos(ByVal CodEmpresa As String, ByVal IdRegistroDoc As String, ByVal IdCargo As String, ByVal CCosCodigo As String, ByVal CodIndexDetCargo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, IdRegistroDoc)

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, Trim(CodIndexDetCargo))
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function ActualizarDocumentos2(ByVal CodEmpresa As String, ByVal IdRegistroDoc As String, ByVal NroOrden As String, ByVal IdOrden As String, ByVal CCosCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 43)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdOrden)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, IdRegistroDoc)

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, NroOrden)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function AcOrdenActivo(ByVal CodEmpresa As String, ByVal IdOrden As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 44)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, Trim(IdOrden))
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function EliminarItemDetAnex(ByVal CodEmpresa As String, ByVal IdDetAnexoCargo As String, ByVal IdCargo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, IdDetAnexoCargo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function GrabarDetAnexo(ByVal Opcion As Integer, ByVal IdCargo As String, ByVal IdDetAnexo As String, ByVal CodEmpresa As String, ByVal Ruc As String, ByVal Razon As String, ByVal IdDoc As String, ByVal NroDoc As String, ByVal FechaDocumento As DateTime, ByVal IdMoneda As String, ByVal Importe As Decimal, ByVal NroOC As String, ByVal CCosCodigo As String, ByVal Estado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, Opcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, IdDetAnexo)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, Razon)
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, IdDoc)
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, NroDoc)
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaDocumento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, IdMoneda)
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, Importe)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, NroOC)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fListarSemanas(ByVal CodEmpresa As String, ByVal Periodo As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function

    Public Function fLisDetCabeceraCrono(ByVal EmprCodigo As String, ByVal IdCargo As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function


    Public Function fLisDetAnexoCargo(ByVal EmprCodigo As String, ByVal IdCargo As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function


    Public Function fLisDetCabeceraCronoReport(ByVal EmprCodigo As String, ByVal IdCargo As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 24)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function


    Public Function fLisDetAnexoCronoReport(ByVal EmprCodigo As String, ByVal IdCargo As String) As DataTable

        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 35)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable

    End Function


    Public Function fListarGrupoCCdeCargos(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCC(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fActualizarGrupoCCCargos(ByVal vOpcion As Integer, ByVal IdTipoGasto As String, ByVal EmprCodigo As String, ByVal CodGrupoCCCArgo As String, ByVal Column As String) As Integer
        'Dim iResultadoDetalle As Int32 = 0
        Dim iResultadoCabecera As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Trim(IdTipoGasto))
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, Trim(CodGrupoCCCArgo))
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultadoCabecera = cCapaDatos.fCmdExecuteNonQuery()
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultadoCabecera
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 14)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, Descripcion)
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))
            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCodigo(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "0000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal vOpcion As Integer, ByVal vGruCodigo As String, ByVal Descripcion As String, ByVal CodEmpresa As String, ByVal Estado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, Descripcion)
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, vGruCodigo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion
        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fListarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 18)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarGrupos(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarRucEmpresa(ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 34)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fTraerGrupoCC(ByVal CodEmpresa As String, ByVal CodCentroC As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 20)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, CodCentroC)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fExisteNumCargo(ByVal vCodigoEmpresa As String, ByVal NumeroCargo As String, ByVal Periodo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, NumeroCargo)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fFiltrarNumCargo(ByVal iOpcion As Integer, ByVal vCodigoEmpresa As String, ByVal NumeroCargo As String, ByVal Periodo As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vCodigoEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, NumeroCargo)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fActualizarCargosYDocumentos(ByVal CodigoCargo As String, ByVal CodEmpresa As String, ByVal RecibirODevolver As Integer, ByVal FechaRecibo As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 51)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, CodigoCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, RecibirODevolver)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(FechaRecibo, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion
        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCargosPorMesyGrupo(ByVal CodEmpresa As String, ByVal Periodo As String, ByVal IdGrupo As String, ByVal IdMes As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, IdGrupo)
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, IdMes)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function GenerarCodDetAnexCargo(ByVal CodEmpresa As String, ByVal IdCargo As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspCargosDeDocumentos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 29)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@CriterioBusqueda", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@NumeroSemana", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@IdCargo", DbType.String, 20, IdCargo)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@IdRegistroDoc", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdGrupo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 9, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@Mes", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@IdDetAnexo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NombreProveedor", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@IdDocumento", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NroDocumento", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@FechaDocumento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Importe", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NroOC", DbType.String, 15, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoDetAnexoCargo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

End Class
