Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsBanco

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer, ByVal CodEmpr As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmailBanco", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function



    Public Function fBuscarCod(ByVal vCod As String, ByVal CodEmpr As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vCod)
            cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmailBanco", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdBanco As String, ByVal vNombreBanco As String, ByVal vAbreviado As String, ByVal vRucBanco As String, ByVal vDireccionBanco As String, ByVal vTelefonoBanco As String, ByVal vFaxBanco As String, ByVal vEmailBanco As String, ByVal vWebBanco As String, ByVal vEstado As Integer, ByVal vOpcion As Integer, ByVal vEmprCodigo As String, ByVal vTipoAnaliticoCodigo As String, ByVal vCodMoneda As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, vEstado)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vIdBanco)
            cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, vNombreBanco)
            cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, vAbreviado)
            cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, vRucBanco)
            cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, vDireccionBanco)
            cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, vTelefonoBanco)
            cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, vFaxBanco)
            cCapaDatos.sParametroSQL("@EMailBanco", DbType.String, 80, vEmailBanco)
            cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, vWebBanco)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, vTipoAnaliticoCodigo)
            cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, vCodMoneda)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, Codigo)
            cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EMailBanco", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCodigo(ByVal CodEmpr As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EMailBanco", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, Codigo)
            cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, Descripcion)
            cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EMailBanco", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))
            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fBuscarBancos(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal Ruc As String, ByVal Descri As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBuscarBancos"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 25, Descri)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
