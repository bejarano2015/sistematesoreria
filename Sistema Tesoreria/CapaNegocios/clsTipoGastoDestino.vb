Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsTipoGastoDestino

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

    Public Function fCargarTipoGastoDestino() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IDTIPOGASTODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDTIPOGASTO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDCUENTACONTABLE", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarPlanCuentas() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@IDTIPOGASTODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDTIPOGASTO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDCUENTACONTABLE", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdTipoGastoDestino As Decimal, ByVal vIdEmpresa As String, ByVal vIdTipoGasto As Decimal, ByVal vIdTipoDestino As Decimal, ByVal vIdCuentaContable As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDTIPOGASTODESTINO", DbType.Decimal, 18, vIdTipoGastoDestino)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@IDTIPOGASTO", DbType.Decimal, 18, vIdTipoGasto)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, vIdTipoDestino)
            cCapaDatos.sParametroSQL("@IDCUENTACONTABLE", DbType.String, 50, vIdCuentaContable)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fEliminar(ByVal vIdEmpresa As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDTIPOGASTODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@IDTIPOGASTO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDCUENTACONTABLE", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fCargarTipoGastoDestinoCombo(ByVal vIdEmpresa As String) ', ByVal vIdTipoDestino As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoGastoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 5)
            cCapaDatos.sParametroSQL("@IDTIPOGASTODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@IDTIPOGASTO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDCUENTACONTABLE", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class

