Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsCajaExtraordinaria

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListarArea(ByVal vCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, vCodEmpresa)
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NomResp", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprEmple", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarListarEmpleados(ByVal vCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, vCodEmpresa)
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NomResp", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprEmple", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarListarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NomResp", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprEmple", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer, ByVal vCodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, vCodEmpresa)
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NomResp", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprEmple", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdArea As String, ByVal vIdCaja As String, ByVal vDescripcionCaja As String, ByVal vMontoMinimo As Decimal, ByVal vMontoMaximo As Decimal, ByVal vEstado As Integer, ByVal vOpcion As Integer, ByVal vCodEmpresa As String, ByVal vIdEmpleado As String, ByVal vIdMoneda As String, ByVal TPla As String, ByVal Nombres As String, ByVal EmprEmple As String, ByVal UsuCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, vEstado)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, vIdArea)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, vIdCaja)
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, vDescripcionCaja)
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, vMontoMinimo)
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, vMontoMaximo)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, vCodEmpresa)
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, vIdEmpleado)
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, TPla)
            cCapaDatos.sParametroSQL("@NomResp", DbType.String, 150, Nombres)
            cCapaDatos.sParametroSQL("@EmprEmple", DbType.String, 2, EmprEmple)
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, vIdMoneda)
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 10, UsuCodigo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer, ByVal CodEmpr As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, CodEmpr)
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NomResp", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprEmple", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 10, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fCodigo(ByVal EmprCod As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspCajaExtraordinaria"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdArea", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DescripcionCaja", DbType.String, 80, "")
            cCapaDatos.sParametroSQL("@MontoMinimo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoMaximo", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, 0)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, EmprCod)
            cCapaDatos.sParametroSQL("@IdResponsable", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@TPlaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@NomResp", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@EmprEmple", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdMoneda", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@UsuCodigo", DbType.String, 10, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fBuscarCajas(ByVal iOpcion As Integer, ByVal vCodEmpresa As String, ByVal CodArea As String, ByVal Descripcion As String, ByVal Estado As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspBuscarCajas"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@EmprCod", DbType.String, 2, vCodEmpresa)
            cCapaDatos.sParametroSQL("@CodArea", DbType.String, 2, CodArea)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 80, Descripcion)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 20, Estado)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function Mostrar_ImprimirCajaChica_() As DataTable
        Using cn As New SqlConnection("Data Source=192.168.1.200;Initial Catalog=ERPDHMONT;User Id=sa; Password=!sharingan10;") ' preguntar a jesus por esta cadena
            cn.Open()
            Using cmd As New SqlCommand("[Tesoreria].[uspImprimirCajaChica_]", cn)
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cn
                Using da As New SqlDataAdapter(cmd)
                    Dim dt As New DataTable
                    da.Fill(dt)
                    Return dt
                End Using
            End Using

        End Using
    End Function
End Class
