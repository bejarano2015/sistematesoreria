Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsDetraccion

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

#Region " Retenciones Cabecera"

    Public Function fCargarDetraccion(ByVal vIdEmpresa As String, ByVal vAnio As String, ByVal vMes As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@NROOPERACION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NROCTADETRACCION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, vAnio)
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, vMes)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCargarDocumentos(ByVal vCodProveedor As String, ByVal vIdEmpresa As String, ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@NROOPERACION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, vCodProveedor)
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NROCTADETRACCION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdDetraccionCab As Decimal, ByVal vIdEmpresa As String, ByVal vIdDocumentoOrigen As Decimal, ByVal vNroOperacion As String, ByVal vFecha As DateTime, ByVal vCodProveedor As String, ByVal vNombreProveedor As String, ByVal vRuc As String, ByVal vNroCtaDetraccion As String, ByVal vDireccion As String, ByVal vDescripcion As String, ByVal vTipoCambio As Decimal, ByVal vImporteDetraccion As Decimal, ByVal vImporteTotal As Decimal, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, vIdDetraccionCab)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, vIdEmpresa)
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, vIdDocumentoOrigen)
            cCapaDatos.sParametroSQL("@NROOPERACION", DbType.String, 20, vNroOperacion)
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, vFecha)
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, vCodProveedor)
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, vNombreProveedor)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, vRuc)
            cCapaDatos.sParametroSQL("@NROCTADETRACCION", DbType.String, 20, vNroCtaDetraccion)
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, vDireccion)
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, vDescripcion)
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, vTipoCambio)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, vImporteDetraccion)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, vImporteTotal)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            'iResultado = cCapaDatos.fCmdExecuteNonQuery()
            iResultado = cCapaDatos.fCmdExecuteScalar()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fEliminar(ByVal vIdDetraccionCab As Decimal, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, vIdDetraccionCab)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@NROOPERACION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NROCTADETRACCION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fCargarDocumentos(ByVal vOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@NROOPERACION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NROCTADETRACCION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

#End Region

#Region " Retención Detalle "

    Public Function fCargarDetraccionDetalle(ByVal vIdDetraccionCab As Decimal) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesDet"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IDDETRACCIONDET", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, vIdDetraccionCab)
            cCapaDatos.sParametroSQL("@IDREGISTRODOCUMENTO", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDDOCUMENTO", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, 0)
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 15, 0)
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IDMONEDA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IDTIPOSERVICIO", DbType.String, 5, 0)
            cCapaDatos.sParametroSQL("@PORCENTAJE", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@PORCENTAJEDETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTESOLES", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@ValorPorcentaje", DbType.Decimal, 18, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarDet(ByVal vIdDetraccionDet As Decimal, ByVal vIdDetraccionCab As String, ByVal vIdRegistroDocumento As String, ByVal vEmprCodigo As String, ByVal vIdDocumento As String, ByVal vSerie As String, ByVal vNumero As String, ByVal vFecha As DateTime, ByVal vIdMoneda As String, ByVal vIdTipoServicio As String, ByVal vPorcentaje As Decimal, ByVal vPorcentajeDetraccion As Decimal, ByVal vImporteDetraccion As Decimal, ByVal vImporteSoles As Decimal, ByVal vImporteTotal As Decimal, ByVal vValorPorcentaje As Decimal, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesDet"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDDETRACCIONDET", DbType.Decimal, 18, vIdDetraccionDet)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, vIdDetraccionCab)
            cCapaDatos.sParametroSQL("@IDREGISTRODOCUMENTO", DbType.String, 20, vIdRegistroDocumento)
            cCapaDatos.sParametroSQL("@EMPRCODIGO", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@IDDOCUMENTO", DbType.String, 2, vIdDocumento)
            cCapaDatos.sParametroSQL("@SERIE", DbType.String, 5, vSerie)
            cCapaDatos.sParametroSQL("@NUMERO", DbType.String, 15, vNumero)
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, vFecha)
            cCapaDatos.sParametroSQL("@IDMONEDA", DbType.String, 2, vIdMoneda)
            cCapaDatos.sParametroSQL("@IDTIPOSERVICIO", DbType.String, 5, vIdTipoServicio)
            cCapaDatos.sParametroSQL("@PORCENTAJE", DbType.Decimal, 20, vPorcentaje)
            cCapaDatos.sParametroSQL("@PORCENTAJEDETRACCION", DbType.Decimal, 20, vPorcentajeDetraccion)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, vImporteDetraccion)
            cCapaDatos.sParametroSQL("@IMPORTESOLES", DbType.Decimal, 20, vImporteSoles)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, vImporteTotal)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "1")
            cCapaDatos.sParametroSQL("@ValorPorcentaje", DbType.Decimal, 18, vValorPorcentaje)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fImporteTipoCambio(ByVal vFecha As Date, ByVal vOpcion As Integer) As Decimal
        Dim iResultado As Decimal = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetraccionesCab"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDDETRACCIONCAB", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@IDEMPRESA", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@ID_DOCUMENTOORIGEN", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@NROOPERACION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FECHA", DbType.DateTime, 10, vFecha)
            cCapaDatos.sParametroSQL("@CODPROVEEDOR", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NOMBREPROVEEDOR", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NROCTADETRACCION", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DIRECCION", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@DESCRIPCION", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@TIPOCAMBIO", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTEDETRACCION", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@IMPORTETOTAL", DbType.Decimal, 20, 2)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.String, 1, "0")
            cCapaDatos.sParametroSQL("@ANIO", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@MES", DbType.String, 10, "")

            iResultado = cCapaDatos.fCmdExecuteScalar()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

#End Region

End Class

