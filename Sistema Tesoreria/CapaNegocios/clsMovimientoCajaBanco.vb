Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos
Imports BusinessLogicLayer.Procesos
Imports CapaEntidad.Proceso
Imports CapaNegocios
Imports CapaEntidad

Public Class clsMovimientoCajaBanco

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public sCodFuturoRetencion As String
    Public sCodFuturoDetMov As String
    Public sCodFuturoVoucher As String
    Public sCodFuturoRendicion As String
    Public iNroRegistros As Integer = "0"
    Public iNroReg As Integer = "0"


    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'FECHA
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarDetalle(ByVal IdMovimiento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 26)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDetalle2(ByVal IdMovimiento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 34)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarPrimerDetalle(ByVal IdMovimiento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 32)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabarTransaccion(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdChequera As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String, ByVal NroFormaPago As String, ByVal Anulado As Integer, ByVal IdResumen As String, ByVal Ruc As String, ByVal NumeroSesion As Integer, ByVal AreaCodigo As String, ByVal IdCaja As String, ByVal IdCodigoCabLibro As String, ByVal IdCodigoDetLibro As String, ByVal Prestamo As String, ByVal EmprPrestamo As String, ByVal CCPrestamo As String, ByVal Estado As Integer, ByVal IdTransaccion As String, ByVal IdBancoDestino As String, ByVal IdCuentaDestino As String, ByVal Periodo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, NroFormaPago)
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, IdChequera)
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, IdRendicion)
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, Anulado)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, NumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, AreaCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, IdCodigoCabLibro)
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, IdCodigoDetLibro)

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, Prestamo)
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, EmprPrestamo)
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, CCPrestamo)

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, Estado)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, IdTransaccion)
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, IdBancoDestino)
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, IdCuentaDestino)
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fGrabar(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdChequera As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String, ByVal NroFormaPago As String, ByVal Anulado As Integer, ByVal IdResumen As String, ByVal Ruc As String, ByVal NumeroSesion As Integer, ByVal AreaCodigo As String, ByVal IdCaja As String, ByVal IdCodigoCabLibro As String, ByVal IdCodigoDetLibro As String, ByVal Prestamo As String, ByVal EmprPrestamo As String, ByVal CCPrestamo As String, ByVal Estado As Integer, ByVal IdTransaccion As String, ByVal IdBancoDestino As String, ByVal IdCuentaDestino As String, ByVal GlosaDestino As String, ByVal sCodigoLibroDestino As String, ByVal sCodigoDetDestino As String, ByVal Periodo As String, ByVal IdCarta As String, ByVal Usuario As String, ByVal IdLetra As String, ByVal TipoMov As String, ByVal IdRetencion As String, ByVal Serie As String, ByVal ConRetencion As Integer, ByVal TotRetenido As Double, ByVal pEmpresaRetencion As String, ByVal ID_DocPendiente As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, NroFormaPago)
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, IdChequera)
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, IdRendicion)
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, Anulado)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, NumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, AreaCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, IdCodigoCabLibro)
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, IdCodigoDetLibro)

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, Prestamo)
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, EmprPrestamo)
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, CCPrestamo)

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, Estado)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, TipoMov)

            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, IdTransaccion)
            'cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, IdCarta)
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, IdBancoDestino)
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, IdCuentaDestino)
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, GlosaDestino)
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, sCodigoLibroDestino)
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, sCodigoDetDestino)
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, Usuario)
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, IdLetra)
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, IdRetencion)
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, Serie)
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, ConRetencion)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, TotRetenido)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, pEmpresaRetencion)
            'cCapaDatos.sParametroSQL("@IdCorrelativoCajaBanco", DbType.Int64, 20, 0)

            'iResultado = cCapaDatos.fCmdExecuteNonQuery()
            iResultado = cCapaDatos.fCmdExecuteScalar()

            Dim VL_BeanResultado As New BeanResultado.ResultadoTransaccion()
            If ID_DocPendiente <> 0 Then
                Dim VL_SrvPagos As New SrvPagos
                VL_BeanResultado = VL_SrvPagos.Fnc_Registrar_Pagos(iResultado, ID_DocPendiente, EmprCodigo)

            End If

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function



    Public Function fEliminarItemDetalle(ByVal IdMovimiento As String, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 29)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fEliminarItemDetalles(ByVal IdMovimiento As String, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 31)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fGrabarDetalle(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String, ByVal NroFormaPago As String, ByVal Anulado As Integer, ByVal IdResumen As String, ByVal Ruc As String, ByVal NumeroSesion As Integer, ByVal AreaCodigo As String, ByVal IdCaja As String, ByVal IdCodigoCabLibro As String, ByVal IdCodigoDetLibro As String, ByVal EmprCodigopre As String, ByVal Estado As Integer, ByVal CCosCodigo2 As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, NroFormaPago)
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, IdRendicion)
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, Anulado)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, NumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, AreaCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, IdCodigoCabLibro)
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, IdCodigoDetLibro)

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 20, EmprCodigopre)
            cCapaDatos.sParametroSQL("@Estado", DbType.String, 20, Estado)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 20, CCosCodigo2)
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActCantDetCabecera(ByVal vOpcion As Integer, ByVal IdMovimiento As String, ByVal EmprCodigo As String, ByVal CantidadDet As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, CantidadDet)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fActEntregCheq(ByVal vOpcion As Integer, ByVal IdMovimiento As String, ByVal EmprCodigo As String, ByVal Usuario As String, ByVal iEntregado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, iEntregado)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, Usuario)
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function ResActCab(ByVal vOpcion As Integer, ByVal IdMovimiento As String, ByVal EmprCodigo As String, ByVal Ruc As String, ByVal IdResumen As String, ByVal Glosa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, Ruc)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActDetalle(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String, ByVal NroFormaPago As String, ByVal Anulado As Integer, ByVal IdResumen As String, ByVal Ruc As String, ByVal NumeroSesion As Integer, ByVal AreaCodigo As String, ByVal IdCaja As String, ByVal IdCodigoCabLibro As String, ByVal IdCodigoDetLibro As String, ByVal EmprCodigopre As String, ByVal Estado As Integer, ByVal CCosCodigo2 As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, NroFormaPago)
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, IdRendicion)
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, Anulado)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)

            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, NumeroSesion)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, AreaCodigo)
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, IdCaja)
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, IdCodigoCabLibro)
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, IdCodigoDetLibro)

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 20, EmprCodigopre)
            cCapaDatos.sParametroSQL("@Estado", DbType.String, 20, Estado)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 20, CCosCodigo2)
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            'NumeroSesion()
            'AreaCodigo
            'IdCaja

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEditarCheque(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, IdRendicion)
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEditarChequera(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdChequera As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdChequera)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, IdRendicion)
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fEditarResumen(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, IdRendicion)
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fActDetalleCronoIdChek(ByVal vOpcion As Integer, ByVal vFechaMovimiento As DateTime, ByVal IdMovimiento As String, ByVal IdBanco As String, ByVal IdCuenta As String, ByVal CCosCodigo As String, ByVal IdTipoAnexo As String, ByVal IdProveedor As String, ByVal IdTipoMovimiento As String, ByVal IdFormaPago As String, ByVal Monto As Double, ByVal Beneficiario As String, ByVal Glosa As String, ByVal IdTipoGasto As Integer, ByVal NroVoucher As String, ByVal TipoCambio As Double, ByVal IdCheque As String, ByVal IdRendicion As String, ByVal DocReferencia As String, ByVal DocOrigen As String, ByVal vMontoSoles As Decimal, ByVal vMontoDolares As Decimal, ByVal IdSesion As String, ByVal MonCodigo As String, ByVal EmprCodigo As String, ByVal PorcentajeDetr As Double, ByVal MontoDetr As Double, ByVal RetEmprCodigo As String, ByVal IdDetraccion As String, ByVal Serie As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(vFechaMovimiento, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, IdBanco)
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCuenta)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo)
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, IdTipoAnexo)
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, IdProveedor)
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, IdTipoMovimiento)
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdFormaPago)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, Beneficiario)
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, Glosa)
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, IdTipoGasto)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, NroVoucher)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, IdCheque)
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, Trim(IdRendicion))
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, DocReferencia)
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, DocOrigen)
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, vMontoSoles)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, vMontoDolares)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, IdSesion)
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, MonCodigo)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, IdDetraccion)
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, Serie)
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, PorcentajeDetr)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, MontoDetr)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, RetEmprCodigo)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'FECHA
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fCodigo(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo
    End Function


    Public Function fCodigoDetalleMov(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoDetMov = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo
    End Function

    Public Function fCodigoVouCher(ByVal CodEmpresa As String, ByVal Periodo As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, Periodo)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")

            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))

            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoVoucher = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fCodigoRendicion(ByVal CodEmpresa As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 19)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturoRendicion = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function



    Public Function fBuscarDoble(ByVal FechaMovimiento As DateTime, ByVal Codigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, FechaMovimiento) 'FECHA
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))
            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fListarCheque() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarEstadoMovimiento() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarTipoMovimiento() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")

            cCapaDatos.sAdaptadorSQL("R")

            dtTable = cCapaDatos.fListarTabla()

            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarCondicionPago() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarSesionCaja() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarPeriodo() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarMoneda() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 14)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarEmpresa() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarTipoComprobante() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 16)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarProveedor() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 17)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarCCosto() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 18)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd")) 'Fecha)
            cCapaDatos.sParametroSQL("@NombreUsuario", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdEstadoMovimiento", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NumeroCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Observacion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@IdPeriodo", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@TCompCodigo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function


    'Public Function fDetRes(ByVal iOpcion As Integer, ByVal IdCronograma As String, ByVal CodEmpresa As String) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
    '        cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
    '        cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdCronograma)
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
    '        cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
    '        cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
    '        cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
    '        cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
    '        cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
    '        cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
    '        cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
    '        cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
    '        cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
    '        cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
    '        cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

    '        cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
    '        cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

    '        cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
    '        cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")

    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function


    Public Function SaldoDoc(ByVal iOpcion As Integer, ByVal IdReg As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdReg)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function ActSaldoDoc(ByVal vOpcion As Integer, ByVal IdRegistro As String, ByVal CodEmpresa As String, ByVal Monto As Double, ByVal PPago As Integer, ByVal Cancelado As Integer, ByVal IdSituacion As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdRegistro)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, IdSituacion)
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, Monto)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, Cancelado)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, PPago)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function AnularDetResumen(ByVal vOpcion As Integer, ByVal IdResumen As String, ByVal IdDocumento As String, ByVal CodEmpresa As String, ByVal usuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdResumen)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdDocumento)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, usuario)
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function Eliminar(ByVal vOpcion As Integer, ByVal IdMovimiento As String, ByVal CodEmpresa As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function



    Public Function fContarContarDetallesMovimiento(ByVal IdMovimiento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspContarDetallesMovimiento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@CodEmpresa", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@NroMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroReg = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fDetRes(ByVal iOpcion As Integer, ByVal IdMovimiento As String, ByVal IdCronograma As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMovimiento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, IdCronograma)
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fListarDocsMovimiento(ByVal iOpcion As Integer, ByVal IdMoviento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMoviento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDocsMovimiento3(ByVal IdMoviento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 46)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMoviento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarDocsMovimiento2(ByVal IdMoviento As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspMovimientoCajaBanco"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 45)
            cCapaDatos.sParametroSQL("@FechaMovimiento", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@IdMovimiento", DbType.String, 20, IdMoviento)
            cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuenta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTipoAnexo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdProveedor", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@IdTipoMovimiento", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@IdFormaPago", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@NroFormaPago", DbType.String, 30, "")
            cCapaDatos.sParametroSQL("@Monto", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Beneficiario", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@Glosa", DbType.String, 400, "")
            cCapaDatos.sParametroSQL("@IdTipoGasto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@NroVaucher", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdCheque", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdChequera", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRendicion", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@DocumentoReferencia", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocumentoOrigen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoSoles", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDolares", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdSesion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Anulado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdResumen", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@NumeroSesion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@AreaCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@IdCaja", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoCabLibro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCodigoDetLibro", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@Prestamo", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@EmprPrestamo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@CCPrestamo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@EmprCodigopre", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Estado", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@CCosCodigo2", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@IdTransaccion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdCarta", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdBancoDestino", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@IdCuentaDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@GlosaDestino", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@sCodigoLibroDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@sCodigoDetDestino", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Periodo", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdLetra", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@ConRetencion", DbType.Int64, 1, 0)
            cCapaDatos.sParametroSQL("@TotRetenido", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@MontoDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@RetEmprCodigo", DbType.String, 2, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function


    Public Function fCodigoRetencion(ByVal vOpcion As Integer, ByVal EmprCodigo As String, ByVal Serie As String, ByVal NroIniSerie As String) As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000000000000000000"

        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRetenciones"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, Serie)
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Proveedor", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@SubTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If

            sCodFuturoRetencion = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

            If Trim(sCodFuturoRetencion) = "00000000000000000001" Then
                sCodFuturoRetencion = Trim(Right(sSufijo & Convert.ToString(NroIniSerie), sSufijo.Length))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo

    End Function

    Public Function fGrabarRetencion(ByVal vOpcion As Integer, ByVal IdRetencion As String, ByVal EmprCodigo As String, ByVal Serie As String, ByVal Numero As String, ByVal Fecha As DateTime, ByVal Proveedor As String, ByVal Direccion As String, ByVal RUC As String, ByVal Descripcion As String, ByVal SubTotal As Double, ByVal Porcentaje As Double, ByVal PorcentajeDetr As Double, ByVal TipoCambio As Double, ByVal Total As Double, ByVal Estado As Integer, ByVal Usuario As String, ByVal CCosCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRetenciones"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, IdRetencion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, EmprCodigo)
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, Serie)
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 7, Numero)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Fecha, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Proveedor", DbType.String, 250, Proveedor)
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 250, Direccion)
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, RUC)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 1000, Descripcion)
            cCapaDatos.sParametroSQL("@SubTotal", DbType.Decimal, 20, SubTotal)
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, Porcentaje)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, PorcentajeDetr)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, TipoCambio)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, Total)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, Usuario) '@CCosCodigo
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, CCosCodigo) '@CCosCodigo
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function

    Public Function fListarParametrosRetencion(ByVal iOpcion As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRetenciones"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Proveedor", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@SubTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fListarCCSerie(ByVal iOpcion As Integer, ByVal IdSerie As String, ByVal CodEmpresa As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspRetenciones"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdRetencion", DbType.String, 20, IdSerie)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
            cCapaDatos.sParametroSQL("@Serie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@Numero", DbType.String, 7, "")
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@Proveedor", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@Direccion", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@RUC", DbType.String, 11, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 1000, "")
            cCapaDatos.sParametroSQL("@SubTotal", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@PorcentajeDetr", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@TipoCambio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

End Class
