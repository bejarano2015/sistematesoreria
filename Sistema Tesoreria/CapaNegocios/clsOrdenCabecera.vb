Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsOrdenCabecera
    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal vOcoNumero As String, ByVal vEmprCodigo As String, ByVal vPrvRazonSocial As String, ByVal vREQFECHAEMISION01 As DateTime, ByVal vREQFECHAEMISION02 As DateTime, ByVal vEstadoC01 As Integer, ByVal vEstadoC02 As Integer, ByVal vEstadoC03 As Integer, ByVal vEstadoC04 As Integer, ByVal vOcoObservacion As String, ByVal vOcReqCodigo As String, ByVal vOcCotCodigo As String, ByVal vUsuCreacion As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, vOcoNumero)
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, vOcoObservacion)

            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, vEstadoC01)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, vEstadoC02)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, vEstadoC03)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, vEstadoC04)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(vREQFECHAEMISION01, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(vREQFECHAEMISION02, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, vOcReqCodigo)
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, vOcCotCodigo)

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, vUsuCreacion)
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            '@Percepcion  decimal(12,2)
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)

            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fGrabarOriguinal(ByVal vOcoTipoCompServ As String, ByVal vPdoCodigo As String, ByVal vPdoDescripcion As String, ByVal vEmprCodigo As String, ByVal vOcoFechaEmision As DateTime, ByVal vCCosCodigo As String, ByVal vCCosDescripcion As String, ByVal vPrvCodigo As String, ByVal vPrvRazonSocial As String, ByVal vPrvRUC As String, ByVal vConCodigo As String, ByVal vConContacto As String, ByVal vConMovil As String, ByVal vUsuEncargado As String, ByVal vUsuEncargadoAlias As String, ByVal vFechaEntrega As DateTime, ByVal vOcoLugarEntrega As String, ByVal vOcoPlazoEntrega As String, ByVal vTCambio As Decimal, ByVal vMoneda As String, ByVal vFormaPago As String, ByVal vOcoAdelanto As Decimal, ByVal vOcoEstado As String, ByVal vOcoParaQue As String, ByVal vOcoObservacion As String, ByVal vSubtotal As Decimal, ByVal vDscto As Decimal, ByVal vIGV As Decimal, ByVal vTotal As Decimal, ByVal vUsuario As String, ByVal vOcReqCodigo As String, ByVal vOcCotCodigo As String, ByVal vOcoNroCta As String, ByVal vOcoPDirecto As Integer, ByVal vOcoManual As Integer, ByVal vOcoCCI As String, ByVal vNacional As Integer, ByVal vFlag_Percepcion As Integer, ByVal vFlag_Detraccion As Integer, ByVal vMonCodigo As String, ByVal vFlag_IGV As String, ByVal vTasa_IGV As Decimal, ByVal vPercepcion As Decimal) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            'cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 1)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 51)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, vOcoTipoCompServ)
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, vPdoCodigo)
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, vPdoDescripcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(vOcoFechaEmision, "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, vCCosCodigo)
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, vCCosDescripcion)

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, vPrvCodigo)
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, vPrvRUC)
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, vConCodigo)
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, vConContacto)
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, vConMovil)
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, vUsuEncargado)
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, vUsuEncargadoAlias)
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(vFechaEntrega, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, vOcoLugarEntrega)
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, vOcoPlazoEntrega)
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, vTCambio)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, vMoneda)
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, vFormaPago)
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, vOcoAdelanto)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, vOcoEstado)
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, vOcoParaQue)
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, vOcoObservacion)
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, vSubtotal)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, vDscto)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, vIGV)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, vTotal)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, vOcReqCodigo)
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, vOcCotCodigo)

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, vOcoNroCta)

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, vOcoPDirecto)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, vOcoManual)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, vOcoCCI)
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, vNacional)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, vFlag_Percepcion)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, vFlag_Detraccion)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, vMonCodigo)
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, vFlag_IGV)
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, vTasa_IGV)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, vPercepcion)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fListarInsercionTemporal(ByVal vOcoCodigo As String, ByVal vEmprCodigo As String, ByVal vUsuario As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 2)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fBorradoTotal(ByVal vOcoCodigo As String, ByVal vEmprCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEdicionOriguinal(ByVal vOcoTipoCompServ As String, ByVal vPdoCodigo As String, ByVal vPdoDescripcion As String, ByVal vEmprCodigo As String, ByVal vOcoFechaEmision As DateTime, ByVal vCCosCodigo As String, ByVal vCCosDescripcion As String, ByVal vPrvCodigo As String, ByVal vPrvRazonSocial As String, ByVal vPrvRUC As String, ByVal vConCodigo As String, ByVal vConContacto As String, ByVal vConMovil As String, ByVal vUsuEncargado As String, ByVal vUsuEncargadoAlias As String, ByVal vFechaEntrega As DateTime, ByVal vOcoLugarEntrega As String, ByVal vOcoPlazoEntrega As String, ByVal vTCambio As Decimal, ByVal vMoneda As String, ByVal vFormaPago As String, ByVal vOcoAdelanto As Decimal, ByVal vOcoEstado As String, ByVal vOcoParaQue As String, ByVal vOcoObservacion As String, ByVal vSubtotal As Decimal, ByVal vDscto As Decimal, ByVal vIGV As Decimal, ByVal vTotal As Decimal, ByVal vUsuario As String, ByVal vOcoCodigo As String, ByVal vOcoNumero As String, ByVal vOcoNroCta As String, ByVal vOcoPDirecto As String, ByVal vOcoManual As Integer, ByVal vOcoCCI As String, ByVal vNacional As Integer, ByVal vFlag_Percepcion As Integer, ByVal vFlag_Detraccion As Integer, ByVal vMonCodigo As String, ByVal vFlag_IGV As String, ByVal vTasa_IGV As Decimal, ByVal vCODIGOGESTIONCab As String, ByVal vPercepcion As Decimal) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            'cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 52)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, vOcoNumero)
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, vOcoTipoCompServ)
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, vPdoCodigo)
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, vPdoDescripcion)
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(vOcoFechaEmision, "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, vCCosCodigo)
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, vCCosDescripcion)

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, vPrvCodigo)
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, vPrvRUC)
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, vConCodigo)
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, vConContacto)
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, vConMovil)
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, vUsuEncargado)
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, vUsuEncargadoAlias)
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(vFechaEntrega, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, vOcoLugarEntrega)
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, vOcoPlazoEntrega)
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, vTCambio)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, vMoneda)
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, vFormaPago)
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, vOcoAdelanto)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, vOcoEstado)
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, vOcoParaQue)
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, vOcoObservacion)
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, vSubtotal)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, vDscto)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, vIGV)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, vTotal)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")


            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, vOcoNroCta)

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, vOcoPDirecto)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, vOcoManual)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, vOcoCCI)
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, vNacional)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, vFlag_Percepcion)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, vFlag_Detraccion)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, vMonCodigo)
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, vFlag_IGV)
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, vTasa_IGV)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, vCODIGOGESTIONCab)
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, vPercepcion)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fBorradoTemporal(ByVal vUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fBorradoItem(ByVal vUsuario As String, ByVal vNroItem As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 8)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")



            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, vNroItem)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fListarDetalleTempo(ByVal vUsuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            'cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 9)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 50)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fCambiarEstadoItem(ByVal vOcoEstado As String, ByVal vUsuario As String, ByVal vDocoCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 10)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, vOcoEstado)
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, vDocoCodigo)

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fBorrarDesabilitados(ByVal vUsuario As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 11)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fActualizarPrecios(ByVal vDocoPrecioGrupalDetalle As Decimal, ByVal vDocoPjeDsctoGrupalDetalle As Decimal, ByVal vDocoDsctoGrupalDetalle As Decimal, ByVal vDocoImporteGrupalDetalle As Decimal, ByVal vDocoPrecioUniDetalle As Decimal, ByVal vDocoImporteUniDetalle As Decimal, ByVal vUsuario As String, ByVal vNroItem As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try

            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 12)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, vDocoPrecioGrupalDetalle)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, vDocoPjeDsctoGrupalDetalle)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, vDocoDsctoGrupalDetalle)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, vDocoImporteGrupalDetalle)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, vDocoPrecioUniDetalle)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, vDocoImporteUniDetalle)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, vNroItem)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEditarEstado(ByVal vOcoCodigo As String, ByVal vEmprCodigo As String, ByVal vOcoEstado As String, ByVal vOcoObservacion As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 13)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, vOcoEstado)
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, vOcoObservacion)
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEditarEstadoActivo(ByVal vOcoCodigo As String, ByVal vEmprCodigo As String, ByVal vOcoEstado As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 14)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, vOcoEstado)
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")


            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fListarEmpresaCCosto(ByVal vEmprCodigo As String, ByVal vCCosCodigo As String, ByVal vEstadoC01 As Integer, ByVal vEstadoC02 As Integer, ByVal vEstadoC03 As Integer, ByVal vEstadoC04 As Integer, ByVal vREQFECHAEMISION01 As DateTime, ByVal vREQFECHAEMISION02 As DateTime, ByVal vPrvRazonSocial As String, ByVal vOcoNumero As String, ByVal vcabProNombreComercial As String, ByVal vOcoEstadoOrden As String, ByVal vcabProCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 15)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, vOcoNumero)
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, vCCosCodigo)
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)



            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, vEstadoC01)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, vEstadoC02)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, vEstadoC03)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, vEstadoC04)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(vREQFECHAEMISION01, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(vREQFECHAEMISION02, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, vcabProNombreComercial)
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, vOcoEstadoOrden)

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, vcabProCodigo)

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function



    Public Function fListarRetrocederRequerimiento(ByVal vOcoCodigo As String, ByVal vEmprCodigo As String, ByVal vUsuario As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 18)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)


            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function


    Public Function fListarDetalleTempoOrden(ByVal vUsuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            'cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 19)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 54)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)



            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)


            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function



    Public Function fActualizarOrdenTotal(ByVal vUsuario As String, ByVal vOcoManual As Integer, ByVal vcabProCodigo As String) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 20)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")


            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, vOcoManual)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, vcabProCodigo)

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)


            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function


    Public Function fListarDetalleTempoOrdenActualizado(ByVal vUsuario As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 21)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)


            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function


    Public Function fListarInsercionTemporalRegitros(ByVal vOcoCodigo As String, ByVal vEmprCodigo As String, ByVal vUsuario As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 22)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")


            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)


            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function



    Public Function fListarInsercionTemporalCambioRegitros(ByVal vOcoCodigo As String, ByVal vEmprCodigo As String, ByVal vUsuario As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 23)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)


            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fListarEmpresaCCostoTotales(ByVal vCCosCodigo As String, ByVal vEstadoC01 As Integer, ByVal vEstadoC02 As Integer, ByVal vEstadoC03 As Integer, ByVal vEstadoC04 As Integer, ByVal vREQFECHAEMISION01 As DateTime, ByVal vREQFECHAEMISION02 As DateTime, ByVal vPrvRazonSocial As String, ByVal vOcoNumero As String, ByVal vcabProNombreComercial As String, ByVal vOcoEstadoOrden As String, ByVal vcabProCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 24)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, vOcoNumero)
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, vCCosCodigo)
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)



            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, vEstadoC01)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, vEstadoC02)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, vEstadoC03)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, vEstadoC04)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(vREQFECHAEMISION01, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(vREQFECHAEMISION02, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, vcabProNombreComercial)
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, vOcoEstadoOrden)

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, vcabProCodigo)

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarInsercionTemporalTotales(ByVal vOcoCodigo As String, ByVal vUsuario As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 25)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, vUsuario)
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")



            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)

            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function


    Public Function fListarEmpresaCCostoTotalesProveedor(ByVal vCCosCodigo As String, ByVal vEstadoC01 As Integer, ByVal vEstadoC02 As Integer, ByVal vEstadoC03 As Integer, ByVal vEstadoC04 As Integer, ByVal vREQFECHAEMISION01 As DateTime, ByVal vREQFECHAEMISION02 As DateTime, ByVal vPrvRazonSocial As String, ByVal vcabProNombreComercial As String, ByVal vcabProCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 26)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, vCCosCodigo)
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)



            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, vEstadoC01)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, vEstadoC02)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, vEstadoC03)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, vEstadoC04)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(vREQFECHAEMISION01, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(vREQFECHAEMISION02, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, vcabProNombreComercial)
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, vcabProCodigo)

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fListarEspecial(ByVal vOcoNumero As String, ByVal vEmprCodigo As String, ByVal vPrvRazonSocial As String, ByVal vREQFECHAEMISION01 As DateTime, ByVal vREQFECHAEMISION02 As DateTime, ByVal vEstadoC01 As Integer, ByVal vEstadoC02 As Integer, ByVal vEstadoC03 As Integer, ByVal vEstadoC04 As Integer, ByVal vUsuCreacion As String, ByVal vcabProCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 27)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, vOcoNumero)
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)



            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, vEstadoC01)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, vEstadoC02)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, vEstadoC03)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, vEstadoC04)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(vREQFECHAEMISION01, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(vREQFECHAEMISION02, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, vUsuCreacion)
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, vcabProCodigo)

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function


    Public Function fListarDetalleEspecial(ByVal vOcoCodigo As String, ByVal vcabProCodigo As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 28)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)



            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, vcabProCodigo)

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function


    Public Function fRetrocesoRequerimiento(ByVal vOcoCodigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComenzarTransaccion() '/Inicio de Transaccion
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            'cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 29)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 53)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, vOcoCodigo)
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")


            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)


            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, "")
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")

            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            'If iResultado >= "1" Then
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            'Else
            '    cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            'End If
            cCapaDatos.sConfirmarTransaccion() '/Confirmacion de Transaccion

        Catch ex As Exception
            cCapaDatos.sCancelarTransaccion() '/Cancelacion de Transaccion
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fListarUsuario(ByVal vOcoNumero As String, ByVal vEmprCodigo As String, ByVal vPrvRazonSocial As String, ByVal vREQFECHAEMISION01 As DateTime, ByVal vREQFECHAEMISION02 As DateTime, ByVal vEstadoC01 As Integer, ByVal vEstadoC02 As Integer, ByVal vEstadoC03 As Integer, ByVal vEstadoC04 As Integer, ByVal vOcoObservacion As String, ByVal vOcReqCodigo As String, ByVal vOcCotCodigo As String, ByVal vUsuCreacion As String) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Logistica.uspOrdenCabecera20"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)

            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 2, 30)
            cCapaDatos.sParametroSQL("@OcoCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@OcoSerie", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@OcoNumero", DbType.String, 15, vOcoNumero)
            cCapaDatos.sParametroSQL("@OcoTipoCompServ", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@PdoCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PdoDescripcion", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, vEmprCodigo)
            cCapaDatos.sParametroSQL("@OcoFechaEmision", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))

            cCapaDatos.sParametroSQL("@CCosCodigo", DbType.String, 10, "")
            cCapaDatos.sParametroSQL("@CCosDescripcion", DbType.String, 80, "")

            cCapaDatos.sParametroSQL("@PrvCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@PrvRazonSocial", DbType.String, 70, vPrvRazonSocial)
            cCapaDatos.sParametroSQL("@PrvRUC", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@ConCodigo", DbType.String, 3, "")
            cCapaDatos.sParametroSQL("@ConContacto", DbType.String, 70, "")
            cCapaDatos.sParametroSQL("@ConMovil", DbType.String, 18, "")
            cCapaDatos.sParametroSQL("@UsuEncargado", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@UsuEncargadoAlias", DbType.String, 150, "")
            cCapaDatos.sParametroSQL("@FechaEntrega", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@OcoLugarEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoPlazoEntrega", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@TCambio", DbType.Decimal, 15, 0)
            cCapaDatos.sParametroSQL("@Moneda", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FormaPago", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@OcoAdelanto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@OcoEstado", DbType.String, 15, "")
            cCapaDatos.sParametroSQL("@OcoParaQue", DbType.String, 250, "")
            cCapaDatos.sParametroSQL("@OcoObservacion", DbType.String, 150, vOcoObservacion)
            cCapaDatos.sParametroSQL("@Subtotal", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Dscto", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@IGV", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Total", DbType.Decimal, 14, 0)
            cCapaDatos.sParametroSQL("@Usuario", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@DocoCodigo", DbType.String, 10, "")

            cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 19, 0)
            cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 16, 0)
            cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 16, 0)




            'cCapaDatos.sParametroSQL("@DocoPrecioGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPjeDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoDsctoGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteGrupalDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoPrecioUniDetalle", DbType.Decimal, 14, 0)
            'cCapaDatos.sParametroSQL("@DocoImporteUniDetalle", DbType.Decimal, 14, 0)

            cCapaDatos.sParametroSQL("@EstadoC01", DbType.Int32, 1, vEstadoC01)
            cCapaDatos.sParametroSQL("@EstadoC02", DbType.Int32, 1, vEstadoC02)
            cCapaDatos.sParametroSQL("@EstadoC03", DbType.Int32, 1, vEstadoC03)
            cCapaDatos.sParametroSQL("@EstadoC04", DbType.Int32, 1, vEstadoC04)

            cCapaDatos.sParametroSQL("@REQFECHAEMISION01", DbType.DateTime, 10, Format(vREQFECHAEMISION01, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@REQFECHAEMISION02", DbType.DateTime, 10, Format(vREQFECHAEMISION02, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@NroItem", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@OcReqCodigo", DbType.String, 15, vOcReqCodigo)
            cCapaDatos.sParametroSQL("@OcCotCodigo", DbType.String, 50, vOcCotCodigo)

            cCapaDatos.sParametroSQL("@OcoNroCta", DbType.String, 50, "")

            cCapaDatos.sParametroSQL("@UsuCreacion", DbType.String, 20, vUsuCreacion)
            ' cCapaDatos.sParametroSQL("@OcoTipo", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@OcoPDirecto", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@OcoManual", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@cabProNombreComercial", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@OcoEstadoOrden", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@cabProCodigo", DbType.String, 29, "")

            cCapaDatos.sParametroSQL("@OcoCCI", DbType.String, 50, "")
            cCapaDatos.sParametroSQL("@Nacional", DbType.Int32, 1, 0)

            cCapaDatos.sParametroSQL("@Flag_Percepcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@Flag_Detraccion", DbType.Int32, 1, 0)

            '----------------- datos a incluir pero solo en logistica
            cCapaDatos.sParametroSQL("@MonCodigo", DbType.String, 2, "")
            cCapaDatos.sParametroSQL("@Flag_IGV", DbType.String, 1, "")
            cCapaDatos.sParametroSQL("@Tasa_IGV", DbType.Decimal, 26, 0)

            cCapaDatos.sParametroSQL("@CODIGOGESTIONCab", DbType.String, 13, "")
            cCapaDatos.sParametroSQL("@Percepcion", DbType.Decimal, 14, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function



    Public Function UpdateRequerimientoCantidades(ByVal vDrqCantUniPedida As Integer, ByVal vDrqCantGruPedida As Integer, _
                    ByVal vReqCodigo As String, ByVal vEmprCodigo As String, ByVal vLincodigo As String, ByVal vSubCodigo As String, ByVal vTipCodigo As String, _
                    ByVal vProCodigo As String, ByVal vDrqAgregado As String)
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        'sTextoSQL = "UPDATE Logistica.DetalleRequerimiento " & _
        '            "SET DrqCantUniPedida=" & vDrqCantUniPedida & ",DrqCantGruPedida=" & vDrqCantGruPedida & " " & _
        '            "WHERE NroItem='" & vNroItem & "' "

        sTextoSQL = "UPDATE Logistica.DetalleRequerimiento " & _
                    "SET DrqCantUniPedida=" & vDrqCantUniPedida & ",DrqCantGruPedida=" & vDrqCantGruPedida & " " & _
                    "WHERE ReqCodigo='" & vReqCodigo & "' and EmprCodigo='" & vEmprCodigo & "' and LinCodigo='" & vLincodigo & "' and SubCodigo='" & vSubCodigo & "' and TipCodigo='" & vTipCodigo & "'  and ProCodigo='" & vProCodigo & "' and DrqAgregado='" & vDrqAgregado & "'"


        '"CantidadUniContada=@cCantidadUniContada,CantidadGrupoContada=@cCantidadGrupoContada " & _

        Try
            cCapaDatos.sConectarSQL()
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            cCapaDatos.fCmdExecuteNonQuery()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function


    Public Function PresentacionXItem(ByVal vEmprCodigo As String, ByVal vReqCodigo As String, ByVal vCodMaterial As String, ByVal vDrqAgrega As String) As DataTable
        Dim sUmdCodigo As String
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable

        Try
            cCapaDatos.sConectarSQL()

            '"SELECT RTRIM(LinCodigo)+RTRIM(SubCodigo)+RTRIM(TipCodigo)+RTRIM(ProCodigo),UmdDescripcion "
            sTextoSQL = "SELECT UmdDescripcion " & _
                        "FROM logistica.DetalleRequerimiento " & _
                        "WHERE EmprCodigo='" & vEmprCodigo & "' AND ReqCodigo='" & vReqCodigo & "' AND " & _
                        "RTRIM(LinCodigo)+RTRIM(SubCodigo)+RTRIM(TipCodigo)+RTRIM(ProCodigo)='" & vCodMaterial & "' AND DrqAgregado='" & vDrqAgrega & "'"

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            sUmdCodigo = cCapaDatos.fCmdExecuteScalar

            sTextoSQL = ""
            sTextoSQL = "SELECT um.ProCodigoBarrasExt,um.UmdEquivalencia as Equivalencia,um.umdMinima as Minima, " & _
                        "(ISNULL(dr.DrqCantUnitaria,0) - ISNULL(dr.DrqCantUniPedida,0)) as SalUniReq, (ISNULL(dr.DrqCantGrupal,0) - ISNULL(dr.DrqCantGruPedida,0)) as SalGruReq " & _
                        "FROM logistica.DetalleRequerimiento dr " & _
                        "INNER JOIN Logistica.unidadesmateriales um ON um.ProCodigoBarrasExt=RTRIM(dr.LinCodigo)+RTRIM(dr.SubCodigo)+RTRIM(dr.TipCodigo)+RTRIM(dr.ProCodigo) AND um.UmdCodigo= dr.UmdDescripcion " & _
                        "WHERE dr.EmprCodigo='" & vEmprCodigo & "' AND dr.ReqCodigo='" & vReqCodigo & "' " & _
                        "AND RTRIM(dr.LinCodigo)+RTRIM(dr.SubCodigo)+RTRIM(dr.TipCodigo)+RTRIM(dr.ProCodigo)='" & vCodMaterial & "' and dr.UmdDescripcion='" & sUmdCodigo & "' AND dr.DrqAgregado='" & vDrqAgrega & "'"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla
            Return dtTable
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        End Try
    End Function

    Public Function PresentacionXItemTempo(ByVal vCodMaterial As String, ByVal vUmdCodigo As String) As Double
        Dim EquivalTempo As Double
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "SELECT UmdEquivalencia as Equivalencia " & _
                        "FROM logistica.unidadesmateriales " & _
                        "where umdcodigo='" & vUmdCodigo & "' and procodigobarrasext='" & vCodMaterial & "'"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            EquivalTempo = cCapaDatos.fCmdExecuteScalar
            Return EquivalTempo
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function

    Public Function UnidadXItemTempo(ByVal vCodMaterial As String, ByVal vUmdCodigo As String) As Double
        Dim EquiUniTempo As Double
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "SELECT Umdminima " & _
                        "FROM logistica.unidadesmateriales " & _
                        "where umdcodigo='" & vUmdCodigo & "' and procodigobarrasext='" & vCodMaterial & "'"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            EquiUniTempo = cCapaDatos.fCmdExecuteScalar
            Return EquiUniTempo
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function


    Public Function CantidadGrupal(ByVal vCodEmpresa As String, ByVal vCodOcoNumero As String, ByVal vCodRequerimiento As String, ByVal vCodMaterial As String, ByVal vDrqAgre As String) As Double
        Dim EquivalTempo As Double
        Dim E1 As Double
        Dim E2 As Double
        Dim SUMCANTGRUPOSOL As Double
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            'sTextoSQL = "SELECT ISNULL(CantidadGrupoSol,0) " & _
            '"FROM logistica.ordendetalle " & _
            '"WHERE emprcodigo='" & vCodEmpresa & "' AND OcoNumero='" & vCodOcoNumero & "' AND ReqCodigo='" & vCodRequerimiento & "' AND RTRIM(LinCodigo)+RTRIM(SubCOdigo)+RTRIM(TipCodigo)+RTRIM(ProCodigo)='" & vCodMaterial.Trim & "' "
            'and d.DrqAgregado='-' 

            'sTextoSQL = "SELECT (isnull(sum(d.cantidadGrupoSol),0)*um2.umdequivalencia)/um.umdequivalencia as cantidadGrupoSol " & _
            '            "FROM Logistica.ordenDetalle d " & _
            '            "INNER JOIN Logistica.OrdenCabecera c on c.oconumero=d.oconumero " & _
            '            "INNER JOIN logistica.detallerequerimiento dr on c.ocreqcodigo=dr.reqcodigo " & _
            '            "INNER JOIN logistica.unidadesmateriales um on rtrim(dr.lincodigo)+rtrim(dr.subcodigo)+rtrim(dr.tipcodigo)+rtrim(dr.procodigo)= um.procodigobarrasext And dr.umddescripcion = um.umdcodigo " & _
            '            "INNER JOIN logistica.unidadesmateriales um2 on rtrim(d.lincodigo)+rtrim(d.subcodigo)+rtrim(d.tipcodigo)+rtrim(d.procodigo)=um2.procodigobarrasext and d.umddescripcion=um2.umdcodigo " & _
            '            "WHERE d.EmprCodigo='" & vCodEmpresa & "' AND d.OcoNumero='" & vCodOcoNumero & "' AND dr.ReqCodigo='" & vCodRequerimiento & "' " & _
            '            "and RTRIM(d.LinCodigo) + RTRIM(d.SubCodigo) + RTRIM(d.TipCodigo) + RTRIM(d.ProCodigo)='" & vCodMaterial & "' " & _
            '            "GROUP BY d.cantidadGrupoSol ,um.umdequivalencia,dr.umddescripcion,d.umddescripcion,um2.umdequivalencia"


            '*****************RSYS

            sTextoSQL = "SELECT u.umdequivalencia FROM LOGISTICA.ordenDetalle o INNER JOIN logistica.unidadesmateriales u ON u.ProCodigoBarrasExt=rtrim(o.lincodigo)+rtrim(o.subcodigo)+rtrim(o.tipcodigo)+rtrim(o.procodigo) AND u.UmdCodigo=o.UmdDescripcion WHERE o.OcoNumero='" & vCodOcoNumero & "' AND o.ReqCodigo='" & vCodRequerimiento & "' AND o.EMPRCODIGO='" & vCodEmpresa & "' AND RTRIM(o.lincodigo)+RTRIM(o.subcodigo)+RTRIM(o.tipcodigo)+RTRIM(o.procodigo)='" & vCodMaterial & "' AND o.DrqAgregado='" & vDrqAgre & "'"
            'o.DocoCodigo=@nuevoDocoCodigo AND
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            E1 = cCapaDatos.fCmdExecuteScalar
            sTextoSQL = ""

            sTextoSQL = "SELECT u.umdequivalencia FROM LOGISTICA.detallerequerimiento d INNER JOIN logistica.unidadesmateriales u ON u.ProCodigoBarrasExt=rtrim(d.lincodigo)+rtrim(d.subcodigo)+RTrim(d.tipcodigo)+RTrim(d.procodigo) And u.UmdCodigo = d.UmdDescripcion WHERE d.EMPRCODIGO='" & vCodEmpresa & "' AND d.ReqCodigo='" & vCodRequerimiento & "' AND RTRIM(d.LinCodigo) + RTRIM(d.SubCodigo)+ RTRIM(d.TipCodigo) + RTRIM(d.ProCodigo)='" & vCodMaterial & "' AND d.DrqAgregado='" & vDrqAgre & "'"

            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            E2 = cCapaDatos.fCmdExecuteScalar
            sTextoSQL = ""

            sTextoSQL = "SELECT ISNULL(d.cantidadGrupoSol,0) as cantidadGrupoSol FROM Logistica.ordenDetalle d INNER JOIN Logistica.OrdenCabecera c on c.oconumero=d.oconumero and c.ocoestado<>'3' WHERE d.oconumero='" & vCodOcoNumero & "' AND d.ReqCodigo='" & vCodRequerimiento & "' and RTRIM(d.LinCodigo)+RTRIM(d.SubCodigo)+RTRIM(d.TipCodigo)+RTRIM(d.ProCodigo)='" & vCodMaterial & "' and d.DrqAgregado='" & vDrqAgre & "'"
            'd.DocoCodigo=@NuevoDocoCodigo AND
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            SUMCANTGRUPOSOL = cCapaDatos.fCmdExecuteScalar

            EquivalTempo = (SUMCANTGRUPOSOL * E1) / E2





            'cCapaDatos.sComandoSQL(sTextoSQL, CommandType.Text)
            'EquivalTempo = cCapaDatos.fCmdExecuteScalar
            Return EquivalTempo
        Catch ex As Exception
            MsgBox(ex.Message)
            Return 0
        End Try
    End Function

End Class
