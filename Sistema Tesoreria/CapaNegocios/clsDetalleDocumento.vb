Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsDetalleDocumento

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar(ByVal vEstado01 As Integer, ByVal vEstado02 As Integer) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetalleDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, vEstado01)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, vEstado02)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdItem", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cantidad", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@Precio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, 0)
            cCapaDatos.sParametroSQL("@MontoTotal", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@MontoDescuento", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, 0)
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return dtTable
    End Function

    Public Function fGrabar(ByVal VIdItem As String, ByVal VDescripcion As String, ByVal VCantidad As Integer, ByVal VPrecio As Decimal, ByVal VIdRegistro As String, ByVal VMontoTotal As Decimal, ByVal VMontoDescuento As Decimal, ByVal VEdocCodigo As String, ByVal VOpcion As Integer, ByVal VEstado As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetalleDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, VOpcion)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, VEstado)
            cCapaDatos.sParametroSQL("@IdItem", DbType.String, 20, VIdItem)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, VDescripcion)
            cCapaDatos.sParametroSQL("@Cantidad", DbType.Int32, 10, VCantidad)
            cCapaDatos.sParametroSQL("@Precio", DbType.Decimal, 20, VPrecio)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, VIdRegistro)
            cCapaDatos.sParametroSQL("@MontoTotal", DbType.Decimal, 10, VMontoTotal)
            cCapaDatos.sParametroSQL("@MontoDescuento", DbType.Decimal, 10, VMontoDescuento)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, VEdocCodigo)
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetalleDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdItem", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cantidad", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@Precio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoTotal", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@MontoDescuento", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, "")
            iResultado = cCapaDatos.fCmdExecuteNonQuery()

            If iResultado = 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado
    End Function

    Public Function fCodigo() As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "000"
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetalleDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 6)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdItem", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, "")
            cCapaDatos.sParametroSQL("@Cantidad", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@Precio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoTotal", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@MontoDescuento", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, "")
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iCodigo
    End Function

    Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer

        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos

        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspDetalleDocumento"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
            cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdItem", DbType.String, 20, Codigo)
            cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 200, Descripcion)
            cCapaDatos.sParametroSQL("@Cantidad", DbType.Int32, 10, 0)
            cCapaDatos.sParametroSQL("@Precio", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@IdRegistro", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@MontoTotal", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@MontoDescuento", DbType.Decimal, 10, 0)
            cCapaDatos.sParametroSQL("@EdocCodigo", DbType.String, 2, "")
            iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))

            If iResultado > 0 Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
            End If

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try

        Return iResultado

    End Function

End Class
