Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsITF
    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo As String
    Public iNroRegistros As Integer = "0"

    Public Function fListar() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspItf"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IdItf", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@A�o", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fCodigo() As String
        Dim iCodigo As String = ""
        Dim sSufijo As String = "00000"
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspItf"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 3)
            cCapaDatos.sParametroSQL("@IdItf", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@A�o", DbType.String, 4, "")
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            iCodigo = Trim(Convert.ToString(cCapaDatos.fCmdExecuteScalar()))
            If iCodigo = "" Then
                iCodigo = 1
            Else
                iCodigo += 1
            End If
            sCodFuturo = Trim(Right(sSufijo & Convert.ToString(iCodigo), sSufijo.Length))
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iCodigo
    End Function

    Public Function fGrabar(ByVal iOpcion As Integer, ByVal IdITF As String, ByVal A�o As String, ByVal ITF As Double, ByVal FechaIni As DateTime, ByVal FechaFin As DateTime) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspItf"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
            cCapaDatos.sParametroSQL("@IdItf", DbType.String, 5, IdITF)
            cCapaDatos.sParametroSQL("@A�o", DbType.String, 4, A�o)
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, ITF)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaIni", DbType.DateTime, 10, Format(FechaIni, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(FechaFin, "yyyy-MM-dd"))
            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado
    End Function


    Public Function fBuscarDoble(ByVal A�o As String, ByVal FechaITF As DateTime) As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspItf"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 4)
            cCapaDatos.sParametroSQL("@IdItf", DbType.String, 5, "")
            cCapaDatos.sParametroSQL("@A�o", DbType.String, 4, A�o)
            cCapaDatos.sParametroSQL("@Porcentaje", DbType.Decimal, 20, 0)
            cCapaDatos.sParametroSQL("@Fecha", DbType.DateTime, 10, Format(FechaITF, "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaIni", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sParametroSQL("@FechaFin", DbType.DateTime, 10, Format(Today(), "yyyy-MM-dd"))
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

   

    'Public Function fBuscarCod(ByVal vCod As String, ByVal CodEmpr As String) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspBanco"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 7)
    '        cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, vCod)
    '        cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
    '        cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@EmailBanco", DbType.String, 80, "")
    '        cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
    '        cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try

    '    Return dtTable
    'End Function



    'Public Function fEliminar(ByVal Codigo As String, ByVal vOpcion As Integer, ByVal CodEmpresa As String) As Integer
    '    Dim iResultado As Int32 = 0
    '    cCapaDatos = New clsCapaDatos
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspBanco"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
    '        cCapaDatos.sParametroSQL("@Estado01", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@Estado02", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, Codigo)
    '        cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
    '        cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@EMailBanco", DbType.String, 80, "")
    '        cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpresa)
    '        cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
    '        iResultado = cCapaDatos.fCmdExecuteNonQuery()
    '        If iResultado = 0 Then
    '            cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoDisponible))
    '        Else
    '            cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgEliminacion))
    '        End If
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return iResultado
    'End Function



    'Public Function fBuscarDoble(ByVal Descripcion As String, ByVal Codigo As String) As Integer
    '    Dim iResultado As Int32 = 0
    '    cCapaDatos = New clsCapaDatos
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspBanco"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@OPCION", DbType.Int32, 1, 7)
    '        cCapaDatos.sParametroSQL("@ESTADO01", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@ESTADO02", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@ESTADO", DbType.Int32, 1, 0)
    '        cCapaDatos.sParametroSQL("@IdBanco", DbType.String, 3, Codigo)
    '        cCapaDatos.sParametroSQL("@NombreBanco", DbType.String, 150, Descripcion)
    '        cCapaDatos.sParametroSQL("@Abreviado", DbType.String, 15, "")
    '        cCapaDatos.sParametroSQL("@RucBanco", DbType.String, 20, "")
    '        cCapaDatos.sParametroSQL("@DireccionBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@TelefonoBanco", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@FaxBanco", DbType.String, 50, "")
    '        cCapaDatos.sParametroSQL("@EMailBanco", DbType.String, 80, "")
    '        cCapaDatos.sParametroSQL("@WebBanco", DbType.String, 150, "")
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, "")
    '        cCapaDatos.sParametroSQL("@TipoAnaliticoCodigo", DbType.String, 3, "")
    '        cCapaDatos.sParametroSQL("@CodMoneda", DbType.String, 2, "")
    '        iResultado = Trim(Convert.ToUInt32(cCapaDatos.fCmdExecuteScalar()))
    '        If iResultado > 0 Then
    '            cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgDuplicado))
    '        End If
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return iResultado
    'End Function


    'Public Function fBuscarBancos(ByVal iOpcion As Integer, ByVal CodEmpr As String, ByVal Ruc As String, ByVal Descri As String, ByVal Estado As Integer) As DataTable
    '    cCapaDatos = New clsCapaDatos
    '    dtTable = New DataTable
    '    Try
    '        cCapaDatos.sConectarSQL()
    '        sTextoSQL = "Tesoreria.uspBuscarBancos"
    '        cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
    '        cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, iOpcion)
    '        cCapaDatos.sParametroSQL("@EmprCodigo", DbType.String, 2, CodEmpr)
    '        cCapaDatos.sParametroSQL("@Ruc", DbType.String, 11, Ruc)
    '        cCapaDatos.sParametroSQL("@Descripcion", DbType.String, 25, Descri)
    '        cCapaDatos.sParametroSQL("@Estado", DbType.Int32, 1, Estado)
    '        cCapaDatos.sAdaptadorSQL("R")
    '        dtTable = cCapaDatos.fListarTabla()
    '        iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)
    '    Catch ex As Exception
    '        cTempo.sMensaje(Convert.ToString(ex.Message))
    '    Finally
    '        cCapaDatos.sDesconectarSQL()
    '    End Try
    '    Return dtTable
    'End Function

End Class
