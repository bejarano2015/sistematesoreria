Imports System.Data
Imports System.Data.SqlClient
Imports CapaDatos

Public Class clsTipoDestino

    Private cTempo As New clsPlantTempo
    Private cCapaDatos As clsCapaDatos
    Private dtTable As DataTable
    Private sTextoSQL As String
    Public sCodFuturo, sSiguienteIdSesion, sCodIdDetalleMovimientoFuturo, sCodVale As String
    Public iNroRegistros As Integer = "0"
    Public iNroRegistrosDetHoy As Integer = "0"
    Public sCadena As Integer

    Public Function fCargarTipoDestino() As DataTable
        cCapaDatos = New clsCapaDatos
        dtTable = New DataTable
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, 0)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, 0)
            cCapaDatos.sParametroSQL("@NOMBRE", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@ABREAVIATURA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FLGESTADO", DbType.String, 3, "")
            cCapaDatos.sAdaptadorSQL("R")
            dtTable = cCapaDatos.fListarTabla()
            iNroRegistros = Convert.ToInt32(cCapaDatos.fListarTabla.DefaultView.Count)

        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return dtTable
    End Function

    Public Function fGrabar(ByVal vIdTipoDestino As Decimal, ByVal vNombre As String, ByVal vAbreviatura As String, ByVal vFlgEstado As String, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, vIdTipoDestino)
            cCapaDatos.sParametroSQL("@NOMBRE", DbType.String, 100, vNombre)
            cCapaDatos.sParametroSQL("@ABREAVIATURA", DbType.String, 20, vAbreviatura)
            cCapaDatos.sParametroSQL("@FLGESTADO", DbType.String, 3, vFlgEstado)

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

    Public Function fEliminar(ByVal vIdTipoDestino As Decimal, ByVal vOpcion As Integer) As Integer
        Dim iResultado As Int32 = 0
        cCapaDatos = New clsCapaDatos
        Try
            cCapaDatos.sConectarSQL()
            sTextoSQL = "Tesoreria.uspTipoDestino"
            cCapaDatos.sComandoSQL(sTextoSQL, CommandType.StoredProcedure)
            cCapaDatos.sParametroSQL("@Opcion", DbType.Int32, 1, vOpcion)
            cCapaDatos.sParametroSQL("@IDTIPODESTINO", DbType.Decimal, 18, vIdTipoDestino)
            cCapaDatos.sParametroSQL("@NOMBRE", DbType.String, 100, "")
            cCapaDatos.sParametroSQL("@ABREAVIATURA", DbType.String, 20, "")
            cCapaDatos.sParametroSQL("@FLGESTADO", DbType.String, 3, "")

            iResultado = cCapaDatos.fCmdExecuteNonQuery()
            If iResultado >= "1" Then
                'cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgGrabacion))
            Else
                cTempo.sMensaje(Convert.ToString(clsPlantTempo.sMsgNoGrabacion))
            End If
        Catch ex As Exception
            cTempo.sMensaje(Convert.ToString(ex.Message))
        Finally
            cCapaDatos.sDesconectarSQL()
        End Try
        Return iResultado

    End Function

End Class

